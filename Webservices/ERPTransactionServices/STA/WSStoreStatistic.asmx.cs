﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
namespace ERPTransactionServices.STA
{
    /// <summary>
    /// Summary description for WSStoreStatistic
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSStoreStatistic : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Statistic.BO.STA.StoreStatistic objStoreStatistic,  DateTime? dtmStatisticDate, int intStoreID, int intStoreStatisticTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);            
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Statistic.DA.STA.DA_StoreStatistic().LoadInfo(ref objStoreStatistic,  dtmStatisticDate,  intStoreID,  intStoreStatisticTypeID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Statistic.BO.STA.StoreStatistic objStoreStatistic)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Statistic.DA.STA.DA_StoreStatistic objDA_StoreStatistic = new ERP.Statistic.DA.STA.DA_StoreStatistic();
            objDA_StoreStatistic.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreStatistic.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreStatistic.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreStatistic.CreatedUser = objToken.UserName;
            objResultMessage = objDA_StoreStatistic.Insert(objStoreStatistic);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Statistic.BO.STA.StoreStatistic objStoreStatistic)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Statistic.DA.STA.DA_StoreStatistic objDA_StoreStatistic = new ERP.Statistic.DA.STA.DA_StoreStatistic();
            objDA_StoreStatistic.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreStatistic.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreStatistic.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreStatistic.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_StoreStatistic.Update(objStoreStatistic);
            return objResultMessage;
        }
        
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Statistic.BO.STA.StoreStatistic objStoreStatistic)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Statistic.DA.STA.DA_StoreStatistic objDA_StoreStatistic=new ERP.Statistic.DA.STA.DA_StoreStatistic();
            objDA_StoreStatistic.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreStatistic.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreStatistic.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreStatistic.DeletedUser = objToken.UserName;
            objResultMessage = objDA_StoreStatistic.Delete(objStoreStatistic);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Statistic.DA.STA.DA_StoreStatistic().SearchData(ref tblResult,  objKeywords);
            if (tblResult != null)
                tblResult.TableName = "StoreStatisticList";
            return objResultMessage;
        }
    
    }
}
