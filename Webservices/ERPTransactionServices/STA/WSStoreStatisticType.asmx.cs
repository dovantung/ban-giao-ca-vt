﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
namespace ERPTransactionServices.STA
{
    /// <summary>
    /// Summary description for WSStoreStatisticType
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSStoreStatisticType : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Statistic.BO.STA.StoreStatisticType objStoreStatisticType, int intStoreStatisticTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);            
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Statistic.DA.STA.DA_StoreStatisticType().LoadInfo(ref objStoreStatisticType, intStoreStatisticTypeID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Statistic.BO.STA.StoreStatisticType objStoreStatisticType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Statistic.DA.STA.DA_StoreStatisticType objDA_StoreStatisticType = new ERP.Statistic.DA.STA.DA_StoreStatisticType();
            objDA_StoreStatisticType.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreStatisticType.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreStatisticType.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreStatisticType.CreatedUser = objToken.UserName;
            objResultMessage = objDA_StoreStatisticType.Insert(objStoreStatisticType);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearStoreStatisticTypeCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Statistic.BO.STA.StoreStatisticType objStoreStatisticType, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Statistic.DA.STA.DA_StoreStatisticType objDA_StoreStatisticType = new ERP.Statistic.DA.STA.DA_StoreStatisticType();
            objDA_StoreStatisticType.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreStatisticType.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreStatisticType.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreStatisticType.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_StoreStatisticType.Update(objStoreStatisticType,lstOrderIndex);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearStoreStatisticTypeCache();
            return objResultMessage;
        }
        
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intStoreStatisticTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Statistic.BO.STA.StoreStatisticType objStoreStatisticType = new ERP.Statistic.BO.STA.StoreStatisticType();
            ERP.Statistic.DA.STA.DA_StoreStatisticType objDA_StoreStatisticType=new ERP.Statistic.DA.STA.DA_StoreStatisticType();
            objDA_StoreStatisticType.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreStatisticType.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreStatisticType.objLogObject.LoginLogID = objToken.LoginLogID;            
            objStoreStatisticType.StoreStatisticTypeID = intStoreStatisticTypeID;
            objStoreStatisticType.DeletedUser = objToken.UserName;
            objResultMessage = objDA_StoreStatisticType.Delete(objStoreStatisticType);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearStoreStatisticTypeCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Statistic.DA.STA.DA_StoreStatisticType().SearchData(ref tblResult,  objKeywords);
            if (tblResult != null)
                tblResult.TableName = "StoreStatisticTypeList";
            return objResultMessage;
        }
    
    }
}
