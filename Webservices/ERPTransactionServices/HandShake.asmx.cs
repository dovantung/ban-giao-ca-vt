﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Globalization;
using System.Web.Caching;
using Library.WebCore;

namespace ERPTransactionServices
{
    /// <summary>
    /// Summary description for HandShake
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class HandShake : System.Web.Services.WebService
    {

        private const int KEYLENGTH = 32; // Chiều dài khóa phát sinh trao đổi
        protected static double dbHandShakeCacheHour = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["HandShakeCacheHour"]) * 5;
        protected static int intHandShakeMemCacheHour = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["HandShakeMemCacheHour"]);

        [WebMethod]
        public String KeyExchange(String strClientResponse)
        {
            //CoreLibs.Globals.ERROR_LOG_FILE = Server.MapPath("acceslog.log");
            //System.IO.File.AppendAllText(Server.MapPath("acceslog.log"), HttpContext.Current.Request.ServerVariables["LOGON_USER"] + "-" + HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] + "-" + HttpContext.Current.Request.ServerVariables["REMOTE_HOST"] + Environment.NewLine);

            DiffieHellman.DiffieHellman server = new DiffieHellman.DiffieHellman(KEYLENGTH);
            server.GenerateResponse(strClientResponse);

            String strGUID = Guid.NewGuid().ToString();

            //HttpContext.Current.Cache[strGUID] = Convert.ToBase64String(server.Key);
            ProgramCache.AddCache(strGUID, Convert.ToBase64String(server.Key), dbHandShakeCacheHour, intHandShakeMemCacheHour);

            CultureInfo objCultureInfo = new CultureInfo("vi-VN");

            return server.ToString() + "|" + strGUID + "|" + DateTime.Now.ToString(objCultureInfo);

        }

        [WebMethod]
        public String GetServerTime()
        {
            CultureInfo objCultureInfo = new CultureInfo("vi-VN");
            return DateTime.Now.ToString(objCultureInfo.DateTimeFormat);
        }

        [WebMethod(EnableSession = true)]
        public bool CheckHandShake(String strGUID)
        {
            String strKey = Convert.ToString(ProgramCache.GetCache(strGUID)).Trim();
            return (strKey.Length > 0);
        }

        [WebMethod(EnableSession = true)]
        public String CheckHandShakeExt(String strGUID)
        {
            String strKey = Convert.ToString(ProgramCache.GetCache(strGUID)).Trim();
            if (strKey.Length > 0)
            {
                CultureInfo objCultureInfo = new CultureInfo("vi-VN");
                return DateTime.Now.ToString(objCultureInfo.DateTimeFormat);
            }
            return "";
        }
    }
}
