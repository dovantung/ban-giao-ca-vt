﻿namespace ERPTransactionServices.SYS
{

    using ERP.EOffice.BO.SYS;
    using ERP.EOffice.DA.SYS;
    using Library.WebCore;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Web.Services;

    /// <summary>
    ///     <para>Author: byrs(datnguyen0705@gmail.com)</para>
    ///     <para>Date created: aug 26, 2013</para>
    ///     <para>Description: </para>
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class WSError : System.Web.Services.WebService
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authenData"></param>
        /// <param name="guid"></param>
        /// <param name="resultMessage"></param>
        /// <param name="moduleName"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public List<SysErrorBO> GetByModule(String authenData, String guid, ref ResultMessage resultMessage, String moduleName, Int32 pageIndex, Int32 pageSize)
        {
            resultMessage = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                authenData, guid, ref resultMessage);
            if (objToken == null) return null;

            return SysErrorDA.Current.GetByModule(ref resultMessage, moduleName, pageIndex, pageSize);
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage WriteLog(String authenData, String guid, ResultMessage objResultMessage)
        {
            ResultMessage rtmp = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                authenData, guid, ref rtmp);
            if (objToken != null)
            {
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", "ERPTransactionServices.EO");
            }
            return rtmp;
            
        }

    }

}