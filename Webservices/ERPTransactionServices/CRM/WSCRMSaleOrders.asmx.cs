﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.CRM.BO;
using ERP.CRM.DA;
using System.Data;
namespace ERPTransactionServices.CRM
{
    /// <summary>
    /// Summary description for WSCRMSaleOrders
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSCRMSaleOrders : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref CRMSaleOrder objCRMSaleOrder, string strCRMSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_CRMSaleOrder().LoadInfo(ref objCRMSaleOrder, strCRMSaleOrderID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoNew(String strAuthenData, String strGUID, ref CRMSaleOrder objCRMSaleOrder, string strCRMSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_CRMSaleOrder().LoadInfo_NEW(ref objCRMSaleOrder, strCRMSaleOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateCRMSaleOrderID(String strAuthenData, String strGUID, ref string strCRMSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrder objDA_CRMSaleOrder = new DA_CRMSaleOrder();
            objResultMessage = objDA_CRMSaleOrder.CreateCRMSaleOrderID(ref strCRMSaleOrderID, objToken.LoginStoreID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true, Description = "Tạo đơn hàng CRM", MessageName = "Tao_don_hang_CRM")]
        public Library.WebCore.ResultMessage CreateCRMSaleOrder(String strAuthenData, String strGUID, ref string strCRMSaleOrderID, CRMSaleOrder objCRMSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrder objDA_CRMSaleOrder = new DA_CRMSaleOrder();
            objDA_CRMSaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CRMSaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CRMSaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objCRMSaleOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_CRMSaleOrder.CreateCRMSaleOrder(objCRMSaleOrder);
            strCRMSaleOrderID = objCRMSaleOrder.CRMSaleOrderID;
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrder objDA_CRMSaleOrder = new DA_CRMSaleOrder();
            objResultMessage = objDA_CRMSaleOrder.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "CRMSaleOrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewList(String strAuthenData, String strGUID, CRMSaleOrder_ReviewList objCRMSaleOrder_ReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrder_ReviewList objDA_CRMSaleOrder_ReviewList = new DA_CRMSaleOrder_ReviewList();
            objResultMessage = objDA_CRMSaleOrder_ReviewList.UpdateReviewList(objCRMSaleOrder_ReviewList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage ProcessWorkflow(String strAuthenData, String strGUID, CRMSaleOrder_WorkFlow objCRMSaleOrder_WorkFlow)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrder_WorkFlow objDA_CRMSaleOrder_WorkFlow = new DA_CRMSaleOrder_WorkFlow();
            objResultMessage = objDA_CRMSaleOrder_WorkFlow.Process(objCRMSaleOrder_WorkFlow);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, CRMSaleOrder objCRMSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objCRMSaleOrder.DeletedUser = objToken.UserName;
            DA_CRMSaleOrder objDA_CRMSaleOrder = new DA_CRMSaleOrder();
            objDA_CRMSaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CRMSaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CRMSaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_CRMSaleOrder.Delete(objCRMSaleOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, CRMSaleOrder objCRMSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrder objDA_CRMSaleOrder = new DA_CRMSaleOrder();
            objDA_CRMSaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CRMSaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CRMSaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objCRMSaleOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_CRMSaleOrder.Update(objCRMSaleOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Review(String strAuthenData, String strGUID, CRMSaleOrder objCRMSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrder objDA_CRMSaleOrder = new DA_CRMSaleOrder();
            objDA_CRMSaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CRMSaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CRMSaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objCRMSaleOrder.ReviewedUser = objToken.UserName;
            objResultMessage = objDA_CRMSaleOrder.Review(objCRMSaleOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertActionLog(String strAuthenData, String strGUID, CRMSaleOrder_ActionLog objCRMSaleOrder_ActionLog)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrder_ActionLog objDA_CRMSaleOrder_ActionLog = new DA_CRMSaleOrder_ActionLog();
            objCRMSaleOrder_ActionLog.CertificateString = objToken.CertificateString;
            objCRMSaleOrder_ActionLog.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objCRMSaleOrder_ActionLog.LoginLogID = objToken.LoginLogID;
            objCRMSaleOrder_ActionLog.ActionUser = objToken.UserName;
            objResultMessage = objDA_CRMSaleOrder_ActionLog.Insert(objCRMSaleOrder_ActionLog);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchActionLog(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrder_ActionLog objDA_CRMSaleOrder_ActionLog = new DA_CRMSaleOrder_ActionLog();
            objResultMessage = objDA_CRMSaleOrder_ActionLog.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "CRMSaleOrderAction";
            return objResultMessage;
        }
    }
}
