﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.CRM.BO;
using ERP.CRM.DA;
namespace ERPTransactionServices.CRM
{
    /// <summary>
    /// Summary description for CRMSaleOrderForCMS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSCRMSaleOrderForCMS : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true, Description = "Tạo đơn hàng CRM", MessageName = "Tao_don_hang_CRM")]
        public Library.WebCore.ResultMessage CreateCRMSaleOrder(String strAuthenData, String strGUID, ref string strCRMSaleOrderID, CRMSaleOrderForCMS objCRMSaleOrderForCMS)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CRMSaleOrderForCMS objDA_CRMSaleOrderForCMS = new DA_CRMSaleOrderForCMS();
            objDA_CRMSaleOrderForCMS.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CRMSaleOrderForCMS.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CRMSaleOrderForCMS.objLogObject.LoginLogID = objToken.LoginLogID;
            objCRMSaleOrderForCMS.CreatedUser = objToken.UserName;
            objResultMessage = objDA_CRMSaleOrderForCMS.CreateCRMSaleOrder(objCRMSaleOrderForCMS, ref strCRMSaleOrderID);
            return objResultMessage;
        }
        //[WebMethod(EnableSession = true, Description = "Tạo đơn hàng CRM_Test", MessageName = "Tao_don_hang_CRMTest")]
        //public Library.WebCore.ResultMessage CreateCRMSaleOrder_Test()
        //{
        //    string strCRMSaleOrderID = string.Empty;
        //    CRMSaleOrderForCMS objCRMSaleOrderForCMS = new CRMSaleOrderForCMS();
        //    objCRMSaleOrderForCMS.CRMSaleOrderTypeID = 3;
        //    objCRMSaleOrderForCMS.CustomerEmail = "aaa@gmail.com";
        //    objCRMSaleOrderForCMS.CustomerName = "Anh Ku A";
        //    objCRMSaleOrderForCMS.CustomerPhone = "0909090909";
        //    objCRMSaleOrderForCMS.OutputStoreID = 100;
        //    objCRMSaleOrderForCMS.StaffUser = "camdang";
        //    objCRMSaleOrderForCMS.CreatedUser = "administrator";
        //    objCRMSaleOrderForCMS.Gender = true;
        //    objCRMSaleOrderForCMS.ProvinceID = 14;
        //    objCRMSaleOrderForCMS.DistrictID = 2;
        //    objCRMSaleOrderForCMS.SaleOrderSourceID = 1;
        //    objCRMSaleOrderForCMS.ProductItemList = new List<ProductItem>();
        //    ProductItem objProductItem = new ProductItem();
        //    objProductItem.ProductID = "1010010000417";
        //    objProductItem.Quantity = 1;
        //    objCRMSaleOrderForCMS.ProductItemList.Add(objProductItem);
        //    objCRMSaleOrderForCMS.ProductComboItemList = new List<ProductComboItem>();
        //    ProductComboItem objProductComboItem = new ProductComboItem();
        //    objProductComboItem.ProductComboID = "CB45";
        //    objProductComboItem.Quantity = 1;
        //    objCRMSaleOrderForCMS.ProductComboItemList.Add(objProductComboItem);
        //    DA_CRMSaleOrderForCMS objDA_CRMSaleOrderForCMS = new DA_CRMSaleOrderForCMS();
        //    Library.WebCore.ResultMessage objResultMessage = objDA_CRMSaleOrderForCMS.CreateCRMSaleOrder(objCRMSaleOrderForCMS, ref strCRMSaleOrderID);
        //    return objResultMessage;
        //}
    }
}
