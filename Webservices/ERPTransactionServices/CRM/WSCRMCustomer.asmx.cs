﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.CRM
{
    /// <summary>
    /// Summary description for WSCRMCustomer
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSCRMCustomer : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.CRM.BO.CRMCustomer objCRMCustomer, int intCRMCustomerID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().LoadInfo(ref objCRMCustomer, intCRMCustomerID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadFeedBack(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_FeedBack().GetByID(ref tblResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoSearch(String strAuthenData, String strGUID, ref ERP.CRM.BO.CRMCustomer objCRMCustomer, int intCRMCustomerID,string strCustomerPhone)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().LoadInfo_Search(ref objCRMCustomer, intCRMCustomerID, strCustomerPhone);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.CRM.BO.CRMCustomer objCRMCustomer, ref string strCRMCustomerID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.CRM.DA.DA_CRMCustomer objDA_CRMCustomer = new ERP.CRM.DA.DA_CRMCustomer();
            objDA_CRMCustomer.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CRMCustomer.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CRMCustomer.objLogObject.LoginLogID = objToken.LoginLogID;
            objCRMCustomer.CreatedUser = objToken.UserName;
            objResultMessage = objDA_CRMCustomer.Insert(objCRMCustomer, ref strCRMCustomerID);
            //if (!objResultMessage.IsError)
            //   DataCache.WSDataCache.ClearCRMCustomerCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateForMobile(String strAuthenData, String strGUID, int intCRMCustomerID, string strCustomerName, string strCustomerAddress,
            string strCustomerTaxID, string strCustomerPhone, string strCustomerEmail, string strCustomerBirthday, string strCustomerIDCard,
            int intGender, int intWardID, int intDistrictID, int intProvinceID,string strDescription)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.CRM.DA.DA_CRMCustomer objDA_CRMCustomer = new ERP.CRM.DA.DA_CRMCustomer();
            objDA_CRMCustomer.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CRMCustomer.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CRMCustomer.objLogObject.LoginLogID = objToken.LoginLogID;
            string strUpdateUser = objToken.UserName;
            objResultMessage = objDA_CRMCustomer.UpdateCRMCustomer(intCRMCustomerID, strCustomerName, strCustomerAddress, strCustomerTaxID, strCustomerPhone, strCustomerEmail, strCustomerBirthday, strCustomerIDCard, intGender, intWardID, intDistrictID, intProvinceID, strDescription, strUpdateUser);
            //if (!objResultMessage.IsError)
            //   DataCache.WSDataCache.ClearCRMCustomerCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertFeedBack(String strAuthenData, String strGUID, ERP.CRM.BO.FeedBack objFeedBack)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.CRM.DA.DA_FeedBack objDA_FeedBack = new ERP.CRM.DA.DA_FeedBack();
            objDA_FeedBack.objLogObject.CertificateString = objToken.CertificateString;
            objDA_FeedBack.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_FeedBack.objLogObject.LoginLogID = objToken.LoginLogID;
            objFeedBack.CreatedUser = objToken.UserName;
            objResultMessage = objDA_FeedBack.Insert(objFeedBack);
            //if (!objResultMessage.IsError)
            //   DataCache.WSDataCache.ClearCRMCustomerCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertContact(String strAuthenData, String strGUID, List<ERP.CRM.BO.Contact> lstContact)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.CRM.DA.DA_Contact objDA_Contact = new ERP.CRM.DA.DA_Contact();
            objDA_Contact.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Contact.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Contact.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Contact.Insert(lstContact);
            //if (!objResultMessage.IsError)
            //   DataCache.WSDataCache.ClearCRMCustomerCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.CRM.BO.CRMCustomer objCRMCustomer, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.CRM.DA.DA_CRMCustomer objDA_CRMCustomer = new ERP.CRM.DA.DA_CRMCustomer();
            objDA_CRMCustomer.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CRMCustomer.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CRMCustomer.objLogObject.LoginLogID = objToken.LoginLogID;
            objCRMCustomer.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_CRMCustomer.Update(objCRMCustomer);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearCRMCustomerCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateFeedBack(String strAuthenData, String strGUID, ERP.CRM.BO.FeedBack objFeedBack, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.CRM.DA.DA_FeedBack objDA_FeedBack = new ERP.CRM.DA.DA_FeedBack();
            objDA_FeedBack.objLogObject.CertificateString = objToken.CertificateString;
            objDA_FeedBack.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_FeedBack.objLogObject.LoginLogID = objToken.LoginLogID;
            objFeedBack.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_FeedBack.Update(objFeedBack);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearCRMCustomerCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intCRMCustomerID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.CRM.BO.CRMCustomer objCRMCustomer = new ERP.CRM.BO.CRMCustomer();
            ERP.CRM.DA.DA_CRMCustomer objDA_CRMCustomer = new ERP.CRM.DA.DA_CRMCustomer();
            objDA_CRMCustomer.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CRMCustomer.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CRMCustomer.objLogObject.LoginLogID = objToken.LoginLogID;
            objCRMCustomer.CRMCustomerID = intCRMCustomerID;
            objCRMCustomer.DeletedUser = objToken.UserName;
            objResultMessage = objDA_CRMCustomer.Delete(objCRMCustomer);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearCRMCustomerCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteFeedBack(String strAuthenData, String strGUID, int intFeedBackID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.CRM.BO.FeedBack objFeedBack = new ERP.CRM.BO.FeedBack();
            ERP.CRM.DA.DA_FeedBack objDA_FeedBack = new ERP.CRM.DA.DA_FeedBack();
            objDA_FeedBack.objLogObject.CertificateString = objToken.CertificateString;
            objDA_FeedBack.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_FeedBack.objLogObject.LoginLogID = objToken.LoginLogID;
            objFeedBack.FeedBackID = intFeedBackID;
            objFeedBack.DeletedUser = objToken.UserName;
            objResultMessage = objDA_FeedBack.Delete(objFeedBack);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearCRMCustomerCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataRPT(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().SearchDataRPT(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerListRPT";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataRPTPro(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().SearchDataRPTPro(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerListRPT";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCustomerSearch(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().GetCustomerSearch(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerList";
            return objResultMessage;
        }

        //[WebMethod(EnableSession = true)]
        //public Library.WebCore.ResultMessage SearchDataRPT(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        //{
        //    Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
        //    Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
        //    if (objToken == null)
        //        return objResultMessage;
        //    objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().SearchDataRPT(ref tblResult, objKeywords);
        //    if (tblResult != null)
        //        tblResult.TableName = "CRMCustomerRPT";
        //    return objResultMessage;
        //}



        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataContact(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_Contact().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataReceiveSMS(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer_ReceiveSMS().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerReceiveSMS";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataFeedBack(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_FeedBack().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataFeedBack(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_FeedBack().GetData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "FeedBackList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSaleOrder(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().SearchDataSaleOrder(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerSaleOrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoByCode(String strAuthenData, String strGUID, ref ERP.CRM.BO.CRMCustomer objCRMCustomer, string strKeyword)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().LoadInfoByCode(ref objCRMCustomer, strKeyword);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoCRMByCode(String strAuthenData, String strGUID, ref ERP.CRM.BO.CRMCustomer objCRMCustomer, ref DataTable dtbData, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().LoadInfoCRMByCode(ref objCRMCustomer, ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataVoIP(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().SearchDataVoIP(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerSaleOrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSaleOrderDetail(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().SearchDataSaleOrderDetail(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CRMCustomerSaleOrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataInStockPrice(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.CRM.DA.DA_CRMCustomer().SearchDataInStockPrice(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "InStockPriceList";
            return objResultMessage;
        }
    }
}
