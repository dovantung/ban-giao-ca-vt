﻿namespace ERPTransactionServices.EO
{
    using Library.WebCore;
    using System;
    using System.Data;
    using System.Web;
    using System.Web.Services;
    using System.Collections.Generic;
    using ERP.EOffice.DA;
    using ERP.EOffice.BO;

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class WSWorksSupport : System.Web.Services.WebService
    {
        #region WSCmt

        /// <summary>
        /// Author          : Byrs
        /// Date created    : Apr 18, 2013
        /// Description     : Insert new comment to work support
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="decWSID">Work support ID</param>
        /// <param name="objWSCMT">WorkSupport_Comment object</param>
        /// <param name="objResultMsg"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public decimal AddNewWorkSupport_Comment(String strAuthenData, String strGUID,
            ERP.EOffice.BO.WorksSupport_Comment objWSCMT, ref Library.WebCore.ResultMessage objResultMsg)
        {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if (objToken == null) return 0;

            try
            {
                decimal decRsl = 0;
                objResultMsg = new DA_WorksSupport_Comment().Insert(objWSCMT, ref decRsl);
                return decRsl;
            }
            catch (Exception exc)
            {
                objResultMsg = new ResultMessage
                {
                    ErrorType = SystemError.ErrorTypes.GetData,
                    IsError = true,
                    Message = "Lỗi thêm comment theo WorksSupport ID",
                    MessageDetail = exc.ToString()
                };
                ErrorLog.Add(objResultMsg.Message, objResultMsg.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return 0;
        }

        /// <summary>
        /// Author          : Byrs (datnguyen0705@gmail.com)
        /// Date created    : Jun 10, 2013
        /// Description     : Insert new comment to work support on ipad
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="decWorksSupportID"></param>
        /// <param name="strCommentContent"></param>
        /// <param name="objResultMsg"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public decimal AddNewWorkSupport_CommentOnIPad(String strAuthenData, String strGUID,
            decimal decWorksSupportID, string strCommentContent,string strWSName, ref Library.WebCore.ResultMessage objResultMsg) {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if( objToken == null ) return 0;

            try {
                decimal decRsl = 0;
                ERP.EOffice.BO.WorksSupport_Comment objWSCMT = new ERP.EOffice.BO.WorksSupport_Comment {
                    WorksSupportID = decWorksSupportID,
                    CommentContent = strCommentContent,
                    WorksSupportName=strWSName,
                    CommentUser = objToken.UserName,
                    CreatedUser = objToken.UserName
                };
                objResultMsg = new DA_WorksSupport_Comment().Insert(objWSCMT, ref decRsl);
                return decRsl;
            } catch( Exception exc ) {
                objResultMsg = new ResultMessage {
                    ErrorType = SystemError.ErrorTypes.GetData,
                    IsError = true,
                    Message = "Lỗi thêm comment theo WorksSupport ID",
                    MessageDetail = exc.ToString()
                };
                ErrorLog.Add(objResultMsg.Message, objResultMsg.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return 0;
        }

        /// <summary>
        /// Author          : Byrs
        /// Date created    : Apr 18, 2013
        /// Description     : Get work support's comment by comment id
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="decWSCID">Work support comment identify</param>
        /// <param name="objResultMsg"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public ERP.EOffice.BO.WorksSupport_Comment GetWorkSupport_CommentById(String strAuthenData, String strGUID,
            decimal decWSCID, ref Library.WebCore.ResultMessage objResultMsg)
        {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if (objToken == null) return null;

            try
            {
                ERP.EOffice.BO.WorksSupport_Comment objRsl = null;
                objResultMsg = new DA_WorksSupport_Comment().LoadInfo(ref objRsl, decWSCID);
                return objRsl;
            }
            catch (Exception exc)
            {
                objResultMsg = new ResultMessage
                {
                    ErrorType = SystemError.ErrorTypes.GetData,
                    IsError = true,
                    Message = "Lỗi lấy comment theo ID",
                    MessageDetail = exc.ToString()
                };
                ErrorLog.Add(objResultMsg.Message, objResultMsg.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return null;
        }

        /// <summary>
        /// Author          : Byrs
        /// Date created    : Apr 18, 2013
        /// Description     : Get list of comments by work support identify
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="decWSID">Work support identify</param>
        /// <param name="objResultMsg"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public List<ERP.EOffice.BO.WorksSupport_Comment> GetWorkSupport_CommentsByWSId(String strAuthenData,
            String strGUID,
            decimal decWSID, int intPageIdx, int intPageSize, ref Library.WebCore.ResultMessage objResultMsg)
        {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if (objToken == null) return null;

            try
            {
                return new DA_WorksSupport_Comment().GetByWorksSupportId(decWSID, intPageIdx, intPageSize);
            }
            catch (Exception exc)
            {
                objResultMsg = new ResultMessage
                {
                    ErrorType = SystemError.ErrorTypes.GetData,
                    IsError = true,
                    Message = "Lỗi lấy danh sách comment theo WorksSupport ID",
                    MessageDetail = exc.ToString()
                };
                ErrorLog.Add(objResultMsg.Message, objResultMsg.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return null;
        }

        #endregion

        #region WS

        /// <summary>
        /// Author          : Byrs
        /// Date created    : Apr 19, 2013
        /// Description     : Get list of works support by paging.
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="decWSID">Work support identify</param>
        /// <param name="objResultMsg"></param>
        /// <param name="intPageIndex"></param>
        /// <param name="intPageSize"></param>
        /// <param name="lstWSTypeId">,23,32,23,</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public List<ERP.EOffice.BO.WorksSupport> GetListWSByPageIndex(String strAuthenData, String strGUID,
            ref ResultMessage objResultMsg, int intPageIndex, int intPageSize, string lstWSTypeId)
        {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if (objToken == null) return null;

            try
            {
                return new DA_WorksSupport().GetListWSByPageIndex(ref objResultMsg, intPageIndex, intPageSize, lstWSTypeId, objToken.UserName);
            }
            catch (Exception exc)
            {
                objResultMsg = new ResultMessage
                {
                    ErrorType = SystemError.ErrorTypes.GetData,
                    IsError = true,
                    Message = "Lỗi lấy danh sách comment theo WorksSupport ID",
                    MessageDetail = exc.ToString()
                };
                ErrorLog.Add(objResultMsg.Message, objResultMsg.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public List<ERP.EOffice.BO.WorksSupport> SearchWorksSupport(String strAuthenData, String strGUID,
            ref ResultMessage objResultMsg, int PageIndex, int PageSize, string KeyWord, string CreatedUser, DateTime? CreatedDateFrom, DateTime? CreatedDateTo, int WorksSupportType, int WorksSupportStatus, string lstWSTypeId, int intType)
        {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if (objToken == null) return null;

            try
            {
                return new DA_WorksSupport().SearchWorksSupport(ref objResultMsg, PageIndex, PageSize, KeyWord, CreatedUser, CreatedDateFrom, CreatedDateTo, WorksSupportType, WorksSupportStatus, lstWSTypeId, intType, objToken.UserName);
            }
            catch (Exception exc)
            {
                objResultMsg = new ResultMessage
                {
                    ErrorType = SystemError.ErrorTypes.GetData,
                    IsError = true,
                    Message = "Lỗi tìm kiếm công việc hỗ trợ",
                    MessageDetail = exc.ToString()
                };
                ErrorLog.Add(objResultMsg.Message, objResultMsg.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return null;
        }

        #endregion

        #region WS Type

        /// <summary>
        /// Author          : Byrs
        /// Date created    : Apr 20, 2013
        /// Description     : Get a EO_WorksSupportType by its identify
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="intWSTID">Works support type identify</param>
        /// <param name="objResultMsg"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public ERP.EOffice.BO.EO_WorksSupportType GetWorksSupportType_ById(String strAuthenData, String strGUID,
            int intWSTID, ref Library.WebCore.ResultMessage objResultMsg)
        {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if (objToken == null) return null;

            try
            {
                return DA_WorksSupportType.Current.GetById(intWSTID, ref objResultMsg);
            }
            catch (Exception exc)
            {
                objResultMsg = new ResultMessage
                {
                    ErrorType = SystemError.ErrorTypes.GetData,
                    IsError = true,
                    Message = "Lỗi lấy comment theo ID",
                    MessageDetail = exc.ToString()
                };
                ErrorLog.Add(objResultMsg.Message, objResultMsg.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return null;
        }

        /// <summary>
        /// Author          : Byrs
        /// Date created    : Apr 20, 2013
        /// Description     : Get all of works support type
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objResultMsg"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public List<ERP.EOffice.BO.EO_WorksSupportType> GetWorksSupportType_GetAll(String strAuthenData, String strGUID,
            ref Library.WebCore.ResultMessage objResultMsg)
        {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if (objToken == null) return null;

            try
            {
                return DA_WorksSupportType.Current.GetAll(ref objResultMsg);
            }
            catch (Exception exc)
            {
                objResultMsg = new ResultMessage
                {
                    ErrorType = SystemError.ErrorTypes.GetData,
                    IsError = true,
                    Message = "Lỗi lấy comment theo ID",
                    MessageDetail = exc.ToString()
                };
                ErrorLog.Add(objResultMsg.Message, objResultMsg.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return null;
        }

        #endregion

        #region WS Status

        /// <summary>
        /// Author          : Byrs
        /// Date created    : Apr 19, 2013
        /// Description     : Get list of ws status
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objResultMsg"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public List<ERP.EOffice.BO.EO_WorksSupportStatus> GetAllWorkSupportStatus(String strAuthenData,
            String strGUID,
            ref Library.WebCore.ResultMessage objResultMsg)
        {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if (objToken == null) return null;

            try
            {
                return DA_WorksSupportStatus.Current.GetAll(ref objResultMsg);
            }
            catch (Exception exc)
            {
                objResultMsg = new ResultMessage
                {
                    ErrorType = SystemError.ErrorTypes.GetData,
                    IsError = true,
                    Message = "Lỗi lấy danh sách comment theo WorksSupport ID",
                    MessageDetail = exc.ToString()
                };
                ErrorLog.Add(objResultMsg.Message, objResultMsg.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return null;
        }

        #endregion

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.EOffice.BO.WorksSupport objWorksSupport, decimal intWorksSupportID, bool bolLoadDetail)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().LoadInfo(ref objWorksSupport, intWorksSupportID, bolLoadDetail);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.EOffice.BO.WorksSupport objWorksSupport)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ERP.EOffice.DA.DA_WorksSupport objDA_WorksSupport = new ERP.EOffice.DA.DA_WorksSupport();
                objDA_WorksSupport.objLogObject.CertificateString = objToken.CertificateString;
                objDA_WorksSupport.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
                objDA_WorksSupport.objLogObject.LoginLogID = objToken.LoginLogID;
                objWorksSupport.CreatedUser = objToken.UserName;
                objResultMessage = objDA_WorksSupport.Insert(objWorksSupport);
                //if (!objResultMessage.IsError)
                //    DataCache.WSDataCache.ClearWorksSupportCache();
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertForIpad(String strAuthenData, String strGUID, int intWorksSupportTypeID, string strWorksSupportName,DateTime expectedCompletedDate,string strContent)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ERP.EOffice.BO.WorksSupport objWorksSupport = new ERP.EOffice.BO.WorksSupport();
                objWorksSupport.WorksSupportTypeID = intWorksSupportTypeID;
                objWorksSupport.WorksSupportName = strWorksSupportName;
                objWorksSupport.ExpectedCompletedDate = expectedCompletedDate;
                objWorksSupport.Content = strContent;
                ERP.EOffice.DA.DA_WorksSupport objDA_WorksSupport = new ERP.EOffice.DA.DA_WorksSupport();
                objDA_WorksSupport.objLogObject.CertificateString = objToken.CertificateString;
                objDA_WorksSupport.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
                objDA_WorksSupport.objLogObject.LoginLogID = objToken.LoginLogID;
                objWorksSupport.CreatedUser = objToken.UserName;
                objResultMessage = objDA_WorksSupport.InsertForIpad(objWorksSupport);
                //if (!objResultMessage.IsError)
                //    DataCache.WSDataCache.ClearWorksSupportCache();
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.EOffice.BO.WorksSupport objWorksSupport)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ERP.EOffice.DA.DA_WorksSupport objDA_WorksSupport = new ERP.EOffice.DA.DA_WorksSupport();
                objDA_WorksSupport.objLogObject.CertificateString = objToken.CertificateString;
                objDA_WorksSupport.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
                objDA_WorksSupport.objLogObject.LoginLogID = objToken.LoginLogID;
                objWorksSupport.UpdatedUser = objToken.UserName;
                objResultMessage = objDA_WorksSupport.Update(objWorksSupport);
                //if (!objResultMessage.IsError)
                //    DataCache.WSDataCache.ClearWorksSupportCache();
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateForIpad(String strAuthenData, String strGUID, decimal decWorksSupportID, string strWorksSupportName, DateTime expectedCompletedDate, string strContent)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ERP.EOffice.BO.WorksSupport objWorksSupport = new ERP.EOffice.BO.WorksSupport();
                objWorksSupport.WorksSupportID = decWorksSupportID;
                objWorksSupport.WorksSupportName = strWorksSupportName;
                objWorksSupport.ExpectedCompletedDate = expectedCompletedDate;
                objWorksSupport.Content = strContent;
                ERP.EOffice.DA.DA_WorksSupport objDA_WorksSupport = new ERP.EOffice.DA.DA_WorksSupport();
                objDA_WorksSupport.objLogObject.CertificateString = objToken.CertificateString;
                objDA_WorksSupport.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
                objDA_WorksSupport.objLogObject.LoginLogID = objToken.LoginLogID;
                objWorksSupport.UpdatedUser = objToken.UserName;
                objResultMessage = objDA_WorksSupport.UpdateForIpad(objWorksSupport);
                //if (!objResultMessage.IsError)
                //    DataCache.WSDataCache.ClearWorksSupportCache();
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateComment(String strAuthenData, String strGUID, ERP.EOffice.BO.WorksSupport objWorksSupport)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ERP.EOffice.DA.DA_WorksSupport objDA_WorksSupport = new ERP.EOffice.DA.DA_WorksSupport();
                objDA_WorksSupport.objLogObject.CertificateString = objToken.CertificateString;
                objDA_WorksSupport.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
                objDA_WorksSupport.objLogObject.LoginLogID = objToken.LoginLogID;
                objWorksSupport.UpdatedUser = objToken.UserName;
                objResultMessage = objDA_WorksSupport.UpdateComment(objWorksSupport);
                //if (!objResultMessage.IsError)
                //    DataCache.WSDataCache.ClearWorksSupportCache();
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateStatus(String strAuthenData, String strGUID, int intWorkSupportID, int intWorkSupportStatusID, int intProcess, string strUserUpdate)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;

                ERP.EOffice.DA.DA_WorksSupport objDA_WorksSupport = new ERP.EOffice.DA.DA_WorksSupport();
                objDA_WorksSupport.objLogObject.CertificateString = objToken.CertificateString;
                objDA_WorksSupport.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
                objDA_WorksSupport.objLogObject.LoginLogID = objToken.LoginLogID;

                objResultMessage = objDA_WorksSupport.UpdateStatus(intWorkSupportID, intWorkSupportStatusID, intProcess, strUserUpdate);

                //if (!objResultMessage.IsError)
                //    DataCache.WSDataCache.ClearWorksSupportCache();

                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, decimal intWorksSupportID)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ERP.EOffice.BO.WorksSupport objWorksSupport = new ERP.EOffice.BO.WorksSupport();
                ERP.EOffice.DA.DA_WorksSupport objDA_WorksSupport = new ERP.EOffice.DA.DA_WorksSupport();
                objDA_WorksSupport.objLogObject.CertificateString = objToken.CertificateString;
                objDA_WorksSupport.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
                objDA_WorksSupport.objLogObject.LoginLogID = objToken.LoginLogID;
                objWorksSupport.WorksSupportID = intWorksSupportID;
                objWorksSupport.DeletedUser = objToken.UserName;
                objResultMessage = objDA_WorksSupport.Delete(objWorksSupport);
                //if (!objResultMessage.IsError)
                //DataCache.WSDataCache.ClearWorksSupportCache();
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        //Phong thêm objKeywords ko xai co the de trống
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().SearchData(ref tblResult, objKeywords);
                if (tblResult != null)
                    tblResult.TableName = "WorksSupportList";
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataComment(String strAuthenData, String strGUID, ref List<ERP.EOffice.BO.WorksSupport_Comment> listComment, decimal  decWorksSupportID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            try
            {
                listComment = new ERP.EOffice.DA.DA_WorksSupport_Comment().SearchDataToList(decWorksSupportID);
            }
            catch(Exception ex)
            {
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.SearchData, ex.Message, ex.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetNewComment(String strAuthenData, String strGUID, ref List<ERP.EOffice.BO.WorksSupport_Comment> listWorksSupport_Comment, string arrWorkSupport)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            try
            {
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().CheckNewComment(ref listWorksSupport_Comment, arrWorkSupport);
            }
            catch (Exception ex)
            {
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.SearchData, ex.Message, ex.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchForIPad(String strAuthenData, String strGUID, ref List<ERP.EOffice.BO.WorksSupport> listWorksSupport, DateTime fromdate, DateTime todate, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            try
            {
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().SearchForIPad(ref listWorksSupport,  fromdate,  todate,  strUserName);
            }
            catch (Exception ex)
            {
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.SearchData, ex.Message, ex.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchForGetByArrID(String strAuthenData, String strGUID, ref List<ERP.EOffice.BO.WorksSupport> listWorksSupport, string strArrID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            try
            {
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().SearchForGetByArrID(ref listWorksSupport, strArrID, strUserName);
            }
            catch (Exception ex)
            {
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.SearchData, ex.Message, ex.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStringIDForDel(String strAuthenData, String strGUID, ref string strArrID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            try
            {
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().GetStringIDForDel(ref  strArrID, strUserName);
            }
            catch (Exception ex)
            {
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.SearchData, ex.Message, ex.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetProgress(String strAuthenData, String strGUID, ref List<ERP.EOffice.BO.WorksSupport> listWorksSupport, string arrWorkSupport)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            try
            {
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().GetProgress(ref listWorksSupport,   arrWorkSupport);
            }
            catch (Exception ex)
            {
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.SearchData, ex.Message, ex.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return objResultMessage;
        }


        //public delegate void LongTimeTask_Delegate(string s);
        /// <summary>
        /// Check have new worksupport and comment, get progress and deleted worksupport
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="listWorksSupport"> List WorkSupport new</param>
        /// <param name="listWorksSupport_Comment">List Comment new </param>
        /// <param name="listProgress">List Progress</param>
        /// <param name="strArrIDDeleted">String WorkSupportID Deleted</param>
        /// <param name="arrWorkSupport"> Parameter string array WorkSupport to compare </param>
        /// <param name="strArrComment">Parameter string array WorkSupport_Comment to compare</param>
        /// <param name="strWSPIDToGetProgress">Parameter string array WorkSupportID to get Progress</param>
        /// <param name="strUserName"> o_O </param>
        
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckNewWorkSupport(String strAuthenData, String strGUID, ref List<ERP.EOffice.BO.WorksSupport> listWorksSupport, ref List<ERP.EOffice.BO.WorksSupport_Comment> listWorksSupport_Comment,ref List<ERP.EOffice.BO.WorksSupport> listProgress,ref string  strArrIDDeleted, string arrWorkSupport,string strArrComment,string strWSPIDToGetProgress, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            try
            {
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().SearchForGetByArrID(ref listWorksSupport, arrWorkSupport, strUserName);
                if (objResultMessage.IsError) return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().CheckNewComment(ref listWorksSupport_Comment, strArrComment);
                if (objResultMessage.IsError) return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().GetProgress(ref listProgress, strWSPIDToGetProgress);
                if (objResultMessage.IsError) return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.DA_WorksSupport().GetStringIDForDel(ref strArrIDDeleted, strUserName);
            }
            catch (Exception ex)
            {
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.SearchData, ex.Message, ex.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
            }
            return objResultMessage;
        }
    }
}