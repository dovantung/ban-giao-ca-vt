﻿using ERP.EOffice.BO.EO_INForm;
using ERP.EOffice.BO.InForm;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.EO
{
    /// <summary>
    /// Summary description for WSInForm
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    //To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    //[System.Web.Script.Services.ScriptService]
    public class WSInForm : System.Web.Services.WebService
    {

        /// <summary>
        /// Lấy Thông báo theo User Có Phân Trang
        /// </summary>
        /// <param name="strUser"></param>
        /// <param name="objInForm"></param>
        /// <param name="intPage"></param>
        /// <param name="intRow"></param>
        /// <param name="strAuhenData"></param>
        /// <param name="strGUID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public ResultMessage Get_ALLInformByUser(string strUser, ref List<EO_INForm> objInForm,int intPage,int intRow,ref int totalRows,int informType,int isRead, string keyTitle, string keyCreateUser, DateTime fromDate, DateTime toDate,int UserType,  string strAuhenData, string strGUID)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.DA_EO_INForm().GetALLInForm(ref objInForm, strUser, intPage, intRow,ref totalRows,informType,isRead,keyTitle,keyCreateUser, fromDate,  toDate,UserType);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        /// <summary>
        /// Lấy Loại Thông báo
        /// </summary>
        /// <param name="objInFormType">BO InForm_Type</param>
        /// <param name="strAuhenData"></param>
        /// <param name="strGUID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public ResultMessage GetALL_InformType(ref List<EO_InForm_Type> objInFormType,string strAuhenData, string strGUID)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_InForm_Type().GetAllInFormType(ref objInFormType);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        /// <summary>
        /// Thêm mới Comment
        /// </summary>
        /// <param name="objComment"></param>
        /// <param name="strAuhenData"></param>
        /// <param name="strGUID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession=true)]
        public ResultMessage InsertComment(EO_INFORM_COMMENT objComment,string strAuhenData, string strGUID,ref List<EO_INFORM_COMMENT>LEIC,ref string totalRows)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_COMMENT().Insert(objComment, ref LEIC, ref totalRows);
                return objResultMessage;
            }
            catch (Exception objEX)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi tạo mới bình luận", objEX.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage InsertCommentOnIpad(string strAuhenData, string strGUID,decimal informID, string Content,string Title,string UserName)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                EO_INFORM_COMMENT objComment = new EO_INFORM_COMMENT();
                objComment.INFormID = Convert.ToDecimal(informID);
                objComment.COMMENTContent = Content;
                objComment.CreatedUser = UserName;
                objComment.COMMENTUser = UserName;
                objComment.COMMENTTITLE = Title;

                objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_COMMENT().Insert(objComment);
                return objResultMessage;
            }
            catch (Exception objEX)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi tạo mới bình luận", objEX.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage InsertInform(String strAuthenData, String strGUID, ERP.EOffice.BO.EO_INForm.EO_INForm objIF, string InformTyleList, string UserList, string DepartmentList, string ListFiles, string StorageRoot, string StorageRootWeb)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_USER().InsertInform(objIF, InformTyleList, UserList, DepartmentList, ListFiles, StorageRoot, StorageRootWeb);
                return objResultMessage;
            }
            catch (Exception objEX)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage UpdateInform(String strAuthenData, String strGUID, ERP.EOffice.BO.EO_INForm.EO_INForm objIF, string UserList, string DepartmentList, string ListFiles, string ListFilesFollow, string StorageRoot, string StorageRootWeb)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                //objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_USER().UpdateInform(objIF, UserList, DepartmentList, ListFiles,ListFilesFollow, StorageRoot, StorageRootWeb);
                return objResultMessage;
            }
            catch (Exception objEX)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage LoadInfoDetailsManage(String strAuthenData, String strGUID, ref EO_INForm objEO_INForm, decimal decINFormID)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.DA_EO_INForm().LoadInfoDetailsManage(ref objEO_INForm, decINFormID);
                return objResultMessage;
            }
            catch (Exception objEX)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage LoadInfoInform(String strAuthenData, String strGUID, ref EO_INForm objEO_INFORM_USER, decimal decINFormID, string strUserName)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_USER().LoadInfoInform(ref objEO_INFORM_USER,decINFormID,strUserName);
                return objResultMessage;
            }
            catch (Exception objEX)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
         [WebMethod(EnableSession = true)]
        public ResultMessage SearchDataComment(String strAuthenData, String strGUID, ref List<EO_INFORM_COMMENT> LIFC, decimal informID, int indexRow, int numberRow, ref string totalRows)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_COMMENT().SearchData(ref LIFC, informID,indexRow,numberRow,ref totalRows);
                return objResultMessage;
            }
            catch (Exception objEX)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
         public ResultMessage LoadInfoInformTypeFull(String strAuthenData, String strGUID, ref List<EO_INFormType_ReviewLevel> objLITRL, int intINFormTypeID)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFormType_ReviewLevel().LoadInfoInformTypeFull(ref objLITRL, intINFormTypeID);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }

         [WebMethod(EnableSession = true)]
         public ResultMessage LoadInfoInformReviewListFull(String strAuthenData, String strGUID, ref List<EO_INFormType_ReviewLevel> objLITRL, int intINFormID)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_REVIEWLIST().LoadInfoInformReviewListFull(ref objLITRL, intINFormID);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }

         [WebMethod(EnableSession = true)]
         public ResultMessage UpdateInformRWL(String strAuthenData, String strGUID, EO_INFORM_REVIEWLIST objInformRWL,string flag)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_REVIEWLIST().Update(objInformRWL,flag);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }
         [WebMethod(EnableSession = true)]
         public ResultMessage UpdateInformRWLOnIpad(String strAuthenData, String strGUID,string userName,string informID, string ReviewStatus, string RLevelID, string flag)
         {
             EO_INFORM_REVIEWLIST objInformRWL = new EO_INFORM_REVIEWLIST();
             objInformRWL.INFormID = Convert.ToInt16(informID);
             objInformRWL.UserName = userName;
             objInformRWL.ReviewLevelID = Convert.ToInt16(RLevelID);
             objInformRWL.ReviewStatus = Convert.ToInt16(ReviewStatus);
             objInformRWL.IsReviewed = (ReviewStatus == "2") ? true : false;
             if (ReviewStatus == "2")
             {
                 objInformRWL.ReviewedDate = DateTime.Now;
             }
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_REVIEWLIST().Update(objInformRWL, flag);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }

         [WebMethod(EnableSession = true)]
         public ResultMessage GetALLInFormManage(ref List<EO_INForm> objInForm, int intPage, int intRow, ref int totalRows, int informType, int isReviewed, string keyTitle, string keyCreateUser, DateTime fromDate, DateTime toDate, DateTime? fromDateCreate, DateTime? toDateCreate,int UserType, string strAuhenData, string strGUID)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.DA_EO_INForm().GetALLInFormManage(ref objInForm,intPage, intRow, ref totalRows,informType,isReviewed, keyTitle,keyCreateUser,fromDate,toDate,fromDateCreate, toDateCreate,UserType);
                 return objResultMessage;
             }
             catch (Exception objEx)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }

         [WebMethod(EnableSession = true)]
         public ResultMessage DeleteInform(String strAuthenData, String strGUID,EO_INForm objIF)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.DA_EO_INForm().Delete(objIF);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }
         [WebMethod(EnableSession = true)]
         public ResultMessage DeleteInformOnIpad(String strAuthenData, String strGUID, decimal informID,string deletedUser)
         {
             try
             {
                 EO_INForm objIF = new EO_INForm();
                 objIF.INFormID = informID;
                 objIF.DeletedUser = deletedUser;
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.DA_EO_INForm().Delete(objIF);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }

         [WebMethod(EnableSession = true)]
         public ResultMessage GetUserByInformID(string strAuhenData, string strGUID, ref List<EO_INFORM_USER> objInForm, decimal informID, int type)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_USER().GetUserByInformID(ref objInForm, informID, type);
                 return objResultMessage;
             }
             catch (Exception objEx)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }
         [WebMethod(EnableSession = true)]
         public ResultMessage UpdateISREAD(string strAuhenData, string strGUID, decimal informID, string userName)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_USER().UpdateISREAD(informID,userName);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }
         [WebMethod(EnableSession = true)]
         public ResultMessage InsertInformSam(String strAuthenData, String strGUID, ERP.EOffice.BO.EO_INForm.EO_INForm objIF, string InformTyleList,ref string UserList, string DepartmentList, List<EO_INFORM_ATTACHMENT> ListFiles)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 //objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_USER().InsertInformSam(objIF, InformTyleList,ref UserList, DepartmentList, ListFiles);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }
         [WebMethod(EnableSession = true)]
         public ResultMessage InsertInformOnIpad(String strAuthenData, String strGUID,string Content, DateTime InformDate,int InformType,string UserName,string InformTitle, string InformTyleList, string UserList, string DepartmentList, string ListFiles)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_USER().InsertInform(Content, InformDate,InformType,UserName,InformTitle,InformTyleList, UserList, DepartmentList, ListFiles);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }
         [WebMethod(EnableSession = true)]
         public ResultMessage GetALLInformOnIpad(string strUser, ref List<EO_INForm> objInForm,DateTime fromDate, DateTime toDate,string listID, string strAuhenData, string strGUID)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.DA_EO_INForm().GetALLInFormOnIpad(ref objInForm, strUser, fromDate, toDate, listID);
                 return objResultMessage;
             }
             catch (Exception objEx)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }

         [WebMethod(EnableSession = true)]
         public ResultMessage SearchNewComment(String strAuthenData, String strGUID, ref List<EO_INFORM_COMMENT> LIFC, string ListID)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.InForm.DA_EO_INFORM_COMMENT().SearchNewComment(ref LIFC,ListID);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }

         [WebMethod(EnableSession = true)]
         public ResultMessage CheckDeleteInform(String strAuthenData, String strGUID, ref string objResult, string strUser)
         {
             try
             {
                 Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                 Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                 if (objToken == null)
                     return objResultMessage;
                 objResultMessage = new ERP.EOffice.DA.DA_EO_INForm().CheckDeleteInform(ref objResult, strUser);
                 return objResultMessage;
             }
             catch (Exception objEX)
             {
                 Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEX.ToString());
                 ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                 return objResultMessage;
             }
         }


    }

}
