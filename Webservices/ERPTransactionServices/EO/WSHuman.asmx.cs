﻿using ERP.HRM.Payroll.BO;
using ERP.HRM.Payroll.DA.RewardPoint;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.EO
{
    /// <summary>
    /// Summary description for WSHuman
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSHuman : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public ResultMessage GetAttendanceMarkList(string strAuhenData, string strGUID, string strUser, ref List<HR_AttendanceMark> objADM, DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().GetAttendanceMarkList(ref objADM,strUser, fromDate,toDate);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage GetRewardPoint(string strAuhenData, string strGUID,ref List<RewardPointProduct> ListRPP, DateTime? fromDate, DateTime? toDate, string UserName)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.RewardPoint.Remark().GetRewardPointData(ref ListRPP,fromDate,toDate,UserName);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage GetRewardPointDetail(string strAuhenData, string strGUID, ref List<RewardPointProduct> ListRPP, DateTime? fDate, string UserName)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.RewardPoint.Remark().GetRewardPointDetails(ref ListRPP, fDate, UserName);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage GetBirthdayUser(string strAuhenData, string strGUID,ref List<ERP.HRM.Payroll.DA.Birthday.BirthdayUser> ListBDY)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.Birthday.Birthday().GetBirthdayUser(ref ListBDY);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage GetInfoUser(string strAuhenData, string strGUID,string UserName,ref ERP.HRM.Payroll.BO.User objUser)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_User().LoadInfo(ref objUser,UserName);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage GetUser(string strAuhenData, string strGUID, string UserName, ref ERP.HRM.Payroll.BO.User objUser)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_User().LoadUser(ref objUser, UserName);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage InsertAlbum(string strAuhenData, string strGUID, ERP.HRM.Payroll.BO.UserAlbum objUAlbum, ref List<UserAlbum> ListUAlbum)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum().InsertAlbum(objUAlbum,ref ListUAlbum);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage UpdateAlbum(string strAuhenData, string strGUID, ERP.HRM.Payroll.BO.UserAlbum objUAlbum)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum().Update(objUAlbum);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage InsertListPic(string strAuhenData, string strGUID, List<UserAlbum_Picture> ListUPic, ref List<UserAlbum_Picture> ListUAP)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum_Picture().InsertList(ListUPic, ref ListUAP);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage LoadAlbum(string strAuhenData, string strGUID,ref UserAlbum objUserAlbum, decimal decUserAlbumID)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum().LoadAlbum(ref objUserAlbum,decUserAlbumID);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage InsertComment(string strAuhenData, string strGUID, ref UserAlbum objUserAlbum, decimal decUserAlbumID)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum().LoadAlbum(ref objUserAlbum, decUserAlbumID);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage LoadCommentPic(string strAuhenData, string strGUID, ref List<UserAlbum_Picture_Comment> ListUAPComment, decimal PictureID)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum_Picture_Comment().LoadComment(ref ListUAPComment,PictureID);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage InsertCommentPic(string strAuhenData, string strGUID, UserAlbum_Picture_Comment objUserAlbum_Picture_Comment, ref List<UserAlbum_Picture_Comment> ListUAPComment)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum_Picture_Comment().Insert(objUserAlbum_Picture_Comment, ref ListUAPComment);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage DeletePicture(string strAuhenData, string strGUID, UserAlbum_Picture objUserAlbum_Picture)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum_Picture().Delete(objUserAlbum_Picture);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage DeleteAlbum(string strAuhenData, string strGUID, UserAlbum objUserAlbum)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum().Delete(objUserAlbum);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
        [WebMethod(EnableSession = true)]
        public ResultMessage UpdateUser(string strAuhenData, string strGUID, User objUser)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_User().Update(objUser);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public ResultMessage InsertPicLike(string strAuhenData, string strGUID, UserAlbum_Picture_Like objUAPLike,ref string UserLike)
        {
            try
            {
                Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuhenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.HRM.Payroll.DA.DA_UserAlbum_Picture_Like().Insert(objUAPLike, ref UserLike);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                Library.WebCore.ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông báo", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp thông tin", Globals.ModuleName);
                return objResultMessage;
            }
        }
   
    }
}
