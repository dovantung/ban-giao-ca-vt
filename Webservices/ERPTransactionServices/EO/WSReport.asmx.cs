﻿using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using WebLibs.Cached;

namespace ERPTransactionServices.EO
{
    /// <summary>
    /// Summary description for WSReport
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSReport : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public bool RegisterDeviceToken(String strAuthenData, String strGUID,
            ref ResultMessage objResultMsg, string deviceToken)
        {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if( objToken == null ) return false;

            ICached iCached = CachedFactory.Create(ConfigurationManager.AppSettings["RD_SV"]);
            return iCached.Set<string>(objToken.UserName, deviceToken);
        }

        [WebMethod( EnableSession = true )]
        public string GetUserDeviceToken(String strAuthenData, String strGUID,
            ref ResultMessage objResultMsg) {
            objResultMsg = new ResultMessage();
            Library.WebCore.Token objToken = Authentication.CheckAuthenticate(
                strAuthenData, strGUID, ref objResultMsg);
            if( objToken == null ) return string.Empty;

            ICached iCached = CachedFactory.Create(ConfigurationManager.AppSettings["RD_SV"]);
            return iCached.Get<string>(objToken.UserName);
        }

        //[WebMethod]
        //public bool RegisterDeviceTokenTest( string usr, string deviceToken ) {
        //    ICached iCached = CachedFactory.Create( ConfigurationManager.AppSettings["RD_SV"] );
        //    return iCached.Set<string>( usr, deviceToken );
        //}
    }
}
