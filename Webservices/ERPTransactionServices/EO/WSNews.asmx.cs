﻿using ERP.EOffice.BO;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.EO
{
    /// <summary>
    /// Get information for News
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class WSNews : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertNews(ref EO_News objEO_News, String strAuthenData, String strGUID)
        {
            Library.WebCore.ResultMessage objResultMessage = new ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                if (objEO_News.NewsID == 0)
                    objResultMessage = new ERP.EOffice.DA.DA_EO_News().Insert(objEO_News);
                else
                    objResultMessage = new ERP.EOffice.DA.DA_EO_News().Update(objEO_News);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi tạo tin tức", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteNews(ref EO_News objEO_News, String strAuthenData, String strGUID)
        {
            Library.WebCore.ResultMessage objResultMessage = new ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.DA_EO_News().Delete(objEO_News);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi xóa tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi xoá tin tức", Globals.ModuleName);
                return objResultMessage;
            }
        }

        /// <summary>
        /// Lấy danh mục tin trang home-news
        /// </summary>
        /// <param name="strUSer"></param>
        /// <param name="objInForm"></param>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public List<EO_NewsCategory> Get_AllNewsCateogy(int IsEditNewsCategory, ref Library.WebCore.ResultMessage objResultMessage, String strAuthenData, String strGUID)
        {
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return null;
                List<EO_NewsCategory> objNewsCategoryLst = new List<EO_NewsCategory>();
                objResultMessage = new ERP.EOffice.DA.DA_EO_NewsCategory().GetAll(ref objNewsCategoryLst, IsEditNewsCategory);
                return objNewsCategoryLst;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi nạp danh sách danh mục tin", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi lấy danh sách loại tin tức", Globals.ModuleName);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public List<EO_News> GetNewsByNewsCategory(int intNewsCategoryID, string strKeyword, string strCreatedUser, int intUserType, DateTime? dtmCreatedDateFrom, DateTime? dtmCreatedDateTo, int intCurrentPage, int intRecordPerPage, int IsEditPage, ref Library.WebCore.ResultMessage objResultMessage, String strAuthenData, String strGUID)
        {
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return null;
                List<EO_News> objNewsLst = new List<EO_News>();
                objResultMessage = new ERP.EOffice.DA.DA_EO_News().GetNewsByNewsCategory(ref objNewsLst, intNewsCategoryID, strKeyword, strCreatedUser, intUserType, dtmCreatedDateFrom, dtmCreatedDateTo, intCurrentPage, intRecordPerPage, IsEditPage);
                return objNewsLst;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi nạp danh sách tin theo danh mục tin", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi lấy danh sách tin tức theo loại tin tức", Globals.ModuleName);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public EO_News Get_NewsByID(decimal decNewsID, ref Library.WebCore.ResultMessage objResultMessage, String strAuthenData, String strGUID)
        {
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return null;
                EO_News objNews = new EO_News();
                objResultMessage = new ERP.EOffice.DA.DA_EO_News().LoadInfo(ref objNews, decNewsID);
                return objNews;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi nạp chi tiết tin", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi lấy chi tiết tin tức", Globals.ModuleName);
                return null;
            }
        }


        [WebMethod(EnableSession = true)]
        public List<EO_News_Comment> Get_NewsCommentByNewsID(int intNewsID, int intPageIndex, int intPageSize, ref Library.WebCore.ResultMessage objResultMessage, String strAuthenData, String strGUID)
        {
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return null;
                List<EO_News_Comment> objNews_CommentLst = new List<EO_News_Comment>();
                objResultMessage = new ERP.EOffice.DA.DA_EO_News_Comment().GetNewsCommentByNewsID(ref objNews_CommentLst, intNewsID, intPageIndex, intPageSize);
                return objNews_CommentLst;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi nạp danh sách NewsComment theo NewsID", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi lấy danh sách bình luận tin tức", Globals.ModuleName);
                return null;
            }
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertNewsComment(ref EO_News_Comment objEO_News_Comment, String strAuthenData, String strGUID)
        {
            Library.WebCore.ResultMessage objResultMessage = new ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                objResultMessage = new ERP.EOffice.DA.DA_EO_News_Comment().Insert(objEO_News_Comment);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm bình luận", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi tạo bình luận tin tức", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public EO_News_Comment Get_NewsCommentByID(decimal decNewsCommentID, ref Library.WebCore.ResultMessage objResultMessage, String strAuthenData, String strGUID)
        {
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return null;
                EO_News_Comment objNews_Comment = new EO_News_Comment();
                objResultMessage = new ERP.EOffice.DA.DA_EO_News_Comment().LoadInfo(ref objNews_Comment, decNewsCommentID);
                return objNews_Comment;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi nạp NewsComment theo NewsCommentID", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi lấy danh sách bình luận tin tức", Globals.ModuleName);
                return null;
            }
        }

        #region NewsCategory

        [WebMethod(EnableSession = true)]
        public EO_NewsCategory Get_NewsCategoryByID(int NewsCategoryID, ref Library.WebCore.ResultMessage objResultMessage, String strAuthenData, String strGUID)
        {
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return null;
                EO_NewsCategory objNewsCategory = new EO_NewsCategory();
                objResultMessage = new ERP.EOffice.DA.DA_EO_NewsCategory().LoadInfo(ref objNewsCategory, NewsCategoryID);
                return objNewsCategory;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi nạp chi tiết danh mục tin", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi lấy chi tiết loại tin tức", Globals.ModuleName);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertNewsCategory(ref EO_NewsCategory objEO_NewsCategory, String strAuthenData, String strGUID)
        {
            Library.WebCore.ResultMessage objResultMessage = new ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                if (objEO_NewsCategory.NewsCategoryID == -1)
                    objResultMessage = new ERP.EOffice.DA.DA_EO_NewsCategory().Insert(objEO_NewsCategory);
                else
                {
                    List<EO_NewsCategory> NewsCategoryList = new List<EO_NewsCategory>();
                    new ERP.EOffice.DA.DA_EO_NewsCategory().GetAll(ref NewsCategoryList, 1);
                    List<object> OrderIndexList = new List<object>();
                    int OldOrderIndex = -1;
                    if (NewsCategoryList.Count > 0)
                    {
                        foreach (var item in NewsCategoryList)
                        {
                            if (item.NewsCategoryID == objEO_NewsCategory.NewsCategoryID)
                                OldOrderIndex = item.OrderIndex;
                            OrderIndexList.Add(item.NewsCategoryID);
                        }
                    }

                    // OrderIndex Lessthan OldOrderindex
                    if (OldOrderIndex < OrderIndexList.Count)
                    {
                        if (objEO_NewsCategory.OrderIndex < OldOrderIndex)
                        {
                            int i = OldOrderIndex;
                            var temp = OrderIndexList[i];
                            while (i > objEO_NewsCategory.OrderIndex)
                            {
                                OrderIndexList[i] = OrderIndexList[i - 1];
                                i--;
                            }
                            OrderIndexList[i] = temp;
                        }
                        else
                        {
                            int i = OldOrderIndex;
                            var temp = OrderIndexList[i];
                            while (i < objEO_NewsCategory.OrderIndex)
                            {
                                OrderIndexList[i] = OrderIndexList[i + 1];
                                i++;
                            }
                            OrderIndexList[i] = temp;
                        }
                    }
                    objResultMessage = new ERP.EOffice.DA.DA_EO_NewsCategory().Update(objEO_NewsCategory, OrderIndexList);
                }
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm danh mục tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi thêm danh mục tin tức", Globals.ModuleName);
                return objResultMessage;
            }
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteNewsCategory(ref EO_NewsCategory objEO_NewsCategory, String strAuthenData, String strGUID)
        {
            Library.WebCore.ResultMessage objResultMessage = new ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;

                List<EO_NewsCategory> NewsCategoryList = new List<EO_NewsCategory>();
                new ERP.EOffice.DA.DA_EO_NewsCategory().GetAll(ref NewsCategoryList, 1);
                List<object> OrderIndexList = new List<object>();
                if (NewsCategoryList.Count > 0)
                {
                    foreach (var item in NewsCategoryList)
                    {
                        if (item.NewsCategoryID != objEO_NewsCategory.NewsCategoryID)
                            OrderIndexList.Add(item.NewsCategoryID);
                    }
                }

                objResultMessage = new ERP.EOffice.DA.DA_EO_NewsCategory().Delete(objEO_NewsCategory, OrderIndexList);
                return objResultMessage;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi xóa danh mục tin tức", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi xóa danh mục tin tức", Globals.ModuleName);
                return objResultMessage;
            }
        }
        #endregion

        #region NewsLike

        [WebMethod(EnableSession = true)]
        public List<EO_News_Like> Get_NewsLikeByNewsID(int intNewsID, int intPageIndex, int intPageSize, ref Library.WebCore.ResultMessage objResultMessage, String strAuthenData, String strGUID)
        {
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return null;
                List<EO_News_Like> objNews_LikeLst = new List<EO_News_Like>();
                objResultMessage = new ERP.EOffice.DA.DA_EO_News_Like().GetNewsLikeByNewsID(ref objNews_LikeLst, intNewsID, intPageIndex, intPageSize);
                return objNews_LikeLst;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi nạp danh sách NewsLike theo NewsID", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi nạp danh sách NewsLike theo NewsID", Globals.ModuleName);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public EO_News_Like CheckUserLikeNews(int intNewsID, string strLikeUser, ref Library.WebCore.ResultMessage objResultMessage, String strAuthenData, String strGUID)
        {
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return null;
                EO_News_Like objNews_Like = new EO_News_Like();
                objResultMessage = new ERP.EOffice.DA.DA_EO_News_Like().CheckUserLikeNews(ref objNews_Like, intNewsID, strLikeUser);
                return objNews_Like;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi kiểm ra User đã Like tin tức hay chưa", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi kiểm ra User đã Like tin tức hay chưa", Globals.ModuleName);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertNewsLike(ref EO_News_Like objEO_News_Like, String strAuthenData, String strGUID)
        {
            Library.WebCore.ResultMessage objResultMessage = new ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                if (objEO_News_Like.LikeID < 0)
                {
                    objResultMessage = new ERP.EOffice.DA.DA_EO_News_Like().Insert(ref objEO_News_Like);
                }
                else
                {
                    objResultMessage = new ERP.EOffice.DA.DA_EO_News_Like().Update(objEO_News_Like);
                }

                return objResultMessage;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thêm Like", objEx.ToString());
                ErrorLog.Add(objResultMessage.Message, objResultMessage.MessageDetail, "Lỗi thêm Like", Globals.ModuleName);
                return objResultMessage;
            }
        }

        #endregion
    }
}
