﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using WarrantyRegister = ERP.Inventory.BO.Warranty.WarrantyRegister;
using DA_WarrantyRegister = ERP.Inventory.DA.Warranty.DA_WarrantyRegister;

namespace ERPTransactionServices.Warranty
{
    /// <summary>
    /// Summary description for WSWarrantyRegister
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSWarrantyRegister : System.Web.Services.WebService
    {
        /// <summary>
        /// Tìm kiếm thông tin bảo hành
        /// PM_WARRANTYREGISTER_SRH
        /// </summary>
        [WebMethod(EnableSession = true, Description = "Tìm kiếm đăng ký bảo hành", MessageName = "Tìm kiếm đăng ký bảo hành")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyRegister().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        /// <summary>
        /// Thêm đăng ký báo hành
        /// PM_WARRANTYREGISTER_ADD
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, WarrantyRegister obj)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            obj.CreatedUser = objToken.UserName;
            objResultMessage = new DA_WarrantyRegister().Insert(obj);
            return objResultMessage;
        }

        /// <summary>
        /// Thêm nhiều dòng đăng ký báo hành
        /// PM_WARRANTYREGISTER_ADD
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objWarrantyRegisterList"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertMulti(String strAuthenData, String strGUID, List<WarrantyRegister> objWarrantyRegisterList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyRegister().Insert(objWarrantyRegisterList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage ReRegister(String strAuthenData, String strGUID, WarrantyRegister obj)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyRegister().ReRegister((int)obj.WarrantyRegisterID,objToken.UserName);
            return objResultMessage;
        }

    }
}
