﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using WarrantyInfo = ERP.Inventory.BO.Warranty.WarrantyInfo;
using DA_WarrantyInfo = ERP.Inventory.DA.Warranty.DA_WarrantyInfo;

namespace ERPTransactionServices.Warranty
{
    /// <summary>
    /// Summary description for WSWarrantyInfo
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSWarrantyInfo : System.Web.Services.WebService
    {
        /// <summary>
        /// Tìm kiếm thông tin bảo hành
        /// </summary>
        [WebMethod(EnableSession = true, Description = "Tìm kiếm thông tin bảo hành", MessageName = "Tìm kiếm thông tin bảo hành")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyInfo().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertWarrantyInfo(String strAuthenData, String strGUID, string strIMEI, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyInfo().InsertWarrantyInfo(strIMEI, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CalWarrantyTime(String strAuthenData, String strGUID, string strIMEI, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyInfo().CalWarrantyTime(strIMEI, strProductID);
            return objResultMessage;
        }
    }
}
