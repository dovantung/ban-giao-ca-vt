﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.VtPlus
{
    /// <summary>
    /// Summary description for WSVtPlusCustomer
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSVtPlusCustomer : System.Web.Services.WebService
    {
        
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.VTPlus_Customer objVTPlus_Customer)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Customer objDA_VTPlus_Customer = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Customer();
            objResultMessage = objDA_VTPlus_Customer.Insert(objVTPlus_Customer);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.VTPlus_Customer objVTPlus_Customer, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Customer objDA_VTPlus_Customer = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Customer();
            objResultMessage = objDA_VTPlus_Customer.Update(objVTPlus_Customer);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string intID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.BO.VTPlus_Customer objVTPlus_Customer = new ERP.SalesAndServices.SaleOrders.BO.VTPlus_Customer();
            ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Customer objDA_VTPlus_Customer = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Customer();
            objVTPlus_Customer.ID = intID;
            objResultMessage = objDA_VTPlus_Customer.Delete(objVTPlus_Customer);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Customer().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "VTPlusCustomerList";
            return objResultMessage;
        }
    }
}
