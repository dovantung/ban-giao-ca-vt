﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.VtPlus
{
    /// <summary>
    /// Summary description for WSVtPlus
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSVtPlus : System.Web.Services.WebService
    {
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.SaleOrders.BO.VTPlus_Log objVTPlus_Log, int ID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Log().LoadInfo(ref objVTPlus_Log, ID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.VTPlus_Log objVTPlus_Log)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Log objDA_VTPlus_Log = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Log();
            objResultMessage = objDA_VTPlus_Log.Insert(objVTPlus_Log);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.VTPlus_Log objVTPlus_Log, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Log objDA_VTPlus_Log = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Log();
            objResultMessage = objDA_VTPlus_Log.Update(objVTPlus_Log);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.BO.VTPlus_Log objVTPlus_Log = new ERP.SalesAndServices.SaleOrders.BO.VTPlus_Log();
            ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Log objDA_VTPlus_Log = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Log();
            objVTPlus_Log.ID = intID;
            objResultMessage = objDA_VTPlus_Log.Delete(objVTPlus_Log);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_Log().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "VTPlusLogList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckAddPoint(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_AddPoint().CheckAddPoint(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "VTPlusAddPointList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckDiscountPlus(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_AddPoint().CheckDiscountPlus(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "VTPlusDiscountVTPlusList";
            return objResultMessage;
        }

    }
}
