﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.VtPlus
{
    /// <summary>
    /// Summary description for WSVtplusInventory
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSVtplusInventory : System.Web.Services.WebService
    {

        /// <summary>
        /// Đỗ Văn Tùng 
        /// Thêm dl vào bảng điểm
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objWarrantyExtend_CancelDT"></param>
        /// <returns></returns>

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert_AddPoint(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.VTPlus_AddPoint objVTPlus_AddPoint)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_AddPoint objDA_VTPlus_AddPoint = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_AddPoint();
            objResultMessage = objDA_VTPlus_AddPoint.Insert(objVTPlus_AddPoint);
            return objResultMessage;
        }

        /// <summary>
        /// Đỗ Văn Tùng
        /// Lấy tỉ lệ Viettel ++
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="dtbResult"></param>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPercent(String strAuthenData, String strGUID, ref DataTable dtbResult, string objKeywords, string objKeywords2, string StoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_AddPoint objDA_SaleOrder = new ERP.SalesAndServices.SaleOrders.DA.DA_VTPlus_AddPoint();
            objResultMessage = objDA_SaleOrder.GetPercent(ref dtbResult, objKeywords, objKeywords2, StoreID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectVTPlusPoint(String strAuthenData, String strGUID, ref DataTable dtbData, string StoreProcedure, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.SaleOrders.DA.DA_VtplusInventory().SelectVTPlusPoint(ref dtbData, StoreProcedure, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateVTPlusPoint(String strAuthenData, String strGUID, string StoreProcedure, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.SaleOrders.DA.DA_VtplusInventory().UpdateVTPlusPoint(StoreProcedure, objKeywords);
            return objResultMessage;
        }
    }
}
