﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Account.PostBook
{
    /// <summary>
    /// Summary description for WSACC_PostBook
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSACC_PostBook : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Account.BO.PostBook.ACC_PostBook objPostBook, string strPostBookID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().LoadInfo(ref objPostBook, strPostBookID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Account.BO.PostBook.ACC_PostBook objPostBook)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.PostBook.DA_ACC_PostBook objDA_PostBook = new ERP.Account.DA.PostBook.DA_ACC_PostBook();
            objDA_PostBook.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PostBook.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PostBook.objLogObject.LoginLogID = objToken.LoginLogID;
            objPostBook.CreatedUser = objToken.UserName;
            objResultMessage = objDA_PostBook.Insert(objPostBook);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearPostBookCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertList(String strAuthenData, String strGUID, List<ERP.Account.BO.PostBook.ACC_PostBook> objlstPostBook)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.PostBook.DA_ACC_PostBook objDA_PostBook = new ERP.Account.DA.PostBook.DA_ACC_PostBook();
            objDA_PostBook.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PostBook.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PostBook.objLogObject.LoginLogID = objToken.LoginLogID;
            foreach (ERP.Account.BO.PostBook.ACC_PostBook objPostBook in objlstPostBook)
            {
                if (objPostBook.CreatedUser.Trim().Length == 0)
                    objPostBook.CreatedUser = objToken.UserName;
                else
                    objPostBook.UpdatedUser = objToken.UserName;
            }
            objResultMessage = objDA_PostBook.InsertList(objlstPostBook);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearPostBookCache();
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Account.BO.PostBook.ACC_PostBook objPostBook)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.PostBook.DA_ACC_PostBook objDA_PostBook = new ERP.Account.DA.PostBook.DA_ACC_PostBook();
            objDA_PostBook.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PostBook.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PostBook.objLogObject.LoginLogID = objToken.LoginLogID;
            objPostBook.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_PostBook.Update(objPostBook);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearPostBookCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.BO.PostBook.ACC_PostBook objPostBook = new ERP.Account.BO.PostBook.ACC_PostBook();
            ERP.Account.DA.PostBook.DA_ACC_PostBook objDA_PostBook = new ERP.Account.DA.PostBook.DA_ACC_PostBook();
            objDA_PostBook.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PostBook.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PostBook.objLogObject.LoginLogID = objToken.LoginLogID;
            objPostBook.TransactionID = strTransactionID;
            objPostBook.DeletedUser = objToken.UserName;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().Delete(objPostBook);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearPostBookCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PostBookList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetVoucherCode_NewID(String strAuthenData, String strGUID, ref string strTransactionID, int intTransactionTypeID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().GetVoucherCode_NewID(ref strTransactionID, intTransactionTypeID, intStoreID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPostBook_formTransaction(String strAuthenData, String strGUID, ref DataTable tblResult, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().GetPostBook_formTransaction(ref tblResult, strTransactionID);
            if (tblResult != null)
                tblResult.TableName = "PostBookList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPostBook(String strAuthenData, String strGUID, ref DataTable tblResult, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().GetPostBook(ref tblResult, strTransactionID);
            if (tblResult != null)
                tblResult.TableName = "PostBookList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetLedger(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().GetLedger(ref tblResult, objKeywords);
            
            if (tblResult != null)
            {
                tblResult.TableName = "LedgerList";
            }
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetLedgerToList(String strAuthenData, String strGUID, ref List<ERP.Account.BO.PostBook.ReportObject> listResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DataTable dtb = null;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().GetLedgerToList(ref listResult, objKeywords);
            //strResult = Newtonsoft.Json.JsonConvert.SerializeObject(dtb,Newtonsoft.Json.Formatting.Indented);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataFromStore(String strAuthenData, String strGUID, ref DataTable tblResult,string strStore, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().GetDataFromStore(ref tblResult,strStore, objKeywords);
            if (tblResult != null)
            {
                tblResult.TableName = "PostBookList";
            }
            return objResultMessage;
        }

        /// <summary>
        /// MD_ACCOUNT_VIEWDETAIL
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="tblResult"></param>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataDetail(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().SearchDataDetail(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PostBookList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Kiểm tra copy hạch toán", MessageName = "CheckCopy")]
        public Library.WebCore.ResultMessage CheckCopy(String strAuthenData, String strGUID, ref int intIsCopy, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.PostBook.DA_ACC_PostBook().CheckCopy(ref intIsCopy, strTransactionID);
            return objResultMessage;
        }

    }
}
