﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Account.Transaction.Payable
{
    /// <summary>
    /// Summary description for WSPayableVoucher
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPayableVoucher : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.Payable.DA_PayableVoucher().SearchData(ref tblResult, objKeywords);            
            if (tblResult != null)
                tblResult.TableName = "PayableList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectTotalPayable(String strAuthenData, String strGUID, ref DataTable dtResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.Payable.DA_PayableVoucher().SelectTotalPayable(ref dtResult, objKeywords);
            if (dtResult != null)
                dtResult.TableName = "TotalPayable";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectDetailPayable(String strAuthenData, String strGUID, ref DataTable dtResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.Payable.DA_PayableVoucher().SelectDetailPayable(ref dtResult, objKeywords);
            if (dtResult != null)
                dtResult.TableName = "DetailPayable";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectDetailTransSubDebt(String strAuthenData, String strGUID, ref DataTable dtResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.Payable.DA_PayableVoucher().SelectDetailTransSubDebt(ref dtResult, objKeywords);
            if (dtResult != null)
                dtResult.TableName = "DetailPayable";
            return objResultMessage;
        }

    }
}
