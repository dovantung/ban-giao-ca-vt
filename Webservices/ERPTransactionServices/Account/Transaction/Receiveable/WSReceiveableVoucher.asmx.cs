﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Account.Transaction.Payable
{
    /// <summary>
    /// Summary description for WSReceiveableVoucher
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1) ]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSReceiveableVoucher : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.Receiveable.DA_ReceiveableVoucher().SearchData(ref tblResult, objKeywords);            
            if (tblResult != null)
                tblResult.TableName = "ReceiveableList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectTotalReceiveable(String strAuthenData, String strGUID, ref DataTable dtResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.Receiveable.DA_ReceiveableVoucher().SelectTotalReceiveable(ref dtResult, objKeywords);
            if (dtResult != null)
                dtResult.TableName = "TotalReceiveable";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectDetailReceiveable(String strAuthenData, String strGUID, ref DataTable dtResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.Receiveable.DA_ReceiveableVoucher().SelectDetailReceiveable(ref dtResult, objKeywords);
            if (dtResult != null)
                dtResult.TableName = "DetailReceiveable";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectDetailTransSubDebt(String strAuthenData, String strGUID, ref DataTable dtResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.Receiveable.DA_ReceiveableVoucher().SelectDetailTransSubDebt(ref dtResult, objKeywords);
            if (dtResult != null)
                dtResult.TableName = "DetailReceiveable";
            return objResultMessage;
        }

    }
}
