﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Account.Transaction
{
    /// <summary>
    /// Summary description for WSACC_Transaction
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSACC_Transaction : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Account.BO.Transaction.ACC_Transaction objTransaction, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction().LoadInfo(ref objTransaction, strTransactionID);
            return objResultMessage; 
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertNew(String strAuthenData, String strGUID, ERP.Account.BO.Transaction.ACC_Transaction objTransaction, ref string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.Transaction.DA_ACC_Transaction objDA_Transaction = new ERP.Account.DA.Transaction.DA_ACC_Transaction();
            objDA_Transaction.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Transaction.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Transaction.objLogObject.LoginLogID = objToken.LoginLogID;
            objTransaction.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Transaction.Insert(objTransaction);
            strTransactionID = objTransaction.TransactionID;
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Account.BO.Transaction.ACC_Transaction objTransaction)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.Transaction.DA_ACC_Transaction objDA_Transaction = new ERP.Account.DA.Transaction.DA_ACC_Transaction();
            objDA_Transaction.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Transaction.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Transaction.objLogObject.LoginLogID = objToken.LoginLogID;
            objTransaction.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Transaction.Insert(objTransaction);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransactionCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Account.BO.Transaction.ACC_Transaction objTransaction)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.Transaction.DA_ACC_Transaction objDA_Transaction = new ERP.Account.DA.Transaction.DA_ACC_Transaction();
            objDA_Transaction.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Transaction.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Transaction.objLogObject.LoginLogID = objToken.LoginLogID;
            objTransaction.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_Transaction.Update(objTransaction);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransactionCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.BO.Transaction.ACC_Transaction objTransaction = new ERP.Account.BO.Transaction.ACC_Transaction();
            ERP.Account.DA.Transaction.DA_ACC_Transaction objDA_Transaction = new ERP.Account.DA.Transaction.DA_ACC_Transaction();
            objDA_Transaction.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Transaction.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Transaction.objLogObject.LoginLogID = objToken.LoginLogID;
            objTransaction.TransactionID = strTransactionID;
            objTransaction.DeletedUser = objToken.UserName;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction().Delete(objTransaction);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransactionCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TransactionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetTransaction_NewID(String strAuthenData, String strGUID, ref string strTransactionID, int intTransactionTypeID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction().GetTransaction_NewID(ref strTransactionID,intTransactionTypeID,intStoreID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectDataDetail(String strAuthenData, String strGUID, ref DataTable tblResult, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_TransactionDetail().SelectDataDetail(ref tblResult, strTransactionID);
            if (tblResult != null)
                tblResult.TableName = "TransactionDetailList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Transaction_Locked(String strAuthenData, String strGUID, string strTransactionID, int intTypeLocked)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.Transaction.DA_ACC_Transaction objDA_Transaction = new ERP.Account.DA.Transaction.DA_ACC_Transaction();
            objDA_Transaction.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Transaction.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Transaction.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction().Transaction_Locked(strTransactionID, intTypeLocked, objToken.UserName);
            
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Transaction_InOutcome(String strAuthenData, String strGUID, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.Transaction.DA_ACC_Transaction objDA_Transaction = new ERP.Account.DA.Transaction.DA_ACC_Transaction();

            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction().Transaction_InOutcome(strTransactionID, objToken.UserName);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectDataAttachment(String strAuthenData, String strGUID, ref DataTable tblResult, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction_Attachment().SelectData(ref tblResult, strTransactionID);
            if (tblResult != null)
                tblResult.TableName = "TransactionAttachment";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectDataReviewList(String strAuthenData, String strGUID, ref DataTable tblResult, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction_ReviewList().SelectData(ref tblResult, strTransactionID);
            if (tblResult != null)
                tblResult.TableName = "TransactionReviewList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDataFromTransactionType(String strAuthenData, String strGUID, ref DataTable tblResult, int inttransactionTypeID, decimal decTotalAmount, int intDepartmentID, string strExpenseContnetID, int intCompany)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction_ReviewList().LoadDataFromTransactionType(ref tblResult, inttransactionTypeID, decTotalAmount, intDepartmentID, strExpenseContnetID, intCompany);
            if (tblResult != null)
                tblResult.TableName = "TransactionReviewList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReview(String strAuthenData, String strGUID, ERP.Account.BO.Transaction.ACC_Transaction_ReviewList objReview)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.Transaction.DA_ACC_Transaction_ReviewList objDA_Review = new ERP.Account.DA.Transaction.DA_ACC_Transaction_ReviewList();
            objResultMessage = objDA_Review.Update(objReview);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Report_GetTransaction(String strAuthenData, String strGUID, ref DataSet dsResult, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction().Report_GetTransaction(ref dsResult, strTransactionID);
           
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Report_GetPaymentOrderType(String strAuthenData, String strGUID, ref DataTable dtbResult, string strTransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_Transaction().Report_GetPaymentOrderType(ref dtbResult, strTransactionID);

            return objResultMessage;
        }

    }
}
