﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;

namespace ERPTransactionServices.Account.Transaction
{
    /// <summary>
    /// Summary description for WSACC_TransactionPostClosing
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSACC_TransactionPostClosing : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Account.BO.Transaction.ACC_TransactionPostClosing objTransactionPostClosing, string strTransactionPostClosingID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing().LoadInfo(ref objTransactionPostClosing, strTransactionPostClosingID);
            return objResultMessage;
        }

       

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertData(String strAuthenData, String strGUID,
            List<ERP.Account.BO.Transaction.ACC_TransactionPostClosing> objTransactionPostClosinglst)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing objDA_TransactionPostClosing = new ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing();
            //objDA_TransactionPostClosing.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_TransactionPostClosing.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_TransactionPostClosing.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_TransactionPostClosing.InsertData(objTransactionPostClosinglst,objToken.UserName);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransactionPostClosingCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Account.BO.Transaction.ACC_TransactionPostClosing objTransactionPostClosing)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing objDA_TransactionPostClosing = new ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing();
            //objDA_TransactionPostClosing.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_TransactionPostClosing.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_TransactionPostClosing.objLogObject.LoginLogID = objToken.LoginLogID;
            objTransactionPostClosing.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_TransactionPostClosing.Update(objTransactionPostClosing);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransactionPostClosingCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strClosingtransactionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.BO.Transaction.ACC_TransactionPostClosing objTransactionPostClosing = new ERP.Account.BO.Transaction.ACC_TransactionPostClosing();
            ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing objDA_TransactionPostClosing = new ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing();
            //objDA_TransactionPostClosing.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_TransactionPostClosing.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_TransactionPostClosing.objLogObject.LoginLogID = objToken.LoginLogID;
            objTransactionPostClosing.ClosingTransactionID = strClosingtransactionID;
            objTransactionPostClosing.DeletedUser = objToken.UserName;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing().Delete(objTransactionPostClosing);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransactionPostClosingCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TransactionPostClosingList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadTransactionPostClosing(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing().LoadTransactionPostClosing(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "LoadTransactionPostClosing";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetBalanceSheetByAccount(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.Transaction.DA_ACC_TransactionPostClosing().GetBalanceSheetByAccount(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BalanceSheetData";
            return objResultMessage;
        }
    }
}
