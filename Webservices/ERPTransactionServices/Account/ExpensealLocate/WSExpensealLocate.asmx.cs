﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DAExpensealLocate = ERP.Account.DA.ExpensealLocate;
using BOExpensealLocate = ERP.Account.BO.ExpensealLocate;
using System.Data;
namespace ERPTransactionServices.Account.ExpensealLocate
{
    /// <summary>
    /// Summary description for WSExpensealLocate
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSExpensealLocate : System.Web.Services.WebService
    {
        #region Search Data
        
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "ExpensealLocateList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchVouchers(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate_Voucher().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "VoucherList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchVersions(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate_Version().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "VersionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchCycles(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate_Cycle().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CycleList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDepartments(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate_Department().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "DepartmentList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectDataMethodType(String strAuthenData, String strGUID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate().SelectDataMethodType(ref tblResult);
            if (tblResult != null)
                tblResult.TableName = "DataMethodType";
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataPostBook(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate_Postbook().SearchDataPostBook(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "DataPostBookList";
            return objResultMessage;
        }



        #endregion


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, BOExpensealLocate.ExpensealLocate objExpensealLocate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.ExpensealLocate.DA_ExpensealLocate objDA_ExpensealLocate = new DAExpensealLocate.DA_ExpensealLocate();
            objDA_ExpensealLocate.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ExpensealLocate.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ExpensealLocate.objLogObject.LoginLogID = objToken.LoginLogID;
            objExpensealLocate.CreatedUser = objToken.UserName;
            objExpensealLocate.CreatedStoreID = objToken.LoginStoreID;
            objResultMessage = objDA_ExpensealLocate.Insert(objExpensealLocate);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, BOExpensealLocate.ExpensealLocate objExpensealLocate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.ExpensealLocate.DA_ExpensealLocate objDA_ExpensealLocate = new DAExpensealLocate.DA_ExpensealLocate();
            objDA_ExpensealLocate.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ExpensealLocate.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ExpensealLocate.objLogObject.LoginLogID = objToken.LoginLogID;
            objExpensealLocate.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_ExpensealLocate.Update(objExpensealLocate);
            return objResultMessage;
        }






        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID,  BOExpensealLocate.ExpensealLocate objExpensealLocate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.ExpensealLocate.DA_ExpensealLocate objDA_ExpensealLocate = new DAExpensealLocate.DA_ExpensealLocate();
            objDA_ExpensealLocate.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ExpensealLocate.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ExpensealLocate.objLogObject.LoginLogID = objToken.LoginLogID;
            objExpensealLocate.DeletedUser = objToken.UserName;
            objResultMessage = objDA_ExpensealLocate.Delete(objExpensealLocate);
            return objResultMessage;
        }





        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref BOExpensealLocate.ExpensealLocate objExpensealLocate, int intExpensealLocateID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate().LoadInfo(ref objExpensealLocate, intExpensealLocateID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo_Version(String strAuthenData, String strGUID, ref BOExpensealLocate.ExpensealLocate_Version objExpensealLocate_Version, int intVersionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate_Version().LoadInfo(ref objExpensealLocate_Version, intVersionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CostApportion(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate_Version().CostApportion(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CostApportionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectDataMethodDepartment(String strAuthenData, String strGUID, ref DataTable tblResult, int intEXPENSEALLOCATEDFID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate().SelectDataMethodDepartment(ref tblResult, intEXPENSEALLOCATEDFID);
            if (tblResult != null)
                tblResult.TableName = "DataMethodDepartmentList";
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage PostBook(String strAuthenData, String strGUID, int intCYCLEID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //ERP.Account.DA.ExpensealLocate.DA_ExpensealLocate objDA_ExpensealLocate = new DAExpensealLocate.DA_ExpensealLocate();
            //objDA_ExpensealLocate.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_ExpensealLocate.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_ExpensealLocate.objLogObject.LoginLogID = objToken.LoginLogID;
            //objExpensealLocate.DeletedUser = objToken.UserName;
            objResultMessage = new DAExpensealLocate.DA_ExpensealLocate_Postbook().PostBook(intCYCLEID);
            return objResultMessage;
        }
    }
}
