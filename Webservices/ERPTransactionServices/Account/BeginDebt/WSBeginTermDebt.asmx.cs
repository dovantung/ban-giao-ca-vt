﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Account.BeginDebt
{
    /// <summary>
    /// Summary description for WSBeginTermDebt
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSBeginTermDebt : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Account.BO.BeginDebt.BeginTermDebt objBeginTermDebt, string strBeginTermDebtID, DateTime? dtmBeginTermDebtMonth)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt().LoadInfo(ref objBeginTermDebt, strBeginTermDebtID, dtmBeginTermDebtMonth);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Account.BO.BeginDebt.BeginTermDebt objBeginTermDebt)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.BeginDebt.DA_BeginTermDebt objDA_BeginTermDebt = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt();
            objDA_BeginTermDebt.objLogObject.CertificateString = objToken.CertificateString;
            objDA_BeginTermDebt.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_BeginTermDebt.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_BeginTermDebt.Insert(objBeginTermDebt);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransactionCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Account.BO.BeginDebt.BeginTermDebt objBeginTermDebt)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.BeginDebt.DA_BeginTermDebt objDA_BeginTermDebt = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt();
            objDA_BeginTermDebt.objLogObject.CertificateString = objToken.CertificateString;
            objDA_BeginTermDebt.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_BeginTermDebt.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_BeginTermDebt.Update(objBeginTermDebt);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransactionCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strBeginTermDebtID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.BO.BeginDebt.BeginTermDebt objBeginTermDebt = new ERP.Account.BO.BeginDebt.BeginTermDebt();
            ERP.Account.DA.BeginDebt.DA_BeginTermDebt objDA_BeginTermDebt = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt();
            objDA_BeginTermDebt.objLogObject.CertificateString = objToken.CertificateString;
            objDA_BeginTermDebt.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_BeginTermDebt.objLogObject.LoginLogID = objToken.LoginLogID;
            objBeginTermDebt.BeginTermDebtID = strBeginTermDebtID;
            
            objResultMessage = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt().Delete(objBeginTermDebt);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransactionCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BeginTermDebtList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckExitsBeginDebt(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt().CheckExitsBeginDebt(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BeginTermDebt";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage BalanceBeginTermCalc(String strAuthenData, String strGUID, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.BO.BeginDebt.BeginTermDebt objBeginTermDebt = new ERP.Account.BO.BeginDebt.BeginTermDebt();
            ERP.Account.DA.BeginDebt.DA_BeginTermDebt objDA_BeginTermDebt = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt();
            objResultMessage = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt().BalanceBeginTermCalc(objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage AutoPost(String strAuthenData, String strGUID, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.BO.BeginDebt.BeginTermDebt objBeginTermDebt = new ERP.Account.BO.BeginDebt.BeginTermDebt();
            ERP.Account.DA.BeginDebt.DA_BeginTermDebt objDA_BeginTermDebt = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt();
            objResultMessage = new ERP.Account.DA.BeginDebt.DA_BeginTermDebt().AutoPost(objKeywords);
            return objResultMessage;
        }
    }
}
