﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Account
{
    /// <summary>
    /// Summary description for WSACC_BeginTermBalance
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WS_BeginTermBalance : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Account.BO.BeginTermBalance objBeginTermBalance, string strBeginTermBalanceID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.DA_BeginTermBalance().LoadInfo(ref objBeginTermBalance, strBeginTermBalanceID);
            return objResultMessage;
        }
      
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Account.BO.BeginTermBalance objBeginTermBalance)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.DA_BeginTermBalance objDA_BeginTermBalance = new ERP.Account.DA.DA_BeginTermBalance();
            objBeginTermBalance.CertificateString = objToken.CertificateString;
            objBeginTermBalance.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objBeginTermBalance.LoginLogID = objToken.LoginLogID;
            objBeginTermBalance.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BeginTermBalance.Insert(objBeginTermBalance);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertList(String strAuthenData, String strGUID, DataTable dtb, ref DataTable dtbResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.DA_BeginTermBalance objDA_BeginTermBalance = new ERP.Account.DA.DA_BeginTermBalance();
            objResultMessage = objDA_BeginTermBalance.InsertList(dtb,ref dtbResult, objToken.CertificateString, HttpContext.Current.Request.UserHostAddress, objToken.LoginLogID, objToken.UserName);
            return objResultMessage;
        }
     
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Account.BO.BeginTermBalance objBeginTermBalance)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.DA.DA_BeginTermBalance objDA_BeginTermBalance = new ERP.Account.DA.DA_BeginTermBalance();
            objBeginTermBalance.CertificateString = objToken.CertificateString;
            objBeginTermBalance.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objBeginTermBalance.LoginLogID = objToken.LoginLogID;
            objBeginTermBalance.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BeginTermBalance.Update(objBeginTermBalance);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strArrayID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Account.BO.BeginTermBalance objBeginTermBalance = new ERP.Account.BO.BeginTermBalance();
            ERP.Account.DA.DA_BeginTermBalance objDA_BeginTermBalance = new ERP.Account.DA.DA_BeginTermBalance();
            objBeginTermBalance.CertificateString = objToken.CertificateString;
            objBeginTermBalance.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objBeginTermBalance.LoginLogID = objToken.LoginLogID;
            objResultMessage = new ERP.Account.DA.DA_BeginTermBalance().Delete(strArrayID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.DA_BeginTermBalance().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BeginTermBalanceList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataFromStore(String strAuthenData, String strGUID, ref DataTable tblResult, string strStore, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.DA_BeginTermBalance().GetDataFromStore(ref tblResult, strStore, objKeywords);
            if (tblResult != null)
            {
                tblResult.TableName = "BeginTermBalanceList";
            }
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetInfo(String strAuthenData, String strGUID, ref ERP.Account.BO.BeginTermBalance objBeginTermBalance, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Account.DA.DA_BeginTermBalance().GetInfo(ref objBeginTermBalance, objKeywords);
            return objResultMessage;
        }
    }
}
