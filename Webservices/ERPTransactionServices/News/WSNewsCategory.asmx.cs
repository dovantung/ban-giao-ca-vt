﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.News
{
    /// <summary>
    /// Summary description for WSNewsCategory
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSNewsCategory : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.News.BO.NewsCategory objNewsCategory, int intNewsCategoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.News.DA.DA_NewsCategory().LoadInfo(ref objNewsCategory, intNewsCategoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckBeforeDelete(String strAuthenData, String strGUID, ref int intNumRowDelete, int intNewsCategoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.News.DA.DA_NewsCategory().CheckBeforeDelete(ref intNumRowDelete, intNewsCategoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.News.DA.DA_NewsCategory().SearchData(ref dtbData);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.News.BO.NewsCategory objNewsCategory, ref int intNewsCategoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.News.DA.DA_NewsCategory objDA_NewsCategory = new ERP.News.DA.DA_NewsCategory();
            objDA_NewsCategory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_NewsCategory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_NewsCategory.objLogObject.LoginLogID = objToken.LoginLogID;
            objNewsCategory.CreatedUser = objToken.UserName;
            objResultMessage = objDA_NewsCategory.Insert(objNewsCategory, ref intNewsCategoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.News.BO.NewsCategory objNewsCategory, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.News.DA.DA_NewsCategory objDA_NewsCategory = new ERP.News.DA.DA_NewsCategory();
            objDA_NewsCategory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_NewsCategory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_NewsCategory.objLogObject.LoginLogID = objToken.LoginLogID;
            objNewsCategory.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_NewsCategory.Update(objNewsCategory, lstOrderIndex);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.News.BO.NewsCategory objNewsCategory)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.News.DA.DA_NewsCategory objDA_NewsCategory = new ERP.News.DA.DA_NewsCategory();
            objDA_NewsCategory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_NewsCategory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_NewsCategory.objLogObject.LoginLogID = objToken.LoginLogID;
            objNewsCategory.DeletedUser = objToken.UserName;
            objResultMessage = objDA_NewsCategory.Delete(objNewsCategory);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateUrl(String strAuthenData, String strGUID, int intNewsCategoryID, string strImageURL)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.News.DA.DA_NewsCategory().UpdateUrl(intNewsCategoryID, strImageURL);
            return objResultMessage;
        }
    }
}
