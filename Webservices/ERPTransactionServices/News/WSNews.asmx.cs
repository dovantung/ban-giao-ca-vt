﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.News
{
    /// <summary>
    /// Summary description for WSNews
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSNews : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.News.BO.News objNews, int intNewsID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.News.DA.DA_News().LoadInfo(ref objNews, intNewsID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.News.DA.DA_News().SearchData(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.News.BO.News objNews, ref int intNewsID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.News.DA.DA_News objDA_News = new ERP.News.DA.DA_News();
            objDA_News.objLogObject.CertificateString = objToken.CertificateString;
            objDA_News.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_News.objLogObject.LoginLogID = objToken.LoginLogID;
            objNews.CreatedUser = objToken.UserName;
            objResultMessage = objDA_News.Insert(objNews, ref intNewsID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.News.BO.News objNews, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.News.DA.DA_News objDA_News = new ERP.News.DA.DA_News();
            objDA_News.objLogObject.CertificateString = objToken.CertificateString;
            objDA_News.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_News.objLogObject.LoginLogID = objToken.LoginLogID;
            objNews.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_News.Update(objNews, lstOrderIndex);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.News.BO.News objNews)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.News.DA.DA_News objDA_News = new ERP.News.DA.DA_News();
            objDA_News.objLogObject.CertificateString = objToken.CertificateString;
            objDA_News.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_News.objLogObject.LoginLogID = objToken.LoginLogID;
            objNews.DeletedUser = objToken.UserName;
            objResultMessage = objDA_News.Delete(objNews);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateURL(String strAuthenData, String strGUID, int intNewsID, string strImageURL)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.News.DA.DA_News().UpdateUrl(intNewsID, strImageURL);
            return objResultMessage;
        }

    }
}
