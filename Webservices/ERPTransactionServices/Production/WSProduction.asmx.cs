﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.Production
{
    /// <summary>
    /// Summary description for WSProduction
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSProduction : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder, string strProductionOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().LoadInfo(ref objProductionOrder, strProductionOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadFullInfo(String strAuthenData, String strGUID, ref ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder, string strProductionOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().LoadFullInfo(ref objProductionOrder, strProductionOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert_ProductionOrder(String strAuthenData, String strGUID, ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder, ref string strProductionOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Production.DA.ProductionOrder.DA_ProductionOrder objDA_ProductionOrder = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder();
            objDA_ProductionOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ProductionOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ProductionOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objProductionOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_ProductionOrder.Insert_ProductionOrder(objProductionOrder, ref strProductionOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update_ProductionOrder(String strAuthenData, String strGUID, ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder, string strListProductID_Delete)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Production.DA.ProductionOrder.DA_ProductionOrder objDA_ProductionOrder = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder();
            objDA_ProductionOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ProductionOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ProductionOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objProductionOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_ProductionOrder.Update_ProductionOrder(objProductionOrder, strListProductID_Delete);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Production.DA.ProductionOrder.DA_ProductionOrder objDA_ProductionOrder = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder();
            objDA_ProductionOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ProductionOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ProductionOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objProductionOrder.DeletedUser = objToken.UserName;
            objResultMessage = objDA_ProductionOrder.Delete(objProductionOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateNewProductionOrderID(String strAuthenData, String strGUID, ref String strProductionOrderID, int intCreatedStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().CreateNewProductionOrderID(ref strProductionOrderID, intCreatedStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPriceByStore(String strAuthenData, String strGUID, ref decimal price, String strProductID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().GetPriceByStore(ref price, strProductID, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadProductionOrderDetailTable(String strAuthenData, String strGUID, ref DataTable dtbProductionOrderDetail, String strProductionOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().LoadProductionOrderDetailTable(ref dtbProductionOrderDetail, strProductionOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData_By_KeyWord(String strAuthenData, String strGUID, ref DataTable dtbProductionOrder, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().SearchData_By_KeyWord(ref dtbProductionOrder, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchProductionOrder_Prostep(String strAuthenData, String strGUID, ref DataTable dtbProcessStep, string strProductionOrderID, int intProcessStepID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().SearchProductionOrder_Prostep(ref dtbProcessStep, intProcessStepID, strProductionOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchProductionOrderdDetail_WFStep(String strAuthenData, String strGUID, ref DataTable dtbProductionWorkFlow, string strProductionOrderID, int intProductionWStepID, string strProductionOrderDetailID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().SearchProductionOrderdDetail_WFStep(ref dtbProductionWorkFlow, intProductionWStepID, strProductionOrderID, strProductionOrderDetailID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateProductionOrderDetail_WFStep(String strAuthenData, String strGUID, ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder, string strProductionOrderDetailID, int intProductionWFStepID, string strProductionOrderID, bool bolIsProcess, DateTime dtOrderDate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().UpdateProductionOrderDetail_WFStep(objProductionOrder, strProductionOrderDetailID, intProductionWFStepID, strProductionOrderID, bolIsProcess, dtOrderDate, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadWorkFlowProductIDByProductionOrderType(String strAuthenData, String strGUID, ref DataTable dtbWorkFlowProductID, int intProductionOrderTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().LoadWorkFlowProductIDByProductionOrderType(ref dtbWorkFlowProductID, intProductionOrderTypeID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetRptContractProduction(String strAuthenData, String strGUID, ref DataTable dtbData, string strProductionOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().GetRptContractProduction(ref dtbData, strProductionOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateProductionOrderDetail_LastWFStep(String strAuthenData, String strGUID, ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder, int intProcessStepID, string strProductionOrderDetailID, int intProductionWFStepID, string strProductionOrderID, bool bolIsProcess, DateTime dtOrderDate, int intProductionStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().UpdateProductionOrderDetail_LastWFStep(objProductionOrder, intProcessStepID, strProductionOrderDetailID, intProductionWFStepID, strProductionOrderID, bolIsProcess, dtOrderDate, objToken.UserName, intProductionStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateProductionOrder_ProcessStep(String strAuthenData, String strGUID, string strProductionOrderID, int intProcessStepID, bool bolIsProcess, DateTime dtOrderDate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().UpdateProductionOrder_ProcessStep(strProductionOrderID, intProcessStepID, bolIsProcess, dtOrderDate, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateProductionOrder_LastProcessStep(String strAuthenData, String strGUID, ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder, int intProcessStepID, bool bolIsProcess, DateTime dtOrderDate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().UpdateProductionOrder_LastProcessStep(objProductionOrder, intProcessStepID, bolIsProcess, dtOrderDate, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckVoucher_ProductionOrder(String strAuthenData, String strGUID, ref string strVoucherID, string strProductionOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().CheckVoucher_ProductionOrder(ref strVoucherID, strProductionOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoVoucher(String strAuthenData, String strGUID, ref ERP.SalesAndServices.Payment.BO.Voucher objVoucher, string strVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Payment.DA.DA_Voucher().LoadInfo(ref objVoucher, strVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckOutputVoucher_ProductionOrder(String strAuthenData, String strGUID, ref string strOutputVoucherID, string strProductionOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().CheckOutputVoucher_ProductionOrder(ref strOutputVoucherID, strProductionOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoOutputVoucher(String strAuthenData, String strGUID, ref ERP.Inventory.BO.OutputVoucher objOutputVoucher, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_OutputVoucher().LoadInfo(ref objOutputVoucher, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateVoucher(String strAuthenData, String strGUID, ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder, ERP.SalesAndServices.Payment.BO.Voucher objVoucher, List<ERP.Inventory.BO.OutputVoucher> lstOutputVoucherList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Production.DA.ProductionOrder.DA_ProductionOrder objDA_ProductionOrder = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder();
            objDA_ProductionOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ProductionOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ProductionOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_ProductionOrder.CreateVoucher(objProductionOrder, objVoucher, lstOutputVoucherList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListOrderDetailMaterial(String strAuthenData, String strGUID, ref List<ERP.Production.BO.ProductionOrder.ProductionOrderDetailMaterial> lstProductionOrderDetailMaterial, string strProductionOderID, string strProductionOrderDetailID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Production.DA.ProductionOrder.DA_ProductionOrderDetailMaterial objDA_ProductionOrderDetailMaterial = new ERP.Production.DA.ProductionOrder.DA_ProductionOrderDetailMaterial();
            objResultMessage = objDA_ProductionOrderDetailMaterial.LoadListOrderDetailMaterial(ref lstProductionOrderDetailMaterial, strProductionOderID, strProductionOrderDetailID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchRecomandByPOT(String strAuthenData, String strGUID, ref DataTable dtbData, int intStoreID, int intProductionOrderTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrderDetailMaterial().SearchRecomandByPOT(ref dtbData, intStoreID, intProductionOrderTypeID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SaveOutputVoucher(String strAuthenData, String strGUID, ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder, int intProductionOrderTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().SaveOutputVoucher(objProductionOrder, intProductionOrderTypeID, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SaveInputVoucher(String strAuthenData, String strGUID, ERP.Production.BO.ProductionOrder.ProductionOrder objProductionOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().SaveInputVoucher(objProductionOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateOutputVoucher(String strAuthenData, String strGUID, List<ERP.Inventory.BO.OutputVoucher> lstOutputVoucherList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().CreateOutputVoucher( lstOutputVoucherList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadProductByInputVoucher(String strAuthenData, String strGUID, ref DataTable dtbData, string strInputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().LoadProductByInputVoucher(ref dtbData, strInputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage AddProductionStoreChange(String strAuthenData, String strGUID, object[]objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Production.DA.ProductionOrder.DA_ProductionOrder().AddProductionStoreChange(objKeywords);
            return objResultMessage;
        }
    }
}
