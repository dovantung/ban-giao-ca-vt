﻿using ERP.SalesAndServices.SaleOrders.BO;
using ERP.SalesAndServices.SaleOrders.DA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.SalesAndServices.SaleOrders
{
    /// <summary>
    /// Summary description for WSPreSaleOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPreSaleOrder : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref PreSaleOrder objPreSaleOrder, string strPreSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_PreSaleOrder().LoadInfo(ref objPreSaleOrder, strPreSaleOrderID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PreSaleOrder objDA_PreSaleOrder = new DA_PreSaleOrder();
            objResultMessage = objDA_PreSaleOrder.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "PreSaleOrder";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchRequestData(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PSORequest objDA_PreSaleOrder = new DA_PSORequest();
            objResultMessage = objDA_PreSaleOrder.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "PreSaleOrder";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetProduct(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PreSaleOrder objDA_PreSaleOrder = new DA_PreSaleOrder();
            objResultMessage = objDA_PreSaleOrder.GetProduct(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "Product";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, PreSaleOrder objPreSaleOrder, ERP.SalesAndServices.Payment.BO.Voucher objVoucher, ref string intPreSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PreSaleOrder objDA_PreSaleOrder = new DA_PreSaleOrder();
            objDA_PreSaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PreSaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PreSaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objPreSaleOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_PreSaleOrder.Insert(objPreSaleOrder, objVoucher);
            intPreSaleOrderID = objPreSaleOrder.PreSaleOrderID;
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, PreSaleOrder objPreSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PreSaleOrder objDA_PreSaleOrder = new DA_PreSaleOrder();
            objDA_PreSaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PreSaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PreSaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objPreSaleOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_PreSaleOrder.Update(objPreSaleOrder);
            return objResultMessage;
        }
        

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataReQuest(String strAuthenData, String strGUID, ref System.Data.DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PSORequest objDA_PSORequest = new DA_PSORequest();
            objResultMessage = objDA_PSORequest.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "PSORequest";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoReQuest(String strAuthenData, String strGUID, ref PSORequest objPSORequest, string strPSORequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PSORequest objDA_PSORequest = new DA_PSORequest();
            objResultMessage = objDA_PSORequest.LoadInfo(ref objPSORequest, strPSORequestID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoReQuestBy(String strAuthenData, String strGUID, ref PSORequest objPSORequest, string strPSORequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PSORequest objDA_PSORequest = new DA_PSORequest();
            objResultMessage = objDA_PSORequest.LoadInfoBy(ref objPSORequest, strPSORequestID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertReQuest(String strAuthenData, String strGUID, PSORequest objPSORequest, ref string PSORequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PSORequest objDA_PSORequest = new DA_PSORequest();
            objDA_PSORequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PSORequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PSORequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objPSORequest.CreatedUser = objToken.UserName;
            objResultMessage = objDA_PSORequest.Insert(objPSORequest, ref PSORequestID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReQuest(String strAuthenData, String strGUID, PSORequest objPSORequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PSORequest objDA_PSORequest = new DA_PSORequest();
            objDA_PSORequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PSORequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PSORequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objPSORequest.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_PSORequest.Update(objPSORequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteReQuest(String strAuthenData, String strGUID, PSORequest objPSORequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PSORequest objDA_PSORequest = new DA_PSORequest();
            objPSORequest.DeletedUser = objToken.UserName;
            objResultMessage = objDA_PSORequest.Delete(objPSORequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewListReQuest(String strAuthenData, String strGUID, PSORequest_ReviewList objPSORequest_ReviewList, int intCurrentReviewLevelID, string strContentReview)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PSORequest_ReviewList objDA_PSORequest_ReviewList = new DA_PSORequest_ReviewList();
            objResultMessage = objDA_PSORequest_ReviewList.UpdateReviewList(objPSORequest_ReviewList, intCurrentReviewLevelID, strContentReview);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeletePreSaleOrder(String strAuthenData, String strGUID, PreSaleOrder objPreSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PreSaleOrder objDA_PreSaleOrder = new DA_PreSaleOrder();
            objPreSaleOrder.DeletedUser = objToken.UserName;
            objResultMessage = objDA_PreSaleOrder.Delete(objPreSaleOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPromotionOutPutTypeID(String strAuthenData, String strGUID, ref int intOutputTypeID, int intSaleOrderTypeId, int bolIsHasPrice)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PreSaleOrder objDA_PreSaleOrder = new DA_PreSaleOrder();
            intOutputTypeID = objDA_PreSaleOrder.GetOutPutTypeID(intSaleOrderTypeId, bolIsHasPrice);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage AutoReviewRequest(String strAuthenData, String strGUID, PSORequest objPSORequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PSORequest objDA_PSORequest = new DA_PSORequest();
            objResultMessage = objDA_PSORequest.AutoReview(objPSORequest);
            return objResultMessage;
        }
    }
}
