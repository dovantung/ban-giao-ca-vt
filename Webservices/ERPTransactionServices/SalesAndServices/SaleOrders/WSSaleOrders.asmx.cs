﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.SalesAndServices.SaleOrders.BO;
using ERP.SalesAndServices.SaleOrders.DA;
using ERP.SalesAndServices.Payment.BO;
using ERP.SalesAndServices.Payment.DA;
using ERP.Inventory.BO;
using System.Data;

namespace ERPTransactionServices.SalesAndServices.SaleOrders
{
    /// <summary>
    /// Summary description for WSSaleOrders
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSSaleOrders : System.Web.Services.WebService
    {
        // Phongnt - 19/03/2020
        // Get url api
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetUrlApi(String strAuthenData, String strGUID, ref DataTable dtbResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetUrlApi(ref dtbResult);
            if (dtbResult != null)
                dtbResult.TableName = "GetUrlApi";
            return objResultMessage;
        }
        // Phongnt - 16/03/2020
        // Get name SaleOrder by TypeId
        [WebMethod(EnableSession = true)]
        public string GetSaleOrderTypeName(String strAuthenData, String strGUID, int saleOrderTypeId)
        {
            string saleOrderName = "";
            DA_SaleOrder objDA_SaleOrder_GetName = new DA_SaleOrder();
            saleOrderName = objDA_SaleOrder_GetName.GetSaleOrderName(saleOrderTypeId);
            return saleOrderName;
        }
        // Phongnt - 20/04/2020
        // Lấy token
        [WebMethod(EnableSession = true)]
        public string GetToken(String strAuthenData, String strGUID, string phoneNumber)
        {
            string token = "";
            DA_SaleOrder objDA_SaleOrder_GetToken = new DA_SaleOrder();
            token = objDA_SaleOrder_GetToken.GetToken(phoneNumber);
            return token;
        }
        // Phongnt - 20/04/2020
        // Lấy điểm cần tiêu
        [WebMethod(EnableSession = true)]
        public int GetRedeemPoint(String strAuthenData, String strGUID, string saleOrderId, string phoneNumber)
        {
            int pointAmount = 0;
            DA_SaleOrder objDA_SaleOrder_GetRedeemPoint = new DA_SaleOrder();
            pointAmount = objDA_SaleOrder_GetRedeemPoint.GetRedeemPoint(saleOrderId, phoneNumber);
            return pointAmount;
        }
        [WebMethod(EnableSession = true)]
        public int GetAddPoint(String strAuthenData, String strGUID, string saleOrderId, string phoneNumber)
        {
            int pointAmount = 0;
            DA_SaleOrder objDA_SaleOrder_GetAddPoint = new DA_SaleOrder();
            pointAmount = objDA_SaleOrder_GetAddPoint.GetAddPoint(saleOrderId, phoneNumber);
            return pointAmount;
        }
        [WebMethod(EnableSession = true)]
        public int GetTranId(String strAuthenData, String strGUID, string saleOrderId, string phoneNumber)
        {
            int tranId = 0;
            DA_SaleOrder objDA_SaleOrder_GetTranId = new DA_SaleOrder();
            tranId = objDA_SaleOrder_GetTranId.GetTranId(saleOrderId, phoneNumber);
            return tranId;
        }

        [WebMethod(EnableSession = true)]
        public int GetIdAddPoint(String strAuthenData, String strGUID, string idExchange, int type)
        {
            int Id = 0;
            DA_SaleOrder objDA_SaleOrder_GetIdAddPoint = new DA_SaleOrder();
            Id = objDA_SaleOrder_GetIdAddPoint.GetIdAddPoint(idExchange, type);
            return Id;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateInforByVatInVoiceId(String strAuthenData, String strGUID, string vatInvocieId, string taxCustomerAddress, string customerTaxID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_VatInvoice objDA_VTPlus_Update = new DA_VTPlus_VatInvoice();
            objResultMessage = objDA_VTPlus_Update.UpdateInforByVatInVoiceId(vatInvocieId, taxCustomerAddress, customerTaxID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateInforByVatInVoiceIdExt(String strAuthenData, String strGUID, string vatInvocieId, string taxCustomerAddress, string customerTaxID, string CustomerAddress, string CustomerEmail, string Buyer, string CustomerName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_VatInvoice objDA_VTPlus_Update = new DA_VTPlus_VatInvoice();
            objResultMessage = objDA_VTPlus_Update.UpdateInforByVatInVoiceIdExt(vatInvocieId, taxCustomerAddress, customerTaxID, CustomerAddress, CustomerEmail, Buyer, CustomerName);
            return objResultMessage;
        }
        // Phongnt - 04/04/2020
        // Tích điểm lại
        [WebMethod(EnableSession = true)]
        public void UpdateAddpoint(String strAuthenData, String strGUID, string saleOrderId, int type)
        {
            DA_SaleOrder objDA_SaleOrder_UpdateAddpoint = new DA_SaleOrder();
            objDA_SaleOrder_UpdateAddpoint.UpdateAddpoint(saleOrderId, type);
        }

        // Phongnt - 23/04/2020
        // Thay đổi lại trạng thái
        [WebMethod(EnableSession = true)]
        public void UpdateStatusApi(String strAuthenData, String strGUID, string saleOrderId, bool status)
        {
            DA_SaleOrder objDA_SaleOrder_UpdateStatusApi = new DA_SaleOrder();
            objDA_SaleOrder_UpdateStatusApi.UpdateStatusApi(saleOrderId, status);
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref SaleOrder objSaleOrder, string strSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleOrder().LoadInfo(ref objSaleOrder, strSaleOrderID);
            return objResultMessage;
        }

        

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadCustomer_OutPutVoucherID(String strAuthenData, String strGUID, ref SaleOrder objSaleOrder, string strOutPutVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleOrder().LoadCustomer_OutPutVoucherID(ref objSaleOrder, strOutPutVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tạo đơn hàng, không tạo phiếu thu, không tạo phiếu xuất", MessageName = "Tao_don_hang")]
        public Library.WebCore.ResultMessage CreateSaleOrder(String strAuthenData, String strGUID, ref string strSaleOrderID, SaleOrder objSaleOrder)
        {
            return CreateSaleOrder(strAuthenData, strGUID, ref strSaleOrderID, objSaleOrder, null, new List<OutputVoucher>(), null);
        }

        [WebMethod(EnableSession = true, Description = "Tạo đơn hàng, có thể tạo phiếu thu và phiếu xuất", MessageName = "Tao_don_hang_co_the_thu_va_xuat")]
        public Library.WebCore.ResultMessage CreateSaleOrder(String strAuthenData, String strGUID, ref string strSaleOrderID, SaleOrder objSaleOrder,
            Voucher objVoucher, List<OutputVoucher> lstOutputVoucherList, ERP.CRM.BO.CRMSaleOrder objCRMSaleOrder, string strPreSaleOrderID = "")
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.CreateSaleOrder(objSaleOrder, objVoucher, lstOutputVoucherList, objCRMSaleOrder, strPreSaleOrderID);
            if (objResultMessage.IsError == false)
            {
                strSaleOrderID = objSaleOrder.SaleOrderID;
            }
          
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, SaleOrder objSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.Update(objSaleOrder);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateAfterOutput(String strAuthenData, String strGUID, SaleOrder objSaleOrder, VoucherDetail voucherDetail)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.Update(objSaleOrder, voucherDetail);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, SaleOrder objSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objSaleOrder.DeletedUser = objToken.UserName;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.Delete(objSaleOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "SaleOrderList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataVtPlus(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.SearchDataVtPlus(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "SaleOrderVtPlusList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateSaleOrderID(String strAuthenData, String strGUID, ref string strSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.CreateSaleOrderID(ref strSaleOrderID, objToken.LoginStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateSaleOrderDetail(String strAuthenData, String strGUID, ref List<SaleOrderDetail> lstSaleOrderDetail)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrderDetail objDA_SaleOrderDetail = new DA_SaleOrderDetail();
            objResultMessage = objDA_SaleOrderDetail.GetDataList(ref lstSaleOrderDetail, string.Empty);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Review(String strAuthenData, String strGUID, SaleOrder objSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            if (string.IsNullOrEmpty(objSaleOrder.ReviewedUser))
                objSaleOrder.ReviewedUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.Review(objSaleOrder);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewList(String strAuthenData, String strGUID, SaleOrder_ReviewList objSaleOrder_ReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder_ReviewList objDA_SaleOrder_ReviewList = new DA_SaleOrder_ReviewList();
            objResultMessage = objDA_SaleOrder_ReviewList.UpdateReviewList(objSaleOrder_ReviewList);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delivery(String strAuthenData, String strGUID, SaleOrder objSaleOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.DeliveryUpdateUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.Delivery(objSaleOrder);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateVoucher(String strAuthenData, String strGUID, SaleOrder objSaleOrderDelivery, Voucher objVoucher, List<OutputVoucher> lstOutputVoucherList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.CreateVoucher(objSaleOrderDelivery, objVoucher, lstOutputVoucherList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateVoucherMulti(String strAuthenData, String strGUID, List<SaleOrderOnOne> lstSaleOrderOnOne)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.CreateVoucherMulti(lstSaleOrderOnOne);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateSupplementaryVoucher(String strAuthenData, String strGUID, Voucher objSupplementaryVoucher, Voucher objVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.CreateSupplementaryVoucher(objSupplementaryVoucher, objVoucher);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateOutputVoucher(String strAuthenData, String strGUID, SaleOrder objSaleOrder, List<OutputVoucher> lstOutputVoucherList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.IsOutProductInMobile = false;
            objResultMessage = objDA_SaleOrder.CreateOutputVoucher(objSaleOrder, lstOutputVoucherList);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateOutputVoucherForMobile(String strAuthenData, String strGUID, SaleOrder objSaleOrder,
            List<OutputVoucher> lstOutputVoucherList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.IsOutProductInMobile = true;
            objSaleOrder.OutputUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.CreateOutputVoucher(objSaleOrder, lstOutputVoucherList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertActionLog(String strAuthenData, String strGUID, SaleOrder_ActionLog objSaleOrder_ActionLog)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder_ActionLog objDA_SaleOrder_ActionLog = new DA_SaleOrder_ActionLog();
            objSaleOrder_ActionLog.CertificateString = objToken.CertificateString;
            objSaleOrder_ActionLog.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objSaleOrder_ActionLog.LoginLogID = objToken.LoginLogID;
            objSaleOrder_ActionLog.ActionUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder_ActionLog.Insert(objSaleOrder_ActionLog);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchActionLog(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder_ActionLog objDA_SaleOrder_ActionLog = new DA_SaleOrder_ActionLog();
            objResultMessage = objDA_SaleOrder_ActionLog.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "SaleOrderAction";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertReportLog(String strAuthenData, String strGUID, SaleOrder_ReportLog objSaleOrder_ReportLog)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder_ReportLog objDA_SaleOrder_ReportLog = new DA_SaleOrder_ReportLog();
            objSaleOrder_ReportLog.CertificateString = objToken.CertificateString;
            objSaleOrder_ReportLog.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objSaleOrder_ReportLog.LoginLogID = objToken.LoginLogID;
            objSaleOrder_ReportLog.ActionUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder_ReportLog.Insert(objSaleOrder_ReportLog);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchReportLog(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder_ReportLog objDA_SaleOrder_ReportLog = new DA_SaleOrder_ReportLog();
            objResultMessage = objDA_SaleOrder_ReportLog.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "SaleOrderReportLog";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckProductInputByCustomer(String strAuthenData, String strGUID, ref bool bolIsResult, string strProductID, int intCustomerID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.CheckProductInputByCustomer(ref bolIsResult, strProductID, intCustomerID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckOutputReturnIMEI(String strAuthenData, String strGUID, ref bool bolIsResult,
            string strProductID, string strIMEI, int intOutputStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.CheckOutputReturnIMEI(ref bolIsResult, strProductID, strIMEI, intOutputStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetSaleOrderInfoForMB(String strAuthenData, String strGUID, ref DataSet dsResult, string strSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetSaleOrderInfoForMB(ref dsResult, strSaleOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lay danh sach don hang cho Mobile_All", MessageName = "GetSaleOrder_Created_by_Mobile_All")]
        public Library.WebCore.ResultMessage SearchDataForMobile(String strAuthenData, String strGUID, ref DataSet dsResult,
            DateTime dtmFromDate, DateTime dtmToDate, string strSaleOrderTypeIDList, int intOutputStoreID, int intIsOutProduct, int intIsReviewed,
            int intIsIncome, int intIsDelivery, string strKeyword, int intSearchType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.SearchDataForMobile(ref dsResult, dtmFromDate, dtmToDate, strSaleOrderTypeIDList,
                objToken.UserName, intOutputStoreID, intIsOutProduct, intIsReviewed, intIsIncome, intIsDelivery, strKeyword, intSearchType);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage ProcessWorkflow(String strAuthenData, String strGUID, SaleOrder_WorkFlow objSaleOrder_WorkFlow)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder_WorkFlow objDA_SaleOrder_WorkFlow = new DA_SaleOrder_WorkFlow();
            objResultMessage = objDA_SaleOrder_WorkFlow.Process(objSaleOrder_WorkFlow);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm lịch sử IMEI lắp đặt (For Mobile)", MessageName = "Tim_kiem_lich_su_imei_lap_dat")]
        public Library.WebCore.ResultMessage SearchInstallIMEILog(String strAuthenData, String strGUID, ref DataTable dtbResult, string strIMEI)
        {
            List<object> lstKeywordList = new List<object>();
            lstKeywordList.Add("@IMEI"); lstKeywordList.Add(strIMEI);
            return SearchInstallIMEILog(strAuthenData, strGUID, ref dtbResult, lstKeywordList.ToArray());
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm lịch sử IMEI lắp đặt_Mới (For Mobile)", MessageName = "Tim_kiem_lich_su_imei_lap_dat_moi")]
        public Library.WebCore.ResultMessage SearchInstallIMEILog(String strAuthenData, String strGUID, ref DataTable dtbResult, string strIMEI, bool bolIsGetSaleOutput)
        {
            List<object> lstKeywordList = new List<object>();
            lstKeywordList.Add("@IMEI"); lstKeywordList.Add(strIMEI);
            lstKeywordList.Add("@IsGetSaleOutput"); lstKeywordList.Add(bolIsGetSaleOutput);
            return SearchInstallIMEILog(strAuthenData, strGUID, ref dtbResult, lstKeywordList.ToArray());
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchInstallIMEILog(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.SearchInstallIMEILog(ref dtbResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataMiniPrint(String strAuthenData, String strGUID, ref DataTable dtbResult, string saleOrderId)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetDataMiniPrint(ref dtbResult, saleOrderId);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage MiniPrintGetImei(String strAuthenData, String strGUID, ref DataTable dtbResult, string saleOrderId, string productID, decimal price)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.MiniPrintGetImei(ref dtbResult, saleOrderId, productID, price);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage MiniPrintCheckStore(String strAuthenData, String strGUID, ref bool bolIsPrint, int storeid)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.MiniPrintCheckStore(ref bolIsPrint, storeid);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadWarrantyInfoByIMEI(String strAuthenData, String strGUID, ref WarrantyInfo objWarrantyInfo, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_WarrantyInfo objDA_WarrantyInfo = new DA_WarrantyInfo();
            objResultMessage = objDA_WarrantyInfo.LoadInfoByIMEI(ref objWarrantyInfo, strIMEI);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadWarrantyInfoByIMEI_CheckDate(String strAuthenData, String strGUID, ref WarrantyInfo objWarrantyInfo, string strIMEI, DateTime dteCheckDate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_WarrantyInfo objDA_WarrantyInfo = new DA_WarrantyInfo();
            objResultMessage = objDA_WarrantyInfo.LoadInfoByIMEI(ref objWarrantyInfo, strIMEI, dteCheckDate);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadSaleIMEIInfoByIMEI(String strAuthenData, String strGUID, ref SaleIMEIList objSaleIMEIList, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleIMEIList objDA_SaleIMEIList = new DA_SaleIMEIList();
            objResultMessage = objDA_SaleIMEIList.LoadInfo(ref objSaleIMEIList, strIMEI);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchWarrantyExtend(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_WarrantyExtend objDA_WarrantyExtend = new DA_WarrantyExtend();
            objResultMessage = objDA_WarrantyExtend.SearchData(ref dtbResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetInstallProduct(String strAuthenData, String strGUID, ref string strInstallProductID,
            string strInstallIMEI, int intInstalledMonthCheck, string strSaleOrderTypeIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetInstallProduct(ref strInstallProductID, strInstallIMEI, intInstalledMonthCheck, strSaleOrderTypeIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateWifiVoucher(String strAuthenData, String strGUID, string strSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.UpdateWifiVoucher(strSaleOrderID, objToken.LoginStoreID, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetProcessList(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetProcessList(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "ProcessList";
            return objResultMessage;
        }
        /// <summary>
        /// Lấy danh sách đơn hàng cùng số điện thoại, chưa xuất
        /// SM_SALEORDER_GETSIMILAR
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="dtbResult"></param>
        /// <param name="strCustomerPhone"></param>
        /// <param name="intOutputStoreID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "Lấy danh sách đơn hàng cùng số điện thoại, chưa xuất", MessageName = "DS_don_hang_cung_sdt_chua_xuat")]
        public Library.WebCore.ResultMessage GetDataSimilar(String strAuthenData, String strGUID, ref DataTable dtbResult,
            string strCustomerPhone, int intOutputStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetDataSimilar(ref dtbResult, strCustomerPhone, intOutputStoreID);
            if (dtbResult != null)
                dtbResult.TableName = "DataSimilar";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeletePending(String strAuthenData, String strGUID, string strSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.DeletePending(strSaleOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckRetailPrice_InstallIMEI(String strAuthenData, String strGUID, ref string strResult,
            string strInstallIMEI, decimal decAmountRetailPrice, decimal decMaxAdjustSalePricePercent)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.CheckRetailPrice_InstallIMEI(ref strResult, strInstallIMEI, decAmountRetailPrice, decMaxAdjustSalePricePercent);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Kiem tra danh sach san pham lap dat cua IMEI")]
        public Library.WebCore.ResultMessage CheckProductForInstallIMEI(String strAuthenData, String strGUID, ref string strResult,
            string strInstallIMEI, decimal decMaxAdjustSalePricePercent, List<SaleOrderDetail> lstSaleOrderDetailList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.CheckProductForInstallIMEI(ref strResult, strInstallIMEI, decMaxAdjustSalePricePercent, lstSaleOrderDetailList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Khoi phục thong tin lap dat cua IMEI")]
        public Library.WebCore.ResultMessage RevertInstallIMEI(String strAuthenData, String strGUID, ref string strResult, string strSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.RevertInstallIMEI(ref strResult, strSaleOrderID, objToken.UserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckCoupon(String strAuthenData, String strGUID, ref CouponIssue objCouponIssue, CouponQuery objCouponQuery)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            if (objCouponQuery == null)
            {
                objCouponIssue = new CouponIssue();
                objCouponIssue.Message = "CouponQuery bị null";
                return objResultMessage;
            }
            DA_CouponIssue objDA_CouponIssue = new DA_CouponIssue();
            objDA_CouponIssue.objLogObject.CertificateString = objToken.CertificateString;
            objDA_CouponIssue.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_CouponIssue.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_CouponIssue.LoadInfo(ref objCouponIssue, objCouponQuery, objToken.UserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCouponApplySaleOrder(String strAuthenData, String strGUID, ref DataTable dtbResult,
            string strSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CouponIssue objDA_CouponIssue = new DA_CouponIssue();
            objResultMessage = objDA_CouponIssue.GetApplySaleOrder(ref dtbResult, strSaleOrderID);
            if (dtbResult != null)
                dtbResult.TableName = "CouponApply";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lay danh sach don hang cung mot khach", MessageName = "SearchDataForMobile")]
        public Library.WebCore.ResultMessage SearchSaleOrderRelate(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.SearchSaleOrderRelate(ref dtbResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lay chi tiet cua nhieu don hang", MessageName = "GetSaleOrderDetail")]
        public Library.WebCore.ResultMessage GetSaleOrderDetail(String strAuthenData, String strGUID, ref DataTable dtbResult, string strSaleOrderIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetSaleOrderDetail(ref dtbResult, strSaleOrderIDList);
            return objResultMessage;
        }


        #region WarrantyExtend_Cancel

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo_WarrantyExtend_Cancel(String strAuthenData, String strGUID, ref PM_WarrantyExtend_Cancel objWarrantyExtend_Cancel, string strWarrantyExtendID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyExtend_Cancel().LoadInfo(ref objWarrantyExtend_Cancel, strWarrantyExtendID);
            if (objResultMessage != null)
            {
                DataTable dtbWarrantyExtend = null;
                string strProduct = string.Empty;
                objResultMessage = new DA_WarrantyExtend_Cancel().SearchDataByIMEI(ref dtbWarrantyExtend, ref strProduct, objWarrantyExtend_Cancel.IMEI, strWarrantyExtendID, 0);
                if (objResultMessage.IsError)
                    return objResultMessage;

                if (dtbWarrantyExtend != null)
                    dtbWarrantyExtend.TableName = "WarrantyExtend";
                objWarrantyExtend_Cancel.PM_WarrantyExtend_DTB = dtbWarrantyExtend;
                objWarrantyExtend_Cancel.Product = strProduct;
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataCustom_WarrantyExtend_Cancel(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyExtend_Cancel().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "WarrantyExtend_Cancel";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert_WarrantyExtend_Cancel(String strAuthenData, String strGUID, PM_WarrantyExtend_Cancel objWarrantyExtend_Cancel, ref string strWarrantyExtendCancelID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_WarrantyExtend_Cancel objDA_WarrantyExtend_Cancel = new DA_WarrantyExtend_Cancel();
            objWarrantyExtend_Cancel.CreateUser = objToken.UserName;
            objResultMessage = objDA_WarrantyExtend_Cancel.Insert(objWarrantyExtend_Cancel, ref strWarrantyExtendCancelID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update_WarrantyExtend_Cancel(String strAuthenData, String strGUID, PM_WarrantyExtend_Cancel objWarrantyExtend_Cancel, bool bolIsHandle)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_WarrantyExtend_Cancel objDA_WarrantyExtend_Cancel = new DA_WarrantyExtend_Cancel();
            objWarrantyExtend_Cancel.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_WarrantyExtend_Cancel.Update(objWarrantyExtend_Cancel, bolIsHandle);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete_WarrantyExtend_Cancel(String strAuthenData, String strGUID, PM_WarrantyExtend_Cancel objWarrantyExtend_Cancel)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_WarrantyExtend_Cancel objDA_WarrantyExtend_Cancel = new DA_WarrantyExtend_Cancel();
            objWarrantyExtend_Cancel.DeletedUser = objToken.UserName;
            objResultMessage = objDA_WarrantyExtend_Cancel.Delete(objWarrantyExtend_Cancel);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo_WarrantyExtend_CancelDT(String strAuthenData, String strGUID, ref PM_WarrantyExtend_CancelDT objWarrantyExtend_CancelDT, string strWarrantyExtendID, decimal decWarrantyExtendID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyExtend_CancelDT().LoadInfo(ref objWarrantyExtend_CancelDT, strWarrantyExtendID, decWarrantyExtendID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataCustom_WarrantyExtend_CancelDT(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyExtend_CancelDT().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "WarrantyExtend_CancelDT";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert_WarrantyExtend_CancelDT(String strAuthenData, String strGUID, PM_WarrantyExtend_CancelDT objWarrantyExtend_CancelDT)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_WarrantyExtend_CancelDT objDA_WarrantyExtend_Cancel = new DA_WarrantyExtend_CancelDT();
            objResultMessage = objDA_WarrantyExtend_Cancel.Insert(objWarrantyExtend_CancelDT);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update_WarrantyExtend_CancelDT(String strAuthenData, String strGUID, PM_WarrantyExtend_CancelDT objWarrantyExtend_CancelDT)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_WarrantyExtend_CancelDT objDA_WarrantyExtend_Cancel = new DA_WarrantyExtend_CancelDT();
            objResultMessage = objDA_WarrantyExtend_Cancel.Update(objWarrantyExtend_CancelDT);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete_WarrantyExtend_CancelDT(String strAuthenData, String strGUID, PM_WarrantyExtend_CancelDT objWarrantyExtend_CancelDT)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_WarrantyExtend_CancelDT objDA_WarrantyExtend_CancelDT = new DA_WarrantyExtend_CancelDT();
            objResultMessage = objDA_WarrantyExtend_CancelDT.Delete(objWarrantyExtend_CancelDT);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataByIMEI_WarrantyExtend_Cancel(String strAuthenData, String strGUID, ref DataTable tblResult, ref string strProduct, string strIMEI, int IsInsert)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_WarrantyExtend_Cancel().SearchDataByIMEI(ref tblResult, ref strProduct, strIMEI, IsInsert);
            if (tblResult != null)
                tblResult.TableName = "WarrantyExtend_Cancel_ByIMEI";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage ReportLog_PrintTime(String strAuthenData, String strGUID, ref int intPrintTime, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleOrder_ReportLog().ReportLog_PrintTime(ref intPrintTime, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteListReportLog(String strAuthenData, String strGUID, List<string> lstReportLogID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleOrder_ReportLog().DeleteListReportLog(lstReportLogID, objToken.UserName);
            return objResultMessage;
        }

        #endregion

        #region Đăng bổ sung hàm hỗ trợ bên ERP Viettel
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetFormTypebySaleOrderType(String strAuthenData, String strGUID, ref DataTable dtbFormType, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleOrder().GetFormTypebySaleOrderType(ref dtbFormType, objKeywords);
            if (!objResultMessage.IsError)
            {
                dtbFormType.TableName = "dtbFormType";
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetAllProductNoTax(String strAuthenData, String strGUID, ref DataTable dtbProductNotax)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetAllProductNoTax(ref dtbProductNotax);
            if (dtbProductNotax != null)
            {
                dtbProductNotax.TableName = "dtbProductNotax";
                DataColumn[] PrimaryKey = new DataColumn[1];
                PrimaryKey[0] = dtbProductNotax.Columns["PRODUCTID"];
                dtbProductNotax.PrimaryKey = PrimaryKey;
            }
            return objResultMessage;
        }
        /*
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateSaleOrder(String strAuthenData, String strGUID, ref string strSaleOrderID, SaleOrder objSaleOrder,
            Voucher objVoucher, ref List<OutputVoucher> lstOutputVoucherListOut, List<OutputVoucher> lstOutputVoucherList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.CreateSaleOrder(objSaleOrder, objVoucher, lstOutputVoucherList);
            strSaleOrderID = objSaleOrder.SaleOrderID;
            if (!objResultMessage.IsError)
                lstOutputVoucherListOut = lstOutputVoucherList.ToList();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateVoucherReturn(String strAuthenData, String strGUID, SaleOrder objSaleOrderDelivery, Voucher objVoucher, List<OutputVoucher> lstOutputVoucherList, ref List<OutputVoucher> lstOutputVoucherListOut)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.CreateVoucher(objSaleOrderDelivery, objVoucher, lstOutputVoucherList);
            if (!objResultMessage.IsError)
                lstOutputVoucherListOut = lstOutputVoucherList.ToList();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateOutputVoucherReturn(String strAuthenData, String strGUID, SaleOrder objSaleOrder, ref List<OutputVoucher> lstOutputVoucherListOut, List<OutputVoucher> lstOutputVoucherList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.IsOutProductInMobile = false;
            objResultMessage = objDA_SaleOrder.CreateOutputVoucher(objSaleOrder, lstOutputVoucherList);
            if (!objResultMessage.IsError)
                lstOutputVoucherListOut = lstOutputVoucherList.ToList();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateVoucherMultiReturn(String strAuthenData, String strGUID, List<SaleOrderOnOne> lstSaleOrderOnOne, ref List<SaleOrderOnOne> lstSaleOrderOnOneOut)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.CreateVoucherMulti(lstSaleOrderOnOne);
            if (!objResultMessage.IsError)
                lstSaleOrderOnOneOut = lstSaleOrderOnOne;
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateListSaleOrder(String strAuthenData, String strGUID, ref string strParentSaleOrderID, ref List<SaleOrder_SimConnection> lstSaleOrder_SimConnectionLISTOut, List<SaleOrder_SimConnection> lstSaleOrder_SimConnectionLIST)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            foreach (SaleOrder_SimConnection objSaleOrder_SimConnection in lstSaleOrder_SimConnectionLIST)
            {
                objSaleOrder_SimConnection.SaleOrder.CreatedUser = objToken.UserName;
            }
            objResultMessage = objDA_SaleOrder.CreateListSaleOrder(lstSaleOrder_SimConnectionLIST);
            strParentSaleOrderID = lstSaleOrder_SimConnectionLIST[0].SaleOrder.SaleOrderID;
            if (!objResultMessage.IsError)
                lstSaleOrder_SimConnectionLISTOut = lstSaleOrder_SimConnectionLIST.ToList();
            return objResultMessage;
        }
        */
        #region Đăng bổ sung 06/03/2018
        [WebMethod(EnableSession = true, Description = "Tao don hang va tao hoa don tai server")]
        public Library.WebCore.ResultMessage CreateSaleOrder(String strAuthenData, String strGUID, ref string strSaleOrderID, SaleOrder objSaleOrder,
           Voucher objVoucher, List<OutputVoucher> lstOutputVoucherList, ref List<string> lstVATInvoiceList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.CreateSaleOrder(objSaleOrder, objVoucher, lstOutputVoucherList, ref lstVATInvoiceList);
            if(objResultMessage.IsError == false)
            { 
                strSaleOrderID = objSaleOrder.SaleOrderID;
                objDA_SaleOrder.UpdateApplyPromotion(strSaleOrderID);
            }
            //if (!objResultMessage.IsError)
            //    foreach (var item in lstOutputVoucherList)
            //    {

            //    }
            return objResultMessage;
        }
        //huent66 bổ sung
        [WebMethod(EnableSession = true, Description = "Tao don hang va tao hoa don tai server TCDT ADMIN")]
        public Library.WebCore.ResultMessage CreateSaleOrderAdmin(string transtype,String strAuthenData, String strGUID, ref string strSaleOrderID, SaleOrder objSaleOrder,
          Voucher objVoucher, List<OutputVoucher> lstOutputVoucherList, ref List<string> lstVATInvoiceList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.CreateSaleOrderAdmin(transtype, objSaleOrder, objVoucher, lstOutputVoucherList, ref lstVATInvoiceList);
            if (objResultMessage.IsError == false)
            {
                strSaleOrderID = objSaleOrder.SaleOrderID;
            }
         
            //if (!objResultMessage.IsError)
            //    foreach (var item in lstOutputVoucherList)
            //    {

            //    }
            return objResultMessage;
        }
        [WebMethod(EnableSession = true, Description = "Thu tien va xuat hang nhanh => tao hoa don tai server")]
        public Library.WebCore.ResultMessage CreateVoucherMultiReturn(String strAuthenData, String strGUID, List<SaleOrderOnOne> lstSaleOrderOnOne, ref List<SaleOrderOnOneVAT> lstSaleOrderOnOneVAT)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.CreateVoucherMulti(lstSaleOrderOnOne, ref lstSaleOrderOnOneVAT);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tao don hang sim => tao hoa don tai server")]
        public Library.WebCore.ResultMessage CreateListSaleOrder(String strAuthenData, String strGUID, ref string strParentSaleOrderID, ref List<SaleOrderOnOneVAT> lstSaleOrderOnOneVAT, List<SaleOrder_SimConnection> lstSaleOrder_SimConnectionLIST)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            foreach (SaleOrder_SimConnection objSaleOrder_SimConnection in lstSaleOrder_SimConnectionLIST)
            {
                objSaleOrder_SimConnection.SaleOrder.CreatedUser = objToken.UserName;
            }
            objResultMessage = objDA_SaleOrder.CreateListSaleOrder(lstSaleOrder_SimConnectionLIST, ref lstSaleOrderOnOneVAT);
            strParentSaleOrderID = lstSaleOrder_SimConnectionLIST[0].SaleOrder.SaleOrderID;
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Tao phieu xuat va tao hoa don tai server")]
        public Library.WebCore.ResultMessage CreateVoucherReturn(String strAuthenData, String strGUID, SaleOrder objSaleOrderDelivery, Voucher objVoucher, List<OutputVoucher> lstOutputVoucherList, ref List<string> lstVATInvoiceList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_SaleOrder.CreateVoucher(objSaleOrderDelivery, objVoucher, lstOutputVoucherList, ref lstVATInvoiceList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tao phieu xuat  va tao hoa don tai server")]
        public Library.WebCore.ResultMessage CreateOutputVoucherReturn(String strAuthenData, String strGUID, SaleOrder objSaleOrder, ref List<string> lstVATInvoiceList, List<OutputVoucher> lstOutputVoucherList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.IsOutProductInMobile = false;
            objResultMessage = objDA_SaleOrder.CreateOutputVoucher(objSaleOrder, lstOutputVoucherList, ref lstVATInvoiceList);
            return objResultMessage;
        }
        #endregion

        #endregion

        //Đăng Kiểm tra sản phẩm có phải là sản phẩm đặc quyền không?
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckPromotionOffer(String strAuthenData, String strGUID, ref bool bolResult, int intPromotionOfferID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_Privilege objDA_Privilege = new DA_Privilege();
            objResultMessage = objDA_Privilege.CheckPromotionOffer(ref bolResult, intPromotionOfferID);
            return objResultMessage;
        }

        //Đăng Kiểm tra phiếu xuất có sản phẩm đặc quyền không?
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckOutputVoucher(String strAuthenData, String strGUID, ref bool bolResult, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_Privilege objDA_Privilege = new DA_Privilege();
            objResultMessage = objDA_Privilege.CheckOutputVoucher(ref bolResult, strOutputVoucherID);
            return objResultMessage;
        }
        //Kiểm tra IMEI có áp dụng sản phẩm đặc quyền không?
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckPrivilegeIMEI(String strAuthenData, String strGUID, ref bool bolResult, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_Privilege objDA_Privilege = new DA_Privilege();
            objResultMessage = objDA_Privilege.CheckPrivilegeIMEI(ref bolResult, strIMEI);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertPreOrder_SO_OrderIndex(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.SaleOrder objSaleOrder, int intCreateType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                new DA_SaleOrder().InsertPreOrder_SO_OrderIndex(objSaleOrder, intCreateType);
            }
            catch (Exception ex)
            {
                objResultMessage.IsError = true;
                objResultMessage.Message = ex.Message;
            }

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdatePreOrder_SO_OrderIndex(String strAuthenData, String strGUID, DataTable dtPreOrder_SO_OrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                new DA_SaleOrder().UpdatePreOrder_SO_OrderIndex(dtPreOrder_SO_OrderIndex);
            }
            catch (Exception ex)
            {
                objResultMessage.IsError = true;
                objResultMessage.Message = ex.Message;
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateSaleOrderAndVoucher(String strAuthenData, String strGUID, SaleOrder objSaleOrder, Voucher objVoucher, DataTable dtPreOrder_SO_OrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.UpdateSaleOrderAndVoucher(objSaleOrder, objVoucher, dtPreOrder_SO_OrderIndex);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateFreOrder(String strAuthenData, String strGUID, SaleOrder objSaleOrder, Voucher objVoucher, DataTable dtPreOrder_SO_OrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objDA_SaleOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_SaleOrder.UpdateSaleOrderFreOrder(objSaleOrder, objVoucher, dtPreOrder_SO_OrderIndex);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckSaleOrderIsSubsidy(String strAuthenData, String strGUID, ref string strResult, string strSaleOrderId)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.CheckSaleOrderIsSubsidy(ref strResult, strSaleOrderId);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDisCountByOrder(String strAuthenData, String strGUID, ref decimal decResult, string strSaleOrderId)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetDisCountByOrder(ref decResult, strSaleOrderId);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckDepositSubsidy(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.CheckDepositSubsidy(ref dtResult, objKeywords);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoByPreOrder(String strAuthenData, String strGUID, ref SaleOrder objSaleOrder,ref Voucher objVoucher, string strPreSaleOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleOrder().LoadInfoByPreOrder(ref objSaleOrder,ref objVoucher, strPreSaleOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckExistContractID(String strAuthenData, String strGUID, ref bool bolIsExist, string strContractID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleOrder().CheckExistsContractID(ref bolIsExist, strContractID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertLogVNPay(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.VNPay objVNPay)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                new DA_VNPay().InsertVNPayLog(objVNPay);
            }
            catch (Exception ex)
            {
                objResultMessage.IsError = true;
                objResultMessage.Message = ex.Message;
            }

            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetConf(String strAuthenData, String strGUID, ref string ouput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ouput =new DA_VNPay().GetConf();
            }
            catch (Exception ex)
            {
                objResultMessage.IsError = true;
                objResultMessage.Message = ex.Message;
            }

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetLogVNPAY(String strAuthenData, String strGUID, ref DataTable ouput, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ouput = new DA_VNPay().GetLogVNPay(objKeywords);
            }
            catch (Exception ex)
            {
                objResultMessage.IsError = true;
                objResultMessage.Message = ex.Message;
            }

            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetTeminal(String strAuthenData, String strGUID, ref DataTable ouput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ouput = new DA_VNPay().GetTeminals();
            }
            catch (Exception ex)
            {
                objResultMessage    .IsError = true;
                objResultMessage.Message = ex.Message;
            }

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPromotionVNPay(String strAuthenData, String strGUID, ref DataTable ouput, string lstProduct, string storeid)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            try
            {
                Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
                if (objToken == null)
                    return objResultMessage;
                ouput = new DA_VNPay().GetPromotionVNPay(lstProduct, storeid);
            }
            catch (Exception ex)
            {
                objResultMessage.IsError = true;
                objResultMessage.Message = ex.Message;
            }

            return objResultMessage;
        }


        #region Thêm service tích hợp Viettel ++

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreVTPlus(String strAuthenData, String strGUID, ref DataTable dtbResult, string storeId)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetStoreVTPlus(ref dtbResult, storeId);
            if (dtbResult != null)
                dtbResult.TableName = "GetStoreVTPlus";
            return objResultMessage;
        }

        /// <summary>
        /// Đỗ Văn Tùng
        /// Add service lấy danh sách điểm
        /// </summary>
        /// <param name="strAuthenData"></param>d
        /// <param name="strGUID"></param>
        /// <param name="dtbResult"></param>
        /// <param name="objKeywords"></param>
        /// <returns></returns>

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataAddPoint(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_AddPoint objDA_VTPlus_AddPoint = new DA_VTPlus_AddPoint();
            objResultMessage = objDA_VTPlus_AddPoint.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "AddPointList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSubPoint(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_AddPoint objDA_VTPlus_AddPoint = new DA_VTPlus_AddPoint();
            objResultMessage = objDA_VTPlus_AddPoint.SearchDataSubPoint(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "SubPointList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataCanRe(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_AddPoint objDA_VTPlus_AddPoint = new DA_VTPlus_AddPoint();
            objResultMessage = objDA_VTPlus_AddPoint.SearchDataCanReHistory(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "CanReHistoryList";
            return objResultMessage;
        }

        /// <summary>
        /// Đỗ Văn Tùng 
        /// Thêm dl vào bảng điểm
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objWarrantyExtend_CancelDT"></param>
        /// <returns></returns>

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert_AddPoint(String strAuthenData, String strGUID, VTPlus_AddPoint objVTPlus_AddPoint)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_AddPoint objDA_VTPlus_AddPoint = new DA_VTPlus_AddPoint();
            objResultMessage = objDA_VTPlus_AddPoint.Insert(objVTPlus_AddPoint);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert_AddPointDetail(String strAuthenData, String strGUID, VTPlus_AddPointDetail objVTPlus_AddPointDetail)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_AddPointDetail objDA_VTPlus_AddPointDetail = new DA_VTPlus_AddPointDetail();
            objResultMessage = objDA_VTPlus_AddPointDetail.Insert(objVTPlus_AddPointDetail);
            return objResultMessage;
        }

        //public Library.WebCore.ResultMessage Update_AddPoint(String strAuthenData, String strGUID, VTPlus_AddPoint objVTPlus_AddPoint)
        //{
        //    Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
        //    Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
        //    if (objToken == null)
        //        return objResultMessage;
        //    DA_VTPlus_AddPoint objDA_VTPlus_AddPoint = new DA_VTPlus_AddPoint();
        //    objResultMessage = objDA_VTPlus_AddPoint.Update(objVTPlus_AddPoint);
        //    return objResultMessage;
        //}

        // check loại hình thức bán
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetSaleOrderTypeId(String strAuthenData, String strGUID, ref DataTable dtbResult, int saleOrderTypeId)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SaleOrder objDA_SaleOrder = new DA_SaleOrder();
            objResultMessage = objDA_SaleOrder.GetSaleOrderTypeId(ref dtbResult, saleOrderTypeId);
            if (dtbResult != null)
                dtbResult.TableName = "GetSaleOrderTypeId";
            return objResultMessage;
        }

        /// <summary>
        /// Nguyễn Thế Phong
        /// Lấy tổng tiền trừ PQT, tiêu điểm đã được phân bổ
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="dtbResult"></param>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataTotalBeforVAT(String strAuthenData, String strGUID, ref DataTable dtbResult, string invoiceId, int pointAmount)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_AddPoint objDA_SaleOrder = new DA_VTPlus_AddPoint();
            objResultMessage = objDA_SaleOrder.GetDataTotalBeforVAT(ref dtbResult, invoiceId, pointAmount);

            return objResultMessage;
        }

        /// <summary>
        /// Đỗ Văn Tùng
        /// Lấy tỉ lệ Viettel ++
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="dtbResult"></param>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPercent(String strAuthenData, String strGUID, ref DataTable dtbResult, string objKeywords, string objKeywords2, string objStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_AddPoint objDA_SaleOrder = new DA_VTPlus_AddPoint();
            objResultMessage = objDA_SaleOrder.GetPercent(ref dtbResult, objKeywords, objKeywords2, objStoreID);

            return objResultMessage;
        }

        /// <summary>
        /// Đỗ Văn Tùng
        /// Lấy tỉ lệ Viettel ++
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="dtbResult"></param>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetInfoAddress(String strAuthenData, String strGUID, ref DataTable dtbResult, string provinceCode, string districtCode, string wardCode)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_VTPlus_AddPoint objDA_SaleOrder = new DA_VTPlus_AddPoint();
            objResultMessage = objDA_SaleOrder.GetInfoAddress(ref dtbResult, provinceCode, districtCode, wardCode);

            return objResultMessage;
        }

        #endregion
    }

}

