﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.SalesAndServices.SaleOrders.BO;
using ERP.SalesAndServices.SaleOrders.DA;
namespace ERPTransactionServices.SalesAndServices.SaleOrders
{
    /// <summary>
    /// Summary description for WSGiftVoucherIssue
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSGiftVoucherIssue : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref GiftVoucherIssue objGiftVoucherIssue, decimal decimalGiftVoucherIssueID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_GiftVoucherIssue().LoadInfo(ref objGiftVoucherIssue, decimalGiftVoucherIssueID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CertifyGiftVoucher(String strAuthenData, String strGUID, ref GiftVoucherIssue objGiftVoucherIssue, int intGiftVoucherTypeID,
            string strCertifyString)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_GiftVoucherIssue objDA_GiftVoucherIssue = new DA_GiftVoucherIssue();
            objDA_GiftVoucherIssue.objLogObject.CertificateString = objToken.CertificateString;
            objDA_GiftVoucherIssue.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_GiftVoucherIssue.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_GiftVoucherIssue.CertifyGiftVoucher(ref objGiftVoucherIssue, intGiftVoucherTypeID, strCertifyString, objToken.UserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CertifyGiftVoucherForMobile(String strAuthenData, String strGUID, ref GiftVoucherIssue objGiftVoucherIssue, int intGiftVoucherTypeID,
            string strCertifyString, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_GiftVoucherIssue objDA_GiftVoucherIssue = new DA_GiftVoucherIssue();
            objDA_GiftVoucherIssue.objLogObject.CertificateString = objToken.CertificateString;
            objDA_GiftVoucherIssue.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_GiftVoucherIssue.objLogObject.LoginLogID = objToken.LoginLogID;
            if (string.IsNullOrEmpty(strUserName))
            {
                strUserName = objToken.UserName;
            }
            objResultMessage = objDA_GiftVoucherIssue.CertifyGiftVoucher(ref objGiftVoucherIssue, intGiftVoucherTypeID, strCertifyString, strUserName);
            return objResultMessage;
        }
    }
}
