﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.SalesAndServices.Price
{
    /// <summary>
    /// Summary description for WSPriceIMEI
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPriceIMEI : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.Price.BO.PriceIMEI objPriceIMEI, string strIMEI, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_PriceIMEI().LoadInfo(ref objPriceIMEI, strIMEI, strProductID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, DataTable dtbPriceIMEI, bool bolUpdatePrice, bool bolUpdateRewardPoint, bool bolUpdateAll)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceIMEI objDA_PriceIMEI = new ERP.SalesAndServices.Price.DA.DA_PriceIMEI();
            //objDA_PriceIMEI.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_PriceIMEI.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_PriceIMEI.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PriceIMEI.Update(dtbPriceIMEI, bolUpdatePrice, bolUpdateRewardPoint, bolUpdateAll, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchPriceByIMEI(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceIMEI objDA_Price = new ERP.SalesAndServices.Price.DA.DA_PriceIMEI();
            objResultMessage = objDA_Price.SearchPriceByIMEI(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PriceIMEI";
            return objResultMessage;
        }

    }
}
