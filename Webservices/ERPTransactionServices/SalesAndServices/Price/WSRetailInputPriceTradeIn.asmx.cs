﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.SalesAndServices.Price
{
    /// <summary>
    /// Summary description for WSRetailInputPriceTradeIn
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRetailInputPriceTradeIn : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.SalesAndServices.Price.BO.RetailInputPrice_TRADEIN objRetailInputPrice_TRADEIN)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_RetailInputPrice_TRADEIN objDA_RetailInputPrice_TRADEIN = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice_TRADEIN();
            objDA_RetailInputPrice_TRADEIN.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RetailInputPrice_TRADEIN.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RetailInputPrice_TRADEIN.objLogObject.LoginLogID = objToken.LoginLogID;
            objRetailInputPrice_TRADEIN.CreatedUser = objToken.UserName;
            objResultMessage = objDA_RetailInputPrice_TRADEIN.Insert(objRetailInputPrice_TRADEIN);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.SalesAndServices.Price.BO.RetailInputPrice_TRADEIN objRetailInputPrice_TRADEIN)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_RetailInputPrice_TRADEIN objDA_RetailInputPrice_TRADEIN = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice_TRADEIN();
            objDA_RetailInputPrice_TRADEIN.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RetailInputPrice_TRADEIN.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RetailInputPrice_TRADEIN.objLogObject.LoginLogID = objToken.LoginLogID;
            objRetailInputPrice_TRADEIN.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RetailInputPrice_TRADEIN.Update(objRetailInputPrice_TRADEIN);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateIsReview(String strAuthenData, String strGUID, ERP.SalesAndServices.Price.BO.RetailInputPrice_TRADEIN objRetailInputPrice_TRADEIN)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_RetailInputPrice_TRADEIN objDA_RetailInputPrice_TRADEIN = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice_TRADEIN();
            objDA_RetailInputPrice_TRADEIN.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RetailInputPrice_TRADEIN.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RetailInputPrice_TRADEIN.objLogObject.LoginLogID = objToken.LoginLogID;
            objRetailInputPrice_TRADEIN.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RetailInputPrice_TRADEIN.UpdateIsReview(objRetailInputPrice_TRADEIN);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strRetailInputPriceID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.BO.RetailInputPrice_TRADEIN objRetailInputPrice_TRADEIN = new ERP.SalesAndServices.Price.BO.RetailInputPrice_TRADEIN();
            ERP.SalesAndServices.Price.DA.DA_RetailInputPrice_TRADEIN objDA_RetailInputPrice_TRADEIN = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice_TRADEIN();
            objDA_RetailInputPrice_TRADEIN.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RetailInputPrice_TRADEIN.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RetailInputPrice_TRADEIN.objLogObject.LoginLogID = objToken.LoginLogID;
            objRetailInputPrice_TRADEIN.RetailInputPriceID = strRetailInputPriceID;
            objRetailInputPrice_TRADEIN.DeletedUser = objToken.UserName;
            objResultMessage = objDA_RetailInputPrice_TRADEIN.Delete(objRetailInputPrice_TRADEIN);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetRetailInputPrice_RPT(String strAuthenData, String strGUID, ref DataTable tblResult, string strReteailInputPriceID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice_TRADEIN().GetRetailInputPriceTradeIn_RPT(ref tblResult, strReteailInputPriceID);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }
    }
}
