﻿using ERP.SalesAndServices.Payment.BO;
using ERP.SalesAndServices.Payment.DA;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.SalesAndServices.Price
{
    /// <summary>
    /// Summary description for WSExportCSV
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSExportCSV : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public void ExportFileCSV(List<int> lst,string products )
        {
            SynchronizePriceSupermarket func = new SynchronizePriceSupermarket();
            string path= ConfigurationManager.AppSettings["PATH_FILE_CSV"];
            if (!string.IsNullOrWhiteSpace(path)) func.SetPathExport = path;
            var _DO = func.Do(products, lst);
        }
        
        [WebMethod(EnableSession = true)]
        public string DowloadFile(string filename)
        {
            try {
                string pathValue = ConfigurationManager.AppSettings["PATH_FILE_DOWNLOAD_PRINT_FILE"];
                pathValue = pathValue.TrimEnd('\\');
                var Bytes = File.ReadAllBytes(string.Format(@"{0}\{1}", pathValue, filename));
                string base64String = Convert.ToBase64String(Bytes);
                return base64String;
            } catch(Exception ex) {
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public DataTable GetStore()
        {
            var objGetStores = new ERP.SalesAndServices.Payment.DA.DA_ExportPrintCabinet();
            var result = objGetStores.GetStores();
            return result;
        }
        [WebMethod(EnableSession = true)]
        public DataTable GetProducts(int storeid, int promotionid)
        {
            var objGetStores = new ERP.SalesAndServices.Payment.DA.DA_ExportPrintCabinet();
            var tblResult = objGetStores.GetProductPrint(storeid, promotionid);
            tblResult.TableName = "ProductPrint";
            return tblResult;
        }

        [WebMethod(EnableSession = true)]
        public DataTable GetLabelsPrint()
        {
            var objGetStores = new ERP.SalesAndServices.Payment.DA.DA_ExportPrintCabinet();
            var tblResult = objGetStores.GetLabelsPrint();
            tblResult.TableName = "GetLabelsPrint";
            return tblResult;
        }

        [WebMethod(EnableSession = true)]
        public DataTable GetTitle(int promotionid)
        {
            var objGetStores = new ERP.SalesAndServices.Payment.DA.DA_ExportPrintCabinet();
            var tblResult = objGetStores.GetTitle(promotionid);
            tblResult.TableName = "GetTitle";
            return tblResult;
        }

        [WebMethod(EnableSession = true)]
        public DataTable GetListMSBByCabinetID(int intCabinetID)
        {
            var objGetStores = new ERP.SalesAndServices.Payment.DA.DA_ExportPrintCabinet();
            var tblResult = objGetStores.GetListMSBByCabinetID(intCabinetID);
            tblResult.TableName = "GetListMSBByCabinetID";
            return tblResult;
        }
    }
}
