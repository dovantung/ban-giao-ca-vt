﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.SalesAndServices.Price
{
    /// <summary>
    /// Summary description for WSInputPrice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSInputPrice : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.Price.BO.InputPrice objInputPrice, int InputPriceTypeID, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_InputPrice().LoadInfo(ref objInputPrice, InputPriceTypeID, strProductID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.SalesAndServices.Price.BO.InputPrice objInputPrice)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_InputPrice objDA_InputPrice = new ERP.SalesAndServices.Price.DA.DA_InputPrice();
            objDA_InputPrice.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputPrice.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputPrice.objLogObject.LoginLogID = objToken.LoginLogID;
            objInputPrice.CreatedUser = objToken.UserName;
            objResultMessage = objDA_InputPrice.Insert(objInputPrice);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertList(String strAuthenData, String strGUID, List<ERP.SalesAndServices.Price.BO.InputPrice> lstInputPrice)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_InputPrice objDA_InputPrice = new ERP.SalesAndServices.Price.DA.DA_InputPrice();
            objDA_InputPrice.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputPrice.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputPrice.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_InputPrice.Insert(lstInputPrice);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.SalesAndServices.Price.BO.InputPrice objInputPrice)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_InputPrice objDA_InputPrice = new ERP.SalesAndServices.Price.DA.DA_InputPrice();
            objDA_InputPrice.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputPrice.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputPrice.objLogObject.LoginLogID = objToken.LoginLogID;
            objInputPrice.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_InputPrice.Update(objInputPrice);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intInputPriceTypeID, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.BO.InputPrice objInputPrice = new ERP.SalesAndServices.Price.BO.InputPrice();
            ERP.SalesAndServices.Price.DA.DA_InputPrice objDA_InputPrice = new ERP.SalesAndServices.Price.DA.DA_InputPrice();
            objDA_InputPrice.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputPrice.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputPrice.objLogObject.LoginLogID = objToken.LoginLogID;
            objInputPrice.InputPriceTypeID = intInputPriceTypeID;
            objInputPrice.ProductID = strProductID;
            objResultMessage = objDA_InputPrice.Delete(objInputPrice);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_InputPrice().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }
    }
}
