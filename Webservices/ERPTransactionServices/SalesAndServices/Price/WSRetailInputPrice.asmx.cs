﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.SalesAndServices.Price
{
    /// <summary>
    /// Summary description for WSRetailInputPrice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRetailInputPrice : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.Price.BO.RetailInputPrice objRetailInputPrice, string strRetailInputPriceID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().LoadInfo(ref objRetailInputPrice, strRetailInputPriceID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.SalesAndServices.Price.BO.RetailInputPrice objRetailInputPrice)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_RetailInputPrice objDA_RetailInputPrice = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice();
            objDA_RetailInputPrice.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RetailInputPrice.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RetailInputPrice.objLogObject.LoginLogID = objToken.LoginLogID;
            objRetailInputPrice.CreatedUser = objToken.UserName;
            objResultMessage = objDA_RetailInputPrice.Insert(objRetailInputPrice);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.SalesAndServices.Price.BO.RetailInputPrice objRetailInputPrice)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_RetailInputPrice objDA_RetailInputPrice = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice();
            objDA_RetailInputPrice.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RetailInputPrice.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RetailInputPrice.objLogObject.LoginLogID = objToken.LoginLogID;
            objRetailInputPrice.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RetailInputPrice.Update(objRetailInputPrice);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateIsReview(String strAuthenData, String strGUID, ERP.SalesAndServices.Price.BO.RetailInputPrice objRetailInputPrice)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_RetailInputPrice objDA_RetailInputPrice = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice();
            objDA_RetailInputPrice.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RetailInputPrice.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RetailInputPrice.objLogObject.LoginLogID = objToken.LoginLogID;
            objRetailInputPrice.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RetailInputPrice.UpdateIsReview(objRetailInputPrice);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, MessageName = "Mac_dinh")]
        public Library.WebCore.ResultMessage UpdateIsReviewForMobile(String strAuthenData, String strGUID, string strRetailInputPriceID)
        {
            return UpdateIsReviewForMobile(strAuthenData, strGUID, strRetailInputPriceID, string.Empty);
        }
        [WebMethod(EnableSession = true, MessageName = "Duyet_theo_nv_duyet")]
        public Library.WebCore.ResultMessage UpdateIsReviewForMobile(String strAuthenData, String strGUID,
            string strRetailInputPriceID, string strReviewedUser)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new Library.WebCore.ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Chức năng này bị cấm sử dụng", "Không được duyệt định giá trên IPAD nữa");
            return objResultMessage;
            //if (objToken == null)
            //    return objResultMessage;
            //ERP.SalesAndServices.Price.DA.DA_RetailInputPrice objDA_RetailInputPrice = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice();
            //objDA_RetailInputPrice.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_RetailInputPrice.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_RetailInputPrice.objLogObject.LoginLogID = objToken.LoginLogID;
            //ERP.SalesAndServices.Price.BO.RetailInputPrice objRetailInputPrice = new ERP.SalesAndServices.Price.BO.RetailInputPrice();
            //objRetailInputPrice.RetailInputPriceID = strRetailInputPriceID;
            //objRetailInputPrice.IsReviewed = true;
            //if (string.IsNullOrEmpty(strReviewedUser))
            //{
            //    objRetailInputPrice.ReviewedUser = objToken.UserName;
            //}
            //else
            //    objRetailInputPrice.ReviewedUser = strReviewedUser;
            //objResultMessage = objDA_RetailInputPrice.UpdateIsReview(objRetailInputPrice);
            //return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strRetailInputPriceID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.BO.RetailInputPrice objRetailInputPrice = new ERP.SalesAndServices.Price.BO.RetailInputPrice();
            ERP.SalesAndServices.Price.DA.DA_RetailInputPrice objDA_RetailInputPrice = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice();
            objDA_RetailInputPrice.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RetailInputPrice.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RetailInputPrice.objLogObject.LoginLogID = objToken.LoginLogID;
            objRetailInputPrice.RetailInputPriceID = strRetailInputPriceID;
            objRetailInputPrice.DeletedUser = objToken.UserName;
            objResultMessage = objDA_RetailInputPrice.Delete(objRetailInputPrice);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }

        /// <summary>
        /// Nạp thông tin định giá in phiếu thu mua
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="tblResult"></param>
        /// <param name="strReteailInputPriceID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetRetailInputPrice_RPT(String strAuthenData, String strGUID, ref DataTable tblResult, string strReteailInputPriceID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().GetRetailInputPrice_RPT(ref tblResult, strReteailInputPriceID);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataExp(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchDataExp(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }

        /// <summary>
        /// Nạp danh sách Bảng khai báo các trạng thái của một thông tin sản phẩm 
        /// PR_PRODUCT_SPEC_STATUS_SRH
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="tblResult"></param>
        /// <param name="strProductID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetProductSpecForMobile(String strAuthenData, String strGUID, ref DataTable tblResult, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            object[] objKeywords = new object[] { "@PRODUCTID", strProductID };
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchDataExp(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "DATAPRODUCTSPEC";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataExpEdit(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchDataExpEdit(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;

        }


        /// <summary>
        /// Hàm load chi tiết thông tin định giá sản phẩm cho MOBILE
        /// Tương ứng với hàm SearchDataExpEdit ở DUI
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="tblResult"></param>
        /// <param name="strProductID"></param>
        /// <param name="strRetailInputPriceID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetRTInputPrice_ProductSpecEdit(String strAuthenData, String strGUID, ref DataTable tblResult, string strProductID, string strRetailInputPriceID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().GetRTInputPrice_ProductSpecEdit(ref tblResult, strProductID, strRetailInputPriceID);
            if (tblResult != null)
                tblResult.TableName = "RetailInputPrice_ProductSpec";
            return objResultMessage;

        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSpecStatus(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchDataSpecStatus(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetProductSpecStatusForMobile(String strAuthenData, String strGUID, ref DataTable tblResult, string strProductID, int intMainGroupID, int intProductSpecID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            object[] objKeywords = new object[] { "@PRODUCTID", strProductID, "@MAINGROUPID", intMainGroupID };
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchDataSpecStatus(ref tblResult, objKeywords);
            if (tblResult != null)
            {
                tblResult.TableName = "ProductSpecStatus";
                if (intProductSpecID > 0)
                    tblResult = Library.WebCore.DataTableClass.Select(tblResult, string.Format("PRODUCTSPECID={0}", intProductSpecID));
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataValue(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchDataValue(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetProductSpectStatusValueForMobile(String strAuthenData, String strGUID, ref DataTable tblResult, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            object[] objKeywords = new object[] { "@PRODUCTID", strProductID };
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchDataValue(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "ProductSpectStatusValue";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataReManager(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchDataReManager(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataForMobile(String strAuthenData, String strGUID, ref DataTable tblResult
            , string strFromDate, string strToDate, int intMainGroupID, int intSubGroupID, int intCompanyID, int intStoreID, string strIMEI,
            string strProductID, string strSetPriceUser, int intIsReviewed, int intIsInput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DateTime dtmFromDate = DateTime.MinValue;
            DateTime dtmToDate = DateTime.MinValue;
            if (strFromDate.Trim() != string.Empty)
                DateTime.TryParseExact(strFromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtmFromDate);
            if (strToDate.Trim() != string.Empty)
                DateTime.TryParseExact(strToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtmToDate);
            object[] objKeywords = new object[] { "@FromDate", dtmFromDate,
                                                      "@ToDate", dtmToDate,
                                                      "@MainGroupID", intMainGroupID,
                                                      "@SubGroupID", intSubGroupID,
                                                      "@CompanyID", intCompanyID,
                                                      "@StoreID", intStoreID,
                                                      "@IMEI", strIMEI, 
                                                      "@ProductID", strProductID, 
                                                      "@SetPriceUser", strSetPriceUser, 
                                                      "@IsReviewed", intIsReviewed, 
                                                      "@IsInput", intIsInput};
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().SearchDataReManager(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckWarrantyMonth(String strAuthenData, String strGUID, ref DataTable tblResult, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().CheckWarrantyMonth(ref tblResult, strProductID);
            if (tblResult != null)
                tblResult.TableName = "WarrantyMonthList";
            return objResultMessage;
        }

        /// <summary>
        /// Kiem tra IMEI da thu mua lan nao chua
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="bolIsInput"></param>
        /// <param name="strIMEI"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckIsInput(String strAuthenData, String strGUID, ref bool bolIsInput, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_RetailInputPrice().CheckIsInput(ref bolIsInput, strIMEI);
            return objResultMessage;
        }
    }
}
