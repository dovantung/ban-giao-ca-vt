﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.SalesAndServices.Price
{
    /// <summary>
    /// Summary description for WSPriceIMEITemp
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPriceIMEITemp : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.Price.BO.PriceIMEITEMP objPriceIMEITEMP, string strProductID, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP().LoadInfo(ref objPriceIMEITEMP, strProductID, strIMEI);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP objDA_PriceIMEITEMP = new ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP();
            objResultMessage = objDA_PriceIMEITEMP.SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PriceIMEITEMP";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, DataTable dtbPriceIMEITEMP)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP objDA_PriceIMEITEMP = new ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP();
            objDA_PriceIMEITEMP.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PriceIMEITEMP.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PriceIMEITEMP.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PriceIMEITEMP.Update(dtbPriceIMEITEMP, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetChangePriceArea(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP objDA_PriceIMEITEMP = new ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP();
            objResultMessage = objDA_PriceIMEITEMP.GetChangePriceArea(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PriceIMEITEMP";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SetSyncPrice(String strAuthenData, String strGUID, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP objDA_PriceIMEITEMP = new ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP();
            objResultMessage = objDA_PriceIMEITEMP.SetSyncPrice(objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SetSyncRW(String strAuthenData, String strGUID, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP objDA_PriceIMEITEMP = new ERP.SalesAndServices.Price.DA.DA_PriceIMEITEMP();
            objResultMessage = objDA_PriceIMEITEMP.SetSyncRW(objKeywords);
            return objResultMessage;
        }

    }
}
