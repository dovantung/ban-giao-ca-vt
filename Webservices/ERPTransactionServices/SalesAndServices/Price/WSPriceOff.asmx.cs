﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Library.WebCore;

namespace ERPTransactionServices.SalesAndServices.Price
{
    /// <summary>
    /// Summary description for WSPriceOff
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPriceOff : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertList(String strAuthenData, String strGUID, List<ERP.SalesAndServices.Price.BO.Price_Off> lstPriceOff)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_Price_Off objDA_Price_Off = new ERP.SalesAndServices.Price.DA.DA_Price_Off();
            objDA_Price_Off.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Price_Off.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Price_Off.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Price_Off.InsertData(lstPriceOff);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_Price_Off().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "Price_Off_Data";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataPriceOff_SearchPrice(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_Price_Off().GetDataPriceOff_SearchPrice(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "Price_Data";
            return objResultMessage;
        }
    }
}
