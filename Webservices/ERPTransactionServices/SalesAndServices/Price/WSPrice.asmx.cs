﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.SalesAndServices.Price
{
    /// <summary>
    /// Summary description for WSPrice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPrice : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.Price.BO.PriceBO objPriceBO, int intOutputTypeID, int intPriceAreaID, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_Price().LoadInfo(ref objPriceBO, intOutputTypeID, intPriceAreaID, strProductID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, DataTable dtbPrice, bool bolUpdatePrice, bool bolUpdateRewardPoint, bool bolUpdateAll)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_Price objDA_Price = new ERP.SalesAndServices.Price.DA.DA_Price();
            objDA_Price.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Price.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Price.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Price.Update(dtbPrice, bolUpdatePrice, bolUpdateRewardPoint, bolUpdateAll, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchPriceByProduct(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_Price objDA_Price = new ERP.SalesAndServices.Price.DA.DA_Price();
            objResultMessage = objDA_Price.SearchPriceByProduct(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PriceProduct";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage AutoCalAverageCostPrice(String strAuthenData, String strGUID, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_Price objDA_Price = new ERP.SalesAndServices.Price.DA.DA_Price();
            objDA_Price.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Price.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Price.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Price.AutoCalAverageCostPrice(objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public bool CheckAutoCalAverageCostPrice(String strAuthenData, String strGUID, ref Library.WebCore.ResultMessage objResultMessage, DateTime dtmMonth)
        {
            objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return false;
            ERP.SalesAndServices.Price.DA.DA_Price objDA_Price = new ERP.SalesAndServices.Price.DA.DA_Price();
            bool bolResult = objDA_Price.CheckAutoCalAverageCostPrice(ref objResultMessage, dtmMonth);
            return bolResult;
        }

        [WebMethod(EnableSession = true)]
        public bool CheckACPPreMonth(String strAuthenData, String strGUID, ref Library.WebCore.ResultMessage objResultMessage, object[] objKeywords)
        {
            objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return false;
            ERP.SalesAndServices.Price.DA.DA_Price objDA_Price = new ERP.SalesAndServices.Price.DA.DA_Price();
            bool bolResult = objDA_Price.CheckACPPreMonth(ref objResultMessage, objKeywords);
            return bolResult;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchAverageCostPrice(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_Price objDA_Price = new ERP.SalesAndServices.Price.DA.DA_Price();
            objResultMessage = objDA_Price.SearchAverageCostPrice(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PriceProduct";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateDailyProduct(String strAuthenData, String strGUID, DateTime dtFromDate, DateTime dtToDate, string strMainGroupIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_DailyPrice objDA_DailyPrice = new ERP.SalesAndServices.Price.DA.DA_DailyPrice();
            objResultMessage = objDA_DailyPrice.UpdateProduct(dtFromDate, dtToDate, strMainGroupIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateDailyIMEI(String strAuthenData, String strGUID, DateTime dtFromDate, DateTime dtToDate, string strMainGroupIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_DailyPrice objDA_DailyPrice = new ERP.SalesAndServices.Price.DA.DA_DailyPrice();
            objResultMessage = objDA_DailyPrice.UpdateIMEI(dtFromDate, dtToDate, strMainGroupIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPrice(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_Price objDA_Price = new ERP.SalesAndServices.Price.DA.DA_Price();
            objResultMessage = objDA_Price.GetPrice(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PriceProduct";
            return objResultMessage;
        }
    }
}
