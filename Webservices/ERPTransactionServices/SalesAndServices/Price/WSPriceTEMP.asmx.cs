﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.SalesAndServices.Price
{
    /// <summary>
    /// Summary description for WSPriceTEMP
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPriceTEMP : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.Price.BO.PriceTEMP objPriceTEMP, int intOutputTypeID, int intPriceAreaID, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP().LoadInfo(ref objPriceTEMP, intOutputTypeID, intPriceAreaID, strProductID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, DataTable dtbPriceTEMP)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceTEMP objDA_PriceTEMP = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP();
            objDA_PriceTEMP.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PriceTEMP.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PriceTEMP.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PriceTEMP.Update(dtbPriceTEMP, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateOutputType(String strAuthenData, String strGUID, DataTable dtbPriceTEMP)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceTEMP objDA_PriceTEMP = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP();
            objDA_PriceTEMP.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PriceTEMP.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PriceTEMP.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PriceTEMP.UpdateOutputType(dtbPriceTEMP, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceTEMP objDA_PriceTEMP = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP();
            objResultMessage = objDA_PriceTEMP.SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PriceTEMP";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataForOutputType(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceTEMP objDA_PriceTEMP = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP();
            objResultMessage = objDA_PriceTEMP.SearchDataForOutputType(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PriceTEMP";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataPriceArea(String strAuthenData, String strGUID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceTEMP objDA_PriceTEMP = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP();
            objResultMessage = objDA_PriceTEMP.GetDataPriceArea(ref tblResult);
            if (tblResult != null)
                tblResult.TableName = "PriceArea";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetChangePriceArea(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceTEMP objDA_PriceTEMP = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP();
            objResultMessage = objDA_PriceTEMP.GetChangePriceArea(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PriceTEMP";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SetSyncPrice(String strAuthenData, String strGUID, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceTEMP objDA_PriceTEMP = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP();
            objResultMessage = objDA_PriceTEMP.SetSyncPrice(objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SetSyncRW(String strAuthenData, String strGUID, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceTEMP objDA_PriceTEMP = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP();
            objResultMessage = objDA_PriceTEMP.SetSyncRW(objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, DataTable dtbPriceTEMP)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Price.DA.DA_PriceTEMP objDA_PriceTEMP = new ERP.SalesAndServices.Price.DA.DA_PriceTEMP();
            objResultMessage = objDA_PriceTEMP.Delete_V1(dtbPriceTEMP, objToken.UserName);
            return objResultMessage;
        }

    }
}
