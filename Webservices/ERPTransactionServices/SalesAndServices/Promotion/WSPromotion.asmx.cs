﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using ERP.SalesAndServices.Promotion.BO;
using ERP.SalesAndServices.Promotion.DA;

namespace ERPTransactionServices.SalesAndServices.Promotion
{
    /// <summary>
    /// Summary description for WSPromotion
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPromotion : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert_Update_Promotion(String strAuthenData, String strGUID, ERP.SalesAndServices.Promotion.BO.Promotion objPromotion)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_Promotion objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_Promotion();
            objDA_Promotion.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Promotion.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Promotion.objLogObject.LoginLogID = objToken.LoginLogID;
            objPromotion.CreatedUser = objToken.UserName;
            objPromotion.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_Promotion.Insert_Update_Promotion(objPromotion);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intPromotionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.BO.Promotion objPromotion = new ERP.SalesAndServices.Promotion.BO.Promotion();
            ERP.SalesAndServices.Promotion.DA.DA_Promotion objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_Promotion();
            objDA_Promotion.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Promotion.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Promotion.objLogObject.LoginLogID = objToken.LoginLogID;
            objPromotion.PromotionID = intPromotionID;
            objPromotion.DeletedUser = objToken.UserName;
            objResultMessage = objDA_Promotion.Delete(objPromotion);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearAreaCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.Promotion.BO.Promotion objPromotion, int intPromotionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadInfo(ref objPromotion, intPromotionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PromotionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadAllApplyCondition(String strAuthenData, String strGUID, ref DataSet dsResult, int intPromotionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            dsResult = new DataSet();
            DA_Promotion objDA_Promotion = new DA_Promotion();
            objResultMessage = objDA_Promotion.LoadAllApplyCondition(ref dsResult, intPromotionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListPromotionOffer(String strAuthenData, String strGUID, ref List<PromotionOffer> ListPromotionOffer, int PromotionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_PromotionOffer().LoadListPromotionOffer(ref ListPromotionOffer, PromotionID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListPromotionByProduct(String strAuthenData, String strGUID, ref DataTable tblResult, string strProductID, int intStoreID, int intOutputTypeID, bool bolIsNew, bool bolIsShowProduct, decimal decPrice, int intVAT, int intVATPercent, String strMSPromotionLevelIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadListPromotionByProduct(ref tblResult, strProductID, intStoreID, intOutputTypeID, bolIsNew, bolIsShowProduct, decPrice, intVAT, intVATPercent, strMSPromotionLevelIDList);
            if (tblResult != null)
                tblResult.TableName = "PromotionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListPromotionByPreOrder(String strAuthenData, String strGUID, ref DataTable tblResult, string strProductID, int intStoreID, int intOutputTypeID, int intPreOrderID, bool bolIsCheckInStock)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadListPromotionByPreOrder(ref tblResult, strProductID, intStoreID, intOutputTypeID, intPreOrderID, bolIsCheckInStock);
            if (tblResult != null)
            {
                tblResult.TableName = "PromotionList";
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListPromotionByOrder(String strAuthenData, String strGUID, ref DataTable tblResult, int intStoreID, int intOutputTypeID, String strMSPromotionLevelIDList, int intSaleProgram = 0)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadListPromotionByOrder(ref tblResult, intStoreID, intOutputTypeID, strMSPromotionLevelIDList, intSaleProgram);
            if (tblResult != null)
                tblResult.TableName = "PromotionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListAdditionPromotion(String strAuthenData, String strGUID, ref DataTable tblResult, String strSaleOrderID, String strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadListAdditionPromotion(ref tblResult, strSaleOrderID, strIMEI, objToken.LoginStoreID);
            if (tblResult != null)
                tblResult.TableName = "PromotionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListAdditionPromotionCRM(String strAuthenData, String strGUID, ref DataTable tblResult, String strSaleOrderID, String strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadListAdditionPromotionCRM(ref tblResult, strSaleOrderID, strIMEI, objToken.LoginStoreID);
            if (tblResult != null)
                tblResult.TableName = "PromotionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListPromotionByMainGroup(String strAuthenData, String strGUID, ref DataTable tblResult, int intStoreID, int intOutputTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadListPromotionByMainGroup(ref tblResult, intStoreID, intOutputTypeID);
            if (tblResult != null)
                tblResult.TableName = "PromotionList";
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListProductPromotionInfo(String strAuthenData, String strGUID, ref DataTable tblResult, String strProductID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadListProductPromotionInfo(ref tblResult, strProductID, intStoreID);
            if (tblResult != null)
                tblResult.TableName = "PromotionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckApplyProductCombo(String strAuthenData, String strGUID, ref bool bolResult,
            String strProductComboID, int intOutputStoreID, int intOutputTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().CheckApplyProductCombo(ref bolResult, strProductComboID, intOutputStoreID, intOutputTypeID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật thông tin khuyến mãi hiện tại")]
        public Library.WebCore.ResultMessage Update_CurrentPromotionInfo(String strAuthenData, String strGUID, string strMainGroupIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_Promotion objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_Promotion();
            objDA_Promotion.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Promotion.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Promotion.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Promotion.Update_CurrentPromotionInfo(strMainGroupIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchValidPromotion(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().SearchValidPromotion(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "ValidPromotionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật thứ tự hiển thị chương trình khuyến mãi")]
        public Library.WebCore.ResultMessage UpdateValidPromotionOrder(String strAuthenData, String strGUID, DataTable dtbData)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_Promotion objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_Promotion();
            objResultMessage = objDA_Promotion.UpdateValidPromotionOrder(dtbData, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadApplyProduct(String strAuthenData, String strGUID, ref DataTable dtResult, int intPromotionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            dtResult = new DataTable();
            DA_Promotion objDA_Promotion = new DA_Promotion();
            objResultMessage = objDA_Promotion.LoadApplyProduct(ref dtResult, intPromotionID);
            if (dtResult != null)
                dtResult.TableName = "ProductApply";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetExcludePromotionProgram(String strAuthenData, String strGUID, ref DataTable tblResult, String strPromotionIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().GetExcludePromotionProgram(ref tblResult, strPromotionIDList);
            if (tblResult != null)
                tblResult.TableName = "ExcludePromotion";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadApplyIMEI(String strAuthenData, String strGUID, ref List<Promotion_IMEI> lstPromotion_IMEI, int intPromotionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_Promotion objDA_Promotion = new DA_Promotion();
            objResultMessage = objDA_Promotion.LoadApplyIMEI(ref lstPromotion_IMEI, intPromotionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckApplyIMEI(String strAuthenData, String strGUID, ref Promotion_IMEI objPromotion_IMEI, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_Promotion objDA_Promotion = new DA_Promotion();
            objResultMessage = objDA_Promotion.CheckApplyIMEI(ref objPromotion_IMEI, strIMEI);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadPrivilege(String strAuthenData, String strGUID, ref DataTable dtResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            dtResult = new DataTable();
            DA_Promotion objDA_Promotion = new DA_Promotion();
            objResultMessage = objDA_Promotion.LoadPrivilege(ref dtResult);
            if (dtResult != null)
                dtResult.TableName = "ProductApply";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListPromotionByIMEI(String strAuthenData, String strGUID, ref DataTable tblResult, string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, bool bolIsNew, bool bolIsShowProduct, decimal decPrice, int intVAT, int intVATPercent, String strMSPromotionLevelIDList, bool bolIsOnlyIMEI = false, int intSaleProgram = 0, string strPartnerCode = "")
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadListPromotionByIMEI(ref tblResult, strProductID, strIMEI, intStoreID, intOutputTypeID, bolIsNew, bolIsShowProduct, decPrice, intVAT, intVATPercent, strMSPromotionLevelIDList, bolIsOnlyIMEI, intSaleProgram, strPartnerCode);
            if (tblResult != null)
                tblResult.TableName = "PromotionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListPromotion_InputChangeOrder(String strAuthenData, String strGUID, ref DataTable tblResult, string strIMEI, int intStoreID, int intOutputTypeID, string strOutputVoucherDetailID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().LoadListPromotion_InputChangeOrder(ref tblResult, strIMEI, intStoreID, intOutputTypeID, strOutputVoucherDetailID);
            if (tblResult != null)
                tblResult.TableName = "PromotionList";
            return objResultMessage;
        }

        //Đăng kiểm tra thông tin mã chương trình
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckProgramCode(String strAuthenData, String strGUID, ref PromotionOffer_Coupon objPromotionOffer_Coupon, string strProgramCode, DateTime dtpFromDate, DateTime dtpToDate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PromotionOffer objDA_PromotionOffer = new DA_PromotionOffer();
            objResultMessage = objDA_PromotionOffer.CheckProgramCode(ref objPromotionOffer_Coupon, strProgramCode, dtpFromDate, dtpToDate);
            return objResultMessage;
        }

        //Đăng Kiểm tra trương trình coupon có yêu cầu nhập CMND không?
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckConditionCoupon(String strAuthenData, String strGUID, ref bool bolResult, int intPromotionOfferID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PromotionOffer objDA_PromotionOffer = new DA_PromotionOffer();
            objResultMessage = objDA_PromotionOffer.CheckConditionCoupon(ref bolResult, intPromotionOfferID);
            return objResultMessage;
        }

        //21/09/2017: Đăng bổ sung lấy thông tin chuong trình trả góp
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadApplySaleProgram(String strAuthenData, String strGUID, ref List<Promotion_SaleProgram> lstPromotion_SaleProgram, int intPromotionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_Promotion_SaleProgram objDA_Promotion_SaleProgram = new DA_Promotion_SaleProgram();
            objResultMessage = objDA_Promotion_SaleProgram.LoadInfo(ref lstPromotion_SaleProgram, intPromotionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPromotionExcludeCoupon(String strAuthenData, String strGUID, ref DataTable tblResult, String strPromotionIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().GetPromotionExcludeCoupon(ref tblResult, strPromotionIDList);
            if (tblResult != null)
                tblResult.TableName = "PromotionExcludeCoupon";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckPromotionExcludeCoupon(String strAuthenData, String strGUID, String strPromotionIDList, String strSaleOrderID, decimal decCouponIssueID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Promotion().CheckPromotionExcludeCoupon(strPromotionIDList, strSaleOrderID, decCouponIssueID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckPromotionOfferDeposit(String strAuthenData, String strGUID, ref string strResult, string lstPromotionOfferID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PromotionOffer objDA_PromotionOffer = new DA_PromotionOffer();
            objResultMessage = objDA_PromotionOffer.CheckPromotionOfferDeposit(ref strResult, lstPromotionOfferID);
            return objResultMessage;
            
        }

        //[WebMethod(EnableSession = true)]
        //public Library.WebCore.ResultMessage LoadListPromotionByPreOrder(String strAuthenData, String strGUID, ref DataTable tblResult, string strProductID, int intStoreID, int intOutputTypeID, int intPreOrderID, bool bolIsCheckInStock)
        //{
        //    Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
        //    Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
        //    if (objToken == null)
        //        return objResultMessage;
        //    objResultMessage = new DA_Promotion().LoadListPromotionByPreOrder(ref tblResult, strProductID, intStoreID, intOutputTypeID, intPreOrderID, bolIsCheckInStock);
        //    if (tblResult != null)
        //        tblResult.TableName = "PromotionList";
        //    return objResultMessage;
        //}


        #region old

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateCodeConfirm(String strAuthenData, String strGUID, ERP.SalesAndServices.Promotion.BO.Promotion objPromotion, int levelConfirm)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_Promotion objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_Promotion();
            objResultMessage = objDA_Promotion.Update_CodeConfirm(objPromotion, levelConfirm);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetLevelConfirmPromotion(String strAuthenData, String strGUID, ref List<PromotionConfirm> ListPromotionOffer, int PromotionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_Promotion objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_Promotion();
            objResultMessage = objDA_Promotion.GetLevelConfirmPromotion(ref ListPromotionOffer, PromotionID);
            return objResultMessage;
        }

        #endregion

        #region new
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchPromotionManagerLevel(String strAuthenData, String strGUID, ref DataTable dtbResult,string keyword)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel();
            objResultMessage = objDA_Promotion.SearchData(ref dtbResult, keyword);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchPromotionLevel(String strAuthenData, String strGUID, ref List<PromotionConfirmManager> ListPromotionLevel,int promotiontypeid,int promotionmanagerlevelid)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel();
            objResultMessage = objDA_Promotion.SearchLevel(ref ListPromotionLevel, promotiontypeid, promotionmanagerlevelid);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertPromotionLevel(String strAuthenData, String strGUID, PromotionLevel objInput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel();
            if(objInput.PromotionManagerID > 0)
                objResultMessage = objDA_Promotion.UpdatePromotionManagerLevel(objInput);
            else
                objResultMessage = objDA_Promotion.AddPromotionManagerLevel(objInput);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdatePromotionLevel(String strAuthenData, String strGUID, PromotionLevel objInput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel();
            objResultMessage = objDA_Promotion.UpdatePromotionManagerLevel(objInput);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchLevelFollowPromotionType(String strAuthenData, String strGUID, ref PromotionLevelFollowPromotionType objPromotionLevels, int promotiontypeid)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel objDA_Promotion = new ERP.SalesAndServices.Promotion.DA.DA_PromotionLevel();
            objResultMessage = objDA_Promotion.SearchLevelFollowPromotionType(ref objPromotionLevels, promotiontypeid);
            return objResultMessage;
        }


        #endregion

    }
}
