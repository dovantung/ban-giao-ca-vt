﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using ERP.SalesAndServices.SaleOrders.BO;
using ERP.SalesAndServices.SaleOrders.DA;

namespace ERPTransactionServices.SalesAndServices.SaleProgram
{
    /// <summary>
    /// Summary description for WSSaleProgram
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSSaleProgram : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.SaleProgram objSaleProgram)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.DA.DA_SaleProgram objDA_SaleProgram = new ERP.SalesAndServices.SaleOrders.DA.DA_SaleProgram();
            objDA_SaleProgram.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleProgram.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleProgram.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleProgram.CreatedUser = objToken.UserName;
            objResultMessage = objDA_SaleProgram.Insert(objSaleProgram);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.c();
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.SaleProgram objSaleProgram)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.DA.DA_SaleProgram objDA_SaleProgram = new ERP.SalesAndServices.SaleOrders.DA.DA_SaleProgram();
            objDA_SaleProgram.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleProgram.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleProgram.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleProgram.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_SaleProgram.Update(objSaleProgram);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearAreaCache();
            return objResultMessage;
        }
          [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Review(String strAuthenData, String strGUID, ERP.SalesAndServices.SaleOrders.BO.SaleProgram objSaleProgram)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.DA.DA_SaleProgram objDA_SaleProgram = new ERP.SalesAndServices.SaleOrders.DA.DA_SaleProgram();
            objDA_SaleProgram.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleProgram.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleProgram.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleProgram.ReviewedUser = objToken.UserName;
            objResultMessage = objDA_SaleProgram.Review(objSaleProgram);
            return objResultMessage;
        }
        
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intSaleProgramID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.SaleOrders.BO.SaleProgram objSaleProgram = new ERP.SalesAndServices.SaleOrders.BO.SaleProgram();
            ERP.SalesAndServices.SaleOrders.DA.DA_SaleProgram objDA_SaleProgram = new ERP.SalesAndServices.SaleOrders.DA.DA_SaleProgram();
            objDA_SaleProgram.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SaleProgram.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SaleProgram.objLogObject.LoginLogID = objToken.LoginLogID;
            objSaleProgram.SaleProgramID = intSaleProgramID;
            objSaleProgram.DeletedUser = objToken.UserName;
            objResultMessage = objDA_SaleProgram.Delete(objSaleProgram);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearAreaCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.SaleOrders.BO.SaleProgram objSaleProgram, int intSaleProgramID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleProgram().LoadInfo(ref objSaleProgram, intSaleProgramID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadFullInfo(String strAuthenData, String strGUID, ref ERP.SalesAndServices.SaleOrders.BO.SaleProgram objSaleProgram, int intSaleProgramID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleProgram().LoadFullInfo(ref objSaleProgram, intSaleProgramID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleProgram().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "SaleProgramList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertProduct(String strAuthenData, String strGUID, DataTable dtbSaleProgram_Product)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleProgram().InsertProduct(dtbSaleProgram_Product);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadProgramFile(String strAuthenData, String strGUID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleProgram().SearchData(ref tblResult);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDataSaleOrderType(String strAuthenData, String strGUID, ref List<ERP.SalesAndServices.SaleOrders.BO.SaleProgram_SaleOrderType> lstSaleProgram_SaleOrderType, int intSaleProgramID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleProgram_SaleOrderType().LoadInfo(ref lstSaleProgram_SaleOrderType, intSaleProgramID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSaleProgramType(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SaleProgram_SaleOrderType().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "SaleProgramDTB";
            return objResultMessage;
        }
    }
}
