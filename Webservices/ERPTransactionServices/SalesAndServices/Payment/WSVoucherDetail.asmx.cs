﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.SalesAndServices.Payment.DA;
using ERP.SalesAndServices.Payment.BO;
using System.Data;
namespace ERPTransactionServices.SalesAndServices.Payment
{
    /// <summary>
    /// Summary description for WSVoucherDetail
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSVoucherDetail : System.Web.Services.WebService
    {
      
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref VoucherDetail objVoucherDetail, string strVoucherDetailID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_VoucherDetail().LoadInfo(ref objVoucherDetail, strVoucherDetailID);
            return objResultMessage;
        }
      
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.VoucherDetail objVoucherDetail)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_VoucherDetail objDA_VoucherDetail = new ERP.SalesAndServices.Payment.DA.DA_VoucherDetail();
            objVoucherDetail.CreatedUser = objToken.UserName;
            objResultMessage = objDA_VoucherDetail.Insert(objVoucherDetail);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearTransportTypeCache();
            return objResultMessage;   
        }



        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, string strKeyWord)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Payment.DA.DA_Voucher().SearchData(ref tblResult, new object[] { "@KeyWord", strKeyWord});
            if (tblResult != null)
                tblResult.TableName = "VoucherDetailList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.VoucherDetail objVoucherDetail)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_VoucherDetail objDA_VoucherDetail = new ERP.SalesAndServices.Payment.DA.DA_VoucherDetail();
            objVoucherDetail.UpdatedUser = objToken.UserName;
            objDA_VoucherDetail.objLogObject.CertificateString = objToken.CertificateString;
            objDA_VoucherDetail.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_VoucherDetail.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_VoucherDetail.Update(objVoucherDetail);
            return objResultMessage;
        }
    }
}
