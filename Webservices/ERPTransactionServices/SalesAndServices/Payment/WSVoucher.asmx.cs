﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using ERP.SalesAndServices.Payment.DA;
using ERP.SalesAndServices.Payment.BO;
namespace ERPTransactionServices.SalesAndServices.Payment
{
    /// <summary>
    /// Summary description for WSVoucher
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSVoucher : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateVoucherID(String strAuthenData, String strGUID, ref string strVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_Voucher objDA_Voucher = new DA_Voucher();
            objResultMessage = objDA_Voucher.CreateVoucherID(ref strVoucherID, objToken.LoginStoreID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref Voucher objVoucher, string strVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Voucher().LoadInfo(ref objVoucher, strVoucherID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCurrentInvoice(String strAuthenData, String strGUID, ref string strInvoiceID, ref string strInvoiceSymbol)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_Voucher objDA_Voucher = new DA_Voucher();
            objResultMessage = objDA_Voucher.GetCurrentInvoice(ref strInvoiceID, ref strInvoiceSymbol, objToken.LoginStoreID, objToken.UserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.Voucher objVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new ERP.SalesAndServices.Payment.DA.DA_Voucher();
            objDA_Voucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Voucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Voucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objVoucher.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Voucher.Insert(objVoucher);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateVoucher(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.Voucher objVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new ERP.SalesAndServices.Payment.DA.DA_Voucher();
            objVoucher.CreatedUser = objToken.UserName;
            objDA_Voucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Voucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Voucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Voucher.CreateVoucher(objVoucher);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tạo thu chi, trả về mã", MessageName = "Tra_ve_ma_phieu")]
        public Library.WebCore.ResultMessage CreateVoucher(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.Voucher objVoucher, ref string strVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new ERP.SalesAndServices.Payment.DA.DA_Voucher();
            objVoucher.CreatedUser = objToken.UserName;
            objDA_Voucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Voucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Voucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Voucher.CreateVoucher(objVoucher);
            strVoucherID = objVoucher.VoucherID;
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateVoucher(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.Voucher objVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new ERP.SalesAndServices.Payment.DA.DA_Voucher();
            objVoucher.CreatedUser = objToken.UserName;
            objDA_Voucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Voucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Voucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Voucher.UpdateVoucher(objVoucher);
            return objResultMessage;
        }



        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, string strKeyWord, int intVoucherTypeID, string strUserName, DateTime dtFromDate, DateTime dtToDate, int intSearchType, string strStoreIDList, int intIsSearchAll, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Payment.DA.DA_Voucher().SearchData(ref tblResult, new object[] { "@KeyWord", strKeyWord, "@VoucherTypeID", intVoucherTypeID, "@UserName", strUserName, "@FromDate", dtFromDate, "@ToDate", dtToDate, "@SearchType", intSearchType, "@StoreIDList", strStoreIDList, "@IsSearchAll", intIsSearchAll, "StoreID", intStoreID });
            if (tblResult != null)
                tblResult.TableName = "VoucherList";
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataNew(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Payment.DA.DA_Voucher().SearchDataNew(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "VoucherListNew";
            return objResultMessage;
        }


        // Trieu them 01/11/12
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateVoucherInfo(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.Voucher objVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new ERP.SalesAndServices.Payment.DA.DA_Voucher();
            objVoucher.CreatedUser = objToken.UserName;

            // them ngay 08/01/2013
            objDA_Voucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Voucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Voucher.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_Voucher.UpdateVoucherInfo(objVoucher);


            return objResultMessage;
        }



        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage AddVoucherDetail(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.Voucher objVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new ERP.SalesAndServices.Payment.DA.DA_Voucher();
            objVoucher.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Voucher.AddVoucherDetail(objVoucher);


            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoVoucherDT_AdvanceRequest(String strAuthenData, String strGUID, ref List<VoucherDetail_AdvanceRequestDetail> lstVoucherDetail_AdvanceRequestDetail, string strVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Voucher().LoadInfoVoucherDT_AdvanceRequest(ref lstVoucherDetail_AdvanceRequestDetail, strVoucherID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Tạo chi cấp ứng", MessageName = "Ap_Dung_Cap_Ung")]
        public Library.WebCore.ResultMessage CreateVoucherByAdvance(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.Voucher objVoucher,List<ERP.Account.BO.Transaction.ACC_Transaction> lstTransaction)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new ERP.SalesAndServices.Payment.DA.DA_Voucher();
            objVoucher.CreatedUser = objToken.UserName;
            objDA_Voucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Voucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Voucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Voucher.CreateVoucherByAdvance(objVoucher, lstTransaction);
            return objResultMessage;
        }
    }
}
