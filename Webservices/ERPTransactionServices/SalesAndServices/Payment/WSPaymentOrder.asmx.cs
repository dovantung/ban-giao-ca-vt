﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using ERP.SalesAndServices.Payment.DA;
using ERP.SalesAndServices.Payment.BO;
namespace ERPTransactionServices.SalesAndServices.Payment
{
    /// <summary>
    /// Summary description for WSPaymentOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPaymentOrder : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreatePaymentOrderID(String strAuthenData, String strGUID, ref string strPaymentOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_PaymentOrder objDA_PaymentOrder = new DA_PaymentOrder();
            objResultMessage = objDA_PaymentOrder.CreatePaymentOrderID(ref strPaymentOrderID, objToken.LoginStoreID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref PaymentOrder objPaymentOrder, string strPaymentOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_PaymentOrder().LoadInfoAll(ref objPaymentOrder, strPaymentOrderID);
            return objResultMessage;
        }
     

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreatePaymentOrder(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.PaymentOrder objPaymentOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_PaymentOrder objDA_PaymentOrder = new ERP.SalesAndServices.Payment.DA.DA_PaymentOrder();
            objPaymentOrder.CreatedUser = objToken.UserName;
            objDA_PaymentOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PaymentOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PaymentOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PaymentOrder.CreatePaymentOrder(objPaymentOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewPaymentOrder(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.PaymentOrder_ReviewList objPaymentOrder_ReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_PaymentOrder_ReviewList objDA_PaymentOrder_ReviewList = new DA_PaymentOrder_ReviewList();
            objDA_PaymentOrder_ReviewList.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PaymentOrder_ReviewList.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PaymentOrder_ReviewList.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PaymentOrder_ReviewList.Update(objPaymentOrder_ReviewList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateWorkFlowPaymentOrder(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.PaymentOrder_WorkFlow objPaymentOrder_WorkFlow)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_PaymentOrder_WorkFlow objDA_PaymentOrder_WorkFlow = new DA_PaymentOrder_WorkFlow();
            //objDA_PaymentOrder.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_PaymentOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_PaymentOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PaymentOrder_WorkFlow.Update(objPaymentOrder_WorkFlow);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdatePaymentOrder(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.PaymentOrder objPaymentOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_PaymentOrder objDA_PaymentOrder = new ERP.SalesAndServices.Payment.DA.DA_PaymentOrder();
            objPaymentOrder.UpdatedUser = objToken.UserName;
            objDA_PaymentOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PaymentOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PaymentOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PaymentOrder.UpdatePaymentOrder(objPaymentOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.SalesAndServices.Payment.BO.PaymentOrder objPaymentOrder,ref string strResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.SalesAndServices.Payment.DA.DA_PaymentOrder objDA_PaymentOrder = new ERP.SalesAndServices.Payment.DA.DA_PaymentOrder();
            objPaymentOrder.DeletedUser = objToken.UserName;
            objDA_PaymentOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PaymentOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PaymentOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PaymentOrder.Delete(objPaymentOrder,ref strResult);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Payment.DA.DA_PaymentOrder().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PaymentOrderList";
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetReivewUserList(String strAuthenData, String strGUID, ref DataTable tblResult, int intPaymentOrderTypeID, string strPaymentOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Payment.DA.DA_PaymentOrder_ReviewList().GetReivewUserList(ref tblResult, intPaymentOrderTypeID,strPaymentOrderID);
            if (tblResult != null)
                tblResult.TableName = "ReivewList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetWorkFlowList(String strAuthenData, String strGUID, ref DataTable tblResult, int intPaymentOrderTypeID, string strPaymentOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Payment.DA.DA_PaymentOrder_WorkFlow().GetWorkFlowList(ref tblResult, intPaymentOrderTypeID, strPaymentOrderID);
            if (tblResult != null)
                tblResult.TableName = "ReivewList";
            return objResultMessage;
        }  
    }
}
