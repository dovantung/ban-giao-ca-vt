﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MisaProductStock_Att = ERP.Inventory.BO.CheckDifferenceStockERPAndMisa.MisaProductStock_Att;
using DA_MisaProductStock_Att = ERP.Inventory.DA.CheckDifferenceStockERPAndMisa.DA_MisaProductStock_Att;
using System.Data;

namespace ERPTransactionServices.CheckDifferenceStockERPAndMisa
{
    /// <summary>
    /// Summary description for WSMisaProductStock_Att
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSMisaProductStock_Att : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref MisaProductStock_Att objMisaProductStock_Att, string strMISAPRODUCTSTOCKID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_MisaProductStock_Att().LoadInfo(ref objMisaProductStock_Att, strMISAPRODUCTSTOCKID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, MisaProductStock_Att objMisaProductStock_Att)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_MisaProductStock_Att objDA_MisaProductStock_Att = new DA_MisaProductStock_Att();
            objMisaProductStock_Att.CREATEDUSER = objToken.UserName;
            objResultMessage = objDA_MisaProductStock_Att.Insert(objMisaProductStock_Att);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_MisaProductStock_Att().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "MisaProductStock_AttList";
            return objResultMessage;
        }

    }
}
