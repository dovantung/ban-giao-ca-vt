﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.RepairWarranty
{
    /// <summary>
    /// Summary description for WSRepairReturnOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRepairReturnOrder : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.WarrantyRepair.BO.RepairWarrantyReturn.RepairReturnOrder objRepairReturnOrder, ref string strRepairReturnOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyReturn.DA_RepairReturnOrder objDA_RepairReturnOrder = new ERP.WarrantyRepair.DA.RepairWarrantyReturn.DA_RepairReturnOrder();
            objRepairReturnOrder.CreatedUser = objToken.UserName;
            objRepairReturnOrder.CreatedStoreID = objToken.LoginStoreID;
            objResultMessage = objDA_RepairReturnOrder.InsertRepairReturnOrder(objRepairReturnOrder, ref strRepairReturnOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyReturn.DA_RepairReturnOrder objDA_RepairReturnOrder = new ERP.WarrantyRepair.DA.RepairWarrantyReturn.DA_RepairReturnOrder();
            objResultMessage = objDA_RepairReturnOrder.SearchData(ref dtbData, objKeywords);
            return objResultMessage;
        }
    }
}
