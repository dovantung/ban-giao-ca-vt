﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.RepairWarranty
{
    /// <summary>
    /// Summary description for WSRepairOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRepairOrder : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder objRepairOrder, string strRepairOrderID/*, bool bolIsShowForReturn, bool bolIsLoadDetail*/)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder().LoadInfo(ref objRepairOrder, strRepairOrderID/*, bolIsShowForReturn, bolIsLoadDetail*/);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder objRepairOrder, bool bolIsAutoReviewed, ref string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            //objDA_RepairOrder.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_RepairOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_RepairOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objRepairOrder.CreatedUser = objToken.UserName;
            objRepairOrder.CreatedStoreID = objToken.LoginStoreID;
            objResultMessage = objDA_RepairOrder.Insert(objRepairOrder, bolIsAutoReviewed, ref strRepairOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder objRepairOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            //objDA_RepairOrder.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_RepairOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_RepairOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objRepairOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RepairOrder.Update(objRepairOrder);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strRepairOrderID, string strContentDelete)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder objRepairOrder = new ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder();
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            //objDA_RepairOrder.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_RepairOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_RepairOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objRepairOrder.RepairOrderID = strRepairOrderID;
            objRepairOrder.DeletedUser = objToken.UserName;
            objRepairOrder.ContentDeleted = strContentDelete;
            objResultMessage = objDA_RepairOrder.Delete(objRepairOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateRepairOrderID(String strAuthenData, String strGUID, ref string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.CreateRepairOrderID(ref strRepairOrderID, objToken.LoginStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchRepairSolution(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_MachineStatus objDA_RepairOrder_MachineStatus = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_MachineStatus();
            objResultMessage = objDA_RepairOrder_MachineStatus.SearchRepairSolution(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.SearchData(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SaveOutputVoucher(String strAuthenData, String strGUID, ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder objRepairOrder, int intComponentOutputTypeID, ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_OutputOrder objRepairOrder_OutputOrder, bool bolIsVirtualOutput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            string strCreateUser = objToken.UserName;
            int intStoreID = objToken.LoginStoreID;
            objResultMessage = objDA_RepairOrder.SaveOutputVoucher(objRepairOrder, intComponentOutputTypeID, objRepairOrder_OutputOrder, strCreateUser, intStoreID, bolIsVirtualOutput);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateIsReview(String strAuthenData, String strGUID, string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            string strReviewedUser = objToken.UserName;
            objResultMessage = objDA_RepairOrder.UpdateIsReview(strRepairOrderID, strReviewedUser);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoVoucher(String strAuthenData, String strGUID, ref ERP.SalesAndServices.Payment.BO.Voucher objVoucher, string strVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Payment.DA.DA_Voucher().LoadInfo(ref objVoucher, strVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateVoucher(String strAuthenData, String strGUID, ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder objRepairOrder, ERP.SalesAndServices.Payment.BO.Voucher objVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.CreateVoucher(objRepairOrder, objVoucher);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetTotalAmount(String strAuthenData, String strGUID, bool bolIsUpdate, string strRepairOrderID, ref decimal decTotalAmount)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.GetTotalAmount(bolIsUpdate, strRepairOrderID, ref decTotalAmount);
            return objResultMessage;
        }

        //[WebMethod(EnableSession = true)]
        //public Library.WebCore.ResultMessage UpdateListAttachment(String strAuthenData, String strGUID, List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_Attachment> lstRepairOrder_Attachment)
        //{
        //    Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
        //    Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
        //    if (objToken == null)
        //        return objResultMessage;
        //    ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Attachment objDA_RepairOrder_Attachment = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Attachment();

        //    objResultMessage = objDA_RepairOrder_Attachment.UpdateListAttachment(lstRepairOrder_Attachment);
        //    return objResultMessage;
        //}

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckStoreInStock(String strAuthenData, String strGUID, ref int intResult, string strProductID, int intStoreID, int intQuantity)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.CheckStoreInStock(ref intResult, strProductID, intStoreID, intQuantity);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateQuantityForReturn(String strAuthenData, String strGUID, DataTable dtbData, string strOutputOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_OutputOrderDT objDA_RepairOrder_OutputOrderDT = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_OutputOrderDT();
            objResultMessage = objDA_RepairOrder_OutputOrderDT.UpdateQuantityForReturn(dtbData, strOutputOrderID, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchIMEIHistory(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.SearchIMEIHistory(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public decimal GetSalePrice(String strAuthenData, String strGUID, int intOutputTypeID, string strProductID, int intStoreID, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return 0;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            return objDA_RepairOrder.GetSalePrice(intOutputTypeID, strProductID, intStoreID, strIMEI);
            
        }

        [WebMethod(EnableSession = true)]
        public decimal GetCostPrice(String strAuthenData, String strGUID, string strProductID, int intStoreID, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return 0;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            return objDA_RepairOrder.GetCostPrice(strProductID, intStoreID, strIMEI);

        }

        [WebMethod(EnableSession = true)]
        public decimal GetFirstPrice(String strAuthenData, String strGUID, string strProductID, int intStoreID, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return 0;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            return objDA_RepairOrder.GetFirstPrice(strProductID, intStoreID, strIMEI);

        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchCustomerByIMEI(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.SearchCustomerByIMEI(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertWorkFlow(String strAuthenData, String strGUID, ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_WorkFlow objRepairOrder_WorkFlow)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_WorkFlow objDA_RepairOrder_WorkFlow = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_WorkFlow();
            objResultMessage = objDA_RepairOrder_WorkFlow.Insert(objRepairOrder_WorkFlow);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateWorkFlow(String strAuthenData, String strGUID, string strWorkFlowID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_WorkFlow objDA_RepairOrder_WorkFlow = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_WorkFlow();
            objResultMessage = objDA_RepairOrder_WorkFlow.UpdateIsReceive(strWorkFlowID, objToken.UserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListMachineAccessoryByMG(String strAuthenData, String strGUID, ref List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_Accessory> lstAccessory, int intMainGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Accessory objDA_RepairOrder_Accessory = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Accessory();
            objResultMessage = objDA_RepairOrder_Accessory.LoadListMachineAccessoryByMG(ref lstAccessory, intMainGroupID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetVoucherType(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.GetVoucherType(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetInputType(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.GetInputType(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertList(String strAuthenData, String strGUID, List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder> lstRepairOrder, bool bolIsAutoReviewed)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.InsertList(lstRepairOrder, bolIsAutoReviewed);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage ReviewList(String strAuthenData, String strGUID, List<string> lstRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            string strUserName = objToken.UserName;
            objResultMessage = objDA_RepairOrder.ReviewList(lstRepairOrderID, strUserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListMachineStatus(String strAuthenData, String strGUID, ref List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_MachineStatus> lstRepairOrder_MachineStatus, string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_MachineStatus objDA_RepairOrder_MachineStatus = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_MachineStatus();
            objResultMessage = objDA_RepairOrder_MachineStatus.LoadListMachineStatus(ref lstRepairOrder_MachineStatus, strRepairOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListError(String strAuthenData, String strGUID, ref List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_Error> lstRepairOrder_Error, string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Error objDA_RepairOrder_Error = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Error();
            objResultMessage = objDA_RepairOrder_Error.LoadListError(ref lstRepairOrder_Error, strRepairOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListRepairUser(String strAuthenData, String strGUID, ref List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_RepairUser> lstRepairOrder_RepairUser, string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_RepairUser objDA_RepairOrder_RepairUser = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_RepairUser();
            objResultMessage = objDA_RepairOrder_RepairUser.LoadListRepairUser(ref lstRepairOrder_RepairUser, strRepairOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListOutputOrder(String strAuthenData, String strGUID, ref List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_OutputOrder> lstRepairOrder_OutputOrder, string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_OutputOrder objDA_RepairOrder_OutputOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_OutputOrder();
            objResultMessage = objDA_RepairOrder_OutputOrder.LoadListOutputOrder(ref lstRepairOrder_OutputOrder, strRepairOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListRepairWorkFlow(String strAuthenData, String strGUID, ref List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_WorkFlow> lstRepairOrder_WorkFlow, string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_WorkFlow objDA_RepairOrder_WorkFlow = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_WorkFlow();
            objResultMessage = objDA_RepairOrder_WorkFlow.LoadListRepairWorkFlow(ref lstRepairOrder_WorkFlow, strRepairOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListRepairAttachment(String strAuthenData, String strGUID, ref List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_Attachment> lstRepairOrder_Attachment, string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Attachment objDA_RepairOrder_Attachment = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Attachment();
            objResultMessage = objDA_RepairOrder_Attachment.LoadListRepairAttachment(ref lstRepairOrder_Attachment, strRepairOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadListMachineAccessory(String strAuthenData, String strGUID, ref List<ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_Accessory> lstRepairOrder_Accessory, string strRepairOrderID, int intMainGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Accessory objDA_RepairOrder_Accessory = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_Accessory();
            objResultMessage = objDA_RepairOrder_Accessory.LoadListMachineAccessory(ref lstRepairOrder_Accessory, strRepairOrderID, intMainGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInventoryForReturn(String strAuthenData, String strGUID, ref DataTable dtbData, string strRepairOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder objDA_RepairOrder = new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder();
            objResultMessage = objDA_RepairOrder.LoadInventoryForReturn(strRepairOrderID, ref dtbData);
            return objResultMessage;
        }
    }
}
