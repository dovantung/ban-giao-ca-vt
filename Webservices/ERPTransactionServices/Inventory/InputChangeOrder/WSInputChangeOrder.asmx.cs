﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using ERP.Inventory.BO.InputChangeOrder;
using ERP.Inventory.DA.InputChangeOrder;

namespace ERPTransactionServices.Inventory.InputChangeOrder
{
    /// <summary>
    /// Summary description for WSInputChangeOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSInputChangeOrder : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true, Description = "Lấy thông tin yêu cầu nhập đổi/trả hàng", MessageName = "LoadInfo")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, string strInputChangeOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().LoadInfo(ref objInputChangeOrder, strInputChangeOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách yêu cầu nhập đổi/trả hàng", MessageName = "SearchData")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy chi tiết phiếu xuất cho yêu cầu nhập đổi/trả hàng", MessageName = "LoadOutputVoucherDetailForReturn")]
        public Library.WebCore.ResultMessage LoadOutputVoucherDetailForReturn(String strAuthenData, String strGUID, String strOutputVoucherID, int intInputChangeOrderTypeID, ref DataTable dtResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().LoadOutputVoucherDetailForReturn(strOutputVoucherID, intInputChangeOrderTypeID, ref dtResult);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách loại nhập đổi/trả hàng", MessageName = "LoadInputChangeOrderType")]
        public Library.WebCore.ResultMessage LoadInputChangeOrderType(String strAuthenData, String strGUID, String strOutputVoucherID, int intApplyErrorType, ref DataTable dtResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().LoadInputChangeOrderType(strOutputVoucherID, intApplyErrorType, objToken.UserName, ref dtResult);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Thêm yêu cầu nhập đổi/trả hàng", MessageName = "Insert")]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, DataTable dtbMachineErrorIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder objDA_InputChangeOrder = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder();
            objDA_InputChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objInputChangeOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_InputChangeOrder.Insert(objInputChangeOrder, dtbMachineErrorIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tạo phiếu nhập, phiếu xuất và phiếu thu(hoặc chi) cho yêu cầu nhập đổi/trả hàng", MessageName = "CreateInputVoucherAndOutputVoucherAndInOutVoucher")]
        public Library.WebCore.ResultMessage CreateInputVoucherAndOutputVoucherAndInOutVoucher(String strAuthenData, String strGUID, ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, ERP.Inventory.BO.InputVoucher objInputVoucherBO, ERP.Inventory.BO.OutputVoucher objOutputVoucherBO, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForReturnFee, ERP.SalesAndServices.Payment.BO.Voucher objInOutVoucher, ref List<string> lstVATInvoiceList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder objDA_InputChangeOrder = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder();
            objDA_InputChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_InputChangeOrder.CreateInputVoucherAndOutputVoucherAndInOutVoucherNew(objToken.UserName, objInputChangeOrder, objInputVoucherBO, objOutputVoucherBO, objInOutVoucher, objVoucherForReturnFee, null, ref lstVATInvoiceList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tạo phiếu nhập và phiếu chi cho yêu cầu nhập đổi/trả hàng", MessageName = "CreateInputVoucherAndOutVoucher")]
        public Library.WebCore.ResultMessage CreateInputVoucherAndOutVoucher(String strAuthenData, String strGUID, ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, ERP.Inventory.BO.InputVoucher objInputVoucherBO, ERP.SalesAndServices.Payment.BO.Voucher objOutVoucher, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForSaleOrder, ref List<string> lstVATInvoiceList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder objDA_InputChangeOrder = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder();
            objDA_InputChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_InputChangeOrder.CreateInputVoucherAndOutVoucher(objToken.UserName, objInputChangeOrder, objInputVoucherBO, objOutVoucher, objVoucherForSaleOrder, ref lstVATInvoiceList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tạo phiếu nhập và phiếu chi cho yêu cầu nhập đổi/trả hàng", MessageName = "CreateInputVoucherAndOutVoucherViettel")]
        public Library.WebCore.ResultMessage CreateInputVoucherAndOutVoucherViettel(String strAuthenData, String strGUID, ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, ERP.Inventory.BO.InputVoucher objInputVoucherBO, ERP.SalesAndServices.Payment.BO.Voucher objOutVoucher, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForReturnFee, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForSaleOrder, ref string strInputVoucherReturnId, ref List<string> lstVATInvoiceList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder objDA_InputChangeOrder = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder();
            objDA_InputChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_InputChangeOrder.CreateInputVoucherAndOutVoucherViettel(objToken.UserName, objInputChangeOrder, objInputVoucherBO, objOutVoucher, objVoucherForReturnFee, objVoucherForSaleOrder, ref strInputVoucherReturnId, ref lstVATInvoiceList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy mức duyệt yêu cầu nhập đổi/trả hàng", MessageName = "GetReviewList")]
        public Library.WebCore.ResultMessage GetReviewList(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetReviewList(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy mức duyệt yêu cầu nhập đổi/trả hàng", MessageName = "GetReviewList_InputChangeOrderType")]
        public Library.WebCore.ResultMessage GetReviewList(String strAuthenData, String strGUID, ref DataTable dtResult, int intInputChangeOrderType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetReviewList(ref dtResult, intInputChangeOrderType);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật duyệt yêu cầu nhập đổi/trả hàng", MessageName = "UpdateReviewList")]
        public Library.WebCore.ResultMessage UpdateReviewList(String strAuthenData, String strGUID, InputChangeOrder_ReviewList objQuitJobRequest_ReviewList, int intCountReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_InputChangeOrder_ReviewList objDA_InputChangeOrder_ReviewList = new DA_InputChangeOrder_ReviewList();
            objQuitJobRequest_ReviewList.UpdatedUser = objToken.UserName;
            objQuitJobRequest_ReviewList.CreatedUser = objToken.UserName;
            objResultMessage = objDA_InputChangeOrder_ReviewList.UpdateReviewList(objQuitJobRequest_ReviewList, intCountReviewList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật bước xử lý yêu cầu nhập đổi/trả hàng", MessageName = "UpdateProcess")]
        public Library.WebCore.ResultMessage UpdateProcess(String strAuthenData, String strGUID, InputChangeOrder_WorkFlow objQuitJobRequest_WorkFlow)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_InputChangeOrder_WorkFlow objDA_InputChangeOrder_WorkFlow = new DA_InputChangeOrder_WorkFlow();
            objResultMessage = objDA_InputChangeOrder_WorkFlow.Update(objQuitJobRequest_WorkFlow);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Thêm bình luận yêu cầu nhập đổi/trả hàng", MessageName = "InsertComment")]
        public Library.WebCore.ResultMessage InsertComment(String strAuthenData, String strGUID, ref string strCommentID, InputChangeOrder_Comment objQuitJobRequest_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_InputChangeOrder_Comment objDA_InputChangeOrder_Comment = new DA_InputChangeOrder_Comment();
            objQuitJobRequest_Comment.CreatedUser = objToken.UserName;
            objResultMessage = objDA_InputChangeOrder_Comment.Insert(ref strCommentID, objQuitJobRequest_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xóa bình luận yêu cầu nhập đổi/trả hàng", MessageName = "DeleteComment")]
        public Library.WebCore.ResultMessage DeleteComment(String strAuthenData, String strGUID, InputChangeOrder_Comment objQuitJobRequest_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_InputChangeOrder_Comment objDA_InputChangeOrder_Comment = new DA_InputChangeOrder_Comment();
            objQuitJobRequest_Comment.DeletedUser = objToken.UserName;
            objResultMessage = objDA_InputChangeOrder_Comment.Delete(objQuitJobRequest_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách chi tiết yêu cầu nhập đổi/trả hàng", MessageName = "GetDetail")]
        public Library.WebCore.ResultMessage GetDetail(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetDetail(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách chi tiết yêu cầu nhập đổi/trả hàng", MessageName = "GetDetail_OutputVoucher")]
        public Library.WebCore.ResultMessage GetDetail(String strAuthenData, String strGUID, ref DataTable dtResult, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetDetail(ref dtResult, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách chi tiết yêu cầu nhập đổi/trả hàng", MessageName = "GetListReturn")]
        public Library.WebCore.ResultMessage GetListReturn(String strAuthenData, String strGUID, ref DataTable dtResult, string strOutputVoucherID, string strInputChangeOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrderDetail().GetListReturn(ref dtResult, strOutputVoucherID, strInputChangeOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách tập tin đính kèm của yêu cầu nhập đổi/trả hàng", MessageName = "GetAttachment")]
        public Library.WebCore.ResultMessage GetAttachment(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetAttachment(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách qui trình của yêu cầu nhập đổi/trả hàng", MessageName = "GetWorkFlow")]
        public Library.WebCore.ResultMessage GetWorkFlow(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetWorkFlow(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách khai báo qui trình của loại yêu cầu nhập đổi/trả hàng", MessageName = "GetWorkFlow_InputChangeOrderType")]
        public Library.WebCore.ResultMessage GetWorkFlow(String strAuthenData, String strGUID, ref DataTable dtResult, int intInputChangeOrderType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetWorkFlow(ref dtResult, intInputChangeOrderType);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách bình luận của yêu cầu nhập đổi/trả hàng", MessageName = "GetComment")]
        public Library.WebCore.ResultMessage GetComment(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetComment(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xóa yêu cầu nhập đổi/trả hàng", MessageName = "Delete")]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strInputChangeOrderID, string strContentDelete)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder = new ERP.Inventory.BO.InputChangeOrder.InputChangeOrder();
            objInputChangeOrder.InputChangeOrderID = strInputChangeOrderID;
            objInputChangeOrder.ContentDeleted = strContentDelete;
            objInputChangeOrder.DeletedUser = objToken.UserName;
            ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder objDA_InputChangeOrder = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder();
            objDA_InputChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_InputChangeOrder.Delete(objInputChangeOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật yêu cầu nhập đổi/trả hàng", MessageName = "Update")]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, DataTable dtbMachineErrorIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder objDA_InputChangeOrder = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder();
            objDA_InputChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objInputChangeOrder.UpdatedUser = objToken.UserName;

            objResultMessage = objDA_InputChangeOrder.Update(objInputChangeOrder, dtbMachineErrorIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Kiểm tra xóa chứng từ liên quan: phiếu nhập, phiếu chi", MessageName = "CheckDelete")]
        public Library.WebCore.ResultMessage CheckDelete(String strAuthenData, String strGUID, ref bool bolIsDeleted, string strInputVoucherID, string strVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().CheckDelete(ref bolIsDeleted, strInputVoucherID, strVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Hỗ trợ trả Care", MessageName = "UpdateCare")]
        public Library.WebCore.ResultMessage UpdateCare(String strAuthenData, String strGUID, ref int intResult, string strOutputVoucherDetailID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().UpdateCare(ref intResult, strOutputVoucherDetailID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách lỗi theo Mã InputChangeOrder", MessageName = "GetMachineError")]
        public Library.WebCore.ResultMessage GetMachineError(String strAuthenData, String strGUID, ref DataTable dtResult, string strInputChangeOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetListMachineError(ref dtResult, strInputChangeOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy danh sách IMEI thu hồi", MessageName = "GetEvictionIMEI")]
        public Library.WebCore.ResultMessage GetEvictionIMEI(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.InputChangeOrder.DA_InputChangeOrder().GetListIMEIInEvictionIMEI(ref dtResult, objKeywords);
            return objResultMessage;
        }

    }
}
