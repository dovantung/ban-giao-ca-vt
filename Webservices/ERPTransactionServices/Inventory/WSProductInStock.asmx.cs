﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
namespace ERPTransactionServices.Inventory
{
    /// <summary>
    /// Summary description for WSProductInStock
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSProductInStock : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm", MessageName = "Barcode_StoreID")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID)
        {
            return LoadInfo(strAuthenData, strGUID, ref objProductInStock, strBarcode, intStoreID, 0, string.Empty, false);
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm", MessageName = "Barcode_StoreID_OutputTypeID_AutoGetIMEIList")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID,
            int intOutputTypeID, string strAutoGetIMEIList)
        {
            return LoadInfo(strAuthenData, strGUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, true);
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm mới- có tự động lấy IMEI không", MessageName = "Barcode_StoreID_OutputTypeID_AutoGetIMEIList_Choose")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID,
            int intOutputTypeID, string strAutoGetIMEIList, bool bolIsAutoGetIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().LoadInfo(ref objProductInStock, strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, bolIsAutoGetIMEI, 1);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm - theo trạng thái sp tồn kho", MessageName = "Barcode_StoreID_OutputTypeID_AutoGetIMEIList_InstockStatus")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID,
            int intOutputTypeID, string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().LoadInfo(ref objProductInStock, strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm - theo đơn hàng", MessageName = "Barcode_StoreID_OutputTypeID_AutoGetIMEIList_InstockStatus_OrderID")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID,
            int intOutputTypeID, string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID, int intCustomerID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().LoadInfo(ref objProductInStock, strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, intCustomerID);
            return objResultMessage;
        }


        #region Tuan Anh: Bo sung check Fifo
        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm", MessageName = "Barcode_StoreID_IsUpdateOrdering")]
        public Library.WebCore.ResultMessage LoadInfoAndUpdateIsOrdering(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID, bool IsUpdateOrdering)
        {
            return LoadInfoAndUpdateIsOrdering(strAuthenData, strGUID, ref objProductInStock, strBarcode, intStoreID, 0, string.Empty, false, IsUpdateOrdering);
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm", MessageName = "Barcode_StoreID_OutputTypeID_AutoGetIMEIList_IsUpdateOrdering")]
        public Library.WebCore.ResultMessage LoadInfoAndUpdateIsOrdering(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID,
            int intOutputTypeID, string strAutoGetIMEIList, bool IsUpdateOrdering)
        {
            return LoadInfoAndUpdateIsOrdering(strAuthenData, strGUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, true, IsUpdateOrdering);
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm mới- có tự động lấy IMEI không", MessageName = "Barcode_StoreID_OutputTypeID_AutoGetIMEIList_Choose_IsUpdateOrdering")]
        public Library.WebCore.ResultMessage LoadInfoAndUpdateIsOrdering(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID,
            int intOutputTypeID, string strAutoGetIMEIList, bool bolIsAutoGetIMEI, bool IsUpdateOrdering)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().LoadInfoAndUpdateIsOrdering(ref objProductInStock, strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, bolIsAutoGetIMEI, 1, IsUpdateOrdering);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm - theo trạng thái sp tồn kho", MessageName = "Barcode_StoreID_OutputTypeID_AutoGetIMEIList_InstockStatus_IsUpdateOrdering")]
        public Library.WebCore.ResultMessage LoadInfoAndUpdateIsOrdering(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID,
            int intOutputTypeID, string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID, bool IsUpdateOrdering)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().LoadInfoAndUpdateIsOrdering(ref objProductInStock, strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, IsUpdateOrdering);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm - theo đơn hàng", MessageName = "Barcode_StoreID_OutputTypeID_AutoGetIMEIList_InstockStatus_OrderID_IsUpdateOrdering")]
        public Library.WebCore.ResultMessage LoadInfoAndUpdateIsOrdering(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strBarcode, int intStoreID,
            int intOutputTypeID, string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID, int intCustomerID, bool IsUpdateOrdering)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().LoadInfoAndUpdateIsOrdering(ref objProductInStock, strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, IsUpdateOrdering, intCustomerID);
            return objResultMessage;
        }

        #endregion


        /// <summary>
        /// Lấy thông tin và số lượng tồn của sản phẩm
        /// MD_PRODUCT_INSTOCK_GETQUANTITY
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objProductInStock"></param>
        /// <param name="strProduct"></param>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm", MessageName = "ProductInStockGetInfo")]
        public Library.WebCore.ResultMessage GetQuantity(String strAuthenData, String strGUID, ref ProductInStock objProductInStock, string strProduct, int intStoreID, int intIsNew, int intIsShowProduct, int intIsCheckRealInput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().GetQuantity(ref objProductInStock, strProduct, intStoreID, intIsNew, intIsShowProduct, intIsCheckRealInput);
            return objResultMessage;
        }


        /// <summary>
        /// Kiểm tra tồn tại IMEI
        /// </summary>
        [WebMethod(EnableSession = true, Description = "Kiểm tra tồn tại IMEI", MessageName = "Kiem_Tra_IMEI_Co_Ton_Kho")]
        public Library.WebCore.ResultMessage CheckIMEIExists(String strAuthenData, String strGUID, ref bool bolResult, string strProductID, string strIMEI, int intStoreID, int intIsNew, int intOutputTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().CheckIMEIExists(ref bolResult, strProductID, strIMEI, intStoreID, intIsNew, intOutputTypeID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho của sản phẩm", MessageName = "LoadProductStoreInSock")]
        public Library.WebCore.ResultMessage LoadProductStoreInSock(String strAuthenData, String strGUID, String strProductID, int intAreaID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().LoadProductStoreInSock(strProductID, intAreaID, -1, ref tblResult);
            return objResultMessage;
        }


        /// <summary>
        /// Lấy giá của imei máy cũ
        /// PM_GETIMEIPRICE_FORMOBILE
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="tblResult"></param>
        /// <param name="strProductID"></param>
        /// <param name="intOutputTypeID"></param>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "Lấy giá của imei máy cũ  (for MOBILE) ", MessageName = "GetIMEIPrice_ForMobile")]
        public Library.WebCore.ResultMessage GetIMEIPrice_ForMobile(String strAuthenData, String strGUID, ref DataTable tblResult, String strProductID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().GetIMEIPrice_ForMobile(ref tblResult, strProductID, intStoreID);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy sản phẩm tồn kho theo Model (for MOBILE)    
        /// PM_PRODUCTINSTOCK_BYMODEL
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="tblResult"></param>
        /// <param name="strModelID"></param>
        /// <param name="intOutputTypeID"></param>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "Lấy sản phẩm tồn kho theo Model (for MOBILE) ", MessageName = "GetProductInstock_ByModel")]
        public Library.WebCore.ResultMessage GetProductInstock_ByModel(String strAuthenData, String strGUID, ref DataTable tblResult, String strModelID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().GetProductInstock_ByModel(ref tblResult, strModelID, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Kiểm tra điều kiện tự động lấy IMEI", MessageName = "Kiem_Tra_IMEI_Co_Tu_Dong_Lay")]
        public Library.WebCore.ResultMessage CheckAutoGenIMEI(String strAuthenData, String strGUID, ref bool bolResult, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_CurrentInStockDetail().CheckAutoGenIMEI(ref bolResult, strProductID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Lấy danh sách IMEI theo số lượng cần ", MessageName = "GetIMEI")]
        public Library.WebCore.ResultMessage GetIMEIByQuantity(String strAuthenData, String strGUID, ref DataTable tblResult, String strProductID, int intStoreID, int intQuantity)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_CurrentInStockDetail().GetIMEIByQuantity(ref tblResult, strProductID, intStoreID, intQuantity, -1);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy danh sách IMEI theo số lượng cần ", MessageName = "GetIMEIByInStockStatusID")]
        public Library.WebCore.ResultMessage GetIMEIByQuantity(String strAuthenData, String strGUID, ref DataTable tblResult, String strProductID, int intStoreID, int intQuantity, int intInStockStatusID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_CurrentInStockDetail().GetIMEIByQuantity(ref tblResult, strProductID, intStoreID, intQuantity, intInStockStatusID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy danh sách IMEI theo số lượng cần ", MessageName = "GetIMEIByQuantityCustom")]
        public Library.WebCore.ResultMessage GetIMEIByQuantityCustom(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_CurrentInStockDetail().GetIMEIByQuantityCustom(ref tblResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy check product theo IMEI ", MessageName = "CheckIMEICustom")]
        public Library.WebCore.ResultMessage CheckIMEICustom(String strAuthenData, String strGUID, ref string strResult, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_CurrentInStockDetail().CheckIMEICustom(ref strResult, strIMEI);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Kiểm tra IMEI ", MessageName = "CheckIMEIInStock1")]
        public Library.WebCore.ResultMessage CheckIMEIInStock1(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_CurrentInStockDetail().CheckIMEIInStock1(ref tblResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật trạng thái đăt hàng ", MessageName = "Update_IsOrdering1")]
        public Library.WebCore.ResultMessage Update_IsOrdering(String strAuthenData, String strGUID, DataTable tblImei, int inIsOrdering, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().Update_IsOrdering(tblImei, inIsOrdering, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật trạng thái đăt hàng 2", MessageName = "Update_IsOrdering2")]
        public Library.WebCore.ResultMessage Update_IsOrderingByString(String strAuthenData, String strGUID, String xmlImei, int inIsOrdering, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().Update_IsOrdering(xmlImei, inIsOrdering, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lay danh sach imei fifo", MessageName = "GetFIFOList_ByProductID")]
        public Library.WebCore.ResultMessage GetFIFOList_ByProductID(String strAuthenData, String strGUID, ref DataTable dtbFIFO, string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().GetFIFOList_ByProductID(ref dtbFIFO, strProductID,strIMEI, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Lay danh sach imei fifo 1", MessageName = "GetFIFOList_ByIMEI1")]
        public Library.WebCore.ResultMessage GetFIFOList_ByIMEI(String strAuthenData, String strGUID, ref DataTable dtbFIFO, DataTable dtbIMEI, string strProductID, int intStoreID, int intOutputTypeID,
         int intMaxIMEICanShow = 4)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().GetFIFOList_ByIMEI(ref  dtbFIFO, dtbIMEI, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lay danh sach imei fifo 2", MessageName = "GetFIFOList_ByIMEI2")]
        public Library.WebCore.ResultMessage GetFIFOList_ByIMEI(String strAuthenData, String strGUID, ref DataTable dtbFIFO, String xmlImei, string strProductID, int intStoreID, int intOutputTypeID,
         int intMaxIMEICanShow = 4)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().GetFIFOList_ByIMEI(ref  dtbFIFO, xmlImei, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Lay danh sach imei fifo theo danh sach san pham va IMEI 1", MessageName = "GetFIFOList_ByIMEI_Product1")]
        public Library.WebCore.ResultMessage GetFIFOList_ByIMEI_Product(String strAuthenData, String strGUID, ref DataTable dtbFIFO, DataTable dtbIMEI, DataTable dtbProduct, int intStoreID, int intOutputTypeID,
         int intMaxIMEICanShow = 4)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().GetFIFOList_ByIMEI_Product(ref  dtbFIFO, dtbIMEI, dtbProduct, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lay danh sach imei fifo theo danh sach san pham va IMEI 1", MessageName = "GetFIFOList_ByIMEI_Product2")]
        public Library.WebCore.ResultMessage GetFIFOList_ByIMEI_Product(String strAuthenData, String strGUID, ref DataTable dtbFIFO, String xmlImei, string xmlProduct, int intStoreID, int intOutputTypeID,
         int intMaxIMEICanShow = 4)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_ProductInStock().GetFIFOList_ByIMEI_Product(ref  dtbFIFO, xmlImei, xmlProduct, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            return objResultMessage;
        }
    }
}
