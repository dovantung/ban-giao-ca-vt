﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Inventory.PM
{
    /// <summary>
    /// Summary description for WSIMEISalesInfo
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSLotIMEISalesInfo : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.PM.LotIMEISalesInfo objLotIMEISalesInfo, string strLotIMEISalesInfo)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo().LoadInfo(ref objLotIMEISalesInfo, strLotIMEISalesInfo);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Inventory.BO.PM.LotIMEISalesInfo objLotIMEISalesInfo)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo objDA_LotIMEISalesInfo = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo();
            objDA_LotIMEISalesInfo.objLogObject.CertificateString = objToken.CertificateString;
            objDA_LotIMEISalesInfo.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_LotIMEISalesInfo.objLogObject.LoginLogID = objToken.LoginLogID;
            objLotIMEISalesInfo.CreatedUser = objToken.UserName;
            objResultMessage = objDA_LotIMEISalesInfo.Insert(objLotIMEISalesInfo);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.PM.LotIMEISalesInfo objLotIMEISalesInfo)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo objDA_LotIMEISalesInfo = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo();
            objDA_LotIMEISalesInfo.objLogObject.CertificateString = objToken.CertificateString;
            objDA_LotIMEISalesInfo.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_LotIMEISalesInfo.objLogObject.LoginLogID = objToken.LoginLogID;
            objLotIMEISalesInfo.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_LotIMEISalesInfo.Update(objLotIMEISalesInfo);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Detete(ref int intResult, String strAuthenData, String strGUID, ERP.Inventory.BO.PM.LotIMEISalesInfo objLotIMEISalesInfo)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo objDA_LotIMEISalesInfo = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo();
            objDA_LotIMEISalesInfo.objLogObject.CertificateString = objToken.CertificateString;
            objDA_LotIMEISalesInfo.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_LotIMEISalesInfo.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_LotIMEISalesInfo.Delete(objLotIMEISalesInfo);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData_LotIMEISalesInfo_ProSpec_ToList(String strAuthenData, String strGUID, ref List<ERP.Inventory.BO.PM.LotIMEISalesInfo_ProSpec> lstLotIMEISalesInfo_ProSpec, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_ProSpec().SearchDataToList(ref lstLotIMEISalesInfo_ProSpec, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Review(String strAuthenData, String strGUID, List<ERP.Inventory.BO.PM.LotIMEISalesInfo> lstLotIMEISalesInfo)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo objDA_LotIMEISalesInfo = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo();
            objResultMessage = objDA_LotIMEISalesInfo.Review(lstLotIMEISalesInfo);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckExistIMEI(String strAuthenData, String strGUID, string strIMEI, string strLotIMEISalesInfoID, ref bool bolIsExist)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.BO.PM.LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI = new ERP.Inventory.BO.PM.LotIMEISalesInfo_IMEI();
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_IMEI objDA_LotIMEISalesInfo_IMEI = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_IMEI();
            objResultMessage = objDA_LotIMEISalesInfo_IMEI.LoadInfo(ref objLotIMEISalesInfo_IMEI, strIMEI, strLotIMEISalesInfoID);
            bolIsExist = objLotIMEISalesInfo_IMEI.IsExist;
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, List<ERP.Inventory.BO.PM.LotIMEISalesInfo> lstLotIMEISalesInfo)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo objDA_LotIMEISalesInfo = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo();
            objResultMessage = objDA_LotIMEISalesInfo.Delete(lstLotIMEISalesInfo);
            return objResultMessage;
        }
    }
}
