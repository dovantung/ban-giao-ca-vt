﻿using ERP.Inventory.BO.PM.PriceProtected;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Inventory.PM.PriceProtected
{
    /// <summary>
    /// Summary description for WSPriceProtect
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPriceProtect : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID,ref ERP.Inventory.BO.PM.PriceProtected.PriceProtect objPriceProtect)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect objDA_PriceProtect = new ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect();
            objDA_PriceProtect.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PriceProtect.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PriceProtect.objLogObject.LoginLogID = objToken.LoginLogID;
            objPriceProtect.CreatedUser = objToken.UserName;
            objResultMessage = objDA_PriceProtect.Insert(ref objPriceProtect);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CalInStock(String strAuthenData, String strGUID, ref DataTable dtbInStock,object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect objDA_PriceProtect = new ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect();
            objResultMessage = objDA_PriceProtect.CalInStock(ref dtbInStock, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CalculatorQuantity(String strAuthenData, String strGUID, ref List<ERP.Inventory.BO.PM.PriceProtected.PriceProtectDetail> lstPriceProtectDetail, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect objDA_PriceProtect = new ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect();
            objResultMessage = objDA_PriceProtect.CalculatorQuantity(ref lstPriceProtectDetail, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref PriceProtect objPriceProtect, string strPriceProtectID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect().LoadInfo(ref objPriceProtect, strPriceProtectID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, PriceProtect objPriceProtect)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objPriceProtect.UpdatedUser = objToken.UserName;
            objResultMessage = new ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect().Update(objPriceProtect);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, PriceProtect objPriceProtect)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objPriceProtect.DeletedUser = objToken.UserName;
            objResultMessage = new ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect().Delete(objPriceProtect);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateInternalInvoice(String strAuthenData, String strGUID, string strPriceProtectID, int intOutInvoiceTransTypeID, int intInInvoiceTransTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect objDA_PriceProtect = new ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect();
            objDA_PriceProtect.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PriceProtect.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PriceProtect.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PriceProtect.CreateInternalInvoice(strPriceProtectID, objToken.UserName, intOutInvoiceTransTypeID, intInInvoiceTransTypeID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateAdjustVoucher(String strAuthenData, String strGUID, string strPriceProtectID, int intOutputTypeID, int intInputTypeID, DateTime dteVoucherDate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect objDA_PriceProtect = new ERP.Inventory.DA.PM.PriceProtected.DA_PriceProtect();
            objDA_PriceProtect.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PriceProtect.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PriceProtect.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PriceProtect.CreateAdjustVoucher(strPriceProtectID, objToken.UserName, intOutputTypeID, intInputTypeID, dteVoucherDate);
            return objResultMessage;
        }
    }
}
