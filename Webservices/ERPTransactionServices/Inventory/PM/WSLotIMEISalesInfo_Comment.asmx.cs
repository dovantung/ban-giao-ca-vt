﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Inventory.PM
{
    /// <summary>
    /// Summary description for WSLotIMEISalesInfo_Comment
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSLotIMEISalesInfo_Comment : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.PM.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment, int intCommentID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment().LoadInfo(ref objLotIMEISalesInfo_Comment, intCommentID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Inventory.BO.PM.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment objDA_LotIMEISalesInfo_Comment = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment();
            objDA_LotIMEISalesInfo_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_LotIMEISalesInfo_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_LotIMEISalesInfo_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            objLotIMEISalesInfo_Comment.CreatedUser = objToken.UserName;
            objResultMessage = objDA_LotIMEISalesInfo_Comment.Insert(objLotIMEISalesInfo_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.PM.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment objDA_LotIMEISalesInfo_Comment = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment();
            objDA_LotIMEISalesInfo_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_LotIMEISalesInfo_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_LotIMEISalesInfo_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            objLotIMEISalesInfo_Comment.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_LotIMEISalesInfo_Comment.Update(objLotIMEISalesInfo_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeteteList(String strAuthenData, String strGUID, List<ERP.Inventory.BO.PM.LotIMEISalesInfo_Comment> lstLotIMEISalesInfo_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment objDA_LotIMEISalesInfo_Comment = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment();
            objDA_LotIMEISalesInfo_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_LotIMEISalesInfo_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_LotIMEISalesInfo_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_LotIMEISalesInfo_Comment.DeleteList(lstLotIMEISalesInfo_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateData(String strAuthenData, String strGUID, ERP.Inventory.BO.PM.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment_Update, ERP.Inventory.BO.PM.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment_Insert)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment objDA_LotIMEISalesInfo_Comment = new ERP.Inventory.DA.PM.DA_LotIMEISalesInfo_Comment();
            objDA_LotIMEISalesInfo_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_LotIMEISalesInfo_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_LotIMEISalesInfo_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            if(objLotIMEISalesInfo_Comment_Update!=null)
            objLotIMEISalesInfo_Comment_Update.UpdatedUser = objToken.UserName;
            if(objLotIMEISalesInfo_Comment_Insert!=null)
                objLotIMEISalesInfo_Comment_Insert.CreatedUser = objToken.UserName;
            objResultMessage = objDA_LotIMEISalesInfo_Comment.UpdateData(objLotIMEISalesInfo_Comment_Update, objLotIMEISalesInfo_Comment_Insert);
            return objResultMessage;
        }
    }
}
