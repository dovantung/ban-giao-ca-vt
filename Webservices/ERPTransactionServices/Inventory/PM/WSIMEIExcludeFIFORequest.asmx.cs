﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Inventory.PM
{
    /// <summary>
    /// Summary description for WSIMEIExcludeFIFORequest
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSIMEIExcludeFIFORequest : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.PM.IMEIExcludeFIFORequest objIMEIExcludeFIFORequest, string strIMEIExcludeFIFORequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_IMEIExcludeFIFORequest().LoadInfo(ref objIMEIExcludeFIFORequest, strIMEIExcludeFIFORequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Inventory.BO.PM.IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_IMEIExcludeFIFORequest objDA_IMEIExcludeFIFORequest = new ERP.Inventory.DA.PM.DA_IMEIExcludeFIFORequest();
            objDA_IMEIExcludeFIFORequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEIExcludeFIFORequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEIExcludeFIFORequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objIMEIExcludeFIFORequest.CreatedUser = objToken.UserName;
            objResultMessage = objDA_IMEIExcludeFIFORequest.Insert(objIMEIExcludeFIFORequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.PM.IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_IMEIExcludeFIFORequest objDA_IMEIExcludeFIFORequest = new ERP.Inventory.DA.PM.DA_IMEIExcludeFIFORequest();
            objDA_IMEIExcludeFIFORequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEIExcludeFIFORequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEIExcludeFIFORequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objIMEIExcludeFIFORequest.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_IMEIExcludeFIFORequest.Update(objIMEIExcludeFIFORequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Detete(ref int intResult, String strAuthenData, String strGUID, ERP.Inventory.BO.PM.IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_IMEIExcludeFIFORequest objDA_IMEIExcludeFIFORequest = new ERP.Inventory.DA.PM.DA_IMEIExcludeFIFORequest();
            objDA_IMEIExcludeFIFORequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEIExcludeFIFORequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEIExcludeFIFORequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objIMEIExcludeFIFORequest.DeletedUser = objToken.UserName;
            objResultMessage = objDA_IMEIExcludeFIFORequest.Delete(objIMEIExcludeFIFORequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_IMEIExcludeFIFORequest().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetIMEIExcludeFIFORequestNewID(String strAuthenData, String strGUID, ref string strIMEIExcludeFIFORequestID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_IMEIExcludeFIFORequest().GetIMEIExcludeFIFORequestNewID(ref strIMEIExcludeFIFORequestID, intStoreID);
            return objResultMessage;
        }
    }
}
