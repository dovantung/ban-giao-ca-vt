﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;
using System.IO;
using System.IO.Compression;

namespace ERPTransactionServices.Inventory.Inventory
{
    /// <summary>
    /// Summary description for WSInventory
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSInventory : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true, Description = "Khởi tạo mã phiếu kiểm kê", MessageName = "GetInventoryNewID")]
        public Library.WebCore.ResultMessage GetInventoryNewID(String strAuthenData, String strGUID, ref string strInventoryNewID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetInventoryNewID(ref strInventoryNewID, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu tồn kho sản phẩm của kỳ kiểm kê", MessageName = "GetListTermStock")]
        public Library.WebCore.ResultMessage GetListTermStock(String strAuthenData, String strGUID, ref DataTable dtbInStockProduct, int intInventoryTermID, int intStoreID, int intMainGroupID, int intSubGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetListTermStock(ref dtbInStockProduct, intInventoryTermID, intStoreID, intMainGroupID, intSubGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu tồn kho IMEI của kỳ kiểm kê", MessageName = "GetListTermStockIMEI")]
        public Library.WebCore.ResultMessage GetListTermStockIMEI(String strAuthenData, String strGUID, ref DataTable dtbInStockProductIMEI, int intInventoryTermID, int intStoreID, int intMainGroupID, int intSubGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetListTermStockIMEI(ref dtbInStockProductIMEI, intInventoryTermID, intStoreID, intMainGroupID, intSubGroupID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu tồn kho sản phẩm của kỳ kiểm kê", MessageName = "GetListTermStock2")]
        public Library.WebCore.ResultMessage GetListTermStockList(String strAuthenData, String strGUID, ref DataTable dtbInStockProduct, int intInventoryTermID, int intStoreID, string strMainGroupIDList, string strSubGroupIDList, int intProductStatusID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetListTermStock(ref dtbInStockProduct, intInventoryTermID, intStoreID, strMainGroupIDList, strSubGroupIDList, intProductStatusID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu tồn kho IMEI của kỳ kiểm kê", MessageName = "GetListTermStockIMEI2")]
        public Library.WebCore.ResultMessage GetListTermStockIMEIList(String strAuthenData, String strGUID, ref DataTable dtbInStockProductIMEI, int intInventoryTermID, int intStoreID, string strMainGroupIDList, string strSubGroupIDList, int intProductStatusID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetListTermStockIMEI(ref dtbInStockProductIMEI, intInventoryTermID, intStoreID, strMainGroupIDList, strSubGroupIDList, intProductStatusID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu tồn kho IMEI của kỳ kiểm kê", MessageName = "GetListTermStockIMEI_Unique")]
        public Library.WebCore.ResultMessage GetListTermStockIMEI_Unique(String strAuthenData, String strGUID, ref DataTable dtbInStockProductIMEI, int intInventoryTermID, int intStoreID, int intMainGroupID, int intSubGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetListTermStockIMEI_Unique(ref dtbInStockProductIMEI, intInventoryTermID, intStoreID, intMainGroupID, intSubGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Thêm mới phiếu kiểm kê", MessageName = "AddInventory")]
        public Library.WebCore.ResultMessage AddInventory(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.Inventory objInventory, List<ERP.Inventory.BO.Inventory.InventoryDetail> objInventoryDetailList, DataTable dtbUserReviewLevel, ref string strInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_Inventory objDA_Inventory = new ERP.Inventory.DA.Inventory.DA_Inventory();
            objDA_Inventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Inventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Inventory.objLogObject.LoginLogID = objToken.LoginLogID;
            objInventory.CreatedUser = objToken.UserName;

            objResultMessage = objDA_Inventory.AddInventory(objInventory, objInventoryDetailList, dtbUserReviewLevel, ref strInventoryID);
            return objResultMessage;

        }

        [WebMethod(EnableSession = true, Description = "Thêm mới phiếu kiểm kê VTS", MessageName = "AddInventoryVTS")]
        public Library.WebCore.ResultMessage AddInventoryVTS(String strAuthenData, String strGUID,
            ERP.Inventory.BO.Inventory.Inventory objInventory, DataTable dtbInventoryDetail, ref string strInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_Inventory objDA_Inventory = new ERP.Inventory.DA.Inventory.DA_Inventory();
            objDA_Inventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Inventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Inventory.objLogObject.LoginLogID = objToken.LoginLogID;
            objInventory.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Inventory.AddInventoryVTS(objInventory, dtbInventoryDetail, ref strInventoryID);
            return objResultMessage;

        }


        [WebMethod(EnableSession = true, Description = "Cập nhật phiếu kiểm kê", MessageName = "UpdateInventory")]
        public Library.WebCore.ResultMessage UpdateInventory(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.Inventory objInventory, List<ERP.Inventory.BO.Inventory.InventoryDetail> objInventoryDetailList, List<ERP.Inventory.BO.Inventory.InventoryDetailIMEI> lstProductIMEI_Delete)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().UpdateInventory(objInventory, objInventoryDetailList, lstProductIMEI_Delete);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật phiếu kiểm kê và thêm mức duyệt", MessageName = "UpdateInventory_AddReviewList")]
        public Library.WebCore.ResultMessage UpdateInventory_AddReviewList(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.Inventory objInventory, List<ERP.Inventory.BO.Inventory.InventoryDetail> objInventoryDetailList, List<ERP.Inventory.BO.Inventory.InventoryDetailIMEI> lstProductIMEI_Delete, DataTable dtbUserReviewLevel)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().UpdateInventory_AddReviewList(objInventory, objInventoryDetailList, lstProductIMEI_Delete, dtbUserReviewLevel);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm phiếu kiểm kê", MessageName = "GetInventorySearch")]
        public Library.WebCore.ResultMessage GetInventorySearch(String strAuthenData, String strGUID, ref DataTable dtbInventorySearch, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetInventorySearch(ref dtbInventorySearch, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp thông tin phiếu kiểm kê", MessageName = "LoadInventoryInfo")]
        public Library.WebCore.ResultMessage LoadInventoryInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.Inventory.Inventory objInventory, string strInventoryID, ref DataTable dtbInventoryDetail)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().LoadInfo(ref objInventory, strInventoryID, ref dtbInventoryDetail);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy danh sách người dùng duyệt phiếu kiểm kê", MessageName = "GetUserReviewFunction")]
        public Library.WebCore.ResultMessage GetUserReviewFunction(String strAuthenData, String strGUID, ref DataTable dtbUserReviewFunction, string strReviewFunctionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetUserReviewFunction(ref dtbUserReviewFunction, strReviewFunctionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy mức duyệt-người dùng duyệt phiếu kiểm kê", MessageName = "GetInventoryReviewLevel")]
        public Library.WebCore.ResultMessage GetInventoryReviewLevel(String strAuthenData, String strGUID, ref DataTable dtbInventoryReviewLevel, string strInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetInventoryReviewLevel(ref dtbInventoryReviewLevel, strInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy người dùng duyệt phiếu kiểm kê", MessageName = "GetInventoryUserReviewed")]
        public Library.WebCore.ResultMessage GetInventoryUserReviewed(String strAuthenData, String strGUID, ref DataTable dtbInventoryUserReviewed, string strReviewFunctionID, string strInventoryID, int intReviewLevelID, int intStoreID, bool bolIsCheckStorePermission)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetInventoryUserReviewed(ref dtbInventoryUserReviewed, strReviewFunctionID, strInventoryID, intReviewLevelID, intStoreID, bolIsCheckStorePermission);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Kiểm tra phiếu kiểm kê", MessageName = "CheckExistInventory")]
        public Library.WebCore.ResultMessage CheckExistInventory(String strAuthenData, String strGUID, ref string strInventoryID, int intInventoryTermID, int intInventoryStoreID, int intMainGroupID, int intProductInstockID, int intSubGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().CheckExistInventory(ref strInventoryID, intInventoryTermID, intInventoryStoreID, intMainGroupID, intProductInstockID, intSubGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật người duyệt phiếu kiểm kê", MessageName = "UpdateInventoryReviewStatus")]
        public Library.WebCore.ResultMessage UpdateInventoryReviewStatus(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.Inventory_ReviewList objInventoryReview, bool bolIsReviewedInventory)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_Inventory objDA_Inventory = new ERP.Inventory.DA.Inventory.DA_Inventory();
            objDA_Inventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Inventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Inventory.objLogObject.LoginLogID = objToken.LoginLogID;


            objResultMessage = objDA_Inventory.UpdateInventoryReviewStatus(objInventoryReview, bolIsReviewedInventory);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật chênh lệch kiểm kê", MessageName = "AddInventoryUnEvent")]
        public Library.WebCore.ResultMessage AddInventoryUnEvent(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.Inventory objInventory, DataTable dtbInventoryUnEvent)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().AddInventoryUnEvent(objInventory, dtbInventoryUnEvent, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy chênh lệch kiểm kê", MessageName = "GetInventoryUnEvent")]
        public Library.WebCore.ResultMessage GetInventoryUnEvent(String strAuthenData, String strGUID, ref DataTable dtbInventoryUnEvent, string strInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetInventoryUnEvent(ref dtbInventoryUnEvent, strInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật giải trình chênh lệch kiểm kê", MessageName = "UpdateExplainUnEvent")]
        public Library.WebCore.ResultMessage UpdateExplainUnEvent(String strAuthenData, String strGUID, string strInventoryID, DataTable dtbInventoryUnEvent)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().UpdateExplainUnEvent(strInventoryID, dtbInventoryUnEvent, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy xử lý chênh lệch kiểm kê", MessageName = "GetInventoryHandleUnEvent")]
        public Library.WebCore.ResultMessage GetInventoryHandleUnEvent(String strAuthenData, String strGUID, ref DataTable dtbInventoryHandleUnEvent, string strInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetInventoryHandleUnEvent(ref dtbInventoryHandleUnEvent, strInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xóa thông tin phiếu kiểm kê", MessageName = "DeleteInventory")]
        public Library.WebCore.ResultMessage DeleteInventory(String strAuthenData, String strGUID, string strInventoryID, string strContentDelete)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().DeleteInventory(strInventoryID, strContentDelete, objToken.UserName, objToken.CertificateString, HttpContext.Current.Request.UserHostAddress, objToken.LoginLogID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật nội dung phiếu kiểm kê", MessageName = "UpdateContent")]
        public Library.WebCore.ResultMessage UpdateContent(String strAuthenData, String strGUID, string strInventoryID, string strContent)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().UpdateContent(strInventoryID, strContent);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu phiếu kiểm kê", MessageName = "GetInventory")]
        public Library.WebCore.ResultMessage GetInventory(String strAuthenData, String strGUID, ref ERP.Inventory.BO.Inventory.Inventory objInventory, string strInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetInventory(ref objInventory, strInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu hàng đi đường", MessageName = "GetCheckRealQuantity")]
        public Library.WebCore.ResultMessage GetCheckRealQuantity(String strAuthenData, String strGUID, ref DataTable dtbCheckRealQuantity, int intInventoryTermID, string strInventoryID, int intStoreID, int intIsNew)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetCheckRealQuantity(ref dtbCheckRealQuantity, intInventoryTermID, strInventoryID, intStoreID, intIsNew);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tính số phiếu kiểm kê trên cùng 1 kho", MessageName = "CountSimilarInventory")]
        public Library.WebCore.ResultMessage CountSimilarInventory(String strAuthenData, String strGUID, ref int intCountInventory, int intInventoryTermID, int intInventoryStoreID, int intMainGroupID, int intIsNew, int intSubGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().CountSimilarInventory(ref intCountInventory, intInventoryTermID, intInventoryStoreID, intMainGroupID, intIsNew, intSubGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nối dữ liệu kiểm kê", MessageName = "JoinInventory")]
        public Library.WebCore.ResultMessage JoinInventory(String strAuthenData, String strGUID, int intInventoryTermID, int intInventoryStoreID, int intMainGroupID, int intIsNew)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().JoinInventory(intInventoryTermID, intInventoryStoreID, intMainGroupID, intIsNew);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nối dữ liệu kiểm kê", MessageName = "JoinInventorySubGroup")]
        public Library.WebCore.ResultMessage JoinInventorySubGroup(String strAuthenData, String strGUID, int intInventoryTermID, int intInventoryStoreID, int intMainGroupID, int intIsNew, int intSubGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().JoinInventorySubGroup(intInventoryTermID, intInventoryStoreID, intMainGroupID, intIsNew, intSubGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu ngày nhập IMEI", MessageName = "GetCheckIMEI_InputStore")]
        public Library.WebCore.ResultMessage GetCheckIMEI_InputStore(String strAuthenData, String strGUID, ref DataTable dtbCheckIMEI_InputStore, string strInIMEILIST)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetCheckIMEI_InputStore(ref dtbCheckIMEI_InputStore, strInIMEILIST);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu ngày xuất IMEI", MessageName = "GetCheckIMEI_OutputStore")]
        public Library.WebCore.ResultMessage GetCheckIMEI_OutputStore(String strAuthenData, String strGUID, ref DataTable dtbCheckIMEI_OutputStore, string strOutIMEILIST)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetCheckIMEI_OutputStore(ref dtbCheckIMEI_OutputStore, strOutIMEILIST);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy danh sách người dùng có thể duyệt tất cả phiếu kiểm kê đã chọn", MessageName = "GetUserCanReviewAll")]
        public Library.WebCore.ResultMessage GetUserCanReviewAll(String strAuthenData, String strGUID, ref DataTable dtbUser, string strInventoryIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetUserCanReviewAll(ref dtbUser, strInventoryIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xác nhận xử lý dữ liệu conflict của các phiếu kiểm kê thuộc một kho trong một kỳ kiểm kê", MessageName = "InventoryResolveConflict")]

        public Library.WebCore.ResultMessage InventoryResolveConflict(String strAuthenData, String strGUID, DataTable dtbResolveData)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().InventoryResolveConflict(dtbResolveData, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nối các phiếu kiểm kê của một kho trong một kỳ kiểm kê", MessageName = "InventorySuperJoin")]

        public Library.WebCore.ResultMessage InventorySuperJoin(String strAuthenData, String strGUID, ref string newInventoryID, int inventoryTermID, int storeID)
        {
            newInventoryID = string.Empty;
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().InventorySuperJoin(inventoryTermID, storeID, objToken.UserName, ref newInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật người duyệt danh sách phiếu kiểm kê", MessageName = "UpdateInventoryReviewList")]
        public Library.WebCore.ResultMessage UpdateInventoryReviewList(String strAuthenData, String strGUID, List<ERP.Inventory.BO.Inventory.Inventory_ReviewList> lstInventoryReview, List<bool> lstIsReviewedInventory)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_Inventory objDA_Inventory = new ERP.Inventory.DA.Inventory.DA_Inventory();
            objDA_Inventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Inventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Inventory.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_Inventory.UpdateInventoryReviewList(lstInventoryReview, lstIsReviewedInventory);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu sản phẩm chốt tồn", MessageName = "Tra ve DataTable")]
        public Library.WebCore.ResultMessage INVProduct_SelInStock(String strAuthenData, String strGUID, ref DataTable dsData,
            string strBarcode, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_Inventory objDA_Inventory = new ERP.Inventory.DA.Inventory.DA_Inventory();
            objDA_Inventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Inventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Inventory.objLogObject.LoginLogID = objToken.LoginLogID;
            dsData = null;
            objResultMessage = objDA_Inventory.INVProduct_SelInStock(ref dsData, strBarcode, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu datatable", MessageName = "Heavy_Tra ve Byte")]
        public Library.WebCore.ResultMessage GetHeavyData(String strAuthenData, String strGUID, ref Byte[] objByte,
            string strReportStoreName, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_Inventory objDA_Inventory = new ERP.Inventory.DA.Inventory.DA_Inventory();
            objDA_Inventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Inventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Inventory.objLogObject.LoginLogID = objToken.LoginLogID;
            DataSet dsData = null;
            objResultMessage = objDA_Inventory.GetHeavyData(ref dsData, strReportStoreName, objKeywords);
            if (!objResultMessage.IsError && dsData != null)
                objByte = ConvertDataSetToByte(dsData);
            return objResultMessage;
        }


        private Byte[] ConvertDataSetToByte(DataSet dsData)
        {
            MemoryStream objMemoryStream = new MemoryStream();
            GZipStream objGZipStream = new GZipStream(objMemoryStream, CompressionMode.Compress);
            dsData.WriteXml(objGZipStream, XmlWriteMode.WriteSchema);
            objGZipStream.Close();
            Byte[] objByte = objMemoryStream.ToArray();
            objMemoryStream.Close();
            return objByte;
        }

        private Byte[] ConvertDataTabeToByte(DataTable dsData)
        {
            MemoryStream objMemoryStream = new MemoryStream();
            GZipStream objGZipStream = new GZipStream(objMemoryStream, CompressionMode.Compress);
            dsData.WriteXml(objGZipStream, XmlWriteMode.WriteSchema);
            objGZipStream.Close();
            Byte[] objByte = objMemoryStream.ToArray();
            objMemoryStream.Close();
            return objByte;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu tồn kho sản phẩm của kỳ kiểm kê", MessageName = "GetListTermStock_to_Byte")]
        public Library.WebCore.ResultMessage GetListTermStockList_Byte(String strAuthenData, String strGUID, ref Byte[] objByte, int intInventoryTermID, int intStoreID, string strMainGroupIDList, string strSubGroupIDList, int intProductStatusID)
        {

            DataTable dtbInStockProduct = null;
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetListTermStock(ref dtbInStockProduct, intInventoryTermID, intStoreID, strMainGroupIDList, strSubGroupIDList, intProductStatusID);
            if (!objResultMessage.IsError && dtbInStockProduct != null)
                objByte = ConvertDataTabeToByte(dtbInStockProduct);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu tồn kho IMEI của kỳ kiểm kê", MessageName = "GetListTermStockIMEI_to_Byte")]
        public Library.WebCore.ResultMessage GetListTermStockIMEIList_Byte(String strAuthenData, String strGUID, ref Byte[] objByte, int intInventoryTermID, int intStoreID, string strMainGroupIDList, string strSubGroupIDList, int intProductStatusID)
        {
            DataTable dtbInStockProductIMEI = null;
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().GetListTermStockIMEI(ref dtbInStockProductIMEI, intInventoryTermID, intStoreID, strMainGroupIDList, strSubGroupIDList, intProductStatusID);
            if (!objResultMessage.IsError && dtbInStockProductIMEI != null)
                objByte = ConvertDataTabeToByte(dtbInStockProductIMEI);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xác nhận giải trình chênh lệch kiểm kê", MessageName = "AccessExplainUnEvent")]
        public Library.WebCore.ResultMessage AccessExplainUnEvent(String strAuthenData, String strGUID, string strInventoryID, string strUsername)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_Inventory().AccessExplainUnEvent(strInventoryID, strUsername);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Cap nhat muc duyet", MessageName = "AddInventoryAFJoin")]
        public Library.WebCore.ResultMessage AddInventoryAFJoin(String strAuthenData, String strGUID,
            ERP.Inventory.BO.Inventory.Inventory objInventory, DataTable dtbUserReviewLevel, ref string strInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_Inventory objDA_Inventory = new ERP.Inventory.DA.Inventory.DA_Inventory();
            objDA_Inventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Inventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Inventory.objLogObject.LoginLogID = objToken.LoginLogID;
            objInventory.CreatedUser = objToken.UserName;

            objResultMessage = objDA_Inventory.AddInventoryAFJoin(objInventory, dtbUserReviewLevel, ref strInventoryID);
            return objResultMessage;

        }
        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu nguyen nhan loi", MessageName = "SearchDataCauseGroups")]
        public Library.WebCore.ResultMessage SearchDataCauseGroups(String strAuthenData, String strGUID, ref DataTable dsData)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryTerm().SearchDataCauseGroups(ref dsData);
            return objResultMessage;
        }
        

    }
}
