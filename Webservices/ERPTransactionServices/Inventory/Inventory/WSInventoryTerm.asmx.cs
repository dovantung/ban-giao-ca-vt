﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.Inventory
{
    /// <summary>
    /// Summary description for WSInventoryTerm
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSInventoryTerm : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true, Description = "Lấy danh sách khai báo kỳ kiểm kê", MessageName = "SearchData")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryTerm().SearchData(ref dtbData, objKeywords);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true, Description = "Lấy danh sách khai báo kỳ kiểm kê theo user", MessageName = "SearchData_ByUser")]
        public Library.WebCore.ResultMessage SearchData_ByUser(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryTerm().SearchData_ByUser(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin kỳ kiểm kê", MessageName = "LoadInfo")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.Inventory.InventoryTerm objInventoryTerm, int intInventoryTermID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            string strUserName = objToken.UserName;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryTerm().LoadInfo(strUserName, ref objInventoryTerm, intInventoryTermID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin kỳ kiểm kê theo user", MessageName = "LoadInfo_ByUser")]
        public Library.WebCore.ResultMessage LoadInfo_ByUser(String strAuthenData, String strGUID, ref ERP.Inventory.BO.Inventory.InventoryTerm objInventoryTerm, int intInventoryTermID,string strUser)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            string strUserName = strUser;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryTerm().LoadInfo_ByUser(strUserName, ref objInventoryTerm, intInventoryTermID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Thêm mới kỳ kiểm kê", MessageName = "Insert")]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.InventoryTerm objInventoryTerm)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_InventoryTerm objDA_InventoryTerm = new ERP.Inventory.DA.Inventory.DA_InventoryTerm();
            objDA_InventoryTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InventoryTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InventoryTerm.objLogObject.LoginLogID = objToken.LoginLogID;
            objInventoryTerm.CreatedUser = objToken.UserName;

            objResultMessage = objDA_InventoryTerm.Insert(objInventoryTerm, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật kỳ kiểm kê", MessageName = "Update")]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.InventoryTerm objInventoryTerm)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_InventoryTerm objDA_InventoryTerm = new ERP.Inventory.DA.Inventory.DA_InventoryTerm();
            objDA_InventoryTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InventoryTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InventoryTerm.objLogObject.LoginLogID = objToken.LoginLogID;
            objInventoryTerm.CreatedUser = objToken.UserName;

            objResultMessage = objDA_InventoryTerm.Update(objInventoryTerm, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy kho kiểm kê", MessageName = "GetAllStore")]
        public Library.WebCore.ResultMessage GetAllStore(String strAuthenData, String strGUID, ref DataTable dtbStore, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryTerm().GetAllStore(ref dtbStore, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy ngành hàng kiểm kê", MessageName = "GetAllMainGroup")]
        public Library.WebCore.ResultMessage GetAllMainGroup(String strAuthenData, String strGUID, ref DataTable dtbMainGroup, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryTerm().GetAllMainGroup(ref dtbMainGroup, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy nhà sản xuất kiểm kê", MessageName = "GetAllBrand")]
        public Library.WebCore.ResultMessage GetAllBrand(String strAuthenData, String strGUID, ref DataTable dtbBrand, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryTerm().GetAllBrand(ref dtbBrand, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xóa kỳ kiểm kê", MessageName = "Delete")]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intInventoryTermID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_InventoryTerm objDA_InventoryTerm = new ERP.Inventory.DA.Inventory.DA_InventoryTerm();
            objDA_InventoryTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InventoryTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InventoryTerm.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_InventoryTerm.Delete(intInventoryTermID, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tính tồn kho kỳ kiểm kê", MessageName = "CalStockData")]
        public Library.WebCore.ResultMessage CalStockData(String strAuthenData, String strGUID, int intInventoryTermID, DataTable dtbStore)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryTerm().CalStockData(intInventoryTermID, dtbStore);
            return objResultMessage;
        }

    }
}
