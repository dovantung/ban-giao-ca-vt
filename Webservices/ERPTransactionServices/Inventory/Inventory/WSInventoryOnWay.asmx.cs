﻿using ERP.Inventory.BO.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Inventory.Inventory
{
    /// <summary>
    /// Summary description for WSInventoryOnWay
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSInventoryOnWay : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true, Description = "Tìm kiếm phiếu kiểm kê hàng đi đường", MessageName = "InventoryOnWaySearch")]
        public Library.WebCore.ResultMessage InventoryOnWaySearch(String strAuthenData, String strGUID, ref List<INV_InventoryOnWay> lstINV_InventoryOnWay, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryOnWay().SearchDataToList(ref lstINV_InventoryOnWay, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm chi tiết phiếu kiểm kê hàng đi đường", MessageName = "InventoryOnWayDetailSearch")]
        public Library.WebCore.ResultMessage InventoryOnWayDetailSearch(String strAuthenData, String strGUID, ref List<INV_InventoryOnWayDetail> lstINV_InventoryOnWayDetail, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryOnWayDetail().SearchDataToList(ref lstINV_InventoryOnWayDetail, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm chi tiết phiếu kiểm kê hàng đi đường", MessageName = "SearchDataDetail")]
        public Library.WebCore.ResultMessage SearchDataDetail(String strAuthenData, String strGUID, ref List<INV_InventoryOnWayDetail> lstINV_InventoryOnWayDetail, ref List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryOnWay().SearchDataDetail(ref lstINV_InventoryOnWayDetail, ref lstINV_InventoryOnWay_AttachMent, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật phiếu kiểm kê hàng đi đường", MessageName = "InsertAndUpdate")]
        public Library.WebCore.ResultMessage InsertAndUpdate(String strAuthenData, String strGUID, List<INV_InventoryOnWay> lstINV_InventoryOnWay, Boolean bolIsConfirm)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_InventoryOnWay objDA_InventoryOnWay = new ERP.Inventory.DA.Inventory.DA_InventoryOnWay();
            objDA_InventoryOnWay.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InventoryOnWay.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InventoryOnWay.objLogObject.LoginLogID = objToken.LoginLogID;
           
            objResultMessage = objDA_InventoryOnWay.InsertAndUpdate(lstINV_InventoryOnWay, objToken.UserName, bolIsConfirm);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Tự động giải trình hệ thống", MessageName = "AutoListInputVoucher")]
        public Library.WebCore.ResultMessage AutoListInputVoucher(String strAuthenData, String strGUID, string lstInputVoucher,int isInputStore,string user,DateTime timeInput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryOnWay().InventwayAutoReviewed(lstInputVoucher, user, timeInput, isInputStore);
            return objResultMessage;
        }

    }
}
