﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.Inventory
{
    /// <summary>
    /// Summary description for WSInventoryProcess
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSInventoryProcess : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true, Description = "Cập nhật xử lý chênh lệch kiểm kê", MessageName = "AddInventoryProcess")]
        public Library.WebCore.ResultMessage AddInventoryProcess(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.InventoryProcess objInventoryProcess, DataTable dtbProcessHandler)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            objInventoryProcess.CreatedUser = objToken.UserName;

            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().AddInventoryProcess(objInventoryProcess, dtbProcessHandler);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm thông tin quản lý yêu cầu kiểm kê", MessageName = "SearchData")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().SearchData(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp thông tin quản lý yêu cầu kiểm kê", MessageName = "LoadInfo")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.Inventory.InventoryProcess objInventoryProcess, string strInventoryProcessID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            objInventoryProcess.CreatedUser = objToken.UserName;

            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().LoadInfo(ref objInventoryProcess, strInventoryProcessID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách nhân viên truy thu", MessageName = "LoadInventProcessArrear")]
        public Library.WebCore.ResultMessage LoadInventProcessArrear(String strAuthenData, String strGUID, ref DataTable dtbUserArrear, string strInventoryProcessID, string strRelateVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().LoadInventProcessArrear(ref dtbUserArrear, strInventoryProcessID, strRelateVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật xử lý chênh lệch kiểm kê", MessageName = "UpdateInventoryProcess")]
        public Library.WebCore.ResultMessage UpdateInventoryProcess(String strAuthenData, String strGUID, string strInventoryProcessID, string strInputContent, string strOutputContent, string strPrChangeContent, string strSTChangeContent, 
            int intOriginateStoreID, DataTable dtbInput, DataTable dtbOutput, DataTable dtbProductChange, DataTable dtbStatusChange, ref DataTable dtbCollectArrear)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().UpdateInventoryProcess(strInventoryProcessID, strInputContent, strOutputContent, strPrChangeContent, strSTChangeContent, intOriginateStoreID, dtbInput, dtbOutput, dtbProductChange, dtbStatusChange, ref dtbCollectArrear);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Duyệt xử lý chênh lệch kiểm kê", MessageName = "ReviewedInventProcess")]
        public Library.WebCore.ResultMessage ReviewedInventProcess(String strAuthenData, String strGUID, string strInventoryProcessID, bool bolIsReviewed)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().ReviewedInventProcess(strInventoryProcessID, bolIsReviewed, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xử lý chênh lệch kiểm kê", MessageName = "ProcessedInventProcess")]
        public Library.WebCore.ResultMessage ProcessedInventProcess(String strAuthenData, String strGUID, string strInventoryProcessID, bool bolIsProcess, int intOriginateStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().ProcessedInventProcess(strInventoryProcessID, bolIsProcess, objToken.UserName, intOriginateStoreID, objToken.CertificateString, HttpContext.Current.Request.UserHostAddress, objToken.LoginLogID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu", MessageName = "GetDataSourceByStoreName")]
        public Library.WebCore.ResultMessage GetDataSourceByStoreName(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords, string strStoreName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().GetDataSourceByStoreName(ref dtbData, objKeywords, strStoreName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Kiểm tra phiếu xử lý kiểm kê", MessageName = "CheckExistInventoryProcess")]
        public Library.WebCore.ResultMessage CheckExistInventoryProcess(String strAuthenData, String strGUID, ref string strInventoryProcessID, string strInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().CheckExistInventoryProcess(ref strInventoryProcessID, strInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xóa thông tin phiếu xử lý kiểm kê", MessageName = "DeleteInventoryProcess")]
        public Library.WebCore.ResultMessage DeleteInventoryProcess(String strAuthenData, String strGUID, string strInventoryProcessID, string strDeletedReason)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_InventoryProcess().DeleteInventoryProcess(strInventoryProcessID, strDeletedReason, objToken.UserName);
            return objResultMessage;
        }

    }
}
