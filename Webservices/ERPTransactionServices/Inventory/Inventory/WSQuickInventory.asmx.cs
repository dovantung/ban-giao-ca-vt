﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.Inventory
{
    /// <summary>
    /// Summary description for WSQuickInventory
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSQuickInventory : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true, Description = "Khởi tạo mã phiếu kiểm kê nhanh", MessageName = "GetNewID")]
        public Library.WebCore.ResultMessage GetNewID(String strAuthenData, String strGUID, ref string strQuickInventoryNewID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_QuickInventory().GetNewID(ref strQuickInventoryNewID, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu tồn kho", MessageName = "GetInstockData")]
        public Library.WebCore.ResultMessage GetInstockData(String strAuthenData, String strGUID, ref DataTable dtbInStockData, ref DataTable dtbInStockIMEI, DateTime dtmLockStockTime, int intStoreID, int intMainGroupID,
            int intSubGroupID, string strBrandIDList, int intIsNew, int intIsCheckRealInput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_QuickInventory().GetInstockData(ref dtbInStockData, ref dtbInStockIMEI, dtmLockStockTime, intStoreID, intMainGroupID, intSubGroupID, strBrandIDList, intIsNew, intIsCheckRealInput);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Thêm mới phiếu kiểm kê nhanh", MessageName = "Insert")]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.QuickInventory objQuickInventory, ref string strQuickInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_QuickInventory objDA_QuickInventory = new ERP.Inventory.DA.Inventory.DA_QuickInventory();
            objDA_QuickInventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_QuickInventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_QuickInventory.objLogObject.LoginLogID = objToken.LoginLogID;
            objQuickInventory.CreatedUser = objToken.UserName;

            objResultMessage = objDA_QuickInventory.Insert(objQuickInventory, ref strQuickInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật phiếu kiểm kê nhanh", MessageName = "Update")]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.QuickInventory objQuickInventory, List<ERP.Inventory.BO.Inventory.QuickInventoryDetail> DeleteList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_QuickInventory objDA_QuickInventory = new ERP.Inventory.DA.Inventory.DA_QuickInventory();
            objDA_QuickInventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_QuickInventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_QuickInventory.objLogObject.LoginLogID = objToken.LoginLogID;
            objQuickInventory.UpdatedUser = objToken.UserName;

            objResultMessage = objDA_QuickInventory.Update(objQuickInventory, DeleteList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xóa phiếu kiểm kê nhanh", MessageName = "Delete")]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strQuickInventoryID, string strDeletedNote)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.BO.Inventory.QuickInventory objQuickInventory = new ERP.Inventory.BO.Inventory.QuickInventory();
            objQuickInventory.QuickInventoryID = strQuickInventoryID;
            ERP.Inventory.DA.Inventory.DA_QuickInventory objDA_QuickInventory = new ERP.Inventory.DA.Inventory.DA_QuickInventory();
            objDA_QuickInventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_QuickInventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_QuickInventory.objLogObject.LoginLogID = objToken.LoginLogID;
            objQuickInventory.DeletedUser = objToken.UserName;
            objQuickInventory.DeletedNote = strDeletedNote;

            objResultMessage = objDA_QuickInventory.Delete(objQuickInventory);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy nạp danh sách phiếu kiểm kê nhanh
        /// </summary>
        /// <param name="intType">1:QuickInventoryID, 2:StoreName, 3:MainGroupName, 4:InventoryUser, 5:CreatedUser, 6:ReviewedUser</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "Lấy nạp danh sách phiếu kiểm kê nhanh", MessageName = "SearchData")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbQuickInventoryData, int intStoreID, string strMainGroupListID, string strInventoryUser, string strReviewedUser,
            DateTime dtmInventoryDateFrom, DateTime dtmInventoryDateTo, DateTime dtmLockStockTimeFrom, DateTime dtmLockStockTimeTo , int intIsDeleted, int intIsReviewed, string strKeyword, int intType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_QuickInventory().SearchData(ref dtbQuickInventoryData, intStoreID, strMainGroupListID, strInventoryUser, strReviewedUser, dtmInventoryDateFrom, dtmInventoryDateTo, dtmLockStockTimeFrom, dtmLockStockTimeTo, objToken.UserName, intIsDeleted, intIsReviewed, strKeyword, intType);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu chi tiết phiếu kiểm kê nhanh", MessageName = "GetDetail")]
        public Library.WebCore.ResultMessage GetDetail(String strAuthenData, String strGUID, ref DataTable dtbDetail, string strQuickInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_QuickInventoryDetail().SearchData(ref dtbDetail, strQuickInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách mã phiếu kiểm kê nhanh - nhà sản xuất", MessageName = "GetListBrandID")]
        public Library.WebCore.ResultMessage GetListBrandID(String strAuthenData, String strGUID, ref DataTable dtbBrand, string strQuickInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_QuickInventory().GetListBrandID(ref dtbBrand, strQuickInventoryID);
            return objResultMessage;
        }
        
        #region 30/11/2017 Đăng bổ sung

        [WebMethod(EnableSession = true, Description = "Lấy dữ liệu tồn kho viettel", MessageName = "GetInstockData1")]
        public Library.WebCore.ResultMessage GetInstockData1(String strAuthenData, String strGUID, ref DataTable dtbInStockData, ref DataTable dtbInStockIMEI, DateTime dtmLockStockTime, int intStoreID, string strMainGroupIDList,
            string strSubGroupIDList, string strBrandIDList, int intIsNew, int intIsCheckRealInput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_QuickInventory().GetInstockData1(ref dtbInStockData, ref dtbInStockIMEI, dtmLockStockTime, intStoreID, strMainGroupIDList, strSubGroupIDList, strBrandIDList, intIsNew, intIsCheckRealInput);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Thêm mới phiếu kiểm kê nhanh viettel", MessageName = "Insert1")]
        public Library.WebCore.ResultMessage Insert1(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.QuickInventory objQuickInventory, ref string strQuickInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_QuickInventory objDA_QuickInventory = new ERP.Inventory.DA.Inventory.DA_QuickInventory();
            objDA_QuickInventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_QuickInventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_QuickInventory.objLogObject.LoginLogID = objToken.LoginLogID;
            objQuickInventory.CreatedUser = objToken.UserName;
            objQuickInventory.IsReviewed = true;
            objQuickInventory.InventoryUser = objToken.UserName;
            objQuickInventory.ReviewedUser = objToken.UserName;
            objResultMessage = objDA_QuickInventory.Insert1(objQuickInventory, ref strQuickInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật phiếu kiểm kê nhanh viettel", MessageName = "Update1")]
        public Library.WebCore.ResultMessage Update1(String strAuthenData, String strGUID, ERP.Inventory.BO.Inventory.QuickInventory objQuickInventory, List<ERP.Inventory.BO.Inventory.QuickInventoryDetail> DeleteList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.Inventory.DA_QuickInventory objDA_QuickInventory = new ERP.Inventory.DA.Inventory.DA_QuickInventory();
            objDA_QuickInventory.objLogObject.CertificateString = objToken.CertificateString;
            objDA_QuickInventory.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_QuickInventory.objLogObject.LoginLogID = objToken.LoginLogID;
            objQuickInventory.UpdatedUser = objToken.UserName;

            objResultMessage = objDA_QuickInventory.Update1(objQuickInventory, DeleteList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách mã phiếu kiểm kê nhanh - ngành hàng", MessageName = "GetListMaingroupID")]
        public Library.WebCore.ResultMessage GetListMaingroupID(String strAuthenData, String strGUID, ref DataTable dtbMainGroup, string strQuickInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_QuickInventory().GetListMainGroupID(ref dtbMainGroup, strQuickInventoryID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Nạp danh sách mã phiếu kiểm kê nhanh - nhóm hàng", MessageName = "GetListSubGroupID")]
        public Library.WebCore.ResultMessage GetListSubGroupID(String strAuthenData, String strGUID, ref DataTable dtbSubGroup, string strQuickInventoryID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Inventory.DA_QuickInventory().GetListSubGroupID(ref dtbSubGroup, strQuickInventoryID);
            return objResultMessage;
        }
        #endregion

    }
}
