﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Inventory.SM
{
    /// <summary>
    /// Summary description for WSIMEISalesInfo
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSSequenceIMEI : System.Web.Services.WebService
    {
        /// <summary>
        /// Cập nhật STT cho IMEI
        /// PM_SequenceIMEI_UPD_ORDIDX
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ref List<ERP.Inventory.BO.SM.SequenceIMEI> lstSequenceIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.SM.DA_SequenceIMEI().Insert (ref lstSequenceIMEI);
            return objResultMessage;
        }

        /// <summary>
        /// Kiểm tra imei tồn kho
        /// PM_SequenceIMEI_UPD_ORDIDX
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckExist(String strAuthenData, String strGUID, ref bool bolIsResult, string strIMEI, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_CurrentInStockDetail().CheckExist(ref bolIsResult, strIMEI, strProductID);
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm imei tồn kho
        /// PM_SequenceIMEI_UPD_ORDIDX
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.SM.DA_SequenceIMEI().SearchData(ref dtbResult, objKeywords);
            return objResultMessage;
        }
    }
}
