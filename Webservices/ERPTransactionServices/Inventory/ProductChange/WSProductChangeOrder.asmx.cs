﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Inventory.ProductChange
{
    /// <summary>
    /// Summary description for WSProductChangeOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSProductChangeOrder : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.ProductChange.ProductChangeOrder objProductChangeOrder, string strProductChangeOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder().LoadAllInfo(ref objProductChangeOrder, strProductChangeOrderID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "ProductChangeOrder";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertProductChangeOrder(String strAuthenData, String strGUID, ERP.Inventory.BO.ProductChange.ProductChangeOrder objProductChangeOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder objDA_ProductChangeOrder = new ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder();
            objDA_ProductChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ProductChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ProductChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objProductChangeOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_ProductChangeOrder.Insert(objProductChangeOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage ApproveProductChangeOrder(String strAuthenData, String strGUID, string productChangeOrderId, int reviewLevelId, string userName, bool unApprove, string Reason)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder objDA_ProductChangeOrder = new ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder();
            objResultMessage = objDA_ProductChangeOrder.Approve(productChangeOrderId, reviewLevelId, userName, unApprove, Reason);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateProductChangeOrder(String strAuthenData, String strGUID, ERP.Inventory.BO.ProductChange.ProductChangeOrder objProductChangeOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder objDA_ProductChangeOrder = new ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder();
            objDA_ProductChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ProductChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ProductChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objProductChangeOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_ProductChangeOrder.Update(objProductChangeOrder);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteProductChangeOrder(String strAuthenData, String strGUID, ERP.Inventory.BO.ProductChange.ProductChangeOrder objProductChangeOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder objDA_ProductChangeOrder = new ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder();
            objDA_ProductChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ProductChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ProductChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objProductChangeOrder.DeletedUser = objToken.UserName;
            objResultMessage = objDA_ProductChangeOrder.Delete(objProductChangeOrder);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckExistIMEI(String strAuthenData, String strGUID, ref string strIMEIExist, List<ERP.Inventory.BO.ProductChange.ProductChangeOrderDT> listProductChangeOrderDT)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            
            foreach (var item in listProductChangeOrderDT)
            {
                bool bolResult = false;
                objResultMessage = new ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder().CheckExistIMEI(ref bolResult, item.IMEI_IN,"");
                if (bolResult)
                {
                    strIMEIExist = item.IMEI_IN;
                    return objResultMessage;
                }
            }            
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckExistIMEISingle(String strAuthenData, String strGUID, ref string strIMEIExist, string IMEI, string ProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            
            bool bolResult = false;
            objResultMessage = new ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder().CheckExistIMEI(ref bolResult, IMEI, ProductID);
            if (bolResult)
            {
                strIMEIExist = IMEI;
                return objResultMessage;
            }
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Nạp danh sách tập tin đính kèm của yêu cầu nhập đổi/trả hàng", MessageName = "GetAttachment")]
        public Library.WebCore.ResultMessage GetAttachment(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.ProductChange.DA_ProductChangeOrder().GetAttachment(ref dtResult, objKeywords);
            return objResultMessage;
        }


    }
}
