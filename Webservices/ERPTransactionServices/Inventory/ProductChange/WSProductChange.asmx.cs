﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.ProductChange
{
    /// <summary>
    /// Summary description for WSProductChange
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSProductChange : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true, Description = "Lấy thông tin tồn kho", MessageName = "GetListStoreInStock")]
        public Library.WebCore.ResultMessage GetListStoreInStock(String strAuthenData, String strGUID, ref DataTable dtbStoreInStock, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.ProductChange.DA_ProductChange().GetListStoreInStock(ref dtbStoreInStock, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin đổi hàng", MessageName = "LoadInfo")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.ProductChange.ProductChange objProductChange, string strInputOrOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.ProductChange.DA_ProductChange().LoadInfoByInputOrOutputVoucherID(ref objProductChange,strInputOrOutputVoucherID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true, Description = "Xuất đổi hàng", MessageName = "InsertMasterAndDetail")]
        public Library.WebCore.ResultMessage InsertMasterAndDetail(String strAuthenData, String strGUID, ERP.Inventory.BO.ProductChange.ProductChange objProductChange)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.ProductChange.DA_ProductChange objDA_ProductChange = new ERP.Inventory.DA.ProductChange.DA_ProductChange();
            objDA_ProductChange.objLogObject.CertificateString = objToken.CertificateString;
            objDA_ProductChange.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_ProductChange.objLogObject.LoginLogID = objToken.LoginLogID;
            objProductChange.CreatedUser = objToken.UserName;

            objResultMessage = objDA_ProductChange.InsertMasterAndDetail(objProductChange);
            return objResultMessage;
        }
    }
}
