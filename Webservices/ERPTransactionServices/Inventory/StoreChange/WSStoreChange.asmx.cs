﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.StoreChange
{
    /// <summary>
    /// Summary description for WSStoreChange
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSStoreChange : System.Web.Services.WebService
    {
        /// <summary>
        /// Tìm kiếm phiếu nhập
        /// </summary>
        [WebMethod(EnableSession = true, Description = "Nạp thông tin phiếu xuất chuyển kho", MessageName = "PM_xuất_chuyển_kho")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.StoreChange objStoreChange, string strStoreChangeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().LoadInfo(ref objStoreChange, strStoreChangeID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true, Description = "Nạp thông tin phiếu xuất chuyển kho", MessageName = "PM_xuất_chuyển_kho_v1")]
        public Library.WebCore.ResultMessage LoadInfo_v1(String strAuthenData, String strGUID, ref ERP.Inventory.BO.StoreChange objStoreChange, string strStoreChangeID, string outputvoucherid , string intputvoucherid, DateTime storechangedate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().LoadInfo(ref objStoreChange, strStoreChangeID,outputvoucherid,intputvoucherid, storechangedate);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy mã phiếu xuất chuyển kho
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreChangeNewID(String strAuthenData, String strGUID, ref string strStoreChangeID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().GetStoreChangeNewID(ref strStoreChangeID, intStoreID);
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm phiếu xuất chuyển kho
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbStoreChange, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().SearchData(ref dtbStoreChange, objKeywords);
            return objResultMessage;
        }

        /// <summary>
        /// 
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadStoreChangeDetail(String strAuthenData, String strGUID, ref DataTable dtbStoreChangeDetail, string strStoreChangeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().LoadStoreChangeDetail(ref dtbStoreChangeDetail, strStoreChangeID);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy mã phiếu xuất chuyển kho
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertStoreChange(String strAuthenData, String strGUID,
            DataTable dtbStoreChangeDetail, ERP.Inventory.BO.StoreChange objStoreChangeBO, ref string strStoreChangeID, ref string strOutPutVoucherID, int intStoreChangeOrderTypeID = 0, int intCreateVATInvoice = 0)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_StoreChange objDA_StoreChange = new DA_StoreChange();
            objDA_StoreChange.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChange.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChange.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChangeBO.CreatedUser = objToken.UserName;
            objDA_StoreChange.InsertStoreChange(ref objResultMessage, dtbStoreChangeDetail, objStoreChangeBO, ref strStoreChangeID, ref strOutPutVoucherID, intStoreChangeOrderTypeID, intCreateVATInvoice);
            return objResultMessage;
        }

        /// <summary>
        /// Cập nhật phiếu chuyển kho
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateStoreChange(String strAuthenData, String strGUID, ERP.Inventory.BO.StoreChange objStoreChangeBO, bool bolIsUpdateAll)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_StoreChange objDA_StoreChange = new DA_StoreChange();
            objDA_StoreChange.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChange.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChange.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChangeBO.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_StoreChange.UpdateStoreChange(objStoreChangeBO, bolIsUpdateAll);
            return objResultMessage;
        }

        /// <summary>
        /// Update chuyển kho list
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateStoreChangeList(String strAuthenData, String strGUID, List<ERP.Inventory.BO.StoreChange> objStoreChangeList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            objResultMessage = new DA_StoreChange().UpdateStoreChangeList(objToken.UserName, objStoreChangeList);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy thông tin hàng hóa có kích thước tối đa của phiếu chuyển kho
        /// PM_StoreChange_TransferedList
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetProductInfoMaxSize(String strAuthenData, String strGUID, ref DataTable dtbResult, string strStoreChangeIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().GetProductInfoMaxSize(ref dtbResult, strStoreChangeIDList);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy thông tin tồn kho chính và kho trưng bày
        /// PM_STORECHANGE_GETINSTOCK
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreChange_GetInstock(String strAuthenData, String strGUID, ref DataSet dsResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().GetStoreChange_GetInstock(ref dsResult, objKeywords);
            return objResultMessage;
        }


        /// <summary>
        /// Lấy thông tin tồn kho chính mà không có ở kho trưng bày
        /// PM_STORECHANGE_GETINSTOCK_1
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreChange_GetInstock_1(String strAuthenData, String strGUID, ref DataTable dtResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().GetStoreChange_GetInstock_1(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreChange_BySCOID(String strAuthenData, String strGUID, ref DataTable dtResult, string strStoreChangeOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().GetStoreChange_BySCOID(ref dtResult, strStoreChangeOrderID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreChangeOrder_GetCode(String strAuthenData, String strGUID, ref DataTable dtResult, string strStoreChangeOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().GetStoreChangeOrder_GetCode(ref dtResult, strStoreChangeOrderID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreChangeRPT_GetInfo(String strAuthenData, String strGUID, ref DataTable dtResult, string strStoreChangeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().GetStoreChangeRPT_GetInfo(ref dtResult, strStoreChangeID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreChangeRPT_GetData(String strAuthenData, String strGUID, ref DataTable dtResult, string strStoreChangeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().GetStoreChangeRPT_GetData(ref dtResult, strStoreChangeID);
            return objResultMessage;
        }
        /// <summary>
        /// Nạp chi tiết phiếu xuất kiêm vận chuyển nội bộ
        /// PM_Rpt_OutputVoucherTransferRpt
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Report_GetOutputVoucherTranferData(String strAuthenData, String strGUID, ref DataTable dtbResult, string strStoreChangeID)
        {


            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_StoreChange().Report_GetOutputVoucherTranferData(ref dtbResult, strStoreChangeID, objToken.UserName);
            return objResultMessage;
        }

        //Đăng bổ sung
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetFormTypebyStoreChangeOrderType(String strAuthenData, String strGUID, ref DataTable dtbFormType, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChange().GetFormTypebyStoreChangeOrderType(ref dtbFormType, objKeywords);
            if (!objResultMessage.IsError)
            {
                dtbFormType.TableName = "dtbFormType";
            }
            return objResultMessage;
        }

        //Nam bổ sung
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertConfigPrint(String strAuthenData, String strGUID, StoredChange_CurrentVoucher objStoredChange_CurrentVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoredChange_CurrentVoucher objDA_StoredChange_CurrentVoucher = new ERP.Inventory.DA.DA_StoredChange_CurrentVoucher();
            objDA_StoredChange_CurrentVoucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoredChange_CurrentVoucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoredChange_CurrentVoucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoredChange_CurrentVoucher.UserName = objToken.UserName;
            objResultMessage = objDA_StoredChange_CurrentVoucher.Insert(objStoredChange_CurrentVoucher);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateConfigPrint(String strAuthenData, String strGUID, StoredChange_CurrentVoucher objStoredChange_CurrentVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoredChange_CurrentVoucher objDA_StoredChange_CurrentVoucher = new ERP.Inventory.DA.DA_StoredChange_CurrentVoucher();
            objDA_StoredChange_CurrentVoucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoredChange_CurrentVoucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoredChange_CurrentVoucher.objLogObject.LoginLogID = objToken.LoginLogID;
           // objStoredChange_CurrentVoucher.UserName = objToken.UserName;
            objResultMessage = objDA_StoredChange_CurrentVoucher.Update(objStoredChange_CurrentVoucher);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadConfigPrint(String strAuthenData, String strGUID, int intStoredId, ref StoredChange_CurrentVoucher objStoredChange_CurrentVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoredChange_CurrentVoucher().LoadInfo(ref objStoredChange_CurrentVoucher, intStoredId, objToken.UserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateStoreChange_Voucher(String strAuthenData, String strGUID, StoreChange_CurrentVoucherUpdate objStoreChange_CurrentVoucherUpdate, ref bool bolIsDuplicate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoredChange_CurrentVoucher objDA_StoredChange_CurrentVoucher = new ERP.Inventory.DA.DA_StoredChange_CurrentVoucher();
            objDA_StoredChange_CurrentVoucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoredChange_CurrentVoucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoredChange_CurrentVoucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChange_CurrentVoucherUpdate.UserName = objToken.UserName;
            objResultMessage = objDA_StoredChange_CurrentVoucher.UpdateVoucher(objStoreChange_CurrentVoucherUpdate, ref bolIsDuplicate);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetHistoryPrint(String strAuthenData, String strGUID, ref DataTable dtbResult, string strStoreChangeId)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoredChange_CurrentVoucher().GetHistoryPrint(ref dtbResult, strStoreChangeId);
            if (dtbResult != null)
                dtbResult.TableName = "dtbHistoryPrint";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataConfigPrint(String strAuthenData, String strGUID, ref DataTable dtbConfigPrint, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoredChange_CurrentVoucher().SearchData(ref dtbConfigPrint, objKeywords);
            if (dtbConfigPrint != null)
                dtbConfigPrint.TableName = "dtbConfigPrint";
            return objResultMessage;
        }

        /// <summary>
        /// Lấy mã phiếu xuất chuyển kho tao hoa don duoi server
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertStoreChangeNew(String strAuthenData, String strGUID,
            DataTable dtbStoreChangeDetail, ERP.Inventory.BO.StoreChange objStoreChangeBO, ref string strStoreChangeID, ref string strOutPutVoucherID, ref List<string> lstStoreChangeID, ref List<string> lstVATInvoiceID, int intStoreChangeOrderTypeID = 0, int intCreateVATInvoice = 0, int intPrepareVAT = 15)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_StoreChange objDA_StoreChange = new DA_StoreChange();
            objDA_StoreChange.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChange.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChange.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChangeBO.CreatedUser = objToken.UserName;
            //if (intStoreChangeOrderTypeID == 0 || intCreateVATInvoice == 0)
            //{
            //    objDA_StoreChange.InsertStoreChange(ref objResultMessage, dtbStoreChangeDetail, objStoreChangeBO, ref strStoreChangeID, ref strOutPutVoucherID, intStoreChangeOrderTypeID, intCreateVATInvoice);
            //    lstStoreChangeID = new List<string>();
            //    lstStoreChangeID.Add(strStoreChangeID);
            //}               
            //else
            objDA_StoreChange.InsertStoreChangeNew(ref objResultMessage, dtbStoreChangeDetail, objStoreChangeBO, ref strStoreChangeID, ref strOutPutVoucherID, ref lstStoreChangeID, ref lstVATInvoiceID, intStoreChangeOrderTypeID, intCreateVATInvoice, intPrepareVAT);
            return objResultMessage;
        }
    }
}
