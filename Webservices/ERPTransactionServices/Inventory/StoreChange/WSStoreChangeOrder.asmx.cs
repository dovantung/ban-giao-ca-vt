﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.StoreChange
{
    /// <summary>
    /// Summary description for WSStoreChangeOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSStoreChangeOrder : System.Web.Services.WebService
    {
        /// <summary>
        /// Tìm kiếm yêu cầu chuyển kho
        /// </summary>
        [WebMethod(EnableSession = true, Description = "Tìm kiếm yêu cầu chuyển kho", MessageName = "PM_STORECHANGEORDER_SRH")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChangeOrder().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }
        /// <summary>
        /// Tìm kiếm yêu cầu chuyển kho
        /// </summary>
        [WebMethod(EnableSession = true, Description = "Tìm kiếm yêu cầu chuyển kho", MessageName = "PM_STORECHANGEORTHERORDER_SRH")]
        public Library.WebCore.ResultMessage SearchDataOtherTransfer(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChangeOrder().SearchDataOtherTransfer(ref dtResult, objKeywords);
            return objResultMessage;
        }

        /// <summary>
        /// Load info
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objStoreChangeOrder"></param>
        /// <param name="strStoreChangeOrderID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref StoreChangeOrder objStoreChangeOrder, string strStoreChangeOrderID, int intStoreChangeOrderTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChangeOrder().LoadInfo(ref objStoreChangeOrder, strStoreChangeOrderID, intStoreChangeOrderTypeID);
            return objResultMessage;
        }


        /// <summary>
        /// Lấy mã yêu cầu chuyển kho
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="strStoreChangeOrderID"> out: ma hop dong tra ve  </param>
        /// <param name="intStoreID"> Default store ID</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreChangeOrderNewID(String strAuthenData, String strGUID, ref string strStoreChangeOrderID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_StoreChangeOrder().GetStoreChangeOrderNewID(ref strStoreChangeOrderID, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, StoreChangeOrder objStoreChangeOrder, ref string strStoreChangeOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objDA_StoreChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChangeOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_StoreChangeOrder.Insert(objStoreChangeOrder, ref strStoreChangeOrderID);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        /// <summary>
        /// Insert StoreChangeOrderAttachment
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "Insert StoreChangeOrderAttachment", MessageName = "PM_STORECHANGEORDER_ATM_ADD")]
        public Library.WebCore.ResultMessage InsertStoreChangeOrderAttachment(String strAuthenData, String strGUID,StoreChangeOrderAttachment objStoreChangeOrderAttachment) {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null) return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrderAttachment objDA_StoreChangeOrderAttachment = new ERP.Inventory.DA.DA_StoreChangeOrderAttachment();
            objResultMessage = objDA_StoreChangeOrderAttachment.Insert(objStoreChangeOrderAttachment);
            return objResultMessage;
        }



        /// <summary>
        /// check tồn kho
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "ReturnInstock", MessageName = "PM_STORECHANGEINSTOCKCHECK")]
        public bool ReturnInstock(String strAuthenData, String strGUID, StoreChangeOrder StoreChangeOrder)
        {
            bool bolResult = false;
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null) return false;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder= new ERP.Inventory.DA.DA_StoreChangeOrder();
            bolResult = objDA_StoreChangeOrder.ReturnInstock(StoreChangeOrder);
            return bolResult;
        }
 
        /// <summary>
        /// Insert StoreChangeOrderAttachment
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "Update StoreChangeOrderAttachment", MessageName = "PM_STORECHANGEORDER_ATM_UPD")]
        public Library.WebCore.ResultMessage UpdateStoreChangeOrderAttachment(String strAuthenData, String strGUID, StoreChangeOrderAttachment objStoreChangeOrderAttachment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null) return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrderAttachment objDA_StoreChangeOrderAttachment = new ERP.Inventory.DA.DA_StoreChangeOrderAttachment();
            objResultMessage = objDA_StoreChangeOrderAttachment.Update(objStoreChangeOrderAttachment);
            return objResultMessage;
        }


        /// <summary>
        /// Created by: Nguyễn Linh Tuấn
        /// Tạo yêu cầu chuyển kho từ lệnh chuyển kho
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objStoreChangeOrder"></param>
        /// <param name="tblStoreChangeCommand"></param>
        /// <param name="tblFromStore"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateStoreChangeOrder(String strAuthenData, String strGUID, StoreChangeOrder objStoreChangeOrder, DataTable tblStoreChangeCommand, DataTable tblFromStore)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objDA_StoreChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChangeOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_StoreChangeOrder.CreateStoreChangeOrder(objStoreChangeOrder, tblStoreChangeCommand, tblFromStore);
            return objResultMessage;
        }

        /// <summary>
        /// Created by: Nguyễn Linh Tuấn
        /// Tạo yêu cầu chuyển kho từ đảo hàng bày mẫu
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objStoreChangeOrder"></param>
        /// <param name="tblStoreChangeCommand"></param>
        /// <param name="tblFromStore"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateStoreChangeOrderShowProduct(String strAuthenData, String strGUID, StoreChangeOrder objStoreChangeOrder, DataTable tblStoreChangeDetail)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objDA_StoreChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChangeOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_StoreChangeOrder.CreateStoreChangeOrderShowProduct(objStoreChangeOrder, tblStoreChangeDetail);
            return objResultMessage;
        }

        /// <summary>
        /// Created by: Nguyễn Linh Tuấn
        /// Tạo yêu cầu chuyển kho từ kho chính sang kho bày mẫu
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objStoreChangeOrder"></param>
        /// <param name="tblStoreChangeCommand"></param>
        /// <param name="tblFromStore"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateStoreChangeOrderShowProduct_1(String strAuthenData, String strGUID, StoreChangeOrder objStoreChangeOrder, DataTable tblStoreChangeDetail)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objDA_StoreChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChangeOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_StoreChangeOrder.CreateStoreChangeOrderShowProduct_1(objStoreChangeOrder, tblStoreChangeDetail);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, StoreChangeOrder objStoreChangeOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_StoreChangeOrder objDA_StoreChangeOrder = new DA_StoreChangeOrder();
            objDA_StoreChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChangeOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_StoreChangeOrder.Update(objStoreChangeOrder);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, StoreChangeOrder objStoreChangeOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objDA_StoreChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objStoreChangeOrder.DeletedUser = objToken.UserName;
            objResultMessage = objDA_StoreChangeOrder.Delete(objStoreChangeOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteList(String strAuthenData, String strGUID, List<StoreChangeOrder> lstStoreChangeOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objDA_StoreChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            // lstStoreChangeOrder.DeletedUser = objToken.UserName;
            objResultMessage = objDA_StoreChangeOrder.DeleteList(lstStoreChangeOrder, objToken.UserName);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateMainGroupMenuItem(String strAuthenData, String strGUID, string strStoreChangeOrderID, ref DataTable dtbResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objResultMessage = objDA_StoreChangeOrder.CreateMainGroupMenuItem(ref dtbResult, strStoreChangeOrderID, objToken.UserName);
            return objResultMessage;
        }
        /// <summary>
        /// load danh sach chi tiet phieu y.c ck
        /// PM_SCO_DETAIL_LSC
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="strStoreChangeOrderID"></param>
        /// <param name="dtbResult"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDetailForStoreChange(String strAuthenData, String strGUID, string strMainGroupIDList, string strStoreChangeOrderID,int intInputTypeID,int intOutputTypeID, ref DataTable dtbResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objResultMessage = objDA_StoreChangeOrder.LoadDetailForStoreChangeByList(ref dtbResult, strMainGroupIDList, strStoreChangeOrderID,intInputTypeID,intOutputTypeID,objToken.UserName);
            return objResultMessage;
        }

        /// <summary>
        /// load danh sach chi tiet  y.c ck từ lệnh chuyển kho
        /// PM_STORECHANGEORDER_IDFRCMD
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="strStoreChangeOrderID"></param>
        /// <param name="dtbResult"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadStoreChangeOrderDetailFromCommand(String strAuthenData, String strGUID, string strStoreChangeCommandIDList, ref DataTable dtbResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objResultMessage = objDA_StoreChangeOrder.LoadStoreChangeOrderDetailFromCommand(ref dtbResult, strStoreChangeCommandIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage ReviewStoreChangeOrder(String strAuthenData, String strGUID, string strStoreChangeOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objDA_StoreChangeOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_StoreChangeOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_StoreChangeOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_StoreChangeOrder.ReviewStoreChangeOrder(strStoreChangeOrderID, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckStorePermission(String strAuthenData, String strGUID, int intFromStoreID, int intToStoreID, string strUserName, ref bool bolFromStorePermission, ref bool bolToStorePermission)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objResultMessage = objDA_StoreChangeOrder.CheckStorePermission(intFromStoreID, intToStoreID, strUserName, ref bolFromStorePermission, ref bolToStorePermission);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadMainGroupPermission(String strAuthenData, String strGUID, string strUserName, ref DataTable dtbData)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objResultMessage = objDA_StoreChangeOrder.LoadMainGroupPermission(strUserName, ref dtbData);
            return objResultMessage;
        }

        /// <summary>
        /// Nguyen Van Tai Bo sung kiem tra thông tin xuất tất cả ngành hàng trên yêu cầu xuất chuyển kho
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="strUserName"></param>
        /// <param name="dtbData"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetOutputAll(String strAuthenData, String strGUID, ref DataTable dtbData, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_StoreChangeOrder objDA_StoreChangeOrder = new ERP.Inventory.DA.DA_StoreChangeOrder();
            objResultMessage = objDA_StoreChangeOrder.GetOutputAll(ref dtbData, objKeywords);
            return objResultMessage;
        }
    }
}
