﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Inventory.IMEISales
{
    /// <summary>
    /// Summary description for WSIMEISalesInfo
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSIMEISalesInfo : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.IMEISalesInfo objIMEISalesInfo_Comment, string strIMEI, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo().LoadInfo(ref objIMEISalesInfo_Comment, strIMEI, strProductID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Inventory.BO.IMEISalesInfo objIMEISalesInfo_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_IMEISalesInfo objDA_IMEISalesInfo_Comment_Comment = new ERP.Inventory.DA.DA_IMEISalesInfo();
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            objIMEISalesInfo_Comment.CreatedUser = objToken.UserName;
            objResultMessage = objDA_IMEISalesInfo_Comment_Comment.Insert(objIMEISalesInfo_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.IMEISalesInfo objIMEISalesInfo_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_IMEISalesInfo objDA_IMEISalesInfo_Comment_Comment = new ERP.Inventory.DA.DA_IMEISalesInfo();
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_IMEISalesInfo_Comment_Comment.Update(objIMEISalesInfo_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Detete(ref int intResult, String strAuthenData, String strGUID, ERP.Inventory.BO.IMEISalesInfo objIMEISalesInfo_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_IMEISalesInfo objDA_IMEISalesInfo_Comment_Comment = new ERP.Inventory.DA.DA_IMEISalesInfo();
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_IMEISalesInfo_Comment_Comment.Delete(objIMEISalesInfo_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataProSpec(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo_ProSpec().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSpec(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo_ProSpec().SearchDataSpec(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSpecStatus(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo_ProSpec().SearchDataSpecStatus(ref dtResult, objKeywords);
            //string str = System.Configuration.ConfigurationManager.AppSettings["HostAddress"].ToString();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Review(String strAuthenData, String strGUID, List<ERP.Inventory.BO.IMEISalesInfo> lstIMEISalesInfo)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_IMEISalesInfo objDA_IMEISalesInfo_Comment_Comment = new ERP.Inventory.DA.DA_IMEISalesInfo();
            objResultMessage = objDA_IMEISalesInfo_Comment_Comment.Review(lstIMEISalesInfo);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData_IMEISalesInfo_ProSpec_ToList(String strAuthenData, String strGUID, ref List<ERP.Inventory.BO.IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo_ProSpec().SearchDataToList(ref lstIMEISalesInfo_ProSpec, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckIMEI(String strAuthenData, String strGUID, ref DataTable dtResult, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo().CheckIMEI(ref dtResult, strIMEI);
            //string str = System.Configuration.ConfigurationManager.AppSettings["HostAddress"].ToString();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataIMEIOrLotIMEI(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo().SearchDataIMEIOrLotIMEI(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoComment(String strAuthenData, String strGUID, ref ERP.Inventory.BO.IMEISalesInfo_Comment objIMEISalesInfo_Comment, int intCommentID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo_Comment().LoadInfo(ref objIMEISalesInfo_Comment, intCommentID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateOrderIndex(String strAuthenData, String strGUID, DataTable dtbResource)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_IMEISalesInfo objDA_IMEISalesInfo_Comment_Comment = new ERP.Inventory.DA.DA_IMEISalesInfo();
            objResultMessage = objDA_IMEISalesInfo_Comment_Comment.UpdateOrderIndex(dtbResource);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertComment(String strAuthenData, String strGUID, ERP.Inventory.BO.IMEISalesInfo_Comment objIMEISalesInfo_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_IMEISalesInfo_Comment objDA_IMEISalesInfo_Comment_Comment = new ERP.Inventory.DA.DA_IMEISalesInfo_Comment();
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_IMEISalesInfo_Comment_Comment.Insert(objIMEISalesInfo_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateComment(String strAuthenData, String strGUID, ERP.Inventory.BO.IMEISalesInfo_Comment objIMEISalesInfo_Comment, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_IMEISalesInfo_Comment objDA_IMEISalesInfo_Comment_Comment = new ERP.Inventory.DA.DA_IMEISalesInfo_Comment();
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_IMEISalesInfo_Comment_Comment.Update(objIMEISalesInfo_Comment, lstOrderIndex);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeteteComment(ref int intResult, String strAuthenData, String strGUID, List<ERP.Inventory.BO.IMEISalesInfo_Comment> lstIMEISalesInfo_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_IMEISalesInfo_Comment objDA_IMEISalesInfo_Comment_Comment = new ERP.Inventory.DA.DA_IMEISalesInfo_Comment();
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEISalesInfo_Comment_Comment.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_IMEISalesInfo_Comment_Comment.Delete(lstIMEISalesInfo_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataComment(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_IMEISalesInfo_Comment().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }
    }
}
