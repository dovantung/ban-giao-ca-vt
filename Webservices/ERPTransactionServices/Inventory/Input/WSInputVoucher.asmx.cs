﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.Input
{
    /// <summary>
    /// Summary description for WSInputVoucher
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSInputVoucher : System.Web.Services.WebService
    {
        /// <summary>
        /// Tìm kiếm phiếu nhập
        /// </summary>
        [WebMethod(EnableSession = true, Description = "Tìm kiếm phiếu nhập", MessageName = "INPUT_VOUCHER_SEARCH")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucher().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        /// <summary>
        /// Load info
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objInputVoucher"></param>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref InputVoucher objInputVoucher, string strInputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucher().LoadInfo(ref objInputVoucher, strInputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetListAttachmentByInputVoucherID(String strAuthenData, String strGUID, ref List<ERP.Inventory.BO.InputVoucher_Attachment> objInputVoucher_AttachmentList, string strInputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucher_Attachment().GetListByInputVoucherID(ref objInputVoucher_AttachmentList, strInputVoucherID);
            return objResultMessage;
        }
        /// <summary>
        /// Lấy mã phiếu nhập
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="strInputVoucherID"></param>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetInputVoucherNewID(String strAuthenData, String strGUID, ref string strInputVoucherID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucher().GetInputVoucherNewID(ref strInputVoucherID, intStoreID);
            return objResultMessage;
        }


        /// <summary>
        /// Kiểm tra phiếu nhập đã có hóa đơn hay chưa
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="strInputVoucherID"></param>
        /// <param name="bolIsHasVATInvoice"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckIsHasVatInvoice(String strAuthenData, String strGUID, string strInputVoucherID, ref bool bolIsHasVATInvoice)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucher().CheckIsHasVatInvoice(strInputVoucherID, ref bolIsHasVATInvoice);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy mã phiếu chi
        /// CM_VOUCHER_NEWID
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetVoucherNewID(String strAuthenData, String strGUID, ref string strVoucherID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.SalesAndServices.Payment.DA.DA_Voucher().CreateVoucherID(ref strVoucherID, intStoreID);
            return objResultMessage;
        }

        /// <summary>
        /// Tạo IMEI phát sinh tự động
        /// PM_INPUTVOUCHER_GENAUTOIMEI
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GenAutoIMEI(String strAuthenData, String strGUID, ref DataTable dtbResult, string strProductID, int intQuantity)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_InputVoucher().GenAutoImei(ref dtbResult, strProductID, intQuantity);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy thông tin phiếu nhập 
        /// PM_Rpt_InputVoucherReport
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Report_GetInputVoucherData(String strAuthenData, String strGUID, ref DataTable dtbResult, string strInputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_InputVoucher().Report_GetInputVoucherData(ref dtbResult, strInputVoucherID, objToken.UserName);
            return objResultMessage;
        }


        /// <summary>
        /// Nạp chi tiết phiếu nhập 
        /// PM_Rpt_InputVoucherDetailRpt
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Report_GetInputVoucherDetailData(String strAuthenData, String strGUID, ref DataTable dtbResult, string strInputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_InputVoucher().Report_GetInputVoucherDetailData(ref dtbResult, strInputVoucherID);
            return objResultMessage;
        }

        /// <summary>
        ///  Kiểm tra số lương IMEI cần thêm vào có vượt quá giớ hạn không
        /// PM_INPUTVOUCHER_GETMAXIMEI
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetMaxIMEI(String strAuthenData, String strGUID, ref int intMaxIMEI, string strProductID, int intQuantity)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_InputVoucher().GetMaxImei(ref intMaxIMEI, strProductID, intQuantity);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertInputVoucherAndVoucher(String strAuthenData, String strGUID, ref string strInputvoucherID, ERP.Inventory.BO.InputVoucher objInputVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();
            objDA_InputVoucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputVoucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputVoucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objInputVoucher.CreatedUser = objToken.UserName;
            objResultMessage = objDA_InputVoucher.InsertInputVoucherAndVoucher(objInputVoucher, ref strInputvoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, InputVoucher objInputVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();
            objDA_InputVoucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputVoucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputVoucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_InputVoucher.Update(objInputVoucher);
            return objResultMessage;
        }

        /// <summary>
        /// Cập nhật thông tin thực nhập của phiếu nhập
        /// PM_InputVcher_UpdChkRealInput
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="strInputVoucherID"></param>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateCheckRealInput(String strAuthenData, String strGUID, List<ERP.Inventory.BO.InputVoucher> objInputVoucherList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            string strUpdateUser = objToken.UserName;
            objResultMessage = new DA_InputVoucher().UpdateCheckRealInput(objInputVoucherList, strUpdateUser);
            return objResultMessage;
        }

        /// <summary>
        /// Hủy phiếu nhập
        /// PM_INPUTVOUCHER_DELETE
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="objInputVoucher"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Detete(ref int intResult, String strAuthenData, String strGUID, ERP.Inventory.BO.InputVoucher objInputVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();
            objInputVoucher.DeletedUser = objToken.UserName;
            objDA_InputVoucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputVoucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputVoucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_InputVoucher.Delete(ref intResult, objInputVoucher);
            return objResultMessage;
        }
        /// <summary>
        /// Tìm kiếm lịch sử IMEI
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataProductIMEIHistory(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucher().SearchDataProductIMEIHistory(ref dtResult, objKeywords);
            if (dtResult != null)
                dtResult.TableName = "DataProductIMEIHistory";
            return objResultMessage;
        }
        /// <summary>
        /// Tìm kiếm IMEI lock FIFO
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataProductIMEILockFIFO(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucher().SearchDataProductIMEILockFIFO(ref dtResult, objKeywords);
            if (dtResult != null)
                dtResult.TableName = "DataProductIMEILockFIFO";
            return objResultMessage;
        }
        /// <summary>
        /// Lấy ghi chú (Làm giá theo IMEI) theo chuỗi IMEI tìm kiếm 
        /// trường hợp có tồn tại IMEI 
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetNoteByIMEI(String strAuthenData, String strGUID, ref string strNote, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucher().GetNoteByIMEI(ref strNote, strIMEI);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy dữ liệu export cho đơn hàng bán
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetExportDataToSaleOrder(String strAuthenData, String strGUID, ref DataTable dtbResult, string strInputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_InputVoucher().GetExportDataToSaleOrder(ref dtbResult, strInputVoucherID);
            return objResultMessage;
        }

        /// <summary>
        /// Đăng Quy trình giao hàng thẳng tại siêu thị
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertData(String strAuthenData, String strGUID,
            InputVoucher objInputVoucher,
            InputVoucher objInputVoucherCenter,
            StoreChangeOrder objStoreChangeOrder,
            ERP.Inventory.BO.StoreChange objStoreChangeBO,
            ERP.PrintVAT.BO.PrintVAT.VAT_Invoice objVAT_Invoice,
            string strFormTypeBranchID,
            ERP.PrintVAT.BO.PInvoice.VAT_PInvoice objVAT_PInvoice,
            ref string strInputvoucherID,
            ref string strInputvoucherCenterID,
            ref string strStoreChangeID,
            ref string strStoreChangeOrderID,
            ref string strOutPutVoucherID,
            ref string strVATInvoiceID
            )
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();
            objDA_InputVoucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputVoucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputVoucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objInputVoucherCenter.CreatedUser = objToken.UserName;
            objInputVoucher.CreatedUser = objToken.UserName;
            objStoreChangeOrder.CreatedUser = objToken.UserName;
            objStoreChangeBO.CreatedUser = objToken.UserName;
            objResultMessage = objDA_InputVoucher.InsertData(objInputVoucher, objInputVoucherCenter, objStoreChangeOrder, objStoreChangeBO, objVAT_Invoice, strFormTypeBranchID, objVAT_PInvoice, ref strInputvoucherID, ref strInputvoucherCenterID, ref strStoreChangeID, ref strStoreChangeOrderID, ref strOutPutVoucherID, ref strVATInvoiceID);
            return objResultMessage;
        }



        /// <summary>
        /// Nạp dữ liệu IMEI cho phiếu nhập kho chi tiết
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="dtbData"></param>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetInfoDetailImei(String strAuthenData, String strGUID, ref DataTable dtbData, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_InputVoucherDetailIMEI().GetInfo(ref dtbData, objKeywords);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetListByInputVoucherID(String strAuthenData, String strGUID, ref List<ERP.Inventory.BO.InputVoucherDetail> objInputVoucherDetail, string strInputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objInputVoucherDetail = new DA_InputVoucherDetail().GetListByInputVoucherID(strInputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm danh sách thông tin phiếu nhập", MessageName = "SearchData2")]
        public Library.WebCore.ResultMessage SearchData2(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucher().SearchData2(ref dtbData, objKeywords);
            if (dtbData != null)
                dtbData.TableName = "InputVoucherTable";
            return objResultMessage;
        }
    }
}
