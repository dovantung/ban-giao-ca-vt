﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using ERP.Inventory.DA.Input;
using ERP.Inventory.BO.Input;


namespace ERPTransactionServices.Inventory.Input
{
    /// <summary>
    /// Summary description for WSInputVoucherReturn
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSInputVoucherReturn : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true, Description = "Lấy chi tiết phiếu xuất để nhập trả hàng", MessageName = "LoadOutputVoucherDetailForReturn")]
        public Library.WebCore.ResultMessage LoadOutputVoucherDetailForReturn(String strAuthenData, String strGUID, String strOutputVoucherID, bool bolIsReturnWithFee, bool bolIsAllowReturnAllProduct, ref DataTable dtResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucherReturn().LoadOutputVoucherDetailForReturn(strOutputVoucherID, bolIsReturnWithFee, bolIsAllowReturnAllProduct, ref dtResult);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy phiếu xuất bảo hành mở rộng của imei nhập trả", MessageName = "LoadOutputVoucherWarrantyExtend")]
        public Library.WebCore.ResultMessage LoadOutputVoucherWarrantyExtend(String strAuthenData, String strGUID, String strOutputVoucherID, String strIMEI, ref DataTable dtResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucherReturn().LoadOutputVoucherWarrantyExtend(strOutputVoucherID, strIMEI, ref dtResult);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Thêm phiếu nhập trả hàng và phiếu chi", MessageName = "InsertMasterAndDetailAndVoucher")]
        public Library.WebCore.ResultMessage InsertMasterAndDetailAndVoucher(String strAuthenData, String strGUID, InputVoucherReturn objInputVoucherReturn, ERP.SalesAndServices.Payment.BO.Voucher objVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_InputVoucherReturn objDA_InputVoucherReturn = new DA_InputVoucherReturn();
            objDA_InputVoucherReturn.objLogObject.CertificateString = objToken.CertificateString;
            objDA_InputVoucherReturn.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_InputVoucherReturn.objLogObject.LoginLogID = objToken.LoginLogID;
            objInputVoucherReturn.CreatedUser = objToken.UserName;

            objResultMessage = objDA_InputVoucherReturn.InsertMasterAndDetailAndVoucher(objInputVoucherReturn, objVoucher);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Kiểm tra phiếu nhập trả hàng", MessageName = "CheckInputVoucherReturnByOutputVoucher")]
        public Library.WebCore.ResultMessage CheckInputVoucherReturnByOutputVoucher(String strAuthenData, String strGUID, ref string strInputVoucherReturnID, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucherReturn().CheckInputVoucherReturnByOutputVoucher(ref strInputVoucherReturnID, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin cảnh báo đơn hàng thanh toán thẻ", MessageName = "GetVoucherAlert")]
        public Library.WebCore.ResultMessage GetVoucherAlert(String strAuthenData, String strGUID, String strSaleOrderID, ref string strVoucherAlert)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_InputVoucherReturn().GetVoucherAlert(strSaleOrderID, ref strVoucherAlert);
            return objResultMessage;
        }

    }
}
