﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.SplitProduct
{
    /// <summary>
    /// Summary description for WSSplitProduct
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSSplitProduct : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.SplitProduct objSplitProduct, string strSplitProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SplitProduct().LoadInfo(ref objSplitProduct, strSplitProductID);
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm phiếu tách linh kiện
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbSplitProduct, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_SplitProduct().SearchData(ref dtbSplitProduct, objKeywords);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy mã phiếu tách linh kiện
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertData(String strAuthenData, String strGUID, ERP.Inventory.BO.SplitProduct objSplitProductBO)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_SplitProduct objDA_SplitProduct = new DA_SplitProduct();
            objDA_SplitProduct.objLogObject.CertificateString = objToken.CertificateString;
            objDA_SplitProduct.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_SplitProduct.objLogObject.LoginLogID = objToken.LoginLogID;
            objSplitProductBO.CreatedUser = objToken.UserName;
            objResultMessage = objDA_SplitProduct.InsertData(objSplitProductBO);
            return objResultMessage;
        }
    }
}
