﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO.BGT;
using ERP.Inventory.DA.BGT;
using System.Data;

namespace ERPTransactionServices.Inventory.BGT
{
    /// <summary>
    /// Summary description for WSBeginTermInStock
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSBeginTermInStock : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref BeginTermInStock objBeginTermInStock, DateTime? dtmBeginTermDate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_BeginTermInStock objDA_BeginTermInStock = new DA_BeginTermInStock();
            objResultMessage = objDA_BeginTermInStock.LoadInfo(ref objBeginTermInStock, dtmBeginTermDate);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_BeginTermInStock objDA_BeginTermInStock = new DA_BeginTermInStock();
            objResultMessage = objDA_BeginTermInStock.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "SearchDataList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, BeginTermInStock objBeginTermInStock)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_BeginTermInStock objDA_BeginTermInStock = new DA_BeginTermInStock();
            objResultMessage = objDA_BeginTermInStock.Delete(objBeginTermInStock);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Calculate(String strAuthenData, String strGUID, BeginTermInStock objBeginTermInStock)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_BeginTermInStock objDA_BeginTermInStock = new DA_BeginTermInStock();
            objBeginTermInStock.CreatedUser = objToken.UserName;
            objResultMessage = objDA_BeginTermInStock.Update(objBeginTermInStock);
            return objResultMessage;
        }
    }
}
