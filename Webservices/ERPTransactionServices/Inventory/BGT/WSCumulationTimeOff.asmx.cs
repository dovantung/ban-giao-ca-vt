﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO.BGT;
using ERP.Inventory.DA.BGT;
using System.Data;

namespace ERPTransactionServices.Inventory.BGT
{
    /// <summary>
    /// Summary description for WSCumulationTimeOff
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSCumulationTimeOff : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref CumulationTimeOff objCumulationTimeOff, int intNewYear, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CumulationTimeOff objDA_CumulationTimeOff = new DA_CumulationTimeOff();
            objResultMessage = objDA_CumulationTimeOff.LoadInfo(ref objCumulationTimeOff, intNewYear, strUserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CumulationTimeOff objDA_CumulationTimeOff = new DA_CumulationTimeOff();
            objResultMessage = objDA_CumulationTimeOff.SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "SearchDataList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Calculate(String strAuthenData, String strGUID, int intNewYear)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_CumulationTimeOff objDA_CumulationTimeOff = new DA_CumulationTimeOff();
            objResultMessage = objDA_CumulationTimeOff.Calculate(intNewYear);
            return objResultMessage;
        }
    }
}
