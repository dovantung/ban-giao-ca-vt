﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO.BGT;
using ERP.Inventory.DA.BGT;
using System.Data;

namespace ERPTransactionServices.Inventory.BGT
{
    /// <summary>
    /// Summary description for WSBeginTermMoney
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSBeginTermMoney : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CalcAll(String strAuthenData, String strGUID, bool bolIsCalAll, DateTime dtBeginTermMoneyDate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_BeginTermMoney objDA_BeginTermMoney = new DA_BeginTermMoney();
            objResultMessage = objDA_BeginTermMoney.CalcAll(bolIsCalAll, dtBeginTermMoneyDate, objToken.UserName, HttpContext.Current.Request.UserHostAddress, objToken.CertificateString, objToken.LoginLogID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataMoneyInfo(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_BeginTermMoney objDA_BeginTermMoney = new DA_BeginTermMoney();
            objResultMessage = objDA_BeginTermMoney.SearchDataMoneyInfo(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "SearchDataMoneyInfoList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataMoneyInfoLog(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_BeginTermMoney objDA_BeginTermMoney = new DA_BeginTermMoney();
            objResultMessage = objDA_BeginTermMoney.SearchDataMoneyInfoLog(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "SearchDataMoneyInfoLogList";
            return objResultMessage;
        }
    }
}
