﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Inventory.BorrowProduct
{
    /// <summary>
    /// Summary description for WSBorrowProduct_Delivery
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSBorrowProduct_Delivery : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.BorrowProduct.BorrowProduct_Delivery objBorrowProduct_Delivery, string strBorrowProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_Delivery().LoadInfo(ref objBorrowProduct_Delivery, strBorrowProductID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Inventory.BO.BorrowProduct.BorrowProduct_Delivery objBorrowProduct_Delivery, ref string strBorrowProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_Delivery objDA_BorrowProduct_Delivery = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_Delivery();
            objBorrowProduct_Delivery.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BorrowProduct_Delivery.Insert(objBorrowProduct_Delivery, ref strBorrowProductID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.BorrowProduct.BorrowProduct_Delivery objBorrowProduct_Delivery)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_Delivery objDA_BorrowProduct_Delivery = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_Delivery();
            objBorrowProduct_Delivery.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BorrowProduct_Delivery.Update(objBorrowProduct_Delivery);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Inventory.BO.BorrowProduct.BorrowProduct_Delivery objBorrowProduct_Delivery)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_Delivery objDA_BorrowProduct_Delivery = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_Delivery();
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_Delivery().Delete(objBorrowProduct_Delivery);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_Delivery().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BorrowProduct_DeliveryList";
            return objResultMessage;
        }
    }
}
