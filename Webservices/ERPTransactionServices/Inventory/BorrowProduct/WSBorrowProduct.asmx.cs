﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Inventory.BorrowProduct
{
    /// <summary>
    /// Summary description for WSBorrowProduct
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSBorrowProduct : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct, string strBorrowProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct().LoadInfo(ref objBorrowProduct, strBorrowProductID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID,
                ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct, 
                ref string strBorrowProductID,
                ref DataTable dtbBorrowProduct
                )
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct objDA_BorrowProduct = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct();
            objDA_BorrowProduct.objLogObject.CertificateString = objToken.CertificateString;
            objDA_BorrowProduct.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_BorrowProduct.objLogObject.LoginLogID = objToken.LoginLogID;
            objBorrowProduct.CreatedUser = objToken.UserName;
            objBorrowProduct.CreatedStoreID = objToken.LoginStoreID;
            objResultMessage = objDA_BorrowProduct.Insert(objBorrowProduct, ref strBorrowProductID,ref dtbBorrowProduct);
            if (dtbBorrowProduct != null)
            {
                objResultMessage = new Library.WebCore.ResultMessage();
                objResultMessage.IsError = true;
                objResultMessage.Message = "Tồn tại sản phẩm đã tạo phiếu yêu cầu hoặc vượt giới hạn tồn kho!";
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct
            ,   ref DataTable dtbBorrowProduct)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct objDA_BorrowProduct = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct();
            objDA_BorrowProduct.objLogObject.CertificateString = objToken.CertificateString;
            objDA_BorrowProduct.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_BorrowProduct.objLogObject.LoginLogID = objToken.LoginLogID;
            objBorrowProduct.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BorrowProduct.Update(objBorrowProduct, ref dtbBorrowProduct);
            if (dtbBorrowProduct != null)
            {
                objResultMessage = new Library.WebCore.ResultMessage();
                objResultMessage.IsError = true;
                objResultMessage.Message = "Tồn tại sản phẩm đã tạo phiếu yêu cầu hoặc vượt giới hạn tồn kho!";
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct objDA_BorrowProduct = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct();
            objDA_BorrowProduct.objLogObject.CertificateString = objToken.CertificateString;
            objDA_BorrowProduct.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_BorrowProduct.objLogObject.LoginLogID = objToken.LoginLogID;
            objBorrowProduct.DeletedUser = objToken.UserName;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct().Delete(objBorrowProduct);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BorrowProductList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataMachineStatusReturn(String strAuthenData, String strGUID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct().GetDataMachineStatusReturn(ref tblResult);
            if (tblResult != null)
                tblResult.TableName = "MachineStatusReturnList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataMachineAccessory(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct().SearchDataMachineAccessory(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "MachineAccessoryList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataReview(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_RVL().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BorrowProductReviewList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoAll(String strAuthenData, String strGUID, ref ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct, string strBorrowProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct().LoadInfoAll(ref objBorrowProduct, strBorrowProductID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetBorrowProductNewID(String strAuthenData, String strGUID, ref string strBorrowProductID, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct().GetBorrowProductNewID(ref strBorrowProductID, intStoreID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UserReview(String strAuthenData, String strGUID, ERP.Inventory.BO.BorrowProduct.BorrowProduct_RVL objBorrowProduct_RVL)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_RVL objDA_BorrowProduct_RVL = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct_RVL();
            objResultMessage = objDA_BorrowProduct_RVL.Update(objBorrowProduct_RVL, objToken.CertificateString, HttpContext.Current.Request.UserHostAddress, objToken.LoginLogID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LockOutPut_CHKIMEI(String strAuthenData, String strGUID, ref decimal decOut, int intStoreID, string strProductID, string strIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct objDA_BorrowProduct = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct();
            objResultMessage = objDA_BorrowProduct.LockOutPut_CHKIMEI(ref decOut, intStoreID, strProductID, strIMEI);
            return objResultMessage;
        }
    }
}
