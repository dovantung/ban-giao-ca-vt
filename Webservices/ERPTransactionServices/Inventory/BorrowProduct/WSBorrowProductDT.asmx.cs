﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Inventory.BorrowProduct
{
    /// <summary>
    /// Summary description for WSBorrowProductDT
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSBorrowProductDT : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Inventory.BO.BorrowProduct.BorrowProductDT objBorrowProductDT, string strBorrowProductDTID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProductDT().LoadInfo(ref objBorrowProductDT, strBorrowProductDTID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Inventory.BO.BorrowProduct.BorrowProductDT objBorrowProductDT)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProductDT objDA_BorrowProductDT = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProductDT();
            objBorrowProductDT.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BorrowProductDT.Insert(objBorrowProductDT);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Inventory.BO.BorrowProduct.BorrowProductDT objBorrowProductDT)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProductDT objDA_BorrowProductDT = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProductDT();
            objBorrowProductDT.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BorrowProductDT.Update(objBorrowProductDT);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Inventory.BO.BorrowProduct.BorrowProductDT objBorrowProductDT)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct objDA_BorrowProduct = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProduct();
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProductDT().Delete(objBorrowProductDT);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.BorrowProduct.DA_BorrowProductDT().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BorrowProductDTList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public int CheckImeiExist(String strAuthenData, String strGUID, string strBORROWPRODUCTID, string strPRODUCTID, bool bolISNEW, string strIMEI, decimal decQuantity, int intStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return -1;
            return new ERP.Inventory.DA.BorrowProduct.DA_BorrowProductDT().CheckImeiExist(strBORROWPRODUCTID, strPRODUCTID, bolISNEW, strIMEI, decQuantity, intStoreID);
        }
    }
}
