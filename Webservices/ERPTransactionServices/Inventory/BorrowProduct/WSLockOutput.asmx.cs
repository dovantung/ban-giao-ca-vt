﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Inventory.BorrowProduct
{
    /// <summary>
    /// Summary description for WSLockOutPut
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSLockOutPut : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LockOutPut_CHKIMEI(String strAuthenData, String strGUID, ref string strOut, int intStoreID, string strProductID, string strIMEI, int intIsCheck)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.BorrowProduct.DA_LockOutput objDA_LockOutput = new ERP.Inventory.DA.BorrowProduct.DA_LockOutput();
            objResultMessage = objDA_LockOutput.LockOutPut_CHKIMEI(ref strOut, intStoreID, strProductID, strIMEI, intIsCheck);
            return objResultMessage;
        }
    }
}
