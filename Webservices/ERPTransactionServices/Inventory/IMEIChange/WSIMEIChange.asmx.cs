﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Inventory.IMEIChange
{
    /// <summary>
    /// Summary description for WSIMEIChange
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSIMEIChange : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true, Description = "Tìm kiếm danh sách IMEI đổi", MessageName = "SearchData")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_IMEIChange().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "IMEIChangeList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm danh sách IMEI chưa áp dụng đổi", MessageName = "GetList")]
        public Library.WebCore.ResultMessage GetList(String strAuthenData, String strGUID, ref DataTable tblResult, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_IMEIChange().GetList(ref tblResult, strOutputVoucherID);
            if (tblResult != null)
                tblResult.TableName = "IMEIChangeList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Thêm IMEI đổi", MessageName = "Insert")]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, List<ERP.Inventory.BO.PM.IMEIChange> lstIMEIChange)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_IMEIChange objDA_IMEIChange = new ERP.Inventory.DA.PM.DA_IMEIChange();
            objResultMessage = objDA_IMEIChange.Insert(lstIMEIChange, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xóa IMEI đổi", MessageName = "Delete")]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strIMEIChangeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.PM.DA_IMEIChange objDA_IMEIChange = new ERP.Inventory.DA.PM.DA_IMEIChange();
            objDA_IMEIChange.objLogObject.CertificateString = objToken.CertificateString;
            objDA_IMEIChange.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_IMEIChange.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_IMEIChange.Delete(strIMEIChangeID, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy IMEI cũ", MessageName = "GetIMEI")]
        public Library.WebCore.ResultMessage GetIMEI(String strAuthenData, String strGUID, ref string strFromIMEI, string strToIMEI)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.PM.DA_IMEIChange().GetIMEI(ref strFromIMEI, strToIMEI);
            return objResultMessage;
        }

    }
}
