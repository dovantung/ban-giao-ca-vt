﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.StoreChangeCommand
{
    /// <summary>
    /// Summary description for WSAutoSplitProduct
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSAutoSplitProduct : System.Web.Services.WebService
    {
        #region Chia hàng từ đơn hàng
        /// <summary>
        /// Kiểm tra đơn hàng đã đc duyệt chưa
        /// </summary>
        [WebMethod(EnableSession = true, Description = "Kiểm tra đơn hàng đã đc duyệt chưa", MessageName = "Kiểm_Tra_Đơn_Hàng_Duyệt")]
        public Library.WebCore.ResultMessage CheckOrderReviewStatus(String strAuthenData, String strGUID, ref int intResult, string strOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_AutoSplitProduct().CheckOrderReviewStatus(ref intResult, strOrderID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetAverageSale(String strAuthenData, String strGUID, ref DataTable dtbData, string strOrderID,
            int intNumDay, string strAreaIDList, string strStoreIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_AutoSplitProduct().GetAverageSale(ref dtbData, strOrderID, intNumDay, objToken.UserName, strAreaIDList, strStoreIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetInStockByOrder(String strAuthenData, String strGUID, ref DataTable dtbData, string strOrderID, string strAreaIDList, string strStoreIDList,int intIsCheckRealInput)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_AutoSplitProduct().GetInStockByOrder(ref dtbData, strOrderID, strAreaIDList, strStoreIDList, intIsCheckRealInput);
            return objResultMessage;
        }



        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetProductAverageSale(String strAuthenData, String strGUID, ref DataTable dtbData, string strProductID, int intNumDay, string strAreaIDList, string strStoreIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_AutoSplitProduct().GetProductAverageSale(ref dtbData, strProductID, intNumDay, objToken.UserName, strAreaIDList, strStoreIDList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetProductListByOrderID(String strAuthenData, String strGUID,
           ref DataTable dtbData, string strOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_AutoSplitProduct objDA_AutoSplitProduct = new DA_AutoSplitProduct();
            objDA_AutoSplitProduct.GetProductListByOrderID(ref dtbData, strOrderID);
            return objResultMessage;
        } 
        #endregion
        #region Chia hàng từ động giữa các kho
        /// <summary>
        /// lấy dữ liệu tồn kho theo kho
        /// RPT_ReportStoreInStock_GetList
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetStoreInStock(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_AutoSplitProduct().GetStoreInStock(ref dtbData, objKeywords);
            return objResultMessage;
        }

        /// <summary>
        /// lấy dữ liệu nhập theo xuất chuyển kho
        /// PM_INPUTVOUCHER_GETSTOCHANGE
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetInputVoucherByStorechange(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_AutoSplitProduct().GetInputVoucherByStorechange(ref dtbData, objKeywords);
            return objResultMessage;
        }

        /// <summary>
        /// Tính số lượng bán trung bình theo kho
        /// PM_RPT_STOREINSTOCK_AVGSALE
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetAvgSaleStoreInStock(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_AutoSplitProduct().GetAvgSaleStoreInStock(ref dtbData, objKeywords);
            return objResultMessage;
        }


        /// <summary>
        /// nạp số lượng đã tạo lệnh chuyển kho, tạo yêu cầu
        /// GETAUTOSTORECHANGEQUANTITY
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetAutoStoreChangeQuantity(String strAuthenData, String strGUID, ref DataTable dtbData, string strPruductIDList,string strFromStoreIDList,string strToStoreIDList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_AutoSplitProduct().GetAutoStoreChangeQuantity(ref dtbData,strPruductIDList,strFromStoreIDList,strToStoreIDList);
            return objResultMessage;
        }
        #endregion
    }
}
