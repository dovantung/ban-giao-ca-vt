﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Inventory.Barcode
{
    /// <summary>
    /// Summary description for WSBarcode
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSProductBarcode : System.Web.Services.WebService
    {

        /// <summary>
        /// Tìm kiếm sản phẩm tồn kho cần in barcode
        /// PM_CurrentInstock_GetPrints
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCurrentInstockPrints(String strAuthenData, String strGUID, ref DataTable dtbData, int intMainGroupID,
            int intSubGroupID, string strProductID, string strBrandIDList, int intStoreID, int intIsNew, bool bolIsInStock, string strInputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_ProductBarcode().GetCurrentInstockPrints(ref dtbData, objToken.UserName, intMainGroupID, intSubGroupID, strProductID, strBrandIDList, intStoreID, intIsNew, bolIsInStock,strInputVoucherID);
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm sản phẩm tồn kho cần in barcode
        /// PM_CurrentInstock_IMEI_GetPr
        /// </summary>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCurrentInstockPrints_IMEI(String strAuthenData, String strGUID, ref DataTable dtbData, int intMainGroupID,
            int intSubGroupID, string strProductID, string strBrandIDList, int intStoreID, int intIsNew, string strInputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.DA_ProductBarcode().GetCurrentInstockPrints_IMEI(ref dtbData, objToken.UserName, intMainGroupID, intSubGroupID, strProductID, strBrandIDList, intStoreID, intIsNew, strInputVoucherID);
            return objResultMessage;
        }
    }
}
