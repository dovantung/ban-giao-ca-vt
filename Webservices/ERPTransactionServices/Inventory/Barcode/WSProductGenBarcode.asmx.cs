﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.Barcode
{
    /// <summary>
    /// Summary description for WSProductGenBarcode
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSProductGenBarcode : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true, Description = "Lấy danh sách sản phẩm Barcode", MessageName = "LoadInfo")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref Product_GenBarcode objProduct_GenBarcode, decimal strBaracode)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Product_GenBarcode().LoadInfo(ref objProduct_GenBarcode, strBaracode);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm danh sách sản phẩm Barcode", MessageName = "SearchData")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_Product_GenBarcode().SearchData(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, List<Product_GenBarcode> objlistProduct_GenBarcode)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_Product_GenBarcode objDA_Product_GenBarcoder = new ERP.Inventory.DA.DA_Product_GenBarcode();
            objResultMessage = objDA_Product_GenBarcoder.Insert(objlistProduct_GenBarcode);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GenBarcode(String strAuthenData, String strGUID, String strProductID, String strStringBarcode, ref String strBarcode)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_Product_GenBarcode objDA_Product_GenBarcoder = new ERP.Inventory.DA.DA_Product_GenBarcode();
            objResultMessage = objDA_Product_GenBarcoder.GenBarcode( strProductID, strStringBarcode, ref strBarcode);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, List<Product_GenBarcode> objlistProduct_GenBarcode)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Inventory.DA.DA_Product_GenBarcode objDA_Product_GenBarcode = new ERP.Inventory.DA.DA_Product_GenBarcode();
            objResultMessage = objDA_Product_GenBarcode.Update(objlistProduct_GenBarcode);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }
    }
}
