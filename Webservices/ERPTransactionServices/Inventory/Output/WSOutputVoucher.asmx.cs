﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.Inventory.BO;
using ERP.Inventory.DA;
using System.Data;

namespace ERPTransactionServices.Inventory.Output
{
    /// <summary>
    /// Summary description for WSOutputVoucher
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSOutputVoucher : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true, Description = "Lấy thông tin phiếu xuất", MessageName = "LoadInfo")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref OutputVoucher objOutputVoucher, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_OutputVoucher().LoadInfo(ref objOutputVoucher, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tìm kiếm danh sách thông tin phiếu xuất", MessageName = "SearchData")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_OutputVoucher().SearchData(ref dtbData, objKeywords);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true, Description = "Tìm kiếm danh sách thông tin Bảo hiêm PVI", MessageName = "SearchDataPVI")]
        public Library.WebCore.ResultMessage SearchDataPVI(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_OutputVoucher().SearchDataPVI(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy danh sách chi tiết phiếu xuất", MessageName = "GetList")]
        public Library.WebCore.ResultMessage GetList(String strAuthenData, String strGUID, ref DataTable dtbData, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_OutputVoucherDetail().GetList(ref dtbData, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy danh sách chi tiết phiếu xuất", MessageName = "GetAll")]
        public Library.WebCore.ResultMessage GetAll(String strAuthenData, String strGUID, ref DataTable dtbData, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_OutputVoucherDetail().GetAll(ref dtbData, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Kiểm tra phiếu thu", MessageName = "CheckVoucherByOrderID")]
        public Library.WebCore.ResultMessage CheckVoucherByOrderID(String strAuthenData, String strGUID, ref int intVoucherTypeID, string strSaleOrderID, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_OutputVoucher().CheckVoucherByOrderID(ref intVoucherTypeID, strSaleOrderID, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy số tiền đã thu trong chi tiết phiếu thu", MessageName = "GetTotalPaidByOrderID")]
        public Library.WebCore.ResultMessage GetTotalPaidByOrderID(String strAuthenData, String strGUID, ref string strVoucherID, ref decimal decTotalPaid, ref decimal decDebt, string strSaleOrderID, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_OutputVoucher().GetTotalPaidByOrderID(ref strVoucherID, ref decTotalPaid, ref decDebt, strSaleOrderID, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật thông tin khách hàng", MessageName = "UpdateCustomer")]
        public Library.WebCore.ResultMessage UpdateCustomer(String strAuthenData, String strGUID, OutputVoucher objOutputVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();
            objDA_OutputVoucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_OutputVoucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_OutputVoucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_OutputVoucher.UpdateCustomer(objOutputVoucher, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy danh sách phiếu xuất qua đơn hàng", MessageName = "GetListOV")]
        public Library.WebCore.ResultMessage GetListOV(String strAuthenData, String strGUID, ref DataTable dtbData, string strOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_OutputVoucher().GetList(ref dtbData, strOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckOutputType_IsSubDebtOutput(String strAuthenData, String strGUID, ref bool bolIsError, string strOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();
            objResultMessage = objDA_OutputVoucher.CheckOutputType_IsSubDebtOutput(ref bolIsError, strOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Tạo phiếu xuất", MessageName = "CreateOutputVoucher")]
        public Library.WebCore.ResultMessage CreateOutputVoucher(String strAuthenData, String strGUID, OutputVoucher objOutputVoucher)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();
            objDA_OutputVoucher.objLogObject.CertificateString = objToken.CertificateString;
            objDA_OutputVoucher.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_OutputVoucher.objLogObject.LoginLogID = objToken.LoginLogID;
            objOutputVoucher.OutputVoucherDetailList = objOutputVoucher.OutputVoucherDetailList.OrderBy(x => x.IsPromotionProduct).ToList();
            objResultMessage = objDA_OutputVoucher.CreateOutputVoucher(objOutputVoucher);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckOV_OrderID(String strAuthenData, String strGUID, ref DataTable dtbData, string strOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();
            objResultMessage = objDA_OutputVoucher.CheckOV_Order(ref dtbData, strOrderID);
            if(dtbData != null)
            {
                dtbData.TableName = "CHECKORDER";
            }
            return objResultMessage;
        }


    }
}
