﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.Reward
{
    /// <summary>
    /// Summary description for WSRewardPosition
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRewardPosition : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Reward.BO.Define.RewardPosition objRewardPosition, decimal decRewardPositionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardPosition().LoadInfo(ref objRewardPosition, decRewardPositionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RewardPosition objRewardPosition, ref string strRewardPositionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPosition objDA_RewardPosition = new ERP.Reward.DA.Define.DA_RewardPosition();
            objDA_RewardPosition.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RewardPosition.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RewardPosition.objLogObject.LoginLogID = objToken.LoginLogID;
            objRewardPosition.CreatedUser = objToken.UserName;
            objResultMessage = objDA_RewardPosition.Insert(objRewardPosition, ref strRewardPositionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertPercent(String strAuthenData, String strGUID, decimal decRewardPositionID, decimal decMainGroupID, decimal decSubGroupID, bool bolIsRewardPercent, decimal decRewardValue)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPosition objDA_RewardPosition = new ERP.Reward.DA.Define.DA_RewardPosition();
            objResultMessage = objDA_RewardPosition.InsertPercent(decRewardPositionID, decMainGroupID, decSubGroupID, bolIsRewardPercent, decRewardValue);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RewardPosition objRewardPosition, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPosition objDA_RewardPosition = new ERP.Reward.DA.Define.DA_RewardPosition();
            objDA_RewardPosition.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RewardPosition.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RewardPosition.objLogObject.LoginLogID = objToken.LoginLogID;
            objRewardPosition.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RewardPosition.Update(objRewardPosition, lstOrderIndex);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RewardPosition objRewardPosition)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPosition objDA_RewardPosition = new ERP.Reward.DA.Define.DA_RewardPosition();
            objDA_RewardPosition.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RewardPosition.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RewardPosition.objLogObject.LoginLogID = objToken.LoginLogID;
            objRewardPosition.DeletedUser = objToken.UserName;
            objResultMessage = objDA_RewardPosition.Delete(objRewardPosition);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeletePercent(String strAuthenData, String strGUID, decimal decRewardPositionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPosition objDA_RewardPosition = new ERP.Reward.DA.Define.DA_RewardPosition();
            objResultMessage = objDA_RewardPosition.DeletePercent(decRewardPositionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbRewardPosition, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardPosition().SearchData(ref dtbRewardPosition, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataPercent(String strAuthenData, String strGUID, ref DataTable dtbRewardPosition, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardPosition().SearchDataPercent(ref dtbRewardPosition, objKeywords);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataOutputType(String strAuthenData, String strGUID, ref DataTable dtbOutputType, decimal decRewardPositionID, int intOutputType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardPosition().SearchDataOutputType(ref dtbOutputType,decRewardPositionID,intOutputType);
            return objResultMessage;
        }
    }
}
