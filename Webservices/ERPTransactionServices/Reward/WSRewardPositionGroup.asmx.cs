﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.Reward
{
    /// <summary>
    /// Summary description for WSRewardPositionGroup
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRewardPositionGroup : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Reward.BO.Define.RewardPositionGroup objRewardPositionGroup, int intRewardPositionGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardPositionGroup().LoadInfo(ref objRewardPositionGroup, intRewardPositionGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RewardPositionGroup objRewardPositionGroup, ref string strRewardPositionGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPositionGroup objDA_RewardPositionGroup = new ERP.Reward.DA.Define.DA_RewardPositionGroup();
            objRewardPositionGroup.CreatedUser = objToken.UserName;
            objResultMessage = objDA_RewardPositionGroup.Insert(objRewardPositionGroup, ref strRewardPositionGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertPosition_PositionGroup(String strAuthenData, String strGUID, decimal decRewardPositionGroupID, decimal decRewardPositionID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPositionGroup objDA_RewardPositionGroup = new ERP.Reward.DA.Define.DA_RewardPositionGroup();
            objResultMessage = objDA_RewardPositionGroup.InsertPosition_PositionGroup(decRewardPositionGroupID, decRewardPositionID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RewardPositionGroup objRewardPositionGroup, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPositionGroup objDA_RewardPositionGroup = new ERP.Reward.DA.Define.DA_RewardPositionGroup();
            objRewardPositionGroup.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RewardPositionGroup.Update(objRewardPositionGroup, lstOrderIndex);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RewardPositionGroup objRewardPositionGroup)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPositionGroup objDA_RewardPositionGroup = new ERP.Reward.DA.Define.DA_RewardPositionGroup();
            objRewardPositionGroup.DeletedUser = objToken.UserName;
            objResultMessage = objDA_RewardPositionGroup.Delete(objRewardPositionGroup);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeletePosition_PositionGroup(String strAuthenData, String strGUID, decimal decRewardPositionGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardPositionGroup objDA_RewardPositionGroup = new ERP.Reward.DA.Define.DA_RewardPositionGroup();
            objResultMessage = objDA_RewardPositionGroup.DeletePosition_PositionGroup(decRewardPositionGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbRewardPositionGroup, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardPositionGroup().SearchData(ref dtbRewardPositionGroup, objKeywords);
            return objResultMessage;
        }
        
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataPosition_PositionGroup(String strAuthenData, String strGUID, ref DataTable dtbRewardPositionGroup, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardPositionGroup().SearchDataPosition_PositionGroup(ref dtbRewardPositionGroup, objKeywords);
            return objResultMessage;
        }
    }
}
