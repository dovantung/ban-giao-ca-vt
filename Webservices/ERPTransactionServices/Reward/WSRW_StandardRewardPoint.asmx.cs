﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Services;
using ERP.Reward.BO;
using ERP.Reward.DA;

namespace ERPTransactionServices.Reward
{
    /// <summary>
    /// Summary description for WSRW_StandardRewardPoint
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRW_StandardRewardPoint : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Reward.BO.Define.RW_StandardRewardPoint objRW_StandardRewardPoint, int intBrandID,int intIsNewType, int intPriceRangeID ,int intSubGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RW_StandardRewardPoint().LoadInfo(ref objRW_StandardRewardPoint,intBrandID,intIsNewType,intPriceRangeID,intSubGroupID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RW_StandardRewardPoint objRW_StandardRewardPoint)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RW_StandardRewardPoint objDA_RW_StandardRewardPoint = new ERP.Reward.DA.Define.DA_RW_StandardRewardPoint();
            objDA_RW_StandardRewardPoint.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RW_StandardRewardPoint.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RW_StandardRewardPoint.objLogObject.LoginLogID = objToken.LoginLogID;
            objRW_StandardRewardPoint.CreatedUser = objToken.UserName;
            objResultMessage = new ERP.Reward.DA.Define.DA_RW_StandardRewardPoint().Insert(objRW_StandardRewardPoint);
            return objResultMessage;
        }

        
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RW_StandardRewardPoint objRW_StandardRewardPoint)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RW_StandardRewardPoint objDA_RW_StandardRewardPoint = new ERP.Reward.DA.Define.DA_RW_StandardRewardPoint();
            objDA_RW_StandardRewardPoint.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RW_StandardRewardPoint.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RW_StandardRewardPoint.objLogObject.LoginLogID = objToken.LoginLogID;
            objRW_StandardRewardPoint.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RW_StandardRewardPoint.Update(objRW_StandardRewardPoint);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RW_StandardRewardPoint objRW_StandardRewardPoint)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RW_StandardRewardPoint objDA_RW_StandardRewardPoint = new ERP.Reward.DA.Define.DA_RW_StandardRewardPoint();
            objDA_RW_StandardRewardPoint.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RW_StandardRewardPoint.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RW_StandardRewardPoint.objLogObject.LoginLogID = objToken.LoginLogID;
            objRW_StandardRewardPoint.DeletedUser = objToken.UserName;
            objResultMessage = objDA_RW_StandardRewardPoint.Delete(objRW_StandardRewardPoint);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbRW_StandardRewardPoint, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RW_StandardRewardPoint().SearchData(ref dtbRW_StandardRewardPoint, objKeywords);
            return objResultMessage;
        }
    }
}
