﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.Reward
{
    /// <summary>
    /// Summary description for WSRewardCalcSchedule
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRewardCalcSchedule : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Reward.BO.Define.RewardCalcSchedule objRewardCalcSchedule, string strRewardCalcScheduleID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardCalcSchedule().LoadInfo(ref objRewardCalcSchedule, strRewardCalcScheduleID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoByDate(String strAuthenData, String strGUID, ref ERP.Reward.BO.Define.RewardCalcSchedule objRewardCalcSchedule, DateTime dteRewardDate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardCalcSchedule().LoadInfo(ref objRewardCalcSchedule, dteRewardDate);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RewardCalcSchedule objRewardCalcSchedule)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardCalcSchedule objDA_RewardCalcSchedule = new ERP.Reward.DA.Define.DA_RewardCalcSchedule();
            objDA_RewardCalcSchedule.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RewardCalcSchedule.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RewardCalcSchedule.objLogObject.LoginLogID = objToken.LoginLogID;
            objRewardCalcSchedule.CreatedUser = objToken.UserName;
            objResultMessage = objDA_RewardCalcSchedule.Insert(objRewardCalcSchedule);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RewardCalcSchedule objRewardCalcSchedule, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardCalcSchedule objDA_RewardCalcSchedule = new ERP.Reward.DA.Define.DA_RewardCalcSchedule();
            objDA_RewardCalcSchedule.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RewardCalcSchedule.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RewardCalcSchedule.objLogObject.LoginLogID = objToken.LoginLogID;
            objRewardCalcSchedule.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RewardCalcSchedule.Update(objRewardCalcSchedule);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Reward.BO.Define.RewardCalcSchedule objRewardCalcSchedule)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_RewardCalcSchedule objDA_RewardCalcSchedule = new ERP.Reward.DA.Define.DA_RewardCalcSchedule();
            objDA_RewardCalcSchedule.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RewardCalcSchedule.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RewardCalcSchedule.objLogObject.LoginLogID = objToken.LoginLogID;
            objRewardCalcSchedule.DeletedUser = objToken.UserName;
            objResultMessage = objDA_RewardCalcSchedule.Delete(objRewardCalcSchedule);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbRewardCalcSchedule, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardCalcSchedule().SearchData(ref dtbRewardCalcSchedule, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataLog(String strAuthenData, String strGUID, ref DataTable dtbRewardCalcSchedule, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_RewardCalcSchedule().SearchDataLog(ref dtbRewardCalcSchedule, objKeywords);
            return objResultMessage;
        }
    }
}
