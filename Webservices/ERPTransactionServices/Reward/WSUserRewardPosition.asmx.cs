﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.ComponentModel;

namespace ERPTransactionServices.Reward
{
    /// <summary>
    /// Summary description for WSUserRewardPosition
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSUserRewardPosition : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Reward.BO.Define.UserRewardPosition objUserRewardPosition, int intRewardPositionID,string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_UserRewardPosition().LoadInfo(ref objUserRewardPosition, intRewardPositionID, strUserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Reward.BO.Define.UserRewardPosition objUserRewardPosition)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_UserRewardPosition objDA_UserRewardPosition = new ERP.Reward.DA.Define.DA_UserRewardPosition();
            objUserRewardPosition.CreatedUser = objToken.UserName;
            objResultMessage = objDA_UserRewardPosition.Insert(objUserRewardPosition);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertGroup(String strAuthenData, String strGUID, string strUserName, decimal decRewardPositionGroupID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_UserRewardPosition objDA_UserRewardPosition = new ERP.Reward.DA.Define.DA_UserRewardPosition();
            ERP.Reward.BO.Define.UserRewardPosition objUserRewardPosition = new ERP.Reward.BO.Define.UserRewardPosition();
            objResultMessage = objDA_UserRewardPosition.InsertGroup(strUserName, decRewardPositionGroupID, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Reward.BO.Define.UserRewardPosition objUserRewardPosition)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_UserRewardPosition objDA_UserRewardPosition = new ERP.Reward.DA.Define.DA_UserRewardPosition();
            objUserRewardPosition.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_UserRewardPosition.Update(objUserRewardPosition);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Reward.BO.Define.UserRewardPosition objUserRewardPosition)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_UserRewardPosition objDA_UserRewardPosition = new ERP.Reward.DA.Define.DA_UserRewardPosition();
            objResultMessage = objDA_UserRewardPosition.Delete(objUserRewardPosition);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteGroup(String strAuthenData, String strGUID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Reward.DA.Define.DA_UserRewardPosition objDA_UserRewardPosition = new ERP.Reward.DA.Define.DA_UserRewardPosition();
            objResultMessage = objDA_UserRewardPosition.DeleteGroup(strUserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbUserRewardPosition, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_UserRewardPosition().SearchData(ref dtbUserRewardPosition, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataGroup(String strAuthenData, String strGUID, ref DataTable dtbUserRewardPosition, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_UserRewardPosition().SearchDataGroup(ref dtbUserRewardPosition, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataPosGroup_Pos(String strAuthenData, String strGUID, ref DataTable dtbUserRewardPosition, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Reward.DA.Define.DA_UserRewardPosition().SearchDataPosGroup_Pos(ref dtbUserRewardPosition, objKeywords);
            return objResultMessage;
        }
    }
}
