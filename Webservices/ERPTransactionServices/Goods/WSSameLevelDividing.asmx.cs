﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Goods
{
    /// <summary>
    /// Summary description for WSSameLevelDividing
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSSameLevelDividing : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Goods.DA_SameLevelDividing().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchRotatingHistory(String strAuthenData, String strGUID, ref DataTable dtRedundance, ref DataTable dtLack,
            ref DataTable dtGoods, ref DataTable dtShop, ref DataTable dtResult,
            String goods_rotate_history_id, String size)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Goods.DA_SameLevelDividing().SearchRotatingHistory( ref dtRedundance, ref dtLack,
            ref dtGoods, ref dtShop, ref dtResult, goods_rotate_history_id, size);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GoodsRotate(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords, object[] objGoods, object[] objShops)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Goods.DA_SameLevelDividing().GoodsRotate(ref dtResult, objKeywords, objGoods, objShops);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage RecyclingRotateGoods(String strAuthenData, String strGUID, ref DataTable dtResult, String goods_rotate_history_id)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Goods.DA_SameLevelDividing().RecyclingRotateGoods(ref dtResult, goods_rotate_history_id);
            return objResultMessage;
        }
    }
}
