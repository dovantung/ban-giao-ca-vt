﻿using ERP.Procurement.BO.PMPOReturn;
using ERP.Procurement.DA.PMPOReturn;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Procurement.PMPOReturn
{
    /// <summary>
    /// Summary description for WSPOReturn
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPOReturn : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Procurement.BO.PMPOReturn.POReturn objPOReturn, string strPOReturnID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.PMPOReturn.DA_POReturn().LoadInfo(ref objPOReturn, strPOReturnID);
            if (objResultMessage != null && !objResultMessage.IsError)
            {
                // Load chi tiết sản phẩm
                List<ERP.Procurement.BO.PMPOReturn.POReturnDetail> lstPOReturnDetail = new List<ERP.Procurement.BO.PMPOReturn.POReturnDetail>();
                //DataTable dtbPOReturnDetailTable = null;
                objResultMessage = new ERP.Procurement.DA.PMPOReturn.DA_POReturnDetail().SearchDataToList(ref lstPOReturnDetail, strPOReturnID);
                if (objResultMessage.IsError)
                    return objResultMessage;

                //objResultMessage = new ERP.Procurement.DA.PMPOReturn.DA_POReturnDetail().SearchDataToTable(ref dtbPOReturnDetailTable, strPOReturnID);
                //if (objResultMessage.IsError)
                //    return objResultMessage;
                objPOReturn.POReturnDetailList = lstPOReturnDetail.ToList();
                //objPOReturn.POReturnDetailTable = dtbPOReturnDetailTable.Copy();
                //objPOReturn.POReturnDetailTable.TableName = "POReturnTable";

                // Load đính kèm file
                List<ERP.Procurement.BO.PMPOReturn.POReturn_Attachment> lstPOReturn_Attachment = new List<ERP.Procurement.BO.PMPOReturn.POReturn_Attachment>();
                objResultMessage = new ERP.Procurement.DA.PMPOReturn.DA_POReturn_Attachment().SearchDataToList(ref lstPOReturn_Attachment, objPOReturn.POReturnID.Trim());
                if (objResultMessage.IsError)
                    return objResultMessage;
                objPOReturn.POReturn_AttachmentList = lstPOReturn_Attachment.ToList();

                // Load MUC DUYET
                DataTable dtbPOReturn_ReviewdList = null;
                dtbPOReturn_ReviewdList = new ERP.Procurement.DA.PMPOReturn.DA_POReturn_ReviewList().GetDataTable(objPOReturn.POReturnID.Trim());
                if (objResultMessage.IsError)
                    return objResultMessage;
                objPOReturn.ReviewListTable = dtbPOReturn_ReviewdList;
            }
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Procurement.BO.PMPOReturn.POReturn objPOReturn, ref string strPOReturnID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.PMPOReturn.DA_POReturn objDA_POReturn = new ERP.Procurement.DA.PMPOReturn.DA_POReturn();
            objDA_POReturn.objLogObject.CertificateString = objToken.CertificateString;
            objDA_POReturn.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_POReturn.objLogObject.LoginLogID = objToken.LoginLogID;
            objPOReturn.CreatedUser = objToken.UserName;
            objResultMessage = objDA_POReturn.Insert(objPOReturn, ref strPOReturnID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Procurement.BO.PMPOReturn.POReturn objPOReturn)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.PMPOReturn.DA_POReturn objDA_POReturn = new ERP.Procurement.DA.PMPOReturn.DA_POReturn();
            objDA_POReturn.objLogObject.CertificateString = objToken.CertificateString;
            objDA_POReturn.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_POReturn.objLogObject.LoginLogID = objToken.LoginLogID;
            objPOReturn.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_POReturn.Update(objPOReturn);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Procurement.BO.PMPOReturn.POReturn objPOReturn)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.PMPOReturn.DA_POReturn objDA_POReturn = new ERP.Procurement.DA.PMPOReturn.DA_POReturn();
            objDA_POReturn.objLogObject.CertificateString = objToken.CertificateString;
            objDA_POReturn.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_POReturn.objLogObject.LoginLogID = objToken.LoginLogID;
            objPOReturn.DeletedUser = objToken.UserName;
            objResultMessage = objDA_POReturn.Delete(objPOReturn);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.PMPOReturn.DA_POReturn().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "POReturnTable";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData_InputVoucher(String strAuthenData, String strGUID, ref Byte[] objByte, string[] arrOutString, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DataSet dsData = null;
            objResultMessage = new ERP.Procurement.DA.PMPOReturn.DA_POReturn().SearchData_InputVoucher(ref dsData, arrOutString, objKeywords);
            if (!objResultMessage.IsError && dsData != null)
                objByte = ConvertDataSetToByte(dsData);
            return objResultMessage;
        }



        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Created_OutputVoucher(String strAuthenData, String strGUID, string strPOReturnID, List<ERP.Inventory.BO.OutputVoucher> lstOutputVoucher, ref List<string> lstOutputVoucherID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.PMPOReturn.DA_POReturn objDA_POReturn = new ERP.Procurement.DA.PMPOReturn.DA_POReturn();
            objDA_POReturn.objLogObject.CertificateString = objToken.CertificateString;
            objDA_POReturn.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_POReturn.objLogObject.LoginLogID = objToken.LoginLogID;
            foreach (var objOutputVoucher in lstOutputVoucher)
            {
                objOutputVoucher.CreatedUser = objToken.UserName;
            }
            objResultMessage = objDA_POReturn.Created_OutputVoucherList(strPOReturnID, lstOutputVoucher, ref lstOutputVoucherID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData_OutputVoucher(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.PMPOReturn.DA_POReturn().SearchData_OutputVoucher(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "POReturnDetailTable";
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewList(String strAuthenData, String strGUID, POReturn_ReviewList objPOReturn_ReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_POReturn_ReviewList objDA_POReturn_ReviewList = new DA_POReturn_ReviewList();
            objResultMessage = objDA_POReturn_ReviewList.UpdateReviewList(objPOReturn_ReviewList);
            return objResultMessage;
        }
        private Byte[] ConvertDataSetToByte(DataSet dsData)
        {
            MemoryStream objMemoryStream = new MemoryStream();
            GZipStream objGZipStream = new GZipStream(objMemoryStream, CompressionMode.Compress);
            dsData.WriteXml(objGZipStream, XmlWriteMode.WriteSchema);
            objGZipStream.Close();
            Byte[] objByte = objMemoryStream.ToArray();
            objMemoryStream.Close();
            return objByte;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckProductInstock(String strAuthenData, String strGUID,int intStoreID,int intCustomerID, DataTable dtbProduct, ref Byte[] objByte, int intInstockID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_POReturn objDA_POReturn = new DA_POReturn();
            DataSet dsResult = null;
            objResultMessage = objDA_POReturn.CheckInstockProduct(intStoreID, intCustomerID, dtbProduct, ref dsResult, intInstockID);
            if (!objResultMessage.IsError && dsResult != null)
                objByte = ConvertDataSetToByte(dsResult);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckIMEIInstock(String strAuthenData, String strGUID, int intStoreID, DataTable dtbImei, ref Byte[] objByte)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_POReturn objDA_POReturn = new DA_POReturn();
            DataSet dsResult = null;
            objResultMessage = objDA_POReturn.CheckIMEIInstock(intStoreID, dtbImei, ref dsResult);
            if (!objResultMessage.IsError && dsResult != null)
                objByte = ConvertDataSetToByte(dsResult);
            return objResultMessage;
        }
    }
}
