﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Procurement
{
    /// <summary>
    /// Summary description for WSPlanOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPlanOrder : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Procurement.BO.PlanOrder objPlanOrder, string strPlanOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PlanOrder().LoadInfo(ref objPlanOrder, strPlanOrderID);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoPlanOrder_ReviewList(String strAuthenData, String strGUID, ref ERP.Procurement.BO.PlanOrder_ReviewList objPlanOrder_ReviewList, string strOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PlanOrder_ReviewList().LoadInfo(ref objPlanOrder_ReviewList, strOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Procurement.BO.PlanOrder objPlanOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_PlanOrder objDA_PlanOrder = new ERP.Procurement.DA.DA_PlanOrder();
            objDA_PlanOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PlanOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PlanOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PlanOrder.Insert(objPlanOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Procurement.BO.PlanOrder objPlanOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_PlanOrder objDA_PlanOrder = new ERP.Procurement.DA.DA_PlanOrder();
            objDA_PlanOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PlanOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PlanOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_PlanOrder.Update(objPlanOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strPlanOrderID, string UserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.BO.PlanOrder objOrder = new ERP.Procurement.BO.PlanOrder();
            ERP.Procurement.DA.DA_PlanOrder objDA_PlanOrder = new ERP.Procurement.DA.DA_PlanOrder();
            objDA_PlanOrder.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PlanOrder.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PlanOrder.objLogObject.LoginLogID = objToken.LoginLogID;
            objOrder.PlanOrderID = strPlanOrderID;
            objOrder.DeletedUser = UserName;
            objResultMessage = objDA_PlanOrder.Delete(objOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PlanOrder().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "PlanOrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDataNew(String strAuthenData, String strGUID, ref DataSet dsResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PlanOrder().LoadDataNew(ref dsResult);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDataEdit(String strAuthenData, String strGUID, ref DataSet dsResult, string strOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PlanOrder().LoadDataEdit(ref dsResult, strOrderID);

            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateStatusReview(String strAuthenData, String strGUID, ERP.Procurement.BO.PlanOrder_ReviewList objPlanOrder_ReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_PlanOrder_ReviewList objDA_Order_ReviewList = new ERP.Procurement.DA.DA_PlanOrder_ReviewList();
            objResultMessage = objDA_Order_ReviewList.Update(objPlanOrder_ReviewList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDataFromPOType(String strAuthenData, String strGUID, ref DataTable dsResult, DataSet dsCond, int intPOTypeID, string strMainGroupID, string strBrandID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PlanOrder_ReviewList().LoadDataFromPOType(ref dsResult, dsCond, intPOTypeID, strMainGroupID, strBrandID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckCustomerOrderID(String strAuthenData, String strGUID, ref DataTable dsResult, string strCustomerIDlist)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PlanOrderDetail().checkCustomerOrderID(ref dsResult, strCustomerIDlist);
            if (dsResult != null)
                dsResult.TableName = "CustomerOrderIDList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LayGiaiTrinh(String strAuthenData, String strGUID, ref DataTable dsResult, string strPlanOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PlanOrder().LoadGiaiTrinh(ref dsResult, strPlanOrderID);
            if (dsResult != null)
                dsResult.TableName = "POM_PLANORDER_GT";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage PrintDonDatHang(String strAuthenData, String strGUID, ref DataTable dsResult, string strPlanOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PlanOrder().PrintDonDatHang(ref dsResult, strPlanOrderID);
            if (dsResult != null)
                dsResult.TableName = "POM_PLANORDER_PRINT";
            return objResultMessage;
        }

    }
}
