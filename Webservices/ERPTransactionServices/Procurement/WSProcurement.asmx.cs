﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Procurement
{
    /// <summary>
    /// Summary description for WSProcurement
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSProcurement : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Procurement.BO.Order objOrder, string strOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order().LoadInfo(ref objOrder, strOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoOrderCustumer(String strAuthenData, String strGUID, ref ERP.Procurement.BO.Order_Custumer objOrder_Custumer, string strOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order_Custumer().LoadInfo(ref objOrder_Custumer, strOrderID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Procurement.BO.Order objOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_Order objDA_Order = new ERP.Procurement.DA.DA_Order();
            objDA_Order.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Order.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Order.objLogObject.LoginLogID = objToken.LoginLogID;
            objOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Order.Insert(objOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert_Order(String strAuthenData, String strGUID, List<ERP.Procurement.BO.Order> objlistOrder, ERP.Procurement.BO.PlanOrder objPlanOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_Order objDA_Order = new ERP.Procurement.DA.DA_Order();
            objDA_Order.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Order.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Order.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Order.Insert_Order(objlistOrder, objPlanOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Procurement.BO.Order objOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_Order objDA_Order = new ERP.Procurement.DA.DA_Order();
            objDA_Order.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Order.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Order.objLogObject.LoginLogID = objToken.LoginLogID;
            objOrder.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_Order.Update(objOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strOrderID, string strDeletedContent)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.BO.Order objOrder = new ERP.Procurement.BO.Order();
            ERP.Procurement.DA.DA_Order objDA_Order = new ERP.Procurement.DA.DA_Order();
            objDA_Order.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Order.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Order.objLogObject.LoginLogID = objToken.LoginLogID;
            objOrder.OrderID = strOrderID;
            objOrder.DeletedUser = objToken.UserName;
            objOrder.DeletedContent = strDeletedContent;

            objResultMessage = objDA_Order.Delete(objOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDataNew(String strAuthenData, String strGUID, ref DataSet dsResult, int intPOTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order().LoadDataNew(ref dsResult, intPOTypeID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDataEdit(String strAuthenData, String strGUID, ref DataSet dsResult, string strOrderID, int intPOTypeID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order().LoadDataEdit(ref dsResult, strOrderID, intPOTypeID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDataFromPOType(String strAuthenData, String strGUID, ref DataTable dsResult, DataSet dsCond, int intPOTypeID, string strMainGroupID, string strBrandID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order_ReviewList().LoadDataFromPOType(ref dsResult, dsCond, intPOTypeID, strMainGroupID, strBrandID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetOrder_NewID(String strAuthenData, String strGUID, ref string Order_ID, int intInputStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order().GetOrder_NewID(ref Order_ID, intInputStoreID);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateStatusReview(String strAuthenData, String strGUID, ERP.Procurement.BO.Order_ReviewList objOrder_ReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_Order_ReviewList objDA_Order_ReviewList = new ERP.Procurement.DA.DA_Order_ReviewList();
            objResultMessage = objDA_Order_ReviewList.Update(objOrder_ReviewList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateStatus_WF_Process(String strAuthenData, String strGUID, ERP.Procurement.BO.Order_WF_ProcessUser objOrder_WF_ProcessUser)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_Order_WF_ProcessUser objDA_Order_WF_ProcessUser = new ERP.Procurement.DA.DA_Order_WF_ProcessUser();
            objResultMessage = objDA_Order_WF_ProcessUser.Update(objOrder_WF_ProcessUser);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchOrderSuggestion(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order().SearchOrderSuggestion(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderSuggestionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchOrderSuggestion_New(String strAuthenData, String strGUID, ref Byte[] byteList, params object[] objKeywords)
        {
            DataTable tblResult = null;
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order().SearchOrderSuggestion_New(ref tblResult, objKeywords);
            if (tblResult != null)
            {
                tblResult.TableName = "OrderSuggestionList";
                if (!objResultMessage.IsError && tblResult != null)
                    byteList = ConvertDataSetToByte(tblResult);
            }
            return objResultMessage;
        }

        private Byte[] ConvertDataSetToByte(DataTable dsData)
        {
            System.IO.MemoryStream objMemoryStream = new System.IO.MemoryStream();
            System.IO.Compression.GZipStream objGZipStream = new System.IO.Compression.GZipStream(objMemoryStream, System.IO.Compression.CompressionMode.Compress);
            dsData.WriteXml(objGZipStream, XmlWriteMode.WriteSchema);
            objGZipStream.Close();
            Byte[] objByte = objMemoryStream.ToArray();
            objMemoryStream.Close();
            return objByte;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckVATCustomerOrderID(String strAuthenData, String strGUID, ref DataTable dsResult, string strCustomerIDlist, int intType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order().checkVATCustomerOrderID(ref dsResult, strCustomerIDlist, intType);
            if (dsResult != null)
                dsResult.TableName = "CustomerOrderIDList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertListOrderMonth(String strAuthenData, String strGUID, ERP.Procurement.BO.OrderMonthParent objOrderMonthParent, List<ERP.Procurement.BO.OrderMonth> lstOrderMonth)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_OrderMonth().InsertListOrderMonth(objOrderMonthParent, lstOrderMonth);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateListOrderMonth(String strAuthenData, String strGUID, ERP.Procurement.BO.OrderMonthParent objOrderMonthParent, List<ERP.Procurement.BO.OrderMonth> lstOrderMonth)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_OrderMonth().UpdateListOrderMonth(objOrderMonthParent, lstOrderMonth);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetAllMainGroupSubGroup(String strAuthenData, String strGUID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_OrderMonth().GetAllMainGroup_SubGroup(ref tblResult);
            if (tblResult != null)
                tblResult.TableName = "MAINGROUP_SUBGROUP";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetByParent(String strAuthenData, String strGUID, string parentID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_OrderMonth().GetByParent(parentID, ref tblResult);
            if (tblResult != null)
                tblResult.TableName = "POM_ORDERMONTH";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPlanOrderCheck(String strAuthenData, String strGUID, DateTime startDate, DateTime endDate, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_OrderMonth().GetPlanOrderCheck(startDate, endDate, ref tblResult);
            if (tblResult != null)
                tblResult.TableName = "POM_ORDERMONTH_CHECK";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage POM_ORDERMONT_Review(String strAuthenData, String strGUID, string pomParenID, int reviewLevelID, string user, bool approve, string reason)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_OrderMonth().Review(pomParenID, reviewLevelID, user, approve, reason);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataOrderMonthParent(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_OrderMonthParent().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "OrderMonthParentList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetNewID(String strAuthenData, String strGUID, ref string strNewProductID, string month)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_OrderMonthParent().GetNewID(ref strNewProductID, month);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteOrderMonth(String strAuthenData, String strGUID, ERP.Procurement.BO.OrderMonthParent objOrderMonthParent)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_OrderMonthParent().Delete(objOrderMonthParent);
            return objResultMessage;
        }
    }
}
