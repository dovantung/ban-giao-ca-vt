﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
namespace ERPTransactionServices.Procurement
{
    /// <summary>
    /// Summary description for WSOrder_B2B
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSOrder_B2B : System.Web.Services.WebService
    {
         [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Procurement.BO.Order_B2B objOrder_B2B, string strB2BID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order_B2B().LoadInfo(ref objOrder_B2B, strB2BID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order_B2B().SearchData(ref tblResult,objKeywords);
            if (tblResult != null)
                tblResult.TableName = "Order_B2BList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage ReOrder(String strAuthenData, String strGUID, string strB2BID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_Order_B2B objDA_Order_B2B = new ERP.Procurement.DA.DA_Order_B2B();
            ERP.Procurement.BO.Order_B2B objOrder_B2B = new ERP.Procurement.BO.Order_B2B();
            objOrder_B2B.B2BID = strB2BID.Trim();
            objOrder_B2B.UpdateLogUser = objToken.UserName.Trim();
            objResultMessage = objDA_Order_B2B.ReOrder(objOrder_B2B);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadLogByID(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Order_B2B().LoadLogByID(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "B2BLogList";
            return objResultMessage;
        }
    }
}
