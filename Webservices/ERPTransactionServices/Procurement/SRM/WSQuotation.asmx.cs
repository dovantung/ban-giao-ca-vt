﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Procurement.SRM
{
    /// <summary>
    /// Summary description for WSQUOTATION
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSQuotation : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Procurement.BO.Quotation objQuotation, string strProductID, int intCustomerBranchID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Quotation().LoadInfo(ref objQuotation, strProductID, intCustomerBranchID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Quotation().SearchData(ref dtbData, objKeywords);
            if (dtbData != null)
                dtbData.TableName = "QuotationList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadHistory(String strAuthenData, String strGUID, ref DataTable dtbData, string strProductID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_Quotation().LoadHistory(ref dtbData, strProductID);
            if (dtbData != null)
                dtbData.TableName = "QuotationHistory";
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Procurement.BO.Quotation objQuotation)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_Quotation objDA_Quotation = new ERP.Procurement.DA.DA_Quotation();
            objDA_Quotation.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Quotation.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Quotation.objLogObject.LoginLogID = objToken.LoginLogID;
            objQuotation.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Quotation.Insert(objQuotation);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearQuotationCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Procurement.BO.Quotation objQuotation)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_Quotation objDA_Quotation = new ERP.Procurement.DA.DA_Quotation();
            objDA_Quotation.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Quotation.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Quotation.objLogObject.LoginLogID = objToken.LoginLogID;
            objQuotation.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_Quotation.Update(objQuotation);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearQuotationCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Procurement.BO.Quotation objQuotation)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_Quotation objDA_Quotation = new ERP.Procurement.DA.DA_Quotation();
            objDA_Quotation.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Quotation.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Quotation.objLogObject.LoginLogID = objToken.LoginLogID;
            objQuotation.DeletedUser = objToken.UserName;
            objResultMessage = objDA_Quotation.Delete(objQuotation);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearQuotationCache();
            return objResultMessage;
        }
    }
}
