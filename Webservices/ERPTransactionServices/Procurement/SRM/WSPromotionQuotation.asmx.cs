﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.Procurement.SRM
{
    /// <summary>
    /// Summary description for WSQUOTATION
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSPromotionQuotation : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.Procurement.BO.PromotionQuotation objPromotionQuotation, string strPromotionQuotationID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PromotionQuotation().LoadInfo(ref objPromotionQuotation, strPromotionQuotationID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.DA_PromotionQuotation().SearchData(ref dtbData, objKeywords);
            if (dtbData != null)
                dtbData.TableName = "PromotionQuotationList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Procurement.BO.PromotionQuotation objPromotionQuotation)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_PromotionQuotation objDA_PromotionQuotation = new ERP.Procurement.DA.DA_PromotionQuotation();
            objDA_PromotionQuotation.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PromotionQuotation.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PromotionQuotation.objLogObject.LoginLogID = objToken.LoginLogID;
            objPromotionQuotation.CreatedUser = objToken.UserName;
            objResultMessage = objDA_PromotionQuotation.Insert(objPromotionQuotation);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearPromotionQuotationCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.Procurement.BO.PromotionQuotation objPromotionQuotation)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_PromotionQuotation objDA_PromotionQuotation = new ERP.Procurement.DA.DA_PromotionQuotation();
            objDA_PromotionQuotation.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PromotionQuotation.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PromotionQuotation.objLogObject.LoginLogID = objToken.LoginLogID;
            objPromotionQuotation.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_PromotionQuotation.Update(objPromotionQuotation);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearPromotionQuotationCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.Procurement.BO.PromotionQuotation objPromotionQuotation)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.DA_PromotionQuotation objDA_PromotionQuotation = new ERP.Procurement.DA.DA_PromotionQuotation();
            objDA_PromotionQuotation.objLogObject.CertificateString = objToken.CertificateString;
            objDA_PromotionQuotation.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_PromotionQuotation.objLogObject.LoginLogID = objToken.LoginLogID;
            objPromotionQuotation.DeletedUser = objToken.UserName;
            objResultMessage = objDA_PromotionQuotation.Delete(objPromotionQuotation);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearPromotionQuotationCache();
            return objResultMessage;
        }
    }
}
