﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Procurement
{
    /// <summary>
    /// Summary description for WSSamSungOrder
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSSamSungOrder : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetNewID(String strAuthenData, String strGUID, ref string strNewSSOrderID, string month)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder().GetNewID(ref strNewSSOrderID, month);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.Procurement.BO.SamSungOrder.SamSungOrder objOrder, List<ERP.Procurement.BO.SamSungOrder.SamSungOrderDetail> lstDetail)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder objDA_Order = new ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder();
            objOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Order.Insert(objOrder, lstDetail);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateStatus(String strAuthenData, String strGUID, ERP.Procurement.BO.SamSungOrder.SamSungOrder objOrder)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder objDA_Order = new ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder();
            objOrder.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Order.Update(objOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateSentStatus(String strAuthenData, String strGUID, string SSOID, string strStoreList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder objDA_Order = new ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder();
            objResultMessage = objDA_Order.UpdateSentStatus(SSOID, strStoreList);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSSOrder(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "SamSungOrderList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSSOrderDetail(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Procurement.DA.SamSungOrder.DA_SamSungOrderDetail().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "SamSungOrderDetailList";
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strSSOrderID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.Procurement.BO.SamSungOrder.SamSungOrder objOrder = new ERP.Procurement.BO.SamSungOrder.SamSungOrder();
            ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder objDA_SSOrder = new ERP.Procurement.DA.SamSungOrder.DA_SamSungOrder();
            objOrder.SamSungOrderID = strSSOrderID;
            objOrder.DeletedUser = objToken.UserName;
            objResultMessage = objDA_SSOrder.Delete(objOrder);
            //if (!objResultMessage.IsError)
            //    DataCache.WSDataCache.ClearOrderCache();
            return objResultMessage;
        }


    }
}
