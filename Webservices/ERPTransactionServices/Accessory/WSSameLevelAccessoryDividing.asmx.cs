﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ERPTransactionServices.Accessory
{
    /// <summary>
    /// Summary description for WSSameLevelAccessoryDividing
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSSameLevelAccessoryDividing : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtResult, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Accessory.DA_SameLevelAccessoryDividing().SearchData(ref dtResult, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchRotatingHistory(String strAuthenData, String strGUID, ref DataTable dtRedundance, ref DataTable dtLack,
            ref DataTable dtGoods, ref DataTable dtShop, ref DataTable dtResult,
            String goods_rotate_history_id, String size)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Accessory.DA_SameLevelAccessoryDividing().SearchRotatingHistory(ref dtRedundance, ref dtLack,
            ref dtGoods, ref dtShop, ref dtResult, goods_rotate_history_id, size);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage AccessRotate(String strAuthenData, String strGUID, ref String goods_rotate_history_id, object[] objKeywords, object[][] objGoods, object[] objShops)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Accessory.DA_SameLevelAccessoryDividing().AccessRotate(ref goods_rotate_history_id, objKeywords, objGoods, objShops);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage RecyclingRotateAccess(String strAuthenData, String strGUID, ref DataTable dtResult, String goods_rotate_history_id)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Accessory.DA_SameLevelAccessoryDividing().RecyclingRotateAccess(ref dtResult, goods_rotate_history_id);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetAccessRotate(String strAuthenData, String strGUID, ref DataTable dtResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.Inventory.DA.Accessory.DA_SameLevelAccessoryDividing().GetAccessRotate(ref dtResult);
            return objResultMessage;
        }
    }
}
