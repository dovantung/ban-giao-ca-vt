﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using ERP.HRM.Employee.BO.Transfer;

namespace ERPTransactionServices.HRM.Transfer
{
    /// <summary>
    /// Summary description for WSTransferStaff
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSTransferStaff : System.Web.Services.WebService
    {

            [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Transfer.HR_TransferStaff objTransferStaff, int intTransferID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Transfer.DA_TransferStaff().LoadInfo(ref objTransferStaff, intTransferID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Transfer.HR_TransferStaff objTransferStaff, ref int intIsExists, ref string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Transfer.DA_TransferStaff objDA_Transfer = new ERP.HRM.Employee.DA.Transfer.DA_TransferStaff();
            objTransferStaff.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Transfer.Insert(objTransferStaff, ref intIsExists, ref strUserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Transfer.HR_TransferStaff objTransferStaff)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Transfer.DA_TransferStaff objDA_Transfer = new ERP.HRM.Employee.DA.Transfer.DA_TransferStaff();
            objResultMessage = objDA_Transfer.Update(objTransferStaff);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intTransferID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.BO.Transfer.HR_TransferStaff objTransferStaff = new ERP.HRM.Employee.BO.Transfer.HR_TransferStaff();
            ERP.HRM.Employee.DA.Transfer.DA_TransferStaff objDA_Transfer = new ERP.HRM.Employee.DA.Transfer.DA_TransferStaff();
            objTransferStaff.TransferID = intTransferID;
            objTransferStaff.DeletedUser = objToken.UserName;
            objResultMessage = objDA_Transfer.Delete(objTransferStaff);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateIsReviewed(String strAuthenData, String strGUID, HR_TransferStaff objTransferStaffStaff)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Transfer.DA_TransferStaff objDA_Transfer = new ERP.HRM.Employee.DA.Transfer.DA_TransferStaff();
            objTransferStaffStaff.ReviewUser = objToken.UserName;
            objResultMessage = objDA_Transfer.UpdateIsReviewed(objTransferStaffStaff);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, object[] objKeyword, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Transfer.DA_TransferStaff().SearchData(ref tblResult, objKeyword);
            if (tblResult != null)
                tblResult.TableName = "TransferList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSuggest(String strAuthenData, String strGUID, object[] objKeyword, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Transfer.DA_TransferStaff().SearchDataSuggest(ref tblResult, objKeyword);
            if (tblResult != null)
                tblResult.TableName = "TransferSuggestList";
            return objResultMessage;
        }

        //[WebMethod(EnableSession = true)]
        //public Library.WebCore.ResultMessage SendEmail(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Transfer.MailInfo objMailInfo)
        //{
        //    Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
        //    Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
        //    if (objToken == null)
        //        return objResultMessage;
        //    ERP.HRM.Employee.DA.Transfer.DA_TransferStaff objDA_TransferStaff = new ERP.HRM.Employee.DA.Transfer.DA_TransferStaff();
        //    objResultMessage = objDA_TransferStaff.SendEmail(objMailInfo);
        //    return objResultMessage;
        //}

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertSendMail(String strAuthenData, String strGUID, params object[] objKeyWords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Transfer.DA_TransferStaff objDA_TransferStaff = new ERP.HRM.Employee.DA.Transfer.DA_TransferStaff();
            objResultMessage = objDA_TransferStaff.InsertSendMail(objKeyWords);
            return objResultMessage;
        }
    }
}
