﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.HRM.BusinessTravel
{
    /// <summary>
    /// Summary description for WSBusinessTravel
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSBusinessTravel : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel objBusinessTravel, string strBusinessTravelID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel().LoadInfo(ref objBusinessTravel, strBusinessTravelID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ref string strBusinessTravelID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel objBusinessTravel, DataTable dtbPartyStaff, DataTable dtbSupport, DataTable dtbAdvanceItem, DataTable dtbPaidItem, DataTable dtbReviewList, ref DataTable dtbAttachment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel objDA_BusinessTravel = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel();
            objDA_BusinessTravel.objLogObject.CertificateString = objToken.CertificateString;
            objDA_BusinessTravel.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_BusinessTravel.objLogObject.LoginLogID = objToken.LoginLogID;
            objBusinessTravel.CreatedUser = objToken.UserName;
            objResultMessage = objDA_BusinessTravel.Insert(ref strBusinessTravelID, objBusinessTravel, dtbPartyStaff, dtbSupport, dtbAdvanceItem, dtbPaidItem, dtbReviewList, ref dtbAttachment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strBusinessTravelID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel objBusinessTravel = new ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel();
            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel objDA_BusinessTravel = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel();
            objDA_BusinessTravel.objLogObject.CertificateString = objToken.CertificateString;
            objDA_BusinessTravel.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_BusinessTravel.objLogObject.LoginLogID = objToken.LoginLogID;
            objBusinessTravel.BusinessTravelID = strBusinessTravelID;
            objBusinessTravel.DeletedUser = objToken.UserName;
            objResultMessage = objDA_BusinessTravel.Delete(objBusinessTravel);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel objBusinessTravel, DataTable dtbPartyStaff, DataTable dtbSupport, DataTable dtbAdvanceItem, DataTable dtbPaidItem, DataTable dtbReviewList, DataTable dtbAttachment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel objDA_BusinessTravel = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel();
            objDA_BusinessTravel.objLogObject.CertificateString = objToken.CertificateString;
            objDA_BusinessTravel.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_BusinessTravel.objLogObject.LoginLogID = objToken.LoginLogID;
            objBusinessTravel.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BusinessTravel.Update(objBusinessTravel, dtbPartyStaff, dtbSupport, dtbAdvanceItem, dtbPaidItem, dtbReviewList, dtbAttachment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BusinessTravel";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataRVList(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_ReviewList().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "HR_BUSINESSTRAVEL_RVLIST_SRH";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataPartyStaff(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_PartyStaff().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BusinessTravel_PartyStaff";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataSupport(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_Support().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BusinessTravel_Support";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataAdvance(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_AdvanceItem().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BusinessTravel_Advance";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataPaid(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_PaidItem().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BusinessTravel_Paid";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataAttachment(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_Attachment().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "BusinessTravel_Attachment";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteAttachment(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel_Attachment objAttachment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_Attachment objDA_Attachment = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_Attachment();
            objAttachment.DeletedUser = objToken.UserName;
            objResultMessage = objDA_Attachment.Delete(objAttachment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeletePartyStaff(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel_PartyStaff objPartyStaff)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            
            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_PartyStaff objDA_PartyStaff = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_PartyStaff();
            objPartyStaff.DeletedUser = objToken.UserName;
            objResultMessage = objDA_PartyStaff.Delete(objPartyStaff);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteSupport(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel_Support objSupport)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_Support objDA_Support = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_Support();
            objSupport.DeletedUser = objToken.UserName;
            objResultMessage = objDA_Support.Delete(objSupport);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteAdvanceItem(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel_AdvanceItem objAdvanceItem)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_AdvanceItem objDA_AdvanceItem = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_AdvanceItem();
            objAdvanceItem.DeletedUser = objToken.UserName;
            objResultMessage = objDA_AdvanceItem.Delete(objAdvanceItem);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeletePaidItem(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel_PaidItem objPaidItem)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_PaidItem objDA_PaidItem = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_PaidItem();
            objPaidItem.DeletedUser = objToken.UserName;
            objResultMessage = objDA_PaidItem.Delete(objPaidItem);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewLevel(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel_ReviewList objReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_ReviewList objDA_ReviewList = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_ReviewList();
            objReviewList.UpdatedUser = objToken.UserName;
            objReviewList.CreatedUser = objToken.UserName;
            objResultMessage = objDA_ReviewList.UpdateReviewLevel(objReviewList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateLastReviewLevel(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel_ReviewList objReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_ReviewList objDA_ReviewList = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_ReviewList();
            objReviewList.UpdatedUser = objToken.UserName;
            objReviewList.CreatedUser = objToken.UserName;
            objResultMessage = objDA_ReviewList.UpdateLastReviewLevel(objReviewList, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateIsAdvance(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel objBusinessTravel)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel objDA_BusinessTravel = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel();
            objBusinessTravel.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BusinessTravel.UpdateIsAdvance(objBusinessTravel);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateIsSettlement(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.BusinessTravel.HR_BusinessTravel objBusinessTravel)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel objDA_BusinessTravel = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel();
            objBusinessTravel.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_BusinessTravel.UpdateIsSettlement(objBusinessTravel);
            return objResultMessage;
        }

        //[WebMethod(EnableSession = true)]
        //public Library.WebCore.ResultMessage UpdatePathAttachment(String strAuthenData, String strGUID, DataTable dtbAttachment)
        //{
        //    Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
        //    Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
        //    if (objToken == null)
        //        return objResultMessage;

        //    ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_Attachment objDA_BusinessTravel_Attachment = new ERP.HRM.Payroll.DA.BusinessTravel.DA_HR_BusinessTravel_Attachment();
        //    objResultMessage = objDA_BusinessTravel_Attachment.UpdatePath(dtbAttachment);
        //    return objResultMessage;
        //}

    }
}
