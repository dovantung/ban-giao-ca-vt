﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using ERP.SalesAndServices.Payment.DA;
using ERP.SalesAndServices.Payment.BO;
using ERP.HRM.Employee.BO;
namespace ERPTransactionServices.HRM.Office
{
    /// <summary>
    /// Summary description for WSOfficeChangeRequest
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/", Description = "Service yêu cầu chuyển kho làm việc")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSOfficeChangeRequest : System.Web.Services.WebService
    {

        #region OfficeChangeRequest
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Office.OfficeChangeRequest objOfficeChangeRequest, string intOfficeChangeRequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest().LoadInfo(ref objOfficeChangeRequest, intOfficeChangeRequestID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataToList(String strAuthenData, String strGUID, ref List<ERP.HRM.Employee.BO.Office.OfficeChangeRequest> listOfficeChangeRequest, params object[] objKeywors)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest().SearchDataToList(ref listOfficeChangeRequest, objKeywors);
            return objResultMessage;
        }
      
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Office.OfficeChangeRequest objOfficeChangeRequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest objDA_OfficeChangeRequest = new ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest();
            objOfficeChangeRequest.CreatedUser = objToken.UserName;
            objDA_OfficeChangeRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_OfficeChangeRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_OfficeChangeRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_OfficeChangeRequest.Insert(objOfficeChangeRequest);
           
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Office.OfficeChangeRequest objOfficeChangeRequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest objDA_OfficeChangeRequest = new ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest();
            objDA_OfficeChangeRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_OfficeChangeRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_OfficeChangeRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objOfficeChangeRequest.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_OfficeChangeRequest.Update(objOfficeChangeRequest);


            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Review(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Office.OfficeChangeRequest objOfficeChangeRequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest objDA_OfficeChangeRequest = new ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest();
            objDA_OfficeChangeRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_OfficeChangeRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_OfficeChangeRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objOfficeChangeRequest.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_OfficeChangeRequest.Review(objOfficeChangeRequest);
            //Library.WebCore.ProgramCache.ClearCache("ERPMasterDataServices.DataCache-GetStore-" + objOfficeChangeRequest.RequestUser);
            //Library.WebCore.ProgramCache.ClearMemCache("ERPMasterDataServices.DataCache-GetStore-" + objOfficeChangeRequest.RequestUser);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Office.OfficeChangeRequest objOfficeChangeRequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest objDA_OfficeChangeRequest = new ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest();
            objDA_OfficeChangeRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_OfficeChangeRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_OfficeChangeRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objOfficeChangeRequest.DeletedUser = objToken.UserName;
            objResultMessage = objDA_OfficeChangeRequest.Delete(objOfficeChangeRequest);
            return objResultMessage;
        }
   


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] keyWord)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest().SearchData(ref tblResult, keyWord);
            if (tblResult != null)
                tblResult.TableName = "OfficeChangeRequestList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateNewID(String strAuthenData, String strGUID, ref string strID, int intCreatedStoreID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Office.DA_OfficeChangeRequest().CreateNewID(ref strID, intCreatedStoreID);
            return objResultMessage;
        }
        #endregion
        
    }
}
