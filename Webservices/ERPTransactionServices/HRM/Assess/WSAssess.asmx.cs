﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.HRM.Assess
{
    /// <summary>
    /// Summary description for WSAssess
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSAssess : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Assess.Assess objAssess, int intAssessID, string strUserName, int intAssessTermID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Assess.DA_Assess().LoadInfo(ref objAssess, intAssessID, strUserName, intAssessTermID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Assess.Assess objAssess)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Assess.DA_Assess objDA_Assess = new ERP.HRM.Employee.DA.Assess.DA_Assess();
            objDA_Assess.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Assess.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Assess.objLogObject.LoginLogID = objToken.LoginLogID;
            objAssess.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Assess.Insert(objAssess);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Assess.Assess objAssess)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Assess.DA_Assess objDA_Assess = new ERP.HRM.Employee.DA.Assess.DA_Assess();
            objDA_Assess.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Assess.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Assess.objLogObject.LoginLogID = objToken.LoginLogID;
            objAssess.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_Assess.Update(objAssess);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intAssessID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.BO.Assess.Assess objAssess = new ERP.HRM.Employee.BO.Assess.Assess();
            ERP.HRM.Employee.DA.Assess.DA_Assess objDA_Assess = new ERP.HRM.Employee.DA.Assess.DA_Assess();
            objDA_Assess.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Assess.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Assess.objLogObject.LoginLogID = objToken.LoginLogID;
            objAssess.AssessID = intAssessID;
            objAssess.DeletedUser = objToken.UserName;
            objResultMessage = objDA_Assess.Delete(objAssess);
            return objResultMessage;
        }

        //[WebMethod(EnableSession = true)]
        //public Library.WebCore.ResultMessage UpdateIsReviewed(String strAuthenData, String strGUID, int intAssessID)
        //{
        //    Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
        //    Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
        //    if (objToken == null)
        //        return objResultMessage;
        //    ERP.HRM.Employee.BO.Assess.Assess objAssess = new ERP.HRM.Employee.BO.Assess.Assess();
        //    ERP.HRM.Employee.DA.Assess.DA_Assess objDA_Assess = new ERP.HRM.Employee.DA.Assess.DA_Assess();
        //    objDA_Assess.objLogObject.CertificateString = objToken.CertificateString;
        //    objDA_Assess.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
        //    objDA_Assess.objLogObject.LoginLogID = objToken.LoginLogID;
        //    objAssess.AssessID = intAssessID;
        //    objAssess.ReviewedUser = objToken.UserName;
        //    objResultMessage = objDA_Assess.UpdateIsReviewed(objAssess);
        //    return objResultMessage;
        //}

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, object[] objKeyword, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Assess.DA_Assess().SearchData(ref tblResult, objKeyword);
            if (tblResult != null)
                tblResult.TableName = "AssessList";
            return objResultMessage;
        }
    }
}
