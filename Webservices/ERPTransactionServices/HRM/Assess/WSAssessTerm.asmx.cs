﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.HRM.Assess
{
    /// <summary>
    /// Summary description for WSAssessTerm
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSAssessTerm : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Assess.AssessTerm objAssessTerm, int intAssessTermID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Assess.DA_AssessTerm().LoadInfo(ref objAssessTerm, intAssessTermID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Assess.AssessTerm objAssessTerm)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Assess.DA_AssessTerm objDA_AssessTerm = new ERP.HRM.Employee.DA.Assess.DA_AssessTerm();
            objDA_AssessTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AssessTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AssessTerm.objLogObject.LoginLogID = objToken.LoginLogID;
            objAssessTerm.CreatedUser = objToken.UserName;
            objResultMessage = objDA_AssessTerm.Insert(objAssessTerm);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Assess.AssessTerm objAssessTerm, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Assess.DA_AssessTerm objDA_AssessTerm = new ERP.HRM.Employee.DA.Assess.DA_AssessTerm();
            objDA_AssessTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AssessTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AssessTerm.objLogObject.LoginLogID = objToken.LoginLogID;
            objAssessTerm.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_AssessTerm.Update(objAssessTerm, lstOrderIndex);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intAssessTermID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.BO.Assess.AssessTerm objAssessTerm = new ERP.HRM.Employee.BO.Assess.AssessTerm();
            ERP.HRM.Employee.DA.Assess.DA_AssessTerm objDA_AssessTerm = new ERP.HRM.Employee.DA.Assess.DA_AssessTerm();
            objDA_AssessTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AssessTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AssessTerm.objLogObject.LoginLogID = objToken.LoginLogID;
            objAssessTerm.AssessTermID = intAssessTermID;
            objAssessTerm.DeletedUser = objToken.UserName;
            objResultMessage = objDA_AssessTerm.Delete(objAssessTerm);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateIsReviewed(String strAuthenData, String strGUID, int intAssessTermID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.BO.Assess.AssessTerm objAssessTerm = new ERP.HRM.Employee.BO.Assess.AssessTerm();
            ERP.HRM.Employee.DA.Assess.DA_AssessTerm objDA_AssessTerm = new ERP.HRM.Employee.DA.Assess.DA_AssessTerm();
            objDA_AssessTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AssessTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AssessTerm.objLogObject.LoginLogID = objToken.LoginLogID;
            objAssessTerm.AssessTermID = intAssessTermID;
            objAssessTerm.ReviewedUser = objToken.UserName;
            objResultMessage = objDA_AssessTerm.UpdateIsReviewed(objAssessTerm);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, object[] objKeyword, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Assess.DA_AssessTerm().SearchData(ref tblResult, objKeyword);
            if (tblResult != null)
                tblResult.TableName = "AssessTermList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InitData(String strAuthenData, String strGUID, object[] objKeyword, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Assess.DA_AssessTerm().InitData(ref tblResult, objKeyword);
            if (tblResult != null)
                tblResult.TableName = "AssessDetailList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataCache(String strAuthenData, String strGUID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Assess.DA_AssessTerm().GetDataCache(ref tblResult);
            if (tblResult != null)
                tblResult.TableName = "AssessTermList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchByAssessType(String strAuthenData, String strGUID, object[] objKeyword, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Assess.DA_AssessTerm().SearchByAssessType(ref tblResult, objKeyword);
            if (tblResult != null)
                tblResult.TableName = "AssessTermList";
            return objResultMessage;
        }
    }
}
