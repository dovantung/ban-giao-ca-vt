﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.HRM.TO
{
    /// <summary>
    /// Summary description for WSTimeOff
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSTimeOff : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Payroll.BO.TO.TimeOff objTimeOff, decimal decTimeOffID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().LoadInfo(ref objTimeOff, decTimeOffID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.TO.TimeOff objTimeOff)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.TO.DA_TimeOff objDA_TimeOff = new ERP.HRM.Payroll.DA.TO.DA_TimeOff();
            objDA_TimeOff.objLogObject.CertificateString = objToken.CertificateString;
            objDA_TimeOff.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_TimeOff.objLogObject.LoginLogID = objToken.LoginLogID;
            objTimeOff.CertificateString = objToken.CertificateString;
            objTimeOff.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objTimeOff.CreatedUser = objToken.UserName;
            objResultMessage = objDA_TimeOff.Insert(objTimeOff);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intTimeOffID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.BO.TO.TimeOff objTimeOff = new ERP.HRM.Payroll.BO.TO.TimeOff();
            ERP.HRM.Payroll.DA.TO.DA_TimeOff objDA_TimeOff = new ERP.HRM.Payroll.DA.TO.DA_TimeOff();
            objDA_TimeOff.objLogObject.CertificateString = objToken.CertificateString;
            objDA_TimeOff.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_TimeOff.objLogObject.LoginLogID = objToken.LoginLogID;
            objTimeOff.TimeOffID = intTimeOffID;
            objTimeOff.DeletedUser = objToken.UserName;
            objResultMessage = objDA_TimeOff.Delete(objTimeOff);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.TO.TimeOff objTimeOff)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.TO.DA_TimeOff objDA_TimeOff = new ERP.HRM.Payroll.DA.TO.DA_TimeOff();
            objDA_TimeOff.objLogObject.CertificateString = objToken.CertificateString;
            objDA_TimeOff.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_TimeOff.objLogObject.LoginLogID = objToken.LoginLogID;
            objTimeOff.CertificateString = objToken.CertificateString;
            objTimeOff.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objTimeOff.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_TimeOff.Update(objTimeOff);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReview(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.TO.TimeOff objTimeOff)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.TO.DA_TimeOff objDA_TimeOff = new ERP.HRM.Payroll.DA.TO.DA_TimeOff();
            objTimeOff.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_TimeOff.UpdateReview(objTimeOff);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewListTimeOff(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.TO.TimeOff objTimeOff)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objTimeOff.CreatedUser = objToken.UserName;
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().UpdateReviewList(objTimeOff);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewList(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.TO.TimeOff_ReviewList objTimeOff_ReviewList, string strUserRegister)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().UpdateReviewList(objTimeOff_ReviewList, strUserRegister, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateCumulationTimeOff(String strAuthenData, String strGUID, string strUserName, decimal decDays)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.TO.DA_TimeOff objDA_TimeOff = new ERP.HRM.Payroll.DA.TO.DA_TimeOff();
            //objTimeOff.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_TimeOff.UpdateCumulationTimeOff(strUserName, decDays);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TimeOffList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataRVList(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff_ReviewList().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TimeOffRVList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataRVL(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().SearchDataRVL(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TimeOffRVLList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectByCode(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().SelectByCode(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TimeOffList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectByDateForMB(String strAuthenData, String strGUID, ref DataTable tblResult, string strUserName, DateTime dteFromDate, DateTime dteToDate, decimal decTimeOffID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            object[] objKeywords = new object[] { "@UserName", strUserName, "@FromDate", dteFromDate, "@ToDate", dteToDate, "@TimeOffID", decTimeOffID };
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().SelectByCode(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TimeOffList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectCumulationTimeOff(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().SelectCumulationTimeOff(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TimeOffList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SelectCumulationTimeOffForMB(String strAuthenData, String strGUID, ref DataTable tblResult, string strUserName, decimal decDay, decimal decTimeOffID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            object[] objKeywords = new object[] { "@UserName", strUserName, "@Days", decDay, "@TimeOffID", decTimeOffID };
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().SelectCumulationTimeOff(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TimeOffList";
            return objResultMessage;
        }
        
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataTimeOffByUserForMB(String strAuthenData, String strGUID, ref DataTable tblResult, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            object[] objKeywords = new object[] { "@UserName", strUserName };
            objResultMessage = new ERP.HRM.Payroll.DA.TO.DA_TimeOff().SearchDataTimeOffByUserForMB(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TimeOffByUserList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateCumulationTimeOffByList(String strAuthenData, String strGUID, DataTable dtbForUpdate, DataTable dtbTimeOffTypeCache)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.TO.DA_TimeOff objDA_TimeOff = new ERP.HRM.Payroll.DA.TO.DA_TimeOff();
            objResultMessage = objDA_TimeOff.UpdateCumulationTimeOffByList(dtbForUpdate,dtbTimeOffTypeCache);
            return objResultMessage;
        }

    }
}
