﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using ERP.SalesAndServices.Payment.DA;
using ERP.SalesAndServices.Payment.BO;
using ERP.HRM.Employee.BO;
namespace ERPTransactionServices.HRM.Candidate
{
    /// <summary>
    /// Summary description for WSCandidate
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/", Description = "Service hồ sơ nhân sự")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSCandidate : System.Web.Services.WebService
    {

        #region Candidate
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Candidate objCandidate, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Candidate().LoadInfo(ref objCandidate,  strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataToList(String strAuthenData, String strGUID, ref List<ERP.HRM.Employee.BO.Candidate> listCandidate, params object[] objKeywors)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //objResultMessage = new ERP.HRM.Employee.DA.DA_Candidate().SearchDataToList(ref listCandidate, objKeywors);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckIdCard(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Candidate objCandidate, string strUserName,string strIDCard)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //objResultMessage = new ERP.HRM.Employee.DA.DA_Candidate().CheckIdCard(ref objCandidate, strUserName, strIDCard);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Candidate objCandidate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.DA_Candidate objDA_Candidate = new ERP.HRM.Employee.DA.DA_Candidate();
            objCandidate.CreatedUser = objToken.UserName;
            //objDA_Candidate.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_Candidate.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_Candidate.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Candidate.Insert(objCandidate);
           
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Candidate objCandidate)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.DA_Candidate objDA_Candidate = new ERP.HRM.Employee.DA.DA_Candidate();
            //objDA_Candidate.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_Candidate.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_Candidate.objLogObject.LoginLogID = objToken.LoginLogID;
            objCandidate.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_Candidate.Update(objCandidate);


            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strCandidateID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.DA_Candidate objDA_Candidate = new ERP.HRM.Employee.DA.DA_Candidate();
            ERP.HRM.Employee.BO.Candidate objCandidate = new ERP.HRM.Employee.BO.Candidate();
            //objDA_Candidate.objLogObject.CertificateString = objToken.CertificateString;
            //objDA_Candidate.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            //objDA_Candidate.objLogObject.LoginLogID = objToken.LoginLogID;
            objCandidate.CandidateID = strCandidateID;
            objCandidate.DeletedUser = objToken.UserName;
            objResultMessage = objDA_Candidate.Delete(objCandidate);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] keyWords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Candidate().SearchData(ref tblResult, keyWords);
            if (tblResult != null)
                tblResult.TableName = "CandidateList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataFromStore(String strAuthenData, String strGUID, string store, ref DataTable tblResult, params object[] keyWord)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //objResultMessage = new ERP.HRM.Employee.DA.DA_Candidate().SearchDataFromStore(store,ref tblResult, keyWord);
            if (tblResult != null)
                tblResult.TableName = store;
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataFromListParamter(String strAuthenData, String strGUID, ref DataTable dtb,
            string strReportStoreName, List<ERP.MasterData.DA.Parameter> listParameter)
        {

            object[] objKeywords = null;
            if (listParameter != null)
            {
                List<object> listKeyWords = new List<object>();
                foreach (var item in listParameter)
                {
                    listKeyWords.Add(item.Key);
                    if (item.DateTimeValue != null)
                        listKeyWords.Add(item.DateTimeValue.Value);
                    else
                        listKeyWords.Add(item.Value);
                }
                objKeywords = listKeyWords.ToArray();
            }
            return SearchDataFromStore(strAuthenData, strGUID,  strReportStoreName,ref dtb, objKeywords);
        }
        #endregion

        #region Detail
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadEducationList(String strAuthenData, String strGUID, ref List<Candidate_EducationList> listCandidate_EducationList, string strEducationListID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //objResultMessage = new ERP.HRM.Employee.DA.DA_Candidate_EducationList().SearchData(ref listCandidate_EducationList, strEducationListID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadCertification(String strAuthenData, String strGUID, ref List<Candidate_Certification> listCandidate_Certification, string strCertificationListID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //objResultMessage = new ERP.HRM.Employee.DA.DA_Candidate_Certification().SearchData(ref listCandidate_Certification, strCertificationListID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadExperience(String strAuthenData, String strGUID, ref List<Candidate_Experience> listCandidate_Experience, string strExperienceID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //objResultMessage = new ERP.HRM.Employee.DA.DA_Candidate_Experience().SearchData(ref listCandidate_Experience, strExperienceID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadAttachment(String strAuthenData, String strGUID, ref List<Candidate_Attachment> list, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //objResultMessage = new ERP.HRM.Employee.DA.DA_Candidate_Attachment().SearchDataToList(ref list, strUserName);
            return objResultMessage;
        }
        #endregion
    }
}
