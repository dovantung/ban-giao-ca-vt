﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using ERP.SalesAndServices.Payment.DA;
using ERP.SalesAndServices.Payment.BO;
using ERP.HRM.Payroll.BO;
namespace ERPTransactionServices.HRM.Payroll
{
    /// <summary>
    /// Summary description for WSTeaBreakMark
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSTeaBreakMark : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Payroll.BO.TeaBreakMark objTeaBreakMark, DateTime? dtmMarkDate, string strUserName, int intWorkingShiftID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_TeaBreakMark().LoadInfo(ref objTeaBreakMark, dtmMarkDate, strUserName, intWorkingShiftID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.TeaBreakMark objTeaBreakMark)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_TeaBreakMark objDA_TeaBreakMark = new ERP.HRM.Payroll.DA.DA_TeaBreakMark();
            objTeaBreakMark.CreatedUser = objToken.UserName;
            objResultMessage = objDA_TeaBreakMark.Insert(objTeaBreakMark);

            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, List<ERP.HRM.Payroll.BO.TeaBreakMark> lstInsertObj, List<ERP.HRM.Payroll.BO.TeaBreakMark> lstUpdateObj)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //ERP.HRM.Payroll.DA.DA_TeaBreakMark objDA_TeaBreakMark = new ERP.HRM.Payroll.DA.DA_TeaBreakMark();
            //string strTokenUser = objToken.UserName;
            //objResultMessage = objDA_TeaBreakMark.Update(lstInsertObj, lstUpdateObj, strTokenUser);
            return objResultMessage;
        }
        
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_TeaBreakMark().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "TeaBreakMarkCacheList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataNew(String strAuthenData, String strGUID, ref DataTable tblResult, string keyWord, string strUserName, DateTime? dtmMarkDateFrom, DateTime? dtmMarkDateTo, string workingShiftIDList, string storeIDList, int searchType, string areaIDList, int departmentID, int comfirmType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            //objResultMessage = new ERP.HRM.Payroll.DA.DA_TeaBreakMark().SearchDataNew(ref tblResult, keyWord, strUserName, dtmMarkDateFrom, dtmMarkDateTo, workingShiftIDList, storeIDList, searchType, areaIDList, departmentID, comfirmType);
            if (tblResult != null)
                tblResult.TableName = "TeaBreakMarkListNew";
            return objResultMessage;
        }
    }
}
