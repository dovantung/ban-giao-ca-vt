﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ERP.HRM.Payroll.BO;
using ERP.HRM.Payroll.DA;
using System.Data;

namespace ERPTransactionServices.HRM.Payroll
{
    /// <summary>
    /// Summary description for WSWorkingPlan
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSWorkingPlan : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true, Description = "Tìm kiếm thông tin bảng phân ca làm việc của nhân viên", MessageName = "SearchData")]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbData, object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_WorkingPlan().SearchData(ref dtbData, objKeywords);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Lấy thông tin bảng phân ca làm việc của nhân viên", MessageName = "LoadInfo")]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Payroll.BO.HR_WorkingPlan objHR_WorkingPlan, string strUserName, DateTime dtmWorkingDate, int intWorkingShiftID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_WorkingPlan().LoadInfo(ref objHR_WorkingPlan, strUserName, dtmWorkingDate, intWorkingShiftID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Thêm thông tin bảng phân ca làm việc của nhân viên", MessageName = "Insert")]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.HR_WorkingPlan objHR_WorkingPlan)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_WorkingPlan objDA_HR_WorkingPlan = new ERP.HRM.Payroll.DA.DA_HR_WorkingPlan();
            objDA_HR_WorkingPlan.objLogObject.CertificateString = objToken.CertificateString;
            objDA_HR_WorkingPlan.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_HR_WorkingPlan.objLogObject.LoginLogID = objToken.LoginLogID;
            objHR_WorkingPlan.CreatedUser = objToken.UserName;

            objResultMessage = objDA_HR_WorkingPlan.Insert(objHR_WorkingPlan);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Thêm thông tin bảng phân ca làm việc của nhân viên", MessageName = "InsertMulti")]
        public Library.WebCore.ResultMessage InsertMulti(String strAuthenData, String strGUID, DataTable dtbWorkingPlan)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_WorkingPlan objDA_HR_WorkingPlan = new ERP.HRM.Payroll.DA.DA_HR_WorkingPlan();
            objDA_HR_WorkingPlan.objLogObject.CertificateString = objToken.CertificateString;
            objDA_HR_WorkingPlan.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_HR_WorkingPlan.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_HR_WorkingPlan.Insert(dtbWorkingPlan, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật thông tin bảng phân ca làm việc của nhân viên", MessageName = "Update")]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.HR_WorkingPlan objHR_WorkingPlan)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_WorkingPlan objDA_HR_WorkingPlan = new ERP.HRM.Payroll.DA.DA_HR_WorkingPlan();
            objDA_HR_WorkingPlan.objLogObject.CertificateString = objToken.CertificateString;
            objDA_HR_WorkingPlan.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_HR_WorkingPlan.objLogObject.LoginLogID = objToken.LoginLogID;
            objHR_WorkingPlan.UpdatedUser = objToken.UserName;

            objResultMessage = objDA_HR_WorkingPlan.Update(objHR_WorkingPlan);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Cập nhật thông tin bảng phân ca làm việc của nhân viên", MessageName = "UpdateMulti")]
        public Library.WebCore.ResultMessage UpdateMulti(String strAuthenData, String strGUID, DataTable dtbWorkingPlan, DataTable dtbDeleteByChangeWorkingShift)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_WorkingPlan objDA_HR_WorkingPlan = new ERP.HRM.Payroll.DA.DA_HR_WorkingPlan();
            objDA_HR_WorkingPlan.objLogObject.CertificateString = objToken.CertificateString;
            objDA_HR_WorkingPlan.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_HR_WorkingPlan.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_HR_WorkingPlan.Update(dtbWorkingPlan, dtbDeleteByChangeWorkingShift, objToken.UserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true, Description = "Xóa thông tin bảng phân ca làm việc của nhân viên", MessageName = "DeleteMulti")]
        public Library.WebCore.ResultMessage DeleteMulti(String strAuthenData, String strGUID, DataTable dtbDeleteWorkingPlan)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_WorkingPlan objDA_HR_WorkingPlan = new ERP.HRM.Payroll.DA.DA_HR_WorkingPlan();
            objDA_HR_WorkingPlan.objLogObject.CertificateString = objToken.CertificateString;
            objDA_HR_WorkingPlan.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_HR_WorkingPlan.objLogObject.LoginLogID = objToken.LoginLogID;

            objResultMessage = objDA_HR_WorkingPlan.Delete(dtbDeleteWorkingPlan, objToken.UserName);
            return objResultMessage;
        }

    }
}
