﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using ERP.SalesAndServices.Payment.DA;
using ERP.SalesAndServices.Payment.BO;
using ERP.HRM.Payroll.BO;
namespace ERPTransactionServices.HRM.Payroll
{
    /// <summary>
    /// Summary description for WSAttendanceMark
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSAttendanceMark : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Payroll.BO.HR_AttendanceMark objAttendanceMark, DateTime? dtmMarkDate, string strUserName, int intWorkingShiftID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().LoadInfo(ref objAttendanceMark, dtmMarkDate, strUserName, intWorkingShiftID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetInfo(String strAuthenData, String strGUID, ref ERP.HRM.Payroll.BO.HR_AttendanceMark objAttendanceMark, DateTime? dtmMarkDate, string strUserName, int intWorkingShiftID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().GetInfo(ref objAttendanceMark, dtmMarkDate, strUserName, intWorkingShiftID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.HR_AttendanceMark objAttendanceMark)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_AttendanceMark objDA_AttendanceMark = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark();
            objDA_AttendanceMark.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AttendanceMark.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AttendanceMark.objLogObject.LoginLogID = objToken.LoginLogID;
            objAttendanceMark.CreatedUser = objToken.UserName;
            objResultMessage = objDA_AttendanceMark.Insert(objAttendanceMark);

            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SychRevenue(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.SychRevenue objSychRevenue)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_AttendanceMark objDA_AttendanceMark = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark();
            objDA_AttendanceMark.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AttendanceMark.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AttendanceMark.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_AttendanceMark.SychRevenue(objSychRevenue);

            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertNew(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.HR_AttendanceMark objAttendanceMark)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_AttendanceMark objDA_AttendanceMark = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark();
            objDA_AttendanceMark.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AttendanceMark.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AttendanceMark.objLogObject.LoginLogID = objToken.LoginLogID;
            objAttendanceMark.CreatedUser = objToken.UserName;
            objResultMessage = objDA_AttendanceMark.InsertNew(objAttendanceMark);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, List<ERP.HRM.Payroll.BO.HR_AttendanceMark> lstInsertObj, List<ERP.HRM.Payroll.BO.HR_AttendanceMark> lstUpdateObj)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_AttendanceMark objDA_AttendanceMark = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark();
            objDA_AttendanceMark.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AttendanceMark.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AttendanceMark.objLogObject.LoginLogID = objToken.LoginLogID;
            string strTokenUser = objToken.UserName;
            objResultMessage = objDA_AttendanceMark.Update(lstInsertObj, lstUpdateObj, strTokenUser);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update_Out(String strAuthenData, String strGUID, ERP.HRM.Payroll.BO.HR_AttendanceMark objAttendanceMark)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_AttendanceMark objDA_AttendanceMark = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark();
            objDA_AttendanceMark.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AttendanceMark.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AttendanceMark.objLogObject.LoginLogID = objToken.LoginLogID;
            objAttendanceMark.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_AttendanceMark.UpdateTimeEnd(objAttendanceMark);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataCache(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "AttendanceMarkCacheList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataNew(String strAuthenData, String strGUID, ref DataTable tblResult, string keyWord, string strUserName, DateTime? dtmMarkDateFrom, DateTime? dtmMarkDateTo, string workingShiftIDList, string storeIDList, int searchType, string areaIDList, int departmentID, int comfirmType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().SearchDataNew(ref tblResult, keyWord, strUserName, dtmMarkDateFrom, dtmMarkDateTo, workingShiftIDList, storeIDList, searchType, areaIDList, departmentID, comfirmType);
            if (tblResult != null)
                tblResult.TableName = "AttendanceMarkListNew";
            return objResultMessage;
        }
        //Phong thêm check chọn phòng ban con
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData_New(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().SearchData_New(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "AttendanceMarkList_New";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoCheck(String strAuthenData, String strGUID, ref ERP.HRM.Payroll.BO.HR_AttendanceMark objAttendanceMark, DateTime? dtmMarkDate, string strUserName, int workingShiftID, int intConfirmWorkingShiftID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().LoadInfoCheck(ref objAttendanceMark, dtmMarkDate, strUserName, workingShiftID, intConfirmWorkingShiftID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertByFingerPrint(String strAuthenData, String strGUID, ref string strRetrunMessage, string strUserName, int intWorkingShiftID)
        {
            return InsertByFingerPrint(strAuthenData, strGUID, ref strRetrunMessage, (int)ERP.HRM.Payroll.DA.DA_HR_AttendanceMark.MarkType.Attendance,
                strUserName, intWorkingShiftID);
        }

        [WebMethod(EnableSession = true, Description = "Chấm công vân tay", MessageName = "Cham_cong_van_tay")]
        public Library.WebCore.ResultMessage InsertByFingerPrint(String strAuthenData, String strGUID, ref string strRetrunMessage,
            int intMarkType, string strUserName, int intWorkingShiftID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Payroll.DA.DA_HR_AttendanceMark objDA_AttendanceMark = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark();
            objDA_AttendanceMark.objLogObject.CertificateString = objToken.CertificateString;
            objDA_AttendanceMark.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_AttendanceMark.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_AttendanceMark.InsertByFingerPrint(ref strRetrunMessage, (ERP.HRM.Payroll.DA.DA_HR_AttendanceMark.MarkType)intMarkType,
                strUserName, objToken.LoginStoreID, intWorkingShiftID);
            return objResultMessage;
        }

        /// <summary>
        /// Kiểm tra thời gian hiện tại có phải thời gian làm việc của nhân viên hay không
        /// </summary>
        /// <param name="strAuthenData"></param>
        /// <param name="strGUID"></param>
        /// <param name="bolResult"></param>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage AttendanceMark_CheckTime(String strAuthenData, String strGUID, ref bool bolResult, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, strUserName);
            return objResultMessage;
        }
    }
}
