﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using ERP.HRM.Employee.BO.QJR;
using ERP.HRM.Employee.DA.QJR;
namespace ERPTransactionServices.HRM.QJR
{
    /// <summary>
    /// Summary description for WSQuitJobRequest
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSQuitJobRequest : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref QuitJobRequest objQuitJobRequest, string strQuitJobRequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_QuitJobRequest().LoadInfo(ref objQuitJobRequest, strQuitJobRequestID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, QuitJobRequest objQuitJobRequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_QuitJobRequest objDA_QuitJobRequest = new DA_QuitJobRequest();
            objQuitJobRequest.CreatedUser = objToken.UserName;
            objDA_QuitJobRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_QuitJobRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_QuitJobRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_QuitJobRequest.Insert(objQuitJobRequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, QuitJobRequest objQuitJobRequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_QuitJobRequest objDA_QuitJobRequest = new DA_QuitJobRequest();
            objDA_QuitJobRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_QuitJobRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_QuitJobRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objQuitJobRequest.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_QuitJobRequest.Update(objQuitJobRequest);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strQuitJobRequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID);
            if (objToken == null)
                return objResultMessage;
            QuitJobRequest objQuitJobRequest = new QuitJobRequest();
            objQuitJobRequest.QuitJobRequestID = strQuitJobRequestID;
            objQuitJobRequest.DeletedUser = objToken.UserName;
            DA_QuitJobRequest objDA_QuitJobRequest = new DA_QuitJobRequest();
            objDA_QuitJobRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_QuitJobRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_QuitJobRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_QuitJobRequest.Delete(objQuitJobRequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_QuitJobRequest().SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "QuitJobRequestList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateQuitJobRequestID(String strAuthenData, String strGUID, ref string strQuitJobRequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_QuitJobRequest objDA_QuitJobRequest = new DA_QuitJobRequest();
            objResultMessage = objDA_QuitJobRequest.CreateQuitJobRequestID(ref strQuitJobRequestID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewList(String strAuthenData, String strGUID, QuitJobRequest_ReviewList objQuitJobRequest_ReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_QuitJobRequest_ReviewList objDA_QuitJobRequest_ReviewList = new DA_QuitJobRequest_ReviewList();
            objQuitJobRequest_ReviewList.UpdatedUser = objToken.UserName;
            objQuitJobRequest_ReviewList.CreatedUser = objToken.UserName;
            objResultMessage = objDA_QuitJobRequest_ReviewList.UpdateReviewList(objQuitJobRequest_ReviewList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCommentByQuitJobRequest(String strAuthenData, String strGUID, ref DataTable dtbResult, string strQuitJobRequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_QuitJobRequest_Comment().GetDataByQuitJobRequest(ref dtbResult, strQuitJobRequestID);
            if (dtbResult != null)
                dtbResult.TableName = "QuitJobRequestCommentList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertComment(String strAuthenData, String strGUID,
            ref string strCommentID, QuitJobRequest_Comment objQuitJobRequest_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_QuitJobRequest_Comment objDA_QuitJobRequest_Comment = new DA_QuitJobRequest_Comment();
            objQuitJobRequest_Comment.UpdatedUser = objToken.UserName;
            objQuitJobRequest_Comment.CreatedUser = objToken.UserName;
            objResultMessage = objDA_QuitJobRequest_Comment.Insert(ref strCommentID, objQuitJobRequest_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteComment(String strAuthenData, String strGUID,
            QuitJobRequest_Comment objQuitJobRequest_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_QuitJobRequest_Comment objDA_QuitJobRequest_Comment = new DA_QuitJobRequest_Comment();
            objQuitJobRequest_Comment.DeletedUser = objToken.UserName;
            //objResultMessage = objDA_QuitJobRequest_Comment.Delete(objQuitJobRequest_Comment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetAttachment(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_QuitJobRequest_Attachment().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "AttachmentList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetReviewListByQuitJobRequest(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.QJR.DA_QuitJobRequest().GetReviewListByQuitJobRequest(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "QuitJobRequest_ReviewList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetWorkFlowByQuitJobRequest(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.QJR.DA_QuitJobRequest_WorkFlow().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "WorkFlowList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetWorkFlowList(String strAuthenData, String strGUID, ref DataTable tblResult, int intQuitJobRequestType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.QJR.DA_QuitJobRequest().GetWorkFlowList(ref tblResult, intQuitJobRequestType);
            if (tblResult != null)
                tblResult.TableName = "WorkFlowList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetPermission(String strAuthenData, String strGUID, ref DataTable tblResult, int intQuitJobRequestType)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.QJR.DA_QuitJobRequest().GetPermission(ref tblResult, intQuitJobRequestType);
            if (tblResult != null)
                tblResult.TableName = "PermissionTable";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateProcess(String strAuthenData, String strGUID, QuitJobRequest_WorkFlow objWorkFlow)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_QuitJobRequest_WorkFlow objDA_WorkFlow = new DA_QuitJobRequest_WorkFlow();
            objResultMessage = objDA_WorkFlow.Update(objWorkFlow);
            return objResultMessage;
        }

    }
}
