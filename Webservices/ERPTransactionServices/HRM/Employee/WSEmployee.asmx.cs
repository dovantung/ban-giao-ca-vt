﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using ERP.SalesAndServices.Payment.DA;
using ERP.SalesAndServices.Payment.BO;
using ERP.HRM.Employee.BO;
namespace ERPTransactionServices.HRM.Employee
{
    /// <summary>
    /// Summary description for WSEmployee
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/", Description = "Service hồ sơ nhân sự")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSEmployee : System.Web.Services.WebService
    {

        #region Employee
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Employee objEmployee, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee().LoadInfo(ref objEmployee,  strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataToList(String strAuthenData, String strGUID, ref List<ERP.HRM.Employee.BO.Employee> listEmployee, params object[] objKeywors)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee().SearchDataToList(ref listEmployee, objKeywors);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CheckIdCard(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Employee objEmployee, string strUserName,string strIDCard)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee().CheckIdCard(ref objEmployee, strUserName, strIDCard);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Employee objEmployee)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.DA_Employee objDA_Employee = new ERP.HRM.Employee.DA.DA_Employee();
            objEmployee.CreatedUser = objToken.UserName;
            objDA_Employee.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Employee.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Employee.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_Employee.Insert(objEmployee);
           
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Employee objEmployee)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.DA_Employee objDA_Employee = new ERP.HRM.Employee.DA.DA_Employee();
            objDA_Employee.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Employee.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Employee.objLogObject.LoginLogID = objToken.LoginLogID;
            objEmployee.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_Employee.Update(objEmployee);


            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Employee objEmployee)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.DA_Employee objDA_Employee = new ERP.HRM.Employee.DA.DA_Employee();
            objDA_Employee.objLogObject.CertificateString = objToken.CertificateString;
            objDA_Employee.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_Employee.objLogObject.LoginLogID = objToken.LoginLogID;
            objEmployee.DeletedUser = objToken.UserName;
            objResultMessage = objDA_Employee.Delete(objEmployee);
            return objResultMessage;
        }
   


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] keyWord)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee().SearchData(ref tblResult, keyWord);
            if (tblResult != null)
                tblResult.TableName = "EmployeeList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchDataFromStore(String strAuthenData, String strGUID, string store, ref DataTable tblResult, params object[] keyWord)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee().SearchDataFromStore(store,ref tblResult, keyWord);
            if (tblResult != null)
                tblResult.TableName = store;
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDataFromListParamter(String strAuthenData, String strGUID, ref DataTable dtb,
            string strReportStoreName, List<ERP.MasterData.DA.Parameter> listParameter)
        {

            object[] objKeywords = null;
            if (listParameter != null)
            {
                List<object> listKeyWords = new List<object>();
                foreach (var item in listParameter)
                {
                    listKeyWords.Add(item.Key);
                    if (item.DateTimeValue != null)
                        listKeyWords.Add(item.DateTimeValue.Value);
                    else
                        listKeyWords.Add(item.Value);
                }
                objKeywords = listKeyWords.ToArray();
            }
            return SearchDataFromStore(strAuthenData, strGUID,  strReportStoreName,ref dtb, objKeywords);
        }
        #endregion

        #region Detail
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadRelationship(String strAuthenData, String strGUID, ref List<Employee_RelationShip> listEmployee_RelationShip,string strRelationShipID,  string strUserName )
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_RelationShip().SearchData(ref listEmployee_RelationShip,strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadLabourContract(String strAuthenData, String strGUID, ref List<Employee_LabourContract> listEmployee_LabourContract, string strLabourContractID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_LabourContract().SearchData(ref listEmployee_LabourContract, strLabourContractID, strUserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadWorkHistory(String strAuthenData, String strGUID, ref List<Employee_WorkHistory> listEmployee_WorkHistory, string strWorkHistoryID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_WorkHistory().SearchData(ref listEmployee_WorkHistory, strWorkHistoryID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadSalaryHistory(String strAuthenData, String strGUID, ref List<Employee_SalaryHistory> listEmployee_SalaryHistory, string strSalaryHistoryID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_SalaryHistory().SearchData(ref listEmployee_SalaryHistory, strSalaryHistoryID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadEducationList(String strAuthenData, String strGUID, ref List<Employee_EducationList> listEmployee_EducationList, string strEducationListID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_EducationList().SearchData(ref listEmployee_EducationList, strEducationListID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadCertification(String strAuthenData, String strGUID, ref List<Employee_Certification> listEmployee_Certification, string strCertificationListID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_Certification().SearchData(ref listEmployee_Certification, strCertificationListID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadExperience(String strAuthenData, String strGUID, ref List<Employee_Experience> listEmployee_Experience, string strExperienceID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_Experience().SearchData(ref listEmployee_Experience, strExperienceID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadSkillList(String strAuthenData, String strGUID, ref List<Employee_SkillList> listEmployee_SkillList, string strSkillListID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_SkillList().SearchData(ref listEmployee_SkillList, strSkillListID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadReward(String strAuthenData, String strGUID, ref List<Employee_Reward> listEmployee_Reward, string strRewardID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_Reward().SearchData(ref listEmployee_Reward, strRewardID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadDiscipline(String strAuthenData, String strGUID, ref List<Employee_Discipline> listEmployee_Discipline, string strEmployee_DisciplineID, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_Discipline().SearchData(ref listEmployee_Discipline, strEmployee_DisciplineID, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadFrequentDeduct(String strAuthenData, String strGUID, ref List<Employee_FrequentDeduct> listEmployee_FrequentDeduct,   string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_FrequentDeduct().SearchData(ref listEmployee_FrequentDeduct,  strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadFrequentIncome(String strAuthenData, String strGUID, ref List<Employee_FrequentIncome> listEmployee_FrequentIncome, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_FrequentIncome().SearchData(ref listEmployee_FrequentIncome, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadWorkingPosition(String strAuthenData, String strGUID, ref List<Employee_WorkingPosition> listEmployee_WorkingPosition, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_WorkingPosition().SearchData(ref listEmployee_WorkingPosition, strUserName);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadAttachment(String strAuthenData, String strGUID, ref List<Employee_Attachment> list, string strUserName)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_Attachment().SearchDataToList(ref list, strUserName);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfoLabourContract(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Employee_LabourContract objEmployee_LabourContract, string strLabourContractID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.DA_Employee_LabourContract().LoadInfo(ref objEmployee_LabourContract, strLabourContractID);
            return objResultMessage;
        }
        #endregion
    }
}
