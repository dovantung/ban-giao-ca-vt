﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using ERP.HRM.Payroll.BO.Reward;
using ERP.HRM.Payroll.DA.Reward;
namespace ERPTransactionServices.HRM.Reward
{
    /// <summary>
    /// Summary description for WSRewardRequest
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRewardRequest : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref RewardRequest objRewardRequest, string strRewardRequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_RewardRequest().LoadInfo(ref objRewardRequest, strRewardRequestID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, RewardRequest objRewardRequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_RewardRequest objDA_RewardRequest = new DA_RewardRequest();
            objRewardRequest.CreatedUser = objToken.UserName;
            objDA_RewardRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RewardRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RewardRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_RewardRequest.Insert(objRewardRequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, RewardRequest objRewardRequest)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_RewardRequest objDA_RewardRequest = new DA_RewardRequest();
            objDA_RewardRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RewardRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RewardRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objRewardRequest.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RewardRequest.Update(objRewardRequest);
            return objResultMessage;
        }


        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, string strRewardRequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID);
            if (objToken == null)
                return objResultMessage;
            RewardRequest objRewardRequest = new RewardRequest();
            objRewardRequest.RewardRequestID = strRewardRequestID;
            objRewardRequest.DeletedUser = objToken.UserName;
            DA_RewardRequest objDA_RewardRequest = new DA_RewardRequest();
            objDA_RewardRequest.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RewardRequest.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RewardRequest.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_RewardRequest.Delete(objRewardRequest);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable dtbResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_RewardRequest().SearchData(ref dtbResult, objKeywords);
            if (dtbResult != null)
                dtbResult.TableName = "RewardRequestList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage CreateRewardRequestID(String strAuthenData, String strGUID, ref string strRewardRequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_RewardRequest objDA_RewardRequest = new DA_RewardRequest();
            objResultMessage = objDA_RewardRequest.CreateRewardRequestID(ref strRewardRequestID, objToken.LoginStoreID);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewList(String strAuthenData, String strGUID, RewardRequest_ReviewList objRewardRequest_ReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_RewardRequest_ReviewList objDA_RewardRequest_ReviewList = new DA_RewardRequest_ReviewList();
            objResultMessage = objDA_RewardRequest_ReviewList.UpdateReviewList(objRewardRequest_ReviewList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCommentByRewardRequest(String strAuthenData, String strGUID, ref DataTable dtbResult, string strRewardRequestID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_RewardRequest_Comment().GetDataByRewardRequest(ref dtbResult, strRewardRequestID);
            if (dtbResult != null)
                dtbResult.TableName = "RewardRequestCommentList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertComment(String strAuthenData, String strGUID,
            RewardRequest_Comment objRewardRequest_Comment, ref string strCommentID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_RewardRequest_Comment objDA_RewardRequest_Comment = new DA_RewardRequest_Comment();
            objResultMessage = objDA_RewardRequest_Comment.Insert(objRewardRequest_Comment, ref strCommentID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteComment(String strAuthenData, String strGUID,
            RewardRequest_Comment objRewardRequest_Comment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            DA_RewardRequest_Comment objDA_RewardRequest_Comment = new DA_RewardRequest_Comment();
            objRewardRequest_Comment.DeletedUser = objToken.UserName;
            objResultMessage = objDA_RewardRequest_Comment.Delete(objRewardRequest_Comment);
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GenDecideID(String strAuthenData, String strGUID, ref string strAssetID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new DA_RewardRequest().GenDecideID(ref strAssetID);
            return objResultMessage;
        }
    }
}
