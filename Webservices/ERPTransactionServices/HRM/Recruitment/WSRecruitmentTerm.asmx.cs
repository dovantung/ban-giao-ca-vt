﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ERPTransactionServices.HRM.Recruitment
{
    /// <summary>
    /// Summary description for WSRecruitmentTerm
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSRecruitmentTerm : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage SearchData(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "RecruitmentTerm";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage LoadInfo(String strAuthenData, String strGUID, ref ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm objRecruitmentTerm, int intRecruitmentTermID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm().LoadInfo(ref objRecruitmentTerm, intRecruitmentTermID);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Delete(String strAuthenData, String strGUID, int intRecruitmentTermID)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm objRecruitmentTerm = new ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm();
            objRecruitmentTerm.RecruitmentTermID = intRecruitmentTermID;
            objRecruitmentTerm.DeletedUser = objToken.UserName;
            objRecruitmentTerm.IsDeleted = true;
            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm objDA_RecruitmentTerm = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm();
            objDA_RecruitmentTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RecruitmentTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RecruitmentTerm.objLogObject.LoginLogID = objToken.LoginLogID;
            objResultMessage = objDA_RecruitmentTerm.Delete(objRecruitmentTerm);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Insert(String strAuthenData, String strGUID, ref int intRecruitmentTermID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm objRecruitmentTerm)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm objDA_RecruitmentTerm = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm();
            objDA_RecruitmentTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RecruitmentTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RecruitmentTerm.objLogObject.LoginLogID = objToken.LoginLogID;
            objRecruitmentTerm.CreatedUser = objToken.UserName;
            objResultMessage = objDA_RecruitmentTerm.Insert(ref intRecruitmentTermID, objRecruitmentTerm);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage Update(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm objRecruitmentTerm)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm objDA_RecruitmentTerm = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm();
            objDA_RecruitmentTerm.objLogObject.CertificateString = objToken.CertificateString;
            objDA_RecruitmentTerm.objLogObject.UserHostAddress = HttpContext.Current.Request.UserHostAddress;
            objDA_RecruitmentTerm.objLogObject.LoginLogID = objToken.LoginLogID;
            objRecruitmentTerm.UpdatedUser = objToken.UserName;
            objResultMessage = objDA_RecruitmentTerm.Update(objRecruitmentTerm);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateReviewList(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_ReviewList objReviewList)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_ReviewList objDA_ReviewList = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_ReviewList();
            objReviewList.UpdatedUser = objToken.UserName;
            objReviewList.CreatedUser = objToken.UserName;
            objResultMessage = objDA_ReviewList.UpdateReviewList(objReviewList);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateComment(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_Comment objComment, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Comment objDA_Comment = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Comment();
            objComment.UpdatedUser = objToken.UserName;
            objComment.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Comment.Update(objComment, lstOrderIndex);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage InsertComment(String strAuthenData, String strGUID, ref string strCommentID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_Comment objComment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Comment objDA_Comment = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Comment();
            objComment.UpdatedUser = objToken.UserName;
            objComment.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Comment.Insert(ref strCommentID, objComment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage DeleteComment(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_Comment objComment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Comment objDA_Comment = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Comment();
            objComment.DeletedUser = objToken.UserName;
            objResultMessage = objDA_Comment.Delete(objComment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateCost(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_Cost objCost)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Cost objDA_Cost = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Cost();
            objResultMessage = objDA_Cost.Update(objCost);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateCouncil(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_Council objCouncil)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Council objDA_Council = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Council();
            objCouncil.UpdatedUser = objToken.UserName;
            objCouncil.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Council.Update(objCouncil);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateCycle(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_Cycle objCycle, List<object> lstOrderIndex)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Cycle objDA_Cycle = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Cycle();
            objResultMessage = objDA_Cycle.Update(objCycle, lstOrderIndex);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateCycle_ExamSubject(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_Cycle_ExamSubject objCycle_ExamSubject)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Cycle_ExamSubject objDA_Cycle_ExamSubject = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Cycle_ExamSubject();
            objResultMessage = objDA_Cycle_ExamSubject.Update(objCycle_ExamSubject);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateWPosition(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_WPosition objWPosition)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_WPosition objDA_WPosition = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_WPosition();
            objWPosition.UpdatedUser = objToken.UserName;
            objWPosition.CreatedUser = objToken.UserName;
            objResultMessage = objDA_WPosition.Update(objWPosition);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage UpdateAttachment(String strAuthenData, String strGUID, ERP.HRM.Employee.BO.Recruitment.RecruitmentTerm_Attachment objAttachment)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;

            ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Attachment objDA_Attachment = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Attachment();
            objAttachment.CreatedUser = objToken.UserName;
            objResultMessage = objDA_Attachment.Update(objAttachment);
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetReviewList(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_ReviewList().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "ReviewListList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetAttachment(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Attachment().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "AttachmentList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetComment(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Comment().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CommentList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCost(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Cost().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CostList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCouncil(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Council().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CouncilList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCycle(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Cycle().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CycleList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetCycle_ExamSubject(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_Cycle_ExamSubject().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "CycleExamSubjectList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetWPosition(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_WPosition().SearchData(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "WPositionList";
            return objResultMessage;
        }

        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetReviewListByRecruitmentTerm(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm().GetReviewListByRecruitmentTerm(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "RecruitmentTerm_ReviewList";
            return objResultMessage;
        }
        #region GetDirectCache
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDirectRecruitmentTerm(String strAuthenData, String strGUID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm().GetDirectDataCache(ref tblResult);
            if (tblResult != null)
                tblResult.TableName = "DirectDataCache_RecruitmentTermList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetDirectWPosition(String strAuthenData, String strGUID, ref DataTable tblResult, params object[] objKeywords)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm_WPosition().GetDirectDataCache(ref tblResult, objKeywords);
            if (tblResult != null)
                tblResult.TableName = "DirectDataCache_WPositionList";
            return objResultMessage;
        }
        [WebMethod(EnableSession = true)]
        public Library.WebCore.ResultMessage GetRelatedRecruitmentTermData(String strAuthenData, String strGUID, ref DataTable tblResult)
        {
            Library.WebCore.ResultMessage objResultMessage = new Library.WebCore.ResultMessage();
            Library.WebCore.Token objToken = Library.WebCore.Authentication.CheckAuthenticate(strAuthenData, strGUID, ref objResultMessage);
            if (objToken == null)
                return objResultMessage;
            objResultMessage = new ERP.HRM.Employee.DA.Recruitment.DA_RecruitmentTerm().GetRelatedRecruitmentTermData(ref tblResult);
            if (tblResult != null)
                tblResult.TableName = "RelatedRecruitmentTermDataList";
            return objResultMessage;
        }
        # endregion
    }
}
