﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO
{
    public class AutoStoreChangeContext
    {
        //PK
        public int FromStore { get; set; }
        //PK
        public int ToStore { get; set; }
        //PK
        public int ProductState { get; set; }

        /// <summary>
        /// Nội dung yêu cầu
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Chuyển gấp
        /// </summary>
        public bool IsUrgent { get; set; }
        public int StoreChangeCommandType { get; set; }

        /// <summary>
        /// Hình thức vận chuyển
        /// </summary>
        public int TransportTypeID { get; set; }
        public int TransportCompanyID { get; set; }
        public int TransportServicesID { get; set; }

        /// <summary>
        /// Ngày hết hạn yêu cầu
        /// </summary>
        public DateTime? ExpiredDate { get; set; }
        public List<AutoStoreChangeContextDetail> Detail { get; set; }
        /// <summary>
        /// Loại chuyển kho
        /// </summary>
        public int StoreChangeTypeID { get; set; }
        public int StoreChangeOrderTypeID { get; set; }
        public string StoreChangeCommandID { get; set; }
        public bool IsReviewed { get; set; }
    }
    public class AutoStoreChangeContextDetail
    {
        public bool IsSelected { get; set; }
        public string ProductID { get; set; }
        public string IMEI { get; set; }
        public decimal Quantity { get; set; }
        public string Note { get; set; }
        public bool IsManualAdd { get; set; }
        public bool IsValidConsignmentType { get; set; }
    }
    public enum EProductState
    {
        New = 0,
        Old = 1
    }
}
