
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Hoang Nhu Phong 
	/// Created date 	: 9/28/2012 
	/// Chi tiết yêu cầu chuyển kho
	/// </summary>	
	public class StoreChangeOrderDetail
	{	
	
	
		#region Member Variables

		private string strStoreChangeOrderDetailID = string.Empty;
		private string strStoreChangeOrderID = string.Empty;
		private DateTime? dtmOrderDate;
		private string strProductID = string.Empty;
		private decimal decCommandQuantity;
		private decimal decQuantity;
		private decimal decStoreChangeQuantity;
		private bool bolIsManualAdd = false;
		private bool bolISAttachProduct = false;
		private string strStoreChangeCommandIDList = string.Empty;
		private string strNote = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private bool bolIsExist = false;
        private int intFromStoreID = -1;
        private int intToStoreID = -1;
        private List<StoreChangeOrderDetailIMEI> lstStoreChangeOrderDetailIMEI = null;

		#endregion


		#region Properties 
        /// <summary>
        /// FromStoreID
        /// Kho xuất
        /// </summary>
        public int FromStoreID
        {
            get { return intFromStoreID; }
            set { intFromStoreID = value; }
        }

        /// <summary>
        /// ToStoreID
        /// Kho nhập
        /// </summary>
        public int ToStoreID
        {
            get { return intToStoreID; }
            set { intToStoreID = value; }
        }

		/// <summary>
		/// StoreChangeOrderDetailID
		/// 
		/// </summary>
		public string StoreChangeOrderDetailID
		{
			get { return  strStoreChangeOrderDetailID; }
			set { strStoreChangeOrderDetailID = value; }
		}

		/// <summary>
		/// StoreChangeOrderID
		/// 
		/// </summary>
		public string StoreChangeOrderID
		{
			get { return  strStoreChangeOrderID; }
			set { strStoreChangeOrderID = value; }
		}

		/// <summary>
		/// OrderDate
		/// 
		/// </summary>
		public DateTime? OrderDate
		{
			get { return  dtmOrderDate; }
			set { dtmOrderDate = value; }
		}

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// CommandQuantity
		/// Số lượng từ lệnh chuyển kho
		/// </summary>
		public decimal CommandQuantity
		{
			get { return  decCommandQuantity; }
			set { decCommandQuantity = value; }
		}

		/// <summary>
		/// Quantity
		/// Số lượng yêu cầu
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// StoreChangeQuantity
		/// Số lượng chuyển
		/// </summary>
		public decimal StoreChangeQuantity
		{
			get { return  decStoreChangeQuantity; }
			set { decStoreChangeQuantity = value; }
		}

		/// <summary>
		/// IsManualAdd
		/// Có thêm bằng tay hay không?
		/// </summary>
		public bool IsManualAdd
		{
			get { return  bolIsManualAdd; }
			set { bolIsManualAdd = value; }
		}

		/// <summary>
		/// ISAttachProduct
		/// 
		/// </summary>
		public bool ISAttachProduct
		{
			get { return  bolISAttachProduct; }
			set { bolISAttachProduct = value; }
		}

		/// <summary>
		/// StoreChangeCommandIDList
		/// 
		/// </summary>
		public string StoreChangeCommandIDList
		{
			get { return  strStoreChangeCommandIDList; }
			set { strStoreChangeCommandIDList = value; }
		}

		/// <summary>
		/// Note
		/// Ghi chú
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}

        public List<StoreChangeOrderDetailIMEI> StoreChangeOrderDetailIMEILIST
        {
            get { return lstStoreChangeOrderDetailIMEI; }
            set { lstStoreChangeOrderDetailIMEI = value; }
        }
		#endregion			
		
		
		#region Constructor

		public StoreChangeOrderDetail()
		{
		}
		public StoreChangeOrderDetail(string strStoreChangeOrderDetailID, string strStoreChangeOrderID, DateTime? dtmOrderDate, string strProductID, decimal decCommandQuantity, decimal decQuantity, decimal decStoreChangeQuantity, bool bolIsManualAdd, bool bolISAttachProduct, string strStoreChangeCommandIDList, string strNote, int intCreatedStoreID, string strCreatedUser, DateTime? dtmCreatedDate, string strUpdatedUser, DateTime? dtmUpdatedDate, bool bolIsDeleted, string strDeletedUser, DateTime? dtmDeletedDate)
		{
			this.strStoreChangeOrderDetailID = strStoreChangeOrderDetailID;
            this.strStoreChangeOrderID = strStoreChangeOrderID;
            this.dtmOrderDate = dtmOrderDate;
            this.strProductID = strProductID;
            this.decCommandQuantity = decCommandQuantity;
            this.decQuantity = decQuantity;
            this.decStoreChangeQuantity = decStoreChangeQuantity;
            this.bolIsManualAdd = bolIsManualAdd;
            this.bolISAttachProduct = bolISAttachProduct;
            this.strStoreChangeCommandIDList = strStoreChangeCommandIDList;
            this.strNote = strNote;
            this.intCreatedStoreID = intCreatedStoreID;
            this.strCreatedUser = strCreatedUser;
            this.dtmCreatedDate = dtmCreatedDate;
            this.strUpdatedUser = strUpdatedUser;
            this.dtmUpdatedDate = dtmUpdatedDate;
            this.bolIsDeleted = bolIsDeleted;
            this.strDeletedUser = strDeletedUser;
            this.dtmDeletedDate = dtmDeletedDate;
            
		}
		#endregion


		#region Column Names

		public const String colStoreChangeOrderDetailID = "StoreChangeOrderDetailID";
		public const String colStoreChangeOrderID = "StoreChangeOrderID";
		public const String colOrderDate = "OrderDate";
		public const String colProductID = "ProductID";
		public const String colCommandQuantity = "CommandQuantity";
		public const String colQuantity = "Quantity";
		public const String colStoreChangeQuantity = "StoreChangeQuantity";
		public const String colIsManualAdd = "IsManualAdd";
		public const String colISAttachProduct = "ISAttachProduct";
		public const String colStoreChangeCommandIDList = "StoreChangeCommandIDList";
		public const String colNote = "Note";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
