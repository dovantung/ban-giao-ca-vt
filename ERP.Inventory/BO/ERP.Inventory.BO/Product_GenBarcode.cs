
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Quốc Hiệp
	/// Created date 	: 6/28/2017 
	/// 
	/// </summary>	
	public class Product_GenBarcode
    {	
	
	
		#region Member Variables

		private decimal decBARCode;
		private string strProductID = string.Empty;
		private string strBrandAddress = string.Empty;
		private string strSetUPCountry = string.Empty;
		private string strSetUPYear = string.Empty;
		private string strQUANTIFIED = string.Empty;
		private string strCAPACITY = string.Empty;
		private string strMATERITION = string.Empty;
		private string strUSES = string.Empty;
		private string strUSEProduct = string.Empty;
		private string strPRESERVE = string.Empty;
		private string strHOWUSE = string.Empty;
		private string strDISTRIBUTE = string.Empty;
        private string strBRANDNAME = string.Empty;
        #endregion


        #region Properties 

        /// <summary>
        /// BARCode
        /// Khóa barcode
        /// </summary>
        public decimal BARCode
		{
			get { return  decBARCode; }
			set { decBARCode = value; }
		}

		/// <summary>
		/// ProductID
		/// Mã sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// BrandAddress
		/// Địa chỉ NSX
		/// </summary>
		public string BrandAddress
		{
			get { return  strBrandAddress; }
			set { strBrandAddress = value; }
		}

		/// <summary>
		/// SetUPCountry
		/// Nước SX
		/// </summary>
		public string SetUPCountry
		{
			get { return  strSetUPCountry; }
			set { strSetUPCountry = value; }
		}

		/// <summary>
		/// SetUPYear
		/// Năm SX
		/// </summary>
		public string SetUPYear
		{
			get { return  strSetUPYear; }
			set { strSetUPYear = value; }
		}

		/// <summary>
		/// QUANTIFIED
		/// Định lượng
		/// </summary>
		public string QUANTIFIED
		{
			get { return  strQUANTIFIED; }
			set { strQUANTIFIED = value; }
		}

		/// <summary>
		/// CAPACITY
		/// Dung lượng
		/// </summary>
		public string CAPACITY
		{
			get { return  strCAPACITY; }
			set { strCAPACITY = value; }
		}

		/// <summary>
		/// MATERITION
		/// Chất liệu
		/// </summary>
		public string MATERITION
		{
			get { return  strMATERITION; }
			set { strMATERITION = value; }
		}

		/// <summary>
		/// USES
		/// Công dụng
		/// </summary>
		public string USES
		{
			get { return  strUSES; }
			set { strUSES = value; }
		}

		/// <summary>
		/// USEProduct
		/// Dùng cho sản phẩm
		/// </summary>
		public string USEProduct
		{
			get { return  strUSEProduct; }
			set { strUSEProduct = value; }
		}

		/// <summary>
		/// PRESERVE
		/// Bảo quản
		/// </summary>
		public string PRESERVE
		{
			get { return  strPRESERVE; }
			set { strPRESERVE = value; }
		}

		/// <summary>
		/// HOWUSE
		/// HDSD
		/// </summary>
		public string HOWUSE
		{
			get { return  strHOWUSE; }
			set { strHOWUSE = value; }
		}

		/// <summary>
		/// DISTRIBUTE
		/// Nhập khẩu và phân phối
		/// </summary>
		public string DISTRIBUTE
		{
			get { return  strDISTRIBUTE; }
			set { strDISTRIBUTE = value; }
		}

        /// <summary>
		/// BRANDNAME
		/// 
		/// </summary>
		public string BRANDNAME
        {
            get { return strBRANDNAME; }
            set { strBRANDNAME = value; }
        }
        #endregion


        #region Constructor

        public Product_GenBarcode()
		{
		}
		#endregion


		#region Column Names

		public const String colBARCode = "BARCODE";
		public const String colProductID = "PRODUCTID";
		public const String colBrandAddress = "BRANDADDRESS";
        public const String colBrandName = "BRANDNAME";
        public const String colSetUPCountry = "SETUPCOUNTRY";
		public const String colSetUPYear = "SETUPYEAR";
		public const String colQUANTIFIED = "QUANTIFIED";
		public const String colCAPACITY = "CAPACITY";
		public const String colMATERITION = "MATERITION";
		public const String colUSES = "USES";
		public const String colUSEProduct = "USEPRODUCT";
		public const String colPRESERVE = "PRESERVE";
		public const String colHOWUSE = "HOWUSE";
		public const String colDISTRIBUTE = "DISTRIBUTE";

		#endregion //Column names

		
	}
}
