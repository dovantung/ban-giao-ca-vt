
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Hoang Nhu Phong 
	/// Created date 	: 9/28/2012 
	/// Bảng danh sách duyêt của  yêu cầu chuyển kho( Lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
	/// </summary>	
	public class StoreChangeOrder_RVList
	{	
	
	
		#region Member Variables

		private string strStoreChangeOrderID = string.Empty;
		private DateTime? dtmOrderDate;
		private int intReviewLevelID = 0;
		private int intReviewStatus = 0;
		private bool bolIsReviewed = false;
		private string strReviewedUser = string.Empty;
		private DateTime? dtmReviewedDate;
		private string strNote = string.Empty;
		private bool bolIsExist = false;

		#endregion


		#region Properties 

		/// <summary>
		/// StoreChangeOrderID
		/// Mã yêu cầu xuất chuyển kho
		/// </summary>
		public string StoreChangeOrderID
		{
			get { return  strStoreChangeOrderID; }
			set { strStoreChangeOrderID = value; }
		}

		/// <summary>
		/// OrderDate
		/// 
		/// </summary>
		public DateTime? OrderDate
		{
			get { return  dtmOrderDate; }
			set { dtmOrderDate = value; }
		}

		/// <summary>
		/// ReviewLevelID
		/// Mức duyệt
		/// </summary>
		public int ReviewLevelID
		{
			get { return  intReviewLevelID; }
			set { intReviewLevelID = value; }
		}

		/// <summary>
		/// ReviewStatus
		/// Trạng thái duyệt; 0: chưa duyệt, 1: đang xử lý(Processing), 2: Từ chối(Reject), 3: đồng ý(Accept)
		/// </summary>
		public int ReviewStatus
		{
			get { return  intReviewStatus; }
			set { intReviewStatus = value; }
		}

		/// <summary>
		/// IsReviewed
		/// Đã duyệt
		/// </summary>
		public bool IsReviewed
		{
			get { return  bolIsReviewed; }
			set { bolIsReviewed = value; }
		}

		/// <summary>
		/// ReviewedUser
		/// Người duyệt
		/// </summary>
		public string ReviewedUser
		{
			get { return  strReviewedUser; }
			set { strReviewedUser = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// Ngày duyệt
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// Note
		/// Ghi chú lịch sử duyệt( cộng chuỗi )
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}

		#endregion			
		
		
		#region Constructor

		public StoreChangeOrder_RVList()
		{
		}
		public StoreChangeOrder_RVList(string strStoreChangeOrderID, DateTime? dtmOrderDate, int intReviewLevelID, int intReviewStatus, bool bolIsReviewed, string strReviewedUser, DateTime? dtmReviewedDate, string strNote)
		{
			this.strStoreChangeOrderID = strStoreChangeOrderID;
            this.dtmOrderDate = dtmOrderDate;
            this.intReviewLevelID = intReviewLevelID;
            this.intReviewStatus = intReviewStatus;
            this.bolIsReviewed = bolIsReviewed;
            this.strReviewedUser = strReviewedUser;
            this.dtmReviewedDate = dtmReviewedDate;
            this.strNote = strNote;
            
		}
		#endregion


		#region Column Names

		public const String colStoreChangeOrderID = "StoreChangeOrderID";
		public const String colOrderDate = "OrderDate";
		public const String colReviewLevelID = "ReviewLevelID";
		public const String colReviewStatus = "ReviewStatus";
		public const String colIsReviewed = "IsReviewed";
		public const String colReviewedUser = "ReviewedUser";
		public const String colReviewedDate = "ReviewedDate";
		public const String colNote = "Note";

		#endregion //Column names

		
	}
}
