
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.ProductChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/1/2012 
	/// 
	/// </summary>	
	public class ProductChange
	{	
	
	
		#region Member Variables

		private string strProductChangeID = string.Empty;
		private string strProductChangeOrderID = string.Empty;
		private int intStoreID;
		private bool bolIsNew = false;
		private string strOutputVoucherID = string.Empty;
		private string strInputVoucherID = string.Empty;
		private string strProductChangeUser = string.Empty;
		private DateTime? dtmInVoiceDate;
		private DateTime? dtmProductChangeDate;
		private string strContent = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strContentDeleted = string.Empty;
        private List<ProductChangeDetail> objProductChangeDetailList = new List<ProductChangeDetail>();
        private ERP.Inventory.BO.InputVoucher objInputVoucherBO = new ERP.Inventory.BO.InputVoucher();
        private ERP.Inventory.BO.OutputVoucher objOutputVoucherBO = new ERP.Inventory.BO.OutputVoucher();

		#endregion


		#region Properties 

		/// <summary>
		/// ProductChangeID
		/// 
		/// </summary>
		public string ProductChangeID
		{
			get { return  strProductChangeID; }
			set { strProductChangeID = value; }
		}

		/// <summary>
		/// ProductChangeOrderID
		/// 
		/// </summary>
		public string ProductChangeOrderID
		{
			get { return  strProductChangeOrderID; }
			set { strProductChangeOrderID = value; }
		}

		/// <summary>
		/// StoreID
		/// 
		/// </summary>
		public int StoreID
		{
			get { return  intStoreID; }
            set { intStoreID = value; }
		}

		/// <summary>
		/// IsNew
		/// 
		/// </summary>
		public bool IsNew
		{
			get { return  bolIsNew; }
			set { bolIsNew = value; }
		}

		/// <summary>
		/// OutputVoucherID
		/// 
		/// </summary>
		public string OutputVoucherID
		{
			get { return  strOutputVoucherID; }
			set { strOutputVoucherID = value; }
		}

		/// <summary>
		/// InputVoucherID
		/// 
		/// </summary>
		public string InputVoucherID
		{
			get { return  strInputVoucherID; }
			set { strInputVoucherID = value; }
		}

		/// <summary>
		/// ProductChangeUser
		/// 
		/// </summary>
		public string ProductChangeUser
		{
			get { return  strProductChangeUser; }
			set { strProductChangeUser = value; }
		}

		/// <summary>
		/// InVoiceDate
		/// 
		/// </summary>
		public DateTime? InVoiceDate
		{
			get { return  dtmInVoiceDate; }
			set { dtmInVoiceDate = value; }
		}

		/// <summary>
		/// ProductChangeDate
		/// 
		/// </summary>
		public DateTime? ProductChangeDate
		{
			get { return  dtmProductChangeDate; }
			set { dtmProductChangeDate = value; }
		}

		/// <summary>
		/// Content
		/// 
		/// </summary>
		public string Content
		{
			get { return  strContent; }
			set { strContent = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// ContentDeleted
		/// 
		/// </summary>
		public string ContentDeleted
		{
			get { return  strContentDeleted; }
			set { strContentDeleted = value; }
		}

        public List<ProductChangeDetail> ProductChangeDetailList
        {
            get { return objProductChangeDetailList; }
            set { objProductChangeDetailList = value; }
        }

        public ERP.Inventory.BO.InputVoucher InputVoucherBO
        {
            get { return objInputVoucherBO; }
            set { objInputVoucherBO = value; }
        }

        public ERP.Inventory.BO.OutputVoucher OutputVoucherBO
        {
            get { return objOutputVoucherBO; }
            set { objOutputVoucherBO = value; }
        }


		#endregion			
		
		
		#region Constructor

		public ProductChange()
		{
		}
		#endregion


		#region Column Names

		public const String colProductChangeID = "ProductChangeID";
		public const String colProductChangeOrderID = "ProductChangeOrderID";
		public const String colStoreID = "StoreID";
		public const String colIsNew = "IsNew";
		public const String colOutputVoucherID = "OutputVoucherID";
		public const String colInputVoucherID = "InputVoucherID";
		public const String colProductChangeUser = "ProductChangeUser";
		public const String colInVoiceDate = "InVoiceDate";
		public const String colProductChangeDate = "ProductChangeDate";
		public const String colContent = "Content";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colContentDeleted = "ContentDeleted";

		#endregion //Column names

		
	}
}
