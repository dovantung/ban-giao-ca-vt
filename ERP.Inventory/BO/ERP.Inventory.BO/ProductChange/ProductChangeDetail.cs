
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.ProductChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/1/2012 
	/// 
	/// </summary>	
	public class ProductChangeDetail
	{	
	
	
		#region Member Variables

		private string strProductChangeDetailID = string.Empty;
		private string strProductChangeID = string.Empty;
		private int intStoreID;
		private DateTime? dtmProductChangeDate;
		private string strProductID_Out = string.Empty;
		private string strIMEI_Out = string.Empty;
		private string strProductID_In = string.Empty;
		private string strIMEI_In = string.Empty;
		private decimal decQuantity;
		private string strOutputVoucherDetailID = string.Empty;
		private string strInputVoucherDetailID = string.Empty;
		private string strOldInputVoucherDetailID = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strContentDeleted = string.Empty;
        private int intOutputTypeID = 0;
        private int intInputTypeID = 0;
        private int intInStockStatusID_in = -1;
        private int intInStockStatusID_Out = -1;
        #endregion


        #region Properties 

        /// <summary>
        /// ProductChangeDetailID
        /// 
        /// </summary>
        public string ProductChangeDetailID
		{
			get { return  strProductChangeDetailID; }
			set { strProductChangeDetailID = value; }
		}

		/// <summary>
		/// ProductChangeID
		/// 
		/// </summary>
		public string ProductChangeID
		{
			get { return  strProductChangeID; }
			set { strProductChangeID = value; }
		}

		/// <summary>
		/// StoreID
		/// 
		/// </summary>
		public int StoreID
		{
            get { return intStoreID; }
            set { intStoreID = value; }
		}

		/// <summary>
		/// ProductChangeDate
		/// 
		/// </summary>
		public DateTime? ProductChangeDate
		{
			get { return  dtmProductChangeDate; }
			set { dtmProductChangeDate = value; }
		}

		/// <summary>
		/// ProductID_Out
		/// 
		/// </summary>
		public string ProductID_Out
		{
			get { return  strProductID_Out; }
			set { strProductID_Out = value; }
		}

		/// <summary>
		/// IMEI_Out
		/// 
		/// </summary>
		public string IMEI_Out
		{
			get { return  strIMEI_Out; }
			set { strIMEI_Out = value; }
		}

		/// <summary>
		/// ProductID_IN
		/// 
		/// </summary>
		public string ProductID_In
		{
			get { return  strProductID_In; }
			set { strProductID_In = value; }
		}

		/// <summary>
		/// IMEI_IN
		/// 
		/// </summary>
		public string IMEI_In
		{
			get { return  strIMEI_In; }
			set { strIMEI_In = value; }
		}

		/// <summary>
		/// Quantity
		/// 
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// OutputVoucherDetailID
		/// 
		/// </summary>
		public string OutputVoucherDetailID
		{
			get { return  strOutputVoucherDetailID; }
			set { strOutputVoucherDetailID = value; }
		}

		/// <summary>
		/// InputVoucherDetailID
		/// 
		/// </summary>
		public string InputVoucherDetailID
		{
			get { return  strInputVoucherDetailID; }
			set { strInputVoucherDetailID = value; }
		}

		/// <summary>
		/// OLDInputVoucherDetailID
		/// 
		/// </summary>
		public string OldInputVoucherDetailID
		{
			get { return  strOldInputVoucherDetailID; }
			set { strOldInputVoucherDetailID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// ContentDeleted
		/// 
		/// </summary>
		public string ContentDeleted
		{
			get { return  strContentDeleted; }
			set { strContentDeleted = value; }
		}

        public int OutputTypeID
        {
            get { return intOutputTypeID; }
            set { intOutputTypeID = value; }
        }

        public int InputTypeID
        {
            get { return intInputTypeID; }
            set { intInputTypeID = value; }
        }

        public int InStockStatusID_in
        {
            get
            {
                return intInStockStatusID_in;
            }

            set
            {
                intInStockStatusID_in = value;
            }
        }

        public int InStockStatusID_Out
        {
            get
            {
                return intInStockStatusID_Out;
            }

            set
            {
                intInStockStatusID_Out = value;
            }
        }


        #endregion


        #region Constructor

        public ProductChangeDetail()
		{
		}
		#endregion


		#region Column Names

		public const String colProductChangeDetailID = "ProductChangeDetailID";
		public const String colProductChangeID = "ProductChangeID";
		public const String colStoreID = "StoreID";
		public const String colProductChangeDate = "ProductChangeDate";
		public const String colProductID_Out = "ProductID_Out";
		public const String colIMEI_Out = "IMEI_Out";
		public const String colProductID_In = "ProductID_In";
		public const String colIMEI_In = "IMEI_In";
		public const String colQuantity = "Quantity";
		public const String colOutputVoucherDetailID = "OutputVoucherDetailID";
		public const String colInputVoucherDetailID = "InputVoucherDetailID";
		public const String colOldInputVoucherDetailID = "OldInputVoucherDetailID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colContentDeleted = "ContentDeleted";
        public const String colInStockStatusID_in = "InStockStatusID_in";
        public const String colInStockStatusID_Out = "InStockStatusID_Out";

        #endregion //Column names


    }
}
