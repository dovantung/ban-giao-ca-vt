
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.ProductChange
{
    /// <summary>
	/// Created by 		: Phan Tiến Lộc 
	/// Created date 	: 2016-11-12 
	/// Chi tiết yêu cầu xuất đổi
    /// 
    /// Update by       : Phạm Hải Đăng
    /// Created date 	: 2016-12-15
    /// Thêm 1 cột ghi chú
	/// </summary>	
	public class ProductChangeOrderDT
	{	
	
	
		#region Member Variables

		private string strProductChangeOrderDTID = string.Empty;
        private string strProductChangeOrderID;
		private string strNputVoucherConcernID = string.Empty;
		private string strProductID_Out = string.Empty;
		private string strIMEI_Out = string.Empty;
		private bool bolIsNew_Out = false;
		private string strProductID_IN = string.Empty;
		private string strIMEI_IN = string.Empty;
		private bool bolIsNew_IN = false;
		private decimal decQuantity;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private int intInstockStatusID_In = 1;
        private int intInstockStatusID_Out = 1;
        private string strProductID_INName;
        private bool bolIsRequestIMEI = false;


        public string ProductID_INName
        {
            get { return strProductID_INName; }
            set { strProductID_INName = value; }
        }


        private string strProductID_OutName;

        public string ProductID_OutName
        {
            get { return strProductID_OutName; }
            set { strProductID_OutName = value; }
        }

        private string strChangeNote;       
        #endregion


        #region Properties 

        /// <summary>
        /// ProductChangeOrderDTID
        /// Mã chi tiết yêu cầu
        /// </summary>
        public string ProductChangeOrderDTID
		{
			get { return  strProductChangeOrderDTID; }
			set { strProductChangeOrderDTID = value; }
		}

		/// <summary>
		/// ProductChangeOrderID
		/// Mã yêu cầu
		/// </summary>
		public string ProductChangeOrderID
		{
			get { return  strProductChangeOrderID; }
			set { strProductChangeOrderID = value; }
		}

		/// <summary>
		/// NPUTVoucherConcernID
		/// Phiếu nhập liên quan
		/// </summary>
		public string NputVoucherConcernID
		{
			get { return  strNputVoucherConcernID; }
			set { strNputVoucherConcernID = value; }
		}

		/// <summary>
		/// ProductID_Out
		/// Mã sản phẩm xuất
		/// </summary>
		public string ProductID_Out
		{
			get { return  strProductID_Out; }
			set { strProductID_Out = value; }
		}

		/// <summary>
		/// IMEI_Out
		/// IMEI xuất
		/// </summary>
		public string IMEI_Out
		{
			get { return  strIMEI_Out; }
			set { strIMEI_Out = value; }
		}

		/// <summary>
		/// IsNew_Out
		/// Trạng thái Mới/Cũ xuất
		/// </summary>
		public bool IsNew_Out
		{
			get { return  bolIsNew_Out; }
			set { bolIsNew_Out = value; }
		}

		/// <summary>
		/// ProductID_IN
		/// Mã sản phẩm nhập
		/// </summary>
		public string ProductID_IN
		{
			get { return  strProductID_IN; }
			set { strProductID_IN = value; }
		}

		/// <summary>
		/// IMEI_IN
		/// IMEI nhập
		/// </summary>
		public string IMEI_IN
		{
			get { return  strIMEI_IN; }
			set { strIMEI_IN = value; }
		}

		/// <summary>
		/// IsNew_IN
		/// Trạng thái Mới/Cũ nhập
		/// </summary>
		public bool IsNew_IN
		{
			get { return  bolIsNew_IN; }
			set { bolIsNew_IN = value; }
		}

		/// <summary>
		/// Quantity
		/// Số lượng
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// CreatedUser
		/// Nhân viên tạo
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// Ngày tạo
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// Đã xóa
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// Nhân viên xóa
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// Ngày xóa
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}
        /// <summary>
        /// ChangeNote
        /// Ghi Chú
        /// </summary>
        public string ChangeNote
        {
            get { return strChangeNote; }
            set { strChangeNote = value; }
        }
        /// <summary>
        /// Trạng thái sản phẩm nhập mặc định là mới =1
        /// </summary>
        public int InStockStatusID_In
        {
            get
            {
                return intInstockStatusID_In;
            }

            set
            {
                intInstockStatusID_In = value;
            }
        }

        /// <summary>
        /// Trạng thái sản phẩm xuất mặc định là mới =1
        /// </summary>
        public int InStockStatusID_Out
        {
            get
            {
                return intInstockStatusID_Out;
            }

            set
            {
                intInstockStatusID_Out = value;
            }
        }


        //lỗi và nhóm lỗi
        private string strMachineErrorName;

        public string MachineErrorName
        {
            get { return strMachineErrorName; }
            set { strMachineErrorName = value; }
        }

        private string strMachineErrorGroupName;

        public string MachineErrorGroupName
        {
            get { return strMachineErrorGroupName; }
            set { strMachineErrorGroupName = value; }
        }
        private string strMachineErrorID;

        public string MachineErrorID
        {
            get { return strMachineErrorID; }
            set { strMachineErrorID = value; }
        }

        public bool IsRequestIMEI
        {
            get
            {
                return bolIsRequestIMEI;
            }

            set
            {
               bolIsRequestIMEI = value;
            }
        }

        #endregion


        #region Constructor

        public ProductChangeOrderDT()
		{
		}
		#endregion


		#region Column Names

		public const String colProductChangeOrderDTID = "ProductChangeOrderDTID";
		public const String colProductChangeOrderID = "ProductChangeOrderID";
		public const String colNPUTVoucherConcernID = "NPUTVoucherConcernID";
		public const String colProductID_Out = "ProductID_Out";
        public const String colProductID_OutName = "ProductID_OutName";
        public const String colIMEI_Out = "IMEI_Out";
		public const String colIsNew_Out = "IsNew_Out";
		public const String colProductID_IN = "ProductID_IN";
        public const String colProductID_INName = "ProductID_INName";
        public const String colIMEI_IN = "IMEI_IN";
		public const String colIsNew_IN = "IsNew_IN";
		public const String colQuantity = "Quantity";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colChangeNote = "ChangeNote";

        public const String colInstockStatusID_In = "InstockStatusID_In";
        public const String colInstockStatusID_Out = "InstockStatusID_Out";
        #endregion //Column names


    }
}
