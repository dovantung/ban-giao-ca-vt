
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.ProductChange
{
    /// <summary>
	/// Created by 		: Phan Tiến Lộc 
	/// Created date 	: 2016-11-12 
	/// Yêu cầu xuất đổi hàng
	/// </summary>	
	public class ProductChangeOrder
	{	
	
	
		#region Member Variables

		private string strProductChangeOrderID = string.Empty;
		private int intProductChangeTypeID = 0;
		private string strDescription = string.Empty;
		private int intProductChangeStoreID = 0;
		private int intReviewedStatus = 0;
		private string strReviewedUser = string.Empty;
		private DateTime? dtmReviewedDate;
		private bool bolIsChanged = false;
		private string strChangedUser = string.Empty;
		private DateTime? dtmChangedDate;
		private int intCurrentReviewLevelID = 0;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strDeletedReason = string.Empty;
		private DateTime? dtmExpiryDate;
        private string strCreatedUserFull;

        public string CreatedUserFull
        {
            get { return strCreatedUserFull; }
            set { strCreatedUserFull = value; }
        }

        private string strChangedUserFull;

        public string ChangedUserFull
        {
            get { return strChangedUserFull; }
            set { strChangedUserFull = value; }
        }

        private string strReviewedUserFull;

        public string ReviewedUserFull
        {
            get { return strReviewedUserFull; }
            set { strReviewedUserFull = value; }
        }

        private List<ProductChangeOrderDT> lstProductChangeOrderDT;

        public List<ProductChangeOrderDT> ProductChangeOrderDT
        {
            get { return lstProductChangeOrderDT; }
            set { lstProductChangeOrderDT = value; }
        }
        private List<ProductChangeOrder_RVL> lstProductChangeOrder_RVL;

        public List<ProductChangeOrder_RVL> ProductChangeOrder_RVL
        {
            get { return lstProductChangeOrder_RVL; }
            set { lstProductChangeOrder_RVL = value; }
        }

        private List<ProductChangeOrder_ATT> objProductChangeOrder_ATTList = new List<ProductChangeOrder_ATT>();
        #endregion


        #region Properties 

        /// <summary>
        /// ProductChangeOrderID
        /// Mã yêu cầu xuất đổi
        /// </summary>
        public string ProductChangeOrderID
		{
			get { return  strProductChangeOrderID; }
			set { strProductChangeOrderID = value; }
		}

		/// <summary>
		/// ProductChangeTypeID
		/// Loại yêu cầu
		/// </summary>
		public int ProductChangeTypeID
		{
			get { return  intProductChangeTypeID; }
			set { intProductChangeTypeID = value; }
		}

		/// <summary>
		/// Description
		/// Mô tả
		/// </summary>
		public string Description
		{
			get { return  strDescription; }
			set { strDescription = value; }
		}

		/// <summary>
		/// ProductChangeStoreID
		/// Kho xuất đổi
		/// </summary>
		public int ProductChangeStoreID
		{
			get { return  intProductChangeStoreID; }
			set { intProductChangeStoreID = value; }
		}

		/// <summary>
		/// ReviewedStatus
		/// Trạng thái duyệt: -1-Đang xử lý; 0-Từ chối; 1-Đồng ý
		/// </summary>
		public int ReviewedStatus
		{
			get { return  intReviewedStatus; }
			set { intReviewedStatus = value; }
		}

		/// <summary>
		/// ReviewedUser
		/// Nhân viên duyệt
		/// </summary>
		public string ReviewedUser
		{
			get { return  strReviewedUser; }
			set { strReviewedUser = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// Ngày duyệt
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// ISChangeD
		/// Đã xuất đổi
		/// </summary>
		public bool IsChanged
		{
			get { return  bolIsChanged; }
			set { bolIsChanged = value; }
		}

		/// <summary>
		/// ChangeDUser
		/// Nhân viên đổi
		/// </summary>
		public string ChangedUser
		{
			get { return  strChangedUser; }
			set { strChangedUser = value; }
		}

		/// <summary>
		/// ChangeDDate
		/// Ngày đổi
		/// </summary>
		public DateTime? ChangedDate
		{
			get { return  dtmChangedDate; }
			set { dtmChangedDate = value; }
		}

		/// <summary>
		/// CurrentReviewLevelID
		/// Mức duyệt hiên tại
		/// </summary>
		public int CurrentReviewLevelID
		{
			get { return  intCurrentReviewLevelID; }
			set { intCurrentReviewLevelID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// Kho tạo
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// Nhân viên tạo
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// Ngày tạo
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// Nhân viên cập nhật
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// Ngày cập nhật
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// Đã xóa
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// Nhân viên xóa
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// Ngày xóa
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// DeletedReason
		/// Nội dung xóa
		/// </summary>
		public string DeletedReason
		{
			get { return  strDeletedReason; }
			set { strDeletedReason = value; }
		}

		/// <summary>
		/// ExpiryDate
		/// Ngày hết hạn yêu cầu
		/// </summary>
		public DateTime? ExpiryDate
		{
			get { return  dtmExpiryDate; }
			set { dtmExpiryDate = value; }
		}


        public List<ProductChangeOrder_ATT> ProductChangeOrder_ATTList
        {
            get { return objProductChangeOrder_ATTList; }
            set { objProductChangeOrder_ATTList = value; }
        }

        #endregion


        #region Constructor

        public ProductChangeOrder()
		{
		}
		#endregion


		#region Column Names

		public const String colProductChangeOrderID = "ProductChangeOrderID";
		public const String colProductChangeTypeID = "ProductChangeTypeID";
		public const String colDescription = "Description";
		public const String colProductChangeStoreID = "ProductChangeStoreID";
		public const String colReviewedStatus = "ReviewedStatus";
		public const String colReviewedUser = "ReviewedUser";
		public const String colReviewedDate = "ReviewedDate";
		public const String colISChangeD = "ISChangeD";
		public const String colChangeDUser = "ChangeDUser";
		public const String colChangeDDate = "ChangeDDate";
		public const String colCurrentReviewLevelID = "CurrentReviewLevelID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colDeletedReason = "DeletedReason";
		public const String colExpiryDate = "ExpiryDate";
        public const String colCreatedUserFull = "CreatedUserFull";
        public const String colReviewedUserFull = "ReviewedUserFull";
        public const String colChangedUserFull = "ChangedUserFull";

        #endregion //Column names


    }
}
