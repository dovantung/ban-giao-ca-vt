
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.ProductChange
{
    /// <summary>
	/// Created by 		: Phạm Hải Đăng 
	/// Created date 	: 11-Jan-18 
	/// 
	/// </summary>	
	public class ProductChangeOrder_ATT
	{	
	
	
		#region Member Variables

		private string strAttachMentID = string.Empty;
		private string strProductChangeOrderID = string.Empty;
		private string strFileID = string.Empty;
		private string strAttachMentName = string.Empty;
		private string strAttachMentPath = string.Empty;
		private string strDescription = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;

		#endregion


		#region Properties 

		/// <summary>
		/// AttachMentID
		/// 
		/// </summary>
		public string AttachMentID
		{
			get { return  strAttachMentID; }
			set { strAttachMentID = value; }
		}

		/// <summary>
		/// ProductChangeOrderID
		/// 
		/// </summary>
		public string ProductChangeOrderID
		{
			get { return  strProductChangeOrderID; }
			set { strProductChangeOrderID = value; }
		}

		/// <summary>
		/// FileID
		/// 
		/// </summary>
		public string FileID
		{
			get { return  strFileID; }
			set { strFileID = value; }
		}

		/// <summary>
		/// AttachMentName
		/// 
		/// </summary>
		public string AttachMentName
		{
			get { return  strAttachMentName; }
			set { strAttachMentName = value; }
		}

		/// <summary>
		/// AttachMentPath
		/// 
		/// </summary>
		public string AttachMentPath
		{
			get { return  strAttachMentPath; }
			set { strAttachMentPath = value; }
		}

		/// <summary>
		/// Description
		/// 
		/// </summary>
		public string Description
		{
			get { return  strDescription; }
			set { strDescription = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        public bool IsAddNew { get; set; }

        public int STT { get; set; }

        public string CreatedFullName { get; set; }

        public string AttachMentLocalPath { get; set; }
        #endregion


        #region Constructor

        public ProductChangeOrder_ATT()
		{
		}
		#endregion


		#region Column Names

		public const String colAttachMentID = "AttachMentID";
		public const String colProductChangeOrderID = "ProductChangeOrderID";
		public const String colFileID = "FileID";
		public const String colAttachMentName = "AttachMentName";
		public const String colAttachMentPath = "AttachMentPath";
		public const String colDescription = "Description";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
