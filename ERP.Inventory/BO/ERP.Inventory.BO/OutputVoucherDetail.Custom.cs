﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO
{
    public partial class OutputVoucherDetail
    {
        #region Variable
        private string strInvoiceID = string.Empty;
        private string strSaleOrderID = string.Empty;
        #endregion
        #region Property
        public string InvoiceID
        {
            get { return strInvoiceID; }
            set { strInvoiceID = value; }
        }

        public string SaleOrderID
        {
            get { return strSaleOrderID; }
            set { strSaleOrderID = value; }
        }

        private bool bolIsAddPromotionDetail = false;

        public bool IsAddPromotionDetail
        {
            get { return bolIsAddPromotionDetail; }
            set { bolIsAddPromotionDetail = value; }
        }


        #endregion
    }
}
