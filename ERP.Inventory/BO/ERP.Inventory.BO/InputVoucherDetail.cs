
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// Chi tiết phiếu nhập
    /// 
    /// Modifield by    : Đặng Thế Hùng
    /// Date            : 26/06/2018
    /// Bổ sung thêm    : ProductStatusName
	/// </summary>	
	public class InputVoucherDetail
	{	
	
	
		#region Member Variables

		private string strInputVoucherDetailID = string.Empty;
		private string strInputVoucherID = string.Empty;
		private string strProductID = string.Empty;
        private string strProductName = string.Empty;
        private bool bolIsRequestIMEI = false;
        private bool bolIsAutoCreateIMEI = false;
        private bool bolIsAllowDecimal = false;
        private string strIMEIFormatEXP = string.Empty;
        private decimal decOrderQuantity = 0;
        private decimal decAmount = 0;
        private string strQuantityUnitName = string.Empty;

		private decimal decQuantity;
		private int intVAT = 0;
		private int intVATPercent = 0;
        private int intVATProduct = 0;
		private decimal decInputPrice;
		private decimal decORIGINALInputPrice;
		private decimal decCostPrice;
		private decimal decFirstPrice;
		private decimal decReturnFee;
		private decimal decAdjustPrice;
		private string strAdjustPriceContent = string.Empty;
		private string strAdjustPriceUser = string.Empty;
		private DateTime? dtmInputDate;
		private DateTime? dtmENDWarrantyDate;
		private DateTime? dtmProductIONDate;
		private DateTime? dtmProductChangeDate;
		private DateTime? dtmReturnDate;
		private DateTime? dtmChangeToOLDDate;
		private DateTime? dtmFirstInputDate;
		private DateTime? dtmFirstInVoiceDate;
		private int intFirstCustomerID = 0;
		private decimal decFirstInputTypeID;
		private string strFirstInputVoucherID = string.Empty;
		private string strFirstInputVoucherDetailID = string.Empty;
		private bool bolIsNew = false;
		private bool bolIsShowProduct = false;
		private bool bolIsReturnProduct = false;
		private bool bolIsReturnWithFee = false;
		private bool bolIsCheckRealInput = false;
		private bool bolISProductChange = false;
		private bool bolISAddInStock = false;
		private int intCreatedStoreID = 0;
		private int intInputStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private bool bolIsExist = false;
        private int intOrderIndex = 0;
        private List<InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = null;
        private string strProductStatusName = string.Empty;


        private DataTable InputVoucherDetailIMEIDTB = null;

        public DataTable dtbInputVoucherDetailIMEIDTB
        {
            get { return InputVoucherDetailIMEIDTB; }
            set { InputVoucherDetailIMEIDTB = value; }
        }

        public int OrderIndex
        {
            get { return intOrderIndex; }
            set { intOrderIndex = value; }
        }

        private decimal decTaxedFee = 0;
        private decimal decPurchaseFee = 0;
        private string strOrderDetailID = string.Empty;
        private bool bolIsErrorProduct = false;
        private decimal decTotalOrderQuantity = 0;
        
        // LE VAN DONG - 06/09/2017
        private bool bolIsRequirePinCode = false;
        //Phạm Thiên Ân - 29/09/2017
        private decimal decQuantityToInput = 0;
        private bool bolIsCheckQuantity = false;
        private decimal decQuantityRemain = 0;
        private decimal decInvoiceQuantity = 0;
        public bool IsRequirePinCode
        {
            get { return bolIsRequirePinCode; }
            set { bolIsRequirePinCode = value; }
        }

        public decimal TotalOrderQuantity
        {
            get { return decTotalOrderQuantity; }
            set { decTotalOrderQuantity = value; }
        }

        public bool IsErrorProduct
        {
            get { return bolIsErrorProduct; }
            set { bolIsErrorProduct = value; }
        }
        public string OrderDetailID
        {
            get { return strOrderDetailID; }
            set { strOrderDetailID = value; }
        }

        private string strNote;

        public string Note
        {
            get { return strNote; }
            set { strNote = value; }
        }


        //Đăng bổ sung
        private bool bolIsHasWarranty = false;
        private string strStoreChangeOrderDetailID = string.Empty;
        private decimal decOutputPrice;
        private int intOutputVAT = 0;
		#endregion

		#region Properties 
        
        /// <summary>
        /// chi phi thuế đơn hàng
        /// </summary>
        public decimal OrderTaxedFee
        {
            get;
            set;
        }

        /// <summary>
        /// chi phí mua hàng đơn hàng
        /// </summary>
        public decimal OrderPurchaseFee
        {
            get;
            set;
        }

        public decimal TaxedFee
        {
            get { return decTaxedFee; }
            set { decTaxedFee = value; }
        }
        
        public decimal PurchaseFee
        {
            get { return decPurchaseFee; }
            set { decPurchaseFee = value; }
        }

        /// <summary>
        /// Danh sach IMEI của sản phẩm
        /// </summary>
        public List<InputVoucherDetailIMEI> InputVoucherDetailIMEIList
        {
            get { return objInputVoucherDetailIMEIList; }
            set { objInputVoucherDetailIMEIList = value; }
        }

        /// <summary>
        /// Don vi
        /// </summary>
        public string QuantityUnitName
        {
            get { return strQuantityUnitName; }
            set { strQuantityUnitName = value; }
        }

        /// <summary>
        /// Thanh tien
        /// </summary>
        public decimal Amount
        {
            get { return decAmount; }
            set { decAmount = value; }
        }
        /// <summary>
        /// So luong dat hang
        /// </summary>
        public decimal OrderQuantity
        {
            get { return decOrderQuantity; }
            set { decOrderQuantity = value; }
        }
      
        /// <summary>
        /// IMEI mo rong
        /// </summary>
        public string IMEIFormatEXP
        {
            get { return strIMEIFormatEXP; }
            set { strIMEIFormatEXP = value; }
        }

        /// <summary>
        /// Cho phep nhap so luong le
        /// </summary>
        public bool IsAllowDecimal
        {
            get { return bolIsAllowDecimal; }
            set { bolIsAllowDecimal = value; }
        }
        /// <summary>
        /// Tao IMEI cho san pham tu dong
        /// </summary>
        public bool IsAutoCreateIMEI
        {
            get { return bolIsAutoCreateIMEI; }
            set { bolIsAutoCreateIMEI = value; }
        }

        /// <summary>
        /// San pham co yeu cau co Imei?
        /// </summary>
        public bool IsRequestIMEI
        {
            get { return bolIsRequestIMEI; }
            set { bolIsRequestIMEI = value; }
        }

        /// <summary>
        /// Ten san pham
        /// </summary>
        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }

        /// <summary>
        /// luu  VAT cua san pham
        /// </summary>
        public int VATProduct
        {
            get { return intVATProduct; }
            set { intVATProduct = value; }
        }
		/// <summary>
		/// InputVoucherDetailID
		/// Mã chi tiết phiếu nhập
		/// </summary>
		public string InputVoucherDetailID
		{
			get { return  strInputVoucherDetailID; }
			set { strInputVoucherDetailID = value; }
		}

		/// <summary>
		/// InputVoucherID
		/// Mã phiếu nhập
		/// </summary>
		public string InputVoucherID
		{
			get { return  strInputVoucherID; }
			set { strInputVoucherID = value; }
		}

		/// <summary>
		/// ProductID
		/// Mã sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// Quantity
		/// Số lượng
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// VAT
		/// Thuế
		/// </summary>
		public int VAT
		{
			get { return  intVAT; }
			set { intVAT = value; }
		}

		/// <summary>
		/// VATPercent
		/// Phần trăm thuế
		/// </summary>
		public int VATPercent
		{
			get { return  intVATPercent; }
			set { intVATPercent = value; }
		}

		/// <summary>
		/// InputPrice
		/// Giá nhập sau cùng
		/// </summary>
		public decimal InputPrice
		{
			get { return  decInputPrice; }
			set { decInputPrice = value; }
		}

		/// <summary>
		/// ORIGINALInputPrice
		/// Giá nhập trước khi điều chỉnh
		/// </summary>
		public decimal ORIGINALInputPrice
		{
			get { return  decORIGINALInputPrice; }
			set { decORIGINALInputPrice = value; }
		}

		/// <summary>
		/// CostPrice
		/// Giá vốn
		/// </summary>
		public decimal CostPrice
		{
			get { return  decCostPrice; }
			set { decCostPrice = value; }
		}

		/// <summary>
		/// FirstPrice
		/// Giá nhập đầu tiên(lần nhập đầu tiên)
		/// </summary>
		public decimal FirstPrice
		{
			get { return  decFirstPrice; }
			set { decFirstPrice = value; }
		}

		/// <summary>
		/// ReturnFee
		/// Phí trả hàng
		/// </summary>
		public decimal ReturnFee
		{
			get { return  decReturnFee; }
			set { decReturnFee = value; }
		}

		/// <summary>
		/// AdjustPrice
		/// Mức điều chỉnh giá 
		/// </summary>
		public decimal AdjustPrice
		{
			get { return  decAdjustPrice; }
			set { decAdjustPrice = value; }
		}

		/// <summary>
		/// AdjustPriceContent
		/// Nội dung điều chỉnh
		/// </summary>
		public string AdjustPriceContent
		{
			get { return  strAdjustPriceContent; }
			set { strAdjustPriceContent = value; }
		}

		/// <summary>
		/// AdjustPriceUser
		/// Nhân viên điều chỉnh
		/// </summary>
		public string AdjustPriceUser
		{
			get { return  strAdjustPriceUser; }
			set { strAdjustPriceUser = value; }
		}

		/// <summary>
		/// InputDate
		/// Ngày nhập
		/// </summary>
		public DateTime? InputDate
		{
			get { return  dtmInputDate; }
			set { dtmInputDate = value; }
		}

		/// <summary>
		/// ENDWarrantyDate
		/// Ngày hết hạn BH
		/// </summary>
		public DateTime? ENDWarrantyDate
		{
			get { return  dtmENDWarrantyDate; }
			set { dtmENDWarrantyDate = value; }
		}

		/// <summary>
		/// ProductIONDate
		/// Ngày sản xuất
		/// </summary>
		public DateTime? ProductIONDate
		{
			get { return  dtmProductIONDate; }
			set { dtmProductIONDate = value; }
		}

		/// <summary>
		/// ProductChangeDate
		/// Ngày đổi hàng
		/// </summary>
		public DateTime? ProductChangeDate
		{
			get { return  dtmProductChangeDate; }
			set { dtmProductChangeDate = value; }
		}

		/// <summary>
		/// ReturnDate
		/// Ngày trả hàng
		/// </summary>
		public DateTime? ReturnDate
		{
			get { return  dtmReturnDate; }
			set { dtmReturnDate = value; }
		}

		/// <summary>
		/// ChangeToOLDDate
		/// Ngày chuyển sang máy cũ
		/// </summary>
		public DateTime? ChangeToOLDDate
		{
			get { return  dtmChangeToOLDDate; }
			set { dtmChangeToOLDDate = value; }
		}

		/// <summary>
		/// FirstInputDate
		/// Ngày nhập đầu tiên
		/// </summary>
		public DateTime? FirstInputDate
		{
			get { return  dtmFirstInputDate; }
			set { dtmFirstInputDate = value; }
		}

		/// <summary>
		/// FirstInVoiceDate
		/// Ngày hóa đơn nhập đầu tiên
		/// </summary>
		public DateTime? FirstInVoiceDate
		{
			get { return  dtmFirstInVoiceDate; }
			set { dtmFirstInVoiceDate = value; }
		}

		/// <summary>
		/// FirstCustomerID
		/// Khách hàng nhập đầu tiên
		/// </summary>
		public int FirstCustomerID
		{
			get { return  intFirstCustomerID; }
			set { intFirstCustomerID = value; }
		}

		/// <summary>
		/// FirstInputTypeID
		/// Hình thức nhập đầu tiền
		/// </summary>
		public decimal FirstInputTypeID
		{
			get { return  decFirstInputTypeID; }
			set { decFirstInputTypeID = value; }
		}

		/// <summary>
		/// FirstInputVoucherID
		/// Mã phiếu nhập đầu tiên
		/// </summary>
		public string FirstInputVoucherID
		{
			get { return  strFirstInputVoucherID; }
			set { strFirstInputVoucherID = value; }
		}

		/// <summary>
		/// FirstInputVoucherDetailID
		/// Mã chi tiết phiếu nhập đầu tiên
		/// </summary>
		public string FirstInputVoucherDetailID
		{
			get { return  strFirstInputVoucherDetailID; }
			set { strFirstInputVoucherDetailID = value; }
		}

		/// <summary>
		/// IsNew
		/// Mới/cũ
		/// </summary>
		public bool IsNew
		{
			get { return  bolIsNew; }
			set { bolIsNew = value; }
		}

		/// <summary>
		/// IsShowProduct
		/// Hàng trưng bày
		/// </summary>
		public bool IsShowProduct
		{
			get { return  bolIsShowProduct; }
			set { bolIsShowProduct = value; }
		}

		/// <summary>
		/// IsReturnProduct
		/// Là sản phẩm nhập trả
		/// </summary>
		public bool IsReturnProduct
		{
			get { return  bolIsReturnProduct; }
			set { bolIsReturnProduct = value; }
		}

		/// <summary>
		/// IsReturnWithFee
		/// Là sản phẩm nhập trả có thu phí
		/// </summary>
		public bool IsReturnWithFee
		{
			get { return  bolIsReturnWithFee; }
			set { bolIsReturnWithFee = value; }
		}

		/// <summary>
		/// IsCheckRealInput
		/// Đã xác nhận nhận hàng
		/// </summary>
		public bool IsCheckRealInput
		{
			get { return  bolIsCheckRealInput; }
			set { bolIsCheckRealInput = value; }
		}

		/// <summary>
		/// ISProductChange
		/// Là sản phẩm đổi hàng
		/// </summary>
		public bool ISProductChange
		{
			get { return  bolISProductChange; }
			set { bolISProductChange = value; }
		}

		/// <summary>
		/// ISAddInStock
		/// Thêm vào tồn kho
		/// </summary>
		public bool ISAddInStock
		{
			get { return  bolISAddInStock; }
			set { bolISAddInStock = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// InputStoreID
		/// 
		/// </summary>
		public int InputStoreID
		{
			get { return  intInputStoreID; }
			set { intInputStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}
        
        /// <summary>
		/// Số lượng cần nhập
		/// </summary>
		public decimal QuantityToInput
        {
            get { return decQuantityToInput; }
            set { decQuantityToInput = value; }
        }
        /// <summary>
		/// kiểm tra số lượng  khác 0
		/// </summary>
		public bool IsCheckQuantity
        {
            get { return bolIsCheckQuantity; }
            set { bolIsCheckQuantity = value; }
        }
        /// <summary>
		/// Số lượng còn lại
		/// </summary>
		public decimal QuantityRemain
        {
            get { return decQuantityRemain; }
            set { decQuantityRemain = value; }
        }

        public bool IsHasWarranty
        {
            get { return bolIsHasWarranty; }
            set { bolIsHasWarranty = value; }
        }

        public string StoreChangeOrderDetailID
        {
            get { return strStoreChangeOrderDetailID; }
            set { strStoreChangeOrderDetailID = value; }
        }

        public decimal OutputPrice
        {
            get { return decOutputPrice; }
            set { decOutputPrice = value; }
        }

        public int OutputVAT
        {
            get { return intOutputVAT; }
            set { intOutputVAT = value; }
        }

        /// <summary>
        /// InvoiceQuantity
        ///
        /// </summary>
        public decimal InvoiceQuantity
        {
            get { return decInvoiceQuantity; }
            set { decInvoiceQuantity = value; }
        }

        public string ProductStatusName
        {
            get { return strProductStatusName; }
            set { strProductStatusName = value; }
        }

        #endregion


        #region Constructor

        public InputVoucherDetail()
		{
		}
		public InputVoucherDetail(string strInputVoucherDetailID, string strInputVoucherID, string strProductID, decimal decQuantity, int intVAT, int intVATPercent, decimal decInputPrice, decimal decORIGINALInputPrice, decimal decCostPrice, decimal decFirstPrice, decimal decReturnFee, decimal decAdjustPrice, string strAdjustPriceContent, string strAdjustPriceUser, DateTime? dtmInputDate, DateTime? dtmENDWarrantyDate, DateTime? dtmProductIONDate, DateTime? dtmProductChangeDate, DateTime? dtmReturnDate, DateTime? dtmChangeToOLDDate, DateTime? dtmFirstInputDate, DateTime? dtmFirstInVoiceDate, int intFirstCustomerID, decimal decFirstInputTypeID, string strFirstInputVoucherID, string strFirstInputVoucherDetailID, bool bolIsNew, bool bolIsShowProduct, bool bolIsReturnProduct, bool bolIsReturnWithFee, bool bolIsCheckRealInput, bool bolISProductChange, bool bolISAddInStock, int intCreatedStoreID, int intInputStoreID, string strCreatedUser, DateTime? dtmCreatedDate, string strUpdatedUser, DateTime? dtmUpdatedDate, bool bolIsDeleted, string strDeletedUser, DateTime? dtmDeletedDate)
		{
			this.strInputVoucherDetailID = strInputVoucherDetailID;
            this.strInputVoucherID = strInputVoucherID;
            this.strProductID = strProductID;
            this.decQuantity = decQuantity;
            this.intVAT = intVAT;
            this.intVATPercent = intVATPercent;
            this.decInputPrice = decInputPrice;
            this.decORIGINALInputPrice = decORIGINALInputPrice;
            this.decCostPrice = decCostPrice;
            this.decFirstPrice = decFirstPrice;
            this.decReturnFee = decReturnFee;
            this.decAdjustPrice = decAdjustPrice;
            this.strAdjustPriceContent = strAdjustPriceContent;
            this.strAdjustPriceUser = strAdjustPriceUser;
            this.dtmInputDate = dtmInputDate;
            this.dtmENDWarrantyDate = dtmENDWarrantyDate;
            this.dtmProductIONDate = dtmProductIONDate;
            this.dtmProductChangeDate = dtmProductChangeDate;
            this.dtmReturnDate = dtmReturnDate;
            this.dtmChangeToOLDDate = dtmChangeToOLDDate;
            this.dtmFirstInputDate = dtmFirstInputDate;
            this.dtmFirstInVoiceDate = dtmFirstInVoiceDate;
            this.intFirstCustomerID = intFirstCustomerID;
            this.decFirstInputTypeID = decFirstInputTypeID;
            this.strFirstInputVoucherID = strFirstInputVoucherID;
            this.strFirstInputVoucherDetailID = strFirstInputVoucherDetailID;
            this.bolIsNew = bolIsNew;
            this.bolIsShowProduct = bolIsShowProduct;
            this.bolIsReturnProduct = bolIsReturnProduct;
            this.bolIsReturnWithFee = bolIsReturnWithFee;
            this.bolIsCheckRealInput = bolIsCheckRealInput;
            this.bolISProductChange = bolISProductChange;
            this.bolISAddInStock = bolISAddInStock;
            this.intCreatedStoreID = intCreatedStoreID;
            this.intInputStoreID = intInputStoreID;
            this.strCreatedUser = strCreatedUser;
            this.dtmCreatedDate = dtmCreatedDate;
            this.strUpdatedUser = strUpdatedUser;
            this.dtmUpdatedDate = dtmUpdatedDate;
            this.bolIsDeleted = bolIsDeleted;
            this.strDeletedUser = strDeletedUser;
            this.dtmDeletedDate = dtmDeletedDate;
            
		}
		#endregion


		#region Column Names

		public const String colInputVoucherDetailID = "InputVoucherDetailID";
		public const String colInputVoucherID = "InputVoucherID";
		public const String colProductID = "ProductID";
		public const String colQuantity = "Quantity";
		public const String colVAT = "VAT";
		public const String colVATPercent = "VATPercent";
		public const String colInputPrice = "InputPrice";
		public const String colORIGINALInputPrice = "ORIGINALInputPrice";
		public const String colCostPrice = "CostPrice";
		public const String colFirstPrice = "FirstPrice";
		public const String colReturnFee = "ReturnFee";
		public const String colAdjustPrice = "AdjustPrice";
		public const String colAdjustPriceContent = "AdjustPriceContent";
		public const String colAdjustPriceUser = "AdjustPriceUser";
		public const String colInputDate = "InputDate";
		public const String colENDWarrantyDate = "ENDWarrantyDate";
		public const String colProductIONDate = "ProductIONDate";
		public const String colProductChangeDate = "ProductChangeDate";
		public const String colReturnDate = "ReturnDate";
		public const String colChangeToOLDDate = "ChangeToOLDDate";
		public const String colFirstInputDate = "FirstInputDate";
		public const String colFirstInVoiceDate = "FirstInVoiceDate";
		public const String colFirstCustomerID = "FirstCustomerID";
		public const String colFirstInputTypeID = "FirstInputTypeID";
		public const String colFirstInputVoucherID = "FirstInputVoucherID";
		public const String colFirstInputVoucherDetailID = "FirstInputVoucherDetailID";
		public const String colIsNew = "IsNew";
		public const String colIsShowProduct = "IsShowProduct";
		public const String colIsReturnProduct = "IsReturnProduct";
		public const String colIsReturnWithFee = "IsReturnWithFee";
		public const String colIsCheckRealInput = "IsCheckRealInput";
		public const String colISProductChange = "ISProductChange";
		public const String colISAddInStock = "ISAddInStock";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colInputStoreID = "InputStoreID";
		public const String colCreatedUser = "CreatedUser";

		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colOrderDetailID = "OrderDetailID";
        public const String colIsErrorProduct = "IsErrorProduct";
        public const String colTaxedFee = "TaxedFee";
        public const String colPurchaseFee = "PurchaseFee";
        public const String colNote = "Note";
        public const String colOrderIndex = "OrderIndex";
        public const String colInvoiceQuantity = "InvoiceQuantity";
        public const String colProductStatusName = "ProductStatusName";
        #endregion //Column names


    }
}
