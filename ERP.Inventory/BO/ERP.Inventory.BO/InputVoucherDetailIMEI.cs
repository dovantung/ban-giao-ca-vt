
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// Chi tiết IMEI phiếu nhập
	/// </summary>	
	public class InputVoucherDetailIMEI
	{	
	
	
		#region Member Variables

		private string strInputVoucherDetailID = string.Empty;
		private string strIMEI = string.Empty;
		private string strPINCode = string.Empty;
		private string strNote = string.Empty;
		private string strIMEIChangeCHAIN = string.Empty;
		private bool bolIsHasWarranty = false;
		private int intCreatedStoreID = 0;
		private int intInputStoreID = 0;
		private DateTime? dtmInputDate;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private bool bolIsExist = false;

        private bool bolIsSelect = false;
        private string strProductID = string.Empty;
        private string strStatus = string.Empty;
        private bool bolIsValidate = true;
        private bool bolIsFind = false;
        private bool bolIsError = false;
        private decimal decCostPrice;
        private decimal decFirstPrice;
        private string strInputUser = string.Empty;
        private string strProductName = string.Empty;

        // LE VAN DONG - 06/09/2017
        private bool bolIsRequirePinCode = false;
        private bool bolIsEdit = false;
        public bool IsRequirePinCode
        {
            get { return bolIsRequirePinCode; }
            set { bolIsRequirePinCode = value; }
        }
        private DateTime? dtmEndWarrantyDate;

        public DateTime? EndWarrantyDate
        {
            get { return dtmEndWarrantyDate; }
            set { dtmEndWarrantyDate = value; }
        }


        /// <summary>
        /// IsEdit
        /// Chỉnh sửa trên lưới nhập IMEI
        /// </summary>
        public bool IsEdit
        {
            get { return bolIsEdit; }
            set { bolIsEdit = value; }
        }
        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }


        public bool IsError
        {
            get { return bolIsError; }
            set { bolIsError = value; }
        }

        public bool IsFind
        {
            get { return bolIsFind; }
            set { bolIsFind = value; }
        }

        public bool IsValidate
        {
            get { return bolIsValidate; }
            set { bolIsValidate = value; }
        }

        public string Status
        {
            get { return strStatus; }
            set { strStatus = value; }
        }
        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }

        public bool IsSelect
        {
            get { return bolIsSelect; }
            set { bolIsSelect = value; }
        }

       

		#endregion


		#region Properties 

        /// <summary>
        /// CostPrice
        /// 
        /// </summary>
        public decimal CostPrice
        {
            get { return decCostPrice; }
            set { decCostPrice = value; }
        }

        /// <summary>
        /// FirstPrice
        /// 
        /// </summary>
        public decimal FirstPrice
        {
            get { return decFirstPrice; }
            set { decFirstPrice = value; }
        }

        /// <summary>
        /// InputUser
        /// 
        /// </summary>
        public string InputUser
        {
            get { return strInputUser; }
            set { strInputUser = value; }
        }


		/// <summary>
		/// InputVoucherDetailID
		/// Mã chi tiết phiếu nhập
		/// </summary>
		public string InputVoucherDetailID
		{
			get { return  strInputVoucherDetailID; }
			set { strInputVoucherDetailID = value; }
		}

		/// <summary>
		/// IMEI
		/// Serial/IMEI
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// PINCode
		/// Mã PIN
		/// </summary>
		public string PINCode
		{
			get { return  strPINCode; }
			set { strPINCode = value; }
		}

		/// <summary>
		/// Note
		/// Ghi chú
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// IMEIChangeCHAIN
		/// Chuổi IMEI
		/// </summary>
		public string IMEIChangeCHAIN
		{
			get { return  strIMEIChangeCHAIN; }
			set { strIMEIChangeCHAIN = value; }
		}

		/// <summary>
		/// IsHasWarranty
		/// Có bảo hành
		/// </summary>
		public bool IsHasWarranty
		{
			get { return  bolIsHasWarranty; }
			set { bolIsHasWarranty = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// Kho tạo
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// InputStoreID
		/// Kho nhập
		/// </summary>
		public int InputStoreID
		{
			get { return  intInputStoreID; }
			set { intInputStoreID = value; }
		}

		/// <summary>
		/// InputDate
		/// Ngày nhập
		/// </summary>
		public DateTime? InputDate
		{
			get { return  dtmInputDate; }
			set { dtmInputDate = value; }
		}

		/// <summary>
		/// CreatedUser
		/// Người nhập
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}

		#endregion			
		
		
		#region Constructor

		public InputVoucherDetailIMEI()
		{
		}
		public InputVoucherDetailIMEI(string strInputVoucherDetailID, string strIMEI, string strPINCode, string strNote, string strIMEIChangeCHAIN, bool bolIsHasWarranty, int intCreatedStoreID, int intInputStoreID, DateTime? dtmInputDate, string strCreatedUser, DateTime? dtmCreatedDate, string strUpdatedUser, DateTime? dtmUpdatedDate, bool bolIsDeleted, string strDeletedUser, DateTime? dtmDeletedDate)
		{
			this.strInputVoucherDetailID = strInputVoucherDetailID;
            this.strIMEI = strIMEI;
            this.strPINCode = strPINCode;
            this.strNote = strNote;
            this.strIMEIChangeCHAIN = strIMEIChangeCHAIN;
            this.bolIsHasWarranty = bolIsHasWarranty;
            this.intCreatedStoreID = intCreatedStoreID;
            this.intInputStoreID = intInputStoreID;
            this.dtmInputDate = dtmInputDate;
            this.strCreatedUser = strCreatedUser;
            this.dtmCreatedDate = dtmCreatedDate;
            this.strUpdatedUser = strUpdatedUser;
            this.dtmUpdatedDate = dtmUpdatedDate;
            this.bolIsDeleted = bolIsDeleted;
            this.strDeletedUser = strDeletedUser;
            this.dtmDeletedDate = dtmDeletedDate;
            
		}
		#endregion


		#region Column Names

		public const String colInputVoucherDetailID = "InputVoucherDetailID";
		public const String colIMEI = "IMEI";
		public const String colPINCode = "PINCode";
		public const String colNote = "Note";
		public const String colIMEIChangeCHAIN = "IMEIChangeCHAIN";
		public const String colIsHasWarranty = "IsHasWarranty";
        public const String colENDWARRANTYDATE = "ENDWARRANTYDATE";
        public const String colCreatedStoreID = "CreatedStoreID";
		public const String colInputStoreID = "InputStoreID";
		public const String colInputDate = "InputDate";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colCostPrice = "CostPrice";
        public const String colFirstPrice = "FirstPrice";
        public const String colInputUser = "InputUser";
		#endregion //Column names

		
	}
}
