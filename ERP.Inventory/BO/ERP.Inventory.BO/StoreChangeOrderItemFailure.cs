﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO
{
    public class StoreChangeOrderItemFailure
    {
        public int FromStoreID { get; set; }
        public string FromStoreName { get; set; }
        public int ToStoreID { get; set; }
        public string ToStoreName { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string IMEI { get; set; }
        public int InStockStatusID { get; set; }
        public string InStockStatusName { get; set; }
        public decimal TransferQuantity { get; set; }
        public string Note { get; set; }
    }
}
