
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 11/6/14 
	/// Tách linh kiện từ sản phẩm
	/// </summary>	
	public class SplitProduct
	{	
	
	
		#region Member Variables

		private string strSplitProductID = string.Empty;
		private string strProductID = string.Empty;
		private string strIMEI = string.Empty;
		private decimal decPrice;
		private string strSplitContent = string.Empty;
		private string strOutputVoucherID = string.Empty;
		private string strInputVoucherID1 = string.Empty;
		private string strInputVoucherID2 = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private InputVoucher objInputVoucher = null;
        private InputVoucher objInputVoucherIMEI = null;
        private OutputVoucher objOutputVoucherIMEI = null;
        private decimal decPriceRemain = 0;

        /// <summary>
        /// Giá nhập lại IMEI còn lại
        /// </summary>
        public decimal PriceRemain
        {
            get { return decPriceRemain; }
            set { decPriceRemain = value; }
        }
        /// <summary>
        /// Phiếu nhập linh kiện
        /// </summary>
        public InputVoucher InputVoucher
        {
            get { return objInputVoucher; }
            set { objInputVoucher = value; }
        }
        
        /// <summary>
        /// Phiếu nhập lại IMEI đã bị tách
        /// </summary>
        public InputVoucher InputVoucherIMEI
        {
            get { return objInputVoucherIMEI; }
            set { objInputVoucherIMEI = value; }
        }
        
        /// <summary>
        /// Thông tin phiếu xuất IMEI cần tách
        /// </summary>
        public OutputVoucher OutputVoucherIMEI
        {
            get { return objOutputVoucherIMEI; }
            set { objOutputVoucherIMEI = value; }
        }

		#endregion


		#region Properties 

		/// <summary>
		/// SplitProductID
		/// 
		/// </summary>
		public string SplitProductID
		{
			get { return  strSplitProductID; }
			set { strSplitProductID = value; }
		}

		/// <summary>
		/// ProductID
		/// Mã sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// IMEI
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// Price
		/// Giá sản phẩm
		/// </summary>
		public decimal Price
		{
			get { return  decPrice; }
			set { decPrice = value; }
		}

		/// <summary>
		/// SplitContent
		/// Nội dung
		/// </summary>
		public string SplitContent
		{
			get { return  strSplitContent; }
			set { strSplitContent = value; }
		}

		/// <summary>
		/// OutputVoucherID
		/// Mã phiếu xuất
		/// </summary>
		public string OutputVoucherID
		{
			get { return  strOutputVoucherID; }
			set { strOutputVoucherID = value; }
		}

		/// <summary>
		/// InputVoucherID1
		/// Mã phiếu nhập lại
		/// </summary>
		public string InputVoucherID1
		{
			get { return  strInputVoucherID1; }
			set { strInputVoucherID1 = value; }
		}

		/// <summary>
		/// InputVoucherID2
		/// Mã phiếu nhập linh kiện
		/// </summary>
		public string InputVoucherID2
		{
			get { return  strInputVoucherID2; }
			set { strInputVoucherID2 = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public SplitProduct()
		{
		}
		#endregion


		#region Column Names

		public const String colSplitProductID = "SplitProductID";
		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
		public const String colPrice = "Price";
		public const String colSplitContent = "SplitContent";
		public const String colOutputVoucherID = "OutputVoucherID";
		public const String colInputVoucherID1 = "InputVoucherID1";
		public const String colInputVoucherID2 = "InputVoucherID2";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
