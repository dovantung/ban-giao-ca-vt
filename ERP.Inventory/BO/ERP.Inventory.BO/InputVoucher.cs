
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using ERP.SalesAndServices.Payment.BO;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// Phiếu nhập
    /// 
    /// Modifield by    : Phạm Hải Đăng
    /// Date            : 11/10/2017
    /// bo sung them ISAUTOCREATEINVOICE
    /// 
    ///  Modifield by    : Nguyễn Phạm Minh Hiếu
    /// Date            : 12/10/2017
    /// bo sung them note
	/// </summary>	
	public class InputVoucher
    {


        #region Member Variables

        private string strInputVoucherID = string.Empty;
        private string strOrderID = string.Empty;
        private string strInVoiceID = string.Empty;
        private string strInVoiceSymbol = string.Empty;
        private string strDenominator = string.Empty;
        private int intCustomerID = 0;
        private string strCustomerName = string.Empty;
        private string strCustomerAddress = string.Empty;
        private string strCustomerPhone = string.Empty;
        private string strCustomerTaxID = string.Empty;
        private string strCustomerEmail = string.Empty;
        private DateTime? dtmCustomerBirthday;
        private string strCustomerIDCard = string.Empty;
        private string strContent = string.Empty;
        private int intCreatedStoreID = 0;
        private int intInputStoreID = 0;
        private int intAutoStoreChangeToStoreID = 0;
        private int intInputTypeID = 0;
        private int intPayableTypeID = 0;
        private int intCurrencyUnitID = 0;
        private int intDiscountReasonID = 0;
        private DateTime? dtmCreateDate;
        private DateTime? dtmInVoiceDate;
        private DateTime? dtmInputDate;
        private DateTime? dtmPayableDate;
        private DateTime? dtmTaxMonth;
        private decimal decCurrencyExchange;
        private decimal decDiscount;
        private decimal decProtectPriceDiscount;
        private decimal decTotalAmountBFT;
        private decimal decTotalVAT;
        private decimal decTotalAmount;
        private string strCreatedUser = string.Empty;
        private string strStaffUser = string.Empty;
        private bool bolIsNew = false;
        private bool bolIsReturnWithFee = false;
        private bool bolISStoreChange = false;
        private bool bolIsCheckRealInput = false;
        private bool bolISposted = false;
        private string strCheckRealInputNote = string.Empty;
        private string strCheckRealInputUser = string.Empty;
        private DateTime? dtmCheckRealInputTime;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private string strContentDeleted = string.Empty;
        private bool bolIsExist = false;
        private List<InputVoucherDetail> lstInputVoucherDetail = null;
        private List<InputVoucher_Attachment> lstInputVoucher_Attachmnet = null;
        private List<InputVoucher_Attachment> lstInputVoucher_Attachmnet_Del = null;
        private Voucher objVoucher = null;
        private bool bolInsertInputChange = false;
        private bool bolIsInputFromOrder = false;
        private bool bolIsCreateOutVoucher = false;
        private bool bolIsInputFromRetailInputPrice = false;
        private string strRetailInputPriceID = string.Empty;
        private int intTransportCustomerID = -1;
        private DateTime? dtmIdCardIssueDate;
        private string strIdCardIssuePlace = string.Empty;
        private bool bolIsErrorProduct = false;
        private string strErrorProductNote = string.Empty;
        private DataTable dtbUserNotify = null;
        private bool bolIsAutoCreateInvoice = false;
        private string strNote = string.Empty;
        private DateTime? dtmDeliveryNoteDate;

        #endregion

        #region Properties

        public DataTable UserNotifyData
        {
            get { return dtbUserNotify; }
            set { dtbUserNotify = value; }
        }
        public string ErrorProductNote
        {
            get { return strErrorProductNote; }
            set { strErrorProductNote = value; }
        }

        public bool IsErrorProduct
        {
            get { return bolIsErrorProduct; }
            set { bolIsErrorProduct = value; }
        }
        public Voucher Voucher
        {
            get { return objVoucher; }
            set { objVoucher = value; }
        }

        public List<InputVoucher_Attachment> InputVoucher_AttachmnetList
        {
            get { return lstInputVoucher_Attachmnet; }
            set { lstInputVoucher_Attachmnet = value; }
        }

        public List<InputVoucher_Attachment> InputVoucher_AttachmnetDelList
        {
            get { return lstInputVoucher_Attachmnet_Del; }
            set { lstInputVoucher_Attachmnet_Del = value; }
        }

        public List<InputVoucherDetail> InputVoucherDetailList
        {
            get { return lstInputVoucherDetail; }
            set { lstInputVoucherDetail = value; }
        }

        /// <summary>
        /// InputVoucherID
        /// Mã phiếu nhập
        /// </summary>
        public string InputVoucherID
        {
            get { return strInputVoucherID; }
            set { strInputVoucherID = value; }
        }


        public int TransportCustomerID
        {
            get { return intTransportCustomerID; }
            set { intTransportCustomerID = value; }
        }


        public DateTime? IdCardIssueDate
        {
            get { return dtmIdCardIssueDate; }
            set { dtmIdCardIssueDate = value; }
        }


        public string IdCardIssuePlace
        {
            get { return strIdCardIssuePlace; }
            set { strIdCardIssuePlace = value; }
        }
        public string RetailInputPriceID
        {
            get { return strRetailInputPriceID; }
            set { strRetailInputPriceID = value; }
        }
        public bool IsCreateOutVoucher
        {
            get { return bolIsCreateOutVoucher; }
            set { bolIsCreateOutVoucher = value; }
        }
        public bool IsInputFromOrder
        {
            get { return bolIsInputFromOrder; }
            set { bolIsInputFromOrder = value; }
        }

        public bool IsInputFromRetailInputPrice
        {
            get { return bolIsInputFromRetailInputPrice; }
            set { bolIsInputFromRetailInputPrice = value; }
        }

        public bool IsInsertInputChange
        {
            get { return bolInsertInputChange; }
            set { bolInsertInputChange = value; }
        }

        /// <summary>
        /// OrderID
        /// Mã đơn hàng
        /// </summary>
        public string OrderID
        {
            get { return strOrderID; }
            set { strOrderID = value; }
        }

        /// <summary>
        /// InVoiceID
        /// Số hóa đơn
        /// </summary>
        public string InVoiceID
        {
            get { return strInVoiceID; }
            set { strInVoiceID = value; }
        }

        /// <summary>
        /// InVoiceSymbol
        /// Ký hiệu hóa đơn
        /// </summary>
        public string InVoiceSymbol
        {
            get { return strInVoiceSymbol; }
            set { strInVoiceSymbol = value; }
        }

        /// <summary>
        /// DENOMinAToR
        /// Mẫu số
        /// </summary>
        public string Denominator
        {
            get { return strDenominator; }
            set { strDenominator = value; }
        }

        /// <summary>
        /// CustomerID
        /// Mã khách hàng
        /// </summary>
        public int CustomerID
        {
            get { return intCustomerID; }
            set { intCustomerID = value; }
        }

        /// <summary>
        /// CustomerName
        /// Tên khách hàng
        /// </summary>
        public string CustomerName
        {
            get { return strCustomerName; }
            set { strCustomerName = value; }
        }

        /// <summary>
        /// CustomerAddress
        /// Địa chỉ khách hàng
        /// </summary>
        public string CustomerAddress
        {
            get { return strCustomerAddress; }
            set { strCustomerAddress = value; }
        }

        /// <summary>
        /// CustomerPhone
        /// Số điện thoại khách hàng
        /// </summary>
        public string CustomerPhone
        {
            get { return strCustomerPhone; }
            set { strCustomerPhone = value; }
        }

        /// <summary>
        /// CustomerTaxID
        /// Mã số thuế khách hàng
        /// </summary>
        public string CustomerTaxID
        {
            get { return strCustomerTaxID; }
            set { strCustomerTaxID = value; }
        }

        /// <summary>
        /// CustomerEmail
        /// Email khách hàng
        /// </summary>
        public string CustomerEmail
        {
            get { return strCustomerEmail; }
            set { strCustomerEmail = value; }
        }

        /// <summary>
        /// CustomerBirthday
        /// Ngày SN khách hàng
        /// </summary>
        public DateTime? CustomerBirthday
        {
            get { return dtmCustomerBirthday; }
            set { dtmCustomerBirthday = value; }
        }

        /// <summary>
        /// CustomerIDCard
        /// CMND khách hàng
        /// </summary>
        public string CustomerIDCard
        {
            get { return strCustomerIDCard; }
            set { strCustomerIDCard = value; }
        }

        /// <summary>
        /// Content
        /// Nội dung nhập
        /// </summary>
        public string Content
        {
            get { return strContent; }
            set { strContent = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// Kho tạo
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// InputStoreID
        /// Kho nhập
        /// </summary>
        public int InputStoreID
        {
            get { return intInputStoreID; }
            set { intInputStoreID = value; }
        }

        /// <summary>
        /// AutoStoreChangeToStoreID
        /// Kho tự động chuyển đến
        /// </summary>
        public int AutoStoreChangeToStoreID
        {
            get { return intAutoStoreChangeToStoreID; }
            set { intAutoStoreChangeToStoreID = value; }
        }

        /// <summary>
        /// InputTypeID
        /// Hình thức nhập
        /// </summary>
        public int InputTypeID
        {
            get { return intInputTypeID; }
            set { intInputTypeID = value; }
        }

        /// <summary>
        /// PayableTypeID
        /// Hình thức thanh toán
        /// </summary>
        public int PayableTypeID
        {
            get { return intPayableTypeID; }
            set { intPayableTypeID = value; }
        }

        /// <summary>
        /// CurrencyUnitID
        /// Loại tiền
        /// </summary>
        public int CurrencyUnitID
        {
            get { return intCurrencyUnitID; }
            set { intCurrencyUnitID = value; }
        }

        /// <summary>
        /// DiscountReasonID
        /// Lý do giảm giá
        /// </summary>
        public int DiscountReasonID
        {
            get { return intDiscountReasonID; }
            set { intDiscountReasonID = value; }
        }

        /// <summary>
        /// CreateDate
        /// Ngày tạo
        /// </summary>
        public DateTime? CreateDate
        {
            get { return dtmCreateDate; }
            set { dtmCreateDate = value; }
        }

        /// <summary>
        /// InVoiceDate
        /// Ngày hóa đơn
        /// </summary>
        public DateTime? InVoiceDate
        {
            get { return dtmInVoiceDate; }
            set { dtmInVoiceDate = value; }
        }

        /// <summary>
        /// InputDate
        /// Ngày nhập
        /// </summary>
        public DateTime? InputDate
        {
            get { return dtmInputDate; }
            set { dtmInputDate = value; }
        }

        /// <summary>
        /// PayABLedate
        /// Ngày thanh toán
        /// </summary>
        public DateTime? PayableDate
        {
            get { return dtmPayableDate; }
            set { dtmPayableDate = value; }
        }

        /// <summary>
        /// TaxMonth
        /// Tháng khai báo thuế
        /// </summary>
        public DateTime? TaxMonth
        {
            get { return dtmTaxMonth; }
            set { dtmTaxMonth = value; }
        }

        /// <summary>
        /// CurrencyExchange
        /// Tỷ giá
        /// </summary>
        public decimal CurrencyExchange
        {
            get { return decCurrencyExchange; }
            set { decCurrencyExchange = value; }
        }

        /// <summary>
        /// Discount
        /// Giảm giá
        /// </summary>
        public decimal Discount
        {
            get { return decDiscount; }
            set { decDiscount = value; }
        }

        /// <summary>
        /// ProtectPriceDiscount
        /// Chiết khấu bảo vệ giá
        /// </summary>
        public decimal ProtectPriceDiscount
        {
            get { return decProtectPriceDiscount; }
            set { decProtectPriceDiscount = value; }
        }

        /// <summary>
        /// TotalAmountBFT
        /// Tổng tiền trước thuế
        /// </summary>
        public decimal TotalAmountBFT
        {
            get { return decTotalAmountBFT; }
            set { decTotalAmountBFT = value; }
        }

        /// <summary>
        /// TotalVAT
        /// Tổng tiền thuế
        /// </summary>
        public decimal TotalVAT
        {
            get { return decTotalVAT; }
            set { decTotalVAT = value; }
        }

        /// <summary>
        /// TotalAmount
        /// Tổng tiền sau thuế
        /// </summary>
        public decimal TotalAmount
        {
            get { return decTotalAmount; }
            set { decTotalAmount = value; }
        }

        /// <summary>
        /// CreatedUser
        /// Nhân viên tạo phiếu
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// StaffUser
        /// Nhân viên nhập
        /// </summary>
        public string StaffUser
        {
            get { return strStaffUser; }
            set { strStaffUser = value; }
        }

        /// <summary>
        /// IsNew
        /// Là hàng mới
        /// </summary>
        public bool IsNew
        {
            get { return bolIsNew; }
            set { bolIsNew = value; }
        }

        /// <summary>
        /// IsReturnWithFee
        /// Là nhập trả hàng có thu phí
        /// </summary>
        public bool IsReturnWithFee
        {
            get { return bolIsReturnWithFee; }
            set { bolIsReturnWithFee = value; }
        }

        /// <summary>
        /// ISStoreChange
        /// Là phiếu chuyển kho
        /// </summary>
        public bool ISStoreChange
        {
            get { return bolISStoreChange; }
            set { bolISStoreChange = value; }
        }

        /// <summary>
        /// IsCheckRealInput
        /// Đã xác nhận nhập hàng
        /// </summary>
        public bool IsCheckRealInput
        {
            get { return bolIsCheckRealInput; }
            set { bolIsCheckRealInput = value; }
        }

        /// <summary>
        /// ISPOSTED
        /// Đã hạch toán
        /// </summary>
        public bool IsPosted
        {
            get { return bolISposted; }
            set { bolISposted = value; }
        }

        /// <summary>
        /// CheckRealInputNote
        /// Nội dung xác nhận nhận hàng
        /// </summary>
        public string CheckRealInputNote
        {
            get { return strCheckRealInputNote; }
            set { strCheckRealInputNote = value; }
        }

        /// <summary>
        /// CheckRealInputUser
        /// Người xác nhận nhận hàng
        /// </summary>
        public string CheckRealInputUser
        {
            get { return strCheckRealInputUser; }
            set { strCheckRealInputUser = value; }
        }

        /// <summary>
        /// CheckRealInputTime
        /// TG xác nhận nhận hàng
        /// </summary>
        public DateTime? CheckRealInputTime
        {
            get { return dtmCheckRealInputTime; }
            set { dtmCheckRealInputTime = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        /// <summary>
        /// ContentDeleted
        /// 
        /// </summary>
        public string ContentDeleted
        {
            get { return strContentDeleted; }
            set { strContentDeleted = value; }
        }

        /// <summary>
        /// Có tồn tại không?
        /// </summary>
        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        public bool IsAutoCreateInvoice
        {
            get { return bolIsAutoCreateInvoice; }
            set { bolIsAutoCreateInvoice = value; }
        }

        public string Note
        {
            get { return strNote; }
            set { strNote = value; }
        }

        public DateTime? DeliveryNoteDate
        {
            get { return dtmDeliveryNoteDate; }
            set { dtmDeliveryNoteDate = value; }
        }
        #endregion


        #region Constructor

        public InputVoucher()
        {
        }
        public InputVoucher(string strInputVoucherID, string strOrderID, string strInVoiceID, string strInVoiceSymbol, string strDenominator, int intCustomerID, string strCustomerName, string strCustomerAddress, string strCustomerPhone, string strCustomerTaxID, string strCustomerEmail, DateTime? dtmCustomerBirthday, string strCustomerIDCard, string strContent, int intCreatedStoreID, int intInputStoreID, int intAutoStoreChangeToStoreID, int intInputTypeID, int intPayableTypeID, int intCurrencyUnitID, int intDiscountReasonID, DateTime? dtmCreateDate, DateTime? dtmInVoiceDate, DateTime? dtmInputDate, DateTime? dtmPayABLedate, DateTime? dtmTaxMonth, decimal decCurrencyExchange, decimal decDiscount, decimal decProtectPriceDiscount, decimal decTotalAmountBFT, decimal decTotalVAT, decimal decTotalAmount, string strCreatedUser, string strStaffUser, bool bolIsNew, bool bolIsReturnWithFee, bool bolISStoreChange, bool bolIsCheckRealInput, bool bolISposted, string strCheckRealInputNote, string strCheckRealInputUser, DateTime? dtmCheckRealInputTime, string strUpdatedUser, DateTime? dtmUpdatedDate, bool bolIsDeleted, string strDeletedUser, DateTime? dtmDeletedDate, string strContentDeleted)
        {
            this.strInputVoucherID = strInputVoucherID;
            this.strOrderID = strOrderID;
            this.strInVoiceID = strInVoiceID;
            this.strInVoiceSymbol = strInVoiceSymbol;
            this.strDenominator = strDenominator;
            this.intCustomerID = intCustomerID;
            this.strCustomerName = strCustomerName;
            this.strCustomerAddress = strCustomerAddress;
            this.strCustomerPhone = strCustomerPhone;
            this.strCustomerTaxID = strCustomerTaxID;
            this.strCustomerEmail = strCustomerEmail;
            this.dtmCustomerBirthday = dtmCustomerBirthday;
            this.strCustomerIDCard = strCustomerIDCard;
            this.strContent = strContent;
            this.intCreatedStoreID = intCreatedStoreID;
            this.intInputStoreID = intInputStoreID;
            this.intAutoStoreChangeToStoreID = intAutoStoreChangeToStoreID;
            this.intInputTypeID = intInputTypeID;
            this.intPayableTypeID = intPayableTypeID;
            this.intCurrencyUnitID = intCurrencyUnitID;
            this.intDiscountReasonID = intDiscountReasonID;
            this.dtmCreateDate = dtmCreateDate;
            this.dtmInVoiceDate = dtmInVoiceDate;
            this.dtmInputDate = dtmInputDate;
            this.dtmPayableDate = dtmPayABLedate;
            this.dtmTaxMonth = dtmTaxMonth;
            this.decCurrencyExchange = decCurrencyExchange;
            this.decDiscount = decDiscount;
            this.decProtectPriceDiscount = decProtectPriceDiscount;
            this.decTotalAmountBFT = decTotalAmountBFT;
            this.decTotalVAT = decTotalVAT;
            this.decTotalAmount = decTotalAmount;
            this.strCreatedUser = strCreatedUser;
            this.strStaffUser = strStaffUser;
            this.bolIsNew = bolIsNew;
            this.bolIsReturnWithFee = bolIsReturnWithFee;
            this.bolISStoreChange = bolISStoreChange;
            this.bolIsCheckRealInput = bolIsCheckRealInput;
            this.bolISposted = bolISposted;
            this.strCheckRealInputNote = strCheckRealInputNote;
            this.strCheckRealInputUser = strCheckRealInputUser;
            this.dtmCheckRealInputTime = dtmCheckRealInputTime;
            this.strUpdatedUser = strUpdatedUser;
            this.dtmUpdatedDate = dtmUpdatedDate;
            this.bolIsDeleted = bolIsDeleted;
            this.strDeletedUser = strDeletedUser;
            this.dtmDeletedDate = dtmDeletedDate;
            this.strContentDeleted = strContentDeleted;

        }
        #endregion


        #region Column Names

        public const String colInputVoucherID = "InputVoucherID";
        public const String colOrderID = "OrderID";
        public const String colInVoiceID = "InVoiceID";
        public const String colInVoiceSymbol = "InVoiceSymbol";
        public const String colDENOMinAToR = "DENOMINATOR";
        public const String colCustomerID = "CustomerID";
        public const String colCustomerName = "CUSTOMERNAME";
        public const String colCustomerAddress = "CUSTOMERADDRESS";
        public const String colCustomerPhone = "CustomerPhone";
        public const String colCustomerTaxID = "CustomerTaxID";
        public const String colCustomerEmail = "CustomerEmail";
        public const String colCustomerBirthday = "CustomerBirthday";
        public const String colCustomerIDCard = "CustomerIDCard";
        public const String colContent = "Content";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colInputStoreID = "InputStoreID";
        public const String colAutoStoreChangeToStoreID = "AutoStoreChangeToStoreID";
        public const String colInputTypeID = "InputTypeID";
        public const String colPayableTypeID = "PayableTypeID";
        public const String colCurrencyUnitID = "CurrencyUnitID";
        public const String colDiscountReasonID = "DiscountReasonID";
        public const String colCreateDate = "CreateDate";
        public const String colInVoiceDate = "InVoiceDate";
        public const String colInputDate = "InputDate";
        public const String colPayABLedate = "PAYABLEDATE";
        public const String colTaxMonth = "TaxMonth";
        public const String colCurrencyExchange = "CurrencyExchange";
        public const String colDiscount = "Discount";
        public const String colPROTECTPriceDiscount = "PROTECTPRICEDISCOUNT";
        public const String colTotalAmountBFT = "TOTALAMOUNTBFT";
        public const String colTotalVAT = "TOTALVAT";
        public const String colTotalAmount = "TotalAmount";
        public const String colCreatedUser = "CreatedUser";
        public const String colStaffUser = "StaffUser";
        public const String colIsNew = "IsNew";
        public const String colIsReturnWithFee = "ISRETURNWITHFEE";
        public const String colISStoreChange = "ISSTORECHANGE";
        public const String colIsCheckRealInput = "ISCHECKREALINPUT";
        public const String colISPOSTED = "ISPOSTED";
        public const String colCheckRealInputNote = "CheckRealInputNote";
        public const String colCheckRealInputUser = "CheckRealInputUser";
        public const String colCheckRealInputTime = "CheckRealInputTime";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colContentDeleted = "ContentDeleted";
        public const String colIdCardIssueDate = "IdCardIssueDate";
        public const String colIdCardIssuePlace = "IdCardIssuePlace";
        public const String colTransportCustomerID = "TransportCustomerID";

        public const String colIDCardIssueDate = "IdCardIssueDate";
        public const String colIDCardIssuePlace = "IdCardIssuePlace";
        public const String colIsErrorProduct = "IsErrorProduct";
        public const String colErrorProductNote = "ErrorProductNote";
        public const String colIsAutoCreateInvoice = "IsAutoCreateInvoice";
        public const String colNote = "Note";
        public const String colDeliveryNoteDate = "DeliveryNoteDate";
        
        #endregion //Column names


    }
}
