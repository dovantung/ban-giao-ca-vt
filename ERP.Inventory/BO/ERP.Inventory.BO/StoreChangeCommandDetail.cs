
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn  
	/// Created date 	: 11/22/2012 
	/// 
	/// </summary>	
	public class StoreChangeCommandDetail
	{	
	
	
		#region Member Variables

		private string strStoreChangeCommandDetailID = string.Empty;
		private string strStoreChangeCommandID = string.Empty;
		private string strProductID = string.Empty;
		private decimal decQuantity;
		private decimal decStoreChangeQuantity;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;

        private string strOrderID = string.Empty;
        public string OrderID
        {
            get { return strOrderID; }
            set { strOrderID = value; }
        }
		#endregion


		#region Properties 

		/// <summary>
		/// StoreChangeCommandDetailID
		/// 
		/// </summary>
		public string StoreChangeCommandDetailID
		{
			get { return  strStoreChangeCommandDetailID; }
			set { strStoreChangeCommandDetailID = value; }
		}

		/// <summary>
		/// StoreChangeCommandID
		/// 
		/// </summary>
		public string StoreChangeCommandID
		{
			get { return  strStoreChangeCommandID; }
			set { strStoreChangeCommandID = value; }
		}

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// Quantity
		/// 
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// StoreChangeQuantity
		/// 
		/// </summary>
		public decimal StoreChangeQuantity
		{
			get { return  decStoreChangeQuantity; }
			set { decStoreChangeQuantity = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public StoreChangeCommandDetail()
		{
		}
		#endregion


		#region Column Names

		public const String colStoreChangeCommandDetailID = "StoreChangeCommandDetailID";
		public const String colStoreChangeCommandID = "StoreChangeCommandID";
		public const String colProductID = "ProductID";
		public const String colQuantity = "Quantity";
		public const String colStoreChangeQuantity = "StoreChangeQuantity";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
