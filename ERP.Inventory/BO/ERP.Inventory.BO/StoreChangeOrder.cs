
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
    /// Created by 		: Hoang Nhu Phong 
    /// Created date 	: 9/28/2012 
    /// Yêu cầu chuyển kho
    /// </summary>	
    public partial class StoreChangeOrder
    {


        #region Member Variables

        private string strStoreChangeOrderID = string.Empty;
        private int intStoreChangeOrderTypeID = 0;
        private int intStoreChangeTypeID = 0;
        private int intTransportTypeID = -1;
        private int intTransportCompanyID = -1;
        private int intTransportServicesID = -1;
        private int intFromStoreID = -1;
        private int intToStoreID = -1;
        private string strContent = string.Empty;
        private DateTime? dtmOrderDate;
        private DateTime? dtmExpiryDate;
        private bool bolIsReviewed = false;
        private bool bolISORTHERPRODUCT = false;
        private string strReviewedUser = string.Empty;
        private DateTime? dtmReviewedDate;
        private int intCreatedStoreID = 0;
        private int intStoreChangeStatus = 0;
        private bool bolIsNew = false;
        private decimal decTotalCommandQuantity;
        private decimal decTotalQuantity;
        private decimal decTotalStoreChangeQuantity;
        private bool bolIsUrgent = false;
        private string strUpdatedIsUrgentIUser = string.Empty;
        private DateTime? dtmUpdatedIsUrgentDate;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private string strContentDeleted = string.Empty;
        private bool bolIsExist = false;

        #endregion


        #region Properties

        /// <summary>
        /// StoreChangeOrderID
        /// Mã yêu cầu xuất chuyển
        /// </summary>
        public string StoreChangeOrderID
        {
            get { return strStoreChangeOrderID; }
            set { strStoreChangeOrderID = value; }
        }

        /// <summary>
        /// StoreChangeOrderTypeID
        /// Loại yêu cầu chuyển kho
        /// </summary>
        public int StoreChangeOrderTypeID
        {
            get { return intStoreChangeOrderTypeID; }
            set { intStoreChangeOrderTypeID = value; }
        }

        /// <summary>
        /// StoreChangeTypeID
        /// Loại chuyển kho
        /// </summary>
        public int StoreChangeTypeID
        {
            get { return intStoreChangeTypeID; }
            set { intStoreChangeTypeID = value; }
        }

        /// <summary>
        /// TransportTypeID
        /// Phương tiện vận chuyển
        /// </summary>
        public int TransportTypeID
        {
            get { return intTransportTypeID; }
            set { intTransportTypeID = value; }
        }


        /// <summary>
        /// TransportTypeID
        /// đối tác vận chuyển
        /// </summary>
        public int TransportCompanyID
        {
            get { return intTransportCompanyID; }
            set { intTransportCompanyID = value; }
        }

        /// <summary>
        /// TransportTypeID
        /// dịch vụ vận chuyển
        /// </summary>
        public int TransportServicesID
        {
            get { return intTransportServicesID; }
            set { intTransportServicesID = value; }
        }
        /// <summary>
        /// FromStoreID
        /// Kho xuất
        /// </summary>
        public int FromStoreID
        {
            get { return intFromStoreID; }
            set { intFromStoreID = value; }
        }

        /// <summary>
        /// ToStoreID
        /// Kho nhập
        /// </summary>
        public int ToStoreID
        {
            get { return intToStoreID; }
            set { intToStoreID = value; }
        }

        /// <summary>
        /// Content
        /// 
        /// </summary>
        public string Content
        {
            get { return strContent; }
            set { strContent = value; }
        }

        /// <summary>
        /// OrderDate
        /// 
        /// </summary>
        public DateTime? OrderDate
        {
            get { return dtmOrderDate; }
            set { dtmOrderDate = value; }
        }

        /// <summary>
        /// ExpiryDate
        /// Ngày hết hạn chuyển kho
        /// </summary>
        public DateTime? ExpiryDate
        {
            get { return dtmExpiryDate; }
            set { dtmExpiryDate = value; }
        }

        /// <summary>
        /// IsReviewed
        /// Đã duyệt hay chưa
        /// </summary>
        public bool IsReviewed
        {
            get { return bolIsReviewed; }
            set { bolIsReviewed = value; }
        }
        /// <summary>
        /// ISORTHERPRODUCT
        /// hàng hóa khác 
        /// </summary>
        public bool ISORTHERPRODUCT
        {
            get { return bolISORTHERPRODUCT; }
            set { bolISORTHERPRODUCT = value; }
        }

        /// <summary>
        /// ReviewedUser
        /// Nhân viên duyệt
        /// </summary>
        public string ReviewedUser
        {
            get { return strReviewedUser; }
            set { strReviewedUser = value; }
        }

        /// <summary>
        /// ReviewedDate
        /// Ngày duyệt
        /// </summary>
        public DateTime? ReviewedDate
        {
            get { return dtmReviewedDate; }
            set { dtmReviewedDate = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// 
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// StoreChangeStatus
        /// Trạng thái chuyển kho; 0: Chưa chuyển kho, 1: Đã chuyển nhưng chưa xong, 2: Đã chuyển kho xong; 3: Kết thúc
        /// </summary>
        public int StoreChangeStatus
        {
            get { return intStoreChangeStatus; }
            set { intStoreChangeStatus = value; }
        }

        /// <summary>
        /// IsNew
        /// Mới hay cũ
        /// </summary>
        public bool IsNew
        {
            get { return bolIsNew; }
            set { bolIsNew = value; }
        }

        /// <summary>
        /// TotalCommandQuantity
        /// Tổng số lượng từ lệnh chuyển kho
        /// </summary>
        public decimal TotalCommandQuantity
        {
            get { return decTotalCommandQuantity; }
            set { decTotalCommandQuantity = value; }
        }

        /// <summary>
        /// TotalQuantity
        /// Tổng số lượng yêu cầu
        /// </summary>
        public decimal TotalQuantity
        {
            get { return decTotalQuantity; }
            set { decTotalQuantity = value; }
        }

        /// <summary>
        /// TotalStoreChangeQuantity
        /// Tổng số lượng chuyển kho
        /// </summary>
        public decimal TotalStoreChangeQuantity
        {
            get { return decTotalStoreChangeQuantity; }
            set { decTotalStoreChangeQuantity = value; }
        }

        /// <summary>
        /// IsUrgent
        /// Là yêu cầu chuyển kho khẩn cấp
        /// </summary>
        public bool IsUrgent
        {
            get { return bolIsUrgent; }
            set { bolIsUrgent = value; }
        }

        /// <summary>
        /// UpdatedIsUrgentIUser
        /// 
        /// </summary>
        public string UpdatedIsUrgentIUser
        {
            get { return strUpdatedIsUrgentIUser; }
            set { strUpdatedIsUrgentIUser = value; }
        }

        /// <summary>
        /// UpdatedIsUrgentDate
        /// 
        /// </summary>
        public DateTime? UpdatedIsUrgentDate
        {
            get { return dtmUpdatedIsUrgentDate; }
            set { dtmUpdatedIsUrgentDate = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        /// <summary>
        /// ContentDeleted
        /// 
        /// </summary>
        public string ContentDeleted
        {
            get { return strContentDeleted; }
            set { strContentDeleted = value; }
        }
        

        /// <summary>
        /// Có tồn tại không?
        /// </summary>
        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        public string StoreChangeCommandID { get; set; }

        #endregion


        #region Constructor

        public StoreChangeOrder()
        {
        }
        public StoreChangeOrder(string strStoreChangeOrderID, int intStoreChangeOrderTypeID, int intStoreChangeTypeID, int intTransportTypeID, int intFromStoreID, int intToStoreID, string strContent, DateTime? dtmOrderDate, DateTime? dtmExpiryDate, bool bolIsReviewed, string strReviewedUser, DateTime? dtmReviewedDate, int intCreatedStoreID, int intStoreChangeStatus, bool bolIsNew, decimal decTotalCommandQuantity, decimal decTotalQuantity, decimal decTotalStoreChangeQuantity, bool bolIsUrgent, string strUpdatedIsUrgentIUser, DateTime? dtmUpdatedIsUrgentDate, string strCreatedUser, DateTime? dtmCreatedDate, string strUpdatedUser, DateTime? dtmUpdatedDate, bool bolIsDeleted, string strDeletedUser, DateTime? dtmDeletedDate, string strContentDeleted)
        {
            this.strStoreChangeOrderID = strStoreChangeOrderID;
            this.intStoreChangeOrderTypeID = intStoreChangeOrderTypeID;
            this.intStoreChangeTypeID = intStoreChangeTypeID;
            this.intTransportTypeID = intTransportTypeID;
            this.intFromStoreID = intFromStoreID;
            this.intToStoreID = intToStoreID;
            this.strContent = strContent;
            this.dtmOrderDate = dtmOrderDate;
            this.dtmExpiryDate = dtmExpiryDate;
            this.bolIsReviewed = bolIsReviewed;
            this.strReviewedUser = strReviewedUser;
            this.dtmReviewedDate = dtmReviewedDate;
            this.intCreatedStoreID = intCreatedStoreID;
            this.intStoreChangeStatus = intStoreChangeStatus;
            this.bolIsNew = bolIsNew;
            this.decTotalCommandQuantity = decTotalCommandQuantity;
            this.decTotalQuantity = decTotalQuantity;
            this.decTotalStoreChangeQuantity = decTotalStoreChangeQuantity;
            this.bolIsUrgent = bolIsUrgent;
            this.strUpdatedIsUrgentIUser = strUpdatedIsUrgentIUser;
            this.dtmUpdatedIsUrgentDate = dtmUpdatedIsUrgentDate;
            this.strCreatedUser = strCreatedUser;
            this.dtmCreatedDate = dtmCreatedDate;
            this.strUpdatedUser = strUpdatedUser;
            this.dtmUpdatedDate = dtmUpdatedDate;
            this.bolIsDeleted = bolIsDeleted;
            this.strDeletedUser = strDeletedUser;
            this.dtmDeletedDate = dtmDeletedDate;
            this.strContentDeleted = strContentDeleted;

        }
        #endregion


        #region Column Names

        public const String colStoreChangeOrderID = "StoreChangeOrderID";
        public const String colStoreChangeOrderTypeID = "StoreChangeOrderTypeID";
        public const String colStoreChangeTypeID = "StoreChangeTypeID";
        public const String colTransportTypeID = "TransportTypeID";
        public const String colTransportCompanyID = "TransportCompanyID";
        public const String colTransportServicesID = "TransportServicesID";
        public const String colFromStoreID = "FromStoreID";
        public const String colToStoreID = "ToStoreID";
        public const String colContent = "Content";
        public const String colOrderDate = "OrderDate";
        public const String colExpiryDate = "ExpiryDate";
        public const String colIsReviewed = "IsReviewed";
        public const String colISORTHERPRODUCT = "ISORTHERPRODUCT"; 
        public const String colReviewedUser = "ReviewedUser";
        public const String colReviewedDate = "ReviewedDate";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colStoreChangeStatus = "StoreChangeStatus";
        public const String colIsNew = "IsNew";
        public const String colTotalCommandQuantity = "TotalCommandQuantity";
        public const String colTotalQuantity = "TotalQuantity";
        public const String colTotalStoreChangeQuantity = "TotalStoreChangeQuantity";
        public const String colIsUrgent = "IsUrgent";
        public const String colUpdatedIsUrgentIUser = "UpdatedIsUrgentIUser";
        public const String colUpdatedIsUrgentDate = "UpdatedIsUrgentDate";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colContentDeleted = "ContentDeleted";
        public const String colIsExpired = "IsExpired";
        public const String colStoreChangeCommandID = "StoreChangeCommandID";
        public const String colInStockStatusID = "INSTOCKSTATUSID";
        #endregion //Column names


    }
}
