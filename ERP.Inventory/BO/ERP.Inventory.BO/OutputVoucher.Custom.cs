﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO
{
    public partial class OutputVoucher
    {
        private List<OutputVoucherDetail> lstOutputVoucherDetailList = null;

        public List<OutputVoucherDetail> OutputVoucherDetailList
        {
            get { return lstOutputVoucherDetailList; }
            set { lstOutputVoucherDetailList = value; }
        }

        private List<OutputVoucherDetail> lstOutputVoucherDetailCouponList = null;

        public List<OutputVoucherDetail> OutputVoucherDetailCouponList
        {
            get { return lstOutputVoucherDetailCouponList; }
            set { lstOutputVoucherDetailCouponList = value; }
        }

    }
}
