
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
    /// Created by 		: Nguyễn Quang Tú 
    /// Created date 	: 17/06/2013 
    /// Thông tin về thuộc tính sản phẩm
    /// </summary>	
    public class IMEISalesInfo_ProSpec
    {


        #region Member Variables

        private string strProductID = string.Empty;
        private string strIMEI = string.Empty;
        private int intProductSpecID = 0;
        private int intProductSpecStatusID = 0;
        private string strProductSpecName = string.Empty;
        private string strProductSpecStatusName = string.Empty;
        private int intSetPriceProductSpecStatusID = 0;
        private string strSetPriceProductSpecStatusName = string.Empty;
        private string strNote = string.Empty;
        private bool bolIsExist = false;
        private string strUpdatedUser = string.Empty;
        private bool bolIsUsedBySalesInfo = false;
        private bool bolIsCanPrint = false;

        public bool IsCanPrint
        {
            get { return bolIsCanPrint; }
            set { bolIsCanPrint = value; }
        }
        
        #endregion


        #region Properties

        /// <summary>
        /// ProductID
        /// 
        /// </summary>
        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }

        /// <summary>
        /// IMEI
        /// 
        /// </summary>
        public string IMEI
        {
            get { return strIMEI; }
            set { strIMEI = value; }
        }

        /// <summary>
        /// ProductSpecID
        /// 
        /// </summary>
        public int ProductSpecID
        {
            get { return intProductSpecID; }
            set { intProductSpecID = value; }
        }

        /// <summary>
        /// ProductSpecStatusID
        /// 
        /// </summary>
        public int ProductSpecStatusID
        {
            get { return intProductSpecStatusID; }
            set { intProductSpecStatusID = value; }
        }

        public string ProductSpecName
        {
            get { return strProductSpecName; }
            set { strProductSpecName = value; }
        }

        public string ProductSpecStatusName
        {
            get { return strProductSpecStatusName; }
            set { strProductSpecStatusName = value; }
        }

        public int SetPriceProductSpecStatusID
        {
            get { return intSetPriceProductSpecStatusID; }
            set { intSetPriceProductSpecStatusID = value; }
        }
        public string SetPriceProductSpecStatusName
        {
            get { return strSetPriceProductSpecStatusName; }
            set { strSetPriceProductSpecStatusName = value; }
        }
        /// <summary>
        /// Note
        /// 
        /// </summary>
        public string Note
        {
            get { return strNote; }
            set { strNote = value; }
        }

        /// <summary>
        /// Có tồn tại không?
        /// </summary>
        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        /// <summary>
        /// UpdateUser
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        public bool IsUsedBySalesInfo
        {
            get { return bolIsUsedBySalesInfo; }
            set { bolIsUsedBySalesInfo = value; }
        }
        #endregion


        #region Constructor

        public IMEISalesInfo_ProSpec()
        {
        }
        public IMEISalesInfo_ProSpec(string strProductID, string strIMEI, int intProductSpecID, int intProductSpecStatusID, string strNote)
        {
            this.strProductID = strProductID;
            this.strIMEI = strIMEI;
            this.intProductSpecID = intProductSpecID;
            this.intProductSpecStatusID = intProductSpecStatusID;
            this.strNote = strNote;
        }
        #endregion


        #region Column Names

        public const String colProductID = "ProductID";
        public const String colIMEI = "IMEI";
        public const String colProductSpecID = "ProductSpecID";
        public const String colProductSpecStatusID = "ProductSpecStatusID";
        public const String colNote = "Note";
        public const String colProductSpecName = "ProductSpecName";
        public const String colSetPriceProductSpecStatusID = "SetPriceProductSpecStatusID";
        public const String colSetPriceProductSpecStatusName = "SetPriceProductSpecStatusName";
        public const String colIsUsedBySalesInfo = "IsUsedBySalesInfo";
        public const String colIsCanPrint = "IsCanPrint";
        
        #endregion //Column names


    }
}
