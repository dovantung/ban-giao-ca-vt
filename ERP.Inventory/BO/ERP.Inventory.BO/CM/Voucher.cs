
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.CM
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/18/2012 
	/// Phiếu thu
	/// </summary>	
	public class Voucher
	{	
	
	
		#region Member Variables

		private string strVoucherID = string.Empty;
		private string strVoucherConcern = string.Empty;
		private string strOrderID = string.Empty;
		private string strInVoiceID = string.Empty;
		private string strInVoiceSymbol = string.Empty;
		private string strDENOMinAToR = string.Empty;
		private int intCustomerID = 0;
		private string strCustomerName = string.Empty;
		private string strCustomerAddress = string.Empty;
		private string strCustomerPhone = string.Empty;
		private string strCustomerTaxID = string.Empty;
		private string strCustomerEmail = string.Empty;
		private DateTime? dtmCustomerBirthday;
		private string strCustomerIDCard = string.Empty;
		private int intProvinceID = 0;
		private int intDistrictID = 0;
		private bool bolGender = false;
		private int intAgeRangeID = 0;
		private DateTime? dtmInVoiceDate;
		private DateTime? dtmVoucherDate;
		private string strCashierUser = string.Empty;
		private string strContent = string.Empty;
		private int intCreatedStoreID = 0;
		private int intVoucherStoreID = 0;
		private int intVoucherTypeID = 0;
		private int intCurrencyUnitID = 0;
		private int intVAT = 0;
		private decimal decCurrencyExchange;
		private decimal decTotalMoney;
		private decimal decTotalAdvance;
		private decimal decDiscount;
		private decimal decTotalLiquidate;
		private decimal decDebt;
		private decimal decPromotionDiscount;
		private decimal decShippingCost;
		private bool bolIsSupplementARY = false;
		private decimal decPayableAmount;
		private bool bolISInVoucherOFSaleOrder = false;
		private bool bolIsViewEDINPayable = false;
		private DateTime? dtmFinishPaidDate;
		private bool bolIsCheckEDPayable = false;
		private string strNoteCheckEDPayable = string.Empty;
		private bool bolIsCreateDByGetABLE = false;
		private bool bolIsMove = false;
		private string strMoveMENTVoucherDetailID = string.Empty;
		private string strUserMove = string.Empty;
		private DateTime? dtmMoveTime;
		private string strMoveLOGID = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreateDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strContentDeleted = string.Empty;
		private bool bolIsExist = false;
        private List<VoucherDetail> lstVoucherDetail = null;
     
		#endregion

		#region Properties 
        public List<VoucherDetail> VoucherDetailList
        {
            get { return lstVoucherDetail; }
            set { lstVoucherDetail = value; }
        }

		/// <summary>
		/// VoucherID
		/// Mã phiếu thu
		/// </summary>
		public string VoucherID
		{
			get { return  strVoucherID; }
			set { strVoucherID = value; }
		}

		/// <summary>
		/// VoucherConcern
		/// Mã chứng từ liên quan
		/// </summary>
		public string VoucherConcern
		{
			get { return  strVoucherConcern; }
			set { strVoucherConcern = value; }
		}

		/// <summary>
		/// OrderID
		/// Mã đơn hàng
		/// </summary>
		public string OrderID
		{
			get { return  strOrderID; }
			set { strOrderID = value; }
		}

		/// <summary>
		/// InVoiceID
		/// Số hóa đơn
		/// </summary>
		public string InVoiceID
		{
			get { return  strInVoiceID; }
			set { strInVoiceID = value; }
		}

		/// <summary>
		/// InVoiceSymbol
		/// Ký hiệu hóa đơn
		/// </summary>
		public string InVoiceSymbol
		{
			get { return  strInVoiceSymbol; }
			set { strInVoiceSymbol = value; }
		}

		/// <summary>
		/// DENOMinAToR
		/// Mẫu số
		/// </summary>
		public string DENOMinAToR
		{
			get { return  strDENOMinAToR; }
			set { strDENOMinAToR = value; }
		}

		/// <summary>
		/// CustomerID
		/// Mã khách hàng
		/// </summary>
		public int CustomerID
		{
			get { return  intCustomerID; }
			set { intCustomerID = value; }
		}

		/// <summary>
		/// CustomerName
		/// Tên khách hàng
		/// </summary>
		public string CustomerName
		{
			get { return  strCustomerName; }
			set { strCustomerName = value; }
		}

		/// <summary>
		/// CustomerAddress
		/// Địa chỉ
		/// </summary>
		public string CustomerAddress
		{
			get { return  strCustomerAddress; }
			set { strCustomerAddress = value; }
		}

		/// <summary>
		/// CustomerPhone
		/// Số điện thoại
		/// </summary>
		public string CustomerPhone
		{
			get { return  strCustomerPhone; }
			set { strCustomerPhone = value; }
		}

		/// <summary>
		/// CustomerTaxID
		/// Mã số thuế
		/// </summary>
		public string CustomerTaxID
		{
			get { return  strCustomerTaxID; }
			set { strCustomerTaxID = value; }
		}

		/// <summary>
		/// CustomerEmail
		/// Email khách hàng
		/// </summary>
		public string CustomerEmail
		{
			get { return  strCustomerEmail; }
			set { strCustomerEmail = value; }
		}

		/// <summary>
		/// CustomerBirthday
		/// Ngày sinh
		/// </summary>
		public DateTime? CustomerBirthday
		{
			get { return  dtmCustomerBirthday; }
			set { dtmCustomerBirthday = value; }
		}

		/// <summary>
		/// CustomerIDCard
		/// Số chứng minh nhân nhân khách hàng
		/// </summary>
		public string CustomerIDCard
		{
			get { return  strCustomerIDCard; }
			set { strCustomerIDCard = value; }
		}

		/// <summary>
		/// ProvinceID
		/// Tỉnh/Tp
		/// </summary>
		public int ProvinceID
		{
			get { return  intProvinceID; }
			set { intProvinceID = value; }
		}

		/// <summary>
		/// DistrictID
		/// Quận/huyện
		/// </summary>
		public int DistrictID
		{
			get { return  intDistrictID; }
			set { intDistrictID = value; }
		}

		/// <summary>
		/// Gender
		/// Giới tính
		/// </summary>
		public bool Gender
		{
			get { return  bolGender; }
			set { bolGender = value; }
		}

		/// <summary>
		/// AgeRangeID
		/// Độ tuổi
		/// </summary>
		public int AgeRangeID
		{
			get { return  intAgeRangeID; }
			set { intAgeRangeID = value; }
		}

		/// <summary>
		/// InVoiceDate
		/// Ngày hóa đơn
		/// </summary>
		public DateTime? InVoiceDate
		{
			get { return  dtmInVoiceDate; }
			set { dtmInVoiceDate = value; }
		}

		/// <summary>
		/// VoucherDate
		/// Ngày thu chi
		/// </summary>
		public DateTime? VoucherDate
		{
			get { return  dtmVoucherDate; }
			set { dtmVoucherDate = value; }
		}

		/// <summary>
		/// CashierUser
		/// Thu ngân
		/// </summary>
		public string CashierUser
		{
			get { return  strCashierUser; }
			set { strCashierUser = value; }
		}

		/// <summary>
		/// Content
		/// Nội dung thu chi
		/// </summary>
		public string Content
		{
			get { return  strContent; }
			set { strContent = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// Kho tạo
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// VoucherStoreID
		/// Kho thu chi
		/// </summary>
		public int VoucherStoreID
		{
			get { return  intVoucherStoreID; }
			set { intVoucherStoreID = value; }
		}

		/// <summary>
		/// VoucherTypeID
		/// Loại phiếu thu
		/// </summary>
		public int VoucherTypeID
		{
			get { return  intVoucherTypeID; }
			set { intVoucherTypeID = value; }
		}

		/// <summary>
		/// CurrencyUnitID
		/// Tỷ giá
		/// </summary>
		public int CurrencyUnitID
		{
			get { return  intCurrencyUnitID; }
			set { intCurrencyUnitID = value; }
		}

		/// <summary>
		/// VAT
		/// Thuế
		/// </summary>
		public int VAT
		{
			get { return  intVAT; }
			set { intVAT = value; }
		}

		/// <summary>
		/// CurrencyExchange
		/// Tỷ giá
		/// </summary>
		public decimal CurrencyExchange
		{
			get { return  decCurrencyExchange; }
			set { decCurrencyExchange = value; }
		}

		/// <summary>
		/// TotalMoney
		/// Tổng tiền thu
		/// </summary>
		public decimal TotalMoney
		{
			get { return  decTotalMoney; }
			set { decTotalMoney = value; }
		}

		/// <summary>
		/// TotalAdvance
		/// Đã tạm ứng
		/// </summary>
		public decimal TotalAdvance
		{
			get { return  decTotalAdvance; }
			set { decTotalAdvance = value; }
		}

		/// <summary>
		/// Discount
		/// Giảm giá
		/// </summary>
		public decimal Discount
		{
			get { return  decDiscount; }
			set { decDiscount = value; }
		}

		/// <summary>
		/// TotalLiquidate
		/// Số tiền phải thu
		/// </summary>
		public decimal TotalLiquidate
		{
			get { return  decTotalLiquidate; }
			set { decTotalLiquidate = value; }
		}

		/// <summary>
		/// Debt
		/// Còn nợ lại
		/// </summary>
		public decimal Debt
		{
			get { return  decDebt; }
			set { decDebt = value; }
		}

		/// <summary>
		/// PromotionDiscount
		/// Giảm giá KM
		/// </summary>
		public decimal PromotionDiscount
		{
			get { return  decPromotionDiscount; }
			set { decPromotionDiscount = value; }
		}

		/// <summary>
		/// ShippingCost
		/// Tiền vận chuyển
		/// </summary>
		public decimal ShippingCost
		{
			get { return  decShippingCost; }
			set { decShippingCost = value; }
		}

		/// <summary>
		/// IsSupplementARY
		/// Là phiếu thu bổ sung
		/// </summary>
		public bool IsSupplementARY
		{
			get { return  bolIsSupplementARY; }
			set { bolIsSupplementARY = value; }
		}

		/// <summary>
		/// PayableAmount
		/// 
		/// </summary>
		public decimal PayableAmount
		{
			get { return  decPayableAmount; }
			set { decPayableAmount = value; }
		}

		/// <summary>
		/// ISInVoucherOFSaleOrder
		/// Là phiếu thu tiền từ đơn hàng bán
		/// </summary>
		public bool ISInVoucherOFSaleOrder
		{
			get { return  bolISInVoucherOFSaleOrder; }
			set { bolISInVoucherOFSaleOrder = value; }
		}

		/// <summary>
		/// IsViewEDINPayable
		/// 
		/// </summary>
		public bool IsViewEDINPayable
		{
			get { return  bolIsViewEDINPayable; }
			set { bolIsViewEDINPayable = value; }
		}

		/// <summary>
		/// FinishPaidDate
		/// Ngày thanh toán hết(Debt = 0)
		/// </summary>
		public DateTime? FinishPaidDate
		{
			get { return  dtmFinishPaidDate; }
			set { dtmFinishPaidDate = value; }
		}

		/// <summary>
		/// IsCheckEDPayable
		/// 
		/// </summary>
		public bool IsCheckEDPayable
		{
			get { return  bolIsCheckEDPayable; }
			set { bolIsCheckEDPayable = value; }
		}

		/// <summary>
		/// NoteCheckEDPayable
		/// 
		/// </summary>
		public string NoteCheckEDPayable
		{
			get { return  strNoteCheckEDPayable; }
			set { strNoteCheckEDPayable = value; }
		}

		/// <summary>
		/// IsCreateDByGetABLE
		/// 
		/// </summary>
		public bool IsCreateDByGetABLE
		{
			get { return  bolIsCreateDByGetABLE; }
			set { bolIsCreateDByGetABLE = value; }
		}

		/// <summary>
		/// IsMove
		/// 
		/// </summary>
		public bool IsMove
		{
			get { return  bolIsMove; }
			set { bolIsMove = value; }
		}

		/// <summary>
		/// MoveMENTVoucherDetailID
		/// 
		/// </summary>
		public string MoveMENTVoucherDetailID
		{
			get { return  strMoveMENTVoucherDetailID; }
			set { strMoveMENTVoucherDetailID = value; }
		}

		/// <summary>
		/// UserMove
		/// 
		/// </summary>
		public string UserMove
		{
			get { return  strUserMove; }
			set { strUserMove = value; }
		}

		/// <summary>
		/// MoveTime
		/// 
		/// </summary>
		public DateTime? MoveTime
		{
			get { return  dtmMoveTime; }
			set { dtmMoveTime = value; }
		}

		/// <summary>
		/// MoveLOGID
		/// 
		/// </summary>
		public string MoveLOGID
		{
			get { return  strMoveLOGID; }
			set { strMoveLOGID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreateDate
		/// 
		/// </summary>
		public DateTime? CreateDate
		{
			get { return  dtmCreateDate; }
			set { dtmCreateDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// ContentDeleted
		/// 
		/// </summary>
		public string ContentDeleted
		{
			get { return  strContentDeleted; }
			set { strContentDeleted = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}

		#endregion			
		
		
		#region Constructor

        public Voucher()
		{
		}
		#endregion


		#region Column Names

		public const String colVoucherID = "VoucherID";
		public const String colVoucherConcern = "VoucherConcern";
		public const String colOrderID = "OrderID";
		public const String colInVoiceID = "InVoiceID";
		public const String colInVoiceSymbol = "InVoiceSymbol";
		public const String colDENOMinAToR = "DENOMinAToR";
		public const String colCustomerID = "CustomerID";
		public const String colCustomerName = "CustomerName";
		public const String colCustomerAddress = "CustomerAddress";
		public const String colCustomerPhone = "CustomerPhone";
		public const String colCustomerTaxID = "CustomerTaxID";
		public const String colCustomerEmail = "CustomerEmail";
		public const String colCustomerBirthday = "CustomerBirthday";
		public const String colCustomerIDCard = "CustomerIDCard";
		public const String colProvinceID = "ProvinceID";
		public const String colDistrictID = "DistrictID";
		public const String colGender = "Gender";
		public const String colAgeRangeID = "AgeRangeID";
		public const String colInVoiceDate = "InVoiceDate";
		public const String colVoucherDate = "VoucherDate";
		public const String colCashierUser = "CashierUser";
		public const String colContent = "Content";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colVoucherStoreID = "VoucherStoreID";
		public const String colVoucherTypeID = "VoucherTypeID";
		public const String colCurrencyUnitID = "CurrencyUnitID";
		public const String colVAT = "VAT";
		public const String colCurrencyExchange = "CurrencyExchange";
		public const String colTotalMoney = "TotalMoney";
		public const String colTotalAdvance = "TotalAdvance";
		public const String colDiscount = "Discount";
		public const String colTotalLiquidate = "TotalLiquidate";
		public const String colDebt = "Debt";
		public const String colPromotionDiscount = "PromotionDiscount";
		public const String colShippingCost = "ShippingCost";
		public const String colIsSupplementARY = "IsSupplementARY";
		public const String colPayableAmount = "PayableAmount";
		public const String colISInVoucherOFSaleOrder = "ISInVoucherOFSaleOrder";
		public const String colIsViewEDINPayable = "IsViewEDINPayable";
		public const String colFinishPaidDate = "FinishPaidDate";
		public const String colIsCheckEDPayable = "IsCheckEDPayable";
		public const String colNoteCheckEDPayable = "NoteCheckEDPayable";
		public const String colIsCreateDByGetABLE = "IsCreateDByGetABLE";
		public const String colIsMove = "IsMove";
		public const String colMoveMENTVoucherDetailID = "MoveMENTVoucherDetailID";
		public const String colUserMove = "UserMove";
		public const String colMoveTime = "MoveTime";
		public const String colMoveLOGID = "MoveLOGID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreateDate = "CreateDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colContentDeleted = "ContentDeleted";

		#endregion //Column names

		
	}
}
