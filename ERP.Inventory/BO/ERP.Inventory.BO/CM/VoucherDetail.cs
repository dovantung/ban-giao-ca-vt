
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.CM
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/18/2012 
	/// Chi tiết thu chi
	/// </summary>	
	public class VoucherDetail
	{	
	
	
		#region Member Variables

		private string strVoucherDetailID = string.Empty;
		private string strVoucherID = string.Empty;
		private decimal decVNDCASH;
		private decimal decForeIGNCASH;
		private decimal decForeIGNCASHExchange;
		private decimal decPaymentCardAmount;
		private int intPaymentCardID = 0;
		private decimal decPaymentCardSpend;
		private string strPaymentCardVoucherID = string.Empty;
		private decimal decGiftVoucherAmount;
		private decimal decUNEVENTPriceGiftVoucher;
		private decimal decRefUNDAmount;
		private DateTime? dtmVoucherDate;
		private string strCashierUser = string.Empty;
		private int intCreatedStoreID = 0;
		private int intVoucherStoreID = 0;
		private bool bolIsSupplementARYDetail = false;
		private string strSUPPLEMENTARYVoucherID = string.Empty;
		private bool bolISPOSTED = false;
		private bool bolISNOCASH = false;
		private string strNote = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreateDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strContentDeleted = string.Empty;
		private bool bolIsExist = false;

		#endregion


		#region Properties 

		/// <summary>
		/// VoucherDetailID
		/// Mã chi tiết thu chi
		/// </summary>
		public string VoucherDetailID
		{
			get { return  strVoucherDetailID; }
			set { strVoucherDetailID = value; }
		}

		/// <summary>
		/// VoucherID
		/// Mã phiếu thu
		/// </summary>
		public string VoucherID
		{
			get { return  strVoucherID; }
			set { strVoucherID = value; }
		}

		/// <summary>
		/// VNDCASH
		/// Tiền mặt VND
		/// </summary>
		public decimal VNDCASH
		{
			get { return  decVNDCASH; }
			set { decVNDCASH = value; }
		}

		/// <summary>
		/// ForeIGNCASH
		/// Tiền mặt ngoại tệ
		/// </summary>
		public decimal ForeIGNCASH
		{
			get { return  decForeIGNCASH; }
			set { decForeIGNCASH = value; }
		}

		/// <summary>
		/// ForeIGNCASHExchange
		/// Tiền mặt ngoại tệ quy đổi ra VNĐ
		/// </summary>
		public decimal ForeIGNCASHExchange
		{
			get { return  decForeIGNCASHExchange; }
			set { decForeIGNCASHExchange = value; }
		}

		/// <summary>
		/// PaymentCardAmount
		/// Tiền thanh toán thẻ
		/// </summary>
		public decimal PaymentCardAmount
		{
			get { return  decPaymentCardAmount; }
			set { decPaymentCardAmount = value; }
		}

		/// <summary>
		/// PaymentCardID
		/// Loại thẻ
		/// </summary>
		public int PaymentCardID
		{
			get { return  intPaymentCardID; }
			set { intPaymentCardID = value; }
		}

		/// <summary>
		/// PaymentCardSpend
		/// Phí thẻ
		/// </summary>
		public decimal PaymentCardSpend
		{
			get { return  decPaymentCardSpend; }
			set { decPaymentCardSpend = value; }
		}

		/// <summary>
		/// PaymentCardVoucherID
		/// Mã phiếu thanh toán thẻ
		/// </summary>
		public string PaymentCardVoucherID
		{
			get { return  strPaymentCardVoucherID; }
			set { strPaymentCardVoucherID = value; }
		}

		/// <summary>
		/// GiftVoucherAmount
		/// Tiền thanh toán phiếu quà tặng
		/// </summary>
		public decimal GiftVoucherAmount
		{
			get { return  decGiftVoucherAmount; }
			set { decGiftVoucherAmount = value; }
		}

		/// <summary>
		/// UNEVENTPriceGiftVoucher
		/// Tổng tiền chênh lệch giữa mệnh giá thẻ và giá bán thẻ
		/// </summary>
		public decimal UNEVENTPriceGiftVoucher
		{
			get { return  decUNEVENTPriceGiftVoucher; }
			set { decUNEVENTPriceGiftVoucher = value; }
		}

		/// <summary>
		/// RefUNDAmount
		/// Tổng tiền thối lại
		/// </summary>
		public decimal RefUNDAmount
		{
			get { return  decRefUNDAmount; }
			set { decRefUNDAmount = value; }
		}

		/// <summary>
		/// VoucherDate
		/// Ngày phiếu thu
		/// </summary>
		public DateTime? VoucherDate
		{
			get { return  dtmVoucherDate; }
			set { dtmVoucherDate = value; }
		}

		/// <summary>
		/// CashierUser
		/// Nhân viên thu tiền
		/// </summary>
		public string CashierUser
		{
			get { return  strCashierUser; }
			set { strCashierUser = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// Nơi tạo
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// VoucherStoreID
		/// Nơi thu chi
		/// </summary>
		public int VoucherStoreID
		{
			get { return  intVoucherStoreID; }
			set { intVoucherStoreID = value; }
		}

		/// <summary>
		/// IsSupplementARYDetail
		/// Là chi tiết thu bổ sung
		/// </summary>
		public bool IsSupplementARYDetail
		{
			get { return  bolIsSupplementARYDetail; }
			set { bolIsSupplementARYDetail = value; }
		}

		/// <summary>
		/// SUPPLEMENTARYVoucherID
		/// Mã phiếu thu bổ sung
		/// </summary>
		public string SUPPLEMENTARYVoucherID
		{
			get { return  strSUPPLEMENTARYVoucherID; }
			set { strSUPPLEMENTARYVoucherID = value; }
		}

		/// <summary>
		/// ISPOSTED
		/// Đã hạch toán
		/// </summary>
		public bool ISPOSTED
		{
			get { return  bolISPOSTED; }
			set { bolISPOSTED = value; }
		}

		/// <summary>
		/// ISNOCASH
		/// Chi tiết thu này không thu chi tiền mặt
		/// </summary>
		public bool ISNOCASH
		{
			get { return  bolISNOCASH; }
			set { bolISNOCASH = value; }
		}

		/// <summary>
		/// Note
		/// Ghi chú
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreateDate
		/// 
		/// </summary>
		public DateTime? CreateDate
		{
			get { return  dtmCreateDate; }
			set { dtmCreateDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// ContentDeleted
		/// 
		/// </summary>
		public string ContentDeleted
		{
			get { return  strContentDeleted; }
			set { strContentDeleted = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}

		#endregion			
		
		
		#region Constructor

        public VoucherDetail()
		{
		}
		#endregion


		#region Column Names

		public const String colVoucherDetailID = "VoucherDetailID";
		public const String colVoucherID = "VoucherID";
		public const String colVNDCASH = "VNDCASH";
		public const String colForeIGNCASH = "ForeIGNCASH";
		public const String colForeIGNCASHExchange = "ForeIGNCASHExchange";
		public const String colPaymentCardAmount = "PaymentCardAmount";
		public const String colPaymentCardID = "PaymentCardID";
		public const String colPaymentCardSpend = "PaymentCardSpend";
		public const String colPaymentCardVoucherID = "PaymentCardVoucherID";
		public const String colGiftVoucherAmount = "GiftVoucherAmount";
		public const String colUNEVENTPriceGiftVoucher = "UNEVENTPriceGiftVoucher";
		public const String colRefUNDAmount = "RefUNDAmount";
		public const String colVoucherDate = "VoucherDate";
		public const String colCashierUser = "CashierUser";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colVoucherStoreID = "VoucherStoreID";
		public const String colIsSupplementARYDetail = "IsSupplementARYDetail";
		public const String colSUPPLEMENTARYVoucherID = "SUPPLEMENTARYVoucherID";
		public const String colISPOSTED = "ISPOSTED";
		public const String colISNOCASH = "ISNOCASH";
		public const String colNote = "Note";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreateDate = "CreateDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colContentDeleted = "ContentDeleted";

		#endregion //Column names

		
	}
}
