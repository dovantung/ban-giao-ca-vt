
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 12/01/14 
	/// 
	/// </summary>	
	public class IMEISalesInfo_Comment
	{	
	
	
		#region Member Variables

		private int intCommentID;
		private decimal decReplyToCommentID;
		private string strProductID = string.Empty;
		private string strIMEI = string.Empty;
		private DateTime? dtmCommentDate;
		private string strCommentTitle = string.Empty;
		private string strCommentContent = string.Empty;
		private string strCommentUser = string.Empty;
		private string strCommentEmail = string.Empty;
		private int intModelRate = 0;
		private bool bolISCommentUserLogin = false;
		private string strUserHostAddress = string.Empty;
		private string strUserComputerName = string.Empty;
		private string strUserAgent = string.Empty;
		private bool bolIsStaffComment = false;
		private string strCommentFullName = string.Empty;
		private decimal decOrderIndex;
		private bool bolIsActive = false;
		private bool bolIsSystem = false;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strPhoneNumber = string.Empty;
        private bool bolIsExist = false;
        private bool bolIsAddNew = false;

		#endregion


		#region Properties 

		/// <summary>
		/// CommentID
		/// 
		/// </summary>
		public int CommentID
		{
			get { return  intCommentID; }
			set { intCommentID = value; }
		}

		/// <summary>
		/// ReplyToCommentID
		/// Trả lời cho bình luận nào
		/// </summary>
		public decimal ReplyToCommentID
		{
			get { return  decReplyToCommentID; }
			set { decReplyToCommentID = value; }
		}

		/// <summary>
		/// ProductID
		/// Sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// IMEI
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// CommentDate
		/// Ngày bình luận
		/// </summary>
		public DateTime? CommentDate
		{
			get { return  dtmCommentDate; }
			set { dtmCommentDate = value; }
		}

		/// <summary>
		/// CommentTitle
		/// Tiêu đề
		/// </summary>
		public string CommentTitle
		{
			get { return  strCommentTitle; }
			set { strCommentTitle = value; }
		}

		/// <summary>
		/// CommentContent
		/// Nội dung bình luận
		/// </summary>
		public string CommentContent
		{
			get { return  strCommentContent; }
			set { strCommentContent = value; }
		}

		/// <summary>
		/// CommentUser
		/// Tên người bình luận
		/// </summary>
		public string CommentUser
		{
			get { return  strCommentUser; }
			set { strCommentUser = value; }
		}

		/// <summary>
		/// CommentEmail
		/// Email người bình luận
		/// </summary>
		public string CommentEmail
		{
			get { return  strCommentEmail; }
			set { strCommentEmail = value; }
		}

		/// <summary>
		/// ModelRate
		/// Mức đánh giá
		/// </summary>
		public int ModelRate
		{
			get { return  intModelRate; }
			set { intModelRate = value; }
		}

		/// <summary>
		/// ISCommentUserLogin
		/// Có đăng nhập để Bình luận
		/// </summary>
		public bool ISCommentUserLogin
		{
			get { return  bolISCommentUserLogin; }
			set { bolISCommentUserLogin = value; }
		}

		/// <summary>
		/// UserHostAddress
		/// 
		/// </summary>
		public string UserHostAddress
		{
			get { return  strUserHostAddress; }
			set { strUserHostAddress = value; }
		}

		/// <summary>
		/// UserComputerName
		/// 
		/// </summary>
		public string UserComputerName
		{
			get { return  strUserComputerName; }
			set { strUserComputerName = value; }
		}

		/// <summary>
		/// UserAgent
		/// Của trình duyệt
		/// </summary>
		public string UserAgent
		{
			get { return  strUserAgent; }
			set { strUserAgent = value; }
		}

		/// <summary>
		/// IsStaffComment
		/// Là nhân viên trả lời
		/// </summary>
		public bool IsStaffComment
		{
			get { return  bolIsStaffComment; }
			set { bolIsStaffComment = value; }
		}

		/// <summary>
		/// CommentFullName
		/// Tên đầy đủ của người Comment
		/// </summary>
		public string CommentFullName
		{
			get { return  strCommentFullName; }
			set { strCommentFullName = value; }
		}

		/// <summary>
		/// OrderIndex
		/// 
		/// </summary>
		public decimal OrderIndex
		{
			get { return  decOrderIndex; }
			set { decOrderIndex = value; }
		}

		/// <summary>
		/// IsActive
		/// 
		/// </summary>
		public bool IsActive
		{
			get { return  bolIsActive; }
			set { bolIsActive = value; }
		}

		/// <summary>
		/// IsSystem
		/// 
		/// </summary>
		public bool IsSystem
		{
			get { return  bolIsSystem; }
			set { bolIsSystem = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// PhoneNumber
		/// số điện thoại
		/// </summary>
		public string PhoneNumber
		{
			get { return  strPhoneNumber; }
			set { strPhoneNumber = value; }
		}

        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        public bool IsAddNew
        {
            get { return bolIsAddNew; }
            set { bolIsAddNew = value; }
        }
        
		#endregion			
		
		
		#region Constructor

		public IMEISalesInfo_Comment()
		{
		}
		#endregion


		#region Column Names

		public const String colCommentID = "CommentID";
		public const String colReplyToCommentID = "ReplyToCommentID";
		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
		public const String colCommentDate = "CommentDate";
		public const String colCommentTitle = "CommentTitle";
		public const String colCommentContent = "CommentContent";
		public const String colCommentUser = "CommentUser";
		public const String colCommentEmail = "CommentEmail";
		public const String colModelRate = "ModelRate";
		public const String colISCommentUserLogin = "ISCommentUserLogin";
		public const String colUserHostAddress = "UserHostAddress";
		public const String colUserComputerName = "UserComputerName";
		public const String colUserAgent = "UserAgent";
		public const String colIsStaffComment = "IsStaffComment";
		public const String colCommentFullName = "CommentFullName";
		public const String colOrderIndex = "OrderIndex";
		public const String colIsActive = "IsActive";
		public const String colIsSystem = "IsSystem";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colPhoneNumber = "PhoneNumber";

		#endregion //Column names

		
	}
}
