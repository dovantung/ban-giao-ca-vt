
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.BGT
{
    /// <summary>
	/// Created by 		: Trương Trung Lợi 
	/// Created date 	: 11/28/2012 
	/// 
	/// </summary>	
	public class BeginTermInStock
	{	
	
	
		#region Member Variables

		private DateTime? dtmBeginTermDate;
		private string strBeginTermTable = string.Empty;
		private string strBeginTermTableDetail = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;

		#endregion


		#region Properties 

		/// <summary>
		/// BeginTermDate
		/// 
		/// </summary>
		public DateTime? BeginTermDate
		{
			get { return  dtmBeginTermDate; }
			set { dtmBeginTermDate = value; }
		}

		/// <summary>
		/// BeginTermTable
		/// 
		/// </summary>
		public string BeginTermTable
		{
			get { return  strBeginTermTable; }
			set { strBeginTermTable = value; }
		}

		/// <summary>
		/// BeginTermTableDetail
		/// 
		/// </summary>
		public string BeginTermTableDetail
		{
			get { return  strBeginTermTableDetail; }
			set { strBeginTermTableDetail = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public BeginTermInStock()
		{
		}
		#endregion


		#region Column Names

		public const String colBeginTermDate = "BeginTermDate";
		public const String colBeginTermTable = "BeginTermTable";
		public const String colBeginTermTableDetail = "BeginTermTableDetail";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";

		#endregion //Column names

		
	}
}
