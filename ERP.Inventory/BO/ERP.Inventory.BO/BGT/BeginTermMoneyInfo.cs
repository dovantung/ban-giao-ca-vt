
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.BGT
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 6/25/2013 
	/// 
	/// </summary>	
	public class BeginTermMoneyInfo
	{	
	
	
		#region Member Variables

		private DateTime? dtmBeginTermMoneyDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private string strUserHostAddress = string.Empty;
		private string strCertificateString = string.Empty;
		private string strLoginLogID = string.Empty;

		#endregion


		#region Properties 

		/// <summary>
		/// BeginTermMoneyDate
		/// 
		/// </summary>
		public DateTime? BeginTermMoneyDate
		{
			get { return  dtmBeginTermMoneyDate; }
			set { dtmBeginTermMoneyDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// UserHostAddress
		/// 
		/// </summary>
		public string UserHostAddress
		{
			get { return  strUserHostAddress; }
			set { strUserHostAddress = value; }
		}

		/// <summary>
		/// CertificateString
		/// 
		/// </summary>
		public string CertificateString
		{
			get { return  strCertificateString; }
			set { strCertificateString = value; }
		}

		/// <summary>
		/// LoginLogID
		/// 
		/// </summary>
		public string LoginLogID
		{
			get { return  strLoginLogID; }
			set { strLoginLogID = value; }
		}


		#endregion			
		
		
		#region Constructor

		public BeginTermMoneyInfo()
		{
		}
		#endregion


		#region Column Names

		public const String colBeginTermMoneyDate = "BeginTermMoneyDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colUserHostAddress = "UserHostAddress";
		public const String colCertificateString = "CertificateString";
		public const String colLoginLogID = "LoginLogID";

		#endregion //Column names

		
	}
}
