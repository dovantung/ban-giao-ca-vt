
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.BGT
{
    /// <summary>
	/// Created by 		: Cao Huu Vu Lam 
	/// Created date 	: 10-Mar-15 
	/// 
	/// </summary>	
	public class CumulationTimeOff
	{	
	
	
		#region Member Variables

		private int intNewYear = 0;
		private string strUserName = string.Empty;
		private decimal decCumulationTimeOffDays;
		private decimal decUsedTimeOffDays;
		private decimal decRemainTimeOffDays;
		private bool bolIsActive = false;

		#endregion


		#region Properties 

		/// <summary>
		/// NewYear
		/// 
		/// </summary>
		public int NewYear
		{
			get { return  intNewYear; }
			set { intNewYear = value; }
		}

		/// <summary>
		/// UserName
		/// 
		/// </summary>
		public string UserName
		{
			get { return  strUserName; }
			set { strUserName = value; }
		}

		/// <summary>
		/// CumulationTimeOffDays
		/// 
		/// </summary>
		public decimal CumulationTimeOffDays
		{
			get { return  decCumulationTimeOffDays; }
			set { decCumulationTimeOffDays = value; }
		}

		/// <summary>
		/// UsedTimeOffDays
		/// 
		/// </summary>
		public decimal UsedTimeOffDays
		{
			get { return  decUsedTimeOffDays; }
			set { decUsedTimeOffDays = value; }
		}

		/// <summary>
		/// RemainTimeOffDays
		/// 
		/// </summary>
		public decimal RemainTimeOffDays
		{
			get { return  decRemainTimeOffDays; }
			set { decRemainTimeOffDays = value; }
		}

		/// <summary>
		/// IsActive
		/// 
		/// </summary>
		public bool IsActive
		{
			get { return  bolIsActive; }
			set { bolIsActive = value; }
		}


		#endregion			
		
		
		#region Constructor

		public CumulationTimeOff()
		{
		}
		#endregion


		#region Column Names

		public const String colNewYear = "NewYear";
		public const String colUserName = "UserName";
		public const String colCumulationTimeOffDays = "CumulationTimeOffDays";
		public const String colUsedTimeOffDays = "UsedTimeOffDays";
		public const String colRemainTimeOffDays = "RemainTimeOffDays";
		public const String colIsActive = "IsActive";

		#endregion //Column names

		
	}
}
