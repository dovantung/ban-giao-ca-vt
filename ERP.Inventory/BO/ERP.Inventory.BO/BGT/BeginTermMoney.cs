
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.BGT
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 6/25/2013 
	/// 
	/// </summary>	
	public class BeginTermMoney
	{	
	
	
		#region Member Variables

		private DateTime? dtmBeginTermMoneyDate;
		private int intStoreID = 0;
		private decimal decVNDCash;
		private decimal decForeignCash;
		private decimal decForeignCashExchange;
		private decimal decPaymentCardAmount;
		private decimal decPaymentCardSpend;
		private decimal decGiftVoucherAmount;
		private decimal decUnEventPriceGiftVoucher;
		private decimal decRefundAmount;

		#endregion


		#region Properties 

		/// <summary>
		/// BeginTermMoneyDate
		/// 
		/// </summary>
		public DateTime? BeginTermMoneyDate
		{
			get { return  dtmBeginTermMoneyDate; }
			set { dtmBeginTermMoneyDate = value; }
		}

		/// <summary>
		/// StoreID
		/// 
		/// </summary>
		public int StoreID
		{
			get { return  intStoreID; }
			set { intStoreID = value; }
		}

		/// <summary>
		/// VNDCash
		/// 
		/// </summary>
		public decimal VNDCash
		{
			get { return  decVNDCash; }
			set { decVNDCash = value; }
		}

		/// <summary>
		/// ForeignCash
		/// 
		/// </summary>
		public decimal ForeignCash
		{
			get { return  decForeignCash; }
			set { decForeignCash = value; }
		}

		/// <summary>
		/// ForeignCashExchange
		/// 
		/// </summary>
		public decimal ForeignCashExchange
		{
			get { return  decForeignCashExchange; }
			set { decForeignCashExchange = value; }
		}

		/// <summary>
		/// PaymentCardAmount
		/// 
		/// </summary>
		public decimal PaymentCardAmount
		{
			get { return  decPaymentCardAmount; }
			set { decPaymentCardAmount = value; }
		}

		/// <summary>
		/// PaymentCardSpend
		/// 
		/// </summary>
		public decimal PaymentCardSpend
		{
			get { return  decPaymentCardSpend; }
			set { decPaymentCardSpend = value; }
		}

		/// <summary>
		/// GiftVoucherAmount
		/// 
		/// </summary>
		public decimal GiftVoucherAmount
		{
			get { return  decGiftVoucherAmount; }
			set { decGiftVoucherAmount = value; }
		}

		/// <summary>
		/// UnEventPriceGiftVoucher
		/// 
		/// </summary>
		public decimal UnEventPriceGiftVoucher
		{
			get { return  decUnEventPriceGiftVoucher; }
			set { decUnEventPriceGiftVoucher = value; }
		}

		/// <summary>
		/// RefundAmount
		/// 
		/// </summary>
		public decimal RefundAmount
		{
			get { return  decRefundAmount; }
			set { decRefundAmount = value; }
		}


		#endregion			
		
		
		#region Constructor

		public BeginTermMoney()
		{
		}
		#endregion


		#region Column Names

		public const String colBeginTermMoneyDate = "BeginTermMoneyDate";
		public const String colStoreID = "StoreID";
		public const String colVNDCash = "VNDCash";
		public const String colForeignCash = "ForeignCash";
		public const String colForeignCashExchange = "ForeignCashExchange";
		public const String colPaymentCardAmount = "PaymentCardAmount";
		public const String colPaymentCardSpend = "PaymentCardSpend";
		public const String colGiftVoucherAmount = "GiftVoucherAmount";
		public const String colUnEventPriceGiftVoucher = "UnEventPriceGiftVoucher";
		public const String colRefundAmount = "RefundAmount";

		#endregion //Column names

		
	}
}
