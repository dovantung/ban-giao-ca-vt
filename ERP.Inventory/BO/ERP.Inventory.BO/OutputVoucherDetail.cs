
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Trương Trung Lợi 
	/// Created date 	: 13/09/2012 
	/// 
	/// </summary>	
	public partial class OutputVoucherDetail
	{	
	
	
		#region Member Variables

		private string strOutputVoucherDetailID = string.Empty;
		private string strOutputVoucherID = string.Empty;
		private string strProductID = string.Empty;
		private int intProductSalesKitID = 0;
		private string strRefProductID = string.Empty;
		private string strProductComboID = string.Empty;
		private string strIMEI = string.Empty;
		private string strExtendWarrantyIMEI = string.Empty;
		private string strPINCode = string.Empty;
		private decimal decQuantity;
		private decimal decReturnQuantity;
		private decimal decSalePrice;
        private decimal decSalePriceAfVat;
        private decimal decBFAdjustSalePrice;
		private decimal decOriginalSalePrice;
		private decimal decCostPrice;
		private decimal decRetailPrice;
		private int intVAT = 0;
		private int intVATPercent = 0;
		private int intOutputTypeID = 0;
		private string strInputVoucherDetailID = string.Empty;
		private DateTime? dtmEndWarrantyDate;
		private bool bolIsNew = false;
		private bool bolIsShowProduct = false;
		private bool bolIsHasWarranty = false;
		private DateTime? dtmFirstInputDate;
		private DateTime? dtmFirstInvoiceDate;
		private int intFirstCustomerID = 0;
		private int intFirstInputTypeID = 0;
		private string strFirstInputVoucherID = string.Empty;
		private string strFirstInputVoucherDetailID = string.Empty;
		private int intPromotionID = 0;
		private int intPromotionOfferID = 0;
		private string strApplyProductID = string.Empty;
		private string strApplySaleOrderDetailID = string.Empty;
		private string strApplySaleOrderID = string.Empty;
		private string strSaleOrderDetailID = string.Empty;
		private int intPackingNumber = 0;
		private bool bolIsReturnProduct = false;
		private bool bolIsReturnWithFee = false;
		private bool bolIsProductChange = false;
		private bool bolIsSubtractInStock = false;
		private bool bolIsRequireVoucher = false;
		private bool bolIsConsignmentInput = false;
		private bool bolIsInputChange = false;
		private decimal decInStockStoreID;
		private int intCreatedStoreID = 0;
		private int intOutputStoreID = 0;
		private DateTime? dtmOutputDate;
		private DateTime? dtmReturnDate;
		private DateTime? dtmConsignmentInputDate;
		private DateTime? dtmInputChangeDate;
		private string strRelateVoucherID = string.Empty;
		private string strIMEIChangeChain = string.Empty;
		private string strInvoiceNote = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private decimal decAdjustPrice;
		private bool bolIsPromotionProduct = false;
        private decimal decTaxAmount = 0;
        #endregion


        #region Properties 

        /// <summary>
        /// OutputVoucherDetailID
        /// Mã chi tiết phiếu xuất
        /// </summary>
        public string OutputVoucherDetailID
		{
			get { return  strOutputVoucherDetailID; }
			set { strOutputVoucherDetailID = value; }
		}

		/// <summary>
		/// OutputVoucherID
		/// Mã phiếu xuất
		/// </summary>
		public string OutputVoucherID
		{
			get { return  strOutputVoucherID; }
			set { strOutputVoucherID = value; }
		}

		/// <summary>
		/// ProductID
		/// Mã sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// ProductSalesKitID
		/// Bộ KIT
		/// </summary>
		public int ProductSalesKitID
		{
			get { return  intProductSalesKitID; }
			set { intProductSalesKitID = value; }
		}

		/// <summary>
		/// RefProductID
		/// Sản phẩm tham khảo
		/// </summary>
		public string RefProductID
		{
			get { return  strRefProductID; }
			set { strRefProductID = value; }
		}

		/// <summary>
		/// ProductComboID
		/// Mã sản phẩm combo
		/// </summary>
		public string ProductComboID
		{
			get { return  strProductComboID; }
			set { strProductComboID = value; }
		}

		/// <summary>
		/// IMEI
		/// 
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// ExtendWarrantyIMEI
		/// Số IMEI bảo hành mở rộng
		/// </summary>
		public string ExtendWarrantyIMEI
		{
			get { return  strExtendWarrantyIMEI; }
			set { strExtendWarrantyIMEI = value; }
		}

		/// <summary>
		/// PINCode
		/// Mã PIN
		/// </summary>
		public string PINCode
		{
			get { return  strPINCode; }
			set { strPINCode = value; }
		}

		/// <summary>
		/// Quantity
		/// Số lượng
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// ReturnQuantity
		/// Số lượng nhập trả
		/// </summary>
		public decimal ReturnQuantity
		{
			get { return  decReturnQuantity; }
			set { decReturnQuantity = value; }
		}

		/// <summary>
		/// SalePrice
		/// Giá bán
		/// </summary>
		public decimal SalePrice
		{
			get { return  decSalePrice; }
			set { decSalePrice = value; }
		}

        /// <summary>
        /// SalePriceAfVat
        /// Giá bán sau thue
        /// </summary>
        public decimal SalePriceAfVat
        {
            get { return decSalePriceAfVat; }
            set { decSalePriceAfVat = value; }
        }
        /// <summary>
        /// BFAdjustSalePrice
        /// Giá bán trước khi điều chỉnh giá
        /// </summary>
        public decimal BFAdjustSalePrice
		{
			get { return  decBFAdjustSalePrice; }
			set { decBFAdjustSalePrice = value; }
		}

		/// <summary>
		/// OriginalSalePrice
		/// Giá bán gốc(chưa qua KM, chỉnh sửa)
		/// </summary>
		public decimal OriginalSalePrice
		{
			get { return  decOriginalSalePrice; }
			set { decOriginalSalePrice = value; }
		}

		/// <summary>
		/// CostPrice
		/// Giá vốn : Giá nhập
		/// </summary>
		public decimal CostPrice
		{
			get { return  decCostPrice; }
			set { decCostPrice = value; }
		}

		/// <summary>
		/// RetailPrice
		/// Giá bán lẻ
		/// </summary>
		public decimal RetailPrice
		{
			get { return  decRetailPrice; }
			set { decRetailPrice = value; }
		}

		/// <summary>
		/// VAT
		/// Thuế suất
		/// </summary>
		public int VAT
		{
			get { return  intVAT; }
			set { intVAT = value; }
		}

		/// <summary>
		/// VATPercent
		/// Phần trăm VAT phải nộp
		/// </summary>
		public int VATPercent
		{
			get { return  intVATPercent; }
			set { intVATPercent = value; }
		}

		/// <summary>
		/// OutputTypeID
		/// Mã hình thức xuất
		/// </summary>
		public int OutputTypeID
		{
			get { return  intOutputTypeID; }
			set { intOutputTypeID = value; }
		}

		/// <summary>
		/// InputVoucherDetailID
		/// Mã phiếu nhập gần nhất
		/// </summary>
		public string InputVoucherDetailID
		{
			get { return  strInputVoucherDetailID; }
			set { strInputVoucherDetailID = value; }
		}

		/// <summary>
		/// EndWarrantyDate
		/// Ngày hết bảo hành
		/// </summary>
		public DateTime? EndWarrantyDate
		{
			get { return  dtmEndWarrantyDate; }
			set { dtmEndWarrantyDate = value; }
		}

		/// <summary>
		/// IsNew
		/// Sản phẩm mới?
		/// </summary>
		public bool IsNew
		{
			get { return  bolIsNew; }
			set { bolIsNew = value; }
		}

		/// <summary>
		/// IsShowProduct
		/// Là hàng trưng bày hay không?
		/// </summary>
		public bool IsShowProduct
		{
			get { return  bolIsShowProduct; }
			set { bolIsShowProduct = value; }
		}

		/// <summary>
		/// IsHasWarranty
		/// 
		/// </summary>
		public bool IsHasWarranty
		{
			get { return  bolIsHasWarranty; }
			set { bolIsHasWarranty = value; }
		}

		/// <summary>
		/// FirstInputDate
		/// 
		/// </summary>
		public DateTime? FirstInputDate
		{
			get { return  dtmFirstInputDate; }
			set { dtmFirstInputDate = value; }
		}

		/// <summary>
		/// FirstInvoiceDate
		/// 
		/// </summary>
		public DateTime? FirstInvoiceDate
		{
			get { return  dtmFirstInvoiceDate; }
			set { dtmFirstInvoiceDate = value; }
		}

		/// <summary>
		/// FirstCustomerID
		/// 
		/// </summary>
		public int FirstCustomerID
		{
			get { return  intFirstCustomerID; }
			set { intFirstCustomerID = value; }
		}

		/// <summary>
		/// FirstInputTypeID
		/// 
		/// </summary>
		public int FirstInputTypeID
		{
			get { return  intFirstInputTypeID; }
			set { intFirstInputTypeID = value; }
		}

		/// <summary>
		/// FirstInputVoucherID
		/// 
		/// </summary>
		public string FirstInputVoucherID
		{
			get { return  strFirstInputVoucherID; }
			set { strFirstInputVoucherID = value; }
		}

		/// <summary>
		/// FirstInputVoucherDetailID
		/// 
		/// </summary>
		public string FirstInputVoucherDetailID
		{
			get { return  strFirstInputVoucherDetailID; }
			set { strFirstInputVoucherDetailID = value; }
		}

		/// <summary>
		/// PromotionID
		/// 
		/// </summary>
		public int PromotionID
		{
			get { return  intPromotionID; }
			set { intPromotionID = value; }
		}

		/// <summary>
		/// PromotionOfferID
		/// Mã khuyến mãi
		/// </summary>
		public int PromotionOfferID
		{
			get { return  intPromotionOfferID; }
			set { intPromotionOfferID = value; }
		}

		/// <summary>
		/// ApplyProductID
		/// 
		/// </summary>
		public string ApplyProductID
		{
			get { return  strApplyProductID; }
			set { strApplyProductID = value; }
		}

		/// <summary>
		/// ApplySaleOrderDetailID
		/// Chi tiết đơn hàng (chính) áp dụng cho sản phẩm KM này (TH xuất bổ sung KM)
		/// </summary>
		public string ApplySaleOrderDetailID
		{
			get { return  strApplySaleOrderDetailID; }
			set { strApplySaleOrderDetailID = value; }
		}

		/// <summary>
		/// ApplySaleOrderID
		/// Mã đơn hàng áp dụng KM cho dòng này
		/// </summary>
		public string ApplySaleOrderID
		{
			get { return  strApplySaleOrderID; }
			set { strApplySaleOrderID = value; }
		}

		/// <summary>
		/// SaleOrderDetailID
		/// Chi tiết yêu cầu xuất (Y/C xuất trực tiếp)
		/// </summary>
		public string SaleOrderDetailID
		{
			get { return  strSaleOrderDetailID; }
			set { strSaleOrderDetailID = value; }
		}

		/// <summary>
		/// PackingNumber
		/// Số thùng(dùng trong chuyển kho)
		/// </summary>
		public int PackingNumber
		{
			get { return  intPackingNumber; }
			set { intPackingNumber = value; }
		}

		/// <summary>
		/// IsReturnProduct
		/// Là sản phẩm đã nhập trả hàng (xuất bán sản phẩm từng nhập trả hàng)
		/// </summary>
		public bool IsReturnProduct
		{
			get { return  bolIsReturnProduct; }
			set { bolIsReturnProduct = value; }
		}

		/// <summary>
		/// IsReturnWithFee
		/// Sản phẩm này có phải nhập trả hàng có thu phí hay không?
		/// </summary>
		public bool IsReturnWithFee
		{
			get { return  bolIsReturnWithFee; }
			set { bolIsReturnWithFee = value; }
		}

		/// <summary>
		/// IsProductChange
		/// Là sản phẩm đổi hàng
		/// </summary>
		public bool IsProductChange
		{
			get { return  bolIsProductChange; }
			set { bolIsProductChange = value; }
		}

		/// <summary>
		/// IsSubtractInStock
		/// Có trừ tồn kho không?
		/// </summary>
		public bool IsSubtractInStock
		{
			get { return  bolIsSubtractInStock; }
			set { bolIsSubtractInStock = value; }
		}

		/// <summary>
		/// IsRequireVoucher
		/// Yêu cầu nhập chứng từ
		/// </summary>
		public bool IsRequireVoucher
		{
			get { return  bolIsRequireVoucher; }
			set { bolIsRequireVoucher = value; }
		}

		/// <summary>
		/// IsConsignmentInput
		/// Đã tạo phiếu nhập mua hàng ký gửi tạm
		/// </summary>
		public bool IsConsignmentInput
		{
			get { return  bolIsConsignmentInput; }
			set { bolIsConsignmentInput = value; }
		}

		/// <summary>
		/// IsInputChange
		/// Là sản phẩm nhập đổi hàng bán tại siêu thị
		/// </summary>
		public bool IsInputChange
		{
			get { return  bolIsInputChange; }
			set { bolIsInputChange = value; }
		}

		/// <summary>
		/// InStockStoreID
		/// Xuất ở kho tồn nào
		/// </summary>
		public decimal InStockStoreID
		{
			get { return  decInStockStoreID; }
			set { decInStockStoreID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// OutputStoreID
		/// 
		/// </summary>
		public int OutputStoreID
		{
			get { return  intOutputStoreID; }
			set { intOutputStoreID = value; }
		}

		/// <summary>
		/// OutputDate
		/// 
		/// </summary>
		public DateTime? OutputDate
		{
			get { return  dtmOutputDate; }
			set { dtmOutputDate = value; }
		}

		/// <summary>
		/// ReturnDate
		/// Ngày nhập trả(InputDate phiếu nhập trả -nếu có)
		/// </summary>
		public DateTime? ReturnDate
		{
			get { return  dtmReturnDate; }
			set { dtmReturnDate = value; }
		}

		/// <summary>
		/// ConsignmentInputDate
		/// Ngày tạo phiếu nhập mua hàng ký gửi tạm
		/// </summary>
		public DateTime? ConsignmentInputDate
		{
			get { return  dtmConsignmentInputDate; }
			set { dtmConsignmentInputDate = value; }
		}

		/// <summary>
		/// InputChangeDate
		/// Ngày nhập đổi hàng bán
		/// </summary>
		public DateTime? InputChangeDate
		{
			get { return  dtmInputChangeDate; }
			set { dtmInputChangeDate = value; }
		}

		/// <summary>
		/// RelateVoucherID
		/// Mã chứng từ liên quan
		/// </summary>
		public string RelateVoucherID
		{
			get { return  strRelateVoucherID; }
			set { strRelateVoucherID = value; }
		}

		/// <summary>
		/// IMEIChangeChain
		/// Chuổi lịch sử thay đổi IMEI
		/// </summary>
		public string IMEIChangeChain
		{
			get { return  strIMEIChangeChain; }
			set { strIMEIChangeChain = value; }
		}

		/// <summary>
		/// InvoiceNote
		/// Ghi chú trên HĐ
		/// </summary>
		public string InvoiceNote
		{
			get { return  strInvoiceNote; }
			set { strInvoiceNote = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// AdjustPrice
		/// Giá điều chỉnh
		/// </summary>
		public decimal AdjustPrice
		{
			get { return  decAdjustPrice; }
			set { decAdjustPrice = value; }
		}

		/// <summary>
		/// IsPromotionProduct
		/// Là sản phẩm khuyến mãi
		/// </summary>
		public bool IsPromotionProduct
		{
			get { return  bolIsPromotionProduct; }
			set { bolIsPromotionProduct = value; }
		}

        /// <summary>
        /// TaxAmount
        /// Tiền thuế
        /// </summary>
        public decimal TaxAmount
        {
            get { return decTaxAmount; }
            set { decTaxAmount = value; }
        }

        public string IDLink1 { get; set; }
        public int IDLink2 { get; set; }
        public int IDLink3 { get; set; }
        
		#endregion			
		
		
		#region Constructor

		public OutputVoucherDetail()
		{
		}
		#endregion


		#region Column Names

		public const String colOutputVoucherDetailID = "OutputVoucherDetailID";
		public const String colOutputVoucherID = "OutputVoucherID";
		public const String colProductID = "ProductID";
		public const String colProductSalesKitID = "ProductSalesKitID";
		public const String colRefProductID = "RefProductID";
		public const String colProductComboID = "ProductComboID";
		public const String colIMEI = "IMEI";
		public const String colExtendWarrantyIMEI = "ExtendWarrantyIMEI";
		public const String colPINCode = "PINCode";
		public const String colQuantity = "Quantity";
		public const String colReturnQuantity = "ReturnQuantity";
		public const String colSalePrice = "SalePrice";
        public const String colSalePriceAfVat = "SalePriceAfVat";///huent66 cập nhật: SalePriceAfVat 
        public const String colBFAdjustSalePrice = "BFAdjustSalePrice";
		public const String colOriginalSalePrice = "OriginalSalePrice";
		public const String colCostPrice = "CostPrice";
		public const String colRetailPrice = "RetailPrice";
		public const String colVAT = "VAT";
		public const String colVATPercent = "VATPercent";
		public const String colOutputTypeID = "OutputTypeID";
		public const String colInputVoucherDetailID = "InputVoucherDetailID";
		public const String colEndWarrantyDate = "EndWarrantyDate";
		public const String colIsNew = "IsNew";
		public const String colIsShowProduct = "IsShowProduct";
		public const String colIsHasWarranty = "IsHasWarranty";
		public const String colFirstInputDate = "FirstInputDate";
		public const String colFirstInvoiceDate = "FirstInvoiceDate";
		public const String colFirstCustomerID = "FirstCustomerID";
		public const String colFirstInputTypeID = "FirstInputTypeID";
		public const String colFirstInputVoucherID = "FirstInputVoucherID";
		public const String colFirstInputVoucherDetailID = "FirstInputVoucherDetailID";
		public const String colPromotionID = "PromotionID";
		public const String colPromotionOfferID = "PromotionOfferID";
		public const String colApplyProductID = "ApplyProductID";
		public const String colApplySaleOrderDetailID = "ApplySaleOrderDetailID";
		public const String colApplySaleOrderID = "ApplySaleOrderID";
		public const String colSaleOrderDetailID = "SaleOrderDetailID";
		public const String colPackingNumber = "PackingNumber";
		public const String colIsReturnProduct = "IsReturnProduct";
		public const String colIsReturnWithFee = "IsReturnWithFee";
		public const String colIsProductChange = "IsProductChange";
		public const String colIsSubtractInStock = "IsSubtractInStock";
		public const String colIsRequireVoucher = "IsRequireVoucher";
		public const String colIsConsignmentInput = "IsConsignmentInput";
		public const String colIsInputChange = "IsInputChange";
		public const String colInStockStoreID = "InStockStoreID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colOutputStoreID = "OutputStoreID";
		public const String colOutputDate = "OutputDate";
		public const String colReturnDate = "ReturnDate";
		public const String colConsignmentInputDate = "ConsignmentInputDate";
		public const String colInputChangeDate = "InputChangeDate";
		public const String colRelateVoucherID = "RelateVoucherID";
		public const String colIMEIChangeChain = "IMEIChangeChain";
		public const String colInvoiceNote = "InvoiceNote";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colAdjustPrice = "AdjustPrice";
		public const String colIsPromotionProduct = "IsPromotionProduct";
        public const String colTaxAmount = "TaxAmount";
        #endregion //Column names


    }
}
