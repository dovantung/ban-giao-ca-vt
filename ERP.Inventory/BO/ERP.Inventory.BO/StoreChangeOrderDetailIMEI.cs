
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 12/5/2012 
	/// Chi tiết yêu cầu chuyển kho theo IMEI
	/// </summary>	
	public class StoreChangeOrderDetailIMEI
	{	
	
	
		#region Member Variables

		private string strStoreChangeOrderDetailID = string.Empty;
		private string strIMEI = string.Empty;
		private DateTime? dtmOrderDate;
		private bool bolIsStoreChange = false;
		private string strNote = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;

		#endregion


		#region Properties 

		/// <summary>
		/// StoreChangeOrderDetailID
		/// 
		/// </summary>
		public string StoreChangeOrderDetailID
		{
			get { return  strStoreChangeOrderDetailID; }
			set { strStoreChangeOrderDetailID = value; }
		}

		/// <summary>
		/// IMEI
		/// 
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// OrderDate
		/// 
		/// </summary>
		public DateTime? OrderDate
		{
			get { return  dtmOrderDate; }
			set { dtmOrderDate = value; }
		}

		/// <summary>
		/// IsStoreChange
		/// Đã chuyển kho hay chưa
		/// </summary>
		public bool IsStoreChange
		{
			get { return  bolIsStoreChange; }
			set { bolIsStoreChange = value; }
		}

		/// <summary>
		/// Note
		/// Ghi chú
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public StoreChangeOrderDetailIMEI()
		{
		}
		#endregion


		#region Column Names

		public const String colStoreChangeOrderDetailID = "StoreChangeOrderDetailID";
		public const String colIMEI = "IMEI";
		public const String colOrderDate = "OrderDate";
		public const String colIsStoreChange = "IsStoreChange";
		public const String colNote = "Note";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
