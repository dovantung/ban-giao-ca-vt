﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO
{
    public class ProductInStock
    {
        private string strProductID = string.Empty;
        private String strProductName = string.Empty;
        private int intVAT = 0;
        private int intVATPercent = 0;
        private bool bolIsRequestIMEI = false;
        private bool bolIsInstock = false;
        private string strIMEI = string.Empty;
        private decimal decQuantity = 0;
        private String strSMSProductCode = string.Empty;
        private bool bolIsService = false;
        private int intFirstCustomerID = 0;
        private decimal decCostPrice = 0;
        private decimal decFirstPrice = 0;
        private int intFirstInputTypeID = 0;
        private int intReturnOutputTypeID = 0;
        private bool bolIsNew = true;
        private bool bolIsShowProduct = false;
        private DateTime dtmEndWarrantyDate = DateTime.Now;
        private String strInputVoucherDetailID = string.Empty;
        private String strFirstInputVoucherID = string.Empty;
        private DateTime dtmFirstInvoiceDate = DateTime.Now;
        private bool bolIsOrder = false;
        private bool bolIsOrdering = false;
        private bool bolIsDelivery = false;
        private String strOrderID = string.Empty;
        private bool bolIsHasWarranty = false;
        private String strNote = string.Empty;
        private bool bolIsReturnProduct = false;
        private String strInputStaffUser = string.Empty;
        private bool bolIsCheckRealInput = false;
        private bool bolIsReturnWithFee = false;
        private bool bolIsAutoGetIMEI = false;
        private bool bolIsCheckStockQuantity = true;
        private bool bolIsCanReturnOutput = true;
        private bool bolIsAllowDecimal = false;
        private decimal decPrice = 0;
        private decimal decPriceAfVat = 0; // ////huent66 cập nhật: SalePriceAfVat 
        private int intBrandID = 0;
        private int intMainGroupID = 0;
        private int intSubGroupID = 0;
        private String strSubGroupName = string.Empty;
        private String strMainGroupName = string.Empty;
        private decimal decRetailPrice = 0;
        private bool bolIsPriceOfProduct = true;
        private decimal decMinInStock = 0;
        private string strQuantityUnitName = string.Empty;
        private DateTime dtmInputVoucherDate = DateTime.Now;
        private bool bolIsPriceOfOutputType = false;
        private decimal decRealQuantity = 0;
        //19/09/2017 Đăng bổ sung
        private decimal decPriceProtect = 0;
        private int intInStockStatusID = -1;

        // 21.06.2018 Tuan Anh bổ sung trang thai fifo 
        private int intStatusFIFOID = 0;
        private string strStatusFIFO = string.Empty;


        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }

        public String ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }

        public String SMSProductCode
        {
            get { return strSMSProductCode; }
            set { strSMSProductCode = value; }
        }

        public int VAT
        {
            get { return intVAT; }
            set { intVAT = value; }
        }

        public bool IsRequestIMEI
        {
            get { return bolIsRequestIMEI; }
            set { bolIsRequestIMEI = value; }
        }

        public bool IsInstock
        {
            get { return bolIsInstock; }
            set { bolIsInstock = value; }
        }
        public string IMEI
        {
            get { return strIMEI; }
            set { strIMEI = value; }
        }

        public DateTime EndWarrantyDate
        {
            get { return dtmEndWarrantyDate; }
            set { dtmEndWarrantyDate = value; }
        }

        public decimal Price
        {
            get { return decPrice; }
            set { decPrice = value; }
        }

        public decimal PriceAfVat////huent66 cập nhật: SalePriceAfVat 
        {
            get { return decPriceAfVat; }
            set { decPriceAfVat = value; }
        }



        public decimal Quantity
        {
            get { return decQuantity; }
            set { decQuantity = value; }
        }

        public int VATPercent
        {
            get { return intVATPercent; }
            set { intVATPercent = value; }
        }

        public bool IsService
        {
            get { return bolIsService; }
            set { bolIsService = value; }
        }

        public int FirstCustomerID
        {
            get { return intFirstCustomerID; }
            set { intFirstCustomerID = value; }
        }


        public decimal CostPrice
        {
            get { return decCostPrice; }
            set { decCostPrice = value; }
        }

        public decimal FirstPrice
        {
            get { return decFirstPrice; }
            set { decFirstPrice = value; }
        }

        public int FirstInputTypeID
        {
            get { return intFirstInputTypeID; }
            set { intFirstInputTypeID = value; }
        }

        public bool IsNew
        {
            get { return bolIsNew; }
            set { bolIsNew = value; }
        }


        public bool IsShowProduct
        {
            get { return bolIsShowProduct; }
            set { bolIsShowProduct = value; }
        }


        public String InputVoucherDetailID
        {
            get { return strInputVoucherDetailID; }
            set { strInputVoucherDetailID = value; }
        }

        public String FirstInputVoucherID
        {
            get { return strFirstInputVoucherID; }
            set { strFirstInputVoucherID = value; }
        }


        public DateTime InputVoucherDate
        {
            get { return dtmInputVoucherDate; }
            set { dtmInputVoucherDate = value; }
        }

        public DateTime FirstInvoiceDate
        {
            get { return dtmFirstInvoiceDate; }
            set { dtmFirstInvoiceDate = value; }
        }

        public bool IsOrder
        {
            get { return bolIsOrder; }
            set { bolIsOrder = value; }
        }
        public bool IsOrdering
        {
            get { return bolIsOrdering; }
            set { bolIsOrdering = value; }
        }



        public bool IsDelivery
        {
            get { return bolIsDelivery; }
            set { bolIsDelivery = value; }
        }


        public String OrderID
        {
            get { return strOrderID; }
            set { strOrderID = value; }
        }

        public bool IsHasWarranty
        {
            get { return bolIsHasWarranty; }
            set { bolIsHasWarranty = value; }
        }

        public String Note
        {
            get { return strNote; }
            set { strNote = value; }
        }

        public bool IsReturnProduct
        {
            get { return bolIsReturnProduct; }
            set { bolIsReturnProduct = value; }
        }


        public String InputStaffUser
        {
            get { return strInputStaffUser; }
            set { strInputStaffUser = value; }
        }

        public bool IsCheckRealInput
        {
            get { return bolIsCheckRealInput; }
            set { bolIsCheckRealInput = value; }
        }


        public bool IsReturnWithFee
        {
            get { return bolIsReturnWithFee; }
            set { bolIsReturnWithFee = value; }
        }

        public int ReturnOutputTypeID
        {
            get { return intReturnOutputTypeID; }
            set { intReturnOutputTypeID = value; }
        }

        public bool IsAutoGetIMEI
        {
            get { return bolIsAutoGetIMEI; }
            set { bolIsAutoGetIMEI = value; }
        }


        public int MainGroupID
        {
            get { return intMainGroupID; }
            set { intMainGroupID = value; }
        }


        public int SubGroupID
        {
            get { return intSubGroupID; }
            set { intSubGroupID = value; }
        }


        public int BrandID
        {
            get { return intBrandID; }
            set { intBrandID = value; }
        }

        public String SubGroupName
        {
            get { return strSubGroupName; }
            set { strSubGroupName = value; }
        }


        public String MainGroupName
        {
            get { return strMainGroupName; }
            set { strMainGroupName = value; }
        }

        public bool IsCheckStockQuantity
        {
            get { return bolIsCheckStockQuantity; }
            set { bolIsCheckStockQuantity = value; }
        }


        public bool IsCanReturnOutput
        {
            get { return bolIsCanReturnOutput; }
            set { bolIsCanReturnOutput = value; }
        }

        public bool IsAllowDecimal
        {
            get { return bolIsAllowDecimal; }
            set { bolIsAllowDecimal = value; }
        }

        public decimal RetailPrice
        {
            get { return decRetailPrice; }
            set { decRetailPrice = value; }
        }

        public bool IsPriceOfProduct
        {
            get { return bolIsPriceOfProduct; }
            set { bolIsPriceOfProduct = value; }
        }

        /// <summary>
        /// Số lượng tồn kho tối thiểu của sản phẩm (được khai báo)
        /// </summary>
        public decimal MinInStock
        {
            get { return decMinInStock; }
            set { decMinInStock = value; }
        }

        /// <summary>
        /// Tên đơn vị tính
        /// </summary>
        public string QuantityUnitName
        {
            get { return strQuantityUnitName; }
            set { strQuantityUnitName = value; }
        }

        public bool IsPriceOfOutputType
        {
            get { return bolIsPriceOfOutputType; }
            set { bolIsPriceOfOutputType = value; }
        }
        /// <summary>
        /// số lượng tồn thực của sản phẩm
        /// </summary>
        public decimal RealQuantity
        {
            get { return decRealQuantity; }
            set { decRealQuantity = value; }
        }
        /// <summary>
        /// Loại lấy IMEI
        /// 0: Mặc định
        /// 1: Theo FIFO
        /// 2: Theo độ ưu tiên khai báo
        /// </summary>
        public int GetIMEIType = 0;
        public string InvoiceNote = string.Empty;
        public decimal RewardPoint = 0;


        public decimal PriceProtect
        {
            get { return decPriceProtect; }
            set { decPriceProtect = value; }
        }

        public int InStockStatusID
        {
            get
            {
                return intInStockStatusID;
            }

            set
            {
                intInStockStatusID = value;
            }
        }

        private decimal decRealQuantityProduct = 0;
        /// <summary>
        /// số lượng tồn thực của sản phẩm
        /// </summary>
        public decimal RealQuantityProduct
        {
            get { return decRealQuantityProduct; }
            set { decRealQuantityProduct = value; }
        }

        private int decConsignmentType = 0;
        /// <summary>
        /// số lượng tồn thực của sản phẩm
        /// </summary>
        public int ConsignmentType
        {
            get { return decConsignmentType; }
            set { decConsignmentType = value; }
        }


        public int StatusFIFOID
        {
            get { return intStatusFIFOID; }
            set { intStatusFIFOID = value; }
        }
        public string StatusFIFO 
        {
            get { return strStatusFIFO; }
            set { strStatusFIFO = value; }
        }

        public ProductInStock() { }
    }
}
