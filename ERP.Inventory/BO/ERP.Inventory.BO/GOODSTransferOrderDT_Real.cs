﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.SalesAndServices.SaleOrders.BO
{
    /// <summary>
	/// Created by 		: Do Van Tung 
	/// Created date 	: 06/03/2020 
	/// Bàn giao thực tế
	/// </summary>	
    public class GOODSTransferOrderDT_Real
    {
        #region Member Variables

        private string strGOODSTransferOrderDetailID = string.Empty;
        private string strRealTransferID = string.Empty;
        private string strProductID = string.Empty;
        private int intQUANTITTYTransfer = 0;
        private int intQuantityRealTransfer = 0;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolInStockStatusID = false;

        #endregion


        #region Properties 

        /// <summary>
        /// GOODSTransferOrderDetailID
        /// Mã phiếu bàn giao chi tiết
        /// </summary>
        public string GOODSTransferOrderDetailID
        {
            get { return strGOODSTransferOrderDetailID; }
            set { strGOODSTransferOrderDetailID = value; }
        }

        /// <summary>
        /// RealTransferID
        /// Mã phiếu bàn giao thực tế
        /// </summary>
        public string RealTransferID
        {
            get { return strRealTransferID; }
            set { strRealTransferID = value; }
        }

        /// <summary>
        /// ProductID
        /// Mã sản phẩm
        /// </summary>
        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }

        /// <summary>
        /// QUANTITTYTransfer
        /// Số lượng bàn giao
        /// </summary>
        public int QUANTITTYTransfer
        {
            get { return intQUANTITTYTransfer; }
            set { intQUANTITTYTransfer = value; }
        }

        /// <summary>
        /// QuantityRealTransfer
        /// Số lượng thực tế
        /// </summary>
        public int QuantityRealTransfer
        {
            get { return intQuantityRealTransfer; }
            set { intQuantityRealTransfer = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// Người chỉnh sửa cuối
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// Ngày chỉnh sửa cuối
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// InStockStatusID
        /// Trạng thái hàng trong kho
        /// </summary>
        public bool InStockStatusID
        {
            get { return bolInStockStatusID; }
            set { bolInStockStatusID = value; }
        }


        #endregion


        #region Constructor

        public GOODSTransferOrderDT_Real()
        {
        }
        #endregion


        #region Column Names

        public const String colGOODSTransferOrderDetailID = "GOODSTransferOrderDetailID";
        public const String colRealTransferID = "RealTransferID";
        public const String colProductID = "ProductID";
        public const String colQUANTITTYTransfer = "QUANTITTYTransfer";
        public const String colQuantityRealTransfer = "QuantityRealTransfer";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colInStockStatusID = "InStockStatusID";

        #endregion //Column names


    }
}


