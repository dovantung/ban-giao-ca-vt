
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 12/26/2012 
	/// Chi tiết IMEI phiếu kiểm kê
	/// </summary>	
	public class InventoryDetailIMEI
	{	
	
	
		#region Member Variables

		private string strInventoryDetailID = string.Empty;
		private DateTime? dtmInventoryDate;
		private int intInventoryStoreID = 0;
		private string strIMEI = string.Empty;
		private string strNote = string.Empty;
		private int intCabinetNumber = 0;
		private bool bolIsInStock = false;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strDeletedNote = string.Empty;
        private int intProductStatusID = -1;
        private decimal decQuantityStock = 0;
        #endregion


        #region Properties 
        public int ProductStatusID
        {
            get { return intProductStatusID; }
            set { intProductStatusID = value; }
        }
		/// <summary>
		/// InventoryDetailID
		/// 
		/// </summary>
		public string InventoryDetailID
		{
			get { return  strInventoryDetailID; }
			set { strInventoryDetailID = value; }
		}

		/// <summary>
		/// InventoryDate
		/// 
		/// </summary>
		public DateTime? InventoryDate
		{
			get { return  dtmInventoryDate; }
			set { dtmInventoryDate = value; }
		}

		/// <summary>
		/// InventoryStoreID
		/// 
		/// </summary>
		public int InventoryStoreID
		{
			get { return  intInventoryStoreID; }
			set { intInventoryStoreID = value; }
		}

		/// <summary>
		/// IMEI
		/// 
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// CabinetNumber
		/// 
		/// </summary>
		public int CabinetNumber
		{
			get { return  intCabinetNumber; }
			set { intCabinetNumber = value; }
		}

		/// <summary>
		/// IsInStock
		/// 
		/// </summary>
		public bool IsInStock
		{
			get { return  bolIsInStock; }
			set { bolIsInStock = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// DeletedNote
		/// 
		/// </summary>
		public string DeletedNote
		{
			get { return  strDeletedNote; }
			set { strDeletedNote = value; }
		}

        public decimal QuantityStock
        {
            get { return decQuantityStock; }
            set { decQuantityStock = value; }
        }
        #endregion


        #region Constructor

        public InventoryDetailIMEI()
		{
		}
		#endregion


		#region Column Names

		public const String colInventoryDetailID = "InventoryDetailID";
		public const String colInventoryDate = "InventoryDate";
		public const String colInventoryStoreID = "InventoryStoreID";
		public const String colIMEI = "IMEI";
		public const String colNote = "Note";
		public const String colCabinetNumber = "CabinetNumber";
		public const String colIsInStock = "IsInStock";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colDeletedNote = "DeletedNote";
        public const String colProductStatusID = "ProductStatusID";
        public const String colQuantityStock = "QuantityStock";
        
        #endregion //Column names


    }
}
