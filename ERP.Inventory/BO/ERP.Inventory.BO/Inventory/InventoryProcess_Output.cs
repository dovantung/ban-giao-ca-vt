
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 01/28/13 
	/// Chi tiết quản lý yêu cầu kiểm kê - xuất thiếu
	/// </summary>	
	public class InventoryProcess_Output
	{	
	
	
		#region Member Variables

		private string strOutputProcessID = string.Empty;
		private string strInventoryProcessID = string.Empty;
		private DateTime? dtmInventoryDate;
		private string strProductID = string.Empty;
		private string strIMEI = string.Empty;
		private decimal decQuantity;
		private int intVAT = 0;
		private int intVATPercent = 0;
		private decimal decOutputPrice;
		private decimal decCollectArrearPrice;
		private string strUnEventID = string.Empty;
		private string strOutputVoucherID = string.Empty;
		private string strOutputVoucherDetailID = string.Empty;
		private int intCreatedStoreID = 0;
        private string strUnEventNote = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strDeletedReason = string.Empty;
        private int intProductStatusID = -1;
		#endregion


		#region Properties 
        public int ProductStatusID
        {
            get { return intProductStatusID; }
            set { intProductStatusID = value; }
        }
		/// <summary>
		/// OutputProcessID
		/// 
		/// </summary>
		public string OutputProcessID
		{
			get { return  strOutputProcessID; }
			set { strOutputProcessID = value; }
		}

		/// <summary>
		/// InventoryProcessID
		/// Mã yêu cầu kiểm kê
		/// </summary>
		public string InventoryProcessID
		{
			get { return  strInventoryProcessID; }
			set { strInventoryProcessID = value; }
		}

		/// <summary>
		/// InventoryDate
		/// 
		/// </summary>
		public DateTime? InventoryDate
		{
			get { return  dtmInventoryDate; }
			set { dtmInventoryDate = value; }
		}

		/// <summary>
		/// ProductID
		/// Mã sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// 
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// Quantity
		/// 
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// VAT
		/// 
		/// </summary>
		public int VAT
		{
			get { return  intVAT; }
			set { intVAT = value; }
		}

		/// <summary>
		/// VATPercent
		/// 
		/// </summary>
		public int VATPercent
		{
			get { return  intVATPercent; }
			set { intVATPercent = value; }
		}

		/// <summary>
		/// OutputPrice
		/// Giá xuất đã bao gồm VAT
		/// </summary>
		public decimal OutputPrice
		{
			get { return  decOutputPrice; }
			set { decOutputPrice = value; }
		}

		/// <summary>
		/// CollectArrearPrice
		/// Tiền truy thu
		/// </summary>
		public decimal CollectArrearPrice
		{
			get { return  decCollectArrearPrice; }
			set { decCollectArrearPrice = value; }
		}

		/// <summary>
		/// UnEventID
		/// 
		/// </summary>
		public string UnEventID
		{
			get { return  strUnEventID; }
			set { strUnEventID = value; }
		}

		/// <summary>
		/// OutputVoucherID
		/// Mã phiếu xuất thiếu
		/// </summary>
		public string OutputVoucherID
		{
			get { return  strOutputVoucherID; }
			set { strOutputVoucherID = value; }
		}

		/// <summary>
		/// OutputVoucherDetailID
		/// Mã chi tiết phiếu xuất thiếu
		/// </summary>
		public string OutputVoucherDetailID
		{
			get { return  strOutputVoucherDetailID; }
			set { strOutputVoucherDetailID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

        public string UnEventNote
        {
            get { return strUnEventNote; }
            set { strUnEventNote = value; }
        }

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// DeletedReason
		/// 
		/// </summary>
		public string DeletedReason
		{
			get { return  strDeletedReason; }
			set { strDeletedReason = value; }
		}


		#endregion			
		
		
		#region Constructor

		public InventoryProcess_Output()
		{
		}
		#endregion


		#region Column Names

		public const String colOutputProcessID = "OutputProcessID";
		public const String colInventoryProcessID = "InventoryProcessID";
		public const String colInventoryDate = "InventoryDate";
		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
		public const String colQuantity = "Quantity";
		public const String colVAT = "VAT";
		public const String colVATPercent = "VATPercent";
		public const String colOutputPrice = "OutputPrice";
		public const String colCollectArrearPrice = "CollectArrearPrice";
		public const String colUnEventID = "UnEventID";
		public const String colOutputVoucherID = "OutputVoucherID";
		public const String colOutputVoucherDetailID = "OutputVoucherDetailID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colDeletedReason = "DeletedReason";
        public const String colProductStatusID = "ProductStatusID";
		#endregion //Column names

		
	}
}
