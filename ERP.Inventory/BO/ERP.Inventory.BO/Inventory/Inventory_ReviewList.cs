
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 12/26/2012 
	/// Danh sách duyệt một phiếu kiểm kê
	/// </summary>	
	public class Inventory_ReviewList
	{	
	
	
		#region Member Variables

		private string strInventoryID = string.Empty;
		private int intReviewLevelID = 0;
		private string strUserName = string.Empty;
		private DateTime? dtmInventoryDate;
		private int intReviewStatus = 0;
		private bool bolIsReviewed = false;
		private string strNote = string.Empty;
		private DateTime? dtmReviewedDate;
		private string strReviewedUserHostAddress = string.Empty;
		private string strReviewedCertificateString = string.Empty;
		private string strReviewedLoginLogID = string.Empty;
		private DateTime? dtmCreatedDate;

		#endregion


		#region Properties 

		/// <summary>
		/// InventoryID
		/// 
		/// </summary>
		public string InventoryID
		{
			get { return  strInventoryID; }
			set { strInventoryID = value; }
		}

		/// <summary>
		/// ReviewLevelID
		/// 
		/// </summary>
		public int ReviewLevelID
		{
			get { return  intReviewLevelID; }
			set { intReviewLevelID = value; }
		}

		/// <summary>
		/// UserName
		/// 
		/// </summary>
		public string UserName
		{
			get { return  strUserName; }
			set { strUserName = value; }
		}

		/// <summary>
		/// InventoryDate
		/// 
		/// </summary>
		public DateTime? InventoryDate
		{
			get { return  dtmInventoryDate; }
			set { dtmInventoryDate = value; }
		}

		/// <summary>
		/// ReviewStatus
		/// Trạng thái duyệt; 0: Chưa duyệt, 1: Đang xử lý, 2: Đồng ý, 3: Từ chối
		/// </summary>
		public int ReviewStatus
		{
			get { return  intReviewStatus; }
			set { intReviewStatus = value; }
		}

		/// <summary>
		/// IsReviewed
		/// Đã duyệt
		/// </summary>
		public bool IsReviewed
		{
			get { return  bolIsReviewed; }
			set { bolIsReviewed = value; }
		}

		/// <summary>
		/// Note
		/// Ghi chú
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// 
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// ReviewedUserHostAddress
		/// 
		/// </summary>
		public string ReviewedUserHostAddress
		{
			get { return  strReviewedUserHostAddress; }
			set { strReviewedUserHostAddress = value; }
		}

		/// <summary>
		/// ReviewedCertificateString
		/// 
		/// </summary>
		public string ReviewedCertificateString
		{
			get { return  strReviewedCertificateString; }
			set { strReviewedCertificateString = value; }
		}

		/// <summary>
		/// ReviewedLoginLogID
		/// 
		/// </summary>
		public string ReviewedLoginLogID
		{
			get { return  strReviewedLoginLogID; }
			set { strReviewedLoginLogID = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public Inventory_ReviewList()
		{
		}
		#endregion


		#region Column Names

		public const String colInventoryID = "InventoryID";
		public const String colReviewLevelID = "ReviewLevelID";
		public const String colUserName = "UserName";
		public const String colInventoryDate = "InventoryDate";
		public const String colReviewStatus = "ReviewStatus";
		public const String colIsReviewed = "IsReviewed";
		public const String colNote = "Note";
		public const String colReviewedDate = "ReviewedDate";
		public const String colReviewedUserHostAddress = "ReviewedUserHostAddress";
		public const String colReviewedCertificateString = "ReviewedCertificateString";
		public const String colReviewedLoginLogID = "ReviewedLoginLogID";
		public const String colCreatedDate = "CreatedDate";

		#endregion //Column names

		
	}
}
