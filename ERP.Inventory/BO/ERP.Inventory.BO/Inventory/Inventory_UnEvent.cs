
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 12/26/2012 
	/// 
	/// </summary>	
	public class Inventory_UnEvent
	{	
	
	
		#region Member Variables

		private string strUnEventID = string.Empty;
		private string strInventoryID = string.Empty;
		private DateTime? dtmInventoryDate;
		private string strProductID = string.Empty;
		private int intCabinetNumber = 0;
		private string strStockIMEI = string.Empty;
		private decimal decStockQuantity;
		private string strInventoryIMEI = string.Empty;
		private decimal decInventoryQuantity;
		private decimal decUnEventQuantity;
		private string strUnEventExplain = string.Empty;
		private string strExplainNote = string.Empty;
		private string strExplainUser = string.Empty;
		private DateTime? dtmExplainDate;
		private decimal decAdjustUnEventQuantity;
		private decimal decFinalUnEventQuantity;
		private decimal decTotalCost;
		private string strAdjustNote = string.Empty;
		private string strAdjustUser = string.Empty;
		private DateTime? dtmAdjustDate;
		private bool bolISReport = false;
		private decimal decCostPrice;
		private decimal decSalePrice;
		private string strDutyUser = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;

		#endregion


		#region Properties 

		/// <summary>
		/// UnEventID
		/// 
		/// </summary>
		public string UnEventID
		{
			get { return  strUnEventID; }
			set { strUnEventID = value; }
		}

		/// <summary>
		/// InventoryID
		/// 
		/// </summary>
		public string InventoryID
		{
			get { return  strInventoryID; }
			set { strInventoryID = value; }
		}

		/// <summary>
		/// InventoryDate
		/// 
		/// </summary>
		public DateTime? InventoryDate
		{
			get { return  dtmInventoryDate; }
			set { dtmInventoryDate = value; }
		}

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// CabinetNumber
		/// 
		/// </summary>
		public int CabinetNumber
		{
			get { return  intCabinetNumber; }
			set { intCabinetNumber = value; }
		}

		/// <summary>
		/// StockIMEI
		/// 
		/// </summary>
		public string StockIMEI
		{
			get { return  strStockIMEI; }
			set { strStockIMEI = value; }
		}

		/// <summary>
		/// StockQuantity
		/// 
		/// </summary>
		public decimal StockQuantity
		{
			get { return  decStockQuantity; }
			set { decStockQuantity = value; }
		}

		/// <summary>
		/// InventoryIMEI
		/// 
		/// </summary>
		public string InventoryIMEI
		{
			get { return  strInventoryIMEI; }
			set { strInventoryIMEI = value; }
		}

		/// <summary>
		/// InventoryQuantity
		/// 
		/// </summary>
		public decimal InventoryQuantity
		{
			get { return  decInventoryQuantity; }
			set { decInventoryQuantity = value; }
		}

		/// <summary>
		/// UnEventQuantity
		/// 
		/// </summary>
		public decimal UnEventQuantity
		{
			get { return  decUnEventQuantity; }
			set { decUnEventQuantity = value; }
		}

		/// <summary>
		/// UnEventExplain
		/// 
		/// </summary>
		public string UnEventExplain
		{
			get { return  strUnEventExplain; }
			set { strUnEventExplain = value; }
		}

		/// <summary>
		/// ExplainNote
		/// 
		/// </summary>
		public string ExplainNote
		{
			get { return  strExplainNote; }
			set { strExplainNote = value; }
		}

		/// <summary>
		/// ExplainUser
		/// 
		/// </summary>
		public string ExplainUser
		{
			get { return  strExplainUser; }
			set { strExplainUser = value; }
		}

		/// <summary>
		/// ExplainDate
		/// 
		/// </summary>
		public DateTime? ExplainDate
		{
			get { return  dtmExplainDate; }
			set { dtmExplainDate = value; }
		}

		/// <summary>
		/// AdjustUnEventQuantity
		/// 
		/// </summary>
		public decimal AdjustUnEventQuantity
		{
			get { return  decAdjustUnEventQuantity; }
			set { decAdjustUnEventQuantity = value; }
		}

		/// <summary>
		/// FinalUnEventQuantity
		/// 
		/// </summary>
		public decimal FinalUnEventQuantity
		{
			get { return  decFinalUnEventQuantity; }
			set { decFinalUnEventQuantity = value; }
		}

		/// <summary>
		/// TotalCost
		/// 
		/// </summary>
		public decimal TotalCost
		{
			get { return  decTotalCost; }
			set { decTotalCost = value; }
		}

		/// <summary>
		/// AdjustNote
		/// 
		/// </summary>
		public string AdjustNote
		{
			get { return  strAdjustNote; }
			set { strAdjustNote = value; }
		}

		/// <summary>
		/// AdjustUser
		/// 
		/// </summary>
		public string AdjustUser
		{
			get { return  strAdjustUser; }
			set { strAdjustUser = value; }
		}

		/// <summary>
		/// AdjustDate
		/// 
		/// </summary>
		public DateTime? AdjustDate
		{
			get { return  dtmAdjustDate; }
			set { dtmAdjustDate = value; }
		}

		/// <summary>
		/// ISReport
		/// 
		/// </summary>
		public bool ISReport
		{
			get { return  bolISReport; }
			set { bolISReport = value; }
		}

		/// <summary>
		/// CostPrice
		/// 
		/// </summary>
		public decimal CostPrice
		{
			get { return  decCostPrice; }
			set { decCostPrice = value; }
		}

		/// <summary>
		/// SalePrice
		/// 
		/// </summary>
		public decimal SalePrice
		{
			get { return  decSalePrice; }
			set { decSalePrice = value; }
		}

		/// <summary>
		/// DutyUser
		/// 
		/// </summary>
		public string DutyUser
		{
			get { return  strDutyUser; }
			set { strDutyUser = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public Inventory_UnEvent()
		{
		}
		#endregion


		#region Column Names

		public const String colUnEventID = "UnEventID";
		public const String colInventoryID = "InventoryID";
		public const String colInventoryDate = "InventoryDate";
		public const String colProductID = "ProductID";
		public const String colCabinetNumber = "CabinetNumber";
		public const String colStockIMEI = "StockIMEI";
		public const String colStockQuantity = "StockQuantity";
		public const String colInventoryIMEI = "InventoryIMEI";
		public const String colInventoryQuantity = "InventoryQuantity";
		public const String colUnEventQuantity = "UnEventQuantity";
		public const String colUnEventExplain = "UnEventExplain";
		public const String colExplainNote = "ExplainNote";
		public const String colExplainUser = "ExplainUser";
		public const String colExplainDate = "ExplainDate";
		public const String colAdjustUnEventQuantity = "AdjustUnEventQuantity";
		public const String colFinalUnEventQuantity = "FinalUnEventQuantity";
		public const String colTotalCost = "TotalCost";
		public const String colAdjustNote = "AdjustNote";
		public const String colAdjustUser = "AdjustUser";
		public const String colAdjustDate = "AdjustDate";
		public const String colISReport = "ISReport";
		public const String colCostPrice = "CostPrice";
		public const String colSalePrice = "SalePrice";
		public const String colDutyUser = "DutyUser";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
