
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 01/28/13 
	/// Quản lý yêu cầu kiểm kê
	/// </summary>	
	public class InventoryProcess
	{	
	
	
		#region Member Variables

		private string strInventoryProcessID = string.Empty;
		private string strInventoryID = string.Empty;
		private int intInventoryStoreID = 0;
		private int intMainGroupID = 0;
		private bool bolIsNew = false;
		private int intInventoryTermID = 0;
		private DateTime? dtmInventoryDate;
		private string strInputContent = string.Empty;
		private string strOutputContent = string.Empty;
		private string strPrChangeContent = string.Empty;
		private bool bolIsReviewed = false;
		private string strReviewedUser = string.Empty;
		private DateTime? dtmReviewedDate;
		private bool bolIsProcess = false;
		private string strProcessUser = string.Empty;
		private DateTime? dtmProcessDate;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strDeletedReason = string.Empty;
        private List<InventoryProcess_Input> objInventoryProcessInputList = null;
        private List<InventoryProcess_Output> objInventoryProcessOutputList = null;
        private List<InventoryProcess_PrChange> objInventoryProcessPrChangeList = null;
        private List<InventoryProcess_StChange> objInventoryProcessStChangeList = null;
        private DataTable dtbInventoryProcessInput = null;
        private DataTable dtbInventoryProcessOutput = null;
        private DataTable dtbInventoryProcessPrChange = null;
        private DataTable dtbInventoryProcessStChange = null;
		#endregion


		#region Properties 

		/// <summary>
		/// InventoryProcessID
		/// 
		/// </summary>
		public string InventoryProcessID
		{
			get { return  strInventoryProcessID; }
			set { strInventoryProcessID = value; }
		}

		/// <summary>
		/// InventoryID
		/// Mã phiếu kiểm kê
		/// </summary>
		public string InventoryID
		{
			get { return  strInventoryID; }
			set { strInventoryID = value; }
		}

		/// <summary>
		/// InventoryStoreID
		/// Kho kiểm kê
		/// </summary>
		public int InventoryStoreID
		{
			get { return  intInventoryStoreID; }
			set { intInventoryStoreID = value; }
		}

		/// <summary>
		/// MainGroupID
		/// Ngành hàng kiểm kê
		/// </summary>
		public int MainGroupID
		{
			get { return  intMainGroupID; }
			set { intMainGroupID = value; }
		}

		/// <summary>
		/// IsNew
		/// Mới cũ
		/// </summary>
		public bool IsNew
		{
			get { return  bolIsNew; }
			set { bolIsNew = value; }
		}

		/// <summary>
		/// InventoryTermID
		/// Mã kỳ kiểm kê
		/// </summary>
		public int InventoryTermID
		{
			get { return  intInventoryTermID; }
			set { intInventoryTermID = value; }
		}

		/// <summary>
		/// InventoryDate
		/// Ngày kiểm kê
		/// </summary>
		public DateTime? InventoryDate
		{
			get { return  dtmInventoryDate; }
			set { dtmInventoryDate = value; }
		}

		/// <summary>
		/// InputContent
		/// 
		/// </summary>
		public string InputContent
		{
			get { return  strInputContent; }
			set { strInputContent = value; }
		}

		/// <summary>
		/// OutputContent
		/// 
		/// </summary>
		public string OutputContent
		{
			get { return  strOutputContent; }
			set { strOutputContent = value; }
		}

		/// <summary>
		/// PrChangeContent
		/// 
		/// </summary>
		public string PrChangeContent
		{
			get { return  strPrChangeContent; }
			set { strPrChangeContent = value; }
		}

		/// <summary>
		/// IsReviewed
		/// Đã duyệt
		/// </summary>
		public bool IsReviewed
		{
			get { return  bolIsReviewed; }
			set { bolIsReviewed = value; }
		}

		/// <summary>
		/// ReviewedUser
		/// Người duyệt
		/// </summary>
		public string ReviewedUser
		{
			get { return  strReviewedUser; }
			set { strReviewedUser = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// Ngày duyệt
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// IsProcess
		/// Đã xử lý yêu cầu
		/// </summary>
		public bool IsProcess
		{
			get { return  bolIsProcess; }
			set { bolIsProcess = value; }
		}

		/// <summary>
		/// ProcessUser
		/// Người xử lý phiếu yêu cầu
		/// </summary>
		public string ProcessUser
		{
			get { return  strProcessUser; }
			set { strProcessUser = value; }
		}

		/// <summary>
		/// ProcessDate
		/// Người xử lý phiếu yêu cầu
		/// </summary>
		public DateTime? ProcessDate
		{
			get { return  dtmProcessDate; }
			set { dtmProcessDate = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// Nhân viên tạo
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// Ngày tạo
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// DeletedReason
		/// Lý do hủy phiếu yêu cầu xử lý kiểm kê
		/// </summary>
		public string DeletedReason
		{
			get { return  strDeletedReason; }
			set { strDeletedReason = value; }
		}

        public List<InventoryProcess_Input> InventoryProcessInputList
        {
            get { return objInventoryProcessInputList; }
            set { objInventoryProcessInputList = value; }
        }

        public List<InventoryProcess_Output> InventoryProcessOutputList
        {
            get { return objInventoryProcessOutputList; }
            set { objInventoryProcessOutputList = value; }
        }

        public List<InventoryProcess_PrChange> InventoryProcessPrChangeList
        {
            get { return objInventoryProcessPrChangeList; }
            set { objInventoryProcessPrChangeList = value; }
        }

        public List<InventoryProcess_StChange> InventoryProcessStChangeList
        {
            get { return objInventoryProcessStChangeList; }
            set { objInventoryProcessStChangeList = value; }
        }

        public DataTable tblInventoryProcessInput
        {
            get { return dtbInventoryProcessInput; }
            set { dtbInventoryProcessInput = value; }
        }

        public DataTable tblInventoryProcessOutput
        {
            get { return dtbInventoryProcessOutput; }
            set { dtbInventoryProcessOutput = value; }
        }

        public DataTable tblInventoryProcessPrChange
        {
            get { return dtbInventoryProcessPrChange; }
            set { dtbInventoryProcessPrChange = value; }
        }

        public DataTable tblInventoryProcessStChange
        {
            get { return dtbInventoryProcessStChange; }
            set { dtbInventoryProcessStChange = value; }
        }

        public string StatusChangeContent { get; set; }
        #endregion


        #region Constructor

        public InventoryProcess()
		{
		}
		#endregion

		#region Column Names

		public const String colInventoryProcessID = "InventoryProcessID";
		public const String colInventoryID = "InventoryID";
		public const String colInventoryStoreID = "InventoryStoreID";
		public const String colMainGroupID = "MainGroupID";
		public const String colIsNew = "IsNew";
		public const String colInventoryTermID = "InventoryTermID";
		public const String colInventoryDate = "InventoryDate";
		public const String colInputContent = "InputContent";
		public const String colOutputContent = "OutputContent";
		public const String colPrChangeContent = "PrChangeContent";
		public const String colIsReviewed = "IsReviewed";
		public const String colReviewedUser = "ReviewedUser";
		public const String colReviewedDate = "ReviewedDate";
		public const String colIsProcess = "IsProcess";
		public const String colProcessUser = "ProcessUser";
		public const String colProcessDate = "ProcessDate";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colDeletedReason = "DeletedReason";
        public const string colStatusChangeContent = "STATUSCHANGECONTENT";

        #endregion //Column names

    }
}
