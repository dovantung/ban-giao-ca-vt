
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 14/06/2014 
	/// Chi tiết phiếu kiểm kê nhanh
	/// </summary>	
	public class QuickInventoryDetail
	{	
	
	
		#region Member Variables

		private string strQuickInventoryDetailID = string.Empty;
		private string strQuickInventoryID = string.Empty;
		private DateTime? dtmInventoryDate;
		private int intInventoryStoreID = 0;
		private string strProductID = string.Empty;
		private int intCabinetNumber = 0;
		private decimal decQuantity;
		private decimal decInstockQuantity;
		private string strNote = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strDeletedNote = string.Empty;
        private int intProductStatusID = -1;
		#endregion


		#region Properties 
        public int ProductStatusID
        {
            get { return intProductStatusID; }
            set { intProductStatusID = value; }
        }
		/// <summary>
		/// QuickInventoryDetailID
		/// 
		/// </summary>
		public string QuickInventoryDetailID
		{
			get { return  strQuickInventoryDetailID; }
			set { strQuickInventoryDetailID = value; }
		}

		/// <summary>
		/// QuickInventoryID
		/// 
		/// </summary>
		public string QuickInventoryID
		{
			get { return  strQuickInventoryID; }
			set { strQuickInventoryID = value; }
		}

		/// <summary>
		/// InventoryDate
		/// 
		/// </summary>
		public DateTime? InventoryDate
		{
			get { return  dtmInventoryDate; }
			set { dtmInventoryDate = value; }
		}

		/// <summary>
		/// InventoryStoreID
		/// 
		/// </summary>
		public int InventoryStoreID
		{
			get { return  intInventoryStoreID; }
			set { intInventoryStoreID = value; }
		}

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// CabinetNumber
		/// Tủ kiểm kê
		/// </summary>
		public int CabinetNumber
		{
			get { return  intCabinetNumber; }
			set { intCabinetNumber = value; }
		}

		/// <summary>
		/// Quantity
		/// Số lượng kiểm kê
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// InstockQuantity
		/// Số lượng tồn
		/// </summary>
		public decimal InstockQuantity
		{
			get { return  decInstockQuantity; }
			set { decInstockQuantity = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// DeletedNote
		/// 
		/// </summary>
		public string DeletedNote
		{
			get { return  strDeletedNote; }
			set { strDeletedNote = value; }
		}


		#endregion			
		
		
		#region Constructor

		public QuickInventoryDetail()
		{
		}
		#endregion


		#region Column Names

		public const String colQuickInventoryDetailID = "QuickInventoryDetailID";
		public const String colQuickInventoryID = "QuickInventoryID";
		public const String colInventoryDate = "InventoryDate";
		public const String colInventoryStoreID = "InventoryStoreID";
		public const String colProductID = "ProductID";
		public const String colCabinetNumber = "CabinetNumber";
		public const String colQuantity = "Quantity";
		public const String colInstockQuantity = "InstockQuantity";
		public const String colNote = "Note";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colDeletedNote = "DeletedNote";
        public const String colProductStatusID = "ProductStatusID";
		#endregion //Column names

		
	}
}
