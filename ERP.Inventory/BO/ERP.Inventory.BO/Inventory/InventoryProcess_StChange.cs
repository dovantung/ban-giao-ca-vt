
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 01/28/13 
	/// Chi tiết quản lý yêu cầu kiểm kê - nhập đổi
	/// </summary>	
	public class InventoryProcess_StChange
	{	
	
	
		#region Member Variables

		private string strProductChangeProcessID = string.Empty;
		private string strInventoryProcessID = string.Empty;
		private DateTime? dtmInventoryDate;
		private string strProductID_Out = string.Empty;
		private string strIMEI_Out = string.Empty;
		private string strProductID_In = string.Empty;
		private string strIMEI_In = string.Empty;
		private decimal decQuantity;
		private decimal decRefSalePrice_Out;
		private decimal decRefSalePrice_In;
		private decimal decCollectArrearPrice;
		private decimal decUnEventAmount;
		private string strUnEventID = string.Empty;
		private string strProductChangID = string.Empty;
		private string strProductChangeDetailID = string.Empty;
        private int intCreatedStoreID = 0;
        private string strUnEventNote = string.Empty;
        private string strChangeUnEventID = string.Empty;
        private string strChangeUnEventNote = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strDeletedReason = string.Empty;
        private int intIn_ProductStatusID = -1;
        private int intOut_ProductStatusID = -1;
		#endregion


		#region Properties 
        public int In_ProductStatusID
        {
            get { return intIn_ProductStatusID; }
            set { intIn_ProductStatusID = value; }
        }
        public int Out_ProductStatusID
        {
            get { return intOut_ProductStatusID; }
            set { intOut_ProductStatusID = value; }
        }
		/// <summary>
		/// ProductChangeProcessID
		/// 
		/// </summary>
		public string ProductChangeProcessID
		{
			get { return  strProductChangeProcessID; }
			set { strProductChangeProcessID = value; }
		}

		/// <summary>
		/// InventoryProcessID
		/// INVENTORYPROCESSID
		/// </summary>
		public string InventoryProcessID
		{
			get { return  strInventoryProcessID; }
			set { strInventoryProcessID = value; }
		}

		/// <summary>
		/// InventoryDate
		/// 
		/// </summary>
		public DateTime? InventoryDate
		{
			get { return  dtmInventoryDate; }
			set { dtmInventoryDate = value; }
		}

		/// <summary>
		/// ProductID_Out
		/// 
		/// </summary>
		public string ProductID_Out
		{
			get { return  strProductID_Out; }
			set { strProductID_Out = value; }
		}

		/// <summary>
		/// IMEI_Out
		/// 
		/// </summary>
		public string IMEI_Out
		{
			get { return  strIMEI_Out; }
			set { strIMEI_Out = value; }
		}

		/// <summary>
		/// ProductID_In
		/// 
		/// </summary>
		public string ProductID_In
		{
			get { return  strProductID_In; }
			set { strProductID_In = value; }
		}

		/// <summary>
		/// IMEI_In
		/// 
		/// </summary>
		public string IMEI_In
		{
			get { return  strIMEI_In; }
			set { strIMEI_In = value; }
		}

		/// <summary>
		/// Quantity
		/// 
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// RefSalePrice_Out
		/// Giá bán sản phẩm xuất tham khảo
		/// </summary>
		public decimal RefSalePrice_Out
		{
			get { return  decRefSalePrice_Out; }
			set { decRefSalePrice_Out = value; }
		}

		/// <summary>
		/// RefSalePrice_In
		/// Giá bán sản phẩm nhập tham khảo
		/// </summary>
		public decimal RefSalePrice_In
		{
			get { return  decRefSalePrice_In; }
			set { decRefSalePrice_In = value; }
		}

		/// <summary>
		/// CollectArrearPrice
		/// Tiền truy thu
		/// </summary>
		public decimal CollectArrearPrice
		{
			get { return  decCollectArrearPrice; }
			set { decCollectArrearPrice = value; }
		}

		/// <summary>
		/// UnEventAmount
		/// Số tiền chênh lệch
		/// </summary>
		public decimal UnEventAmount
		{
			get { return  decUnEventAmount; }
			set { decUnEventAmount = value; }
		}

		/// <summary>
		/// UnEventID
		/// 
		/// </summary>
		public string UnEventID
		{
			get { return  strUnEventID; }
			set { strUnEventID = value; }
		}

		/// <summary>
		/// ProductChangID
		/// 
		/// </summary>
		public string ProductChangID
		{
			get { return  strProductChangID; }
			set { strProductChangID = value; }
		}

		/// <summary>
		/// ProductChangeDetailID
		/// 
		/// </summary>
		public string ProductChangeDetailID
		{
			get { return  strProductChangeDetailID; }
			set { strProductChangeDetailID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

        public string UnEventNote
        {
            get { return strUnEventNote; }
            set { strUnEventNote = value; }
        }

        public string ChangeUnEventID
        {
            get { return strChangeUnEventID; }
            set { strChangeUnEventID = value; }
        }

        public string ChangeUnEventNote
        {
            get { return strChangeUnEventNote; }
            set { strChangeUnEventNote = value; }
        }

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// DeletedReason
		/// 
		/// </summary>
		public string DeletedReason
		{
			get { return  strDeletedReason; }
			set { strDeletedReason = value; }
		}


		#endregion			
		
		
		#region Constructor

        public InventoryProcess_StChange()
		{
		}
		#endregion


		#region Column Names

		public const String colProductChangeProcessID = "ProductChangeProcessID";
		public const String colInventoryProcessID = "InventoryProcessID";
		public const String colInventoryDate = "InventoryDate";
		public const String colProductID_Out = "ProductID_Out";
		public const String colIMEI_Out = "IMEI_Out";
		public const String colProductID_In = "ProductID_In";
		public const String colIMEI_In = "IMEI_In";
		public const String colQuantity = "Quantity";
		public const String colRefSalePrice_Out = "RefSalePrice_Out";
		public const String colRefSalePrice_In = "RefSalePrice_In";
		public const String colCollectArrearPrice = "CollectArrearPrice";
		public const String colUnEventAmount = "UnEventAmount";
		public const String colUnEventID = "UnEventID";
		public const String colProductChangID = "ProductChangID";
		public const String colProductChangeDetailID = "ProductChangeDetailID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colDeletedReason = "DeletedReason";
        public const String colIn_ProductStatusID = "IN_ProductStatusID";
        public const String colOut_ProductStatusID = "OUT_ProductStatusID";
		#endregion //Column names

		
	}
}
