
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 14/06/2014 
	/// 
	/// </summary>	
	public class QuickInventory
    {


        #region Member Variables

        private string strQuickInventoryID = string.Empty;
        private int intInventoryStoreID = 0;
        private int intMainGroupID = 0;
        private int intSubGroupID = 0;
        private int intBrandID = 0;
        private int intIsNewType = 0;
        private DateTime? dtmLockStockTime;
        private DateTime? dtmInventoryDate;
        private string strInventoryUser = string.Empty;
        private string strContent = string.Empty;
        private bool bolIsReviewed = false;
        private string strReviewedUser = string.Empty;
        private DateTime? dtmReviewedDate;
        private int intCreatedStoreID = 0;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private string strDeletedNote = string.Empty;
        private string strNote = string.Empty;
        #endregion


        #region Properties 

        /// <summary>
        /// QuickInventoryID
        /// 
        /// </summary>
        public string QuickInventoryID
        {
            get { return strQuickInventoryID; }
            set { strQuickInventoryID = value; }
        }

        /// <summary>
        /// InventoryStoreID
        /// 
        /// </summary>
        public int InventoryStoreID
        {
            get { return intInventoryStoreID; }
            set { intInventoryStoreID = value; }
        }

        /// <summary>
        /// MainGroupID
        /// 
        /// </summary>
        public int MainGroupID
        {
            get { return intMainGroupID; }
            set { intMainGroupID = value; }
        }

        /// <summary>
        /// SubGroupID
        /// 
        /// </summary>
        public int SubGroupID
        {
            get { return intSubGroupID; }
            set { intSubGroupID = value; }
        }

        /// <summary>
        /// BrandID
        /// 
        /// </summary>
        public int BrandID
        {
            get { return intBrandID; }
            set { intBrandID = value; }
        }

        /// <summary>
        /// IsNewType
        /// 
        /// </summary>
        public int IsNewType
        {
            get { return intIsNewType; }
            set { intIsNewType = value; }
        }

        /// <summary>
        /// LockStockTime
        /// 
        /// </summary>
        public DateTime? LockStockTime
        {
            get { return dtmLockStockTime; }
            set { dtmLockStockTime = value; }
        }

        /// <summary>
        /// InventoryDate
        /// 
        /// </summary>
        public DateTime? InventoryDate
        {
            get { return dtmInventoryDate; }
            set { dtmInventoryDate = value; }
        }

        /// <summary>
        /// InventoryUser
        /// 
        /// </summary>
        public string InventoryUser
        {
            get { return strInventoryUser; }
            set { strInventoryUser = value; }
        }

        /// <summary>
        /// Content
        /// 
        /// </summary>
        public string Content
        {
            get { return strContent; }
            set { strContent = value; }
        }

        /// <summary>
        /// IsReviewed
        /// 
        /// </summary>
        public bool IsReviewed
        {
            get { return bolIsReviewed; }
            set { bolIsReviewed = value; }
        }

        /// <summary>
        /// ReviewedUser
        /// 
        /// </summary>
        public string ReviewedUser
        {
            get { return strReviewedUser; }
            set { strReviewedUser = value; }
        }

        /// <summary>
        /// ReviewedDate
        /// 
        /// </summary>
        public DateTime? ReviewedDate
        {
            get { return dtmReviewedDate; }
            set { dtmReviewedDate = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// 
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        /// <summary>
        /// DeletedNote
        /// 
        /// </summary>
        public string DeletedNote
        {
            get { return strDeletedNote; }
            set { strDeletedNote = value; }
        }

        public List<QuickInventoryDetail> QuickInventoryDetailList { get; set; }
        public List<int> BrandIDList { get; set; }
        public List<string> MainGroupIDList { get; set; }
        public List<string> SubGroupIDList { get; set; }
        public string Note { get; set; }
        #endregion


        #region Constructor

        public QuickInventory()
        {
        }
        #endregion


        #region Column Names

        public const String colQuickInventoryID = "QuickInventoryID";
        public const String colInventoryStoreID = "InventoryStoreID";
        public const String colMainGroupID = "MainGroupID";
        public const String colSubGroupID = "SubGroupID";
        public const String colBrandID = "BrandID";
        public const String colIsNewType = "IsNewType";
        public const String colLockStockTime = "LockStockTime";
        public const String colInventoryDate = "InventoryDate";
        public const String colInventoryUser = "InventoryUser";
        public const String colContent = "Content";
        public const String colIsReviewed = "IsReviewed";
        public const String colReviewedUser = "ReviewedUser";
        public const String colReviewedDate = "ReviewedDate";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colDeletedNote = "DeletedNote";
        public const String colNote = "Note";

        #endregion //Column names


    }
}
