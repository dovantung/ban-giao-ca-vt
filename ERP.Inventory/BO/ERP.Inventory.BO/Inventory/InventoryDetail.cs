
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
    /// Created by 		: Đặng Minh Hùng 
    /// Created date 	: 12/26/2012 
    /// Chi tiết phiếu kiểm kê
    /// </summary>	
    public class InventoryDetail
    {


        #region Member Variables

        private string strInventoryDetailID = string.Empty;
        private string strInventoryID = string.Empty;
        private DateTime? dtmInventoryDate;
        private int intInventoryStoreID = 0;
        private string strProductID = string.Empty;
        private int intCabinetNumber = 0;
        private decimal decQuantity;
        private decimal decInStockQuantity;
        private string strNote = string.Empty;
        private int intCreatedStoreID = 0;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private string strDeletedNote = string.Empty;
        private List<InventoryDetailIMEI> objInventoryDetailIMEIList = null;
        private bool bolIsRequestImei = false;
        private int intProductStatusID = -1;
        private string strProductName = string.Empty;
        private string strProductBCCSID = string.Empty;
        private bool bolIsBCCS = false;
        private decimal decQuantityStock = 0;
        
        #endregion


        #region Properties
        public int ProductStatusID
        {
            get { return intProductStatusID; }
            set { intProductStatusID = value; }
        }
        /// <summary>
        /// InventoryDetailID
        /// 
        /// </summary>
        public string InventoryDetailID
        {
            get { return strInventoryDetailID; }
            set { strInventoryDetailID = value; }
        }

        /// <summary>
        /// InventoryID
        /// 
        /// </summary>
        public string InventoryID
        {
            get { return strInventoryID; }
            set { strInventoryID = value; }
        }

        /// <summary>
        /// InventoryDate
        /// 
        /// </summary>
        public DateTime? InventoryDate
        {
            get { return dtmInventoryDate; }
            set { dtmInventoryDate = value; }
        }

        /// <summary>
        /// InventoryStoreID
        /// 
        /// </summary>
        public int InventoryStoreID
        {
            get { return intInventoryStoreID; }
            set { intInventoryStoreID = value; }
        }

        /// <summary>
        /// ProductID
        /// 
        /// </summary>
        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }

        /// <summary>
        /// CabinetNumber
        /// Tủ kiểm kê
        /// </summary>
        public int CabinetNumber
        {
            get { return intCabinetNumber; }
            set { intCabinetNumber = value; }
        }

        /// <summary>
        /// Quantity
        /// Số lượng kiểm kê
        /// </summary>
        public decimal Quantity
        {
            get { return decQuantity; }
            set { decQuantity = value; }
        }

        /// <summary>
        /// InStockQuantity
        /// Số lượng tồn
        /// </summary>
        public decimal InStockQuantity
        {
            get { return decInStockQuantity; }
            set { decInStockQuantity = value; }
        }

        /// <summary>
        /// Note
        /// 
        /// </summary>
        public string Note
        {
            get { return strNote; }
            set { strNote = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// 
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        /// <summary>
        /// DeletedNote
        /// 
        /// </summary>
        public string DeletedNote
        {
            get { return strDeletedNote; }
            set { strDeletedNote = value; }
        }

        /// <summary>
        /// IsRequestImei
        /// </summary>
        public bool IsRequestImei
        {
            get { return bolIsRequestImei; }
            set { bolIsRequestImei = value; }
        }
        public List<InventoryDetailIMEI> InventoryDetailIMEIList
        {
            get { return objInventoryDetailIMEIList; }
            set { objInventoryDetailIMEIList = value; }
        }
        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }
        public string ProductBCCSID
        {
            get { return strProductBCCSID; }
            set { strProductBCCSID = value; }
        }
        public bool IsBCCS
        {
            get { return bolIsBCCS; }
            set { bolIsBCCS = value; }
        }

        public decimal QuantityStock
        {
            get { return decQuantityStock; }
            set { decQuantityStock = value; }
        }
        #endregion


        #region Constructor

        public InventoryDetail()
        {
        }
        #endregion


        #region Column Names

        public const String colInventoryDetailID = "InventoryDetailID";
        public const String colInventoryID = "InventoryID";
        public const String colInventoryDate = "InventoryDate";
        public const String colInventoryStoreID = "InventoryStoreID";
        public const String colProductID = "ProductID";
        public const String colCabinetNumber = "CabinetNumber";
        public const String colQuantity = "Quantity";
        public const String colInStockQuantity = "InStockQuantity";
        public const String colNote = "Note";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colDeletedNote = "DeletedNote";
        public const String colIsRequestImei = "IsRequestImei";
        public const String colProductStatusID = "ProductStatusID";
        public const String colProductName = "ProductName";
        public const String colProductBCCSID = "ProductBCCSID";
        public const String colIsBCCS = "IsBCCS";
        public const String colQuantityStock = "QuantityStock";

        #endregion //Column names


    }
}
