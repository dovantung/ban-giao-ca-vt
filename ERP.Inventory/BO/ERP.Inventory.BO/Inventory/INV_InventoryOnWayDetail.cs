
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 05/25/2018 
	/// Chi tiết phiếu kiểm kê hàng đi đường
	/// </summary>	
	public class INV_InventoryOnWayDetail
	{	
	
	
		#region Member Variables

		private string strINVENToRYONWAYDetailID = string.Empty;
		private string strStoreChangeOrderID = string.Empty;
		private string strProductID = string.Empty;
		private string strIMEI = string.Empty;
		private decimal decQuantity;
		private decimal decCheckQuantity;
		private string strNote = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
        private string strProductName = string.Empty;
        private string strOutputVoucherID = string.Empty;
        private string strInputVoucherID = string.Empty;
        private bool bolIsInputStore = false;
       
        #endregion


        #region Properties 

        /// <summary>
        /// INVENToRYONWAYDetailID
        /// 
        /// </summary>
        public string INVENToRYONWAYDetailID
		{
			get { return  strINVENToRYONWAYDetailID; }
			set { strINVENToRYONWAYDetailID = value; }
		}

		/// <summary>
		/// StoreChangeOrderID
		/// Mã phiếu chuyển kho
		/// </summary>
		public string StoreChangeOrderID
		{
			get { return  strStoreChangeOrderID; }
			set { strStoreChangeOrderID = value; }
		}

		/// <summary>
		/// ProductID
		/// Mã sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// IMEI
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// Quantity
		/// Số lượng
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// CheckQuantity
		/// Số lượng kiểm kê
		/// </summary>
		public decimal CheckQuantity
		{
			get { return  decCheckQuantity; }
			set { decCheckQuantity = value; }
		}

		/// <summary>
		/// Note
		/// Ghi chú
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// CreatedUser
		/// Người tạo
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// Ngày tạo
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// Người cập nhật
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// Ngày cập nhật
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }

        public string OutputVoucherID
        {
            get { return strOutputVoucherID; }
            set { strOutputVoucherID = value; }
        }

        public string InputVoucherID
        {
            get { return strInputVoucherID; }
            set { strInputVoucherID = value; }
        }


        public bool IsInputStore
        {
            get { return bolIsInputStore; }
            set { bolIsInputStore = value; }
        }
        #endregion


        #region Constructor

        public INV_InventoryOnWayDetail()
		{
		}
		#endregion


		#region Column Names

		public const String colINVENToRYONWAYDetailID = "INVENToRYONWAYDetailID";
		public const String colStoreChangeOrderID = "StoreChangeOrderID";
		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
		public const String colQuantity = "Quantity";
		public const String colCheckQuantity = "CheckQuantity";
		public const String colNote = "Note";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
        public const String colOutputVoucherID = "OutputVoucherID";
        public const String colInputVoucherID = "InputVoucherID";
        public const String colIsInputStore = "IsInputStore";
        #endregion //Column names


    }
}
