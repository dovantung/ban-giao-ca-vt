
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 05/25/2018 
	/// Danh sách file đính kèm - Phiếu kiểm kê hàng đi đường
	/// </summary>	
	public class INV_InventoryOnWay_AttachMent
	{	
	
	
		#region Member Variables

		private string strINVENToRYONWAYAttachMENTID = string.Empty;
		private string strStoreChangeOrderID = string.Empty;
		private string strAttachMENTName = string.Empty;
		private string strAttachMENTPath = string.Empty;
		private string strDescription = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strFileID = string.Empty;
        private string strOutputVoucherID = string.Empty;
        private string strInputVoucherID = string.Empty;
        private bool bolIsInputStore = false;
        #endregion


        #region Properties 

        /// <summary>
        /// INVENToRYONWAYAttachMENTID
        /// Mã tập tin đính kèm
        /// </summary>
        public string INVENToRYONWAYAttachMENTID
		{
			get { return  strINVENToRYONWAYAttachMENTID; }
			set { strINVENToRYONWAYAttachMENTID = value; }
		}

		/// <summary>
		/// StoreChangeOrderID
		/// Mã yêu cầu chuyển kho
		/// </summary>
		public string StoreChangeOrderID
		{
			get { return  strStoreChangeOrderID; }
			set { strStoreChangeOrderID = value; }
		}

		/// <summary>
		/// AttachMENTName
		/// Tên tập tin đính kèm
		/// </summary>
		public string AttachMENTName
		{
			get { return  strAttachMENTName; }
			set { strAttachMENTName = value; }
		}

		/// <summary>
		/// AttachMENTPath
		/// Đường dẫn tập tin 
		/// </summary>
		public string AttachMENTPath
		{
			get { return  strAttachMENTPath; }
			set { strAttachMENTPath = value; }
		}

		/// <summary>
		/// Description
		/// Diễn giải
		/// </summary>
		public string Description
		{
			get { return  strDescription; }
			set { strDescription = value; }
		}

		/// <summary>
		/// CreatedUser
		/// Người tạo 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// Ngày tạo
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// Đã xóa
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// Người xóa
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// Ngày xóa
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// FileID
		/// Mã tập tin (FMS_FILE)
		/// </summary>
		public string FileID
		{
			get { return  strFileID; }
			set { strFileID = value; }
		}

        public string OutputVoucherID
        {
            get { return strOutputVoucherID; }
            set { strOutputVoucherID = value; }
        }

        public string InputVoucherID
        {
            get { return strInputVoucherID; }
            set { strInputVoucherID = value; }
        }
        public bool IsInputStore
        {
            get { return bolIsInputStore; }
            set { bolIsInputStore = value; }
        }
        #endregion


        #region Constructor

        public INV_InventoryOnWay_AttachMent()
		{
		}
		#endregion


		#region Column Names

		public const String colINVENToRYONWAYAttachMENTID = "INVENToRYONWAYAttachMENTID";
		public const String colStoreChangeOrderID = "StoreChangeOrderID";
		public const String colAttachMENTName = "AttachMENTName";
		public const String colAttachMENTPath = "AttachMENTPath";
		public const String colDescription = "Description";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colFileID = "FileID";
        public const String colOutputVoucherID = "OutputVoucherID";
        public const String colInputVoucherID = "InputVoucherID";
        public const String colIsInputStore = "IsInputStore";
        #endregion //Column names


    }
}
