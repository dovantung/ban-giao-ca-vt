
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
    /// Created by 		: Đặng Minh Hùng 
    /// Created date 	: 12/8/2012 
    /// Khai báo kỳ kiểm kê
    /// </summary>	
    public class InventoryTerm
    {


        #region Member Variables

        private int intInventoryTermID;
        private int intInventoryTypeID = 0;
        private string strInventoryTermName = string.Empty;
        private string strDescription = string.Empty;
        private DateTime? dtmInventoryDate;
        private DateTime? dtmBeginInventoryTime;
        private DateTime? dtmLockDataTime;
        private int intIsNew = -1;
        private int intProductStatusID = -1;

        private int intInventoryTermStatus = 0;
        private int intCreatedStoreID = 0;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;

        public DataTable dtbTerm_Store = null;
        public DataTable dtbTerm_MainGroup = null;
        public DataTable dtbTerm_Brand = null;
        public DataTable dtbTerm_SubGroup = null;
        private bool bolIsCallStockBySchedule = false;
        private DateTime? dtmEndInventoryTime;
        public bool bolInv_AllowBarcode = false;

        
        #endregion


        #region Properties

        /// <summary>
        /// InventoryTermID
        /// Mã kỳ kiểm kê
        /// </summary>
        public int InventoryTermID
        {
            get { return intInventoryTermID; }
            set { intInventoryTermID = value; }
        }

        /// <summary>
        /// InventoryTypeID
        /// Loại kiểm kê; 0: Định kỳ, 1: Bất thường
        /// </summary>
        public int InventoryTypeID
        {
            get { return intInventoryTypeID; }
            set { intInventoryTypeID = value; }
        }

        /// <summary>
        /// InventoryTermName
        /// Tên kỳ kiểm kê
        /// </summary>
        public string InventoryTermName
        {
            get { return strInventoryTermName; }
            set { strInventoryTermName = value; }
        }

        /// <summary>
        /// Description
        /// Mô tả
        /// </summary>
        public string Description
        {
            get { return strDescription; }
            set { strDescription = value; }
        }

        /// <summary>
        /// InventoryDate
        /// Ngày kiểm kê
        /// </summary>
        public DateTime? InventoryDate
        {
            get { return dtmInventoryDate; }
            set { dtmInventoryDate = value; }
        }

        /// <summary>
        /// BeginInventoryTime
        /// Thời gian bắt đầu kiểm kê
        /// </summary>
        public DateTime? BeginInventoryTime
        {
            get { return dtmBeginInventoryTime; }
            set { dtmBeginInventoryTime = value; }
        }

        /// <summary>
        /// LockDataTime
        /// Thời gian chốt dữ liệu tồn kho
        /// </summary>
        public DateTime? LockDataTime
        {
            get { return dtmLockDataTime; }
            set { dtmLockDataTime = value; }
        }

        /// <summary>
        /// IsNew
        /// Mới, cũ; -1: Tất cả, 0: cũ, 1: mới
        /// </summary>
        public int IsNew
        {
            get { return intIsNew; }
            set { intIsNew = value; }
        }

        public int ProductStatusID
        {
            get { return intProductStatusID; }
            set { intProductStatusID = value; }
        }
        /// <summary>
        /// InventoryTermStatus
        /// Trạng thái của kỳ kiểm kê; 1: Khởi tạo, 2: Đang kiểm kê, 3: Hoàn thành
        /// </summary>
        public int InventoryTermStatus
        {
            get { return intInventoryTermStatus; }
            set { intInventoryTermStatus = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// 
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        public bool IsCallStockBySchedule
        {
            get { return bolIsCallStockBySchedule; }
            set { bolIsCallStockBySchedule = value; }
        }
        public DateTime? EndInventoryTime
        {
            get { return dtmEndInventoryTime; }
            set { dtmEndInventoryTime = value; }
        }
        public bool Inv_AllowBarcode
        {
            get { return bolInv_AllowBarcode; }
            set { bolInv_AllowBarcode = value; }
        }

       
        #endregion


        #region Constructor

        public InventoryTerm()
        {
        }
        #endregion


        #region Column Names

        public const String colInventoryTermID = "InventoryTermID";
        public const String colInventoryTypeID = "InventoryTypeID";
        public const String colInventoryTermName = "InventoryTermName";
        public const String colDescription = "Description";
        public const String colInventoryDate = "InventoryDate";
        public const String colBeginInventoryTime = "BeginInventoryTime";
        public const String colLockDataTime = "LockDataTime";
        public const String colIsNew = "IsNew";
        public const String colInventoryTermStatus = "InventoryTermStatus";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colProductStatusID = "ProductStatusID";
        public const String colIsCallStockBySchedule = "IsCallStockBySchedule";
        public const String colEndInventoryTime = "EndInventoryTime";
        public const String colInv_AllowBarcode = "Inv_AllowBarcode";

        #endregion //Column names


    }
}
