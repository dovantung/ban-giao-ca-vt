
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 05/25/2018 
	/// Phiếu kiểm kê hàng đi đường
	/// </summary>	
	public class INV_InventoryOnWay
	{	
	
	
		#region Member Variables

		private string strStoreChangeOrderID = string.Empty;
		private string strOutputVoucherID = string.Empty;
		private string strInputVoucherID = string.Empty;
		private int intFromStoreID = 0;
		private int intToStoreID = 0;
		private DateTime? dtmOutputDate;
		private decimal decQuantity;
		private decimal decTotalAmount;
		private string strOutputUser = string.Empty;
		private bool bolIsReviewed = false;
		private string strCreatedUser = string.Empty;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedDate = string.Empty;
        private bool bolIsSelected = false;
        private string strFromStoreName = string.Empty;
        private string strToStoreName = string.Empty;
        private string strOutputUserFullName = string.Empty;

        private List<INV_InventoryOnWayDetail> lstINV_InventoryOnWayDetail = null;
        private List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent = null;
        private List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent_Del = null;

		#endregion


		#region Properties 

		/// <summary>
		/// StoreChangeOrderID
		/// Mã phiếu chuyển kho
		/// </summary>
		public string StoreChangeOrderID
		{
			get { return  strStoreChangeOrderID; }
			set { strStoreChangeOrderID = value; }
		}

		/// <summary>
		/// OutputVoucherID
		/// Mã phiếu xuất
		/// </summary>
		public string OutputVoucherID
		{
			get { return  strOutputVoucherID; }
			set { strOutputVoucherID = value; }
		}

		/// <summary>
		/// InputVoucherID
		/// Mã phiếu nhập
		/// </summary>
		public string InputVoucherID
		{
			get { return  strInputVoucherID; }
			set { strInputVoucherID = value; }
		}

		/// <summary>
		/// FromStoreID
		/// Kho xuất
		/// </summary>
		public int FromStoreID
		{
			get { return  intFromStoreID; }
			set { intFromStoreID = value; }
		}

		/// <summary>
		/// ToStoreID
		/// Kho nhập
		/// </summary>
		public int ToStoreID
		{
			get { return  intToStoreID; }
			set { intToStoreID = value; }
		}

		/// <summary>
		/// OutputDate
		/// Ngày xuất
		/// </summary>
		public DateTime? OutputDate
		{
			get { return  dtmOutputDate; }
			set { dtmOutputDate = value; }
		}

		/// <summary>
		/// Quantity
		/// Tổng số lượng
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// TotalAmount
		/// Tổng giá trị
		/// </summary>
		public decimal TotalAmount
		{
			get { return  decTotalAmount; }
			set { decTotalAmount = value; }
		}

		/// <summary>
		/// OutputUser
		/// Người xuất
		/// </summary>
		public string OutputUser
		{
			get { return  strOutputUser; }
			set { strOutputUser = value; }
		}

		/// <summary>
		/// IsReviewed
		/// Đã xác nhận
		/// </summary>
		public bool IsReviewed
		{
			get { return  bolIsReviewed; }
			set { bolIsReviewed = value; }
		}

		/// <summary>
		/// CreatedUser
		/// Người tạo
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// Người update
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// Ngày tạo
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// Ngày cập nhật
		/// </summary>
		public string UpdatedDate
		{
			get { return  strUpdatedDate; }
			set { strUpdatedDate = value; }
		}

        public bool IsSelected
        {
            get { return bolIsSelected; }
            set { bolIsSelected = value; }
        }

        public string FromStoreName
        {
            get { return strFromStoreName; }
            set { strFromStoreName = value; }
        }

        public string ToStoreName
        {
            get { return strToStoreName; }
            set { strToStoreName = value; }
        }

        public string OutputUserFullName
        {
            get { return strOutputUserFullName; }
            set { strOutputUserFullName = value; }
        }

        public List<INV_InventoryOnWayDetail> InventoryOnWayDetailList
        {
            get { return lstINV_InventoryOnWayDetail; }
            set { lstINV_InventoryOnWayDetail = value; }
        }

        public List<INV_InventoryOnWay_AttachMent> InventoryOnWay_AttachMentList
        {
            get { return lstINV_InventoryOnWay_AttachMent; }
            set { lstINV_InventoryOnWay_AttachMent = value; }
        }

        public List<INV_InventoryOnWay_AttachMent> InventoryOnWay_AttachMent_DelList
        {
            get { return lstINV_InventoryOnWay_AttachMent_Del; }
            set { lstINV_InventoryOnWay_AttachMent_Del = value; }
        }

        public bool IsInputStore { get; set; }
        #endregion


        #region Constructor

        public INV_InventoryOnWay()
		{
		}
		#endregion


		#region Column Names

		public const String colStoreChangeOrderID = "StoreChangeOrderID";
		public const String colOutputVoucherID = "OutputVoucherID";
		public const String colInputVoucherID = "InputVoucherID";
		public const String colFromStoreID = "FromStoreID";
		public const String colToStoreID = "ToStoreID";
		public const String colOutputDate = "OutputDate";
		public const String colQuantity = "Quantity";
		public const String colTotalAmount = "TotalAmount";
		public const String colOutputUser = "OutputUser";
		public const String colIsReviewed = "IsReviewed";
		public const String colCreatedUser = "CreatedUser";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedDate = "UpdatedDate";

		#endregion //Column names

		
	}
}
