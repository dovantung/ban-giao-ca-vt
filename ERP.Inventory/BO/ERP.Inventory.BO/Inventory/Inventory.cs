
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Inventory
{
    /// <summary>
    /// Created by 		: Đặng Minh Hùng 
    /// Created date 	: 12/26/2012 
    /// Phiếu kiểm kê
    /// </summary>	
    public class Inventory
    {


        #region Member Variables

        private string strInventoryID = string.Empty;
        private int intInventoryTermID = 0;
        private int intInventoryTypeID = 0;
        private bool bolIsAutoReview = false;
        private int intInventoryStoreID = 0;
        private string strMainGroupIDList = string.Empty;
        private string strSubGroupIDList = string.Empty;

        private int intMainGroupID = -1;
        private int intSubGroupID = -1;//string.Empty;
        private bool bolIsBCCS = false;
        private bool bolIsNew = false;
        private bool bolIsConnected = false;
        private bool bolIsParent = false;
        private DateTime? dtmInventoryDate;
        private string strInventoryUser = string.Empty;
        private DateTime? dtmBeginInventoryTime;
        private DateTime? dtmEndInventoryTime;
        private string strContent = string.Empty;
        private bool bolIsReviewed = false;
        private bool bolIsUpdateUnEvent = false;
        private string strUpdateUnEventUser = string.Empty;
        private DateTime? dtmUpdateUnEventTime;
        private bool bolIsLock = false;
        private string strLockUser = string.Empty;
        private DateTime? dtmLockTime;
        private bool bolIsExplainUnEvent = false;
        private string strExplainUnEventUser = string.Empty;
        private DateTime? dtmExplainUnEventTime;
        private bool bolIsAdjustUnEvent = false;
        private string strAdjustUnEventUser = string.Empty;
        private DateTime? dtmAdjustUnEventTime;
        private bool bolIsReviewUnEvent = false;
        private string strReviewUnEventUser = string.Empty;
        private DateTime? dtmReviewUnEventTime;
        private int intCreatedStoreID = 0;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private string strDeletedNote = string.Empty;

        private int intProductStatusID = -1;
        private int intCurrentReviewLevelID = 0;

        private int intReviewStatusID = -1;
        private string strReviewStatusName = string.Empty;
        private bool bolIsExplainAccess = false;
        private string strExplainAccessUser = string.Empty;
        private DateTime? dtpExplainAccessDate;
        #endregion


        #region Properties
        public int ProductStatusID
        {
            get { return intProductStatusID; }
            set { intProductStatusID = value; }
        }
        /// <summary>
        /// CurrentReviewLevel
        /// Mức duyệt hiện tại phiếu kiểm kê
        /// </summary>
        public int CurrentReviewLevelID
        {
            get { return intCurrentReviewLevelID; }
            set { intCurrentReviewLevelID = value; }
        }

        /// <summary>
        /// InventoryID
        /// Mã phiếu kiểm kê
        /// </summary>
        public string InventoryID
        {
            get { return strInventoryID; }
            set { strInventoryID = value; }
        }

        /// <summary>
        /// InventoryTermID
        /// Mã kỳ kiểm kê
        /// </summary>
        public int InventoryTermID
        {
            get { return intInventoryTermID; }
            set { intInventoryTermID = value; }
        }

        public int InventoryTypeID
        {
            get { return intInventoryTypeID; }
            set { intInventoryTypeID = value; }
        }

        public bool IsAutoReview
        {
            get { return bolIsAutoReview; }
            set { bolIsAutoReview = value; }
        }

        public bool IsBCCS
        {
            get { return bolIsBCCS; }
            set { bolIsBCCS = value; }
        }

        /// <summary>
        /// InventoryStoreID
        /// Kho kiểm kê
        /// </summary>
        public int InventoryStoreID
        {
            get { return intInventoryStoreID; }
            set { intInventoryStoreID = value; }
        }

        /// <summary>
        /// MainGroupID
        /// Mã ngành hàng
        /// </summary>
        /// 

        public int MainGroupID
        {
            get { return intMainGroupID; }
            set { intMainGroupID = value; }
        }

        public string MainGroupIDList
        {
            get { return strMainGroupIDList; }
            set { strMainGroupIDList = value; }
        }

        /// <summary>
        /// IsNew
        /// Mới/cũ
        /// </summary>
        public bool IsNew
        {
            get { return bolIsNew; }
            set { bolIsNew = value; }
        }

        public bool IsConnected
        {
            get { return bolIsConnected; }
            set { bolIsConnected = value; }
        }

        public bool IsParent
        {
            get { return bolIsParent; }
            set { bolIsParent = value; }
        }
        /// <summary>
        /// InventoryDate
        /// Ngày kiểm kê
        /// </summary>
        public DateTime? InventoryDate
        {
            get { return dtmInventoryDate; }
            set { dtmInventoryDate = value; }
        }

        /// <summary>
        /// InventoryUser
        /// Nhân viên kiểm kê
        /// </summary>
        public string InventoryUser
        {
            get { return strInventoryUser; }
            set { strInventoryUser = value; }
        }

        /// <summary>
        /// BeginInventoryTime
        /// Thời gian bắt đầu kiểm kê
        /// </summary>
        public DateTime? BeginInventoryTime
        {
            get { return dtmBeginInventoryTime; }
            set { dtmBeginInventoryTime = value; }
        }

        /// <summary>
        /// EndInventoryTime
        /// Thời gian kết thúc kiểm kê
        /// </summary>
        public DateTime? EndInventoryTime
        {
            get { return dtmEndInventoryTime; }
            set { dtmEndInventoryTime = value; }
        }

        /// <summary>
        /// Content
        /// Nội dung
        /// </summary>
        public string Content
        {
            get { return strContent; }
            set { strContent = value; }
        }

        /// <summary>
        /// IsReviewed
        /// Đã duyệt
        /// </summary>
        public bool IsReviewed
        {
            get { return bolIsReviewed; }
            set { bolIsReviewed = value; }
        }

        /// <summary>
        /// IsUpdateUnEvent
        /// Đã cập nhật chênh lệch kiểm kê
        /// </summary>
        public bool IsUpdateUnEvent
        {
            get { return bolIsUpdateUnEvent; }
            set { bolIsUpdateUnEvent = value; }
        }

        /// <summary>
        /// UpdateUnEventUser
        /// Người cập nhật chênh lệch kiểm kê
        /// </summary>
        public string UpdateUnEventUser
        {
            get { return strUpdateUnEventUser; }
            set { strUpdateUnEventUser = value; }
        }

        /// <summary>
        /// UpdateUnEventTime
        /// Ngày cập nhật chênh lệch kiểm kê
        /// </summary>
        public DateTime? UpdateUnEventTime
        {
            get { return dtmUpdateUnEventTime; }
            set { dtmUpdateUnEventTime = value; }
        }

        /// <summary>
        /// IsLock
        /// Đã khóa dữ liệu kiểm kê
        /// </summary>
        public bool IsLock
        {
            get { return bolIsLock; }
            set { bolIsLock = value; }
        }

        /// <summary>
        /// LockUser
        /// Người khóa
        /// </summary>
        public string LockUser
        {
            get { return strLockUser; }
            set { strLockUser = value; }
        }

        /// <summary>
        /// LockTime
        /// Thời gian khóa
        /// </summary>
        public DateTime? LockTime
        {
            get { return dtmLockTime; }
            set { dtmLockTime = value; }
        }

        /// <summary>
        /// IsExplainUnEvent
        /// Đã giải trình chênh lệch kiểm kê
        /// </summary>
        public bool IsExplainUnEvent
        {
            get { return bolIsExplainUnEvent; }
            set { bolIsExplainUnEvent = value; }
        }

        /// <summary>
        /// ExplainUnEventUser
        /// Người giải trình
        /// </summary>
        public string ExplainUnEventUser
        {
            get { return strExplainUnEventUser; }
            set { strExplainUnEventUser = value; }
        }

        /// <summary>
        /// ExplainUnEventTime
        /// Thời gian giải trình
        /// </summary>
        public DateTime? ExplainUnEventTime
        {
            get { return dtmExplainUnEventTime; }
            set { dtmExplainUnEventTime = value; }
        }

        /// <summary>
        /// IsAdjustUnEvent
        /// Đã điều chỉnh chênh lệch kiểm kê
        /// </summary>
        public bool IsAdjustUnEvent
        {
            get { return bolIsAdjustUnEvent; }
            set { bolIsAdjustUnEvent = value; }
        }

        /// <summary>
        /// AdjustUnEventUser
        /// Người điều chỉnh
        /// </summary>
        public string AdjustUnEventUser
        {
            get { return strAdjustUnEventUser; }
            set { strAdjustUnEventUser = value; }
        }

        /// <summary>
        /// AdjustUnEventTime
        /// Thời gian điều chỉnh
        /// </summary>
        public DateTime? AdjustUnEventTime
        {
            get { return dtmAdjustUnEventTime; }
            set { dtmAdjustUnEventTime = value; }
        }

        /// <summary>
        /// IsReviewUnEvent
        /// Đã duyệt chênh lệch kiểm kê
        /// </summary>
        public bool IsReviewUnEvent
        {
            get { return bolIsReviewUnEvent; }
            set { bolIsReviewUnEvent = value; }
        }

        /// <summary>
        /// ReviewUnEventUser
        /// Người duyệt
        /// </summary>
        public string ReviewUnEventUser
        {
            get { return strReviewUnEventUser; }
            set { strReviewUnEventUser = value; }
        }

        /// <summary>
        /// ReviewUnEventTime
        /// Thời gian duyệt
        /// </summary>
        public DateTime? ReviewUnEventTime
        {
            get { return dtmReviewUnEventTime; }
            set { dtmReviewUnEventTime = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// Kho tạo phiếu
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        /// <summary>
        /// DeletedNote
        /// 
        /// </summary>
        public string DeletedNote
        {
            get { return strDeletedNote; }
            set { strDeletedNote = value; }
        }

        /// <summary>
        /// SubGroupID
        /// Mã nhóm hàng
        /// </summary>
        /// 
        public int SubGroupID
        {
            get { return intSubGroupID; }
            set { intSubGroupID = value; }
        }

        public string SubGroupIDList
        {
            get { return strSubGroupIDList; }
            set { strSubGroupIDList = value; }
        }

        public int ReviewStatusID
        {
            get { return intReviewStatusID; }
            set { intReviewStatusID = value; }
        }

        public string ReviewStatusName
        {
            get { return strReviewStatusName; }
            set { strReviewStatusName = value; }
        }

        public bool IsExplainAccess
        {
            get { return bolIsExplainAccess; }
            set { bolIsExplainAccess = value; }
        }

        public string ExplainAccessUser
        {
            get { return strExplainAccessUser; }
            set { strExplainAccessUser = value; }
        }

        public DateTime? ExplainAccessDate
        {
            get { return dtpExplainAccessDate; }
            set { dtpExplainAccessDate = value; }
        }



        #endregion


        #region Constructor

        public Inventory()
        {
        }
        #endregion


        #region Column Names

        public const String colInventoryID = "InventoryID";
        public const String colInventoryTermID = "InventoryTermID";
        public const String colInventoryStoreID = "InventoryStoreID";
        public const String colMainGroupID = "MainGroupID";
        public const String colSubGroupID = "SubGroupID";
        public const String colIsNew = "IsNew";
        public const String colInventoryDate = "InventoryDate";
        public const String colInventoryUser = "InventoryUser";
        public const String colBeginInventoryTime = "BeginInventoryTime";
        public const String colEndInventoryTime = "EndInventoryTime";
        public const String colContent = "Content";
        public const String colIsReviewed = "IsReviewed";
        public const String colIsUpdateUnEvent = "IsUpdateUnEvent";
        public const String colUpdateUnEventUser = "UpdateUnEventUser";
        public const String colUpdateUnEventTime = "UpdateUnEventTime";
        public const String colIsLock = "IsLock";
        public const String colLockUser = "LockUser";
        public const String colLockTime = "LockTime";
        public const String colIsExplainUnEvent = "IsExplainUnEvent";
        public const String colExplainUnEventUser = "ExplainUnEventUser";
        public const String colExplainUnEventTime = "ExplainUnEventTime";
        public const String colIsAdjustUnEvent = "IsAdjustUnEvent";
        public const String colAdjustUnEventUser = "AdjustUnEventUser";
        public const String colAdjustUnEventTime = "AdjustUnEventTime";
        public const String colIsReviewUnEvent = "IsReviewUnEvent";
        public const String colReviewUnEventUser = "ReviewUnEventUser";
        public const String colReviewUnEventTime = "ReviewUnEventTime";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colDeletedNote = "DeletedNote";
        public const String colProductStatusID = "ProductStatusID";
        public const String colIsConnected = "ISCONNECTED";
        public const String colIsParent = "ISPARENT";
        public const String colReviewStatusID = "REVIEWSTATUSID";
        public const String colReviewStatusName = "REVIEWSTATUSNAME";

        public const String colIsExplainAccess = "ISEXPLAINACCESS";
        public const String colExplainAccessUser = "EXPLAINACCESSUSER";
        public const String colExplainAccessDate = "EXPLAINACCESSDATE";
        public const String colIsBCCS = "ISBCCS";
        
        #endregion //Column names


    }
}
