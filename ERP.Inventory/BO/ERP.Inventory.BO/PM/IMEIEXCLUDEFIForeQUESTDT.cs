
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.PM
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 06/20/2018 
	/// Chi tiết yêu cầu IMEI không FIFO
	/// </summary>	
	public class IMEIExcludeFIFORequestDT
	{	
	
	
		#region Member Variables

		private string strIMEIExcludeFIFORequestDTID = string.Empty;
		private string strIMEIExcludeFIFORequestID = string.Empty;
		private string strProductID = string.Empty;
		private string strIMEI = string.Empty;
        private int intInStockStatusID = 0;
		private DateTime? dtmFirstInputDate;
		private string strNote = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private string strVoucherConcern = string.Empty;
        private string strProductName = string.Empty;

		#endregion


		#region Properties 

        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }

        public string VoucherConcern
        {
            get { return strVoucherConcern; }
            set { strVoucherConcern = value; }
        }

		/// <summary>
		/// IMEIExcludeFIFORequestDTID
		/// 
		/// </summary>
		public string IMEIExcludeFIFORequestDTID
		{
			get { return  strIMEIExcludeFIFORequestDTID; }
			set { strIMEIExcludeFIFORequestDTID = value; }
		}

		/// <summary>
		/// IMEIExcludeFIFORequestID
		/// 
		/// </summary>
		public string IMEIExcludeFIFORequestID
		{
			get { return  strIMEIExcludeFIFORequestID; }
			set { strIMEIExcludeFIFORequestID = value; }
		}

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// 
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// Status
		/// 
		/// </summary>
        public int InStockStatusID
		{
            get { return intInStockStatusID; }
            set { intInStockStatusID = value; }
		}

		/// <summary>
		/// FirstInputDate
		/// 
		/// </summary>
		public DateTime? FirstInputDate
		{
			get { return  dtmFirstInputDate; }
			set { dtmFirstInputDate = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public IMEIExcludeFIFORequestDT()
		{
		}
		#endregion


		#region Column Names

		public const String colIMEIExcludeFIFORequestDTID = "IMEIExcludeFIFORequestDTID";
		public const String colIMEIExcludeFIFORequestID = "IMEIExcludeFIFORequestID";
		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
        public const String colInStockStatusID = "InStockStatusID";
		public const String colFirstInputDate = "FirstInputDate";
		public const String colNote = "Note";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colVoucherConcern = "VoucherConcern";

		#endregion //Column names

		
	}
}
