
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.PM
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 06/20/2018 
	/// Yêu cầu IMEI không FIFO
	/// </summary>	
	public class IMEIExcludeFIFORequest
	{	
	
	
		#region Member Variables

		private string strIMEIExcludeFIFORequestID = string.Empty;
		private string strNote = string.Empty;
		private string strRequestUser = string.Empty;
		private int intRequestStoreID = 0;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strReviewedNote = string.Empty;
		private string strReviewedUser = string.Empty;
		private DateTime? dtmReviewedDate;
        private int intReviewedStatus = -1;
		private string strDeletedReason = string.Empty;
        private int intIMEIExcludeFIFORequestTypeID = 0;
        private List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDTList = null;
        private List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT_Del_List = null;

		#endregion


		#region Properties 

        public List<IMEIExcludeFIFORequestDT> IMEIExcludeFIFORequestDTList
        {
            get { return lstIMEIExcludeFIFORequestDTList; }
            set { lstIMEIExcludeFIFORequestDTList = value; }
        }

        public List<IMEIExcludeFIFORequestDT> IMEIExcludeFIFORequestDT_Del_List
        {
            get { return lstIMEIExcludeFIFORequestDT_Del_List; }
            set { lstIMEIExcludeFIFORequestDT_Del_List = value; }
        }

        public int IMEIExcludeFIFORequestTypeID
        {
            get { return intIMEIExcludeFIFORequestTypeID; }
            set { intIMEIExcludeFIFORequestTypeID = value; }
        }

		/// <summary>
		/// IMEIExcludeFIFORequestID
		/// 
		/// </summary>
		public string IMEIExcludeFIFORequestID
		{
			get { return  strIMEIExcludeFIFORequestID; }
			set { strIMEIExcludeFIFORequestID = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// RequestUser
		/// 
		/// </summary>
		public string RequestUser
		{
			get { return  strRequestUser; }
			set { strRequestUser = value; }
		}

		/// <summary>
		/// RequestStoreID
		/// 
		/// </summary>
		public int RequestStoreID
		{
			get { return  intRequestStoreID; }
			set { intRequestStoreID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// ReviewedNote
		/// Nội dung duyệt cuối cùng
		/// </summary>
		public string ReviewedNote
		{
			get { return  strReviewedNote; }
			set { strReviewedNote = value; }
		}

		/// <summary>
		/// ReviewedUser
		/// nhân viên duyệt cuối cùng
		/// </summary>
		public string ReviewedUser
		{
			get { return  strReviewedUser; }
			set { strReviewedUser = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// Ngày duyệt cuối cùng
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// ReviewedStatus
		/// trạng thái duyệt cuối cùng:(-1: Chưa duyệt; 0: TỪ CHỐI; 1 ĐỒNG Ý)
		/// </summary>
		public int ReviewedStatus
		{
            get { return intReviewedStatus; }
            set { intReviewedStatus = value; }
		}

		/// <summary>
		/// DeletedReason
		/// Lý do hủy
		/// </summary>
		public string DeletedReason
		{
			get { return  strDeletedReason; }
			set { strDeletedReason = value; }
		}


		#endregion			
		
		
		#region Constructor

        public IMEIExcludeFIFORequest()
		{
		}
		#endregion


		#region Column Names

		public const String colIMEIExcludeFIFORequestID = "IMEIExcludeFIFORequestID";
		public const String colNote = "Note";
		public const String colRequestUser = "RequestUser";
		public const String colRequestStoreID = "RequestStoreID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colReviewedNote = "ReviewedNote";
		public const String colReviewedUser = "ReviewedUser";
		public const String colReviewedDate = "ReviewedDate";
		public const String colReviewedStatus = "ReviewedStatus";
		public const String colDeletedReason = "DeletedReason";
        public const String colIMEIExcludeFIFORequestTypeID = "IMEIExcludeFIFORequestTypeID";

		#endregion //Column names

		
	}
}
