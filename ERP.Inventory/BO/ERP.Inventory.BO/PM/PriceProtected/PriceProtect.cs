﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.PM.PriceProtected
{
    public class PriceProtect
    {
        #region Member Variables

        private string strPriceProtectID = string.Empty;
        private DateTime? dtmPriceProtectDate = null;

        private int intCustomerID = 0;
        private string strCustomerName = string.Empty;
        private string strCustomerAddress = string.Empty;
        private string strPriceProtectNO = string.Empty;
        private DateTime? dtmStockDate = null;
        private string strNote = string.Empty;
        private int intStatus = 0;
        private bool bolIsInvoice = false;
        private string strVatPinVoiceID = string.Empty;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private int intIsRoadTrip = 0;
        private int? intInVoiceTransTypeID = null;
        private int intPriceProtectTypeID = 0;
        private DateTime? dteVoucherDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dteUpdatedDate;
        private int intCreatedStoreID = 0;
        private int intStoreID = 0;
        private int intCompanyID = 0;
        private bool bolIsHasInternalInvoice = false;
        private bool bolIsHasAdjustVoucher = false;
        private bool bolIsSupplierCal = false;
        private DateTime? dteInputFromDate;
        private DateTime? dteInputToDate;
        private DateTime? dtePostDate;

        private bool bolIsHasInternalInvoice_Postdate = false;
        private bool bolIsHasAdjustVoucher_Postdate = false;
        private bool bolIsHasAllocateByPostdate = false;
        private bool bolIsApproved = false;
        private string strApprovedUser = string.Empty;
        private DateTime? dteApprovedDate;


        private List<PriceProtect_Attachment> lstPriceProtect_Attachment = new List<PriceProtect_Attachment>();
        private List<PriceProtect_Customer> lstPriceProtect_Customer = new List<PriceProtect_Customer>();
        private List<PriceProtectDetail> lstPriceProtectDetail = new List<PriceProtectDetail>();
        private List<PriceProtectDetail> lstPriceProtectDetail2 = new List<PriceProtectDetail>();
        private List<PriceProtectDetail_IMEI> lstPriceProtectDetail_IMEI = new List<PriceProtectDetail_IMEI>();
        #endregion

        public List<PriceProtect_Attachment> ListPriceProtect_Attachment
        {
            get { return lstPriceProtect_Attachment; }
            set { lstPriceProtect_Attachment = value; }
        }

        public List<PriceProtect_Customer> ListPriceProtect_Customer
        {
            get { return lstPriceProtect_Customer; }
            set { lstPriceProtect_Customer = value; }
        }

        public List<PriceProtectDetail_IMEI> LstPriceProtectDetail_IMEI
        {
            get { return lstPriceProtectDetail_IMEI; }
            set { lstPriceProtectDetail_IMEI = value; }
        }
        public List<PriceProtectDetail> LstPriceProtectDetail
        {
            get { return lstPriceProtectDetail; }
            set { lstPriceProtectDetail = value; }
        }

        public int PriceProtectTypeID
        {
            get { return intPriceProtectTypeID; }
            set { intPriceProtectTypeID = value; }
        }
        public int? InVoiceTransTypeID
        {
            get { return intInVoiceTransTypeID; }
            set { intInVoiceTransTypeID = value; }
        }
        public int IsRoadTrip
        {
            get { return intIsRoadTrip; }
            set { intIsRoadTrip = value; }
        }

        public string VatPinVoiceID
        {
            get { return strVatPinVoiceID; }
            set { strVatPinVoiceID = value; }
        }
        public bool IsInvoice
        {
            get { return bolIsInvoice; }
            set { bolIsInvoice = value; }
        }
        public int Status
        {
            get { return intStatus; }
            set { intStatus = value; }
        }


        public string CustomerAddress
        {
            get { return strCustomerAddress; }
            set { strCustomerAddress = value; }
        }
        public string PriceProtectNO
        {
            get { return strPriceProtectNO; }
            set { strPriceProtectNO = value; }
        }
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }
        public string Note
        {
            get { return strNote; }
            set { strNote = value; }
        }
        public DateTime? StockDate
        {
            get { return dtmStockDate; }
            set { dtmStockDate = value; }
        }
        public string CustomerName
        {
            get { return strCustomerName; }
            set { strCustomerName = value; }
        }

        public int CustomerID
        {
            get { return intCustomerID; }
            set { intCustomerID = value; }
        }
        public DateTime? PriceProtectDate
        {
            get { return dtmPriceProtectDate; }
            set { dtmPriceProtectDate = value; }
        }
        public string PriceProtectID
        {
            get { return strPriceProtectID; }
            set { strPriceProtectID = value; }
        }

        /// <summary>
        /// VoucherDate
        /// Ngày biên bản
        /// </summary>
        public DateTime? VoucherDate
        {
            get { return dteVoucherDate; }
            set { dteVoucherDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        ///
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// CreatedStoreID
        ///
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// CompanyID
        ///
        /// </summary>
        public int CompanyID
        {
            get { return intCompanyID; }
            set { intCompanyID = value; }
        }

        /// <summary>
        /// UpdatedDate
        ///
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dteUpdatedDate; }
            set { dteUpdatedDate = value; }
        }
        /// <summary>
        /// CreatedStoreID
        ///
        /// </summary>
        public int StoreID
        {
            get { return intStoreID; }
            set { intStoreID = value; }
        }

        /// <summary>
        /// IsHasInternalInvoice
        /// Đã tạo hóa đơn giảm trừ nội bộ
        /// </summary>
        public bool IsHasInternalInvoice
        {
            get { return bolIsHasInternalInvoice; }
            set { bolIsHasInternalInvoice = value; }
        }

        /// <summary>
        /// IsHasAdjustVoucher
        ///
        /// </summary>
        public bool IsHasAdjustVoucher
        {
            get { return bolIsHasAdjustVoucher; }
            set { bolIsHasAdjustVoucher = value; }
        }

        /// <summary>
        /// IsSupplierCal
        ///
        /// </summary>
        public bool IsSupplierCal
        {
            get { return bolIsSupplierCal; }
            set { bolIsSupplierCal = value; }
        }

        /// <summary>
        /// InputFromDate
        ///
        /// </summary>
        public DateTime? InputFromDate
        {
            get { return dteInputFromDate; }
            set { dteInputFromDate = value; }
        }

        /// <summary>
        /// InputToDate
        ///
        /// </summary>
        public DateTime? InputToDate
        {
            get { return dteInputToDate; }
            set { dteInputToDate = value; }
        }

        public DateTime? PostDate
        {
            get { return dtePostDate; }
            set { dtePostDate = value; }
        }

        public bool IsHasInternalInvoice_Postdate
        {
            get { return bolIsHasInternalInvoice_Postdate; }
            set { bolIsHasInternalInvoice_Postdate = value; }
        }

        public bool IsHasAdjustVoucher_Postdate
        {
            get { return bolIsHasAdjustVoucher_Postdate; }
            set { bolIsHasAdjustVoucher_Postdate = value; }
        }

        public bool IsHasAllocateByPostdate
        {
            get { return bolIsHasAllocateByPostdate; }
            set { bolIsHasAllocateByPostdate = value; }
        }

        public bool IsApproved
        {
            get { return bolIsApproved; }
            set { bolIsApproved = value; }
        }

        public string ApprovedUser
        {
            get { return strApprovedUser; }
            set { strApprovedUser = value; }
        }

        public DateTime? ApprovedDate
        {
            get { return dteApprovedDate; }
            set { dteApprovedDate = value; }
        }


        #region Column Names
        public const String colVoucherDate = "VoucherDate";
        public const String colPriceProtectID = "PRICEPROTECTID";
        public const String colPriceProtectDate = "PricePROTECTDate";
        public const String colCustomerID = "CustomerID";
        public const String colCustomerName = "CustomerName";
        public const String colCustomerAddress = "CustomerAddress";
        public const String colPriceProtectNO = "PricePROTECTNO";
        public const String colStockDate = "StockDate";
        public const String colNote = "Note";
        public const String colStatus = "Status";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colIsInvoice = "ISInvoice";
        public const String colVATPINVOICEID = "VATPINVOICEID";
        public const String colIsRoadTrip = "ISROADTRIP";
        public const String colInVoiceTransTypeID = "INVOICETRANSTYPEID";
        public const String colPriceProtectTypeID = "PRICEPROTECTTYPEID";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colStoreID = "StoreID";
        public const String colCompanyID = "CompanyID";
        public const String colIsHasInternalInvoice = "IsHasInternalInvoice";
        public const String colIsHasAdjustVoucher = "IsHasAdjustVoucher";
        public const String colIsSupplierCal = "IsSupplierCal";
        public const String colInputFromDate = "InputFromDate";
        public const String colInputToDate = "InputToDate";
        public const String colPostDate = "PostDate";
        public const String colIsHasInternalInvoice_PostDate = "IsHasInternalInvoice_PostDate";
        public const String colIsHasAdjustVoucher_PostDate = "IsHasAdjustVoucher_PostDate";
        public const String colIsHasAllocateByPostdate = "ISHASALLOCATEBYPOSTDATE";
        public const String colIsApproved = "IsApproved";
        public const String colApprovedUser = "ApprovedUser";
        public const String colApprovedDate = "ApprovedDate";


        #endregion //Column names
    }

}
