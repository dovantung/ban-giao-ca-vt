﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.PM.PriceProtected
{
    public class PriceProtectDetail
    {
        #region Member Variables
        private string strPriceProtectDetailID = string.Empty;
        private string strPriceProtectID = string.Empty;
        private string strProductID = string.Empty;
        private string strProductName = string.Empty;
        private int? intQuantity = null;
        private int? intInStockQuantity = 0;

        private decimal? decPrice = null;
        private decimal? decVat = null;
        private decimal? decVatAmount = null;
        private decimal? decTotalAmount = null;
        private decimal? decPriceNoVat = null;
        private decimal? decTotalAmountNoVat = null;

        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;

        private string strInputVoucherID = string.Empty;
        private string strInVoiceID = string.Empty;
        private DateTime? dtmInVoiceDate = null;
        private List<PriceProtectDetail_Allocate> lstPriceProtectDetail_Allocate = new List<PriceProtectDetail_Allocate>();
        private List<PriceProtectDetail_Allocate> lstPriceProtectDetail_Allocate2 = new List<PriceProtectDetail_Allocate>();
        private List<PriceProtectDetail_IMEI> lstPriceProtectDetail_Imei = new List<PriceProtectDetail_IMEI>();
        private List<PriceProtectDetail_IMEI> lstPriceProtectDetail_Imei1 = new List<PriceProtectDetail_IMEI>();

        private int? intQuantityNoInput = null;
        private int? intQuantityBuy = null;
        private int? intQuantityStock = null;
        private int? intQuantityInStockNonIsCenter = null;
        private int? intQuantityBuyNonIsCenter = null;
        private int? intQuantityOtherIncome = 0;

        private decimal? decTotalAmountStock = 0;
        private decimal? decTotalAmountBuy = 0;
        private decimal? decTotalAmountStockNonIsCenter = 0; // Tong tien tồn ở kho tỉnh
        private decimal? decTotalAmountBuyNonIsCenter = 0; // tong tien ban o kho tỉnh
        private decimal? decTotalAmountOtherIncome = 0;

        private int intInVoiceTransTypeID = 0;
        private int intVatTypeID = -1;
        private decimal decVatPercent = 0;
        private string strVatPInvoiceDTID = string.Empty;
        private bool bolIsRequestIMEI = false;

        #region 08/02/2020 Đăng bổ sung
        private decimal? decPriceVoucher = null;
        private decimal? decPriceNoVatVoucher = null;
        private decimal? decVatAmountVoucher = null;
        private decimal? decTotalAmountVoucher = null;
        private decimal? decTotalAmountNoVatVoucher = null;

        private int? intQuantityNoInputVoucher = null;
        private int? intQuantityBuyVoucher = null;
        private int? intQuantityStockVoucher = null;
        private int? intQuantityInStockNonIsCenterVoucher = null;
        private int? intQuantityBuyNonIsCenterVoucher = null;
        private int? intQuantityOtherIncomeVoucher = 0;

        private decimal? decTotalAmountStockVoucher = 0;
        private decimal? decTotalAmountBuyVoucher = 0;
        private decimal? decTotalAmountStockNonIsCenterVoucher = 0; // Tong tien tồn ở kho tỉnh
        private decimal? decTotalAmountBuyNonIsCenterVoucher = 0; // tong tien ban o kho tỉnh
        private decimal? decTotalAmountOtherIncomeVoucher = 0;


        private int? intQuantityDifference = null;
        private decimal? decPriceDifference = null;
        private decimal? decPriceNoVatDifference = null;
        private decimal? decVatAmountDifference = null;
        private decimal? decTotalAmountDifference = null;
        private decimal? decTotalAmountNoVatDifference = null;


        private int? intQuantityNoInputDifference= null;
        private int? intQuantityBuyDifference= null;
        private int? intQuantityStockDifference= null;
        private int? intQuantityInStockNonIsCenterDifference= null;
        private int? intQuantityBuyNonIsCenterDifference= null;
        private int? intQuantityOtherIncomeDifference= 0;

        private decimal? decTotalAmountStockDifference= 0;
        private decimal? decTotalAmountBuyDifference= 0;
        private decimal? decTotalAmountStockNonIsCenterDifference= 0; // Tong tien tồn ở kho tỉnh
        private decimal? decTotalAmountBuyNonIsCenterDifference= 0; // tong tien ban o kho tỉnh
        private decimal? decTotalAmountOtherIncomeDifference= 0;


        #endregion
        #endregion

        #region Properties

        public string InputVoucherID
        {
            get { return strInputVoucherID; }
            set { strInputVoucherID = value; }
        }
        public decimal? TotalAmountNoVat
        {
            get { return decTotalAmountNoVat; }
            set { decTotalAmountNoVat = value; }
        }
        public decimal? PriceNoVat
        {
            get { return decPriceNoVat; }
            set { decPriceNoVat = value; }
        }
        public int? Quantity
        {
            get { return intQuantity; }
            set { intQuantity = value; }
        }

        /// <summary>
        /// InStockQuantity
        ///
        /// </summary>
        public int? InStockQuantity
        {
            get { return intInStockQuantity; }
            set { intInStockQuantity = value; }
        }

        public decimal? Price
        {
            get { return decPrice; }
            set { decPrice = value; }
        }
        public decimal? Vat
        {
            get { return decVat; }
            set { decVat = value; }
        }

        public decimal? VATAmount
        {
            get { return decVatAmount; }
            set { decVatAmount = value; }
        }

        public decimal? TotalAmount
        {
            get { return decTotalAmount; }
            set { decTotalAmount = value; }
        }
        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }
        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }
        public string PriceProtectDetailID
        {
            get { return strPriceProtectDetailID; }
            set { strPriceProtectDetailID = value; }
        }
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }
        public string PriceProtectID
        {
            get { return strPriceProtectID; }
            set { strPriceProtectID = value; }
        }

        /// <summary>
        /// VatTypeID
        ///
        /// </summary>
        public int VatTypeID
        {
            get { return intVatTypeID; }
            set { intVatTypeID = value; }
        }

        /// <summary>
        /// VatPercent
        ///
        /// </summary>
        public decimal VatPercent
        {
            get { return decVatPercent; }
            set { decVatPercent = value; }
        }

        /// <summary>
        /// VatPInvoiceDTID
        ///
        /// </summary>
        public string VatPInvoiceDTID
        {
            get { return strVatPInvoiceDTID; }
            set { strVatPInvoiceDTID = value; }
        }

        /// <summary>
        /// IsRequestIMEI
        ///
        /// </summary>
        public bool IsRequestIMEI
        {
            get { return bolIsRequestIMEI; }
            set { bolIsRequestIMEI = value; }
        }

        public int InVoiceTransTypeID
        {
            get { return intInVoiceTransTypeID; }
            set { intInVoiceTransTypeID = value; }
        }

        public decimal? TotalAmountBuyNonIsCenter
        {
            get { return decTotalAmountBuyNonIsCenter; }
            set { decTotalAmountBuyNonIsCenter = value; }
        }
        public decimal? TotalAmountStockNonIsCenter
        {
            get { return decTotalAmountStockNonIsCenter; }
            set { decTotalAmountStockNonIsCenter = value; }
        }

        public int? QuantityStock
        {
            get { return intQuantityStock; }
            set { intQuantityStock = value; }
        }

        public int? QuantityBuyNonIsCenter
        {
            get { return intQuantityBuyNonIsCenter; }
            set { intQuantityBuyNonIsCenter = value; }
        }

        public int? QuantityInStockNonIsCenter
        {
            get { return intQuantityInStockNonIsCenter; }
            set { intQuantityInStockNonIsCenter = value; }
        }

        public int? QuantityBuy
        {
            get { return intQuantityBuy; }
            set { intQuantityBuy = value; }
        }

        public decimal? TotalAmountBuy
        {
            get { return decTotalAmountBuy; }
            set { decTotalAmountBuy = value; }
        }

        public decimal? TotalAmountStock
        {
            get { return decTotalAmountStock; }
            set { decTotalAmountStock = value; }
        }
        public List<PriceProtectDetail_Allocate> PriceProtectDetail_Allocate
        {
            get { return lstPriceProtectDetail_Allocate; }
            set { lstPriceProtectDetail_Allocate = value; }
        }

        public List<PriceProtectDetail_IMEI> PriceProtectDetail_ImeiList
        {
            get { return lstPriceProtectDetail_Imei; }
            set { lstPriceProtectDetail_Imei = value; }
        }
        public List<PriceProtectDetail_IMEI> PriceProtectDetail_ImeiList1
        {
            get { return lstPriceProtectDetail_Imei1; }
            set { lstPriceProtectDetail_Imei1 = value; }
        }
        public DateTime? InVoiceDate
        {
            get { return dtmInVoiceDate; }
            set { dtmInVoiceDate = value; }
        }

        public string InVoiceID
        {
            get { return strInVoiceID; }
            set { strInVoiceID = value; }
        }

        /// <summary>
        /// OtherIncomeQuantity
        ///
        /// </summary>
        public int? QuantityOtherIncome
        {
            get { return intQuantityOtherIncome; }
            set { intQuantityOtherIncome = value; }
        }

        /// <summary>
        /// TotalAmountOtherIncome
        ///
        /// </summary>
        public decimal? TotalAmountOtherIncome
        {
            get { return decTotalAmountOtherIncome; }
            set { decTotalAmountOtherIncome = value; }
        }

        public decimal? PriceVoucher
        {
            get { return decPriceVoucher; }
            set { decPriceVoucher = value; }
        }

        public decimal? VatAmountVoucher
        {
            get { return decVatAmountVoucher; }
            set { decVatAmountVoucher = value; }
        }

        public decimal? TotalAmountVoucher
        {
            get { return decTotalAmountVoucher; }
            set { decTotalAmountVoucher = value; }
        }
        public decimal? PriceNoVatVoucher
        {
            get { return decPriceNoVatVoucher; }
            set { decPriceNoVatVoucher = value; }
        }

        public decimal? TotalAmountNoVatVoucher
        {
            get { return decTotalAmountNoVatVoucher; }
            set { decTotalAmountNoVatVoucher = value; }
        }

        public int? QuantityBuyVoucher
        {
            get { return intQuantityBuyVoucher; }
            set { intQuantityBuyVoucher = value; }
        }

        public int? QuantityStockVoucher
        {
            get { return intQuantityStockVoucher; }
            set { intQuantityStockVoucher = value; }
        }

        public int? QuantityInStockNonIsCenterVoucher
        {
            get { return intQuantityInStockNonIsCenterVoucher; }
            set { intQuantityInStockNonIsCenterVoucher = value; }
        }

        public int? QuantityBuyNonIsCenterVoucher
        {
            get { return intQuantityBuyNonIsCenterVoucher; }
            set { intQuantityBuyNonIsCenterVoucher = value; }
        }

        public int? QuantityOtherIncomeVoucher
        {
            get { return intQuantityOtherIncomeVoucher; }
            set { intQuantityOtherIncomeVoucher = value; }
        }

        public decimal? TotalAmountStockVoucher
        {
            get { return decTotalAmountStockVoucher; }
            set { decTotalAmountStockVoucher = value; }
        }

        public decimal? TotalAmountBuyVoucher
        {
            get { return decTotalAmountBuyVoucher; }
            set { decTotalAmountBuyVoucher = value; }
        }

        public decimal? TotalAmountStockNonIsCenterVoucher
        {
            get { return decTotalAmountStockNonIsCenterVoucher; }
            set { decTotalAmountStockNonIsCenterVoucher = value; }
        }

        public decimal? TotalAmountBuyNonIsCenterVoucher
        {
            get { return decTotalAmountBuyNonIsCenterVoucher; }
            set { decTotalAmountBuyNonIsCenterVoucher = value; }
        }

        public decimal? TotalAmountOtherIncomeVoucher
        {
            get { return decTotalAmountOtherIncomeVoucher; }
            set { decTotalAmountOtherIncomeVoucher = value; }
        }

        public List<PriceProtectDetail_Allocate> PriceProtectDetail_Allocate2
        {
            get { return lstPriceProtectDetail_Allocate2; }
            set { lstPriceProtectDetail_Allocate2 = value; }
        }

        public int? QuantityNoInput
        {
            get { return intQuantityNoInput; }
            set { intQuantityNoInput = value; }
        }

        public int? QuantityNoInputVoucher
        {
            get { return intQuantityNoInputVoucher; }
            set { intQuantityNoInputVoucher = value; }
        }

        public int? QuantityDifference
        {
            get { return intQuantityDifference; }
            set { intQuantityDifference = value; }
        }

        public decimal? PriceDifference
        {
            get { return decPriceDifference; }
            set { decPriceDifference = value; }
        }

        public decimal? PriceNoVatDifference
        {
            get { return decPriceNoVatDifference; }
            set { decPriceNoVatDifference = value; }
        }

        public decimal? VatAmountDifference
        {
            get { return decVatAmountDifference; }
            set { decVatAmountDifference = value; }
        }

        public decimal? TotalAmountDifference
        {
            get { return decTotalAmountDifference; }
            set { decTotalAmountDifference = value; }
        }

        public decimal? TotalAmountNoVatDifference
        {
            get { return decTotalAmountNoVatDifference; }
            set { decTotalAmountNoVatDifference = value; }
        }

        public int? QuantityNoInpuDifference
        {
            get { return intQuantityNoInputDifference; }
            set { intQuantityNoInputDifference = value; }
        }

        public int? QuantityNoInputDifference
        {
            get { return intQuantityNoInputDifference; }
            set { intQuantityNoInputDifference = value; }
        }

        public int? QuantityBuyDifference
        {
            get { return intQuantityBuyDifference; }
            set { intQuantityBuyDifference = value; }
        }

        public int? QuantityStockDifference
        {
            get { return intQuantityStockDifference; }
            set { intQuantityStockDifference = value; }
        }

        public int? QuantityInStockNonIsCenterDifference
        {
            get { return intQuantityInStockNonIsCenterDifference; }
            set { intQuantityInStockNonIsCenterDifference = value; }
        }

        public int? QuantityBuyNonIsCenterDifference
        {
            get { return intQuantityBuyNonIsCenterDifference; }
            set { intQuantityBuyNonIsCenterDifference = value; }
        }

        public int? QuantityOtherIncomeDifference
        {
            get { return intQuantityOtherIncomeDifference; }
            set { intQuantityOtherIncomeDifference = value; }
        }

        public decimal? TotalAmountStockDifference
        {
            get { return decTotalAmountStockDifference; }
            set { decTotalAmountStockDifference = value; }
        }

        public decimal? TotalAmountBuyDifference
        {
            get { return decTotalAmountBuyDifference; }
            set { decTotalAmountBuyDifference = value; }
        }

        public decimal? TotalAmountStockNonIsCenterDifference
        {
            get { return decTotalAmountStockNonIsCenterDifference; }
            set { decTotalAmountStockNonIsCenterDifference = value; }
        }

        public decimal? TotalAmountBuyNonIsCenterDifference
        {
            get { return decTotalAmountBuyNonIsCenterDifference; }
            set { decTotalAmountBuyNonIsCenterDifference = value; }
        }

        public decimal? TotalAmountOtherIncomeDifference
        {
            get { return decTotalAmountOtherIncomeDifference; }
            set { decTotalAmountOtherIncomeDifference = value; }
        }

        #endregion

        public const String colPriceProtectDetailID = "PriceProtectDetailID";
        public const String colPriceProtectID = "PriceProtectID";
        public const String colProductID = "ProductID";
        public const String colProductName = "ProductName";
        public const String colQuantity = "Quantity";
        public const String colPrice = "Price";
        public const String colVat = "Vat";
        public const String colTotalAmount = "TotalAmount";
        public const String colVatAmount = "VatAmount";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colPriceNoVat = "PRICENOVAT";
        public const String colTotalAmountNoVat = "TOTALAMOUNTNOVAT";
        public const String colInputVoucherID = "INPUTVOUCHERID";
        public const String colInVoiceID = "INVOICEID";
        public const String colInVoiceTransTypeID = "INVOICETRANSTYPEID";
        public const String colVatTypeID = "VatTypeID";
        public const String colVatPercent = "VatPercent";
        public const String colVatPInvoiceDTID = "VatPInvoiceDTID";
        public const String colIsRequestIMEI = "IsRequestIMEI";
        public const String colInStockQuantity = "InStockQuantity";
        public const String colOtherIncomeQuantity = "OtherIncomeQuantity";
        public const String colTotalAmountOtherIncome = "TotalAmountOtherIncome";
        public const String colPriceNoVatVoucher = "PRICENOVATVOUCHER";
        public const String colPriceVoucher = "PRICEVOUCHER";
        public const String colTotalAmountNoVatVoucher = "TOTALAMOUNTNOVATVOUCHER";
        public const String colTotalAmountVoucher = "TOTALAMOUNTVOUCHER";
        public const String colVATAmountVoucher = "VatAmountVoucher";

    }
}
