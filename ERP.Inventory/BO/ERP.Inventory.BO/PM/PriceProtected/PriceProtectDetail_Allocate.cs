﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.PM.PriceProtected
{
    public class PriceProtectDetail_Allocate
    {
        #region Member Variables
        private string strPriceProtectDetailID = string.Empty;
        private string strVatPinVoiceID = string.Empty;
        private int? intInVoiceTransTypeID = 0;
        private int intAllocateTypeID = 0; // 1: Hàng đã bán ra khỏi hệ thống; 2: Hàng còn tồn ở chi nhánh trung tâm; 3: Hàng tồn ở chi nhánh tỉnh
        private int? intQuantity = null;
        private decimal? decPrice = null;
        private decimal? decVat = null;
        private decimal? decTotalAmount = null;
        private decimal? decPriceNoVat = null;
        private decimal? decTotalAmountNoVat = null;
        private string strPriceProtectDetailAllocateID = string.Empty;
        private int intStoreID = -1;
        private string strStoreName = string.Empty;
        #endregion

        #region Propertive
        public string PriceProtectDetailAllocateID
        {
            get { return strPriceProtectDetailAllocateID; }
            set { strPriceProtectDetailAllocateID = value; }
        }
        public decimal? TotalAmountNoVat
        {
            get { return decTotalAmountNoVat; }
            set { decTotalAmountNoVat = value; }
        }
        public decimal? PriceNoVat
        {
            get { return decPriceNoVat; }
            set { decPriceNoVat = value; }
        }
        public decimal? TotalAmount
        {
            get { return decTotalAmount; }
            set { decTotalAmount = value; }
        }
        public decimal? Vat
        {
            get { return decVat; }
            set { decVat = value; }
        }
        public decimal? Price
        {
            get { return decPrice; }
            set { decPrice = value; }
        }
        public int? Quantity
        {
            get { return intQuantity; }
            set { intQuantity = value; }
        }
        public int AllocateTypeID
        {
            get { return intAllocateTypeID; }
            set { intAllocateTypeID = value; }
        }
        public int? InVoiceTransTypeID
        {
            get { return intInVoiceTransTypeID; }
            set { intInVoiceTransTypeID = value; }
        }
        public string VatPinVoiceID
        {
            get { return strVatPinVoiceID; }
            set { strVatPinVoiceID = value; }
        }
        public string PriceProtectDetailID
        {
            get { return strPriceProtectDetailID; }
            set { strPriceProtectDetailID = value; }
        }

        public int StoreID
        {
            get { return intStoreID; }
            set { intStoreID = value; }
        }

        /// <summary>
        /// StoreName
        ///
        /// </summary>
        public string StoreName
        {
            get { return strStoreName; }
            set { strStoreName = value; }
        }
        #endregion

        public const String colPriceProtectDetailID = "PriceProtectDetailID";
        public const String colPriceProtectID = "PriceProtectID";
        public const String colQuantity = "Quantity";
        public const String colPrice = "Price";
        public const String colVat = "Vat";
        public const String colTotalAmount = "TotalAmount";
        public const String colPriceNoVat = "PRICENOVAT";
        public const String colTotalAmountNoVat = "TOTALAMOUNTNOVAT";
        public const String colVatPinVoicedID = "VATPINVOICEDTID";
        public const String colInVoiceTransTypeID = "INVOICETRANSTYPEID";
        public const String colAllocateTypeID = "ALLOCATETYPEID";
        public const String colPriceProtectDetailAllocateID = "PRICEPROTECTDETAILALLOCATEID";
        public const String colStoreID = "StoreID";
        public const String colStoreName = "StoreName";
    }
}