
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.PM.PriceProtected
{
    /// <summary>
	/// Created by 		: LÊ VĂN ĐÔNG 
	/// Created date 	: 7/6/2018 
	/// Danh sách nhà cung cấp của phiếu hỗ trợ giá
	/// </summary>	
	public class PriceProtect_Customer
	{	
	
	
		#region Member Variables

		private int intCustomerID = 0;
		private string strPriceProtectID = string.Empty;
		private bool bolIsDefault = false;
		private string strCreatedUser = string.Empty;
		private DateTime dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strCustomerName = string.Empty;
		private string strCustomerAddress = string.Empty;
		private string strCustomerTaxID = string.Empty;
		private string strCustomerPhone = string.Empty;

        private bool bolIsUpdate = false;
        #endregion


        #region Properties 

        public bool IsUpdate
        {
            get { return bolIsUpdate; }
            set { bolIsUpdate = value; }
        }

        /// <summary>
        /// CustomerID
        /// Mã nhà cung cấp
        /// </summary>
        public int CustomerID
		{
			get { return  intCustomerID; }
			set { intCustomerID = value; }
		}

		/// <summary>
		/// PriceProtectID
		/// Mã phiếu hỗ trợ giá
		/// </summary>
		public string PriceProtectID
		{
			get { return  strPriceProtectID; }
			set { strPriceProtectID = value; }
		}

		/// <summary>
		/// IsDefault
		/// NCC mặc đinh
		/// </summary>
		public bool IsDefault
		{
			get { return  bolIsDefault; }
			set { bolIsDefault = value; }
		}

		/// <summary>
		/// CreatedUser
		/// Người tạo
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// Ngày tạo
		/// </summary>
		public DateTime CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// Nguoi cập nhật
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// Ngày cập nhật
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// Đã xóa
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// Người xóa
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// Ngày xóa
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// CustomerName
		/// Tên khách hàng
		/// </summary>
		public string CustomerName
		{
			get { return  strCustomerName; }
			set { strCustomerName = value; }
		}

		/// <summary>
		/// CustomerAddress
		/// Địa chỉ
		/// </summary>
		public string CustomerAddress
		{
			get { return  strCustomerAddress; }
			set { strCustomerAddress = value; }
		}

		/// <summary>
		/// CustomerTaxID
		/// Mã số thuế
		/// </summary>
		public string CustomerTaxID
		{
			get { return  strCustomerTaxID; }
			set { strCustomerTaxID = value; }
		}

		/// <summary>
		/// CustomerPhone
		/// Số điện thoại
		/// </summary>
		public string CustomerPhone
		{
			get { return  strCustomerPhone; }
			set { strCustomerPhone = value; }
		}


		#endregion			
		
		
		#region Constructor

		public PriceProtect_Customer()
		{
		}
		#endregion


		#region Column Names

		public const String colCustomerID = "CustomerID";
		public const String colPriceProtectID = "PriceProtectID";
		public const String colIsDefault = "IsDefault";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colCustomerName = "CustomerName";
		public const String colCustomerAddress = "CustomerAddress";
		public const String colCustomerTaxID = "CustomerTaxID";
		public const String colCustomerPhone = "CustomerPhone";

		#endregion //Column names

		
	}
}
