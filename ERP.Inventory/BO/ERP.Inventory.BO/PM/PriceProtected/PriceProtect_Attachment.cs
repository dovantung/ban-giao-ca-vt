
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.PM.PriceProtected
{
    /// <summary>
	/// Created by 		: LE VAN DONG
	/// Created date 	: 06/07/2018
	/// Danh sách file đính kèm của phiếu hỗ trợ giá
	/// </summary>	

    public class PriceProtect_Attachment
	{	
	
	
		#region Member Variables

		private string strAttachmentID = string.Empty;
		private string strPriceProtectID = string.Empty;
		private string strDescription = string.Empty;
		private string strFilePath = string.Empty;
		private string strFileName = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private bool bolIsExist = false;
        private string strLocalFilePath = string.Empty;
        private bool bolIsNew = false;
        private string strFileID = string.Empty;
        private string strCreatedUserName = string.Empty;
        private bool bolIsUpdate = false;
        #endregion

        #region Properties 
        public bool IsUpdate
        {
            get { return bolIsUpdate; }
            set { bolIsUpdate = value; }
        }

        public string FileID
        {
            get { return strFileID; }
            set { strFileID = value; }
        }

        public bool IsNew
        {
            get { return bolIsNew; }
            set { bolIsNew = value; }
        }

        //Luu duong dan local
        public string LocalFilePath
        {
            get { return strLocalFilePath; }
            set { strLocalFilePath = value; }
        }

		/// <summary>
		/// AttachmentID
		/// 
		/// </summary>
		public string AttachmentID
		{
			get { return  strAttachmentID; }
			set { strAttachmentID = value; }
		}

		/// <summary>
		/// PriceProtectID
		/// Mã phiếu nhập
		/// </summary>
		public string PriceProtectID
		{
			get { return  strPriceProtectID; }
			set { strPriceProtectID = value; }
		}

		/// <summary>
		/// Description
		/// Ghi chú
		/// </summary>
		public string Description
		{
			get { return  strDescription; }
			set { strDescription = value; }
		}

		/// <summary>
		/// FilePath
		/// Đường dẫn URL file
		/// </summary>
		public string FilePath
		{
			get { return  strFilePath; }
			set { strFilePath = value; }
		}

		/// <summary>
		/// FileName
		/// Tên file
		/// </summary>
		public string FileName
		{
			get { return  strFileName; }
			set { strFileName = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

        /// <summary>
        /// CreatedUserName
        ///
        /// </summary>
        public string CreatedUserName
        {
            get { return strCreatedUserName; }
            set { strCreatedUserName = value; }
        }


        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}

		#endregion			
		
		
		#region Constructor

        public PriceProtect_Attachment()
		{
		}
		#endregion


		#region Column Names

		public const String colAttachmentID = "AttachmentID";
		public const String colPriceProtectID = "PriceProtectID";
		public const String colDescription = "Description";
		public const String colFilePath = "FilePath";
		public const String colFileName = "FileName";
		public const String colCreatedUser = "CreatedUser";
        public const String colCreatedUserName = "CreatedUserName";
        public const String colCreatedDate = "CreatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colFileID = "FileID";
        #endregion //Column names


    }
}
