﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.PM.PriceProtected
{
    public class PriceProtectDetail_IMEI
    {
        #region Property
        private string strPriceProtectDetailID = string.Empty;
        private string strProductID = string.Empty;

        private string strProductIMEI = null;
        private string strInputVoucherID = string.Empty;
        private DateTime? dtmInputDate = null;
        private int intAllocateTypeID = 0;
        private int intStoreID = -1;
        private string strStoreName = string.Empty;
        private bool bolIsValid = false;

        private decimal decOriginalCostPrice = 0;
        private decimal decAdjustCostPrice = 0;
        private decimal decCostPrice = 0;
        private string strInputVoucherDetailID = string.Empty;
        private string strOutputVoucherDetailID = string.Empty;
        private string strLastInputVoucherDetailID = string.Empty;
        private bool bolIsCenterBranch = false;

        private DateTime? dtmAllocateDate;
        private int intNumOfAllocate = 0;
        private string strVATInvoiceDTID1 = string.Empty;
        private string strInputVoucherDetailID1 = string.Empty;
        private string strOutputVoucherDetailID1 = string.Empty;
        private decimal decAdjustCostPrice1 = 0;
        private decimal decCostPrice1 = 0;


        #endregion
        public string ProductIMEI
        {
            get { return strProductIMEI; }
            set { strProductIMEI = value; }
        }
        public string PriceProtectDetailID
        {
            get { return strPriceProtectDetailID; }
            set { strPriceProtectDetailID = value; }
        }
        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }
        public string InputVoucherID
        {
            get { return strInputVoucherID; }
            set { strInputVoucherID = value; }
        }
        public DateTime? InputDate
        {
            get { return dtmInputDate; }
            set { dtmInputDate = value; }
        }

        public int AllocateTypeID
        {
            get { return intAllocateTypeID; }
            set { intAllocateTypeID = value; }
        }
        public bool IsValid
        {
            get { return bolIsValid; }
            set { bolIsValid = value; }
        }
        public int StoreID
        {
            get { return intStoreID; }
            set { intStoreID = value; }
        }

        /// <summary>
        /// StoreName
        ///
        /// </summary>
        public string StoreName
        {
            get { return strStoreName; }
            set { strStoreName = value; }
        }

        /// <summary>
        /// CostPrice
        /// Giá vốn sau điều chỉnh
        /// </summary>
        public decimal CostPrice
        {
            get { return decCostPrice; }
            set { decCostPrice = value; }
        }

        /// <summary>
        /// OriginalCostPrice
        /// Giá vốn trước điều chỉnh
        /// </summary>
        public decimal OriginalCostPrice
        {
            get { return decOriginalCostPrice; }
            set { decOriginalCostPrice = value; }
        }

        /// <summary>
        /// AdjustCostPrice
        /// Giá điều chỉnh
        /// </summary>
        public decimal AdjustCostPrice
        {
            get { return decAdjustCostPrice; }
            set { decAdjustCostPrice = value; }
        }



        /// <summary>
        /// LastInputVoucherDetailID
        ///
        /// </summary>
        public string LastInputVoucherDetailID
        {
            get { return strLastInputVoucherDetailID; }
            set { strLastInputVoucherDetailID = value; }
        }

        /// <summary>
        /// InputVoucherDetailID
        ///
        /// </summary>
        public string InputVoucherDetailID
        {
            get { return strInputVoucherDetailID; }
            set { strInputVoucherDetailID = value; }
        }

        /// <summary>
        /// OutputVoucherDetailID
        ///
        /// </summary>
        public string OutputVoucherDetailID
        {
            get { return strOutputVoucherDetailID; }
            set { strOutputVoucherDetailID = value; }
        }

        /// <summary>
        /// IsCenterBranch
        ///
        /// </summary>
        public bool IsCenterBranch
        {
            get { return bolIsCenterBranch; }
            set { bolIsCenterBranch = value; }
        }

        public DateTime? AllocateDate
        {
            get { return dtmAllocateDate; }
            set { dtmAllocateDate = value; }
        }
        public int NumOfAllocate
        {
            get { return intNumOfAllocate; }
            set { intNumOfAllocate = value; }
        }

        public string VATInvoiceDTID1
        {
            get { return strVATInvoiceDTID1; }
            set { strVATInvoiceDTID1 = value; }
        }

        public string InputVoucherDetailID1
        {
            get { return strInputVoucherDetailID1; }
            set { strInputVoucherDetailID1 = value; }
        }

        public string OutputVoucherDetailID1
        {
            get { return strOutputVoucherDetailID1; }
            set { strOutputVoucherDetailID1 = value; }
        }

        public decimal AdjustCostPrice1
        {
            get { return decAdjustCostPrice1; }
            set { decAdjustCostPrice1 = value; }
        }

        public decimal CostPrice1
        {
            get { return decCostPrice1; }
            set { decCostPrice1 = value; }
        }


        #region Column Names
        public const String colPriceProtectDetailID = "PriceProtectDetailID";
        public const String colProductID = "ProductID";
        public const String colProductIMEI = "ProductIMEI";
        public const String colInputVoucherID = "InputVoucherID";
        public const String colInputDate = "InputDate";
        public const String colAllocateTypeID = "AllocateTypeID";
        public const String colStoreID = "StoreID";
        public const String colStoreName = "StoreName";
        public const String colCostPrice = "CostPrice";
        public const String colOriginalCostPrice = "OriginalCostPrice";
        public const String colAdjustCostPrice = "AdjustCostPrice";
        public const String colLastInputVoucherDetailID = "LastInputVoucherDetailID";
        public const String colInputVoucherDetailID = "InputVoucherDetailID";
        public const String colOutputVoucherDetailID = "OutputVoucherDetailID";
        public const String colIsCenterBranch = "IsCenterBranch";
        public const String colNumOfAllocate = "NumOfAllocate";
        public const String colAllocateDate = "AllocateDate";
        public const String colVATInvoiceDTID1 = "VATInvoiceDTID1";
        public const String colInputVoucherDetailID1 = "InputVoucherDetailID1";
        public const String colOutputVoucherDetailID1 = "OutputVoucherDetailID1";
        public const String colAdjustCostPrice1 = "AdjustCostPrice1";
        public const String colCostPrice1 = "CostPrice1";

        #endregion

    }
}
