
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.PM
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 4/2/2014 
	/// Thông tin bán hàng của 1 lô IMEI
	/// </summary>	
	public class LotIMEISalesInfo
	{	
	
	
		#region Member Variables

		private string strLotIMEISalesInfoID = string.Empty;
		private string strLotIMEISalesInfoName = string.Empty;
		private string strProductID = string.Empty;
		private string strDescription = string.Empty;
		private bool bolIsBrandNewWarranty = false;
		private DateTime? dtmEndWarrantyDate;
		private string strNote = string.Empty;
		private string strPrintVATContent = string.Empty;
		private bool bolIsConfirm = false;
		private bool bolIsReviewed = false;
		private string strReviewedUser = string.Empty;
		private DateTime? dtmReviewedDate;
		private string strWebProductSpecContent = string.Empty;
		private bool bolIsActive = false;
		private bool bolIsSystem = false;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private bool bolIsExist = false;
        private int intCreatedStoreID = -1;
        private int intOrderIndex = 0;
        private string strProductName = string.Empty;
        private List<LotIMEISalesInfo_Images> lstLotIMEISalesInfo_Images = null;
        private List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = null;
        private List<LotIMEISalesInfo_ProSpec> lstLotIMEISalesInfo_ProSpec = null; 

		#endregion


		#region Properties 

		/// <summary>
		/// LotIMEISalesInfoID
		/// Mã thông tin bán hàng của 1 lô(Tự động phát sinh theo định dạng xxxLIYYMMxxxxxx)
		/// </summary>
		public string LotIMEISalesInfoID
		{
			get { return  strLotIMEISalesInfoID; }
			set { strLotIMEISalesInfoID = value; }
		}

		/// <summary>
		/// LotIMEISalesInfoName
		/// Tên thông tin bán hàng của 1 lô
		/// </summary>
		public string LotIMEISalesInfoName
		{
			get { return  strLotIMEISalesInfoName; }
			set { strLotIMEISalesInfoName = value; }
		}

		/// <summary>
		/// ProductID
		/// Mã sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// Description
		/// 
		/// </summary>
		public string Description
		{
			get { return  strDescription; }
			set { strDescription = value; }
		}

		/// <summary>
		/// IsBrandNewWarranty
		/// 
		/// </summary>
		public bool IsBrandNewWarranty
		{
			get { return  bolIsBrandNewWarranty; }
			set { bolIsBrandNewWarranty = value; }
		}

		/// <summary>
		/// EndWarrantyDate
		/// 
		/// </summary>
		public DateTime? EndWarrantyDate
		{
			get { return  dtmEndWarrantyDate; }
			set { dtmEndWarrantyDate = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// PrintVATContent
		/// 
		/// </summary>
		public string PrintVATContent
		{
			get { return  strPrintVATContent; }
			set { strPrintVATContent = value; }
		}

		/// <summary>
		/// IsConfirm
		/// 
		/// </summary>
		public bool IsConfirm
		{
			get { return  bolIsConfirm; }
			set { bolIsConfirm = value; }
		}

		/// <summary>
		/// IsReviewed
		/// 
		/// </summary>
		public bool IsReviewed
		{
			get { return  bolIsReviewed; }
			set { bolIsReviewed = value; }
		}

		/// <summary>
		/// ReviewedUser
		/// 
		/// </summary>
		public string ReviewedUser
		{
			get { return  strReviewedUser; }
			set { strReviewedUser = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// 
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// WebProductSpecContent
		/// 
		/// </summary>
		public string WebProductSpecContent
		{
			get { return  strWebProductSpecContent; }
			set { strWebProductSpecContent = value; }
		}

		/// <summary>
		/// IsActive
		/// 
		/// </summary>
		public bool IsActive
		{
			get { return  bolIsActive; }
			set { bolIsActive = value; }
		}

		/// <summary>
		/// IsSystem
		/// 
		/// </summary>
		public bool IsSystem
		{
			get { return  bolIsSystem; }
			set { bolIsSystem = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        public int OrderIndex
        {
            get { return intOrderIndex; }
            set { intOrderIndex = value; }
        }

        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }

        public List<LotIMEISalesInfo_Images> LotIMEISalesInfo_ImagesList
        {
            get { return lstLotIMEISalesInfo_Images; }
            set { lstLotIMEISalesInfo_Images = value; }
        }

        public List<LotIMEISalesInfo_IMEI> LotIMEISalesInfo_IMEIList
        {
            get { return lstLotIMEISalesInfo_IMEI; }
            set { lstLotIMEISalesInfo_IMEI = value; }
        }

        public List<LotIMEISalesInfo_ProSpec> LotIMEISalesInfo_ProSpecList
        {
            get { return lstLotIMEISalesInfo_ProSpec; }
            set { lstLotIMEISalesInfo_ProSpec = value; }
        }
        
		#endregion			
		
		
		#region Constructor

		public LotIMEISalesInfo()
		{
		}
		#endregion


		#region Column Names

		public const String colLotIMEISalesInfoID = "LotIMEISalesInfoID";
		public const String colLotIMEISalesInfoName = "LotIMEISalesInfoName";
		public const String colProductID = "ProductID";
		public const String colDescription = "Description";
		public const String colIsBrandNewWarranty = "IsBrandNewWarranty";
		public const String colEndWarrantyDate = "EndWarrantyDate";
		public const String colNote = "Note";
		public const String colPrintVATContent = "PrintVATContent";
		public const String colIsConfirm = "IsConfirm";
		public const String colIsReviewed = "IsReviewed";
		public const String colReviewedUser = "ReviewedUser";
		public const String colReviewedDate = "ReviewedDate";
		public const String colWebProductSpecContent = "WebProductSpecContent";
		public const String colIsActive = "IsActive";
		public const String colIsSystem = "IsSystem";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colProductName = "ProductName";
        public const String colOrderIndex = "OrderIndex";
		#endregion //Column names

		
	}
}
