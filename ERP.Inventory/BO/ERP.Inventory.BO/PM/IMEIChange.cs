﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.PM
{
    public class IMEIChange
    {
        #region Member Variables

        private string strOutputVoucherDetailID = string.Empty;
        private string strFromIMEI = string.Empty;
        private string strToIMEI = string.Empty;
        private bool bolIsSystem = false;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private bool bolIsExist = false;

        #endregion

        #region Properties

        public string OutputVoucherDetailID
        {
            get { return strOutputVoucherDetailID; }
            set { strOutputVoucherDetailID = value; }
        }

        public string FromIMEI
        {
            get { return strFromIMEI; }
            set { strFromIMEI = value; }
        }

        public string ToIMEI
        {
            get { return strToIMEI; }
            set { strToIMEI = value; }
        }
        
        public bool IsSystem
        {
            get { return bolIsSystem; }
            set { bolIsSystem = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        #endregion

        #region Constructor

        public IMEIChange()
		{
		}
		#endregion

        #region Column Names

        public const String colOutputVoucherDetailID = "OutputVoucherDetailID";
        public const String colFromIMEI = "FromIMEI";
        public const String colToIMEI = "ToIMEI";
        public const String colIsSystem = "IsSystem";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        #endregion //Column names

    }
}
