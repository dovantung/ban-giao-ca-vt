
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.PM
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 4/2/2014 
	/// 
	/// </summary>	
	public class LotIMEISalesInfo_ProSpec
	{	
	
	
		#region Member Variables

		private string strLotIMEISalesInfoID = string.Empty;
		private int intProductSpecID = 0;
		private int intProductSpecStatusID = 0;
		private string strNote = string.Empty;
        private bool bolIsExist = false;
        private string strProductSpecName = string.Empty;

		#endregion


		#region Properties 

		/// <summary>
		/// LotIMEISalesInfoID
		/// 
		/// </summary>
		public string LotIMEISalesInfoID
		{
			get { return  strLotIMEISalesInfoID; }
			set { strLotIMEISalesInfoID = value; }
		}

		/// <summary>
		/// ProductSpecID
		/// 
		/// </summary>
		public int ProductSpecID
		{
			get { return  intProductSpecID; }
			set { intProductSpecID = value; }
		}

		/// <summary>
		/// ProductSpecStatusID
		/// 
		/// </summary>
		public int ProductSpecStatusID
		{
			get { return  intProductSpecStatusID; }
			set { intProductSpecStatusID = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        public string ProductSpecName
        {
            get { return strProductSpecName; }
            set { strProductSpecName = value; }
        }
		#endregion			
		
		
		#region Constructor

		public LotIMEISalesInfo_ProSpec()
		{
		}
		#endregion


		#region Column Names

		public const String colLotIMEISalesInfoID = "LotIMEISalesInfoID";
		public const String colProductSpecID = "ProductSpecID";
		public const String colProductSpecStatusID = "ProductSpecStatusID";
		public const String colNote = "Note";
        public const String colProductSpecName = "ProductSpecName";
		#endregion //Column names

		
	}
}
