﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.PM
{
	public partial class LotIMEISalesInfo_IMEI
	{
        private decimal decSalePrice = 0;
        /// <summary>
        /// Giá bán
        /// </summary>
        public decimal SalePrice
        {
            get { return decSalePrice; }
            set { decSalePrice = value; }
        }

	}
}
