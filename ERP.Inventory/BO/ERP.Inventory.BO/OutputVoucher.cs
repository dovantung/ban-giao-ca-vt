
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Trương Trung Lợi 
	/// Created date 	: 11/09/2012 
	/// Phiếu xuất
	/// </summary>	
	public partial class OutputVoucher
    {


        #region Member Variables

        private string strOutputVoucherID = string.Empty;
        private string strOrderID = string.Empty;
        private string strInvoiceID = string.Empty;
        private string strInvoiceSymbol = string.Empty;
        private string strDenominator = string.Empty;
        private int intCreatedStoreID = 0;
        private int intOutputStoreID = 0;
        private int intCustomerID = 0;
        private string strCustomerName = string.Empty;
        private string strCustomerAddress = string.Empty;
        private string strCustomerPhone = string.Empty;
        private string strCustomerTaxID = string.Empty;
        private int intCurrencyUnitID = 0;
        private int intPayableTypeID = 0;
        private int intDiscountReasonID = 0;
        private string strOutputContent = string.Empty;
        private DateTime? dtmInvoiceDate;
        private DateTime? dtmOutputDate;
        private DateTime? dtmPayableDate;
        private string strStaffUser = string.Empty;
        private decimal decCurrencyExchange;
        private decimal decDiscount;
        private decimal decTotalAmountBFT;
        private decimal decTotalVAT;
        private decimal decTotalAmount;
        private decimal decPromotionDiscount;
        private decimal decTotalCardSpend;
        private bool bolIsStoreChange = false;
        private bool bolIsPosted = false;
        private bool bolIsError = false;
        private string strErrorContent = string.Empty;
        private string strErrorOutputVoucherID = string.Empty;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private string strOutputNote = string.Empty;
        private string strTaxCustomerAddress = string.Empty;
        private bool bolIsSInvoice = false;
        #endregion


        #region Properties 

        /// <summary>
        /// OutputVoucherID
        /// Mã phiếu xuất
        /// </summary>
        public string OutputVoucherID
        {
            get { return strOutputVoucherID; }
            set { strOutputVoucherID = value; }
        }

        /// <summary>
        /// OrderID
        /// Mã đơn hàng
        /// </summary>
        public string OrderID
        {
            get { return strOrderID; }
            set { strOrderID = value; }
        }

        /// <summary>
        /// InvoiceID
        /// Số hóa đơn
        /// </summary>
        public string InvoiceID
        {
            get { return strInvoiceID; }
            set { strInvoiceID = value; }
        }

        /// <summary>
        /// InvoiceSymbol
        /// Ký hiệu hóa đơn
        /// </summary>
        public string InvoiceSymbol
        {
            get { return strInvoiceSymbol; }
            set { strInvoiceSymbol = value; }
        }

        /// <summary>
        /// Denominator
        /// Mẩu số
        /// </summary>
        public string Denominator
        {
            get { return strDenominator; }
            set { strDenominator = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// Kho tạo
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// OutputStoreID
        /// Kho xuất
        /// </summary>
        public int OutputStoreID
        {
            get { return intOutputStoreID; }
            set { intOutputStoreID = value; }
        }

        /// <summary>
        /// CustomerID
        /// Mã khách hàng
        /// </summary>
        public int CustomerID
        {
            get { return intCustomerID; }
            set { intCustomerID = value; }
        }

        /// <summary>
        /// CustomerName
        /// Tên khách hàng
        /// </summary>
        public string CustomerName
        {
            get { return strCustomerName; }
            set { strCustomerName = value; }
        }

        /// <summary>
        /// CustomerAddress
        /// Địa chỉ khách hàng
        /// </summary>
        public string CustomerAddress
        {
            get { return strCustomerAddress; }
            set { strCustomerAddress = value; }
        }

        /// <summary>
        /// CustomerPhone
        /// Số điện thoại
        /// </summary>
        public string CustomerPhone
        {
            get { return strCustomerPhone; }
            set { strCustomerPhone = value; }
        }

        /// <summary>
        /// CustomerTaxID
        /// Mã số thuế khách hàng
        /// </summary>
        public string CustomerTaxID
        {
            get { return strCustomerTaxID; }
            set { strCustomerTaxID = value; }
        }

        /// <summary>
        /// CurrencyUnitID
        /// Loại tiền
        /// </summary>
        public int CurrencyUnitID
        {
            get { return intCurrencyUnitID; }
            set { intCurrencyUnitID = value; }
        }

        /// <summary>
        /// PayableTypeID
        /// Hình thức thanh toán
        /// </summary>
        public int PayableTypeID
        {
            get { return intPayableTypeID; }
            set { intPayableTypeID = value; }
        }

        /// <summary>
        /// DiscountReasonID
        /// Lý do giảm giá
        /// </summary>
        public int DiscountReasonID
        {
            get { return intDiscountReasonID; }
            set { intDiscountReasonID = value; }
        }

        /// <summary>
        /// OutputContent
        /// Nội dung xuất
        /// </summary>
        public string OutputContent
        {
            get { return strOutputContent; }
            set { strOutputContent = value; }
        }

        /// <summary>
        /// InvoiceDate
        /// Ngày hóa đơn
        /// </summary>
        public DateTime? InvoiceDate
        {
            get { return dtmInvoiceDate; }
            set { dtmInvoiceDate = value; }
        }

        /// <summary>
        /// OutputDate
        /// Ngày xuất
        /// </summary>
        public DateTime? OutputDate
        {
            get { return dtmOutputDate; }
            set { dtmOutputDate = value; }
        }

        /// <summary>
        /// PayableDate
        /// Ngày thanh toán
        /// </summary>
        public DateTime? PayableDate
        {
            get { return dtmPayableDate; }
            set { dtmPayableDate = value; }
        }

        /// <summary>
        /// StaffUser
        /// Nhân viên bán hàng
        /// </summary>
        public string StaffUser
        {
            get { return strStaffUser; }
            set { strStaffUser = value; }
        }

        /// <summary>
        /// CurrencyExchange
        /// Tỷ giá
        /// </summary>
        public decimal CurrencyExchange
        {
            get { return decCurrencyExchange; }
            set { decCurrencyExchange = value; }
        }

        /// <summary>
        /// Discount
        /// Giảm giá
        /// </summary>
        public decimal Discount
        {
            get { return decDiscount; }
            set { decDiscount = value; }
        }

        /// <summary>
        /// TotalAmountBFT
        /// Tổng tiền trước thuế
        /// </summary>
        public decimal TotalAmountBFT
        {
            get { return decTotalAmountBFT; }
            set { decTotalAmountBFT = value; }
        }

        /// <summary>
        /// TotalVAT
        /// Tổng tiền thuế
        /// </summary>
        public decimal TotalVAT
        {
            get { return decTotalVAT; }
            set { decTotalVAT = value; }
        }

        /// <summary>
        /// TotalAmount
        /// Tổng tiền sau thuế
        /// </summary>
        public decimal TotalAmount
        {
            get { return decTotalAmount; }
            set { decTotalAmount = value; }
        }

        /// <summary>
        /// PromotionDiscount
        /// Giảm giá KM
        /// </summary>
        public decimal PromotionDiscount
        {
            get { return decPromotionDiscount; }
            set { decPromotionDiscount = value; }
        }

        /// <summary>
        /// TotalCardSpend
        /// Tổng tiền thanh toán thẻ
        /// </summary>
        public decimal TotalCardSpend
        {
            get { return decTotalCardSpend; }
            set { decTotalCardSpend = value; }
        }

        /// <summary>
        /// IsStoreChange
        /// Là phiếu xuất chuyển kho
        /// </summary>
        public bool IsStoreChange
        {
            get { return bolIsStoreChange; }
            set { bolIsStoreChange = value; }
        }

        /// <summary>
        /// IsPosted
        /// Đã hạch toán
        /// </summary>
        public bool IsPosted
        {
            get { return bolIsPosted; }
            set { bolIsPosted = value; }
        }

        /// <summary>
        /// IsError
        /// Là phiếu xuất lỗi
        /// </summary>
        public bool IsError
        {
            get { return bolIsError; }
            set { bolIsError = value; }
        }

        /// <summary>
        /// ErrorContent
        /// Nội dung lỗi
        /// </summary>
        public string ErrorContent
        {
            get { return strErrorContent; }
            set { strErrorContent = value; }
        }

        /// <summary>
        /// ErrorOutputVoucherID
        /// Mã phiếu xuất lỗi(Trong trường hợp phiếu này là phiếu sửa lỗi)
        /// </summary>
        public string ErrorOutputVoucherID
        {
            get { return strErrorOutputVoucherID; }
            set { strErrorOutputVoucherID = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        public string OutputNote
        {
            get{ return strOutputNote; }
            set { strOutputNote = value; }
        }
        /// <summary>
        /// TaxCustomerAddress
        /// Đăng bổ sung 1 cột địa chỉ khách hàng
        /// </summary>
        public string TaxCustomerAddress
        {
            get { return strTaxCustomerAddress; }
            set { strTaxCustomerAddress = value; }
        }

        /// <summary>
        /// IsSInvoice
        /// Đăng bổ sung 1 cột là hóa đơn điện tử
        /// </summary>
        public bool IsSInvoice
        {
            get { return bolIsSInvoice; }
            set { bolIsSInvoice = value; }
        }

        public string BrokerUser { get; set; }

        #endregion


        #region Constructor

        public OutputVoucher()
        {
        }
        #endregion


        #region Column Names

        public const String colOutputVoucherID = "OutputVoucherID";
        public const String colOrderID = "OrderID";
        public const String colInvoiceID = "InvoiceID";
        public const String colInvoiceSymbol = "InvoiceSymbol";
        public const String colDenominator = "Denominator";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colOutputStoreID = "OutputStoreID";
        public const String colCustomerID = "CustomerID";
        public const String colCustomerName = "CustomerName";
        public const String colCustomerAddress = "CustomerAddress";
        public const String colCustomerPhone = "CustomerPhone";
        public const String colCustomerTaxID = "CustomerTaxID";
        public const String colCurrencyUnitID = "CurrencyUnitID";
        public const String colPayableTypeID = "PayableTypeID";
        public const String colDiscountReasonID = "DiscountReasonID";
        public const String colOutputContent = "OutputContent";
        public const String colInvoiceDate = "InvoiceDate";
        public const String colOutputDate = "OutputDate";
        public const String colPayableDate = "PayableDate";
        public const String colStaffUser = "StaffUser";
        public const String colCurrencyExchange = "CurrencyExchange";
        public const String colDiscount = "Discount";
        public const String colTotalAmountBFT = "TotalAmountBFT";
        public const String colTotalVAT = "TotalVAT";
        public const String colTotalAmount = "TotalAmount";
        public const String colPromotionDiscount = "PromotionDiscount";
        public const String colTotalCardSpend = "TotalCardSpend";
        public const String colIsStoreChange = "IsStoreChange";
        public const String colIsPosted = "IsPosted";
        public const String colIsError = "IsError";
        public const String colErrorContent = "ErrorContent";
        public const String colErrorOutputVoucherID = "ErrorOutputVoucherID";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colOutputNote = "OutputNote";
        public const String colTaxCustomerAddress = "TaxCustomerAddress";
        public const String colIsSInvoice = "IsSInvoice";
        public const String colBrokerUser = "BROKERUSER";
        #endregion //Column names


    }
}
