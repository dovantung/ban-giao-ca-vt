﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO
{
    public class StoredChange_CurrentVoucher
    {
        #region Member Variables

        private int intStoreID = 0;
        private string strUserName = string.Empty;
        private decimal decInvoiceID;
        private string strInvoiceSymbol = string.Empty;
        private string strDenominator = string.Empty;

        #endregion


        #region Properties 

        /// <summary>
        /// StoreID
        /// 
        /// </summary>
        public int StoreID
        {
            get { return intStoreID; }
            set { intStoreID = value; }
        }

        /// <summary>
        /// UserName
        /// 
        /// </summary>
        public string UserName
        {
            get { return strUserName; }
            set { strUserName = value; }
        }

        /// <summary>
        /// InvoiceID
        /// 
        /// </summary>
        public decimal InvoiceID
        {
            get { return decInvoiceID; }
            set { decInvoiceID = value; }
        }

        /// <summary>
        /// InvoiceSymbol
        /// 
        /// </summary>
        public string InvoiceSymbol
        {
            get { return strInvoiceSymbol; }
            set { strInvoiceSymbol = value; }
        }

        /// <summary>
        /// Denominator
        /// 
        /// </summary>
        public string Denominator
        {
            get { return strDenominator; }
            set { strDenominator = value; }
        }
        private decimal decInvoiceIdStart = 0;

        public decimal InvoiceIdStart
        {
            get { return decInvoiceIdStart; }
            set { decInvoiceIdStart = value; }
        }

        private string strSTORECHANGECURVOUCHERID;

        public string STORECHANGECURVOUCHERID
        {
            get { return strSTORECHANGECURVOUCHERID; }
            set { strSTORECHANGECURVOUCHERID = value; }
        }

        private decimal decINVOICEEND;

        public decimal INVOICEEND
        {
            get { return decINVOICEEND; }
            set { decINVOICEEND = value; }
        }
        private DateTime dtpFROMDATE;

        public DateTime FROMDATE
        {
            get { return dtpFROMDATE; }
            set { dtpFROMDATE = value; }
        }

        private DateTime? dtpTODATE = null;

        public DateTime? TODATE
        {
            get { return dtpTODATE; }
            set { dtpTODATE = value; }
        }

        private DateTime? dtpCREATEDDATE = null;

        public DateTime? CREATEDDATE
        {
            get { return dtpCREATEDDATE; }
            set { dtpCREATEDDATE = value; }
        }

        private DateTime? dtpUPDATEDDATE;

        public DateTime? UPDATEDDATE
        {
            get { return dtpUPDATEDDATE; }
            set { dtpUPDATEDDATE = value; }
        }




        #endregion


        #region Constructor

        public StoredChange_CurrentVoucher()
        {
        }
        #endregion


        #region Column Names

        public const String colStoreID = "StoreID";
        public const String colUserName = "UserName";
        public const String colInvoiceID = "InvoiceID";
        public const String colInvoiceSymbol = "InvoiceSymbol";
        public const String colDenominator = "Denominator";

        #endregion //Column names
    }
}
