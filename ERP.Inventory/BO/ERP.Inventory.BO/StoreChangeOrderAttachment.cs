﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO
{
    public class StoreChangeOrderAttachment
    {
        #region Member Variables

		private string strAttachMENTID = string.Empty;
		private string strStoreChangeOrderID = string.Empty;
		private string strAttachMENTName = string.Empty;
		private string strAttachMENTPath = string.Empty;
		private string strFileID = string.Empty;
		private string strDescription = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private string strVOFFICEID = string.Empty;

		#endregion


		#region Properties 

		/// <summary>
		/// AttachMENTID
		/// 
		/// </summary>
		public string AttachMENTID
		{
			get { return  strAttachMENTID; }
			set { strAttachMENTID = value; }
		}

		/// <summary>
		/// StoreChangeOrderID
		/// 
		/// </summary>
		public string StoreChangeOrderID
		{
			get { return  strStoreChangeOrderID; }
			set { strStoreChangeOrderID = value; }
		}

		/// <summary>
		/// AttachMENTName
		/// 
		/// </summary>
		public string AttachMENTName
		{
			get { return  strAttachMENTName; }
			set { strAttachMENTName = value; }
		}

		/// <summary>
		/// AttachMENTPath
		/// 
		/// </summary>
		public string AttachMENTPath
		{
			get { return  strAttachMENTPath; }
			set { strAttachMENTPath = value; }
		}

		/// <summary>
		/// FileID
		/// 
		/// </summary>
		public string FileID
		{
			get { return  strFileID; }
			set { strFileID = value; }
		}

		/// <summary>
		/// Description
		/// 
		/// </summary>
		public string Description
		{
			get { return  strDescription; }
			set { strDescription = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// VOFFICEID
		/// 
		/// </summary>
		public string VOFFICEID
		{
			get { return  strVOFFICEID; }
			set { strVOFFICEID = value; }
		}


		#endregion			
		
		
		#region Constructor

        public StoreChangeOrderAttachment()
		{
		}
		#endregion


		#region Column Names

		public const String colAttachMENTID = "AttachMENTID";
		public const String colStoreChangeOrderID = "StoreChangeOrderID";
		public const String colAttachMENTName = "AttachMENTName";
		public const String colAttachMENTPath = "AttachMENTPath";
		public const String colFileID = "FileID";
		public const String colDescription = "Description";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colVOFFICEID = "VOFFICEID";

		#endregion //Column names
    }
}
