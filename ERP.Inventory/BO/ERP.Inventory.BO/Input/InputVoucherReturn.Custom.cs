﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.Input
{
    public partial class InputVoucherReturn
    {

        private string strInVoiceID = string.Empty;
        private string strInVoiceSymbol = string.Empty;
        private string strDenominator = string.Empty;
        private int intCustomerID = 0;
        private string strCustomerName = string.Empty;
        private string strCustomerAddress = string.Empty;
        private string strCustomerPhone = string.Empty;
        private string strCustomerTaxID = string.Empty;        
        private int intInputTypeID = 0;
        private int intPayableTypeID = 0;
        private int intCurrencyUnitID = 0;
        private int intDiscountReasonID = 0;        
        private decimal decCurrencyExchange;        
        private bool bolIsNew = false;

        private List<InputVoucherReturnDetail> lstInputVoucherReturnDetailList = new List<InputVoucherReturnDetail>();


        /// <summary>
        /// InVoiceID
        /// Số hóa đơn
        /// </summary>
        public string InVoiceID
        {
            get { return strInVoiceID; }
            set { strInVoiceID = value; }
        }

        /// <summary>
        /// InVoiceSymbol
        /// Ký hiệu hóa đơn
        /// </summary>
        public string InVoiceSymbol
        {
            get { return strInVoiceSymbol; }
            set { strInVoiceSymbol = value; }
        }

        /// <summary>
        /// Denominator
        /// Mẫu số
        /// </summary>
        public string Denominator
        {
            get { return strDenominator; }
            set { strDenominator = value; }
        }

        /// <summary>
        /// CustomerID
        /// Mã khách hàng
        /// </summary>
        public int CustomerID
        {
            get { return intCustomerID; }
            set { intCustomerID = value; }
        }

        /// <summary>
        /// CustomerName
        /// Tên khách hàng
        /// </summary>
        public string CustomerName
        {
            get { return strCustomerName; }
            set { strCustomerName = value; }
        }

        /// <summary>
        /// CustomerAddress
        /// Địa chỉ khách hàng
        /// </summary>
        public string CustomerAddress
        {
            get { return strCustomerAddress; }
            set { strCustomerAddress = value; }
        }

        /// <summary>
        /// CustomerPhone
        /// Số điện thoại khách hàng
        /// </summary>
        public string CustomerPhone
        {
            get { return strCustomerPhone; }
            set { strCustomerPhone = value; }
        }

        /// <summary>
        /// CustomerTaxID
        /// Mã số thuế khách hàng
        /// </summary>
        public string CustomerTaxID
        {
            get { return strCustomerTaxID; }
            set { strCustomerTaxID = value; }
        }

       
        /// <summary>
        /// InputTypeID
        /// Hình thức nhập
        /// </summary>
        public int InputTypeID
        {
            get { return intInputTypeID; }
            set { intInputTypeID = value; }
        }

        /// <summary>
        /// PayableTypeID
        /// Hình thức thanh toán
        /// </summary>
        public int PayableTypeID
        {
            get { return intPayableTypeID; }
            set { intPayableTypeID = value; }
        }

        /// <summary>
        /// CurrencyUnitID
        /// Loại tiền
        /// </summary>
        public int CurrencyUnitID
        {
            get { return intCurrencyUnitID; }
            set { intCurrencyUnitID = value; }
        }

        /// <summary>
        /// DiscountReasonID
        /// Lý do giảm giá
        /// </summary>
        public int DiscountReasonID
        {
            get { return intDiscountReasonID; }
            set { intDiscountReasonID = value; }
        }

        

        /// <summary>
        /// CurrencyExchange
        /// Tỷ giá
        /// </summary>
        public decimal CurrencyExchange
        {
            get { return decCurrencyExchange; }
            set { decCurrencyExchange = value; }
        }

        

        /// <summary>
        /// IsNew
        /// Là hàng mới
        /// </summary>
        public bool IsNew
        {
            get { return bolIsNew; }
            set { bolIsNew = value; }
        }

        
        public List<InputVoucherReturnDetail> InputVoucherReturnDetailList
        {
            get { return lstInputVoucherReturnDetailList; }
            set { lstInputVoucherReturnDetailList = value; }
        }
    }
}
