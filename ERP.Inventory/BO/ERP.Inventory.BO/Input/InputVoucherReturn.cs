﻿
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Input
{
    /// <summary>
    /// Created by 		: Võ Minh Hiếu 
    /// Created date 	: 12/10/2012 
    /// Bảng phiếu nhập trả hàng
    /// </summary>	
    public partial class InputVoucherReturn
    {


        #region Member Variables

        private string strInputVoucherReturnID = string.Empty;
        private string strOutputVoucherID = string.Empty;
        private string strInputVoucherID = string.Empty;
        private string strOutVoucherID = string.Empty;
        private int intReturnStoreID = 0;
        private DateTime? dtmReturnDate;
        private bool bolIsReturnWithFee = false;
        private string strReturnReason = string.Empty;
        private string strReturnNote = string.Empty;
        private decimal decDiscount;
        private decimal decTotalAmountBFT;
        private decimal decTotalVAT;
        private decimal decTotalAmount;
        private decimal decTotalVATLost;
        private decimal decTotalReturnFee;
        private int intCreatedStoreID = 0;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private bool bolIsExist = false;

        #endregion


        #region Properties

        /// <summary>
        /// InputVoucherReturnID
        /// Mã phiếu nhập trả
        /// </summary>
        public string InputVoucherReturnID
        {
            get { return strInputVoucherReturnID; }
            set { strInputVoucherReturnID = value; }
        }

        /// <summary>
        /// OutputVoucherID
        /// Mã phiếu xuất
        /// </summary>
        public string OutputVoucherID
        {
            get { return strOutputVoucherID; }
            set { strOutputVoucherID = value; }
        }

        /// <summary>
        /// InputVoucherID
        /// Mã phiếu nhập
        /// </summary>
        public string InputVoucherID
        {
            get { return strInputVoucherID; }
            set { strInputVoucherID = value; }
        }

        /// <summary>
        /// OutVoucherID
        /// Mã phiếu chi
        /// </summary>
        public string OutVoucherID
        {
            get { return strOutVoucherID; }
            set { strOutVoucherID = value; }
        }

        /// <summary>
        /// ReturnStoreID
        /// Kho nhập trả
        /// </summary>
        public int ReturnStoreID
        {
            get { return intReturnStoreID; }
            set { intReturnStoreID = value; }
        }

        /// <summary>
        /// ReturnDate
        /// Ngày nhập trả
        /// </summary>
        public DateTime? ReturnDate
        {
            get { return dtmReturnDate; }
            set { dtmReturnDate = value; }
        }

        /// <summary>
        /// IsReturnWithFee
        /// Nhập trả hàng có thu phí
        /// </summary>
        public bool IsReturnWithFee
        {
            get { return bolIsReturnWithFee; }
            set { bolIsReturnWithFee = value; }
        }

        /// <summary>
        /// ReturnReason
        /// Lý do nhập trả
        /// </summary>
        public string ReturnReason
        {
            get { return strReturnReason; }
            set { strReturnReason = value; }
        }

        /// <summary>
        /// ReturnNote
        /// Ghi chú nhập trả
        /// </summary>
        public string ReturnNote
        {
            get { return strReturnNote; }
            set { strReturnNote = value; }
        }

        /// <summary>
        /// Discount
        /// Giảm giá
        /// </summary>
        public decimal Discount
        {
            get { return decDiscount; }
            set { decDiscount = value; }
        }

        /// <summary>
        /// TotalAmountBFT
        /// Tổng tiền trước thuế
        /// </summary>
        public decimal TotalAmountBFT
        {
            get { return decTotalAmountBFT; }
            set { decTotalAmountBFT = value; }
        }

        /// <summary>
        /// TotalVAT
        /// Tổng thuế
        /// </summary>
        public decimal TotalVAT
        {
            get { return decTotalVAT; }
            set { decTotalVAT = value; }
        }

        /// <summary>
        /// TotalAmount
        /// Tổng tiền sau thuế
        /// </summary>
        public decimal TotalAmount
        {
            get { return decTotalAmount; }
            set { decTotalAmount = value; }
        }

        /// <summary>
        /// TotalVATLost
        /// Tổng số tiền trừ khi mất VAT
        /// </summary>
        public decimal TotalVATLost
        {
            get { return decTotalVATLost; }
            set { decTotalVATLost = value; }
        }

        /// <summary>
        /// TotalReturnFee
        /// Tổng tiền trừ phí nhập trả
        /// </summary>
        public decimal TotalReturnFee
        {
            get { return decTotalReturnFee; }
            set { decTotalReturnFee = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// Kho tạo phiếu
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        /// <summary>
        /// Có tồn tại không?
        /// </summary>
        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        #endregion


        #region Constructor

        public InputVoucherReturn()
        {
        }
        #endregion


        #region Column Names

        public const String colInputVoucherReturnID = "InputVoucherReturnID";
        public const String colOutputVoucherID = "OutputVoucherID";
        public const String colInputVoucherID = "InputVoucherID";
        public const String colOutVoucherID = "OutVoucherID";
        public const String colReturnStoreID = "ReturnStoreID";
        public const String colReturnDate = "ReturnDate";
        public const String colIsReturnWithFee = "IsReturnWithFee";
        public const String colReturnReason = "ReturnReason";
        public const String colReturnNote = "ReturnNote";
        public const String colDiscount = "Discount";
        public const String colTotalAmountBFT = "TotalAmountBFT";
        public const String colTotalVAT = "TotalVAT";
        public const String colTotalAmount = "TotalAmount";
        public const String colTotalVATLost = "TotalVATLost";
        public const String colTotalReturnFee = "TotalReturnFee";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";

        #endregion //Column names


    }
}


