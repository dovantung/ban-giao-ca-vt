﻿
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Input
{
    /// <summary>
    /// Created by 		: Võ Minh Hiếu 
    /// Created date 	: 12/10/2012 
    /// Chi tiết phiếu nhập trả hàng
    /// </summary>	
    public partial class InputVoucherReturnDetail
    {


        #region Member Variables

        private string strInputVoucherReturnDetailID = string.Empty;
        private string strInputVoucherReturnID = string.Empty;
        private int intReturnStoreID = 0;
        private DateTime? dtmReturnDate;
        private string strOutputVoucherID = string.Empty;
        private string strOutputVoucherDetailID = string.Empty;
        private string strInputVoucherDetailID = string.Empty;
        private string strProductID = string.Empty;
        private decimal decQuantity;
        private int intVAT = 0;
        private int intVATPercent = 0;
        private string strIMEI = string.Empty;
        private bool bolIsReturnWithFee = false;
        private decimal decSalePrice;
        private decimal decReturnFee;
        private decimal decOriginalReturnPrice;
        private decimal decTotalVATLost;
        private decimal decAdjustPrice;
        private decimal decReturnPrice;
        private int intCreatedStoreID = 0;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private bool bolIsExist = false;

        // 09/02/2015 Mi thêm dùng cho store "PM_INPUTVOUCHERRETURNDT_ADD"
        private decimal decCostPrice = 0;
        #endregion


        #region Properties
        public decimal CostPrice
        {
            get { return decCostPrice; }
            set { decCostPrice = value; }
        } 
        /// <summary>
        /// InputVoucherReturnDetailID
        /// Mã chi tiết phiếu nhập trả
        /// </summary>
        public string InputVoucherReturnDetailID
        {
            get { return strInputVoucherReturnDetailID; }
            set { strInputVoucherReturnDetailID = value; }
        }

        /// <summary>
        /// InputVoucherReturnID
        /// Mã phiếu nhập trả
        /// </summary>
        public string InputVoucherReturnID
        {
            get { return strInputVoucherReturnID; }
            set { strInputVoucherReturnID = value; }
        }

        /// <summary>
        /// ReturnStoreID
        /// Kho nhập trả
        /// </summary>
        public int ReturnStoreID
        {
            get { return intReturnStoreID; }
            set { intReturnStoreID = value; }
        }

        /// <summary>
        /// ReturnDate
        /// Ngày nhập trả
        /// </summary>
        public DateTime? ReturnDate
        {
            get { return dtmReturnDate; }
            set { dtmReturnDate = value; }
        }

        /// <summary>
        /// OutputVoucherID
        /// 
        /// </summary>
        public string OutputVoucherID
        {
            get { return strOutputVoucherID; }
            set { strOutputVoucherID = value; }
        }

        /// <summary>
        /// OutputVoucherDetailID
        /// Mã chi tiết phiếu xuất
        /// </summary>
        public string OutputVoucherDetailID
        {
            get { return strOutputVoucherDetailID; }
            set { strOutputVoucherDetailID = value; }
        }

        /// <summary>
        /// InputVoucherDetailID
        /// Mã chi tiết phiếu nhập
        /// </summary>
        public string InputVoucherDetailID
        {
            get { return strInputVoucherDetailID; }
            set { strInputVoucherDetailID = value; }
        }

        /// <summary>
        /// ProductID
        /// Mã sản phẩm
        /// </summary>
        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }

        /// <summary>
        /// Quantity
        /// Số lượng
        /// </summary>
        public decimal Quantity
        {
            get { return decQuantity; }
            set { decQuantity = value; }
        }

        /// <summary>
        /// VAT
        /// 
        /// </summary>
        public int VAT
        {
            get { return intVAT; }
            set { intVAT = value; }
        }

        /// <summary>
        /// VATPercent
        /// 
        /// </summary>
        public int VATPercent
        {
            get { return intVATPercent; }
            set { intVATPercent = value; }
        }

        /// <summary>
        /// IMEI
        /// 
        /// </summary>
        public string IMEI
        {
            get { return strIMEI; }
            set { strIMEI = value; }
        }

        /// <summary>
        /// IsReturnWithFee
        /// Là nhập trả hàng có thu phí
        /// </summary>
        public bool IsReturnWithFee
        {
            get { return bolIsReturnWithFee; }
            set { bolIsReturnWithFee = value; }
        }

        /// <summary>
        /// SalePrice
        /// Giá bán
        /// </summary>
        public decimal SalePrice
        {
            get { return decSalePrice; }
            set { decSalePrice = value; }
        }

        /// <summary>
        /// ReturnFee
        /// Phí nhập trả
        /// </summary>
        public decimal ReturnFee
        {
            get { return decReturnFee; }
            set { decReturnFee = value; }
        }

        /// <summary>
        /// OriginalReturnPrice
        /// Giá nhập trả trước điều chỉnh
        /// </summary>
        public decimal OriginalReturnPrice
        {
            get { return decOriginalReturnPrice; }
            set { decOriginalReturnPrice = value; }
        }

        /// <summary>
        /// TotalVATLost
        /// Tổng tiền trừ khi mất hóa đơn VAT
        /// </summary>
        public decimal TotalVATLost
        {
            get { return decTotalVATLost; }
            set { decTotalVATLost = value; }
        }

        /// <summary>
        /// AdjustPrice
        /// Điều chỉnh giá
        /// </summary>
        public decimal AdjustPrice
        {
            get { return decAdjustPrice; }
            set { decAdjustPrice = value; }
        }

        /// <summary>
        /// ReturnPrice
        /// Giá nhập trả
        /// </summary>
        public decimal ReturnPrice
        {
            get { return decReturnPrice; }
            set { decReturnPrice = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// 
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        /// <summary>
        /// Có tồn tại không?
        /// </summary>
        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        #endregion


        #region Constructor

        public InputVoucherReturnDetail()
        {
        }
        #endregion


        #region Column Names

        public const String colInputVoucherReturnDetailID = "InputVoucherReturnDetailID";
        public const String colInputVoucherReturnID = "InputVoucherReturnID";
        public const String colReturnStoreID = "ReturnStoreID";
        public const String colReturnDate = "ReturnDate";
        public const String colOutputVoucherID = "OutputVoucherID";
        public const String colOutputVoucherDetailID = "OutputVoucherDetailID";
        public const String colInputVoucherDetailID = "InputVoucherDetailID";
        public const String colProductID = "ProductID";
        public const String colQuantity = "Quantity";
        public const String colVAT = "VAT";
        public const String colVATPercent = "VATPercent";
        public const String colIMEI = "IMEI";
        public const String colIsReturnWithFee = "IsReturnWithFee";
        public const String colSalePrice = "SalePrice";
        public const String colReturnFee = "ReturnFee";
        public const String colOriginalReturnPrice = "OriginalReturnPrice";
        public const String colTotalVATLost = "TotalVATLost";
        public const String colAdjustPrice = "AdjustPrice";
        public const String colReturnPrice = "ReturnPrice";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";

        #endregion //Column names


    }
}


