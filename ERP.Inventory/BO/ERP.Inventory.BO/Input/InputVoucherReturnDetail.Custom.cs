﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.Input
{
    public partial class InputVoucherReturnDetail
    {
        private bool bolIsNew = true;
        private bool bolIsShowProduct = false;
        private String strAdjustPriceUser = "";
        private String strAdjustPriceContent = "";
        private String strInputVoucherID = "";

        

        public bool IsNew
        {
            get { return bolIsNew; }
            set { bolIsNew = value; }
        }
        
        public bool IsShowProduct
        {
            get { return bolIsShowProduct; }
            set { bolIsShowProduct = value; }
        }
        
        public String AdjustPriceUser
        {
            get { return strAdjustPriceUser; }
            set { strAdjustPriceUser = value; }
        }
        

        public String AdjustPriceContent
        {
            get { return strAdjustPriceContent; }
            set { strAdjustPriceContent = value; }
        }

        public String InputVoucherID
        {
            get { return strInputVoucherID; }
            set { strInputVoucherID = value; }
        }
    }
}
