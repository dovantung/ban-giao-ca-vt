
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Hoang Nhu Phong 
	/// Created date 	: 9/28/2012 
	/// Yêu cầu chuyển kho
	/// </summary>	
	public partial class StoreChangeOrder
	{
        private string strFromStoreName = string.Empty;
        private DataTable dtbUserNotify = null;
        private int intInStockStatusID = -1;
        public DataTable UserNotifyData
        {
            get { return dtbUserNotify; }
            set { dtbUserNotify = value; }
        }

        public string FromStoreName
        {
            get { return strFromStoreName; }
            set { strFromStoreName = value; }
        }
        private string strToStoreName = string.Empty;

        public string ToStoreName
        {
            get { return strToStoreName; }
            set { strToStoreName = value; }
        }
        List<StoreChangeOrderDetail> objStoreChangeOrderDetailList = new List<StoreChangeOrderDetail>();
        public List<StoreChangeOrderDetail> StoreChangeOrderDetailList
        {
            get { return objStoreChangeOrderDetailList; }
            set { objStoreChangeOrderDetailList = value; }
        }

        List<StoreChangeOrderDetailIMEI> objStoreChangeOrderDetailIMEIList = new List<StoreChangeOrderDetailIMEI>();

        public List<StoreChangeOrderDetailIMEI> StoreChangeOrderDetailIMEIList
        {
            get { return objStoreChangeOrderDetailIMEIList; }
            set { objStoreChangeOrderDetailIMEIList = value; }
        }

        private DataTable dtbStoreChangeOrderDetail = null;
        public DataTable DtbStoreChangeOrderDetail
        {
            get { return dtbStoreChangeOrderDetail; }
            set { dtbStoreChangeOrderDetail = value; }
        }
        private DataTable dtbReviewLevel = null;
        public DataTable DtbReviewLevel
        {
            get { return dtbReviewLevel; }
            set { dtbReviewLevel = value; }
        }
        private DataTable dtbAttachment = null;
        public DataTable DtbAttachment {
            get { return dtbAttachment; }
            set { dtbAttachment = value; }
        }
        private bool isExpired = false;

        public bool IsExpired
        {
            get { return isExpired; }
            set { isExpired = value; }
        }
        public int InStockStatusID
        {
            get { return intInStockStatusID; }
            set { intInStockStatusID = value; }
        }


	}
}
