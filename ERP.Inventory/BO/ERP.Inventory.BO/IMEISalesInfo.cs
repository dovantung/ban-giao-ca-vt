
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Quang Tú 
	/// Created date 	: 17/06/2013 
	/// Thông tin bán hàng của IMEI(dùng cho máy cũ, hàng trưng bày)
	/// </summary>	
	public class IMEISalesInfo
	{	
	
	
		#region Member Variables

		private string strProductID = string.Empty;
		private string strIMEI = string.Empty;
		private string strInputVoucherDetailID = string.Empty;
		private int intStoreID = 0;
		private bool bolIsBrandNewWarranty = false;
		private DateTime? dtmEndWarrantyDate;
		private string strNote = string.Empty;
		private string strPrintVATContent = string.Empty;
		private bool bolIsConfirm = false;
		private bool bolIsReviewed = false;
		private string strReviewedUser = string.Empty;
		private DateTime? dtmReviewedDate;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
        private string strWebProductSpecContent = string.Empty;
		private bool bolIsExist = false;
        private int intOrderIndex = 0;
        private List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec = new List<IMEISalesInfo_ProSpec>();
        private List<IMEISalesInfo_Images> lstIMEISalesInfo_Images = new List<IMEISalesInfo_Images>();

		#endregion


		#region Properties 
        
        /// <summary>
        /// Tao list thong tin sp
        /// </summary>
        public List<IMEISalesInfo_ProSpec> IMEISalesInfo_ProSpec
        {
            get { return this.lstIMEISalesInfo_ProSpec; }
            set { this.lstIMEISalesInfo_ProSpec = value; }
        }

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// 
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// InputVoucherDetailID
		/// 
		/// </summary>
		public string InputVoucherDetailID
		{
			get { return  strInputVoucherDetailID; }
			set { strInputVoucherDetailID = value; }
		}

		/// <summary>
		/// StoreID
		/// 
		/// </summary>
		public int StoreID
		{
			get { return  intStoreID; }
			set { intStoreID = value; }
		}

		/// <summary>
		/// IsBrandNewWarranty
		/// 
		/// </summary>
		public bool IsBrandNewWarranty
		{
			get { return  bolIsBrandNewWarranty; }
			set { bolIsBrandNewWarranty = value; }
		}

		/// <summary>
		/// EndWarrantyDate
		/// 
		/// </summary>
		public DateTime? EndWarrantyDate
		{
			get { return  dtmEndWarrantyDate; }
			set { dtmEndWarrantyDate = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// PrintVATContent
		/// Nội dung in trên hóa đơn VAT
		/// </summary>
		public string PrintVATContent
		{
			get { return  strPrintVATContent; }
			set { strPrintVATContent = value; }
		}

		/// <summary>
		/// IsConfirm
		/// 
		/// </summary>
		public bool IsConfirm
		{
			get { return  bolIsConfirm; }
			set { bolIsConfirm = value; }
		}

		/// <summary>
		/// IsReviewed
		/// 
		/// </summary>
		public bool IsReviewed
		{
			get { return  bolIsReviewed; }
			set { bolIsReviewed = value; }
		}

		/// <summary>
		/// ReviewedUser
		/// 
		/// </summary>
		public string ReviewedUser
		{
			get { return  strReviewedUser; }
			set { strReviewedUser = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// 
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}

        public int OrderIndex
        {
            get { return intOrderIndex; }
            set { intOrderIndex = value; }
        }

        public string WebProductSpecContent
        {
            get { return strWebProductSpecContent; }
            set { strWebProductSpecContent = value; }
        }

        public List<IMEISalesInfo_Images> IMEISalesInfo_ImagesList
        {
            get { return lstIMEISalesInfo_Images; }
            set { lstIMEISalesInfo_Images = value; }
        }
		#endregion			
		
		
		#region Constructor

		public IMEISalesInfo()
		{
		}
		public IMEISalesInfo(string strProductID, string strIMEI, string strInputVoucherDetailID, int intStoreID, bool bolIsBrandNewWarranty, DateTime? dtmEndWarrantyDate, string strNote, string strPrintVATContent, bool bolIsConfirm, bool bolIsReviewed, string strReviewedUser, DateTime? dtmReviewedDate, string strCreatedUser, DateTime? dtmCreatedDate, string strUpdatedUser, DateTime? dtmUpdatedDate)
		{
			this.strProductID = strProductID;
            this.strIMEI = strIMEI;
            this.strInputVoucherDetailID = strInputVoucherDetailID;
            this.intStoreID = intStoreID;
            this.bolIsBrandNewWarranty = bolIsBrandNewWarranty;
            this.dtmEndWarrantyDate = dtmEndWarrantyDate;
            this.strNote = strNote;
            this.strPrintVATContent = strPrintVATContent;
            this.bolIsConfirm = bolIsConfirm;
            this.bolIsReviewed = bolIsReviewed;
            this.strReviewedUser = strReviewedUser;
            this.dtmReviewedDate = dtmReviewedDate;
            this.strCreatedUser = strCreatedUser;
            this.dtmCreatedDate = dtmCreatedDate;
            this.strUpdatedUser = strUpdatedUser;
            this.dtmUpdatedDate = dtmUpdatedDate;
            
		}
		#endregion


		#region Column Names

		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
		public const String colInputVoucherDetailID = "InputVoucherDetailID";
		public const String colStoreID = "StoreID";
		public const String colIsBrandNewWarranty = "IsBrandNewWarranty";
		public const String colEndWarrantyDate = "EndWarrantyDate";
		public const String colNote = "Note";
		public const String colPrintVATContent = "PrintVATContent";
		public const String colIsConfirm = "IsConfirm";
		public const String colIsReviewed = "IsReviewed";
		public const String colReviewedUser = "ReviewedUser";
		public const String colReviewedDate = "ReviewedDate";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
        public const String colWebProductSpecContent = "WebProductSpecContent";
        public const String colOrderIndex = "OrderIndex";
		#endregion Column names

		
	}
}
