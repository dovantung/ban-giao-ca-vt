
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.InputChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/17/2012 
	/// Bảng phiếu nhập đổi hàng
	/// </summary>	
	public class InputChange
	{	
	
	
		#region Member Variables

		private string strInputChangeID = string.Empty;
		private DateTime? dtmInputChangeDate;
		private int intInputChangeStoreID = 0;
		private string strOldOutputVoucherID = string.Empty;
		private string strNewInputVoucherID = string.Empty;
		private string strNewOutputVoucherID = string.Empty;
		private int intInputChangeTypeID = 0;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private List<InputChangeDetail> objInputChangeDetailList = new List<InputChangeDetail>();
        private ERP.Inventory.BO.InputVoucher objInputVoucherBO = new ERP.Inventory.BO.InputVoucher();
        private ERP.Inventory.BO.OutputVoucher objOutputVoucherBO = new ERP.Inventory.BO.OutputVoucher();

		#endregion


		#region Properties 

		/// <summary>
		/// InputChangeID
		/// Mã phiếu nhập đổi
		/// </summary>
		public string InputChangeID
		{
			get { return  strInputChangeID; }
			set { strInputChangeID = value; }
		}

		/// <summary>
		/// InputChangeDate
		/// Ngày nhập đổi
		/// </summary>
		public DateTime? InputChangeDate
		{
			get { return  dtmInputChangeDate; }
			set { dtmInputChangeDate = value; }
		}

		/// <summary>
		/// InputChangeStoreID
		/// Kho nhập đổi
		/// </summary>
		public int InputChangeStoreID
		{
			get { return  intInputChangeStoreID; }
			set { intInputChangeStoreID = value; }
		}

		/// <summary>
		/// OldOutputVoucherID
		/// Mã phiếu xuất cũ
		/// </summary>
		public string OldOutputVoucherID
		{
			get { return  strOldOutputVoucherID; }
			set { strOldOutputVoucherID = value; }
		}

		/// <summary>
		/// NewInputVoucherID
		/// Mã phiếu nhập mới
		/// </summary>
		public string NewInputVoucherID
		{
			get { return  strNewInputVoucherID; }
			set { strNewInputVoucherID = value; }
		}

		/// <summary>
		/// NewOutputVoucherID
		/// Mã phiếu xuất mới
		/// </summary>
		public string NewOutputVoucherID
		{
			get { return  strNewOutputVoucherID; }
			set { strNewOutputVoucherID = value; }
		}

		/// <summary>
		/// InputChangeTypeID
		/// Loại nhập đổi(Chưa dùng)
		/// </summary>
		public int InputChangeTypeID
		{
			get { return  intInputChangeTypeID; }
			set { intInputChangeTypeID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// Kho tạo
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        public List<InputChangeDetail> InputChangeDetailList
        {
            get { return objInputChangeDetailList; }
            set { objInputChangeDetailList = value; }
        }

        public ERP.Inventory.BO.InputVoucher InputVoucherBO
        {
            get { return objInputVoucherBO; }
            set { objInputVoucherBO = value; }
        }

        public ERP.Inventory.BO.OutputVoucher OutputVoucherBO
        {
            get { return objOutputVoucherBO; }
            set { objOutputVoucherBO = value; }
        }


		#endregion			
		
		
		#region Constructor

		public InputChange()
		{
		}
		#endregion


		#region Column Names

		public const String colInputChangeID = "InputChangeID";
		public const String colInputChangeDate = "InputChangeDate";
		public const String colInputChangeStoreID = "InputChangeStoreID";
		public const String colOldOutputVoucherID = "OldOutputVoucherID";
		public const String colNewInputVoucherID = "NewInputVoucherID";
		public const String colNewOutputVoucherID = "NewOutputVoucherID";
		public const String colInputChangeTypeID = "InputChangeTypeID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
