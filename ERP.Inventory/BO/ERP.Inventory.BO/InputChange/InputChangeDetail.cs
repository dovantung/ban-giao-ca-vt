
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.InputChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/17/2012 
	/// Chi tiết phiếu nhập đổi hàng
	/// </summary>	
	public class InputChangeDetail
	{	
	
	
		#region Member Variables

		private string strInputChangeDetailID = string.Empty;
		private string strInputChangeID = string.Empty;
		private DateTime? dtmInputChangeDate;
		private int intInputChangeStoreID = 0;
		private string strOldOutputVoucherDetailID = string.Empty;
		private string strProductID_In = string.Empty;
		private decimal decQuantity;
		private string strIMEI_In = string.Empty;
		private string strProductID_Out = string.Empty;
		private string strIMEI_Out = string.Empty;
		private string strNewInputVoucherDetailID = string.Empty;
		private string strNewOutputVoucherDetailID = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private int intOutputTypeID = 0;
        private int intInputTypeID = 0;

		#endregion


		#region Properties 

		/// <summary>
		/// InputChangeDetailID
		/// Mã chi tiết nhập trả hàng
		/// </summary>
		public string InputChangeDetailID
		{
			get { return  strInputChangeDetailID; }
			set { strInputChangeDetailID = value; }
		}

		/// <summary>
		/// InputChangeID
		/// Mã phiếu nhập trả
		/// </summary>
		public string InputChangeID
		{
			get { return  strInputChangeID; }
			set { strInputChangeID = value; }
		}

		/// <summary>
		/// InputChangeDate
		/// Ngày nhập trả
		/// </summary>
		public DateTime? InputChangeDate
		{
			get { return  dtmInputChangeDate; }
			set { dtmInputChangeDate = value; }
		}

		/// <summary>
		/// InputChangeStoreID
		/// Kho nhập trả 
		/// </summary>
		public int InputChangeStoreID
		{
			get { return  intInputChangeStoreID; }
			set { intInputChangeStoreID = value; }
		}

		/// <summary>
		/// OldOutputVoucherDetailID
		/// Mã chi tiết phiếu xuất cũ
		/// </summary>
		public string OldOutputVoucherDetailID
		{
			get { return  strOldOutputVoucherDetailID; }
			set { strOldOutputVoucherDetailID = value; }
		}

		/// <summary>
		/// ProductID_In
		/// 
		/// </summary>
		public string ProductID_In
		{
			get { return  strProductID_In; }
			set { strProductID_In = value; }
		}

		/// <summary>
		/// Quantity
		/// Số lượng
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// IMEI_In
		/// IMEI nhập
		/// </summary>
		public string IMEI_In
		{
			get { return  strIMEI_In; }
			set { strIMEI_In = value; }
		}

		/// <summary>
		/// ProductID_Out
		/// Mã sản phẩm xuất đổi
		/// </summary>
		public string ProductID_Out
		{
			get { return  strProductID_Out; }
			set { strProductID_Out = value; }
		}

		/// <summary>
		/// IMEI_Out
		/// IMEI xuất đổi
		/// </summary>
		public string IMEI_Out
		{
			get { return  strIMEI_Out; }
			set { strIMEI_Out = value; }
		}

		/// <summary>
		/// NewInputVoucherDetailID
		/// Mã chi tiết phiếu nhập mới
		/// </summary>
		public string NewInputVoucherDetailID
		{
			get { return  strNewInputVoucherDetailID; }
			set { strNewInputVoucherDetailID = value; }
		}

		/// <summary>
		/// NewOutputVoucherDetailID
		/// Mã chi tiết phiếu xuất mới
		/// </summary>
		public string NewOutputVoucherDetailID
		{
			get { return  strNewOutputVoucherDetailID; }
			set { strNewOutputVoucherDetailID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// Kho tạo
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        public int OutputTypeID
        {
            get { return intOutputTypeID; }
            set { intOutputTypeID = value; }
        }

        public int InputTypeID
        {
            get { return intInputTypeID; }
            set { intInputTypeID = value; }
        }

		#endregion			
		
		
		#region Constructor

		public InputChangeDetail()
		{
		}
		#endregion


		#region Column Names

		public const String colInputChangeDetailID = "InputChangeDetailID";
		public const String colInputChangeID = "InputChangeID";
		public const String colInputChangeDate = "InputChangeDate";
		public const String colInputChangeStoreID = "InputChangeStoreID";
		public const String colOldOutputVoucherDetailID = "OldOutputVoucherDetailID";
		public const String colProductID_In = "ProductID_In";
		public const String colQuantity = "Quantity";
		public const String colIMEI_In = "IMEI_In";
		public const String colProductID_Out = "ProductID_Out";
		public const String colIMEI_Out = "IMEI_Out";
		public const String colNewInputVoucherDetailID = "NewInputVoucherDetailID";
		public const String colNewOutputVoucherDetailID = "NewOutputVoucherDetailID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
