﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.BorrowProduct
{
    /// <summary>
    /// Created by 		: Người tạo 
    /// Created date 	: 06-Dec-16 
    /// Yêu cầu xuất đổi - Mức duyệt
    /// </summary>	
    public class BorrowProduct_RVL
    {
        #region Member Variables

        private string strBorrowProductRVLID = string.Empty;
        private string strBorrowProductID = string.Empty;
        private int intReviewLevelID = 0;
        private int intReviewType = 0;
        private string strUserName = string.Empty;
        private int intReviewedStatus = 0;
        private DateTime? dtmReviewedDate;
        private string strNote = string.Empty;
        private int intSequenceReview = 0;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private bool bolIsDeleted = false;
        private bool bolIsReviewedCheckInByFinger = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;

        #endregion


        #region Properties

        /// <summary>
        /// BORRowProductRVLID
        /// Mã chi tiết - Mức duyệt
        /// </summary>
        public string BorrowProductRVLID
        {
            get { return strBorrowProductRVLID; }
            set { strBorrowProductRVLID = value; }
        }

        /// <summary>
        /// BORRowProductID
        /// Mã yêu cầu
        /// </summary>
        public string BorrowProductID
        {
            get { return strBorrowProductID; }
            set { strBorrowProductID = value; }
        }

        /// <summary>
        /// ReviewLevelID
        /// Mã mức duyệt
        /// </summary>
        public int ReviewLevelID
        {
            get { return intReviewLevelID; }
            set { intReviewLevelID = value; }
        }

        /// <summary>
        /// ReviewType
        /// Hình thức duyệt(0: Tất cả phải duyệt, 1:Chỉ cần một người duyệt) 
        /// </summary>
        public int ReviewType
        {
            get { return intReviewType; }
            set { intReviewType = value; }
        }

        /// <summary>
        /// UserName
        /// Nhân viên duyệt
        /// </summary>
        public string UserName
        {
            get { return strUserName; }
            set { strUserName = value; }
        }

        /// <summary>
        /// ReviewedStatus
        /// Trạng thái duyệt: -1-chưa duyệt; 0-Từ chối; 1-Đồng ý
        /// </summary>
        public int ReviewedStatus
        {
            get { return intReviewedStatus; }
            set { intReviewedStatus = value; }
        }

        /// <summary>
        /// ReviewedDate
        /// Ngày duyệt
        /// </summary>
        public DateTime? ReviewedDate
        {
            get { return dtmReviewedDate; }
            set { dtmReviewedDate = value; }
        }

        /// <summary>
        /// Note
        /// Ghi chú
        /// </summary>
        public string Note
        {
            get { return strNote; }
            set { strNote = value; }
        }

        /// <summary>
        /// SEQUENCEReview
        /// Thứ tự duyệt
        /// </summary>
        public int SequenceReview
        {
            get { return intSequenceReview; }
            set { intSequenceReview = value; }
        }

        /// <summary>
        /// CreatedUser
        /// Người tạo
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// Ngày tạo
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// Đã xóa
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }
        public bool IsReviewedCheckInByFinger
        {
            get { return bolIsReviewedCheckInByFinger; }
            set { bolIsReviewedCheckInByFinger = value; }
        }

        /// <summary>
        /// DeletedUser
        /// Nhân viên xóa
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// Ngày xóa
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }


        #endregion


        #region Constructor

        public BorrowProduct_RVL()
        {
        }
        #endregion


        #region Column Names

        public const String colBORRowProductRVLID = "BorrowProductRVLID";
        public const String colBORRowProductID = "BorrowProductID";
        public const String colReviewLevelID = "ReviewLevelID";
        public const String colReviewType = "ReviewType";
        public const String colUserName = "UserName";
        public const String colReviewedStatus = "ReviewedStatus";
        public const String colReviewedDate = "ReviewedDate";
        public const String colNote = "Note";
        public const String colSEQUENCEReview = "SequenceReview";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colIsReviewedCheckInByFinger = "ISREVIEWEDCHECKINBYFINGER";
        #endregion //Column names
    }
}
