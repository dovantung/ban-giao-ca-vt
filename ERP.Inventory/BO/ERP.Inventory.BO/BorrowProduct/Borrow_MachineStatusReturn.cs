
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.BorrowProduct
{
    /// <summary>
	/// Created by 		: LÊ VĂN ĐÔNG 
	/// Created date 	: 07/03/2017 
	/// Tình trạng máy sau khi trả lại hàng mượn
	/// </summary>	
	public class Borrow_MachineStatusReturn
    {


        #region Member Variables

        private int intMachineStatusID = 0;
        private string strMachineStatusName = string.Empty;

        #endregion

    
        #region Properties 

        /// <summary>
        /// MachineStatusID
        /// Mã
        /// </summary>
        public int MachineStatusID
        {
            get { return intMachineStatusID; }
            set { intMachineStatusID = value; }
        }

        /// <summary>
        /// MachineStatusName
        /// Tên
        /// </summary>
        public string MachineStatusName
        {
            get { return strMachineStatusName; }
            set { strMachineStatusName = value; }
        }


        #endregion


        #region Constructor

        public Borrow_MachineStatusReturn()
        {
        }
        #endregion


        #region Column Names

        public const String colMachineStatusID = "MachineSTATUSID";
        public const String colMachineStatusName = "MachineSTATUSNAME";

        #endregion //Column names


    }
}