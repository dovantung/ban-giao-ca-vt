
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.BorrowProduct
{
    /// <summary>
	/// Created by 		: LE VAN DONG 
	/// Created date 	: 06/15/2017 
	/// Phụ kiện mượn kèm
	/// </summary>	
	public class BorrowProductDT_Accessory
	{	
	
	
		#region Member Variables

		private string strBorrowProductDTID = string.Empty;
		private int intMachineAccessoryID = 0;
		private string strNote = string.Empty;
        private int intStatus = -1;
		#endregion


		#region Properties 

		/// <summary>
		/// BorrowProductDTID
		/// Mã phiếu mượn
		/// </summary>
		public string BorrowProductDTID
		{
			get { return  strBorrowProductDTID; }
			set { strBorrowProductDTID = value; }
		}

        /// <summary>
        /// MachineAccessoryID
        /// Mã phụ kiện
        /// </summary>
        public int MachineAccessoryID
        {
			get { return intMachineAccessoryID; }
			set { intMachineAccessoryID = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

        /// <summary>
        /// Trạng thái: -1: khong thay doi
        /// 0: Insert
        /// 1: Update
        /// 2: Delete
        /// </summary>
        public int Status
        {
            get { return intStatus; }
            set { intStatus = value; }
        }
		#endregion			
		
		
		#region Constructor

		public BorrowProductDT_Accessory()
		{
		}
		#endregion


		#region Column Names

		public const String colBorrowProductDTID = "BorrowProductDTID";
		public const String colMachineAccessoryID = "MachineAccessoryID";
		public const String colNote = "Note";

		#endregion //Column names

		
	}
}
