
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.BorrowProduct
{
    /// <summary>
	/// Created by 		: Trung Hiếu 
	/// Created date 	: 07/11/2015 
	/// Chứng từ mượn hàng hóa - chi tiết
	/// </summary>	
	public class BorrowProductDT
	{	
	
	
		#region Member Variables

		private string strBorrowProductDTID = string.Empty;
		private string strBorrowProductID = string.Empty;
		private string strProductID = string.Empty;
		private string strIMEI = string.Empty;
		private int intIsNew = 0;
		private decimal decQuantity;
		private string strNote = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private bool bolIsExist = false;


        // LE VAN DONG BO SUNG
        private string strMachineStatus = string.Empty;
        private string strMachineStatusReturn = string.Empty;
        private List<BorrowProductDT_Accessory> lstBorrowProductDT_Accessory = new List<BorrowProductDT_Accessory>();
        #endregion


        #region Properties 

        

        /// <summary>
        /// BorrowProductDTID
        /// Mã chi tiêt chứng từ - Khóa chính(SYS_GUID())
        /// </summary>
        public string BorrowProductDTID
		{
			get { return  strBorrowProductDTID; }
			set { strBorrowProductDTID = value; }
		}

		/// <summary>
		/// BorrowProductID
		/// Mã chứng từ
		/// </summary>
		public string BorrowProductID
		{
			get { return  strBorrowProductID; }
			set { strBorrowProductID = value; }
		}

		/// <summary>
		/// ProductID
		/// Mã sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// IMEI
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// IsNew
		/// Sản phẩm mới
		/// </summary>
		public int IsNew
		{
			get { return  intIsNew; }
			set { intIsNew = value; }
		}

		/// <summary>
		/// Quantity
		/// Số lượng mượn
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// Note
		/// Ghi chú
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        // LE VAN DONG - 15/6/2017
        public List<BorrowProductDT_Accessory> BorrowProductDT_AccessoryList
        {
            get { return lstBorrowProductDT_Accessory; }
            set { lstBorrowProductDT_Accessory = value; }
        }
        /// <summary>
        /// Tình trạng của thiết bị cho mượn
        /// </summary>
        public string MachineStatus
        {
            get { return strMachineStatus; }
            set { strMachineStatus = value; }
        }


        /// <summary>
        /// MachineStatusReturn
        /// Tình trạng máy sau khi trả lại hàng mượn
        /// </summary>
        public string MachineStatusReturn
        {
            get { return strMachineStatusReturn; }
            set { strMachineStatusReturn = value; }
        }
        #endregion


        #region Constructor

        public BorrowProductDT()
		{
		}
		#endregion


		#region Column Names

		public const String colBorrowProductDTID = "BorrowProductDTID";
		public const String colBorrowProductID = "BorrowProductID";
		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
		public const String colIsNew = "IsNew";
		public const String colQuantity = "Quantity";
		public const String colNote = "Note";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colMachineStatus = "MachineStatus";
        public const String colMachineStatusReturn = "MachineStatusReturn";
        #endregion //Column names


    }
}
