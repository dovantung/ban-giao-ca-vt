
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.BorrowProduct
{
    /// <summary>
	/// Created by 		: Trung Hiếu 
	/// Created date 	: 07/11/2015 
	/// Chứng từ mượn hàng hóa
	/// </summary>	
	public class BorrowProduct
	{	
	
	
		#region Member Variables

		private string strBorrowProductID = string.Empty;
		private DateTime? dtmBorrowDate;
		private string strBorrowNote = string.Empty;
		private string strBorrowUser = string.Empty;
		private int intBorrowStoreID = 0;
		private int intBorrowStatus = 0;
        private int intReviewedStatus = 3;
		private bool bolIsFinishED = false;
		private DateTime? dtmFinishEDDate;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private DateTime? dtmReturnPlanDate;
        private bool bolIsExist = false;
        private string strDeletedReason = string.Empty;

        private DataTable dtbBorrowProduct_Delivery = null;
        private DataTable dtbBorrowProductDT = null;
        private DataTable dtbBorrowProductDT_Accesory = null;

        List<BorrowProduct_Delivery> objBorrowProduct_DeliveryList = new List<BorrowProduct_Delivery>();
        List<BorrowProductDT> objBorrowProductDTList = new List<BorrowProductDT>();
        List<BorrowProduct_RVL> objBorrowProduct_RVLList = new List<BorrowProduct_RVL>();
        private int intBorrowProductTypeID = 0;

        // LE VAN DONG - BO SUNG 10/06/2017
        private string strRepairOrderId = string.Empty;
		#endregion


		#region Properties 

		/// <summary>
		/// BorrowProductID
		/// Mã chứng từ(Sinh theo cấu trúc)
		/// </summary>
		public string BorrowProductID
		{
			get { return  strBorrowProductID; }
			set { strBorrowProductID = value; }
		}

		/// <summary>
		/// BorrowDate
		/// Ngày mượn
		/// </summary>
		public DateTime? BorrowDate
		{
			get { return  dtmBorrowDate; }
			set { dtmBorrowDate = value; }
		}

		/// <summary>
		/// BorrowNote
		/// Nội dung mượn
		/// </summary>
		public string BorrowNote
		{
			get { return  strBorrowNote; }
			set { strBorrowNote = value; }
		}

		/// <summary>
		/// BorrowUser
		/// Nhân viên mượn
		/// </summary>
		public string BorrowUser
		{
			get { return  strBorrowUser; }
			set { strBorrowUser = value; }
		}

		/// <summary>
		/// BorrowStoreID
		/// Kho mượn
		/// </summary>
		public int BorrowStoreID
		{
			get { return  intBorrowStoreID; }
			set { intBorrowStoreID = value; }
		}

		/// <summary>
		/// BorrowStatus
		/// Trạng thái mượn; 0: Tạo mới, 1: Đã mượn, 2: Đã trả
		/// </summary>
		public int BorrowStatus
		{
			get { return  intBorrowStatus; }
			set { intBorrowStatus = value; }
		}

        public int ReviewedStatus
        {
            get { return intReviewedStatus; }
            set { intReviewedStatus = value; }
        }

		/// <summary>
		/// IsFinishED
		/// Đã hoàn thành
		/// </summary>
		public bool IsFinishED
		{
			get { return  bolIsFinishED; }
			set { bolIsFinishED = value; }
		}

		/// <summary>
		/// FinishEDDate
		/// Ngày hoàn thành
		/// </summary>
		public DateTime? FinishEDDate
		{
			get { return  dtmFinishEDDate; }
			set { dtmFinishEDDate = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        /// <summary>
        /// ReturnPlanDate
        /// Ngày trả
        /// </summary>
        public DateTime? ReturnPlanDate
        {
            get { return dtmReturnPlanDate; }
            set { dtmReturnPlanDate = value; }
        }

        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }

        public DataTable DtbBorrowProduct_Delivery
        {
            get { return dtbBorrowProduct_Delivery; }
            set { dtbBorrowProduct_Delivery = value; }
        }

        public DataTable DtbBorrowProductDT
        {
            get { return dtbBorrowProductDT; }
            set { dtbBorrowProductDT = value; }
        }

        public DataTable DtbBorrowProductDT_Accesory
        {
            get { return dtbBorrowProductDT_Accesory; }
            set { dtbBorrowProductDT_Accesory = value; }
        }
        
        public List<BorrowProduct_Delivery> BorrowProduct_DeliveryList
        {
            get { return objBorrowProduct_DeliveryList; }
            set { objBorrowProduct_DeliveryList = value; }
        }

        public List<BorrowProductDT> BorrowProductDTList
        {
            get { return objBorrowProductDTList; }
            set { objBorrowProductDTList = value; }
        }

        public int BorrowProductTypeID
        {
            get { return intBorrowProductTypeID; }
            set { intBorrowProductTypeID = value; }
        }
        public List<BorrowProduct_RVL> BorrowProduct_RVLList
        {
            get { return objBorrowProduct_RVLList; }
            set { objBorrowProduct_RVLList = value; }
        }
        public string DeletedReason
        {
            get { return strDeletedReason; }
            set { strDeletedReason = value; }
        }

        public string RepairOrderID
        {
            get { return strRepairOrderId; }
            set { strRepairOrderId = value; }
        }
		#endregion			
		
		
		#region Constructor

		public BorrowProduct()
		{
		}
		#endregion


		#region Column Names

		public const String colBorrowProductID = "BorrowProductID";
		public const String colBorrowDate = "BorrowDate";
		public const String colBorrowNote = "BorrowNote";
		public const String colBorrowUser = "BorrowUser";
		public const String colBorrowStoreID = "BorrowStoreID";
		public const String colBorrowStatus = "BorrowStatus";
		public const String colIsFinishED = "IsFinishED";
		public const String colFinishEDDate = "FinishEDDate";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colReturnPlanDate = "ReturnPlanDate";
        public const String colBorrowProductTypeID = "BorrowProductTypeID";
        public const String colReviewedStatus = "ReviewedStatus";
        public const String colDeletedReason = "DeletedReason";
        public const String colRepairOrderID = "RepairOrderID";
        

        #endregion //Column names


    }
}
