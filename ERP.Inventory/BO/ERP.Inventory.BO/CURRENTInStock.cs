
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// 
	/// </summary>	
	public class CurrentInStock
	{	
	
	
		#region Member Variables

		private int intStoreID = 0;
		private string strProductID = string.Empty;
		private decimal decQuantity;
		private decimal decISOLDQuantity;
		private decimal decIsShowProductQuantity;
		private decimal decOLDShowProductQuantity;
		private bool bolIsExist = false;

		#endregion


		#region Properties 

		/// <summary>
		/// StoreID
		/// 
		/// </summary>
		public int StoreID
		{
			get { return  intStoreID; }
			set { intStoreID = value; }
		}

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// Quantity
		/// 
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// ISOLDQuantity
		/// 
		/// </summary>
		public decimal ISOLDQuantity
		{
			get { return  decISOLDQuantity; }
			set { decISOLDQuantity = value; }
		}

		/// <summary>
		/// IsShowProductQuantity
		/// 
		/// </summary>
		public decimal IsShowProductQuantity
		{
			get { return  decIsShowProductQuantity; }
			set { decIsShowProductQuantity = value; }
		}

		/// <summary>
		/// OLDShowProductQuantity
		/// 
		/// </summary>
		public decimal OLDShowProductQuantity
		{
			get { return  decOLDShowProductQuantity; }
			set { decOLDShowProductQuantity = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}

		#endregion			
		
		
		#region Constructor

		public CurrentInStock()
		{
		}
		public CurrentInStock(int intStoreID, string strProductID, decimal decQuantity, decimal decISOLDQuantity, decimal decIsShowProductQuantity, decimal decOLDShowProductQuantity)
		{
			this.intStoreID = intStoreID;
            this.strProductID = strProductID;
            this.decQuantity = decQuantity;
            this.decISOLDQuantity = decISOLDQuantity;
            this.decIsShowProductQuantity = decIsShowProductQuantity;
            this.decOLDShowProductQuantity = decOLDShowProductQuantity;
            
		}
		#endregion


		#region Column Names

		public const String colStoreID = "StoreID";
		public const String colProductID = "ProductID";
		public const String colQuantity = "Quantity";
		public const String colISOLDQuantity = "ISOLDQuantity";
		public const String colIsShowProductQuantity = "IsShowProductQuantity";
		public const String colOLDShowProductQuantity = "OLDShowProductQuantity";

		#endregion //Column names

		
	}
}
