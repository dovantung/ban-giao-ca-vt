
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Danh sách bình luận của yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public class InputChangeOrder_Comment
	{	
	
	
		#region Member Variables

		private string strCommentID = string.Empty;
		private string strInputChangeOrderID = string.Empty;
		private DateTime? dtmInputChangeOrderDate;
		private DateTime? dtmCommentDate;
		private string strCommentTitle = string.Empty;
		private string strCommentContent = string.Empty;
		private string strCommentUser = string.Empty;
		private bool bolIsActive = false;
		private bool bolIsSystem = false;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;

		#endregion


		#region Properties 

		/// <summary>
		/// CommentID
		/// 
		/// </summary>
		public string CommentID
		{
			get { return  strCommentID; }
			set { strCommentID = value; }
		}

		/// <summary>
		/// InputChangeOrderID
		/// 
		/// </summary>
		public string InputChangeOrderID
		{
			get { return  strInputChangeOrderID; }
			set { strInputChangeOrderID = value; }
		}

		/// <summary>
		/// InputChangeOrderDate
		/// 
		/// </summary>
		public DateTime? InputChangeOrderDate
		{
			get { return  dtmInputChangeOrderDate; }
			set { dtmInputChangeOrderDate = value; }
		}

		/// <summary>
		/// CommentDate
		/// 
		/// </summary>
		public DateTime? CommentDate
		{
			get { return  dtmCommentDate; }
			set { dtmCommentDate = value; }
		}

		/// <summary>
		/// CommentTitle
		/// 
		/// </summary>
		public string CommentTitle
		{
			get { return  strCommentTitle; }
			set { strCommentTitle = value; }
		}

		/// <summary>
		/// CommentContent
		/// 
		/// </summary>
		public string CommentContent
		{
			get { return  strCommentContent; }
			set { strCommentContent = value; }
		}

		/// <summary>
		/// CommentUser
		/// 
		/// </summary>
		public string CommentUser
		{
			get { return  strCommentUser; }
			set { strCommentUser = value; }
		}

		/// <summary>
		/// IsActive
		/// 
		/// </summary>
		public bool IsActive
		{
			get { return  bolIsActive; }
			set { bolIsActive = value; }
		}

		/// <summary>
		/// IsSystem
		/// 
		/// </summary>
		public bool IsSystem
		{
			get { return  bolIsSystem; }
			set { bolIsSystem = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public InputChangeOrder_Comment()
		{
		}
		#endregion


		#region Column Names

		public const String colCommentID = "CommentID";
		public const String colInputChangeOrderID = "InputChangeOrderID";
		public const String colInputChangeOrderDate = "InputChangeOrderDate";
		public const String colCommentDate = "CommentDate";
		public const String colCommentTitle = "CommentTitle";
		public const String colCommentContent = "CommentContent";
		public const String colCommentUser = "CommentUser";
		public const String colIsActive = "IsActive";
		public const String colIsSystem = "IsSystem";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
