
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Danh sách file đính kèm của yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public class InputChangeOrder_Attachment
	{	
	
	
		#region Member Variables

		private string strAttachmentID = string.Empty;
		private string strInputChangeOrderID = string.Empty;
		private DateTime? dtmInputChangeOrderDate;
		private string strAttachmentName = string.Empty;
		private string strAttachmentPath = string.Empty;
		private string strFileID = string.Empty;
		private string strDescription = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;

		#endregion


		#region Properties 

		/// <summary>
		/// AttachmentID
		/// 
		/// </summary>
		public string AttachmentID
		{
			get { return  strAttachmentID; }
			set { strAttachmentID = value; }
		}

		/// <summary>
		/// InputChangeOrderID
		/// 
		/// </summary>
		public string InputChangeOrderID
		{
			get { return  strInputChangeOrderID; }
			set { strInputChangeOrderID = value; }
		}

		/// <summary>
		/// InputChangeOrderDate
		/// 
		/// </summary>
		public DateTime? InputChangeOrderDate
		{
			get { return  dtmInputChangeOrderDate; }
			set { dtmInputChangeOrderDate = value; }
		}

		/// <summary>
		/// AttachmentName
		/// 
		/// </summary>
		public string AttachmentName
		{
			get { return  strAttachmentName; }
			set { strAttachmentName = value; }
		}

		/// <summary>
		/// AttachmentPath
		/// 
		/// </summary>
		public string AttachmentPath
		{
			get { return  strAttachmentPath; }
			set { strAttachmentPath = value; }
		}

		/// <summary>
		/// FileID
		/// 
		/// </summary>
		public string FileID
		{
			get { return  strFileID; }
			set { strFileID = value; }
		}

		/// <summary>
		/// Description
		/// 
		/// </summary>
		public string Description
		{
			get { return  strDescription; }
			set { strDescription = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        public bool IsAddNew { get; set; }

        public int STT { get; set; }

        public string CreatedFullName { get; set; }

        public string AttachmentLocalPath { get; set; }

		#endregion			
		
		
		#region Constructor

		public InputChangeOrder_Attachment()
		{
		}
		#endregion


		#region Column Names

		public const String colAttachmentID = "AttachmentID";
		public const String colInputChangeOrderID = "InputChangeOrderID";
		public const String colInputChangeOrderDate = "InputChangeOrderDate";
		public const String colAttachmentName = "AttachmentName";
		public const String colAttachmentPath = "AttachmentPath";
		public const String colFileID = "FileID";
		public const String colDescription = "Description";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
