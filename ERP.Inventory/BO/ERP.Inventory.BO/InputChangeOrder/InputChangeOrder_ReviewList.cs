
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Danh sách người duyệt của yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public class InputChangeOrder_ReviewList
	{	
	
	
		#region Member Variables

		private string strInputChangeOrderID = string.Empty;
		private int intReviewLevelID = 0;
		private string strUserName = string.Empty;
		private DateTime? dtmInputChangeOrderDate;
		private int intReviewStatus = 0;
		private bool bolIsReviewed = false;
		private DateTime? dtmReviewedDate;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private decimal decRowSCN = 0;
        private bool bolIsReviewByFinger = false;
		#endregion


		#region Properties 

		/// <summary>
		/// InputChangeOrderID
		/// 
		/// </summary>
		public string InputChangeOrderID
		{
			get { return  strInputChangeOrderID; }
			set { strInputChangeOrderID = value; }
		}

		/// <summary>
		/// ReviewLevelID
		/// 
		/// </summary>
		public int ReviewLevelID
		{
			get { return  intReviewLevelID; }
			set { intReviewLevelID = value; }
		}

		/// <summary>
		/// UserName
		/// 
		/// </summary>
		public string UserName
		{
			get { return  strUserName; }
			set { strUserName = value; }
		}

		/// <summary>
		/// InputChangeOrderDate
		/// 
		/// </summary>
		public DateTime? InputChangeOrderDate
		{
			get { return  dtmInputChangeOrderDate; }
			set { dtmInputChangeOrderDate = value; }
		}

		/// <summary>
		/// ReviewStatus
		/// 
		/// </summary>
		public int ReviewStatus
		{
			get { return  intReviewStatus; }
			set { intReviewStatus = value; }
		}

		/// <summary>
		/// IsReviewed
		/// 
		/// </summary>
		public bool IsReviewed
		{
			get { return  bolIsReviewed; }
			set { bolIsReviewed = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// 
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        public int PrecedingOrderStepID { get; set; }

        public int AutoUpdateOrderStepID { get; set; }

        public decimal RowSCN
        {
            get { return decRowSCN; }
            set { decRowSCN = value; }
        }

        public bool IsReviewByFinger
        {
            get { return bolIsReviewByFinger; }
            set { bolIsReviewByFinger = value; }
        }
        
		#endregion			
		
		
		#region Constructor

		public InputChangeOrder_ReviewList()
		{
		}
		#endregion


		#region Column Names
        public const String colRowSCN = "RowSCN";
		public const String colInputChangeOrderID = "InputChangeOrderID";
		public const String colReviewLevelID = "ReviewLevelID";
		public const String colUserName = "UserName";
		public const String colInputChangeOrderDate = "InputChangeOrderDate";
		public const String colReviewStatus = "ReviewStatus";
		public const String colIsReviewed = "IsReviewed";
		public const String colReviewedDate = "ReviewedDate";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colIsReviewByFinger = "ISREVIEWBYFINGER";
		#endregion //Column names

		
	}
}
