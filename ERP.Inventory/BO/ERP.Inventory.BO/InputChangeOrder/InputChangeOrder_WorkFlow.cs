
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Danh sách qui trình của yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public class InputChangeOrder_WorkFlow
	{	
	
	
		#region Member Variables

		private string strInputChangeOrderID = string.Empty;
		private int intInputChangeOrderStepID = 0;
		private DateTime? dtmInputChangeOrderDate;
		private bool bolIsProcessed = false;
		private string strProcessedUser = string.Empty;
		private DateTime? dtmProcessedTime;
		private string strNote = string.Empty;
		private DateTime? dtmCreatedDate;

		#endregion


		#region Properties 

		/// <summary>
		/// InputChangeOrderID
		/// 
		/// </summary>
		public string InputChangeOrderID
		{
			get { return  strInputChangeOrderID; }
			set { strInputChangeOrderID = value; }
		}

		/// <summary>
		/// InputChangeOrderStepID
		/// 
		/// </summary>
		public int InputChangeOrderStepID
		{
			get { return  intInputChangeOrderStepID; }
			set { intInputChangeOrderStepID = value; }
		}

		/// <summary>
		/// InputChangeOrderDate
		/// 
		/// </summary>
		public DateTime? InputChangeOrderDate
		{
			get { return  dtmInputChangeOrderDate; }
			set { dtmInputChangeOrderDate = value; }
		}

		/// <summary>
		/// IsProcessed
		/// 
		/// </summary>
		public bool IsProcessed
		{
			get { return  bolIsProcessed; }
			set { bolIsProcessed = value; }
		}

		/// <summary>
		/// ProcessedUser
		/// 
		/// </summary>
		public string ProcessedUser
		{
			get { return  strProcessedUser; }
			set { strProcessedUser = value; }
		}

		/// <summary>
		/// ProcessedTime
		/// 
		/// </summary>
		public DateTime? ProcessedTime
		{
			get { return  dtmProcessedTime; }
			set { dtmProcessedTime = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public InputChangeOrder_WorkFlow()
		{
		}
		#endregion


		#region Column Names

		public const String colInputChangeOrderID = "InputChangeOrderID";
		public const String colInputChangeOrderStepID = "InputChangeOrderStepID";
		public const String colInputChangeOrderDate = "InputChangeOrderDate";
		public const String colIsProcessed = "IsProcessed";
		public const String colProcessedUser = "ProcessedUser";
		public const String colProcessedTime = "ProcessedTime";
		public const String colNote = "Note";
		public const String colCreatedDate = "CreatedDate";

		#endregion //Column names

		
	}
}
