
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Bảng yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public partial class InputChangeOrder
	{	
	
	
		#region Member Variables

		private string strInputChangeOrderID = string.Empty;
		private int intInputChangeOrderTypeID = 0;
		private DateTime? dtmInputChangeOrderDate;
		private int intInputChangeOrderStoreID = 0;
		private string strOldOutputVoucherID = string.Empty;
		private string strInputChangeOrderContent = string.Empty;
		private bool bolIsReviewed = false;
		private DateTime? dtmReviewedDate;
		private bool bolIsInputChanged = false;
		private DateTime? dtmInputChangeDate;
		private string strInputChangeID = string.Empty;
		private string strNewInputVoucherID = string.Empty;
		private string strNewOutputVoucherID = string.Empty;
		private string strInVoucherID = string.Empty;
		private string strOutVoucherID = string.Empty;
        private string strSaleOrderID = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private bool bolIsInputReturn = false;
        private decimal decUnevenAmount;
        private int intInputChangeReasonID = 0;
        private string strInputChangeReasonNote = string.Empty;
        private BO.Input.InputVoucherReturn objInputVoucherReturn = null;
        private bool bolApplyErrorType = false;
        private decimal decRowSCN = 0;
        
		#endregion


		#region Properties 
        public bool ApplyErrorType
        {
            get { return bolApplyErrorType; }
            set { bolApplyErrorType = value; }
        }
		/// <summary>
		/// InputChangeOrderID
		/// 
		/// </summary>
		public string InputChangeOrderID
		{
			get { return  strInputChangeOrderID; }
			set { strInputChangeOrderID = value; }
		}

		/// <summary>
		/// InputChangeOrderTypeID
		/// 
		/// </summary>
		public int InputChangeOrderTypeID
		{
			get { return  intInputChangeOrderTypeID; }
			set { intInputChangeOrderTypeID = value; }
		}

		/// <summary>
		/// InputChangeOrderDate
		/// 
		/// </summary>
		public DateTime? InputChangeOrderDate
		{
			get { return  dtmInputChangeOrderDate; }
			set { dtmInputChangeOrderDate = value; }
		}

		/// <summary>
		/// InputChangeOrderStoreID
		/// 
		/// </summary>
		public int InputChangeOrderStoreID
		{
			get { return  intInputChangeOrderStoreID; }
			set { intInputChangeOrderStoreID = value; }
		}
        public decimal RowSCN
        {
            get { return decRowSCN; }
            set { decRowSCN = value; }
        }
		/// <summary>
		/// OldOutputVoucherID
		/// 
		/// </summary>
		public string OldOutputVoucherID
		{
			get { return  strOldOutputVoucherID; }
			set { strOldOutputVoucherID = value; }
		}

		/// <summary>
		/// InputChangeOrderContent
		/// 
		/// </summary>
		public string InputChangeOrderContent
		{
			get { return  strInputChangeOrderContent; }
			set { strInputChangeOrderContent = value; }
		}

		/// <summary>
		/// IsReviewed
		/// 
		/// </summary>
		public bool IsReviewed
		{
			get { return  bolIsReviewed; }
			set { bolIsReviewed = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// 
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// IsInputChanged
		/// 
		/// </summary>
		public bool IsInputChanged
		{
			get { return  bolIsInputChanged; }
			set { bolIsInputChanged = value; }
		}

		/// <summary>
		/// InputChangeDate
		/// 
		/// </summary>
		public DateTime? InputChangeDate
		{
			get { return  dtmInputChangeDate; }
			set { dtmInputChangeDate = value; }
		}

		/// <summary>
		/// InputChangeID
		/// 
		/// </summary>
		public string InputChangeID
		{
			get { return  strInputChangeID; }
			set { strInputChangeID = value; }
		}

		/// <summary>
		/// NewInputVoucherID
		/// 
		/// </summary>
        public string NewInputVoucherID
        {
            get { return strNewInputVoucherID; }
            set { strNewInputVoucherID = value; }
        }

		/// <summary>
		/// NewOutputVoucherID
		/// 
		/// </summary>
		public string NewOutputVoucherID
		{
			get { return  strNewOutputVoucherID; }
			set { strNewOutputVoucherID = value; }
		}

		/// <summary>
		/// InVoucherID
		/// 
		/// </summary>
		public string InVoucherID
		{
			get { return  strInVoucherID; }
			set { strInVoucherID = value; }
		}

		/// <summary>
		/// OutVoucherID
		/// 
		/// </summary>
		public string OutVoucherID
		{
			get { return  strOutVoucherID; }
			set { strOutVoucherID = value; }
		}

        /// <summary>
        /// SaleOrderID
        /// 
        /// </summary>
        public string SaleOrderID
        {
            get { return strSaleOrderID; }
            set { strSaleOrderID = value; }
        }

        public bool IsInputReturn
        {
            get { return bolIsInputReturn; }
            set { bolIsInputReturn = value; }
        }
        /// <summary>
        /// UnevenAmount
        /// Tiền chênh lệch: less than 0 là chi, ngược lại là thu
        /// </summary>
        public decimal UnevenAmount
        {
            get { return decUnevenAmount; }
            set { decUnevenAmount = value; }
        }

        public BO.Input.InputVoucherReturn InputVoucherReturnBO
        {
            get { return objInputVoucherReturn; }
            set { objInputVoucherReturn = value; }
        }

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        public int InputChangeReasonID
        {
            get { return intInputChangeReasonID; }
            set { intInputChangeReasonID = value; }
        }

        public string InputChangeReasonNote
        {
            get { return strInputChangeReasonNote; }
            set { strInputChangeReasonNote = value; }
        }
        public string BrokerUser {get; set;}
        public string ContentDeleted { get; set; }

        public List<InputChangeOrderDetail> InputChangeOrderDetailList { get; set; }

        public DataTable InputChangeOrder_ReviewTable { get; set; }

        public DataTable InputChangeOrder_WorkFlowTable { get; set; }

        public List<InputChangeOrder_Attachment> InputChangeOrder_AttachmentList { get; set; }

		#endregion			
		
		
		#region Constructor

		public InputChangeOrder()
		{
		}
		#endregion


		#region Column Names
        public const String colRowSCN = "RowSCN";
		public const String colInputChangeOrderID = "InputChangeOrderID";
		public const String colInputChangeOrderTypeID = "InputChangeOrderTypeID";
		public const String colInputChangeOrderDate = "InputChangeOrderDate";
		public const String colInputChangeOrderStoreID = "InputChangeOrderStoreID";
        public const String colOldOutputVoucherID = "OldOutputVoucherID";
		public const String colInputChangeOrderContent = "InputChangeOrderContent";
		public const String colIsReviewed = "IsReviewed";
		public const String colReviewedDate = "ReviewedDate";
		public const String colIsInputChanged = "IsInputChanged";
		public const String colInputChangeDate = "InputChangeDate";
		public const String colInputChangeID = "InputChangeID";
		public const String colNewInputVoucherID = "NewInputVoucherID";
		public const String colNewOutputVoucherID = "NewOutputVoucherID";
		public const String colInVoucherID = "InVoucherID";
		public const String colOutVoucherID = "OutVoucherID";
        public const String colSaleOrderID = "SaleOrderID";
        public const String colIsInputReturn = "IsInputReturn";
        public const String colUnevenAmount = "UnevenAmount";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colInputChangeReasonID = "InputChangeReasonID";
        public const String colInputChangeReasonNote = "InputChangeReasonNote";

		#endregion //Column names

		
	}
}
