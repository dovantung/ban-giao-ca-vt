﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.InputChangeOrder
{
    public partial class InputChangeOrder
    {
        public ERP.SalesAndServices.SaleOrders.BO.SaleOrder objSaleOrder = null;
        public bool IsDebt { get; set; }
        public bool IsNotCreateOutVoucherDetail { get; set; }
        private bool bolIsDebtOrder = false;

        public bool IsDebtOrder
        {
            get { return bolIsDebtOrder; }
            set { bolIsDebtOrder = value; }
        }


    }
}
