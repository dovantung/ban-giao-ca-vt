
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using ERP.SalesAndServices.Payment.BO;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 10/1/2012 
	/// Bảng xuất chuyển kho
	/// </summary>	
	public partial class StoreChange
	{	
	
	
		#region Member Variables

		private string strStoreChangeID = string.Empty;
		private string strStoreChangeOrderID = string.Empty;
		private int intStoreChangeTypeID = 0;
		private int intTransportTypeID = 0;
		private int intFromStoreID = 0;
		private int intToStoreID = 0;
		private int intCreatedStoreID = 0;
		private DateTime? dtmStoreChangeDate;
		private bool bolIsNew = false;
		private string strInvoiceID = string.Empty;
		private string strCaskCode = string.Empty;
		private int intTotalPacking = 0;
		private string strToUser1 = string.Empty;
		private string strToUser2 = string.Empty;
		private decimal decTotalWeight;
		private decimal decTotalSize;
		private decimal decTotalShippingCost;
		private string strTransportVoucherID = string.Empty;
		private string strContent = string.Empty;
		private string strStoreChangeUser = string.Empty;
		private string strOutputVoucherID = string.Empty;
		private string strInputVoucherID = string.Empty;
		private string strInVoucherID = string.Empty;
		private string strOutVoucherID = string.Empty;
		private bool bolIsReceive = false;
		private string strReceiveNote = string.Empty;
		private string strUserReceive = string.Empty;
		private DateTime? dtmDateReceive;
		private bool bolIsUrgent = false;
		private bool bolIsTransfered = false;
		private DateTime? dtmTransferedDate;
		private string strTransferedUser = string.Empty;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private int intOutputTypeID = 0;
        private DataTable dtbUserNotify = null;
        private bool bolIsReceivedTransfer = false;
        private string strReceivedTransferUser = string.Empty;
        private DateTime? dtmReceivedTransferDate;
        private bool bolIsReceivedCheckinByFinger = false;
        private bool bolIsTransferedCheckinByFinger = false;
        private bool bolIsSignReceive = false;
        private string strSignReceiveUser = string.Empty;
        private DateTime? dtmSignReceiveDate;
        private string strSignReceiveNote;
        private string strInvoiceSymbol = string.Empty;

        private bool bolIsSignReceivedCheckinByFinger = false;
        private bool bolIsReadReceivedCheckinByFinger = false;
        #endregion

        #region Properties 

        public string InvoiceSymbol
        {
            get { return strInvoiceSymbol; }
            set { strInvoiceSymbol = value; }
        }

        public bool IsSignReceive
        {
            get { return bolIsSignReceive; }
            set { bolIsSignReceive = value; }
        }


        public string SignReceiveUser
        {
            get { return strSignReceiveUser; }
            set { strSignReceiveUser = value; }
        }

        public DateTime? SignReceiveDate
        {
            get { return dtmSignReceiveDate; }
            set { dtmSignReceiveDate = value; }
        }

        public string SignReceiveNote
        {
            get { return strSignReceiveNote; }
            set { strSignReceiveNote = value; }
        }

        public bool IsTransferedCheckinByFinger
        {
            get { return bolIsTransferedCheckinByFinger; }
            set { bolIsTransferedCheckinByFinger = value; }
        }

        public bool IsReceivedCheckinByFinger
        {
            get { return bolIsReceivedCheckinByFinger; }
            set { bolIsReceivedCheckinByFinger = value; }
        }

        public DateTime? ReceivedTransferDate
        {
            get { return dtmReceivedTransferDate; }
            set { dtmReceivedTransferDate = value; }
        }

        public string ReceivedTransferUser
        {
            get { return strReceivedTransferUser; }
            set { strReceivedTransferUser = value; }
        }

        public bool IsReceivedTransfer
        {
            get { return bolIsReceivedTransfer; }
            set { bolIsReceivedTransfer = value; }
        }

        public DataTable UserNotifyData
        {
            get { return dtbUserNotify; }
            set { dtbUserNotify = value; }
        }

        public int OutputTypeID
        {
            get { return intOutputTypeID; }
            set { intOutputTypeID = value; }
        }
       
		/// <summary>
		/// StoreChangeID
		/// 
		/// </summary>
		public string StoreChangeID
		{
			get { return  strStoreChangeID; }
			set { strStoreChangeID = value; }
		}

		/// <summary>
		/// StoreChangeOrderID
		/// 
		/// </summary>
		public string StoreChangeOrderID
		{
			get { return  strStoreChangeOrderID; }
			set { strStoreChangeOrderID = value; }
		}

		/// <summary>
		/// StoreChangeTypeID
		/// 
		/// </summary>
		public int StoreChangeTypeID
		{
			get { return  intStoreChangeTypeID; }
			set { intStoreChangeTypeID = value; }
		}

		/// <summary>
		/// TransportTypeID
		/// 
		/// </summary>
		public int TransportTypeID
		{
			get { return  intTransportTypeID; }
			set { intTransportTypeID = value; }
		}

		/// <summary>
		/// FromStoreID
		/// 
		/// </summary>
		public int FromStoreID
		{
			get { return  intFromStoreID; }
			set { intFromStoreID = value; }
		}

		/// <summary>
		/// ToStoreID
		/// 
		/// </summary>
		public int ToStoreID
		{
			get { return  intToStoreID; }
			set { intToStoreID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// StoreChangeDate
		/// 
		/// </summary>
		public DateTime? StoreChangeDate
		{
			get { return  dtmStoreChangeDate; }
			set { dtmStoreChangeDate = value; }
		}

		/// <summary>
		/// IsNew
		/// Mới/cũ
		/// </summary>
		public bool IsNew
		{
			get { return  bolIsNew; }
			set { bolIsNew = value; }
		}

		/// <summary>
		/// InvoiceID
		/// Số hóa đơn chuyển
		/// </summary>
		public string InvoiceID
		{
			get { return  strInvoiceID; }
			set { strInvoiceID = value; }
		}

		/// <summary>
		/// CaskCode
		/// Mã số thùng hàng
		/// </summary>
		public string CaskCode
		{
			get { return  strCaskCode; }
			set { strCaskCode = value; }
		}

		/// <summary>
		/// TotalPacking
		/// Tổng số thùng hàng
		/// </summary>
		public int TotalPacking
		{
			get { return  intTotalPacking; }
			set { intTotalPacking = value; }
		}

		/// <summary>
		/// ToUser1
		/// Người nhận 1
		/// </summary>
		public string ToUser1
		{
			get { return  strToUser1; }
			set { strToUser1 = value; }
		}

		/// <summary>
		/// ToUser2
		/// Người nhận 2
		/// </summary>
		public string ToUser2
		{
			get { return  strToUser2; }
			set { strToUser2 = value; }
		}

		/// <summary>
		/// TotalWeight
		/// Tổng khối lượng
		/// </summary>
		public decimal TotalWeight
		{
			get { return  decTotalWeight; }
			set { decTotalWeight = value; }
		}

		/// <summary>
		/// TotalSize
		/// Tổng thể tích
		/// </summary>
		public decimal TotalSize
		{
			get { return  decTotalSize; }
			set { decTotalSize = value; }
		}

		/// <summary>
		/// TotalShippingCost
		/// Tổng chi phí vận chuyển
		/// </summary>
		public decimal TotalShippingCost
		{
			get { return  decTotalShippingCost; }
			set { decTotalShippingCost = value; }
		}

		/// <summary>
		/// TransportVoucherID
		/// Chứng từ vận chuyển
		/// </summary>
		public string TransportVoucherID
		{
			get { return  strTransportVoucherID; }
			set { strTransportVoucherID = value; }
		}

		/// <summary>
		/// Content
		/// 
		/// </summary>
		public string Content
		{
			get { return  strContent; }
			set { strContent = value; }
		}

		/// <summary>
		/// StoreChangeUser
		/// Nhân viên chuyển
		/// </summary>
		public string StoreChangeUser
		{
			get { return  strStoreChangeUser; }
			set { strStoreChangeUser = value; }
		}

		/// <summary>
		/// OutputVoucherID
		/// 
		/// </summary>
		public string OutputVoucherID
		{
			get { return  strOutputVoucherID; }
			set { strOutputVoucherID = value; }
		}

		/// <summary>
		/// InputVoucherID
		/// 
		/// </summary>
		public string InputVoucherID
		{
			get { return  strInputVoucherID; }
			set { strInputVoucherID = value; }
		}

		/// <summary>
		/// INVoucherID
		/// Mã phiếu thu
		/// </summary>
		public string InVoucherID
		{
			get { return  strInVoucherID; }
			set { strInVoucherID = value; }
		}

		/// <summary>
		/// OutVoucherID
		/// Mã phiếu chi
		/// </summary>
		public string OutVoucherID
		{
			get { return  strOutVoucherID; }
			set { strOutVoucherID = value; }
		}

		/// <summary>
		/// ISReceive
		/// Đã nhận hàng hay chưa?
		/// </summary>
		public bool IsReceive
		{
			get { return  bolIsReceive; }
			set { bolIsReceive = value; }
		}

		/// <summary>
		/// ReceiveNote
		/// Ghi chú nhận
		/// </summary>
		public string ReceiveNote
		{
			get { return  strReceiveNote; }
			set { strReceiveNote = value; }
		}

		/// <summary>
		/// UserReceive
		/// Người nhận hàng
		/// </summary>
		public string UserReceive
		{
			get { return  strUserReceive; }
			set { strUserReceive = value; }
		}

		/// <summary>
		/// DateReceive
		/// 
		/// </summary>
		public DateTime? DateReceive
		{
			get { return  dtmDateReceive; }
			set { dtmDateReceive = value; }
		}

		/// <summary>
		/// ISURGENT
		/// Là phiếu chuyển kho khẩn cấp
		/// </summary>
		public bool IsUrgent
		{
			get { return  bolIsUrgent; }
			set { bolIsUrgent = value; }
		}

		/// <summary>
		/// IsTransferED
		/// Đã chuyển
		/// </summary>
		public bool IsTransfered
		{
			get { return  bolIsTransfered; }
			set { bolIsTransfered = value; }
		}

		/// <summary>
		/// TransferEDDate
		/// Ngày chuyển hàng(Ngày giao cho giao nhận)
		/// </summary>
		public DateTime? TransferedDate
		{
			get { return  dtmTransferedDate; }
			set { dtmTransferedDate = value; }
		}

		/// <summary>
		/// TransferEDUser
		/// Nhân viên xác nhận trạng thái đã chuyển
		/// </summary>
		public string TransferedUser
		{
			get { return  strTransferedUser; }
			set { strTransferedUser = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

        public bool IsSignReceivedCheckinByFinger
        {
            get { return bolIsSignReceivedCheckinByFinger; }
            set { bolIsSignReceivedCheckinByFinger = value; }
        }

        public bool IsReadReceivedCheckinByFinger
        {
            get { return bolIsReadReceivedCheckinByFinger; }
            set { bolIsReadReceivedCheckinByFinger = value; }
        }

        #endregion


        #region Constructor

        public StoreChange()
		{
		}
		#endregion


		#region Column Names

		public const String colStoreChangeID = "StoreChangeID";
		public const String colStoreChangeOrderID = "StoreChangeOrderID";
		public const String colStoreChangeTypeID = "StoreChangeTypeID";
		public const String colTransportTypeID = "TransportTypeID";
		public const String colFromStoreID = "FromStoreID";
		public const String colToStoreID = "ToStoreID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colStoreChangeDate = "StoreChangeDate";
		public const String colIsNew = "IsNew";
		public const String colInvoiceID = "InvoiceID";
		public const String colCASKCode = "CaskCode";
		public const String colTotalPacking = "TotalPacking";
		public const String colToUser1 = "ToUser1";
		public const String colToUser2 = "ToUser2";
		public const String colTotalWEIGHT = "TotalWeight";
		public const String colTotalSize = "TotalSize";
		public const String colTotalShippingCost = "TotalShippingCost";
		public const String colTransportVoucherID = "TransportVoucherID";
		public const String colContent = "Content";
		public const String colStoreChangeUser = "StoreChangeUser";
		public const String colOutputVoucherID = "OutputVoucherID";
		public const String colInputVoucherID = "InputVoucherID";
		public const String colINVoucherID = "InVoucherID";
		public const String colOutVoucherID = "OutVoucherID";
		public const String colISReceive = "ISReceive";
		public const String colReceiveNote = "ReceiveNote";
		public const String colUserReceive = "UserReceive";
		public const String colDateReceive = "DateReceive";
		public const String colISURGENT = "IsUrgent";
		public const String colIsTransferED = "IsTransfered";
		public const String colTransferEDDate = "TransferedDate";
		public const String colTransferEDUser = "TransferedUser";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colIsReceivedTransfer = "IsReceivedTransfer";
        public const String colReceivedTransferUser = "ReceivedTransferUser";
        public const String colReceivedTransferDate = "ReceivedTransferDate";
        public const String colIsReceivedCheckinByFinger = "IsReceivedCheckinByFinger";
        public const String colIsTransferedCheckinByFinger = "IsTransferedCheckinByFinger";
        public const String colIsSignReceive = "IsSignReceive";
        public const String colSignReceiveUser = "SignReceiveUser";
        public const String colSignReceiveDate = "SignReceiveDate";
        public const String colSignReceiveNote = "SignReceiveNote";
        public const String colInvoiceSymbol = "InvoiceSymbol";
        public const String colInStockStatusID = "INSTOCKSTATUSID";
		#endregion //Column names

		
	}
}
