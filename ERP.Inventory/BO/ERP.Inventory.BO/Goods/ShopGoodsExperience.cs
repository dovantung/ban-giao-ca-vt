﻿
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Goods
{
    /// <summary>
    /// Created by 		: TruongTX 
    /// Created date 	: 9/18/2018 
    /// 
    /// </summary>	
    public class ShopGoodsExperience
    {


        #region Member Variables

        private decimal decSHOP_GOODS_EXPERIENCE_ID;
        private decimal decSHOP_ID;
        private decimal decGOODS_ID;
        private decimal decGOODS_Group_ID;
        private decimal decProduct_Type;
        private DateTime? dtmFrom_Date;
        private DateTime? dtmTo_Date;
        private string strUser_ID = string.Empty;
        private decimal decUser_SHOP_ID;
        private DateTime? dtmCreate_Date;
        private string strCOUPLE_GOODS_Code = string.Empty;

        #endregion


        #region Properties

        /// <summary>
        /// SHOP_GOODS_EXPERIENCE_ID
        /// 
        /// </summary>
        public decimal SHOP_GOODS_EXPERIENCE_ID
        {
            get { return decSHOP_GOODS_EXPERIENCE_ID; }
            set { decSHOP_GOODS_EXPERIENCE_ID = value; }
        }

        /// <summary>
        /// SHOP_ID
        /// 
        /// </summary>
        public decimal SHOP_ID
        {
            get { return decSHOP_ID; }
            set { decSHOP_ID = value; }
        }

        /// <summary>
        /// GOODS_ID
        /// 
        /// </summary>
        public decimal GOODS_ID
        {
            get { return decGOODS_ID; }
            set { decGOODS_ID = value; }
        }

        /// <summary>
        /// GOODS_Group_ID
        /// 
        /// </summary>
        public decimal GOODS_Group_ID
        {
            get { return decGOODS_Group_ID; }
            set { decGOODS_Group_ID = value; }
        }

        /// <summary>
        /// Product_Type
        /// 
        /// </summary>
        public decimal Product_Type
        {
            get { return decProduct_Type; }
            set { decProduct_Type = value; }
        }

        /// <summary>
        /// From_Date
        /// 
        /// </summary>
        public DateTime? From_Date
        {
            get { return dtmFrom_Date; }
            set { dtmFrom_Date = value; }
        }

        /// <summary>
        /// To_Date
        /// 
        /// </summary>
        public DateTime? To_Date
        {
            get { return dtmTo_Date; }
            set { dtmTo_Date = value; }
        }

        /// <summary>
        /// User_ID
        /// 
        /// </summary>
        public string User_ID
        {
            get { return strUser_ID; }
            set { strUser_ID = value; }
        }

        /// <summary>
        /// User_SHOP_ID
        /// 
        /// </summary>
        public decimal User_SHOP_ID
        {
            get { return decUser_SHOP_ID; }
            set { decUser_SHOP_ID = value; }
        }

        /// <summary>
        /// Create_Date
        /// 
        /// </summary>
        public DateTime? Create_Date
        {
            get { return dtmCreate_Date; }
            set { dtmCreate_Date = value; }
        }

        /// <summary>
        /// COUPLE_GOODS_Code
        /// 
        /// </summary>
        public string COUPLE_GOODS_Code
        {
            get { return strCOUPLE_GOODS_Code; }
            set { strCOUPLE_GOODS_Code = value; }
        }


        #endregion


        #region Constructor

        public ShopGoodsExperience()
        {
        }
        #endregion


        #region Column Names

        public const String colSHOP_GOODS_EXPERIENCE_ID = "SHOP_GOODS_EXPERIENCE_ID";
        public const String colSHOP_ID = "SHOP_ID";
        public const String colGOODS_ID = "GOODS_ID";
        public const String colGOODS_Group_ID = "GOODS_Group_ID";
        public const String colProduct_Type = "Product_Type";
        public const String colFrom_Date = "From_Date";
        public const String colTo_Date = "To_Date";
        public const String colUser_ID = "User_ID";
        public const String colUser_SHOP_ID = "User_SHOP_ID";
        public const String colCreate_Date = "Create_Date";
        public const String colCOUPLE_GOODS_Code = "COUPLE_GOODS_Code";

        #endregion //Column names


    }
}


