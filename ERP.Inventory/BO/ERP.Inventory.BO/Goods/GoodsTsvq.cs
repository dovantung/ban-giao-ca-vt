﻿
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Goods
{
    /// <summary>
    /// Created by 		: TruongTX 
    /// Created date 	: 9/19/2018 
    /// 
    /// </summary>	
    public class GoodsTsvq
    {


        #region Member Variables

        private decimal decGOODS_TSVQ_ID;
        private decimal decGOODS_ID;
        private decimal decTSVQ;
        private DateTime? dtmFrom_Date;
        private DateTime? dtmTo_Date;
        private DateTime? dtmCreate_DateTime;
        private decimal decUser_ID;
        private decimal decUser_SHOP_ID;
        private int intStock_ID = 0;
        private string strCOUPLE_GOODS_Code = string.Empty;

        #endregion


        #region Properties

        /// <summary>
        /// GOODS_TSVQ_ID
        /// 
        /// </summary>
        public decimal GOODS_TSVQ_ID
        {
            get { return decGOODS_TSVQ_ID; }
            set { decGOODS_TSVQ_ID = value; }
        }

        /// <summary>
        /// GOODS_ID
        /// 
        /// </summary>
        public decimal GOODS_ID
        {
            get { return decGOODS_ID; }
            set { decGOODS_ID = value; }
        }

        /// <summary>
        /// TSVQ
        /// 
        /// </summary>
        public decimal TSVQ
        {
            get { return decTSVQ; }
            set { decTSVQ = value; }
        }

        /// <summary>
        /// From_Date
        /// 
        /// </summary>
        public DateTime? From_Date
        {
            get { return dtmFrom_Date; }
            set { dtmFrom_Date = value; }
        }

        /// <summary>
        /// To_Date
        /// 
        /// </summary>
        public DateTime? To_Date
        {
            get { return dtmTo_Date; }
            set { dtmTo_Date = value; }
        }

        /// <summary>
        /// Create_DateTime
        /// 
        /// </summary>
        public DateTime? Create_DateTime
        {
            get { return dtmCreate_DateTime; }
            set { dtmCreate_DateTime = value; }
        }

        /// <summary>
        /// User_ID
        /// 
        /// </summary>
        public decimal User_ID
        {
            get { return decUser_ID; }
            set { decUser_ID = value; }
        }

        /// <summary>
        /// User_SHOP_ID
        /// 
        /// </summary>
        public decimal User_SHOP_ID
        {
            get { return decUser_SHOP_ID; }
            set { decUser_SHOP_ID = value; }
        }

        /// <summary>
        /// Stock_ID
        /// 
        /// </summary>
        public int Stock_ID
        {
            get { return intStock_ID; }
            set { intStock_ID = value; }
        }

        /// <summary>
        /// COUPLE_GOODS_Code
        /// 
        /// </summary>
        public string COUPLE_GOODS_Code
        {
            get { return strCOUPLE_GOODS_Code; }
            set { strCOUPLE_GOODS_Code = value; }
        }


        #endregion


        #region Constructor

        public GoodsTsvq()
        {
        }
        #endregion


        #region Column Names

        public const String colGOODS_TSVQ_ID = "GOODS_TSVQ_ID";
        public const String colGOODS_ID = "GOODS_ID";
        public const String colTSVQ = "TSVQ";
        public const String colFrom_Date = "From_Date";
        public const String colTo_Date = "To_Date";
        public const String colCreate_DateTime = "Create_DateTime";
        public const String colUser_ID = "User_ID";
        public const String colUser_SHOP_ID = "User_SHOP_ID";
        public const String colStock_ID = "Stock_ID";
        public const String colCOUPLE_GOODS_Code = "COUPLE_GOODS_Code";

        #endregion //Column names


    }
}


