﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO.CheckDifferenceStockERPAndMisa
{
    public class MisaProductStock_Att
    {
        #region Member Variables

		private string strMISAPRODUCTSTOCKID = string.Empty;
		private string strDESCRIPTION = string.Empty;
		private string strCREATEDUSER= string.Empty;
		private DateTime? dtmCREATEDDATE;
		private bool bolISDELETED;
		private string strDELETEDUSER= string.Empty;
		private DateTime? dtmDELETEDDATE;
		private string strFILEID= string.Empty;

		#endregion


		#region Properties 

		/// <summary>
		/// MISAPRODUCTSTOCKID
		/// 
		/// </summary>
		public string MISAPRODUCTSTOCKID
		{
			get { return  strMISAPRODUCTSTOCKID; }
			set { strMISAPRODUCTSTOCKID = value; }
		}

		/// <summary>
		/// DESCRIPTION
		/// 
		/// </summary>
		public string DESCRIPTION
		{
			get { return  strDESCRIPTION; }
			set { strDESCRIPTION = value; }
		}

		/// <summary>
		/// CREATEDUSER
		/// 
		/// </summary>
		public string CREATEDUSER
		{
			get { return  strCREATEDUSER; }
			set { strCREATEDUSER = value; }
		}

		/// <summary>
		/// CREATEDDATE
		/// 
		/// </summary>
		public DateTime? CREATEDDATE
		{
			get { return  dtmCREATEDDATE; }
			set { dtmCREATEDDATE = value; }
		}

		/// <summary>
		/// ISDELETED
		/// 
		/// </summary>
		public bool ISDELETED
		{
			get { return  bolISDELETED; }
			set { bolISDELETED = value; }
		}

		/// <summary>
		/// DELETEDUSER
		/// 
		/// </summary>
		public string DELETEDUSER
		{
			get { return  strDELETEDUSER; }
			set { strDELETEDUSER = value; }
		}

		/// <summary>
		/// DELETEDDATE
		/// </summary>
		public DateTime? DELETEDDATE
		{
  			get { return dtmDELETEDDATE; }
   			set { dtmDELETEDDATE = value; }
		}
        /// <summary>
		/// FILEID
		/// 
		/// </summary>
		public string FILEID
		{
			get { return  strFILEID; }
			set { strFILEID = value; }
		}

		#endregion			
		
		
		#region Constructor

        public MisaProductStock_Att()
		{
		}
		

		#endregion


		#region Column Names

        public const String colMISAPRODUCTSTOCKID = "MISAPRODUCTSTOCKID";
        public const String colDESCRIPTION = "DESCRIPTION";
        public const String colCREATEDUSER = "CREATEDUSER";
        public const String colCREATEDDATE = "CREATEDDATE";
        public const String colISDELETED = "ISDELETED";
        public const String colDELETEDUSER = "DELETEDUSER";
        public const String colDELETEDDATE = "DELETEDDATE";
        public const String colFILEID = "FILEID";

		#endregion //Column names

    }
}
