
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// 
	/// </summary>	
	public class CurrentInStockDetail
	{	
	
	
		#region Member Variables

		private int intStoreID = 0;
		private string strProductID = string.Empty;
		private string strIMEI = string.Empty;
		private string strPINCode = string.Empty;
		private string strInputVoucherDetailID = string.Empty;
		private int intFirstCustomerID = 0;
		private DateTime? dtmENDWarrantyDate;
		private decimal decCostPrice;
		private bool bolIsNew = false;
		private bool bolIsShowProduct = false;
		private bool bolIsOrder = false;
		private bool bolISDelivery = false;
		private string strOrderID = string.Empty;
		private string strNote = string.Empty;
		private bool bolIsExist = false;
        private int intOrderIndex = -1;
        private bool bolIsSuccess = false;
        private string strProductName = string.Empty;

		#endregion


		#region Properties 

		/// <summary>
		/// StoreID
		/// 
		/// </summary>
		public int StoreID
		{
			get { return  intStoreID; }
			set { intStoreID = value; }
		}

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// 
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// PINCode
		/// 
		/// </summary>
		public string PINCode
		{
			get { return  strPINCode; }
			set { strPINCode = value; }
		}

		/// <summary>
		/// InputVoucherDetailID
		/// 
		/// </summary>
		public string InputVoucherDetailID
		{
			get { return  strInputVoucherDetailID; }
			set { strInputVoucherDetailID = value; }
		}

		/// <summary>
		/// FirstCustomerID
		/// 
		/// </summary>
		public int FirstCustomerID
		{
			get { return  intFirstCustomerID; }
			set { intFirstCustomerID = value; }
		}

		/// <summary>
		/// ENDWarrantyDate
		/// 
		/// </summary>
		public DateTime? ENDWarrantyDate
		{
			get { return  dtmENDWarrantyDate; }
			set { dtmENDWarrantyDate = value; }
		}

		/// <summary>
		/// CostPrice
		/// 
		/// </summary>
		public decimal CostPrice
		{
			get { return  decCostPrice; }
			set { decCostPrice = value; }
		}

		/// <summary>
		/// IsNew
		/// 
		/// </summary>
		public bool IsNew
		{
			get { return  bolIsNew; }
			set { bolIsNew = value; }
		}

		/// <summary>
		/// IsShowProduct
		/// 
		/// </summary>
		public bool IsShowProduct
		{
			get { return  bolIsShowProduct; }
			set { bolIsShowProduct = value; }
		}

		/// <summary>
		/// IsOrder
		/// 
		/// </summary>
		public bool IsOrder
		{
			get { return  bolIsOrder; }
			set { bolIsOrder = value; }
		}

		/// <summary>
		/// ISDelivery
		/// 
		/// </summary>
		public bool ISDelivery
		{
			get { return  bolISDelivery; }
			set { bolISDelivery = value; }
		}

		/// <summary>
		/// OrderID
		/// 
		/// </summary>
		public string OrderID
		{
			get { return  strOrderID; }
			set { strOrderID = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// Có tồn tại không?
		/// </summary>
		public bool IsExist
		{
  			get { return bolIsExist; }
   			set { bolIsExist = value; }
		}

        public int OrderIndex
        {
            get { return intOrderIndex; }
            set { intOrderIndex = value; }
        }

        public bool IsSuccess
        {
            get { return bolIsSuccess; }
            set { bolIsSuccess = value; }
        }
        
        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }
		#endregion			
		
		
		#region Constructor

		public CurrentInStockDetail()
		{
		}
        public CurrentInStockDetail(int intStoreID, string strProductID, string strIMEI, string strPINCode, string strInputVoucherDetailID, int intFirstCustomerID, DateTime? dtmENDWarrantyDate, decimal decCostPrice, bool bolIsNew, bool bolIsShowProduct, bool bolIsOrder, bool bolISDelivery, string strOrderID, string strNote)
		{
			this.intStoreID = intStoreID;
            this.strProductID = strProductID;
            this.strIMEI = strIMEI;
            this.strPINCode = strPINCode;
            this.strInputVoucherDetailID = strInputVoucherDetailID;
            this.intFirstCustomerID = intFirstCustomerID;
            this.dtmENDWarrantyDate = dtmENDWarrantyDate;
            this.decCostPrice = decCostPrice;
            this.bolIsNew = bolIsNew;
            this.bolIsShowProduct = bolIsShowProduct;
            this.bolIsOrder = bolIsOrder;
            this.bolISDelivery = bolISDelivery;
            this.strOrderID = strOrderID;
            this.strNote = strNote;
            
		}
		#endregion


		#region Column Names

		public const String colStoreID = "StoreID";
		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
		public const String colPINCode = "PINCode";
		public const String colInputVoucherDetailID = "InputVoucherDetailID";
		public const String colFirstCustomerID = "FirstCustomerID";
		public const String colENDWarrantyDate = "ENDWarrantyDate";
		public const String colCostPrice = "CostPrice";
		public const String colIsNew = "IsNew";
		public const String colIsShowProduct = "IsShowProduct";
		public const String colIsOrder = "IsOrder";
		public const String colISDelivery = "ISDelivery";
		public const String colOrderID = "OrderID";
        public const String colOrderIndex = "OrderIndex";
        public const String colNote = "Note";

		#endregion //Column names

		
	}
}
