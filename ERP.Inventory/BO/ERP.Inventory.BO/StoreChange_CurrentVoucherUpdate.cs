﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.BO
{
   public class StoreChange_CurrentVoucherUpdate
    {
        private string strStoreChangeId;

        public string StoreChangeId
        {
            get { return strStoreChangeId; }
            set { strStoreChangeId = value; }
        }

        private string strOutputVoucherID;

        public string OutputVoucherID
        {
            get { return strOutputVoucherID; }
            set { strOutputVoucherID = value; }
        }

        private string strDenominator;

        public string Denominator
        {
            get { return strDenominator; }
            set { strDenominator = value; }
        }

        private string strInvoiceSymbol;

        public string InvoiceSymbol
        {
            get { return strInvoiceSymbol; }
            set { strInvoiceSymbol = value; }
        }
        private string strInvoiceId;

        public string InvoiceId
        {
            get { return strInvoiceId; }
            set { strInvoiceId = value; }
        }

        private int intStoreId;

        public int StoreId
        {
            get { return intStoreId; }
            set { intStoreId = value; }
        }

        private string strPrintedReason;

        public string PrintedReason
        {
            get { return strPrintedReason; }
            set { strPrintedReason = value; }
        }

        private string strInvoiceIdNew;

        public string InvoiceIdNew
        {
            get { return strInvoiceIdNew; }
            set { strInvoiceIdNew = value; }
        }
        private string strUserName;

        public string UserName
        {
            get { return strUserName; }
            set { strUserName = value; }
        }

        private bool bolIsReNewPrint;

        public bool IsReNewPrint
        {
            get { return bolIsReNewPrint; }
            set { bolIsReNewPrint = value; }
        }

        private bool bolIsReOldPrint;

        public bool IsReOldPrint
        {
            get { return bolIsReOldPrint; }
            set { bolIsReOldPrint = value; }
        }

        private string strSTORECHANGECURVOUCHERID;

        public string STORECHANGECURVOUCHERID
        {
            get { return strSTORECHANGECURVOUCHERID; }
            set { strSTORECHANGECURVOUCHERID = value; }
        }

        private string strSTORECHANGECURVOUCHERIDOld;

        public string STORECHANGECURVOUCHERIDOld
        {
            get { return strSTORECHANGECURVOUCHERIDOld; }
            set { strSTORECHANGECURVOUCHERIDOld = value; }
        }


        private string strDenominatorOld;

        public string DenominatorOld
        {
            get { return strDenominatorOld; }
            set { strDenominatorOld = value; }
        }

        private string strInvoiceSymbolOld;

        public string InvoiceSymbolOld
        {
            get { return strInvoiceSymbolOld; }
            set { strInvoiceSymbolOld = value; }
        }

    }
}
