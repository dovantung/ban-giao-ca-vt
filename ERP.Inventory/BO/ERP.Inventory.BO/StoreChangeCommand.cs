
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn  
	/// Created date 	: 11/22/2012 
	/// Danh sách lệnh chuyển kho được tính lúc chia hàng
	/// </summary>	
	public class StoreChangeCommand
	{	
	
	
		#region Member Variables

		private string strStoreChangeCommandID = string.Empty;
		private int intStoreChangeCommandTypeID = 0;
		private int intFromStoreID = 0;
		private int intToStoreID = 0;
		private string strContent = string.Empty;
		private string strNote = string.Empty;
		private bool bolIsUrgent = false;
		private bool bolIsCreateStoreChangeOrder = false;
		private string strStoreChangeOrderID = string.Empty;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        
        

        private List<StoreChangeCommandDetail> objStoreChangeCommandDetailList = null;
        public List<StoreChangeCommandDetail> StoreChangeCommandDetailList
        {
            get { return objStoreChangeCommandDetailList; }
            set { objStoreChangeCommandDetailList = value; }
        }
		#endregion


		#region Properties 

		/// <summary>
		/// StoreChangeCommandID
		/// 
		/// </summary>
		public string StoreChangeCommandID
		{
			get { return  strStoreChangeCommandID; }
			set { strStoreChangeCommandID = value; }
		}

		/// <summary>
		/// StoreChangeCommandTypeID
		/// Loại lệnh chuyển kho
		/// </summary>
		public int StoreChangeCommandTypeID
		{
			get { return  intStoreChangeCommandTypeID; }
			set { intStoreChangeCommandTypeID = value; }
		}

		/// <summary>
		/// FromStoreID
		/// Từ kho
		/// </summary>
		public int FromStoreID
		{
			get { return  intFromStoreID; }
			set { intFromStoreID = value; }
		}

		/// <summary>
		/// ToStoreID
		/// Đến kho
		/// </summary>
		public int ToStoreID
		{
			get { return  intToStoreID; }
			set { intToStoreID = value; }
		}

		/// <summary>
		/// Content
		/// 
		/// </summary>
		public string Content
		{
			get { return  strContent; }
			set { strContent = value; }
		}

		/// <summary>
		/// Note
		/// 
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// ISURGENT
		/// Lệnh chuyển kho khẩn 
		/// </summary>
        public bool IsUrgent
		{
			get { return  bolIsUrgent; }
			set { bolIsUrgent = value; }
		}

		/// <summary>
		/// IsCreateStoreChangeOrder
		/// 
		/// </summary>
		public bool IsCreateStoreChangeOrder
		{
			get { return  bolIsCreateStoreChangeOrder; }
			set { bolIsCreateStoreChangeOrder = value; }
		}

		/// <summary>
		/// StoreChangeOrderID
		/// Mã yêu cầu chuyển kho
		/// </summary>
		public string StoreChangeOrderID
		{
			get { return  strStoreChangeOrderID; }
			set { strStoreChangeOrderID = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public StoreChangeCommand()
		{
		}
		#endregion


		#region Column Names

		public const String colStoreChangeCommandID = "StoreChangeCommandID";
		public const String colStoreChangeCommandTypeID = "StoreChangeCommandTypeID";
		public const String colFromStoreID = "FromStoreID";
		public const String colToStoreID = "ToStoreID";
		public const String colContent = "Content";
		public const String colNote = "Note";
		public const String colISURGENT = "ISURGENT";
		public const String colIsCreateStoreChangeOrder = "IsCreateStoreChangeOrder";
		public const String colStoreChangeOrderID = "StoreChangeOrderID";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
