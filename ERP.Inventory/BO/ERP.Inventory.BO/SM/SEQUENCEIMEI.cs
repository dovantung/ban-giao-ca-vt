
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.SM
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 10/09/14 
	/// Mức ưu tiên xuất kho của IMEI
	/// </summary>	
	public class SequenceIMEI
	{	
	
	
		#region Member Variables

		private string strSequenceIMEILID = string.Empty;
		private string strProductID = string.Empty;
		private string strIMEI = string.Empty;
		private int intOrderIndex = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
		private DateTime? dtmSynchDate;
        private bool bolIsSuccess = false;
        private string strProductName = string.Empty;
        private bool bolIsExist = false;
		#endregion


		#region Properties 

		/// <summary>
		/// SequenceIMEILID
		/// 
		/// </summary>
		public string SequenceIMEILID
		{
			get { return  strSequenceIMEILID; }
			set { strSequenceIMEILID = value; }
		}

		/// <summary>
		/// ProductID
		/// Mã sản phẩm
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// IMEI
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// OrderIndex
		/// Mức ưu tiên
		/// </summary>
		public int OrderIndex
		{
			get { return  intOrderIndex; }
			set { intOrderIndex = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}

		/// <summary>
		/// SYNCHDate
		/// Ngày đồng bộ
		/// </summary>
		public DateTime? SynchDate
		{
			get { return  dtmSynchDate; }
			set { dtmSynchDate = value; }
		}

        public bool IsSuccess
        {
            get { return bolIsSuccess; }
            set { bolIsSuccess = value; }
        }

        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }

        /// <summary>
        /// Có tồn tại không?
        /// </summary>
        public bool IsExist
        {
            get { return bolIsExist; }
            set { bolIsExist = value; }
        }
		#endregion			
		
		
		#region Constructor

		public SequenceIMEI()
		{
		}
		#endregion


		#region Column Names

		public const String colSequenceIMEILID = "SequenceIMEILID";
		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
		public const String colOrderIndex = "OrderIndex";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
		public const String colSYNCHDate = "SYNCHDate";

		#endregion //Column names

		
	}
}
