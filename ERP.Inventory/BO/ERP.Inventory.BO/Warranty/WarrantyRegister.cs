
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.Warranty
{
    /// <summary>
    /// Created by 		: Nguyễn Linh Tuấn
	/// Created date 	: 02/05/2013 
	/// Quản lý đăng ký bảo hành
	/// </summary>	
	public class WarrantyRegister
	{	
	
	
		#region Member Variables

		private decimal decWarrantyRegisterID;
		private int intWarrantyRegisterTypeID = 0;
		private DateTime? dtmOutputDate;
		private string strOutputVoucherID = string.Empty;
		private string strOutputVoucherDetailID = string.Empty;
		private string strSaleOrderID = string.Empty;
		private string strSaleOrderDetailID = string.Empty;
		private string strProductID = string.Empty;
        private string strProductName = string.Empty;

        
		private string strIMEI = string.Empty;
		private string strCustomerPhone = string.Empty;
		private string strCustomerName = string.Empty;
		private string strCustomerAddress = string.Empty;
		private decimal decRegisterStoreID;
        private string strRegisterStoreName = string.Empty;

		private bool bolIsRegisterFromOrder = false;
		private bool bolIsRegistered = false;
		private DateTime? dtmRegisterEDTime;
		private decimal decReplyStatusID;
		private string strReplyMessage = string.Empty;
		private DateTime? dtmReplyTime;
		private bool bolISRegisterError = false;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        
		#endregion


		#region Properties 
        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }

        public string RegisterStoreName
        {
            get { return strRegisterStoreName; }
            set { strRegisterStoreName = value; }
        }

		/// <summary>
		/// WarrantyRegisterID
		/// 
		/// </summary>
		public decimal WarrantyRegisterID
		{
			get { return  decWarrantyRegisterID; }
			set { decWarrantyRegisterID = value; }
		}

		/// <summary>
		/// WarrantyRegisterTypeID
		/// 
		/// </summary>
		public int WarrantyRegisterTypeID
		{
			get { return  intWarrantyRegisterTypeID; }
			set { intWarrantyRegisterTypeID = value; }
		}

		/// <summary>
		/// OutputDate
		/// 
		/// </summary>
		public DateTime? OutputDate
		{
			get { return  dtmOutputDate; }
			set { dtmOutputDate = value; }
		}

		/// <summary>
		/// OutputVoucherID
		/// 
		/// </summary>
		public string OutputVoucherID
		{
			get { return  strOutputVoucherID; }
			set { strOutputVoucherID = value; }
		}

		/// <summary>
		/// OutputVoucherDetailID
		/// 
		/// </summary>
		public string OutputVoucherDetailID
		{
			get { return  strOutputVoucherDetailID; }
			set { strOutputVoucherDetailID = value; }
		}

		/// <summary>
		/// SaleOrderID
		/// 
		/// </summary>
		public string SaleOrderID
		{
			get { return  strSaleOrderID; }
			set { strSaleOrderID = value; }
		}

		/// <summary>
		/// SaleOrderDetailID
		/// 
		/// </summary>
		public string SaleOrderDetailID
		{
			get { return  strSaleOrderDetailID; }
			set { strSaleOrderDetailID = value; }
		}

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// IMEI
		/// 
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// CustomerPhone
		/// 
		/// </summary>
		public string CustomerPhone
		{
			get { return  strCustomerPhone; }
			set { strCustomerPhone = value; }
		}

		/// <summary>
		/// CustomerName
		/// 
		/// </summary>
		public string CustomerName
		{
			get { return  strCustomerName; }
			set { strCustomerName = value; }
		}

		/// <summary>
		/// CustomerAddress
		/// 
		/// </summary>
		public string CustomerAddress
		{
			get { return  strCustomerAddress; }
			set { strCustomerAddress = value; }
		}

		/// <summary>
		/// RegisterStoreID
		/// 
		/// </summary>
		public decimal RegisterStoreID
		{
			get { return  decRegisterStoreID; }
			set { decRegisterStoreID = value; }
		}

		/// <summary>
		/// IsRegisterFromOrder
		/// 
		/// </summary>
		public bool IsRegisterFromOrder
		{
			get { return  bolIsRegisterFromOrder; }
			set { bolIsRegisterFromOrder = value; }
		}

		/// <summary>
		/// IsRegistered
		/// 
		/// </summary>
		public bool IsRegistered
		{
			get { return  bolIsRegistered; }
			set { bolIsRegistered = value; }
		}

		/// <summary>
		/// RegisterEDTime
		/// 
		/// </summary>
		public DateTime? RegisterEDTime
		{
			get { return  dtmRegisterEDTime; }
			set { dtmRegisterEDTime = value; }
		}

		/// <summary>
		/// ReplyStatusID
		/// 
		/// </summary>
		public decimal ReplyStatusID
		{
			get { return  decReplyStatusID; }
			set { decReplyStatusID = value; }
		}

		/// <summary>
		/// ReplyMessage
		/// 
		/// </summary>
		public string ReplyMessage
		{
			get { return  strReplyMessage; }
			set { strReplyMessage = value; }
		}

		/// <summary>
		/// ReplyTime
		/// 
		/// </summary>
		public DateTime? ReplyTime
		{
			get { return  dtmReplyTime; }
			set { dtmReplyTime = value; }
		}

		/// <summary>
		/// ISRegisterError
		/// 
		/// </summary>
		public bool ISRegisterError
		{
			get { return  bolISRegisterError; }
			set { bolISRegisterError = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public WarrantyRegister()
		{
		}
		#endregion


		#region Column Names

		public const String colWarrantyRegisterID = "WarrantyRegisterID";
		public const String colWarrantyRegisterTypeID = "WarrantyRegisterTypeID";
		public const String colOutputDate = "OutputDate";
		public const String colOutputVoucherID = "OutputVoucherID";
		public const String colOutputVoucherDetailID = "OutputVoucherDetailID";
		public const String colSaleOrderID = "SaleOrderID";
		public const String colSaleOrderDetailID = "SaleOrderDetailID";
		public const String colProductID = "ProductID";
		public const String colIMEI = "IMEI";
		public const String colCustomerPhone = "CustomerPhone";
		public const String colCustomerName = "CustomerName";
		public const String colCustomerAddress = "CustomerAddress";
		public const String colRegisterStoreID = "RegisterStoreID";
		public const String colISRegisterFromOrder = "IsRegisterFromOrder";
		public const String colISRegisterED = "IsRegistered";
		public const String colRegisterEDTime = "RegisterEDTime";
		public const String colReplyStatusID = "ReplyStatusID";
		public const String colReplyMessage = "ReplyMessage";
		public const String colReplyTime = "ReplyTime";
		public const String colISRegisterError = "ISRegisterError";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
