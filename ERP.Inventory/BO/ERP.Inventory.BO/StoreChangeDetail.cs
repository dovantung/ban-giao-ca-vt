
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 10/1/2012 
	/// 
	/// </summary>	
	public class StoreChangeDetail
	{	
	
	
		#region Member Variables

		private string strStoreChangeDetailID = string.Empty;
		private string strStoreChangeID = string.Empty;
		private DateTime? dtmStoreChangeDate;
		private string strProductID = string.Empty;
		private decimal decQuantity;
		private string strIMEI = string.Empty;
		private string strStoreChangeOrderDetailID = string.Empty;
		private string strOutputVoucherDetailID = string.Empty;
		private string strInputVoucherDetailID = string.Empty;
		private int intPackingNumber = 0;
		private decimal decOrderQuantity;
		private string strUserNote = string.Empty;
		private DateTime? dtmEndWarrantyDate;
		private bool bolIsHasWarranty = false;
		private decimal decInputPrice;
		private decimal decOutputPrice;
		private int intCreatedStoreID = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private string strUpdatedUser = string.Empty;
		private DateTime? dtmUpdatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private string strNote = string.Empty;
      
		#endregion


		#region Properties 
        
		/// <summary>
		/// StoreChangeDetailID
		/// 
		/// </summary>
		public string StoreChangeDetailID
		{
			get { return  strStoreChangeDetailID; }
			set { strStoreChangeDetailID = value; }
		}

		/// <summary>
		/// StoreChangeID
		/// 
		/// </summary>
		public string StoreChangeID
		{
			get { return  strStoreChangeID; }
			set { strStoreChangeID = value; }
		}

		/// <summary>
		/// StoreChangeDate
		/// 
		/// </summary>
		public DateTime? StoreChangeDate
		{
			get { return  dtmStoreChangeDate; }
			set { dtmStoreChangeDate = value; }
		}

		/// <summary>
		/// ProductID
		/// 
		/// </summary>
		public string ProductID
		{
			get { return  strProductID; }
			set { strProductID = value; }
		}

		/// <summary>
		/// Quantity
		/// 
		/// </summary>
		public decimal Quantity
		{
			get { return  decQuantity; }
			set { decQuantity = value; }
		}

		/// <summary>
		/// IMEI
		/// 
		/// </summary>
		public string IMEI
		{
			get { return  strIMEI; }
			set { strIMEI = value; }
		}

		/// <summary>
		/// StoreChangeOrderDetailID
		/// 
		/// </summary>
		public string StoreChangeOrderDetailID
		{
			get { return  strStoreChangeOrderDetailID; }
			set { strStoreChangeOrderDetailID = value; }
		}

		/// <summary>
		/// OutputVoucherDetailID
		/// 
		/// </summary>
		public string OutputVoucherDetailID
		{
			get { return  strOutputVoucherDetailID; }
			set { strOutputVoucherDetailID = value; }
		}

		/// <summary>
		/// InputVoucherDetailID
		/// 
		/// </summary>
		public string InputVoucherDetailID
		{
			get { return  strInputVoucherDetailID; }
			set { strInputVoucherDetailID = value; }
		}

		/// <summary>
		/// PackingNumber
		/// 
		/// </summary>
		public int PackingNumber
		{
			get { return  intPackingNumber; }
			set { intPackingNumber = value; }
		}

		/// <summary>
		/// OrderQuantity
		/// 
		/// </summary>
		public decimal OrderQuantity
		{
			get { return  decOrderQuantity; }
			set { decOrderQuantity = value; }
		}

		/// <summary>
		/// UserNote
		/// 
		/// </summary>
		public string UserNote
		{
			get { return  strUserNote; }
			set { strUserNote = value; }
		}

		/// <summary>
		/// EndWarrantyDate
		/// 
		/// </summary>
		public DateTime? EndWarrantyDate
		{
			get { return  dtmEndWarrantyDate; }
			set { dtmEndWarrantyDate = value; }
		}

		/// <summary>
		/// IsHasWarranty
		/// 
		/// </summary>
		public bool IsHasWarranty
		{
			get { return  bolIsHasWarranty; }
			set { bolIsHasWarranty = value; }
		}

		/// <summary>
		/// InputPrice
		/// 
		/// </summary>
		public decimal InputPrice
		{
			get { return  decInputPrice; }
			set { decInputPrice = value; }
		}

		/// <summary>
		/// OutputPrice
		/// 
		/// </summary>
		public decimal OutputPrice
		{
			get { return  decOutputPrice; }
			set { decOutputPrice = value; }
		}

		/// <summary>
		/// CreatedStoreID
		/// 
		/// </summary>
		public int CreatedStoreID
		{
			get { return  intCreatedStoreID; }
			set { intCreatedStoreID = value; }
		}

		/// <summary>
		/// CreatedUser
		/// 
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// UpdatedUser
		/// 
		/// </summary>
		public string UpdatedUser
		{
			get { return  strUpdatedUser; }
			set { strUpdatedUser = value; }
		}

		/// <summary>
		/// UpdatedDate
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			get { return  dtmUpdatedDate; }
			set { dtmUpdatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// 
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// 
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// 
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public StoreChangeDetail()
		{
		}
		#endregion


		#region Column Names

		public const String colStoreChangeDetailID = "StoreChangeDetailID";
		public const String colStoreChangeID = "StoreChangeID";
		public const String colStoreChangeDate = "StoreChangeDate";
		public const String colProductID = "ProductID";
		public const String colQuantity = "Quantity";
		public const String colIMEI = "IMEI";
		public const String colStoreChangeOrderDetailID = "StoreChangeOrderDetailID";
		public const String colOutputVoucherDetailID = "OutputVoucherDetailID";
		public const String colInputVoucherDetailID = "InputVoucherDetailID";
		public const String colPackingNumber = "PackingNumber";
		public const String colOrderQuantity = "OrderQuantity";
		public const String colUserNote = "UserNote";
		public const String colEndWarrantyDate = "EndWarrantyDate";
		public const String colIsHasWarranty = "IsHasWarranty";
		public const String colInputPrice = "InputPrice";
		public const String colOutputPrice = "OutputPrice";
		public const String colCreatedStoreID = "CreatedStoreID";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colUpdatedUser = "UpdatedUser";
		public const String colUpdatedDate = "UpdatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";

		#endregion //Column names

		
	}
}
