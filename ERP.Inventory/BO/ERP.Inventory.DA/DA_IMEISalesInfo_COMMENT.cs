
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using IMEISalesInfo_Comment = ERP.Inventory.BO.IMEISalesInfo_Comment;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 12/01/14 
	/// 
	/// </summary>	
	public partial class DA_IMEISalesInfo_Comment
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objIMEISalesInfo_Comment">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref IMEISalesInfo_Comment objIMEISalesInfo_Comment, int intCommentID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentID, intCommentID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objIMEISalesInfo_Comment = new IMEISalesInfo_Comment();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colCommentID])) objIMEISalesInfo_Comment.CommentID = Convert.ToInt32(reader[IMEISalesInfo_Comment.colCommentID]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colReplyToCommentID])) objIMEISalesInfo_Comment.ReplyToCommentID = Convert.ToDecimal(reader[IMEISalesInfo_Comment.colReplyToCommentID]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colProductID])) objIMEISalesInfo_Comment.ProductID = Convert.ToString(reader[IMEISalesInfo_Comment.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colIMEI])) objIMEISalesInfo_Comment.IMEI = Convert.ToString(reader[IMEISalesInfo_Comment.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colCommentDate])) objIMEISalesInfo_Comment.CommentDate = Convert.ToDateTime(reader[IMEISalesInfo_Comment.colCommentDate]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colCommentTitle])) objIMEISalesInfo_Comment.CommentTitle = Convert.ToString(reader[IMEISalesInfo_Comment.colCommentTitle]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colCommentContent])) objIMEISalesInfo_Comment.CommentContent = Convert.ToString(reader[IMEISalesInfo_Comment.colCommentContent]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colCommentUser])) objIMEISalesInfo_Comment.CommentUser = Convert.ToString(reader[IMEISalesInfo_Comment.colCommentUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colCommentEmail])) objIMEISalesInfo_Comment.CommentEmail = Convert.ToString(reader[IMEISalesInfo_Comment.colCommentEmail]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colModelRate])) objIMEISalesInfo_Comment.ModelRate = Convert.ToInt32(reader[IMEISalesInfo_Comment.colModelRate]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colISCommentUserLogin])) objIMEISalesInfo_Comment.ISCommentUserLogin = Convert.ToBoolean(reader[IMEISalesInfo_Comment.colISCommentUserLogin]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colUserHostAddress])) objIMEISalesInfo_Comment.UserHostAddress = Convert.ToString(reader[IMEISalesInfo_Comment.colUserHostAddress]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colUserComputerName])) objIMEISalesInfo_Comment.UserComputerName = Convert.ToString(reader[IMEISalesInfo_Comment.colUserComputerName]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colUserAgent])) objIMEISalesInfo_Comment.UserAgent = Convert.ToString(reader[IMEISalesInfo_Comment.colUserAgent]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colIsStaffComment])) objIMEISalesInfo_Comment.IsStaffComment = Convert.ToBoolean(reader[IMEISalesInfo_Comment.colIsStaffComment]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colCommentFullName])) objIMEISalesInfo_Comment.CommentFullName = Convert.ToString(reader[IMEISalesInfo_Comment.colCommentFullName]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colOrderIndex])) objIMEISalesInfo_Comment.OrderIndex = Convert.ToDecimal(reader[IMEISalesInfo_Comment.colOrderIndex]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colIsActive])) objIMEISalesInfo_Comment.IsActive = Convert.ToBoolean(reader[IMEISalesInfo_Comment.colIsActive]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colIsSystem])) objIMEISalesInfo_Comment.IsSystem = Convert.ToBoolean(reader[IMEISalesInfo_Comment.colIsSystem]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colCreatedUser])) objIMEISalesInfo_Comment.CreatedUser = Convert.ToString(reader[IMEISalesInfo_Comment.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colCreatedDate])) objIMEISalesInfo_Comment.CreatedDate = Convert.ToDateTime(reader[IMEISalesInfo_Comment.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colUpdatedUser])) objIMEISalesInfo_Comment.UpdatedUser = Convert.ToString(reader[IMEISalesInfo_Comment.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colUpdatedDate])) objIMEISalesInfo_Comment.UpdatedDate = Convert.ToDateTime(reader[IMEISalesInfo_Comment.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colIsDeleted])) objIMEISalesInfo_Comment.IsDeleted = Convert.ToBoolean(reader[IMEISalesInfo_Comment.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colDeletedUser])) objIMEISalesInfo_Comment.DeletedUser = Convert.ToString(reader[IMEISalesInfo_Comment.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colDeletedDate])) objIMEISalesInfo_Comment.DeletedDate = Convert.ToDateTime(reader[IMEISalesInfo_Comment.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_Comment.colPhoneNumber])) objIMEISalesInfo_Comment.PhoneNumber = Convert.ToString(reader[IMEISalesInfo_Comment.colPhoneNumber]).Trim();
                    objIMEISalesInfo_Comment.IsExist = true;
                }
 				else
 				{
                    objIMEISalesInfo_Comment = new IMEISalesInfo_Comment();
                    objIMEISalesInfo_Comment.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_Comment -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIMEISalesInfo_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(IMEISalesInfo_Comment objIMEISalesInfo_Comment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objIMEISalesInfo_Comment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_Comment -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEISalesInfo_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, IMEISalesInfo_Comment objIMEISalesInfo_Comment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				//objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentID, objIMEISalesInfo_Comment.CommentID);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colReplyToCommentID, objIMEISalesInfo_Comment.ReplyToCommentID);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colProductID, objIMEISalesInfo_Comment.ProductID);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colIMEI, objIMEISalesInfo_Comment.IMEI);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentDate, objIMEISalesInfo_Comment.CommentDate);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentTitle, objIMEISalesInfo_Comment.CommentTitle);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentContent, objIMEISalesInfo_Comment.CommentContent);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentUser, objIMEISalesInfo_Comment.CommentUser);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentEmail, objIMEISalesInfo_Comment.CommentEmail);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colModelRate, objIMEISalesInfo_Comment.ModelRate);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colISCommentUserLogin, objIMEISalesInfo_Comment.ISCommentUserLogin);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colUserHostAddress, objIMEISalesInfo_Comment.UserHostAddress);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colUserComputerName, objIMEISalesInfo_Comment.UserComputerName);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colUserAgent, objIMEISalesInfo_Comment.UserAgent);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colIsStaffComment, objIMEISalesInfo_Comment.IsStaffComment);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentFullName, objIMEISalesInfo_Comment.CommentFullName);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colOrderIndex, objIMEISalesInfo_Comment.OrderIndex);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colIsActive, objIMEISalesInfo_Comment.IsActive);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colIsSystem, objIMEISalesInfo_Comment.IsSystem);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCreatedUser, objIMEISalesInfo_Comment.CreatedUser);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colPhoneNumber, objIMEISalesInfo_Comment.PhoneNumber);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIMEISalesInfo_Comment">Đối tượng truyền vào</param>
		/// <param name="lstOrderIndex">Danh sách OrderIndex</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(IMEISalesInfo_Comment objIMEISalesInfo_Comment, List<object> lstOrderIndex)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.BeginTransaction();
				Update(objIData, objIMEISalesInfo_Comment);
                if (lstOrderIndex != null && lstOrderIndex.Count > 0)
                {
                    for (int i = 0; i < lstOrderIndex.Count; i++)
                    {
                        UpdateOrderIndex(objIData, Convert.ToInt32(lstOrderIndex[i]), i);
                    }
                }
				objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_Comment -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEISalesInfo_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, IMEISalesInfo_Comment objIMEISalesInfo_Comment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentID, objIMEISalesInfo_Comment.CommentID);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colReplyToCommentID, objIMEISalesInfo_Comment.ReplyToCommentID);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colProductID, objIMEISalesInfo_Comment.ProductID);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colIMEI, objIMEISalesInfo_Comment.IMEI);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentDate, objIMEISalesInfo_Comment.CommentDate);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentTitle, objIMEISalesInfo_Comment.CommentTitle);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentContent, objIMEISalesInfo_Comment.CommentContent);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentUser, objIMEISalesInfo_Comment.CommentUser);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentEmail, objIMEISalesInfo_Comment.CommentEmail);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colModelRate, objIMEISalesInfo_Comment.ModelRate);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colISCommentUserLogin, objIMEISalesInfo_Comment.ISCommentUserLogin);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colUserHostAddress, objIMEISalesInfo_Comment.UserHostAddress);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colUserComputerName, objIMEISalesInfo_Comment.UserComputerName);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colUserAgent, objIMEISalesInfo_Comment.UserAgent);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colIsStaffComment, objIMEISalesInfo_Comment.IsStaffComment);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentFullName, objIMEISalesInfo_Comment.CommentFullName);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colOrderIndex, objIMEISalesInfo_Comment.OrderIndex);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colIsActive, objIMEISalesInfo_Comment.IsActive);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colIsSystem, objIMEISalesInfo_Comment.IsSystem);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colUpdatedUser, objIMEISalesInfo_Comment.UpdatedUser);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colPhoneNumber, objIMEISalesInfo_Comment.PhoneNumber);
               // objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="intOrderIndex">Thứ tự</param>
		/// <returns>Kết quả trả về</returns>
		public void UpdateOrderIndex(IData objIData, decimal decCommentID, int intOrderIndex)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATEINDEX);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentID, decCommentID);
                objIData.AddParameter("@OrderIndex", intOrderIndex);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIMEISalesInfo_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(IMEISalesInfo_Comment objIMEISalesInfo_Comment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objIMEISalesInfo_Comment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_Comment -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEISalesInfo_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, IMEISalesInfo_Comment objIMEISalesInfo_Comment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colCommentID, objIMEISalesInfo_Comment.CommentID);
				objIData.AddParameter("@" + IMEISalesInfo_Comment.colDeletedUser, objIMEISalesInfo_Comment.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_IMEISalesInfo_Comment()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_IMEISALESINFO_Comment_ADD";
		public const String SP_UPDATE = "PM_IMEISALESINFO_Comment_UPD";
		public const String SP_DELETE = "PM_IMEISALESINFO_Comment_DEL";
		public const String SP_SELECT = "PM_IMEISALESINFO_Comment_SEL";
		public const String SP_SEARCH = "PM_IMEISALESINFO_Comment_SRH";
		public const String SP_UPDATEINDEX = "PM_IMEISALESINFO_Comment_UPDINDEX";
		#endregion
		
	}
}
