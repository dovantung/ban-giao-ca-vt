
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using SequenceIMEI = ERP.Inventory.BO.SM.SequenceIMEI;
#endregion
namespace ERP.Inventory.DA.SM
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 10/09/14 
	/// Mức ưu tiên xuất kho của IMEI
	/// </summary>	
	public partial class DA_SequenceIMEI
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin mức ưu tiên xuất kho của imei
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_SequenceIMEI.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin mức ưu tiên xuất kho của imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SEQUENCEIMEI -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// Thêm thông tin mức ưu tiên xuất kho của imei
        /// </summary>
        /// <param name="objSequenceIMEI">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ref List<SequenceIMEI> lstSequenceIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                foreach (var objSequenceIMEI in lstSequenceIMEI)
                {
                    objSequenceIMEI.IsSuccess = Insert(objIData, objSequenceIMEI);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin mức ưu tiên xuất kho của imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SequenceIMEI -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
       
		#endregion
		
		
	}
}
