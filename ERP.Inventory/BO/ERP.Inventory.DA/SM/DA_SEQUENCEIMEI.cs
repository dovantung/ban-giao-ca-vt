
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using SequenceIMEI = ERP.Inventory.BO.SM.SequenceIMEI;
#endregion
namespace ERP.Inventory.DA.SM
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 10/09/14 
	/// Mức ưu tiên xuất kho của IMEI
	/// </summary>	
	public partial class DA_SequenceIMEI
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin mức ưu tiên xuất kho của imei
		/// </summary>
		/// <param name="objSequenceIMEI">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref SequenceIMEI objSequenceIMEI, string strSequenceIMEILID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + SequenceIMEI.colSequenceIMEILID, strSequenceIMEILID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objSequenceIMEI = new SequenceIMEI();
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colSequenceIMEILID])) objSequenceIMEI.SequenceIMEILID = Convert.ToString(reader[SequenceIMEI.colSequenceIMEILID]).Trim();
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colProductID])) objSequenceIMEI.ProductID = Convert.ToString(reader[SequenceIMEI.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colIMEI])) objSequenceIMEI.IMEI = Convert.ToString(reader[SequenceIMEI.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colOrderIndex])) objSequenceIMEI.OrderIndex = Convert.ToInt32(reader[SequenceIMEI.colOrderIndex]);
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colCreatedUser])) objSequenceIMEI.CreatedUser = Convert.ToString(reader[SequenceIMEI.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colCreatedDate])) objSequenceIMEI.CreatedDate = Convert.ToDateTime(reader[SequenceIMEI.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colIsDeleted])) objSequenceIMEI.IsDeleted = Convert.ToBoolean(reader[SequenceIMEI.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colDeletedUser])) objSequenceIMEI.DeletedUser = Convert.ToString(reader[SequenceIMEI.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colDeletedDate])) objSequenceIMEI.DeletedDate = Convert.ToDateTime(reader[SequenceIMEI.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[SequenceIMEI.colSYNCHDate])) objSequenceIMEI.SynchDate = Convert.ToDateTime(reader[SequenceIMEI.colSYNCHDate]);
 				}
 				else
 				{
 					objSequenceIMEI = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin mức ưu tiên xuất kho của imei", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SequenceIMEI -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

        /// <summary>
        /// Thêm thông tin mức ưu tiên xuất kho của imei
        /// </summary>
        /// <param name="objSequenceIMEI">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(SequenceIMEI objSequenceIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objSequenceIMEI);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin mức ưu tiên xuất kho của imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SequenceIMEI -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


		/// <summary>
		/// Thêm thông tin mức ưu tiên xuất kho của imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objSequenceIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public bool Insert(IData objIData, SequenceIMEI objSequenceIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				//objIData.AddParameter("@" + SequenceIMEI.colSequenceIMEILID, objSequenceIMEI.SequenceIMEILID);
				objIData.AddParameter("@" + SequenceIMEI.colProductID, objSequenceIMEI.ProductID);
				objIData.AddParameter("@" + SequenceIMEI.colIMEI, objSequenceIMEI.IMEI);
				objIData.AddParameter("@" + SequenceIMEI.colOrderIndex, objSequenceIMEI.OrderIndex);
				objIData.AddParameter("@" + SequenceIMEI.colCreatedUser, objSequenceIMEI.CreatedUser);
				objIData.AddParameter("@" + SequenceIMEI.colSYNCHDate, objSequenceIMEI.SynchDate);
                objIData.ExecNonQuery();
                return true;
			}
			catch (Exception objEx)
			{
                //objIData.RollBackTransaction();
                //throw (objEx);
                return false;
			}
		}


		/// <summary>
		/// Cập nhật thông tin mức ưu tiên xuất kho của imei
		/// </summary>
		/// <param name="objSequenceIMEI">Đối tượng truyền vào</param>
		/// <param name="lstOrderIndex">Danh sách OrderIndex</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(SequenceIMEI objSequenceIMEI, List<object> lstOrderIndex)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.BeginTransaction();
				Update(objIData, objSequenceIMEI);
				if (lstOrderIndex != null && lstOrderIndex.Count > 0)
				{
					for (int i = 0; i < lstOrderIndex.Count; i++)
					{
						UpdateOrderIndex(objIData, Convert.ToString(lstOrderIndex[i]), i);
					}
				}
				objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin mức ưu tiên xuất kho của imei", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SequenceIMEI -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin mức ưu tiên xuất kho của imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objSequenceIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, SequenceIMEI objSequenceIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + SequenceIMEI.colSequenceIMEILID, objSequenceIMEI.SequenceIMEILID);
				objIData.AddParameter("@" + SequenceIMEI.colProductID, objSequenceIMEI.ProductID);
				objIData.AddParameter("@" + SequenceIMEI.colIMEI, objSequenceIMEI.IMEI);
				objIData.AddParameter("@" + SequenceIMEI.colOrderIndex, objSequenceIMEI.OrderIndex);
				objIData.AddParameter("@" + SequenceIMEI.colSYNCHDate, objSequenceIMEI.SynchDate);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin mức ưu tiên xuất kho của imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="intOrderIndex">Thứ tự</param>
		/// <returns>Kết quả trả về</returns>
		public void UpdateOrderIndex(IData objIData, string strSequenceIMEILID, int intOrderIndex)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATEINDEX);
				objIData.AddParameter("@" + SequenceIMEI.colSequenceIMEILID, strSequenceIMEILID);
                objIData.AddParameter("@OrderIndex", intOrderIndex);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin mức ưu tiên xuất kho của imei
		/// </summary>
		/// <param name="objSequenceIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(SequenceIMEI objSequenceIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objSequenceIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin mức ưu tiên xuất kho của imei", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SequenceIMEI -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin mức ưu tiên xuất kho của imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objSequenceIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, SequenceIMEI objSequenceIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + SequenceIMEI.colSequenceIMEILID, objSequenceIMEI.SequenceIMEILID);
				objIData.AddParameter("@" + SequenceIMEI.colDeletedUser, objSequenceIMEI.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_SequenceIMEI()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "SM_SequenceIMEI_ADD";
		public const String SP_UPDATE = "SM_SequenceIMEI_UPD";
		public const String SP_DELETE = "SM_SequenceIMEI_DEL";
		public const String SP_SELECT = "SM_SequenceIMEI_SEL";
		public const String SP_SEARCH = "SM_SequenceIMEI_SRH";
		public const String SP_UPDATEINDEX = "SM_SequenceIMEI_UPDINDEX";
		#endregion
		
	}
}
