
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using OutputVoucherDetail = ERP.Inventory.BO.OutputVoucherDetail;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
    /// Created by 		: Trương Trung Lợi 
    /// Created date 	: 13/09/2012 
    /// 
    /// </summary>	
    public partial class DA_OutputVoucherDetail
    {


        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods


        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objOutputVoucherDetail">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref OutputVoucherDetail objOutputVoucherDetail, string strOutputVoucherDetailID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputVoucherDetailID, strOutputVoucherDetailID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objOutputVoucherDetail = new OutputVoucherDetail();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colOutputVoucherDetailID])) objOutputVoucherDetail.OutputVoucherDetailID = Convert.ToString(reader[OutputVoucherDetail.colOutputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colOutputVoucherID])) objOutputVoucherDetail.OutputVoucherID = Convert.ToString(reader[OutputVoucherDetail.colOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colProductID])) objOutputVoucherDetail.ProductID = Convert.ToString(reader[OutputVoucherDetail.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colProductSalesKitID])) objOutputVoucherDetail.ProductSalesKitID = Convert.ToInt32(reader[OutputVoucherDetail.colProductSalesKitID]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colRefProductID])) objOutputVoucherDetail.RefProductID = Convert.ToString(reader[OutputVoucherDetail.colRefProductID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colProductComboID])) objOutputVoucherDetail.ProductComboID = Convert.ToString(reader[OutputVoucherDetail.colProductComboID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIMEI])) objOutputVoucherDetail.IMEI = Convert.ToString(reader[OutputVoucherDetail.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colExtendWarrantyIMEI])) objOutputVoucherDetail.ExtendWarrantyIMEI = Convert.ToString(reader[OutputVoucherDetail.colExtendWarrantyIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colPINCode])) objOutputVoucherDetail.PINCode = Convert.ToString(reader[OutputVoucherDetail.colPINCode]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colQuantity])) objOutputVoucherDetail.Quantity = Convert.ToDecimal(reader[OutputVoucherDetail.colQuantity]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colReturnQuantity])) objOutputVoucherDetail.ReturnQuantity = Convert.ToDecimal(reader[OutputVoucherDetail.colReturnQuantity]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colSalePrice])) objOutputVoucherDetail.SalePrice = Convert.ToDecimal(reader[OutputVoucherDetail.colSalePrice]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colBFAdjustSalePrice])) objOutputVoucherDetail.BFAdjustSalePrice = Convert.ToDecimal(reader[OutputVoucherDetail.colBFAdjustSalePrice]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colOriginalSalePrice])) objOutputVoucherDetail.OriginalSalePrice = Convert.ToDecimal(reader[OutputVoucherDetail.colOriginalSalePrice]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colCostPrice])) objOutputVoucherDetail.CostPrice = Convert.ToDecimal(reader[OutputVoucherDetail.colCostPrice]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colRetailPrice])) objOutputVoucherDetail.RetailPrice = Convert.ToDecimal(reader[OutputVoucherDetail.colRetailPrice]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colVAT])) objOutputVoucherDetail.VAT = Convert.ToInt32(reader[OutputVoucherDetail.colVAT]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colVATPercent])) objOutputVoucherDetail.VATPercent = Convert.ToInt32(reader[OutputVoucherDetail.colVATPercent]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colOutputTypeID])) objOutputVoucherDetail.OutputTypeID = Convert.ToInt32(reader[OutputVoucherDetail.colOutputTypeID]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colInputVoucherDetailID])) objOutputVoucherDetail.InputVoucherDetailID = Convert.ToString(reader[OutputVoucherDetail.colInputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colEndWarrantyDate])) objOutputVoucherDetail.EndWarrantyDate = Convert.ToDateTime(reader[OutputVoucherDetail.colEndWarrantyDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsNew])) objOutputVoucherDetail.IsNew = Convert.ToBoolean(reader[OutputVoucherDetail.colIsNew]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsShowProduct])) objOutputVoucherDetail.IsShowProduct = Convert.ToBoolean(reader[OutputVoucherDetail.colIsShowProduct]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsHasWarranty])) objOutputVoucherDetail.IsHasWarranty = Convert.ToBoolean(reader[OutputVoucherDetail.colIsHasWarranty]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colFirstInputDate])) objOutputVoucherDetail.FirstInputDate = Convert.ToDateTime(reader[OutputVoucherDetail.colFirstInputDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colFirstInvoiceDate])) objOutputVoucherDetail.FirstInvoiceDate = Convert.ToDateTime(reader[OutputVoucherDetail.colFirstInvoiceDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colFirstCustomerID])) objOutputVoucherDetail.FirstCustomerID = Convert.ToInt32(reader[OutputVoucherDetail.colFirstCustomerID]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colFirstInputTypeID])) objOutputVoucherDetail.FirstInputTypeID = Convert.ToInt32(reader[OutputVoucherDetail.colFirstInputTypeID]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colFirstInputVoucherID])) objOutputVoucherDetail.FirstInputVoucherID = Convert.ToString(reader[OutputVoucherDetail.colFirstInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colFirstInputVoucherDetailID])) objOutputVoucherDetail.FirstInputVoucherDetailID = Convert.ToString(reader[OutputVoucherDetail.colFirstInputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colPromotionID])) objOutputVoucherDetail.PromotionID = Convert.ToInt32(reader[OutputVoucherDetail.colPromotionID]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colPromotionOfferID])) objOutputVoucherDetail.PromotionOfferID = Convert.ToInt32(reader[OutputVoucherDetail.colPromotionOfferID]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colApplyProductID])) objOutputVoucherDetail.ApplyProductID = Convert.ToString(reader[OutputVoucherDetail.colApplyProductID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colApplySaleOrderDetailID])) objOutputVoucherDetail.ApplySaleOrderDetailID = Convert.ToString(reader[OutputVoucherDetail.colApplySaleOrderDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colApplySaleOrderID])) objOutputVoucherDetail.ApplySaleOrderID = Convert.ToString(reader[OutputVoucherDetail.colApplySaleOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colSaleOrderDetailID])) objOutputVoucherDetail.SaleOrderDetailID = Convert.ToString(reader[OutputVoucherDetail.colSaleOrderDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colPackingNumber])) objOutputVoucherDetail.PackingNumber = Convert.ToInt32(reader[OutputVoucherDetail.colPackingNumber]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsReturnProduct])) objOutputVoucherDetail.IsReturnProduct = Convert.ToBoolean(reader[OutputVoucherDetail.colIsReturnProduct]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsReturnWithFee])) objOutputVoucherDetail.IsReturnWithFee = Convert.ToBoolean(reader[OutputVoucherDetail.colIsReturnWithFee]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsProductChange])) objOutputVoucherDetail.IsProductChange = Convert.ToBoolean(reader[OutputVoucherDetail.colIsProductChange]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsSubtractInStock])) objOutputVoucherDetail.IsSubtractInStock = Convert.ToBoolean(reader[OutputVoucherDetail.colIsSubtractInStock]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsRequireVoucher])) objOutputVoucherDetail.IsRequireVoucher = Convert.ToBoolean(reader[OutputVoucherDetail.colIsRequireVoucher]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsConsignmentInput])) objOutputVoucherDetail.IsConsignmentInput = Convert.ToBoolean(reader[OutputVoucherDetail.colIsConsignmentInput]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsInputChange])) objOutputVoucherDetail.IsInputChange = Convert.ToBoolean(reader[OutputVoucherDetail.colIsInputChange]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colInStockStoreID])) objOutputVoucherDetail.InStockStoreID = Convert.ToDecimal(reader[OutputVoucherDetail.colInStockStoreID]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colCreatedStoreID])) objOutputVoucherDetail.CreatedStoreID = Convert.ToInt32(reader[OutputVoucherDetail.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colOutputStoreID])) objOutputVoucherDetail.OutputStoreID = Convert.ToInt32(reader[OutputVoucherDetail.colOutputStoreID]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colOutputDate])) objOutputVoucherDetail.OutputDate = Convert.ToDateTime(reader[OutputVoucherDetail.colOutputDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colReturnDate])) objOutputVoucherDetail.ReturnDate = Convert.ToDateTime(reader[OutputVoucherDetail.colReturnDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colConsignmentInputDate])) objOutputVoucherDetail.ConsignmentInputDate = Convert.ToDateTime(reader[OutputVoucherDetail.colConsignmentInputDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colInputChangeDate])) objOutputVoucherDetail.InputChangeDate = Convert.ToDateTime(reader[OutputVoucherDetail.colInputChangeDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colRelateVoucherID])) objOutputVoucherDetail.RelateVoucherID = Convert.ToString(reader[OutputVoucherDetail.colRelateVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIMEIChangeChain])) objOutputVoucherDetail.IMEIChangeChain = Convert.ToString(reader[OutputVoucherDetail.colIMEIChangeChain]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colInvoiceNote])) objOutputVoucherDetail.InvoiceNote = Convert.ToString(reader[OutputVoucherDetail.colInvoiceNote]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colCreatedUser])) objOutputVoucherDetail.CreatedUser = Convert.ToString(reader[OutputVoucherDetail.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colCreatedDate])) objOutputVoucherDetail.CreatedDate = Convert.ToDateTime(reader[OutputVoucherDetail.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colUpdatedUser])) objOutputVoucherDetail.UpdatedUser = Convert.ToString(reader[OutputVoucherDetail.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colUpdatedDate])) objOutputVoucherDetail.UpdatedDate = Convert.ToDateTime(reader[OutputVoucherDetail.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsDeleted])) objOutputVoucherDetail.IsDeleted = Convert.ToBoolean(reader[OutputVoucherDetail.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colDeletedUser])) objOutputVoucherDetail.DeletedUser = Convert.ToString(reader[OutputVoucherDetail.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colDeletedDate])) objOutputVoucherDetail.DeletedDate = Convert.ToDateTime(reader[OutputVoucherDetail.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colAdjustPrice])) objOutputVoucherDetail.AdjustPrice = Convert.ToDecimal(reader[OutputVoucherDetail.colAdjustPrice]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colIsPromotionProduct])) objOutputVoucherDetail.IsPromotionProduct = Convert.ToBoolean(reader[OutputVoucherDetail.colIsPromotionProduct]);
                    if (!Convert.IsDBNull(reader[OutputVoucherDetail.colTaxAmount])) objOutputVoucherDetail.TaxAmount = Convert.ToDecimal(reader[OutputVoucherDetail.colTaxAmount]);

                }
                else
                {
                    objOutputVoucherDetail = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucherDetail -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objOutputVoucherDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(OutputVoucherDetail objOutputVoucherDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objOutputVoucherDetail);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucherDetail -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
     

        /// <summary>
        /// huê
        /// Thêm thông tin phiếu xuất chi tiết có afvat
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objOutputVoucherDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert_V2(IData objIData, OutputVoucherDetail objOutputVoucherDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD_V2);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputVoucherID, objOutputVoucherDetail.OutputVoucherID);
                objIData.AddParameter("@" + OutputVoucherDetail.colProductID, objOutputVoucherDetail.ProductID);
                objIData.AddParameter("@" + OutputVoucherDetail.colProductSalesKitID, objOutputVoucherDetail.ProductSalesKitID);
                objIData.AddParameter("@" + OutputVoucherDetail.colRefProductID, objOutputVoucherDetail.RefProductID);
                objIData.AddParameter("@" + OutputVoucherDetail.colProductComboID, objOutputVoucherDetail.ProductComboID);
                objIData.AddParameter("@" + OutputVoucherDetail.colIMEI, objOutputVoucherDetail.IMEI);
                objIData.AddParameter("@" + OutputVoucherDetail.colExtendWarrantyIMEI, objOutputVoucherDetail.ExtendWarrantyIMEI);
                objIData.AddParameter("@" + OutputVoucherDetail.colPINCode, objOutputVoucherDetail.PINCode);
                objIData.AddParameter("@" + OutputVoucherDetail.colQuantity, objOutputVoucherDetail.Quantity);
                objIData.AddParameter("@" + OutputVoucherDetail.colReturnQuantity, objOutputVoucherDetail.ReturnQuantity);
                objIData.AddParameter("@" + OutputVoucherDetail.colSalePrice, objOutputVoucherDetail.SalePrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colSalePriceAfVat, objOutputVoucherDetail.SalePriceAfVat); ////huent66 cập nhật: SalePriceAfVat   
                objIData.AddParameter("@" + OutputVoucherDetail.colBFAdjustSalePrice, objOutputVoucherDetail.BFAdjustSalePrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colOriginalSalePrice, objOutputVoucherDetail.OriginalSalePrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colCostPrice, objOutputVoucherDetail.CostPrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colRetailPrice, objOutputVoucherDetail.RetailPrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colVAT, objOutputVoucherDetail.VAT);
                objIData.AddParameter("@" + OutputVoucherDetail.colVATPercent, objOutputVoucherDetail.VATPercent);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputTypeID, objOutputVoucherDetail.OutputTypeID);
                objIData.AddParameter("@" + OutputVoucherDetail.colInputVoucherDetailID, objOutputVoucherDetail.InputVoucherDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colEndWarrantyDate, objOutputVoucherDetail.EndWarrantyDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsNew, objOutputVoucherDetail.IsNew);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsShowProduct, objOutputVoucherDetail.IsShowProduct);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsHasWarranty, objOutputVoucherDetail.IsHasWarranty);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputDate, objOutputVoucherDetail.FirstInputDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInvoiceDate, objOutputVoucherDetail.FirstInvoiceDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstCustomerID, objOutputVoucherDetail.FirstCustomerID);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputTypeID, objOutputVoucherDetail.FirstInputTypeID);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputVoucherID, objOutputVoucherDetail.FirstInputVoucherID);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputVoucherDetailID, objOutputVoucherDetail.FirstInputVoucherDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colPromotionID, objOutputVoucherDetail.PromotionID);
                objIData.AddParameter("@" + OutputVoucherDetail.colPromotionOfferID, objOutputVoucherDetail.PromotionOfferID);
                objIData.AddParameter("@" + OutputVoucherDetail.colApplyProductID, objOutputVoucherDetail.ApplyProductID);
                objIData.AddParameter("@" + OutputVoucherDetail.colApplySaleOrderDetailID, objOutputVoucherDetail.ApplySaleOrderDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colApplySaleOrderID, objOutputVoucherDetail.ApplySaleOrderID);
                objIData.AddParameter("@" + OutputVoucherDetail.colSaleOrderDetailID, objOutputVoucherDetail.SaleOrderDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colPackingNumber, objOutputVoucherDetail.PackingNumber);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsReturnProduct, objOutputVoucherDetail.IsReturnProduct);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsReturnWithFee, objOutputVoucherDetail.IsReturnWithFee);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsProductChange, objOutputVoucherDetail.IsProductChange);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsSubtractInStock, objOutputVoucherDetail.IsSubtractInStock);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsRequireVoucher, objOutputVoucherDetail.IsRequireVoucher);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsConsignmentInput, objOutputVoucherDetail.IsConsignmentInput);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsInputChange, objOutputVoucherDetail.IsInputChange);
                objIData.AddParameter("@" + OutputVoucherDetail.colInStockStoreID, objOutputVoucherDetail.InStockStoreID);
                objIData.AddParameter("@" + OutputVoucherDetail.colCreatedStoreID, objOutputVoucherDetail.CreatedStoreID);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputStoreID, objOutputVoucherDetail.OutputStoreID);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputDate, objOutputVoucherDetail.OutputDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colReturnDate, objOutputVoucherDetail.ReturnDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colConsignmentInputDate, objOutputVoucherDetail.ConsignmentInputDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colInputChangeDate, objOutputVoucherDetail.InputChangeDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colRelateVoucherID, objOutputVoucherDetail.RelateVoucherID);
                objIData.AddParameter("@" + OutputVoucherDetail.colIMEIChangeChain, objOutputVoucherDetail.IMEIChangeChain);
                objIData.AddParameter("@" + OutputVoucherDetail.colInvoiceNote, objOutputVoucherDetail.InvoiceNote);
                objIData.AddParameter("@" + OutputVoucherDetail.colCreatedUser, objOutputVoucherDetail.CreatedUser);
                objIData.AddParameter("@" + OutputVoucherDetail.colAdjustPrice, objOutputVoucherDetail.AdjustPrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsPromotionProduct, objOutputVoucherDetail.IsPromotionProduct);
                if (objOutputVoucherDetail.TaxAmount != 0)
                    objIData.AddParameter("@" + OutputVoucherDetail.colTaxAmount, objOutputVoucherDetail.TaxAmount);

                objIData.AddParameter("@SaleOrderID", objOutputVoucherDetail.SaleOrderID);
                objIData.AddParameter("@InvoiceID", objOutputVoucherDetail.InvoiceID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + OutputVoucherDetail.colCreatedDate, objOutputVoucherDetail.CreatedDate);
                objOutputVoucherDetail.OutputVoucherDetailID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objOutputVoucherDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, OutputVoucherDetail objOutputVoucherDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputVoucherID, objOutputVoucherDetail.OutputVoucherID);
                objIData.AddParameter("@" + OutputVoucherDetail.colProductID, objOutputVoucherDetail.ProductID);
                objIData.AddParameter("@" + OutputVoucherDetail.colProductSalesKitID, objOutputVoucherDetail.ProductSalesKitID);
                objIData.AddParameter("@" + OutputVoucherDetail.colRefProductID, objOutputVoucherDetail.RefProductID);
                objIData.AddParameter("@" + OutputVoucherDetail.colProductComboID, objOutputVoucherDetail.ProductComboID);
                objIData.AddParameter("@" + OutputVoucherDetail.colIMEI, objOutputVoucherDetail.IMEI);
                objIData.AddParameter("@" + OutputVoucherDetail.colExtendWarrantyIMEI, objOutputVoucherDetail.ExtendWarrantyIMEI);
                objIData.AddParameter("@" + OutputVoucherDetail.colPINCode, objOutputVoucherDetail.PINCode);
                objIData.AddParameter("@" + OutputVoucherDetail.colQuantity, objOutputVoucherDetail.Quantity);
                objIData.AddParameter("@" + OutputVoucherDetail.colReturnQuantity, objOutputVoucherDetail.ReturnQuantity);
                objIData.AddParameter("@" + OutputVoucherDetail.colSalePrice, objOutputVoucherDetail.SalePrice);
                             
                objIData.AddParameter("@" + OutputVoucherDetail.colBFAdjustSalePrice, objOutputVoucherDetail.BFAdjustSalePrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colOriginalSalePrice, objOutputVoucherDetail.OriginalSalePrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colCostPrice, objOutputVoucherDetail.CostPrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colRetailPrice, objOutputVoucherDetail.RetailPrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colVAT, objOutputVoucherDetail.VAT);
                objIData.AddParameter("@" + OutputVoucherDetail.colVATPercent, objOutputVoucherDetail.VATPercent);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputTypeID, objOutputVoucherDetail.OutputTypeID);
                objIData.AddParameter("@" + OutputVoucherDetail.colInputVoucherDetailID, objOutputVoucherDetail.InputVoucherDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colEndWarrantyDate, objOutputVoucherDetail.EndWarrantyDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsNew, objOutputVoucherDetail.IsNew);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsShowProduct, objOutputVoucherDetail.IsShowProduct);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsHasWarranty, objOutputVoucherDetail.IsHasWarranty);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputDate, objOutputVoucherDetail.FirstInputDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInvoiceDate, objOutputVoucherDetail.FirstInvoiceDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstCustomerID, objOutputVoucherDetail.FirstCustomerID);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputTypeID, objOutputVoucherDetail.FirstInputTypeID);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputVoucherID, objOutputVoucherDetail.FirstInputVoucherID);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputVoucherDetailID, objOutputVoucherDetail.FirstInputVoucherDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colPromotionID, objOutputVoucherDetail.PromotionID);
                objIData.AddParameter("@" + OutputVoucherDetail.colPromotionOfferID, objOutputVoucherDetail.PromotionOfferID);
                objIData.AddParameter("@" + OutputVoucherDetail.colApplyProductID, objOutputVoucherDetail.ApplyProductID);
                objIData.AddParameter("@" + OutputVoucherDetail.colApplySaleOrderDetailID, objOutputVoucherDetail.ApplySaleOrderDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colApplySaleOrderID, objOutputVoucherDetail.ApplySaleOrderID);
                objIData.AddParameter("@" + OutputVoucherDetail.colSaleOrderDetailID, objOutputVoucherDetail.SaleOrderDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colPackingNumber, objOutputVoucherDetail.PackingNumber);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsReturnProduct, objOutputVoucherDetail.IsReturnProduct);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsReturnWithFee, objOutputVoucherDetail.IsReturnWithFee);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsProductChange, objOutputVoucherDetail.IsProductChange);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsSubtractInStock, objOutputVoucherDetail.IsSubtractInStock);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsRequireVoucher, objOutputVoucherDetail.IsRequireVoucher);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsConsignmentInput, objOutputVoucherDetail.IsConsignmentInput);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsInputChange, objOutputVoucherDetail.IsInputChange);
                objIData.AddParameter("@" + OutputVoucherDetail.colInStockStoreID, objOutputVoucherDetail.InStockStoreID);
                objIData.AddParameter("@" + OutputVoucherDetail.colCreatedStoreID, objOutputVoucherDetail.CreatedStoreID);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputStoreID, objOutputVoucherDetail.OutputStoreID);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputDate, objOutputVoucherDetail.OutputDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colReturnDate, objOutputVoucherDetail.ReturnDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colConsignmentInputDate, objOutputVoucherDetail.ConsignmentInputDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colInputChangeDate, objOutputVoucherDetail.InputChangeDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colRelateVoucherID, objOutputVoucherDetail.RelateVoucherID);
                objIData.AddParameter("@" + OutputVoucherDetail.colIMEIChangeChain, objOutputVoucherDetail.IMEIChangeChain);
                objIData.AddParameter("@" + OutputVoucherDetail.colInvoiceNote, objOutputVoucherDetail.InvoiceNote);
                objIData.AddParameter("@" + OutputVoucherDetail.colCreatedUser, objOutputVoucherDetail.CreatedUser);
                objIData.AddParameter("@" + OutputVoucherDetail.colAdjustPrice, objOutputVoucherDetail.AdjustPrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsPromotionProduct, objOutputVoucherDetail.IsPromotionProduct);
                if (objOutputVoucherDetail.TaxAmount != 0)
                    objIData.AddParameter("@" + OutputVoucherDetail.colTaxAmount, objOutputVoucherDetail.TaxAmount);

                objIData.AddParameter("@SaleOrderID", objOutputVoucherDetail.SaleOrderID);
                objIData.AddParameter("@InvoiceID", objOutputVoucherDetail.InvoiceID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + OutputVoucherDetail.colCreatedDate, objOutputVoucherDetail.CreatedDate);
                objOutputVoucherDetail.OutputVoucherDetailID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objOutputVoucherDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(OutputVoucherDetail objOutputVoucherDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objOutputVoucherDetail);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucherDetail -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objOutputVoucherDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, OutputVoucherDetail objOutputVoucherDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputVoucherDetailID, objOutputVoucherDetail.OutputVoucherDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputVoucherID, objOutputVoucherDetail.OutputVoucherID);
                objIData.AddParameter("@" + OutputVoucherDetail.colProductID, objOutputVoucherDetail.ProductID);
                objIData.AddParameter("@" + OutputVoucherDetail.colProductSalesKitID, objOutputVoucherDetail.ProductSalesKitID);
                objIData.AddParameter("@" + OutputVoucherDetail.colRefProductID, objOutputVoucherDetail.RefProductID);
                objIData.AddParameter("@" + OutputVoucherDetail.colProductComboID, objOutputVoucherDetail.ProductComboID);
                objIData.AddParameter("@" + OutputVoucherDetail.colIMEI, objOutputVoucherDetail.IMEI);
                objIData.AddParameter("@" + OutputVoucherDetail.colExtendWarrantyIMEI, objOutputVoucherDetail.ExtendWarrantyIMEI);
                objIData.AddParameter("@" + OutputVoucherDetail.colPINCode, objOutputVoucherDetail.PINCode);
                objIData.AddParameter("@" + OutputVoucherDetail.colQuantity, objOutputVoucherDetail.Quantity);
                objIData.AddParameter("@" + OutputVoucherDetail.colReturnQuantity, objOutputVoucherDetail.ReturnQuantity);
                objIData.AddParameter("@" + OutputVoucherDetail.colSalePrice, objOutputVoucherDetail.SalePrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colBFAdjustSalePrice, objOutputVoucherDetail.BFAdjustSalePrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colOriginalSalePrice, objOutputVoucherDetail.OriginalSalePrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colCostPrice, objOutputVoucherDetail.CostPrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colRetailPrice, objOutputVoucherDetail.RetailPrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colVAT, objOutputVoucherDetail.VAT);
                objIData.AddParameter("@" + OutputVoucherDetail.colVATPercent, objOutputVoucherDetail.VATPercent);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputTypeID, objOutputVoucherDetail.OutputTypeID);
                objIData.AddParameter("@" + OutputVoucherDetail.colInputVoucherDetailID, objOutputVoucherDetail.InputVoucherDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colEndWarrantyDate, objOutputVoucherDetail.EndWarrantyDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsNew, objOutputVoucherDetail.IsNew);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsShowProduct, objOutputVoucherDetail.IsShowProduct);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsHasWarranty, objOutputVoucherDetail.IsHasWarranty);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputDate, objOutputVoucherDetail.FirstInputDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInvoiceDate, objOutputVoucherDetail.FirstInvoiceDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstCustomerID, objOutputVoucherDetail.FirstCustomerID);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputTypeID, objOutputVoucherDetail.FirstInputTypeID);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputVoucherID, objOutputVoucherDetail.FirstInputVoucherID);
                objIData.AddParameter("@" + OutputVoucherDetail.colFirstInputVoucherDetailID, objOutputVoucherDetail.FirstInputVoucherDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colPromotionID, objOutputVoucherDetail.PromotionID);
                objIData.AddParameter("@" + OutputVoucherDetail.colPromotionOfferID, objOutputVoucherDetail.PromotionOfferID);
                objIData.AddParameter("@" + OutputVoucherDetail.colApplyProductID, objOutputVoucherDetail.ApplyProductID);
                objIData.AddParameter("@" + OutputVoucherDetail.colApplySaleOrderDetailID, objOutputVoucherDetail.ApplySaleOrderDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colApplySaleOrderID, objOutputVoucherDetail.ApplySaleOrderID);
                objIData.AddParameter("@" + OutputVoucherDetail.colSaleOrderDetailID, objOutputVoucherDetail.SaleOrderDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colPackingNumber, objOutputVoucherDetail.PackingNumber);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsReturnProduct, objOutputVoucherDetail.IsReturnProduct);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsReturnWithFee, objOutputVoucherDetail.IsReturnWithFee);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsProductChange, objOutputVoucherDetail.IsProductChange);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsSubtractInStock, objOutputVoucherDetail.IsSubtractInStock);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsRequireVoucher, objOutputVoucherDetail.IsRequireVoucher);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsConsignmentInput, objOutputVoucherDetail.IsConsignmentInput);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsInputChange, objOutputVoucherDetail.IsInputChange);
                objIData.AddParameter("@" + OutputVoucherDetail.colInStockStoreID, objOutputVoucherDetail.InStockStoreID);
                objIData.AddParameter("@" + OutputVoucherDetail.colCreatedStoreID, objOutputVoucherDetail.CreatedStoreID);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputStoreID, objOutputVoucherDetail.OutputStoreID);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputDate, objOutputVoucherDetail.OutputDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colReturnDate, objOutputVoucherDetail.ReturnDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colConsignmentInputDate, objOutputVoucherDetail.ConsignmentInputDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colInputChangeDate, objOutputVoucherDetail.InputChangeDate);
                objIData.AddParameter("@" + OutputVoucherDetail.colRelateVoucherID, objOutputVoucherDetail.RelateVoucherID);
                objIData.AddParameter("@" + OutputVoucherDetail.colIMEIChangeChain, objOutputVoucherDetail.IMEIChangeChain);
                objIData.AddParameter("@" + OutputVoucherDetail.colInvoiceNote, objOutputVoucherDetail.InvoiceNote);
                objIData.AddParameter("@" + OutputVoucherDetail.colUpdatedUser, objOutputVoucherDetail.UpdatedUser);
                objIData.AddParameter("@" + OutputVoucherDetail.colAdjustPrice, objOutputVoucherDetail.AdjustPrice);
                objIData.AddParameter("@" + OutputVoucherDetail.colIsPromotionProduct, objOutputVoucherDetail.IsPromotionProduct);
                if (objOutputVoucherDetail.TaxAmount != 0)
                    objIData.AddParameter("@" + OutputVoucherDetail.colTaxAmount, objOutputVoucherDetail.TaxAmount);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objOutputVoucherDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(OutputVoucherDetail objOutputVoucherDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objOutputVoucherDetail);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucherDetail -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objOutputVoucherDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, OutputVoucherDetail objOutputVoucherDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + OutputVoucherDetail.colOutputVoucherDetailID, objOutputVoucherDetail.OutputVoucherDetailID);
                objIData.AddParameter("@" + OutputVoucherDetail.colDeletedUser, objOutputVoucherDetail.DeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_OutputVoucherDetail()
        {
        }
        #endregion


        #region Stored Procedure Names
        public const String SP_ADD_V2 = "PM_OUTPUTVOUCHERDETAIL_ADD_V2";
        public const String SP_ADD = "PM_OUTPUTVOUCHERDETAIL_ADD";
        public const String SP_UPDATE = "PM_OUTPUTVOUCHERDETAIL_UPD";
        public const String SP_DELETE = "PM_OUTPUTVOUCHERDETAIL_DEL";
        public const String SP_SELECT = "PM_OUTPUTVOUCHERDETAIL_SEL";
        public const String SP_SEARCH = "PM_OUTPUTVOUCHERDETAIL_SRH";
        public const String SP_UPDATEINDEX = "PM_OUTPUTVOUCHERDETAIL_UPDINDEX";
        #endregion

    }
}
