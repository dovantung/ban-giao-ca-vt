
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Library.WebCore;
using Library.DataAccess;
using System.Text;
using System.Xml;
using ERP.Inventory.BO;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 11/22/2012 
	/// Danh sách lệnh chuyển kho được tính lúc chia hàng
	/// </summary>	
	public partial class DA_StoreChangeCommand
    {

        #region Methods			
        
        /// <summary>
        /// Tìm kiếm thông tin danh sách lệnh chuyển kho được tính lúc chia hàng
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataTranCompany(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_StoreChangeCommand.SP_SEARCHCOMPANY);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin danh sách lệnh chuyển kho được tính lúc chia hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommand -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        /// <summary>
        /// Tìm kiếm thông tin danh sách lệnh chuyển kho được tính lúc chia hàng
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_StoreChangeCommand.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin danh sách lệnh chuyển kho được tính lúc chia hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommand -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm lệnh chuyển kho
        /// </summary>
        /// <param name="objStoreChangeCommandList"></param>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        public ResultMessage InsertMulti(List<BO.StoreChangeCommand> objStoreChangeCommandList, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (BO.StoreChangeCommand objStoreChangeCommand in objStoreChangeCommandList)
                {
                    string strStoreChangeCommandID = NewStoreChangeCommandID(objIData, objStoreChangeCommand.CreatedStoreID);
                    objStoreChangeCommand.CreatedUser = strUserName;
                    objStoreChangeCommand.StoreChangeCommandID = strStoreChangeCommandID;
                    Insert(objIData, objStoreChangeCommand);
                    DA_StoreChangeCommandDetail objDA_StoreChangeCommandDetail = new DA_StoreChangeCommandDetail();
                    foreach (BO.StoreChangeCommandDetail objStoreChangeCommandDetail in objStoreChangeCommand.StoreChangeCommandDetailList)
                    {
                        objStoreChangeCommandDetail.StoreChangeCommandID = objStoreChangeCommand.StoreChangeCommandID;
                        objStoreChangeCommandDetail.CreatedUser = strUserName;
                        objDA_StoreChangeCommandDetail.Insert(objIData, objStoreChangeCommandDetail);
                    }
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi Thêm lệnh chuyển kho", objEx.ToString());
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa lệnh chuyển kho
        /// </summary>
        /// <param name="objStoreChangeCommandList"></param>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        public ResultMessage DeleteMulti(List<BO.StoreChangeCommand> objStoreChangeCommandList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (BO.StoreChangeCommand objStoreChangeCommand in objStoreChangeCommandList)
                {
                    Delete(objIData, objStoreChangeCommand);
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi xóa lệnh chuyển kho", objEx.ToString());
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Cập nhật ghi chú lệnh chuyển kho
        /// PM_STORECHANGECOMMAND_UPDNOTE
        /// </summary>
        /// <param name="objStoreChangeCommandList"></param>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        public ResultMessage UpdateNote(List<BO.StoreChangeCommand> objStoreChangeCommandList, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (BO.StoreChangeCommand objStoreChangeCommand in objStoreChangeCommandList)
                {
                    objStoreChangeCommand.UpdatedUser = strUserName;
                    objIData.CreateNewStoredProcedure("PM_STORECHANGECOMMAND_UPDNOTE");
                    objIData.AddParameter("@" + BO.StoreChangeCommand.colStoreChangeCommandID, objStoreChangeCommand.StoreChangeCommandID);
                    objIData.AddParameter("@" + BO.StoreChangeCommand.colNote, objStoreChangeCommand.Note);
                    objIData.AddParameter("@" + BO.StoreChangeCommand.colUpdatedUser, objStoreChangeCommand.UpdatedUser);
                    objIData.ExecNonQuery();
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi cập nhật ghi chú lệnh chuyển kho", objEx.ToString());
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        private String NewStoreChangeCommandID(IData objIData, int intDefaultStoreID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_StoreChangeCommand_CreateID");
                objIData.AddParameter("@StoreID", intDefaultStoreID);
                return objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                throw objEx;
            }
        }


        /// <summary>
        /// Tìm kiếm dữ liệu STORECHANGEORDERTYPE theo lệnh chuyển kho
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataStoreChangeOrderType(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_STORECHANGEORDERTYPE_SRHSCC");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm dữ liệu loại yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommand -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm lệnh chuyển kho
        /// </summary>
        /// <param name="objStoreChangeCommandList"></param>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        public ResultMessage InsertMultiAndStoreChangeOrder(List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (BO.StoreChangeCommand objStoreChangeCommand in objStoreChangeCommandList)
                {
                    string strStoreChangeCommandID = NewStoreChangeCommandID(objIData, objStoreChangeCommand.CreatedStoreID);
                    objStoreChangeCommand.CreatedUser = strUserName;
                    objStoreChangeCommand.StoreChangeCommandID = strStoreChangeCommandID;
                    Insert(objIData, objStoreChangeCommand);
                    DA_StoreChangeCommandDetail objDA_StoreChangeCommandDetail = new DA_StoreChangeCommandDetail();
                    foreach (BO.StoreChangeCommandDetail objStoreChangeCommandDetail in objStoreChangeCommand.StoreChangeCommandDetailList)
                    {
                        objStoreChangeCommandDetail.StoreChangeCommandID = objStoreChangeCommand.StoreChangeCommandID;
                        objStoreChangeCommandDetail.CreatedUser = strUserName;
                        objDA_StoreChangeCommandDetail.Insert(objIData, objStoreChangeCommandDetail);
                    }
                }
                objResultMessage = CreateStoreChangeOrder(objIData, objStoreChangeCommandList, objStoreChangeOrder, tblFromStore);
                //objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi Thêm lệnh chuyển kho", objEx.ToString());
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CreateStoreChangeOrder(IData objIData, List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder StoreChangeOrder, DataTable tblFromStore)
        {
            ResultMessage objResultMessage = new ResultMessage();
            try
            {
                DA_StoreChangeOrder objDA_StoreChangeOrder = new DA_StoreChangeOrder();
                foreach (DataRow objFromStore in tblFromStore.Rows)
                {
                    int intFromStoreID = Convert.ToInt32(objFromStore["FromStoreID"]);
                    var tblToStore = objStoreChangeCommandList.FindAll(x => x.FromStoreID == intFromStoreID).GroupBy(x => x.ToStoreID);
                    //DataTable tblToStore = Library.WebCore.Globals.SelectDistinct(tblStoreChangeCommand, "ToStoreID", "FromStoreID = " + intFromStoreID.ToString());
                    if (tblToStore.Count() > 0)
                    {
                        foreach (var objToStore in tblToStore)
                        {
                            int intToStoreID = Convert.ToInt32(objToStore.Key);
                            String strStoreChangeCommandIDList = GetStoreChangeCommandIDList(objStoreChangeCommandList, intFromStoreID, intToStoreID);
                            DataTable dtbStoreChangeOrderDetail = LoadStoreChangeOrderDetailFromCommand(objIData, strStoreChangeCommandIDList);
                            if (dtbStoreChangeOrderDetail != null && dtbStoreChangeOrderDetail.Rows.Count > 0)
                            {

                                StoreChangeOrder.FromStoreID = intFromStoreID;
                                StoreChangeOrder.ToStoreID = intToStoreID;
                                if (dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", "") == null)
                                    StoreChangeOrder.TotalCommandQuantity = 0;
                                else
                                    StoreChangeOrder.TotalCommandQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", ""));
                                if (dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", "") == null)
                                    StoreChangeOrder.TotalQuantity = 0;
                                else
                                    StoreChangeOrder.TotalQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", ""));
                                objDA_StoreChangeOrder.Insert(objIData, StoreChangeOrder);
                                if (!StoreChangeOrder.IsReviewed)
                                {
                                    objDA_StoreChangeOrder.InsertStoreChangeOrder_ReviewList(objIData, StoreChangeOrder.StoreChangeOrderID, StoreChangeOrder.StoreChangeOrderTypeID);
                                }
                                DA_StoreChangeOrderDetail objDA_StoreChangeOrderDetail = new DA_StoreChangeOrderDetail();
                                DA_StoreChangeOrderDetailIMEI objDA_StoreChangeOrderDetailIMEI = new DA_StoreChangeOrderDetailIMEI();
                                foreach (DataRow dtrStoreChangeOrderDetail in dtbStoreChangeOrderDetail.Rows)
                                {
                                    BO.StoreChangeOrderDetail objStoreChangeOrderDetailBO = new BO.StoreChangeOrderDetail();
                                    BO.StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEIBO = new BO.StoreChangeOrderDetailIMEI();
                                    objStoreChangeOrderDetailBO.ProductID = dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim();
                                    objStoreChangeOrderDetailBO.CommandQuantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["CommandQuantity"]);
                                    objStoreChangeOrderDetailBO.Quantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["OrderQuantity"]);
                                    objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                    objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                    objStoreChangeOrderDetailBO.IsManualAdd = Convert.ToBoolean(dtrStoreChangeOrderDetail["IsManualAdd"]);
                                    objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                    objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                    objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                    objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                                    //Lưu db
                                    string strStoreChangeOrderDetailID = objDA_StoreChangeOrderDetail.Insert(objIData, objStoreChangeOrderDetailBO);
                                    if (dtrStoreChangeOrderDetail["IMEI"].ToString().Trim() != string.Empty)
                                    {
                                        objStoreChangeOrderDetailIMEIBO.CreatedStoreID = objStoreChangeOrderDetailBO.CreatedStoreID;
                                        objStoreChangeOrderDetailIMEIBO.CreatedUser = objStoreChangeOrderDetailBO.CreatedUser;
                                        objStoreChangeOrderDetailIMEIBO.IMEI = dtrStoreChangeOrderDetail["IMEI"].ToString().Trim();
                                        objStoreChangeOrderDetailIMEIBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                        objStoreChangeOrderDetailIMEIBO.StoreChangeOrderDetailID = strStoreChangeOrderDetailID;
                                        objDA_StoreChangeOrderDetailIMEI.Insert(objIData, objStoreChangeOrderDetailIMEIBO);
                                    }
                                }
                            }
                        }
                    }
                }
                objIData.CommitTransaction();
                /*
                DataSet dsUserNotify = null;
                List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
                if (dsUserNotify != null && dsUserNotify.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in dsUserNotify.Tables[0].Rows)
                    {
                        Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
                        obj.UserName = item["UserName"].ToString();
                        if (!objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                        {
                            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
                            {
                                bool bolResult = false;
                                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, obj.UserName);
                                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                                    objNotification_UserList.Add(obj);
                            }
                            else
                                objNotification_UserList.Add(obj);
                        }
                    }
                }
                ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
                objNotification.NotificationID = Guid.NewGuid().ToString();
                objNotification.NotificationTypeID = 9;
                objNotification.TaskParameters = StoreChangeOrder.StoreChangeOrderID;
                objNotification.NotificationName = "Yêu cầu chuyển kho";
                objNotification.Content = "Từ kho " + StoreChangeOrder.FromStoreName + " -> " + StoreChangeOrder.ToStoreName + ". Nội dung: " + StoreChangeOrder.Content;
                objNotification.CreatedUser = StoreChangeOrder.CreatedUser;
                objNotification.CreatedDate = DateTime.Now;
                objNotification.Notification_UserList = objNotification_UserList;
                objNotification.StartDate = DateTime.Now;
                ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
                objDA_Notification.Broadcast(objNotification);
                */
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi tạo danh sách loại lệnh chuyển kho được tính lúc chia hàng";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChangeOrder -> CreateStoreChangeOrder", "ERP.Inventory.DA");
            }
            return objResultMessage;
        }

        private String GetStoreChangeCommandIDList(List<BO.StoreChangeCommand> objStoreChangeCommandList, int intFromStoreID, int intToStoreID)
        {
            String strResult = "";
            String strSelectExp = "FromStoreID = " + intFromStoreID.ToString() + " and ToStoreID = " + intToStoreID.ToString();
            List<BO.StoreChangeCommand> lstRow = objStoreChangeCommandList.FindAll(x => x.FromStoreID == intFromStoreID && x.ToStoreID == intToStoreID);
            foreach (BO.StoreChangeCommand objRow in lstRow)
            {
                strResult += "'" + objRow.StoreChangeCommandID.Trim() + "',";
            }
            if (strResult.Length > 0)
                strResult = strResult.Substring(0, strResult.Length - 1);
            return strResult;
        }

        private DataTable LoadStoreChangeOrderDetailFromCommand(IData objIData, String strStoreChangeCommandIDList)
        {

            DataTable dtbResult = null;
            try
            {
                objIData.CreateNewStoredProcedure("PM_STORECHANGEORDER_IDFRCMD");
                objIData.AddParameter("@StoreChangeCommandIDList", strStoreChangeCommandIDList, Library.DataAccess.Globals.DATATYPE.CLOB);
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objExc)
            {
                new SystemError(objIData, "Lỗi nạp danh sách sản phẩm từ lệnh chuyển kho", objExc);
            }
            return dtbResult;
        }


        public ResultMessage InsertMultiAndStoreChangeOrder2(List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore, string strUserName, DataTable tblListIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (BO.StoreChangeCommand objStoreChangeCommand in objStoreChangeCommandList)
                {
                    string strStoreChangeCommandID = NewStoreChangeCommandID(objIData, objStoreChangeCommand.CreatedStoreID);
                    objStoreChangeCommand.CreatedUser = strUserName;
                    objStoreChangeCommand.StoreChangeCommandID = strStoreChangeCommandID;
                    Insert(objIData, objStoreChangeCommand);
                    DA_StoreChangeCommandDetail objDA_StoreChangeCommandDetail = new DA_StoreChangeCommandDetail();
                    foreach (BO.StoreChangeCommandDetail objStoreChangeCommandDetail in objStoreChangeCommand.StoreChangeCommandDetailList)
                    {
                        objStoreChangeCommandDetail.StoreChangeCommandID = objStoreChangeCommand.StoreChangeCommandID;
                        objStoreChangeCommandDetail.CreatedUser = strUserName;
                        objDA_StoreChangeCommandDetail.Insert(objIData, objStoreChangeCommandDetail);
                    }
                }
                objResultMessage = CreateStoreChangeOrder2(objIData, objStoreChangeCommandList, objStoreChangeOrder, tblFromStore, tblListIMEI);

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi Thêm lệnh chuyển kho", objEx.ToString());
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CreateStoreChangeOrder2(IData objIData, List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder StoreChangeOrder, DataTable tblFromStore, DataTable tblListIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            try
            {
                bool CheckIMEI = false;
                if (tblListIMEI != null && tblListIMEI.Rows.Count > 0)
                    CheckIMEI = true;
                DA_StoreChangeOrder objDA_StoreChangeOrder = new DA_StoreChangeOrder();
                foreach (DataRow objFromStore in tblFromStore.Rows)
                {
                    int intFromStoreID = Convert.ToInt32(objFromStore["FromStoreID"]);
                    var tblToStore = objStoreChangeCommandList.FindAll(x => x.FromStoreID == intFromStoreID).GroupBy(x => x.ToStoreID);
                    if (tblToStore.Count() > 0)
                    {
                        foreach (var objToStore in tblToStore)
                        {
                            int intToStoreID = Convert.ToInt32(objToStore.Key);
                            String strStoreChangeCommandIDList = GetStoreChangeCommandIDList(objStoreChangeCommandList, intFromStoreID, intToStoreID);
                            DataTable dtbStoreChangeOrderDetail = LoadStoreChangeOrderDetailFromCommand(objIData, strStoreChangeCommandIDList);
                            if (dtbStoreChangeOrderDetail != null && dtbStoreChangeOrderDetail.Rows.Count > 0)
                            {
                                StoreChangeOrder.FromStoreID = intFromStoreID;
                                StoreChangeOrder.ToStoreID = intToStoreID;
                                if (dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", "") == null)
                                    StoreChangeOrder.TotalCommandQuantity = 0;
                                else
                                    StoreChangeOrder.TotalCommandQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", ""));
                                if (dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", "") == null)
                                    StoreChangeOrder.TotalQuantity = 0;
                                else
                                    StoreChangeOrder.TotalQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", ""));
                                objDA_StoreChangeOrder.Insert(objIData, StoreChangeOrder);
                                if (!StoreChangeOrder.IsReviewed)
                                {
                                    objDA_StoreChangeOrder.InsertStoreChangeOrder_ReviewList(objIData, StoreChangeOrder.StoreChangeOrderID, StoreChangeOrder.StoreChangeOrderTypeID);
                                }
                                DA_StoreChangeOrderDetail objDA_StoreChangeOrderDetail = new DA_StoreChangeOrderDetail();
                                DA_StoreChangeOrderDetailIMEI objDA_StoreChangeOrderDetailIMEI = new DA_StoreChangeOrderDetailIMEI();
                                foreach (DataRow dtrStoreChangeOrderDetail in dtbStoreChangeOrderDetail.Rows)
                                {
                                    BO.StoreChangeOrderDetail objStoreChangeOrderDetailBO = new BO.StoreChangeOrderDetail();
                                    BO.StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEIBO = new BO.StoreChangeOrderDetailIMEI();
                                    objStoreChangeOrderDetailBO.ProductID = dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim();
                                    if (CheckIMEI)
                                    {
                                        var listCheckIMEI = tblListIMEI.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                                                                                && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                                                                                && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                        if (listCheckIMEI != null && listCheckIMEI.Count() > 0)
                                            objStoreChangeOrderDetailBO.Quantity = 1;
                                        else
                                            objStoreChangeOrderDetailBO.Quantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["OrderQuantity"]);

                                    }
                                    else
                                        objStoreChangeOrderDetailBO.Quantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["OrderQuantity"]);
                                    objStoreChangeOrderDetailBO.CommandQuantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["CommandQuantity"]);
                                    objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                    objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                    objStoreChangeOrderDetailBO.IsManualAdd = Convert.ToBoolean(dtrStoreChangeOrderDetail["IsManualAdd"]);
                                    objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                    objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                    objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                    objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                                    string strStoreChangeOrderDetailID = objDA_StoreChangeOrderDetail.Insert(objIData, objStoreChangeOrderDetailBO);
                                    //Insert IMEI
                                    if (CheckIMEI)
                                    {
                                        var listCheckIMEI = tblListIMEI.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                                                                                && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                                                                                && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                        if (listCheckIMEI != null && listCheckIMEI.Count() > 0)
                                        {
                                            DataTable tblIMEI = CreateTableIMEI();
                                            foreach (var item in listCheckIMEI)
                                            {
                                                tblIMEI.Rows.Add
                                                (
                                                    strStoreChangeOrderDetailID,
                                                    item["IMEI"].ToString().Trim(),
                                                    ((DateTime)objStoreChangeOrderDetailBO.OrderDate).ToString("yyyy/MM/dd HH:mm:ss"),
                                                    0,
                                                    objStoreChangeOrderDetailBO.Note,
                                                    objStoreChangeOrderDetailBO.CreatedStoreID,
                                                    intFromStoreID,
                                                    objStoreChangeOrderDetailBO.CreatedUser,
                                                    objStoreChangeOrderDetailBO.StoreChangeOrderID,
                                                    objStoreChangeOrderDetailBO.ProductID
                                                );
                                            }
                                            if (tblIMEI != null && tblIMEI.Rows.Count > 0)
                                            {
                                                string xml = CreateXMLFromDataTable(tblIMEI, "SCODETAILIMEI");
                                                InsertImei(objIData, xml, objStoreChangeOrderDetailBO.StoreChangeOrderID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    objIData.CommitTransaction();
                    /*
                    DataSet dsUserNotify = null;
                    List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
                    if (dsUserNotify != null && dsUserNotify.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow item in dsUserNotify.Tables[0].Rows)
                        {
                            Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
                            obj.UserName = item["UserName"].ToString();
                            if (!objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                            {
                                if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
                                {
                                    bool bolResult = false;
                                    new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, obj.UserName);
                                    if (bolResult && !objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                                        objNotification_UserList.Add(obj);
                                }
                                else
                                    objNotification_UserList.Add(obj);
                            }
                        }
                    }
                    ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
                    objNotification.NotificationID = Guid.NewGuid().ToString();
                    objNotification.NotificationTypeID = 9;
                    objNotification.TaskParameters = StoreChangeOrder.StoreChangeOrderID;
                    objNotification.NotificationName = "Yêu cầu chuyển kho";
                    objNotification.Content = "Từ kho " + StoreChangeOrder.FromStoreName + " -> " + StoreChangeOrder.ToStoreName + ". Nội dung: " + StoreChangeOrder.Content;
                    objNotification.CreatedUser = StoreChangeOrder.CreatedUser;
                    objNotification.CreatedDate = DateTime.Now;
                    objNotification.Notification_UserList = objNotification_UserList;
                    objNotification.StartDate = DateTime.Now;
                    ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
                    objDA_Notification.Broadcast(objNotification);
                    */
                }
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi tạo danh sách loại lệnh chuyển kho được tính lúc chia hàng";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChangeOrder -> CreateStoreChangeOrder", "ERP.Inventory.DA");
            }
            return objResultMessage;
        }


        public ResultMessage CreateStoreChangeOrder3(IData objIData, List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder StoreChangeOrder, DataTable tblFromStore, DataTable tblListIMEI, DataTable tblNote)
        {
            ResultMessage objResultMessage = new ResultMessage();
            try
            {
                bool CheckIMEI = false;
                if (tblListIMEI != null && tblListIMEI.Rows.Count > 0)
                    CheckIMEI = true;
                DA_StoreChangeOrder objDA_StoreChangeOrder = new DA_StoreChangeOrder();
                foreach (DataRow objFromStore in tblFromStore.Rows)
                {
                    int intFromStoreID = Convert.ToInt32(objFromStore["FromStoreID"]);
                    var tblToStore = objStoreChangeCommandList.FindAll(x => x.FromStoreID == intFromStoreID).GroupBy(x => x.ToStoreID);
                    if (tblToStore.Count() > 0)
                    {
                        foreach (var objToStore in tblToStore)
                        {
                            int intToStoreID = Convert.ToInt32(objToStore.Key);
                            String strStoreChangeCommandIDList = GetStoreChangeCommandIDList(objStoreChangeCommandList, intFromStoreID, intToStoreID);
                            DataTable dtbStoreChangeOrderDetail = LoadStoreChangeOrderDetailFromCommand(objIData, strStoreChangeCommandIDList);
                            if (dtbStoreChangeOrderDetail != null && dtbStoreChangeOrderDetail.Rows.Count > 0)
                            {
                                StoreChangeOrder.FromStoreID = intFromStoreID;
                                StoreChangeOrder.ToStoreID = intToStoreID;
                                if (dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", "") == null)
                                    StoreChangeOrder.TotalCommandQuantity = 0;
                                else
                                    StoreChangeOrder.TotalCommandQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", ""));
                                if (dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", "") == null)
                                    StoreChangeOrder.TotalQuantity = 0;
                                else
                                    StoreChangeOrder.TotalQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", ""));
                                objDA_StoreChangeOrder.Insert(objIData, StoreChangeOrder);
                                if (!StoreChangeOrder.IsReviewed)
                                    objDA_StoreChangeOrder.InsertStoreChangeOrder_ReviewList(objIData, StoreChangeOrder.StoreChangeOrderID, StoreChangeOrder.StoreChangeOrderTypeID);
                                DA_StoreChangeOrderDetail objDA_StoreChangeOrderDetail = new DA_StoreChangeOrderDetail();
                                DA_StoreChangeOrderDetailIMEI objDA_StoreChangeOrderDetailIMEI = new DA_StoreChangeOrderDetailIMEI();
                                foreach (DataRow dtrStoreChangeOrderDetail in dtbStoreChangeOrderDetail.Rows)
                                {
                                    BO.StoreChangeOrderDetail objStoreChangeOrderDetailBO = new BO.StoreChangeOrderDetail();
                                    BO.StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEIBO = new BO.StoreChangeOrderDetailIMEI();
                                    objStoreChangeOrderDetailBO.ProductID = dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim();
                                    if (CheckIMEI)
                                    {
                                        var listCheckIMEI = tblListIMEI.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                                                                                && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                                                                                && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                        if (listCheckIMEI != null && listCheckIMEI.Count() > 0)
                                        {
                                            objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                            objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                            objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                            //objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                            objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                            objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                                            string strNote = string.Empty;
                                            var objNoteIMEI = tblNote.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                                                                             && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                                                                             && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                            if (objNoteIMEI != null && objNoteIMEI.Count() > 0)
                                                strNote = objNoteIMEI[0]["NOTE"].ToString().Trim();
                                            //var objNote=tblNote.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                            //                                                    && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                            //                                                    && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                            //if (objNote != null && objNote.Count() > 0)
                                            objStoreChangeOrderDetailBO.Note = strNote;
                                            DataTable tblIMEI = CreateTableIMEI();
                                            DataTable tblSCOD = CreateTableStoreChangeOrderDetail();
                                            bool IsHasNoteError = false;
                                            if (listCheckIMEI.CopyToDataTable().Columns.Contains("NOTEERROR"))
                                                IsHasNoteError = true;
                                            //Lưu SCODT

                                            foreach (var item in listCheckIMEI)
                                            {
                                                tblSCOD.Rows.Add(
                                                        StoreChangeOrder.StoreChangeOrderID,
                                                        ((DateTime)StoreChangeOrder.OrderDate).ToString("yyyy/MM/dd HH:mm:ss"),
                                                        dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim(),
                                                        1, 1, Convert.ToInt32(dtrStoreChangeOrderDetail["IsManualAdd"]),
                                                        //dtrStoreChangeOrderDetail["Note"].ToString(),
                                                        IsHasNoteError == true ? item["NOTEERROR"].ToString().Trim() : objStoreChangeOrderDetailBO.Note,
                                                        intFromStoreID, StoreChangeOrder.CreatedUser
                                                    );
                                            }
                                            if (tblSCOD != null && tblSCOD.Rows.Count > 0)
                                            {
                                                string xml = CreateXMLFromDataTable(tblSCOD, "SCODETAIL");
                                                InsertSCODetail(objIData, xml);
                                            }
                                            //Lấy danh sách SCODID lên
                                            DataTable dtbSCODID = GetDataListSCODID(objIData, StoreChangeOrder.StoreChangeOrderID.Trim(), dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim());
                                            int i = 0;

                                            if (dtbSCODID.Rows.Count == listCheckIMEI.Count && dtbSCODID != null)
                                            {
                                                foreach (var item in listCheckIMEI)
                                                {
                                                    tblIMEI.Rows.Add
                                                        (
                                                            dtbSCODID.Rows[i][0].ToString().Trim(),
                                                            item["IMEI"].ToString().Trim(),
                                                            ((DateTime)objStoreChangeOrderDetailBO.OrderDate).ToString("yyyy/MM/dd HH:mm:ss"),
                                                            0,
                                                            IsHasNoteError == true ? item["NOTEERROR"].ToString().Trim() : objStoreChangeOrderDetailBO.Note,
                                                            objStoreChangeOrderDetailBO.CreatedStoreID,
                                                            intFromStoreID,
                                                            objStoreChangeOrderDetailBO.CreatedUser,
                                                            objStoreChangeOrderDetailBO.StoreChangeOrderID,
                                                            objStoreChangeOrderDetailBO.ProductID
                                                        );
                                                    i++;
                                                }
                                                if (tblIMEI != null && tblIMEI.Rows.Count > 0)
                                                {
                                                    string xml = CreateXMLFromDataTable(tblIMEI, "SCODETAILIMEI");
                                                    InsertImei(objIData, xml, objStoreChangeOrderDetailBO.StoreChangeOrderID);
                                                }
                                            }
                                            else
                                            {
                                                objIData.RollBackTransaction();
                                                string strMessage = "Lỗi tạo danh sách loại lệnh chuyển kho được tính lúc chia hàng";
                                                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, "Lỗi danh sách SCODID");
                                                new SystemError(objIData, strMessage, "Lỗi danh sách SCODID", "DA_StoreChangeOrder -> CreateStoreChangeOrder", "ERP.Inventory.DA");
                                            }
                                        }
                                        else
                                        {
                                            objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                            objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                            objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                            objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                            objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                                            string strNote = string.Empty;
                                            var objNoteIMEI = tblNote.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                                                                             && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                                                                             && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                            if (objNoteIMEI != null && objNoteIMEI.Count() > 0)
                                                strNote = objNoteIMEI[0]["NOTE"].ToString().Trim();
                                            objStoreChangeOrderDetailBO.Note = strNote;
                                            DataTable tblSCOD = CreateTableStoreChangeOrderDetail();
                                            tblSCOD.Rows.Add(
                                                    StoreChangeOrder.StoreChangeOrderID,
                                                    ((DateTime)StoreChangeOrder.OrderDate).ToString("yyyy/MM/dd HH:mm:ss"),
                                                    dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim(),
                                                    1, 1, Convert.ToInt32(dtrStoreChangeOrderDetail["IsManualAdd"]),
                                                    objStoreChangeOrderDetailBO.Note,
                                                    intFromStoreID, StoreChangeOrder.CreatedUser
                                                );
                                            if (tblSCOD != null && tblSCOD.Rows.Count > 0)
                                            {
                                                string xml = CreateXMLFromDataTable(tblSCOD, "SCODETAIL");
                                                InsertSCODetail(objIData, xml);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                        objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                        objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                        objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                        objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                                        string strNote = string.Empty;
                                        var objNoteIMEI = tblNote.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                                                                         && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                                                                         && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                        if (objNoteIMEI != null && objNoteIMEI.Count() > 0)
                                            strNote = objNoteIMEI[0]["NOTE"].ToString().Trim();
                                        objStoreChangeOrderDetailBO.Note = strNote;
                                        DataTable tblSCOD = CreateTableStoreChangeOrderDetail();
                                        tblSCOD.Rows.Add(
                                                StoreChangeOrder.StoreChangeOrderID,
                                                ((DateTime)StoreChangeOrder.OrderDate).ToString("yyyy/MM/dd HH:mm:ss"),
                                                objStoreChangeOrderDetailBO.ProductID.ToString().Trim(),
                                                1, 1, Convert.ToInt32(dtrStoreChangeOrderDetail["IsManualAdd"]),
                                                objStoreChangeOrderDetailBO.Note,
                                                intFromStoreID, StoreChangeOrder.CreatedUser
                                            );
                                        if (tblSCOD != null && tblSCOD.Rows.Count > 0)
                                        {
                                            string xml = CreateXMLFromDataTable(tblSCOD, "SCODETAIL");
                                            InsertSCODetail(objIData, xml);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    /*
                    DataSet dsUserNotify = null;
                    List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
                    if (dsUserNotify != null && dsUserNotify.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow item in dsUserNotify.Tables[0].Rows)
                        {
                            Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
                            obj.UserName = item["UserName"].ToString();
                            if (!objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                            {
                                if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
                                {
                                    bool bolResult = false;
                                    new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, obj.UserName);
                                    if (bolResult && !objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                                        objNotification_UserList.Add(obj);
                                }
                                else
                                    objNotification_UserList.Add(obj);
                            }
                        }
                    }
                    ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
                    objNotification.NotificationID = Guid.NewGuid().ToString();
                    objNotification.NotificationTypeID = 9;
                    objNotification.TaskParameters = StoreChangeOrder.StoreChangeOrderID;
                    objNotification.NotificationName = "Yêu cầu chuyển kho";
                    objNotification.Content = "Từ kho " + StoreChangeOrder.FromStoreName + " -> " + StoreChangeOrder.ToStoreName + ". Nội dung: " + StoreChangeOrder.Content;
                    objNotification.CreatedUser = StoreChangeOrder.CreatedUser;
                    objNotification.CreatedDate = DateTime.Now;
                    objNotification.Notification_UserList = objNotification_UserList;
                    objNotification.StartDate = DateTime.Now;
                    ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
                    objDA_Notification.Broadcast(objNotification);
                    */
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi tạo danh sách loại lệnh chuyển kho được tính lúc chia hàng";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChangeOrder -> CreateStoreChangeOrder", "ERP.Inventory.DA");
            }
            return objResultMessage;
        }

        public ResultMessage InsertMultiAndStoreChangeOrder3(List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore, string strUserName, DataTable tblListIMEI, DataTable tblNote)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (BO.StoreChangeCommand objStoreChangeCommand in objStoreChangeCommandList)
                {
                    string strStoreChangeCommandID = NewStoreChangeCommandID(objIData, objStoreChangeCommand.CreatedStoreID);
                    objStoreChangeCommand.CreatedUser = strUserName;
                    objStoreChangeCommand.StoreChangeCommandID = strStoreChangeCommandID;
                    Insert(objIData, objStoreChangeCommand);
                    DA_StoreChangeCommandDetail objDA_StoreChangeCommandDetail = new DA_StoreChangeCommandDetail();
                    foreach (BO.StoreChangeCommandDetail objStoreChangeCommandDetail in objStoreChangeCommand.StoreChangeCommandDetailList)
                    {
                        objStoreChangeCommandDetail.StoreChangeCommandID = objStoreChangeCommand.StoreChangeCommandID;
                        objStoreChangeCommandDetail.CreatedUser = strUserName;
                        objDA_StoreChangeCommandDetail.Insert(objIData, objStoreChangeCommandDetail);
                    }
                }
                objResultMessage = CreateStoreChangeOrder3(objIData, objStoreChangeCommandList, objStoreChangeOrder, tblFromStore, tblListIMEI, tblNote);

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi Thêm lệnh chuyển kho", objEx.ToString());
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage InsertMultiAndStoreChangeOrder4(List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore, string strUserName, DataTable tblListIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (BO.StoreChangeCommand objStoreChangeCommand in objStoreChangeCommandList)
                {
                    string strStoreChangeCommandID = NewStoreChangeCommandID(objIData, objStoreChangeCommand.CreatedStoreID);
                    objStoreChangeCommand.CreatedUser = strUserName;
                    objStoreChangeCommand.StoreChangeCommandID = strStoreChangeCommandID;
                    Insert(objIData, objStoreChangeCommand);
                    DA_StoreChangeCommandDetail objDA_StoreChangeCommandDetail = new DA_StoreChangeCommandDetail();
                    foreach (BO.StoreChangeCommandDetail objStoreChangeCommandDetail in objStoreChangeCommand.StoreChangeCommandDetailList)
                    {
                        objStoreChangeCommandDetail.StoreChangeCommandID = objStoreChangeCommand.StoreChangeCommandID;
                        objStoreChangeCommandDetail.CreatedUser = strUserName;
                        objDA_StoreChangeCommandDetail.Insert(objIData, objStoreChangeCommandDetail);
                    }
                }
                objResultMessage = CreateStoreChangeOrder4(objIData, objStoreChangeCommandList, objStoreChangeOrder, tblFromStore, tblListIMEI);

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi Thêm lệnh chuyển kho", objEx.ToString());
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CreateStoreChangeOrder4(IData objIData, List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder StoreChangeOrder, DataTable tblFromStore, DataTable tblListIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            try
            {
                bool CheckIMEI = false;
                if (tblListIMEI != null && tblListIMEI.Rows.Count > 0)
                    CheckIMEI = true;
                DA_StoreChangeOrder objDA_StoreChangeOrder = new DA_StoreChangeOrder();
                foreach (DataRow objFromStore in tblFromStore.Rows)
                {
                    int intFromStoreID = Convert.ToInt32(objFromStore["FromStoreID"]);
                    var tblToStore = objStoreChangeCommandList.FindAll(x => x.FromStoreID == intFromStoreID).GroupBy(x => x.ToStoreID);
                    if (tblToStore.Count() > 0)
                    {
                        foreach (var objToStore in tblToStore)
                        {
                            int intToStoreID = Convert.ToInt32(objToStore.Key);
                            String strStoreChangeCommandIDList = GetStoreChangeCommandIDList(objStoreChangeCommandList, intFromStoreID, intToStoreID);
                            DataTable dtbStoreChangeOrderDetail = LoadStoreChangeOrderDetailFromCommand(objIData, strStoreChangeCommandIDList);
                            if (dtbStoreChangeOrderDetail != null && dtbStoreChangeOrderDetail.Rows.Count > 0)
                            {
                                StoreChangeOrder.FromStoreID = intFromStoreID;
                                StoreChangeOrder.ToStoreID = intToStoreID;
                                if (dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", "") == null)
                                    StoreChangeOrder.TotalCommandQuantity = 0;
                                else
                                    StoreChangeOrder.TotalCommandQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", ""));
                                if (dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", "") == null)
                                    StoreChangeOrder.TotalQuantity = 0;
                                else
                                    StoreChangeOrder.TotalQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", ""));
                                objDA_StoreChangeOrder.Insert(objIData, StoreChangeOrder);
                                if (!StoreChangeOrder.IsReviewed)
                                {
                                    objDA_StoreChangeOrder.InsertStoreChangeOrder_ReviewList(objIData, StoreChangeOrder.StoreChangeOrderID, StoreChangeOrder.StoreChangeOrderTypeID);
                                }
                                DA_StoreChangeOrderDetail objDA_StoreChangeOrderDetail = new DA_StoreChangeOrderDetail();
                                DA_StoreChangeOrderDetailIMEI objDA_StoreChangeOrderDetailIMEI = new DA_StoreChangeOrderDetailIMEI();
                                foreach (DataRow dtrStoreChangeOrderDetail in dtbStoreChangeOrderDetail.Rows)
                                {
                                    BO.StoreChangeOrderDetail objStoreChangeOrderDetailBO = new BO.StoreChangeOrderDetail();
                                    BO.StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEIBO = new BO.StoreChangeOrderDetailIMEI();
                                    objStoreChangeOrderDetailBO.ProductID = dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim();
                                    if (CheckIMEI)
                                    {
                                        var listCheckIMEI = tblListIMEI.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                                                                                && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                                                                                && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                        if (listCheckIMEI != null && listCheckIMEI.Count() > 0)
                                        {
                                            objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                            objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                            objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                            objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                            objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                            objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;

                                            DataTable tblIMEI = CreateTableIMEI();
                                            DataTable tblSCOD = CreateTableStoreChangeOrderDetail();
                                            //Lưu SCODT
                                            foreach (var item in listCheckIMEI)
                                            {
                                                tblSCOD.Rows.Add(
                                                        StoreChangeOrder.StoreChangeOrderID,
                                                        ((DateTime)StoreChangeOrder.OrderDate).ToString("yyyy/MM/dd HH:mm:ss"),
                                                        dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim(),
                                                        1, 1, Convert.ToInt32(dtrStoreChangeOrderDetail["IsManualAdd"]),
                                                        dtrStoreChangeOrderDetail["Note"].ToString(),
                                                        intFromStoreID, StoreChangeOrder.CreatedUser
                                                    );
                                            }
                                            if (tblSCOD != null && tblSCOD.Rows.Count > 0)
                                            {
                                                string xml = CreateXMLFromDataTable(tblSCOD, "SCODETAIL");
                                                InsertSCODetail(objIData, xml);
                                            }
                                            //Lấy danh sách SCODID lên
                                            DataTable dtbSCODID = GetDataListSCODID(objIData, StoreChangeOrder.StoreChangeOrderID.Trim(), dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim());
                                            int i = 0;
                                            if (dtbSCODID.Rows.Count == listCheckIMEI.Count && dtbSCODID != null)
                                            {
                                                foreach (var item in listCheckIMEI)
                                                {
                                                    tblIMEI.Rows.Add
                                                        (
                                                            dtbSCODID.Rows[i][0].ToString().Trim(),
                                                            item["IMEI"].ToString().Trim(),
                                                            ((DateTime)objStoreChangeOrderDetailBO.OrderDate).ToString("yyyy/MM/dd HH:mm:ss"),
                                                            0,
                                                            objStoreChangeOrderDetailBO.Note,
                                                            objStoreChangeOrderDetailBO.CreatedStoreID,
                                                            intFromStoreID,
                                                            objStoreChangeOrderDetailBO.CreatedUser,
                                                            objStoreChangeOrderDetailBO.StoreChangeOrderID,
                                                            objStoreChangeOrderDetailBO.ProductID
                                                        );
                                                    i++;
                                                }
                                                if (tblIMEI != null && tblIMEI.Rows.Count > 0)
                                                {
                                                    string xml = CreateXMLFromDataTable(tblIMEI, "SCODETAILIMEI");
                                                    InsertImei(objIData, xml, objStoreChangeOrderDetailBO.StoreChangeOrderID);
                                                }
                                            }
                                            else
                                            {
                                                objIData.RollBackTransaction();
                                                string strMessage = "Lỗi tạo danh sách loại lệnh chuyển kho được tính lúc chia hàng";
                                                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, "Lỗi danh sách SCODID");
                                                new SystemError(objIData, strMessage, "Lỗi danh sách SCODID", "DA_StoreChangeOrder -> CreateStoreChangeOrder", "ERP.Inventory.DA");
                                            }
                                        }
                                        else
                                        {
                                            objStoreChangeOrderDetailBO.CommandQuantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["CommandQuantity"]);
                                            objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                            objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                            objStoreChangeOrderDetailBO.IsManualAdd = Convert.ToBoolean(dtrStoreChangeOrderDetail["IsManualAdd"]);
                                            objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                            // objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                            objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                            objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                            objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                                            objStoreChangeOrderDetailBO.Quantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["OrderQuantity"]);
                                            string strStoreChangeOrderDetailID = objDA_StoreChangeOrderDetail.Insert(objIData, objStoreChangeOrderDetailBO);
                                        }
                                    }
                                    else
                                    {
                                        objStoreChangeOrderDetailBO.CommandQuantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["CommandQuantity"]);
                                        objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                        objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                        objStoreChangeOrderDetailBO.IsManualAdd = Convert.ToBoolean(dtrStoreChangeOrderDetail["IsManualAdd"]);
                                        objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                        objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                        objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                        objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                                        objStoreChangeOrderDetailBO.Quantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["OrderQuantity"]);
                                        string strStoreChangeOrderDetailID = objDA_StoreChangeOrderDetail.Insert(objIData, objStoreChangeOrderDetailBO);
                                    }
                                }
                            }
                        }
                    }
                    objIData.CommitTransaction();
                    /*
                    DataSet dsUserNotify = null;
                    List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
                    if (dsUserNotify != null && dsUserNotify.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow item in dsUserNotify.Tables[0].Rows)
                        {
                            Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
                            obj.UserName = item["UserName"].ToString();
                            if (!objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                            {
                                if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
                                {
                                    bool bolResult = false;
                                    new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, obj.UserName);
                                    if (bolResult && !objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                                        objNotification_UserList.Add(obj);
                                }
                                else
                                    objNotification_UserList.Add(obj);
                            }
                        }
                    }
                    ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
                    objNotification.NotificationID = Guid.NewGuid().ToString();
                    objNotification.NotificationTypeID = 9;
                    objNotification.TaskParameters = StoreChangeOrder.StoreChangeOrderID;
                    objNotification.NotificationName = "Yêu cầu chuyển kho";
                    objNotification.Content = "Từ kho " + StoreChangeOrder.FromStoreName + " -> " + StoreChangeOrder.ToStoreName + ". Nội dung: " + StoreChangeOrder.Content;
                    objNotification.CreatedUser = StoreChangeOrder.CreatedUser;
                    objNotification.CreatedDate = DateTime.Now;
                    objNotification.Notification_UserList = objNotification_UserList;
                    objNotification.StartDate = DateTime.Now;
                    ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
                    objDA_Notification.Broadcast(objNotification);
                    */
                }
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi tạo danh sách loại lệnh chuyển kho được tính lúc chia hàng";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChangeOrder -> CreateStoreChangeOrder", "ERP.Inventory.DA");
            }
            return objResultMessage;
        }

        private void InsertSCODetail(IData objIData, string strXML)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_SCODETAIL_ADD1");
                objIData.AddParameter("@XML", strXML, Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw (ex);
            }
        }

        private void InsertImei(IData objIData, string strXML, string strStoreChangeOrderID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_SCODETAILIMEI_ADD1");
                objIData.AddParameter("@XML", strXML, Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.AddParameter("@StoreChangeOrderID", strStoreChangeOrderID);
                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw (ex);
            }
        }

        private DataTable GetDataListSCODID(IData objIData, string strStoreChangeOrderID, string strProductID)
        {
            DataTable dtbResult = null;
            try
            {
                objIData.CreateNewStoredProcedure("PM_GETSCODID_BYID");
                objIData.AddParameter("@STORECHANGEORDERID", strStoreChangeOrderID);
                objIData.AddParameter("@PRODUCTID", strProductID);
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw (ex);
            }
            return dtbResult;
        }

        public string CreateXMLFromDataTable(DataTable dtb, string tableName)
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(tableName))
            { return string.Empty; }
            var sb = new StringBuilder();
            var xtx = XmlWriter.Create(sb);
            xtx.WriteStartDocument();
            xtx.WriteStartElement("", tableName.ToUpper() + "LIST", "");
            foreach (DataRow dr in dtb.Rows)
            {
                xtx.WriteStartElement("", tableName.ToUpper(), "");
                foreach (DataColumn dc in dtb.Columns)
                { xtx.WriteAttributeString(dc.ColumnName.ToUpper(), dr[dc.ColumnName].ToString()); }
                xtx.WriteEndElement();
            }
            xtx.WriteEndElement(); xtx.Flush(); xtx.Close(); return sb.ToString();
        }

        private DataTable CreateTableIMEI()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("STORECHANGEORDERDETAILID");
            dt.Columns.Add("IMEI");
            dt.Columns.Add("ORDERDATE");
            dt.Columns.Add("ISSTORECHANGE");
            dt.Columns.Add("NOTE");
            dt.Columns.Add("CREATEDSTOREID");
            dt.Columns.Add("FROMSTORE");
            dt.Columns.Add("CREATEDUSER");
            dt.Columns.Add("STORECHANGEORDERID");
            dt.Columns.Add("PRODUCTID");
            return dt;
        }


        private DataTable CreateTableStoreChangeOrderDetail()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("STORECHANGEORDERID");
            dt.Columns.Add("ORDERDATE");
            dt.Columns.Add("PRODUCTID");
            dt.Columns.Add("COMMANDQUANTITY");
            dt.Columns.Add("QUANTITY");
            dt.Columns.Add("ISMANUALADD");
            dt.Columns.Add("NOTE");
            dt.Columns.Add("CREATEDSTOREID");
            dt.Columns.Add("CREATEDUSER");
            return dt;
        }

        public ResultMessage InsertMultiAndStoreChangeOrder5(List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore, string strUserName, DataTable tblNote)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (BO.StoreChangeCommand objStoreChangeCommand in objStoreChangeCommandList)
                {
                    string strStoreChangeCommandID = NewStoreChangeCommandID(objIData, objStoreChangeCommand.CreatedStoreID);
                    objStoreChangeCommand.CreatedUser = strUserName;
                    objStoreChangeCommand.StoreChangeCommandID = strStoreChangeCommandID;
                    Insert(objIData, objStoreChangeCommand);
                    DA_StoreChangeCommandDetail objDA_StoreChangeCommandDetail = new DA_StoreChangeCommandDetail();
                    foreach (BO.StoreChangeCommandDetail objStoreChangeCommandDetail in objStoreChangeCommand.StoreChangeCommandDetailList)
                    {
                        objStoreChangeCommandDetail.StoreChangeCommandID = objStoreChangeCommand.StoreChangeCommandID;
                        objStoreChangeCommandDetail.CreatedUser = strUserName;
                        objDA_StoreChangeCommandDetail.Insert(objIData, objStoreChangeCommandDetail);
                    }
                }
                objResultMessage = CreateStoreChangeOrder5(objIData, objStoreChangeCommandList, objStoreChangeOrder, tblFromStore, tblNote);
                //objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi Thêm lệnh chuyển kho", objEx.ToString());
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private bool IsEnoughInStock(string productid, string imei, int instockStatus, decimal checkQuantity, int checkStoreID)
        {
            bool result = false;
            try
            {
                IData objIData = Data.CreateData();
                objIData.Connect();
                if (string.IsNullOrEmpty(imei))
                {
                    string SP_CHECKQUANTITY = "PM_STOCKREPORT_AUTOCHANGE";
                    objIData.CreateNewStoredProcedure(SP_CHECKQUANTITY);
                    objIData.AddParameter("@PRODUCTIDLIST", productid);
                    objIData.AddParameter("@STOREIDLIST", checkStoreID.ToString());
                    objIData.AddParameter("@ISCHECKREALINPUT", 0);
                    objIData.AddParameter("@InStockStatusIDList", instockStatus.ToString());
                    DataTable dtbResult = objIData.ExecStoreToDataTable();

                    if (dtbResult != null)
                    {
                        var dataRow = dtbResult.AsEnumerable().Where(r =>
                           !Convert.IsDBNull(r["PRODUCTID"]) && Convert.ToString(r["PRODUCTID"]) == productid
                           && !Convert.IsDBNull(r["STOREID"]) && Convert.ToInt32(r["STOREID"]) == checkStoreID
                           && !Convert.IsDBNull(r["INSTOCKSTATUSID"]) && Convert.ToInt32(r["INSTOCKSTATUSID"]) == instockStatus
                           && !Convert.IsDBNull(r["QUANTITY"]) && Convert.ToInt32(r["QUANTITY"]) >= checkQuantity).FirstOrDefault();
                        result = dataRow != null;
                    }
                }
                else
                {
                    DataTable dtbIMEI = new DataTable();
                    dtbIMEI.Columns.Add("IMEI", typeof(string));
                    DataRow row = dtbIMEI.NewRow();
                    row["IMEI"] = imei;
                    dtbIMEI.Rows.Add(row);
                    string xmlIMEI = CreateXMLFromDataTable2(dtbIMEI, "IMEI");
                    objIData.CreateNewStoredProcedure("CHECKIMEI_INSTOCK_1");
                    objIData.AddParameter("@IMEI", xmlIMEI);
                    objIData.AddParameter("@UserName", "administrator");
                    DataTable dtbResult = objIData.ExecStoreToDataTable();
                    if (dtbResult != null)
                    {
                        var dataRow = dtbResult.AsEnumerable().Where(r =>
                           !Convert.IsDBNull(r["PRODUCTID"]) && Convert.ToString(r["PRODUCTID"]) == productid
                           && !Convert.IsDBNull(r["IMEI"]) && Convert.ToString(r["IMEI"]) == imei
                           && !Convert.IsDBNull(r["STOREID"]) && Convert.ToInt32(r["STOREID"]) == checkStoreID
                           && !Convert.IsDBNull(r["ISORDER"]) && Convert.ToInt32(r["ISORDER"]) == 0
                           && !Convert.IsDBNull(r["INSTOCKSTATUSID"]) && Convert.ToInt32(r["INSTOCKSTATUSID"]) == instockStatus).FirstOrDefault();
                        result = dataRow != null;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }
        public string CreateXMLFromDataTable2(DataTable dtb, string tableName)
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(tableName))
            {
                return string.Empty;
            }
            var sb = new StringBuilder();
            var xtx = XmlWriter.Create(sb);
            xtx.WriteStartDocument();
            xtx.WriteStartElement("", tableName.ToUpper() + "LIST", "");
            foreach (DataRow dr in dtb.Rows)
            {
                xtx.WriteStartElement("", tableName.ToUpper(), "");
                foreach (DataColumn dc in dtb.Columns)
                {
                    xtx.WriteAttributeString(dc.ColumnName.ToUpper(), dr[dc.ColumnName].ToString());
                }
                xtx.WriteEndElement();
            }
            xtx.WriteEndElement();
            xtx.Flush();
            xtx.Close();
            return sb.ToString();
        }

        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        private class StoreChangeRejectedItem
        {
            public StoreChangeRejectedItem(string productid, string imei, int productStatusID, int fromStoreID, int toStoreID)
            {
                ProductID = productid;
                IMEI = imei;
                ProductStatusID = productStatusID;
                FromStoreID = fromStoreID;
                ToStoreID = toStoreID;
            }
            public string ProductID { get; set; }
            public string IMEI { get; set; }
            public int ProductStatusID { get; set; }
            public int FromStoreID { get; set; }
            public int ToStoreID { get; set; }
        }
        public ResultMessage InsertAutoStoreChange(List<BO.AutoStoreChangeContext> listAutoStoreChangeContext, int createdStoreID, bool isAutoCreateOrders, ref List<StoreChangeOrderItemFailure> failureList, string username = "administrator")
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            failureList = new List<StoreChangeOrderItemFailure>();
            DA_StoreChangeOrder objStoreChangeOrderDbContext = new DA_StoreChangeOrder();
            DA_StoreChangeCommandDetail objStoreChangeCommandDetailDbContext = new DA_StoreChangeCommandDetail();
            DA_StoreChangeOrderDetail objStoreChangeOrderDetailDbContext = new DA_StoreChangeOrderDetail();
            DA_StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEIDbContext = new DA_StoreChangeOrderDetailIMEI();

            objStoreChangeOrderDbContext.objLogObject = objLogObject;
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                foreach (BO.AutoStoreChangeContext objStoreChangeContext in listAutoStoreChangeContext)
                {
                    if (objStoreChangeContext.Detail == null || !objStoreChangeContext.Detail.Any() || objStoreChangeContext.Detail.All(d => !d.IsValidConsignmentType)
                        || !objStoreChangeContext.Detail.Any(d => d.IsSelected))
                        continue;

                    List<StoreChangeRejectedItem> listTempFailed = new List<StoreChangeRejectedItem>();
                    #region StoreChangeCommand & StoreChangeCommandDetail
                    //Tạo command
                  //  string strStoreChangeCommandID = NewStoreChangeCommandID(objIData, createdStoreID);
                    string strStoreChangeCommandID = objStoreChangeContext.StoreChangeCommandID; // huent66 ID truyen từ UI

                    BO.StoreChangeCommand objStoreChangeCommand = new BO.StoreChangeCommand()
                    {
                        Content = objStoreChangeContext.Content,
                        CreatedStoreID = createdStoreID,
                        FromStoreID = objStoreChangeContext.FromStore,
                        ToStoreID = objStoreChangeContext.ToStore,
                        IsUrgent = objStoreChangeContext.IsUrgent,
                        StoreChangeCommandID = strStoreChangeCommandID,
                        StoreChangeCommandTypeID = objStoreChangeContext.StoreChangeCommandType,
                        CreatedUser = username,
                    };
                    Insert(objIData, objStoreChangeCommand);
                    //Command detail
                    foreach (BO.AutoStoreChangeContextDetail objContextDetail in objStoreChangeContext.Detail.Where(d => d.IsValidConsignmentType))
                    {
                        if (!IsEnoughInStock(objContextDetail.ProductID, objContextDetail.IMEI, objStoreChangeContext.ProductState, objContextDetail.Quantity, objStoreChangeCommand.FromStoreID))
                        {
                            failureList.Add(new StoreChangeOrderItemFailure()
                            {
                                FromStoreID = objStoreChangeCommand.FromStoreID,
                                IMEI = objContextDetail.IMEI,
                                InStockStatusID = objStoreChangeContext.ProductState,
                                ProductID = objContextDetail.ProductID,
                                ToStoreID = objStoreChangeCommand.ToStoreID,
                                TransferQuantity = objContextDetail.Quantity,
                                Note = "Không còn tồn trong quá trình chuyển kho!"
                            });
                            listTempFailed.Add(new StoreChangeRejectedItem(objContextDetail.ProductID, objContextDetail.IMEI, objStoreChangeContext.ProductState, objStoreChangeCommand.FromStoreID, objStoreChangeCommand.ToStoreID));
                            continue;
                        }
                        objStoreChangeCommandDetailDbContext.Insert(objIData, new BO.StoreChangeCommandDetail()
                        {
                            CreatedStoreID = createdStoreID,
                            ProductID = objContextDetail.ProductID,
                            Quantity = objContextDetail.Quantity,
                            StoreChangeCommandID = strStoreChangeCommandID,
                            CreatedUser = username,
                            StoreChangeQuantity = 0,
                        });
                    }
                    #endregion

                    #region StoreChangeOrder And StoreChangeOrderDetail
                    //Tạo StoreChangeOrder
                    BO.StoreChangeOrder objStoreChangeOrder = new BO.StoreChangeOrder()
                    {
                        Content = objStoreChangeContext.Content,
                        CreatedUser = username,
                        IsUrgent = objStoreChangeContext.IsUrgent,
                        InStockStatusID = objStoreChangeContext.ProductState,
                        FromStoreID = objStoreChangeContext.FromStore,
                        ToStoreID = objStoreChangeContext.ToStore,
                        ExpiryDate = objStoreChangeContext.ExpiredDate,
                        CreatedStoreID = createdStoreID,
                        TransportTypeID = objStoreChangeContext.TransportTypeID,
                        TransportCompanyID = objStoreChangeContext.TransportCompanyID,
                        TransportServicesID = objStoreChangeContext.TransportServicesID,
                        OrderDate = DateTime.Now,
                        StoreChangeStatus = 0,
                        TotalCommandQuantity = objStoreChangeContext.Detail.Where(cmd => cmd.IsValidConsignmentType).Sum(cmd => cmd.Quantity),
                        IsReviewed = objStoreChangeContext.IsReviewed,
                        StoreChangeTypeID = objStoreChangeContext.StoreChangeTypeID,
                        StoreChangeOrderTypeID = objStoreChangeContext.StoreChangeOrderTypeID,
                        TotalQuantity = objStoreChangeContext.Detail.Where(d => d.IsValidConsignmentType).Sum(d => d.Quantity),
                        IsNew = true,
                        StoreChangeCommandID = objStoreChangeContext.StoreChangeCommandID,
                        ReviewedUser = objStoreChangeContext.IsReviewed ? username : string.Empty,
                    };
                    objStoreChangeOrderDbContext.Insert(objIData, objStoreChangeOrder);
                    if (!objStoreChangeOrder.IsReviewed)
                        objStoreChangeOrderDbContext.InsertStoreChangeOrder_ReviewList(objIData, objStoreChangeOrder.StoreChangeOrderID, objStoreChangeOrder.StoreChangeOrderTypeID);

                    List<BO.StoreChangeOrderDetail> listStoreChangeOrderDetail = new List<BO.StoreChangeOrderDetail>();
                    foreach (BO.AutoStoreChangeContextDetail objContextDetail in objStoreChangeContext.Detail.Where(d => d.IsValidConsignmentType && d.IsSelected))
                    {
                        if (listTempFailed.Any(item => item.ProductID == objContextDetail.ProductID
                        && item.IMEI == objContextDetail.IMEI
                        && objStoreChangeContext.ProductState == item.ProductStatusID
                        && objStoreChangeCommand.FromStoreID == item.FromStoreID
                        && objStoreChangeCommand.ToStoreID == item.ToStoreID))
                            continue;

                        string strStoreChangeOrderDetailID = objStoreChangeOrderDetailDbContext.Insert(objIData, new BO.StoreChangeOrderDetail()
                        {
                            CreatedStoreID = createdStoreID,
                            ProductID = objContextDetail.ProductID,
                            Quantity = objContextDetail.Quantity,
                            CreatedUser = username,
                            StoreChangeQuantity = 0,
                            CommandQuantity = objContextDetail.Quantity,
                            IsManualAdd = objContextDetail.IsManualAdd,
                            Note = objContextDetail.Note,
                            StoreChangeOrderID = objStoreChangeOrder.StoreChangeOrderID,
                            OrderDate = objStoreChangeOrder.OrderDate,
                            FromStoreID = objStoreChangeContext.FromStore,
                            ToStoreID = objStoreChangeContext.ToStore,
                        });
                        if (string.IsNullOrEmpty(strStoreChangeOrderDetailID))
                            continue;

                        if (!string.IsNullOrEmpty(objContextDetail.IMEI))
                        {
                            objStoreChangeOrderDetailIMEIDbContext.Insert(objIData, new BO.StoreChangeOrderDetailIMEI()
                            {
                                StoreChangeOrderDetailID = strStoreChangeOrderDetailID,
                                CreatedStoreID = createdStoreID,
                                IMEI = objContextDetail.IMEI,
                                CreatedUser = username,
                                Note = objContextDetail.Note,
                                OrderDate = DateTime.Now,
                                CreatedDate = DateTime.Now
                            }, true, objStoreChangeOrder.StoreChangeOrderID);
                        }
                    }
                    #endregion
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi tạo yêu cầu chuyển kho", objEx);
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public ResultMessage CreateStoreChangeOrder5(IData objIData, List<BO.StoreChangeCommand> objStoreChangeCommandList, BO.StoreChangeOrder StoreChangeOrder, DataTable tblFromStore, DataTable tblNote)
        {
            ResultMessage objResultMessage = new ResultMessage();
            try
            {
                DA_StoreChangeOrder objDA_StoreChangeOrder = new DA_StoreChangeOrder();
                foreach (DataRow objFromStore in tblFromStore.Rows)
                {
                    int intFromStoreID = Convert.ToInt32(objFromStore["FromStoreID"]);
                    var tblToStore = objStoreChangeCommandList.FindAll(x => x.FromStoreID == intFromStoreID).GroupBy(x => x.ToStoreID);
                    //DataTable tblToStore = Library.WebCore.Globals.SelectDistinct(tblStoreChangeCommand, "ToStoreID", "FromStoreID = " + intFromStoreID.ToString());
                    if (tblToStore.Count() > 0)
                    {
                        foreach (var objToStore in tblToStore)
                        {
                            int intToStoreID = Convert.ToInt32(objToStore.Key);
                            String strStoreChangeCommandIDList = GetStoreChangeCommandIDList(objStoreChangeCommandList, intFromStoreID, intToStoreID);
                            DataTable dtbStoreChangeOrderDetail = LoadStoreChangeOrderDetailFromCommand(objIData, strStoreChangeCommandIDList);
                            if (dtbStoreChangeOrderDetail != null && dtbStoreChangeOrderDetail.Rows.Count > 0)
                            {

                                StoreChangeOrder.FromStoreID = intFromStoreID;
                                StoreChangeOrder.ToStoreID = intToStoreID;
                                if (dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", "") == null)
                                    StoreChangeOrder.TotalCommandQuantity = 0;
                                else
                                    StoreChangeOrder.TotalCommandQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", ""));
                                if (dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", "") == null)
                                    StoreChangeOrder.TotalQuantity = 0;
                                else
                                    StoreChangeOrder.TotalQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", ""));
                                objDA_StoreChangeOrder.Insert(objIData, StoreChangeOrder);
                                if (!StoreChangeOrder.IsReviewed)
                                {
                                    objDA_StoreChangeOrder.InsertStoreChangeOrder_ReviewList(objIData, StoreChangeOrder.StoreChangeOrderID, StoreChangeOrder.StoreChangeOrderTypeID);
                                }
                                DA_StoreChangeOrderDetail objDA_StoreChangeOrderDetail = new DA_StoreChangeOrderDetail();
                                DA_StoreChangeOrderDetailIMEI objDA_StoreChangeOrderDetailIMEI = new DA_StoreChangeOrderDetailIMEI();
                                foreach (DataRow dtrStoreChangeOrderDetail in dtbStoreChangeOrderDetail.Rows)
                                {
                                    BO.StoreChangeOrderDetail objStoreChangeOrderDetailBO = new BO.StoreChangeOrderDetail();
                                    BO.StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEIBO = new BO.StoreChangeOrderDetailIMEI();
                                    objStoreChangeOrderDetailBO.ProductID = dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim();
                                    objStoreChangeOrderDetailBO.CommandQuantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["CommandQuantity"]);
                                    objStoreChangeOrderDetailBO.Quantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["OrderQuantity"]);
                                    objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                    objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                    objStoreChangeOrderDetailBO.IsManualAdd = Convert.ToBoolean(dtrStoreChangeOrderDetail["IsManualAdd"]);
                                    objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                    //objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                    var objNote = tblNote.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                                                                                  && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                                                                                  && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                    if (objNote != null && objNote.Count() > 0)
                                        objStoreChangeOrderDetailBO.Note = objNote[0]["NOTE"].ToString().Trim();
                                    objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                    objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                                    //Lưu db
                                    string strStoreChangeOrderDetailID = objDA_StoreChangeOrderDetail.Insert(objIData, objStoreChangeOrderDetailBO);
                                    if (dtrStoreChangeOrderDetail["IMEI"].ToString().Trim() != string.Empty)
                                    {
                                        objStoreChangeOrderDetailIMEIBO.CreatedStoreID = objStoreChangeOrderDetailBO.CreatedStoreID;
                                        objStoreChangeOrderDetailIMEIBO.CreatedUser = objStoreChangeOrderDetailBO.CreatedUser;
                                        objStoreChangeOrderDetailIMEIBO.IMEI = dtrStoreChangeOrderDetail["IMEI"].ToString().Trim();
                                        //objStoreChangeOrderDetailIMEIBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                        var objNoteIMEI = tblNote.AsEnumerable().Where(p => p["FROMSTOREID"].ToString() == intFromStoreID.ToString()
                                                                                                  && p["TOSTOREID"].ToString() == intToStoreID.ToString()
                                                                                                  && p["PRODUCTID"].ToString().Trim() == objStoreChangeOrderDetailBO.ProductID.Trim()).ToList();
                                        if (objNoteIMEI != null && objNoteIMEI.Count() > 0)
                                            objStoreChangeOrderDetailIMEIBO.Note = objNoteIMEI[0]["NOTE"].ToString().Trim();
                                        objStoreChangeOrderDetailIMEIBO.StoreChangeOrderDetailID = strStoreChangeOrderDetailID;
                                        objDA_StoreChangeOrderDetailIMEI.Insert(objIData, objStoreChangeOrderDetailIMEIBO);
                                    }
                                }
                            }
                        }
                    }
                }
                objIData.CommitTransaction();
                //DataSet dsUserNotify = null;
                //List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
                //if (dsUserNotify != null && dsUserNotify.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow item in dsUserNotify.Tables[0].Rows)
                //    {
                //        Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
                //        obj.UserName = item["UserName"].ToString();
                //        if (!objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                //        {
                //            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
                //            {
                //                bool bolResult = false;
                //                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, obj.UserName);
                //                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                //                    objNotification_UserList.Add(obj);
                //            }
                //            else
                //                objNotification_UserList.Add(obj);
                //        }
                //    }
                //}
                //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
                //objNotification.NotificationID = Guid.NewGuid().ToString();
                //objNotification.NotificationTypeID = 9;
                //objNotification.TaskParameters = StoreChangeOrder.StoreChangeOrderID;
                //objNotification.NotificationName = "Yêu cầu chuyển kho";
                //objNotification.Content = "Từ kho " + StoreChangeOrder.FromStoreName + " -> " + StoreChangeOrder.ToStoreName + ". Nội dung: " + StoreChangeOrder.Content;
                //objNotification.CreatedUser = StoreChangeOrder.CreatedUser;
                //objNotification.CreatedDate = DateTime.Now;
                //objNotification.Notification_UserList = objNotification_UserList;
                //objNotification.StartDate = DateTime.Now;
                //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
                //objDA_Notification.Broadcast(objNotification);
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi tạo danh sách loại lệnh chuyển kho được tính lúc chia hàng";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChangeOrder -> CreateStoreChangeOrder", "ERP.Inventory.DA");
            }
            return objResultMessage;
        }
        #endregion
        public ResultMessage GetStoreChangeCommandNewID(ref string strStoreChangeCommandID, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_STORECHANGECOMMAND_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strStoreChangeCommandID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi lấy mã yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommand -> GetStoreChangeCommandNewID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
    }
}
