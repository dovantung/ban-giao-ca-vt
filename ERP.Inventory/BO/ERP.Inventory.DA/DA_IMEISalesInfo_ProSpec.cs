
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using IMEISalesInfo_ProSpec = ERP.Inventory.BO.IMEISalesInfo_ProSpec;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Quang Tú 
	/// Created date 	: 17/06/2013 
	/// Thông tin về thuộc tính sản phẩm
	/// </summary>	
	public partial class DA_IMEISalesInfo_ProSpec
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="objIMEISalesInfo_ProSpec">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec, string strIMEI, string strProductID, int intProductSpecID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colIMEI, strIMEI);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductID, strProductID);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductSpecID, intProductSpecID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					if (objIMEISalesInfo_ProSpec == null) 
 						objIMEISalesInfo_ProSpec = new IMEISalesInfo_ProSpec();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colProductID])) objIMEISalesInfo_ProSpec.ProductID = Convert.ToString(reader[IMEISalesInfo_ProSpec.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colIMEI])) objIMEISalesInfo_ProSpec.IMEI = Convert.ToString(reader[IMEISalesInfo_ProSpec.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colProductSpecID])) objIMEISalesInfo_ProSpec.ProductSpecID = Convert.ToInt32(reader[IMEISalesInfo_ProSpec.colProductSpecID]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colProductSpecStatusID])) objIMEISalesInfo_ProSpec.ProductSpecStatusID = Convert.ToInt32(reader[IMEISalesInfo_ProSpec.colProductSpecStatusID]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colNote])) objIMEISalesInfo_ProSpec.Note = Convert.ToString(reader[IMEISalesInfo_ProSpec.colNote]).Trim();
 					objIMEISalesInfo_ProSpec.IsExist = true;
 				}
 				else
 				{
 					objIMEISalesInfo_ProSpec.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông tin về thuộc tính sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ProSpec -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="objIMEISalesInfo_ProSpec">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objIMEISalesInfo_ProSpec);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin thông tin về thuộc tính sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ProSpec -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEISalesInfo_ProSpec">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductID, objIMEISalesInfo_ProSpec.ProductID);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colIMEI, objIMEISalesInfo_ProSpec.IMEI);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductSpecID, objIMEISalesInfo_ProSpec.ProductSpecID);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductSpecStatusID, objIMEISalesInfo_ProSpec.ProductSpecStatusID);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colNote, objIMEISalesInfo_ProSpec.Note);
                objIData.AddParameter("@UpdatedUser", objIMEISalesInfo_ProSpec.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="objIMEISalesInfo_ProSpec">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objIMEISalesInfo_ProSpec);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin thông tin về thuộc tính sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ProSpec -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEISalesInfo_ProSpec">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductID, objIMEISalesInfo_ProSpec.ProductID);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colIMEI, objIMEISalesInfo_ProSpec.IMEI);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductSpecID, objIMEISalesInfo_ProSpec.ProductSpecID);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductSpecStatusID, objIMEISalesInfo_ProSpec.ProductSpecStatusID);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colNote, objIMEISalesInfo_ProSpec.Note);
                objIData.AddParameter("@UpdatedUser", objIMEISalesInfo_ProSpec.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="objIMEISalesInfo_ProSpec">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objIMEISalesInfo_ProSpec);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin thông tin về thuộc tính sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ProSpec -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEISalesInfo_ProSpec">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductID, objIMEISalesInfo_ProSpec.ProductID);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colIMEI, objIMEISalesInfo_ProSpec.IMEI);
				objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductSpecID, objIMEISalesInfo_ProSpec.ProductSpecID);
                objIData.AddParameter("@UpdatedUser", objIMEISalesInfo_ProSpec.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
        /// Xóa thông tin thông tin về thuộc tính sản phẩm theo sp va IMEI
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objIMEISalesInfo_ProSpec">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, string strProductID, string strIMEI, string strUpdateUser)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_IMEISALES_PROSPEC_DELBYPROD");
                objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colProductID, strProductID);
                objIData.AddParameter("@" + IMEISalesInfo_ProSpec.colIMEI, strIMEI);
                objIData.AddParameter("@UpdatedUser", strUpdateUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
		#endregion

		
		#region Constructor

		public DA_IMEISalesInfo_ProSpec()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_IMEISALESINFO_PROSPEC_ADD";
		public const String SP_UPDATE = "PM_IMEISALESINFO_PROSPEC_UPD";
		public const String SP_DELETE = "PM_IMEISALESINFO_PROSPEC_DEL";
		public const String SP_SELECT = "PM_IMEISALESINFO_PROSPEC_SEL";
		public const String SP_SEARCH = "PM_IMEISALESINFO_PROSPEC_SRH";
		public const String SP_UPDATEINDEX = "PM_IMEISALESINFO_PROSPEC_UPDINDEX";
		#endregion
		
	}
}
