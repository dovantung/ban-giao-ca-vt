
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using BorrowProductDT_Accessory = ERP.Inventory.BO.BorrowProduct.BorrowProductDT_Accessory;
#endregion
namespace ERP.Inventory.DA.BorrowProduct
{
    /// <summary>
	/// Created by 		: LÊ VĂN ĐÔNG 
	/// Created date 	: 06/15/2017 
	/// Phụ kiện mượn kèm
	/// </summary>	
	public partial class DA_BorrowProductDT_Accessory
	{	
	
		
		#region Methods			
		

		/// <summary>
		/// Nạp thông tin phụ kiện mượn kèm
		/// </summary>
		/// <param name="objBorrowProductDT_Accessory">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref BorrowProductDT_Accessory objBorrowProductDT_Accessory, string strBorrowProductDTID, int intMACHINEAccessoryID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colBorrowProductDTID, strBorrowProductDTID);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colMachineAccessoryID, intMACHINEAccessoryID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objBorrowProductDT_Accessory = new BorrowProductDT_Accessory();
 					if (!Convert.IsDBNull(reader[BorrowProductDT_Accessory.colBorrowProductDTID])) objBorrowProductDT_Accessory.BorrowProductDTID = Convert.ToString(reader[BorrowProductDT_Accessory.colBorrowProductDTID]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProductDT_Accessory.colMachineAccessoryID])) objBorrowProductDT_Accessory.MachineAccessoryID = Convert.ToInt32(reader[BorrowProductDT_Accessory.colMachineAccessoryID]);
 					if (!Convert.IsDBNull(reader[BorrowProductDT_Accessory.colNote])) objBorrowProductDT_Accessory.Note = Convert.ToString(reader[BorrowProductDT_Accessory.colNote]).Trim();
                    objBorrowProductDT_Accessory.Status = -1;

                 }
 				else
 				{
 					objBorrowProductDT_Accessory = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin phụ kiện mượn kèm", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT_Accessory -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin phụ kiện mượn kèm
		/// </summary>
		/// <param name="objBorrowProductDT_Accessory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(BorrowProductDT_Accessory objBorrowProductDT_Accessory)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objBorrowProductDT_Accessory);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin phụ kiện mượn kèm", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT_Accessory -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin phụ kiện mượn kèm
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProductDT_Accessory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, BorrowProductDT_Accessory objBorrowProductDT_Accessory)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colBorrowProductDTID, objBorrowProductDT_Accessory.BorrowProductDTID);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colMachineAccessoryID, objBorrowProductDT_Accessory.MachineAccessoryID);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colNote, objBorrowProductDT_Accessory.Note);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin phụ kiện mượn kèm
		/// </summary>
		/// <param name="objBorrowProductDT_Accessory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(BorrowProductDT_Accessory objBorrowProductDT_Accessory)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objBorrowProductDT_Accessory);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin phụ kiện mượn kèm", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT_Accessory -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin phụ kiện mượn kèm
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProductDT_Accessory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, BorrowProductDT_Accessory objBorrowProductDT_Accessory)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colBorrowProductDTID, objBorrowProductDT_Accessory.BorrowProductDTID);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colMachineAccessoryID, objBorrowProductDT_Accessory.MachineAccessoryID);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colNote, objBorrowProductDT_Accessory.Note);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin phụ kiện mượn kèm
		/// </summary>
		/// <param name="objBorrowProductDT_Accessory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(BorrowProductDT_Accessory objBorrowProductDT_Accessory)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objBorrowProductDT_Accessory);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin phụ kiện mượn kèm", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT_Accessory -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin phụ kiện mượn kèm
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProductDT_Accessory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, BorrowProductDT_Accessory objBorrowProductDT_Accessory)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colBorrowProductDTID, objBorrowProductDT_Accessory.BorrowProductDTID);
				objIData.AddParameter("@" + BorrowProductDT_Accessory.colMachineAccessoryID, objBorrowProductDT_Accessory.MachineAccessoryID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
		/// Xóa tất cả thông tin phụ kiện mượn kèm theo san phẩm mượn
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProductDT_Accessory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void DeleteAll(IData objIData, String strBorrowProductDTID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_BORROWPRODUCTDT_ARY_DELALL");
                objIData.AddParameter("@" + BorrowProductDT_Accessory.colBorrowProductDTID, strBorrowProductDTID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Tìm kiếm thông tin yêu cầu xuất đổi - mức duyệt
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_BorrowProduct_RVL.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin phụ kiện mượn kèm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT_Accessory -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Tìm kiếm thông tin
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadDataByBorrowProduct(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_BORROWPRODUCTDT_ARY_LOAD");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin phụ kiện mượn kèm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT_Accessory -> LoadDataByBorrowProduct", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion


        #region Constructor

        public DA_BorrowProductDT_Accessory()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_BORROWPRODUCTDT_ARY_ADD";
		public const String SP_UPDATE = "PM_BORROWPRODUCTDT_ARY_UPD";
		public const String SP_DELETE = "PM_BORROWPRODUCTDT_ARY_DEL";
		public const String SP_SELECT = "PM_BORROWPRODUCTDT_ARY_SEL";
		public const String SP_SEARCH = "PM_BORROWPRODUCTDT_ARY_SRH";
        public const String SP_LOAD = "PM_BORROWPRODUCTDT_ARY_LOAD";
        #endregion

    }
}
