﻿
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using BOBorrowProduct_RVL = ERP.Inventory.BO.BorrowProduct.BorrowProduct_RVL;
using ERP.Inventory.BO.BorrowProduct;
#endregion
namespace ERP.Inventory.DA.BorrowProduct
{
    /// <summary>
    /// Created by 		: Luong Tuan Anh 
    /// Created date 	: 06-Dec-16 
    /// Yêu cầu xuất đổi - Mức duyệt
    /// </summary>	
    public partial class DA_BorrowProduct_RVL
    {
        
		#region Methods			
		

		/// <summary>
		/// Nạp thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objBORRowProduct_RVL">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref BorrowProduct_RVL objBORRowProduct_RVL, string strBORRowProductRVLID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + BorrowProduct_RVL.colBORRowProductRVLID, strBORRowProductRVLID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objBORRowProduct_RVL = new BorrowProduct_RVL();
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colBORRowProductRVLID])) objBORRowProduct_RVL.BorrowProductRVLID = Convert.ToString(reader[BorrowProduct_RVL.colBORRowProductRVLID]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colBORRowProductID])) objBORRowProduct_RVL.BorrowProductID = Convert.ToString(reader[BorrowProduct_RVL.colBORRowProductID]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colReviewLevelID])) objBORRowProduct_RVL.ReviewLevelID = Convert.ToInt32(reader[BorrowProduct_RVL.colReviewLevelID]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colReviewType])) objBORRowProduct_RVL.ReviewType = Convert.ToInt32(reader[BorrowProduct_RVL.colReviewType]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colUserName])) objBORRowProduct_RVL.UserName = Convert.ToString(reader[BorrowProduct_RVL.colUserName]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colReviewedStatus])) objBORRowProduct_RVL.ReviewedStatus = Convert.ToInt32(reader[BorrowProduct_RVL.colReviewedStatus]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colReviewedDate])) objBORRowProduct_RVL.ReviewedDate = Convert.ToDateTime(reader[BorrowProduct_RVL.colReviewedDate]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colNote])) objBORRowProduct_RVL.Note = Convert.ToString(reader[BorrowProduct_RVL.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colSEQUENCEReview])) objBORRowProduct_RVL.SequenceReview = Convert.ToInt32(reader[BorrowProduct_RVL.colSEQUENCEReview]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colCreatedUser])) objBORRowProduct_RVL.CreatedUser = Convert.ToString(reader[BorrowProduct_RVL.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colCreatedDate])) objBORRowProduct_RVL.CreatedDate = Convert.ToDateTime(reader[BorrowProduct_RVL.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colIsDeleted])) objBORRowProduct_RVL.IsDeleted = Convert.ToBoolean(reader[BorrowProduct_RVL.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colDeletedUser])) objBORRowProduct_RVL.DeletedUser = Convert.ToString(reader[BorrowProduct_RVL.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colDeletedDate])) objBORRowProduct_RVL.DeletedDate = Convert.ToDateTime(reader[BorrowProduct_RVL.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[BorrowProduct_RVL.colIsReviewedCheckInByFinger])) objBORRowProduct_RVL.IsReviewedCheckInByFinger = Convert.ToBoolean(reader[BorrowProduct_RVL.colIsReviewedCheckInByFinger]);
 				}
 				else
 				{
 					objBORRowProduct_RVL = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin yêu cầu xuất đổi - mức duyệt", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BORRowProduct_RVL -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objBORRowProduct_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(BorrowProduct_RVL objBORRowProduct_RVL)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objBORRowProduct_RVL);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin yêu cầu xuất đổi - mức duyệt", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BORRowProduct_RVL -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBORRowProduct_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public bool Insert(IData objIData, BorrowProduct_RVL objBORRowProduct_RVL)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + BorrowProduct_RVL.colBORRowProductID, objBORRowProduct_RVL.BorrowProductID);
				objIData.AddParameter("@" + BorrowProduct_RVL.colReviewLevelID, objBORRowProduct_RVL.ReviewLevelID);
				objIData.AddParameter("@" + BorrowProduct_RVL.colReviewType, objBORRowProduct_RVL.ReviewType);
				objIData.AddParameter("@" + BorrowProduct_RVL.colUserName, objBORRowProduct_RVL.UserName);
				//objIData.AddParameter("@" + BorrowProduct_RVL.colReviewedStatus, objBORRowProduct_RVL.ReviewedStatus);
				objIData.AddParameter("@" + BorrowProduct_RVL.colCreatedUser, objBORRowProduct_RVL.CreatedUser);
                objIData.AddParameter("@" + BorrowProduct_RVL.colSEQUENCEReview, objBORRowProduct_RVL.SequenceReview);
                objIData.ExecNonQuery();
                return true;
			}
			catch (Exception objEx)
			{
                objIData.RollBackTransaction();
				throw (objEx);
			}
            return false;
		}


		/// <summary>
		/// Cập nhật thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objBORRowProduct_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Update(BorrowProduct_RVL objBORRowProduct_RVL, string CertificateString, string UserHostAddress, string LoginLogID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objBORRowProduct_RVL, CertificateString, UserHostAddress, LoginLogID);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin yêu cầu xuất đổi - mức duyệt", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BORRowProduct_RVL -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBORRowProduct_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, BorrowProduct_RVL objBORRowProduct_RVL, string CertificateString, string UserHostAddress, string LoginLogID)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + BorrowProduct_RVL.colBORRowProductID, objBORRowProduct_RVL.BorrowProductID);
				objIData.AddParameter("@" + BorrowProduct_RVL.colReviewLevelID, objBORRowProduct_RVL.ReviewLevelID);
				objIData.AddParameter("@" + BorrowProduct_RVL.colUserName, objBORRowProduct_RVL.UserName);
				objIData.AddParameter("@" + BorrowProduct_RVL.colReviewedStatus, objBORRowProduct_RVL.ReviewedStatus);
				objIData.AddParameter("@" + BorrowProduct_RVL.colNote, objBORRowProduct_RVL.Note);
				objIData.AddParameter("@" + BorrowProduct_RVL.colSEQUENCEReview, objBORRowProduct_RVL.SequenceReview);
                objIData.AddParameter("@" + BorrowProduct_RVL.colIsReviewedCheckInByFinger, objBORRowProduct_RVL.IsReviewedCheckInByFinger);
                objIData.AddParameter("@UserHostAddress", UserHostAddress);
                objIData.AddParameter("@CertificateString", CertificateString);
                objIData.AddParameter("@LoginLogID", LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objBORRowProduct_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(BorrowProduct_RVL objBORRowProduct_RVL)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objBORRowProduct_RVL);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin yêu cầu xuất đổi - mức duyệt", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BORRowProduct_RVL -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBORRowProduct_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, BorrowProduct_RVL objBORRowProduct_RVL)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + BorrowProduct_RVL.colBORRowProductRVLID, objBORRowProduct_RVL.BorrowProductRVLID);
				objIData.AddParameter("@" + BorrowProduct_RVL.colDeletedUser, objBORRowProduct_RVL.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_BorrowProduct_RVL()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_BORROWPRODUCT_RVL_ADD";
		public const String SP_UPDATE = "PM_BORROWPRODUCT_RVL_UPD";
		public const String SP_DELETE = "PM_BORROWPRODUCT_RVL1_DEL";
		public const String SP_SELECT = "PM_BORROWPRODUCT_RVL_SEL";
		public const String SP_SEARCH = "PM_BORROWPRODUCT_RVL_SRH";
		public const String SP_UPDATEINDEX = "PM_BORROWPRODUCT_RVL_UPDINDEX";
		#endregion
		
    }
}
