
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using BorrowProduct = ERP.Inventory.BO.BorrowProduct.BorrowProduct;
using ERP.Inventory.BO.BorrowProduct;
#endregion

namespace ERP.Inventory.DA.BorrowProduct
{
    /// <summary>
	/// Created by 		: Trung Hiếu 
	/// Created date 	: 07/11/2015 
	/// Chứng từ mượn hàng hóa
    /// 
    /// Update by 		: Phạm Hải Đăng
    /// Update date 	: 16/05/2017 
    /// khóa imei xuất theo fifo
	/// </summary>	
	public partial class DA_BorrowProduct
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chứng từ mượn hàng hóa
		/// </summary>
		/// <param name="objBorrowProduct">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct, string strBorrowProductID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductID, strBorrowProductID);
				IDataReader reader = objIData.ExecStoreToDataReader();
                if (objBorrowProduct == null)
                    objBorrowProduct = new BO.BorrowProduct.BorrowProduct();
				if (reader.Read())
 				{
                    //objBorrowProduct = new ERP.Inventory.BO.BorrowProduct.BorrowProduct();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductID])) objBorrowProduct.BorrowProductID = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductID]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowDate])) objBorrowProduct.BorrowDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowNote])) objBorrowProduct.BorrowNote = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowNote]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowUser])) objBorrowProduct.BorrowUser = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStoreID])) objBorrowProduct.BorrowStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStoreID]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStatus])) objBorrowProduct.BorrowStatus = Convert.ToInt32(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStatus]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReviewedStatus])) objBorrowProduct.ReviewedStatus = Convert.ToInt32(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReviewedStatus]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsFinishED])) objBorrowProduct.IsFinishED = Convert.ToBoolean(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsFinishED]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colFinishEDDate])) objBorrowProduct.FinishEDDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colFinishEDDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedStoreID])) objBorrowProduct.CreatedStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedUser])) objBorrowProduct.CreatedUser = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedDate])) objBorrowProduct.CreatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colUpdatedUser])) objBorrowProduct.UpdatedUser = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colUpdatedDate])) objBorrowProduct.UpdatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsDeleted])) objBorrowProduct.IsDeleted = Convert.ToBoolean(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedUser])) objBorrowProduct.DeletedUser = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedDate])) objBorrowProduct.DeletedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReturnPlanDate])) objBorrowProduct.ReturnPlanDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReturnPlanDate]);
                    objBorrowProduct.IsExist = true;
 				}
 				else
 				{
                    objBorrowProduct.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chứng từ mượn hàng hóa", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chứng từ mượn hàng hóa
		/// </summary>
		/// <param name="objBorrowProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct, ref string strBorrowProductID, ref DataTable dtbBorrowProduct)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
                Insert(objIData, objBorrowProduct);
                strBorrowProductID = objBorrowProduct.BorrowProductID;
                // Insert Duyet
                if (objBorrowProduct.BorrowProduct_RVLList != null
                    || objBorrowProduct.BorrowProduct_RVLList.Count > 0)
                {
                    foreach (BorrowProduct_RVL objBorrowProduct_RVL in objBorrowProduct.BorrowProduct_RVLList)
                    {
                        objBorrowProduct_RVL.BorrowProductID = strBorrowProductID;
                        objBorrowProduct_RVL.CreatedUser = objBorrowProduct.CreatedUser;
                        if (!new DA_BorrowProduct_RVL().Insert(objIData, objBorrowProduct_RVL))
                        {
                            objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chứng từ mượn hàng hóa", string.Empty);
                            return objResultMessage;
                        }
                    }
                }


                // LE VAN DONG 10/06/2017
                if (!string.IsNullOrEmpty(objBorrowProduct.RepairOrderID))
                {
                    ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_BorrowProduct objRepairOrder_BorrowProduct = new WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_BorrowProduct();
                    objRepairOrder_BorrowProduct.BorrowProductID = strBorrowProductID;
                    objRepairOrder_BorrowProduct.RepairOrderID = objBorrowProduct.RepairOrderID;
                    new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_BorrowProduct().Insert(objIData, objRepairOrder_BorrowProduct);
                }//> Le Van Dong


                if(InsertProduct(objIData, objBorrowProduct))
                    objIData.CommitTransaction();
                else{
                     dtbBorrowProduct = objBorrowProduct.DtbBorrowProductDT.Copy();
                     objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Tồn tại sản phẩm đã được mượn!",string.Empty);
                }


			}
			catch (Exception objEx)
			{
                dtbBorrowProduct = objBorrowProduct.DtbBorrowProductDT.Copy();
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chứng từ mượn hàng hóa", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
                
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}
        private bool InsertProduct(IData objIData, BO.BorrowProduct.BorrowProduct objBorrowProduct, bool CheckStatus = true)
        {
            try
            {
                BO.BorrowProduct.BorrowProductDT objBorrowProductDT = new BO.BorrowProduct.BorrowProductDT();
                objBorrowProductDT.BorrowProductID = objBorrowProduct.BorrowProductID;
                objBorrowProductDT.DeletedUser = objBorrowProduct.UpdatedUser;
                new DA_BorrowProductDT().Delete(objIData, objBorrowProductDT);

                bool bolError = false;
                foreach (DataRow row in objBorrowProduct.DtbBorrowProductDT.Rows)
                {
                    //BO.BorrowProduct.BorrowProductDT objBorrowProductDT = new BO.BorrowProduct.BorrowProductDT();
                    objBorrowProductDT.BorrowProductID = objBorrowProduct.BorrowProductID;
                    objBorrowProductDT.CreatedUser = objBorrowProduct.CreatedUser;
                    objBorrowProductDT.IsNew = Convert.ToInt16(row["ISNEW"]);
                    objBorrowProductDT.Note = Convert.ToString(row["NOTE"]);
                    objBorrowProductDT.ProductID = Convert.ToString(row["PRODUCTID"]);
                    objBorrowProductDT.IMEI = Convert.ToString(row["IMEI"]);
                    objBorrowProductDT.MachineStatus = Convert.ToString(row["MACHINESTATUS"]);
                    objBorrowProductDT.Quantity = Convert.ToInt32(row["QUANTITY"]);
                    int intStatus = 0;
                    string strBorrowProductDTId = string.Empty;
                    if (new DA_BorrowProductDT().Insert(objIData, objBorrowProductDT, bolError, CheckStatus, ref intStatus, ref strBorrowProductDTId) && CheckStatus)
                    {
                        bolError = true;
                        row["STATUS"] = intStatus;
                    }

                    // LE VAN DONG - THEM PHỤ KIỆN MƯỢN
                    if (strBorrowProductDTId != string.Empty && objBorrowProduct.DtbBorrowProductDT_Accesory != null)
                    {
                        foreach (DataRow rowAry in objBorrowProduct.DtbBorrowProductDT_Accesory.Rows)
                        {
                            BorrowProductDT_Accessory objBorrowProductDT_Accessory = new BorrowProductDT_Accessory();
                            objBorrowProductDT_Accessory.BorrowProductDTID = strBorrowProductDTId;
                            objBorrowProductDT_Accessory.MachineAccessoryID = Convert.ToInt32(rowAry["MACHINEACCESSORYID"]);
                            objBorrowProductDT_Accessory.Note = Convert.ToString(rowAry["NOTE"]);
                            new DA_BorrowProductDT_Accessory().Insert(objIData, objBorrowProductDT_Accessory);
                        }
                    }
                }
                if (bolError && CheckStatus)
                {
                    objIData.RollBackTransaction();
                    return false;
                    //throw new Exception("Tồn tại sản phẩm đã được mượn!");
                }
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw ex;
            }
            return true;
        }

		/// <summary>
		/// Thêm thông tin chứng từ mượn hàng hóa
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
                //objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductID, objBorrowProduct.BorrowProductID);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowDate, objBorrowProduct.BorrowDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowNote, objBorrowProduct.BorrowNote);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowUser, objBorrowProduct.BorrowUser);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStoreID, objBorrowProduct.BorrowStoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStatus, objBorrowProduct.BorrowStatus);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsFinishED, objBorrowProduct.IsFinishED);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colFinishEDDate, objBorrowProduct.FinishEDDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedStoreID, objBorrowProduct.CreatedStoreID);
                objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductTypeID, objBorrowProduct.BorrowProductTypeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedUser, objBorrowProduct.CreatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReturnPlanDate, objBorrowProduct.ReturnPlanDate);
                //objIData.ExecNonQuery();
                objBorrowProduct.BorrowProductID = objIData.ExecStoreToString();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chứng từ mượn hàng hóa
		/// </summary>
		/// <param name="objBorrowProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Update(ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct, ref DataTable dtbBorrowProduct)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
                Update(objIData, objBorrowProduct);
                // Insert Duyet
                if (objBorrowProduct.BorrowProduct_RVLList != null
                    || objBorrowProduct.BorrowProduct_RVLList.Count > 0)
                {
                    foreach (BorrowProduct_RVL objBorrowProduct_RVL in objBorrowProduct.BorrowProduct_RVLList)
                    {
                        objBorrowProduct_RVL.BorrowProductID = objBorrowProduct.BorrowProductID;
                        objBorrowProduct_RVL.CreatedUser = objBorrowProduct.UpdatedUser;
                        if (!new DA_BorrowProduct_RVL().Insert(objIData, objBorrowProduct_RVL))
                        {
                            objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi thêm thông tin chứng từ mượn hàng hóa", string.Empty);
                            return objResultMessage;
                        }
                    }
                }

                //if (InsertProduct(objIData, objBorrowProduct, false))
                //    objIData.CommitTransaction();
                //else
                //{
                //    dtbBorrowProduct = objBorrowProduct.DtbBorrowProductDT.Copy();
                //    objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Tồn tại sản phẩm đã được mượn!", string.Empty);
                //}

                if (objBorrowProduct.BorrowStatus == 2) // Khi trả hàng thì update tình trạng sau khi mượn
                {
                    BO.BorrowProduct.BorrowProductDT objBorrowProductDT = new BO.BorrowProduct.BorrowProductDT();
                    foreach (DataRow row in objBorrowProduct.DtbBorrowProductDT.Rows)
                    {
                        objBorrowProductDT.BorrowProductID = objBorrowProduct.BorrowProductID;
                        objBorrowProductDT.BorrowProductDTID = Convert.ToString(row["BORROWPRODUCTDTID"]).Trim();
                        objBorrowProductDT.UpdatedUser = objBorrowProduct.UpdatedUser;
                        objBorrowProductDT.IsNew = Convert.ToInt16(row["ISNEW"]);
                        objBorrowProductDT.Note = Convert.ToString(row["NOTE"]);
                        objBorrowProductDT.ProductID = Convert.ToString(row["PRODUCTID"]);
                        objBorrowProductDT.IMEI = Convert.ToString(row["IMEI"]);
                        objBorrowProductDT.MachineStatus = Convert.ToString(row["MACHINESTATUS"]);
                        objBorrowProductDT.MachineStatusReturn = Convert.ToString(row["MACHINESTATUSRETURN"]);
                        objBorrowProductDT.Quantity = Convert.ToInt32(row["QUANTITY"]);
                        if (new DA_BorrowProductDT().Update(objIData, objBorrowProductDT))
                        {
                        }
                    }
                }
                

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
                dtbBorrowProduct = objBorrowProduct.DtbBorrowProductDT.Copy();
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chứng từ mượn hàng hóa", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chứng từ mượn hàng hóa
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductID, objBorrowProduct.BorrowProductID);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowDate, objBorrowProduct.BorrowDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowNote, objBorrowProduct.BorrowNote);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowUser, objBorrowProduct.BorrowUser);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStoreID, objBorrowProduct.BorrowStoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStatus, objBorrowProduct.BorrowStatus);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsFinishED, objBorrowProduct.IsFinishED);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colFinishEDDate, objBorrowProduct.FinishEDDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedStoreID, objBorrowProduct.CreatedStoreID);
                objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductTypeID, objBorrowProduct.BorrowProductTypeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colUpdatedUser, objBorrowProduct.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReturnPlanDate, objBorrowProduct.ReturnPlanDate);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chứng từ mượn hàng hóa
		/// </summary>
		/// <param name="objBorrowProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
                //ERP.Inventory.DA.BorrowProduct.DA_BorrowProductDT objDA_BorrowProductDT = new DA_BorrowProductDT();
                //ERP.Inventory.BO.BorrowProduct.BorrowProductDT objBorrowProductDT = new BO.BorrowProduct.BorrowProductDT();
                //objBorrowProductDT.BorrowProductID = objBorrowProduct.BorrowProductID;
                //objBorrowProductDT.DeletedUser = objBorrowProduct.DeletedUser;
                //objDA_BorrowProductDT.Delete(objIData, objBorrowProductDT);
                

                Delete(objIData, objBorrowProduct);
                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chứng từ mượn hàng hóa", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chứng từ mượn hàng hóa
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct)
		{
			try 
			{
                string strBorrowProductID = objBorrowProduct.BorrowProductID;

                objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductID, objBorrowProduct.BorrowProductID);
				objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedUser, objBorrowProduct.DeletedUser);
                objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedReason, objBorrowProduct.DeletedReason);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();

                // LE VAN DONG 10/06/2017
                
                if(!string.IsNullOrEmpty(objBorrowProduct.RepairOrderID))
                {
                    ERP.WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_BorrowProduct objRepairOrder_BorrowProduct = new WarrantyRepair.BO.RepairWarrantyOrder.RepairOrder_BorrowProduct();
                    objRepairOrder_BorrowProduct.BorrowProductID = strBorrowProductID;
                    objRepairOrder_BorrowProduct.RepairOrderID = objBorrowProduct.RepairOrderID;
                    new ERP.WarrantyRepair.DA.RepairWarrantyOrder.DA_RepairOrder_BorrowProduct().Delete(objRepairOrder_BorrowProduct);
                }//> Le Van Dong
            }
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_BorrowProduct()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_BorrowPRODUCT_ADD";
		public const String SP_UPDATE = "PM_BorrowPRODUCT_UPD";
		public const String SP_DELETE = "PM_BorrowPRODUCT_DEL";
		public const String SP_SELECT = "PM_BorrowPRODUCT_SEL";
		public const String SP_SEARCH = "PM_BorrowPRODUCT_SRH";
		public const String SP_UPDATEINDEX = "PM_BorrowPRODUCT_UPDINDEX";
		#endregion
		
	}
}
