
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using BorrowProductDT = ERP.Inventory.BO.BorrowProduct.BorrowProductDT;
#endregion
namespace ERP.Inventory.DA.BorrowProduct
{
    /// <summary>
	/// Created by 		: Trung Hiếu 
	/// Created date 	: 07/11/2015 
	/// Chứng từ mượn hàng hóa - chi tiết
	/// </summary>	
	public partial class DA_BorrowProductDT
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chứng từ mượn hàng hóa - chi tiết
		/// </summary>
		/// <param name="objBorrowProductDT">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref BorrowProductDT objBorrowProductDT, string strBorrowProductDTID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + BorrowProductDT.colBorrowProductDTID, strBorrowProductDTID);
				IDataReader reader = objIData.ExecStoreToDataReader();
                if (objBorrowProductDT == null)
                    objBorrowProductDT = new BorrowProductDT();
				if (reader.Read())
 				{
                    //objBorrowProductDT = new BorrowProductDT();
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colBorrowProductDTID])) objBorrowProductDT.BorrowProductDTID = Convert.ToString(reader[BorrowProductDT.colBorrowProductDTID]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colBorrowProductID])) objBorrowProductDT.BorrowProductID = Convert.ToString(reader[BorrowProductDT.colBorrowProductID]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colProductID])) objBorrowProductDT.ProductID = Convert.ToString(reader[BorrowProductDT.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colIMEI])) objBorrowProductDT.IMEI = Convert.ToString(reader[BorrowProductDT.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colIsNew])) objBorrowProductDT.IsNew = Convert.ToInt16(reader[BorrowProductDT.colIsNew]);
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colQuantity])) objBorrowProductDT.Quantity = Convert.ToDecimal(reader[BorrowProductDT.colQuantity]);
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colNote])) objBorrowProductDT.Note = Convert.ToString(reader[BorrowProductDT.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colCreatedUser])) objBorrowProductDT.CreatedUser = Convert.ToString(reader[BorrowProductDT.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colCreatedDate])) objBorrowProductDT.CreatedDate = Convert.ToDateTime(reader[BorrowProductDT.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colUpdatedUser])) objBorrowProductDT.UpdatedUser = Convert.ToString(reader[BorrowProductDT.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colUpdatedDate])) objBorrowProductDT.UpdatedDate = Convert.ToDateTime(reader[BorrowProductDT.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colIsDeleted])) objBorrowProductDT.IsDeleted = Convert.ToBoolean(reader[BorrowProductDT.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colDeletedUser])) objBorrowProductDT.DeletedUser = Convert.ToString(reader[BorrowProductDT.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProductDT.colDeletedDate])) objBorrowProductDT.DeletedDate = Convert.ToDateTime(reader[BorrowProductDT.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[BorrowProductDT.colMachineStatus])) objBorrowProductDT.MachineStatus = Convert.ToString(reader[BorrowProductDT.colMachineStatus]).Trim();
                    if (!Convert.IsDBNull(reader[BorrowProductDT.colMachineStatusReturn])) objBorrowProductDT.MachineStatusReturn = Convert.ToString(reader[BorrowProductDT.colMachineStatusReturn]).Trim();
                    objBorrowProductDT.IsExist = true;
 				}
 				else
 				{
                    objBorrowProductDT.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chứng từ mượn hàng hóa - chi tiết", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chứng từ mượn hàng hóa - chi tiết
		/// </summary>
		/// <param name="objBorrowProductDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(BorrowProductDT objBorrowProductDT)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
                int intStatus = 0;
                string strBorrowProductDTId = string.Empty;
                Insert(objIData, objBorrowProductDT, false,false, ref intStatus, ref strBorrowProductDTId);
                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chứng từ mượn hàng hóa - chi tiết", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chứng từ mượn hàng hóa - chi tiết
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProductDT">Đối tượng truyền vào</param>
        /// <param name="bolIsError">Co lỗi trước đó.. Không cần insert chi kiểm tra</param>
		/// <returns>Kết quả trả về</returns>
		public bool Insert(IData objIData, BorrowProductDT objBorrowProductDT, bool bolIsError, bool bolIsCheck, ref int intStatus, ref string strBorrowProductDTId)
		{
            intStatus = 0;
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + BorrowProductDT.colBorrowProductDTID, objBorrowProductDT.BorrowProductDTID);
				objIData.AddParameter("@" + BorrowProductDT.colBorrowProductID, objBorrowProductDT.BorrowProductID);
				objIData.AddParameter("@" + BorrowProductDT.colProductID, objBorrowProductDT.ProductID);
				objIData.AddParameter("@" + BorrowProductDT.colIMEI, objBorrowProductDT.IMEI);
				objIData.AddParameter("@" + BorrowProductDT.colIsNew, objBorrowProductDT.IsNew);
				objIData.AddParameter("@" + BorrowProductDT.colQuantity, objBorrowProductDT.Quantity);
				objIData.AddParameter("@" + BorrowProductDT.colNote, objBorrowProductDT.Note);
				objIData.AddParameter("@" + BorrowProductDT.colCreatedUser, objBorrowProductDT.CreatedUser);
                objIData.AddParameter("@" + BorrowProductDT.colMachineStatus, objBorrowProductDT.MachineStatus);
                
                objIData.AddParameter("@ISERROR", bolIsError);
                objIData.AddParameter("@ISCHECK", bolIsCheck);
                string strError = objIData.ExecStoreToString();

                string[] strArray = strError.Split('|');

                intStatus = Convert.ToInt32(strArray[0]);
                if (strArray[0] != "0") 
                    return true;
                else
                {
                    if (strArray.Length == 2)
                        strBorrowProductDTId = strArray[1].ToString().Trim();
                    return false;
                }
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chứng từ mượn hàng hóa - chi tiết
		/// </summary>
		/// <param name="objBorrowProductDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(BorrowProductDT objBorrowProductDT)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
				Update(objIData, objBorrowProductDT);
                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chứng từ mượn hàng hóa - chi tiết", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chứng từ mượn hàng hóa - chi tiết
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProductDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public bool Update(IData objIData, BorrowProductDT objBorrowProductDT)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + BorrowProductDT.colBorrowProductDTID, objBorrowProductDT.BorrowProductDTID);
				objIData.AddParameter("@" + BorrowProductDT.colBorrowProductID, objBorrowProductDT.BorrowProductID);
				objIData.AddParameter("@" + BorrowProductDT.colProductID, objBorrowProductDT.ProductID);
				objIData.AddParameter("@" + BorrowProductDT.colIMEI, objBorrowProductDT.IMEI);
				objIData.AddParameter("@" + BorrowProductDT.colIsNew, objBorrowProductDT.IsNew);
				objIData.AddParameter("@" + BorrowProductDT.colQuantity, objBorrowProductDT.Quantity);
				objIData.AddParameter("@" + BorrowProductDT.colNote, objBorrowProductDT.Note);
				objIData.AddParameter("@" + BorrowProductDT.colUpdatedUser, objBorrowProductDT.UpdatedUser);
                objIData.AddParameter("@" + BorrowProductDT.colMachineStatus, objBorrowProductDT.MachineStatus);
                objIData.AddParameter("@" + BorrowProductDT.colMachineStatusReturn, objBorrowProductDT.MachineStatusReturn);

                string strStatus = objIData.ExecStoreToString();
                if (strStatus == "1")
                    return false;
                else
                {
                    // LE VAN DONG - BO SUNG 15/6/2016
                    // CAP NHẬT CHI TIET PHU KIEN MUON KEM
                    //if (objBorrowProductDT.BorrowProductDT_AccessoryList != null && objBorrowProductDT.BorrowProductDT_AccessoryList.Count > 0)
                    //{
                    //    foreach (ERP.Inventory.BO.BorrowProduct.BorrowProductDT_Accessory objBorrowProductDT_Accessory in objBorrowProductDT.BorrowProductDT_AccessoryList)
                    //    {
                    //        objBorrowProductDT_Accessory.BorrowProductDTID = objBorrowProductDT.BorrowProductDTID;
                    //        if (objBorrowProductDT_Accessory.Status == 0)
                    //            new DA_BorrowProductDT_Accessory().Insert(objIData, objBorrowProductDT_Accessory);
                    //        else if (objBorrowProductDT_Accessory.Status == 1)
                    //            new DA_BorrowProductDT_Accessory().Update(objIData, objBorrowProductDT_Accessory);
                    //        if (objBorrowProductDT_Accessory.Status == 2)
                    //            new DA_BorrowProductDT_Accessory().Delete(objIData, objBorrowProductDT_Accessory);
                    //    }
                    //}
                    return true;
                }
            }
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chứng từ mượn hàng hóa - chi tiết
		/// </summary>
		/// <param name="objBorrowProductDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(BorrowProductDT objBorrowProductDT)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
				Delete(objIData, objBorrowProductDT);
                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chứng từ mượn hàng hóa - chi tiết", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chứng từ mượn hàng hóa - chi tiết
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProductDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, BorrowProductDT objBorrowProductDT)
		{
			try 
			{
                string strBorrowProductDTID = objBorrowProductDT.BorrowProductDTID;

                objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + BorrowProductDT.colBorrowProductID, objBorrowProductDT.BorrowProductID);
				objIData.AddParameter("@" + BorrowProductDT.colDeletedUser, objBorrowProductDT.DeletedUser);
 				objIData.ExecNonQuery();

                // LE VAN DONG - BO SUNG 15/6/2016
                // XOA PHU KIEN MUON KEM
                new DA_BorrowProductDT_Accessory().DeleteAll(objIData, strBorrowProductDTID);
            }
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_BorrowProductDT()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_BorrowPRODUCTDT_ADD";
		public const String SP_UPDATE = "PM_BorrowPRODUCTDT_UPD";
		public const String SP_DELETE = "PM_BorrowPRODUCTDT_DEL";
		public const String SP_SELECT = "PM_BorrowPRODUCTDT_SEL";
		public const String SP_SEARCH = "PM_BorrowPRODUCTDT_SRH";
		public const String SP_UPDATEINDEX = "PM_BorrowPRODUCTDT_UPDINDEX";
		#endregion
		
	}
}
