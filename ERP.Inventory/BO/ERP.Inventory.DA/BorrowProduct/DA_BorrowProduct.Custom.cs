
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.BorrowProduct
{
    /// <summary>
	/// Created by 		: Trung Hiếu 
	/// Created date 	: 07/11/2015 
	/// Chứng từ mượn hàng hóa
	/// </summary>	
	public partial class DA_BorrowProduct
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin chứng từ mượn hàng hóa
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_BorrowProduct.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chứng từ mượn hàng hóa", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
		/// Tìm kiếm thông tin PHỤ KIỆN THEO MAINGROUP
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchDataMachineAccessory(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_MACHINEARY_MAINGROUP_LOAD");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm phụ kiện theo máy", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> SearchDataMachineAccessory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetBorrowProductNewID(ref string strBorrowProductID, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_BORROWPRODUCT_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strBorrowProductID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi lấy mã chứng từ mượn hàng hóa", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> GetBorrowProductNewID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage LoadInfoAll(ref ERP.Inventory.BO.BorrowProduct.BorrowProduct objBorrowProduct, string strBorrowProductID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            if (string.IsNullOrWhiteSpace(strBorrowProductID))
            {
                if (objBorrowProduct == null)
                    objBorrowProduct = new BO.BorrowProduct.BorrowProduct();
                objBorrowProduct.BorrowProductID = "-1";
                objResultMessage = GetSearchData(objBorrowProduct);
                return objResultMessage;
            }
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductID, strBorrowProductID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (objBorrowProduct == null)
                    objBorrowProduct = new BO.BorrowProduct.BorrowProduct();
                if (reader.Read())
                {
                    //objBorrowProduct = new ERP.Inventory.BO.BorrowProduct.BorrowProduct();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductID])) objBorrowProduct.BorrowProductID = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowProductID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowDate])) objBorrowProduct.BorrowDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowNote])) objBorrowProduct.BorrowNote = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowNote]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowUser])) objBorrowProduct.BorrowUser = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStoreID])) objBorrowProduct.BorrowStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStoreID]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStatus])) objBorrowProduct.BorrowStatus = Convert.ToInt32(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colBorrowStatus]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReviewedStatus])) objBorrowProduct.ReviewedStatus = Convert.ToInt32(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReviewedStatus]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsFinishED])) objBorrowProduct.IsFinishED = Convert.ToBoolean(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsFinishED]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colFinishEDDate])) objBorrowProduct.FinishEDDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colFinishEDDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedStoreID])) objBorrowProduct.CreatedStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedUser])) objBorrowProduct.CreatedUser = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedDate])) objBorrowProduct.CreatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colUpdatedUser])) objBorrowProduct.UpdatedUser = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colUpdatedDate])) objBorrowProduct.UpdatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsDeleted])) objBorrowProduct.IsDeleted = Convert.ToBoolean(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedUser])) objBorrowProduct.DeletedUser = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedDate])) objBorrowProduct.DeletedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReturnPlanDate])) objBorrowProduct.ReturnPlanDate = Convert.ToDateTime(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colReturnPlanDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colRepairOrderID])) objBorrowProduct.RepairOrderID = Convert.ToString(reader[ERP.Inventory.BO.BorrowProduct.BorrowProduct.colRepairOrderID]).Trim();
                    objResultMessage = GetSearchData(objBorrowProduct);
                }
                else
                {
                    objBorrowProduct = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chứng từ mượn hàng hóa", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> LoadInfoAll", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private ResultMessage GetSearchData(BO.BorrowProduct.BorrowProduct objBorrowProduct)
        {
            ResultMessage objResultMessage = new ResultMessage();
            DataTable dtb = null;
            objResultMessage = new DA_BorrowProductDT().SearchData(ref dtb, new object[] { "@BORROWPRODUCTID", objBorrowProduct.BorrowProductID });
            if (objResultMessage.IsError)
            {
                return objResultMessage;
            }
            objBorrowProduct.DtbBorrowProductDT = dtb;
            dtb = null;
            objResultMessage = new DA_BorrowProduct_Delivery().SearchData(ref dtb, new object[] { "@BORROWPRODUCTID", objBorrowProduct.BorrowProductID });
            if (objResultMessage.IsError)
            {
                return objResultMessage;
            }
            objBorrowProduct.DtbBorrowProduct_Delivery = dtb;
            dtb = null;
            return objResultMessage;
        }

        // <summary>
        /// Kiểm tra IMEI đã được khóa không có transation
        /// </summary>
        /// <param name="intStoreID">Đối tượng truyền vào</param>
        /// <param name="strProductID">Đối tượng truyền vào</param>
        /// <param name="strIMEI">Đối tượng truyền vào</param>
        /// <param name="intIsCheck">Đối tượng truyền vào</param> // 0: không trả về số lượng tồn, 1: trả về số lượng tồn
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LockOutPut_CHKIMEI(ref decimal decOut, int intStoreID, string strProductID, string strIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_LOCKOUTPUTFIFO_CHKIMEI");
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@Product", strProductID);
                objIData.AddParameter("@IMEI", strIMEI);
                decOut = Convert.ToDecimal(objIData.ExecStoreToString());
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chứng từ mượn hàng hóa", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> LockOutPutFIFO_CHKIMEI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Kiểm tra IMEI đã được khóa có transation
        /// </summary>
        /// <param name="strProductID">Đối tượng truyền vào</param>
        /// <param name="strIMEI">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public decimal LockOutPut_CHKIMEI(IData objIData, int intStoreID, string strProductID, string strIMEI)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_LOCKOUTPUTFIFO_CHKIMEI");
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@Product", strProductID);
                objIData.AddParameter("@IMEI", strIMEI);
                return Convert.ToDecimal(objIData.ExecStoreToString());
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
		/// Tìm kiếm thông tin chứng từ mượn hàng hóa
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage GetDataMachineStatusReturn(ref DataTable dtbData)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_BORROW_MACHINESTTRETURN_SRH");
                //objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp thông tin tình trạng máy sau khi trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct -> GetDataMachineStatusReturn", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion

    }
}
