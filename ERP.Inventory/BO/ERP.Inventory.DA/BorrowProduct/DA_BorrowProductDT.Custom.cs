
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.BorrowProduct
{
    /// <summary>
	/// Created by 		: Trung Hiếu 
	/// Created date 	: 07/11/2015 
	/// Chứng từ mượn hàng hóa - chi tiết
	/// </summary>	
	public partial class DA_BorrowProductDT
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin chứng từ mượn hàng hóa - chi tiết
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_BorrowProductDT.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chứng từ mượn hàng hóa - chi tiết", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}



        public int CheckImeiExist(string strBORROWPRODUCTID, string strPRODUCTID, bool bolISNEW, string strIMEI, decimal decQuantity, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_BORROWPRODUCTDT_EXSIT");
                objIData.AddParameter(
                "@BORROWPRODUCTID", strBORROWPRODUCTID,
                "@PRODUCTID", strPRODUCTID,
                "@ISNEW", bolISNEW,
                "@IMEI", strIMEI,
                "@QUANTITY", decQuantity,
                "@STOREID", intStoreID
                );
                string strReturn = objIData.ExecStoreToString();
                return Convert.ToInt32(strReturn);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chứng từ mượn hàng hóa - chi tiết", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProductDT -> SearchData", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            return -1;
        }
		#endregion
		
		
	}
}
