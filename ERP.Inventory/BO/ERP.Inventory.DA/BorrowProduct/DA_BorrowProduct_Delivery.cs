
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using BorrowProduct_Delivery = ERP.Inventory.BO.BorrowProduct.BorrowProduct_Delivery;
#endregion
namespace ERP.Inventory.DA.BorrowProduct
{
    /// <summary>
	/// Created by 		: Trung Hiếu 
	/// Created date 	: 07/11/2015 
	/// Chứng từ mượn hàng hóa - giao nhận
	/// </summary>	
	public partial class DA_BorrowProduct_Delivery
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chứng từ mượn hàng hóa - giao nhận
		/// </summary>
		/// <param name="objBorrowProduct_Delivery">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref BorrowProduct_Delivery objBorrowProduct_Delivery, string strBorrowProductID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colBorrowProductID, strBorrowProductID);
				IDataReader reader = objIData.ExecStoreToDataReader();
                if (objBorrowProduct_Delivery == null)
                    objBorrowProduct_Delivery = new BorrowProduct_Delivery();
				if (reader.Read())
 				{
                    //objBorrowProduct_Delivery = new BorrowProduct_Delivery();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colBorrowProductDeliveryID])) objBorrowProduct_Delivery.BorrowProductDeliveryID = Convert.ToString(reader[BorrowProduct_Delivery.colBorrowProductDeliveryID]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colBorrowProductID])) objBorrowProduct_Delivery.BorrowProductID = Convert.ToString(reader[BorrowProduct_Delivery.colBorrowProductID]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colDeliveryType])) objBorrowProduct_Delivery.DeliveryType = Convert.ToBoolean(reader[BorrowProduct_Delivery.colDeliveryType]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colDeliveryDate])) objBorrowProduct_Delivery.DeliveryDate = Convert.ToDateTime(reader[BorrowProduct_Delivery.colDeliveryDate]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colRECIEVEUser])) objBorrowProduct_Delivery.RECIEVEUser = Convert.ToString(reader[BorrowProduct_Delivery.colRECIEVEUser]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colDeliverySER])) objBorrowProduct_Delivery.DeliverySER = Convert.ToString(reader[BorrowProduct_Delivery.colDeliverySER]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colNote])) objBorrowProduct_Delivery.Note = Convert.ToString(reader[BorrowProduct_Delivery.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colUserHostAddress])) objBorrowProduct_Delivery.UserHostAddress = Convert.ToString(reader[BorrowProduct_Delivery.colUserHostAddress]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colCertificateString])) objBorrowProduct_Delivery.CertificateString = Convert.ToString(reader[BorrowProduct_Delivery.colCertificateString]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colLoginLogID])) objBorrowProduct_Delivery.LoginLogID = Convert.ToString(reader[BorrowProduct_Delivery.colLoginLogID]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colCreatedUser])) objBorrowProduct_Delivery.CreatedUser = Convert.ToString(reader[BorrowProduct_Delivery.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colCreatedDate])) objBorrowProduct_Delivery.CreatedDate = Convert.ToDateTime(reader[BorrowProduct_Delivery.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colUpdatedUser])) objBorrowProduct_Delivery.UpdatedUser = Convert.ToString(reader[BorrowProduct_Delivery.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colUpdatedDate])) objBorrowProduct_Delivery.UpdatedDate = Convert.ToDateTime(reader[BorrowProduct_Delivery.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colIsDeleted])) objBorrowProduct_Delivery.IsDeleted = Convert.ToBoolean(reader[BorrowProduct_Delivery.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colDeletedUser])) objBorrowProduct_Delivery.DeletedUser = Convert.ToString(reader[BorrowProduct_Delivery.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BorrowProduct_Delivery.colDeletedDate])) objBorrowProduct_Delivery.DeletedDate = Convert.ToDateTime(reader[BorrowProduct_Delivery.colDeletedDate]);
                    objBorrowProduct_Delivery.IsExist = true;
 				}
 				else
 				{
                    objBorrowProduct_Delivery.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chứng từ mượn hàng hóa - giao nhận", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct_Delivery -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chứng từ mượn hàng hóa - giao nhận
		/// </summary>
		/// <param name="objBorrowProduct_Delivery">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(BorrowProduct_Delivery objBorrowProduct_Delivery, ref string strBorrowProductID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objBorrowProduct_Delivery);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chứng từ mượn hàng hóa - giao nhận", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct_Delivery -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chứng từ mượn hàng hóa - giao nhận
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProduct_Delivery">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, BorrowProduct_Delivery objBorrowProduct_Delivery)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colBorrowProductDeliveryID, objBorrowProduct_Delivery.BorrowProductDeliveryID);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colBorrowProductID, objBorrowProduct_Delivery.BorrowProductID);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colDeliveryType, objBorrowProduct_Delivery.DeliveryType);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colDeliveryDate, objBorrowProduct_Delivery.DeliveryDate);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colRECIEVEUser, objBorrowProduct_Delivery.RECIEVEUser);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colDeliverySER, objBorrowProduct_Delivery.DeliverySER);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colNote, objBorrowProduct_Delivery.Note);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colUserHostAddress, objBorrowProduct_Delivery.UserHostAddress);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colCertificateString, objBorrowProduct_Delivery.CertificateString);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colLoginLogID, objBorrowProduct_Delivery.LoginLogID);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colCreatedUser, objBorrowProduct_Delivery.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chứng từ mượn hàng hóa - giao nhận
		/// </summary>
		/// <param name="objBorrowProduct_Delivery">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(BorrowProduct_Delivery objBorrowProduct_Delivery)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objBorrowProduct_Delivery);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chứng từ mượn hàng hóa - giao nhận", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct_Delivery -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chứng từ mượn hàng hóa - giao nhận
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProduct_Delivery">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, BorrowProduct_Delivery objBorrowProduct_Delivery)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colBorrowProductDeliveryID, objBorrowProduct_Delivery.BorrowProductDeliveryID);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colBorrowProductID, objBorrowProduct_Delivery.BorrowProductID);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colDeliveryType, objBorrowProduct_Delivery.DeliveryType);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colDeliveryDate, objBorrowProduct_Delivery.DeliveryDate);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colRECIEVEUser, objBorrowProduct_Delivery.RECIEVEUser);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colDeliverySER, objBorrowProduct_Delivery.DeliverySER);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colNote, objBorrowProduct_Delivery.Note);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colUserHostAddress, objBorrowProduct_Delivery.UserHostAddress);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colCertificateString, objBorrowProduct_Delivery.CertificateString);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colLoginLogID, objBorrowProduct_Delivery.LoginLogID);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colUpdatedUser, objBorrowProduct_Delivery.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chứng từ mượn hàng hóa - giao nhận
		/// </summary>
		/// <param name="objBorrowProduct_Delivery">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(BorrowProduct_Delivery objBorrowProduct_Delivery)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objBorrowProduct_Delivery);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chứng từ mượn hàng hóa - giao nhận", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BorrowProduct_Delivery -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chứng từ mượn hàng hóa - giao nhận
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBorrowProduct_Delivery">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, BorrowProduct_Delivery objBorrowProduct_Delivery)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colBorrowProductDeliveryID, objBorrowProduct_Delivery.BorrowProductDeliveryID);
				objIData.AddParameter("@" + BorrowProduct_Delivery.colDeletedUser, objBorrowProduct_Delivery.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_BorrowProduct_Delivery()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_BorrowPRODUCT_DELIVERY_ADD";
		public const String SP_UPDATE = "PM_BorrowPRODUCT_DELIVERY_UPD";
		public const String SP_DELETE = "PM_BorrowPRODUCT_DELIVERY_DEL";
		public const String SP_SELECT = "PM_BorrowPRODUCT_DELIVERY_SEL";
		public const String SP_SEARCH = "PM_BorrowPRODUCT_DELIVERY_SRH";
		public const String SP_UPDATEINDEX = "PM_BorrowPRODUCT_DELIVERY_UPDINDEX";
		#endregion
		
	}
}
