
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputChangeOrder_WorkFlow = ERP.Inventory.BO.InputChangeOrder.InputChangeOrder_WorkFlow;
#endregion
namespace ERP.Inventory.DA.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Danh sách qui trình của yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public partial class DA_InputChangeOrder_WorkFlow
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_WorkFlow">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InputChangeOrder_WorkFlow objInputChangeOrder_WorkFlow, string strInputChangeOrderID, int intInputChangeOrderStepID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderID, strInputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderStepID, intInputChangeOrderStepID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objInputChangeOrder_WorkFlow = new InputChangeOrder_WorkFlow();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_WorkFlow.colInputChangeOrderID])) objInputChangeOrder_WorkFlow.InputChangeOrderID = Convert.ToString(reader[InputChangeOrder_WorkFlow.colInputChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_WorkFlow.colInputChangeOrderStepID])) objInputChangeOrder_WorkFlow.InputChangeOrderStepID = Convert.ToInt32(reader[InputChangeOrder_WorkFlow.colInputChangeOrderStepID]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_WorkFlow.colInputChangeOrderDate])) objInputChangeOrder_WorkFlow.InputChangeOrderDate = Convert.ToDateTime(reader[InputChangeOrder_WorkFlow.colInputChangeOrderDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_WorkFlow.colIsProcessed])) objInputChangeOrder_WorkFlow.IsProcessed = Convert.ToBoolean(reader[InputChangeOrder_WorkFlow.colIsProcessed]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_WorkFlow.colProcessedUser])) objInputChangeOrder_WorkFlow.ProcessedUser = Convert.ToString(reader[InputChangeOrder_WorkFlow.colProcessedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_WorkFlow.colProcessedTime])) objInputChangeOrder_WorkFlow.ProcessedTime = Convert.ToDateTime(reader[InputChangeOrder_WorkFlow.colProcessedTime]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_WorkFlow.colNote])) objInputChangeOrder_WorkFlow.Note = Convert.ToString(reader[InputChangeOrder_WorkFlow.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_WorkFlow.colCreatedDate])) objInputChangeOrder_WorkFlow.CreatedDate = Convert.ToDateTime(reader[InputChangeOrder_WorkFlow.colCreatedDate]);
 				}
 				else
 				{
 					objInputChangeOrder_WorkFlow = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_WorkFlow -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_WorkFlow">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InputChangeOrder_WorkFlow objInputChangeOrder_WorkFlow)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInputChangeOrder_WorkFlow);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_WorkFlow -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_WorkFlow">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InputChangeOrder_WorkFlow objInputChangeOrder_WorkFlow)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderID, objInputChangeOrder_WorkFlow.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderStepID, objInputChangeOrder_WorkFlow.InputChangeOrderStepID);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderDate, objInputChangeOrder_WorkFlow.InputChangeOrderDate);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colIsProcessed, objInputChangeOrder_WorkFlow.IsProcessed);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colProcessedUser, objInputChangeOrder_WorkFlow.ProcessedUser);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colProcessedTime, objInputChangeOrder_WorkFlow.ProcessedTime);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colNote, objInputChangeOrder_WorkFlow.Note);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_WorkFlow">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InputChangeOrder_WorkFlow objInputChangeOrder_WorkFlow)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInputChangeOrder_WorkFlow);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_WorkFlow -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_WorkFlow">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InputChangeOrder_WorkFlow objInputChangeOrder_WorkFlow)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderID, objInputChangeOrder_WorkFlow.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderStepID, objInputChangeOrder_WorkFlow.InputChangeOrderStepID);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderDate, objInputChangeOrder_WorkFlow.InputChangeOrderDate);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colIsProcessed, objInputChangeOrder_WorkFlow.IsProcessed);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colProcessedUser, objInputChangeOrder_WorkFlow.ProcessedUser);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colProcessedTime, objInputChangeOrder_WorkFlow.ProcessedTime);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colNote, objInputChangeOrder_WorkFlow.Note);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_WorkFlow">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InputChangeOrder_WorkFlow objInputChangeOrder_WorkFlow)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInputChangeOrder_WorkFlow);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_WorkFlow -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách qui trình của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_WorkFlow">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InputChangeOrder_WorkFlow objInputChangeOrder_WorkFlow)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderID, objInputChangeOrder_WorkFlow.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_WorkFlow.colInputChangeOrderStepID, objInputChangeOrder_WorkFlow.InputChangeOrderStepID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InputChangeOrder_WorkFlow()
		{
		}
		#endregion


		#region Stored Procedure Names

        public const String SP_ADD = "PM_INPUTCHANGEORDER_WFL_ADD";
        public const String SP_UPDATE = "PM_INPUTCHANGEORDER_WFL_UPD";
		public const String SP_DELETE = "PM_INPUTCHANGEORDER_WORKFLOW_DEL";
        public const String SP_SELECT = "PM_INPUTCHANGEORDER_WFL_SEL";
		public const String SP_SEARCH = "PM_INPUTCHANGEORDER_WORKFLOW_SRH";
		public const String SP_UPDATEINDEX = "PM_INPUTCHANGEORDER_WORKFLOW_UPDINDEX";
		#endregion
		
	}
}
