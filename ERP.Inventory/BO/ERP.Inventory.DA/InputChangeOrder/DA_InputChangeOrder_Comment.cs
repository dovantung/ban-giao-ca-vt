
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputChangeOrder_Comment = ERP.Inventory.BO.InputChangeOrder.InputChangeOrder_Comment;
#endregion
namespace ERP.Inventory.DA.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Danh sách bình luận của yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public partial class DA_InputChangeOrder_Comment
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_Comment">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InputChangeOrder_Comment objInputChangeOrder_Comment, string strCommentID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentID, strCommentID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objInputChangeOrder_Comment = new InputChangeOrder_Comment();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colCommentID])) objInputChangeOrder_Comment.CommentID = Convert.ToString(reader[InputChangeOrder_Comment.colCommentID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colInputChangeOrderID])) objInputChangeOrder_Comment.InputChangeOrderID = Convert.ToString(reader[InputChangeOrder_Comment.colInputChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colInputChangeOrderDate])) objInputChangeOrder_Comment.InputChangeOrderDate = Convert.ToDateTime(reader[InputChangeOrder_Comment.colInputChangeOrderDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colCommentDate])) objInputChangeOrder_Comment.CommentDate = Convert.ToDateTime(reader[InputChangeOrder_Comment.colCommentDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colCommentTitle])) objInputChangeOrder_Comment.CommentTitle = Convert.ToString(reader[InputChangeOrder_Comment.colCommentTitle]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colCommentContent])) objInputChangeOrder_Comment.CommentContent = Convert.ToString(reader[InputChangeOrder_Comment.colCommentContent]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colCommentUser])) objInputChangeOrder_Comment.CommentUser = Convert.ToString(reader[InputChangeOrder_Comment.colCommentUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colIsActive])) objInputChangeOrder_Comment.IsActive = Convert.ToBoolean(reader[InputChangeOrder_Comment.colIsActive]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colIsSystem])) objInputChangeOrder_Comment.IsSystem = Convert.ToBoolean(reader[InputChangeOrder_Comment.colIsSystem]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colCreatedUser])) objInputChangeOrder_Comment.CreatedUser = Convert.ToString(reader[InputChangeOrder_Comment.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colCreatedDate])) objInputChangeOrder_Comment.CreatedDate = Convert.ToDateTime(reader[InputChangeOrder_Comment.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colUpdatedUser])) objInputChangeOrder_Comment.UpdatedUser = Convert.ToString(reader[InputChangeOrder_Comment.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colUpdatedDate])) objInputChangeOrder_Comment.UpdatedDate = Convert.ToDateTime(reader[InputChangeOrder_Comment.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colIsDeleted])) objInputChangeOrder_Comment.IsDeleted = Convert.ToBoolean(reader[InputChangeOrder_Comment.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colDeletedUser])) objInputChangeOrder_Comment.DeletedUser = Convert.ToString(reader[InputChangeOrder_Comment.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Comment.colDeletedDate])) objInputChangeOrder_Comment.DeletedDate = Convert.ToDateTime(reader[InputChangeOrder_Comment.colDeletedDate]);
 				}
 				else
 				{
 					objInputChangeOrder_Comment = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_Comment -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ref string strCommentID, InputChangeOrder_Comment objInputChangeOrder_Comment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                Insert(objIData, ref strCommentID, objInputChangeOrder_Comment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_Comment -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, ref string strCommentID, InputChangeOrder_Comment objInputChangeOrder_Comment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colInputChangeOrderID, objInputChangeOrder_Comment.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentTitle, objInputChangeOrder_Comment.CommentTitle);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentContent, objInputChangeOrder_Comment.CommentContent);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentUser, objInputChangeOrder_Comment.CommentUser);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colIsActive, objInputChangeOrder_Comment.IsActive);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colIsSystem, objInputChangeOrder_Comment.IsSystem);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCreatedUser, objInputChangeOrder_Comment.CreatedUser);
                strCommentID = objIData.ExecStoreToString();
                objInputChangeOrder_Comment.CommentID = strCommentID;
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InputChangeOrder_Comment objInputChangeOrder_Comment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInputChangeOrder_Comment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_Comment -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InputChangeOrder_Comment objInputChangeOrder_Comment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentID, objInputChangeOrder_Comment.CommentID);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colInputChangeOrderID, objInputChangeOrder_Comment.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colInputChangeOrderDate, objInputChangeOrder_Comment.InputChangeOrderDate);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentDate, objInputChangeOrder_Comment.CommentDate);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentTitle, objInputChangeOrder_Comment.CommentTitle);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentContent, objInputChangeOrder_Comment.CommentContent);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentUser, objInputChangeOrder_Comment.CommentUser);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colIsActive, objInputChangeOrder_Comment.IsActive);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colIsSystem, objInputChangeOrder_Comment.IsSystem);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colUpdatedUser, objInputChangeOrder_Comment.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InputChangeOrder_Comment objInputChangeOrder_Comment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInputChangeOrder_Comment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_Comment -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách bình luận của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_Comment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InputChangeOrder_Comment objInputChangeOrder_Comment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colCommentID, objInputChangeOrder_Comment.CommentID);
				objIData.AddParameter("@" + InputChangeOrder_Comment.colDeletedUser, objInputChangeOrder_Comment.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InputChangeOrder_Comment()
		{
		}
		#endregion


		#region Stored Procedure Names

        public const String SP_ADD = "PM_InputChangeOrderComment_ADD";
        public const String SP_DELETE = "PM_InputChangeOrderComment_DEL";
        public const String SP_UPDATE = "PM_INPUTCHANGEORDER_COMMENT_UPD";
		public const String SP_SELECT = "PM_INPUTCHANGEORDER_COMMENT_SEL";
		public const String SP_SEARCH = "PM_INPUTCHANGEORDER_COMMENT_SRH";
		public const String SP_UPDATEINDEX = "PM_INPUTCHANGEORDER_COMMENT_UPDINDEX";
		#endregion
		
	}
}
