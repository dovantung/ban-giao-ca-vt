
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputChangeOrder_Attachment = ERP.Inventory.BO.InputChangeOrder.InputChangeOrder_Attachment;
#endregion
namespace ERP.Inventory.DA.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Danh sách file đính kèm của yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public partial class DA_InputChangeOrder_Attachment
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_Attachment">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InputChangeOrder_Attachment objInputChangeOrder_Attachment, string strAttachmentID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colAttachmentID, strAttachmentID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objInputChangeOrder_Attachment = new InputChangeOrder_Attachment();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colAttachmentID])) objInputChangeOrder_Attachment.AttachmentID = Convert.ToString(reader[InputChangeOrder_Attachment.colAttachmentID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colInputChangeOrderID])) objInputChangeOrder_Attachment.InputChangeOrderID = Convert.ToString(reader[InputChangeOrder_Attachment.colInputChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colInputChangeOrderDate])) objInputChangeOrder_Attachment.InputChangeOrderDate = Convert.ToDateTime(reader[InputChangeOrder_Attachment.colInputChangeOrderDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colAttachmentName])) objInputChangeOrder_Attachment.AttachmentName = Convert.ToString(reader[InputChangeOrder_Attachment.colAttachmentName]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colAttachmentPath])) objInputChangeOrder_Attachment.AttachmentPath = Convert.ToString(reader[InputChangeOrder_Attachment.colAttachmentPath]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colFileID])) objInputChangeOrder_Attachment.FileID = Convert.ToString(reader[InputChangeOrder_Attachment.colFileID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colDescription])) objInputChangeOrder_Attachment.Description = Convert.ToString(reader[InputChangeOrder_Attachment.colDescription]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colCreatedUser])) objInputChangeOrder_Attachment.CreatedUser = Convert.ToString(reader[InputChangeOrder_Attachment.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colCreatedDate])) objInputChangeOrder_Attachment.CreatedDate = Convert.ToDateTime(reader[InputChangeOrder_Attachment.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colIsDeleted])) objInputChangeOrder_Attachment.IsDeleted = Convert.ToBoolean(reader[InputChangeOrder_Attachment.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colDeletedUser])) objInputChangeOrder_Attachment.DeletedUser = Convert.ToString(reader[InputChangeOrder_Attachment.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_Attachment.colDeletedDate])) objInputChangeOrder_Attachment.DeletedDate = Convert.ToDateTime(reader[InputChangeOrder_Attachment.colDeletedDate]);
 				}
 				else
 				{
 					objInputChangeOrder_Attachment = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_Attachment -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InputChangeOrder_Attachment objInputChangeOrder_Attachment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInputChangeOrder_Attachment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_Attachment -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InputChangeOrder_Attachment objInputChangeOrder_Attachment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colAttachmentID, objInputChangeOrder_Attachment.AttachmentID);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colInputChangeOrderID, objInputChangeOrder_Attachment.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colInputChangeOrderDate, objInputChangeOrder_Attachment.InputChangeOrderDate);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colAttachmentName, objInputChangeOrder_Attachment.AttachmentName);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colAttachmentPath, objInputChangeOrder_Attachment.AttachmentPath);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colFileID, objInputChangeOrder_Attachment.FileID);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colDescription, objInputChangeOrder_Attachment.Description);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colCreatedUser, objInputChangeOrder_Attachment.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InputChangeOrder_Attachment objInputChangeOrder_Attachment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInputChangeOrder_Attachment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_Attachment -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InputChangeOrder_Attachment objInputChangeOrder_Attachment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colAttachmentID, objInputChangeOrder_Attachment.AttachmentID);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colInputChangeOrderID, objInputChangeOrder_Attachment.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colInputChangeOrderDate, objInputChangeOrder_Attachment.InputChangeOrderDate);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colAttachmentName, objInputChangeOrder_Attachment.AttachmentName);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colAttachmentPath, objInputChangeOrder_Attachment.AttachmentPath);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colFileID, objInputChangeOrder_Attachment.FileID);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colDescription, objInputChangeOrder_Attachment.Description);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InputChangeOrder_Attachment objInputChangeOrder_Attachment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInputChangeOrder_Attachment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_Attachment -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách file đính kèm của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InputChangeOrder_Attachment objInputChangeOrder_Attachment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colAttachmentID, objInputChangeOrder_Attachment.AttachmentID);
				objIData.AddParameter("@" + InputChangeOrder_Attachment.colDeletedUser, objInputChangeOrder_Attachment.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InputChangeOrder_Attachment()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_INPUTCHANGEORDER_ATTACHMENT_ADD";
		public const String SP_UPDATE = "PM_INPUTCHANGEORDER_ATTACHMENT_UPD";
		public const String SP_DELETE = "PM_INPUTCHANGEORDER_ATTACHMENT_DEL";
		public const String SP_SELECT = "PM_INPUTCHANGEORDER_ATTACHMENT_SEL";
		public const String SP_SEARCH = "PM_INPUTCHANGEORDER_ATTACHMENT_SRH";
		public const String SP_UPDATEINDEX = "PM_INPUTCHANGEORDER_ATTACHMENT_UPDINDEX";
		#endregion
		
	}
}
