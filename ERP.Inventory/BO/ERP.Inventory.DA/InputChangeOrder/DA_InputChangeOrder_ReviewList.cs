
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputChangeOrder_ReviewList = ERP.Inventory.BO.InputChangeOrder.InputChangeOrder_ReviewList;
#endregion
namespace ERP.Inventory.DA.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Danh sách người duyệt của yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public partial class DA_InputChangeOrder_ReviewList
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_ReviewList">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InputChangeOrder_ReviewList objInputChangeOrder_ReviewList, string strInputChangeOrderID, int intReviewLevelID, string strUserName)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colInputChangeOrderID, strInputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewLevelID, intReviewLevelID);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colUserName, strUserName);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objInputChangeOrder_ReviewList = new InputChangeOrder_ReviewList();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colInputChangeOrderID])) objInputChangeOrder_ReviewList.InputChangeOrderID = Convert.ToString(reader[InputChangeOrder_ReviewList.colInputChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colReviewLevelID])) objInputChangeOrder_ReviewList.ReviewLevelID = Convert.ToInt32(reader[InputChangeOrder_ReviewList.colReviewLevelID]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colUserName])) objInputChangeOrder_ReviewList.UserName = Convert.ToString(reader[InputChangeOrder_ReviewList.colUserName]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colInputChangeOrderDate])) objInputChangeOrder_ReviewList.InputChangeOrderDate = Convert.ToDateTime(reader[InputChangeOrder_ReviewList.colInputChangeOrderDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colReviewStatus])) objInputChangeOrder_ReviewList.ReviewStatus = Convert.ToInt32(reader[InputChangeOrder_ReviewList.colReviewStatus]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colIsReviewed])) objInputChangeOrder_ReviewList.IsReviewed = Convert.ToBoolean(reader[InputChangeOrder_ReviewList.colIsReviewed]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colReviewedDate])) objInputChangeOrder_ReviewList.ReviewedDate = Convert.ToDateTime(reader[InputChangeOrder_ReviewList.colReviewedDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colCreatedUser])) objInputChangeOrder_ReviewList.CreatedUser = Convert.ToString(reader[InputChangeOrder_ReviewList.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colCreatedDate])) objInputChangeOrder_ReviewList.CreatedDate = Convert.ToDateTime(reader[InputChangeOrder_ReviewList.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colUpdatedUser])) objInputChangeOrder_ReviewList.UpdatedUser = Convert.ToString(reader[InputChangeOrder_ReviewList.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colUpdatedDate])) objInputChangeOrder_ReviewList.UpdatedDate = Convert.ToDateTime(reader[InputChangeOrder_ReviewList.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colIsDeleted])) objInputChangeOrder_ReviewList.IsDeleted = Convert.ToBoolean(reader[InputChangeOrder_ReviewList.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colDeletedUser])) objInputChangeOrder_ReviewList.DeletedUser = Convert.ToString(reader[InputChangeOrder_ReviewList.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeOrder_ReviewList.colDeletedDate])) objInputChangeOrder_ReviewList.DeletedDate = Convert.ToDateTime(reader[InputChangeOrder_ReviewList.colDeletedDate]);
 				}
 				else
 				{
 					objInputChangeOrder_ReviewList = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_ReviewList -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InputChangeOrder_ReviewList objInputChangeOrder_ReviewList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInputChangeOrder_ReviewList);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_ReviewList -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InputChangeOrder_ReviewList objInputChangeOrder_ReviewList)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colInputChangeOrderID, objInputChangeOrder_ReviewList.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewLevelID, objInputChangeOrder_ReviewList.ReviewLevelID);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colUserName, objInputChangeOrder_ReviewList.UserName);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colInputChangeOrderDate, objInputChangeOrder_ReviewList.InputChangeOrderDate);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewStatus, objInputChangeOrder_ReviewList.ReviewStatus);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colIsReviewed, objInputChangeOrder_ReviewList.IsReviewed);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewedDate, objInputChangeOrder_ReviewList.ReviewedDate);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colCreatedUser, objInputChangeOrder_ReviewList.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        public void Insert(IData objIData, DataRow rReviewList)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colInputChangeOrderID, rReviewList["InputChangeOrderID"]);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewLevelID, rReviewList["ReviewLevelID"]);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colUserName, rReviewList["UserName"]);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colInputChangeOrderDate, rReviewList["InputChangeOrderDate"]);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewStatus, rReviewList["ReviewStatus"]);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colIsReviewed, rReviewList["IsReviewed"]);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewedDate, rReviewList["ReviewedDate"]);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colCreatedUser, rReviewList["CreatedUser"]);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

		/// <summary>
		/// Cập nhật thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InputChangeOrder_ReviewList objInputChangeOrder_ReviewList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInputChangeOrder_ReviewList);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_ReviewList -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InputChangeOrder_ReviewList objInputChangeOrder_ReviewList)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colInputChangeOrderID, objInputChangeOrder_ReviewList.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewLevelID, objInputChangeOrder_ReviewList.ReviewLevelID);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colUserName, objInputChangeOrder_ReviewList.UserName);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colInputChangeOrderDate, objInputChangeOrder_ReviewList.InputChangeOrderDate);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewStatus, objInputChangeOrder_ReviewList.ReviewStatus);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colIsReviewed, objInputChangeOrder_ReviewList.IsReviewed);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewedDate, objInputChangeOrder_ReviewList.ReviewedDate);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colUpdatedUser, objInputChangeOrder_ReviewList.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
        public ResultMessage UpdateReviewList(InputChangeOrder_ReviewList objInputChangeOrder_ReviewList, int intCountReviewList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_UPDREVIEW");
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colInputChangeOrderID, objInputChangeOrder_ReviewList.InputChangeOrderID);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewLevelID, objInputChangeOrder_ReviewList.ReviewLevelID);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colUserName, objInputChangeOrder_ReviewList.UserName);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewStatus, objInputChangeOrder_ReviewList.ReviewStatus);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colRowSCN, objInputChangeOrder_ReviewList.RowSCN);
                objIData.AddParameter("@" + InputChangeOrder_ReviewList.colIsReviewByFinger, objInputChangeOrder_ReviewList.IsReviewByFinger);
                objIData.AddParameter("@CountReviewList", intCountReviewList);
                objIData.ExecNonQuery();

                if (objInputChangeOrder_ReviewList.ReviewStatus == 3 && objInputChangeOrder_ReviewList.AutoUpdateOrderStepID > 0)
                {
                    BO.InputChangeOrder.InputChangeOrder_WorkFlow objWorkFlow = null;
                    objIData.CreateNewStoredProcedure(DA_InputChangeOrder_WorkFlow.SP_SELECT);
                    objIData.AddParameter("@" + BO.InputChangeOrder.InputChangeOrder_WorkFlow.colInputChangeOrderID, objInputChangeOrder_ReviewList.InputChangeOrderID);
                    objIData.AddParameter("@" + BO.InputChangeOrder.InputChangeOrder_WorkFlow.colInputChangeOrderStepID, objInputChangeOrder_ReviewList.AutoUpdateOrderStepID);
                    IDataReader reader = objIData.ExecStoreToDataReader();
                    if (reader.Read())
                    {
                        objWorkFlow = new BO.InputChangeOrder.InputChangeOrder_WorkFlow();
                        if (!Convert.IsDBNull(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colInputChangeOrderID])) objWorkFlow.InputChangeOrderID = Convert.ToString(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colInputChangeOrderID]).Trim();
                        if (!Convert.IsDBNull(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colInputChangeOrderStepID])) objWorkFlow.InputChangeOrderStepID = Convert.ToInt32(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colInputChangeOrderStepID]);
                        if (!Convert.IsDBNull(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colIsProcessed])) objWorkFlow.IsProcessed = Convert.ToBoolean(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colIsProcessed]);
                        if (!Convert.IsDBNull(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colProcessedUser])) objWorkFlow.ProcessedUser = Convert.ToString(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colProcessedUser]).Trim();
                        if (!Convert.IsDBNull(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colProcessedTime])) objWorkFlow.ProcessedTime = Convert.ToDateTime(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colProcessedTime]);
                        if (!Convert.IsDBNull(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colNote])) objWorkFlow.Note = Convert.ToString(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colNote]).Trim();
                        if (!Convert.IsDBNull(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colCreatedDate])) objWorkFlow.CreatedDate = Convert.ToDateTime(reader[BO.InputChangeOrder.InputChangeOrder_WorkFlow.colCreatedDate]);
                        if (!Convert.IsDBNull(reader["InputChangeOrderDate"])) objWorkFlow.InputChangeOrderDate = Convert.ToDateTime(reader["InputChangeOrderDate"]);
                    }
                    reader.Close();

                    if (objWorkFlow != null)
                    {
                        objWorkFlow.IsProcessed = true;
                        objWorkFlow.ProcessedUser = objInputChangeOrder_ReviewList.UserName;
                        objWorkFlow.ProcessedTime = DateTime.Now;
                        objWorkFlow.Note = "Tự động cập nhật Đã xử lý khi mã nhân viên " + objInputChangeOrder_ReviewList.UserName + " duyệt mức duyệt " + objInputChangeOrder_ReviewList.ReviewLevelID;

                        objIData.CreateNewStoredProcedure(DA_InputChangeOrder_WorkFlow.SP_UPDATE);
                        objIData.AddParameter("@" + BO.InputChangeOrder.InputChangeOrder_WorkFlow.colInputChangeOrderID, objWorkFlow.InputChangeOrderID);
                        objIData.AddParameter("@" + BO.InputChangeOrder.InputChangeOrder_WorkFlow.colInputChangeOrderStepID, objWorkFlow.InputChangeOrderStepID);
                        objIData.AddParameter("@InputChangeOrderDate", objWorkFlow.InputChangeOrderDate);
                        objIData.AddParameter("@" + BO.InputChangeOrder.InputChangeOrder_WorkFlow.colIsProcessed, objWorkFlow.IsProcessed);
                        objIData.AddParameter("@" + BO.InputChangeOrder.InputChangeOrder_WorkFlow.colProcessedUser, objWorkFlow.ProcessedUser);
                        objIData.AddParameter("@" + BO.InputChangeOrder.InputChangeOrder_WorkFlow.colProcessedTime, objWorkFlow.ProcessedTime);
                        objIData.AddParameter("@" + BO.InputChangeOrder.InputChangeOrder_WorkFlow.colNote, objWorkFlow.Note);
                        objIData.ExecNonQuery();
                    }
                }


                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật duyệt yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_ReviewList -> UpdateReviewList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

		/// <summary>
		/// Xóa thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objInputChangeOrder_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InputChangeOrder_ReviewList objInputChangeOrder_ReviewList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInputChangeOrder_ReviewList);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder_ReviewList -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách người duyệt của yêu cầu nhập đổi/trả hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeOrder_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InputChangeOrder_ReviewList objInputChangeOrder_ReviewList)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colInputChangeOrderID, objInputChangeOrder_ReviewList.InputChangeOrderID);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colReviewLevelID, objInputChangeOrder_ReviewList.ReviewLevelID);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colUserName, objInputChangeOrder_ReviewList.UserName);
				objIData.AddParameter("@" + InputChangeOrder_ReviewList.colDeletedUser, objInputChangeOrder_ReviewList.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InputChangeOrder_ReviewList()
		{
		}
		#endregion


		#region Stored Procedure Names

        public const String SP_ADD = "PM_INPUTCHANGEORDER_RVLIST_ADD";
        public const String SP_UPDATE = "PM_INPUTCHANGEORDER_RVLIST_UPD";
        public const String SP_DELETE = "PM_INPUTCHANGEORDER_RVLIST_DEL";
		public const String SP_SELECT = "PM_INPUTCHANGEORDER_REVIEWLIST_SEL";
		public const String SP_SEARCH = "PM_INPUTCHANGEORDER_REVIEWLIST_SRH";
		public const String SP_UPDATEINDEX = "PM_INPUTCHANGEORDER_REVIEWLIST_UPDINDEX";
		#endregion
		
	}
}
