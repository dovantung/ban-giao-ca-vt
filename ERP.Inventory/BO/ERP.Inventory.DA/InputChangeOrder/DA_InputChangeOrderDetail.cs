
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputChangeOrderDetail = ERP.Inventory.BO.InputChangeOrder.InputChangeOrderDetail;
#endregion
namespace ERP.Inventory.DA.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Bảng chi tiết yêu cầu nhập đổi/trả hàng
    /// 
    /// Modified by : Nguyen Van Tai
    /// Modified date : 30/11/2017
    /// Desc : Bo sung Nhom loi, loi
	/// </summary>	
	public partial class DA_InputChangeOrderDetail
    {

        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods			


        /// <summary>
        /// Nạp thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objInputChangeOrderDetail">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref InputChangeOrderDetail objInputChangeOrderDetail, string strInputChangeOrderDetailID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderDetailID, strInputChangeOrderDetailID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objInputChangeOrderDetail = new InputChangeOrderDetail();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colInputChangeOrderDetailID])) objInputChangeOrderDetail.InputChangeOrderDetailID = Convert.ToString(reader[InputChangeOrderDetail.colInputChangeOrderDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colInputChangeOrderID])) objInputChangeOrderDetail.InputChangeOrderID = Convert.ToString(reader[InputChangeOrderDetail.colInputChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colInputChangeOrderDate])) objInputChangeOrderDetail.InputChangeOrderDate = Convert.ToDateTime(reader[InputChangeOrderDetail.colInputChangeOrderDate]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colInputChangeOrderStoreID])) objInputChangeOrderDetail.InputChangeOrderStoreID = Convert.ToInt32(reader[InputChangeOrderDetail.colInputChangeOrderStoreID]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colOLDOutputVoucherDetailID])) objInputChangeOrderDetail.OldOutputVoucherDetailID = Convert.ToString(reader[InputChangeOrderDetail.colOLDOutputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colProductID_In])) objInputChangeOrderDetail.ProductID_In = Convert.ToString(reader[InputChangeOrderDetail.colProductID_In]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colQuantity])) objInputChangeOrderDetail.Quantity = Convert.ToDecimal(reader[InputChangeOrderDetail.colQuantity]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colIMEI_In])) objInputChangeOrderDetail.IMEI_In = Convert.ToString(reader[InputChangeOrderDetail.colIMEI_In]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colInputPrice])) objInputChangeOrderDetail.InputPrice = Convert.ToDecimal(reader[InputChangeOrderDetail.colInputPrice]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colProductID_Out])) objInputChangeOrderDetail.ProductID_Out = Convert.ToString(reader[InputChangeOrderDetail.colProductID_Out]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colIMEI_Out])) objInputChangeOrderDetail.IMEI_Out = Convert.ToString(reader[InputChangeOrderDetail.colIMEI_Out]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colSalePrice])) objInputChangeOrderDetail.SalePrice = Convert.ToDecimal(reader[InputChangeOrderDetail.colSalePrice]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colInputChangeDetailID])) objInputChangeOrderDetail.InputChangeDetailID = Convert.ToString(reader[InputChangeOrderDetail.colInputChangeDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colNewInputVoucherDetailID])) objInputChangeOrderDetail.NewInputVoucherDetailID = Convert.ToString(reader[InputChangeOrderDetail.colNewInputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colNewOutputVoucherDetailID])) objInputChangeOrderDetail.NewOutputVoucherDetailID = Convert.ToString(reader[InputChangeOrderDetail.colNewOutputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colCreatedStoreID])) objInputChangeOrderDetail.CreatedStoreID = Convert.ToInt32(reader[InputChangeOrderDetail.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colCreatedUser])) objInputChangeOrderDetail.CreatedUser = Convert.ToString(reader[InputChangeOrderDetail.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colCreatedDate])) objInputChangeOrderDetail.CreatedDate = Convert.ToDateTime(reader[InputChangeOrderDetail.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colUpdatedUser])) objInputChangeOrderDetail.UpdatedUser = Convert.ToString(reader[InputChangeOrderDetail.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colUpdatedDate])) objInputChangeOrderDetail.UpdatedDate = Convert.ToDateTime(reader[InputChangeOrderDetail.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colIsDeleted])) objInputChangeOrderDetail.IsDeleted = Convert.ToBoolean(reader[InputChangeOrderDetail.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colDeletedUser])) objInputChangeOrderDetail.DeletedUser = Convert.ToString(reader[InputChangeOrderDetail.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colDeletedDate])) objInputChangeOrderDetail.DeletedDate = Convert.ToDateTime(reader[InputChangeOrderDetail.colDeletedDate]);
                    //Nguyen Van Tai bo sung theo yeu cau cua BA Trung - 30/11/2017
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colMachineErrorGroupId])) objInputChangeOrderDetail.MachineErrorGroupId = Convert.ToInt32(reader[InputChangeOrderDetail.colMachineErrorGroupId]);
                    if (!Convert.IsDBNull(reader[InputChangeOrderDetail.colMachineErrorId])) objInputChangeOrderDetail.MachineErrorId = Convert.ToInt32(reader[InputChangeOrderDetail.colMachineErrorId]);
                    //End                    
                }
                else
                {
                    objInputChangeOrderDetail = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrderDetail -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objInputChangeOrderDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(InputChangeOrderDetail objInputChangeOrderDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objInputChangeOrderDetail);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrderDetail -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputChangeOrderDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        /// 

        public void Insert(IData objIData, InputChangeOrderDetail objInputChangeOrderDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderID, objInputChangeOrderDetail.InputChangeOrderID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderDate, objInputChangeOrderDetail.InputChangeOrderDate);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderStoreID, objInputChangeOrderDetail.InputChangeOrderStoreID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colOLDOutputVoucherDetailID, objInputChangeOrderDetail.OldOutputVoucherDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colProductID_In, objInputChangeOrderDetail.ProductID_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colQuantity, objInputChangeOrderDetail.Quantity);
                objIData.AddParameter("@" + InputChangeOrderDetail.colIMEI_In, objInputChangeOrderDetail.IMEI_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputPrice, objInputChangeOrderDetail.InputPrice);
                objIData.AddParameter("@" + InputChangeOrderDetail.colProductID_Out, objInputChangeOrderDetail.ProductID_Out);
                objIData.AddParameter("@" + InputChangeOrderDetail.colIMEI_Out, objInputChangeOrderDetail.IMEI_Out);
                objIData.AddParameter("@" + InputChangeOrderDetail.colSalePrice, objInputChangeOrderDetail.SalePrice);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeDetailID, objInputChangeOrderDetail.InputChangeDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colNewInputVoucherDetailID, objInputChangeOrderDetail.NewInputVoucherDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colNewOutputVoucherDetailID, objInputChangeOrderDetail.NewOutputVoucherDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colReturnFee, objInputChangeOrderDetail.ReturnFee);
                objIData.AddParameter("@" + InputChangeOrderDetail.colReturnInputTypeID, objInputChangeOrderDetail.ReturnInputTypeID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colIsNew_In, objInputChangeOrderDetail.IsNew_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colVAT_In, objInputChangeOrderDetail.VAT_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colVATPercent_In, objInputChangeOrderDetail.VATPercent_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colIsHasWarranty_In, objInputChangeOrderDetail.IsHasWarranty_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colOutputTypeID_Out, objInputChangeOrderDetail.OutputTypeID_Out);
                objIData.AddParameter("@" + InputChangeOrderDetail.colEndWarrantyDate, objInputChangeOrderDetail.EndWarrantyDate);
                objIData.AddParameter("@" + InputChangeOrderDetail.colCreatedStoreID, objInputChangeOrderDetail.CreatedStoreID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colCreatedUser, objInputChangeOrderDetail.CreatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                //Nguyen Van Tai bo sung theo yeu cau cua BA Trung - 30/11/2017
                objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorGroupId, objInputChangeOrderDetail.MachineErrorGroupId);
                objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorId, objInputChangeOrderDetail.MachineErrorId);
                objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorContent, objInputChangeOrderDetail.MachineErrorContent);
                //End
                objIData.AddParameter("@" + InputChangeOrderDetail.colInStockStatusID_in, objInputChangeOrderDetail.InStockStatusID_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInStockStatusID_Out, objInputChangeOrderDetail.InStockStatusID_Out);
                objIData.AddParameter("@IsUpdateConcern", objInputChangeOrderDetail.IsUpdateConcern);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objInputChangeOrderDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(InputChangeOrderDetail objInputChangeOrderDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objInputChangeOrderDetail);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrderDetail -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputChangeOrderDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, InputChangeOrderDetail objInputChangeOrderDetail, string strMachineErrorIDList = "")
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderDetailID, objInputChangeOrderDetail.InputChangeOrderDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colUpdatedUser, objInputChangeOrderDetail.UpdatedUser);
                //Nguyen Van Tai bo sung theo yeu cau cua BA Trung - 30/11/2017
                objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorGroupId, objInputChangeOrderDetail.MachineErrorGroupId);
                objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorContent, objInputChangeOrderDetail.MachineErrorContent);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@MACHINEERRORIDLIST", strMachineErrorIDList == "" ? string.Empty : strMachineErrorIDList);
                //End
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objInputChangeOrderDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(InputChangeOrderDetail objInputChangeOrderDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objInputChangeOrderDetail);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrderDetail -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin bảng chi tiết yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputChangeOrderDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, InputChangeOrderDetail objInputChangeOrderDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderDetailID, objInputChangeOrderDetail.InputChangeOrderDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colDeletedUser, objInputChangeOrderDetail.DeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_InputChangeOrderDetail()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "PM_INPUTCHANGEORDERDETAIL_ADD";
        public const String SP_UPDATE = "PM_INPUTCHANGEORDERDETAIL_UPD";
        public const String SP_DELETE = "PM_INPUTCHANGEORDERDETAIL_DEL";
        public const String SP_SELECT = "PM_INPUTCHANGEORDERDETAIL_SEL";
        public const String SP_SEARCH = "PM_INPUTCHANGEORDERDETAIL_SRH";
        public const String SP_UPDATEINDEX = "PM_INPUTCHANGEORDERDETAIL_UPDINDEX";
        #endregion

    }
}
