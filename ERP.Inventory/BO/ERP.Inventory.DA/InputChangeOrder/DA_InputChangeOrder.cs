
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using System.Linq;
using InputChangeOrder = ERP.Inventory.BO.InputChangeOrder.InputChangeOrder;
#endregion
namespace ERP.Inventory.DA.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Bảng yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public partial class DA_InputChangeOrder
    {


        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods			


        /// <summary>
        /// Nạp thông tin bảng yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objInputChangeOrder">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, string strInputChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderID, strInputChangeOrderID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objInputChangeOrder = new ERP.Inventory.BO.InputChangeOrder.InputChangeOrder();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderID])) objInputChangeOrder.InputChangeOrderID = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderTypeID])) objInputChangeOrder.InputChangeOrderTypeID = Convert.ToInt32(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderTypeID]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderDate])) objInputChangeOrder.InputChangeOrderDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderStoreID])) objInputChangeOrder.InputChangeOrderStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderStoreID]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colOldOutputVoucherID])) objInputChangeOrder.OldOutputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colOldOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderContent])) objInputChangeOrder.InputChangeOrderContent = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderContent]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsReviewed])) objInputChangeOrder.IsReviewed = Convert.ToBoolean(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsReviewed]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colReviewedDate])) objInputChangeOrder.ReviewedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colReviewedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsInputChanged])) objInputChangeOrder.IsInputChanged = Convert.ToBoolean(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsInputChanged]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeDate])) objInputChangeOrder.InputChangeDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeID])) objInputChangeOrder.InputChangeID = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colNewInputVoucherID])) objInputChangeOrder.NewInputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colNewInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colNewOutputVoucherID])) objInputChangeOrder.NewOutputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colNewOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInVoucherID])) objInputChangeOrder.InVoucherID = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colOutVoucherID])) objInputChangeOrder.OutVoucherID = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colOutVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colSaleOrderID])) objInputChangeOrder.SaleOrderID = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colSaleOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsInputReturn])) objInputChangeOrder.IsInputReturn = Convert.ToBoolean(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsInputReturn]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colUnevenAmount])) objInputChangeOrder.UnevenAmount = Convert.ToDecimal(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colUnevenAmount]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colCreatedStoreID])) objInputChangeOrder.CreatedStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colCreatedUser])) objInputChangeOrder.CreatedUser = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colCreatedDate])) objInputChangeOrder.CreatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colUpdatedUser])) objInputChangeOrder.UpdatedUser = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colUpdatedDate])) objInputChangeOrder.UpdatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsDeleted])) objInputChangeOrder.IsDeleted = Convert.ToBoolean(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colDeletedUser])) objInputChangeOrder.DeletedUser = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colDeletedDate])) objInputChangeOrder.DeletedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colDeletedDate]);
                    if (!Convert.IsDBNull(reader["ContentDeleted"])) objInputChangeOrder.ContentDeleted = Convert.ToString(reader["ContentDeleted"]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeReasonID])) objInputChangeOrder.InputChangeReasonID = Convert.ToInt32(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeReasonID]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeReasonNote])) objInputChangeOrder.InputChangeReasonNote = Convert.ToString(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeReasonNote]).Trim();
                    if (!Convert.IsDBNull(reader["APPLYERRORTYPE"])) objInputChangeOrder.ApplyErrorType = Convert.ToBoolean(reader["APPLYERRORTYPE"]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colRowSCN])) objInputChangeOrder.RowSCN = Convert.ToDecimal(reader[ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colRowSCN]);
                    if (!Convert.IsDBNull(reader["ISDEBTORDER"])) objInputChangeOrder.IsDebtOrder = Convert.ToBoolean(reader["ISDEBTORDER"]);
                    if (!Convert.IsDBNull(reader["BROKERUSER"])) objInputChangeOrder.BrokerUser = Convert.ToString(reader["BROKERUSER"]);
                }
                else
                {
                    objInputChangeOrder = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin bảng yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin bảng yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objInputChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, DataTable dtbMachineErrorIDList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                //Tạo mới mã yêu cầu nhập đổi/trả hàng
                string strInputChangeOrderID = GetInputChangeOrderNewID(objIData, objInputChangeOrder.CreatedStoreID);
                objInputChangeOrder.InputChangeOrderID = strInputChangeOrderID;
                Insert(objIData, objInputChangeOrder);

                DA_InputChangeOrderDetail objDA_Detail = new DA_InputChangeOrderDetail();
                foreach (BO.InputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail in objInputChangeOrder.InputChangeOrderDetailList)
                {
                    objInputChangeOrderDetail.InputChangeOrderID = objInputChangeOrder.InputChangeOrderID;
                    objInputChangeOrderDetail.InputChangeOrderDate = objInputChangeOrder.InputChangeOrderDate;
                    objInputChangeOrderDetail.InputChangeOrderStoreID = objInputChangeOrder.InputChangeOrderStoreID;
                    objInputChangeOrderDetail.CreatedStoreID = objInputChangeOrder.CreatedStoreID;
                    objInputChangeOrderDetail.CreatedUser = objInputChangeOrder.CreatedUser;
                    //objDA_Detail.Ins(objIData, objInputChangeOrderDetail);
                    if (!Convert.IsDBNull(dtbMachineErrorIDList))
                    {
                        string strMachineErrorIDList = string.Empty;

                        //Lấy string
                        var listError = dtbMachineErrorIDList.AsEnumerable().Where(p => p["OLDOUTPUTVOUCHERDETAILID"].ToString().Trim() == objInputChangeOrderDetail.OldOutputVoucherDetailID.ToString().Trim());
                        if (listError.Any())
                        {
                            foreach (var item in listError)
                                strMachineErrorIDList += item["MACHINEERRORID"].ToString() + ",";
                            strMachineErrorIDList = strMachineErrorIDList.TrimEnd(',');
                            objDA_Detail.InsertWithMachineError(objIData, objInputChangeOrderDetail, strMachineErrorIDList);
                        }
                        else
                            objDA_Detail.Insert(objIData, objInputChangeOrderDetail);
                    }
                    else
                        objDA_Detail.Insert(objIData, objInputChangeOrderDetail);
                }

                //if (objInputChangeOrder.InputChangeOrder_ReviewTable != null && objInputChangeOrder.InputChangeOrder_ReviewTable.Rows.Count > 0)
                //{
                //    DA_InputChangeOrder_ReviewList objDA_ReviewList = new DA_InputChangeOrder_ReviewList();
                //    for (int i = 0; i < objInputChangeOrder.InputChangeOrder_ReviewTable.Rows.Count; i++)
                //    {
                //        DataRow rReviewList = objInputChangeOrder.InputChangeOrder_ReviewTable.Rows[i];
                //        rReviewList["InputChangeOrderID"] = strInputChangeOrderID;
                //        rReviewList["CreatedUser"] = objInputChangeOrder.CreatedUser;
                //        objDA_ReviewList.Insert(objIData, rReviewList);
                //    }
                //}

                if (objInputChangeOrder.InputChangeOrder_WorkFlowTable != null && objInputChangeOrder.InputChangeOrder_WorkFlowTable.Rows.Count > 0)
                {
                    DA_InputChangeOrder_WorkFlow objDA_WorkFlow = new DA_InputChangeOrder_WorkFlow();
                    for (int i = 0; i < objInputChangeOrder.InputChangeOrder_WorkFlowTable.Rows.Count; i++)
                    {
                        DataRow rWorkFlow = objInputChangeOrder.InputChangeOrder_WorkFlowTable.Rows[i];
                        BO.InputChangeOrder.InputChangeOrder_WorkFlow objWorkFlow = new BO.InputChangeOrder.InputChangeOrder_WorkFlow();
                        objWorkFlow.InputChangeOrderID = strInputChangeOrderID;
                        objWorkFlow.InputChangeOrderStepID = Convert.ToInt32(rWorkFlow["InputChangeOrderStepID"]);
                        objWorkFlow.InputChangeOrderDate = objInputChangeOrder.InputChangeOrderDate;
                        if (Convert.ToBoolean(rWorkFlow["IsInitStep"]))
                        {
                            objWorkFlow.ProcessedUser = objInputChangeOrder.CreatedUser;
                            objWorkFlow.ProcessedTime = DateTime.Now;
                            objWorkFlow.IsProcessed = true;
                        }
                        objDA_WorkFlow.Insert(objIData, objWorkFlow);
                    }
                }

                InsertAttachment(objIData, strInputChangeOrderID, (DateTime)objInputChangeOrder.InputChangeOrderDate, objInputChangeOrder.CreatedUser, objInputChangeOrder.InputChangeOrder_AttachmentList);

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin bảng yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin bảng yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderID, objInputChangeOrder.InputChangeOrderID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderTypeID, objInputChangeOrder.InputChangeOrderTypeID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderDate, objInputChangeOrder.InputChangeOrderDate);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderStoreID, objInputChangeOrder.InputChangeOrderStoreID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colOldOutputVoucherID, objInputChangeOrder.OldOutputVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderContent, objInputChangeOrder.InputChangeOrderContent);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsReviewed, objInputChangeOrder.IsReviewed);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colReviewedDate, objInputChangeOrder.ReviewedDate);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsInputChanged, objInputChangeOrder.IsInputChanged);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeDate, objInputChangeOrder.InputChangeDate);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeID, objInputChangeOrder.InputChangeID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colNewInputVoucherID, objInputChangeOrder.NewInputVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colNewOutputVoucherID, objInputChangeOrder.NewOutputVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInVoucherID, objInputChangeOrder.InVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colOutVoucherID, objInputChangeOrder.OutVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colSaleOrderID, objInputChangeOrder.SaleOrderID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsInputReturn, objInputChangeOrder.IsInputReturn);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colUnevenAmount, objInputChangeOrder.UnevenAmount);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colCreatedStoreID, objInputChangeOrder.CreatedStoreID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colCreatedUser, objInputChangeOrder.CreatedUser);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeReasonID, objInputChangeOrder.InputChangeReasonID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeReasonNote, objInputChangeOrder.InputChangeReasonNote);
                objIData.AddParameter("@BROKERUSER", objInputChangeOrder.BrokerUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@APPLYERRORTYPE", objInputChangeOrder.ApplyErrorType);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin bảng yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objInputChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, DataTable dtbMachineErrorIDList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                objIData.CreateNewStoredProcedure("PM_InputChangeOrder_UPD");
                objIData.AddParameter("@InputChangeOrderID", objInputChangeOrder.InputChangeOrderID);
                objIData.AddParameter("@InputChangeOrderContent", objInputChangeOrder.InputChangeOrderContent);
                objIData.AddParameter("@InputChangeReasonID", objInputChangeOrder.InputChangeReasonID);
                objIData.AddParameter("@InputChangeReasonNote", objInputChangeOrder.InputChangeReasonNote);
                //objIData.AddParameter("@MachineError", objInputChangeOrder.InputChangeReasonNote);
                objIData.AddParameter("@UpdatedUser", objInputChangeOrder.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();

                //Update detail
                DA_InputChangeOrderDetail objDA_InputChangeOrderDetail = new DA_InputChangeOrderDetail();
                foreach (var dt in objInputChangeOrder.InputChangeOrderDetailList)
                {
                    string strMachineErrorIDList = string.Empty;
                    if (!Convert.IsDBNull(dtbMachineErrorIDList))
                    {                     
                        //Lấy string
                        var listError = dtbMachineErrorIDList.AsEnumerable().Where(p => p["OLDOUTPUTVOUCHERDETAILID"].ToString().Trim() == dt.OldOutputVoucherDetailID.ToString().Trim());
                        if (listError.Any())
                        {
                            foreach (var item in listError)
                                strMachineErrorIDList += item["MACHINEERRORID"].ToString() + ",";
                            strMachineErrorIDList = strMachineErrorIDList.TrimEnd(',');
                           // objDA_InputChangeOrderDetail.UpdateWithMachineError(objIData, dt, strMachineErrorIDList);
                        }
                    }
                    objDA_InputChangeOrderDetail.Update(objIData, dt, strMachineErrorIDList);
                }
                InsertAttachment(objIData, objInputChangeOrder.InputChangeOrderID, (DateTime)objInputChangeOrder.InputChangeOrderDate, objInputChangeOrder.UpdatedUser, objInputChangeOrder.InputChangeOrder_AttachmentList);
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi cập nhật thông tin bảng yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin bảng yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderID, objInputChangeOrder.InputChangeOrderID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderTypeID, objInputChangeOrder.InputChangeOrderTypeID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderDate, objInputChangeOrder.InputChangeOrderDate);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderStoreID, objInputChangeOrder.InputChangeOrderStoreID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colOldOutputVoucherID, objInputChangeOrder.OldOutputVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderContent, objInputChangeOrder.InputChangeOrderContent);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsReviewed, objInputChangeOrder.IsReviewed);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colReviewedDate, objInputChangeOrder.ReviewedDate);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsInputChanged, objInputChangeOrder.IsInputChanged);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeDate, objInputChangeOrder.InputChangeDate);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeID, objInputChangeOrder.InputChangeID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colNewInputVoucherID, objInputChangeOrder.NewInputVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colNewOutputVoucherID, objInputChangeOrder.NewOutputVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInVoucherID, objInputChangeOrder.InVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colOutVoucherID, objInputChangeOrder.OutVoucherID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colSaleOrderID, objInputChangeOrder.SaleOrderID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colIsInputReturn, objInputChangeOrder.IsInputReturn);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colUnevenAmount, objInputChangeOrder.UnevenAmount);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colCreatedStoreID, objInputChangeOrder.CreatedStoreID);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colUpdatedUser, objInputChangeOrder.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin bảng yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objInputChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objInputChangeOrder);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin bảng yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin bảng yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, ERP.Inventory.BO.InputChangeOrder.InputChangeOrder objInputChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colInputChangeOrderID, objInputChangeOrder.InputChangeOrderID);
                objIData.AddParameter("@ContentDeleted", objInputChangeOrder.ContentDeleted);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChangeOrder.InputChangeOrder.colDeletedUser, objInputChangeOrder.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        #endregion


        #region Constructor

        public DA_InputChangeOrder()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "PM_INPUTCHANGEORDER_ADD";
        public const String SP_UPDATE = "PM_INPUTCHANGEORDER_UPD";
        public const String SP_DELETE = "PM_INPUTCHANGEORDER_DEL";
        public const String SP_SELECT = "PM_INPUTCHANGEORDER_SEL";
        public const String SP_SEARCH = "PM_INPUTCHANGEORDER_SRH";
        public const String SP_UPDATEINDEX = "PM_INPUTCHANGEORDER_UPDINDEX";
        #endregion

    }
}
