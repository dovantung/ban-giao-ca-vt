
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO.InputChangeOrder;
#endregion
namespace ERP.Inventory.DA.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// 
	/// </summary>	
	public partial class DA_InputChangeOrderDetail
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin 
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_InputChangeOrderDetail.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrderDetail -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public ResultMessage GetListReturn(ref DataTable dtbData, string strOutputVoucherID, string strInputChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_OUTPUTVOUCHERDETAIL_GetList");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                objIData.AddParameter("@InputChangeOrderID", strInputChangeOrderID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy chi tiết yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrderDetail -> GetListReturn", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion

        public void InsertWithMachineError(IData objIData, InputChangeOrderDetail objInputChangeOrderDetail, string strMachineErrorIDList)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDERDETAIL_ADD2");
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderID, objInputChangeOrderDetail.InputChangeOrderID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderDate, objInputChangeOrderDetail.InputChangeOrderDate);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderStoreID, objInputChangeOrderDetail.InputChangeOrderStoreID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colOLDOutputVoucherDetailID, objInputChangeOrderDetail.OldOutputVoucherDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colProductID_In, objInputChangeOrderDetail.ProductID_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colQuantity, objInputChangeOrderDetail.Quantity);
                objIData.AddParameter("@" + InputChangeOrderDetail.colIMEI_In, objInputChangeOrderDetail.IMEI_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputPrice, objInputChangeOrderDetail.InputPrice);
                objIData.AddParameter("@" + InputChangeOrderDetail.colProductID_Out, objInputChangeOrderDetail.ProductID_Out);
                objIData.AddParameter("@" + InputChangeOrderDetail.colIMEI_Out, objInputChangeOrderDetail.IMEI_Out);
                objIData.AddParameter("@" + InputChangeOrderDetail.colSalePrice, objInputChangeOrderDetail.SalePrice);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeDetailID, objInputChangeOrderDetail.InputChangeDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colNewInputVoucherDetailID, objInputChangeOrderDetail.NewInputVoucherDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colNewOutputVoucherDetailID, objInputChangeOrderDetail.NewOutputVoucherDetailID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colReturnFee, objInputChangeOrderDetail.ReturnFee);
                objIData.AddParameter("@" + InputChangeOrderDetail.colReturnInputTypeID, objInputChangeOrderDetail.ReturnInputTypeID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colIsNew_In, objInputChangeOrderDetail.IsNew_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colVAT_In, objInputChangeOrderDetail.VAT_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colVATPercent_In, objInputChangeOrderDetail.VATPercent_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colIsHasWarranty_In, objInputChangeOrderDetail.IsHasWarranty_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colOutputTypeID_Out, objInputChangeOrderDetail.OutputTypeID_Out);
                objIData.AddParameter("@" + InputChangeOrderDetail.colEndWarrantyDate, objInputChangeOrderDetail.EndWarrantyDate);
                objIData.AddParameter("@" + InputChangeOrderDetail.colCreatedStoreID, objInputChangeOrderDetail.CreatedStoreID);
                objIData.AddParameter("@" + InputChangeOrderDetail.colCreatedUser, objInputChangeOrderDetail.CreatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                //Nguyen Van Tai bo sung theo yeu cau cua BA Trung - 30/11/2017
                objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorGroupId, objInputChangeOrderDetail.MachineErrorGroupId);
                objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorId, objInputChangeOrderDetail.MachineErrorId);
                objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorContent, objInputChangeOrderDetail.MachineErrorContent);
                //End
                objIData.AddParameter("@" + InputChangeOrderDetail.colInStockStatusID_in, objInputChangeOrderDetail.InStockStatusID_In);
                objIData.AddParameter("@" + InputChangeOrderDetail.colInStockStatusID_Out, objInputChangeOrderDetail.InStockStatusID_Out);
                objIData.AddParameter("@MACHINEERRORIDLIST", strMachineErrorIDList);
                objIData.AddParameter("@IsUpdateConcern", objInputChangeOrderDetail.IsUpdateConcern);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        //public void UpdateWithMachineError(IData objIData, InputChangeOrderDetail objInputChangeOrderDetail, string strMachineErrorIDList)
        //{
        //    try
        //    {
        //        objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDERDT_UPDNEW");
        //        objIData.AddParameter("@" + InputChangeOrderDetail.colInputChangeOrderDetailID, objInputChangeOrderDetail.InputChangeOrderDetailID);
        //        objIData.AddParameter("@" + InputChangeOrderDetail.colUpdatedUser, objInputChangeOrderDetail.UpdatedUser);
        //        //Nguyen Van Tai bo sung theo yeu cau cua BA Trung - 30/11/2017
        //        objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorGroupId, objInputChangeOrderDetail.MachineErrorGroupId);
        //        objIData.AddParameter("@" + InputChangeOrderDetail.colMachineErrorContent, objInputChangeOrderDetail.MachineErrorContent);
        //        objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
        //        objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
        //        objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
        //        objIData.AddParameter("@MACHINEERRORIDLIST", strMachineErrorIDList);
                
        //        //End
        //        objIData.ExecNonQuery();
        //    }
        //    catch (Exception objEx)
        //    {
        //        objIData.RollBackTransaction();
        //        throw (objEx);
        //    }
        //}
    }
}
