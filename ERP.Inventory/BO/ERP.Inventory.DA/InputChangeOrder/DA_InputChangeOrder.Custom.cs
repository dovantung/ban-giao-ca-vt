
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO;
using System.Linq;
#endregion
namespace ERP.Inventory.DA.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// Bảng yêu cầu nhập đổi/trả hàng
	/// </summary>	
	public partial class DA_InputChangeOrder
    {

        #region Methods			

        /// <summary>
        /// Tìm kiếm thông tin bảng yêu cầu nhập đổi/trả hàng
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InputChangeOrder.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin bảng yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage LoadOutputVoucherDetailForReturn(String strOutputVoucherID, int intInputChangeOrderTypeID, ref DataTable dtbData)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_InputChangOrde_GetLisReturn");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                objIData.AddParameter("@InputChangeOrderTypeID", intInputChangeOrderTypeID);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "OutputVoucherDetail";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy chi tiết phiếu xuất để nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> LoadOutputVoucherDetailForReturn", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage LoadInputChangeOrderType(String strOutputVoucherID, int intApplyErrorType, string strUserAction, ref DataTable dtbData)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_InputChangOrdeType_GetByOV");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                objIData.AddParameter("@ApplyErrorType", intApplyErrorType);
                objIData.AddParameter("@UserName", strUserAction);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "InputChangeOrderType";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp danh sách loại nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> LoadInputChangeOrderType", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private void InsertAttachment(IData objIData, string strInputChangeOrderID, DateTime dtmInputChangeOrderDate, string strUserName, List<BO.InputChangeOrder.InputChangeOrder_Attachment> lstQuitJobRequest_Attachment)
        {
            if (lstQuitJobRequest_Attachment == null)
                return;
            for (int i = 0; i < lstQuitJobRequest_Attachment.Count; i++)
            {
                if (lstQuitJobRequest_Attachment[i].IsAddNew)
                {
                    lstQuitJobRequest_Attachment[i].InputChangeOrderID = strInputChangeOrderID;
                    try
                    {
                        objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_ATTACH_ADD");
                        objIData.AddParameter("@AttachmentName", lstQuitJobRequest_Attachment[i].AttachmentName);
                        objIData.AddParameter("@AttachmentPath", lstQuitJobRequest_Attachment[i].AttachmentPath);
                        objIData.AddParameter("@CreatedUser", strUserName);
                        objIData.AddParameter("@Description", lstQuitJobRequest_Attachment[i].Description);
                        objIData.AddParameter("@InputChangeOrderID", strInputChangeOrderID);
                        objIData.AddParameter("@InputChangeOrderDate", dtmInputChangeOrderDate);
                        objIData.AddParameter("@FileID", lstQuitJobRequest_Attachment[i].FileID);
                        objIData.ExecNonQuery();
                    }
                    catch (Exception objEx)
                    {
                        objIData.RollBackTransaction();
                        throw (objEx);
                    }
                }
                else
                {
                    if (lstQuitJobRequest_Attachment[i].IsDeleted)
                    {
                        try
                        {
                            objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_ATTACH_DEL");
                            objIData.AddParameter("@AttachmentID", lstQuitJobRequest_Attachment[i].AttachmentID);
                            objIData.AddParameter("@DeletedUser", strUserName);
                            objIData.ExecNonQuery();
                        }
                        catch (Exception objEx)
                        {
                            objIData.RollBackTransaction();
                            throw (objEx);
                        }
                    }
                    else
                    {
                        try
                        {
                            objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_ATTACH_UPD");
                            objIData.AddParameter("@AttachmentID", lstQuitJobRequest_Attachment[i].AttachmentID);
                            objIData.AddParameter("@Description", lstQuitJobRequest_Attachment[i].Description);
                            objIData.ExecNonQuery();
                        }
                        catch (Exception objEx)
                        {
                            objIData.RollBackTransaction();
                            throw (objEx);
                        }
                    }
                }
            }
        }

        private string GetInputChangeOrderNewID(IData objIData, int intStoreID)
        {
            string strInputChangeNewID = string.Empty;
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strInputChangeNewID = objIData.ExecStoreToString();
                return strInputChangeNewID;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage CreateInputVoucherAndOutVoucher(string strActionUser, BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, BO.InputVoucher objInputVoucherBO, ERP.SalesAndServices.Payment.BO.Voucher objOutVoucher, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForSaleOrder, ref List<string> lstVATInvoiceList)
        {
            ResultMessage objResultMessage = CreateInputVoucherAndOutputVoucherAndInOutVoucher(strActionUser, objInputChangeOrder, objInputVoucherBO, null, objOutVoucher, objVoucherForSaleOrder, ref lstVATInvoiceList);
            return objResultMessage;
        }

        public ResultMessage CreateInputVoucherAndOutputVoucherAndInOutVoucher(string strActionUser, BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, BO.InputVoucher objInputVoucherBO, BO.OutputVoucher objOutputVoucherBO, ERP.SalesAndServices.Payment.BO.Voucher objInOutVoucher, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForSaleOrder, ref List<string> lstVATInvoiceList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            lstVATInvoiceList = new List<string>();
            string strSaleOrderID = objInputChangeOrder.SaleOrderID;
            List<OutputVoucher> lstOutputVoucherList = new List<OutputVoucher>();
            SalesAndServices.SaleOrders.DA.DA_SaleOrder objDA_SaleOrder = new SalesAndServices.SaleOrders.DA.DA_SaleOrder();
            string strNewVoucherForSaleOrder = string.Empty;
            string strNewOutputVoucherForSaleOrder = string.Empty;

            if (!string.IsNullOrEmpty(strSaleOrderID))
            {
                lstOutputVoucherList = objDA_SaleOrder.PrepareOutputVoucher(strSaleOrderID, objVoucherForSaleOrder.VoucherDetailList[0].PaymentCardSpend);
                if (lstOutputVoucherList.Count < 1)
                {
                    objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Không tạo được thông tin phiếu xuất", string.Empty);
                    return objResultMessage;
                }
            }

            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();

                if (objOutputVoucherBO != null)
                {
                    //Tạo phiếu xuất
                    objOutputVoucherBO.CreatedUser = strActionUser;
                    objDA_OutputVoucher.objLogObject = this.objLogObject;
                    objDA_OutputVoucher.Insert(objIData, objOutputVoucherBO);
                }
                else
                {
                    objOutputVoucherBO = new BO.OutputVoucher();

                    if (lstOutputVoucherList.Count > 0)
                    {
                        objVoucherForSaleOrder.VoucherDate = DateTime.Now;
                        objVoucherForSaleOrder.VoucherDetailList[0].VoucherDate = DateTime.Now;
                        objDA_SaleOrder.CreateVoucherForInputChange(objIData, objVoucherForSaleOrder, lstOutputVoucherList);
                        strNewVoucherForSaleOrder = objVoucherForSaleOrder.VoucherID;
                        strNewOutputVoucherForSaleOrder = lstOutputVoucherList[0].OutputVoucherID;
                    }
                }

                DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();

                //Tạo phiếu nhập
                objInputVoucherBO.CreatedUser = strActionUser;
                objDA_InputVoucher.objLogObject = this.objLogObject;
                objDA_InputVoucher.Insert(objIData, objInputVoucherBO);

                objInputChangeOrder.NewInputVoucherID = objInputVoucherBO.InputVoucherID;
                objInputChangeOrder.NewOutputVoucherID = objOutputVoucherBO.OutputVoucherID;

                DA.InputChange.DA_InputChange objDA_InputChange = new DA.InputChange.DA_InputChange();
                BO.InputChange.InputChange objInputChangeBO = new BO.InputChange.InputChange();
                objInputChangeBO.OldOutputVoucherID = objInputChangeOrder.OldOutputVoucherID;
                objInputChangeBO.NewInputVoucherID = objInputVoucherBO.InputVoucherID;
                objInputChangeBO.NewOutputVoucherID = objOutputVoucherBO.OutputVoucherID;
                objInputChangeBO.InputChangeStoreID = objInputChangeOrder.InputChangeOrderStoreID;
                objInputChangeBO.CreatedStoreID = objInputChangeOrder.InputChangeOrderStoreID;
                objInputChangeBO.CreatedUser = strActionUser;
                objInputChangeBO.InputChangeDate = DateTime.Now;

                if (!string.IsNullOrEmpty(objOutputVoucherBO.OutputVoucherID))
                {
                    //Lưu đồng bộ PM_InputChange
                    objInputChangeBO.InputChangeID = objDA_InputChange.GetInputChangeNewID(objIData, objInputChangeBO.CreatedStoreID);
                    objDA_InputChange.objLogObject = this.objLogObject;
                    objDA_InputChange.Insert(objIData, objInputChangeBO);
                }

                string strInputVoucherReturnID = string.Empty;
                if (objInputChangeOrder.InputVoucherReturnBO != null)
                {
                    objInputChangeOrder.InputVoucherReturnBO.InputVoucherID = objInputChangeOrder.NewInputVoucherID;
                    Input.DA_InputVoucherReturn objDA_InputVoucherReturn = new Input.DA_InputVoucherReturn();
                    objDA_InputVoucherReturn.Insert(objIData, objInputChangeOrder.InputVoucherReturnBO);
                    strInputVoucherReturnID = objInputChangeOrder.InputVoucherReturnBO.InputVoucherReturnID;
                }

                DA_InputChangeOrderDetail objDA_InputChangeOrderDetail = new DA_InputChangeOrderDetail();
                for (int i = 0; i < objInputChangeOrder.InputChangeOrderDetailList.Count; i++)
                {
                    BO.InputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail = objInputChangeOrder.InputChangeOrderDetailList[i];
                    //Cập nhật chi tiết nhập, xuất cho PM_InputchangeOrderDetail
                    objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_UPDDETAIL");
                    objIData.AddParameter("@InputChangeOrderDetailID", objInputChangeOrderDetail.InputChangeOrderDetailID);
                    objIData.AddParameter("@InputChangeID", objInputChangeBO.InputChangeID);
                    objIData.AddParameter("@InputChangeOrderID", objInputChangeOrder.InputChangeOrderID);
                    objIData.AddParameter("@InputVoucherReturnID", strInputVoucherReturnID);
                    objIData.AddParameter("@InputChangeOrderStoreID", objInputChangeOrder.InputChangeOrderStoreID);
                    objIData.AddParameter("@OutputVoucherID", objInputChangeOrder.NewOutputVoucherID);
                    objIData.AddParameter("@OutputVoucherIDSaleOrder", strNewOutputVoucherForSaleOrder);
                    objIData.AddParameter("@InputVoucherID", objInputChangeOrder.NewInputVoucherID);
                    objIData.AddParameter("@OldOutputVoucherDetailID", objInputChangeOrderDetail.OldOutputVoucherDetailID);
                    objIData.AddParameter("@ProductID_In", objInputChangeOrderDetail.ProductID_In);
                    objIData.AddParameter("@Quantity", objInputChangeOrderDetail.Quantity);
                    objIData.AddParameter("@IMEI_In", objInputChangeOrderDetail.IMEI_In);
                    objIData.AddParameter("@ProductID_Out", objInputChangeOrderDetail.ProductID_Out);
                    objIData.AddParameter("@IMEI_Out", objInputChangeOrderDetail.IMEI_Out);
                    objIData.AddParameter("@CreatedStoreID", objInputChangeOrder.InputChangeOrderStoreID);
                    objIData.AddParameter("@CreatedUser", strActionUser);
                    objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                    objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                    objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);

                    objIData.AddParameter("@Quantity_IN", objInputChangeOrderDetail.Quantity);
                    objIData.AddParameter("@IsNew_IN", objInputVoucherBO.InputVoucherDetailList[0].IsNew);
                    objIData.AddParameter("@InputTypeID_IN", objInputVoucherBO.InputTypeID);
                    objIData.AddParameter("@CustomerID_IN", objInputVoucherBO.CustomerID);
                    //29/12/2016 Thiên sửa lấy giá, phí theo Chi tiết yêu cầu Nhập đổi/trả
                    objIData.AddParameter("@InputPrice_IN", objInputChangeOrderDetail.InputPrice);
                    objIData.AddParameter("@ReturnFee", objInputChangeOrderDetail.ReturnFee);
                    for (int j = 0; j < objInputVoucherBO.InputVoucherDetailList.Count; j++)
                    {
                        if (objInputVoucherBO.InputVoucherDetailList[j].ProductID == objInputChangeOrderDetail.ProductID_In)
                        {
                            //objIData.AddParameter("@InputPrice_IN", objInputVoucherBO.InputVoucherDetailList[j].InputPrice);
                            //objIData.AddParameter("@ReturnFee", objInputVoucherBO.InputVoucherDetailList[j].ReturnFee);
                            objIData.AddParameter("@ENDWarrantyDate_IN", objInputVoucherBO.InputVoucherDetailList[j].ENDWarrantyDate);
                            for (int k = 0; k < objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList.Count; k++)
                            {
                                if (objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList[k].IMEI == objInputChangeOrderDetail.IMEI_In)
                                {
                                    objIData.AddParameter("@IsHasWarranty_IN", objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList[k].IsHasWarranty);
                                    break;
                                }
                            }
                            break;
                        }
                    }

                    objIData.AddParameter("@OutputTypeID_Out", objInputChangeOrderDetail.OutputTypeID);

                    objIData.AddParameter("@Quantity_Out", objInputChangeOrderDetail.Quantity);
                    if (objOutputVoucherBO.OutputVoucherDetailList != null && objOutputVoucherBO.OutputVoucherDetailList.Count > 0)
                    {
                        objIData.AddParameter("@IsNew_Out", objOutputVoucherBO.OutputVoucherDetailList[0].IsNew);

                        for (int j = 0; j < objOutputVoucherBO.OutputVoucherDetailList.Count; j++)
                        {
                            if (objOutputVoucherBO.OutputVoucherDetailList[j].ProductID == objInputChangeOrderDetail.ProductID_Out)
                            {
                                objIData.AddParameter("@SalePrice_Out", objOutputVoucherBO.OutputVoucherDetailList[j].SalePrice);
                                break;
                            }
                        }
                    }
                    else
                    {
                        objIData.AddParameter("@IsNew_Out", objInputVoucherBO.InputVoucherDetailList[0].IsNew);
                        objIData.AddParameter("@SalePrice_Out", 0);
                    }
                    objIData.AddParameter("@INSTOCKSTATUSID_IN", objInputChangeOrderDetail.InStockStatusID_In);
                    objIData.AddParameter("@INSTOCKSTATUSID_OUT", objInputChangeOrderDetail.InStockStatusID_Out);
                    objIData.ExecNonQuery();
                }

                //Tạo phiếu chi hoặc thu
                if (objInOutVoucher != null && objInOutVoucher.VoucherTypeID > 0)
                {
                    objInOutVoucher.VoucherDate = DateTime.Now;
                    //Nếu là phiếu chi
                    if (objInOutVoucher.IsSpend)
                        objInOutVoucher.VoucherConcern = objInputVoucherBO.InputVoucherID;
                    else
                        objInOutVoucher.VoucherConcern = objOutputVoucherBO.OutputVoucherID;

                    ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new SalesAndServices.Payment.DA.DA_Voucher();
                    objDA_Voucher.objLogObject = this.objLogObject;

                    if (objInOutVoucher.VoucherDetailList.Count > 0)
                    {
                        if (objInOutVoucher.VoucherDetailList[0].VNDCash == 0 && objInOutVoucher.VoucherDetailList[0].ForeignCash == 0
                            && objInOutVoucher.VoucherDetailList[0].PaymentCardAmount == 0 && objInOutVoucher.VoucherDetailList[0].ForeignCashExchange == 0)
                        {
                            objInOutVoucher.VoucherDetailList = new List<SalesAndServices.Payment.BO.VoucherDetail>();
                        }
                        objDA_Voucher.Insert(objIData, objInOutVoucher);
                    }
                    else
                    {
                        if (objInputChangeOrder.IsInputReturn && objInputChangeOrder.IsDebt && objInputChangeOrder.IsNotCreateOutVoucherDetail)
                        {
                            //vẫn tạo phiếu chi nếu là yêu cầu trả hàng được tạo từ đơn hàng trả góp và được đánh dấu ko tạo chi tiết phiếu chi từ y/c trả hàng
                            objDA_Voucher.Insert(objIData, objInOutVoucher);
                        }
                    }

                    ERP.SalesAndServices.Payment.DA.DA_VoucherDetail objDA_VoucherDetail = new SalesAndServices.Payment.DA.DA_VoucherDetail();
                    if (objInOutVoucher.VoucherDetailList.Count > 0 && objInOutVoucher.VoucherID != string.Empty)
                    {
                        foreach (var objVoucherDetail in objInOutVoucher.VoucherDetailList)
                        {
                            //ERP.SalesAndServices.Payment.BO.VoucherDetail objVoucherDetail = objInOutVoucher.VoucherDetailList[0];
                            objVoucherDetail.VoucherID = objInOutVoucher.VoucherID;
                            if (objVoucherDetail.VNDCash != 0 || objVoucherDetail.ForeignCash != 0 || objVoucherDetail.PaymentCardAmount != 0 || objVoucherDetail.ForeignCashExchange != 0)
                            {
                                objVoucherDetail.VoucherDate = DateTime.Now;
                                objDA_VoucherDetail.objLogObject = this.objLogObject;
                                objDA_VoucherDetail.Insert(objIData, objVoucherDetail);
                            }
                        }
                    }

                    //cập nhật phiếu
                    objIData.CreateNewStoredProcedure("PM_INPUCHANORDE_UPDInOutVouche");
                    objIData.AddParameter("@InputChangeOrderID", objInputChangeOrder.InputChangeOrderID);
                    //Nếu là phiếu chi
                    if (objInOutVoucher.IsSpend)
                    {
                        objIData.AddParameter("@InVoucherID", strNewVoucherForSaleOrder);
                        objIData.AddParameter("@OutVoucherID", objInOutVoucher.VoucherID);
                    }
                    else
                    {
                        objIData.AddParameter("@InVoucherID", objInOutVoucher.VoucherID);
                        objIData.AddParameter("@OutVoucherID", string.Empty);
                    }
                    objIData.AddParameter("@InputVoucherReturnID", strInputVoucherReturnID);
                    objIData.ExecNonQuery();
                }

                if (!string.IsNullOrWhiteSpace(objInputChangeOrder.InputChangeOrderID))
                {
                    MasterData.BO.MD.InputChangeOrderType objInputChangeOrderType = new MasterData.BO.MD.InputChangeOrderType();
                    new ERP.MasterData.DA.MD.DA_InputChangeOrderType().LoadInfo(ref objInputChangeOrderType, objInputChangeOrder.InputChangeOrderTypeID);
                    if (objInputChangeOrderType.IsAutoCreateVatInvoice == 1)
                    {
                        string strVATInvoiceID = string.Empty;
                        string strVATPInvoiceID = string.Empty;
                        PrintVAT.DA.PrintVAT.DA_VAT_Invoice objDA_VAT_Invoice = new PrintVAT.DA.PrintVAT.DA_VAT_Invoice();
                        var objResultMessageVAT = objDA_VAT_Invoice.CreateVATInvoiceInputChange(objIData, objInputChangeOrder.InputChangeOrderID, ref strVATInvoiceID, ref strVATPInvoiceID);
                        if (objResultMessageVAT.IsError)
                        {
                            objIData.RollBackTransaction();
                            ErrorLog.Add(objIData, objResultMessageVAT.Message, objResultMessageVAT.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceInputChange", "PrintVAT.DA.PrintVAT");
                            return objResultMessageVAT;
                        }
                        if (!string.IsNullOrWhiteSpace(strVATPInvoiceID))
                            lstVATInvoiceList.Add(strVATPInvoiceID);
                        if (!string.IsNullOrWhiteSpace(strVATInvoiceID))
                            lstVATInvoiceList.Add(strVATInvoiceID);
                    }
                }
                else
                {
                    objIData.RollBackTransaction();
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> CreateInputVoucherAndOutputVoucherAndInOutVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            if (lstOutputVoucherList != null) // Đăng bổ sung
            {
                ERP.SalesAndServices.SaleOrders.DA.DA_Privilege objDA_Privilege = new ERP.SalesAndServices.SaleOrders.DA.DA_Privilege();
                foreach (OutputVoucher objOutputVoucher in lstOutputVoucherList)
                {
                    if (objOutputVoucher.OutputVoucherDetailList != null && objOutputVoucher.OutputVoucherDetailList.Count > 0)
                    {
                        List<OutputVoucherDetail> lstOutputVoucherDetail = objOutputVoucher.OutputVoucherDetailList.FindAll(x => x.ApplySaleOrderDetailID != string.Empty);
                        if (lstOutputVoucherDetail != null)
                        {
                            foreach (OutputVoucherDetail objOutputVoucherDetail in lstOutputVoucherDetail)
                            {
                                objDA_Privilege.Update(objOutputVoucherDetail.ApplySaleOrderDetailID, objOutputVoucherDetail.OutputVoucherDetailID);
                            }
                        }

                    }
                }
            }
            return objResultMessage;
        }

        public ResultMessage CreateInputVoucherAndOutputVoucherAndInOutVoucherNew(string strActionUser, BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, BO.InputVoucher objInputVoucherBO, BO.OutputVoucher objOutputVoucherBO, ERP.SalesAndServices.Payment.BO.Voucher objInOutVoucher, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForReturnFee, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForSaleOrder, ref List<string> lstVATInvoiceList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            lstVATInvoiceList = new List<string>();
            string strSaleOrderID = objInputChangeOrder.SaleOrderID;
            List<OutputVoucher> lstOutputVoucherList = new List<OutputVoucher>();
            SalesAndServices.SaleOrders.DA.DA_SaleOrder objDA_SaleOrder = new SalesAndServices.SaleOrders.DA.DA_SaleOrder();
            string strNewVoucherForSaleOrder = string.Empty;
            string strNewOutputVoucherForSaleOrder = string.Empty;

            if (!string.IsNullOrEmpty(strSaleOrderID))
            {
                lstOutputVoucherList = objDA_SaleOrder.PrepareOutputVoucher(strSaleOrderID, objVoucherForSaleOrder.VoucherDetailList[0].PaymentCardSpend);
                if (lstOutputVoucherList.Count < 1)
                {
                    objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Không tạo được thông tin phiếu xuất", string.Empty);
                    return objResultMessage;
                }
            }

            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();

                if (objOutputVoucherBO != null)
                {
                    //Tạo phiếu xuất
                    objOutputVoucherBO.CreatedUser = strActionUser;
                    objDA_OutputVoucher.objLogObject = this.objLogObject;
                    objDA_OutputVoucher.Insert(objIData, objOutputVoucherBO);
                }
                else
                {
                    objOutputVoucherBO = new BO.OutputVoucher();

                    if (lstOutputVoucherList.Count > 0)
                    {
                        objVoucherForSaleOrder.VoucherDate = DateTime.Now;
                        objVoucherForSaleOrder.VoucherDetailList[0].VoucherDate = DateTime.Now;
                        objDA_SaleOrder.CreateVoucherForInputChange(objIData, objVoucherForSaleOrder, lstOutputVoucherList);
                        strNewVoucherForSaleOrder = objVoucherForSaleOrder.VoucherID;
                        strNewOutputVoucherForSaleOrder = lstOutputVoucherList[0].OutputVoucherID;
                    }
                }

                DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();

                //Tạo phiếu nhập
                objInputVoucherBO.CreatedUser = strActionUser;
                objDA_InputVoucher.objLogObject = this.objLogObject;
                objDA_InputVoucher.Insert(objIData, objInputVoucherBO);

                objInputChangeOrder.NewInputVoucherID = objInputVoucherBO.InputVoucherID;
                objInputChangeOrder.NewOutputVoucherID = objOutputVoucherBO.OutputVoucherID;

                DA.InputChange.DA_InputChange objDA_InputChange = new DA.InputChange.DA_InputChange();
                BO.InputChange.InputChange objInputChangeBO = new BO.InputChange.InputChange();
                objInputChangeBO.OldOutputVoucherID = objInputChangeOrder.OldOutputVoucherID;
                objInputChangeBO.NewInputVoucherID = objInputVoucherBO.InputVoucherID;
                objInputChangeBO.NewOutputVoucherID = objOutputVoucherBO.OutputVoucherID;
                objInputChangeBO.InputChangeStoreID = objInputChangeOrder.InputChangeOrderStoreID;
                objInputChangeBO.CreatedStoreID = objInputChangeOrder.InputChangeOrderStoreID;
                objInputChangeBO.CreatedUser = strActionUser;
                objInputChangeBO.InputChangeDate = DateTime.Now;

                if (!string.IsNullOrEmpty(objOutputVoucherBO.OutputVoucherID))
                {
                    //Lưu đồng bộ PM_InputChange
                    objInputChangeBO.InputChangeID = objDA_InputChange.GetInputChangeNewID(objIData, objInputChangeBO.CreatedStoreID);
                    objDA_InputChange.objLogObject = this.objLogObject;
                    objDA_InputChange.Insert(objIData, objInputChangeBO);
                }

                string strInputVoucherReturnID = string.Empty;
                if (objInputChangeOrder.InputVoucherReturnBO != null)
                {
                    objInputChangeOrder.InputVoucherReturnBO.InputVoucherID = objInputChangeOrder.NewInputVoucherID;
                    Input.DA_InputVoucherReturn objDA_InputVoucherReturn = new Input.DA_InputVoucherReturn();
                    objDA_InputVoucherReturn.Insert(objIData, objInputChangeOrder.InputVoucherReturnBO);
                    strInputVoucherReturnID = objInputChangeOrder.InputVoucherReturnBO.InputVoucherReturnID;
                }

                DA_InputChangeOrderDetail objDA_InputChangeOrderDetail = new DA_InputChangeOrderDetail();
                for (int i = 0; i < objInputChangeOrder.InputChangeOrderDetailList.Count; i++)
                {
                    BO.InputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail = objInputChangeOrder.InputChangeOrderDetailList[i];
                    //Cập nhật chi tiết nhập, xuất cho PM_InputchangeOrderDetail
                    objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_UPDDETAIL");
                    objIData.AddParameter("@InputChangeOrderDetailID", objInputChangeOrderDetail.InputChangeOrderDetailID);
                    objIData.AddParameter("@InputChangeID", objInputChangeBO.InputChangeID);
                    objIData.AddParameter("@InputChangeOrderID", objInputChangeOrder.InputChangeOrderID);
                    objIData.AddParameter("@InputVoucherReturnID", strInputVoucherReturnID);
                    objIData.AddParameter("@InputChangeOrderStoreID", objInputChangeOrder.InputChangeOrderStoreID);
                    objIData.AddParameter("@OutputVoucherID", objInputChangeOrder.NewOutputVoucherID);
                    objIData.AddParameter("@OutputVoucherIDSaleOrder", strNewOutputVoucherForSaleOrder);
                    objIData.AddParameter("@InputVoucherID", objInputChangeOrder.NewInputVoucherID);
                    objIData.AddParameter("@OldOutputVoucherDetailID", objInputChangeOrderDetail.OldOutputVoucherDetailID);
                    objIData.AddParameter("@ProductID_In", objInputChangeOrderDetail.ProductID_In);
                    objIData.AddParameter("@Quantity", objInputChangeOrderDetail.Quantity);
                    objIData.AddParameter("@IMEI_In", objInputChangeOrderDetail.IMEI_In);
                    objIData.AddParameter("@ProductID_Out", objInputChangeOrderDetail.ProductID_Out);
                    objIData.AddParameter("@IMEI_Out", objInputChangeOrderDetail.IMEI_Out);
                    objIData.AddParameter("@CreatedStoreID", objInputChangeOrder.InputChangeOrderStoreID);
                    objIData.AddParameter("@CreatedUser", strActionUser);
                    objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                    objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                    objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);

                    objIData.AddParameter("@Quantity_IN", objInputChangeOrderDetail.Quantity);
                    objIData.AddParameter("@IsNew_IN", objInputVoucherBO.InputVoucherDetailList[0].IsNew);
                    objIData.AddParameter("@InputTypeID_IN", objInputVoucherBO.InputTypeID);
                    objIData.AddParameter("@CustomerID_IN", objInputVoucherBO.CustomerID);
                    //29/12/2016 Thiên sửa lấy giá, phí theo Chi tiết yêu cầu Nhập đổi/trả
                    objIData.AddParameter("@InputPrice_IN", objInputChangeOrderDetail.InputPrice);
                    objIData.AddParameter("@ReturnFee", objInputChangeOrderDetail.ReturnFee);
                    for (int j = 0; j < objInputVoucherBO.InputVoucherDetailList.Count; j++)
                    {
                        if (objInputVoucherBO.InputVoucherDetailList[j].ProductID == objInputChangeOrderDetail.ProductID_In)
                        {
                            //objIData.AddParameter("@InputPrice_IN", objInputVoucherBO.InputVoucherDetailList[j].InputPrice);
                            //objIData.AddParameter("@ReturnFee", objInputVoucherBO.InputVoucherDetailList[j].ReturnFee);

                            if (objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList.Count < 1)
                            {
                                objIData.AddParameter("@Note", objInputVoucherBO.InputVoucherDetailList[j].Note);
                            }

                            objIData.AddParameter("@ENDWarrantyDate_IN", objInputVoucherBO.InputVoucherDetailList[j].ENDWarrantyDate);
                            for (int k = 0; k < objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList.Count; k++)
                            {
                                if (objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList[k].IMEI == objInputChangeOrderDetail.IMEI_In)
                                {
                                    objIData.AddParameter("@Note", objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList[k].Note);
                                    objIData.AddParameter("@IsHasWarranty_IN", objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList[k].IsHasWarranty);
                                    break;
                                }
                            }
                            break;
                        }
                    }

                    objIData.AddParameter("@OutputTypeID_Out", objInputChangeOrderDetail.OutputTypeID);

                    objIData.AddParameter("@Quantity_Out", objInputChangeOrderDetail.Quantity);
                    if (objOutputVoucherBO.OutputVoucherDetailList != null && objOutputVoucherBO.OutputVoucherDetailList.Count > 0)
                    {
                        objIData.AddParameter("@IsNew_Out", objOutputVoucherBO.OutputVoucherDetailList[0].IsNew);

                        for (int j = 0; j < objOutputVoucherBO.OutputVoucherDetailList.Count; j++)
                        {
                            if (objOutputVoucherBO.OutputVoucherDetailList[j].ProductID == objInputChangeOrderDetail.ProductID_Out)
                            {
                                objIData.AddParameter("@SalePrice_Out", objOutputVoucherBO.OutputVoucherDetailList[j].SalePrice);
                                break;
                            }
                        }
                    }
                    else
                    {
                        objIData.AddParameter("@IsNew_Out", objInputVoucherBO.InputVoucherDetailList[0].IsNew);
                        objIData.AddParameter("@SalePrice_Out", 0);
                    }
                    objIData.AddParameter("@INSTOCKSTATUSID_IN", objInputChangeOrderDetail.InStockStatusID_In);
                    objIData.AddParameter("@INSTOCKSTATUSID_OUT", objInputChangeOrderDetail.InStockStatusID_Out);
                    objIData.ExecNonQuery();
                }

                //Tạo phiếu chi hoặc thu
                if (objInOutVoucher != null && objInOutVoucher.VoucherTypeID > 0)
                {
                    objInOutVoucher.VoucherDate = DateTime.Now;
                    //Nếu là phiếu chi
                    if (objInOutVoucher.IsSpend)
                        objInOutVoucher.VoucherConcern = objInputVoucherBO.InputVoucherID;
                    else
                    {

                        objInOutVoucher.VoucherConcern = objOutputVoucherBO.OutputVoucherID;
                    }

                    ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new SalesAndServices.Payment.DA.DA_Voucher();
                    objDA_Voucher.objLogObject = this.objLogObject;

                    if (objInOutVoucher.VoucherDetailList.Count > 0)
                    {
                        if (objInOutVoucher.VoucherDetailList[0].VNDCash == 0 && objInOutVoucher.VoucherDetailList[0].ForeignCash == 0
                            && objInOutVoucher.VoucherDetailList[0].PaymentCardAmount == 0 && objInOutVoucher.VoucherDetailList[0].ForeignCashExchange == 0)
                        {
                            objInOutVoucher.VoucherDetailList = new List<SalesAndServices.Payment.BO.VoucherDetail>();
                        }
                        objDA_Voucher.Insert(objIData, objInOutVoucher);
                    }
                    else
                    {
                        if (objInputChangeOrder.IsInputReturn && objInputChangeOrder.IsDebt && objInputChangeOrder.IsNotCreateOutVoucherDetail)
                        {
                            //vẫn tạo phiếu chi nếu là yêu cầu trả hàng được tạo từ đơn hàng trả góp và được đánh dấu ko tạo chi tiết phiếu chi từ y/c trả hàng
                            objDA_Voucher.Insert(objIData, objInOutVoucher);
                        }
                    }

                    ERP.SalesAndServices.Payment.DA.DA_VoucherDetail objDA_VoucherDetail = new SalesAndServices.Payment.DA.DA_VoucherDetail();
                    if (objInOutVoucher.VoucherDetailList.Count > 0 && objInOutVoucher.VoucherID != string.Empty)
                    {
                        foreach (var objVoucherDetail in objInOutVoucher.VoucherDetailList)
                        {
                            //ERP.SalesAndServices.Payment.BO.VoucherDetail objVoucherDetail = objInOutVoucher.VoucherDetailList[0];
                            objVoucherDetail.VoucherID = objInOutVoucher.VoucherID;
                            if (objVoucherDetail.VNDCash != 0 || objVoucherDetail.ForeignCash != 0 || objVoucherDetail.PaymentCardAmount != 0 || objVoucherDetail.ForeignCashExchange != 0)
                            {
                                objVoucherDetail.VoucherDate = DateTime.Now;
                                objDA_VoucherDetail.objLogObject = this.objLogObject;
                                objDA_VoucherDetail.Insert(objIData, objVoucherDetail);
                            }
                        }
                    }

                    //cập nhật phiếu
                    objIData.CreateNewStoredProcedure("PM_INPUCHANORDE_UPDInOutVouche");
                    objIData.AddParameter("@InputChangeOrderID", objInputChangeOrder.InputChangeOrderID);
                    //Nếu là phiếu chi
                    if (objInOutVoucher.IsSpend)
                    {
                        objIData.AddParameter("@InVoucherID", strNewVoucherForSaleOrder);
                        objIData.AddParameter("@OutVoucherID", objInOutVoucher.VoucherID);
                    }
                    else
                    {
                        objIData.AddParameter("@InVoucherID", objInOutVoucher.VoucherID);
                        objIData.AddParameter("@OutVoucherID", string.Empty);
                    }
                    objIData.AddParameter("@InputVoucherReturnID", strInputVoucherReturnID);
                    objIData.ExecNonQuery();
                }
                //Tạo phiếu thu phí đổi trả
                if (objVoucherForReturnFee != null && objVoucherForReturnFee.VoucherTypeID > 0)
                {
                    objVoucherForReturnFee.VoucherDate = DateTime.Now;

                    //cần xem lại
                    //  objVoucherForReturnFee.VoucherConcern = objOutputVoucherBO.OutputVoucherID;

                    ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new SalesAndServices.Payment.DA.DA_Voucher();
                    objDA_Voucher.objLogObject = this.objLogObject;

                    if (objVoucherForReturnFee.VoucherDetailList.Count > 0)
                    {
                        if (objVoucherForReturnFee.VoucherDetailList[0].VNDCash == 0 && objVoucherForReturnFee.VoucherDetailList[0].ForeignCash == 0
                            && objVoucherForReturnFee.VoucherDetailList[0].PaymentCardAmount == 0 && objVoucherForReturnFee.VoucherDetailList[0].ForeignCashExchange == 0)
                        {
                            objVoucherForReturnFee.VoucherDetailList = new List<SalesAndServices.Payment.BO.VoucherDetail>();
                        }
                        objDA_Voucher.Insert(objIData, objVoucherForReturnFee);
                    }
                    //else
                    //{
                    //    if (objInputChangeOrder.IsInputReturn && objInputChangeOrder.IsDebt && objInputChangeOrder.IsNotCreateOutVoucherDetail)
                    //    {
                    //        //vẫn tạo phiếu chi nếu là yêu cầu trả hàng được tạo từ đơn hàng trả góp và được đánh dấu ko tạo chi tiết phiếu chi từ y/c trả hàng
                    //        objDA_Voucher.Insert(objIData, objInOutVoucher);
                    //    }
                    //}

                    ERP.SalesAndServices.Payment.DA.DA_VoucherDetail objDA_VoucherDetail = new SalesAndServices.Payment.DA.DA_VoucherDetail();
                    if (objVoucherForReturnFee.VoucherDetailList.Count > 0 && objVoucherForReturnFee.VoucherID != string.Empty)
                    {
                        foreach (var objVoucherReturnFeeDetail in objVoucherForReturnFee.VoucherDetailList)
                        {
                            //ERP.SalesAndServices.Payment.BO.VoucherDetail objVoucherDetail = objInOutVoucher.VoucherDetailList[0];
                            objVoucherReturnFeeDetail.VoucherID = objVoucherForReturnFee.VoucherID;
                            if (objVoucherReturnFeeDetail.VNDCash != 0 || objVoucherReturnFeeDetail.ForeignCash != 0 || objVoucherReturnFeeDetail.PaymentCardAmount != 0 || objVoucherReturnFeeDetail.ForeignCashExchange != 0)
                            {
                                objVoucherReturnFeeDetail.VoucherDate = DateTime.Now;
                                objDA_VoucherDetail.objLogObject = this.objLogObject;
                                objDA_VoucherDetail.Insert(objIData, objVoucherReturnFeeDetail);
                            }
                        }
                    }

                    //cập nhật phiếu
                    //objIData.CreateNewStoredProcedure("PM_INPUCHANORDE_UPDInOutVouche");
                    //objIData.AddParameter("@InputChangeOrderID", objInputChangeOrder.InputChangeOrderID);
                    ////Nếu là phiếu chi
                    //if (objInOutVoucher.IsSpend)
                    //{
                    //    objIData.AddParameter("@InVoucherID", strNewVoucherForSaleOrder);
                    //    objIData.AddParameter("@OutVoucherID", objInOutVoucher.VoucherID);
                    //}
                    //else
                    //{
                    //    objIData.AddParameter("@InVoucherID", objVoucherForReturnFee.VoucherID);
                    //    objIData.AddParameter("@OutVoucherID", string.Empty);
                    //}
                    //objIData.AddParameter("@InputVoucherReturnID", strInputVoucherReturnID);
                    //objIData.ExecNonQuery();
                }
                if (!string.IsNullOrWhiteSpace(objInputChangeOrder.InputChangeOrderID))
                {
                    MasterData.BO.MD.InputChangeOrderType objInputChangeOrderType = new MasterData.BO.MD.InputChangeOrderType();
                    new ERP.MasterData.DA.MD.DA_InputChangeOrderType().LoadInfo(ref objInputChangeOrderType, objInputChangeOrder.InputChangeOrderTypeID);
                    if (objInputChangeOrderType.IsAutoCreateVatInvoice == 1)
                    {
                        string strVATInvoiceID = string.Empty;
                        string strVATPInvoiceID = string.Empty;
                        PrintVAT.DA.PrintVAT.DA_VAT_Invoice objDA_VAT_Invoice = new PrintVAT.DA.PrintVAT.DA_VAT_Invoice();
                        var objResultMessageVAT = objDA_VAT_Invoice.CreateVATInvoiceInputChange(objIData, objInputChangeOrder.InputChangeOrderID, ref strVATInvoiceID, ref strVATPInvoiceID);
                        if (objResultMessageVAT.IsError)
                        {
                            objIData.RollBackTransaction();
                            ErrorLog.Add(objIData, objResultMessageVAT.Message, objResultMessageVAT.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceInputChange", "PrintVAT.DA.PrintVAT");
                            return objResultMessageVAT;
                        }
                        if (!string.IsNullOrWhiteSpace(strVATPInvoiceID))
                            lstVATInvoiceList.Add(strVATPInvoiceID);
                        if (!string.IsNullOrWhiteSpace(strVATInvoiceID))
                            lstVATInvoiceList.Add(strVATInvoiceID);
                    }
                }
                else
                {
                    objIData.RollBackTransaction();
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> CreateInputVoucherAndOutputVoucherAndInOutVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            if (lstOutputVoucherList != null) // Đăng bổ sung
            {
                ERP.SalesAndServices.SaleOrders.DA.DA_Privilege objDA_Privilege = new ERP.SalesAndServices.SaleOrders.DA.DA_Privilege();
                foreach (OutputVoucher objOutputVoucher in lstOutputVoucherList)
                {
                    if (objOutputVoucher.OutputVoucherDetailList != null && objOutputVoucher.OutputVoucherDetailList.Count > 0)
                    {
                        List<OutputVoucherDetail> lstOutputVoucherDetail = objOutputVoucher.OutputVoucherDetailList.FindAll(x => x.ApplySaleOrderDetailID != string.Empty);
                        if (lstOutputVoucherDetail != null)
                        {
                            foreach (OutputVoucherDetail objOutputVoucherDetail in lstOutputVoucherDetail)
                            {
                                objDA_Privilege.Update(objOutputVoucherDetail.ApplySaleOrderDetailID, objOutputVoucherDetail.OutputVoucherDetailID);
                            }
                        }

                    }
                }
            }
            return objResultMessage;
        }

        public ResultMessage CreateInputVoucherAndOutVoucherViettel(string strActionUser, BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, BO.InputVoucher objInputVoucherBO, ERP.SalesAndServices.Payment.BO.Voucher objOutVoucher, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForReturnFee, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForSaleOrder, ref string strInputVoucherReturnID, ref List<string> lstVATInvoiceList)
        {
            ResultMessage objResultMessage = CreateInputVoucherAndOutputVoucherAndInOutVoucherViettel(strActionUser, objInputChangeOrder, objInputVoucherBO, null, objOutVoucher, objVoucherForReturnFee, objVoucherForSaleOrder, ref strInputVoucherReturnID, ref lstVATInvoiceList);
            return objResultMessage;
        }

        public ResultMessage CreateInputVoucherAndOutputVoucherAndInOutVoucherViettel(string strActionUser, BO.InputChangeOrder.InputChangeOrder objInputChangeOrder, BO.InputVoucher objInputVoucherBO, BO.OutputVoucher objOutputVoucherBO, ERP.SalesAndServices.Payment.BO.Voucher objInOutVoucher, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForReturnFee, ERP.SalesAndServices.Payment.BO.Voucher objVoucherForSaleOrder, ref string strInputVoucherReturnID, ref List<string> lstVATInvoiceList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            lstVATInvoiceList = new List<string>();
            string strSaleOrderID = objInputChangeOrder.SaleOrderID;
            List<OutputVoucher> lstOutputVoucherList = new List<OutputVoucher>();
            SalesAndServices.SaleOrders.DA.DA_SaleOrder objDA_SaleOrder = new SalesAndServices.SaleOrders.DA.DA_SaleOrder();
            string strNewVoucherForSaleOrder = string.Empty;
            string strNewOutputVoucherForSaleOrder = string.Empty;
            string strNewOutputVoucherDetailIdForSaleOrder = string.Empty;
            if (!string.IsNullOrEmpty(strSaleOrderID))
            {
                lstOutputVoucherList = objDA_SaleOrder.PrepareOutputVoucher(strSaleOrderID, objVoucherForSaleOrder.VoucherDetailList[0].PaymentCardSpend);
                if (lstOutputVoucherList.Count < 1)
                {
                    objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Không tạo được thông tin phiếu xuất", string.Empty);
                    return objResultMessage;
                }
            }

            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                MasterData.BO.MD.SaleOrderType objSaleOrderType = null;
                MasterData.BO.MD.StoreChangeType objStoreChangeType = null;

                DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();
                OutputVoucher objOutputVoucher_Temp = new OutputVoucher();

                string strStoreChangeID = string.Empty;
                if (objOutputVoucherBO != null)
                {
                    //Tạo phiếu xuất
                    objOutputVoucherBO.CreatedUser = strActionUser;
                    objDA_OutputVoucher.objLogObject = this.objLogObject;
                    objDA_OutputVoucher.Insert(objIData, objOutputVoucherBO);
                }
                else
                {
                    objOutputVoucherBO = new BO.OutputVoucher();

                    if (lstOutputVoucherList.Count > 0)
                    {
                        objVoucherForSaleOrder.VoucherDate = DateTime.Now;
                        objVoucherForSaleOrder.VoucherDetailList[0].VoucherDate = DateTime.Now;
                        objDA_SaleOrder.CreateVoucherForInputChange(objIData, objVoucherForSaleOrder, lstOutputVoucherList, strSaleOrderID);
                        strNewVoucherForSaleOrder = objVoucherForSaleOrder.VoucherID;
                        strNewOutputVoucherForSaleOrder = lstOutputVoucherList[0].OutputVoucherID;
                        var OutputVoucherDetail = lstOutputVoucherList[0].OutputVoucherDetailList.Where(x => x.IsPromotionProduct == false).SingleOrDefault();
                        if (OutputVoucherDetail != null)
                            strNewOutputVoucherDetailIdForSaleOrder = OutputVoucherDetail.OutputVoucherDetailID;
                        objOutputVoucher_Temp = lstOutputVoucherList[0];
                        int intSaleOrderTypeID = 0;
                        objDA_SaleOrder.GetSaleorderTypeID(objIData, objVoucherForSaleOrder.VoucherConcern, ref intSaleOrderTypeID);

                        new ERP.MasterData.DA.MD.DA_SaleOrderType().LoadInfo(objIData, ref objSaleOrderType, intSaleOrderTypeID, false);

                        if (objSaleOrderType != null && objSaleOrderType.StoreChangeTypeID > 0)
                        {
                            new ERP.MasterData.DA.MD.DA_StoreChangeType().LoadInfo(objIData, ref objStoreChangeType, objSaleOrderType.StoreChangeTypeID);

                            strStoreChangeID = new DA_StoreChange().CreateStoreChangeID(ref objResultMessage, objIData, objOutputVoucher_Temp.CreatedStoreID);
                        }
                    }
                }

                DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();

                //Tạo phiếu nhập
                objInputVoucherBO.CreatedUser = strActionUser;
                objDA_InputVoucher.objLogObject = this.objLogObject;
                if (objStoreChangeType != null)
                {
                    objInputVoucherBO.InputStoreID = objSaleOrderType.InvoiceStoreID;
                }
                objDA_InputVoucher.Insert(objIData, objInputVoucherBO);

                objInputChangeOrder.NewInputVoucherID = objInputVoucherBO.InputVoucherID;
                objInputChangeOrder.NewOutputVoucherID = objOutputVoucherBO.OutputVoucherID;

                DA.InputChange.DA_InputChange objDA_InputChange = new DA.InputChange.DA_InputChange();
                BO.InputChange.InputChange objInputChangeBO = new BO.InputChange.InputChange();
                objInputChangeBO.OldOutputVoucherID = objInputChangeOrder.OldOutputVoucherID;
                objInputChangeBO.NewInputVoucherID = objInputVoucherBO.InputVoucherID;
                objInputChangeBO.NewOutputVoucherID = string.IsNullOrEmpty(objOutputVoucherBO.OutputVoucherID) ? strNewOutputVoucherForSaleOrder : objOutputVoucherBO.OutputVoucherID;
                objInputChangeBO.InputChangeStoreID = objInputChangeOrder.InputChangeOrderStoreID;
                objInputChangeBO.CreatedStoreID = objInputChangeOrder.InputChangeOrderStoreID;
                objInputChangeBO.CreatedUser = strActionUser;
                objInputChangeBO.InputChangeDate = DateTime.Now;

                if (!string.IsNullOrEmpty(objOutputVoucherBO.OutputVoucherID) || !string.IsNullOrEmpty(strNewOutputVoucherForSaleOrder))
                {
                    //Lưu đồng bộ PM_InputChange
                    objInputChangeBO.InputChangeID = objDA_InputChange.GetInputChangeNewID(objIData, objInputChangeBO.CreatedStoreID);
                    objDA_InputChange.objLogObject = this.objLogObject;
                    objDA_InputChange.Insert(objIData, objInputChangeBO);
                }

                strInputVoucherReturnID = string.Empty;
                if (objInputChangeOrder.InputVoucherReturnBO != null)
                {
                    objInputChangeOrder.InputVoucherReturnBO.InputVoucherID = objInputChangeOrder.NewInputVoucherID;
                    Input.DA_InputVoucherReturn objDA_InputVoucherReturn = new Input.DA_InputVoucherReturn();
                    objDA_InputVoucherReturn.Insert(objIData, objInputChangeOrder.InputVoucherReturnBO);
                    strInputVoucherReturnID = objInputChangeOrder.InputVoucherReturnBO.InputVoucherReturnID;
                }
                string strInputVoucherID = objInputVoucherBO.InputVoucherID;
                string strVoucherConcern = objInputVoucherBO.InputVoucherID;

                bool bolIsCreateMaster = true;
                DA_InputChangeOrderDetail objDA_InputChangeOrderDetail = new DA_InputChangeOrderDetail();
                for (int i = 0; i < objInputChangeOrder.InputChangeOrderDetailList.Count; i++)
                {
                    if (objStoreChangeType != null)
                    {
                    }

                    BO.InputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail = objInputChangeOrder.InputChangeOrderDetailList[i];
                    //Cập nhật chi tiết nhập, xuất cho PM_InputchangeOrderDetail
                    objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_UPDDETAIL");
                    objIData.AddParameter("@InputChangeOrderDetailID", objInputChangeOrderDetail.InputChangeOrderDetailID);
                    objIData.AddParameter("@InputChangeID", objInputChangeBO.InputChangeID);
                    objIData.AddParameter("@InputChangeOrderID", objInputChangeOrder.InputChangeOrderID);
                    objIData.AddParameter("@InputVoucherReturnID", strInputVoucherReturnID);

                    if (objStoreChangeType != null)
                        objIData.AddParameter("@InputChangeOrderStoreID", objSaleOrderType.InvoiceStoreID);
                    else
                        objIData.AddParameter("@InputChangeOrderStoreID", objInputChangeOrder.InputChangeOrderStoreID);
                    objIData.AddParameter("@OutputVoucherID", objInputChangeOrder.NewOutputVoucherID);
                    objIData.AddParameter("@OutputVoucherIDSaleOrder", strNewOutputVoucherForSaleOrder);
                    objIData.AddParameter("@NewOutDetailForSaleOrder", strNewOutputVoucherDetailIdForSaleOrder);
                    objIData.AddParameter("@InputVoucherID", objInputChangeOrder.NewInputVoucherID);
                    objIData.AddParameter("@OldOutputVoucherDetailID", objInputChangeOrderDetail.OldOutputVoucherDetailID);
                    objIData.AddParameter("@ProductID_In", objInputChangeOrderDetail.ProductID_In);
                    objIData.AddParameter("@Quantity", objInputChangeOrderDetail.Quantity);
                    objIData.AddParameter("@IMEI_In", objInputChangeOrderDetail.IMEI_In);
                    objIData.AddParameter("@ProductID_Out", objInputChangeOrderDetail.ProductID_Out);
                    objIData.AddParameter("@IMEI_Out", objInputChangeOrderDetail.IMEI_Out);
                    objIData.AddParameter("@CreatedStoreID", objInputChangeOrder.InputChangeOrderStoreID);
                    objIData.AddParameter("@CreatedUser", strActionUser);
                    objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                    objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                    objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);

                    objIData.AddParameter("@Quantity_IN", objInputChangeOrderDetail.Quantity);
                    objIData.AddParameter("@IsNew_IN", objInputVoucherBO.InputVoucherDetailList[0].IsNew);
                    objIData.AddParameter("@InputTypeID_IN", objInputVoucherBO.InputTypeID);
                    objIData.AddParameter("@CustomerID_IN", objInputVoucherBO.CustomerID);
                    //29/12/2016 Thiên sửa lấy giá, phí theo Chi tiết yêu cầu Nhập đổi/trả
                    objIData.AddParameter("@InputPrice_IN", objInputChangeOrderDetail.InputPrice);
                    objIData.AddParameter("@ReturnFee", objInputChangeOrderDetail.ReturnFee);
                    for (int j = 0; j < objInputVoucherBO.InputVoucherDetailList.Count; j++)
                    {
                        if (objInputVoucherBO.InputVoucherDetailList[j].ProductID == objInputChangeOrderDetail.ProductID_In)
                        {
                            //objIData.AddParameter("@InputPrice_IN", objInputVoucherBO.InputVoucherDetailList[j].InputPrice);
                            //objIData.AddParameter("@ReturnFee", objInputVoucherBO.InputVoucherDetailList[j].ReturnFee);

                            objIData.AddParameter("@ENDWarrantyDate_IN", objInputVoucherBO.InputVoucherDetailList[j].ENDWarrantyDate);
                            if (objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList.Count < 1)
                            {
                                objIData.AddParameter("@Note", objInputVoucherBO.InputVoucherDetailList[j].Note);
                            }
                            for (int k = 0; k < objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList.Count; k++)
                            {
                                if (objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList[k].IMEI == objInputChangeOrderDetail.IMEI_In)
                                {
                                    objIData.AddParameter("@Note", objInputVoucherBO.InputVoucherDetailList[j].Note);
                                    objIData.AddParameter("@IsHasWarranty_IN", objInputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList[k].IsHasWarranty);
                                    break;
                                }
                            }
                            break;
                        }
                    }

                    objIData.AddParameter("@OutputTypeID_Out", objInputChangeOrderDetail.OutputTypeID);

                    objIData.AddParameter("@Quantity_Out", objInputChangeOrderDetail.Quantity);
                    if (objOutputVoucherBO.OutputVoucherDetailList != null && objOutputVoucherBO.OutputVoucherDetailList.Count > 0)
                    {
                        objIData.AddParameter("@IsNew_Out", objOutputVoucherBO.OutputVoucherDetailList[0].IsNew);

                        for (int j = 0; j < objOutputVoucherBO.OutputVoucherDetailList.Count; j++)
                        {
                            if (objOutputVoucherBO.OutputVoucherDetailList[j].ProductID == objInputChangeOrderDetail.ProductID_Out)
                            {
                                objIData.AddParameter("@SalePrice_Out", objOutputVoucherBO.OutputVoucherDetailList[j].SalePrice);
                                break;
                            }
                        }
                    }
                    else
                    {
                        objIData.AddParameter("@IsNew_Out", objInputVoucherBO.InputVoucherDetailList[0].IsNew);
                        objIData.AddParameter("@SalePrice_Out", 0);
                    }
                    objIData.AddParameter("@INSTOCKSTATUSID_IN", objInputChangeOrderDetail.InStockStatusID_In);
                    objIData.AddParameter("@INSTOCKSTATUSID_OUT", objInputChangeOrderDetail.InStockStatusID_Out);

                    objIData.ExecNonQuery();
                    #region Cập nhật nợ khuyến mãi

                    objIData.CreateNewStoredProcedure("SM_CONTROLPROMOTIONUPDBYRETURN");
                    objIData.AddParameter("@OLDOUTPUTVOUCHERDETAILID", objInputChangeOrderDetail.OldOutputVoucherDetailID);
                    objIData.AddParameter("@QUANTITY", objInputChangeOrderDetail.Quantity);
                    objIData.ExecNonQuery();

                    #endregion

                    #region Tạo xuất chuyển kho về kho siêu thị PVI
                    if (objStoreChangeType != null)
                    {
                        objOutputVoucher_Temp.OutputStoreID = objSaleOrderType.InvoiceStoreID;
                        objOutputVoucher_Temp.TotalAmount = objInputVoucherBO.TotalAmount;
                        objOutputVoucher_Temp.TotalAmountBFT = objInputVoucherBO.TotalAmountBFT;
                        objOutputVoucher_Temp.TotalVAT = objInputVoucherBO.TotalVAT;
                        objOutputVoucher_Temp.TotalCardSpend = 0;
                        objOutputVoucher_Temp.IsStoreChange = true;
                        objOutputVoucher_Temp.OutputDate = ((DateTime)objInputVoucherBO.InputDate).AddSeconds(1);
                        CreateInputVoucherPVI(objIData, objOutputVoucher_Temp, objInputChangeOrder.InputChangeOrderStoreID, objInputVoucherBO, objStoreChangeType,
                            bolIsCreateMaster, objInputChangeOrderDetail.InputChangeOrderDetailID, objInputChangeOrderDetail.ProductID_In, strStoreChangeID);
                        bolIsCreateMaster = false;

                    }
                    #endregion

                }

                if(objStoreChangeType!=null)
                {
                    objInputVoucherBO.InputVoucherID = strInputVoucherID;
                }

                #region Tạo phiếu chi hoặc thu

                if (objInOutVoucher != null && objInOutVoucher.VoucherTypeID > 0)
                {
                    objInOutVoucher.VoucherDate = DateTime.Now;
                    //Nếu là phiếu chi
                    if (objInOutVoucher.IsSpend)
                        objInOutVoucher.VoucherConcern = objInputVoucherBO.InputVoucherID;
                    else
                    {

                        objInOutVoucher.VoucherConcern = objOutputVoucherBO.OutputVoucherID;
                    }
                    ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new SalesAndServices.Payment.DA.DA_Voucher();
                    objDA_Voucher.objLogObject = this.objLogObject;

                    if (objInOutVoucher.VoucherDetailList.Count > 0)
                    {
                        if (objInOutVoucher.VoucherDetailList[0].VNDCash == 0 && objInOutVoucher.VoucherDetailList[0].ForeignCash == 0
                            && objInOutVoucher.VoucherDetailList[0].PaymentCardAmount == 0 && objInOutVoucher.VoucherDetailList[0].ForeignCashExchange == 0)
                        {
                            objInOutVoucher.VoucherDetailList = new List<SalesAndServices.Payment.BO.VoucherDetail>();
                        }
                        objDA_Voucher.Insert(objIData, objInOutVoucher);
                    }
                    else
                    {
                        if (objInputChangeOrder.IsInputReturn && objInputChangeOrder.IsDebt && objInputChangeOrder.IsNotCreateOutVoucherDetail)
                        {
                            //vẫn tạo phiếu chi nếu là yêu cầu trả hàng được tạo từ đơn hàng trả góp và được đánh dấu ko tạo chi tiết phiếu chi từ y/c trả hàng
                            objDA_Voucher.Insert(objIData, objInOutVoucher);
                        }
                    }

                    ERP.SalesAndServices.Payment.DA.DA_VoucherDetail objDA_VoucherDetail = new SalesAndServices.Payment.DA.DA_VoucherDetail();
                    if (objInOutVoucher.VoucherDetailList.Count > 0 && objInOutVoucher.VoucherID != string.Empty)
                    {
                        foreach (var objVoucherDetail in objInOutVoucher.VoucherDetailList)
                        {
                            //ERP.SalesAndServices.Payment.BO.VoucherDetail objVoucherDetail = objInOutVoucher.VoucherDetailList[0];
                            objVoucherDetail.VoucherID = objInOutVoucher.VoucherID;
                            if (objVoucherDetail.VNDCash != 0 || objVoucherDetail.ForeignCash != 0 || objVoucherDetail.PaymentCardAmount != 0 || objVoucherDetail.ForeignCashExchange != 0)
                            {
                                objVoucherDetail.VoucherDate = DateTime.Now;
                                objDA_VoucherDetail.objLogObject = this.objLogObject;
                                objDA_VoucherDetail.Insert(objIData, objVoucherDetail);
                            }
                        }
                    }

                    //cập nhật phiếu
                    objIData.CreateNewStoredProcedure("PM_INPUCHANORDE_UPDInOutVouche");
                    objIData.AddParameter("@InputChangeOrderID", objInputChangeOrder.InputChangeOrderID);
                    //Nếu là phiếu chi
                    if (objInOutVoucher.IsSpend)
                    {
                        objIData.AddParameter("@InVoucherID", strNewVoucherForSaleOrder);
                        objIData.AddParameter("@OutVoucherID", objInOutVoucher.VoucherID);
                    }
                    else
                    {
                        objIData.AddParameter("@InVoucherID", objInOutVoucher.VoucherID);
                        objIData.AddParameter("@OutVoucherID", string.Empty);
                    }
                    objIData.AddParameter("@InputVoucherReturnID", strInputVoucherReturnID);
                    objIData.ExecNonQuery();
                }
                #endregion
                //Tạo phiếu thu phí đổi trả
                if (objVoucherForReturnFee != null && objVoucherForReturnFee.VoucherTypeID > 0)
                {
                    objVoucherForReturnFee.VoucherDate = DateTime.Now;

                    //cần xem lại
                    // objVoucherForReturnFee.VoucherConcern = objOutputVoucherBO.OutputVoucherID;

                    ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new SalesAndServices.Payment.DA.DA_Voucher();
                    objDA_Voucher.objLogObject = this.objLogObject;

                    //if (objVoucherForReturnFee.VoucherDetailList.Count > 0)
                    //{
                    //    if (objVoucherForReturnFee.VoucherDetailList[0].VNDCash == 0 && objVoucherForReturnFee.VoucherDetailList[0].ForeignCash == 0
                    //        && objVoucherForReturnFee.VoucherDetailList[0].PaymentCardAmount == 0 && objVoucherForReturnFee.VoucherDetailList[0].ForeignCashExchange == 0)
                    //    {
                    //        objVoucherForReturnFee.VoucherDetailList = new List<SalesAndServices.Payment.BO.VoucherDetail>();
                    //   }
                    objDA_Voucher.Insert(objIData, objVoucherForReturnFee);
                    //   }
                    //else
                    //{
                    //    if (objInputChangeOrder.IsInputReturn && objInputChangeOrder.IsDebt && objInputChangeOrder.IsNotCreateOutVoucherDetail)
                    //    {
                    //        //vẫn tạo phiếu chi nếu là yêu cầu trả hàng được tạo từ đơn hàng trả góp và được đánh dấu ko tạo chi tiết phiếu chi từ y/c trả hàng
                    //        objDA_Voucher.Insert(objIData, objInOutVoucher);
                    //    }
                    //}

                    ERP.SalesAndServices.Payment.DA.DA_VoucherDetail objDA_VoucherDetail = new SalesAndServices.Payment.DA.DA_VoucherDetail();
                    if (objVoucherForReturnFee.VoucherDetailList.Count > 0 && objVoucherForReturnFee.VoucherID != string.Empty)
                    {
                        foreach (var objVoucherReturnFeeDetail in objVoucherForReturnFee.VoucherDetailList)
                        {
                            //ERP.SalesAndServices.Payment.BO.VoucherDetail objVoucherDetail = objInOutVoucher.VoucherDetailList[0];
                            objVoucherReturnFeeDetail.VoucherID = objVoucherForReturnFee.VoucherID;
                            if (objVoucherReturnFeeDetail.VNDCash != 0 || objVoucherReturnFeeDetail.ForeignCash != 0 || objVoucherReturnFeeDetail.PaymentCardAmount != 0 || objVoucherReturnFeeDetail.ForeignCashExchange != 0)
                            {
                                objVoucherReturnFeeDetail.VoucherDate = DateTime.Now;
                                objDA_VoucherDetail.objLogObject = this.objLogObject;
                                objDA_VoucherDetail.Insert(objIData, objVoucherReturnFeeDetail);
                            }
                        }
                    }

                    //cập nhật phiếu
                    //objIData.CreateNewStoredProcedure("PM_INPUCHANORDE_UPDInOutVouche");
                    //objIData.AddParameter("@InputChangeOrderID", objInputChangeOrder.InputChangeOrderID);
                    //////Nếu là phiếu chi
                    ////if (objInOutVoucher.IsSpend)
                    ////{
                    ////    objIData.AddParameter("@InVoucherID", strNewVoucherForSaleOrder);
                    ////    objIData.AddParameter("@OutVoucherID", objInOutVoucher.VoucherID);
                    ////}
                    ////else
                    ////{
                    //objIData.AddParameter("@InVoucherID", objVoucherForReturnFee.VoucherID);
                    //objIData.AddParameter("@OutVoucherID", string.Empty);
                    //// }
                    //objIData.AddParameter("@InputVoucherReturnID", strInputVoucherReturnID);
                    //objIData.ExecNonQuery();
                }
                // tạo hóa đơn tự động
                if (!string.IsNullOrWhiteSpace(objInputChangeOrder.InputChangeOrderID))
                {
                    MasterData.BO.MD.InputChangeOrderType objInputChangeOrderType = new MasterData.BO.MD.InputChangeOrderType();
                    new ERP.MasterData.DA.MD.DA_InputChangeOrderType().LoadInfo(ref objInputChangeOrderType, objInputChangeOrder.InputChangeOrderTypeID);
                    if (objInputChangeOrderType.IsAutoCreateVatInvoice == 1)
                    {
                        string strVATInvoiceID = string.Empty;
                        string strVATPInvoiceID = string.Empty;
                        PrintVAT.DA.PrintVAT.DA_VAT_Invoice objDA_VAT_Invoice = new PrintVAT.DA.PrintVAT.DA_VAT_Invoice();
                        var objResultMessageVAT = objDA_VAT_Invoice.CreateVATInvoiceInputChange(objIData, objInputChangeOrder.InputChangeOrderID, ref strVATInvoiceID, ref strVATPInvoiceID);
                        if (objResultMessageVAT.IsError)
                        {
                            objIData.RollBackTransaction();
                            ErrorLog.Add(objIData, objResultMessageVAT.Message, objResultMessageVAT.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceInputChange", "PrintVAT.DA.PrintVAT");
                            return objResultMessageVAT;
                        }
                        if (!string.IsNullOrWhiteSpace(strVATPInvoiceID))
                            lstVATInvoiceList.Add(strVATPInvoiceID);
                        if (!string.IsNullOrWhiteSpace(strVATInvoiceID))
                            lstVATInvoiceList.Add(strVATInvoiceID);
                    }
                }
                else
                {
                    objIData.RollBackTransaction();
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> CreateInputVoucherAndOutputVoucherAndInOutVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetDetail(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_InputChangeOrderDetail_Get");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "InputChangeOrderDetail";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy chi tiết yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetDetail", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetDetail(ref DataTable dtbData, string strOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_InputChangOrdeDetai_GetByOV");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "InputChangeOrderDetail";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy chi tiết yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetDetail", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetAttachment(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_InputChangeOrder_Attach_Get");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "AttachmentList";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy tập tin đính kèm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetAttachment", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetComment(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_InputChangeOrderComment_GET");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "CommentList";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy bình luận yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetComment", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetWorkFlow(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_InputChangeOrder_WFlow_Get");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "WorkFlowList";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy qui trình yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetWorkFlow", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetWorkFlow(ref DataTable dtbData, int intInputChangeOrderType)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_InputChangeOrderType_WF_Sel");
                objIData.AddParameter("@InputChangeOrderTypeID", intInputChangeOrderType);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "WorkFlowList";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy qui trình yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetWorkFlow", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetReviewList(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_RVL_GET");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "ReviewList";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy mức duyệt", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetReviewList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetReviewList(ref DataTable dtbData, int intInputChangeOrderType)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_InputChangeOrderType_RL_Sel");
                objIData.AddParameter("@InputChangeOrderTypeID", intInputChangeOrderType);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "ReviewList";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy mức duyệt", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetReviewList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CheckDelete(ref bool bolIsDeleted, string strInputVoucherID, string strVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDER_CHKDEL");
                objIData.AddParameter("@InputVoucherID", strInputVoucherID);
                objIData.AddParameter("@VoucherID", strVoucherID);
                bolIsDeleted = (objIData.ExecStoreToString() == "1" ? true : false);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi kiểm tra xóa chứng từ liên quan: phiếu nhập, phiếu chi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> CheckDelete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateCare(ref int intResult, string strOutputVoucherDetailID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_WARRANTYEXTEND_UPDCARE");
                objIData.AddParameter("@OutputVoucherDetailID", strOutputVoucherDetailID);
                intResult = Convert.ToInt32(objIData.ExecStoreToString());
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi hỗ trợ trả Care", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> UpdateCare", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void InsertMachineError(IData objIData, string strInputChangeOrderID, int intMachineErrorID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDTL_ERRORINS");
                objIData.AddParameter("@INPUTCHANGEORDERDETAILID", strInputChangeOrderID);
                objIData.AddParameter("@MACHINEERRORID", intMachineErrorID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage GetListMachineError(ref DataTable dtbData, string strInputChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTCHANGEORDTL_ERRORGET");
                objIData.AddParameter("@INPUTCHANGEORDERID", strInputChangeOrderID);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "MachineErrorDataTable";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy chi tiết yêu cầu nhập đổi/trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetListMachineError", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetListIMEIInEvictionIMEI(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTCHANGE_IMEI");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "EvictionIMEI";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy danh sách IMEI thu hồi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetListIMEIInEvictionIMEI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        #region 24/06/2019 Đăng bổ sung

        /// <summary>
        /// Tạo phiếu xuất PVI - Dang bo sung
        /// Lấy về chi tiết phiếu xuất ( có bảo hành mở rộng)
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="objOutputVoucher"></param>
        /// <param name="lstOutputVoucherDetail_WarExt"></param>
        public void CreateInputVoucherPVI(IData objIData,
            OutputVoucher objOutputVoucher,
            int intInputChangeStoreID,
            InputVoucher objInputVoucher,
            MasterData.BO.MD.StoreChangeType objStoreChangeType,
            bool bolIsCreateMaster,
            string strInputChangeOrderDetailID,
            string strProductID_In,
            string strStoreChangeID)
        {
            try
            {
                DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();
                DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();

                if (bolIsCreateMaster)
                {
                    #region Tạo chuyển kho master
                    ResultMessage objResultMessage = new ResultMessage();
                    //tạo phiếu xuất
                    objOutputVoucher.OrderID = string.Empty;
                    objDA_OutputVoucher.Insert(objIData, objOutputVoucher);

                    //Tạo phiếu nhập
                    string strContentInput = "MPX: " + objOutputVoucher.OutputVoucherID + " - Nội dung: xuất chuyển kho khác kho tạo";
                    objInputVoucher.InputTypeID = Convert.ToInt32(objStoreChangeType.InputTypeID);
                    objInputVoucher.InputStoreID = intInputChangeStoreID;
                    objInputVoucher.Content = strContentInput;
                    objInputVoucher.ISStoreChange = true;

                    objInputVoucher.InputDate = ((DateTime)objInputVoucher.InputDate).AddSeconds(2);
                    objDA_InputVoucher.Insert(objIData, objInputVoucher);

                    string strContentOutput = "MPN: " + objInputVoucher.InputVoucherID + " - Nội dung: xuất chuyển kho khác kho tạo";
                    objIData.CreateNewStoredProcedure("ERP.PM_OUTPUTVOUCHER_UPDCONTENT");
                    objIData.AddParameter("@OutputVoucherID", objOutputVoucher.OutputVoucherID);
                    objIData.AddParameter("@OutputContent", strContentOutput);
                    objIData.ExecNonQuery();

                    // Tạo phiếu xuất chuyển kho
                    decimal decQuantity = objOutputVoucher.OutputVoucherDetailList.Sum(x => x.Quantity);
                    InsertStoreChange(objIData, strStoreChangeID, objOutputVoucher, objStoreChangeType, objInputVoucher.InputVoucherID,
                        intInputChangeStoreID, decQuantity);
                    #endregion
                }


                #region thêm chi tiết phiếu nhập chuyen kho
                foreach (BO.InputVoucherDetail item in objInputVoucher.InputVoucherDetailList)
                {
                    if (item.ProductID.Trim() == strProductID_In.Trim())
                    {
                        item.InputStoreID = objInputVoucher.InputStoreID;
                        item.CreatedUser = objInputVoucher.CreatedUser;
                        item.CreatedStoreID = objInputVoucher.CreatedStoreID;
                        item.CostPrice = 0;
                        item.FirstPrice = 0;
                        item.ORIGINALInputPrice = 0;
                        item.InputPrice = 0;
                        item.InputDate = ((DateTime)objInputVoucher.InputDate).AddSeconds(2);
                        objDA_InputVoucher.Insert(objIData, item, objInputVoucher.InputVoucherID, objOutputVoucher.OutputVoucherID, strStoreChangeID,
                            Convert.ToInt32(objStoreChangeType.OutputTypeID), objOutputVoucher.OutputStoreID, strInputChangeOrderDetailID);

                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw ex;
            }
        }

        private void InsertStoreChange(IData objIData, string strStoreChangeID, OutputVoucher objOutputVoucher,
            MasterData.BO.MD.StoreChangeType objStoreChangeType,
            string strInputVoucherID, int intInvoiceStoreID, decimal decTotalQuantity)
        {
            try
            {

                objIData.CreateNewStoredProcedure("PM_STORECHANGE_ADD");
                objIData.AddParameter("@STORECHANGEID", strStoreChangeID);
                objIData.AddParameter("@STORECHANGEORDERID", "");
                objIData.AddParameter("@FROMSTOREID", objOutputVoucher.OutputStoreID);
                objIData.AddParameter("@TOSTOREID", intInvoiceStoreID);
                objIData.AddParameter("@TRANSPORTTYPEID", 1);
                objIData.AddParameter("@STORECHANGETYPEID", objStoreChangeType.StoreChangeTypeID);
                objIData.AddParameter("@CONTENT", objOutputVoucher.OutputNote);
                objIData.AddParameter("@USERCREATE", objOutputVoucher.CreatedUser);
                objIData.AddParameter("@STORECHANGEDATE", objOutputVoucher.OutputDate);
                objIData.AddParameter("@OUTPUTVOUCHERID", objOutputVoucher.OutputVoucherID);
                objIData.AddParameter("@INPUTVOUCHERID", strInputVoucherID);
                objIData.AddParameter("@STORECHANGEUSER", objOutputVoucher.CreatedUser);
                objIData.AddParameter("@CreatedStoreID", objOutputVoucher.CreatedStoreID);
                objIData.AddParameter("@IsNew", 1);
                objIData.AddParameter("@INSTOCKSTATUSID", 1);
                objIData.AddParameter("@InvoiceID", "");
                objIData.AddParameter("@TotalQuantity", decTotalQuantity);
                objIData.AddParameter("@ISRECEIVE", 1);
                objIData.AddParameter("@InVoucherID", string.Empty);
                objIData.AddParameter("@OutVoucherID", string.Empty);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@InvoiceSymbol", objOutputVoucher.InvoiceSymbol);
                objIData.AddParameter("@IsCheckRealInput", 0);
                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw ex;
            }
        }

        #endregion
        #endregion

    }
}
