﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.DataAccess;
using Library.WebCore;
using StoreChangeOrderAttachment = ERP.Inventory.BO.StoreChangeOrderAttachment;

namespace ERP.Inventory.DA
{
    public partial class DA_StoreChangeOrderAttachment
    {
        #region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objStoreChangeOrder_AttachMENT">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref StoreChangeOrderAttachment objStoreChangeOrderAttachMENT, string strAttachMENTID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colAttachMENTID, strAttachMENTID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
                    objStoreChangeOrderAttachMENT = new StoreChangeOrderAttachment();
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colAttachMENTID])) objStoreChangeOrderAttachMENT.AttachMENTID = Convert.ToString(reader[StoreChangeOrderAttachment.colAttachMENTID]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colStoreChangeOrderID])) objStoreChangeOrderAttachMENT.StoreChangeOrderID = Convert.ToString(reader[StoreChangeOrderAttachment.colStoreChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colAttachMENTName])) objStoreChangeOrderAttachMENT.AttachMENTName = Convert.ToString(reader[StoreChangeOrderAttachment.colAttachMENTName]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colAttachMENTPath])) objStoreChangeOrderAttachMENT.AttachMENTPath = Convert.ToString(reader[StoreChangeOrderAttachment.colAttachMENTPath]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colFileID])) objStoreChangeOrderAttachMENT.FileID = Convert.ToString(reader[StoreChangeOrderAttachment.colFileID]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colDescription])) objStoreChangeOrderAttachMENT.Description = Convert.ToString(reader[StoreChangeOrderAttachment.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colCreatedUser])) objStoreChangeOrderAttachMENT.CreatedUser = Convert.ToString(reader[StoreChangeOrderAttachment.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colCreatedDate])) objStoreChangeOrderAttachMENT.CreatedDate = Convert.ToDateTime(reader[StoreChangeOrderAttachment.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colIsDeleted])) objStoreChangeOrderAttachMENT.IsDeleted = Convert.ToBoolean(reader[StoreChangeOrderAttachment.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colDeletedUser])) objStoreChangeOrderAttachMENT.DeletedUser = Convert.ToString(reader[StoreChangeOrderAttachment.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colDeletedDate])) objStoreChangeOrderAttachMENT.DeletedDate = Convert.ToDateTime(reader[StoreChangeOrderAttachment.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrderAttachment.colVOFFICEID])) objStoreChangeOrderAttachMENT.VOFFICEID = Convert.ToString(reader[StoreChangeOrderAttachment.colVOFFICEID]).Trim();
 				}
 				else
 				{
                    objStoreChangeOrderAttachMENT = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder_AttachMENT -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objStoreChangeOrder_AttachMENT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(StoreChangeOrderAttachment objStoreChangeOrderAttachMENT)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                Insert(objIData,objStoreChangeOrderAttachMENT);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder_AttachMENT -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrder_AttachMENT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData,StoreChangeOrderAttachment objStoreChangeOrder_AttachMENT)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colAttachMENTID, objStoreChangeOrder_AttachMENT.AttachMENTID);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colStoreChangeOrderID, objStoreChangeOrder_AttachMENT.StoreChangeOrderID);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colAttachMENTName, objStoreChangeOrder_AttachMENT.AttachMENTName);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colAttachMENTPath, objStoreChangeOrder_AttachMENT.AttachMENTPath);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colFileID, objStoreChangeOrder_AttachMENT.FileID);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colDescription, objStoreChangeOrder_AttachMENT.Description);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colCreatedUser, objStoreChangeOrder_AttachMENT.CreatedUser);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colVOFFICEID, objStoreChangeOrder_AttachMENT.VOFFICEID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objStoreChangeOrder_AttachMENT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Update(StoreChangeOrderAttachment objStoreChangeOrder_AttachMENT)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objStoreChangeOrder_AttachMENT);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder_AttachMENT -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrder_AttachMENT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, StoreChangeOrderAttachment objStoreChangeOrder_AttachMENT)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colAttachMENTID, objStoreChangeOrder_AttachMENT.AttachMENTID);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colStoreChangeOrderID, objStoreChangeOrder_AttachMENT.StoreChangeOrderID);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colAttachMENTName, objStoreChangeOrder_AttachMENT.AttachMENTName);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colAttachMENTPath, objStoreChangeOrder_AttachMENT.AttachMENTPath);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colFileID, objStoreChangeOrder_AttachMENT.FileID);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colDescription, objStoreChangeOrder_AttachMENT.Description);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colVOFFICEID, objStoreChangeOrder_AttachMENT.VOFFICEID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objStoreChangeOrder_AttachMENT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(StoreChangeOrderAttachment objStoreChangeOrder_AttachMENT)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objStoreChangeOrder_AttachMENT);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder_AttachMENT -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrder_AttachMENT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, StoreChangeOrderAttachment objStoreChangeOrder_AttachMENT)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colAttachMENTID, objStoreChangeOrder_AttachMENT.AttachMENTID);
                objIData.AddParameter("@" + StoreChangeOrderAttachment.colDeletedUser, objStoreChangeOrder_AttachMENT.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

        public DA_StoreChangeOrderAttachment()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_STORECHANGEORDER_ATM_ADD";
        public const String SP_UPDATE = "PM_STORECHANGEORDER_ATM_UPD";
        public const String SP_DELETE = "PM_STORECHANGEORDER_ATM_DEL";
        public const String SP_SELECT = "PM_STORECHANGEORDER_ATM_SEL";
        public const String SP_SEARCH = "PM_STORECHANGEORDER_ATM_SRH";
        public const String SP_UPDATEINDEX = "PM_STORECHANGEORDER_ATM_UPDINDEX";
		#endregion
    }
}
