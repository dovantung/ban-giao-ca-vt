
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputVoucher_Attachment= ERP.Inventory.BO.InputVoucher_Attachment;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// Danh sách file đính kèm của phiếu nhập
	/// </summary>	
	public partial class DA_InputVoucher_Attachment
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách file đính kèm của phiếu nhập
		/// </summary>
		/// <param name="objInputVoucher_Attachment">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InputVoucher_Attachment objInputVoucher_Attachment, string strAttachMENTID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InputVoucher_Attachment.colAttachMENTID, strAttachMENTID);
				IDataReader reader = objIData.ExecStoreToDataReader();
 				if (objInputVoucher_Attachment == null) 
 					objInputVoucher_Attachment = new InputVoucher_Attachment();
				if (reader.Read())
 				{
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colAttachMENTID])) objInputVoucher_Attachment.AttachmentID = Convert.ToString(reader[InputVoucher_Attachment.colAttachMENTID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colInputVoucherID])) objInputVoucher_Attachment.InputVoucherID = Convert.ToString(reader[InputVoucher_Attachment.colInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colDescription])) objInputVoucher_Attachment.Description = Convert.ToString(reader[InputVoucher_Attachment.colDescription]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colFilePath])) objInputVoucher_Attachment.FilePath = Convert.ToString(reader[InputVoucher_Attachment.colFilePath]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colFileName])) objInputVoucher_Attachment.FileName = Convert.ToString(reader[InputVoucher_Attachment.colFileName]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colCreatedUser])) objInputVoucher_Attachment.CreatedUser = Convert.ToString(reader[InputVoucher_Attachment.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colCreatedDate])) objInputVoucher_Attachment.CreatedDate = Convert.ToDateTime(reader[InputVoucher_Attachment.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colIsDeleted])) objInputVoucher_Attachment.IsDeleted = Convert.ToBoolean(reader[InputVoucher_Attachment.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colDeletedUser])) objInputVoucher_Attachment.DeletedUser = Convert.ToString(reader[InputVoucher_Attachment.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher_Attachment.colDeletedDate])) objInputVoucher_Attachment.DeletedDate = Convert.ToDateTime(reader[InputVoucher_Attachment.colDeletedDate]);
                    if (!Convert.IsDBNull(reader["FileID"])) objInputVoucher_Attachment.FileID = Convert.ToString(reader["FileID"]);
 					objInputVoucher_Attachment.IsExist = true;
 				}
 				else
 				{
 					objInputVoucher_Attachment.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách file đính kèm của phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher_Attachment -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách file đính kèm của phiếu nhập
		/// </summary>
		/// <param name="objInputVoucher_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InputVoucher_Attachment objInputVoucher_Attachment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInputVoucher_Attachment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách file đính kèm của phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher_Attachment -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách file đính kèm của phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucher_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InputVoucher_Attachment objInputVoucher_Attachment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InputVoucher_Attachment.colAttachMENTID, objInputVoucher_Attachment.AttachmentID);
				objIData.AddParameter("@" + InputVoucher_Attachment.colInputVoucherID, objInputVoucher_Attachment.InputVoucherID);
				objIData.AddParameter("@" + InputVoucher_Attachment.colDescription, objInputVoucher_Attachment.Description);
				objIData.AddParameter("@" + InputVoucher_Attachment.colFilePath, objInputVoucher_Attachment.FilePath);
				objIData.AddParameter("@" + InputVoucher_Attachment.colFileName, objInputVoucher_Attachment.FileName);
				objIData.AddParameter("@" + InputVoucher_Attachment.colCreatedUser, objInputVoucher_Attachment.CreatedUser);
                objIData.AddParameter("@FileID", objInputVoucher_Attachment.FileID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin danh sách file đính kèm của phiếu nhập
		/// </summary>
		/// <param name="objInputVoucher_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InputVoucher_Attachment objInputVoucher_Attachment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInputVoucher_Attachment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách file đính kèm của phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher_Attachment -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách file đính kèm của phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucher_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InputVoucher_Attachment objInputVoucher_Attachment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InputVoucher_Attachment.colAttachMENTID, objInputVoucher_Attachment.AttachmentID);
				objIData.AddParameter("@" + InputVoucher_Attachment.colInputVoucherID, objInputVoucher_Attachment.InputVoucherID);
				objIData.AddParameter("@" + InputVoucher_Attachment.colDescription, objInputVoucher_Attachment.Description);
				objIData.AddParameter("@" + InputVoucher_Attachment.colFilePath, objInputVoucher_Attachment.FilePath);
				objIData.AddParameter("@" + InputVoucher_Attachment.colFileName, objInputVoucher_Attachment.FileName);
                objIData.AddParameter("@FileID",objInputVoucher_Attachment.FileID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin danh sách file đính kèm của phiếu nhập
		/// </summary>
		/// <param name="objInputVoucher_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InputVoucher_Attachment objInputVoucher_Attachment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInputVoucher_Attachment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách file đính kèm của phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher_Attachment -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách file đính kèm của phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucher_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InputVoucher_Attachment objInputVoucher_Attachment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InputVoucher_Attachment.colAttachMENTID, objInputVoucher_Attachment.AttachmentID);
				objIData.AddParameter("@" + InputVoucher_Attachment.colDeletedUser, objInputVoucher_Attachment.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

		#endregion

		
		#region Constructor

		public DA_InputVoucher_Attachment()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_InputVoucher_Attachment_ADD";
		public const String SP_UPDATE = "PM_InputVoucher_Attachment_UPD";
		public const String SP_DELETE = "PM_InputVoucher_Attachment_DEL";
		public const String SP_SELECT = "PM_InputVoucher_Attachment_SEL";
		public const String SP_SEARCH = "PM_InputVoucher_Attachment_SRH";
        public const String SP_GETLISTBYINPUTVOUCHERID = "PM_INPUTATTACHMENT_INPUTID";
		public const String SP_UPDATEINDEX = "PM_InputVoucher_Attachment_UPDINDEX";
		#endregion
		
	}
}
