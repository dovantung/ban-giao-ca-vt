
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using StoreChangeOrder = ERP.Inventory.BO.StoreChangeOrder;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
    /// Created by 		: Hoang Nhu Phong 
    /// Created date 	: 9/28/2012 
    /// Yêu cầu chuyển kho
    /// </summary>	
    public partial class DA_StoreChangeOrder
    {


        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods






        /// <summary>
        /// Thêm thông tin yêu cầu chuyển kho
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objStoreChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, StoreChangeOrder objStoreChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeOrderID, objStoreChangeOrder.StoreChangeOrderID);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeOrderTypeID, objStoreChangeOrder.StoreChangeOrderTypeID);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeTypeID, objStoreChangeOrder.StoreChangeTypeID);
                objIData.AddParameter("@" + StoreChangeOrder.colTransportTypeID, objStoreChangeOrder.TransportTypeID);
                objIData.AddParameter("@" + StoreChangeOrder.colTransportCompanyID, objStoreChangeOrder.TransportCompanyID);
                objIData.AddParameter("@" + StoreChangeOrder.colTransportServicesID , objStoreChangeOrder.TransportServicesID);
                objIData.AddParameter("@" + StoreChangeOrder.colFromStoreID, objStoreChangeOrder.FromStoreID);
                objIData.AddParameter("@" + StoreChangeOrder.colToStoreID, objStoreChangeOrder.ToStoreID);
                objIData.AddParameter("@" + StoreChangeOrder.colContent, objStoreChangeOrder.Content);
                objIData.AddParameter("@" + StoreChangeOrder.colOrderDate, objStoreChangeOrder.OrderDate);
                objIData.AddParameter("@" + StoreChangeOrder.colExpiryDate, objStoreChangeOrder.ExpiryDate);
                objIData.AddParameter("@" + StoreChangeOrder.colIsReviewed, objStoreChangeOrder.IsReviewed);
                objIData.AddParameter("@" + StoreChangeOrder.colReviewedUser, objStoreChangeOrder.ReviewedUser);
                objIData.AddParameter("@" + StoreChangeOrder.colReviewedDate, objStoreChangeOrder.ReviewedDate);
                objIData.AddParameter("@" + StoreChangeOrder.colCreatedStoreID, objStoreChangeOrder.CreatedStoreID);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeStatus, objStoreChangeOrder.StoreChangeStatus);
                objIData.AddParameter("@" + StoreChangeOrder.colIsNew, objStoreChangeOrder.IsNew);
                objIData.AddParameter("@" + StoreChangeOrder.colTotalCommandQuantity, objStoreChangeOrder.TotalCommandQuantity);
                objIData.AddParameter("@" + StoreChangeOrder.colTotalQuantity, objStoreChangeOrder.TotalQuantity);
                objIData.AddParameter("@" + StoreChangeOrder.colTotalStoreChangeQuantity, objStoreChangeOrder.TotalStoreChangeQuantity);
                objIData.AddParameter("@" + StoreChangeOrder.colIsUrgent, objStoreChangeOrder.IsUrgent);
                objIData.AddParameter("@" + StoreChangeOrder.colUpdatedIsUrgentIUser, objStoreChangeOrder.UpdatedIsUrgentIUser);
                objIData.AddParameter("@" + StoreChangeOrder.colUpdatedIsUrgentDate, objStoreChangeOrder.UpdatedIsUrgentDate);
                objIData.AddParameter("@" + StoreChangeOrder.colCreatedUser, objStoreChangeOrder.CreatedUser);
                objIData.AddParameter("@" + StoreChangeOrder.colContentDeleted, objStoreChangeOrder.ContentDeleted);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeCommandID, objStoreChangeOrder.StoreChangeCommandID);
                objIData.AddParameter("@" + StoreChangeOrder.colInStockStatusID, objStoreChangeOrder.InStockStatusID);
                objIData.AddParameter("@" + StoreChangeOrder.colISORTHERPRODUCT, objStoreChangeOrder.ISORTHERPRODUCT);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objStoreChangeOrder.StoreChangeOrderID = objIData.ExecStoreToString().Trim();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }




        /// <summary>
        /// Cập nhật thông tin yêu cầu chuyển kho
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objStoreChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, StoreChangeOrder objStoreChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeOrderID, objStoreChangeOrder.StoreChangeOrderID);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeOrderTypeID, objStoreChangeOrder.StoreChangeOrderTypeID);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeTypeID, objStoreChangeOrder.StoreChangeTypeID);
                objIData.AddParameter("@" + StoreChangeOrder.colTransportTypeID, objStoreChangeOrder.TransportTypeID);
                objIData.AddParameter("@" + StoreChangeOrder.colTransportCompanyID, objStoreChangeOrder.TransportCompanyID);
                objIData.AddParameter("@" + StoreChangeOrder.colTransportServicesID, objStoreChangeOrder.TransportServicesID);
                objIData.AddParameter("@" + StoreChangeOrder.colFromStoreID, objStoreChangeOrder.FromStoreID);
                objIData.AddParameter("@" + StoreChangeOrder.colToStoreID, objStoreChangeOrder.ToStoreID);
                objIData.AddParameter("@" + StoreChangeOrder.colContent, objStoreChangeOrder.Content);
                objIData.AddParameter("@" + StoreChangeOrder.colOrderDate, objStoreChangeOrder.OrderDate);
                objIData.AddParameter("@" + StoreChangeOrder.colExpiryDate, objStoreChangeOrder.ExpiryDate);
                objIData.AddParameter("@" + StoreChangeOrder.colIsReviewed, objStoreChangeOrder.IsReviewed);
                objIData.AddParameter("@" + StoreChangeOrder.colReviewedUser, objStoreChangeOrder.ReviewedUser);
                objIData.AddParameter("@" + StoreChangeOrder.colReviewedDate, objStoreChangeOrder.ReviewedDate);
                objIData.AddParameter("@" + StoreChangeOrder.colCreatedStoreID, objStoreChangeOrder.CreatedStoreID);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeStatus, objStoreChangeOrder.StoreChangeStatus);
                objIData.AddParameter("@" + StoreChangeOrder.colIsNew, objStoreChangeOrder.IsNew);
                objIData.AddParameter("@" + StoreChangeOrder.colTotalCommandQuantity, objStoreChangeOrder.TotalCommandQuantity);
                objIData.AddParameter("@" + StoreChangeOrder.colTotalQuantity, objStoreChangeOrder.TotalQuantity);
                objIData.AddParameter("@" + StoreChangeOrder.colTotalStoreChangeQuantity, objStoreChangeOrder.TotalStoreChangeQuantity);
                objIData.AddParameter("@" + StoreChangeOrder.colIsUrgent, objStoreChangeOrder.IsUrgent);
                objIData.AddParameter("@" + StoreChangeOrder.colUpdatedIsUrgentIUser, objStoreChangeOrder.UpdatedIsUrgentIUser);
                objIData.AddParameter("@" + StoreChangeOrder.colUpdatedIsUrgentDate, objStoreChangeOrder.UpdatedIsUrgentDate);
                objIData.AddParameter("@" + StoreChangeOrder.colUpdatedUser, objStoreChangeOrder.UpdatedUser);
                objIData.AddParameter("@" + StoreChangeOrder.colContentDeleted, objStoreChangeOrder.ContentDeleted);
                objIData.AddParameter("@" + StoreChangeOrder.colInStockStatusID, objStoreChangeOrder.InStockStatusID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin yêu cầu chuyển kho
        /// </summary>
        /// <param name="objStoreChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(StoreChangeOrder objStoreChangeOrder)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objStoreChangeOrder);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage DeleteList(List<StoreChangeOrder> lstStoreChangeOrder, string strUserDelete)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (var objStoreChangeOrder in lstStoreChangeOrder)
                {
                    objStoreChangeOrder.DeletedUser = strUserDelete;
                    Delete(objIData, objStoreChangeOrder);
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin yêu cầu chuyển kho
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objStoreChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, StoreChangeOrder objStoreChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeOrderID, objStoreChangeOrder.StoreChangeOrderID);
                objIData.AddParameter("@" + StoreChangeOrder.colContentDeleted, objStoreChangeOrder.ContentDeleted);
                objIData.AddParameter("@" + StoreChangeOrder.colDeletedUser, objStoreChangeOrder.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_StoreChangeOrder()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "PM_STORECHANGEORDER_ADD";
        public const String SP_UPDATE = "PM_STORECHANGEORDER_UPD";
        public const String SP_DELETE = "PM_STORECHANGEORDER_DEL";
        public const String SP_SELECT = "PM_STORECHANGEORDER_SEL";
        public const String SP_SEARCH = "PM_STORECHANGEORDER_SRH";
        public const String SP_ORTHERSEARCH = "PM_STORECHANGEORTHERORDER_SRH";

        public const String SP_GET = "MD_OUPUTTYPE_GETOUTPUTALL";
        public const String SP_UPDATEINDEX = "PM_STORECHANGEORDER_UPDINDEX";
        #endregion


        public ResultMessage GetOutputAll(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_StoreChangeOrder.SP_GET);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi Kiểm tra thông tin xuất tất cả ngành hàng trên yêu cầu xuất chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> GetOutputAll", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

       
    }
}
