
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputVoucherDetailIMEI = ERP.Inventory.BO.InputVoucherDetailIMEI;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// Chi tiết IMEI phiếu nhập
	/// </summary>	
	public partial class DA_InputVoucherDetailIMEI
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết imei phiếu nhập
		/// </summary>
		/// <param name="objInputVoucherDetailIMEI">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InputVoucherDetailIMEI objInputVoucherDetailIMEI, string strIMEI, string strInputVoucherDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colIMEI, strIMEI);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputVoucherDetailID, strInputVoucherDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					if (objInputVoucherDetailIMEI == null) 
 						objInputVoucherDetailIMEI = new InputVoucherDetailIMEI();
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colInputVoucherDetailID])) objInputVoucherDetailIMEI.InputVoucherDetailID = Convert.ToString(reader[InputVoucherDetailIMEI.colInputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colIMEI])) objInputVoucherDetailIMEI.IMEI = Convert.ToString(reader[InputVoucherDetailIMEI.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colPINCode])) objInputVoucherDetailIMEI.PINCode = Convert.ToString(reader[InputVoucherDetailIMEI.colPINCode]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colNote])) objInputVoucherDetailIMEI.Note = Convert.ToString(reader[InputVoucherDetailIMEI.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colIMEIChangeCHAIN])) objInputVoucherDetailIMEI.IMEIChangeCHAIN = Convert.ToString(reader[InputVoucherDetailIMEI.colIMEIChangeCHAIN]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colIsHasWarranty])) objInputVoucherDetailIMEI.IsHasWarranty = Convert.ToBoolean(reader[InputVoucherDetailIMEI.colIsHasWarranty]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colCreatedStoreID])) objInputVoucherDetailIMEI.CreatedStoreID = Convert.ToInt32(reader[InputVoucherDetailIMEI.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colInputStoreID])) objInputVoucherDetailIMEI.InputStoreID = Convert.ToInt32(reader[InputVoucherDetailIMEI.colInputStoreID]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colInputDate])) objInputVoucherDetailIMEI.InputDate = Convert.ToDateTime(reader[InputVoucherDetailIMEI.colInputDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colCreatedUser])) objInputVoucherDetailIMEI.CreatedUser = Convert.ToString(reader[InputVoucherDetailIMEI.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colCreatedDate])) objInputVoucherDetailIMEI.CreatedDate = Convert.ToDateTime(reader[InputVoucherDetailIMEI.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colUpdatedUser])) objInputVoucherDetailIMEI.UpdatedUser = Convert.ToString(reader[InputVoucherDetailIMEI.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colUpdatedDate])) objInputVoucherDetailIMEI.UpdatedDate = Convert.ToDateTime(reader[InputVoucherDetailIMEI.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colIsDeleted])) objInputVoucherDetailIMEI.IsDeleted = Convert.ToBoolean(reader[InputVoucherDetailIMEI.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colDeletedUser])) objInputVoucherDetailIMEI.DeletedUser = Convert.ToString(reader[InputVoucherDetailIMEI.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colDeletedDate])) objInputVoucherDetailIMEI.DeletedDate = Convert.ToDateTime(reader[InputVoucherDetailIMEI.colDeletedDate]);

                    if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colCostPrice])) objInputVoucherDetailIMEI.CostPrice = Convert.ToDecimal(reader[InputVoucherDetailIMEI.colCostPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colFirstPrice])) objInputVoucherDetailIMEI.FirstPrice = Convert.ToDecimal(reader[InputVoucherDetailIMEI.colFirstPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetailIMEI.colInputUser])) objInputVoucherDetailIMEI.InputUser = Convert.ToString(reader[InputVoucherDetailIMEI.colInputUser]);
 					objInputVoucherDetailIMEI.IsExist = true;
 				}
 				else
 				{
 					objInputVoucherDetailIMEI.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết imei phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetailIMEI -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết imei phiếu nhập
		/// </summary>
		/// <param name="objInputVoucherDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InputVoucherDetailIMEI objInputVoucherDetailIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInputVoucherDetailIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết imei phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetailIMEI -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết imei phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucherDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InputVoucherDetailIMEI objInputVoucherDetailIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputVoucherDetailID, objInputVoucherDetailIMEI.InputVoucherDetailID);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colIMEI, objInputVoucherDetailIMEI.IMEI);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colPINCode, objInputVoucherDetailIMEI.PINCode);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colNote, objInputVoucherDetailIMEI.Note);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colIMEIChangeCHAIN, objInputVoucherDetailIMEI.IMEIChangeCHAIN);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colIsHasWarranty, objInputVoucherDetailIMEI.IsHasWarranty);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colCreatedStoreID, objInputVoucherDetailIMEI.CreatedStoreID);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputStoreID, objInputVoucherDetailIMEI.InputStoreID);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputDate, objInputVoucherDetailIMEI.InputDate);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colCreatedUser, objInputVoucherDetailIMEI.CreatedUser);
                objIData.AddParameter("@" + InputVoucherDetailIMEI.colCostPrice, objInputVoucherDetailIMEI.CostPrice);
                objIData.AddParameter("@" + InputVoucherDetailIMEI.colFirstPrice, objInputVoucherDetailIMEI.FirstPrice);
                objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputUser, objInputVoucherDetailIMEI.InputUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết imei phiếu nhập
		/// </summary>
		/// <param name="objInputVoucherDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InputVoucherDetailIMEI objInputVoucherDetailIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInputVoucherDetailIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết imei phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetailIMEI -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết imei phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucherDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InputVoucherDetailIMEI objInputVoucherDetailIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputVoucherDetailID, objInputVoucherDetailIMEI.InputVoucherDetailID);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colIMEI, objInputVoucherDetailIMEI.IMEI);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colPINCode, objInputVoucherDetailIMEI.PINCode);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colNote, objInputVoucherDetailIMEI.Note);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colIMEIChangeCHAIN, objInputVoucherDetailIMEI.IMEIChangeCHAIN);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colIsHasWarranty, objInputVoucherDetailIMEI.IsHasWarranty);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colCreatedStoreID, objInputVoucherDetailIMEI.CreatedStoreID);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputStoreID, objInputVoucherDetailIMEI.InputStoreID);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputDate, objInputVoucherDetailIMEI.InputDate);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colUpdatedUser, objInputVoucherDetailIMEI.UpdatedUser);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết imei phiếu nhập
		/// </summary>
		/// <param name="objInputVoucherDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InputVoucherDetailIMEI objInputVoucherDetailIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInputVoucherDetailIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết imei phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetailIMEI -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết imei phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucherDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InputVoucherDetailIMEI objInputVoucherDetailIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputVoucherDetailID, objInputVoucherDetailIMEI.InputVoucherDetailID);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colIMEI, objInputVoucherDetailIMEI.IMEI);
				objIData.AddParameter("@" + InputVoucherDetailIMEI.colDeletedUser, objInputVoucherDetailIMEI.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
        }
        public ResultMessage DeleteTemp()
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                DeleteTemp(objIData);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết imei phiếu nhập tạm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetailIMEI -> DeleteTemp", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public void DeleteTemp(IData objIData)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHERDTIMEI_DELTMP");
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public bool BulkCopy(DataTable data, string toTableName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                BulkCopy(objIData, data, toTableName);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi đẩy dữ liệu dùng bullcopy", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetailIMEI -> BulkCopy", InventoryGlobals.ModuleName);
                return false;
            }
            finally
            {
                objIData.Disconnect();
            }
            return true;
        }
        public void BulkCopy(IData objIData, DataTable data, string toTableName)
        {
            try
            {
                //objIData.BulkCopy(data, toTableName);
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Xóa thông tin chi tiết imei phiếu nhập
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherDetailIMEI">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void InsertFromTempTable(IData objIData)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD_FROM_TEMP);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public ResultMessage InsertFromTempTable()
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                InsertFromTempTable(objIData);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi thêm thông tin chi tiết imei phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetailIMEI -> InsertFromTempTable", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        #endregion


        #region Constructor

        public DA_InputVoucherDetailIMEI()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_INPUTVOUCHERDETAILIMEI_ADD";
		public const String SP_UPDATE = "PM_INPUTVOUCHERDETAILIMEI_UPD";
		public const String SP_DELETE = "PM_INPUTVOUCHERDETAILIMEI_DEL";
        public const String SP_ADD_FROM_TEMP = "PM_INPUTVOUCHERDTIMEI_ADDTMP";
        public const String SP_SELECT = "PM_INPUTVOUCHERDETAILIMEI_SEL";
		public const String SP_SEARCH = "PM_INPUTVOUCHERDETAILIMEI_SRH";
        public const String SP_GET = "PM_INPUTVOUCHERDETAILIMEI_GET";
		public const String SP_UPDATEINDEX = "PM_INPUTVOUCHERDETAILIMEI_UPDINDEX";
		#endregion
		
	}
}
