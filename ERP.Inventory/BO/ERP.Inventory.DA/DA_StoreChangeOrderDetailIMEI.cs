
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using StoreChangeOrderDetailIMEI = ERP.Inventory.BO.StoreChangeOrderDetailIMEI;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 12/5/2012 
	/// Chi tiết yêu cầu chuyển kho theo IMEI
	/// </summary>	
	public partial class DA_StoreChangeOrderDetailIMEI
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết yêu cầu chuyển kho theo imei
		/// </summary>
		/// <param name="objStoreChangeOrderDetailIMEI">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEI, string strIMEI, string strStoreChangeOrderDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colIMEI, strIMEI);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colStoreChangeOrderDetailID, strStoreChangeOrderDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objStoreChangeOrderDetailIMEI = new StoreChangeOrderDetailIMEI();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colStoreChangeOrderDetailID])) objStoreChangeOrderDetailIMEI.StoreChangeOrderDetailID = Convert.ToString(reader[StoreChangeOrderDetailIMEI.colStoreChangeOrderDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colIMEI])) objStoreChangeOrderDetailIMEI.IMEI = Convert.ToString(reader[StoreChangeOrderDetailIMEI.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colOrderDate])) objStoreChangeOrderDetailIMEI.OrderDate = Convert.ToDateTime(reader[StoreChangeOrderDetailIMEI.colOrderDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colIsStoreChange])) objStoreChangeOrderDetailIMEI.IsStoreChange = Convert.ToBoolean(reader[StoreChangeOrderDetailIMEI.colIsStoreChange]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colNote])) objStoreChangeOrderDetailIMEI.Note = Convert.ToString(reader[StoreChangeOrderDetailIMEI.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colCreatedStoreID])) objStoreChangeOrderDetailIMEI.CreatedStoreID = Convert.ToInt32(reader[StoreChangeOrderDetailIMEI.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colCreatedUser])) objStoreChangeOrderDetailIMEI.CreatedUser = Convert.ToString(reader[StoreChangeOrderDetailIMEI.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colCreatedDate])) objStoreChangeOrderDetailIMEI.CreatedDate = Convert.ToDateTime(reader[StoreChangeOrderDetailIMEI.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colUpdatedUser])) objStoreChangeOrderDetailIMEI.UpdatedUser = Convert.ToString(reader[StoreChangeOrderDetailIMEI.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colUpdatedDate])) objStoreChangeOrderDetailIMEI.UpdatedDate = Convert.ToDateTime(reader[StoreChangeOrderDetailIMEI.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colIsDeleted])) objStoreChangeOrderDetailIMEI.IsDeleted = Convert.ToBoolean(reader[StoreChangeOrderDetailIMEI.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colDeletedUser])) objStoreChangeOrderDetailIMEI.DeletedUser = Convert.ToString(reader[StoreChangeOrderDetailIMEI.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetailIMEI.colDeletedDate])) objStoreChangeOrderDetailIMEI.DeletedDate = Convert.ToDateTime(reader[StoreChangeOrderDetailIMEI.colDeletedDate]);
 				}
 				else
 				{
 					objStoreChangeOrderDetailIMEI = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết yêu cầu chuyển kho theo imei", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrderDetailIMEI -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết yêu cầu chuyển kho theo imei
		/// </summary>
		/// <param name="objStoreChangeOrderDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objStoreChangeOrderDetailIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết yêu cầu chuyển kho theo imei", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrderDetailIMEI -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết yêu cầu chuyển kho theo imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrderDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEI, bool hasUpdateCurrentInstock = false, string storeChangeOrderID = "")
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colStoreChangeOrderDetailID, objStoreChangeOrderDetailIMEI.StoreChangeOrderDetailID);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colIMEI, objStoreChangeOrderDetailIMEI.IMEI);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colOrderDate, objStoreChangeOrderDetailIMEI.OrderDate);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colIsStoreChange, objStoreChangeOrderDetailIMEI.IsStoreChange);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colNote, objStoreChangeOrderDetailIMEI.Note);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colCreatedStoreID, objStoreChangeOrderDetailIMEI.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colCreatedUser, objStoreChangeOrderDetailIMEI.CreatedUser);
                if(hasUpdateCurrentInstock && !string.IsNullOrEmpty(storeChangeOrderID))
                {
                    objIData.AddParameter("@HasUpdateCurrentInstock", Convert.ToInt32(hasUpdateCurrentInstock));
                    objIData.AddParameter("@StoreChangeOrderID", storeChangeOrderID);
                }
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết yêu cầu chuyển kho theo imei
		/// </summary>
		/// <param name="objStoreChangeOrderDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objStoreChangeOrderDetailIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết yêu cầu chuyển kho theo imei", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrderDetailIMEI -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết yêu cầu chuyển kho theo imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrderDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colStoreChangeOrderDetailID, objStoreChangeOrderDetailIMEI.StoreChangeOrderDetailID);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colIMEI, objStoreChangeOrderDetailIMEI.IMEI);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colOrderDate, objStoreChangeOrderDetailIMEI.OrderDate);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colIsStoreChange, objStoreChangeOrderDetailIMEI.IsStoreChange);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colNote, objStoreChangeOrderDetailIMEI.Note);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colCreatedStoreID, objStoreChangeOrderDetailIMEI.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colUpdatedUser, objStoreChangeOrderDetailIMEI.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết yêu cầu chuyển kho theo imei
		/// </summary>
		/// <param name="objStoreChangeOrderDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objStoreChangeOrderDetailIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết yêu cầu chuyển kho theo imei", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrderDetailIMEI -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết yêu cầu chuyển kho theo imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrderDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colStoreChangeOrderDetailID, objStoreChangeOrderDetailIMEI.StoreChangeOrderDetailID);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colIMEI, objStoreChangeOrderDetailIMEI.IMEI);
				objIData.AddParameter("@" + StoreChangeOrderDetailIMEI.colDeletedUser, objStoreChangeOrderDetailIMEI.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_StoreChangeOrderDetailIMEI()
		{
		}
		#endregion


		#region Stored Procedure Names

        public const String SP_ADD = "PM_SC_ORDERDETAILIMEI_ADD";
        public const String SP_UPDATE = "PM_SC_ORDERDETAILIMEI_UPD";
        public const String SP_DELETE = "PM_SC_ORDERDETAILIMEI_DEL";
        public const String SP_SELECT = "PM_SC_ORDERDETAILIMEI_SEL";
        public const String SP_SEARCH = "PM_SC_ORDERDETAILIMEI_SRH";
        public const String SP_UPDATEINDEX = "PM_SC_ORDERDETAILIMEI_UPDINDEX";
		#endregion
		
	}
}
