
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace  ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Hoang Nhu Phong 
	/// Created date 	: 9/28/2012 
	/// Bảng danh sách duyêt của  yêu cầu chuyển kho( Lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
	/// </summary>	
	public partial class DA_StoreChangeOrder_RVList
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_StoreChangeOrder_RVList.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder_RVList -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}
		#endregion
		
		
	}
}
