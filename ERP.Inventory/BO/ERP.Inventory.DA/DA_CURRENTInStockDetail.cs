
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using CurrentInStockDetail = ERP.Inventory.BO.CurrentInStockDetail;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// 
	/// </summary>	
	public partial class DA_CurrentInStockDetail
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods		
		


		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objCurrentInStockDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref CurrentInStockDetail objCurrentInStockDetail, string strIMEI, string strInputVoucherDetailID, string strProductID, int intStoreID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + CurrentInStockDetail.colIMEI, strIMEI);
				objIData.AddParameter("@" + CurrentInStockDetail.colInputVoucherDetailID, strInputVoucherDetailID);
				objIData.AddParameter("@" + CurrentInStockDetail.colProductID, strProductID);
				objIData.AddParameter("@" + CurrentInStockDetail.colStoreID, intStoreID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					if (objCurrentInStockDetail == null) 
 						objCurrentInStockDetail = new CurrentInStockDetail();
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colStoreID])) objCurrentInStockDetail.StoreID = Convert.ToInt32(reader[CurrentInStockDetail.colStoreID]);
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colProductID])) objCurrentInStockDetail.ProductID = Convert.ToString(reader[CurrentInStockDetail.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colIMEI])) objCurrentInStockDetail.IMEI = Convert.ToString(reader[CurrentInStockDetail.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colPINCode])) objCurrentInStockDetail.PINCode = Convert.ToString(reader[CurrentInStockDetail.colPINCode]).Trim();
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colInputVoucherDetailID])) objCurrentInStockDetail.InputVoucherDetailID = Convert.ToString(reader[CurrentInStockDetail.colInputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colFirstCustomerID])) objCurrentInStockDetail.FirstCustomerID = Convert.ToInt32(reader[CurrentInStockDetail.colFirstCustomerID]);
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colENDWarrantyDate])) objCurrentInStockDetail.ENDWarrantyDate = Convert.ToDateTime(reader[CurrentInStockDetail.colENDWarrantyDate]);
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colCostPrice])) objCurrentInStockDetail.CostPrice = Convert.ToDecimal(reader[CurrentInStockDetail.colCostPrice]);
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colIsNew])) objCurrentInStockDetail.IsNew = Convert.ToBoolean(reader[CurrentInStockDetail.colIsNew]);
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colIsShowProduct])) objCurrentInStockDetail.IsShowProduct = Convert.ToBoolean(reader[CurrentInStockDetail.colIsShowProduct]);
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colIsOrder])) objCurrentInStockDetail.IsOrder = Convert.ToBoolean(reader[CurrentInStockDetail.colIsOrder]);
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colISDelivery])) objCurrentInStockDetail.ISDelivery = Convert.ToBoolean(reader[CurrentInStockDetail.colISDelivery]);
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colOrderID])) objCurrentInStockDetail.OrderID = Convert.ToString(reader[CurrentInStockDetail.colOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[CurrentInStockDetail.colNote])) objCurrentInStockDetail.Note = Convert.ToString(reader[CurrentInStockDetail.colNote]).Trim();
 					objCurrentInStockDetail.IsExist = true;
 				}
 				else
 				{
 					objCurrentInStockDetail.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CurrentInStockDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objCurrentInStockDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(CurrentInStockDetail objCurrentInStockDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objCurrentInStockDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CurrentInStockDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objCurrentInStockDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, CurrentInStockDetail objCurrentInStockDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + CurrentInStockDetail.colStoreID, objCurrentInStockDetail.StoreID);
				objIData.AddParameter("@" + CurrentInStockDetail.colProductID, objCurrentInStockDetail.ProductID);
				objIData.AddParameter("@" + CurrentInStockDetail.colIMEI, objCurrentInStockDetail.IMEI);
				objIData.AddParameter("@" + CurrentInStockDetail.colPINCode, objCurrentInStockDetail.PINCode);
				objIData.AddParameter("@" + CurrentInStockDetail.colInputVoucherDetailID, objCurrentInStockDetail.InputVoucherDetailID);
				objIData.AddParameter("@" + CurrentInStockDetail.colFirstCustomerID, objCurrentInStockDetail.FirstCustomerID);
				objIData.AddParameter("@" + CurrentInStockDetail.colENDWarrantyDate, objCurrentInStockDetail.ENDWarrantyDate);
				objIData.AddParameter("@" + CurrentInStockDetail.colCostPrice, objCurrentInStockDetail.CostPrice);
				objIData.AddParameter("@" + CurrentInStockDetail.colIsNew, objCurrentInStockDetail.IsNew);
				objIData.AddParameter("@" + CurrentInStockDetail.colIsShowProduct, objCurrentInStockDetail.IsShowProduct);
				objIData.AddParameter("@" + CurrentInStockDetail.colIsOrder, objCurrentInStockDetail.IsOrder);
				objIData.AddParameter("@" + CurrentInStockDetail.colISDelivery, objCurrentInStockDetail.ISDelivery);
				objIData.AddParameter("@" + CurrentInStockDetail.colOrderID, objCurrentInStockDetail.OrderID);
				objIData.AddParameter("@" + CurrentInStockDetail.colNote, objCurrentInStockDetail.Note);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objCurrentInStockDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(CurrentInStockDetail objCurrentInStockDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objCurrentInStockDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CurrentInStockDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objCurrentInStockDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, CurrentInStockDetail objCurrentInStockDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + CurrentInStockDetail.colStoreID, objCurrentInStockDetail.StoreID);
				objIData.AddParameter("@" + CurrentInStockDetail.colProductID, objCurrentInStockDetail.ProductID);
				objIData.AddParameter("@" + CurrentInStockDetail.colIMEI, objCurrentInStockDetail.IMEI);
				objIData.AddParameter("@" + CurrentInStockDetail.colPINCode, objCurrentInStockDetail.PINCode);
				objIData.AddParameter("@" + CurrentInStockDetail.colInputVoucherDetailID, objCurrentInStockDetail.InputVoucherDetailID);
				objIData.AddParameter("@" + CurrentInStockDetail.colFirstCustomerID, objCurrentInStockDetail.FirstCustomerID);
				objIData.AddParameter("@" + CurrentInStockDetail.colENDWarrantyDate, objCurrentInStockDetail.ENDWarrantyDate);
				objIData.AddParameter("@" + CurrentInStockDetail.colCostPrice, objCurrentInStockDetail.CostPrice);
				objIData.AddParameter("@" + CurrentInStockDetail.colIsNew, objCurrentInStockDetail.IsNew);
				objIData.AddParameter("@" + CurrentInStockDetail.colIsShowProduct, objCurrentInStockDetail.IsShowProduct);
				objIData.AddParameter("@" + CurrentInStockDetail.colIsOrder, objCurrentInStockDetail.IsOrder);
				objIData.AddParameter("@" + CurrentInStockDetail.colISDelivery, objCurrentInStockDetail.ISDelivery);
				objIData.AddParameter("@" + CurrentInStockDetail.colOrderID, objCurrentInStockDetail.OrderID);
				objIData.AddParameter("@" + CurrentInStockDetail.colNote, objCurrentInStockDetail.Note);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objCurrentInStockDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(CurrentInStockDetail objCurrentInStockDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objCurrentInStockDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CurrentInStockDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objCurrentInStockDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, CurrentInStockDetail objCurrentInStockDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + CurrentInStockDetail.colStoreID, objCurrentInStockDetail.StoreID);
				objIData.AddParameter("@" + CurrentInStockDetail.colProductID, objCurrentInStockDetail.ProductID);
				objIData.AddParameter("@" + CurrentInStockDetail.colIMEI, objCurrentInStockDetail.IMEI);
				objIData.AddParameter("@" + CurrentInStockDetail.colInputVoucherDetailID, objCurrentInStockDetail.InputVoucherDetailID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_CurrentInStockDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_CurrentInStockDetail_ADD";
		public const String SP_UPDATE = "PM_CurrentInStockDetail_UPD";
		public const String SP_DELETE = "PM_CurrentInStockDetail_DEL";
		public const String SP_SELECT = "PM_CurrentInStockDetail_SEL";
		public const String SP_SEARCH = "PM_CurrentInStockDetail_SRH";
		public const String SP_UPDATEINDEX = "PM_CurrentInStockDetail_UPDINDEX";
		#endregion
		
	}
}
