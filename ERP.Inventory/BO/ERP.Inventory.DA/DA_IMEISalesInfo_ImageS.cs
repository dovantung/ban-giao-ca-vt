
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using IMEISalesInfo_ImageS = ERP.Inventory.BO.IMEISalesInfo_Images;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 3/7/2014 
	/// 
	/// </summary>	
	public partial class DA_IMEISalesInfo_Images
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objIMEISalesInfo_Images">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref IMEISalesInfo_ImageS objIMEISalesInfo_Images, string strImageID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colImageID, strImageID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objIMEISalesInfo_Images = new IMEISalesInfo_ImageS();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colImageID])) objIMEISalesInfo_Images.ImageID = Convert.ToString(reader[IMEISalesInfo_ImageS.colImageID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colProductID])) objIMEISalesInfo_Images.ProductID = Convert.ToString(reader[IMEISalesInfo_ImageS.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colIMEI])) objIMEISalesInfo_Images.IMEI = Convert.ToString(reader[IMEISalesInfo_ImageS.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colImageName])) objIMEISalesInfo_Images.ImageName = Convert.ToString(reader[IMEISalesInfo_ImageS.colImageName]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colDescription])) objIMEISalesInfo_Images.Description = Convert.ToString(reader[IMEISalesInfo_ImageS.colDescription]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colLargeSizeImage])) objIMEISalesInfo_Images.LargeSizeImage = Convert.ToString(reader[IMEISalesInfo_ImageS.colLargeSizeImage]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colMediumSizeImage])) objIMEISalesInfo_Images.MediumSizeImage = Convert.ToString(reader[IMEISalesInfo_ImageS.colMediumSizeImage]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colSmallSizeImage])) objIMEISalesInfo_Images.SmallSizeImage = Convert.ToString(reader[IMEISalesInfo_ImageS.colSmallSizeImage]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colLargeSizeImageFileID])) objIMEISalesInfo_Images.LargeSizeImageFileID = Convert.ToString(reader[IMEISalesInfo_ImageS.colLargeSizeImageFileID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colMediumSizeImageFileID])) objIMEISalesInfo_Images.MediumSizeImageFileID = Convert.ToString(reader[IMEISalesInfo_ImageS.colMediumSizeImageFileID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colSmallSizeImageFileID])) objIMEISalesInfo_Images.SmallSizeImageFileID = Convert.ToString(reader[IMEISalesInfo_ImageS.colSmallSizeImageFileID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colIsDefault])) objIMEISalesInfo_Images.IsDefault = Convert.ToBoolean(reader[IMEISalesInfo_ImageS.colIsDefault]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colOrderIndex])) objIMEISalesInfo_Images.OrderIndex = Convert.ToInt32(reader[IMEISalesInfo_ImageS.colOrderIndex]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colIsActive])) objIMEISalesInfo_Images.IsActive = Convert.ToBoolean(reader[IMEISalesInfo_ImageS.colIsActive]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colIsSystem])) objIMEISalesInfo_Images.IsSystem = Convert.ToBoolean(reader[IMEISalesInfo_ImageS.colIsSystem]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colCreatedUser])) objIMEISalesInfo_Images.CreatedUser = Convert.ToString(reader[IMEISalesInfo_ImageS.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colCreatedDate])) objIMEISalesInfo_Images.CreatedDate = Convert.ToDateTime(reader[IMEISalesInfo_ImageS.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colUpdatedUser])) objIMEISalesInfo_Images.UpdatedUser = Convert.ToString(reader[IMEISalesInfo_ImageS.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colUpdatedDate])) objIMEISalesInfo_Images.UpdatedDate = Convert.ToDateTime(reader[IMEISalesInfo_ImageS.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colIsDeleted])) objIMEISalesInfo_Images.IsDeleted = Convert.ToBoolean(reader[IMEISalesInfo_ImageS.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colDeletedUser])) objIMEISalesInfo_Images.DeletedUser = Convert.ToString(reader[IMEISalesInfo_ImageS.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEISalesInfo_ImageS.colDeletedDate])) objIMEISalesInfo_Images.DeletedDate = Convert.ToDateTime(reader[IMEISalesInfo_ImageS.colDeletedDate]);
 				}
 				else
 				{
 					objIMEISalesInfo_Images = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ImageS -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(IMEISalesInfo_ImageS objIMEISalesInfo_ImageS)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objIMEISalesInfo_ImageS);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ImageS -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, IMEISalesInfo_ImageS objIMEISalesInfo_ImageS)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				//objIData.AddParameter("@" + IMEISalesInfo_ImageS.colImageID, objIMEISalesInfo_ImageS.ImageID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colProductID, objIMEISalesInfo_ImageS.ProductID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colIMEI, objIMEISalesInfo_ImageS.IMEI);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colImageName, objIMEISalesInfo_ImageS.ImageName);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colDescription, objIMEISalesInfo_ImageS.Description);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colLargeSizeImage, objIMEISalesInfo_ImageS.LargeSizeImage);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colMediumSizeImage, objIMEISalesInfo_ImageS.MediumSizeImage);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colSmallSizeImage, objIMEISalesInfo_ImageS.SmallSizeImage);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colLargeSizeImageFileID, objIMEISalesInfo_ImageS.LargeSizeImageFileID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colMediumSizeImageFileID, objIMEISalesInfo_ImageS.MediumSizeImageFileID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colSmallSizeImageFileID, objIMEISalesInfo_ImageS.SmallSizeImageFileID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colIsDefault, objIMEISalesInfo_ImageS.IsDefault);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colOrderIndex, objIMEISalesInfo_ImageS.OrderIndex);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colIsActive, objIMEISalesInfo_ImageS.IsActive);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colIsSystem, objIMEISalesInfo_ImageS.IsSystem);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colCreatedUser, objIMEISalesInfo_ImageS.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <param name="lstOrderIndex">Danh sách OrderIndex</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(IMEISalesInfo_ImageS objIMEISalesInfo_ImageS, List<object> lstOrderIndex)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.BeginTransaction();
				Update(objIData, objIMEISalesInfo_ImageS);
				if (lstOrderIndex != null && lstOrderIndex.Count > 0)
				{
					for (int i = 0; i < lstOrderIndex.Count; i++)
					{
						UpdateOrderIndex(objIData, Convert.ToString(lstOrderIndex[i]), i);
					}
				}
				objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ImageS -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, IMEISalesInfo_ImageS objIMEISalesInfo_ImageS)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colImageID, objIMEISalesInfo_ImageS.ImageID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colProductID, objIMEISalesInfo_ImageS.ProductID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colIMEI, objIMEISalesInfo_ImageS.IMEI);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colImageName, objIMEISalesInfo_ImageS.ImageName);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colDescription, objIMEISalesInfo_ImageS.Description);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colLargeSizeImage, objIMEISalesInfo_ImageS.LargeSizeImage);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colMediumSizeImage, objIMEISalesInfo_ImageS.MediumSizeImage);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colSmallSizeImage, objIMEISalesInfo_ImageS.SmallSizeImage);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colLargeSizeImageFileID, objIMEISalesInfo_ImageS.LargeSizeImageFileID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colMediumSizeImageFileID, objIMEISalesInfo_ImageS.MediumSizeImageFileID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colSmallSizeImageFileID, objIMEISalesInfo_ImageS.SmallSizeImageFileID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colIsDefault, objIMEISalesInfo_ImageS.IsDefault);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colOrderIndex, objIMEISalesInfo_ImageS.OrderIndex);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colIsActive, objIMEISalesInfo_ImageS.IsActive);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colIsSystem, objIMEISalesInfo_ImageS.IsSystem);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colUpdatedUser, objIMEISalesInfo_ImageS.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="intOrderIndex">Thứ tự</param>
		/// <returns>Kết quả trả về</returns>
		public void UpdateOrderIndex(IData objIData, string strImageID, int intOrderIndex)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATEINDEX);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colImageID, strImageID);
                objIData.AddParameter("@OrderIndex", intOrderIndex);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(IMEISalesInfo_ImageS objIMEISalesInfo_ImageS)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objIMEISalesInfo_ImageS);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ImageS -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, IMEISalesInfo_ImageS objIMEISalesInfo_ImageS)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colImageID, objIMEISalesInfo_ImageS.ImageID);
				objIData.AddParameter("@" + IMEISalesInfo_ImageS.colDeletedUser, objIMEISalesInfo_ImageS.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

       
		#endregion

		
		#region Constructor

		public DA_IMEISalesInfo_Images()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_IMEISALESINFO_IMAGES_ADD";
		public const String SP_UPDATE = "PM_IMEISALESINFO_IMAGES_UPD";
		public const String SP_DELETE = "PM_IMEISALESINFO_IMAGES_DEL";
		public const String SP_SELECT = "PM_IMEISALESINFO_IMAGES_SEL";
		public const String SP_SEARCH = "PM_IMEISALESINFO_IMAGES_SRH";
		public const String SP_UPDATEINDEX = "PM_IMEISALESINFO_IMAGES_UPDINDEX";
		#endregion
		
	}
}
