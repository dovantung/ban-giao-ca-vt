
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using StoreChangeOrder_RVList =  ERP.Inventory.BO.StoreChangeOrder_RVList;
#endregion
namespace  ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Hoang Nhu Phong 
	/// Created date 	: 9/28/2012 
	/// Bảng danh sách duyêt của  yêu cầu chuyển kho( Lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
	/// </summary>	
	public partial class DA_StoreChangeOrder_RVList
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
		/// </summary>
		/// <param name="objStoreChangeOrder_RVList">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref StoreChangeOrder_RVList objStoreChangeOrder_RVList, int intReviewLevelID, string strStoreChangeOrderID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewLevelID, intReviewLevelID);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colStoreChangeOrderID, strStoreChangeOrderID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					if (objStoreChangeOrder_RVList == null) 
 						objStoreChangeOrder_RVList = new StoreChangeOrder_RVList();
 					if (!Convert.IsDBNull(reader[StoreChangeOrder_RVList.colStoreChangeOrderID])) objStoreChangeOrder_RVList.StoreChangeOrderID = Convert.ToString(reader[StoreChangeOrder_RVList.colStoreChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrder_RVList.colOrderDate])) objStoreChangeOrder_RVList.OrderDate = Convert.ToDateTime(reader[StoreChangeOrder_RVList.colOrderDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrder_RVList.colReviewLevelID])) objStoreChangeOrder_RVList.ReviewLevelID = Convert.ToInt32(reader[StoreChangeOrder_RVList.colReviewLevelID]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrder_RVList.colReviewStatus])) objStoreChangeOrder_RVList.ReviewStatus = Convert.ToInt32(reader[StoreChangeOrder_RVList.colReviewStatus]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrder_RVList.colIsReviewed])) objStoreChangeOrder_RVList.IsReviewed = Convert.ToBoolean(reader[StoreChangeOrder_RVList.colIsReviewed]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrder_RVList.colReviewedUser])) objStoreChangeOrder_RVList.ReviewedUser = Convert.ToString(reader[StoreChangeOrder_RVList.colReviewedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrder_RVList.colReviewedDate])) objStoreChangeOrder_RVList.ReviewedDate = Convert.ToDateTime(reader[StoreChangeOrder_RVList.colReviewedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrder_RVList.colNote])) objStoreChangeOrder_RVList.Note = Convert.ToString(reader[StoreChangeOrder_RVList.colNote]).Trim();
 					objStoreChangeOrder_RVList.IsExist = true;
 				}
 				else
 				{
 					objStoreChangeOrder_RVList.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder_RVList -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
		/// </summary>
		/// <param name="objStoreChangeOrder_RVList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(StoreChangeOrder_RVList objStoreChangeOrder_RVList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objStoreChangeOrder_RVList);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder_RVList -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrder_RVList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, StoreChangeOrder_RVList objStoreChangeOrder_RVList)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colStoreChangeOrderID, objStoreChangeOrder_RVList.StoreChangeOrderID);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colOrderDate, objStoreChangeOrder_RVList.OrderDate);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewLevelID, objStoreChangeOrder_RVList.ReviewLevelID);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewStatus, objStoreChangeOrder_RVList.ReviewStatus);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colIsReviewed, objStoreChangeOrder_RVList.IsReviewed);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewedUser, objStoreChangeOrder_RVList.ReviewedUser);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewedDate, objStoreChangeOrder_RVList.ReviewedDate);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colNote, objStoreChangeOrder_RVList.Note);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
		/// </summary>
		/// <param name="objStoreChangeOrder_RVList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(StoreChangeOrder_RVList objStoreChangeOrder_RVList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objStoreChangeOrder_RVList);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder_RVList -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrder_RVList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, StoreChangeOrder_RVList objStoreChangeOrder_RVList)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colStoreChangeOrderID, objStoreChangeOrder_RVList.StoreChangeOrderID);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colOrderDate, objStoreChangeOrder_RVList.OrderDate);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewLevelID, objStoreChangeOrder_RVList.ReviewLevelID);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewStatus, objStoreChangeOrder_RVList.ReviewStatus);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colIsReviewed, objStoreChangeOrder_RVList.IsReviewed);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewedUser, objStoreChangeOrder_RVList.ReviewedUser);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewedDate, objStoreChangeOrder_RVList.ReviewedDate);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colNote, objStoreChangeOrder_RVList.Note);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
		/// </summary>
		/// <param name="objStoreChangeOrder_RVList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(StoreChangeOrder_RVList objStoreChangeOrder_RVList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objStoreChangeOrder_RVList);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder_RVList -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin bảng danh sách duyêt của  yêu cầu chuyển kho( lưu những mức đã duyệt hoặc đang trong giai đoạn xử lý)
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrder_RVList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, StoreChangeOrder_RVList objStoreChangeOrder_RVList)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colStoreChangeOrderID, objStoreChangeOrder_RVList.StoreChangeOrderID);
				objIData.AddParameter("@" + StoreChangeOrder_RVList.colReviewLevelID, objStoreChangeOrder_RVList.ReviewLevelID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_StoreChangeOrder_RVList()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_STORECHANGEORDER_RVLIST_ADD";
		public const String SP_UPDATE = "PM_STORECHANGEORDER_RVLIST_UPD";
		public const String SP_DELETE = "PM_STORECHANGEORDER_RVLIST_DEL";
		public const String SP_SELECT = "PM_STORECHANGEORDER_RVLIST_SEL";
		public const String SP_SEARCH = "PM_STORECHANGEORDER_RVLIST_SRH";
		public const String SP_UPDATEINDEX = "PM_STORECHANGEORDER_RVLIST_UPDINDEX";
		#endregion
		
	}
}
