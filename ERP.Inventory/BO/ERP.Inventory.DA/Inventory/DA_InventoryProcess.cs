
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InventoryProcess = ERP.Inventory.BO.Inventory.InventoryProcess;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
    /// Created by 		: Đặng Minh Hùng 
    /// Created date 	: 01/28/13 
    /// Quản lý yêu cầu kiểm kê
    /// </summary>	
    public partial class DA_InventoryProcess
    {



        #region Methods


        /// <summary>
        /// Nạp thông tin quản lý yêu cầu kiểm kê
        /// </summary>
        /// <param name="objInventoryProcess">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref InventoryProcess objInventoryProcess, string strInventoryProcessID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORYPROCESS_SEL");
                objIData.AddParameter("@" + InventoryProcess.colInventoryProcessID, strInventoryProcessID);
                IDataReader reader = objIData.ExecStoreToDataReader();

                if (reader.Read())
                {
                    if (objInventoryProcess == null)
                        objInventoryProcess = new InventoryProcess();

                    if (!Convert.IsDBNull(reader[InventoryProcess.colInventoryProcessID])) objInventoryProcess.InventoryProcessID = Convert.ToString(reader[InventoryProcess.colInventoryProcessID]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colInventoryID])) objInventoryProcess.InventoryID = Convert.ToString(reader[InventoryProcess.colInventoryID]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colInventoryStoreID])) objInventoryProcess.InventoryStoreID = Convert.ToInt32(reader[InventoryProcess.colInventoryStoreID]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colMainGroupID])) objInventoryProcess.MainGroupID = Convert.ToInt32(reader[InventoryProcess.colMainGroupID]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colIsNew])) objInventoryProcess.IsNew = Convert.ToBoolean(reader[InventoryProcess.colIsNew]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colInventoryTermID])) objInventoryProcess.InventoryTermID = Convert.ToInt32(reader[InventoryProcess.colInventoryTermID]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colInventoryDate])) objInventoryProcess.InventoryDate = Convert.ToDateTime(reader[InventoryProcess.colInventoryDate]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colInputContent])) objInventoryProcess.InputContent = Convert.ToString(reader[InventoryProcess.colInputContent]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colOutputContent])) objInventoryProcess.OutputContent = Convert.ToString(reader[InventoryProcess.colOutputContent]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colPrChangeContent])) objInventoryProcess.PrChangeContent = Convert.ToString(reader[InventoryProcess.colPrChangeContent]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colIsReviewed])) objInventoryProcess.IsReviewed = Convert.ToBoolean(reader[InventoryProcess.colIsReviewed]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colReviewedUser])) objInventoryProcess.ReviewedUser = Convert.ToString(reader[InventoryProcess.colReviewedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colReviewedDate])) objInventoryProcess.ReviewedDate = Convert.ToDateTime(reader[InventoryProcess.colReviewedDate]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colIsProcess])) objInventoryProcess.IsProcess = Convert.ToBoolean(reader[InventoryProcess.colIsProcess]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colProcessUser])) objInventoryProcess.ProcessUser = Convert.ToString(reader[InventoryProcess.colProcessUser]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colProcessDate])) objInventoryProcess.ProcessDate = Convert.ToDateTime(reader[InventoryProcess.colProcessDate]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colCreatedStoreID])) objInventoryProcess.CreatedStoreID = Convert.ToInt32(reader[InventoryProcess.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colCreatedUser])) objInventoryProcess.CreatedUser = Convert.ToString(reader[InventoryProcess.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colCreatedDate])) objInventoryProcess.CreatedDate = Convert.ToDateTime(reader[InventoryProcess.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colUpdatedUser])) objInventoryProcess.UpdatedUser = Convert.ToString(reader[InventoryProcess.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colUpdatedDate])) objInventoryProcess.UpdatedDate = Convert.ToDateTime(reader[InventoryProcess.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colIsDeleted])) objInventoryProcess.IsDeleted = Convert.ToBoolean(reader[InventoryProcess.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colDeletedUser])) objInventoryProcess.DeletedUser = Convert.ToString(reader[InventoryProcess.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colDeletedDate])) objInventoryProcess.DeletedDate = Convert.ToDateTime(reader[InventoryProcess.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[InventoryProcess.colDeletedReason])) objInventoryProcess.DeletedReason = Convert.ToString(reader[InventoryProcess.colDeletedReason]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryProcess.colStatusChangeContent])) objInventoryProcess.StatusChangeContent = Convert.ToString(reader[InventoryProcess.colStatusChangeContent]).Trim();

                }
                reader.Close();

                objInventoryProcess.tblInventoryProcessInput = LoadInventoryProcessInput(objIData, strInventoryProcessID);
                objInventoryProcess.tblInventoryProcessOutput = LoadInventoryProcessOutput(objIData, strInventoryProcessID);
                objInventoryProcess.tblInventoryProcessPrChange = LoadInventoryProcessPrChange(objIData, strInventoryProcessID);
                objInventoryProcess.tblInventoryProcessStChange = LoadInventoryProcessStChange(objIData, strInventoryProcessID);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin quản lý yêu cầu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin quản lý yêu cầu kiểm kê
        /// </summary>
        /// <param name="objInventoryProcess">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(InventoryProcess objInventoryProcess)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objInventoryProcess);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin quản lý yêu cầu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin quản lý yêu cầu kiểm kê
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInventoryProcess">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, InventoryProcess objInventoryProcess)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + InventoryProcess.colInventoryProcessID, objInventoryProcess.InventoryProcessID);
                objIData.AddParameter("@" + InventoryProcess.colInventoryID, objInventoryProcess.InventoryID);
                objIData.AddParameter("@" + InventoryProcess.colInventoryStoreID, objInventoryProcess.InventoryStoreID);
                objIData.AddParameter("@" + InventoryProcess.colMainGroupID, objInventoryProcess.MainGroupID);
                objIData.AddParameter("@" + InventoryProcess.colIsNew, objInventoryProcess.IsNew);
                objIData.AddParameter("@" + InventoryProcess.colInventoryTermID, objInventoryProcess.InventoryTermID);
                objIData.AddParameter("@" + InventoryProcess.colInventoryDate, objInventoryProcess.InventoryDate);
                objIData.AddParameter("@" + InventoryProcess.colInputContent, objInventoryProcess.InputContent);
                objIData.AddParameter("@" + InventoryProcess.colOutputContent, objInventoryProcess.OutputContent);
                objIData.AddParameter("@" + InventoryProcess.colPrChangeContent, objInventoryProcess.PrChangeContent);
                objIData.AddParameter("@" + InventoryProcess.colIsReviewed, objInventoryProcess.IsReviewed);
                objIData.AddParameter("@" + InventoryProcess.colReviewedUser, objInventoryProcess.ReviewedUser);
                objIData.AddParameter("@" + InventoryProcess.colReviewedDate, objInventoryProcess.ReviewedDate);
                objIData.AddParameter("@" + InventoryProcess.colIsProcess, objInventoryProcess.IsProcess);
                objIData.AddParameter("@" + InventoryProcess.colProcessUser, objInventoryProcess.ProcessUser);
                objIData.AddParameter("@" + InventoryProcess.colProcessDate, objInventoryProcess.ProcessDate);
                objIData.AddParameter("@" + InventoryProcess.colCreatedStoreID, objInventoryProcess.CreatedStoreID);
                objIData.AddParameter("@" + InventoryProcess.colCreatedUser, objInventoryProcess.CreatedUser);
                objIData.AddParameter("@" + InventoryProcess.colDeletedReason, objInventoryProcess.DeletedReason);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin quản lý yêu cầu kiểm kê
        /// </summary>
        /// <param name="objInventoryProcess">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(InventoryProcess objInventoryProcess)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objInventoryProcess);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin quản lý yêu cầu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin quản lý yêu cầu kiểm kê
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInventoryProcess">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, InventoryProcess objInventoryProcess)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + InventoryProcess.colInventoryProcessID, objInventoryProcess.InventoryProcessID);
                objIData.AddParameter("@" + InventoryProcess.colInventoryID, objInventoryProcess.InventoryID);
                objIData.AddParameter("@" + InventoryProcess.colInventoryStoreID, objInventoryProcess.InventoryStoreID);
                objIData.AddParameter("@" + InventoryProcess.colMainGroupID, objInventoryProcess.MainGroupID);
                objIData.AddParameter("@" + InventoryProcess.colIsNew, objInventoryProcess.IsNew);
                objIData.AddParameter("@" + InventoryProcess.colInventoryTermID, objInventoryProcess.InventoryTermID);
                objIData.AddParameter("@" + InventoryProcess.colInventoryDate, objInventoryProcess.InventoryDate);
                objIData.AddParameter("@" + InventoryProcess.colInputContent, objInventoryProcess.InputContent);
                objIData.AddParameter("@" + InventoryProcess.colOutputContent, objInventoryProcess.OutputContent);
                objIData.AddParameter("@" + InventoryProcess.colPrChangeContent, objInventoryProcess.PrChangeContent);
                objIData.AddParameter("@" + InventoryProcess.colIsReviewed, objInventoryProcess.IsReviewed);
                objIData.AddParameter("@" + InventoryProcess.colReviewedUser, objInventoryProcess.ReviewedUser);
                objIData.AddParameter("@" + InventoryProcess.colReviewedDate, objInventoryProcess.ReviewedDate);
                objIData.AddParameter("@" + InventoryProcess.colIsProcess, objInventoryProcess.IsProcess);
                objIData.AddParameter("@" + InventoryProcess.colProcessUser, objInventoryProcess.ProcessUser);
                objIData.AddParameter("@" + InventoryProcess.colProcessDate, objInventoryProcess.ProcessDate);
                objIData.AddParameter("@" + InventoryProcess.colCreatedStoreID, objInventoryProcess.CreatedStoreID);
                objIData.AddParameter("@" + InventoryProcess.colUpdatedUser, objInventoryProcess.UpdatedUser);
                objIData.AddParameter("@" + InventoryProcess.colDeletedReason, objInventoryProcess.DeletedReason);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin quản lý yêu cầu kiểm kê
        /// </summary>
        /// <param name="objInventoryProcess">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(InventoryProcess objInventoryProcess)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objInventoryProcess);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin quản lý yêu cầu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin quản lý yêu cầu kiểm kê
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInventoryProcess">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, InventoryProcess objInventoryProcess)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + InventoryProcess.colInventoryProcessID, objInventoryProcess.InventoryProcessID);
                objIData.AddParameter("@" + InventoryProcess.colDeletedUser, objInventoryProcess.DeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_InventoryProcess()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "InventoryPROCESS_ADD";
        public const String SP_UPDATE = "InventoryPROCESS_UPD";
        public const String SP_DELETE = "InventoryPROCESS_DEL";
        public const String SP_SELECT = "InventoryPROCESS_SEL";
        public const String SP_SEARCH = "InventoryPROCESS_SRH";
        public const String SP_UPDATEINDEX = "InventoryPROCESS_UPDINDEX";
        #endregion

    }
}
