﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
    /// Created by : Nguyễn Văn Tài
    /// Created date : 04/07/2017
    /// </summary>
    public class DA_PInvoiceProduct
    {
        #region Constructors
        public DA_PInvoiceProduct()
        { }
        #endregion

        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods
        /// <summary>
        /// thêm thông tin hóa đơn hỗ trợ hàng tồn
        /// </summary>
        /// <param name="objPInvoice"></param>
        /// <returns>objPInvoice</returns>
        public ResultMessage Insert(BO.Inventory.VatPInvoiceProduct objPInvoiceProduct)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objPInvoiceProduct, string.Empty);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin hóa đơn hỗ trợ hàng tồn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PInvoice-> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void UpdateProtect(IData objIData, string strPriceProtectId,string strVATPINVOICEID)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_PRICEPROTECT_UPD);
                objIData.AddParameter("@VATPINVOICEID", strVATPINVOICEID);
                objIData.AddParameter("@PRICEPROTECTID", strPriceProtectId);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// thêm thông tin hóa đơn hỗ trợ hàng tồn
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="objPInvoice"></param>
        public void Insert(IData objIData, BO.Inventory.VatPInvoiceProduct objPInvoiceProduct, string strPriceProtectDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoiceProduct.colVATPINVOICEID, objPInvoiceProduct.VATPINVOICEID);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoiceProduct.colQUANTITY, objPInvoiceProduct.QUANTITY);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoiceProduct.colVATINVOICE, objPInvoiceProduct.VATINVOICE);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoiceProduct.colPRICEINVOICE, objPInvoiceProduct.PRICEINVOICE);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoiceProduct.colPRODUCTID, objPInvoiceProduct.PRODUCTID);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoiceProduct.colCREATEUSER, objPInvoiceProduct.CREATEUSER);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoiceProduct.colINPUTVOUCHERDETAILID, objPInvoiceProduct.INPUTVOUCHERDETAILID);
                objIData.AddParameter("@PRICEPROTECTDETAILID", strPriceProtectDetail);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion

        #region Stored Procedure Names
        public const String SP_ADD = "VAT_PINVOICE_PRODUCT_SP_ADD";
        public const String SP_PRICEPROTECT_UPD = "PM_PRICEPROTECT_UPDVAT";
        #endregion
    }
}
