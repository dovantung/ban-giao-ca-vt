
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InventoryTerm = ERP.Inventory.BO.Inventory.InventoryTerm;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 12/8/2012 
	/// Khai báo kỳ kiểm kê
	/// </summary>	
	public partial class DA_InventoryTerm
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin khai báo kỳ kiểm kê
		/// </summary>
		/// <param name="objInventoryTerm">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(string strUserName, ref InventoryTerm objInventoryTerm, int intInventoryTermID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + InventoryTerm.colInventoryTermID, intInventoryTermID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
                    if (objInventoryTerm == null)
                        objInventoryTerm = new InventoryTerm();
                    if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryTermID])) objInventoryTerm.InventoryTermID = Convert.ToInt32(reader[InventoryTerm.colInventoryTermID]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryTypeID])) objInventoryTerm.InventoryTypeID = Convert.ToInt32(reader[InventoryTerm.colInventoryTypeID]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryTermName])) objInventoryTerm.InventoryTermName = Convert.ToString(reader[InventoryTerm.colInventoryTermName]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryTerm.colDescription])) objInventoryTerm.Description = Convert.ToString(reader[InventoryTerm.colDescription]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryDate])) objInventoryTerm.InventoryDate = Convert.ToDateTime(reader[InventoryTerm.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colBeginInventoryTime])) objInventoryTerm.BeginInventoryTime = Convert.ToDateTime(reader[InventoryTerm.colBeginInventoryTime]);
                    
                    if (!Convert.IsDBNull(reader[InventoryTerm.colLockDataTime])) objInventoryTerm.LockDataTime = Convert.ToDateTime(reader[InventoryTerm.colLockDataTime]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colIsNew])) objInventoryTerm.IsNew = Convert.ToInt32(reader[InventoryTerm.colIsNew]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colProductStatusID])) objInventoryTerm.ProductStatusID = Convert.ToInt32(reader[InventoryTerm.colProductStatusID]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryTermStatus])) objInventoryTerm.InventoryTermStatus = Convert.ToInt32(reader[InventoryTerm.colInventoryTermStatus]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colCreatedStoreID])) objInventoryTerm.CreatedStoreID = Convert.ToInt32(reader[InventoryTerm.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colCreatedUser])) objInventoryTerm.CreatedUser = Convert.ToString(reader[InventoryTerm.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryTerm.colCreatedDate])) objInventoryTerm.CreatedDate = Convert.ToDateTime(reader[InventoryTerm.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colUpdatedUser])) objInventoryTerm.UpdatedUser = Convert.ToString(reader[InventoryTerm.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryTerm.colUpdatedDate])) objInventoryTerm.UpdatedDate = Convert.ToDateTime(reader[InventoryTerm.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colIsDeleted])) objInventoryTerm.IsDeleted = Convert.ToBoolean(reader[InventoryTerm.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InventoryTerm.colDeletedUser])) objInventoryTerm.DeletedUser = Convert.ToString(reader[InventoryTerm.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryTerm.colDeletedDate])) objInventoryTerm.DeletedDate = Convert.ToDateTime(reader[InventoryTerm.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colEndInventoryTime])) objInventoryTerm.EndInventoryTime = Convert.ToDateTime(reader[InventoryTerm.colEndInventoryTime]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colIsCallStockBySchedule])) objInventoryTerm.IsCallStockBySchedule = Convert.ToBoolean(reader[InventoryTerm.colIsCallStockBySchedule]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colInv_AllowBarcode])) objInventoryTerm.Inv_AllowBarcode = Convert.ToBoolean(reader[InventoryTerm.colInv_AllowBarcode]);
                }
				reader.Close();

                if (objInventoryTerm != null)
                {
                    GetStore(strUserName, objIData, ref objInventoryTerm.dtbTerm_Store, objInventoryTerm.InventoryTermID);
                    GetMainGroup(objIData, ref objInventoryTerm.dtbTerm_MainGroup, objInventoryTerm.InventoryTermID);
                    GetBrand(objIData, ref objInventoryTerm.dtbTerm_Brand, objInventoryTerm.InventoryTermID);
                    GetSubGroup(objIData, ref objInventoryTerm.dtbTerm_SubGroup, objInventoryTerm.InventoryTermID);
                }
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin khai báo kỳ kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


        public ResultMessage LoadInfo_ByUser(string strUserName, ref InventoryTerm objInventoryTerm, int intInventoryTermID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + InventoryTerm.colInventoryTermID, intInventoryTermID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    if (objInventoryTerm == null)
                        objInventoryTerm = new InventoryTerm();
                    if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryTermID])) objInventoryTerm.InventoryTermID = Convert.ToInt32(reader[InventoryTerm.colInventoryTermID]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryTypeID])) objInventoryTerm.InventoryTypeID = Convert.ToInt32(reader[InventoryTerm.colInventoryTypeID]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryTermName])) objInventoryTerm.InventoryTermName = Convert.ToString(reader[InventoryTerm.colInventoryTermName]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryTerm.colDescription])) objInventoryTerm.Description = Convert.ToString(reader[InventoryTerm.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryDate])) objInventoryTerm.InventoryDate = Convert.ToDateTime(reader[InventoryTerm.colInventoryDate]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colBeginInventoryTime])) objInventoryTerm.BeginInventoryTime = Convert.ToDateTime(reader[InventoryTerm.colBeginInventoryTime]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colLockDataTime])) objInventoryTerm.LockDataTime = Convert.ToDateTime(reader[InventoryTerm.colLockDataTime]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colIsNew])) objInventoryTerm.IsNew = Convert.ToInt32(reader[InventoryTerm.colIsNew]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colProductStatusID])) objInventoryTerm.ProductStatusID = Convert.ToInt32(reader[InventoryTerm.colProductStatusID]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colInventoryTermStatus])) objInventoryTerm.InventoryTermStatus = Convert.ToInt32(reader[InventoryTerm.colInventoryTermStatus]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colCreatedStoreID])) objInventoryTerm.CreatedStoreID = Convert.ToInt32(reader[InventoryTerm.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colCreatedUser])) objInventoryTerm.CreatedUser = Convert.ToString(reader[InventoryTerm.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryTerm.colCreatedDate])) objInventoryTerm.CreatedDate = Convert.ToDateTime(reader[InventoryTerm.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colUpdatedUser])) objInventoryTerm.UpdatedUser = Convert.ToString(reader[InventoryTerm.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryTerm.colUpdatedDate])) objInventoryTerm.UpdatedDate = Convert.ToDateTime(reader[InventoryTerm.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colIsDeleted])) objInventoryTerm.IsDeleted = Convert.ToBoolean(reader[InventoryTerm.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colDeletedUser])) objInventoryTerm.DeletedUser = Convert.ToString(reader[InventoryTerm.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InventoryTerm.colDeletedDate])) objInventoryTerm.DeletedDate = Convert.ToDateTime(reader[InventoryTerm.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colEndInventoryTime])) objInventoryTerm.EndInventoryTime = Convert.ToDateTime(reader[InventoryTerm.colEndInventoryTime]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colIsCallStockBySchedule])) objInventoryTerm.IsCallStockBySchedule = Convert.ToBoolean(reader[InventoryTerm.colIsCallStockBySchedule]);
                    if (!Convert.IsDBNull(reader[InventoryTerm.colInv_AllowBarcode])) objInventoryTerm.Inv_AllowBarcode = Convert.ToBoolean(reader[InventoryTerm.colInv_AllowBarcode]);


                }
                reader.Close();

                if (objInventoryTerm != null)
                {
                    GetStore_ByUser(strUserName, objIData, ref objInventoryTerm.dtbTerm_Store, objInventoryTerm.InventoryTermID);
                    GetMainGroup(objIData, ref objInventoryTerm.dtbTerm_MainGroup, objInventoryTerm.InventoryTermID);
                    GetBrand(objIData, ref objInventoryTerm.dtbTerm_Brand, objInventoryTerm.InventoryTermID);
                    GetSubGroup(objIData, ref objInventoryTerm.dtbTerm_SubGroup, objInventoryTerm.InventoryTermID);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin khai báo kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


		/// <summary>
		/// Thêm thông tin khai báo kỳ kiểm kê
		/// </summary>
		/// <param name="objInventoryTerm">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InventoryTerm objInventoryTerm, string strUserName)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();

                objInventoryTerm.CreatedUser = strUserName;
				Insert(objIData, objInventoryTerm);

                InsertStore(objIData, objInventoryTerm.dtbTerm_Store, objInventoryTerm, strUserName);
                InsertMainGroup(objIData, objInventoryTerm.dtbTerm_MainGroup, objInventoryTerm);
                InsertBrand(objIData, objInventoryTerm.dtbTerm_Brand, objInventoryTerm);
                InsertSubGroup(objIData, objInventoryTerm.dtbTerm_SubGroup, objInventoryTerm);

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin khai báo kỳ kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin khai báo kỳ kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryTerm">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InventoryTerm objInventoryTerm)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InventoryTerm.colInventoryTypeID, objInventoryTerm.InventoryTypeID);
				objIData.AddParameter("@" + InventoryTerm.colInventoryTermName, objInventoryTerm.InventoryTermName);
				objIData.AddParameter("@" + InventoryTerm.colDescription, objInventoryTerm.Description);
				objIData.AddParameter("@" + InventoryTerm.colInventoryDate, objInventoryTerm.InventoryDate);
				objIData.AddParameter("@" + InventoryTerm.colBeginInventoryTime, objInventoryTerm.BeginInventoryTime);
				objIData.AddParameter("@" + InventoryTerm.colLockDataTime, objInventoryTerm.LockDataTime);
				objIData.AddParameter("@" + InventoryTerm.colProductStatusID, objInventoryTerm.ProductStatusID);
				objIData.AddParameter("@" + InventoryTerm.colInventoryTermStatus, objInventoryTerm.InventoryTermStatus);
				objIData.AddParameter("@" + InventoryTerm.colCreatedStoreID, objInventoryTerm.CreatedStoreID);
				objIData.AddParameter("@" + InventoryTerm.colCreatedUser, objInventoryTerm.CreatedUser);
				objIData.AddParameter("@" + InventoryTerm.colInv_AllowBarcode, objInventoryTerm.Inv_AllowBarcode);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + InventoryTerm.colEndInventoryTime, objInventoryTerm.EndInventoryTime);
                objIData.AddParameter("@" + InventoryTerm.colIsCallStockBySchedule, objInventoryTerm.IsCallStockBySchedule);
                objInventoryTerm.InventoryTermID = Convert.ToInt32(objIData.ExecStoreToString());
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin khai báo kỳ kiểm kê
		/// </summary>
		/// <param name="objInventoryTerm">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Update(InventoryTerm objInventoryTerm, string strUserName)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();

                objInventoryTerm.UpdatedUser = strUserName;
				Update(objIData, objInventoryTerm);

                DeleteStore(objIData, objInventoryTerm.InventoryTermID);
                InsertStore(objIData, objInventoryTerm.dtbTerm_Store, objInventoryTerm, strUserName);

                DeleteMainGroup(objIData, objInventoryTerm.InventoryTermID);
                InsertMainGroup(objIData, objInventoryTerm.dtbTerm_MainGroup, objInventoryTerm);

                DeleteBrand(objIData, objInventoryTerm.InventoryTermID);
                InsertBrand(objIData, objInventoryTerm.dtbTerm_Brand, objInventoryTerm);

                DeleteSubGroup(objIData, objInventoryTerm.InventoryTermID);
                InsertSubGroup(objIData, objInventoryTerm.dtbTerm_SubGroup, objInventoryTerm);

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin khai báo kỳ kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin khai báo kỳ kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryTerm">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InventoryTerm objInventoryTerm)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InventoryTerm.colInventoryTermID, objInventoryTerm.InventoryTermID);
				objIData.AddParameter("@" + InventoryTerm.colInventoryTypeID, objInventoryTerm.InventoryTypeID);
				objIData.AddParameter("@" + InventoryTerm.colInventoryTermName, objInventoryTerm.InventoryTermName);
				objIData.AddParameter("@" + InventoryTerm.colDescription, objInventoryTerm.Description);
				objIData.AddParameter("@" + InventoryTerm.colInventoryDate, objInventoryTerm.InventoryDate);
				objIData.AddParameter("@" + InventoryTerm.colBeginInventoryTime, objInventoryTerm.BeginInventoryTime);
				objIData.AddParameter("@" + InventoryTerm.colLockDataTime, objInventoryTerm.LockDataTime);
                objIData.AddParameter("@" + InventoryTerm.colProductStatusID, objInventoryTerm.ProductStatusID);
				objIData.AddParameter("@" + InventoryTerm.colInventoryTermStatus, objInventoryTerm.InventoryTermStatus);
				objIData.AddParameter("@" + InventoryTerm.colCreatedStoreID, objInventoryTerm.CreatedStoreID);
				objIData.AddParameter("@" + InventoryTerm.colUpdatedUser, objInventoryTerm.UpdatedUser);
                objIData.AddParameter("@" + InventoryTerm.colInv_AllowBarcode, objInventoryTerm.Inv_AllowBarcode);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);

                objIData.AddParameter("@" + InventoryTerm.colEndInventoryTime, objInventoryTerm.EndInventoryTime);
                objIData.AddParameter("@" + InventoryTerm.colIsCallStockBySchedule, objInventoryTerm.IsCallStockBySchedule);


                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin khai báo kỳ kiểm kê
		/// </summary>
		/// <param name="objInventoryTerm">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InventoryTerm objInventoryTerm)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInventoryTerm);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin khai báo kỳ kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin khai báo kỳ kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryTerm">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InventoryTerm objInventoryTerm)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InventoryTerm.colInventoryTermID, objInventoryTerm.InventoryTermID);
				objIData.AddParameter("@" + InventoryTerm.colDeletedUser, objInventoryTerm.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InventoryTerm()
		{
		}
		#endregion


		#region Stored Procedure Names

        public const String SP_ADD = "INV_INVENTORYTERM_ADD";
        public const String SP_UPDATE = "INV_INVENTORYTERM_UPD";
        public const String SP_DELETE = "INV_INVENTORYTERM_DEL";
		public const String SP_SELECT = "INV_INVENTORYTERM_SEL";
        public const String SP_SEARCH = "INV_INVENTORYTERM_SRH";
        public const String SP_SEARCH_BYUSER = "INV_INVENTORYTERM_BYUSER_SRH";
		#endregion
		
	}
}
