
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 14/06/2014 
	/// 
	/// </summary>	
	public partial class DA_QuickInventory
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin 
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, int intStoreID, string strMainGroupListID, string strInventoryUser, string strReviewedUser, 
            DateTime dtmInventoryDateFrom, DateTime dtmInventoryDateTo, DateTime dtmLockStockTimeFrom, DateTime dtmILockStockTimeTo, string strUserName, int intIsDeleted, int intIsReviewed, string strKeyword, int intType)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_QuickInventory.SP_SEARCH);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@MainGroupID", strMainGroupListID,Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.AddParameter("@InventoryUser", strInventoryUser);
                objIData.AddParameter("@ReviewedUser", strReviewedUser);
                objIData.AddParameter("@InventoryDateFrom", dtmInventoryDateFrom);
                objIData.AddParameter("@InventoryDateTo", dtmInventoryDateTo);
                objIData.AddParameter("@LockStockTimeFrom", dtmLockStockTimeFrom);
                objIData.AddParameter("@LockStockTimeTo", dtmILockStockTimeTo);
                objIData.AddParameter("@UserName", strUserName);
                objIData.AddParameter("@IsDeleted", intIsDeleted);
                objIData.AddParameter("@IsReviewed", intIsReviewed);
                objIData.AddParameter("@Keyword", strKeyword);
                objIData.AddParameter("@Type", intType);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public string GetNewID(IData objIData, int intStoreID)
        {
            string strQuickInventoryID = string.Empty;
            try
            {
                objIData.CreateNewStoredProcedure("INV_QuickInventory_NewID");
                objIData.AddParameter("@StoreID", intStoreID);
                strQuickInventoryID = objIData.ExecStoreToString();
                return strQuickInventoryID;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage GetNewID(ref string strQuickInventoryID, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_QuickInventory_NewID");
                objIData.AddParameter("@StoreID", intStoreID);
                strQuickInventoryID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi khởi tạo mã phiếu kiểm kê nhanh", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> GetNewID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInstockData(ref DataTable dtbInStockData, ref DataTable dtbInStockIMEI, DateTime dtmLockStockTime, int intStoreID, int intMainGroupID,
            int intSubGroupID, string strBrandIDList, int intIsNew, int intIsCheckRealInput)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Stock_QuickGet");
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@LockStockTime", dtmLockStockTime);
                objIData.AddParameter("@SubGroupID", intSubGroupID);
                objIData.AddParameter("@BrandIDList", strBrandIDList);
                objIData.AddParameter("@IsNew", intIsNew);
                objIData.AddParameter("@IsCheckRealInput", intIsCheckRealInput);
                DataSet dsResult = objIData.ExecStoreToDataSet("v_Out1", "v_Out2");
                dtbInStockData = dsResult.Tables[0].Copy();
                dtbInStockIMEI = dsResult.Tables[1].Copy();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu tồn kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> GetInstockData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetListBrandID(ref DataTable dtbBrand, string strQuickInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_BRAND_SRH");
                objIData.AddParameter("@QuickInventoryID", strQuickInventoryID);
                dtbBrand = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp danh sách mã phiếu kiểm kê nhanh - nhà sản xuất", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> GetListBrandID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInstockData1(ref DataTable dtbInStockData, ref DataTable dtbInStockIMEI, DateTime dtmLockStockTime, int intStoreID, string strMainGroupIDList,
           string strSubGroupIDList, string strBrandIDList, int intIsNew, int intIsCheckRealInput)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_TERM_STOCK_QUICKGET1");
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@MainGroupID", strMainGroupIDList,Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.AddParameter("@LockStockTime", dtmLockStockTime);
                objIData.AddParameter("@SubGroupID", strSubGroupIDList, Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.AddParameter("@BrandIDList", strBrandIDList, Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.AddParameter("@PRODUCTSTATUSID", intIsNew);
                objIData.AddParameter("@IsCheckRealInput", intIsCheckRealInput);
                //DataSet dsResult = objIData.ExecStoreToDataSet("v_Out1", "v_Out2");
                //dtbInStockData = dsResult.Tables[0].Copy();
                //dtbInStockIMEI = dsResult.Tables[1].Copy();
                dtbInStockData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu tồn kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> GetInstockData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objQuickInventory">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert1(BO.Inventory.QuickInventory objQuickInventory, ref string strQuickInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                strQuickInventoryID = GetNewID(objIData, objQuickInventory.CreatedStoreID);
                objQuickInventory.QuickInventoryID = strQuickInventoryID;
                Insert(objIData, objQuickInventory);

                DA_QuickInventoryDetail objDA_QuickInventoryDetail = new DA_QuickInventoryDetail();

                if (objQuickInventory.QuickInventoryDetailList != null && objQuickInventory.QuickInventoryDetailList.Count > 0)
                {
                    foreach (BO.Inventory.QuickInventoryDetail objQuickInventoryDetail in objQuickInventory.QuickInventoryDetailList)
                    {
                        objQuickInventoryDetail.QuickInventoryID = objQuickInventory.QuickInventoryID;
                        objQuickInventoryDetail.InventoryDate = objQuickInventory.InventoryDate;
                        objQuickInventoryDetail.InventoryStoreID = objQuickInventory.InventoryStoreID;
                        objQuickInventoryDetail.CreatedStoreID = objQuickInventory.CreatedStoreID;
                        objQuickInventoryDetail.CreatedUser = objQuickInventory.CreatedUser;
                        objDA_QuickInventoryDetail.Insert(objIData, objQuickInventoryDetail);
                    }
                }
                if (objQuickInventory.BrandIDList != null && objQuickInventory.BrandIDList.Count > 0)
                {
                    for (int i = 0; i < objQuickInventory.BrandIDList.Count; i++)
                    {
                        objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_BRAND_ADD");
                        objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                        objIData.AddParameter("@BrandID", objQuickInventory.BrandIDList[i]);
                        objIData.ExecNonQuery();
                    }
                }
                if (objQuickInventory.MainGroupIDList != null && objQuickInventory.MainGroupIDList.Count > 0)
                {
                    for (int i = 0; i < objQuickInventory.MainGroupIDList.Count; i++)
                    {
                        objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_MAINGRP_ADD");
                        objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                        objIData.AddParameter("@MainGroupID", objQuickInventory.MainGroupIDList[i]);
                        objIData.ExecNonQuery();
                    }
                }
                if (objQuickInventory.SubGroupIDList != null && objQuickInventory.SubGroupIDList.Count > 0)
                {
                    for (int i = 0; i < objQuickInventory.SubGroupIDList.Count; i++)
                    {
                        objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_SUBGRP_ADD");
                        objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                        objIData.AddParameter("@SubGroupID", objQuickInventory.SubGroupIDList[i]);
                        objIData.ExecNonQuery();
                    }
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objQuickInventory">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update1(BO.Inventory.QuickInventory objQuickInventory, List<BO.Inventory.QuickInventoryDetail> DeleteList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                Update(objIData, objQuickInventory);

                DA_QuickInventoryDetail objDA_QuickInventoryDetail = new DA_QuickInventoryDetail();

                if (objQuickInventory.QuickInventoryDetailList != null && objQuickInventory.QuickInventoryDetailList.Count > 0)
                {
                    foreach (BO.Inventory.QuickInventoryDetail objQuickInventoryDetail in objQuickInventory.QuickInventoryDetailList)
                    {
                        objQuickInventoryDetail.QuickInventoryID = objQuickInventory.QuickInventoryID;
                        objQuickInventoryDetail.InventoryDate = objQuickInventory.InventoryDate;
                        objQuickInventoryDetail.InventoryStoreID = objQuickInventory.InventoryStoreID;
                        objQuickInventoryDetail.CreatedStoreID = objQuickInventory.CreatedStoreID;
                        if (objQuickInventoryDetail.QuickInventoryDetailID != null && Convert.ToString(objQuickInventoryDetail.QuickInventoryDetailID).Trim() != string.Empty)
                        {
                            objQuickInventoryDetail.UpdatedUser = objQuickInventory.UpdatedUser;
                            objDA_QuickInventoryDetail.Update(objIData, objQuickInventoryDetail);
                        }
                        else
                        {
                            objQuickInventoryDetail.CreatedUser = objQuickInventory.UpdatedUser;
                            objDA_QuickInventoryDetail.Insert(objIData, objQuickInventoryDetail);
                        }
                    }
                }
                if (DeleteList != null && DeleteList.Count > 0)
                {
                    foreach (BO.Inventory.QuickInventoryDetail objDelete in DeleteList)
                    {
                        if (objDelete.QuickInventoryDetailID != null && Convert.ToString(objDelete.QuickInventoryDetailID).Trim() != string.Empty)
                        {
                            objDelete.DeletedUser = objQuickInventory.UpdatedUser;
                            objDA_QuickInventoryDetail.Delete(objIData, objDelete);
                        }
                    }
                }
                objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_BRAND_DEL");
                objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                objIData.ExecNonQuery();
                if (objQuickInventory.BrandIDList != null && objQuickInventory.BrandIDList.Count > 0)
                {
                    for (int i = 0; i < objQuickInventory.BrandIDList.Count; i++)
                    {
                        objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_BRAND_ADD");
                        objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                        objIData.AddParameter("@BrandID", objQuickInventory.BrandIDList[i]);
                        objIData.ExecNonQuery();
                    }
                }
                objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_MAINGRP_DEL");
                objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                objIData.ExecNonQuery();
                if (objQuickInventory.MainGroupIDList != null && objQuickInventory.MainGroupIDList.Count > 0)
                {
                    for (int i = 0; i < objQuickInventory.MainGroupIDList.Count; i++)
                    {
                        objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_MAINGRP_ADD");
                        objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                        objIData.AddParameter("@MainGroupID", objQuickInventory.MainGroupIDList[i]);
                        objIData.ExecNonQuery();
                    }
                }
                objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_SUBGRP_DEL");
                objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                objIData.ExecNonQuery();
                if (objQuickInventory.SubGroupIDList != null && objQuickInventory.SubGroupIDList.Count > 0)
                {
                    for (int i = 0; i < objQuickInventory.SubGroupIDList.Count; i++)
                    {
                        objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_SUBGRP_ADD");
                        objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                        objIData.AddParameter("@SubGroupID", objQuickInventory.SubGroupIDList[i]);
                        objIData.ExecNonQuery();
                    }
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public ResultMessage GetListMainGroupID(ref DataTable dtbMainGroup, string strQuickInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_MAINGRP_SRH");
                objIData.AddParameter("@QuickInventoryID", strQuickInventoryID);
                dtbMainGroup = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp danh sách mã phiếu kiểm kê nhanh - nhà sản xuất", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> GetListBrandID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public ResultMessage GetListSubGroupID(ref DataTable dtbSubGroup, string strQuickInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_SUBGRP_SRH");
                objIData.AddParameter("@QuickInventoryID", strQuickInventoryID);
                dtbSubGroup = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp danh sách mã phiếu kiểm kê nhanh - nhà sản xuất", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> GetListBrandID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion


    }
}
