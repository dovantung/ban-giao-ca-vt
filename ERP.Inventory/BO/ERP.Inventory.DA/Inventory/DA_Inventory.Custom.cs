
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
    /// Created by 		: Đặng Minh Hùng 
    /// Created date 	: 12/26/2012 
    /// Phiếu kiểm kê
    /// </summary>	
    public partial class DA_Inventory
    {

        #region Methods

        /// <summary>
        /// Tìm kiếm thông tin phiếu kiểm kê
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_Inventory.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInventoryNewID(ref string strInventoryNewID, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORY_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strInventoryNewID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi khởi tạo mã phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetInventoryNewID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public string GetInventoryNewID(IData objIData, int intStoreID)
        {
            string strInventoryNewID = string.Empty;
            try
            {
                objIData.CreateNewStoredProcedure("INV_INVENTORY_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strInventoryNewID = objIData.ExecStoreToString();
                return strInventoryNewID;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage GetListTermStock(ref DataTable dtbInStockProduct, int intInventoryTermID, int intStoreID, int intMainGroupID, int intSubGroupID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Stock_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@SubGroupID", intSubGroupID);
                dtbInStockProduct = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu tồn kho sản phẩm của kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetListTermStock", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetListTermStock(ref DataTable dtbInStockProduct,
            int intInventoryTermID, int intStoreID, string strMainGroupIDList, string strSubGroupIDList, int intProductStatusID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Stock_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@MainGroupIDList", strMainGroupIDList, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.AddParameter("@SubGroupIDList", strSubGroupIDList, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.AddParameter("@PRODUCTSTATUSID", intProductStatusID);
                dtbInStockProduct = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu tồn kho sản phẩm của kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetListTermStock", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetListTermStockIMEI(ref DataTable dtbInStockProductIMEI,
            int intInventoryTermID, int intStoreID, int intMainGroupID, int intSubGroupID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_StockIMEI_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@SubGroupID", intSubGroupID);
                dtbInStockProductIMEI = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu tồn kho IMEI của kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetListTermStockIMEI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetListTermStockIMEI(ref DataTable dtbInStockProductIMEI,
            int intInventoryTermID, int intStoreID, string strMainGroupIDList, string strSubGroupIDList, int intProductStatusID
            )
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_StockIMEI_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@MainGroupIDList", strMainGroupIDList, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.AddParameter("@SubGroupIDList", strSubGroupIDList, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.AddParameter("@PRODUCTSTATUSID", intProductStatusID);
                dtbInStockProductIMEI = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu tồn kho IMEI của kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetListTermStockIMEI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        public ResultMessage GetListTermStockIMEI_Unique(ref DataTable dtbInStockProductIMEI, int intInventoryTermID, int intStoreID, int intMainGroupID, int intSubGroupID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_StockIMEI_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@Type", 1);
                objIData.AddParameter("@SubGroupID", intSubGroupID);
                dtbInStockProductIMEI = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu tồn kho IMEI của kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetListTermStockIMEI_Unique", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage AddInventory(ERP.Inventory.BO.Inventory.Inventory objInventory, List<ERP.Inventory.BO.Inventory.InventoryDetail> objInventoryDetailList, DataTable dtbUserReviewLevel, ref string strInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                strInventoryID = GetInventoryNewID(objIData, objInventory.CreatedStoreID);
                objInventory.InventoryID = strInventoryID;

                if (!AddInventory(objIData, objInventory))
                {
                    return objResultMessage;
                }
                if (!AddInventoryDetail(objIData, strInventoryID, objInventoryDetailList))
                {
                    return objResultMessage;
                }

                if (objInventory.IsAutoReview)
                {
                    objIData.CreateNewStoredProcedure("MD_INVENTORYTYPE_RVLEVEL_SRH");
                    objIData.AddParameter("@INVENTORYTYPEID", objInventory.InventoryTypeID);
                    DataTable dtbReviewLevel = objIData.ExecStoreToDataTable();
                    string strReviewedUser = string.Empty;
                    if (dtbReviewLevel != null && dtbReviewLevel.Rows.Count > 0)
                    {
                        dtbReviewLevel.DefaultView.Sort = "ReviewSequence ASC";
                        dtbReviewLevel = dtbReviewLevel.DefaultView.ToTable();

                        for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
                        {
                            int intReviewLevelID = Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]);
                            if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["ReviewFunctionID"]))
                            {
                                string strReviewFunctionID = Convert.ToString(dtbReviewLevel.Rows[i]["ReviewFunctionID"]).Trim();
                                if (strReviewFunctionID != string.Empty)
                                {
                                    objIData.CreateNewStoredProcedure("INV_UserReviewFunction_GetList");
                                    objIData.AddParameter("@ReviewFunctionID", strReviewFunctionID);
                                    DataTable dtbUserReviewFunction = objIData.ExecStoreToDataTable();

                                    for (int j = 0; j < dtbUserReviewFunction.Rows.Count; j++)
                                    {
                                        DataRow rUserReview = dtbUserReviewFunction.Rows[j];
                                        if ((!Convert.IsDBNull(rUserReview["UserName"])) && Convert.ToString(rUserReview["UserName"]).Trim() != string.Empty)
                                        {
                                            objIData.CreateNewStoredProcedure("INV_Inventory_ReviewList_Upd");
                                            objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                                            objIData.AddParameter("@ReviewLevelID", intReviewLevelID);
                                            objIData.AddParameter("@UserName", Convert.ToString(rUserReview["UserName"]).Trim());
                                            strReviewedUser = Convert.ToString(rUserReview["UserName"]).Trim();
                                            objIData.AddParameter("@InventoryDate", objInventory.InventoryDate);
                                            objIData.AddParameter("@IsReviewed", true);
                                            objIData.AddParameter("@Note", string.Empty);
                                            objIData.AddParameter("@ReviewedUserHostAddress", objLogObject.UserHostAddress);
                                            objIData.AddParameter("@ReviewedCertificateString", objLogObject.CertificateString);
                                            objIData.AddParameter("@ReviewedLoginLogID", objLogObject.LoginLogID);
                                            objIData.ExecNonQuery();
                                        }
                                    }
                                }
                            }
                        }

                    }

                    objIData.CreateNewStoredProcedure("INV_InventoryReview_Upd");
                    objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                    objIData.AddParameter("@IsReviewed", true);
                    objIData.AddParameter("@IsReviewUnEvent", true);
                    if (strReviewedUser.Trim() == string.Empty)
                    {
                        strReviewedUser = objInventory.CreatedUser;
                    }
                    objIData.AddParameter("@ReviewUnEventUser", strReviewedUser);
                    objIData.ExecNonQuery();
                }
                else
                {
                    if (dtbUserReviewLevel != null && dtbUserReviewLevel.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbUserReviewLevel.Rows.Count; i++)
                        {
                            DataRow rUserReviewLevel = dtbUserReviewLevel.Rows[i];
                            if (Convert.ToBoolean(rUserReviewLevel["IsSelect"]))
                            {
                                objIData.CreateNewStoredProcedure("INV_Inventory_RvwLst_StatusUpd");
                                objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                                objIData.AddParameter("@ReviewLevelID", Convert.ToInt32(rUserReviewLevel["ReviewLevelID"]));
                                objIData.AddParameter("@UserName", Convert.ToString(rUserReviewLevel["UserName"]).Trim());
                                objIData.AddParameter("@InventoryDate", objInventory.InventoryDate);
                                objIData.AddParameter("@ReviewStatus", 0);
                                objIData.AddParameter("@IsReviewedInventory", false);
                                objIData.AddParameter("@Note", string.Empty);
                                objIData.AddParameter("@ReviewedUserHostAddress", objLogObject.UserHostAddress);
                                objIData.AddParameter("@ReviewedCertificateString", objLogObject.CertificateString);
                                objIData.AddParameter("@ReviewedLoginLogID", objLogObject.LoginLogID);
                                objIData.ExecNonQuery();
                            }
                        }
                    }
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm mới phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> AddInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public ResultMessage AddInventoryVTS(ERP.Inventory.BO.Inventory.Inventory objInventory, DataTable dtbInventoryDetail, ref string strInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                strInventoryID = GetInventoryNewID(objIData, objInventory.CreatedStoreID);
                objInventory.InventoryID = strInventoryID;

                if (!AddInventory(objIData, objInventory))
                {
                    return objResultMessage;
                }
                if (!AddInventoryDetailVTS(objIData, objInventory, strInventoryID, dtbInventoryDetail))
                {
                    return objResultMessage;
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm mới phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> AddInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        private bool AddInventory(IData objIData, ERP.Inventory.BO.Inventory.Inventory objInventory)
        {
            if (objInventory.InventoryID.Length == 0)
            {
                objIData.RollBackTransaction();
                return false;
            }
            try
            {

                objIData.CreateNewStoredProcedure("INV_Inventory_Add");
                objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                objIData.AddParameter("@InventoryStoreID", objInventory.InventoryStoreID);
                objIData.AddParameter("@CreatedStoreID", objInventory.CreatedStoreID);
                objIData.AddParameter("@MainGroupIDList", objInventory.MainGroupIDList, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.AddParameter("@SubGroupIDList", objInventory.SubGroupIDList, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.AddParameter("@PRODUCTSTATUSID", objInventory.ProductStatusID);
                objIData.AddParameter("@InventoryDate", objInventory.InventoryDate);
                objIData.AddParameter("@BeginInventoryTime", objInventory.BeginInventoryTime);
                objIData.AddParameter("@InventoryTermID", objInventory.InventoryTermID);
                objIData.AddParameter("@InventoryUser", objInventory.InventoryUser);
                objIData.AddParameter("@CreatedUser", objInventory.CreatedUser);
                objIData.AddParameter("@Content", objInventory.Content);
                objIData.AddParameter("@IsBCCS", objInventory.IsBCCS);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

            return true;
        }

        public ResultMessage UpdateInventory(ERP.Inventory.BO.Inventory.Inventory objInventory, List<ERP.Inventory.BO.Inventory.InventoryDetail> objInventoryDetailList, List<ERP.Inventory.BO.Inventory.InventoryDetailIMEI> lstProductIMEI_Delete)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                if (!AddInventoryDetail(objIData, objInventory.InventoryID, objInventoryDetailList))
                {
                    return objResultMessage;
                }

                for (int i = 0; i < lstProductIMEI_Delete.Count; i++)
                {
                    objIData.CreateNewStoredProcedure("INV_INVENTORYDETAILIMEI_Delete");
                    objIData.AddParameter("@InventoryDetailID", lstProductIMEI_Delete[i].InventoryDetailID);
                    objIData.AddParameter("@IMEI", lstProductIMEI_Delete[i].IMEI);
                    objIData.ExecNonQuery();
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi cập nhật phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> UpdateInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateInventory_AddReviewList(ERP.Inventory.BO.Inventory.Inventory objInventory, List<ERP.Inventory.BO.Inventory.InventoryDetail> objInventoryDetailList, List<ERP.Inventory.BO.Inventory.InventoryDetailIMEI> lstProductIMEI_Delete, DataTable dtbUserReviewLevel)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                if (!AddInventoryDetail(objIData, objInventory.InventoryID, objInventoryDetailList))
                {
                    return objResultMessage;
                }

                for (int i = 0; i < lstProductIMEI_Delete.Count; i++)
                {
                    objIData.CreateNewStoredProcedure("INV_INVENTORYDETAILIMEI_Delete");
                    objIData.AddParameter("@InventoryDetailID", lstProductIMEI_Delete[i].InventoryDetailID);
                    objIData.AddParameter("@IMEI", lstProductIMEI_Delete[i].IMEI);
                    objIData.ExecNonQuery();
                }

                if (objInventory.IsAutoReview)
                {
                    objIData.CreateNewStoredProcedure("MD_INVENTORYTYPE_RVLEVEL_SRH");
                    objIData.AddParameter("@INVENTORYTYPEID", objInventory.InventoryTypeID);
                    DataTable dtbReviewLevel = objIData.ExecStoreToDataTable();
                    string strReviewedUser = string.Empty;
                    if (dtbReviewLevel != null && dtbReviewLevel.Rows.Count > 0)
                    {
                        dtbReviewLevel.DefaultView.Sort = "ReviewSequence ASC";
                        dtbReviewLevel = dtbReviewLevel.DefaultView.ToTable();

                        for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
                        {
                            int intReviewLevelID = Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]);
                            if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["ReviewFunctionID"]))
                            {
                                string strReviewFunctionID = Convert.ToString(dtbReviewLevel.Rows[i]["ReviewFunctionID"]).Trim();
                                if (strReviewFunctionID != string.Empty)
                                {
                                    objIData.CreateNewStoredProcedure("INV_UserReviewFunction_GetList");
                                    objIData.AddParameter("@ReviewFunctionID", strReviewFunctionID);
                                    DataTable dtbUserReviewFunction = objIData.ExecStoreToDataTable();

                                    for (int j = 0; j < dtbUserReviewFunction.Rows.Count; j++)
                                    {
                                        DataRow rUserReview = dtbUserReviewFunction.Rows[j];
                                        if ((!Convert.IsDBNull(rUserReview["UserName"])) && Convert.ToString(rUserReview["UserName"]).Trim() != string.Empty)
                                        {
                                            objIData.CreateNewStoredProcedure("INV_Inventory_ReviewList_Upd");
                                            objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                                            objIData.AddParameter("@ReviewLevelID", intReviewLevelID);
                                            objIData.AddParameter("@UserName", Convert.ToString(rUserReview["UserName"]).Trim());
                                            strReviewedUser = Convert.ToString(rUserReview["UserName"]).Trim();
                                            objIData.AddParameter("@InventoryDate", objInventory.InventoryDate);
                                            objIData.AddParameter("@IsReviewed", true);
                                            objIData.AddParameter("@Note", string.Empty);
                                            objIData.AddParameter("@ReviewedUserHostAddress", objLogObject.UserHostAddress);
                                            objIData.AddParameter("@ReviewedCertificateString", objLogObject.CertificateString);
                                            objIData.AddParameter("@ReviewedLoginLogID", objLogObject.LoginLogID);
                                            objIData.ExecNonQuery();
                                        }
                                    }
                                }
                            }
                        }

                    }

                    objIData.CreateNewStoredProcedure("INV_InventoryReview_Upd");
                    objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                    objIData.AddParameter("@IsReviewed", true);
                    objIData.AddParameter("@IsReviewUnEvent", true);
                    if (strReviewedUser.Trim() == string.Empty)
                    {
                        strReviewedUser = objInventory.CreatedUser;
                    }
                    objIData.AddParameter("@ReviewUnEventUser", strReviewedUser);
                    objIData.ExecNonQuery();
                }
                else
                {
                    if (dtbUserReviewLevel != null && dtbUserReviewLevel.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbUserReviewLevel.Rows.Count; i++)
                        {
                            DataRow rUserReviewLevel = dtbUserReviewLevel.Rows[i];
                            if (Convert.ToBoolean(rUserReviewLevel["IsSelect"]))
                            {
                                objIData.CreateNewStoredProcedure("INV_Inventory_RvwLst_StatusUpd");
                                objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                                objIData.AddParameter("@ReviewLevelID", Convert.ToInt32(rUserReviewLevel["ReviewLevelID"]));
                                objIData.AddParameter("@UserName", Convert.ToString(rUserReviewLevel["UserName"]).Trim());
                                objIData.AddParameter("@InventoryDate", objInventory.InventoryDate);
                                objIData.AddParameter("@ReviewStatus", 0);
                                objIData.AddParameter("@IsReviewedInventory", false);
                                objIData.AddParameter("@Note", string.Empty);
                                objIData.AddParameter("@ReviewedUserHostAddress", objLogObject.UserHostAddress);
                                objIData.AddParameter("@ReviewedCertificateString", objLogObject.CertificateString);
                                objIData.AddParameter("@ReviewedLoginLogID", objLogObject.LoginLogID);
                                objIData.ExecNonQuery();
                            }
                        }
                    }
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi cập nhật phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> UpdateInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private bool AddInventoryDetail(IData objIData, string strInventoryID, List<ERP.Inventory.BO.Inventory.InventoryDetail> objInventoryDetailList)
        {
            try
            {
                if (objInventoryDetailList != null && objInventoryDetailList.Count > 0)
                {
                    foreach (ERP.Inventory.BO.Inventory.InventoryDetail objInventoryDetail in objInventoryDetailList)
                    {
                        objInventoryDetail.InventoryID = strInventoryID;
                        string strInventoryDetailID = string.Empty;
                        if (!AddInventoryDetail(objIData, objInventoryDetail))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }

        private bool AddInventoryDetailVTS(IData objIData, ERP.Inventory.BO.Inventory.Inventory objInventory, string strInventoryID, DataTable dtbInventoryDetail)
        {
            try
            {
                string strDetail = CreateXMLFromDetail(dtbInventoryDetail, "DETAILLST");
                objIData.CreateNewStoredProcedure("INV_INVENTORYDETAIL_ADDXML");
                objIData.AddParameter("@INVENTORYID", strInventoryID);
                objIData.AddParameter("@CREATEDSTOREID", objInventory.CreatedStoreID);
                objIData.AddParameter("@INVENTORYDATE", objInventory.InventoryDate);
                objIData.AddParameter("@INVENTORYSTOREID", objInventory.InventoryStoreID);
                objIData.AddParameter("@INVDETAILLIST", strDetail, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.AddParameter("@ISBCCS",  objInventory.IsBCCS);
                string strValue = objIData.ExecStoreToString().Trim();
                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        private bool AddInventoryDetail(IData objIData, ERP.Inventory.BO.Inventory.InventoryDetail objInventoryDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure("INV_InventoryDetail_Add");
                objIData.AddParameter("@InventoryDetailID", objInventoryDetail.InventoryDetailID);
                objIData.AddParameter("@InventoryID", objInventoryDetail.InventoryID);
                objIData.AddParameter("@InventoryDate", objInventoryDetail.InventoryDate);
                objIData.AddParameter("@InventoryStoreID", objInventoryDetail.InventoryStoreID);
                objIData.AddParameter("@CreatedStoreID", objInventoryDetail.CreatedStoreID);
                objIData.AddParameter("@ProductID", objInventoryDetail.ProductID);
                objIData.AddParameter("@Quantity", objInventoryDetail.Quantity);
                objIData.AddParameter("@Note", objInventoryDetail.Note);
                objIData.AddParameter("@PRODUCTSTATUSID", objInventoryDetail.ProductStatusID);
                objIData.AddParameter("@CabinetNumber", objInventoryDetail.CabinetNumber);
                objIData.AddParameter("@InStockQuantity", objInventoryDetail.InStockQuantity);

                objInventoryDetail.InventoryDetailID = objIData.ExecStoreToString().Trim();
                if (!AddInventoryDetailIMEI(objIData, objInventoryDetail.InventoryDetailID, objInventoryDetail.InventoryDetailIMEIList))
                {
                    return false;
                }
                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }

        private string CreateXMLFromDetail(DataTable dtb, string tableName)
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(tableName))
            {
                return string.Empty;
            }
            var sb = new System.Text.StringBuilder();
            var xtx = System.Xml.XmlWriter.Create(sb);
            xtx.WriteStartDocument();
            xtx.WriteStartElement("", tableName.ToUpper() + "LIST", "");
            foreach (DataRow dr in dtb.Rows)
            {
                xtx.WriteStartElement("", tableName.ToUpper(), "");
                foreach (DataColumn dc in dtb.Columns)
                {
                    xtx.WriteAttributeString(dc.ColumnName.ToUpper(), dr[dc.ColumnName].ToString());
                }
                xtx.WriteEndElement();
            }
            xtx.WriteEndElement();
            xtx.Flush();
            xtx.Close();
            return sb.ToString();
        }

        private bool AddInventoryDetailIMEI(IData objIData, string strInventoryDetailID, List<ERP.Inventory.BO.Inventory.InventoryDetailIMEI> objInventoryDetailIMEIList)
        {
            try
            {
                if (objInventoryDetailIMEIList != null && objInventoryDetailIMEIList.Count > 0)
                {
                    foreach (ERP.Inventory.BO.Inventory.InventoryDetailIMEI objInventoryDetailIMEI in objInventoryDetailIMEIList)
                    {
                        objInventoryDetailIMEI.InventoryDetailID = strInventoryDetailID;
                        if (!AddInventoryDetailIMEI(objIData, objInventoryDetailIMEI))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }

        private bool AddInventoryDetailIMEI(IData objIData, ERP.Inventory.BO.Inventory.InventoryDetailIMEI objInventoryDetailIMEI)
        {
            try
            {

                objIData.CreateNewStoredProcedure("INV_INVENTORYDETAILIMEI_Add");
                objIData.AddParameter("@InventoryDetailID", objInventoryDetailIMEI.InventoryDetailID);
                objIData.AddParameter("@IMEI", objInventoryDetailIMEI.IMEI);
                objIData.AddParameter("@PRODUCTSTATUSID", objInventoryDetailIMEI.ProductStatusID);
                objIData.AddParameter("@InventoryDate", objInventoryDetailIMEI.InventoryDate);
                objIData.AddParameter("@InventoryStoreID", objInventoryDetailIMEI.InventoryStoreID);
                objIData.AddParameter("@CreatedStoreID", objInventoryDetailIMEI.CreatedStoreID);
                objIData.AddParameter("@Note", objInventoryDetailIMEI.Note);
                objIData.AddParameter("@CabinetNumber", objInventoryDetailIMEI.CabinetNumber);
                objIData.AddParameter("@IsInStock", objInventoryDetailIMEI.IsInStock);
                objIData.ExecNonQuery();
                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }

        public DataTable LoadInventoryDetail(IData objIData, string strInventoryID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_InventoryDetail_List");
                objIData.AddParameter("@InventoryID", strInventoryID);
                return objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage GetInventorySearch(ref DataTable dtbInventorySearch, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORY_Search");
                objIData.AddParameter(objKeywords);
                dtbInventorySearch = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetInventorySearch", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetUserReviewFunction(ref DataTable dtbUserReviewFunction, string strReviewFunctionID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_UserReviewFunction_GetList");
                objIData.AddParameter("@ReviewFunctionID", strReviewFunctionID);
                dtbUserReviewFunction = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy danh sách người dùng duyệt phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetUserReviewFunction", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInventoryReviewLevel(ref DataTable dtbInventoryReviewLevel, string strInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_InvetoryReviewLevl_GetList");
                objIData.AddParameter("@InventoryID", strInventoryID);
                dtbInventoryReviewLevel = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy mức duyệt-người dùng duyệt phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetInventoryReviewLevel", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInventoryUserReviewed(ref DataTable dtbInventoryUserReviewed, string strReviewFunctionID, string strInventoryID, int intReviewLevelID, int intStoreID, bool bolIsCheckStorePermission)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_InvetoryUserReview_GetList");
                objIData.AddParameter("@ReviewFunctionID", strReviewFunctionID);
                objIData.AddParameter("@InventoryID", strInventoryID);
                objIData.AddParameter("@ReviewLevelID", intReviewLevelID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@IsCheckStorePermission", bolIsCheckStorePermission);
                dtbInventoryUserReviewed = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy người dùng duyệt phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetInventoryUserReviewed", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CheckExistInventory(ref string strInventoryID, int intInventoryTermID, int intInventoryStoreID, int intMainGroupID, int intProductStatusID, int intSubGroupID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_InventoryCheckExist");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@InventoryStoreID", intInventoryStoreID);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@PRODUCTSTATUSID", intProductStatusID);
                objIData.AddParameter("@SubGroupID", intSubGroupID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["InventoryID"]))
                        strInventoryID = Convert.ToString(reader["InventoryID"]).Trim();
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm tra phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> CheckExistInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateInventoryReviewStatus(ERP.Inventory.BO.Inventory.Inventory_ReviewList objInventoryReview, bool bolIsReviewedInventory)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();

                objIData.CreateNewStoredProcedure("INV_Inventory_RvwLst_StatusUpd");
                objIData.AddParameter("@InventoryID", objInventoryReview.InventoryID);
                objIData.AddParameter("@ReviewLevelID", objInventoryReview.ReviewLevelID);
                objIData.AddParameter("@UserName", objInventoryReview.UserName);
                objIData.AddParameter("@InventoryDate", objInventoryReview.InventoryDate);
                objIData.AddParameter("@ReviewStatus", objInventoryReview.ReviewStatus);
                objIData.AddParameter("@ReviewedDate", objInventoryReview.ReviewedDate);
                objIData.AddParameter("@IsReviewedInventory", bolIsReviewedInventory);
                objIData.AddParameter("@Note", objInventoryReview.Note);
                objIData.AddParameter("@ReviewedUserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@ReviewedCertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@ReviewedLoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi cập nhật nhân viên duyệt phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> UpdateInventoryReviewStatus", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage AddInventoryUnEvent(ERP.Inventory.BO.Inventory.Inventory objInventory, DataTable dtbInventoryUnEvent, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                for (int i = 0; i < dtbInventoryUnEvent.Rows.Count; i++)
                {
                    DataRow rUnEvent = dtbInventoryUnEvent.Rows[i];
                    objIData.CreateNewStoredProcedure("INV_Inventory_UnEvent_Add");
                    objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                    objIData.AddParameter("@InventoryDate", objInventory.InventoryDate);
                    objIData.AddParameter("@ProductID", rUnEvent["ProductID"]);

                    objIData.AddParameter("@StockIMEI", rUnEvent["ERP_IMEI"]);
                    decimal decStockQuantity = 0;
                    if (rUnEvent["ERP_QUANTITY"] == null || (!Convert.IsDBNull(rUnEvent["ERP_QUANTITY"])))
                    {
                        decStockQuantity = Convert.ToDecimal(rUnEvent["ERP_QUANTITY"]);
                    }
                    objIData.AddParameter("@StockQuantity", decStockQuantity);

                    objIData.AddParameter("@InventoryIMEI", rUnEvent["INV_IMEI"]);
                    decimal decInventoryQuantity = 0;
                    if (rUnEvent["INV_QUANTITY"] == null || (!Convert.IsDBNull(rUnEvent["INV_QUANTITY"])))
                    {
                        decInventoryQuantity = Convert.ToDecimal(rUnEvent["INV_QUANTITY"]);
                    }
                    objIData.AddParameter("@InventoryQuantity", decInventoryQuantity);
                    objIData.AddParameter("@UnEventQuantity", rUnEvent["Different"]);
                    objIData.AddParameter("@CreatedStoreID", objInventory.CreatedStoreID);
                    objIData.AddParameter("@CreatedUser", strUserName);
                    objIData.AddParameter("@PRODUCTSTATUSID", rUnEvent["INV_PRODUCTSTATUSID"]);
                    objIData.AddParameter("@ERP_PRODUCTSTATUSID", rUnEvent["ERP_PRODUCTSTATUSID"]);
                    objIData.ExecNonQuery();
                }

                objIData.CreateNewStoredProcedure("INV_InventoryUnEvent_Upd");
                objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                objIData.AddParameter("@IsUpdateUnEvent", true);
                objIData.AddParameter("@UpdateUnEventUser", strUserName);
                objIData.ExecNonQuery();

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi cập nhật chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> AddInventoryUnEvent", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInventoryUnEvent(ref DataTable dtbInventoryUnEvent, string strInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Inventory_UnEvent_Srh");
                objIData.AddParameter("@InventoryID", strInventoryID);
                dtbInventoryUnEvent = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetInventoryUnEvent", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateExplainUnEvent(string strInventoryID, DataTable dtbInventoryUnEvent, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                for (int i = 0; i < dtbInventoryUnEvent.Rows.Count; i++)
                {
                    DataRow rUnEvent = dtbInventoryUnEvent.Rows[i];
                    objIData.CreateNewStoredProcedure("INV_INVNTORY_UPDATEEXPLANUNEVN");
                    objIData.AddParameter("@UnEventID", rUnEvent["UnEventID"]);
                    objIData.AddParameter("@InventoryID", strInventoryID);
                    objIData.AddParameter("@ISSYSTEMERROR", rUnEvent["ISSYSTEMERROR"]);
                    objIData.AddParameter("@EXPLAINSYSTEMNOTE", rUnEvent["EXPLAINSYSTEMNOTE"]);
                    objIData.AddParameter("@UnEventExplain", rUnEvent["UnEventExplain"]);
                    objIData.AddParameter("@ExplainNote", rUnEvent["ExplainNote"]);

                    objIData.AddParameter("@QUANTITY_SALE", rUnEvent["QUANTITY_SALE"]);
                    objIData.AddParameter("@QUANTITY_INPUT", rUnEvent["QUANTITY_INPUT"]);
                    objIData.AddParameter("@QUANTITY_STORECHANGE", rUnEvent["QUANTITY_STORECHANGE"]);
                    objIData.AddParameter("@QUANTITY_OUTPUT", rUnEvent["QUANTITY_OUTPUT"]);
                    objIData.AddParameter("@ExplainUser", strUserName);
                    objIData.AddParameter("@CAUSEGROUPSLIST", rUnEvent["CAUSEGROUPSLIST"]);
                    objIData.ExecNonQuery();
                }

                objIData.CreateNewStoredProcedure("INV_InventoryUnEventExpla_Upd");
                objIData.AddParameter("@InventoryID", strInventoryID);
                objIData.AddParameter("@IsExplainUnEvent", true);
                objIData.AddParameter("@ExplainUnEventUser", strUserName);
                objIData.ExecNonQuery();

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi cập nhật giải trình chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> UpdateExplainUnEvent", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage AccessExplainUnEvent(string strInventoryID, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                objIData.CreateNewStoredProcedure("INV_INVUNEVENTEXPLAACCESS_UPD");
                objIData.AddParameter("@InventoryID", strInventoryID);
                objIData.AddParameter("@ISEXPLAINACCESS", true);
                objIData.AddParameter("@EXPLAINACCESSUSER", strUserName);
                objIData.ExecNonQuery();

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi cập nhật giải trình chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> UpdateExplainUnEvent", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInventoryHandleUnEvent(ref DataTable dtbInventoryHandleUnEvent, string strInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Invntory_HandleUnEvnt_Srh");
                objIData.AddParameter("@InventoryID", strInventoryID);
                dtbInventoryHandleUnEvent = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy xử lý chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetInventoryHandleUnEvent", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage DeleteInventory(string strInventoryID, string strContentDelete, string strDeletedUser, string strCertificateString, string strUserHostAddress, string strLoginLogID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORY_Delete");
                objIData.AddParameter("@InventoryID", strInventoryID);
                objIData.AddParameter("@DeletedNote", strContentDelete);
                objIData.AddParameter("@DeletedUser", strDeletedUser);
                objIData.AddParameter("@CertificateString", strCertificateString);
                objIData.AddParameter("@UserHostAddress", strUserHostAddress);
                objIData.AddParameter("@LoginLogID", strLoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> DeleteInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateContent(string strInventoryID, string strContent)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();

                objIData.CreateNewStoredProcedure("INV_Inventory_UpdContent");
                objIData.AddParameter("@InventoryID", strInventoryID);
                objIData.AddParameter("@Content", strContent);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi cập nhật nội dung phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> UpdateContent", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInventory(ref BO.Inventory.Inventory objInventory, string strInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Inventory_Get");
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryID, strInventoryID);
                IDataReader reader = objIData.ExecStoreToDataReader();

                if (reader.Read())
                {
                    if (objInventory == null)
                        objInventory = new BO.Inventory.Inventory();

                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryID])) objInventory.InventoryID = Convert.ToString(reader[BO.Inventory.Inventory.colInventoryID]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryTermID])) objInventory.InventoryTermID = Convert.ToInt32(reader[BO.Inventory.Inventory.colInventoryTermID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryStoreID])) objInventory.InventoryStoreID = Convert.ToInt32(reader[BO.Inventory.Inventory.colInventoryStoreID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colProductStatusID])) objInventory.ProductStatusID = Convert.ToInt32(reader[BO.Inventory.Inventory.colProductStatusID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colMainGroupID])) objInventory.MainGroupIDList = Convert.ToString(reader[BO.Inventory.Inventory.colMainGroupID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colSubGroupID])) objInventory.SubGroupIDList = Convert.ToString(reader[BO.Inventory.Inventory.colSubGroupID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsNew])) objInventory.IsNew = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsNew]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsConnected])) objInventory.IsConnected = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsConnected]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsParent])) objInventory.IsParent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsParent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryDate])) objInventory.InventoryDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colInventoryDate]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryUser])) objInventory.InventoryUser = Convert.ToString(reader[BO.Inventory.Inventory.colInventoryUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colBeginInventoryTime])) objInventory.BeginInventoryTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colBeginInventoryTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colEndInventoryTime])) objInventory.EndInventoryTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colEndInventoryTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colContent])) objInventory.Content = Convert.ToString(reader[BO.Inventory.Inventory.colContent]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsReviewed])) objInventory.IsReviewed = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsReviewed]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsUpdateUnEvent])) objInventory.IsUpdateUnEvent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsUpdateUnEvent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colUpdateUnEventUser])) objInventory.UpdateUnEventUser = Convert.ToString(reader[BO.Inventory.Inventory.colUpdateUnEventUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colUpdateUnEventTime])) objInventory.UpdateUnEventTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colUpdateUnEventTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsLock])) objInventory.IsLock = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsLock]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colLockUser])) objInventory.LockUser = Convert.ToString(reader[BO.Inventory.Inventory.colLockUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colLockTime])) objInventory.LockTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colLockTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsExplainUnEvent])) objInventory.IsExplainUnEvent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsExplainUnEvent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colExplainUnEventUser])) objInventory.ExplainUnEventUser = Convert.ToString(reader[BO.Inventory.Inventory.colExplainUnEventUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colExplainUnEventTime])) objInventory.ExplainUnEventTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colExplainUnEventTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsAdjustUnEvent])) objInventory.IsAdjustUnEvent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsAdjustUnEvent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colAdjustUnEventUser])) objInventory.AdjustUnEventUser = Convert.ToString(reader[BO.Inventory.Inventory.colAdjustUnEventUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colAdjustUnEventTime])) objInventory.AdjustUnEventTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colAdjustUnEventTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsReviewUnEvent])) objInventory.IsReviewUnEvent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsReviewUnEvent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colReviewUnEventUser])) objInventory.ReviewUnEventUser = Convert.ToString(reader[BO.Inventory.Inventory.colReviewUnEventUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colReviewUnEventTime])) objInventory.ReviewUnEventTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colReviewUnEventTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colCreatedStoreID])) objInventory.CreatedStoreID = Convert.ToInt32(reader[BO.Inventory.Inventory.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colCreatedUser])) objInventory.CreatedUser = Convert.ToString(reader[BO.Inventory.Inventory.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colCreatedDate])) objInventory.CreatedDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colUpdatedUser])) objInventory.UpdatedUser = Convert.ToString(reader[BO.Inventory.Inventory.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colUpdatedDate])) objInventory.UpdatedDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsDeleted])) objInventory.IsDeleted = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colDeletedUser])) objInventory.DeletedUser = Convert.ToString(reader[BO.Inventory.Inventory.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colDeletedDate])) objInventory.DeletedDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colDeletedNote])) objInventory.DeletedNote = Convert.ToString(reader[BO.Inventory.Inventory.colDeletedNote]).Trim();
                    if (!Convert.IsDBNull(reader["CurrentReviewLevelID"])) objInventory.CurrentReviewLevelID = Convert.ToInt32(reader["CurrentReviewLevelID"]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colReviewStatusName])) objInventory.ReviewStatusName = Convert.ToString(reader[BO.Inventory.Inventory.colReviewStatusName]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colReviewStatusID])) objInventory.ReviewStatusID = Convert.ToInt32(reader[BO.Inventory.Inventory.colReviewStatusID]);

                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsExplainAccess])) objInventory.IsExplainAccess = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsExplainAccess]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colExplainAccessUser])) objInventory.ExplainAccessUser = Convert.ToString(reader[BO.Inventory.Inventory.colExplainAccessUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colExplainAccessDate])) objInventory.ExplainAccessDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colExplainAccessDate]);
                }

                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi lấy dữ liệu phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetCheckRealQuantity(ref DataTable dtbCheckRealQuantity, int intInventoryTermID, string strInventoryID, int intStoreID, int intIsNew)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_GetCheckRealQuantity");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@InventoryID", strInventoryID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@IsNew", intIsNew);
                dtbCheckRealQuantity = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetCheckRealQuantity", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CountSimilarInventory(ref int intCountInventory, int intInventoryTermID, int intInventoryStoreID, int intMainGroupID, int intIsNew, int intSubGroupID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORY_CheckSimilar");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@StoreID", intInventoryStoreID);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@IsNew", intIsNew);
                objIData.AddParameter("@SubGroupID", intSubGroupID);
                intCountInventory = Convert.ToInt32(objIData.ExecStoreToString());
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi tính số phiếu kiểm kê trên cùng 1 kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> CountSimilarInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage JoinInventory(int intInventoryTermID, int intInventoryStoreID, int intMainGroupID, int intIsNew)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORY_JOIN");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@StoreID", intInventoryStoreID);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@IsNew", intIsNew);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nối dữ liệu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> JoinInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage JoinInventorySubGroup(int intInventoryTermID, int intInventoryStoreID, int intMainGroupID, int intIsNew, int intSubGroupID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORY_JOINSUB");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@StoreID", intInventoryStoreID);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@IsNew", intIsNew);
                objIData.AddParameter("@SubGroupID", intSubGroupID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nối dữ liệu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> JoinInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetCheckIMEI_InputStore(ref DataTable dtbCheckIMEI_InputStore, string strInIMEILIST)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_CHECKIMEI_INPUTSTORE");
                objIData.AddParameter("@InIMEILIST", strInIMEILIST);
                dtbCheckIMEI_InputStore = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu ngày nhập IMEI", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetCheckIMEI_InputStore", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetCheckIMEI_OutputStore(ref DataTable dtbCheckIMEI_OutputStore, string strOutIMEILIST)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_CHECKIMEI_OUTPUTSTORE");
                objIData.AddParameter("@OutIMEILIST", strOutIMEILIST);
                dtbCheckIMEI_OutputStore = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu ngày xuất IMEI", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetCheckIMEI_OutputStore", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetUserCanReviewAll(ref DataTable dtbUser, string strInventoryIDList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_InventoryReviewLevl_Union");
                objIData.AddParameter("@InventoryIDList", strInventoryIDList);
                dtbUser = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "danh sách người dùng có thể kiểm kê tất cả phiếu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetUserCanReviewAll", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void UpdateInventoryReviewStatus(IData objIData, ERP.Inventory.BO.Inventory.Inventory_ReviewList objInventoryReview, bool bolIsReviewedInventory)
        {
            try
            {
                objIData.CreateNewStoredProcedure("INV_Inventory_RvwLst_StatusUpd");
                objIData.AddParameter("@InventoryID", objInventoryReview.InventoryID);
                objIData.AddParameter("@ReviewLevelID", objInventoryReview.ReviewLevelID);
                objIData.AddParameter("@UserName", objInventoryReview.UserName);
                objIData.AddParameter("@InventoryDate", objInventoryReview.InventoryDate);
                objIData.AddParameter("@ReviewStatus", objInventoryReview.ReviewStatus);
                objIData.AddParameter("@ReviewedDate", objInventoryReview.ReviewedDate);
                objIData.AddParameter("@IsReviewedInventory", bolIsReviewedInventory);
                objIData.AddParameter("@Note", objInventoryReview.Note);
                objIData.AddParameter("@ReviewedUserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@ReviewedCertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@ReviewedLoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage UpdateInventoryReviewList(List<ERP.Inventory.BO.Inventory.Inventory_ReviewList> lstInventoryReview, List<bool> lstReviewedInventory)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                for (int i = 0; i < lstInventoryReview.Count; i++)
                {
                    ERP.Inventory.BO.Inventory.Inventory_ReviewList objInventoryReview = lstInventoryReview[i];
                    bool bolIsReviewed = lstReviewedInventory[i];
                    UpdateInventoryReviewStatus(objIData, objInventoryReview, bolIsReviewed);
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi cập nhật trạng thái duyệt danh sách phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> UpdateInventoryReviewList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        public ResultMessage GetHeavyData(ref DataSet dsData, string strReportStoreName, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(strReportStoreName);
                objIData.AddParameter(objKeywords);
                dsData = objIData.ExecStoreToDataSet();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy xử lý chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> GetInventoryHandleUnEvent", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion

        public ResultMessage INVProduct_SelInStock(ref DataTable dtbData,
            string strBarcode, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_PRODUCT_SELINSTOCK");
                objIData.AddParameter("@BARCODE", strBarcode);
                objIData.AddParameter("@STOREID", intStoreID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy thông tin sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> INVProduct_SelInStock", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public ResultMessage InventorySuperJoin(int inventoryTermID, int storeID, string userName, ref string newInvetoryID)
        {
            newInvetoryID = string.Empty;
            string _newInvetoryID = string.Empty;
            ResultMessage objResultMessage = new ResultMessage();
            if (!TryLockProcess(inventoryTermID, storeID))
            {
                return new ResultMessage(true, SystemError.ErrorTypes.Update, "Các phiếu kiểm kê đang bị khóa bởi một tiến trình nối phiếu khác!",
                    new Exception("Các phiếu kiểm kê đang bị khóa bởi một tiến trình nối phiếu khác!"));
            }


            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                objIData.CreateNewStoredProcedure("INV_INVENTORY_JOIN_HOPEFINAL");
                objIData.AddParameter("@InventoryTermID", inventoryTermID);
                objIData.AddParameter("@StoreID", storeID);
                objIData.AddParameter("@INVENTORYUSER", userName);
                var reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    _newInvetoryID = Convert.IsDBNull(reader["NEWINVENTORYID"]) ? string.Empty : Convert.ToString(reader["NEWINVENTORYID"]);
                    break;
                }
                objIData.CommitTransaction();
                newInvetoryID = _newInvetoryID;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nối phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> InventorySuperJoin", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
                UnlockProcess(inventoryTermID, storeID);
            }
            return objResultMessage;
        }

        public ResultMessage InventoryResolveConflict(DataTable dtbResolveData, string userName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {

                DataTable dtbXML = new DataTable();
                dtbXML.Columns.Add("PRODUCTID");
                dtbXML.Columns.Add("IMEI");
                dtbXML.Columns.Add("ISREQUESTIMEI");
                dtbXML.Columns.Add("INVENTORYID");
                dtbXML.Columns.Add("INVENTORYDETAILID");
                dtbXML.Columns.Add("PRODUCTSTATUSID");
                foreach (DataRow row in dtbResolveData.Rows)
                {
                    dtbXML.Rows.Add(Convert.ToString(row["PRODUCTID"]).Trim(), Convert.ToString(row["IMEI"]).Trim(), Convert.ToInt32(row["ISREQUESTIMEI"]), Convert.ToString(row["INVENTORYID"]).Trim(), Convert.ToString(row["INVENTORYDETAILID"]).Trim(), Convert.ToInt32(row["PRODUCTSTATUSID"]));
                }
                string strXML = CreateXMLFromDetail(dtbXML, "DETAILLIST");

                objIData.Connect();
                objIData.CreateNewStoredProcedure("INVENTORY_RESOLVECONFLICT_XML");

                objIData.AddParameter("@XML", strXML,Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.AddParameter("@USERNAME", userName);
                objIData.ExecNonQuery();


                //objIData.Connect();
                //objIData.BeginTransaction();
                //foreach (DataRow row in dtbResolveData.Rows)
                //{
                //    string productID = Convert.ToString(row["PRODUCTID"]).Trim();
                //    string imei = Convert.ToString(row["IMEI"]).Trim();
                //    bool isRequestIMEI = Convert.ToBoolean(row["ISREQUESTIMEI"]);
                //    string inventoryID = Convert.ToString(row["INVENTORYID"]).Trim();
                //    string inventoryDetailID = Convert.ToString(row["INVENTORYDETAILID"]).Trim();
                //    int productStatusID = Convert.ToInt32(row["PRODUCTSTATUSID"]);
                //    if (isRequestIMEI && string.IsNullOrEmpty(imei))
                //        throw new Exception("Lỗi xử lý conflict dữ liệu trước khi thực hiện nối phiếu kiểm kê - [Yêu cầu IMEI nhưng IMEI trống]!");

                //    objIData.CreateNewStoredProcedure("INV_INVENTORY_RESOLVECONFLICT");

                //    objIData.AddParameter("@PRODUCTID", productID);
                //    objIData.AddParameter("@IMEI", imei);
                //    objIData.AddParameter("@ISREQUESTIMEI", isRequestIMEI);
                //    objIData.AddParameter("@INVENTORYID", inventoryID);
                //    objIData.AddParameter("@INVENTORYDETAILID", inventoryDetailID);
                //    objIData.AddParameter("@PRODUCTSTATUSID", productStatusID);
                //    objIData.AddParameter("@USERNAME", userName);
                //    objIData.ExecNonQuery();
                //}
                //objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi xử lý conflict dữ liệu trước khi thực hiện nối phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> InventoryResolveConflict", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public bool TryLockProcess(int inventoryTermID, int storeId)
        {
            IData objIData = Library.DataAccess.Data.CreateData();
            int requestStatus = 0;
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORY_TRYLOCKPROCESS");
                objIData.AddParameter("@InventoryTermID", inventoryTermID);
                objIData.AddParameter("@StoreID", storeId);
                var reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    requestStatus = Convert.IsDBNull(reader["REQUESTSTATUS"]) ? 0 : Convert.ToInt32(reader["REQUESTSTATUS"]);
                    break;
                }
            }
            catch (Exception objEx)
            {
                ResultMessage objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi khóa trạng thái trước khi nối phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> TryLockProcess", InventoryGlobals.ModuleName);
                return false;
            }
            finally
            {
                objIData.Disconnect();
            }
            if (requestStatus == 0)
                return false;
            return true;
        }
        private void UnlockProcess(int inventoryTermID, int storeId)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORY_UNLOCKPROCESS");
                objIData.AddParameter("@InventoryTermID", inventoryTermID);
                objIData.AddParameter("@StoreID", storeId);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi mở khóa trạng thái sau khi nối phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> UnLockProcess", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
        }

        public ResultMessage AddInventoryAFJoin
            (
            ERP.Inventory.BO.Inventory.Inventory objInventory,
            DataTable dtbUserReviewLevel, ref string strInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                //objInventory.InventoryID = strInventoryID;
                if (objInventory.IsAutoReview)
                {
                    objIData.CreateNewStoredProcedure("MD_INVENTORYTYPE_RVLEVEL_SRH");
                    objIData.AddParameter("@INVENTORYTYPEID", objInventory.InventoryTypeID);
                    DataTable dtbReviewLevel = objIData.ExecStoreToDataTable();
                    string strReviewedUser = string.Empty;
                    if (dtbReviewLevel != null && dtbReviewLevel.Rows.Count > 0)
                    {
                        dtbReviewLevel.DefaultView.Sort = "ReviewSequence ASC";
                        dtbReviewLevel = dtbReviewLevel.DefaultView.ToTable();

                        for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
                        {
                            int intReviewLevelID = Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]);
                            if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["ReviewFunctionID"]))
                            {
                                string strReviewFunctionID = Convert.ToString(dtbReviewLevel.Rows[i]["ReviewFunctionID"]).Trim();
                                if (strReviewFunctionID != string.Empty)
                                {
                                    objIData.CreateNewStoredProcedure("INV_UserReviewFunction_GetList");
                                    objIData.AddParameter("@ReviewFunctionID", strReviewFunctionID);
                                    DataTable dtbUserReviewFunction = objIData.ExecStoreToDataTable();

                                    for (int j = 0; j < dtbUserReviewFunction.Rows.Count; j++)
                                    {
                                        DataRow rUserReview = dtbUserReviewFunction.Rows[j];
                                        if ((!Convert.IsDBNull(rUserReview["UserName"])) && Convert.ToString(rUserReview["UserName"]).Trim() != string.Empty)
                                        {
                                            objIData.CreateNewStoredProcedure("INV_Inventory_ReviewList_Upd");
                                            objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                                            objIData.AddParameter("@ReviewLevelID", intReviewLevelID);
                                            objIData.AddParameter("@UserName", Convert.ToString(rUserReview["UserName"]).Trim());
                                            strReviewedUser = Convert.ToString(rUserReview["UserName"]).Trim();
                                            objIData.AddParameter("@InventoryDate", objInventory.InventoryDate);
                                            objIData.AddParameter("@IsReviewed", true);
                                            objIData.AddParameter("@Note", string.Empty);
                                            objIData.AddParameter("@ReviewedUserHostAddress", objLogObject.UserHostAddress);
                                            objIData.AddParameter("@ReviewedCertificateString", objLogObject.CertificateString);
                                            objIData.AddParameter("@ReviewedLoginLogID", objLogObject.LoginLogID);
                                            objIData.ExecNonQuery();
                                        }
                                    }
                                }
                            }
                        }

                    }

                    objIData.CreateNewStoredProcedure("INV_InventoryReview_Upd");
                    objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                    objIData.AddParameter("@IsReviewed", true);
                    objIData.AddParameter("@IsReviewUnEvent", true);
                    if (strReviewedUser.Trim() == string.Empty)
                    {
                        strReviewedUser = objInventory.CreatedUser;
                    }
                    objIData.AddParameter("@ReviewUnEventUser", strReviewedUser);
                    objIData.ExecNonQuery();
                }
                else
                {
                    if (dtbUserReviewLevel != null && dtbUserReviewLevel.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbUserReviewLevel.Rows.Count; i++)
                        {
                            DataRow rUserReviewLevel = dtbUserReviewLevel.Rows[i];
                            if (Convert.ToBoolean(rUserReviewLevel["IsSelect"]))
                            {
                                objIData.CreateNewStoredProcedure("INV_Inventory_RvwLst_StatusUpd");
                                objIData.AddParameter("@InventoryID", objInventory.InventoryID);
                                objIData.AddParameter("@ReviewLevelID", Convert.ToInt32(rUserReviewLevel["ReviewLevelID"]));
                                objIData.AddParameter("@UserName", Convert.ToString(rUserReviewLevel["UserName"]).Trim());
                                objIData.AddParameter("@InventoryDate", objInventory.InventoryDate);
                                objIData.AddParameter("@ReviewStatus", 0);
                                objIData.AddParameter("@IsReviewedInventory", false);
                                objIData.AddParameter("@Note", string.Empty);
                                objIData.AddParameter("@ReviewedUserHostAddress", objLogObject.UserHostAddress);
                                objIData.AddParameter("@ReviewedCertificateString", objLogObject.CertificateString);
                                objIData.AddParameter("@ReviewedLoginLogID", objLogObject.LoginLogID);
                                objIData.ExecNonQuery();
                            }
                        }
                    }
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm mới phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> AddInventory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

    }
}
