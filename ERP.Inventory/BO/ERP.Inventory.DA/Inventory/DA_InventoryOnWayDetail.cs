
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using ERP.Inventory.BO.Inventory;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 05/25/2018 
	/// Chi tiết phiếu kiểm kê hàng đi đường
	/// </summary>	
	public partial class DA_InventoryOnWayDetail
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWayDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref INV_InventoryOnWayDetail objINV_InventoryOnWayDetail, string strINVENToRYONWAYDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colINVENToRYONWAYDetailID, strINVENToRYONWAYDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objINV_InventoryOnWayDetail = new INV_InventoryOnWayDetail();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colINVENToRYONWAYDetailID])) objINV_InventoryOnWayDetail.INVENToRYONWAYDetailID = Convert.ToString(reader[INV_InventoryOnWayDetail.colINVENToRYONWAYDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colStoreChangeOrderID])) objINV_InventoryOnWayDetail.StoreChangeOrderID = Convert.ToString(reader[INV_InventoryOnWayDetail.colStoreChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colProductID])) objINV_InventoryOnWayDetail.ProductID = Convert.ToString(reader[INV_InventoryOnWayDetail.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colIMEI])) objINV_InventoryOnWayDetail.IMEI = Convert.ToString(reader[INV_InventoryOnWayDetail.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colQuantity])) objINV_InventoryOnWayDetail.Quantity = Convert.ToDecimal(reader[INV_InventoryOnWayDetail.colQuantity]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colCheckQuantity])) objINV_InventoryOnWayDetail.CheckQuantity = Convert.ToDecimal(reader[INV_InventoryOnWayDetail.colCheckQuantity]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colNote])) objINV_InventoryOnWayDetail.Note = Convert.ToString(reader[INV_InventoryOnWayDetail.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colCreatedUser])) objINV_InventoryOnWayDetail.CreatedUser = Convert.ToString(reader[INV_InventoryOnWayDetail.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colCreatedDate])) objINV_InventoryOnWayDetail.CreatedDate = Convert.ToDateTime(reader[INV_InventoryOnWayDetail.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colUpdatedUser])) objINV_InventoryOnWayDetail.UpdatedUser = Convert.ToString(reader[INV_InventoryOnWayDetail.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colUpdatedDate])) objINV_InventoryOnWayDetail.UpdatedDate = Convert.ToDateTime(reader[INV_InventoryOnWayDetail.colUpdatedDate]);
 				}
 				else
 				{
 					objINV_InventoryOnWayDetail = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết phiếu kiểm kê hàng đi đường", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWayDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWayDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(INV_InventoryOnWayDetail objINV_InventoryOnWayDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objINV_InventoryOnWayDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWayDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objINV_InventoryOnWayDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, INV_InventoryOnWayDetail objINV_InventoryOnWayDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colINVENToRYONWAYDetailID, objINV_InventoryOnWayDetail.INVENToRYONWAYDetailID);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colStoreChangeOrderID, objINV_InventoryOnWayDetail.StoreChangeOrderID);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colProductID, objINV_InventoryOnWayDetail.ProductID);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colIMEI, objINV_InventoryOnWayDetail.IMEI);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colQuantity, objINV_InventoryOnWayDetail.Quantity);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colCheckQuantity, objINV_InventoryOnWayDetail.CheckQuantity);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colNote, objINV_InventoryOnWayDetail.Note);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colCreatedUser, objINV_InventoryOnWayDetail.CreatedUser);
                objIData.AddParameter("@" + INV_InventoryOnWayDetail.colOutputVoucherID, objINV_InventoryOnWayDetail.OutputVoucherID);
                objIData.AddParameter("@" + INV_InventoryOnWayDetail.colInputVoucherID, objINV_InventoryOnWayDetail.InputVoucherID);
                objIData.AddParameter("@ISINPUTSTORE", objINV_InventoryOnWayDetail.IsInputStore);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWayDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(INV_InventoryOnWayDetail objINV_InventoryOnWayDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objINV_InventoryOnWayDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWayDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objINV_InventoryOnWayDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, INV_InventoryOnWayDetail objINV_InventoryOnWayDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colINVENToRYONWAYDetailID, objINV_InventoryOnWayDetail.INVENToRYONWAYDetailID);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colStoreChangeOrderID, objINV_InventoryOnWayDetail.StoreChangeOrderID);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colProductID, objINV_InventoryOnWayDetail.ProductID);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colIMEI, objINV_InventoryOnWayDetail.IMEI);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colQuantity, objINV_InventoryOnWayDetail.Quantity);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colCheckQuantity, objINV_InventoryOnWayDetail.CheckQuantity);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colNote, objINV_InventoryOnWayDetail.Note);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colUpdatedUser, objINV_InventoryOnWayDetail.UpdatedUser);
                objIData.AddParameter("@" + INV_InventoryOnWayDetail.colOutputVoucherID, objINV_InventoryOnWayDetail.OutputVoucherID);
                objIData.AddParameter("@" + INV_InventoryOnWayDetail.colInputVoucherID, objINV_InventoryOnWayDetail.InputVoucherID);
                objIData.AddParameter("@ISINPUTSTORE", objINV_InventoryOnWayDetail.IsInputStore);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWayDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(INV_InventoryOnWayDetail objINV_InventoryOnWayDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objINV_InventoryOnWayDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWayDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objINV_InventoryOnWayDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, INV_InventoryOnWayDetail objINV_InventoryOnWayDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + INV_InventoryOnWayDetail.colINVENToRYONWAYDetailID, objINV_InventoryOnWayDetail.INVENToRYONWAYDetailID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InventoryOnWayDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "INV_InventoryOnWayDetail_ADD";
		public const String SP_UPDATE = "INV_InventoryOnWayDetail_UPD";
		public const String SP_DELETE = "INV_InventoryOnWayDetail_DEL";
		public const String SP_SELECT = "INV_InventoryOnWayDetail_SEL";
		public const String SP_SEARCH = "INV_InventoryOnWayDetail_SRH";
		public const String SP_UPDATEINDEX = "INV_InventoryOnWayDetail_UPDINDEX";
		#endregion
		
	}
}
