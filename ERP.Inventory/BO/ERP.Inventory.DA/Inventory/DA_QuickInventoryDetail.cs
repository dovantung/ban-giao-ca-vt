
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using QuickInventoryDetail = ERP.Inventory.BO.Inventory.QuickInventoryDetail;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 14/06/2014 
	/// Chi tiết phiếu kiểm kê nhanh
	/// </summary>	
	public partial class DA_QuickInventoryDetail
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết phiếu kiểm kê nhanh
		/// </summary>
		/// <param name="objQuickInventoryDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref QuickInventoryDetail objQuickInventoryDetail, string strQuickInventoryDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + QuickInventoryDetail.colQuickInventoryDetailID, strQuickInventoryDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objQuickInventoryDetail = new QuickInventoryDetail();
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colQuickInventoryDetailID])) objQuickInventoryDetail.QuickInventoryDetailID = Convert.ToString(reader[QuickInventoryDetail.colQuickInventoryDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colQuickInventoryID])) objQuickInventoryDetail.QuickInventoryID = Convert.ToString(reader[QuickInventoryDetail.colQuickInventoryID]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colInventoryDate])) objQuickInventoryDetail.InventoryDate = Convert.ToDateTime(reader[QuickInventoryDetail.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colInventoryStoreID])) objQuickInventoryDetail.InventoryStoreID = Convert.ToInt32(reader[QuickInventoryDetail.colInventoryStoreID]);
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colProductID])) objQuickInventoryDetail.ProductID = Convert.ToString(reader[QuickInventoryDetail.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[QuickInventoryDetail.colProductStatusID])) objQuickInventoryDetail.ProductStatusID = Convert.ToInt32(reader[QuickInventoryDetail.colProductStatusID]);
                    if (!Convert.IsDBNull(reader[QuickInventoryDetail.colCabinetNumber])) objQuickInventoryDetail.CabinetNumber = Convert.ToInt32(reader[QuickInventoryDetail.colCabinetNumber]);
                    
                    if (!Convert.IsDBNull(reader[QuickInventoryDetail.colQuantity])) objQuickInventoryDetail.Quantity = Convert.ToInt32(reader[QuickInventoryDetail.colQuantity]);
                    if (!Convert.IsDBNull(reader[QuickInventoryDetail.colInstockQuantity])) objQuickInventoryDetail.InstockQuantity = Convert.ToInt32(reader[QuickInventoryDetail.colInstockQuantity]);
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colNote])) objQuickInventoryDetail.Note = Convert.ToString(reader[QuickInventoryDetail.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colCreatedStoreID])) objQuickInventoryDetail.CreatedStoreID = Convert.ToInt32(reader[QuickInventoryDetail.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colCreatedUser])) objQuickInventoryDetail.CreatedUser = Convert.ToString(reader[QuickInventoryDetail.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colCreatedDate])) objQuickInventoryDetail.CreatedDate = Convert.ToDateTime(reader[QuickInventoryDetail.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colUpdatedUser])) objQuickInventoryDetail.UpdatedUser = Convert.ToString(reader[QuickInventoryDetail.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colUpdatedDate])) objQuickInventoryDetail.UpdatedDate = Convert.ToDateTime(reader[QuickInventoryDetail.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colIsDeleted])) objQuickInventoryDetail.IsDeleted = Convert.ToBoolean(reader[QuickInventoryDetail.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colDeletedUser])) objQuickInventoryDetail.DeletedUser = Convert.ToString(reader[QuickInventoryDetail.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colDeletedDate])) objQuickInventoryDetail.DeletedDate = Convert.ToDateTime(reader[QuickInventoryDetail.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[QuickInventoryDetail.colDeletedNote])) objQuickInventoryDetail.DeletedNote = Convert.ToString(reader[QuickInventoryDetail.colDeletedNote]).Trim();
 				}
 				else
 				{
 					objQuickInventoryDetail = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết phiếu kiểm kê nhanh", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventoryDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết phiếu kiểm kê nhanh
		/// </summary>
		/// <param name="objQuickInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(QuickInventoryDetail objQuickInventoryDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objQuickInventoryDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết phiếu kiểm kê nhanh", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventoryDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết phiếu kiểm kê nhanh
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objQuickInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, QuickInventoryDetail objQuickInventoryDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + QuickInventoryDetail.colQuickInventoryID, objQuickInventoryDetail.QuickInventoryID);
				//objIData.AddParameter("@" + QuickInventoryDetail.colInventoryDate, objQuickInventoryDetail.InventoryDate);
				objIData.AddParameter("@" + QuickInventoryDetail.colInventoryStoreID, objQuickInventoryDetail.InventoryStoreID);
				objIData.AddParameter("@" + QuickInventoryDetail.colProductID, objQuickInventoryDetail.ProductID);
                objIData.AddParameter("@" + QuickInventoryDetail.colProductStatusID, objQuickInventoryDetail.ProductStatusID);
				objIData.AddParameter("@" + QuickInventoryDetail.colCabinetNumber, objQuickInventoryDetail.CabinetNumber);
				objIData.AddParameter("@" + QuickInventoryDetail.colQuantity, objQuickInventoryDetail.Quantity);
				objIData.AddParameter("@" + QuickInventoryDetail.colInstockQuantity, objQuickInventoryDetail.InstockQuantity);
				objIData.AddParameter("@" + QuickInventoryDetail.colNote, objQuickInventoryDetail.Note);
				objIData.AddParameter("@" + QuickInventoryDetail.colCreatedStoreID, objQuickInventoryDetail.CreatedStoreID);
				objIData.AddParameter("@" + QuickInventoryDetail.colCreatedUser, objQuickInventoryDetail.CreatedUser);
				objIData.AddParameter("@" + QuickInventoryDetail.colDeletedNote, objQuickInventoryDetail.DeletedNote);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        public void Insert(IData objIData, DataRow rDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + QuickInventoryDetail.colQuickInventoryID, rDetail["QuickInventoryID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colInventoryDate, rDetail["InventoryDate"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colInventoryStoreID, rDetail["InventoryStoreID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colProductID, rDetail["ProductID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colProductStatusID, rDetail["ProductStatusID"] );
                objIData.AddParameter("@" + QuickInventoryDetail.colCabinetNumber, rDetail["CabinetNumber"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colQuantity, rDetail["Quantity"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colInstockQuantity, rDetail["InstockQuantity"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colNote, rDetail["Note"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colCreatedStoreID, rDetail["CreatedStoreID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colCreatedUser, rDetail["CreatedUser"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colDeletedNote, rDetail["DeletedNote"]);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu kiểm kê nhanh
		/// </summary>
		/// <param name="objQuickInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(QuickInventoryDetail objQuickInventoryDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objQuickInventoryDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết phiếu kiểm kê nhanh", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventoryDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu kiểm kê nhanh
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objQuickInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, QuickInventoryDetail objQuickInventoryDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + QuickInventoryDetail.colQuickInventoryDetailID, objQuickInventoryDetail.QuickInventoryDetailID);
				objIData.AddParameter("@" + QuickInventoryDetail.colQuickInventoryID, objQuickInventoryDetail.QuickInventoryID);
				objIData.AddParameter("@" + QuickInventoryDetail.colInventoryDate, objQuickInventoryDetail.InventoryDate);
				objIData.AddParameter("@" + QuickInventoryDetail.colInventoryStoreID, objQuickInventoryDetail.InventoryStoreID);
				objIData.AddParameter("@" + QuickInventoryDetail.colProductID, objQuickInventoryDetail.ProductID);
                objIData.AddParameter("@" + QuickInventoryDetail.colProductStatusID, objQuickInventoryDetail.ProductStatusID);
				objIData.AddParameter("@" + QuickInventoryDetail.colCabinetNumber, objQuickInventoryDetail.CabinetNumber);
				objIData.AddParameter("@" + QuickInventoryDetail.colQuantity, objQuickInventoryDetail.Quantity);
				objIData.AddParameter("@" + QuickInventoryDetail.colInstockQuantity, objQuickInventoryDetail.InstockQuantity);
				objIData.AddParameter("@" + QuickInventoryDetail.colNote, objQuickInventoryDetail.Note);
				objIData.AddParameter("@" + QuickInventoryDetail.colCreatedStoreID, objQuickInventoryDetail.CreatedStoreID);
				objIData.AddParameter("@" + QuickInventoryDetail.colUpdatedUser, objQuickInventoryDetail.UpdatedUser);
				objIData.AddParameter("@" + QuickInventoryDetail.colDeletedNote, objQuickInventoryDetail.DeletedNote);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
        public void Update(IData objIData, DataRow rDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + QuickInventoryDetail.colQuickInventoryDetailID, rDetail["QuickInventoryDetailID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colQuickInventoryID, rDetail["QuickInventoryID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colInventoryDate, rDetail["InventoryDate"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colInventoryStoreID, rDetail["InventoryStoreID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colProductID, rDetail["ProductID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colProductStatusID, rDetail["ProductStatusID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colCabinetNumber, rDetail["CabinetNumber"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colQuantity, rDetail["Quantity"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colInstockQuantity, rDetail["InstockQuantity"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colNote, rDetail["Note"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colCreatedStoreID, rDetail["CreatedStoreID"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colUpdatedUser, rDetail["UpdatedUser"]);
                objIData.AddParameter("@" + QuickInventoryDetail.colDeletedNote, rDetail["DeletedNote"]);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

		/// <summary>
		/// Xóa thông tin chi tiết phiếu kiểm kê nhanh
		/// </summary>
		/// <param name="objQuickInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(QuickInventoryDetail objQuickInventoryDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objQuickInventoryDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết phiếu kiểm kê nhanh", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventoryDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết phiếu kiểm kê nhanh
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objQuickInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, QuickInventoryDetail objQuickInventoryDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + QuickInventoryDetail.colQuickInventoryDetailID, objQuickInventoryDetail.QuickInventoryDetailID);
				objIData.AddParameter("@" + QuickInventoryDetail.colDeletedUser, objQuickInventoryDetail.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_QuickInventoryDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "INV_QUICKINVENTORYDETAIL_ADD";
		public const String SP_UPDATE = "INV_QUICKINVENTORYDETAIL_UPD";
		public const String SP_DELETE = "INV_QUICKINVENTORYDETAIL_DEL";
		public const String SP_SELECT = "INV_QUICKINVENTORYDETAIL_SEL";
		public const String SP_SEARCH = "INV_QUICKINVENTORYDETAIL_SRH";
		//public const String SP_UPDATEINDEX = "INV_QUICKINVENTORYDETAIL_UPDINDEX";
		#endregion
		
	}
}
