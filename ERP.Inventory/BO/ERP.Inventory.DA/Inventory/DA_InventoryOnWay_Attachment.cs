
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using ERP.Inventory.BO.Inventory;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 05/25/2018 
	/// Danh sách file đính kèm - Phiếu kiểm kê hàng đi đường
	/// </summary>	
	public partial class DA_InventoryOnWay_Attachment
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWay_AttachMent">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent, string strINVENToRYONWAYAttachMENTID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID, strINVENToRYONWAYAttachMENTID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objINV_InventoryOnWay_AttachMent = new INV_InventoryOnWay_AttachMent();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID])) objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colStoreChangeOrderID])) objINV_InventoryOnWay_AttachMent.StoreChangeOrderID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colStoreChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colAttachMENTName])) objINV_InventoryOnWay_AttachMent.AttachMENTName = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colAttachMENTName]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colAttachMENTPath])) objINV_InventoryOnWay_AttachMent.AttachMENTPath = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colAttachMENTPath]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDescription])) objINV_InventoryOnWay_AttachMent.Description = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colDescription]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colCreatedUser])) objINV_InventoryOnWay_AttachMent.CreatedUser = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colCreatedDate])) objINV_InventoryOnWay_AttachMent.CreatedDate = Convert.ToDateTime(reader[INV_InventoryOnWay_AttachMent.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colIsDeleted])) objINV_InventoryOnWay_AttachMent.IsDeleted = Convert.ToBoolean(reader[INV_InventoryOnWay_AttachMent.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDeletedUser])) objINV_InventoryOnWay_AttachMent.DeletedUser = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDeletedDate])) objINV_InventoryOnWay_AttachMent.DeletedDate = Convert.ToDateTime(reader[INV_InventoryOnWay_AttachMent.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colFileID])) objINV_InventoryOnWay_AttachMent.FileID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colFileID]).Trim();
 				}
 				else
 				{
 					objINV_InventoryOnWay_AttachMent = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay_AttachMent -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWay_AttachMent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objINV_InventoryOnWay_AttachMent);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay_AttachMent -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objINV_InventoryOnWay_AttachMent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID, objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colStoreChangeOrderID, objINV_InventoryOnWay_AttachMent.StoreChangeOrderID);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colAttachMENTName, objINV_InventoryOnWay_AttachMent.AttachMENTName);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colAttachMENTPath, objINV_InventoryOnWay_AttachMent.AttachMENTPath);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colDescription, objINV_InventoryOnWay_AttachMent.Description);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colCreatedUser, objINV_InventoryOnWay_AttachMent.CreatedUser);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colFileID, objINV_InventoryOnWay_AttachMent.FileID);
                objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colOutputVoucherID, objINV_InventoryOnWay_AttachMent.OutputVoucherID);
                objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colInputVoucherID, objINV_InventoryOnWay_AttachMent.InputVoucherID);
                objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colIsInputStore, objINV_InventoryOnWay_AttachMent.IsInputStore);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWay_AttachMent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objINV_InventoryOnWay_AttachMent);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay_AttachMent -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objINV_InventoryOnWay_AttachMent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID, objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colStoreChangeOrderID, objINV_InventoryOnWay_AttachMent.StoreChangeOrderID);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colAttachMENTName, objINV_InventoryOnWay_AttachMent.AttachMENTName);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colAttachMENTPath, objINV_InventoryOnWay_AttachMent.AttachMENTPath);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colDescription, objINV_InventoryOnWay_AttachMent.Description);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colFileID, objINV_InventoryOnWay_AttachMent.FileID);
                objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colIsInputStore, objINV_InventoryOnWay_AttachMent.IsInputStore);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWay_AttachMent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objINV_InventoryOnWay_AttachMent);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay_AttachMent -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objINV_InventoryOnWay_AttachMent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID, objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID);
				objIData.AddParameter("@" + INV_InventoryOnWay_AttachMent.colDeletedUser, objINV_InventoryOnWay_AttachMent.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InventoryOnWay_Attachment()
		{
		}
		#endregion


		#region Stored Procedure Names

        public const String SP_ADD = "INVENTORYONWAY_ATTACHMENT_ADD";
		public const String SP_UPDATE = "InventoryOnWay_AttachMent_UPD";
		public const String SP_DELETE = "InventoryOnWay_AttachMent_DEL";
		public const String SP_SELECT = "InventoryOnWay_AttachMent_SEL";
		public const String SP_SEARCH = "InventoryOnWay_AttachMent_SRH";
		public const String SP_UPDATEINDEX = "INV_InventoryOnWay_AttachMent_UPDINDEX";
		#endregion
		
	}
}
