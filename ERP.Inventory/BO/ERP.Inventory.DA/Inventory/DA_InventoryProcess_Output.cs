
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InventoryProcess_Output = ERP.Inventory.BO.Inventory.InventoryProcess_Output;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 01/28/13 
	/// Chi tiết quản lý yêu cầu kiểm kê - xuất thiếu
	/// </summary>	
	public partial class DA_InventoryProcess_Output
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu
		/// </summary>
		/// <param name="objInventoryProcess_Output">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InventoryProcess_Output objInventoryProcess_Output, string strOutputProcessID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputProcessID, strOutputProcessID);
				IDataReader reader = objIData.ExecStoreToDataReader();

				if (reader.Read())
 				{
                    if (objInventoryProcess_Output == null)
                        objInventoryProcess_Output = new InventoryProcess_Output();

 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colOutputProcessID])) objInventoryProcess_Output.OutputProcessID = Convert.ToString(reader[InventoryProcess_Output.colOutputProcessID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colInventoryProcessID])) objInventoryProcess_Output.InventoryProcessID = Convert.ToString(reader[InventoryProcess_Output.colInventoryProcessID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colInventoryDate])) objInventoryProcess_Output.InventoryDate = Convert.ToDateTime(reader[InventoryProcess_Output.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colProductID])) objInventoryProcess_Output.ProductID = Convert.ToString(reader[InventoryProcess_Output.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colIMEI])) objInventoryProcess_Output.IMEI = Convert.ToString(reader[InventoryProcess_Output.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colQuantity])) objInventoryProcess_Output.Quantity = Convert.ToDecimal(reader[InventoryProcess_Output.colQuantity]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colVAT])) objInventoryProcess_Output.VAT = Convert.ToInt32(reader[InventoryProcess_Output.colVAT]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colVATPercent])) objInventoryProcess_Output.VATPercent = Convert.ToInt32(reader[InventoryProcess_Output.colVATPercent]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colOutputPrice])) objInventoryProcess_Output.OutputPrice = Convert.ToDecimal(reader[InventoryProcess_Output.colOutputPrice]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colCollectArrearPrice])) objInventoryProcess_Output.CollectArrearPrice = Convert.ToDecimal(reader[InventoryProcess_Output.colCollectArrearPrice]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colUnEventID])) objInventoryProcess_Output.UnEventID = Convert.ToString(reader[InventoryProcess_Output.colUnEventID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colOutputVoucherID])) objInventoryProcess_Output.OutputVoucherID = Convert.ToString(reader[InventoryProcess_Output.colOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colOutputVoucherDetailID])) objInventoryProcess_Output.OutputVoucherDetailID = Convert.ToString(reader[InventoryProcess_Output.colOutputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colCreatedStoreID])) objInventoryProcess_Output.CreatedStoreID = Convert.ToInt32(reader[InventoryProcess_Output.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colCreatedUser])) objInventoryProcess_Output.CreatedUser = Convert.ToString(reader[InventoryProcess_Output.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colCreatedDate])) objInventoryProcess_Output.CreatedDate = Convert.ToDateTime(reader[InventoryProcess_Output.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colUpdatedUser])) objInventoryProcess_Output.UpdatedUser = Convert.ToString(reader[InventoryProcess_Output.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colUpdatedDate])) objInventoryProcess_Output.UpdatedDate = Convert.ToDateTime(reader[InventoryProcess_Output.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colIsDeleted])) objInventoryProcess_Output.IsDeleted = Convert.ToBoolean(reader[InventoryProcess_Output.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colDeletedUser])) objInventoryProcess_Output.DeletedUser = Convert.ToString(reader[InventoryProcess_Output.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colDeletedDate])) objInventoryProcess_Output.DeletedDate = Convert.ToDateTime(reader[InventoryProcess_Output.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Output.colDeletedReason])) objInventoryProcess_Output.DeletedReason = Convert.ToString(reader[InventoryProcess_Output.colDeletedReason]).Trim();
 				}

				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_Output -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu
		/// </summary>
		/// <param name="objInventoryProcess_Output">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InventoryProcess_Output objInventoryProcess_Output)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInventoryProcess_Output);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_Output -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryProcess_Output">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InventoryProcess_Output objInventoryProcess_Output)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputProcessID, objInventoryProcess_Output.OutputProcessID);
				objIData.AddParameter("@" + InventoryProcess_Output.colInventoryProcessID, objInventoryProcess_Output.InventoryProcessID);
				objIData.AddParameter("@" + InventoryProcess_Output.colInventoryDate, objInventoryProcess_Output.InventoryDate);
				objIData.AddParameter("@" + InventoryProcess_Output.colProductID, objInventoryProcess_Output.ProductID);
				objIData.AddParameter("@" + InventoryProcess_Output.colIMEI, objInventoryProcess_Output.IMEI);
				objIData.AddParameter("@" + InventoryProcess_Output.colQuantity, objInventoryProcess_Output.Quantity);
				objIData.AddParameter("@" + InventoryProcess_Output.colVAT, objInventoryProcess_Output.VAT);
				objIData.AddParameter("@" + InventoryProcess_Output.colVATPercent, objInventoryProcess_Output.VATPercent);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputPrice, objInventoryProcess_Output.OutputPrice);
				objIData.AddParameter("@" + InventoryProcess_Output.colCollectArrearPrice, objInventoryProcess_Output.CollectArrearPrice);
				objIData.AddParameter("@" + InventoryProcess_Output.colUnEventID, objInventoryProcess_Output.UnEventID);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputVoucherID, objInventoryProcess_Output.OutputVoucherID);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputVoucherDetailID, objInventoryProcess_Output.OutputVoucherDetailID);
				objIData.AddParameter("@" + InventoryProcess_Output.colCreatedStoreID, objInventoryProcess_Output.CreatedStoreID);
				objIData.AddParameter("@" + InventoryProcess_Output.colCreatedUser, objInventoryProcess_Output.CreatedUser);
				objIData.AddParameter("@" + InventoryProcess_Output.colDeletedReason, objInventoryProcess_Output.DeletedReason);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu
		/// </summary>
		/// <param name="objInventoryProcess_Output">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InventoryProcess_Output objInventoryProcess_Output)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInventoryProcess_Output);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_Output -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryProcess_Output">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InventoryProcess_Output objInventoryProcess_Output)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputProcessID, objInventoryProcess_Output.OutputProcessID);
				objIData.AddParameter("@" + InventoryProcess_Output.colInventoryProcessID, objInventoryProcess_Output.InventoryProcessID);
				objIData.AddParameter("@" + InventoryProcess_Output.colInventoryDate, objInventoryProcess_Output.InventoryDate);
				objIData.AddParameter("@" + InventoryProcess_Output.colProductID, objInventoryProcess_Output.ProductID);
				objIData.AddParameter("@" + InventoryProcess_Output.colIMEI, objInventoryProcess_Output.IMEI);
				objIData.AddParameter("@" + InventoryProcess_Output.colQuantity, objInventoryProcess_Output.Quantity);
				objIData.AddParameter("@" + InventoryProcess_Output.colVAT, objInventoryProcess_Output.VAT);
				objIData.AddParameter("@" + InventoryProcess_Output.colVATPercent, objInventoryProcess_Output.VATPercent);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputPrice, objInventoryProcess_Output.OutputPrice);
				objIData.AddParameter("@" + InventoryProcess_Output.colCollectArrearPrice, objInventoryProcess_Output.CollectArrearPrice);
				objIData.AddParameter("@" + InventoryProcess_Output.colUnEventID, objInventoryProcess_Output.UnEventID);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputVoucherID, objInventoryProcess_Output.OutputVoucherID);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputVoucherDetailID, objInventoryProcess_Output.OutputVoucherDetailID);
				objIData.AddParameter("@" + InventoryProcess_Output.colCreatedStoreID, objInventoryProcess_Output.CreatedStoreID);
				objIData.AddParameter("@" + InventoryProcess_Output.colUpdatedUser, objInventoryProcess_Output.UpdatedUser);
				objIData.AddParameter("@" + InventoryProcess_Output.colDeletedReason, objInventoryProcess_Output.DeletedReason);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu
		/// </summary>
		/// <param name="objInventoryProcess_Output">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InventoryProcess_Output objInventoryProcess_Output)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInventoryProcess_Output);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_Output -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết quản lý yêu cầu kiểm kê - xuất thiếu
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryProcess_Output">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InventoryProcess_Output objInventoryProcess_Output)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InventoryProcess_Output.colOutputProcessID, objInventoryProcess_Output.OutputProcessID);
				objIData.AddParameter("@" + InventoryProcess_Output.colDeletedUser, objInventoryProcess_Output.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InventoryProcess_Output()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "INV_INVENTORYPROCESS_OUTPUT_ADD";
		public const String SP_UPDATE = "INV_INVENTORYPROCESS_OUTPUT_UPD";
		public const String SP_DELETE = "INV_INVENTORYPROCESS_OUTPUT_DEL";
		public const String SP_SELECT = "INV_INVENTORYPROCESS_OUTPUT_SEL";
		public const String SP_SEARCH = "INV_INVENTORYPROCESS_OUTPUT_SRH";
		public const String SP_UPDATEINDEX = "INV_INVENTORYPROCESS_OUTPUT_UPDINDEX";
		#endregion
		
	}
}
