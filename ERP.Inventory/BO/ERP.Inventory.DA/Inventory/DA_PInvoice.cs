﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
    /// Created by : Nguyễn Văn Tài
    /// Created date : 04/07/2017
    /// </summary>
    public class DA_PInvoice
    {
        #region Constructors
        public DA_PInvoice()
        {}
        #endregion

        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods
        /// <summary>
        /// thêm thông tin hóa đơn hỗ trợ hàng tồn
        /// </summary>
        /// <param name="objPInvoice"></param>
        /// <returns>objPInvoice</returns>
        public ResultMessage Insert(ref BO.Inventory.VatPInvoice objPInvoice, string[] arrPriceProtectId)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                Insert(objIData, ref objPInvoice);
                List<string> lstPriceProtectDetail = new List<string>();
                foreach (var item in arrPriceProtectId)
                {
                    new DA_PInvoiceProduct().UpdateProtect(objIData, item, objPInvoice.VATPINVOICEID);
                    DataTable dtbPriceProtectDetail = GetPriceProtectDetail(objIData, item);
                    if (dtbPriceProtectDetail != null && dtbPriceProtectDetail.Rows.Count > 0)
                    {
                        foreach (DataRow item2 in dtbPriceProtectDetail.Rows)
                        {
                            lstPriceProtectDetail.Add(item2["PricePROTECTDetailID"].ToString());
                        }
                    }
                }
                int i = 0;
                foreach (var item in objPInvoice.PInvoiceProductLst)
                {
                    item.VATPINVOICEID = objPInvoice.VATPINVOICEID;
                    item.CREATEUSER = objPInvoice.CREATEUSER;
                    string strPriceProtectDetail = string.Empty;
                    if (lstPriceProtectDetail.Count > 0 && lstPriceProtectDetail.Count >= i)
                    {
                        strPriceProtectDetail = lstPriceProtectDetail[i].ToString();
                        i++;
                    }
                    new DA_PInvoiceProduct().Insert(objIData, item, strPriceProtectDetail);
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin hóa đơn hỗ trợ hàng tồn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PInvoice-> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private DataTable GetPriceProtectDetail(IData objIData, string item)
        {
            objIData.CreateNewStoredProcedure("PM_PRICEPROTECTDETAIL_SRH");
            objIData.AddParameter(new object[] { "@PRICEPROTECTID", item });
            DataTable dtbData = objIData.ExecStoreToDataTable();
            return dtbData;
        }

        public ResultMessage SearchData(ref DataTable dtbData,object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin hóa đơn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PInvoice -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// thêm thông tin hóa đơn hỗ trợ hàng tồn
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="objPInvoice"></param>
        public void Insert(IData objIData,ref BO.Inventory.VatPInvoice objPInvoice)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colINVOICETYPE, objPInvoice.INVOICETYPE);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colINVOICENO, objPInvoice.INVOICENO);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colINVOICESYMBOL, objPInvoice.INVOICESYMBOL);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colINVOICEDATE, objPInvoice.INVOICEDATE);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colPAYABLETYPEID, objPInvoice.PAYABLETYPEID);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colDESCRIPTION, objPInvoice.DESCRIPTION);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colCURRENCYUNIT, objPInvoice.CURRENCYUNIT);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colCURRENCYEXCHANGE, objPInvoice.CURRENCYEXCHANGE);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colCUSTOMERID, objPInvoice.CUSTOMERID);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colCREATEUSER, objPInvoice.CREATEUSER);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colINVOICETRANSTYPEID, objPInvoice.INVOICETRANSTYPEID);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colTYPEID, objPInvoice.TYPEID);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colDENOMINATOR, objPInvoice.DENOMINATOR);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colAMOUNTBF, objPInvoice.AMOUNTBF);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colTOTALAMOUNT, objPInvoice.TOTALAMOUNT);
                objIData.AddParameter("@" + BO.Inventory.VatPInvoice.colVATAMOUNT, objPInvoice.VATAMOUNT);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objPInvoice.VATPINVOICEID = objIData.ExecStoreToDataTable().Rows[0][0].ToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion

        #region Stored Procedure Names
        public const String SP_ADD = "VAT_PINVOICE_SP_ADD";
        public const string SP_SEARCH = "PM_PRICEPROTECT_SELVALUE";
        #endregion
    }
}
