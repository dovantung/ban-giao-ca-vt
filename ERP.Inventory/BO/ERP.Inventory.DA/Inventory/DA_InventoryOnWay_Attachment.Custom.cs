
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO.Inventory;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 05/25/2018 
	/// Danh sách file đính kèm - Phiếu kiểm kê hàng đi đường
	/// </summary>	
	public partial class DA_InventoryOnWay_Attachment
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_InventoryOnWay_Attachment.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_INVENToRYONWAY_AttachMENT -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public ResultMessage SearchDataToList(ref List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                lstINV_InventoryOnWay_AttachMent.Clear();
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InventoryOnWay_Attachment.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while(reader.Read())
                {
                    INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent = new INV_InventoryOnWay_AttachMent();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID])) objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colStoreChangeOrderID])) objINV_InventoryOnWay_AttachMent.StoreChangeOrderID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colStoreChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colAttachMENTName])) objINV_InventoryOnWay_AttachMent.AttachMENTName = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colAttachMENTName]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colAttachMENTPath])) objINV_InventoryOnWay_AttachMent.AttachMENTPath = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colAttachMENTPath]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDescription])) objINV_InventoryOnWay_AttachMent.Description = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colCreatedUser])) objINV_InventoryOnWay_AttachMent.CreatedUser = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colCreatedDate])) objINV_InventoryOnWay_AttachMent.CreatedDate = Convert.ToDateTime(reader[INV_InventoryOnWay_AttachMent.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colIsDeleted])) objINV_InventoryOnWay_AttachMent.IsDeleted = Convert.ToBoolean(reader[INV_InventoryOnWay_AttachMent.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDeletedUser])) objINV_InventoryOnWay_AttachMent.DeletedUser = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDeletedDate])) objINV_InventoryOnWay_AttachMent.DeletedDate = Convert.ToDateTime(reader[INV_InventoryOnWay_AttachMent.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colFileID])) objINV_InventoryOnWay_AttachMent.FileID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colFileID]).Trim();

                    lstINV_InventoryOnWay_AttachMent.Add(objINV_InventoryOnWay_AttachMent);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_INVENToRYONWAY_AttachMENT -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage SearchDataToList(IData objIData, ref List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            try
            {
                lstINV_InventoryOnWay_AttachMent.Clear();
                objIData.CreateNewStoredProcedure(DA_InventoryOnWay_Attachment.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent = new INV_InventoryOnWay_AttachMent();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID])) objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colStoreChangeOrderID])) objINV_InventoryOnWay_AttachMent.StoreChangeOrderID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colStoreChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colAttachMENTName])) objINV_InventoryOnWay_AttachMent.AttachMENTName = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colAttachMENTName]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colAttachMENTPath])) objINV_InventoryOnWay_AttachMent.AttachMENTPath = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colAttachMENTPath]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDescription])) objINV_InventoryOnWay_AttachMent.Description = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colCreatedUser])) objINV_InventoryOnWay_AttachMent.CreatedUser = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colCreatedDate])) objINV_InventoryOnWay_AttachMent.CreatedDate = Convert.ToDateTime(reader[INV_InventoryOnWay_AttachMent.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colIsDeleted])) objINV_InventoryOnWay_AttachMent.IsDeleted = Convert.ToBoolean(reader[INV_InventoryOnWay_AttachMent.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDeletedUser])) objINV_InventoryOnWay_AttachMent.DeletedUser = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDeletedDate])) objINV_InventoryOnWay_AttachMent.DeletedDate = Convert.ToDateTime(reader[INV_InventoryOnWay_AttachMent.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colFileID])) objINV_InventoryOnWay_AttachMent.FileID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colFileID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colOutputVoucherID])) objINV_InventoryOnWay_AttachMent.OutputVoucherID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colInputVoucherID])) objINV_InventoryOnWay_AttachMent.InputVoucherID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colInputVoucherID]).Trim();

                    lstINV_InventoryOnWay_AttachMent.Add(objINV_InventoryOnWay_AttachMent);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin danh sách file đính kèm - phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_INVENToRYONWAY_AttachMENT -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            return objResultMessage;
        }

		#endregion
		
		
	}
}
