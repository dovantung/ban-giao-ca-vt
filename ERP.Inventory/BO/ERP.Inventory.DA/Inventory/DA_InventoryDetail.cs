
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InventoryDetail = ERP.Inventory.BO.Inventory.InventoryDetail;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 12/26/2012 
	/// Chi tiết phiếu kiểm kê
	/// </summary>	
	public partial class DA_InventoryDetail
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết phiếu kiểm kê
		/// </summary>
		/// <param name="objInventoryDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InventoryDetail objInventoryDetail, string strInventoryDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InventoryDetail.colInventoryDetailID, strInventoryDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
 				
				if (reader.Read())
 				{
                    if (objInventoryDetail == null)
                        objInventoryDetail = new InventoryDetail();

 					if (!Convert.IsDBNull(reader[InventoryDetail.colInventoryDetailID])) objInventoryDetail.InventoryDetailID = Convert.ToString(reader[InventoryDetail.colInventoryDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetail.colInventoryID])) objInventoryDetail.InventoryID = Convert.ToString(reader[InventoryDetail.colInventoryID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetail.colInventoryDate])) objInventoryDetail.InventoryDate = Convert.ToDateTime(reader[InventoryDetail.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colInventoryStoreID])) objInventoryDetail.InventoryStoreID = Convert.ToInt32(reader[InventoryDetail.colInventoryStoreID]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colProductID])) objInventoryDetail.ProductID = Convert.ToString(reader[InventoryDetail.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetail.colCabinetNumber])) objInventoryDetail.CabinetNumber = Convert.ToInt32(reader[InventoryDetail.colCabinetNumber]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colQuantity])) objInventoryDetail.Quantity = Convert.ToDecimal(reader[InventoryDetail.colQuantity]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colInStockQuantity])) objInventoryDetail.InStockQuantity = Convert.ToDecimal(reader[InventoryDetail.colInStockQuantity]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colNote])) objInventoryDetail.Note = Convert.ToString(reader[InventoryDetail.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetail.colCreatedStoreID])) objInventoryDetail.CreatedStoreID = Convert.ToInt32(reader[InventoryDetail.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colCreatedUser])) objInventoryDetail.CreatedUser = Convert.ToString(reader[InventoryDetail.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetail.colCreatedDate])) objInventoryDetail.CreatedDate = Convert.ToDateTime(reader[InventoryDetail.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colUpdatedUser])) objInventoryDetail.UpdatedUser = Convert.ToString(reader[InventoryDetail.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetail.colUpdatedDate])) objInventoryDetail.UpdatedDate = Convert.ToDateTime(reader[InventoryDetail.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colIsDeleted])) objInventoryDetail.IsDeleted = Convert.ToBoolean(reader[InventoryDetail.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colDeletedUser])) objInventoryDetail.DeletedUser = Convert.ToString(reader[InventoryDetail.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetail.colDeletedDate])) objInventoryDetail.DeletedDate = Convert.ToDateTime(reader[InventoryDetail.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[InventoryDetail.colDeletedNote])) objInventoryDetail.DeletedNote = Convert.ToString(reader[InventoryDetail.colDeletedNote]).Trim();
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết phiếu kiểm kê
		/// </summary>
		/// <param name="objInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InventoryDetail objInventoryDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInventoryDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết phiếu kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InventoryDetail objInventoryDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InventoryDetail.colInventoryDetailID, objInventoryDetail.InventoryDetailID);
				objIData.AddParameter("@" + InventoryDetail.colInventoryID, objInventoryDetail.InventoryID);
				objIData.AddParameter("@" + InventoryDetail.colInventoryDate, objInventoryDetail.InventoryDate);
				objIData.AddParameter("@" + InventoryDetail.colInventoryStoreID, objInventoryDetail.InventoryStoreID);
				objIData.AddParameter("@" + InventoryDetail.colProductID, objInventoryDetail.ProductID);
				objIData.AddParameter("@" + InventoryDetail.colCabinetNumber, objInventoryDetail.CabinetNumber);
				objIData.AddParameter("@" + InventoryDetail.colQuantity, objInventoryDetail.Quantity);
				objIData.AddParameter("@" + InventoryDetail.colInStockQuantity, objInventoryDetail.InStockQuantity);
				objIData.AddParameter("@" + InventoryDetail.colNote, objInventoryDetail.Note);
				objIData.AddParameter("@" + InventoryDetail.colCreatedStoreID, objInventoryDetail.CreatedStoreID);
				objIData.AddParameter("@" + InventoryDetail.colCreatedUser, objInventoryDetail.CreatedUser);
				objIData.AddParameter("@" + InventoryDetail.colDeletedNote, objInventoryDetail.DeletedNote);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu kiểm kê
		/// </summary>
		/// <param name="objInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InventoryDetail objInventoryDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInventoryDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InventoryDetail objInventoryDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InventoryDetail.colInventoryDetailID, objInventoryDetail.InventoryDetailID);
				objIData.AddParameter("@" + InventoryDetail.colInventoryID, objInventoryDetail.InventoryID);
				objIData.AddParameter("@" + InventoryDetail.colInventoryDate, objInventoryDetail.InventoryDate);
				objIData.AddParameter("@" + InventoryDetail.colInventoryStoreID, objInventoryDetail.InventoryStoreID);
				objIData.AddParameter("@" + InventoryDetail.colProductID, objInventoryDetail.ProductID);
				objIData.AddParameter("@" + InventoryDetail.colCabinetNumber, objInventoryDetail.CabinetNumber);
				objIData.AddParameter("@" + InventoryDetail.colQuantity, objInventoryDetail.Quantity);
				objIData.AddParameter("@" + InventoryDetail.colInStockQuantity, objInventoryDetail.InStockQuantity);
				objIData.AddParameter("@" + InventoryDetail.colNote, objInventoryDetail.Note);
				objIData.AddParameter("@" + InventoryDetail.colCreatedStoreID, objInventoryDetail.CreatedStoreID);
				objIData.AddParameter("@" + InventoryDetail.colUpdatedUser, objInventoryDetail.UpdatedUser);
				objIData.AddParameter("@" + InventoryDetail.colDeletedNote, objInventoryDetail.DeletedNote);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết phiếu kiểm kê
		/// </summary>
		/// <param name="objInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InventoryDetail objInventoryDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInventoryDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết phiếu kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InventoryDetail objInventoryDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InventoryDetail.colInventoryDetailID, objInventoryDetail.InventoryDetailID);
				objIData.AddParameter("@" + InventoryDetail.colDeletedUser, objInventoryDetail.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InventoryDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "InventoryDetail_ADD";
		public const String SP_UPDATE = "InventoryDetail_UPD";
		public const String SP_DELETE = "InventoryDetail_DEL";
		public const String SP_SELECT = "InventoryDetail_SEL";
		public const String SP_SEARCH = "InventoryDetail_SRH";
		public const String SP_UPDATEINDEX = "InventoryDetail_UPDINDEX";
		#endregion
		
	}
}
