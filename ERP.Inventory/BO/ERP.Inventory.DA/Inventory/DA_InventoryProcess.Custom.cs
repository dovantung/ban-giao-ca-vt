
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using InventoryProcess = ERP.Inventory.BO.Inventory.InventoryProcess;
using InventoryProcess_Input = ERP.Inventory.BO.Inventory.InventoryProcess_Input;
using InventoryProcess_Output = ERP.Inventory.BO.Inventory.InventoryProcess_Output;
using InventoryProcess_PrChange = ERP.Inventory.BO.Inventory.InventoryProcess_PrChange;
using ERP.Inventory.BO.Inventory;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 01/28/13 
	/// Quản lý yêu cầu kiểm kê
	/// </summary>	
	public partial class DA_InventoryProcess
    {

        #region Methods			

        /// <summary>
        /// Tìm kiếm thông tin quản lý yêu cầu kiểm kê
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORYPROCESS_SRH");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin quản lý yêu cầu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage AddInventoryProcess(InventoryProcess objInventoryProcess, DataTable dtbInventoryProcessHandler)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                if (objInventoryProcess != null)
                {
                    string strInventoryProcessID = AddInventoryProcess(objIData, objInventoryProcess);
                    if (strInventoryProcessID == string.Empty)
                    {
                        return objResultMessage;
                    }
                    objInventoryProcess.InventoryProcessID = strInventoryProcessID;
                    if (objInventoryProcess.InventoryProcessInputList != null && objInventoryProcess.InventoryProcessInputList.Count > 0)
                    {
                        foreach (InventoryProcess_Input objInventoryProcessInput in objInventoryProcess.InventoryProcessInputList)
                        {
                            objInventoryProcessInput.InventoryDate = objInventoryProcess.InventoryDate;
                            objInventoryProcessInput.CreatedStoreID = objInventoryProcess.CreatedStoreID;
                            objInventoryProcessInput.CreatedUser = objInventoryProcess.CreatedUser;
                            if (!AddInventoryProcessInput(objIData, strInventoryProcessID, objInventoryProcess.InventoryID, objInventoryProcessInput))
                            {
                                return objResultMessage;
                            }
                        }
                    }
                    if (objInventoryProcess.InventoryProcessOutputList != null && objInventoryProcess.InventoryProcessOutputList.Count > 0)
                    {
                        foreach (InventoryProcess_Output objInventoryProcessOutput in objInventoryProcess.InventoryProcessOutputList)
                        {
                            objInventoryProcessOutput.InventoryDate = objInventoryProcess.InventoryDate;
                            objInventoryProcessOutput.CreatedStoreID = objInventoryProcess.CreatedStoreID;
                            objInventoryProcessOutput.CreatedUser = objInventoryProcess.CreatedUser;
                            if (!AddInventoryProcessOutput(objIData, strInventoryProcessID, objInventoryProcess.InventoryID, objInventoryProcessOutput))
                            {
                                return objResultMessage;
                            }
                        }
                    }
                    if (objInventoryProcess.InventoryProcessPrChangeList != null && objInventoryProcess.InventoryProcessPrChangeList.Count > 0)
                    {
                        foreach (InventoryProcess_PrChange objInventoryProcessPrChange in objInventoryProcess.InventoryProcessPrChangeList)
                        {
                            objInventoryProcessPrChange.InventoryDate = objInventoryProcess.InventoryDate;
                            objInventoryProcessPrChange.CreatedStoreID = objInventoryProcess.CreatedStoreID;
                            objInventoryProcessPrChange.CreatedUser = objInventoryProcess.CreatedUser;
                            if (!AddInventoryProcessProductChange(objIData, strInventoryProcessID, objInventoryProcess.InventoryID, objInventoryProcessPrChange))
                            {
                                return objResultMessage;
                            }
                        }
                    }
                    if (objInventoryProcess.InventoryProcessStChangeList != null && objInventoryProcess.InventoryProcessStChangeList.Count > 0)
                    {
                        foreach (InventoryProcess_StChange objInventoryProcessStChange in objInventoryProcess.InventoryProcessStChangeList)
                        {
                            objInventoryProcessStChange.InventoryDate = objInventoryProcess.InventoryDate;
                            objInventoryProcessStChange.CreatedStoreID = objInventoryProcess.CreatedStoreID;
                            objInventoryProcessStChange.CreatedUser = objInventoryProcess.CreatedUser;
                            if (!AddInventoryProcessStatusChange(objIData, strInventoryProcessID, objInventoryProcess.InventoryID, objInventoryProcessStChange))
                            {
                                return objResultMessage;
                            }
                        }
                    }

                    if (dtbInventoryProcessHandler != null && dtbInventoryProcessHandler.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbInventoryProcessHandler.Rows.Count; i++)
                        {
                            string inventoryID = (dtbInventoryProcessHandler.Rows[i]["INVENTORYID"] ?? "").ToString().Trim();
                            string processID = strInventoryProcessID;
                            string unEventID = (dtbInventoryProcessHandler.Rows[i]["UNEVENTID"] ?? "").ToString().Trim();
                            if (!string.IsNullOrEmpty(inventoryID) && !string.IsNullOrEmpty(processID) && !string.IsNullOrEmpty(inventoryID))
                            {
                                objIData.CreateNewStoredProcedure("INV_PROCESSHANDLER_ADD");
                                objIData.AddParameter("@INVENTORYID", inventoryID);
                                objIData.AddParameter("@INVENTORYPROCESSID", processID);
                                objIData.AddParameter("@UNEVENTID", unEventID);
                                objIData.ExecNonQuery();
                            }
                        }
                    }
                }

                objIData.CommitTransaction();

                return objResultMessage;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi cập nhật xử lý chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> AddInventoryProcess", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
        }

        private string AddInventoryProcess(IData objIData, InventoryProcess objInventoryProcess)
        {
            try
            {

                objIData.CreateNewStoredProcedure("INV_InventoryProcess_Add");
                objIData.AddParameter("@InventoryID", objInventoryProcess.InventoryID);
                objIData.AddParameter("@InventoryStoreID", objInventoryProcess.InventoryStoreID);
                objIData.AddParameter("@MainGroupID", objInventoryProcess.MainGroupID);
                objIData.AddParameter("@IsNew", objInventoryProcess.IsNew);
                objIData.AddParameter("@InventoryTermID", objInventoryProcess.InventoryTermID);
                objIData.AddParameter("@InventoryDate", objInventoryProcess.InventoryDate);
                objIData.AddParameter("@CreatedUser", objInventoryProcess.CreatedUser);
                objIData.AddParameter("@CreatedStoreID", objInventoryProcess.CreatedStoreID);
                objIData.AddParameter("@IsProcess", objInventoryProcess.IsProcess);
                return objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        private bool AddInventoryProcessInput(IData objIData, string strInventoryProcessID, string inventoryID, InventoryProcess_Input objInventoryProcessInput)
        {
            try
            {
                objIData.CreateNewStoredProcedure("INV_InventoryProcessInput_Add");
                objIData.AddParameter("@INVENTORYID", inventoryID);
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@InventoryDate", objInventoryProcessInput.InventoryDate);
                objIData.AddParameter("@ProductID", objInventoryProcessInput.ProductID);
                objIData.AddParameter("@IMEI", objInventoryProcessInput.IMEI);
                objIData.AddParameter("@Quantity", objInventoryProcessInput.Quantity);
                objIData.AddParameter("@ProductStatusID", objInventoryProcessInput.ProductStatusID);
                objIData.AddParameter("@InputVoucherID", objInventoryProcessInput.InputVoucherID);
                objIData.AddParameter("@InputVoucherDetailID", objInventoryProcessInput.InputVoucherDetailID);
                objIData.AddParameter("@InputPrice", objInventoryProcessInput.InputPrice);
                objIData.AddParameter("@UnEventID", objInventoryProcessInput.UnEventID);
                objIData.AddParameter("@UnEventNote", objInventoryProcessInput.UnEventNote);
                objIData.AddParameter("@CreatedStoreID", objInventoryProcessInput.CreatedStoreID);
                objIData.AddParameter("@CreatedUser", objInventoryProcessInput.CreatedUser);
                objIData.ExecNonQuery();
                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        private bool AddInventoryProcessOutput(IData objIData, string strInventoryProcessID, string inventoryID, InventoryProcess_Output objInventoryProcessOutput)
        {
            try
            {

                objIData.CreateNewStoredProcedure("INV_InventoryProcessOutput_Add");
                objIData.AddParameter("@INVENTORYID", inventoryID);
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@InventoryDate", objInventoryProcessOutput.InventoryDate);
                objIData.AddParameter("@ProductID", objInventoryProcessOutput.ProductID);
                objIData.AddParameter("@IMEI", objInventoryProcessOutput.IMEI);
                objIData.AddParameter("@Quantity", objInventoryProcessOutput.Quantity);
                objIData.AddParameter("@ProductStatusID", objInventoryProcessOutput.ProductStatusID);
                objIData.AddParameter("@OutputVoucherID", objInventoryProcessOutput.OutputVoucherID);
                objIData.AddParameter("@OutputVoucherDetailID", objInventoryProcessOutput.OutputVoucherDetailID);
                objIData.AddParameter("@OutputPrice", objInventoryProcessOutput.OutputPrice);
                objIData.AddParameter("@CollectArrearPrice", objInventoryProcessOutput.CollectArrearPrice);
                objIData.AddParameter("@UnEventID", objInventoryProcessOutput.UnEventID);
                objIData.AddParameter("@UnEventNote", objInventoryProcessOutput.UnEventNote);
                objIData.AddParameter("@CreatedStoreID", objInventoryProcessOutput.CreatedStoreID);
                objIData.AddParameter("@CreatedUser", objInventoryProcessOutput.CreatedUser);
                objIData.ExecNonQuery();
                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        private bool AddInventoryProcessProductChange(IData objIData, string strInventoryProcessID, string inventoryID, InventoryProcess_PrChange objInventoryProcessPrChange)
        {
            try
            {

                objIData.CreateNewStoredProcedure("INV_InventProcessPrChange_Add");
                objIData.AddParameter("@INVENTORYID", inventoryID);
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@InventoryDate", objInventoryProcessPrChange.InventoryDate);
                objIData.AddParameter("@ProductID_Out", objInventoryProcessPrChange.ProductID_Out);
                objIData.AddParameter("@IMEI_Out", objInventoryProcessPrChange.IMEI_Out);
                objIData.AddParameter("@ProductID_In", objInventoryProcessPrChange.ProductID_In);
                objIData.AddParameter("@IMEI_In", objInventoryProcessPrChange.IMEI_In);
                objIData.AddParameter("@Quantity", objInventoryProcessPrChange.Quantity);
                objIData.AddParameter("@In_ProductStatusID", objInventoryProcessPrChange.In_ProductStatusID);
                objIData.AddParameter("@Out_ProductStatusID", objInventoryProcessPrChange.Out_ProductStatusID);
                objIData.AddParameter("@ProductChangeDetailID", objInventoryProcessPrChange.ProductChangeDetailID);
                objIData.AddParameter("@RefSalePrice_Out", objInventoryProcessPrChange.RefSalePrice_Out);
                objIData.AddParameter("@RefSalePrice_In", objInventoryProcessPrChange.RefSalePrice_In);
                objIData.AddParameter("@CollectArrearPrice", objInventoryProcessPrChange.CollectArrearPrice);
                objIData.AddParameter("@UnEventID", objInventoryProcessPrChange.UnEventID);
                objIData.AddParameter("@UnEventNote", objInventoryProcessPrChange.UnEventNote);
                objIData.AddParameter("@ChangeUnEventID", objInventoryProcessPrChange.ChangeUnEventID);
                objIData.AddParameter("@ChangeUnEventNote", objInventoryProcessPrChange.ChangeUnEventNote);
                objIData.AddParameter("@UnEventAmount", objInventoryProcessPrChange.UnEventAmount);
                objIData.AddParameter("@CreatedStoreID", objInventoryProcessPrChange.CreatedStoreID);
                objIData.AddParameter("@CreatedUser", objInventoryProcessPrChange.CreatedUser);
                objIData.ExecNonQuery();
                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        private bool AddInventoryProcessStatusChange(IData objIData, string strInventoryProcessID, string inventoryID, InventoryProcess_StChange objInventoryProcessStChange)
        {
            try
            {

                objIData.CreateNewStoredProcedure("INV_InventProcessStChange_Add");
                objIData.AddParameter("@INVENTORYID", inventoryID);
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@InventoryDate", objInventoryProcessStChange.InventoryDate);
                objIData.AddParameter("@ProductID_Out", objInventoryProcessStChange.ProductID_Out);
                objIData.AddParameter("@IMEI_Out", objInventoryProcessStChange.IMEI_Out);
                objIData.AddParameter("@ProductID_In", objInventoryProcessStChange.ProductID_In);
                objIData.AddParameter("@IMEI_In", objInventoryProcessStChange.IMEI_In);
                objIData.AddParameter("@Quantity", objInventoryProcessStChange.Quantity);
                objIData.AddParameter("@In_ProductStatusID", objInventoryProcessStChange.In_ProductStatusID);
                objIData.AddParameter("@Out_ProductStatusID", objInventoryProcessStChange.Out_ProductStatusID);
                objIData.AddParameter("@ProductChangeDetailID", objInventoryProcessStChange.ProductChangeDetailID);
                objIData.AddParameter("@RefSalePrice_Out", objInventoryProcessStChange.RefSalePrice_Out);
                objIData.AddParameter("@RefSalePrice_In", objInventoryProcessStChange.RefSalePrice_In);
                objIData.AddParameter("@CollectArrearPrice", objInventoryProcessStChange.CollectArrearPrice);
                objIData.AddParameter("@UnEventID", objInventoryProcessStChange.UnEventID);
                objIData.AddParameter("@UnEventNote", objInventoryProcessStChange.UnEventNote);
                objIData.AddParameter("@ChangeUnEventID", objInventoryProcessStChange.ChangeUnEventID);
                objIData.AddParameter("@ChangeUnEventNote", objInventoryProcessStChange.ChangeUnEventNote);
                objIData.AddParameter("@UnEventAmount", objInventoryProcessStChange.UnEventAmount);
                objIData.AddParameter("@CreatedStoreID", objInventoryProcessStChange.CreatedStoreID);
                objIData.AddParameter("@CreatedUser", objInventoryProcessStChange.CreatedUser);
                objIData.ExecNonQuery();
                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        private DataTable LoadInventoryProcessInput(IData objIData, string strInventoryProcessID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORYPROCESS_INPUT_SRH");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                return objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        private DataTable LoadInventoryProcessOutput(IData objIData, string strInventoryProcessID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORYPROCESS_OUTPT_SRH");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                return objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        private DataTable LoadInventoryProcessPrChange(IData objIData, string strInventoryProcessID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORYPROCESS_PR_SRH");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                return objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        private DataTable LoadInventoryProcessStChange(IData objIData, string strInventoryProcessID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORYPROCESS_ST_SRH");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                return objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage LoadInventProcessArrear(ref DataTable dtbUserArrear, string strInventoryProcessID, string strRelateVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_ARREAR_SRH");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@RelateVoucherID", strRelateVoucherID);
                dtbUserArrear = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp danh sách nhân viên truy thu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> LoadInventProcessArrear", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateInventoryProcess(string strInventoryProcessID, string strInputContent, string strOutputContent, string strPrChangeContent, string strStChangeContent, int intOriginateStoreID, DataTable dtbInput, DataTable dtbOutput, DataTable dtbProductChange, DataTable dtbStatusChange, ref DataTable dtbCollectArrear)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_UpdContent");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@InputContent", strInputContent);
                objIData.AddParameter("@OutputContent", strOutputContent);
                objIData.AddParameter("@PrChangeContent", strPrChangeContent);
                objIData.AddParameter("@STATUSCHANGECONTENT", strStChangeContent);
                objIData.ExecNonQuery();

                if (dtbInput != null && dtbInput.Rows.Count > 0)
                {
                    if (!UpdateInventoryProcessInput(objIData, dtbInput))
                    {
                        return objResultMessage;
                    }
                }
                if (dtbOutput != null && dtbOutput.Rows.Count > 0)
                {
                    if (!UpdateInventoryProcessOutput(objIData, strInventoryProcessID, intOriginateStoreID, dtbOutput, ref dtbCollectArrear))
                    {
                        return objResultMessage;
                    }
                }
                if (dtbProductChange != null && dtbProductChange.Rows.Count > 0)
                {
                    if (!UpdateInventoryProcessProductChange(objIData, strInventoryProcessID, intOriginateStoreID, dtbProductChange, ref dtbCollectArrear))
                    {
                        return objResultMessage;
                    }
                }
                if (dtbStatusChange != null && dtbStatusChange.Rows.Count > 0)
                {
                    if (!UpdateInventoryProcessStatusChange(objIData, strInventoryProcessID, intOriginateStoreID, dtbStatusChange, ref dtbCollectArrear))
                    {
                        return objResultMessage;
                    }
                }

                objIData.CommitTransaction();

                return objResultMessage;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi cập nhật xử lý chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> AddInventoryProcess", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
        }

        private bool UpdateInventoryProcessInput(IData objIData, DataTable dtbInput)
        {
            try
            {
                if (dtbInput != null && dtbInput.Rows.Count > 0)
                {
                    foreach (DataRow rInput in dtbInput.Rows)
                    {
                        objIData.CreateNewStoredProcedure("INV_INVENTORYPROCESS_INPUT_UPD");
                        string strInputProcessID = Convert.ToString(rInput["InputProcessID"]);
                        objIData.AddParameter("@InputProcessID", strInputProcessID);
                        objIData.AddParameter("@InputPrice", Convert.ToDecimal(rInput["Price"]));
                        objIData.AddParameter("@VAT", Convert.ToInt32(rInput["VAT"]));
                        objIData.AddParameter("@VATPercent", Convert.ToInt32(rInput["VATPercent"]));
                        objIData.ExecNonQuery();
                    }
                }

                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }

        private bool UpdateInventoryProcessOutput(IData objIData, string strInventoryProcessID, int intOriginateStoreID, DataTable dtbOutput, ref DataTable dtbCollectArrear)
        {
            try
            {
                if (dtbOutput != null && dtbOutput.Rows.Count > 0)
                {
                    foreach (DataRow rOutput in dtbOutput.Rows)
                    {
                        objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_OUTPUT_UPD");
                        string strOutputProcessID = Convert.ToString(rOutput["OutputProcessID"]);
                        objIData.AddParameter("@OutputProcessID", strOutputProcessID);
                        objIData.AddParameter("@OutputPrice", Convert.ToDecimal(rOutput["Price"]));
                        objIData.AddParameter("@VAT", Convert.ToInt32(rOutput["VAT"]));
                        objIData.AddParameter("@VATPercent", Convert.ToInt32(rOutput["VATPercent"]));
                        objIData.AddParameter("@CollectArrearPrice", Convert.ToDecimal(rOutput["CollectArrearPrice"]));
                        objIData.ExecNonQuery();

                        bool bolIsExist = false;
                        CheckInventProcessArrearExist(strInventoryProcessID, strOutputProcessID, ref bolIsExist);
                        if (!bolIsExist)
                        {
                            AddInventProcessArrear(objIData, strInventoryProcessID, 1, strOutputProcessID, intOriginateStoreID, ref dtbCollectArrear);
                        }
                        else
                        {
                            UpdateInventProcessArrear(objIData, strInventoryProcessID, 1, strOutputProcessID, ref dtbCollectArrear, intOriginateStoreID);
                        }
                    }
                }

                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }

        private bool UpdateInventoryProcessProductChange(IData objIData, string strInventoryProcessID, int intOriginateStoreID, DataTable dtbProductChange, ref DataTable dtbCollectArrear)
        {
            try
            {
                if (dtbProductChange != null && dtbProductChange.Rows.Count > 0)
                {
                    foreach (DataRow rProductChange in dtbProductChange.Rows)
                    {
                        objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_PRCHANGE_UPD");
                        string strProductChangeProcessID = Convert.ToString(rProductChange["ProductChangeProcessID"]);
                        objIData.AddParameter("@ProductChangeProcessID", strProductChangeProcessID);
                        objIData.AddParameter("@REFSALEPRICE_OUT", Convert.ToDecimal(rProductChange["REFSALEPRICE_OUT"]));
                        objIData.AddParameter("@REFSALEPRICE_IN", Convert.ToDecimal(rProductChange["REFSALEPRICE_IN"]));
                        objIData.AddParameter("@UNEVENTAMOUNT", Convert.ToDecimal(Convert.IsDBNull(rProductChange["UNEVENTAMOUNT"]) ? 0 : rProductChange["UNEVENTAMOUNT"]));
                        objIData.AddParameter("@CollectArrearPrice", Convert.ToDecimal(rProductChange["CollectArrearPrice"]));
                        objIData.ExecNonQuery();

                        bool bolIsExist = false;
                        CheckInventProcessArrearExist(strInventoryProcessID, strProductChangeProcessID, ref bolIsExist);
                        if (!bolIsExist)
                        {
                            AddInventProcessArrear(objIData, strInventoryProcessID, 2, strProductChangeProcessID, intOriginateStoreID, ref dtbCollectArrear);
                        }
                        else
                        {
                            UpdateInventProcessArrear(objIData, strInventoryProcessID, 2, strProductChangeProcessID, ref dtbCollectArrear, intOriginateStoreID);
                        }
                    }
                }

                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }
        private bool UpdateInventoryProcessStatusChange(IData objIData, string strInventoryProcessID, int intOriginateStoreID, DataTable dtbStatusChange, ref DataTable dtbCollectArrear)
        {
            try
            {
                if (dtbStatusChange != null && dtbStatusChange.Rows.Count > 0)
                {
                    foreach (DataRow rStatusChange in dtbStatusChange.Rows)
                    {
                        objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_STCHANGE_UPD");
                        string strStatusChangeProcessID = Convert.ToString(rStatusChange["PRODUCTCHANGEPROCESSID"]);
                        objIData.AddParameter("@PRODUCTCHANGEPROCESSID", strStatusChangeProcessID);
                        objIData.AddParameter("@REFSALEPRICE_OUT", Convert.ToDecimal(rStatusChange["REFSALEPRICE_OUT"]));
                        objIData.AddParameter("@REFSALEPRICE_IN", Convert.ToDecimal(rStatusChange["REFSALEPRICE_IN"]));
                        objIData.AddParameter("@UNEVENTAMOUNT", Convert.ToDecimal(Convert.IsDBNull(rStatusChange["UNEVENTAMOUNT"]) ? 0 : rStatusChange["UNEVENTAMOUNT"]));
                        objIData.AddParameter("@COLLECTARREARPRICE", Convert.ToDecimal(rStatusChange["COLLECTARREARPRICE"]));
                        objIData.ExecNonQuery();

                        bool bolIsExist = false;
                        CheckInventProcessArrearExist(strInventoryProcessID, strStatusChangeProcessID, ref bolIsExist);
                        if (!bolIsExist)
                        {
                            AddInventProcessArrear(objIData, strInventoryProcessID, 3, strStatusChangeProcessID, intOriginateStoreID, ref dtbCollectArrear);
                        }
                        else
                        {
                            UpdateInventProcessArrear(objIData, strInventoryProcessID, 3, strStatusChangeProcessID, ref dtbCollectArrear, intOriginateStoreID);
                        }
                    }
                }

                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }

        private bool AddInventProcessArrear(IData objIData, string strInventoryProcessID, int intCollectArrearType, string strRelateVoucherID, int intOriginateStoreID, ref DataTable dtbCollectArrear)
        {
            try
            {
                DataRow[] drCollectArrear = dtbCollectArrear.Select("RelateVoucherID = '" + strRelateVoucherID + "'");
                if (drCollectArrear.Length > 0)
                {
                    foreach (DataRow rCollectArrear in drCollectArrear)
                    {
                        objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_ARREAR_Add");
                        objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                        objIData.AddParameter("@CollectArrearType", intCollectArrearType);
                        objIData.AddParameter("@RelateVoucherID", strRelateVoucherID);
                        objIData.AddParameter("@CreatedStoreID", intOriginateStoreID);
                        objIData.AddParameter("@UserName", Convert.ToString(rCollectArrear["UserName"]));
                        objIData.AddParameter("@Note", Convert.ToString(rCollectArrear["Note"]));
                        objIData.AddParameter("@CollectArrearMoney", Convert.ToDecimal(rCollectArrear["CollectArrearMoney"]));
                        rCollectArrear["CollectArrearID"] = objIData.ExecStoreToString();
                    }
                }

                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }

        private bool UpdateInventProcessArrear(IData objIData, string strInventoryProcessID, int intCollectArrearType, string strRelateVoucherID, ref DataTable dtbCollectArrear, int intOriginateStoreID = 0)
        {
            try
            {
                DataRow[] drCollectArrear = dtbCollectArrear.Select("RelateVoucherID = '" + strRelateVoucherID + "'");
                if (drCollectArrear.Length > 0)
                {
                    foreach (DataRow rCollectArrear in drCollectArrear)
                    {
                        string strCollectArrearID = string.Empty;
                        try
                        {
                            strCollectArrearID = Convert.ToString(rCollectArrear["CollectArrearID"]);
                        }
                        catch { }
                        if (strCollectArrearID.Trim() == string.Empty)
                        {
                            objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_ARREAR_Add");
                            objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                            objIData.AddParameter("@CollectArrearType", intCollectArrearType);
                            objIData.AddParameter("@CreatedStoreID", intOriginateStoreID);
                            objIData.AddParameter("@RelateVoucherID", strRelateVoucherID);
                            objIData.AddParameter("@UserName", Convert.ToString(rCollectArrear["UserName"]));
                            objIData.AddParameter("@Note", Convert.ToString(rCollectArrear["Note"]));
                            objIData.AddParameter("@CollectArrearMoney", Convert.ToDecimal(rCollectArrear["CollectArrearMoney"]));
                            rCollectArrear["CollectArrearID"] = objIData.ExecStoreToString();
                        }
                        else
                        {
                            objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_ARREAR_Upd");
                            objIData.AddParameter("@CollectArrearID", Convert.ToString(rCollectArrear["CollectArrearID"]));
                            objIData.AddParameter("@UserName", Convert.ToString(rCollectArrear["UserName"]));
                            objIData.AddParameter("@Note", Convert.ToString(rCollectArrear["Note"]));
                            objIData.AddParameter("@CollectArrearMoney", Convert.ToDecimal(rCollectArrear["CollectArrearMoney"]));
                            bool bolIsDelete = false;
                            try
                            {
                                bolIsDelete = Convert.ToBoolean(rCollectArrear["IsDeleted"]);
                            }
                            catch { }
                            objIData.AddParameter("@IsDeleted", bolIsDelete);
                            string strDeleteUser = string.Empty;
                            try
                            {
                                strDeleteUser = Convert.ToString(rCollectArrear["DeletedUser"]);
                            }
                            catch { }
                            objIData.AddParameter("@DeletedUser", strDeleteUser);
                            objIData.ExecNonQuery();
                        }

                    }
                }

                return true;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }

        }

        public ResultMessage CheckInventProcessArrearExist(string strInventoryProcessID, string strRelateVoucherID, ref bool bolIsExist)
        {
            bolIsExist = false;
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTPRCES_ARREA_ChkExist");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@RelateVoucherID", strRelateVoucherID);
                IDataReader rdResult = objIData.ExecStoreToDataReader();
                if (rdResult.Read())
                {
                    bolIsExist = true;
                }
                rdResult.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm tra nhân viên truy thu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> CheckInventProcessArrearExist", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage ReviewedInventProcess(string strInventoryProcessID, bool bolIsReviewed, string strReviewedUser)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_ReviewedUpd");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@IsReviewed", bolIsReviewed);
                objIData.AddParameter("@ReviewedUser", strReviewedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi duyệt xử lý chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> ReviewedInventProcess", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage ProcessedInventProcess(string strInventoryProcessID, bool bolIsProcess, string strProcessUser, int intOriginateStoreID, string strCertificateString, string strUserHostAddress, string strLoginLogID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTPROCESS_ProcessedUpd");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@IsProcess", bolIsProcess);
                objIData.AddParameter("@ProcessUser", strProcessUser);
                objIData.AddParameter("@OriginateStoreID", intOriginateStoreID);
                objIData.AddParameter("@CertificateString", strCertificateString);
                objIData.AddParameter("@UserHostAddress", strUserHostAddress);
                objIData.AddParameter("@LoginLogID", strLoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi xử lý chênh lệch kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> ProcessedInventProcess", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetDataSourceByStoreName(ref DataTable dtbData, object[] objKeywords, string strStoreName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(strStoreName);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> GetDataSourceByStoreName", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CheckExistInventoryProcess(ref string strInventoryProcessID, string strInventoryID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_InventoryProcessCheckExist");
                objIData.AddParameter("@InventoryID", strInventoryID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["InventoryProcessID"]))
                        strInventoryProcessID = Convert.ToString(reader["InventoryProcessID"]).Trim();
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm tra phiếu xử lý kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess -> CheckExistInventoryProcess", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage DeleteInventoryProcess(string strInventoryProcessID, string strDeletedReason, string strDeletedUser)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_INVENTORYPROCESS_Delete");
                objIData.AddParameter("@InventoryProcessID", strInventoryProcessID);
                objIData.AddParameter("@DeletedReason", strDeletedReason);
                objIData.AddParameter("@DeletedUser", strDeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin phiếu xử lý kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> DeleteInventoryProcess", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        #endregion


    }
}
