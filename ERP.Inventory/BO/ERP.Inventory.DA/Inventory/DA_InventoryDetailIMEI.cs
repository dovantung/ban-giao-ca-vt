
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InventoryDetailIMEI = ERP.Inventory.BO.Inventory.InventoryDetailIMEI;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 12/26/2012 
	/// Chi tiết IMEI phiếu kiểm kê
	/// </summary>	
	public partial class DA_InventoryDetailIMEI
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết imei phiếu kiểm kê
		/// </summary>
		/// <param name="objInventoryDetailIMEI">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InventoryDetailIMEI objInventoryDetailIMEI, string strIMEI, string strInventoryDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InventoryDetailIMEI.colIMEI, strIMEI);
				objIData.AddParameter("@" + InventoryDetailIMEI.colInventoryDetailID, strInventoryDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
 				
				if (reader.Read())
 				{
                    if (objInventoryDetailIMEI == null)
                        objInventoryDetailIMEI = new InventoryDetailIMEI();

 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colInventoryDetailID])) objInventoryDetailIMEI.InventoryDetailID = Convert.ToString(reader[InventoryDetailIMEI.colInventoryDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colInventoryDate])) objInventoryDetailIMEI.InventoryDate = Convert.ToDateTime(reader[InventoryDetailIMEI.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colInventoryStoreID])) objInventoryDetailIMEI.InventoryStoreID = Convert.ToInt32(reader[InventoryDetailIMEI.colInventoryStoreID]);
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colIMEI])) objInventoryDetailIMEI.IMEI = Convert.ToString(reader[InventoryDetailIMEI.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colNote])) objInventoryDetailIMEI.Note = Convert.ToString(reader[InventoryDetailIMEI.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colCabinetNumber])) objInventoryDetailIMEI.CabinetNumber = Convert.ToInt32(reader[InventoryDetailIMEI.colCabinetNumber]);
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colIsInStock])) objInventoryDetailIMEI.IsInStock = Convert.ToBoolean(reader[InventoryDetailIMEI.colIsInStock]);
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colCreatedStoreID])) objInventoryDetailIMEI.CreatedStoreID = Convert.ToInt32(reader[InventoryDetailIMEI.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colCreatedUser])) objInventoryDetailIMEI.CreatedUser = Convert.ToString(reader[InventoryDetailIMEI.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colCreatedDate])) objInventoryDetailIMEI.CreatedDate = Convert.ToDateTime(reader[InventoryDetailIMEI.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colUpdatedUser])) objInventoryDetailIMEI.UpdatedUser = Convert.ToString(reader[InventoryDetailIMEI.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colUpdatedDate])) objInventoryDetailIMEI.UpdatedDate = Convert.ToDateTime(reader[InventoryDetailIMEI.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colIsDeleted])) objInventoryDetailIMEI.IsDeleted = Convert.ToBoolean(reader[InventoryDetailIMEI.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colDeletedUser])) objInventoryDetailIMEI.DeletedUser = Convert.ToString(reader[InventoryDetailIMEI.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colDeletedDate])) objInventoryDetailIMEI.DeletedDate = Convert.ToDateTime(reader[InventoryDetailIMEI.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[InventoryDetailIMEI.colDeletedNote])) objInventoryDetailIMEI.DeletedNote = Convert.ToString(reader[InventoryDetailIMEI.colDeletedNote]).Trim();
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết imei phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryDetailIMEI -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết imei phiếu kiểm kê
		/// </summary>
		/// <param name="objInventoryDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InventoryDetailIMEI objInventoryDetailIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInventoryDetailIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết imei phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryDetailIMEI -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết imei phiếu kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InventoryDetailIMEI objInventoryDetailIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InventoryDetailIMEI.colInventoryDetailID, objInventoryDetailIMEI.InventoryDetailID);
				objIData.AddParameter("@" + InventoryDetailIMEI.colInventoryDate, objInventoryDetailIMEI.InventoryDate);
				objIData.AddParameter("@" + InventoryDetailIMEI.colInventoryStoreID, objInventoryDetailIMEI.InventoryStoreID);
				objIData.AddParameter("@" + InventoryDetailIMEI.colIMEI, objInventoryDetailIMEI.IMEI);
				objIData.AddParameter("@" + InventoryDetailIMEI.colNote, objInventoryDetailIMEI.Note);
				objIData.AddParameter("@" + InventoryDetailIMEI.colCabinetNumber, objInventoryDetailIMEI.CabinetNumber);
				objIData.AddParameter("@" + InventoryDetailIMEI.colIsInStock, objInventoryDetailIMEI.IsInStock);
				objIData.AddParameter("@" + InventoryDetailIMEI.colCreatedStoreID, objInventoryDetailIMEI.CreatedStoreID);
				objIData.AddParameter("@" + InventoryDetailIMEI.colCreatedUser, objInventoryDetailIMEI.CreatedUser);
				objIData.AddParameter("@" + InventoryDetailIMEI.colDeletedNote, objInventoryDetailIMEI.DeletedNote);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết imei phiếu kiểm kê
		/// </summary>
		/// <param name="objInventoryDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InventoryDetailIMEI objInventoryDetailIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInventoryDetailIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết imei phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryDetailIMEI -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết imei phiếu kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InventoryDetailIMEI objInventoryDetailIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InventoryDetailIMEI.colInventoryDetailID, objInventoryDetailIMEI.InventoryDetailID);
				objIData.AddParameter("@" + InventoryDetailIMEI.colInventoryDate, objInventoryDetailIMEI.InventoryDate);
				objIData.AddParameter("@" + InventoryDetailIMEI.colInventoryStoreID, objInventoryDetailIMEI.InventoryStoreID);
				objIData.AddParameter("@" + InventoryDetailIMEI.colIMEI, objInventoryDetailIMEI.IMEI);
				objIData.AddParameter("@" + InventoryDetailIMEI.colNote, objInventoryDetailIMEI.Note);
				objIData.AddParameter("@" + InventoryDetailIMEI.colCabinetNumber, objInventoryDetailIMEI.CabinetNumber);
				objIData.AddParameter("@" + InventoryDetailIMEI.colIsInStock, objInventoryDetailIMEI.IsInStock);
				objIData.AddParameter("@" + InventoryDetailIMEI.colCreatedStoreID, objInventoryDetailIMEI.CreatedStoreID);
				objIData.AddParameter("@" + InventoryDetailIMEI.colUpdatedUser, objInventoryDetailIMEI.UpdatedUser);
				objIData.AddParameter("@" + InventoryDetailIMEI.colDeletedNote, objInventoryDetailIMEI.DeletedNote);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết imei phiếu kiểm kê
		/// </summary>
		/// <param name="objInventoryDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InventoryDetailIMEI objInventoryDetailIMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInventoryDetailIMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết imei phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryDetailIMEI -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết imei phiếu kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryDetailIMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InventoryDetailIMEI objInventoryDetailIMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InventoryDetailIMEI.colInventoryDetailID, objInventoryDetailIMEI.InventoryDetailID);
				objIData.AddParameter("@" + InventoryDetailIMEI.colIMEI, objInventoryDetailIMEI.IMEI);
				objIData.AddParameter("@" + InventoryDetailIMEI.colDeletedUser, objInventoryDetailIMEI.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InventoryDetailIMEI()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "InventoryDETAILIMEI_ADD";
		public const String SP_UPDATE = "InventoryDETAILIMEI_UPD";
		public const String SP_DELETE = "InventoryDETAILIMEI_DEL";
		public const String SP_SELECT = "InventoryDETAILIMEI_SEL";
		public const String SP_SEARCH = "InventoryDETAILIMEI_SRH";
		public const String SP_UPDATEINDEX = "InventoryDETAILIMEI_UPDINDEX";
		#endregion
		
	}
}
