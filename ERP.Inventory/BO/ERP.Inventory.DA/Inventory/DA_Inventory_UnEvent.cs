
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using Inventory_UnEvent = ERP.Inventory.BO.Inventory.Inventory_UnEvent;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 12/26/2012 
	/// 
	/// </summary>	
	public partial class DA_Inventory_UnEvent
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objInventory_UnEvent">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref Inventory_UnEvent objInventory_UnEvent, string strUnEventID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + Inventory_UnEvent.colUnEventID, strUnEventID);
				IDataReader reader = objIData.ExecStoreToDataReader();
 				
				if (reader.Read())
 				{
                    if (objInventory_UnEvent == null) 
 					objInventory_UnEvent = new Inventory_UnEvent();

 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colUnEventID])) objInventory_UnEvent.UnEventID = Convert.ToString(reader[Inventory_UnEvent.colUnEventID]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colInventoryID])) objInventory_UnEvent.InventoryID = Convert.ToString(reader[Inventory_UnEvent.colInventoryID]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colInventoryDate])) objInventory_UnEvent.InventoryDate = Convert.ToDateTime(reader[Inventory_UnEvent.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colProductID])) objInventory_UnEvent.ProductID = Convert.ToString(reader[Inventory_UnEvent.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colCabinetNumber])) objInventory_UnEvent.CabinetNumber = Convert.ToInt32(reader[Inventory_UnEvent.colCabinetNumber]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colStockIMEI])) objInventory_UnEvent.StockIMEI = Convert.ToString(reader[Inventory_UnEvent.colStockIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colStockQuantity])) objInventory_UnEvent.StockQuantity = Convert.ToDecimal(reader[Inventory_UnEvent.colStockQuantity]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colInventoryIMEI])) objInventory_UnEvent.InventoryIMEI = Convert.ToString(reader[Inventory_UnEvent.colInventoryIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colInventoryQuantity])) objInventory_UnEvent.InventoryQuantity = Convert.ToDecimal(reader[Inventory_UnEvent.colInventoryQuantity]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colUnEventQuantity])) objInventory_UnEvent.UnEventQuantity = Convert.ToDecimal(reader[Inventory_UnEvent.colUnEventQuantity]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colUnEventExplain])) objInventory_UnEvent.UnEventExplain = Convert.ToString(reader[Inventory_UnEvent.colUnEventExplain]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colExplainNote])) objInventory_UnEvent.ExplainNote = Convert.ToString(reader[Inventory_UnEvent.colExplainNote]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colExplainUser])) objInventory_UnEvent.ExplainUser = Convert.ToString(reader[Inventory_UnEvent.colExplainUser]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colExplainDate])) objInventory_UnEvent.ExplainDate = Convert.ToDateTime(reader[Inventory_UnEvent.colExplainDate]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colAdjustUnEventQuantity])) objInventory_UnEvent.AdjustUnEventQuantity = Convert.ToDecimal(reader[Inventory_UnEvent.colAdjustUnEventQuantity]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colFinalUnEventQuantity])) objInventory_UnEvent.FinalUnEventQuantity = Convert.ToDecimal(reader[Inventory_UnEvent.colFinalUnEventQuantity]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colTotalCost])) objInventory_UnEvent.TotalCost = Convert.ToDecimal(reader[Inventory_UnEvent.colTotalCost]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colAdjustNote])) objInventory_UnEvent.AdjustNote = Convert.ToString(reader[Inventory_UnEvent.colAdjustNote]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colAdjustUser])) objInventory_UnEvent.AdjustUser = Convert.ToString(reader[Inventory_UnEvent.colAdjustUser]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colAdjustDate])) objInventory_UnEvent.AdjustDate = Convert.ToDateTime(reader[Inventory_UnEvent.colAdjustDate]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colISReport])) objInventory_UnEvent.ISReport = Convert.ToBoolean(reader[Inventory_UnEvent.colISReport]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colCostPrice])) objInventory_UnEvent.CostPrice = Convert.ToDecimal(reader[Inventory_UnEvent.colCostPrice]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colSalePrice])) objInventory_UnEvent.SalePrice = Convert.ToDecimal(reader[Inventory_UnEvent.colSalePrice]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colDutyUser])) objInventory_UnEvent.DutyUser = Convert.ToString(reader[Inventory_UnEvent.colDutyUser]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colCreatedStoreID])) objInventory_UnEvent.CreatedStoreID = Convert.ToInt32(reader[Inventory_UnEvent.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colCreatedUser])) objInventory_UnEvent.CreatedUser = Convert.ToString(reader[Inventory_UnEvent.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colCreatedDate])) objInventory_UnEvent.CreatedDate = Convert.ToDateTime(reader[Inventory_UnEvent.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colUpdatedUser])) objInventory_UnEvent.UpdatedUser = Convert.ToString(reader[Inventory_UnEvent.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colUpdatedDate])) objInventory_UnEvent.UpdatedDate = Convert.ToDateTime(reader[Inventory_UnEvent.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colIsDeleted])) objInventory_UnEvent.IsDeleted = Convert.ToBoolean(reader[Inventory_UnEvent.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colDeletedUser])) objInventory_UnEvent.DeletedUser = Convert.ToString(reader[Inventory_UnEvent.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_UnEvent.colDeletedDate])) objInventory_UnEvent.DeletedDate = Convert.ToDateTime(reader[Inventory_UnEvent.colDeletedDate]);
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory_UnEvent -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objInventory_UnEvent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(Inventory_UnEvent objInventory_UnEvent)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInventory_UnEvent);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory_UnEvent -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventory_UnEvent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, Inventory_UnEvent objInventory_UnEvent)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + Inventory_UnEvent.colUnEventID, objInventory_UnEvent.UnEventID);
				objIData.AddParameter("@" + Inventory_UnEvent.colInventoryID, objInventory_UnEvent.InventoryID);
				objIData.AddParameter("@" + Inventory_UnEvent.colInventoryDate, objInventory_UnEvent.InventoryDate);
				objIData.AddParameter("@" + Inventory_UnEvent.colProductID, objInventory_UnEvent.ProductID);
				objIData.AddParameter("@" + Inventory_UnEvent.colCabinetNumber, objInventory_UnEvent.CabinetNumber);
				objIData.AddParameter("@" + Inventory_UnEvent.colStockIMEI, objInventory_UnEvent.StockIMEI);
				objIData.AddParameter("@" + Inventory_UnEvent.colStockQuantity, objInventory_UnEvent.StockQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colInventoryIMEI, objInventory_UnEvent.InventoryIMEI);
				objIData.AddParameter("@" + Inventory_UnEvent.colInventoryQuantity, objInventory_UnEvent.InventoryQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colUnEventQuantity, objInventory_UnEvent.UnEventQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colUnEventExplain, objInventory_UnEvent.UnEventExplain);
				objIData.AddParameter("@" + Inventory_UnEvent.colExplainNote, objInventory_UnEvent.ExplainNote);
				objIData.AddParameter("@" + Inventory_UnEvent.colExplainUser, objInventory_UnEvent.ExplainUser);
				objIData.AddParameter("@" + Inventory_UnEvent.colExplainDate, objInventory_UnEvent.ExplainDate);
				objIData.AddParameter("@" + Inventory_UnEvent.colAdjustUnEventQuantity, objInventory_UnEvent.AdjustUnEventQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colFinalUnEventQuantity, objInventory_UnEvent.FinalUnEventQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colTotalCost, objInventory_UnEvent.TotalCost);
				objIData.AddParameter("@" + Inventory_UnEvent.colAdjustNote, objInventory_UnEvent.AdjustNote);
				objIData.AddParameter("@" + Inventory_UnEvent.colAdjustUser, objInventory_UnEvent.AdjustUser);
				objIData.AddParameter("@" + Inventory_UnEvent.colAdjustDate, objInventory_UnEvent.AdjustDate);
				objIData.AddParameter("@" + Inventory_UnEvent.colISReport, objInventory_UnEvent.ISReport);
				objIData.AddParameter("@" + Inventory_UnEvent.colCostPrice, objInventory_UnEvent.CostPrice);
				objIData.AddParameter("@" + Inventory_UnEvent.colSalePrice, objInventory_UnEvent.SalePrice);
				objIData.AddParameter("@" + Inventory_UnEvent.colDutyUser, objInventory_UnEvent.DutyUser);
				objIData.AddParameter("@" + Inventory_UnEvent.colCreatedStoreID, objInventory_UnEvent.CreatedStoreID);
				objIData.AddParameter("@" + Inventory_UnEvent.colCreatedUser, objInventory_UnEvent.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objInventory_UnEvent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(Inventory_UnEvent objInventory_UnEvent)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInventory_UnEvent);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory_UnEvent -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventory_UnEvent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, Inventory_UnEvent objInventory_UnEvent)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + Inventory_UnEvent.colUnEventID, objInventory_UnEvent.UnEventID);
				objIData.AddParameter("@" + Inventory_UnEvent.colInventoryID, objInventory_UnEvent.InventoryID);
				objIData.AddParameter("@" + Inventory_UnEvent.colInventoryDate, objInventory_UnEvent.InventoryDate);
				objIData.AddParameter("@" + Inventory_UnEvent.colProductID, objInventory_UnEvent.ProductID);
				objIData.AddParameter("@" + Inventory_UnEvent.colCabinetNumber, objInventory_UnEvent.CabinetNumber);
				objIData.AddParameter("@" + Inventory_UnEvent.colStockIMEI, objInventory_UnEvent.StockIMEI);
				objIData.AddParameter("@" + Inventory_UnEvent.colStockQuantity, objInventory_UnEvent.StockQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colInventoryIMEI, objInventory_UnEvent.InventoryIMEI);
				objIData.AddParameter("@" + Inventory_UnEvent.colInventoryQuantity, objInventory_UnEvent.InventoryQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colUnEventQuantity, objInventory_UnEvent.UnEventQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colUnEventExplain, objInventory_UnEvent.UnEventExplain);
				objIData.AddParameter("@" + Inventory_UnEvent.colExplainNote, objInventory_UnEvent.ExplainNote);
				objIData.AddParameter("@" + Inventory_UnEvent.colExplainUser, objInventory_UnEvent.ExplainUser);
				objIData.AddParameter("@" + Inventory_UnEvent.colExplainDate, objInventory_UnEvent.ExplainDate);
				objIData.AddParameter("@" + Inventory_UnEvent.colAdjustUnEventQuantity, objInventory_UnEvent.AdjustUnEventQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colFinalUnEventQuantity, objInventory_UnEvent.FinalUnEventQuantity);
				objIData.AddParameter("@" + Inventory_UnEvent.colTotalCost, objInventory_UnEvent.TotalCost);
				objIData.AddParameter("@" + Inventory_UnEvent.colAdjustNote, objInventory_UnEvent.AdjustNote);
				objIData.AddParameter("@" + Inventory_UnEvent.colAdjustUser, objInventory_UnEvent.AdjustUser);
				objIData.AddParameter("@" + Inventory_UnEvent.colAdjustDate, objInventory_UnEvent.AdjustDate);
				objIData.AddParameter("@" + Inventory_UnEvent.colISReport, objInventory_UnEvent.ISReport);
				objIData.AddParameter("@" + Inventory_UnEvent.colCostPrice, objInventory_UnEvent.CostPrice);
				objIData.AddParameter("@" + Inventory_UnEvent.colSalePrice, objInventory_UnEvent.SalePrice);
				objIData.AddParameter("@" + Inventory_UnEvent.colDutyUser, objInventory_UnEvent.DutyUser);
				objIData.AddParameter("@" + Inventory_UnEvent.colCreatedStoreID, objInventory_UnEvent.CreatedStoreID);
				objIData.AddParameter("@" + Inventory_UnEvent.colUpdatedUser, objInventory_UnEvent.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objInventory_UnEvent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(Inventory_UnEvent objInventory_UnEvent)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInventory_UnEvent);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory_UnEvent -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventory_UnEvent">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, Inventory_UnEvent objInventory_UnEvent)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + Inventory_UnEvent.colUnEventID, objInventory_UnEvent.UnEventID);
				objIData.AddParameter("@" + Inventory_UnEvent.colDeletedUser, objInventory_UnEvent.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_Inventory_UnEvent()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "Inventory_UnEvent_ADD";
		public const String SP_UPDATE = "Inventory_UnEvent_UPD";
		public const String SP_DELETE = "Inventory_UnEvent_DEL";
		public const String SP_SELECT = "Inventory_UnEvent_SEL";
		public const String SP_SEARCH = "Inventory_UnEvent_SRH";
		public const String SP_UPDATEINDEX = "Inventory_UnEvent_UPDINDEX";
		#endregion
		
	}
}
