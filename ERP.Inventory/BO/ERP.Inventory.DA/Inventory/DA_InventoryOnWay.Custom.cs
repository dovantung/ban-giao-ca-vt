
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO.Inventory;
using System.Linq;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 05/25/2018 
	/// Phiếu kiểm kê hàng đi đường
	/// </summary>	
	public partial class DA_InventoryOnWay
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_InventoryOnWay.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_INVENToRYONWAY -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public ResultMessage SearchDataToList(ref List<INV_InventoryOnWay> lstINV_InventoryOnWay, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                if (lstINV_InventoryOnWay == null)
                    lstINV_InventoryOnWay = new List<INV_InventoryOnWay>();
                else
                    lstINV_InventoryOnWay.Clear();

                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InventoryOnWay.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent = new List<INV_InventoryOnWay_AttachMent>();
                DA_InventoryOnWay_Attachment objDA_InventoryOnWay_Attachment = new DA_InventoryOnWay_Attachment();
                while (reader.Read())
                {
                    INV_InventoryOnWay objINV_InventoryOnWay = new INV_InventoryOnWay();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colStoreChangeOrderID])) objINV_InventoryOnWay.StoreChangeOrderID = Convert.ToString(reader[INV_InventoryOnWay.colStoreChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colOutputVoucherID])) objINV_InventoryOnWay.OutputVoucherID = Convert.ToString(reader[INV_InventoryOnWay.colOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colFromStoreID])) objINV_InventoryOnWay.FromStoreID = Convert.ToInt32(reader[INV_InventoryOnWay.colFromStoreID]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colToStoreID])) objINV_InventoryOnWay.ToStoreID = Convert.ToInt32(reader[INV_InventoryOnWay.colToStoreID]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colInputVoucherID])) objINV_InventoryOnWay.InputVoucherID = Convert.ToString(reader[INV_InventoryOnWay.colInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colOutputDate])) objINV_InventoryOnWay.OutputDate = Convert.ToDateTime(reader[INV_InventoryOnWay.colOutputDate]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colQuantity])) objINV_InventoryOnWay.Quantity = Convert.ToDecimal(reader[INV_InventoryOnWay.colQuantity]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colTotalAmount])) objINV_InventoryOnWay.TotalAmount = Convert.ToDecimal(reader[INV_InventoryOnWay.colTotalAmount]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colOutputUser])) objINV_InventoryOnWay.OutputUser = Convert.ToString(reader[INV_InventoryOnWay.colOutputUser]).Trim();
                    if (!Convert.IsDBNull(reader["FromStoreName"])) objINV_InventoryOnWay.FromStoreName = Convert.ToString(reader["FromStoreName"]).Trim();
                    if (!Convert.IsDBNull(reader["ToStoreName"])) objINV_InventoryOnWay.ToStoreName = Convert.ToString(reader["ToStoreName"]).Trim();
                    if (!Convert.IsDBNull(reader["OutputUserFullName"])) objINV_InventoryOnWay.OutputUserFullName = Convert.ToString(reader["OutputUserFullName"]).Trim();
                    if (!Convert.IsDBNull(reader["IsReviewed"])) objINV_InventoryOnWay.IsReviewed = Convert.ToBoolean(reader["IsReviewed"]);

                    //lstINV_InventoryOnWay_AttachMent = new List<INV_InventoryOnWay_AttachMent>();
                    //objDA_InventoryOnWay_Attachment.SearchDataToList(objIData, ref lstINV_InventoryOnWay_AttachMent, new object[] { "@StoreChangeOrderID", objINV_InventoryOnWay.StoreChangeOrderID });
                    //objINV_InventoryOnWay.InventoryOnWay_AttachMentList = lstINV_InventoryOnWay_AttachMent;
                    lstINV_InventoryOnWay.Add(objINV_InventoryOnWay);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_INVENToRYONWAY -> SearchDataToList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage SearchDataDetail(ref List<INV_InventoryOnWayDetail> lstINV_InventoryOnWayDetail, ref List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                if (lstINV_InventoryOnWayDetail == null)
                    lstINV_InventoryOnWayDetail = new List<INV_InventoryOnWayDetail>();
                else
                    lstINV_InventoryOnWayDetail.Clear();

                if (lstINV_InventoryOnWay_AttachMent == null)
                    lstINV_InventoryOnWay_AttachMent = new List<INV_InventoryOnWay_AttachMent>();
                else
                    lstINV_InventoryOnWay_AttachMent.Clear();

                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InventoryOnWayDetail.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    INV_InventoryOnWayDetail objINV_InventoryOnWayDetail = new INV_InventoryOnWayDetail();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colINVENToRYONWAYDetailID])) objINV_InventoryOnWayDetail.INVENToRYONWAYDetailID = Convert.ToString(reader[INV_InventoryOnWayDetail.colINVENToRYONWAYDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colStoreChangeOrderID])) objINV_InventoryOnWayDetail.StoreChangeOrderID = Convert.ToString(reader[INV_InventoryOnWayDetail.colStoreChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colProductID])) objINV_InventoryOnWayDetail.ProductID = Convert.ToString(reader[INV_InventoryOnWayDetail.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colQuantity])) objINV_InventoryOnWayDetail.Quantity = Convert.ToDecimal(reader[INV_InventoryOnWayDetail.colQuantity]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colIMEI])) objINV_InventoryOnWayDetail.IMEI = Convert.ToString(reader[INV_InventoryOnWayDetail.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colCheckQuantity])) objINV_InventoryOnWayDetail.CheckQuantity = Convert.ToDecimal(reader[INV_InventoryOnWayDetail.colCheckQuantity]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colNote])) objINV_InventoryOnWayDetail.Note = Convert.ToString(reader[INV_InventoryOnWayDetail.colNote]).Trim();
                    if (!Convert.IsDBNull(reader["ProductName"])) objINV_InventoryOnWayDetail.ProductName = Convert.ToString(reader["ProductName"]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colOutputVoucherID])) objINV_InventoryOnWayDetail.OutputVoucherID = Convert.ToString(reader[INV_InventoryOnWayDetail.colOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colInputVoucherID])) objINV_InventoryOnWayDetail.InputVoucherID = Convert.ToString(reader[INV_InventoryOnWayDetail.colInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader["ISINPUTSTORE"])) objINV_InventoryOnWayDetail.IsInputStore = Convert.ToBoolean(reader["ISINPUTSTORE"]);
                    lstINV_InventoryOnWayDetail.Add(objINV_InventoryOnWayDetail);
                }

                objIData.CreateNewStoredProcedure(DA_InventoryOnWay_Attachment.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent = new INV_InventoryOnWay_AttachMent();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID])) objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colINVENToRYONWAYAttachMENTID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colStoreChangeOrderID])) objINV_InventoryOnWay_AttachMent.StoreChangeOrderID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colStoreChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colAttachMENTName])) objINV_InventoryOnWay_AttachMent.AttachMENTName = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colAttachMENTName]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colAttachMENTPath])) objINV_InventoryOnWay_AttachMent.AttachMENTPath = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colAttachMENTPath]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDescription])) objINV_InventoryOnWay_AttachMent.Description = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colCreatedUser])) objINV_InventoryOnWay_AttachMent.CreatedUser = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colCreatedDate])) objINV_InventoryOnWay_AttachMent.CreatedDate = Convert.ToDateTime(reader[INV_InventoryOnWay_AttachMent.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colIsDeleted])) objINV_InventoryOnWay_AttachMent.IsDeleted = Convert.ToBoolean(reader[INV_InventoryOnWay_AttachMent.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDeletedUser])) objINV_InventoryOnWay_AttachMent.DeletedUser = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colDeletedDate])) objINV_InventoryOnWay_AttachMent.DeletedDate = Convert.ToDateTime(reader[INV_InventoryOnWay_AttachMent.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colFileID])) objINV_InventoryOnWay_AttachMent.FileID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colFileID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colOutputVoucherID])) objINV_InventoryOnWay_AttachMent.OutputVoucherID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWay_AttachMent.colInputVoucherID])) objINV_InventoryOnWay_AttachMent.InputVoucherID = Convert.ToString(reader[INV_InventoryOnWay_AttachMent.colInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader["ISINPUTSTORE"])) objINV_InventoryOnWay_AttachMent.IsInputStore = Convert.ToBoolean(reader["ISINPUTSTORE"]);
                    lstINV_InventoryOnWay_AttachMent.Add(objINV_InventoryOnWay_AttachMent);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chi tiết phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_INVENToRYONWAY -> SearchDataDetail", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage InsertAndUpdate(List<INV_InventoryOnWay> lstINV_InventoryOnWay, string strUserName, Boolean bolIsConfirm)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                DA_InventoryOnWayDetail objDA_InventoryOnWayDetail = new DA_InventoryOnWayDetail();
                objDA_InventoryOnWayDetail.objLogObject.CertificateString = objLogObject.CertificateString;
                objDA_InventoryOnWayDetail.objLogObject.UserHostAddress = objLogObject.UserHostAddress;
                objDA_InventoryOnWayDetail.objLogObject.LoginLogID = objLogObject.LoginLogID;
                DA_InventoryOnWay_Attachment objDA_InventoryOnWay_Attachment = new DA_InventoryOnWay_Attachment();
                foreach (INV_InventoryOnWay objINV_InventoryOnWay in lstINV_InventoryOnWay)
                {
                    if (objINV_InventoryOnWay.InventoryOnWayDetailList != null && objINV_InventoryOnWay.InventoryOnWayDetailList.Count > 0)
                    {
                        if(objINV_InventoryOnWay.InventoryOnWayDetailList.Any(x=>string.IsNullOrEmpty(x.INVENToRYONWAYDetailID)))
                        {
                            objINV_InventoryOnWay.CreatedUser = strUserName;
                            objINV_InventoryOnWay.IsReviewed = bolIsConfirm;
                            Insert(objIData, objINV_InventoryOnWay);

                            foreach(INV_InventoryOnWayDetail objINV_InventoryOnWayDetail in objINV_InventoryOnWay.InventoryOnWayDetailList)
                            {
                                objINV_InventoryOnWayDetail.StoreChangeOrderID = objINV_InventoryOnWay.StoreChangeOrderID;
                                objINV_InventoryOnWayDetail.OutputVoucherID = objINV_InventoryOnWay.OutputVoucherID;
                                objINV_InventoryOnWayDetail.InputVoucherID = objINV_InventoryOnWay.InputVoucherID;
                                objINV_InventoryOnWayDetail.CreatedUser = strUserName;
                                objDA_InventoryOnWayDetail.Insert(objIData, objINV_InventoryOnWayDetail);
                            }

                            if (objINV_InventoryOnWay.InventoryOnWay_AttachMentList != null)
                            {
                                foreach (INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent in objINV_InventoryOnWay.InventoryOnWay_AttachMentList)
                                {
                                    objINV_InventoryOnWay_AttachMent.CreatedUser = strUserName;
                                    objINV_InventoryOnWay_AttachMent.StoreChangeOrderID = objINV_InventoryOnWay.StoreChangeOrderID;
                                    objINV_InventoryOnWay_AttachMent.OutputVoucherID = objINV_InventoryOnWay.OutputVoucherID;
                                    objINV_InventoryOnWay_AttachMent.InputVoucherID = objINV_InventoryOnWay.InputVoucherID;
                                    objDA_InventoryOnWay_Attachment.Insert(objIData, objINV_InventoryOnWay_AttachMent);
                                }
                            }
                        }
                        else
                        {
                            objINV_InventoryOnWay.UpdatedUser = strUserName;
                            objINV_InventoryOnWay.IsReviewed = bolIsConfirm;
                            Update(objIData, objINV_InventoryOnWay);

                            foreach (INV_InventoryOnWayDetail objINV_InventoryOnWayDetail in objINV_InventoryOnWay.InventoryOnWayDetailList)
                            {
                                objINV_InventoryOnWayDetail.StoreChangeOrderID = objINV_InventoryOnWay.StoreChangeOrderID;
                                objINV_InventoryOnWayDetail.OutputVoucherID = objINV_InventoryOnWay.OutputVoucherID;
                                objINV_InventoryOnWayDetail.InputVoucherID = objINV_InventoryOnWay.InputVoucherID;
                                objINV_InventoryOnWayDetail.UpdatedUser = strUserName;
                                objDA_InventoryOnWayDetail.Update(objIData, objINV_InventoryOnWayDetail);
                            }

                            if (objINV_InventoryOnWay.InventoryOnWay_AttachMentList != null)
                            {
                                foreach (INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent in objINV_InventoryOnWay.InventoryOnWay_AttachMentList)
                                {
                                    if (string.IsNullOrEmpty(objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID))
                                    {
                                        objINV_InventoryOnWay_AttachMent.CreatedUser = strUserName;
                                        objINV_InventoryOnWay_AttachMent.StoreChangeOrderID = objINV_InventoryOnWay.StoreChangeOrderID;
                                        objINV_InventoryOnWay_AttachMent.OutputVoucherID = objINV_InventoryOnWay.OutputVoucherID;
                                        objINV_InventoryOnWay_AttachMent.InputVoucherID = objINV_InventoryOnWay.InputVoucherID;
                                        objDA_InventoryOnWay_Attachment.Insert(objIData, objINV_InventoryOnWay_AttachMent);
                                    }                                   
                                }
                            }

                            if (objINV_InventoryOnWay.InventoryOnWay_AttachMent_DelList != null)
                            {
                                foreach (INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMentDel in objINV_InventoryOnWay.InventoryOnWay_AttachMent_DelList)
                                {
                                    if (string.IsNullOrEmpty(objINV_InventoryOnWay_AttachMentDel.INVENToRYONWAYAttachMENTID))
                                    {
                                        objINV_InventoryOnWay_AttachMentDel.DeletedUser = strUserName;
                                        objDA_InventoryOnWay_Attachment.Delete(objIData, objINV_InventoryOnWay_AttachMentDel);
                                    }
                                }
                            }
                        }
                    }
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi cập nhật thông tin phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay -> InsertAndUpdate", InventoryGlobals.ModuleName);
                objIData.RollBackTransaction();
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

		#endregion
		
		
	}
}
