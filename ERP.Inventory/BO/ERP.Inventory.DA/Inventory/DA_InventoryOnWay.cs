
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using ERP.Inventory.BO.Inventory;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 05/25/2018 
	/// Phiếu kiểm kê hàng đi đường
	/// </summary>	
	public partial class DA_InventoryOnWay
	{

        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWay">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref INV_InventoryOnWay objINV_InventoryOnWay, string strStoreChangeOrderID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + INV_InventoryOnWay.colStoreChangeOrderID, strStoreChangeOrderID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objINV_InventoryOnWay = new INV_InventoryOnWay();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colStoreChangeOrderID])) objINV_InventoryOnWay.StoreChangeOrderID = Convert.ToString(reader[INV_InventoryOnWay.colStoreChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colOutputVoucherID])) objINV_InventoryOnWay.OutputVoucherID = Convert.ToString(reader[INV_InventoryOnWay.colOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colInputVoucherID])) objINV_InventoryOnWay.InputVoucherID = Convert.ToString(reader[INV_InventoryOnWay.colInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colFromStoreID])) objINV_InventoryOnWay.FromStoreID = Convert.ToInt32(reader[INV_InventoryOnWay.colFromStoreID]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colToStoreID])) objINV_InventoryOnWay.ToStoreID = Convert.ToInt32(reader[INV_InventoryOnWay.colToStoreID]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colOutputDate])) objINV_InventoryOnWay.OutputDate = Convert.ToDateTime(reader[INV_InventoryOnWay.colOutputDate]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colQuantity])) objINV_InventoryOnWay.Quantity = Convert.ToDecimal(reader[INV_InventoryOnWay.colQuantity]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colTotalAmount])) objINV_InventoryOnWay.TotalAmount = Convert.ToDecimal(reader[INV_InventoryOnWay.colTotalAmount]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colOutputUser])) objINV_InventoryOnWay.OutputUser = Convert.ToString(reader[INV_InventoryOnWay.colOutputUser]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colIsReviewed])) objINV_InventoryOnWay.IsReviewed = Convert.ToBoolean(reader[INV_InventoryOnWay.colIsReviewed]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colCreatedUser])) objINV_InventoryOnWay.CreatedUser = Convert.ToString(reader[INV_InventoryOnWay.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colUpdatedUser])) objINV_InventoryOnWay.UpdatedUser = Convert.ToString(reader[INV_InventoryOnWay.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colCreatedDate])) objINV_InventoryOnWay.CreatedDate = Convert.ToDateTime(reader[INV_InventoryOnWay.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[INV_InventoryOnWay.colUpdatedDate])) objINV_InventoryOnWay.UpdatedDate = Convert.ToString(reader[INV_InventoryOnWay.colUpdatedDate]).Trim();
 				}
 				else
 				{
 					objINV_InventoryOnWay = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWay">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(INV_InventoryOnWay objINV_InventoryOnWay)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
				Insert(objIData, objINV_InventoryOnWay);
                if(objINV_InventoryOnWay.InventoryOnWayDetailList != null)
                {
                    DA_InventoryOnWayDetail objDA_InventoryOnWayDetail = new DA_InventoryOnWayDetail();
                    foreach(INV_InventoryOnWayDetail objINV_InventoryOnWayDetail in objINV_InventoryOnWay.InventoryOnWayDetailList)
                    {
                        objDA_InventoryOnWayDetail.Insert(objIData, objINV_InventoryOnWayDetail);
                    }
                }

                if(objINV_InventoryOnWay.InventoryOnWay_AttachMentList != null)
                {
                    DA_InventoryOnWay_Attachment objDA_InventoryOnWay_Attachment = new DA_InventoryOnWay_Attachment();
                    foreach(INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent in objINV_InventoryOnWay.InventoryOnWay_AttachMentList)
                    {
                        objDA_InventoryOnWay_Attachment.Insert(objIData, objINV_InventoryOnWay_AttachMent);
                    }
                }

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin phiếu kiểm kê hàng đi đường", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay -> Insert", InventoryGlobals.ModuleName);
                objIData.RollBackTransaction();
                return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objINV_InventoryOnWay">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, INV_InventoryOnWay objINV_InventoryOnWay)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + INV_InventoryOnWay.colStoreChangeOrderID, objINV_InventoryOnWay.StoreChangeOrderID);
                objIData.AddParameter("@" + INV_InventoryOnWay.colOutputVoucherID, objINV_InventoryOnWay.OutputVoucherID);
                objIData.AddParameter("@" + INV_InventoryOnWay.colInputVoucherID, objINV_InventoryOnWay.InputVoucherID);
                objIData.AddParameter("@" + INV_InventoryOnWay.colFromStoreID, objINV_InventoryOnWay.FromStoreID);
                objIData.AddParameter("@" + INV_InventoryOnWay.colToStoreID, objINV_InventoryOnWay.ToStoreID);
                objIData.AddParameter("@" + INV_InventoryOnWay.colOutputDate, objINV_InventoryOnWay.OutputDate);
                objIData.AddParameter("@" + INV_InventoryOnWay.colQuantity, objINV_InventoryOnWay.Quantity);
                objIData.AddParameter("@" + INV_InventoryOnWay.colTotalAmount, objINV_InventoryOnWay.TotalAmount);
                objIData.AddParameter("@" + INV_InventoryOnWay.colOutputUser, objINV_InventoryOnWay.OutputUser);
                objIData.AddParameter("@" + INV_InventoryOnWay.colIsReviewed, objINV_InventoryOnWay.IsReviewed);
                objIData.AddParameter("@" + INV_InventoryOnWay.colCreatedUser, objINV_InventoryOnWay.CreatedUser);
                objIData.AddParameter("@ISINPUTSTORE", objINV_InventoryOnWay.IsInputStore);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWay">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(INV_InventoryOnWay objINV_InventoryOnWay)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objINV_InventoryOnWay);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objINV_InventoryOnWay">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, INV_InventoryOnWay objINV_InventoryOnWay)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + INV_InventoryOnWay.colStoreChangeOrderID, objINV_InventoryOnWay.StoreChangeOrderID);
				objIData.AddParameter("@" + INV_InventoryOnWay.colOutputVoucherID, objINV_InventoryOnWay.OutputVoucherID);
				objIData.AddParameter("@" + INV_InventoryOnWay.colInputVoucherID, objINV_InventoryOnWay.InputVoucherID);
				objIData.AddParameter("@" + INV_InventoryOnWay.colFromStoreID, objINV_InventoryOnWay.FromStoreID);
				objIData.AddParameter("@" + INV_InventoryOnWay.colToStoreID, objINV_InventoryOnWay.ToStoreID);
				objIData.AddParameter("@" + INV_InventoryOnWay.colOutputDate, objINV_InventoryOnWay.OutputDate);
				objIData.AddParameter("@" + INV_InventoryOnWay.colQuantity, objINV_InventoryOnWay.Quantity);
				objIData.AddParameter("@" + INV_InventoryOnWay.colTotalAmount, objINV_InventoryOnWay.TotalAmount);
				objIData.AddParameter("@" + INV_InventoryOnWay.colOutputUser, objINV_InventoryOnWay.OutputUser);
				objIData.AddParameter("@" + INV_InventoryOnWay.colIsReviewed, objINV_InventoryOnWay.IsReviewed);
				objIData.AddParameter("@" + INV_InventoryOnWay.colUpdatedUser, objINV_InventoryOnWay.UpdatedUser);
                objIData.AddParameter("@ISINPUTSTORE", objINV_InventoryOnWay.IsInputStore);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objINV_InventoryOnWay">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(INV_InventoryOnWay objINV_InventoryOnWay)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objINV_InventoryOnWay);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objINV_InventoryOnWay">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, INV_InventoryOnWay objINV_InventoryOnWay)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + INV_InventoryOnWay.colStoreChangeOrderID, objINV_InventoryOnWay.StoreChangeOrderID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
        
        public ResultMessage InventwayAutoReviewed(string lstInputVoucher,string user,DateTime time,int isStoreID)
        {


            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(INVENTWAY_AUTOREVIEWED);
                objIData.AddParameter("@INPUTVOUCHERIDLIST", lstInputVoucher);
                objIData.AddParameter("@USERNAME", user);
                objIData.AddParameter("@EndDate", time);
                objIData.AddParameter("@ISINPUTSTORE ", isStoreID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi tự động giải trình hệ thống", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_InventoryOnWay -> InventwayAutoReviewed", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion


        #region Constructor

        public DA_InventoryOnWay()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "INV_InventoryOnWay_ADD";
		public const String SP_UPDATE = "INV_InventoryOnWay_UPD";
		public const String SP_DELETE = "INV_InventoryOnWay_DEL";
		public const String SP_SELECT = "INV_InventoryOnWay_SEL";
		public const String SP_SEARCH = "INV_InventoryOnWay_SRH";
		public const String SP_UPDATEINDEX = "INV_InventoryOnWay_UPDINDEX";
        public const String INVENTWAY_AUTOREVIEWED = "INV_INVENTWAY_AUTOREVIEWED";
        #endregion

    }
}
