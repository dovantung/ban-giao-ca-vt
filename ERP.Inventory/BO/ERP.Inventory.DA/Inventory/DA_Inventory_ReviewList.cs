
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using Inventory_ReviewList = ERP.Inventory.BO.Inventory.Inventory_ReviewList;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 12/26/2012 
	/// Danh sách duyệt một phiếu kiểm kê
	/// </summary>	
	public partial class DA_Inventory_ReviewList
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách duyệt một phiếu kiểm kê
		/// </summary>
		/// <param name="objInventory_ReviewList">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref Inventory_ReviewList objInventory_ReviewList, string strInventoryID, int intReviewLevelID, string strUserName)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + Inventory_ReviewList.colInventoryID, strInventoryID);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewLevelID, intReviewLevelID);
				objIData.AddParameter("@" + Inventory_ReviewList.colUserName, strUserName);
				IDataReader reader = objIData.ExecStoreToDataReader();
 				
				if (reader.Read())
 				{
                    if (objInventory_ReviewList == null)
                        objInventory_ReviewList = new Inventory_ReviewList();

 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colInventoryID])) objInventory_ReviewList.InventoryID = Convert.ToString(reader[Inventory_ReviewList.colInventoryID]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colReviewLevelID])) objInventory_ReviewList.ReviewLevelID = Convert.ToInt32(reader[Inventory_ReviewList.colReviewLevelID]);
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colUserName])) objInventory_ReviewList.UserName = Convert.ToString(reader[Inventory_ReviewList.colUserName]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colInventoryDate])) objInventory_ReviewList.InventoryDate = Convert.ToDateTime(reader[Inventory_ReviewList.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colReviewStatus])) objInventory_ReviewList.ReviewStatus = Convert.ToInt32(reader[Inventory_ReviewList.colReviewStatus]);
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colIsReviewed])) objInventory_ReviewList.IsReviewed = Convert.ToBoolean(reader[Inventory_ReviewList.colIsReviewed]);
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colNote])) objInventory_ReviewList.Note = Convert.ToString(reader[Inventory_ReviewList.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colReviewedDate])) objInventory_ReviewList.ReviewedDate = Convert.ToDateTime(reader[Inventory_ReviewList.colReviewedDate]);
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colReviewedUserHostAddress])) objInventory_ReviewList.ReviewedUserHostAddress = Convert.ToString(reader[Inventory_ReviewList.colReviewedUserHostAddress]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colReviewedCertificateString])) objInventory_ReviewList.ReviewedCertificateString = Convert.ToString(reader[Inventory_ReviewList.colReviewedCertificateString]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colReviewedLoginLogID])) objInventory_ReviewList.ReviewedLoginLogID = Convert.ToString(reader[Inventory_ReviewList.colReviewedLoginLogID]).Trim();
 					if (!Convert.IsDBNull(reader[Inventory_ReviewList.colCreatedDate])) objInventory_ReviewList.CreatedDate = Convert.ToDateTime(reader[Inventory_ReviewList.colCreatedDate]);
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách duyệt một phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory_ReviewList -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách duyệt một phiếu kiểm kê
		/// </summary>
		/// <param name="objInventory_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(Inventory_ReviewList objInventory_ReviewList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInventory_ReviewList);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách duyệt một phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory_ReviewList -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách duyệt một phiếu kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventory_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, Inventory_ReviewList objInventory_ReviewList)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + Inventory_ReviewList.colInventoryID, objInventory_ReviewList.InventoryID);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewLevelID, objInventory_ReviewList.ReviewLevelID);
				objIData.AddParameter("@" + Inventory_ReviewList.colUserName, objInventory_ReviewList.UserName);
				objIData.AddParameter("@" + Inventory_ReviewList.colInventoryDate, objInventory_ReviewList.InventoryDate);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewStatus, objInventory_ReviewList.ReviewStatus);
				objIData.AddParameter("@" + Inventory_ReviewList.colIsReviewed, objInventory_ReviewList.IsReviewed);
				objIData.AddParameter("@" + Inventory_ReviewList.colNote, objInventory_ReviewList.Note);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewedDate, objInventory_ReviewList.ReviewedDate);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewedUserHostAddress, objInventory_ReviewList.ReviewedUserHostAddress);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewedCertificateString, objInventory_ReviewList.ReviewedCertificateString);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewedLoginLogID, objInventory_ReviewList.ReviewedLoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin danh sách duyệt một phiếu kiểm kê
		/// </summary>
		/// <param name="objInventory_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(Inventory_ReviewList objInventory_ReviewList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInventory_ReviewList);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách duyệt một phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory_ReviewList -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách duyệt một phiếu kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventory_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, Inventory_ReviewList objInventory_ReviewList)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + Inventory_ReviewList.colInventoryID, objInventory_ReviewList.InventoryID);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewLevelID, objInventory_ReviewList.ReviewLevelID);
				objIData.AddParameter("@" + Inventory_ReviewList.colUserName, objInventory_ReviewList.UserName);
				objIData.AddParameter("@" + Inventory_ReviewList.colInventoryDate, objInventory_ReviewList.InventoryDate);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewStatus, objInventory_ReviewList.ReviewStatus);
				objIData.AddParameter("@" + Inventory_ReviewList.colIsReviewed, objInventory_ReviewList.IsReviewed);
				objIData.AddParameter("@" + Inventory_ReviewList.colNote, objInventory_ReviewList.Note);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewedDate, objInventory_ReviewList.ReviewedDate);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewedUserHostAddress, objInventory_ReviewList.ReviewedUserHostAddress);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewedCertificateString, objInventory_ReviewList.ReviewedCertificateString);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewedLoginLogID, objInventory_ReviewList.ReviewedLoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin danh sách duyệt một phiếu kiểm kê
		/// </summary>
		/// <param name="objInventory_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(Inventory_ReviewList objInventory_ReviewList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInventory_ReviewList);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách duyệt một phiếu kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory_ReviewList -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách duyệt một phiếu kiểm kê
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventory_ReviewList">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, Inventory_ReviewList objInventory_ReviewList)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + Inventory_ReviewList.colInventoryID, objInventory_ReviewList.InventoryID);
				objIData.AddParameter("@" + Inventory_ReviewList.colReviewLevelID, objInventory_ReviewList.ReviewLevelID);
				objIData.AddParameter("@" + Inventory_ReviewList.colUserName, objInventory_ReviewList.UserName);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_Inventory_ReviewList()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "Inventory_REVIEWLIST_ADD";
		public const String SP_UPDATE = "Inventory_REVIEWLIST_UPD";
		public const String SP_DELETE = "Inventory_REVIEWLIST_DEL";
		public const String SP_SELECT = "Inventory_REVIEWLIST_SEL";
		public const String SP_SEARCH = "Inventory_REVIEWLIST_SRH";
		public const String SP_UPDATEINDEX = "Inventory_REVIEWLIST_UPDINDEX";
		#endregion
		
	}
}
