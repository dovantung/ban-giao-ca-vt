
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using QuickInventory = ERP.Inventory.BO.Inventory.QuickInventory;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 14/06/2014 
	/// 
	/// </summary>	
	public partial class DA_QuickInventory
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objQuickInventory">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref QuickInventory objQuickInventory, string strQuickInventoryID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + QuickInventory.colQuickInventoryID, strQuickInventoryID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objQuickInventory = new QuickInventory();
 					if (!Convert.IsDBNull(reader[QuickInventory.colQuickInventoryID])) objQuickInventory.QuickInventoryID = Convert.ToString(reader[QuickInventory.colQuickInventoryID]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventory.colInventoryStoreID])) objQuickInventory.InventoryStoreID = Convert.ToInt32(reader[QuickInventory.colInventoryStoreID]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colMainGroupID])) objQuickInventory.MainGroupID = Convert.ToInt32(reader[QuickInventory.colMainGroupID]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colSubGroupID])) objQuickInventory.SubGroupID = Convert.ToInt32(reader[QuickInventory.colSubGroupID]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colBrandID])) objQuickInventory.BrandID = Convert.ToInt32(reader[QuickInventory.colBrandID]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colIsNewType])) objQuickInventory.IsNewType = Convert.ToInt32(reader[QuickInventory.colIsNewType]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colLockStockTime])) objQuickInventory.LockStockTime = Convert.ToDateTime(reader[QuickInventory.colLockStockTime]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colInventoryDate])) objQuickInventory.InventoryDate = Convert.ToDateTime(reader[QuickInventory.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colInventoryUser])) objQuickInventory.InventoryUser = Convert.ToString(reader[QuickInventory.colInventoryUser]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventory.colContent])) objQuickInventory.Content = Convert.ToString(reader[QuickInventory.colContent]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventory.colIsReviewed])) objQuickInventory.IsReviewed = Convert.ToBoolean(reader[QuickInventory.colIsReviewed]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colReviewedUser])) objQuickInventory.ReviewedUser = Convert.ToString(reader[QuickInventory.colReviewedUser]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventory.colReviewedDate])) objQuickInventory.ReviewedDate = Convert.ToDateTime(reader[QuickInventory.colReviewedDate]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colCreatedStoreID])) objQuickInventory.CreatedStoreID = Convert.ToInt32(reader[QuickInventory.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colCreatedUser])) objQuickInventory.CreatedUser = Convert.ToString(reader[QuickInventory.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventory.colCreatedDate])) objQuickInventory.CreatedDate = Convert.ToDateTime(reader[QuickInventory.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colUpdatedUser])) objQuickInventory.UpdatedUser = Convert.ToString(reader[QuickInventory.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventory.colUpdatedDate])) objQuickInventory.UpdatedDate = Convert.ToDateTime(reader[QuickInventory.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colIsDeleted])) objQuickInventory.IsDeleted = Convert.ToBoolean(reader[QuickInventory.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colDeletedUser])) objQuickInventory.DeletedUser = Convert.ToString(reader[QuickInventory.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[QuickInventory.colDeletedDate])) objQuickInventory.DeletedDate = Convert.ToDateTime(reader[QuickInventory.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[QuickInventory.colDeletedNote])) objQuickInventory.DeletedNote = Convert.ToString(reader[QuickInventory.colDeletedNote]).Trim();
                    if (!Convert.IsDBNull(reader[QuickInventory.colNote])) objQuickInventory.Note = Convert.ToString(reader[QuickInventory.colNote]).Trim();
                }
 				else
 				{
 					objQuickInventory = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objQuickInventory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(QuickInventory objQuickInventory, ref string strQuickInventoryID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();

                strQuickInventoryID = GetNewID(objIData, objQuickInventory.CreatedStoreID);
                objQuickInventory.QuickInventoryID = strQuickInventoryID;
                Insert(objIData, objQuickInventory);

                DA_QuickInventoryDetail objDA_QuickInventoryDetail = new DA_QuickInventoryDetail();

                if (objQuickInventory.QuickInventoryDetailList != null && objQuickInventory.QuickInventoryDetailList.Count > 0)
                {
                    foreach (BO.Inventory.QuickInventoryDetail objQuickInventoryDetail in objQuickInventory.QuickInventoryDetailList)
                    {
                        objQuickInventoryDetail.QuickInventoryID = objQuickInventory.QuickInventoryID;
                        objQuickInventoryDetail.InventoryDate = objQuickInventory.InventoryDate;
                        objQuickInventoryDetail.InventoryStoreID = objQuickInventory.InventoryStoreID;
                        objQuickInventoryDetail.CreatedStoreID = objQuickInventory.CreatedStoreID;
                        objQuickInventoryDetail.CreatedUser = objQuickInventory.CreatedUser;
                        objDA_QuickInventoryDetail.Insert(objIData, objQuickInventoryDetail);
                    }
                }
                if (objQuickInventory.BrandIDList != null && objQuickInventory.BrandIDList.Count > 0)
                {
                    for (int i = 0; i < objQuickInventory.BrandIDList.Count; i++)
                    {
                        objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_BRAND_ADD");
                        objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                        objIData.AddParameter("@BrandID", objQuickInventory.BrandIDList[i]);
                        objIData.ExecNonQuery();
                    }
                }

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
                objIData.RollBackTransaction();
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objQuickInventory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, QuickInventory objQuickInventory)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + QuickInventory.colQuickInventoryID, objQuickInventory.QuickInventoryID);
				objIData.AddParameter("@" + QuickInventory.colInventoryStoreID, objQuickInventory.InventoryStoreID);
				objIData.AddParameter("@" + QuickInventory.colMainGroupID, objQuickInventory.MainGroupID);
				objIData.AddParameter("@" + QuickInventory.colSubGroupID, objQuickInventory.SubGroupID);
				objIData.AddParameter("@" + QuickInventory.colBrandID, objQuickInventory.BrandID);
				objIData.AddParameter("@" + QuickInventory.colIsNewType, objQuickInventory.IsNewType);
				objIData.AddParameter("@" + QuickInventory.colLockStockTime, objQuickInventory.LockStockTime);
				//objIData.AddParameter("@" + QuickInventory.colInventoryDate, objQuickInventory.InventoryDate);
				objIData.AddParameter("@" + QuickInventory.colInventoryUser, objQuickInventory.InventoryUser);
				objIData.AddParameter("@" + QuickInventory.colContent, objQuickInventory.Content);
                objIData.AddParameter("@" + QuickInventory.colNote, objQuickInventory.Note);
                objIData.AddParameter("@" + QuickInventory.colIsReviewed, objQuickInventory.IsReviewed);
				objIData.AddParameter("@" + QuickInventory.colReviewedUser, objQuickInventory.ReviewedUser);
				//objIData.AddParameter("@" + QuickInventory.colReviewedDate, objQuickInventory.ReviewedDate);
				objIData.AddParameter("@" + QuickInventory.colCreatedStoreID, objQuickInventory.CreatedStoreID);
				objIData.AddParameter("@" + QuickInventory.colCreatedUser, objQuickInventory.CreatedUser);
				objIData.AddParameter("@" + QuickInventory.colDeletedNote, objQuickInventory.DeletedNote);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objQuickInventory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Update(QuickInventory objQuickInventory, List<BO.Inventory.QuickInventoryDetail> DeleteList)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
                objIData.Connect();
                objIData.BeginTransaction();

                Update(objIData, objQuickInventory);

                DA_QuickInventoryDetail objDA_QuickInventoryDetail = new DA_QuickInventoryDetail();

                if (objQuickInventory.QuickInventoryDetailList != null && objQuickInventory.QuickInventoryDetailList.Count > 0)
                {
                    foreach (BO.Inventory.QuickInventoryDetail objQuickInventoryDetail in objQuickInventory.QuickInventoryDetailList)
                    {
                        objQuickInventoryDetail.QuickInventoryID = objQuickInventory.QuickInventoryID;
                        objQuickInventoryDetail.InventoryDate = objQuickInventory.InventoryDate;
                        objQuickInventoryDetail.InventoryStoreID = objQuickInventory.InventoryStoreID;
                        objQuickInventoryDetail.CreatedStoreID = objQuickInventory.CreatedStoreID;
                        if (objQuickInventoryDetail.QuickInventoryDetailID != null && Convert.ToString(objQuickInventoryDetail.QuickInventoryDetailID).Trim() != string.Empty)
                        {
                            objQuickInventoryDetail.UpdatedUser = objQuickInventory.UpdatedUser;
                            objDA_QuickInventoryDetail.Update(objIData, objQuickInventoryDetail);
                        }
                        else
                        {
                            objQuickInventoryDetail.CreatedUser = objQuickInventory.UpdatedUser;
                            objDA_QuickInventoryDetail.Insert(objIData, objQuickInventoryDetail);
                        }
                    }
                }
                if (DeleteList != null && DeleteList.Count > 0)
                {
                    foreach (BO.Inventory.QuickInventoryDetail objDelete in DeleteList)
                    {
                        if (objDelete.QuickInventoryDetailID != null && Convert.ToString(objDelete.QuickInventoryDetailID).Trim() != string.Empty)
                        {
                            objDelete.DeletedUser = objQuickInventory.UpdatedUser;
                            objDA_QuickInventoryDetail.Delete(objIData, objDelete);
                        }
                    }
                }
                objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_BRAND_DEL");
                objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                objIData.ExecNonQuery();
                if (objQuickInventory.BrandIDList != null && objQuickInventory.BrandIDList.Count > 0)
                {
                    for (int i = 0; i < objQuickInventory.BrandIDList.Count; i++)
                    {
                        objIData.CreateNewStoredProcedure("INV_QUICKINVENTORY_BRAND_ADD");
                        objIData.AddParameter("@QuickInventoryID", objQuickInventory.QuickInventoryID);
                        objIData.AddParameter("@BrandID", objQuickInventory.BrandIDList[i]);
                        objIData.ExecNonQuery();
                    }
                }

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
                objIData.RollBackTransaction();
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objQuickInventory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, QuickInventory objQuickInventory)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + QuickInventory.colQuickInventoryID, objQuickInventory.QuickInventoryID);
				objIData.AddParameter("@" + QuickInventory.colInventoryStoreID, objQuickInventory.InventoryStoreID);
				objIData.AddParameter("@" + QuickInventory.colMainGroupID, objQuickInventory.MainGroupID);
				objIData.AddParameter("@" + QuickInventory.colSubGroupID, objQuickInventory.SubGroupID);
				objIData.AddParameter("@" + QuickInventory.colBrandID, objQuickInventory.BrandID);
				objIData.AddParameter("@" + QuickInventory.colIsNewType, objQuickInventory.IsNewType);
				objIData.AddParameter("@" + QuickInventory.colLockStockTime, objQuickInventory.LockStockTime);
				objIData.AddParameter("@" + QuickInventory.colInventoryDate, objQuickInventory.InventoryDate);
				objIData.AddParameter("@" + QuickInventory.colInventoryUser, objQuickInventory.InventoryUser);
				objIData.AddParameter("@" + QuickInventory.colContent, objQuickInventory.Content);
				objIData.AddParameter("@" + QuickInventory.colIsReviewed, objQuickInventory.IsReviewed);
				objIData.AddParameter("@" + QuickInventory.colReviewedUser, objQuickInventory.ReviewedUser);
				objIData.AddParameter("@" + QuickInventory.colReviewedDate, objQuickInventory.ReviewedDate);
				objIData.AddParameter("@" + QuickInventory.colCreatedStoreID, objQuickInventory.CreatedStoreID);
				objIData.AddParameter("@" + QuickInventory.colUpdatedUser, objQuickInventory.UpdatedUser);
				objIData.AddParameter("@" + QuickInventory.colDeletedNote, objQuickInventory.DeletedNote);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objQuickInventory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(QuickInventory objQuickInventory)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objQuickInventory);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_QuickInventory -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objQuickInventory">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, QuickInventory objQuickInventory)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + QuickInventory.colQuickInventoryID, objQuickInventory.QuickInventoryID);
				objIData.AddParameter("@" + QuickInventory.colDeletedUser, objQuickInventory.DeletedUser);
                objIData.AddParameter("@" + QuickInventory.colDeletedNote, objQuickInventory.DeletedNote);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_QuickInventory()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "INV_QuickInventory_ADD";
		public const String SP_UPDATE = "INV_QuickInventory_UPD";
		public const String SP_DELETE = "INV_QuickInventory_DEL";
		public const String SP_SELECT = "INV_QuickInventory_SEL";
		public const String SP_SEARCH = "INV_QuickInventory_SRH";
		//public const String SP_UPDATEINDEX = "INV_QuickInventory_UPDINDEX";
		#endregion
		
	}
}
