
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 12/8/2012 
	/// Khai báo kỳ kiểm kê
	/// </summary>	
	public partial class DA_InventoryTerm
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin khai báo kỳ kiểm kê
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_InventoryTerm.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin khai báo kỳ kiểm kê", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public ResultMessage SearchData_ByUser(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InventoryTerm.SP_SEARCH_BYUSER);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin khai báo kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetAllStore(ref DataTable dtbStore, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Store_GetList");
                objIData.AddParameter(objKeywords);
                dtbStore = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy kho kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> GetAllStore", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetAllMainGroup(ref DataTable dtbMainGroup, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_MainGroup_GetList");
                objIData.AddParameter(objKeywords);
                dtbMainGroup = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy ngành hàng kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> GetAllMainGroup", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetAllBrand(ref DataTable dtbBrand, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Brand_GetList");
                objIData.AddParameter(objKeywords);
                dtbBrand = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy nhà sản xuất kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> GetAllBrand", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetStore(ref DataTable dtbStore, int intInventoryTermID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Store_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                dtbStore = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi lấy danh sách kho của kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> GetStore", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void GetStore(string strUserName, IData objIData, ref DataTable dtbStore, int intInventoryTermID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Store_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@UserName", strUserName);
                dtbStore = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public void GetStore_ByUser(string strUserName, IData objIData, ref DataTable dtbStore, int intInventoryTermID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Store_GetList_ByUser");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.AddParameter("@UserName", strUserName);
                dtbStore = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage GetMainGroup(ref DataTable dtbMainGroup, int intInventoryTermID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_MainGroup_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                dtbMainGroup = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi lấy danh sách ngành hàng của kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> GetMainGroup", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void GetMainGroup(IData objIData, ref DataTable dtbMainGroup, int intInventoryTermID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_MainGroup_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                dtbMainGroup = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage GetBrand(ref DataTable dtbBrand, int intInventoryTermID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Brand_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                dtbBrand = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi lấy danh sách nhà sản xuất của kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> GetBrand", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void GetBrand(IData objIData, ref DataTable dtbBrand, int intInventoryTermID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_Brand_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                dtbBrand = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public void GetSubGroup(IData objIData, ref DataTable dtbSubGroup, int intInventoryTermID)
        {
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_Term_SubGroup_GetList");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                dtbSubGroup = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public void InsertStore(IData objIData, DataTable dtbStore, ERP.Inventory.BO.Inventory.InventoryTerm objInventoryTerm, string strUserName)
        {
            try
            {
                for (int i = 0; i < dtbStore.Rows.Count; i++)
                {
                    DataRow rStore = dtbStore.Rows[i];
                    if (Convert.ToBoolean(rStore["IsSelect"]))
                    {
                        objIData.CreateNewStoredProcedure("INV_Term_Store_Add");
                        objIData.AddParameter("@InventoryTermID", objInventoryTerm.InventoryTermID);
                        objIData.AddParameter("@StoreID", Convert.ToInt32(rStore["StoreID"]));
                        objIData.AddParameter("@InventoryDate", objInventoryTerm.InventoryDate);
                        objIData.AddParameter("@InventoryUser", Convert.ToString(rStore["InventoryUser"]));
                        objIData.AddParameter("@IsUpdateStockData", false);
                        objIData.AddParameter("@UpdateStockDataTime", null);
                        objIData.ExecNonQuery();
                    }
                }
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public void DeleteStore(IData objIData, int intInventoryTermID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("INV_Term_Store_Del");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public void InsertMainGroup(IData objIData, DataTable dtbMainGroup, ERP.Inventory.BO.Inventory.InventoryTerm objInventoryTerm)
        {
            try
            {
                for (int i = 0; i < dtbMainGroup.Rows.Count; i++)
                {
                    DataRow rMainGroup = dtbMainGroup.Rows[i];
                    if (Convert.ToBoolean(rMainGroup["IsSelect"]))
                    {
                        objIData.CreateNewStoredProcedure("INV_Term_MainGroup_Add");
                        objIData.AddParameter("@InventoryTermID", objInventoryTerm.InventoryTermID);
                        objIData.AddParameter("@MainGroupID", Convert.ToInt32(rMainGroup["MainGroupID"]));
                        objIData.AddParameter("@InventoryDate", objInventoryTerm.InventoryDate);
                        objIData.ExecNonQuery();
                    }
                }
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public void DeleteMainGroup(IData objIData, int intInventoryTermID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("INV_Term_MainGroup_Del");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public void InsertBrand(IData objIData, DataTable dtbBrand, ERP.Inventory.BO.Inventory.InventoryTerm objInventoryTerm)
        {
            try
            {
                for (int i = 0; i < dtbBrand.Rows.Count; i++)
                {
                    DataRow rBrand = dtbBrand.Rows[i];
                    if (Convert.ToBoolean(rBrand["IsSelect"]))
                    {
                        objIData.CreateNewStoredProcedure("INV_Term_Brand_Add");
                        objIData.AddParameter("@InventoryTermID", objInventoryTerm.InventoryTermID);
                        objIData.AddParameter("@BrandID", Convert.ToInt32(rBrand["BrandID"]));
                        objIData.AddParameter("@InventoryDate", objInventoryTerm.InventoryDate);
                        objIData.ExecNonQuery();
                    }
                }
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public void DeleteBrand(IData objIData, int intInventoryTermID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("INV_Term_Brand_Del");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public void InsertSubGroup(IData objIData, DataTable dtbSubGroup, ERP.Inventory.BO.Inventory.InventoryTerm objInventoryTerm)
        {
            try
            {
                for (int i = 0; i < dtbSubGroup.Rows.Count; i++)
                {
                    DataRow rSubGroup = dtbSubGroup.Rows[i];
                    if (Convert.ToBoolean(rSubGroup["IsSelect"]))
                    {
                        objIData.CreateNewStoredProcedure("INV_Term_SubGroup_Add");
                        objIData.AddParameter("@InventoryTermID", objInventoryTerm.InventoryTermID);
                        objIData.AddParameter("@SubGroupID", Convert.ToInt32(rSubGroup["SubGroupID"]));
                        objIData.AddParameter("@InventoryDate", objInventoryTerm.InventoryDate);
                        objIData.ExecNonQuery();
                    }
                }
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public void DeleteSubGroup(IData objIData, int intInventoryTermID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("INV_Term_SubGroup_Del");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage Delete(int intInventoryTermID, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + ERP.Inventory.BO.Inventory.InventoryTerm.colInventoryTermID, intInventoryTermID);
                objIData.AddParameter("@" + ERP.Inventory.BO.Inventory.InventoryTerm.colDeletedUser, strUserName);
                objIData.ExecNonQuery();

                DeleteStore(objIData, intInventoryTermID);
                DeleteMainGroup(objIData, intInventoryTermID);
                DeleteBrand(objIData, intInventoryTermID);

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi xóa thông tin khai báo kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CalStockData(int intInventoryTermID, DataTable dtbStore)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                objIData.CreateNewStoredProcedure("INV_TermCalStockData");
                objIData.AddParameter("@InventoryTermID", intInventoryTermID);
                objIData.ExecNonQuery();
                objIData.CommitTransaction();
                //objResultMessage.IsError = true;
                //objResultMessage.Message = "Da Run";
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi tính tồn kho kỳ kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> CalStockData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        #endregion

        public ResultMessage SearchDataCauseGroups(ref DataTable dtbData)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INV_CAUSEGROUPS_SRH");
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin khai báo nhóm nguyên nhân lỗi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryTerm -> SearchDataCauseGroups", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
    }
}
