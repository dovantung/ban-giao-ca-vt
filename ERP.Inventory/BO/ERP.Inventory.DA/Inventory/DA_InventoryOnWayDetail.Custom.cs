
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO.Inventory;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 05/25/2018 
	/// Chi tiết phiếu kiểm kê hàng đi đường
	/// </summary>	
	public partial class DA_InventoryOnWayDetail
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin chi tiết phiếu kiểm kê hàng đi đường
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_InventoryOnWayDetail.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chi tiết phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_INVENToRYONWAYDetail -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public ResultMessage SearchDataToList(ref List<INV_InventoryOnWayDetail> lstINV_InventoryOnWayDetail, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                if (lstINV_InventoryOnWayDetail == null)
                    lstINV_InventoryOnWayDetail = new List<INV_InventoryOnWayDetail>();
                else
                    lstINV_InventoryOnWayDetail.Clear();

                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InventoryOnWayDetail.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    INV_InventoryOnWayDetail objINV_InventoryOnWayDetail = new INV_InventoryOnWayDetail();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colStoreChangeOrderID])) objINV_InventoryOnWayDetail.StoreChangeOrderID = Convert.ToString(reader[INV_InventoryOnWayDetail.colStoreChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colProductID])) objINV_InventoryOnWayDetail.ProductID = Convert.ToString(reader[INV_InventoryOnWayDetail.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colQuantity])) objINV_InventoryOnWayDetail.Quantity = Convert.ToDecimal(reader[INV_InventoryOnWayDetail.colQuantity]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colIMEI])) objINV_InventoryOnWayDetail.IMEI = Convert.ToString(reader[INV_InventoryOnWayDetail.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colCheckQuantity])) objINV_InventoryOnWayDetail.CheckQuantity = Convert.ToDecimal(reader[INV_InventoryOnWayDetail.colCheckQuantity]);
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colNote])) objINV_InventoryOnWayDetail.Note = Convert.ToString(reader[INV_InventoryOnWayDetail.colNote]).Trim();
                    if (!Convert.IsDBNull(reader["ProductName"])) objINV_InventoryOnWayDetail.ProductName = Convert.ToString(reader["ProductName"]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colOutputVoucherID])) objINV_InventoryOnWayDetail.OutputVoucherID = Convert.ToString(reader[INV_InventoryOnWayDetail.colOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[INV_InventoryOnWayDetail.colInputVoucherID])) objINV_InventoryOnWayDetail.InputVoucherID = Convert.ToString(reader[INV_InventoryOnWayDetail.colInputVoucherID]).Trim();

                    lstINV_InventoryOnWayDetail.Add(objINV_InventoryOnWayDetail);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chi tiết phiếu kiểm kê hàng đi đường", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_INV_INVENToRYONWAYDetail -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

		#endregion
		
		
	}
}
