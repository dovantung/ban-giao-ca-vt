
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InventoryProcess_PrChange = ERP.Inventory.BO.Inventory.InventoryProcess_PrChange;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 01/28/13 
	/// Chi tiết quản lý yêu cầu kiểm kê - nhập đổi
	/// </summary>	
	public partial class DA_InventoryProcess_PrChange
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi
		/// </summary>
		/// <param name="objInventoryProcess_PrChange">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InventoryProcess_PrChange objInventoryProcess_PrChange, string strProductChangeProcessID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductChangeProcessID, strProductChangeProcessID);
				IDataReader reader = objIData.ExecStoreToDataReader();

				if (reader.Read())
 				{
                    if (objInventoryProcess_PrChange == null)
                        objInventoryProcess_PrChange = new InventoryProcess_PrChange();

 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colProductChangeProcessID])) objInventoryProcess_PrChange.ProductChangeProcessID = Convert.ToString(reader[InventoryProcess_PrChange.colProductChangeProcessID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colInventoryProcessID])) objInventoryProcess_PrChange.InventoryProcessID = Convert.ToString(reader[InventoryProcess_PrChange.colInventoryProcessID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colInventoryDate])) objInventoryProcess_PrChange.InventoryDate = Convert.ToDateTime(reader[InventoryProcess_PrChange.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colProductID_Out])) objInventoryProcess_PrChange.ProductID_Out = Convert.ToString(reader[InventoryProcess_PrChange.colProductID_Out]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colIMEI_Out])) objInventoryProcess_PrChange.IMEI_Out = Convert.ToString(reader[InventoryProcess_PrChange.colIMEI_Out]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colProductID_In])) objInventoryProcess_PrChange.ProductID_In = Convert.ToString(reader[InventoryProcess_PrChange.colProductID_In]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colIMEI_In])) objInventoryProcess_PrChange.IMEI_In = Convert.ToString(reader[InventoryProcess_PrChange.colIMEI_In]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colQuantity])) objInventoryProcess_PrChange.Quantity = Convert.ToDecimal(reader[InventoryProcess_PrChange.colQuantity]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colRefSalePrice_Out])) objInventoryProcess_PrChange.RefSalePrice_Out = Convert.ToDecimal(reader[InventoryProcess_PrChange.colRefSalePrice_Out]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colRefSalePrice_In])) objInventoryProcess_PrChange.RefSalePrice_In = Convert.ToDecimal(reader[InventoryProcess_PrChange.colRefSalePrice_In]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colCollectArrearPrice])) objInventoryProcess_PrChange.CollectArrearPrice = Convert.ToDecimal(reader[InventoryProcess_PrChange.colCollectArrearPrice]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colUnEventAmount])) objInventoryProcess_PrChange.UnEventAmount = Convert.ToDecimal(reader[InventoryProcess_PrChange.colUnEventAmount]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colUnEventID])) objInventoryProcess_PrChange.UnEventID = Convert.ToString(reader[InventoryProcess_PrChange.colUnEventID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colProductChangID])) objInventoryProcess_PrChange.ProductChangID = Convert.ToString(reader[InventoryProcess_PrChange.colProductChangID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colProductChangeDetailID])) objInventoryProcess_PrChange.ProductChangeDetailID = Convert.ToString(reader[InventoryProcess_PrChange.colProductChangeDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colCreatedStoreID])) objInventoryProcess_PrChange.CreatedStoreID = Convert.ToInt32(reader[InventoryProcess_PrChange.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colCreatedUser])) objInventoryProcess_PrChange.CreatedUser = Convert.ToString(reader[InventoryProcess_PrChange.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colCreatedDate])) objInventoryProcess_PrChange.CreatedDate = Convert.ToDateTime(reader[InventoryProcess_PrChange.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colUpdatedUser])) objInventoryProcess_PrChange.UpdatedUser = Convert.ToString(reader[InventoryProcess_PrChange.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colUpdatedDate])) objInventoryProcess_PrChange.UpdatedDate = Convert.ToDateTime(reader[InventoryProcess_PrChange.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colIsDeleted])) objInventoryProcess_PrChange.IsDeleted = Convert.ToBoolean(reader[InventoryProcess_PrChange.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colDeletedUser])) objInventoryProcess_PrChange.DeletedUser = Convert.ToString(reader[InventoryProcess_PrChange.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colDeletedDate])) objInventoryProcess_PrChange.DeletedDate = Convert.ToDateTime(reader[InventoryProcess_PrChange.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_PrChange.colDeletedReason])) objInventoryProcess_PrChange.DeletedReason = Convert.ToString(reader[InventoryProcess_PrChange.colDeletedReason]).Trim();

 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_PrChange -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi
		/// </summary>
		/// <param name="objInventoryProcess_PrChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InventoryProcess_PrChange objInventoryProcess_PrChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInventoryProcess_PrChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_PrChange -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryProcess_PrChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InventoryProcess_PrChange objInventoryProcess_PrChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductChangeProcessID, objInventoryProcess_PrChange.ProductChangeProcessID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colInventoryProcessID, objInventoryProcess_PrChange.InventoryProcessID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colInventoryDate, objInventoryProcess_PrChange.InventoryDate);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductID_Out, objInventoryProcess_PrChange.ProductID_Out);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colIMEI_Out, objInventoryProcess_PrChange.IMEI_Out);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductID_In, objInventoryProcess_PrChange.ProductID_In);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colIMEI_In, objInventoryProcess_PrChange.IMEI_In);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colQuantity, objInventoryProcess_PrChange.Quantity);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colRefSalePrice_Out, objInventoryProcess_PrChange.RefSalePrice_Out);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colRefSalePrice_In, objInventoryProcess_PrChange.RefSalePrice_In);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colCollectArrearPrice, objInventoryProcess_PrChange.CollectArrearPrice);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colUnEventAmount, objInventoryProcess_PrChange.UnEventAmount);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colUnEventID, objInventoryProcess_PrChange.UnEventID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductChangID, objInventoryProcess_PrChange.ProductChangID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductChangeDetailID, objInventoryProcess_PrChange.ProductChangeDetailID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colCreatedStoreID, objInventoryProcess_PrChange.CreatedStoreID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colCreatedUser, objInventoryProcess_PrChange.CreatedUser);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colDeletedReason, objInventoryProcess_PrChange.DeletedReason);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi
		/// </summary>
		/// <param name="objInventoryProcess_PrChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InventoryProcess_PrChange objInventoryProcess_PrChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInventoryProcess_PrChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_PrChange -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryProcess_PrChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InventoryProcess_PrChange objInventoryProcess_PrChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductChangeProcessID, objInventoryProcess_PrChange.ProductChangeProcessID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colInventoryProcessID, objInventoryProcess_PrChange.InventoryProcessID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colInventoryDate, objInventoryProcess_PrChange.InventoryDate);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductID_Out, objInventoryProcess_PrChange.ProductID_Out);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colIMEI_Out, objInventoryProcess_PrChange.IMEI_Out);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductID_In, objInventoryProcess_PrChange.ProductID_In);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colIMEI_In, objInventoryProcess_PrChange.IMEI_In);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colQuantity, objInventoryProcess_PrChange.Quantity);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colRefSalePrice_Out, objInventoryProcess_PrChange.RefSalePrice_Out);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colRefSalePrice_In, objInventoryProcess_PrChange.RefSalePrice_In);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colCollectArrearPrice, objInventoryProcess_PrChange.CollectArrearPrice);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colUnEventAmount, objInventoryProcess_PrChange.UnEventAmount);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colUnEventID, objInventoryProcess_PrChange.UnEventID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductChangID, objInventoryProcess_PrChange.ProductChangID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductChangeDetailID, objInventoryProcess_PrChange.ProductChangeDetailID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colCreatedStoreID, objInventoryProcess_PrChange.CreatedStoreID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colUpdatedUser, objInventoryProcess_PrChange.UpdatedUser);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colDeletedReason, objInventoryProcess_PrChange.DeletedReason);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi
		/// </summary>
		/// <param name="objInventoryProcess_PrChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InventoryProcess_PrChange objInventoryProcess_PrChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInventoryProcess_PrChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_PrChange -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết quản lý yêu cầu kiểm kê - nhập đổi
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryProcess_PrChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InventoryProcess_PrChange objInventoryProcess_PrChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colProductChangeProcessID, objInventoryProcess_PrChange.ProductChangeProcessID);
				objIData.AddParameter("@" + InventoryProcess_PrChange.colDeletedUser, objInventoryProcess_PrChange.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InventoryProcess_PrChange()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "INV_INVENTORYPROCESS_PRChangE_ADD";
		public const String SP_UPDATE = "INV_INVENTORYPROCESS_PRChangE_UPD";
		public const String SP_DELETE = "INV_INVENTORYPROCESS_PRChangE_DEL";
		public const String SP_SELECT = "INV_INVENTORYPROCESS_PRChangE_SEL";
		public const String SP_SEARCH = "INV_INVENTORYPROCESS_PRChangE_SRH";
		public const String SP_UPDATEINDEX = "INV_INVENTORYPROCESS_PRChangE_UPDINDEX";
		#endregion
		
	}
}
