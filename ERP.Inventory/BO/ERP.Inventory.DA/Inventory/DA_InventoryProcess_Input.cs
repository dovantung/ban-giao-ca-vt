
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InventoryProcess_Input = ERP.Inventory.BO.Inventory.InventoryProcess_Input;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 01/28/13 
	/// Chi tiết quản lý yêu cầu kiểm kê - nhập thừa
	/// </summary>	
	public partial class DA_InventoryProcess_Input
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa
		/// </summary>
		/// <param name="objInventoryProcess_Input">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InventoryProcess_Input objInventoryProcess_Input, string strInputProcessID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputProcessID, strInputProcessID);
				IDataReader reader = objIData.ExecStoreToDataReader();

				if (reader.Read())
 				{
                    if (objInventoryProcess_Input == null)
                        objInventoryProcess_Input = new InventoryProcess_Input();

 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colInputProcessID])) objInventoryProcess_Input.InputProcessID = Convert.ToString(reader[InventoryProcess_Input.colInputProcessID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colInventoryProcessID])) objInventoryProcess_Input.InventoryProcessID = Convert.ToString(reader[InventoryProcess_Input.colInventoryProcessID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colInventoryDate])) objInventoryProcess_Input.InventoryDate = Convert.ToDateTime(reader[InventoryProcess_Input.colInventoryDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colProductID])) objInventoryProcess_Input.ProductID = Convert.ToString(reader[InventoryProcess_Input.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colIMEI])) objInventoryProcess_Input.IMEI = Convert.ToString(reader[InventoryProcess_Input.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colQuantity])) objInventoryProcess_Input.Quantity = Convert.ToDecimal(reader[InventoryProcess_Input.colQuantity]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colVAT])) objInventoryProcess_Input.VAT = Convert.ToInt32(reader[InventoryProcess_Input.colVAT]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colVATPercent])) objInventoryProcess_Input.VATPercent = Convert.ToInt32(reader[InventoryProcess_Input.colVATPercent]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colInputPrice])) objInventoryProcess_Input.InputPrice = Convert.ToDecimal(reader[InventoryProcess_Input.colInputPrice]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colUnEventID])) objInventoryProcess_Input.UnEventID = Convert.ToString(reader[InventoryProcess_Input.colUnEventID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colInputVoucherID])) objInventoryProcess_Input.InputVoucherID = Convert.ToString(reader[InventoryProcess_Input.colInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colInputVoucherDetailID])) objInventoryProcess_Input.InputVoucherDetailID = Convert.ToString(reader[InventoryProcess_Input.colInputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colCreatedStoreID])) objInventoryProcess_Input.CreatedStoreID = Convert.ToInt32(reader[InventoryProcess_Input.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colCreatedUser])) objInventoryProcess_Input.CreatedUser = Convert.ToString(reader[InventoryProcess_Input.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colCreatedDate])) objInventoryProcess_Input.CreatedDate = Convert.ToDateTime(reader[InventoryProcess_Input.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colUpdatedUser])) objInventoryProcess_Input.UpdatedUser = Convert.ToString(reader[InventoryProcess_Input.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colUpdatedDate])) objInventoryProcess_Input.UpdatedDate = Convert.ToDateTime(reader[InventoryProcess_Input.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colIsDeleted])) objInventoryProcess_Input.IsDeleted = Convert.ToBoolean(reader[InventoryProcess_Input.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colDeletedUser])) objInventoryProcess_Input.DeletedUser = Convert.ToString(reader[InventoryProcess_Input.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colDeletedDate])) objInventoryProcess_Input.DeletedDate = Convert.ToDateTime(reader[InventoryProcess_Input.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[InventoryProcess_Input.colDeletedReason])) objInventoryProcess_Input.DeletedReason = Convert.ToString(reader[InventoryProcess_Input.colDeletedReason]).Trim();
 				}

				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_Input -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa
		/// </summary>
		/// <param name="objInventoryProcess_Input">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InventoryProcess_Input objInventoryProcess_Input)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInventoryProcess_Input);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_Input -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryProcess_Input">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InventoryProcess_Input objInventoryProcess_Input)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputProcessID, objInventoryProcess_Input.InputProcessID);
				objIData.AddParameter("@" + InventoryProcess_Input.colInventoryProcessID, objInventoryProcess_Input.InventoryProcessID);
				objIData.AddParameter("@" + InventoryProcess_Input.colInventoryDate, objInventoryProcess_Input.InventoryDate);
				objIData.AddParameter("@" + InventoryProcess_Input.colProductID, objInventoryProcess_Input.ProductID);
				objIData.AddParameter("@" + InventoryProcess_Input.colIMEI, objInventoryProcess_Input.IMEI);
				objIData.AddParameter("@" + InventoryProcess_Input.colQuantity, objInventoryProcess_Input.Quantity);
				objIData.AddParameter("@" + InventoryProcess_Input.colVAT, objInventoryProcess_Input.VAT);
				objIData.AddParameter("@" + InventoryProcess_Input.colVATPercent, objInventoryProcess_Input.VATPercent);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputPrice, objInventoryProcess_Input.InputPrice);
				objIData.AddParameter("@" + InventoryProcess_Input.colUnEventID, objInventoryProcess_Input.UnEventID);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputVoucherID, objInventoryProcess_Input.InputVoucherID);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputVoucherDetailID, objInventoryProcess_Input.InputVoucherDetailID);
				objIData.AddParameter("@" + InventoryProcess_Input.colCreatedStoreID, objInventoryProcess_Input.CreatedStoreID);
				objIData.AddParameter("@" + InventoryProcess_Input.colCreatedUser, objInventoryProcess_Input.CreatedUser);
				objIData.AddParameter("@" + InventoryProcess_Input.colDeletedReason, objInventoryProcess_Input.DeletedReason);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa
		/// </summary>
		/// <param name="objInventoryProcess_Input">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InventoryProcess_Input objInventoryProcess_Input)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInventoryProcess_Input);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_Input -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryProcess_Input">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InventoryProcess_Input objInventoryProcess_Input)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputProcessID, objInventoryProcess_Input.InputProcessID);
				objIData.AddParameter("@" + InventoryProcess_Input.colInventoryProcessID, objInventoryProcess_Input.InventoryProcessID);
				objIData.AddParameter("@" + InventoryProcess_Input.colInventoryDate, objInventoryProcess_Input.InventoryDate);
				objIData.AddParameter("@" + InventoryProcess_Input.colProductID, objInventoryProcess_Input.ProductID);
				objIData.AddParameter("@" + InventoryProcess_Input.colIMEI, objInventoryProcess_Input.IMEI);
				objIData.AddParameter("@" + InventoryProcess_Input.colQuantity, objInventoryProcess_Input.Quantity);
				objIData.AddParameter("@" + InventoryProcess_Input.colVAT, objInventoryProcess_Input.VAT);
				objIData.AddParameter("@" + InventoryProcess_Input.colVATPercent, objInventoryProcess_Input.VATPercent);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputPrice, objInventoryProcess_Input.InputPrice);
				objIData.AddParameter("@" + InventoryProcess_Input.colUnEventID, objInventoryProcess_Input.UnEventID);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputVoucherID, objInventoryProcess_Input.InputVoucherID);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputVoucherDetailID, objInventoryProcess_Input.InputVoucherDetailID);
				objIData.AddParameter("@" + InventoryProcess_Input.colCreatedStoreID, objInventoryProcess_Input.CreatedStoreID);
				objIData.AddParameter("@" + InventoryProcess_Input.colUpdatedUser, objInventoryProcess_Input.UpdatedUser);
				objIData.AddParameter("@" + InventoryProcess_Input.colDeletedReason, objInventoryProcess_Input.DeletedReason);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa
		/// </summary>
		/// <param name="objInventoryProcess_Input">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InventoryProcess_Input objInventoryProcess_Input)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInventoryProcess_Input);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InventoryProcess_Input -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết quản lý yêu cầu kiểm kê - nhập thừa
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInventoryProcess_Input">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InventoryProcess_Input objInventoryProcess_Input)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InventoryProcess_Input.colInputProcessID, objInventoryProcess_Input.InputProcessID);
				objIData.AddParameter("@" + InventoryProcess_Input.colDeletedUser, objInventoryProcess_Input.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InventoryProcess_Input()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "INV_INVENTORYPROCESS_INPUT_ADD";
		public const String SP_UPDATE = "INV_INVENTORYPROCESS_INPUT_UPD";
		public const String SP_DELETE = "INV_INVENTORYPROCESS_INPUT_DEL";
		public const String SP_SELECT = "INV_INVENTORYPROCESS_INPUT_SEL";
		public const String SP_SEARCH = "INV_INVENTORYPROCESS_INPUT_SRH";
		public const String SP_UPDATEINDEX = "INV_INVENTORYPROCESS_INPUT_UPDINDEX";
		#endregion
		
	}
}
