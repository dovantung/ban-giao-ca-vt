
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using StoreChangeDetail = ERP.Inventory.BO.StoreChangeDetail;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 10/1/2012 
	/// 
	/// </summary>	
	public partial class DA_StoreChangeDetail
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objStoreChangeDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref StoreChangeDetail objStoreChangeDetail, string strStoreChangeDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeDetailID, strStoreChangeDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objStoreChangeDetail = new StoreChangeDetail();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colStoreChangeDetailID])) objStoreChangeDetail.StoreChangeDetailID = Convert.ToString(reader[StoreChangeDetail.colStoreChangeDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colStoreChangeID])) objStoreChangeDetail.StoreChangeID = Convert.ToString(reader[StoreChangeDetail.colStoreChangeID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colStoreChangeDate])) objStoreChangeDetail.StoreChangeDate = Convert.ToDateTime(reader[StoreChangeDetail.colStoreChangeDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colProductID])) objStoreChangeDetail.ProductID = Convert.ToString(reader[StoreChangeDetail.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colQuantity])) objStoreChangeDetail.Quantity = Convert.ToDecimal(reader[StoreChangeDetail.colQuantity]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colIMEI])) objStoreChangeDetail.IMEI = Convert.ToString(reader[StoreChangeDetail.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colStoreChangeOrderDetailID])) objStoreChangeDetail.StoreChangeOrderDetailID = Convert.ToString(reader[StoreChangeDetail.colStoreChangeOrderDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colOutputVoucherDetailID])) objStoreChangeDetail.OutputVoucherDetailID = Convert.ToString(reader[StoreChangeDetail.colOutputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colInputVoucherDetailID])) objStoreChangeDetail.InputVoucherDetailID = Convert.ToString(reader[StoreChangeDetail.colInputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colPackingNumber])) objStoreChangeDetail.PackingNumber = Convert.ToInt32(reader[StoreChangeDetail.colPackingNumber]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colOrderQuantity])) objStoreChangeDetail.OrderQuantity = Convert.ToDecimal(reader[StoreChangeDetail.colOrderQuantity]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colUserNote])) objStoreChangeDetail.UserNote = Convert.ToString(reader[StoreChangeDetail.colUserNote]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colEndWarrantyDate])) objStoreChangeDetail.EndWarrantyDate = Convert.ToDateTime(reader[StoreChangeDetail.colEndWarrantyDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colIsHasWarranty])) objStoreChangeDetail.IsHasWarranty = Convert.ToBoolean(reader[StoreChangeDetail.colIsHasWarranty]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colInputPrice])) objStoreChangeDetail.InputPrice = Convert.ToDecimal(reader[StoreChangeDetail.colInputPrice]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colOutputPrice])) objStoreChangeDetail.OutputPrice = Convert.ToDecimal(reader[StoreChangeDetail.colOutputPrice]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colCreatedStoreID])) objStoreChangeDetail.CreatedStoreID = Convert.ToInt32(reader[StoreChangeDetail.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colCreatedUser])) objStoreChangeDetail.CreatedUser = Convert.ToString(reader[StoreChangeDetail.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colCreatedDate])) objStoreChangeDetail.CreatedDate = Convert.ToDateTime(reader[StoreChangeDetail.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colUpdatedUser])) objStoreChangeDetail.UpdatedUser = Convert.ToString(reader[StoreChangeDetail.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colUpdatedDate])) objStoreChangeDetail.UpdatedDate = Convert.ToDateTime(reader[StoreChangeDetail.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colIsDeleted])) objStoreChangeDetail.IsDeleted = Convert.ToBoolean(reader[StoreChangeDetail.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colDeletedUser])) objStoreChangeDetail.DeletedUser = Convert.ToString(reader[StoreChangeDetail.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeDetail.colDeletedDate])) objStoreChangeDetail.DeletedDate = Convert.ToDateTime(reader[StoreChangeDetail.colDeletedDate]);
 				}
 				else
 				{
 					objStoreChangeDetail = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objStoreChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(StoreChangeDetail objStoreChangeDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objStoreChangeDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, StoreChangeDetail objStoreChangeDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeDetailID, objStoreChangeDetail.StoreChangeDetailID);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeID, objStoreChangeDetail.StoreChangeID);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeDate, objStoreChangeDetail.StoreChangeDate);
				objIData.AddParameter("@" + StoreChangeDetail.colProductID, objStoreChangeDetail.ProductID);
				objIData.AddParameter("@" + StoreChangeDetail.colQuantity, objStoreChangeDetail.Quantity);
				objIData.AddParameter("@" + StoreChangeDetail.colIMEI, objStoreChangeDetail.IMEI);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeOrderDetailID, objStoreChangeDetail.StoreChangeOrderDetailID);
				objIData.AddParameter("@" + StoreChangeDetail.colOutputVoucherDetailID, objStoreChangeDetail.OutputVoucherDetailID);
				objIData.AddParameter("@" + StoreChangeDetail.colInputVoucherDetailID, objStoreChangeDetail.InputVoucherDetailID);
				objIData.AddParameter("@" + StoreChangeDetail.colPackingNumber, objStoreChangeDetail.PackingNumber);
				objIData.AddParameter("@" + StoreChangeDetail.colOrderQuantity, objStoreChangeDetail.OrderQuantity);
				objIData.AddParameter("@" + StoreChangeDetail.colUserNote, objStoreChangeDetail.UserNote);
				objIData.AddParameter("@" + StoreChangeDetail.colEndWarrantyDate, objStoreChangeDetail.EndWarrantyDate);
				objIData.AddParameter("@" + StoreChangeDetail.colIsHasWarranty, objStoreChangeDetail.IsHasWarranty);
				objIData.AddParameter("@" + StoreChangeDetail.colInputPrice, objStoreChangeDetail.InputPrice);
				objIData.AddParameter("@" + StoreChangeDetail.colOutputPrice, objStoreChangeDetail.OutputPrice);
				objIData.AddParameter("@" + StoreChangeDetail.colCreatedStoreID, objStoreChangeDetail.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeDetail.colCreatedUser, objStoreChangeDetail.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objStoreChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(StoreChangeDetail objStoreChangeDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objStoreChangeDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, StoreChangeDetail objStoreChangeDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeDetailID, objStoreChangeDetail.StoreChangeDetailID);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeID, objStoreChangeDetail.StoreChangeID);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeDate, objStoreChangeDetail.StoreChangeDate);
				objIData.AddParameter("@" + StoreChangeDetail.colProductID, objStoreChangeDetail.ProductID);
				objIData.AddParameter("@" + StoreChangeDetail.colQuantity, objStoreChangeDetail.Quantity);
				objIData.AddParameter("@" + StoreChangeDetail.colIMEI, objStoreChangeDetail.IMEI);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeOrderDetailID, objStoreChangeDetail.StoreChangeOrderDetailID);
				objIData.AddParameter("@" + StoreChangeDetail.colOutputVoucherDetailID, objStoreChangeDetail.OutputVoucherDetailID);
				objIData.AddParameter("@" + StoreChangeDetail.colInputVoucherDetailID, objStoreChangeDetail.InputVoucherDetailID);
				objIData.AddParameter("@" + StoreChangeDetail.colPackingNumber, objStoreChangeDetail.PackingNumber);
				objIData.AddParameter("@" + StoreChangeDetail.colOrderQuantity, objStoreChangeDetail.OrderQuantity);
				objIData.AddParameter("@" + StoreChangeDetail.colUserNote, objStoreChangeDetail.UserNote);
				objIData.AddParameter("@" + StoreChangeDetail.colEndWarrantyDate, objStoreChangeDetail.EndWarrantyDate);
				objIData.AddParameter("@" + StoreChangeDetail.colIsHasWarranty, objStoreChangeDetail.IsHasWarranty);
				objIData.AddParameter("@" + StoreChangeDetail.colInputPrice, objStoreChangeDetail.InputPrice);
				objIData.AddParameter("@" + StoreChangeDetail.colOutputPrice, objStoreChangeDetail.OutputPrice);
				objIData.AddParameter("@" + StoreChangeDetail.colCreatedStoreID, objStoreChangeDetail.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeDetail.colUpdatedUser, objStoreChangeDetail.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objStoreChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(StoreChangeDetail objStoreChangeDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objStoreChangeDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, StoreChangeDetail objStoreChangeDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + StoreChangeDetail.colStoreChangeDetailID, objStoreChangeDetail.StoreChangeDetailID);
				objIData.AddParameter("@" + StoreChangeDetail.colDeletedUser, objStoreChangeDetail.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_StoreChangeDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_STORECHANGEDETAIL_ADD";
		public const String SP_UPDATE = "PM_STORECHANGEDETAIL_UPD";
		public const String SP_DELETE = "PM_STORECHANGEDETAIL_DEL";
		public const String SP_SELECT = "PM_STORECHANGEDETAIL_SEL";
		public const String SP_SEARCH = "PM_STORECHANGEDETAIL_SRH";
		public const String SP_UPDATEINDEX = "PM_STORECHANGEDETAIL_UPDINDEX";
		#endregion
		
	}
}
