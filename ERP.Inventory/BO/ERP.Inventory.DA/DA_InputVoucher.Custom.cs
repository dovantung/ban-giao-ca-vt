
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO;
using ERP.SalesAndServices.Payment.DA;
using System.Linq;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
    /// Created by 		: Nguyễn Linh Tuấn 
    /// Created date 	: 8/13/2012 
    /// Phiếu nhập
    /// </summary>	
    public partial class DA_InputVoucher
    {

        #region Methods
        /// <summary>
        /// Nạp thông tin phiếu nhập và các table quan hệ
        /// </summary>
        /// <param name="objInputVoucher"></param>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public ResultMessage LoadInfo(ref InputVoucher objInputVoucher, string strInputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            try
            {
                //LoadInfo
                objInputVoucher = LoadInfo(strInputVoucherID);
                //load InputVoucherDetail
                DA.DA_InputVoucherDetail objDA_InputVoucherDetail = new DA_InputVoucherDetail();
                objInputVoucher.InputVoucherDetailList = objDA_InputVoucherDetail.GetListByInputVoucherID(strInputVoucherID);
                //Load InputVoucherAttachment
                DA.DA_InputVoucher_Attachment objDA_InputVoucher_Attachment = new DA_InputVoucher_Attachment();
                List<ERP.Inventory.BO.InputVoucher_Attachment> InputVoucher_AttachmnetList = null;
                objDA_InputVoucher_Attachment.GetListByInputVoucherID(ref InputVoucher_AttachmnetList, strInputVoucherID);
                objInputVoucher.InputVoucher_AttachmnetList = InputVoucher_AttachmnetList;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin phiếu nhập", objEx.ToString());
                return objResultMessage;
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin phiếu nhập
        /// </summary>
        /// <param name="objInputVoucher">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage InsertInputVoucherAndVoucher(InputVoucher objInputVoucher, ref string strInputvoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                int strStoreCreateInputVoucherFromRetailInput = objInputVoucher.AutoStoreChangeToStoreID;
                objInputVoucher.AutoStoreChangeToStoreID = 0;

                //thêm phiếu nhập
                Insert(objIData, objInputVoucher);
                strInputvoucherID = objInputVoucher.InputVoucherID;
                //thêm chi tiết phiếu nhập
                if (objInputVoucher.InputVoucherDetailList != null && objInputVoucher.InputVoucherDetailList.Count > 0)
                {
                    DA_InputVoucherDetail objDA_InputVoucherDetail = new DA_InputVoucherDetail();
                    string objInputVoucherDetailIMEIList = string.Empty;
                    int intOrderIndex = 0;
                    foreach (BO.InputVoucherDetail item in objInputVoucher.InputVoucherDetailList)
                    {
                        item.InputVoucherID = objInputVoucher.InputVoucherID;
                        item.CreatedUser = objInputVoucher.CreatedUser;
                        item.FirstCustomerID = objInputVoucher.CustomerID;
                        item.FirstInputDate = objInputVoucher.InputDate;
                        item.CreatedStoreID = objInputVoucher.CreatedStoreID;
                        item.InputDate = objInputVoucher.InputDate;
                        item.IsNew = objInputVoucher.IsNew;
                        item.InputStoreID = objInputVoucher.InputStoreID;
                        //item.CostPrice = item.InputPrice + (item.TaxedFee / item.Quantity) + (item.PurchaseFee / item.Quantity);
                        item.CostPrice = item.InputPrice + item.TaxedFee + item.PurchaseFee;
                        item.FirstPrice = item.InputPrice;
                        item.ORIGINALInputPrice = item.InputPrice;
                        item.FirstInVoiceDate = objInputVoucher.InVoiceDate;
                        item.FirstInputVoucherID = objInputVoucher.InputVoucherID;
                        item.IsCheckRealInput = objInputVoucher.IsCheckRealInput;
                        item.ISAddInStock = true;
                        item.OrderIndex = intOrderIndex;
                        objDA_InputVoucherDetail.Insert(objIData, item);
                        intOrderIndex++;
                        item.FirstInputVoucherDetailID = item.InputVoucherDetailID;
                        objInputVoucherDetailIMEIList += item.InputVoucherDetailID + ",";
                        if (item.InputVoucherDetailIMEIList != null && item.InputVoucherDetailIMEIList.Count > 0)
                        {
                            DA.DA_InputVoucherDetailIMEI objDA_InputVoucherDetailIMEI = new DA.DA_InputVoucherDetailIMEI();
                            if (item.dtbInputVoucherDetailIMEIDTB != null && item.dtbInputVoucherDetailIMEIDTB.Rows.Count > 0)
                            {
                                item.dtbInputVoucherDetailIMEIDTB.AsEnumerable().ToList().ForEach(x => { x["INPUTVOUCHERDETAILID"] = item.InputVoucherDetailID; x["CREATEDUSER"] = objInputVoucher.CreatedUser; x["INPUTUSER"] = objInputVoucher.CreatedUser; x["CREATEDSTOREID"] = item.CreatedStoreID; x["INPUTSTOREID"] = item.InputStoreID; x["COSTPRICE"] = item.CostPrice; x["FIRSTPRICE"] = item.InputPrice; });
                                string strXml = CreateXMLFromDataTable(item.dtbInputVoucherDetailIMEIDTB, "STORECHANGEDETAIL");
                                AddINPUTVOUCHERDETAILxml(objIData, strXml, strInputvoucherID);
                                //objDA_InputVoucherDetailIMEI.DeleteTemp();
                                //if (!objDA_InputVoucherDetailIMEI.BulkCopy(item.dtbInputVoucherDetailIMEIDTB, "ERP.PM_INPUTVOUCHERDETAILIMEI_TMP"))
                                //{
                                //    throw new Exception("Lỗi khi dùng bulkcopy");
                                //}
                                //else
                                //{
                                //    objDA_InputVoucherDetailIMEI.InsertFromTempTable(objIData);
                                //}
                                //if (objInputVoucher.IsInputFromRetailInputPrice)
                                //{
                                //    foreach (BO.InputVoucherDetailIMEI objIMEI in item.InputVoucherDetailIMEIList)
                                //    {
                                //        InsertIMEISalesInfo(objIData, objIMEI);
                                //    }

                                //}
                            }
                            else
                            {
                                foreach (BO.InputVoucherDetailIMEI objIMEI in item.InputVoucherDetailIMEIList)
                                {
                                    objIMEI.InputVoucherDetailID = item.InputVoucherDetailID;
                                    objIMEI.CostPrice = item.CostPrice;
                                    objIMEI.FirstPrice = item.FirstPrice;
                                    objIMEI.InputStoreID = item.InputStoreID;
                                    objIMEI.CreatedStoreID = item.CreatedStoreID;
                                    objIMEI.CreatedUser = item.CreatedUser;
                                    objIMEI.InputUser = objInputVoucher.CreatedUser;
                                    objIMEI.InputDate = item.InputDate;
                                    objDA_InputVoucherDetailIMEI.Insert(objIData, objIMEI);
                                    if (objInputVoucher.IsInputFromRetailInputPrice)
                                    {
                                        InsertIMEISalesInfo(objIData, objIMEI);
                                    }
                                }
                            }

                        }
                        UpdateInvoiceQuantity(objIData, objInputVoucher.OrderID, item.InputVoucherDetailID);
                        //nếu nhập từ đơn hàng thì cập nhật số lượng ở chi tiết đơn hàng để bít là nhập hết chưa
                        if (objInputVoucher.IsInputFromOrder)
                        {
                            // Cập nhật số lượng đã xuất trong OrderDetail
                            UpdateInputedQuantityOrderDetail(objIData, objInputVoucher.OrderID, item.ProductID, item.Quantity);
                        }
                        //nếu nhập máy cũ từ định giá
                        if (objInputVoucher.IsInputFromRetailInputPrice)
                        {
                            // Cập nhật số lượng đã xuất trong RetailInputPrice
                            UpdateInputedQuantityRetailInputPrice(objIData, objInputVoucher.RetailInputPriceID, objInputVoucher.StaffUser, objInputVoucher.InputDate.Value, objInputVoucher.InputVoucherID, objInputVoucher.InputStoreID);
                        }
                    }

                    #region Tạo yêu cầu chuyển kho, phiếu xuất, nhập và hóa đơn từ trung tâm về siêu thị

                    if (objInputVoucher.IsInputFromRetailInputPrice && strStoreCreateInputVoucherFromRetailInput > 0)
                    {
                        objIData.CreateNewStoredProcedure("PR_CREATECHANGEORDERINPUTPRICE");
                        objIData.AddParameter("@" + InputVoucher.colOrderID, objInputVoucher.OrderID);
                        objIData.AddParameter("@" + InputVoucher.colInVoiceID, objInputVoucher.InVoiceID);
                        objIData.AddParameter("@" + InputVoucher.colInVoiceSymbol, objInputVoucher.InVoiceSymbol);
                        objIData.AddParameter("@" + InputVoucher.colDENOMinAToR, objInputVoucher.Denominator);
                        objIData.AddParameter("@" + InputVoucher.colCustomerID, objInputVoucher.CustomerID);
                        objIData.AddParameter("@" + InputVoucher.colContent, objInputVoucher.Content);
                        objIData.AddParameter("@" + InputVoucher.colCreatedStoreID, objInputVoucher.CreatedStoreID);
                        objIData.AddParameter("@" + InputVoucher.colInputStoreID, strStoreCreateInputVoucherFromRetailInput);
                        objIData.AddParameter("@" + InputVoucher.colAutoStoreChangeToStoreID, objInputVoucher.AutoStoreChangeToStoreID);
                        objIData.AddParameter("@" + InputVoucher.colInputTypeID, objInputVoucher.InputTypeID);
                        objIData.AddParameter("@" + InputVoucher.colPayableTypeID, objInputVoucher.PayableTypeID);
                        objIData.AddParameter("@" + InputVoucher.colCurrencyUnitID, objInputVoucher.CurrencyUnitID);
                        objIData.AddParameter("@" + InputVoucher.colDiscountReasonID, objInputVoucher.DiscountReasonID);
                        objIData.AddParameter("@" + InputVoucher.colCreateDate, objInputVoucher.CreateDate);
                        objIData.AddParameter("@" + InputVoucher.colInVoiceDate, objInputVoucher.InVoiceDate);
                        objIData.AddParameter("@" + InputVoucher.colInputDate, objInputVoucher.InputDate);
                        objIData.AddParameter("@" + InputVoucher.colPayABLedate, objInputVoucher.PayableDate);
                        objIData.AddParameter("@" + InputVoucher.colTaxMonth, objInputVoucher.TaxMonth);
                        objIData.AddParameter("@" + InputVoucher.colCurrencyExchange, objInputVoucher.CurrencyExchange);
                        objIData.AddParameter("@" + InputVoucher.colDiscount, objInputVoucher.Discount);
                        objIData.AddParameter("@" + InputVoucher.colPROTECTPriceDiscount, objInputVoucher.ProtectPriceDiscount);
                        objIData.AddParameter("@" + InputVoucher.colTotalAmountBFT, objInputVoucher.TotalAmountBFT);
                        objIData.AddParameter("@" + InputVoucher.colTotalVAT, objInputVoucher.TotalVAT);
                        objIData.AddParameter("@" + InputVoucher.colTotalAmount, objInputVoucher.TotalAmount);
                        objIData.AddParameter("@" + InputVoucher.colCreatedUser, objInputVoucher.CreatedUser);
                        objIData.AddParameter("@" + InputVoucher.colStaffUser, objInputVoucher.StaffUser);
                        objIData.AddParameter("@" + InputVoucher.colIsNew, objInputVoucher.IsNew);
                        objIData.AddParameter("@" + InputVoucher.colIsReturnWithFee, objInputVoucher.IsReturnWithFee);
                        objIData.AddParameter("@" + InputVoucher.colISStoreChange, objInputVoucher.ISStoreChange);
                        objIData.AddParameter("@" + InputVoucher.colIsCheckRealInput, objInputVoucher.IsCheckRealInput);
                        objIData.AddParameter("@" + InputVoucher.colISPOSTED, objInputVoucher.IsPosted);
                        objIData.AddParameter("@" + InputVoucher.colCheckRealInputNote, objInputVoucher.CheckRealInputNote);
                        objIData.AddParameter("@" + InputVoucher.colCheckRealInputUser, objInputVoucher.CheckRealInputUser);
                        objIData.AddParameter("@" + InputVoucher.colCheckRealInputTime, objInputVoucher.CheckRealInputTime);
                        objIData.AddParameter("@" + InputVoucher.colContentDeleted, objInputVoucher.ContentDeleted);
                        objIData.AddParameter("@" + InputVoucher.colIDCardIssueDate, objInputVoucher.IdCardIssueDate);
                        objIData.AddParameter("@" + InputVoucher.colIDCardIssuePlace, objInputVoucher.IdCardIssuePlace);
                        objIData.AddParameter("@" + InputVoucher.colIsErrorProduct, objInputVoucher.IsErrorProduct);
                        objIData.AddParameter("@" + InputVoucher.colErrorProductNote, objInputVoucher.ErrorProductNote);
                        objIData.AddParameter("@" + InputVoucher.colTransportCustomerID, objInputVoucher.TransportCustomerID);
                        objIData.AddParameter("@" + InputVoucher.colIsAutoCreateInvoice, objInputVoucher.IsAutoCreateInvoice);
                        objIData.AddParameter("@" + InputVoucher.colNote, objInputVoucher.Note);
                        objIData.AddParameter("@" + InputVoucherDetail.colProductID, objInputVoucher.InputVoucherDetailList[0].ProductID);
                        objIData.AddParameter("@" + InputVoucherDetail.colQuantity, objInputVoucher.InputVoucherDetailList[0].Quantity);
                        objIData.AddParameter("@" + InputVoucherDetail.colVAT, objInputVoucher.InputVoucherDetailList[0].VAT);
                        objIData.AddParameter("@" + InputVoucherDetail.colVATPercent, objInputVoucher.InputVoucherDetailList[0].VATPercent);
                        objIData.AddParameter("@" + InputVoucherDetail.colInputPrice, objInputVoucher.InputVoucherDetailList[0].InputPrice);
                        objIData.AddParameter("@" + InputVoucherDetail.colORIGINALInputPrice, objInputVoucher.InputVoucherDetailList[0].ORIGINALInputPrice);
                        objIData.AddParameter("@" + InputVoucherDetail.colCostPrice, objInputVoucher.InputVoucherDetailList[0].CostPrice);
                        objIData.AddParameter("@" + InputVoucherDetail.colFirstPrice, objInputVoucher.InputVoucherDetailList[0].FirstPrice);
                        objIData.AddParameter("@" + InputVoucherDetail.colReturnFee, objInputVoucher.InputVoucherDetailList[0].ReturnFee);
                        objIData.AddParameter("@" + InputVoucherDetail.colAdjustPrice, objInputVoucher.InputVoucherDetailList[0].AdjustPrice);
                        objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceContent, objInputVoucher.InputVoucherDetailList[0].AdjustPriceContent);
                        objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceUser, objInputVoucher.InputVoucherDetailList[0].AdjustPriceUser);
                        objIData.AddParameter("@" + InputVoucherDetail.colENDWarrantyDate, objInputVoucher.InputVoucherDetailList[0].ENDWarrantyDate);
                        objIData.AddParameter("@" + InputVoucherDetail.colProductIONDate, objInputVoucher.InputVoucherDetailList[0].ProductIONDate);
                        objIData.AddParameter("@" + InputVoucherDetail.colProductChangeDate, objInputVoucher.InputVoucherDetailList[0].ProductChangeDate);
                        objIData.AddParameter("@" + InputVoucherDetail.colReturnDate, objInputVoucher.InputVoucherDetailList[0].ReturnDate);
                        objIData.AddParameter("@" + InputVoucherDetail.colChangeToOLDDate, objInputVoucher.InputVoucherDetailList[0].ChangeToOLDDate);
                        objIData.AddParameter("@" + InputVoucherDetail.colFirstInputDate, objInputVoucher.InputVoucherDetailList[0].FirstInputDate);
                        objIData.AddParameter("@" + InputVoucherDetail.colFirstInVoiceDate, objInputVoucher.InputVoucherDetailList[0].FirstInVoiceDate);
                        objIData.AddParameter("@" + InputVoucherDetail.colFirstCustomerID, objInputVoucher.InputVoucherDetailList[0].FirstCustomerID);
                        objIData.AddParameter("@" + InputVoucherDetail.colFirstInputTypeID, objInputVoucher.InputVoucherDetailList[0].FirstInputTypeID);
                        objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherID, objInputVoucher.InputVoucherDetailList[0].FirstInputVoucherID);
                        objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherDetailID, objInputVoucher.InputVoucherDetailList[0].FirstInputVoucherDetailID);
                        objIData.AddParameter("@" + InputVoucherDetail.colIsShowProduct, objInputVoucher.InputVoucherDetailList[0].IsShowProduct);
                        objIData.AddParameter("@" + InputVoucherDetail.colIsReturnProduct, objInputVoucher.InputVoucherDetailList[0].IsReturnProduct);
                        objIData.AddParameter("@" + InputVoucherDetail.colISProductChange, objInputVoucher.InputVoucherDetailList[0].ISProductChange);
                        objIData.AddParameter("@" + InputVoucherDetail.colISAddInStock, objInputVoucher.InputVoucherDetailList[0].ISAddInStock);
                        objIData.AddParameter("@" + InputVoucherDetail.colOrderDetailID, objInputVoucher.InputVoucherDetailList[0].OrderDetailID);
                        objIData.AddParameter("@" + InputVoucherDetail.colTaxedFee, objInputVoucher.InputVoucherDetailList[0].TaxedFee);
                        objIData.AddParameter("@" + InputVoucherDetail.colPurchaseFee, objInputVoucher.InputVoucherDetailList[0].PurchaseFee);
                        objIData.AddParameter("@" + InputVoucherDetail.colOrderIndex, objInputVoucher.InputVoucherDetailList[0].OrderIndex);
                        objIData.AddParameter("@REMAINQUANTITY", objInputVoucher.InputVoucherDetailList[0].QuantityRemain);
                        if (objInputVoucher.InputVoucherDetailList[0].InvoiceQuantity > 0)
                            objIData.AddParameter("@" + InputVoucherDetail.colInvoiceQuantity, objInputVoucher.InputVoucherDetailList[0].InvoiceQuantity);
                        objIData.AddParameter("@" + InputVoucherDetailIMEI.colIMEI, objInputVoucher.InputVoucherDetailList[0].InputVoucherDetailIMEIList[0].IMEI);
                        objIData.AddParameter("@" + InputVoucherDetailIMEI.colPINCode, objInputVoucher.InputVoucherDetailList[0].InputVoucherDetailIMEIList[0].PINCode);
                        //objIData.AddParameter("@" + InputVoucherDetailIMEI.colNote, objInputVoucher.InputVoucherDetailList[0].InputVoucherDetailIMEIList[0].Note);
                        objIData.AddParameter("@" + InputVoucherDetailIMEI.colIMEIChangeCHAIN, objInputVoucher.InputVoucherDetailList[0].InputVoucherDetailIMEIList[0].IMEIChangeCHAIN);
                        objIData.AddParameter("@" + InputVoucherDetailIMEI.colIsHasWarranty, objInputVoucher.InputVoucherDetailList[0].InputVoucherDetailIMEIList[0].IsHasWarranty);
                        objIData.AddParameter("@" + InputVoucherDetailIMEI.colInputUser, objInputVoucher.InputVoucherDetailList[0].InputVoucherDetailIMEIList[0].InputUser);
                        objIData.AddParameter("@RETAILINPUTPRICEID", objInputVoucher.RetailInputPriceID);
                        objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                        objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                        objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                        objIData.ExecNonQuery();
                    }

                    #endregion
                }
                //Thêm danh sách file đính kèm
                if (objInputVoucher.InputVoucher_AttachmnetList != null && objInputVoucher.InputVoucher_AttachmnetList.Count > 0)
                {
                    DA_InputVoucher_Attachment objDA_InputVoucher_Attachment = new DA_InputVoucher_Attachment();
                    foreach (BO.InputVoucher_Attachment item in objInputVoucher.InputVoucher_AttachmnetList)
                    {
                        if (item.FileID == string.Empty || item.FilePath == string.Empty)
                            continue;
                        item.InputVoucherID = objInputVoucher.InputVoucherID;
                        objDA_InputVoucher_Attachment.Insert(objIData, item);
                    }
                }
                //Tạo phiếu chi
                if (objInputVoucher.IsCreateOutVoucher)
                {
                    if (objInputVoucher.Voucher != null)
                    {
                        DA_Voucher objDA_Voucher = new DA_Voucher();
                        objInputVoucher.Voucher.VoucherConcern = objInputVoucher.InputVoucherID;
                        objDA_Voucher.CreateVoucher(objIData, objInputVoucher.Voucher);
                    }
                }
                if (objInputVoucher.IsInputFromOrder)
                {
                    // Cập nhật trạng thái nhập hàng của đơn hàng 0: Chưa nhập hàng; 1: Nhập chưa hết; 2: Đã nhập hết
                    UpdateOrder_InputStatus(objIData, objInputVoucher.OrderID);
                    // Tạo nhập xuất nội bộ 
                    //if (objInputVoucher.IntermediateInputStoreID > 0)
                    //{
                    //    if (!AutoStoreChange(objIData, objInputVoucherBO.InputVoucherID, objInputVoucherBO.IntermediateInputStoreID))
                    //    {
                    //        objIData.RollBackTransaction();
                    //        return false;
                    //    }
                    //}
                }
                if (objInputVoucher.IsAutoCreateInvoice)
                {
                    PrintVAT.DA.PrintVAT.DA_VAT_Invoice objDA_VAT_Invoice = new PrintVAT.DA.PrintVAT.DA_VAT_Invoice();
                    objResultMessage = objDA_VAT_Invoice.CreateVATInvoiceInputVoucher(objIData, strInputvoucherID);
                    if (objResultMessage.IsError)
                    {
                        objIData.RollBackTransaction();
                        new SystemError(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceInputVoucher", InventoryGlobals.ModuleName);
                        ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceInputVoucher", InventoryGlobals.ModuleName);
                        return objResultMessage;
                    }
                }
                else
                {
                    PrintVAT.DA.PInvoice.DA_VAT_PInvoice objDA_VAT_PInvoice = new PrintVAT.DA.PInvoice.DA_VAT_PInvoice();
                    //objResultMessage = objDA_VAT_PInvoice.CreateVATInvoiceDT_InputVoucherDetail(objIData, strInputvoucherID, string.Empty);
                    if (objResultMessage.IsError)
                    {
                        objIData.RollBackTransaction();
                        new SystemError(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_PInvoice -> CreateVATInvoiceDT_InputVoucherDetail", InventoryGlobals.ModuleName);
                        ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_PInvoice -> CreateVATInvoiceDT_InputVoucherDetail", InventoryGlobals.ModuleName);
                        return objResultMessage;
                    }
                }

                UpdatePostDate_Pinvoice(objIData, objInputVoucher);

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> InsertInputVoucherAndVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
            //List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
            //if (objInputVoucher.UserNotifyData != null && objInputVoucher.UserNotifyData.Rows.Count > 0)
            //{
            //    foreach (DataRow item in objInputVoucher.UserNotifyData.Rows)
            //    {
            //        Notification.BO.Notification_User objNotification_User = new Notification.BO.Notification_User();
            //        objNotification_User.UserName = item["UserName"].ToString();
            //        if (!objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
            //        {
            //            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
            //            {
            //                bool bolResult = false;
            //                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, objNotification_User.UserName);
            //                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
            //                    objNotification_UserList.Add(objNotification_User);
            //            }
            //            else
            //                objNotification_UserList.Add(objNotification_User);
            //        }
            //    }
            //    objNotification.Notification_UserList = objNotification_UserList;
            //}
            //objNotification.NotificationID = Guid.NewGuid().ToString();
            //objNotification.NotificationTypeID = 8;
            //objNotification.ClickTaskParameters = objInputVoucher.InputVoucherID;
            //objNotification.TaskParameters = "{\"ID\": \"" + objInputVoucher.InputVoucherID + "\"}";
            //objNotification.NotificationName = "Thêm mới phiếu nhập";
            //objNotification.Content = "Nội dung: " + objInputVoucher.Content;
            //objNotification.CreatedUser = objInputVoucher.CreatedUser;
            //objNotification.CreatedDate = DateTime.Now;
            //objNotification.StartDate = DateTime.Now;
            //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
            //ResultMessage objResultMessage1 = new ResultMessage();
            //objResultMessage1 = objDA_Notification.Broadcast(objNotification);
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin bán hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherDetailIMEI">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void InsertIMEISalesInfo(IData objIData, InputVoucherDetailIMEI objInputVoucherDetailIMEI)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_IMEISALESINFO_COLEC");
                objIData.AddParameter("@INPUTVOUCHERDETAILID", objInputVoucherDetailIMEI.InputVoucherDetailID);
                objIData.AddParameter("@PRODUCTID", objInputVoucherDetailIMEI.ProductID);
                objIData.AddParameter("@IMEI", objInputVoucherDetailIMEI.IMEI);
                objIData.AddParameter("@INPUTSTOREID", objInputVoucherDetailIMEI.InputStoreID);
                objIData.AddParameter("@CREATEDUSER", objInputVoucherDetailIMEI.CreatedUser);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Cập nhật trạng thái nhập hàng trên đơn hàng
        /// </summary>
        private void UpdateOrder_InputStatus(IData objIData, String strOrderID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("POM_Order_UpdateIsInputProduct");
                objIData.AddParameter(
                    "@OrderID", strOrderID
                    );
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw objEx;
            }
        }

        /// <summary>
        /// Cập nhật số lượng nhập trên chi tiết đơn hàng
        /// </summary>
        private void UpdateInputedQuantityOrderDetail(IData objIData, String strOrderID, String strProductID, decimal decInputedQuantity)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_OrderDetail_UpdateInptQtity");
                objIData.AddParameter(
                    "@OrderID", strOrderID,
                    "@ProductID", strProductID,
                    "@InputQuantity", decInputedQuantity
                    );

                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw objEx;
            }
        }

        /// <summary>
        /// Cập nhật số lượng nhập trên định giá máy cũ
        /// </summary>
        private void UpdateInputedQuantityRetailInputPrice(IData objIData, String strRetailInputPriceID, string strInputUser, DateTime dtInputDate, string strInputVoucherID, int intInputStoreID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PR_RetailInput_UpdateInptQtity");
                objIData.AddParameter(
                    "@RETAILINPUTPRICEID", strRetailInputPriceID,
                    "@InputUser", strInputUser,
                    "@InputDate", dtInputDate,
                    "@InputVoucherID", strInputVoucherID,
                    "@InputStoreID", intInputStoreID,
                    "@UserHostAddress", objLogObject.UserHostAddress,
                    "@CertificateString", objLogObject.CertificateString,
                    "@LoginLogID", objLogObject.LoginLogID
                    );
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw objEx;
            }
        }

        public ResultMessage Report_GetInputVoucherData(ref DataTable dtbData, string strInputVoucherID, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_Rpt_InputVoucherReport");
                objIData.AddParameter("@InputVoucherID", strInputVoucherID);
                objIData.AddParameter("@UserName", strUserName);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thông tin phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> Report_GetInputVoucherData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// nạp chi tiết phiếu nhập
        /// </summary>
        /// <param name="dtbData"></param>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public ResultMessage Report_GetInputVoucherDetailData(ref DataTable dtbData, string strInputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_Rpt_InputVoucherDetailRpt");
                objIData.AddParameter("@InputVoucherID", strInputVoucherID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi nạp chi tiết phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> Report_GetInputVoucherDetailData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInputVoucherNewID(ref string strInputVoucherID, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strInputVoucherID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi lấy mã phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> GetInputVoucherNewID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm thông tin phiếu nhập
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InputVoucher.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Tìm kiếm thông tin phiếu nhập
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData2(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_SRH2");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// PM_InputVcher_UpdChkRealInput
        /// </summary>
        /// <param name="objInputVoucherList"></param>
        /// <returns></returns>
        public ResultMessage UpdateCheckRealInput(List<ERP.Inventory.BO.InputVoucher> objInputVoucherList, string strUpdateUser)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (BO.InputVoucher objInputVoucher in objInputVoucherList)
                {
                    objIData.CreateNewStoredProcedure("PM_InputVcher_UpdChkRealInput");
                    objIData.AddParameter("@" + InputVoucher.colInputVoucherID, objInputVoucher.InputVoucherID);
                    objIData.AddParameter("@" + InputVoucher.colIsCheckRealInput, objInputVoucher.IsCheckRealInput);
                    objIData.AddParameter("@" + InputVoucher.colCheckRealInputUser, objInputVoucher.CheckRealInputUser);
                    objIData.AddParameter("@" + InputVoucher.colCheckRealInputNote, objInputVoucher.CheckRealInputNote);
                    objIData.AddParameter("@" + InputVoucher.colUpdatedUser, strUpdateUser);
                    objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                    objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                    objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                    objIData.ExecNonQuery();
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi cập nhật thông tin thực nhập của phiếu nhập";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Update, strMessage, objEx.ToString());
                objIData.RollBackTransaction();
                new SystemError(objIData, strMessage, objEx.ToString(), "InputVoucher -> UpdateCheckRealInput", "PM_InputVoucher");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm lịch sử IMEI
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataProductIMEIHistory(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_Product_IMEIHistory");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin lịch sử IMEI", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> SearchDataProductIMEIHistory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Tìm kiếm thông tin IMEI bị lock FIFO hay không
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataProductIMEILockFIFO(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_PRODUCT_IMEILOCKFIFO");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi Tìm kiếm thông tin IMEI bị lock FIFO hay không", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> SearchDataProductIMEILockFIFO", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Lấy ghi chú (Làm giá theo IMEI) theo chuỗi IMEI tìm kiếm 
        /// </summary>
        /// <param name="strNote">Dữ liệu trả về</param>
        /// <param name="strIMEI">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage GetNoteByIMEI(ref string strNote, string strIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PR_PRICEIMEI_GETNOTEBYIMEI");
                objIData.AddParameter("@IMEI", strIMEI);
                strNote = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy ghi chú (Làm giá theo IMEI) theo chuỗi IMEI tìm kiếm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> GetNoteByIMEI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Lấy dữ liệu export cho đơn hàng bán
        /// </summary>
        /// <param name="dtbData"></param>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public ResultMessage GetExportDataToSaleOrder(ref DataTable dtbData, string strInputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHERDETAIL_EXPORT");
                objIData.AddParameter("@InputVoucherID", strInputVoucherID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi lấy dữ liệu export cho đơn hàng bán", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> GetExportDataToSaleOrder", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        #region Đăng Quy trình giao hàng thẳng tại siêu thị

        /// <summary>
        /// Thêm thông tin phiếu nhập -> Giao hàng thẳng
        /// </summary>
        /// <param name="objInputVoucher">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage InsertData(
            InputVoucher objInputVoucher,
            InputVoucher objInputVoucherCenter,
            StoreChangeOrder objStoreChangeOrder,
            BO.StoreChange objStoreChangeBO,
            PrintVAT.BO.PrintVAT.VAT_Invoice objVAT_Invoice,
            string strFormTypeBranchID,
            PrintVAT.BO.PInvoice.VAT_PInvoice objVAT_PInvoice,
            ref string strInputvoucherID,
            ref string strInputvoucherCenterID,
            ref string strStoreChangeID,
            ref string strStoreChangeOrderID,
            ref string strOutPutVoucherID,
            ref string strVATInvoiceID
            )
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                DataTable tblStoreChangeDetail = new DataTable();

                #region phiếu nhập kho trung tâm
                //thêm phiếu nhập
                Insert(objIData, objInputVoucherCenter);
                strInputvoucherCenterID = objInputVoucherCenter.InputVoucherID;
                //thêm chi tiết phiếu nhập
                if (objInputVoucherCenter.InputVoucherDetailList != null && objInputVoucherCenter.InputVoucherDetailList.Count > 0)
                {
                    DA_InputVoucherDetail objDA_InputVoucherDetail = new DA_InputVoucherDetail();
                    string objInputVoucherCenterDetailIMEIList = string.Empty;
                    int intOrderIndex = 0;
                    foreach (BO.InputVoucherDetail item in objInputVoucherCenter.InputVoucherDetailList)
                    {
                        item.InputVoucherID = objInputVoucherCenter.InputVoucherID;
                        item.CreatedUser = objInputVoucherCenter.CreatedUser;
                        item.FirstCustomerID = objInputVoucherCenter.CustomerID;
                        item.FirstInputDate = objInputVoucherCenter.InputDate;
                        item.CreatedStoreID = objInputVoucherCenter.CreatedStoreID;
                        item.InputDate = objInputVoucherCenter.InputDate;
                        item.IsNew = objInputVoucherCenter.IsNew;
                        item.InputStoreID = objInputVoucherCenter.InputStoreID;
                        //item.CostPrice = item.InputPrice + (item.TaxedFee / item.Quantity) + (item.PurchaseFee / item.Quantity);
                        item.CostPrice = item.InputPrice + item.TaxedFee + item.PurchaseFee;
                        item.FirstPrice = item.InputPrice;
                        item.ORIGINALInputPrice = item.InputPrice;
                        item.FirstInVoiceDate = objInputVoucherCenter.InVoiceDate;
                        item.FirstInputVoucherID = objInputVoucherCenter.InputVoucherID;
                        item.IsCheckRealInput = objInputVoucherCenter.IsCheckRealInput;
                        item.ISAddInStock = false;
                        item.OrderIndex = intOrderIndex;
                        objDA_InputVoucherDetail.Insert(objIData, item);
                        intOrderIndex++;
                        objInputVoucherCenterDetailIMEIList += item.InputVoucherDetailID + ",";
                        if (item.InputVoucherDetailIMEIList != null && item.InputVoucherDetailIMEIList.Count > 0)
                        {
                            DA.DA_InputVoucherDetailIMEI objDA_InputVoucherDetailIMEI = new DA.DA_InputVoucherDetailIMEI();
                            if (item.dtbInputVoucherDetailIMEIDTB != null && item.dtbInputVoucherDetailIMEIDTB.Rows.Count > 0)
                            {
                                item.dtbInputVoucherDetailIMEIDTB.AsEnumerable().ToList().ForEach(x => { x["INPUTVOUCHERDETAILID"] = item.InputVoucherDetailID; x["CREATEDUSER"] = objInputVoucherCenter.CreatedUser; x["INPUTUSER"] = objInputVoucherCenter.CreatedUser; x["CREATEDSTOREID"] = item.CreatedStoreID; x["INPUTSTOREID"] = item.InputStoreID; x["COSTPRICE"] = item.CostPrice; x["FIRSTPRICE"] = item.InputPrice; });
                                string strXml = CreateXMLFromDataTable(item.dtbInputVoucherDetailIMEIDTB, "STORECHANGEDETAIL");
                                AddINPUTVOUCHERDETAILxml(objIData, strXml, strInputvoucherID);
                                //objDA_InputVoucherDetailIMEI.DeleteTemp();
                                //if (!objDA_InputVoucherDetailIMEI.BulkCopy(item.dtbInputVoucherDetailIMEIDTB, "ERP.PM_INPUTVOUCHERDETAILIMEI_TMP"))
                                //{
                                //    throw new Exception("Lỗi khi dùng bulkcopy");
                                //}
                                //else
                                //{
                                //    objDA_InputVoucherDetailIMEI.InsertFromTempTable(objIData);
                                //}
                                //if (objInputVoucherCenter.IsInputFromRetailInputPrice)
                                //{
                                //    foreach (BO.InputVoucherDetailIMEI objIMEI in item.InputVoucherDetailIMEIList)
                                //    {
                                //        InsertIMEISalesInfo(objIData, objIMEI);
                                //    }

                                //}
                            }
                            else
                            {
                                foreach (BO.InputVoucherDetailIMEI objIMEI in item.InputVoucherDetailIMEIList)
                                {
                                    objIMEI.InputVoucherDetailID = item.InputVoucherDetailID;
                                    objIMEI.CostPrice = item.CostPrice;
                                    objIMEI.FirstPrice = item.FirstPrice;
                                    objIMEI.InputStoreID = item.InputStoreID;
                                    objIMEI.CreatedStoreID = item.CreatedStoreID;
                                    objIMEI.CreatedUser = item.CreatedUser;
                                    objIMEI.InputUser = objInputVoucherCenter.CreatedUser;
                                    objIMEI.InputDate = item.InputDate;
                                    objDA_InputVoucherDetailIMEI.Insert(objIData, objIMEI);
                                    if (objInputVoucherCenter.IsInputFromRetailInputPrice)
                                    {
                                        InsertIMEISalesInfo(objIData, objIMEI);
                                    }
                                }
                            }

                        }
                        //nếu nhập từ đơn hàng thì cập nhật số lượng ở chi tiết đơn hàng để bít là nhập hết chưa
                        if (objInputVoucherCenter.IsInputFromOrder)
                        {
                            // Cập nhật số lượng đã xuất trong OrderDetail
                            UpdateInputedQuantityOrderDetail(objIData, objInputVoucherCenter.OrderID, item.ProductID, item.Quantity);
                        }
                        //nếu nhập máy cũ từ định giá
                        if (objInputVoucherCenter.IsInputFromRetailInputPrice)
                        {
                            // Cập nhật số lượng đã xuất trong RetailInputPrice
                            UpdateInputedQuantityRetailInputPrice(objIData, objInputVoucherCenter.RetailInputPriceID, objInputVoucherCenter.StaffUser, objInputVoucherCenter.InputDate.Value, objInputVoucherCenter.InputVoucherID, objInputVoucherCenter.CreatedStoreID);
                        }
                    }
                }
                //Thêm danh sách file đính kèm
                if (objInputVoucherCenter.InputVoucher_AttachmnetList != null && objInputVoucherCenter.InputVoucher_AttachmnetList.Count > 0)
                {
                    DA_InputVoucher_Attachment objDA_InputVoucher_Attachment = new DA_InputVoucher_Attachment();
                    foreach (BO.InputVoucher_Attachment item in objInputVoucherCenter.InputVoucher_AttachmnetList)
                    {
                        if (item.FileID == string.Empty || item.FilePath == string.Empty)
                            continue;
                        item.InputVoucherID = objInputVoucherCenter.InputVoucherID;
                        objDA_InputVoucher_Attachment.Insert(objIData, item);
                    }
                }
                //Tạo phiếu chi
                if (objInputVoucherCenter.IsCreateOutVoucher)
                {
                    if (objInputVoucherCenter.Voucher != null)
                    {
                        DA_Voucher objDA_Voucher = new DA_Voucher();
                        objInputVoucherCenter.Voucher.VoucherConcern = objInputVoucherCenter.InputVoucherID;
                        objDA_Voucher.CreateVoucher(objIData, objInputVoucherCenter.Voucher);
                    }
                }
                if (objInputVoucherCenter.IsInputFromOrder)
                {
                    // Cập nhật trạng thái nhập hàng của đơn hàng 0: Chưa nhập hàng; 1: Nhập chưa hết; 2: Đã nhập hết
                    UpdateOrder_InputStatus(objIData, objInputVoucherCenter.OrderID);
                    // Tạo nhập xuất nội bộ 
                    //if (objInputVoucherCenter.IntermediateInputStoreID > 0)
                    //{
                    //    if (!AutoStoreChange(objIData, objInputVoucherCenterBO.InputVoucherID, objInputVoucherCenterBO.IntermediateInputStoreID))
                    //    {
                    //        objIData.RollBackTransaction();
                    //        return false;
                    //    }
                    //}
                }
                //strInputvoucherID = objInputVoucherCenter.InputVoucherID;
                #endregion

                #region Yêu cầu xuất chuyển kho
                new DA_StoreChangeOrder().Insert(objIData, objStoreChangeOrder);
                strStoreChangeOrderID = objStoreChangeOrder.StoreChangeOrderID;
                InsertDetail(objIData, objStoreChangeOrder, objInputVoucherCenter.InputVoucherDetailList);
                //Duyệt tự động
                //objResultMessage = InsertReview(objIData, objStoreChangeOrder);
                //if (objResultMessage.IsError) return objResultMessage;
                #endregion

                #region Xuất chuyển kho
                objStoreChangeBO.OutputVoucherBO.OrderID = strStoreChangeOrderID;
                objInputVoucher.OrderID = strStoreChangeOrderID;
                objStoreChangeBO.StoreChangeOrderID = strStoreChangeOrderID;
                //Tạo phiếu xuất chuyển kho
                objInputVoucher.Note = objInputVoucherCenter.OrderID;
                if (!CreateOuputInputVoucher(ref objResultMessage, objIData, ref objStoreChangeBO, objInputVoucher, objInputVoucherCenter.InputVoucherDetailList, ref strInputvoucherID))
                {
                    objIData.RollBackTransaction();
                    return objResultMessage;
                }

                if (!string.IsNullOrWhiteSpace(objStoreChangeBO.OutputVoucherBO.OutputVoucherID))
                    strOutPutVoucherID = objStoreChangeBO.OutputVoucherBO.OutputVoucherID;
                //không tạo phiếu thu/ chi
                //if (objStoreChangeBO.IsCreateInOutVoucher)
                //{
                //    if (!CreateInOutVoucher(ref objResultMessage, objIData, objStoreChangeBO))
                //    {
                //        objIData.RollBackTransaction();
                //        return;
                //    }
                //}

                //Thêm chi tiết xuất chuyển kho
                //if (!AddStoreChangeDetail1(objIData, objInputVoucherCenter, objInputVoucherCenter.InputVoucherDetailList, objStoreChangeBO))
                //{
                //    objIData.RollBackTransaction();
                //    return objResultMessage;
                //}
                strStoreChangeID = objStoreChangeBO.StoreChangeID;

                //objIData.RollBackTransaction();
                //return objResultMessage;
                PrintVAT.DA.PrintVAT.DA_VAT_Invoice objDA_VAT_Invoice = new PrintVAT.DA.PrintVAT.DA_VAT_Invoice();
                if (objVAT_Invoice != null && objVAT_PInvoice != null && !objResultMessage.IsError)
                {
                    objResultMessage = objDA_VAT_Invoice.CreateVATInvoiceStoreChange(objIData, strInputvoucherID, objStoreChangeBO.OutputVoucherBO.OutputVoucherID, objStoreChangeOrder.StoreChangeOrderTypeID, ref strVATInvoiceID, objStoreChangeBO.OutputVoucherBO.IsSInvoice);
                    if (objResultMessage.IsError)
                    {
                        objIData.RollBackTransaction();
                        new SystemError(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceStoreChange", InventoryGlobals.ModuleName);
                        ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceStoreChange", InventoryGlobals.ModuleName);
                        return objResultMessage;
                    }
                }

                #endregion

                #region Tạo hóa đơn mua hàng
                if (objInputVoucherCenter.IsAutoCreateInvoice)
                {
                    objResultMessage = objDA_VAT_Invoice.CreateVATInvoiceInputVoucher(objIData, strInputvoucherCenterID);
                    if (objResultMessage.IsError)
                    {
                        objIData.RollBackTransaction();
                        new SystemError(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceInputVoucher", InventoryGlobals.ModuleName);
                        ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceInputVoucher", InventoryGlobals.ModuleName);
                        return objResultMessage;
                    }
                }
                else
                {
                    PrintVAT.DA.PInvoice.DA_VAT_PInvoice objDA_VAT_PInvoice = new PrintVAT.DA.PInvoice.DA_VAT_PInvoice();
                    objResultMessage = objDA_VAT_PInvoice.CreateVATInvoiceDT_InputVoucherDetail(objIData, strInputvoucherCenterID, string.Empty);
                    if (objResultMessage.IsError)
                    {
                        objIData.RollBackTransaction();
                        new SystemError(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_PInvoice -> CreateVATInvoiceDT_InputVoucherDetail", InventoryGlobals.ModuleName);
                        ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_PInvoice -> CreateVATInvoiceDT_InputVoucherDetail", InventoryGlobals.ModuleName);
                        return objResultMessage;
                    }
                }

                // Cập nhật lại ngày hạch toán
                UpdatePostDate_Pinvoice(objIData, objInputVoucherCenter);
                #endregion

                objIData.CommitTransaction();

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> InsertData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
                //try
                //{
                //    #region tạo hóa đơn xuất chuyển kho
                //    if (objVAT_Invoice != null && objVAT_PInvoice != null && !objResultMessage.IsError)
                //    {
                //        foreach (PrintVAT.BO.PInvoice.VAT_PInvoice_Product objVAT_PInvoice_Product in objVAT_PInvoice.ProductList)
                //        {
                //            objVAT_PInvoice_Product.InputVoucherDetailID = objInputVoucherCenter.InputVoucherDetailList.Find(x => x.ProductID == objVAT_PInvoice_Product.ProductID).InputVoucherDetailID;
                //        }
                //        objVAT_Invoice.CreateUser = objInputVoucherCenter.CreatedUser;
                //        objVAT_PInvoice.CreateUser = objInputVoucherCenter.CreatedUser;
                //        string strVATINVOICEID = string.Empty;
                //        var objResultMessage2 = new PrintVAT.DA.PrintVAT.DA_VAT_Invoice().Insert_StoreChange(objVAT_Invoice, strFormTypeBranchID, objVAT_PInvoice, strOutPutVoucherID, ref strVATINVOICEID);
                //        if (objResultMessage2.IsError)
                //        {
                //            ErrorLog.Add(objIData, objResultMessage2.Message, objResultMessage2.MessageDetail, "DA_InputVoucher -> InsertData", InventoryGlobals.ModuleName);
                //        }
                //    }
                //    #endregion
                //}
                //catch (Exception ex)
                //{
                //    ErrorLog.Add(objIData, ex.Message, ex.ToString(), "DA_InputVoucher -> InsertData", InventoryGlobals.ModuleName);
                //}

                //CreateInputVoucherTask(objInputVoucherCenter.InputVoucherID);
            }
            //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
            //List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
            //if (objInputVoucherCenter.UserNotifyData != null && objInputVoucherCenter.UserNotifyData.Rows.Count > 0)
            //{
            //    foreach (DataRow item in objInputVoucherCenter.UserNotifyData.Rows)
            //    {
            //        Notification.BO.Notification_User objNotification_User = new Notification.BO.Notification_User();
            //        objNotification_User.UserName = item["UserName"].ToString();
            //        if (!objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
            //        {
            //            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
            //            {
            //                bool bolResult = false;
            //                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, objNotification_User.UserName);
            //                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
            //                    objNotification_UserList.Add(objNotification_User);
            //            }
            //            else
            //                objNotification_UserList.Add(objNotification_User);
            //        }
            //    }
            //    objNotification.Notification_UserList = objNotification_UserList;
            //}
            //objNotification.NotificationID = Guid.NewGuid().ToString();
            //objNotification.NotificationTypeID = 8;
            //objNotification.ClickTaskParameters = objInputVoucherCenter.InputVoucherID;
            //objNotification.TaskParameters = "{\"ID\": \"" + objInputVoucherCenter.InputVoucherID + "\"}";
            //objNotification.NotificationName = "Thêm mới phiếu nhập";
            //objNotification.Content = "Nội dung: " + objInputVoucherCenter.Content;
            //objNotification.CreatedUser = objInputVoucherCenter.CreatedUser;
            //objNotification.CreatedDate = DateTime.Now;
            //objNotification.StartDate = DateTime.Now;
            //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
            //ResultMessage objResultMessage1 = new ResultMessage();
            //objResultMessage1 = objDA_Notification.Broadcast(objNotification);
            return objResultMessage;
        }

        #region Tạo chi tiết cho phiếu xuất chuyển kho và nhập chuyển kho
        #region Cach 1: chay for binh thuong
        private bool AddStoreChangeDetail(IData objIData, InputVoucher objInputVoucherCenter, List<BO.InputVoucherDetail> InputVoucherDetailList, BO.StoreChange objStoreChangeBO)
        {
            DA_StoreChangeDetail objDA_StoreChangeDetail = new DA_StoreChangeDetail();
            foreach (BO.InputVoucherDetail objRow in InputVoucherDetailList)
            {
                String strProductID = objRow.ProductID.Trim();

                DateTime dtEndWarrantyTime = Convert.ToDateTime(objRow.ENDWarrantyDate);//Convert.ToDateTime(objRow["EndWarrantyDate"]);
                bool bolIsHasWarranty = Convert.ToBoolean(objRow.IsHasWarranty);

                decimal decSalePrice = objRow.InputPrice;
                int intPackingNumber = 1;
                decimal decCostPrice = Convert.ToDecimal(objRow.CostPrice);
                int intVAT = Convert.ToInt32(objRow.VAT);
                int intVATPercent = Convert.ToInt32(objRow.VATPercent);
                DateTime dtInputVoucherDate = Convert.ToDateTime(objInputVoucherCenter.InputDate);
                int intInputCustomerID = Convert.ToInt32(objInputVoucherCenter.CustomerID);
                //String strInputUserID = Convert.ToString(objRow["InputUserID"]).Trim();
                //int intInputTypeID = Convert.ToInt32(objInputVoucherCenter.InputTypeID);
                bool bolIsShowProductInput = Convert.ToBoolean(objRow.IsShowProduct);
                String strNote = "";// Convert.ToString(objRow["Note"]).Trim();
                bool bolIsReturnProduct = Convert.ToBoolean(objRow.IsReturnProduct);
                String strStoreChangeOrderDetailID = Convert.ToString(objRow.StoreChangeOrderDetailID).Trim();
                decimal decOrderQuantity = Convert.ToDecimal(objRow.OrderQuantity);
                String strUserNote = "";//Convert.ToString(objRow["UserNote"]).Trim();
                decimal decOutputPrice = Convert.ToDecimal(objRow.InputPrice);
                decimal decInputPrice = Convert.ToDecimal(objRow.InputPrice);
                int intOutputVAT = Convert.ToInt32(objRow.VAT);
                int intInputVAT = Convert.ToInt32(objRow.VAT);
                decimal decQuantity = 0;
                String strIMEI = string.Empty;
                if (strUserNote.Length > 2000) strUserNote = strUserNote.Substring(0, 2000);
                if (objRow.IsRequestIMEI)
                {
                    decQuantity = 1;

                    foreach (InputVoucherDetailIMEI objInputVoucherDetailIMEI in objRow.InputVoucherDetailIMEIList)
                    {
                        strIMEI = objInputVoucherDetailIMEI.IMEI;
                        if (!AddStoreChangeDetail(objIData, strProductID, strIMEI, decQuantity, dtEndWarrantyTime, bolIsHasWarranty,
                        intPackingNumber, intVAT, dtInputVoucherDate, intInputCustomerID, objStoreChangeBO.InputVoucherBO.InputTypeID, objStoreChangeBO.OutputTypeID, bolIsReturnProduct,
                        bolIsShowProductInput, intVATPercent, strNote, strStoreChangeOrderDetailID,
                        decOrderQuantity, strUserNote, decOutputPrice, decInputPrice, intOutputVAT, intInputVAT, decCostPrice, objStoreChangeBO, objRow, objInputVoucherDetailIMEI))
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    decQuantity = objRow.Quantity;
                    strIMEI = "";
                    if (!AddStoreChangeDetail(objIData, strProductID, strIMEI, decQuantity, dtEndWarrantyTime, bolIsHasWarranty,
                                        intPackingNumber, intVAT, dtInputVoucherDate, intInputCustomerID, objStoreChangeBO.InputVoucherBO.InputTypeID, objStoreChangeBO.OutputTypeID, bolIsReturnProduct,
                                        bolIsShowProductInput, intVATPercent, strNote, strStoreChangeOrderDetailID,
                                        decOrderQuantity, strUserNote, decOutputPrice, decInputPrice, intOutputVAT, intInputVAT, decCostPrice, objStoreChangeBO, objRow))
                    {
                        return false;
                    }
                }


            }
            return true;
        }

        public bool AddStoreChangeDetail(IData objIData, string strProductID, string strIMEI, decimal decQuantity, DateTime dtEndWarrantyDate,
          bool bolIsHasWarranty, int intPackingNumber, int intVAT, DateTime dtInputVoucherDate, int intInputCustomerID,
            int intInputTypeID, int intOutputTypeID, bool bolIsReturnProduct, bool bolIsShowProductInput, int intVATPercent,
          String strNote, String strStoreChangeOrderDetailID, decimal decOrderQuantity,
          String strUserNote, decimal decOutputPrice, decimal decInputPrice, int intOutputVAT, int intInputVAT, decimal decCostPrice, BO.StoreChange objStoreChangeBO, InputVoucherDetail objInputVoucherDetail, InputVoucherDetailIMEI objInputVoucherDetailIMEI = null)
        {

            try
            {

                objIData.CreateNewStoredProcedure("PM_STORECHANGEDETAIL_ADD2");
                objIData.AddParameter("@InputVoucherID", objStoreChangeBO.InputVoucherBO.InputVoucherID);
                objIData.AddParameter("@OutputVoucherID", objStoreChangeBO.OutputVoucherBO.OutputVoucherID);
                objIData.AddParameter("@StoreID", objStoreChangeBO.FromStoreID);
                objIData.AddParameter("@StoreID_In", objStoreChangeBO.ToStoreID);
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@IMEI", strIMEI);
                objIData.AddParameter("@Quantity", decQuantity);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@PackingNumber", intPackingNumber);
                objIData.AddParameter("@VATPercent", intVATPercent);
                objIData.AddParameter("@Note", strNote);
                objIData.AddParameter("@IsCheckRealInput", objStoreChangeBO.InputVoucherBO.IsCheckRealInput);
                objIData.AddParameter("@StoreChangeOrderDetailID", strStoreChangeOrderDetailID);
                objIData.AddParameter("@StoreChangeID", objStoreChangeBO.StoreChangeID);
                objIData.AddParameter("@OrderQuantity", decOrderQuantity);
                objIData.AddParameter("@UserNote", strUserNote);
                objIData.AddParameter("@CreatedStoreID", objStoreChangeBO.CreatedStoreID);

                objIData.AddParameter("@CreatedUser", objStoreChangeBO.CreatedUser);
                objIData.AddParameter("@OutputPrice", decOutputPrice);
                objIData.AddParameter("@OutputVAT", intOutputVAT);
                objIData.AddParameter("@InputPrice", decInputPrice);
                objIData.AddParameter("@CostPrice", decCostPrice);
                objIData.AddParameter("@InputVAT", intInputVAT);
                objIData.AddParameter("@IsHasWarranty", bolIsHasWarranty);
                objIData.AddParameter("@IsNew", objStoreChangeBO.IsNew);
                objIData.AddParameter("@InputVoucherDate", dtInputVoucherDate);
                objIData.AddParameter("@InputCustomerID", intInputCustomerID);
                objIData.AddParameter("@InputTypeID", intInputTypeID);
                objIData.AddParameter("@INPUTVOUCHERDETAILID_OUT", objInputVoucherDetail.InputVoucherDetailID);
                objIData.AddParameter("@ISNEW_OUT", objInputVoucherDetail.IsNew);
                objIData.AddParameter("@ISSHOWPRODUCT_OUT", objInputVoucherDetail.IsShowProduct);
                objIData.AddParameter("@ISRETURNPRODUCT_OUT", objInputVoucherDetail.IsReturnProduct);
                objIData.AddParameter("@RETURNDATE_OUT", objInputVoucherDetail.ReturnDate);
                objIData.AddParameter("@ISPRODUCTCHANGE_OUT", objInputVoucherDetail.ISProductChange);
                objIData.AddParameter("@ISRETURNWITHFEE_OUT", objInputVoucherDetail.IsReturnWithFee);

                if (objInputVoucherDetailIMEI != null)
                {
                    objIData.AddParameter("@IMEICHANGECHAIN_OUT", objInputVoucherDetailIMEI.IMEIChangeCHAIN);
                    objIData.AddParameter("@PINCODE_OUT", objInputVoucherDetailIMEI.PINCode);
                    objIData.AddParameter("@IMEICHANGECHAIN_IN", objInputVoucherDetailIMEI.IMEIChangeCHAIN);
                    objIData.AddParameter("@PINCODE_IN", objInputVoucherDetailIMEI.PINCode);
                }
                else
                {
                    objIData.AddParameter("@IMEICHANGECHAIN_OUT", "");
                    objIData.AddParameter("@PINCODE_OUT", "");
                    objIData.AddParameter("@IMEICHANGECHAIN_IN", "");
                    objIData.AddParameter("@PINCODE_IN", "");
                }


                objIData.AddParameter("@INPUTDATE_IN", objInputVoucherDetail.InputDate);
                objIData.AddParameter("@INPUTPRICE_IN", objInputVoucherDetail.InputPrice);
                objIData.AddParameter("@RETURNFEE_IN", objInputVoucherDetail.ReturnFee);
                objIData.AddParameter("@CHANGETOOLDDATE_IN", objInputVoucherDetail.ChangeToOLDDate);
                objIData.AddParameter("@PRODUCTIONDATE", objInputVoucherDetail.ProductIONDate);

                objIData.AddParameter("@FIRSTINPUTTYPEID", objInputVoucherDetail.FirstInputTypeID);
                objIData.AddParameter("@FIRSTINPUTDATE", objInputVoucherDetail.FirstInputDate);
                objIData.AddParameter("@FIRSTINVOICEDATE", objInputVoucherDetail.FirstInVoiceDate);
                objIData.AddParameter("@FIRSTINPUTVOUCHERDETAILID", objInputVoucherDetail.FirstInputVoucherDetailID);
                objIData.AddParameter("@FIRSTCUSTOMERID", objInputVoucherDetail.FirstCustomerID);
                objIData.AddParameter("@FIRSTPRICE", objInputVoucherDetail.FirstPrice);
                objIData.AddParameter("@FIRSTINPUTVOUCHERID", objInputVoucherDetail.FirstInputVoucherID);
                objIData.AddParameter("@INPUTVOUCHERDETAILID", objInputVoucherDetail.InputVoucherDetailID);

                objIData.AddParameter("@IsShowProduct", bolIsShowProductInput);
                objIData.AddParameter("@IsReturnProduct", bolIsReturnProduct);
                objIData.AddParameter("@EndWarrantyDate", dtEndWarrantyDate);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                throw objEx;
            }
            return true;

        }

        #endregion

        #region Cach 2: xml
        private bool AddStoreChangeDetail1(IData objIData, InputVoucher objInputVoucherCenter, List<BO.InputVoucherDetail> InputVoucherDetailList, BO.StoreChange objStoreChangeBO)
        {
            DA_StoreChangeDetail objDA_StoreChangeDetail = new DA_StoreChangeDetail();
            DataTable dtbStoreChage = CreateStoreChangeDetail();
            foreach (BO.InputVoucherDetail objRow in InputVoucherDetailList)
            {
                String strProductID = objRow.ProductID.Trim();

                DateTime dtEndWarrantyTime = Convert.ToDateTime(objRow.ENDWarrantyDate);//Convert.ToDateTime(objRow["EndWarrantyDate"]);
                bool bolIsHasWarranty = Convert.ToBoolean(objRow.IsHasWarranty);

                decimal decSalePrice = objRow.InputPrice;
                int intPackingNumber = 1;
                decimal decCostPrice = Convert.ToDecimal(objRow.CostPrice);
                int intVAT = Convert.ToInt32(objRow.VAT);
                int intVATPercent = Convert.ToInt32(objRow.VATPercent);
                DateTime dtInputVoucherDate = Convert.ToDateTime(objInputVoucherCenter.InputDate);
                int intInputCustomerID = Convert.ToInt32(objInputVoucherCenter.CustomerID);
                //String strInputUserID = Convert.ToString(objRow["InputUserID"]).Trim();
                //int intInputTypeID = Convert.ToInt32(objInputVoucherCenter.InputTypeID);
                bool bolIsShowProductInput = Convert.ToBoolean(objRow.IsShowProduct);
                String strNote = "";// Convert.ToString(objRow["Note"]).Trim();
                bool bolIsReturnProduct = Convert.ToBoolean(objRow.IsReturnProduct);
                String strStoreChangeOrderDetailID = Convert.ToString(objRow.StoreChangeOrderDetailID).Trim();
                decimal decOrderQuantity = Convert.ToDecimal(objRow.OrderQuantity);
                String strUserNote = "";//Convert.ToString(objRow["UserNote"]).Trim();
                decimal decOutputPrice = Convert.ToDecimal(objRow.InputPrice);
                decimal decInputPrice = Convert.ToDecimal(objRow.InputPrice);
                int intOutputVAT = Convert.ToInt32(objRow.VAT);
                int intInputVAT = Convert.ToInt32(objRow.VAT);
                decimal decQuantity = 0;
                String strIMEI = string.Empty;
                if (strUserNote.Length > 2000) strUserNote = strUserNote.Substring(0, 2000);
                if (objRow.IsRequestIMEI)
                {
                    decQuantity = 1;

                    foreach (InputVoucherDetailIMEI objInputVoucherDetailIMEI in objRow.InputVoucherDetailIMEIList)
                    {
                        strIMEI = objInputVoucherDetailIMEI.IMEI;
                        if (!InsertDataStoreChange(dtbStoreChage, strProductID, strIMEI, decQuantity, dtEndWarrantyTime, bolIsHasWarranty,
                        intPackingNumber, intVAT, dtInputVoucherDate, intInputCustomerID, objStoreChangeBO.InputVoucherBO.InputTypeID, objStoreChangeBO.OutputTypeID, bolIsReturnProduct,
                        bolIsShowProductInput, intVATPercent, strNote, strStoreChangeOrderDetailID,
                        decOrderQuantity, strUserNote, decOutputPrice, decInputPrice, intOutputVAT, intInputVAT, decCostPrice, objStoreChangeBO, objRow,
                        objInputVoucherDetailIMEI.IMEIChangeCHAIN, objInputVoucherDetailIMEI.PINCode, objInputVoucherDetailIMEI.IMEIChangeCHAIN, objInputVoucherDetailIMEI.PINCode))
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    decQuantity = objRow.Quantity;
                    strIMEI = "";
                    if (!InsertDataStoreChange(dtbStoreChage, strProductID, strIMEI, decQuantity, dtEndWarrantyTime, bolIsHasWarranty,
                                        intPackingNumber, intVAT, dtInputVoucherDate, intInputCustomerID, objStoreChangeBO.InputVoucherBO.InputTypeID, objStoreChangeBO.OutputTypeID, bolIsReturnProduct,
                                        bolIsShowProductInput, intVATPercent, strNote, strStoreChangeOrderDetailID,
                                        decOrderQuantity, strUserNote, decOutputPrice, decInputPrice, intOutputVAT, intInputVAT, decCostPrice, objStoreChangeBO, objRow
                                        , "", "", "", ""))
                    {
                        return false;
                    }
                }


            }
            if (dtbStoreChage != null && dtbStoreChage.Rows.Count > 0)
            {
                //int iCount = 0;
                //List<string> lstExitsPinCode = new List<string>();
                //while (iCount - 1000 < dtbStoreChage.Rows.Count)
                //{
                //    var lstData = dtbStoreChage.AsEnumerable().Skip(iCount).Take(1000);
                //if(lstData!=null && lstData.Count()>0)
                //{
                string strXml = CreateXMLFromDataTable(dtbStoreChage, "STORECHANGEDETAIL");
                if (!AddStoreChangeDetailxml(objIData, strXml, objStoreChangeBO.InputVoucherBO.InputVoucherID, objStoreChangeBO.StoreChangeID))
                {
                    return false;
                }
                //}
                //iCount += 1000;
                // }
            }

            return true;
        }

        public bool AddStoreChangeDetailxml(IData objIData, string xml, string strINPUTVOUCHERID, string strSTORECHANGEID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_STORECHANGEDETAIL_ADD3");
                objIData.AddParameter("@xml", xml, Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.AddParameter("@INPUTVOUCHERID", strINPUTVOUCHERID);
                objIData.AddParameter("@STORECHANGEID", strSTORECHANGEID);
                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog.Add(objIData, ex.Message, ex.ToString(), "DA_InputVoucher -> InsertData", InventoryGlobals.ModuleName);
                return false;
            }
            return true;

        }

        private DataTable CreateStoreChangeDetail()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("InputVoucherID", typeof(string));
            dt.Columns.Add("OutputVoucherID", typeof(string));
            dt.Columns.Add("StoreID", typeof(int));
            dt.Columns.Add("StoreID_In", typeof(int));
            dt.Columns.Add("ProductID", typeof(string));
            dt.Columns.Add("IMEI", typeof(string));
            dt.Columns.Add("Quantity", typeof(decimal));
            dt.Columns.Add("OutputTypeID", typeof(int));
            dt.Columns.Add("PackingNumber", typeof(int));
            dt.Columns.Add("VATPercent", typeof(int));
            dt.Columns.Add("Note", typeof(string));
            dt.Columns.Add("IsCheckRealInput", typeof(int));
            dt.Columns.Add("StoreChangeOrderDetailID", typeof(string));
            dt.Columns.Add("StoreChangeID", typeof(string));
            dt.Columns.Add("OrderQuantity", typeof(decimal));
            dt.Columns.Add("UserNote", typeof(string));
            dt.Columns.Add("CreatedStoreID", typeof(int));
            dt.Columns.Add("CreatedUser", typeof(string));
            dt.Columns.Add("OutputPrice", typeof(decimal));
            dt.Columns.Add("OutputVAT", typeof(int));
            dt.Columns.Add("InputPrice", typeof(decimal));
            dt.Columns.Add("CostPrice", typeof(decimal));
            dt.Columns.Add("InputVAT", typeof(int));
            dt.Columns.Add("IsHasWarranty", typeof(int));
            dt.Columns.Add("IsNew", typeof(int));
            dt.Columns.Add("InputVoucherDate", typeof(string));
            dt.Columns.Add("InputCustomerID", typeof(int));
            dt.Columns.Add("InputTypeID", typeof(int));
            dt.Columns.Add("INPUTVOUCHERDETAILID_OUT", typeof(string));
            dt.Columns.Add("ISNEW_OUT", typeof(int));
            dt.Columns.Add("ISSHOWPRODUCT_OUT", typeof(int));
            dt.Columns.Add("ISRETURNPRODUCT_OUT", typeof(int));
            dt.Columns.Add("RETURNDATE_OUT", typeof(string));
            dt.Columns.Add("ISPRODUCTCHANGE_OUT", typeof(int));
            dt.Columns.Add("ISRETURNWITHFEE_OUT", typeof(int));
            dt.Columns.Add("IMEICHANGECHAIN_OUT", typeof(string));
            dt.Columns.Add("PINCODE_OUT", typeof(string));
            dt.Columns.Add("IMEICHANGECHAIN_IN", typeof(string));
            dt.Columns.Add("PINCODE_IN", typeof(string));
            dt.Columns.Add("INPUTDATE_IN", typeof(string));
            dt.Columns.Add("INPUTPRICE_IN", typeof(decimal));
            dt.Columns.Add("RETURNFEE_IN", typeof(string));
            dt.Columns.Add("CHANGETOOLDDATE_IN", typeof(string));
            dt.Columns.Add("PRODUCTIONDATE", typeof(string));
            dt.Columns.Add("FIRSTINPUTTYPEID", typeof(int));
            dt.Columns.Add("FIRSTINPUTDATE", typeof(string));
            dt.Columns.Add("FIRSTINVOICEDATE", typeof(string));
            dt.Columns.Add("FIRSTINPUTVOUCHERDETAILID", typeof(string));
            dt.Columns.Add("FIRSTCUSTOMERID", typeof(int));
            dt.Columns.Add("FIRSTPRICE", typeof(decimal));
            dt.Columns.Add("FIRSTINPUTVOUCHERID", typeof(string));
            dt.Columns.Add("INPUTVOUCHERDETAILID", typeof(string));
            dt.Columns.Add("IsShowProduct", typeof(int));
            dt.Columns.Add("IsReturnProduct", typeof(int));
            dt.Columns.Add("EndWarrantyDate", typeof(string));
            return dt;
        }

        public bool InsertDataStoreChange(DataTable dt, string strProductID, string strIMEI, decimal decQuantity, DateTime dtEndWarrantyDate,
          bool bolIsHasWarranty, int intPackingNumber, int intVAT, DateTime dtInputVoucherDate, int intInputCustomerID,
            int intInputTypeID, int intOutputTypeID, bool bolIsReturnProduct, bool bolIsShowProductInput, int intVATPercent,
          String strNote, String strStoreChangeOrderDetailID, decimal decOrderQuantity,
          String strUserNote, decimal decOutputPrice, decimal decInputPrice, int intOutputVAT, int intInputVAT, decimal decCostPrice, BO.StoreChange objStoreChangeBO,
            InputVoucherDetail objInputVoucherDetail, string strIMEICHANGECHAIN_OUT, string strPINCODE_OUT, string strIMEICHANGECHAIN_IN, string strPINCODE_IN)
        {
            try
            {
                dt.Rows.Add(
                    objStoreChangeBO.InputVoucherBO.InputVoucherID
                    , objStoreChangeBO.OutputVoucherBO.OutputVoucherID
                    , objStoreChangeBO.FromStoreID
                    , objStoreChangeBO.ToStoreID
                    , strProductID
                    , strIMEI
                    , decQuantity
                    , intOutputTypeID
                    , intPackingNumber
                    , intVATPercent
                    , strNote
                    , objStoreChangeBO.InputVoucherBO.IsCheckRealInput ? 1 : 0
                    , strStoreChangeOrderDetailID
                    , objStoreChangeBO.StoreChangeID
                    , decOrderQuantity
                    , strUserNote
                    , objStoreChangeBO.CreatedStoreID
                    , objStoreChangeBO.CreatedUser
                    , decOutputPrice
                    , intOutputVAT
                    , decInputPrice
                    , decCostPrice
                    , intInputVAT
                    , bolIsHasWarranty ? 1 : 0
                    , objStoreChangeBO.IsNew ? 1 : 0
                    , dtInputVoucherDate == null ? "" : dtInputVoucherDate.ToString("yyyy/MM/dd HH:mm:ss")
                    , intInputCustomerID
                    , intInputTypeID
                    , objInputVoucherDetail.InputVoucherDetailID
                    , objInputVoucherDetail.IsNew ? 1 : 0
                    , objInputVoucherDetail.IsShowProduct ? 1 : 0
                    , objInputVoucherDetail.IsReturnProduct ? 1 : 0
                    , objInputVoucherDetail.ReturnDate == null ? "" : objInputVoucherDetail.ReturnDate.Value.ToString("yyyy/MM/dd HH:mm:ss")
                    , objInputVoucherDetail.ISProductChange ? 1 : 0
                    , objInputVoucherDetail.IsReturnWithFee ? 1 : 0
                    , strIMEICHANGECHAIN_OUT
                    , strPINCODE_OUT
                    , strIMEICHANGECHAIN_IN
                    , strPINCODE_IN
                    , objInputVoucherDetail.InputDate == null ? "" : objInputVoucherDetail.InputDate.Value.ToString("yyyy/MM/dd HH:mm:ss")
                    , objInputVoucherDetail.InputPrice
                    , objInputVoucherDetail.ReturnFee
                    , objInputVoucherDetail.ChangeToOLDDate == null ? "" : objInputVoucherDetail.ChangeToOLDDate.Value.ToString("yyyy/MM/dd HH:mm:ss")
                    , objInputVoucherDetail.ProductIONDate == null ? "" : objInputVoucherDetail.ProductIONDate.Value.ToString("yyyy/MM/dd HH:mm:ss")
                    , objInputVoucherDetail.FirstInputTypeID
                    , objInputVoucherDetail.FirstInputDate == null ? "" : objInputVoucherDetail.FirstInputDate.Value.ToString("yyyy/MM/dd HH:mm:ss")
                    , objInputVoucherDetail.FirstInVoiceDate == null ? "" : objInputVoucherDetail.FirstInVoiceDate.Value.ToString("yyyy/MM/dd HH:mm:ss")
                    , objInputVoucherDetail.FirstInputVoucherDetailID
                    , objInputVoucherDetail.FirstCustomerID
                    , objInputVoucherDetail.FirstPrice
                    , objInputVoucherDetail.FirstInputVoucherID
                    , objInputVoucherDetail.InputVoucherDetailID
                    , bolIsShowProductInput ? 1 : 0
                    , bolIsReturnProduct ? 1 : 0
                    , dtEndWarrantyDate == null ? "" : dtEndWarrantyDate.ToString("yyyy/MM/dd HH:mm:ss")
                    );
            }
            catch (Exception objEx)
            {
                throw objEx;
            }
            return true;
        }

        private string CreateXMLFromDataTable(DataTable dtb, string tableName, string formatDate = "yyyy/MM/dd HH:mm:ss")
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(tableName))
            {
                return string.Empty;
            }
            var sb = new System.Text.StringBuilder();
            var xtx = System.Xml.XmlWriter.Create(sb);
            xtx.WriteStartDocument();
            xtx.WriteStartElement("", tableName.ToUpper() + "LIST", "");
            foreach (DataRow dr in dtb.Rows)
            {
                xtx.WriteStartElement("", tableName.ToUpper(), "");
                foreach (DataColumn dc in dtb.Columns)
                {
                    if (dc.DataType == typeof(DateTime))
                    {
                        if (!Convert.IsDBNull(dr[dc.ColumnName]))
                            xtx.WriteAttributeString(dc.ColumnName.ToUpper(), Convert.ToDateTime(dr[dc.ColumnName]).ToString(formatDate));
                    }
                    else if (dc.DataType == typeof(DateTime?))
                    {
                        if (Convert.IsDBNull(dr[dc.ColumnName]))
                        {
                            xtx.WriteAttributeString(dc.ColumnName.ToUpper(), DateTime.Now.ToString(formatDate));
                        }
                        else
                        {
                            xtx.WriteAttributeString(dc.ColumnName.ToUpper(), Convert.ToDateTime(dr[dc.ColumnName]).ToString(formatDate));
                        }
                    }
                    else if (dc.DataType == typeof(Boolean))
                    {
                        xtx.WriteAttributeString(dc.ColumnName.ToUpper(), dr[dc.ColumnName].ToString().ToUpper() == "TRUE" ? "1" : "0");
                    }
                    else
                    {
                        xtx.WriteAttributeString(dc.ColumnName.ToUpper(), dr[dc.ColumnName].ToString());
                    }
                }
                xtx.WriteEndElement();
            }
            xtx.WriteEndElement();
            xtx.Flush();
            xtx.Close();
            return sb.ToString();
        }
        #endregion
        #endregion

        private void InsertDetail(IData objIData, StoreChangeOrder objStoreChangeOrder, List<BO.InputVoucherDetail> InputVoucherDetailList)
        {
            try
            {
                new DA_StoreChangeOrderDetail().Delete(objIData, objStoreChangeOrder);
                foreach (BO.InputVoucherDetail row in InputVoucherDetailList)
                {
                    BO.StoreChangeOrderDetail objStoreChangeOrderDetail = new BO.StoreChangeOrderDetail();
                    objStoreChangeOrderDetail.CreatedStoreID = objStoreChangeOrder.CreatedStoreID;
                    objStoreChangeOrderDetail.CreatedUser = objStoreChangeOrder.CreatedUser;
                    objStoreChangeOrderDetail.Note = "";
                    objStoreChangeOrderDetail.OrderDate = objStoreChangeOrder.OrderDate;
                    objStoreChangeOrderDetail.ProductID = row.ProductID;
                    objStoreChangeOrderDetail.Quantity = row.Quantity;
                    objStoreChangeOrderDetail.StoreChangeQuantity = row.Quantity;
                    objStoreChangeOrderDetail.StoreChangeOrderID = objStoreChangeOrder.StoreChangeOrderID;
                    objStoreChangeOrderDetail.FromStoreID = objStoreChangeOrder.FromStoreID;
                    objStoreChangeOrderDetail.ToStoreID = objStoreChangeOrder.ToStoreID;
                    objStoreChangeOrderDetail.StoreChangeOrderDetailID = new DA_StoreChangeOrderDetail().Insert(objIData, objStoreChangeOrderDetail);
                    row.StoreChangeOrderDetailID = objStoreChangeOrderDetail.StoreChangeOrderDetailID;
                    Insert_ORDERDT_SCODT(objIData, row.OrderDetailID, row.InputVoucherDetailID, row.StoreChangeOrderDetailID);
                    //Delete Imei
                    //DeleteIMEI(objIData, objStoreChangeOrderDetail); -- Xóa trong store (Thiên bỏ 07/09/2016)
                    //Insert Imei
                    //if (row.StoreChangeOrderDetailIMEILIST != null)
                    //{
                    //    foreach (BO.StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEI in row.StoreChangeOrderDetailIMEILIST)
                    //    {
                    //        InsertImei(objIData, objStoreChangeOrderDetail, objStoreChangeOrderDetailIMEI.IMEI);
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw ex;
            }
        }

        private void InsertImei(IData objIData, ERP.Inventory.BO.StoreChangeOrderDetail objStoreChangeOrderDetail, string strImei)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_SCODETAILIMEI_ADD");
                objIData.AddParameter("@STORECHANGEORDERDETAILID", objStoreChangeOrderDetail.StoreChangeOrderDetailID);
                objIData.AddParameter("@IMEI", strImei);
                objIData.AddParameter("@ORDERDATE", objStoreChangeOrderDetail.OrderDate);
                objIData.AddParameter("@ISSTORECHANGE", 0);
                objIData.AddParameter("@NOTE", string.Empty);
                objIData.AddParameter("@CREATEDSTOREID", objStoreChangeOrderDetail.CreatedStoreID);
                objIData.AddParameter("@FROMSTORE", objStoreChangeOrderDetail.FromStoreID);
                objIData.AddParameter("@CREATEDUSER", objStoreChangeOrderDetail.CreatedUser);
                objIData.AddParameter("@STORECHANGEORDERID", objStoreChangeOrderDetail.StoreChangeOrderID);
                objIData.AddParameter("@PRODUCTID", objStoreChangeOrderDetail.ProductID);

                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw (ex);
            }
        }

        public bool CreateOuputInputVoucher(ref ResultMessage objResultMessage, IData objIData, ref BO.StoreChange objStoreChangeBO, InputVoucher objInputVoucher, List<BO.InputVoucherDetail> InputVoucherDetailList, ref string strInputvoucherID)
        {
            objStoreChangeBO.OutputVoucherBO.OutputDate = objInputVoucher.InputDate = objInputVoucher.InputDate.Value.AddSeconds(5);

            #region phiếu nhập kho
            objInputVoucher.ISStoreChange = true;//La xuat chuyen kho
            Insert(objIData, objInputVoucher);
            strInputvoucherID = objInputVoucher.InputVoucherID;
            //objStoreChangeBO.InputVoucherBO.InputVoucherID = strInputvoucherID;           

            //Thêm danh sách file đính kèm
            if (objInputVoucher.InputVoucher_AttachmnetList != null && objInputVoucher.InputVoucher_AttachmnetList.Count > 0)
            {
                DA_InputVoucher_Attachment objDA_InputVoucher_Attachment = new DA_InputVoucher_Attachment();
                foreach (BO.InputVoucher_Attachment item in objInputVoucher.InputVoucher_AttachmnetList)
                {
                    if (item.FileID == string.Empty || item.FilePath == string.Empty)
                        continue;
                    item.InputVoucherID = objInputVoucher.InputVoucherID;
                    objDA_InputVoucher_Attachment.Insert(objIData, item);
                }
            }
            //Tạo phiếu chi
            if (objInputVoucher.IsCreateOutVoucher)
            {
                if (objInputVoucher.Voucher != null)
                {
                    DA_Voucher objDA_Voucher = new DA_Voucher();
                    objInputVoucher.Voucher.VoucherConcern = objInputVoucher.InputVoucherID;
                    objDA_Voucher.CreateVoucher(objIData, objInputVoucher.Voucher);
                }
            }
            //if (objInputVoucher.IsInputFromOrder)
            //{
            //    // Cập nhật trạng thái nhập hàng của đơn hàng 0: Chưa nhập hàng; 1: Nhập chưa hết; 2: Đã nhập hết
            //    UpdateOrder_InputStatus(objIData, objInputVoucher.OrderID);
            //    // Tạo nhập xuất nội bộ 
            //    //if (objInputVoucher.IntermediateInputStoreID > 0)
            //    //{
            //    //    if (!AutoStoreChange(objIData, objInputVoucherBO.InputVoucherID, objInputVoucherBO.IntermediateInputStoreID))
            //    //    {
            //    //        objIData.RollBackTransaction();
            //    //        return false;
            //    //    }
            //    //}
            //}
            #endregion
            try
            {
                //string strInputVoucherID = new DA_StoreChange().CreateInputVoucherID(ref objResultMessage, objIData, objStoreChangeBO.CreatedStoreID);
                if (objStoreChangeBO.Content.Length > 400)
                    objStoreChangeBO.OutputVoucherBO.OutputContent = "Chuyển đến " + objStoreChangeBO.ToStoreName + " - MPN: " + strInputvoucherID.Trim() + " - Nội dung: " + objStoreChangeBO.Content.Substring(0, 400);
                else
                    objStoreChangeBO.OutputVoucherBO.OutputContent = "Chuyển đến " + objStoreChangeBO.ToStoreName + " - MPN: " + strInputvoucherID.Trim() + " - Nội dung: " + objStoreChangeBO.Content;
                DA_OutputVoucher objOutputVoucherDAO = new DA_OutputVoucher();
                objOutputVoucherDAO.Insert(objIData, objStoreChangeBO.OutputVoucherBO);
            }
            catch (Exception objEx)
            {

                string strMessage = "Lỗi khi thêm mới một hóa đơn xuất hàng";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateOuputInputVoucher", "PM_OUTPUTVOUCHER_ADD");
                return false;
            }
            // Tính chi phí van chuyển của phiếu chuyển kho này
            try
            {
                objIData.CreateNewStoredProcedure("ERP.MD_SHIPPINGCOST_SEL");
                objIData.AddParameter("@TransportTypeID", objStoreChangeBO.TransportTypeID);
                objIData.AddParameter("@FromStoreID", objStoreChangeBO.FromStoreID);
                objIData.AddParameter("@ToStoreID", objStoreChangeBO.ToStoreID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objStoreChangeBO.TotalShippingCost = (objStoreChangeBO.TotalWeight * Convert.ToDecimal(reader["SHIPPINGCOST"]));
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi khi lấy chi phí vận chuyển giữa 2 kho";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateOuputInputVoucher", "PM_SHIPPINGCOST_SEL");
                return false;
            }

            try
            {
                objStoreChangeBO.StoreChangeID = new DA_StoreChange().CreateStoreChangeID(ref objResultMessage, objIData, objStoreChangeBO.CreatedStoreID);
                objIData.CreateNewStoredProcedure("PM_STORECHANGE_Add");
                objIData.AddParameter("@STORECHANGEID", objStoreChangeBO.StoreChangeID);
                objIData.AddParameter("@STORECHANGEORDERID", objStoreChangeBO.StoreChangeOrderID);
                objIData.AddParameter("@FROMSTOREID", objStoreChangeBO.FromStoreID);
                objIData.AddParameter("@TOSTOREID", objStoreChangeBO.ToStoreID);
                objIData.AddParameter("@TRANSPORTTYPEID", objStoreChangeBO.TransportTypeID);
                objIData.AddParameter("@STORECHANGETYPEID", objStoreChangeBO.StoreChangeTypeID);
                objIData.AddParameter("@CONTENT", objStoreChangeBO.Content);
                objIData.AddParameter("@USERCREATE", objStoreChangeBO.CreatedUser);
                objIData.AddParameter("@STORECHANGEDATE", objStoreChangeBO.StoreChangeDate);
                objIData.AddParameter("@OUTPUTVOUCHERID", objStoreChangeBO.OutputVoucherBO.OutputVoucherID);
                objIData.AddParameter("@INPUTVOUCHERID", strInputvoucherID);
                objIData.AddParameter("@STORECHANGEUSER", objStoreChangeBO.StoreChangeUser);
                objIData.AddParameter("@CreatedStoreID", objStoreChangeBO.CreatedStoreID);
                objIData.AddParameter("@IsNew", objStoreChangeBO.IsNew);
                objIData.AddParameter("@InvoiceID", objStoreChangeBO.InvoiceID);
                objIData.AddParameter("@TotalQuantity", objStoreChangeBO.TotalQuantity);
                objIData.AddParameter("@TotalPacking", objStoreChangeBO.TotalPacking);
                if (objStoreChangeBO.ToUser1 != "-1") objIData.AddParameter("@ToUser1", objStoreChangeBO.ToUser1);
                if (objStoreChangeBO.ToUser2 != "-1") objIData.AddParameter("@ToUser2", objStoreChangeBO.ToUser2);
                objIData.AddParameter("@ISRECEIVE", objStoreChangeBO.IsReceive);
                if (objStoreChangeBO.InVoucherBO != null)
                    objIData.AddParameter("@InVoucherID", objStoreChangeBO.InVoucherBO.VoucherID);
                else
                    objIData.AddParameter("@InVoucherID", string.Empty);
                if (objStoreChangeBO.OutVoucherBO != null)
                    objIData.AddParameter("@OutVoucherID", objStoreChangeBO.OutVoucherBO.VoucherID);
                else
                    objIData.AddParameter("@OutVoucherID", string.Empty);
                objIData.AddParameter("@TotalWeight", objStoreChangeBO.TotalWeight);
                objIData.AddParameter("@TotalSize", objStoreChangeBO.TotalSize);
                objIData.AddParameter("@IsUrgent", objStoreChangeBO.IsUrgent);
                objIData.AddParameter("@TotalShippingCost", objStoreChangeBO.TotalShippingCost);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@TransportVoucherID", objStoreChangeBO.TransportVoucherID);
                objIData.AddParameter("@InvoiceSymbol", objStoreChangeBO.InvoiceSymbol);
                objIData.AddParameter("@IsCheckRealInput", 1);
                objIData.AddParameter("@INSTOCKSTATUSID", objStoreChangeBO.InStockStatusID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi thêm thông tin chuyển kho";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateOuputInputVoucher", "PM_STORECHANGE_Add");
                return false;
            }

            #region thêm chi tiết phiếu nhập
            try
            {
                DA_InputVoucherDetail objDA_InputVoucherDetail = new DA_InputVoucherDetail();
                string objInputVoucherCenterDetailIMEIList = string.Empty;
                foreach (BO.InputVoucherDetail item in InputVoucherDetailList)
                {
                    item.InputStoreID = objInputVoucher.InputStoreID;
                    Insert(objIData, item, strInputvoucherID, objStoreChangeBO.OutputVoucherBO.OutputVoucherID, objStoreChangeBO.StoreChangeID, objStoreChangeBO.OutputTypeID, objStoreChangeBO.FromStoreID);
                }
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi thêm thông tin các phiếu chi tiết";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> PM_INPUTVOUCHERDETAIL_ADD1", "PM_INPUTVOUCHERDETAIL_ADD1");
                return false;
            }

            #endregion
            return true;
        }

        public ResultMessage CreateInputVoucherTask(string strInputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_TASK");
                objIData.AddParameter("@InputVoucherID", strInputVoucherID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi xử lý tác vụ";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_InputVoucher -> CreateInputVoucherTask", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin chi tiết phiếu nhập
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, InputVoucherDetail objInputVoucherDetail, string strInputVoucherID, string strOUTPUTVOUCHERID, string strSTORECHANGEID,
            int intOutputTypeID, int intCENTERSTOREID, string strInputChangeOrderDetailID = "")
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHERDETAIL_ADD1");
                objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherID, strInputVoucherID);
                objIData.AddParameter("@" + InputVoucherDetail.colProductID, objInputVoucherDetail.ProductID);
                objIData.AddParameter("@" + InputVoucherDetail.colQuantity, objInputVoucherDetail.Quantity);
                objIData.AddParameter("@" + InputVoucherDetail.colVAT, objInputVoucherDetail.VAT);
                objIData.AddParameter("@" + InputVoucherDetail.colVATPercent, objInputVoucherDetail.VATPercent);
                objIData.AddParameter("@" + InputVoucherDetail.colInputPrice, objInputVoucherDetail.InputPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colORIGINALInputPrice, objInputVoucherDetail.ORIGINALInputPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colCostPrice, objInputVoucherDetail.CostPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstPrice, objInputVoucherDetail.FirstPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colReturnFee, objInputVoucherDetail.ReturnFee);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPrice, objInputVoucherDetail.AdjustPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceContent, objInputVoucherDetail.AdjustPriceContent);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceUser, objInputVoucherDetail.AdjustPriceUser);
                objIData.AddParameter("@" + InputVoucherDetail.colInputDate, objInputVoucherDetail.InputDate);
                objIData.AddParameter("@" + InputVoucherDetail.colENDWarrantyDate, objInputVoucherDetail.ENDWarrantyDate);
                objIData.AddParameter("@" + InputVoucherDetail.colProductIONDate, objInputVoucherDetail.ProductIONDate);
                objIData.AddParameter("@" + InputVoucherDetail.colProductChangeDate, objInputVoucherDetail.ProductChangeDate);
                objIData.AddParameter("@" + InputVoucherDetail.colReturnDate, objInputVoucherDetail.ReturnDate);
                objIData.AddParameter("@" + InputVoucherDetail.colChangeToOLDDate, objInputVoucherDetail.ChangeToOLDDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputDate, objInputVoucherDetail.FirstInputDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInVoiceDate, objInputVoucherDetail.FirstInVoiceDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstCustomerID, objInputVoucherDetail.FirstCustomerID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputTypeID, objInputVoucherDetail.FirstInputTypeID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherID, objInputVoucherDetail.FirstInputVoucherID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherDetailID, objInputVoucherDetail.FirstInputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colIsNew, objInputVoucherDetail.IsNew);
                objIData.AddParameter("@" + InputVoucherDetail.colIsShowProduct, objInputVoucherDetail.IsShowProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colIsReturnProduct, objInputVoucherDetail.IsReturnProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colIsReturnWithFee, objInputVoucherDetail.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucherDetail.colIsCheckRealInput, objInputVoucherDetail.IsCheckRealInput);
                objIData.AddParameter("@" + InputVoucherDetail.colISProductChange, objInputVoucherDetail.ISProductChange);
                objIData.AddParameter("@" + InputVoucherDetail.colISAddInStock, true);
                objIData.AddParameter("@" + InputVoucherDetail.colCreatedStoreID, objInputVoucherDetail.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucherDetail.colInputStoreID, objInputVoucherDetail.InputStoreID);
                objIData.AddParameter("@" + InputVoucherDetail.colCreatedUser, objInputVoucherDetail.CreatedUser);
                objIData.AddParameter("@" + InputVoucherDetail.colOrderDetailID, objInputVoucherDetail.OrderDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colIsErrorProduct, objInputVoucherDetail.IsErrorProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colTaxedFee, objInputVoucherDetail.TaxedFee);
                objIData.AddParameter("@" + InputVoucherDetail.colPurchaseFee, objInputVoucherDetail.PurchaseFee);
                objIData.AddParameter("@InputVoucherDetailCenterID", objInputVoucherDetail.InputVoucherDetailID);
                objIData.AddParameter("@OUTPUTVOUCHERID", strOUTPUTVOUCHERID);
                objIData.AddParameter("@STORECHANGEID", strSTORECHANGEID);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@CENTERSTOREID", intCENTERSTOREID);
                objIData.AddParameter("@StoreChangeOrderDetailID", objInputVoucherDetail.StoreChangeOrderDetailID);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@InputChangeOrderDetailID", strInputChangeOrderDetailID);
                objInputVoucherDetail.InputVoucherDetailID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public void AddINPUTVOUCHERDETAILxml(IData objIData, string xml, string strInputvoucherID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHERDTIMEI_ADDTMP2");
                objIData.AddParameter("@xml", xml, Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.AddParameter("@InputvoucherID", strInputvoucherID);
                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw (ex);
            }
        }
        #endregion

        /// <summary>
        /// Cập nhật số lượng nhập trên chi tiết phiếu nhập
        /// </summary>
        private void UpdateInvoiceQuantity(IData objIData, string strOrderID, string strInputVoucherDetailID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("VAT_PINVOICE_UPDIQUANTITY");
                objIData.AddParameter(
                    "@ORDERID", strOrderID,
                    "@INPUTVOUCHERDETAILID", strInputVoucherDetailID
                    );
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw objEx;
            }
        }


        /// <summary>
        /// Kiểm tra phiếu nhập đã có hóa đơn hay chưa
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public ResultMessage CheckIsHasVatInvoice(string strInputVoucherID, ref bool bolIsHasVATInvoice)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                string strOut = string.Empty;
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_CHECKISHASVAT");
                objIData.AddParameter("@INPUTVOUCHERID", strInputVoucherID);
                strOut = objIData.ExecStoreToString();
                if (!string.IsNullOrEmpty(strOut))
                    bolIsHasVATInvoice = Convert.ToBoolean(Convert.ToInt32(strOut));
                else
                    bolIsHasVATInvoice = false;
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.CheckData, "Lỗi kiểm tra thông tin phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> CheckIsHasVatInvoice", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private void Insert_ORDERDT_SCODT(IData objIData, string strOrderDetailID, string strInputVoucherDetailID, string strStoreChangeOrderDetailID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("POM_ORDERDT_SCODT_ADD");
                objIData.AddParameter(
                    "@ORDERDETAILID", strOrderDetailID,
                    "@INPUTVOUCHERDETAILID", strInputVoucherDetailID,
                    "@STORECHANGEORDERDETAILID", strStoreChangeOrderDetailID
                    );
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw objEx;
            }
        }
        #endregion
    }
}
