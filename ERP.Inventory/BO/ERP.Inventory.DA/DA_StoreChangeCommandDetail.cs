
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using StoreChangeCommandDetail = ERP.Inventory.BO.StoreChangeCommandDetail;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 11/22/2012 
	/// 
	/// </summary>	
	public partial class DA_StoreChangeCommandDetail
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objStoreChangeCommandDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref StoreChangeCommandDetail objStoreChangeCommandDetail, string strStoreChangeCommandDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colStoreChangeCommandDetailID, strStoreChangeCommandDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objStoreChangeCommandDetail = new StoreChangeCommandDetail();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colStoreChangeCommandDetailID])) objStoreChangeCommandDetail.StoreChangeCommandDetailID = Convert.ToString(reader[StoreChangeCommandDetail.colStoreChangeCommandDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colStoreChangeCommandID])) objStoreChangeCommandDetail.StoreChangeCommandID = Convert.ToString(reader[StoreChangeCommandDetail.colStoreChangeCommandID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colProductID])) objStoreChangeCommandDetail.ProductID = Convert.ToString(reader[StoreChangeCommandDetail.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colQuantity])) objStoreChangeCommandDetail.Quantity = Convert.ToDecimal(reader[StoreChangeCommandDetail.colQuantity]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colStoreChangeQuantity])) objStoreChangeCommandDetail.StoreChangeQuantity = Convert.ToDecimal(reader[StoreChangeCommandDetail.colStoreChangeQuantity]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colCreatedStoreID])) objStoreChangeCommandDetail.CreatedStoreID = Convert.ToInt32(reader[StoreChangeCommandDetail.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colCreatedUser])) objStoreChangeCommandDetail.CreatedUser = Convert.ToString(reader[StoreChangeCommandDetail.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colCreatedDate])) objStoreChangeCommandDetail.CreatedDate = Convert.ToDateTime(reader[StoreChangeCommandDetail.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colUpdatedUser])) objStoreChangeCommandDetail.UpdatedUser = Convert.ToString(reader[StoreChangeCommandDetail.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colUpdatedDate])) objStoreChangeCommandDetail.UpdatedDate = Convert.ToDateTime(reader[StoreChangeCommandDetail.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colIsDeleted])) objStoreChangeCommandDetail.IsDeleted = Convert.ToBoolean(reader[StoreChangeCommandDetail.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colDeletedUser])) objStoreChangeCommandDetail.DeletedUser = Convert.ToString(reader[StoreChangeCommandDetail.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDetail.colDeletedDate])) objStoreChangeCommandDetail.DeletedDate = Convert.ToDateTime(reader[StoreChangeCommandDetail.colDeletedDate]);
 				}
 				else
 				{
 					objStoreChangeCommandDetail = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommandDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objStoreChangeCommandDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(StoreChangeCommandDetail objStoreChangeCommandDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objStoreChangeCommandDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommandDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeCommandDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, StoreChangeCommandDetail objStoreChangeCommandDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colStoreChangeCommandDetailID, objStoreChangeCommandDetail.StoreChangeCommandDetailID);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colStoreChangeCommandID, objStoreChangeCommandDetail.StoreChangeCommandID);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colProductID, objStoreChangeCommandDetail.ProductID);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colQuantity, objStoreChangeCommandDetail.Quantity);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colStoreChangeQuantity, objStoreChangeCommandDetail.StoreChangeQuantity);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colCreatedStoreID, objStoreChangeCommandDetail.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colCreatedUser, objStoreChangeCommandDetail.CreatedUser);
                objIData.AddParameter("@OrderID", objStoreChangeCommandDetail.OrderID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objStoreChangeCommandDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(StoreChangeCommandDetail objStoreChangeCommandDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objStoreChangeCommandDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommandDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeCommandDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, StoreChangeCommandDetail objStoreChangeCommandDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colStoreChangeCommandDetailID, objStoreChangeCommandDetail.StoreChangeCommandDetailID);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colStoreChangeCommandID, objStoreChangeCommandDetail.StoreChangeCommandID);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colProductID, objStoreChangeCommandDetail.ProductID);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colQuantity, objStoreChangeCommandDetail.Quantity);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colStoreChangeQuantity, objStoreChangeCommandDetail.StoreChangeQuantity);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colCreatedStoreID, objStoreChangeCommandDetail.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colUpdatedUser, objStoreChangeCommandDetail.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objStoreChangeCommandDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(StoreChangeCommandDetail objStoreChangeCommandDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objStoreChangeCommandDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommandDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeCommandDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, StoreChangeCommandDetail objStoreChangeCommandDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colStoreChangeCommandDetailID, objStoreChangeCommandDetail.StoreChangeCommandDetailID);
				objIData.AddParameter("@" + StoreChangeCommandDetail.colDeletedUser, objStoreChangeCommandDetail.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_StoreChangeCommandDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_STORECHANGECOMMANDDETAIL_AD";
		public const String SP_UPDATE = "PM_STORECHANGECOMMANDDETAIL_UP";
		public const String SP_DELETE = "PM_STORECHANGECOMMANDDETAIL_DE";
		public const String SP_SELECT = "PM_STORECHANGECOMMANDDETAIL_SE";
		public const String SP_SEARCH = "PM_STORECHANGECOMMANDDETAIL_SR";
		public const String SP_UPDATEINDEX = "PM_STORECHANGECOMMANDDETAIL_UPDINDEX";
		#endregion
		
	}
}
