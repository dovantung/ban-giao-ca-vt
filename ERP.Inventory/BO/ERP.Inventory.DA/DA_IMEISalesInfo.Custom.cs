
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using IMEISalesInfo = ERP.Inventory.BO.IMEISalesInfo;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Quang Tú 
	/// Created date 	: 17/06/2013 
	/// Thông tin bán hàng của IMEI(dùng cho máy cũ, hàng trưng bày)
	/// </summary>	
	public partial class DA_IMEISalesInfo
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_IMEISalesInfo.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// Xóa thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Review(List<IMEISalesInfo> lstIMEISalesInfo)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                foreach (IMEISalesInfo item in lstIMEISalesInfo)
                {
                    Review(objIData, item);                    
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Others, "Lỗi duyệt thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo -> Review", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Review(IData objIData, IMEISalesInfo objIMEISalesInfo)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_IMEISALESINFO_REV");
                objIData.AddParameter("@" + IMEISalesInfo.colProductID, objIMEISalesInfo.ProductID);
                objIData.AddParameter("@" + IMEISalesInfo.colIMEI, objIMEISalesInfo.IMEI);
                objIData.AddParameter("@" + IMEISalesInfo.colReviewedUser, objIMEISalesInfo.ReviewedUser);
                objIData.AddParameter("@" + IMEISalesInfo.colReviewedDate, objIMEISalesInfo.ReviewedDate);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Tìm kiếm thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage CheckIMEI(ref DataTable dtbData, string strIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_IMEISALESINFO_CHECKIMEI");
                objIData.AddParameter("@IMEI", strIMEI);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.CheckData, "Lỗi kiểm tra thông tin IMEI dùng cho thông tin bán hàng (dùng cho máy cũ, hàng trưng bày)", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo -> CheckIMEI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm thông tin thông tin bán hàng của imei/LôIMEI : dùng để chỉnh sửa orderindex
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataIMEIOrLotIMEI(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_IMEISALESINFO_SELINDEX");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin thông tin bán hàng của imei/Lô IMEI", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo -> SearchDataIMEIOrLotIMEI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Câp nhật index thông tin bán hàng của imei/Lô IMEI
        /// </summary>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage UpdateOrderIndex(DataTable dtbResource)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                foreach (DataRow row in dtbResource.Rows)
                {
                    if (Convert.ToBoolean(row["IsIMEI"]))
                    {
                         UpdateOrderIndex(objIData, row["PRODUCTID"].ToString().Trim(), row["IMEI"].ToString().Trim(), Convert.ToInt32(row["OrderIndex"]));
                    }
                    else
                    {
                        new PM.DA_LotIMEISalesInfo().UpdateOrderIndex(objIData, row["IMEI"].ToString().Trim(), Convert.ToInt32(row["OrderIndex"]));
                    }
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi cập nhật index của thông tin bán hàng của imei/lô imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo -> UpdateOrderIndex", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Cập nhật thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void UpdateOrderIndex(IData objIData, string strProductID, string strIMEI, int intOrderIndex)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_IMEISALESINFO_UPDINDEX");
                objIData.AddParameter("@" + IMEISalesInfo.colProductID, strProductID);
                objIData.AddParameter("@" + IMEISalesInfo.colIMEI, strIMEI);
                objIData.AddParameter("@" + IMEISalesInfo.colOrderIndex, intOrderIndex);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
		#endregion
		
		
	}
}
