
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using SplitProduct = ERP.Inventory.BO.SplitProduct;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 11/6/14 
	/// Tách linh kiện từ sản phẩm
	/// </summary>	
	public partial class DA_SplitProduct
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin tách linh kiện từ sản phẩm
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_SplitProduct.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin tách linh kiện từ sản phẩm", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SplitProduct -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// Thêm thông tin tách linh kiện từ sản phẩm
        /// </summary>
        /// <param name="objSplitProduct">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage InsertData(SplitProduct objSplitProduct)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                if (objSplitProduct.OutputVoucherIMEI != null)
                {
                    DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();
                    objDA_OutputVoucher.objLogObject = objLogObject;
                    objSplitProduct.OutputVoucherIMEI.CreatedUser = objSplitProduct.CreatedUser;
                    objSplitProduct.OutputVoucherIMEI.OutputContent = "Xuất tách linh kiện";
                    objDA_OutputVoucher.Insert(objIData, objSplitProduct.OutputVoucherIMEI);
                    if (objSplitProduct.OutputVoucherIMEI.OutputVoucherDetailList != null)
                    {
                        DA_OutputVoucherDetail objDA_OutputVoucherDetail = new DA_OutputVoucherDetail();
                        foreach (var item in objSplitProduct.OutputVoucherIMEI.OutputVoucherDetailList)
                        {
                            item.CreatedUser = objSplitProduct.CreatedUser;
                            item.VAT = 0;
                            item.VATPercent = 100;
                            item.OutputVoucherID = objSplitProduct.OutputVoucherIMEI.OutputVoucherID;
                            item.OutputDate = objSplitProduct.OutputVoucherIMEI.OutputDate;
                            item.CreatedStoreID = objSplitProduct.CreatedStoreID;
                            item.CreatedUser = objSplitProduct.CreatedUser;
                            objDA_OutputVoucherDetail.Insert(objIData, item);
                        }
                    }
                }

                if (objSplitProduct.InputVoucherIMEI != null)
                {
                    DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();
                    objDA_InputVoucher.objLogObject = objLogObject;
                    objSplitProduct.InputVoucherIMEI.CreatedUser = objSplitProduct.CreatedUser;
                    objSplitProduct.InputVoucherIMEI.Content = "Nhập lại sau khi tách linh kiện";
                    objDA_InputVoucher.Insert(objIData, objSplitProduct.InputVoucherIMEI);
                    if (objSplitProduct.InputVoucherIMEI.InputVoucherDetailList != null)
                    {
                        DA_InputVoucherDetail objDA_InputVoucherDetail = new DA_InputVoucherDetail();
                        foreach (var item in objSplitProduct.InputVoucherIMEI.InputVoucherDetailList)
                        {
                            item.CreatedUser = objSplitProduct.CreatedUser;
                            item.InputVoucherID = objSplitProduct.InputVoucherIMEI.InputVoucherID;
                            item.InputDate = objSplitProduct.InputVoucherIMEI.InputDate;
                            item.ISAddInStock = true;
                            item.VAT = 0;
                            item.VATPercent = 100;
                            item.FirstCustomerID = objSplitProduct.InputVoucherIMEI.CustomerID;
                            item.FirstInputDate = DateTime.Now;
                            item.FirstInputTypeID = objSplitProduct.InputVoucherIMEI.InputTypeID;
                            item.FirstInputVoucherDetailID = string.Empty;
                            item.FirstInputVoucherID = objSplitProduct.InputVoucherIMEI.InputVoucherID;
                            item.FirstInVoiceDate = objSplitProduct.InputVoucherIMEI.InVoiceDate;
                            objDA_InputVoucherDetail.Insert(objIData, item);
                            if (item.InputVoucherDetailIMEIList != null)
                            {
                                DA_InputVoucherDetailIMEI objDA_InputVoucherDetailIMEI = new DA_InputVoucherDetailIMEI();
                                foreach (var objIMEI in item.InputVoucherDetailIMEIList)
                                {
                                    objIMEI.InputVoucherDetailID = item.InputVoucherDetailID;
                                    objIMEI.CreatedUser = objSplitProduct.CreatedUser;
                                    objDA_InputVoucherDetailIMEI.Insert(objIData, objIMEI);
                                }
                            }
                        }
                    }
                }
                if (objSplitProduct.InputVoucher != null)
                {
                    DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();
                    objDA_InputVoucher.objLogObject = objLogObject;
                    objSplitProduct.InputVoucher.CreatedUser = objSplitProduct.CreatedUser;
                    objDA_InputVoucher.Insert(objIData, objSplitProduct.InputVoucher);
                    if (objSplitProduct.InputVoucher.InputVoucherDetailList != null)
                    {
                        DA_InputVoucherDetail objDA_InputVoucherDetail = new DA_InputVoucherDetail();
                        foreach (var item in objSplitProduct.InputVoucher.InputVoucherDetailList)
                        {
                            item.CreatedUser = objSplitProduct.CreatedUser;
                            item.InputVoucherID = objSplitProduct.InputVoucher.InputVoucherID;
                            item.InputDate = objSplitProduct.InputVoucher.InputDate;
                            item.FirstInputVoucherID = objSplitProduct.InputVoucher.InputVoucherID;
                            item.ISAddInStock = true;
                            item.VAT = 0;
                            item.VATPercent = 100;
                            objDA_InputVoucherDetail.Insert(objIData, item);
                        }
                    }
                }
                objSplitProduct.InputVoucherID1 = objSplitProduct.InputVoucherIMEI.InputVoucherID;
                objSplitProduct.InputVoucherID2 = objSplitProduct.InputVoucher.InputVoucherID;
                objSplitProduct.OutputVoucherID = objSplitProduct.OutputVoucherIMEI.OutputVoucherID;
                Insert(objIData, objSplitProduct);
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin tách linh kiện từ sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SplitProduct -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

		#endregion
		
		
	}
}
