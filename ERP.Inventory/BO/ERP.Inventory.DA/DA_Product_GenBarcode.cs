
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using Product_GenBarcode = ERP.Inventory.BO.Product_GenBarcode;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Quốc Hiệp
	/// Created date 	: 6/28/2017 
	/// 
	/// </summary>	
	public partial class DA_Product_GenBarcode
    {	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objProduct_GenBarcode">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref Product_GenBarcode objProduct_GenBarcode, decimal decBARCode)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + Product_GenBarcode.colBARCode, decBARCode);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objProduct_GenBarcode = new Product_GenBarcode();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colBARCode])) objProduct_GenBarcode.BARCode = Convert.ToDecimal(reader[Product_GenBarcode.colBARCode]);
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colProductID])) objProduct_GenBarcode.ProductID = Convert.ToString(reader[Product_GenBarcode.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[Product_GenBarcode.colBrandName])) objProduct_GenBarcode.BRANDNAME = Convert.ToString(reader[Product_GenBarcode.colBrandName]).Trim();
                    if (!Convert.IsDBNull(reader[Product_GenBarcode.colBrandAddress])) objProduct_GenBarcode.BrandAddress = Convert.ToString(reader[Product_GenBarcode.colBrandAddress]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colSetUPCountry])) objProduct_GenBarcode.SetUPCountry = Convert.ToString(reader[Product_GenBarcode.colSetUPCountry]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colSetUPYear])) objProduct_GenBarcode.SetUPYear = Convert.ToString(reader[Product_GenBarcode.colSetUPYear]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colQUANTIFIED])) objProduct_GenBarcode.QUANTIFIED = Convert.ToString(reader[Product_GenBarcode.colQUANTIFIED]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colCAPACITY])) objProduct_GenBarcode.CAPACITY = Convert.ToString(reader[Product_GenBarcode.colCAPACITY]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colMATERITION])) objProduct_GenBarcode.MATERITION = Convert.ToString(reader[Product_GenBarcode.colMATERITION]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colUSES])) objProduct_GenBarcode.USES = Convert.ToString(reader[Product_GenBarcode.colUSES]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colUSEProduct])) objProduct_GenBarcode.USEProduct = Convert.ToString(reader[Product_GenBarcode.colUSEProduct]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colPRESERVE])) objProduct_GenBarcode.PRESERVE = Convert.ToString(reader[Product_GenBarcode.colPRESERVE]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colHOWUSE])) objProduct_GenBarcode.HOWUSE = Convert.ToString(reader[Product_GenBarcode.colHOWUSE]).Trim();
 					if (!Convert.IsDBNull(reader[Product_GenBarcode.colDISTRIBUTE])) objProduct_GenBarcode.DISTRIBUTE = Convert.ToString(reader[Product_GenBarcode.colDISTRIBUTE]).Trim();
 				}
 				else
 				{
 					objProduct_GenBarcode = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objProduct_GenBarcode">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(List<Product_GenBarcode> objlistProduct_GenBarcode)
		{
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                foreach(Product_GenBarcode objProduct_GenBarcode in objlistProduct_GenBarcode)
                {
                    objResultMessage =  Insert(objIData, objProduct_GenBarcode);
                    if (objResultMessage.IsError)
                    {
                        return objResultMessage; ;
                    }
                }
			}
			catch (Exception objEx)
			{
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProduct_GenBarcode">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(IData objIData, Product_GenBarcode objProduct_GenBarcode)
		{
            ResultMessage objResultMessage = new ResultMessage();
            try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + Product_GenBarcode.colBARCode, objProduct_GenBarcode.BARCode);
				objIData.AddParameter("@" + Product_GenBarcode.colProductID, objProduct_GenBarcode.ProductID.Trim());
                objIData.AddParameter("@" + Product_GenBarcode.colBrandName, objProduct_GenBarcode.BRANDNAME.Trim());
                objIData.AddParameter("@" + Product_GenBarcode.colBrandAddress, objProduct_GenBarcode.BrandAddress.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colSetUPCountry, objProduct_GenBarcode.SetUPCountry.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colSetUPYear, objProduct_GenBarcode.SetUPYear.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colQUANTIFIED, objProduct_GenBarcode.QUANTIFIED.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colCAPACITY, objProduct_GenBarcode.CAPACITY.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colMATERITION, objProduct_GenBarcode.MATERITION.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colUSES, objProduct_GenBarcode.USES.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colUSEProduct, objProduct_GenBarcode.USEProduct.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colPRESERVE, objProduct_GenBarcode.PRESERVE.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colHOWUSE, objProduct_GenBarcode.HOWUSE.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colDISTRIBUTE, objProduct_GenBarcode.DISTRIBUTE.Trim());
                string strBarcode =  objIData.ExecStoreToString();
                if (Convert.ToDecimal(strBarcode.Trim()) == -1)
                {
                    objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objProduct_GenBarcode.BARCode.ToString() + " Mã Barcode đã có trong hệ thống!");
                    ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> Insert", InventoryGlobals.ModuleName);
                    return objResultMessage;
                }
			}
			catch (Exception objEx)
			{
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
			}
            return objResultMessage;
        }


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objProduct_GenBarcode">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(List<Product_GenBarcode> objlistProduct_GenBarcode)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                foreach (Product_GenBarcode objProduct_GenBarcode in objlistProduct_GenBarcode)
                {
                    objResultMessage =  Update(objIData, objProduct_GenBarcode);
                    if (objResultMessage.IsError)
                    {
                        return objResultMessage; ;
                    }
                }
			}
			catch (Exception objEx)
			{
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProduct_GenBarcode">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(IData objIData, Product_GenBarcode objProduct_GenBarcode)
		{
            ResultMessage objResultMessage = new ResultMessage();
            try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + Product_GenBarcode.colBARCode, objProduct_GenBarcode.BARCode);
				objIData.AddParameter("@" + Product_GenBarcode.colProductID, objProduct_GenBarcode.ProductID.Trim());
                objIData.AddParameter("@" + Product_GenBarcode.colBrandName, objProduct_GenBarcode.BRANDNAME.Trim());
                objIData.AddParameter("@" + Product_GenBarcode.colBrandAddress, objProduct_GenBarcode.BrandAddress.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colSetUPCountry, objProduct_GenBarcode.SetUPCountry.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colSetUPYear, objProduct_GenBarcode.SetUPYear.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colQUANTIFIED, objProduct_GenBarcode.QUANTIFIED.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colCAPACITY, objProduct_GenBarcode.CAPACITY.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colMATERITION, objProduct_GenBarcode.MATERITION.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colUSES, objProduct_GenBarcode.USES.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colUSEProduct, objProduct_GenBarcode.USEProduct.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colPRESERVE, objProduct_GenBarcode.PRESERVE.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colHOWUSE, objProduct_GenBarcode.HOWUSE.Trim());
				objIData.AddParameter("@" + Product_GenBarcode.colDISTRIBUTE, objProduct_GenBarcode.DISTRIBUTE.Trim());
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            return objResultMessage;

        }


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objProduct_GenBarcode">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(Product_GenBarcode objProduct_GenBarcode)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objProduct_GenBarcode);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProduct_GenBarcode">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, Product_GenBarcode objProduct_GenBarcode)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + Product_GenBarcode.colBARCode, objProduct_GenBarcode.BARCode);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="DT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage GenBarcode(string strProductID, String strStringBarcode, ref string strBarcode)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objResultMessage = GenBarcode(objIData, strProductID, strStringBarcode, ref strBarcode);
                if (objResultMessage.IsError)
                {
                  return objResultMessage; ;
                }
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objProduct_GenBarcode">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage GenBarcode(IData objIData, string strProductID, String strStringBarcode, ref string strBarcodeReturn)
        {
            ResultMessage objResultMessage = new ResultMessage();
            try
            {
                objIData.CreateNewStoredProcedure("MD_GENBARCODE");
                objIData.AddParameter("@" + Product_GenBarcode.colProductID, strProductID);
                objIData.AddParameter("@STRINGBARCODE", strStringBarcode);
                string strBarcode = objIData.ExecStoreToString();
                if (Convert.ToDecimal(strBarcode.Trim()) == -1)
                {
                    objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", "Đã hết Mã Barcode");
                    ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> Insert", InventoryGlobals.ModuleName);
                    return objResultMessage;
                }
                else
                {
                    strBarcodeReturn = strBarcode.Trim();
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Product_GenBarcode -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            return objResultMessage;
        }
        #endregion


        #region Constructor

        public DA_Product_GenBarcode()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "MD_Product_Barcode_ADD";
		public const String SP_UPDATE = "MD_Product_Barcode_UPD";
		public const String SP_DELETE = "MD_Product_Barcode_DEL";
		public const String SP_SELECT = "MD_Product_Barcode_SEL";
		public const String SP_SEARCH = "MD_PRODUCT_BARCODE_SRH";
		public const String SP_UPDATEINDEX = "MD_Product_Barcode_UPDINDEX";
		#endregion
		
	}
}
