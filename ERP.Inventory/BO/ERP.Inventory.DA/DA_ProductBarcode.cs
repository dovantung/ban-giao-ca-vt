﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion

namespace ERP.Inventory.DA
{
    /// <summary>
    /// Create: Nguyễn Linh Tuấn
    /// Date: 01/11/2012
    /// </summary>
    public class DA_ProductBarcode
    {

        /// <summary>
        /// Lấy danh sách tồn kho hiện tại để in
        /// </summary>        
        public ResultMessage GetCurrentInstockPrints(ref DataTable dtbData, string strUserName, int intMainGroupID, int intSubGroupID, string strProductID, string strBrandIDList, int intStoreID,int intIsNew,bool bolIsInStock,string strInputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_CurrentInstock_GetPrints");
                objIData.AddParameter("@UserName", strUserName);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@SubGroupID", intSubGroupID);
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@BrandIDList", strBrandIDList);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@IsNew", intIsNew);
                objIData.AddParameter("@ISINSTOCK", bolIsInStock);
                objIData.AddParameter("@INPUTVOUCHERID", strInputVoucherID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi lấy danh sách tồn kho hiện tại để in barcode";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_Barcode -> GetCurrentInstockPrints", "Barcode");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Lấy danh sách tồn kho hiện tại để in
        /// </summary>        
        public ResultMessage GetCurrentInstockPrints_IMEI(ref DataTable dtbData, string strUserName, int intMainGroupID, int intSubGroupID, string strProductID, string strBrandIDList, int intStoreID, int intIsNew, string strInputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_CurrentInstock_IMEI_GetPr");
                objIData.AddParameter("@UserName", strUserName);
                objIData.AddParameter("@MainGroupID", intMainGroupID);
                objIData.AddParameter("@SubGroupID", intSubGroupID);
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@BrandIDList", strBrandIDList);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@IsNew", intIsNew);
                objIData.AddParameter("@INPUTVOUCHERID", strInputVoucherID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi lấy danh sách tồn kho hiện tại để in barcode";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_Barcode -> GetCurrentInstockPrints_IMEI", "Barcode");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
    }
}
