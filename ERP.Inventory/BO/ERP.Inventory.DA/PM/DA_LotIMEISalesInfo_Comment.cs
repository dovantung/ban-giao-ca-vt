﻿
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using LotIMEISalesInfo_Comment = ERP.Inventory.BO.PM.LotIMEISalesInfo_Comment;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
    /// Created by 		: Trần Thị Mi 
    /// Created date 	: 01/12/2014 
    /// 
    /// </summary>	
    public partial class DA_LotIMEISalesInfo_Comment
    {

        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods


        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objLotIMEISalesInfo_Comment">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment, decimal decCommentID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentID, decCommentID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objLotIMEISalesInfo_Comment = new LotIMEISalesInfo_Comment();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colCommentID])) objLotIMEISalesInfo_Comment.CommentID = Convert.ToDecimal(reader[LotIMEISalesInfo_Comment.colCommentID]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colReplyToCommentID])) objLotIMEISalesInfo_Comment.ReplyToCommentID = Convert.ToDecimal(reader[LotIMEISalesInfo_Comment.colReplyToCommentID]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colLotIMEISalesInfoID])) objLotIMEISalesInfo_Comment.LotIMEISalesInfoID = Convert.ToString(reader[LotIMEISalesInfo_Comment.colLotIMEISalesInfoID]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colCommentDate])) objLotIMEISalesInfo_Comment.CommentDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Comment.colCommentDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colCommentTitle])) objLotIMEISalesInfo_Comment.CommentTitle = Convert.ToString(reader[LotIMEISalesInfo_Comment.colCommentTitle]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colCommentContent])) objLotIMEISalesInfo_Comment.CommentContent = Convert.ToString(reader[LotIMEISalesInfo_Comment.colCommentContent]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colCommentUser])) objLotIMEISalesInfo_Comment.CommentUser = Convert.ToString(reader[LotIMEISalesInfo_Comment.colCommentUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colCommentEmail])) objLotIMEISalesInfo_Comment.CommentEmail = Convert.ToString(reader[LotIMEISalesInfo_Comment.colCommentEmail]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colModelRate])) objLotIMEISalesInfo_Comment.ModelRate = Convert.ToInt32(reader[LotIMEISalesInfo_Comment.colModelRate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colIsCommentUserLogin])) objLotIMEISalesInfo_Comment.IsCommentUserLogin = Convert.ToBoolean(reader[LotIMEISalesInfo_Comment.colIsCommentUserLogin]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colUserHostAddress])) objLotIMEISalesInfo_Comment.UserHostAddress = Convert.ToString(reader[LotIMEISalesInfo_Comment.colUserHostAddress]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colUserComputerName])) objLotIMEISalesInfo_Comment.UserComputerName = Convert.ToString(reader[LotIMEISalesInfo_Comment.colUserComputerName]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colUserAgent])) objLotIMEISalesInfo_Comment.UserAgent = Convert.ToString(reader[LotIMEISalesInfo_Comment.colUserAgent]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colIsStaffComment])) objLotIMEISalesInfo_Comment.IsStaffComment = Convert.ToBoolean(reader[LotIMEISalesInfo_Comment.colIsStaffComment]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colCommentFullName])) objLotIMEISalesInfo_Comment.CommentFullName = Convert.ToString(reader[LotIMEISalesInfo_Comment.colCommentFullName]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colOrderIndex])) objLotIMEISalesInfo_Comment.OrderIndex = Convert.ToDecimal(reader[LotIMEISalesInfo_Comment.colOrderIndex]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colIsActive])) objLotIMEISalesInfo_Comment.IsActive = Convert.ToBoolean(reader[LotIMEISalesInfo_Comment.colIsActive]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colIsSystem])) objLotIMEISalesInfo_Comment.IsSystem = Convert.ToBoolean(reader[LotIMEISalesInfo_Comment.colIsSystem]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colCreatedUser])) objLotIMEISalesInfo_Comment.CreatedUser = Convert.ToString(reader[LotIMEISalesInfo_Comment.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colCreatedDate])) objLotIMEISalesInfo_Comment.CreatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Comment.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colUpdatedUser])) objLotIMEISalesInfo_Comment.UpdatedUser = Convert.ToString(reader[LotIMEISalesInfo_Comment.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colUpdatedDate])) objLotIMEISalesInfo_Comment.UpdatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Comment.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colIsDeleted])) objLotIMEISalesInfo_Comment.IsDeleted = Convert.ToBoolean(reader[LotIMEISalesInfo_Comment.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colDeletedUser])) objLotIMEISalesInfo_Comment.DeletedUser = Convert.ToString(reader[LotIMEISalesInfo_Comment.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colDeletedDate])) objLotIMEISalesInfo_Comment.DeletedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Comment.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Comment.colPhoneNumber])) objLotIMEISalesInfo_Comment.PhoneNumber = Convert.ToString(reader[LotIMEISalesInfo_Comment.colPhoneNumber]).Trim();
                }
                else
                {
                    objLotIMEISalesInfo_Comment = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_Comment -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objLotIMEISalesInfo_Comment">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objLotIMEISalesInfo_Comment);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_Comment -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objLotIMEISalesInfo_Comment">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                //objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentID, objLotIMEISalesInfo_Comment.CommentID);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colReplyToCommentID, objLotIMEISalesInfo_Comment.ReplyToCommentID);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colLotIMEISalesInfoID, objLotIMEISalesInfo_Comment.LotIMEISalesInfoID);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentDate, objLotIMEISalesInfo_Comment.CommentDate);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentTitle, objLotIMEISalesInfo_Comment.CommentTitle);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentContent, objLotIMEISalesInfo_Comment.CommentContent);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentUser, objLotIMEISalesInfo_Comment.CommentUser);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentEmail, objLotIMEISalesInfo_Comment.CommentEmail);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colModelRate, objLotIMEISalesInfo_Comment.ModelRate);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colIsCommentUserLogin, objLotIMEISalesInfo_Comment.IsCommentUserLogin);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colUserHostAddress, objLogObject.UserHostAddress);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colUserComputerName, objLotIMEISalesInfo_Comment.UserComputerName);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colUserAgent, objLotIMEISalesInfo_Comment.UserAgent);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colIsStaffComment, objLotIMEISalesInfo_Comment.IsStaffComment);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentFullName, objLotIMEISalesInfo_Comment.CommentFullName);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colOrderIndex, objLotIMEISalesInfo_Comment.OrderIndex);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colIsActive, objLotIMEISalesInfo_Comment.IsActive);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colIsSystem, objLotIMEISalesInfo_Comment.IsSystem);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCreatedUser, objLotIMEISalesInfo_Comment.CreatedUser);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colPhoneNumber, objLotIMEISalesInfo_Comment.PhoneNumber);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString); 
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objLotIMEISalesInfo_Comment">Đối tượng truyền vào</param>
        /// <param name="lstOrderIndex">Danh sách OrderIndex</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                Update(objIData, objLotIMEISalesInfo_Comment);
                //if (lstOrderIndex != null && lstOrderIndex.Count > 0)
                //{
                //    for (int i = 0; i < lstOrderIndex.Count; i++)
                //    {
                //        UpdateOrderIndex(objIData, Convert.ToInt32(lstOrderIndex[i]), i);
                //    }
                //}
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_Comment -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objLotIMEISalesInfo_Comment">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentID, objLotIMEISalesInfo_Comment.CommentID);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colReplyToCommentID, objLotIMEISalesInfo_Comment.ReplyToCommentID);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colLotIMEISalesInfoID, objLotIMEISalesInfo_Comment.LotIMEISalesInfoID);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentDate, objLotIMEISalesInfo_Comment.CommentDate);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentTitle, objLotIMEISalesInfo_Comment.CommentTitle);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentContent, objLotIMEISalesInfo_Comment.CommentContent);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentUser, objLotIMEISalesInfo_Comment.CommentUser);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentEmail, objLotIMEISalesInfo_Comment.CommentEmail);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colModelRate, objLotIMEISalesInfo_Comment.ModelRate);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colIsCommentUserLogin, objLotIMEISalesInfo_Comment.IsCommentUserLogin);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colUserHostAddress, objLotIMEISalesInfo_Comment.UserHostAddress);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colUserComputerName, objLotIMEISalesInfo_Comment.UserComputerName);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colUserAgent, objLotIMEISalesInfo_Comment.UserAgent);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colIsStaffComment, objLotIMEISalesInfo_Comment.IsStaffComment);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentFullName, objLotIMEISalesInfo_Comment.CommentFullName);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colOrderIndex, objLotIMEISalesInfo_Comment.OrderIndex);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colIsActive, objLotIMEISalesInfo_Comment.IsActive);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colIsSystem, objLotIMEISalesInfo_Comment.IsSystem);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colUpdatedUser, objLotIMEISalesInfo_Comment.UpdatedUser);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colPhoneNumber, objLotIMEISalesInfo_Comment.PhoneNumber);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="intOrderIndex">Thứ tự</param>
        /// <returns>Kết quả trả về</returns>
        public void UpdateOrderIndex(IData objIData, decimal decCommentID, int intOrderIndex)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATEINDEX);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentID, decCommentID);
                objIData.AddParameter("@OrderIndex", intOrderIndex);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objLotIMEISalesInfo_Comment">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objLotIMEISalesInfo_Comment);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_Comment -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objLotIMEISalesInfo_Comment">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colCommentID, objLotIMEISalesInfo_Comment.CommentID);
                objIData.AddParameter("@" + LotIMEISalesInfo_Comment.colDeletedUser, objLotIMEISalesInfo_Comment.DeletedUser);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage DeleteList(List<LotIMEISalesInfo_Comment> lstLotIMEISalesInfo_Comment)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment in lstLotIMEISalesInfo_Comment)
                    Delete(objIData, objLotIMEISalesInfo_Comment);
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_Comment -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion


        #region Constructor

        public DA_LotIMEISalesInfo_Comment()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "PM_LOTIMEI_COMMENT_ADD";
        public const String SP_UPDATE = "PM_LOTIMEISALESINFO_CMT_UPD";
        public const String SP_DELETE = "PM_LOTIMEISALESINFO_CMT_DEL";
        public const String SP_SELECT = "PM_LOTIMEISALESINFO_CMT_SEL";
        public const String SP_SEARCH = "PM_LOTIMEISALESINFO_CMT_SRH";
        public const String SP_UPDATEINDEX = "PM_LotIMEISalesInfo_Comment_UPDINDEX";
        #endregion

    }
}


