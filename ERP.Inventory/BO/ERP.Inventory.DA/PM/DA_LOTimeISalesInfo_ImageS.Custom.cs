
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using LotIMEISalesInfo_Images = ERP.Inventory.BO.PM.LotIMEISalesInfo_Images;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: CAO HỮU VŨ LAM 
	/// Created date 	: 4/2/2014 
	/// 
	/// </summary>	
	public partial class DA_LotIMEISalesInfo_Images
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin 
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_LotIMEISalesInfo_Images.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_ImageS -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objLotIMEISalesInfo_ImageS">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public List<LotIMEISalesInfo_Images> SearchDataToList(IData objIData, params object[] objKeywords)
        {
            List<LotIMEISalesInfo_Images> lstLotIMEISalesInfo_Images = new List<LotIMEISalesInfo_Images>();
            try
            {
                objIData.CreateNewStoredProcedure(DA_LotIMEISalesInfo_Images.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    LotIMEISalesInfo_Images objLotIMEISalesInfo_Images = new LotIMEISalesInfo_Images();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colImageID])) objLotIMEISalesInfo_Images.ImageID = Convert.ToString(reader[LotIMEISalesInfo_Images.colImageID]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colLotIMEISalesInfoID])) objLotIMEISalesInfo_Images.LotIMEISalesInfoID = Convert.ToString(reader[LotIMEISalesInfo_Images.colLotIMEISalesInfoID]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colImageName])) objLotIMEISalesInfo_Images.ImageName = Convert.ToString(reader[LotIMEISalesInfo_Images.colImageName]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colDescription])) objLotIMEISalesInfo_Images.Description = Convert.ToString(reader[LotIMEISalesInfo_Images.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colLargeSizeImage])) objLotIMEISalesInfo_Images.LargeSizeImage = Convert.ToString(reader[LotIMEISalesInfo_Images.colLargeSizeImage]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colMediumSizeImage])) objLotIMEISalesInfo_Images.MediumSizeImage = Convert.ToString(reader[LotIMEISalesInfo_Images.colMediumSizeImage]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colSmallSizeImage])) objLotIMEISalesInfo_Images.SmallSizeImage = Convert.ToString(reader[LotIMEISalesInfo_Images.colSmallSizeImage]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colLargeSizeImageFileID])) objLotIMEISalesInfo_Images.LargeSizeImageFileID = Convert.ToString(reader[LotIMEISalesInfo_Images.colLargeSizeImageFileID]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colMediumSizeImageFileID])) objLotIMEISalesInfo_Images.MediumSizeImageFileID = Convert.ToString(reader[LotIMEISalesInfo_Images.colMediumSizeImageFileID]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colSmallSizeImageFileID])) objLotIMEISalesInfo_Images.SmallSizeImageFileID = Convert.ToString(reader[LotIMEISalesInfo_Images.colSmallSizeImageFileID]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colIsDefault])) objLotIMEISalesInfo_Images.IsDefault = Convert.ToBoolean(reader[LotIMEISalesInfo_Images.colIsDefault]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colOrderIndex])) objLotIMEISalesInfo_Images.OrderIndex = Convert.ToInt32(reader[LotIMEISalesInfo_Images.colOrderIndex]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colIsActive])) objLotIMEISalesInfo_Images.IsActive = Convert.ToBoolean(reader[LotIMEISalesInfo_Images.colIsActive]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colIsSystem])) objLotIMEISalesInfo_Images.IsSystem = Convert.ToBoolean(reader[LotIMEISalesInfo_Images.colIsSystem]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colCreatedUser])) objLotIMEISalesInfo_Images.CreatedUser = Convert.ToString(reader[LotIMEISalesInfo_Images.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colCreatedDate])) objLotIMEISalesInfo_Images.CreatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Images.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colUpdatedUser])) objLotIMEISalesInfo_Images.UpdatedUser = Convert.ToString(reader[LotIMEISalesInfo_Images.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colUpdatedDate])) objLotIMEISalesInfo_Images.UpdatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Images.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colIsDeleted])) objLotIMEISalesInfo_Images.IsDeleted = Convert.ToBoolean(reader[LotIMEISalesInfo_Images.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colDeletedUser])) objLotIMEISalesInfo_Images.DeletedUser = Convert.ToString(reader[LotIMEISalesInfo_Images.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colDeletedDate])) objLotIMEISalesInfo_Images.DeletedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Images.colDeletedDate]);                   
                    objLotIMEISalesInfo_Images.IsExist = true;
                    lstLotIMEISalesInfo_Images.Add(objLotIMEISalesInfo_Images);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                throw (objEx);
            }
            return lstLotIMEISalesInfo_Images;
        }
		#endregion
		
		
	}
}
