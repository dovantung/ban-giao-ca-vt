
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using LotIMEISalesInfo = ERP.Inventory.BO.PM.LotIMEISalesInfo;
using ERP.Inventory.BO.PM;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: CAO HỮU VŨ LAM 
	/// Created date 	: 4/2/2014 
	/// Thông tin bán hàng của 1 lô IMEI
	/// </summary>	
	public partial class DA_LotIMEISalesInfo
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin thông tin bán hàng của 1 lô imei
		/// </summary>
		/// <param name="objLotIMEISalesInfo">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref LotIMEISalesInfo objLotIMEISalesInfo, string strLotIMEISalesInfoID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + LotIMEISalesInfo.colLotIMEISalesInfoID, strLotIMEISalesInfoID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objLotIMEISalesInfo = new LotIMEISalesInfo();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colLotIMEISalesInfoID])) objLotIMEISalesInfo.LotIMEISalesInfoID = Convert.ToString(reader[LotIMEISalesInfo.colLotIMEISalesInfoID]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colLotIMEISalesInfoName])) objLotIMEISalesInfo.LotIMEISalesInfoName = Convert.ToString(reader[LotIMEISalesInfo.colLotIMEISalesInfoName]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colProductID])) objLotIMEISalesInfo.ProductID = Convert.ToString(reader[LotIMEISalesInfo.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colDescription])) objLotIMEISalesInfo.Description = Convert.ToString(reader[LotIMEISalesInfo.colDescription]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colIsBrandNewWarranty])) objLotIMEISalesInfo.IsBrandNewWarranty = Convert.ToBoolean(reader[LotIMEISalesInfo.colIsBrandNewWarranty]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colEndWarrantyDate])) objLotIMEISalesInfo.EndWarrantyDate = Convert.ToDateTime(reader[LotIMEISalesInfo.colEndWarrantyDate]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colNote])) objLotIMEISalesInfo.Note = Convert.ToString(reader[LotIMEISalesInfo.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colPrintVATContent])) objLotIMEISalesInfo.PrintVATContent = Convert.ToString(reader[LotIMEISalesInfo.colPrintVATContent]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colIsConfirm])) objLotIMEISalesInfo.IsConfirm = Convert.ToBoolean(reader[LotIMEISalesInfo.colIsConfirm]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colIsReviewed])) objLotIMEISalesInfo.IsReviewed = Convert.ToBoolean(reader[LotIMEISalesInfo.colIsReviewed]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colReviewedUser])) objLotIMEISalesInfo.ReviewedUser = Convert.ToString(reader[LotIMEISalesInfo.colReviewedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colReviewedDate])) objLotIMEISalesInfo.ReviewedDate = Convert.ToDateTime(reader[LotIMEISalesInfo.colReviewedDate]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colWebProductSpecContent])) objLotIMEISalesInfo.WebProductSpecContent = Convert.ToString(reader[LotIMEISalesInfo.colWebProductSpecContent]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colIsActive])) objLotIMEISalesInfo.IsActive = Convert.ToBoolean(reader[LotIMEISalesInfo.colIsActive]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colIsSystem])) objLotIMEISalesInfo.IsSystem = Convert.ToBoolean(reader[LotIMEISalesInfo.colIsSystem]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colCreatedUser])) objLotIMEISalesInfo.CreatedUser = Convert.ToString(reader[LotIMEISalesInfo.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colCreatedDate])) objLotIMEISalesInfo.CreatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colUpdatedUser])) objLotIMEISalesInfo.UpdatedUser = Convert.ToString(reader[LotIMEISalesInfo.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colUpdatedDate])) objLotIMEISalesInfo.UpdatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colIsDeleted])) objLotIMEISalesInfo.IsDeleted = Convert.ToBoolean(reader[LotIMEISalesInfo.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colDeletedUser])) objLotIMEISalesInfo.DeletedUser = Convert.ToString(reader[LotIMEISalesInfo.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colDeletedDate])) objLotIMEISalesInfo.DeletedDate = Convert.ToDateTime(reader[LotIMEISalesInfo.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo.colProductName])) objLotIMEISalesInfo.ProductName = Convert.ToString(reader[LotIMEISalesInfo.colProductName]).Trim();
                    objLotIMEISalesInfo.IsExist = true;
                    objLotIMEISalesInfo.LotIMEISalesInfo_ImagesList = new DA_LotIMEISalesInfo_Images().SearchDataToList(objIData, "@" + LotIMEISalesInfo.colLotIMEISalesInfoID, objLotIMEISalesInfo.LotIMEISalesInfoID);
                    objLotIMEISalesInfo.LotIMEISalesInfo_IMEIList = new DA_LotIMEISalesInfo_IMEI().SearchDataToList(objIData, "@" + LotIMEISalesInfo.colLotIMEISalesInfoID, objLotIMEISalesInfo.LotIMEISalesInfoID);
                    objLotIMEISalesInfo.LotIMEISalesInfo_ProSpecList = new DA_LotIMEISalesInfo_ProSpec().SearchDataToList(objIData, "@" + LotIMEISalesInfo.colLotIMEISalesInfoID, objLotIMEISalesInfo.LotIMEISalesInfoID,
                                                                                                                                    "@" + LotIMEISalesInfo.colProductID, objLotIMEISalesInfo.ProductID);
                }
 				else
 				{
                    objLotIMEISalesInfo = new LotIMEISalesInfo();
                    objLotIMEISalesInfo.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông tin bán hàng của 1 lô imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin thông tin bán hàng của 1 lô imei
		/// </summary>
		/// <param name="objLotIMEISalesInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(LotIMEISalesInfo objLotIMEISalesInfo)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
				Insert(objIData, ref objLotIMEISalesInfo);
                if (objLotIMEISalesInfo.LotIMEISalesInfo_ImagesList != null)
                {
                    DA_LotIMEISalesInfo_Images objDA_LotIMEISalesInfo_Images = new DA_LotIMEISalesInfo_Images();
                    foreach (LotIMEISalesInfo_Images objLotIMEISalesInfo_Images in objLotIMEISalesInfo.LotIMEISalesInfo_ImagesList)
                    {
                        objLotIMEISalesInfo_Images.LotIMEISalesInfoID = objLotIMEISalesInfo.LotIMEISalesInfoID;
                        objLotIMEISalesInfo_Images.CreatedUser = objLotIMEISalesInfo.CreatedUser;
                        objDA_LotIMEISalesInfo_Images.Insert(objIData, objLotIMEISalesInfo_Images); 
                    }      
                }
                if (objLotIMEISalesInfo.LotIMEISalesInfo_IMEIList != null)
                {
                    DA_LotIMEISalesInfo_IMEI objDA_LotIMEISalesInfo_IMEI = new DA_LotIMEISalesInfo_IMEI();
                    foreach (LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI in objLotIMEISalesInfo.LotIMEISalesInfo_IMEIList)
                    {
                        objLotIMEISalesInfo_IMEI.LotIMEISalesInfoID = objLotIMEISalesInfo.LotIMEISalesInfoID;
                        objLotIMEISalesInfo_IMEI.CreatedUser = objLotIMEISalesInfo.CreatedUser;
                        objDA_LotIMEISalesInfo_IMEI.Insert(objIData, objLotIMEISalesInfo_IMEI);
                    }
                }
                if (objLotIMEISalesInfo.LotIMEISalesInfo_ProSpecList != null)
                {
                    DA_LotIMEISalesInfo_ProSpec objDA_LotIMEISalesInfo_ProSpec = new DA_LotIMEISalesInfo_ProSpec();
                    foreach (LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_ProSpec in objLotIMEISalesInfo.LotIMEISalesInfo_ProSpecList)
                    {
                        objLotIMEISalesInfo_ProSpec.LotIMEISalesInfoID = objLotIMEISalesInfo.LotIMEISalesInfoID;
                        objDA_LotIMEISalesInfo_ProSpec.objLogObject = objLogObject;
                        objDA_LotIMEISalesInfo_ProSpec.Insert(objIData, objLotIMEISalesInfo_ProSpec);
                    }
                }
                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
                objIData.RollBackTransaction();
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin thông tin bán hàng của 1 lô imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin thông tin bán hàng của 1 lô imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, ref LotIMEISalesInfo objLotIMEISalesInfo)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				//objIData.AddParameter("@" + LotIMEISalesInfo.colLotIMEISalesInfoID, objLotIMEISalesInfo.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo.colLotIMEISalesInfoName, objLotIMEISalesInfo.LotIMEISalesInfoName);
				objIData.AddParameter("@" + LotIMEISalesInfo.colProductID, objLotIMEISalesInfo.ProductID);
				objIData.AddParameter("@" + LotIMEISalesInfo.colDescription, objLotIMEISalesInfo.Description);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsBrandNewWarranty, objLotIMEISalesInfo.IsBrandNewWarranty);
				objIData.AddParameter("@" + LotIMEISalesInfo.colEndWarrantyDate, objLotIMEISalesInfo.EndWarrantyDate);
				objIData.AddParameter("@" + LotIMEISalesInfo.colNote, objLotIMEISalesInfo.Note);
				objIData.AddParameter("@" + LotIMEISalesInfo.colPrintVATContent, objLotIMEISalesInfo.PrintVATContent);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsConfirm, objLotIMEISalesInfo.IsConfirm);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsReviewed, objLotIMEISalesInfo.IsReviewed);
				objIData.AddParameter("@" + LotIMEISalesInfo.colReviewedUser, objLotIMEISalesInfo.ReviewedUser);
				objIData.AddParameter("@" + LotIMEISalesInfo.colReviewedDate, objLotIMEISalesInfo.ReviewedDate);
				objIData.AddParameter("@" + LotIMEISalesInfo.colWebProductSpecContent, objLotIMEISalesInfo.WebProductSpecContent);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsActive, objLotIMEISalesInfo.IsActive);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsSystem, objLotIMEISalesInfo.IsSystem);
				objIData.AddParameter("@" + LotIMEISalesInfo.colCreatedUser, objLotIMEISalesInfo.CreatedUser);
                objIData.AddParameter("@" + LotIMEISalesInfo.colCreatedStoreID, objLotIMEISalesInfo.CreatedStoreID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objLotIMEISalesInfo.LotIMEISalesInfoID = objIData.ExecStoreToString();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin thông tin bán hàng của 1 lô imei
		/// </summary>
		/// <param name="objLotIMEISalesInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(LotIMEISalesInfo objLotIMEISalesInfo)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
				Update(objIData, objLotIMEISalesInfo);
                DA_LotIMEISalesInfo_Images objDA_LotIMEISalesInfo_Images = new DA_LotIMEISalesInfo_Images();
                objDA_LotIMEISalesInfo_Images.DeleteByLotIMEISalesInfo(objIData, objLotIMEISalesInfo.LotIMEISalesInfoID, objLotIMEISalesInfo.UpdatedUser);
                if (objLotIMEISalesInfo.LotIMEISalesInfo_ImagesList != null)
                {                   
                    foreach (LotIMEISalesInfo_Images objLotIMEISalesInfo_Images in objLotIMEISalesInfo.LotIMEISalesInfo_ImagesList)
                    {
                        if (objLotIMEISalesInfo_Images.IsExist)
                        {
                            objLotIMEISalesInfo_Images.UpdatedUser = objLotIMEISalesInfo.UpdatedUser;
                            objDA_LotIMEISalesInfo_Images.Update(objIData, objLotIMEISalesInfo_Images);
                        }
                        else
                        {
                            objLotIMEISalesInfo_Images.LotIMEISalesInfoID = objLotIMEISalesInfo.LotIMEISalesInfoID;
                            objLotIMEISalesInfo_Images.CreatedUser = objLotIMEISalesInfo.UpdatedUser;
                            objDA_LotIMEISalesInfo_Images.Insert(objIData, objLotIMEISalesInfo_Images);
                        }                       
                    }
                }
                DA_LotIMEISalesInfo_IMEI objDA_LotIMEISalesInfo_IMEI = new DA_LotIMEISalesInfo_IMEI();
                objDA_LotIMEISalesInfo_IMEI.DeleteByLotIMEISalesInfo(objIData, objLotIMEISalesInfo.LotIMEISalesInfoID, objLotIMEISalesInfo.UpdatedUser);
                if (objLotIMEISalesInfo.LotIMEISalesInfo_IMEIList != null)
                {
                    foreach (LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI in objLotIMEISalesInfo.LotIMEISalesInfo_IMEIList)
                    {
                        //if (objLotIMEISalesInfo_IMEI.IsExist)
                        //{
                        //    objLotIMEISalesInfo_IMEI.UpdatedUser = objLotIMEISalesInfo.UpdatedUser;
                        //    objDA_LotIMEISalesInfo_IMEI.Update(objIData, objLotIMEISalesInfo_IMEI);
                        //}         
                        //else
                        //{
                            objLotIMEISalesInfo_IMEI.LotIMEISalesInfoID = objLotIMEISalesInfo.LotIMEISalesInfoID;
                            objLotIMEISalesInfo_IMEI.CreatedUser = objLotIMEISalesInfo.UpdatedUser;
                            objDA_LotIMEISalesInfo_IMEI.Insert(objIData, objLotIMEISalesInfo_IMEI);
                       // }                       
                    }
                }
                DA_LotIMEISalesInfo_ProSpec objDA_LotIMEISalesInfo_ProSpec = new DA_LotIMEISalesInfo_ProSpec();
                objDA_LotIMEISalesInfo_ProSpec.DeleteByLotIMEISalesInfo(objIData, objLotIMEISalesInfo.LotIMEISalesInfoID);
                if (objLotIMEISalesInfo.LotIMEISalesInfo_ProSpecList != null)
                {
                    foreach (LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_ProSpec in objLotIMEISalesInfo.LotIMEISalesInfo_ProSpecList)
                    {
                        objLotIMEISalesInfo_ProSpec.LotIMEISalesInfoID = objLotIMEISalesInfo.LotIMEISalesInfoID;
                        objDA_LotIMEISalesInfo_ProSpec.objLogObject = objLogObject;
                        objDA_LotIMEISalesInfo_ProSpec.Insert(objIData, objLotIMEISalesInfo_ProSpec);
                    }
                }
                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
                objIData.RollBackTransaction();
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin thông tin bán hàng của 1 lô imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin thông tin bán hàng của 1 lô imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, LotIMEISalesInfo objLotIMEISalesInfo)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + LotIMEISalesInfo.colLotIMEISalesInfoID, objLotIMEISalesInfo.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo.colLotIMEISalesInfoName, objLotIMEISalesInfo.LotIMEISalesInfoName);
				objIData.AddParameter("@" + LotIMEISalesInfo.colProductID, objLotIMEISalesInfo.ProductID);
				objIData.AddParameter("@" + LotIMEISalesInfo.colDescription, objLotIMEISalesInfo.Description);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsBrandNewWarranty, objLotIMEISalesInfo.IsBrandNewWarranty);
				objIData.AddParameter("@" + LotIMEISalesInfo.colEndWarrantyDate, objLotIMEISalesInfo.EndWarrantyDate);
				objIData.AddParameter("@" + LotIMEISalesInfo.colNote, objLotIMEISalesInfo.Note);
				objIData.AddParameter("@" + LotIMEISalesInfo.colPrintVATContent, objLotIMEISalesInfo.PrintVATContent);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsConfirm, objLotIMEISalesInfo.IsConfirm);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsReviewed, objLotIMEISalesInfo.IsReviewed);
				objIData.AddParameter("@" + LotIMEISalesInfo.colReviewedUser, objLotIMEISalesInfo.ReviewedUser);
				objIData.AddParameter("@" + LotIMEISalesInfo.colReviewedDate, objLotIMEISalesInfo.ReviewedDate);
				objIData.AddParameter("@" + LotIMEISalesInfo.colWebProductSpecContent, objLotIMEISalesInfo.WebProductSpecContent);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsActive, objLotIMEISalesInfo.IsActive);
				objIData.AddParameter("@" + LotIMEISalesInfo.colIsSystem, objLotIMEISalesInfo.IsSystem);
				objIData.AddParameter("@" + LotIMEISalesInfo.colUpdatedUser, objLotIMEISalesInfo.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin thông tin bán hàng của 1 lô imei
		/// </summary>
		/// <param name="objLotIMEISalesInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(LotIMEISalesInfo objLotIMEISalesInfo)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
                DA_LotIMEISalesInfo_Images objDA_LotIMEISalesInfo_Images = new DA_LotIMEISalesInfo_Images();
                objDA_LotIMEISalesInfo_Images.DeleteByLotIMEISalesInfo(objIData, objLotIMEISalesInfo.LotIMEISalesInfoID, objLotIMEISalesInfo.UpdatedUser);
                DA_LotIMEISalesInfo_IMEI objDA_LotIMEISalesInfo_IMEI = new DA_LotIMEISalesInfo_IMEI();
                objDA_LotIMEISalesInfo_IMEI.DeleteByLotIMEISalesInfo(objIData, objLotIMEISalesInfo.LotIMEISalesInfoID, objLotIMEISalesInfo.UpdatedUser);
                DA_LotIMEISalesInfo_ProSpec objDA_LotIMEISalesInfo_ProSpec = new DA_LotIMEISalesInfo_ProSpec();
                objDA_LotIMEISalesInfo_ProSpec.objLogObject = objLogObject;
                objDA_LotIMEISalesInfo_ProSpec.DeleteByLotIMEISalesInfo(objIData, objLotIMEISalesInfo.LotIMEISalesInfoID);
                Delete(objIData, objLotIMEISalesInfo);
                objIData.CommitTransaction();
			}
			catch (Exception objEx)
            {
                objIData.RollBackTransaction();
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin thông tin bán hàng của 1 lô imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin thông tin bán hàng của 1 lô imei
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, LotIMEISalesInfo objLotIMEISalesInfo)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + LotIMEISalesInfo.colLotIMEISalesInfoID, objLotIMEISalesInfo.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo.colDeletedUser, objLotIMEISalesInfo.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_LotIMEISalesInfo()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_LOTIMEISALESINFO_ADD";
		public const String SP_UPDATE = "PM_LOTIMEISALESINFO_UPD";
		public const String SP_DELETE = "PM_LOTIMEISALESINFO_DEL";
		public const String SP_SELECT = "PM_LOTIMEISALESINFO_SEL";
		public const String SP_SEARCH = "PM_LOTIMEISALESINFO_SRH";
		public const String SP_UPDATEINDEX = "PM_LOTIMEISALESINFO_UPDINDEX";
		#endregion
		
	}
}
