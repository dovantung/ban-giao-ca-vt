﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using IMEIChange = ERP.Inventory.BO.PM.IMEIChange;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
    /// Created by 		: Đặng Minh Hùng 
    /// Created date 	: 02/07/2015 
    /// 
    /// </summary>
    public class DA_IMEIChange
    {
        public DA_IMEIChange()
        {
        }

        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        /// <summary>
        /// Thêm IMEI đổi
        /// </summary>
        public ResultMessage Insert(List<IMEIChange> lstIMEIChange, string strCreatedUser)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (var item in lstIMEIChange)
                {
                    item.CreatedUser = strCreatedUser;
                    Insert(objIData, item);
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thêm IMEI đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIChange -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private void Insert(IData objIData, IMEIChange objIMEIChange)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + IMEIChange.colOutputVoucherDetailID, objIMEIChange.OutputVoucherDetailID);
                objIData.AddParameter("@" + IMEIChange.colFromIMEI, objIMEIChange.FromIMEI);
                objIData.AddParameter("@" + IMEIChange.colToIMEI, objIMEIChange.ToIMEI);
                objIData.AddParameter("@" + IMEIChange.colIsSystem, objIMEIChange.IsSystem);
                objIData.AddParameter("@" + IMEIChange.colCreatedUser, objIMEIChange.CreatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage Delete(string strIMEIChangeID, string strDeletedUser)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_DEL);
                objIData.AddParameter("@IMEICHANGEID", strIMEIChangeID);
                objIData.AddParameter("@DeletedUser", strDeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi xóa IMEI đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIChange -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage SearchData(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm danh sách IMEI đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIChange -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetList(ref DataTable dtbData, string strOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_IMEICHANGE_GetByOV");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm danh sách IMEI chưa áp dụng đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIChange -> GetList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetIMEI(ref string strFromIMEI, string strToIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_IMEICHANGE_GETIMEI");
                objIData.AddParameter("@ToIMEI", strToIMEI);
                strFromIMEI = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi lấy IMEI cũ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIChange -> GetIMEI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public const String SP_ADD = "PM_IMEICHANGE_ADD";
        public const String SP_DEL = "PM_IMEICHANGE_DEL";
        public const String SP_SEARCH = "PM_IMEICHANGE_SRH";

    }
}
