
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using LotIMEISalesInfo = ERP.Inventory.BO.PM.LotIMEISalesInfo;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: CAO HỮU VŨ LAM 
	/// Created date 	: 4/2/2014 
	/// Thông tin bán hàng của 1 lô IMEI
	/// </summary>	
	public partial class DA_LotIMEISalesInfo
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin thông tin bán hàng của 1 lô imei
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_LotIMEISalesInfo.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin thông tin bán hàng của 1 lô imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// Duyệt thông tin thông tin bán hàng của 1 lô imei
        /// </summary>
        /// <param name="objLotIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Review(List<LotIMEISalesInfo> lstLotIMEISalesInfo)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                foreach (LotIMEISalesInfo objLotIMEISalesInfo in lstLotIMEISalesInfo)
                {
                    Review(objIData, objLotIMEISalesInfo);                     
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Others, "Lỗi duyệt thông tin thông tin bán hàng của 1 lô imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo -> Review", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Duyệt thông tin thông tin bán hàng của 1 lô imei
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objLotIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Review(IData objIData, LotIMEISalesInfo objLotIMEISalesInfo)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_LOTIMEISALESINFO_REV");
                objIData.AddParameter("@" + LotIMEISalesInfo.colLotIMEISalesInfoID, objLotIMEISalesInfo.LotIMEISalesInfoID);
                objIData.AddParameter("@" + LotIMEISalesInfo.colReviewedUser, objLotIMEISalesInfo.ReviewedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin thông tin bán hàng của 1 lô imei
        /// </summary>
        /// <param name="objLotIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(List<LotIMEISalesInfo> lstLotIMEISalesInfo)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                DA_LotIMEISalesInfo_Images objDA_LotIMEISalesInfo_Images = new DA_LotIMEISalesInfo_Images();
                DA_LotIMEISalesInfo_IMEI objDA_LotIMEISalesInfo_IMEI = new DA_LotIMEISalesInfo_IMEI();
                DA_LotIMEISalesInfo_ProSpec objDA_LotIMEISalesInfo_ProSpec = new DA_LotIMEISalesInfo_ProSpec();
                objDA_LotIMEISalesInfo_ProSpec.objLogObject = objLogObject;
                foreach (LotIMEISalesInfo objLotIMEISalesInfo in lstLotIMEISalesInfo)
                {
                    objDA_LotIMEISalesInfo_Images.DeleteByLotIMEISalesInfo(objIData, objLotIMEISalesInfo.LotIMEISalesInfoID, objLotIMEISalesInfo.UpdatedUser);
                    objDA_LotIMEISalesInfo_IMEI.DeleteByLotIMEISalesInfo(objIData, objLotIMEISalesInfo.LotIMEISalesInfoID, objLotIMEISalesInfo.UpdatedUser);
                    objDA_LotIMEISalesInfo_ProSpec.DeleteByLotIMEISalesInfo(objIData, objLotIMEISalesInfo.LotIMEISalesInfoID);
                    Delete(objIData, objLotIMEISalesInfo);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin thông tin danh sách bán hàng của 1 lô imei", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Cập nhật thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void UpdateOrderIndex(IData objIData, string strLotIMEISalesInfoID, int intOrderIndex)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_LOTIMEISALESINFO_UPDINDEX");
                objIData.AddParameter("@" + LotIMEISalesInfo.colLotIMEISalesInfoID, strLotIMEISalesInfoID);
                objIData.AddParameter("@" + LotIMEISalesInfo.colOrderIndex, intOrderIndex);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
		#endregion
		
		
	}
}
