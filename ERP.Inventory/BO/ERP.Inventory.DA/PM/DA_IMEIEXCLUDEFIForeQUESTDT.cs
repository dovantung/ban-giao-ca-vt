
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using IMEIExcludeFIFORequestDT = ERP.Inventory.BO.PM.IMEIExcludeFIFORequestDT;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 06/20/2018 
	/// Chi tiết yêu cầu IMEI không FIFO
	/// </summary>	
	public partial class DA_IMEIExcludeFIFORequestDT
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIMEIExcludeFIFORequestDT">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT, string strIMEIExcludeFIFORequestDTID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestDTID, strIMEIExcludeFIFORequestDTID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objIMEIExcludeFIFORequestDT = new IMEIExcludeFIFORequestDT();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestDTID])) objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestDTID = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestDTID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestID])) objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestID = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colProductID])) objIMEIExcludeFIFORequestDT.ProductID = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colIMEI])) objIMEIExcludeFIFORequestDT.IMEI = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colInStockStatusID])) objIMEIExcludeFIFORequestDT.InStockStatusID = Convert.ToInt32(reader[IMEIExcludeFIFORequestDT.colInStockStatusID]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colFirstInputDate])) objIMEIExcludeFIFORequestDT.FirstInputDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequestDT.colFirstInputDate]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colNote])) objIMEIExcludeFIFORequestDT.Note = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colCreatedUser])) objIMEIExcludeFIFORequestDT.CreatedUser = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colCreatedDate])) objIMEIExcludeFIFORequestDT.CreatedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequestDT.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colUpdatedUser])) objIMEIExcludeFIFORequestDT.UpdatedUser = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colUpdatedDate])) objIMEIExcludeFIFORequestDT.UpdatedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequestDT.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colIsDeleted])) objIMEIExcludeFIFORequestDT.IsDeleted = Convert.ToBoolean(reader[IMEIExcludeFIFORequestDT.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colDeletedUser])) objIMEIExcludeFIFORequestDT.DeletedUser = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colDeletedDate])) objIMEIExcludeFIFORequestDT.DeletedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequestDT.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colVoucherConcern])) objIMEIExcludeFIFORequestDT.VoucherConcern = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colVoucherConcern]).Trim();
 				}
 				else
 				{
 					objIMEIExcludeFIFORequestDT = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết yêu cầu imei không fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIExcludeFIFORequestDT -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIMEIExcludeFIFORequestDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objIMEIExcludeFIFORequestDT);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết yêu cầu imei không fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIExcludeFIFORequestDT -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEIExcludeFIFORequestDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestID, objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colProductID, objIMEIExcludeFIFORequestDT.ProductID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colIMEI, objIMEIExcludeFIFORequestDT.IMEI);
                objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colInStockStatusID, objIMEIExcludeFIFORequestDT.InStockStatusID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colFirstInputDate, objIMEIExcludeFIFORequestDT.FirstInputDate);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colNote, objIMEIExcludeFIFORequestDT.Note);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colCreatedUser, objIMEIExcludeFIFORequestDT.CreatedUser);
                objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colVoucherConcern, objIMEIExcludeFIFORequestDT.VoucherConcern);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIMEIExcludeFIFORequestDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objIMEIExcludeFIFORequestDT);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết yêu cầu imei không fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIExcludeFIFORequestDT -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEIExcludeFIFORequestDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestDTID, objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestDTID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestID, objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colProductID, objIMEIExcludeFIFORequestDT.ProductID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colIMEI, objIMEIExcludeFIFORequestDT.IMEI);
                objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colInStockStatusID, objIMEIExcludeFIFORequestDT.InStockStatusID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colFirstInputDate, objIMEIExcludeFIFORequestDT.FirstInputDate);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colNote, objIMEIExcludeFIFORequestDT.Note);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colUpdatedUser, objIMEIExcludeFIFORequestDT.UpdatedUser);
                objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colVoucherConcern, objIMEIExcludeFIFORequestDT.VoucherConcern);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIMEIExcludeFIFORequestDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objIMEIExcludeFIFORequestDT);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết yêu cầu imei không fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIExcludeFIFORequestDT -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEIExcludeFIFORequestDT">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestDTID, objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestDTID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequestDT.colDeletedUser, objIMEIExcludeFIFORequestDT.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_IMEIExcludeFIFORequestDT()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_IMEIExcludeFIFORequestDTADD";
		public const String SP_UPDATE = "PM_IMEIExcludeFIFORequestDTUPD";
		public const String SP_DELETE = "PM_IMEIExcludeFIFORequestDTDEL";
		public const String SP_SELECT = "PM_IMEIExcludeFIFORequestDTSEL";
		public const String SP_SEARCH = "PM_IMEIExcludeFIFORequestDTSRH";
		#endregion
		
	}
}
