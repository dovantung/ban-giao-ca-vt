
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using LotIMEISalesInfo_IMEI = ERP.Inventory.BO.PM.LotIMEISalesInfo_IMEI;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: CAO HỮU VŨ LAM 
	/// Created date 	: 4/2/2014 
	/// 
	/// </summary>	
	public partial class DA_LotIMEISalesInfo_IMEI
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin 
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_LotIMEISalesInfo_IMEI.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_IMEI -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objLotIMEISalesInfo_IMEI">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public List<LotIMEISalesInfo_IMEI> SearchDataToList(IData objIData, params object[] objKeywords)
        {
            List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = new List<LotIMEISalesInfo_IMEI>();
            try
            {
                objIData.CreateNewStoredProcedure(SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI = new LotIMEISalesInfo_IMEI();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colLotIMEISalesInfoID])) objLotIMEISalesInfo_IMEI.LotIMEISalesInfoID = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colLotIMEISalesInfoID]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colIMEI])) objLotIMEISalesInfo_IMEI.IMEI = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colIsSystem])) objLotIMEISalesInfo_IMEI.IsSystem = Convert.ToBoolean(reader[LotIMEISalesInfo_IMEI.colIsSystem]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colCreatedUser])) objLotIMEISalesInfo_IMEI.CreatedUser = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colCreatedDate])) objLotIMEISalesInfo_IMEI.CreatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_IMEI.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colUpdatedUser])) objLotIMEISalesInfo_IMEI.UpdatedUser = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colUpdatedDate])) objLotIMEISalesInfo_IMEI.UpdatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_IMEI.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colIsDeleted])) objLotIMEISalesInfo_IMEI.IsDeleted = Convert.ToBoolean(reader[LotIMEISalesInfo_IMEI.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colDeletedUser])) objLotIMEISalesInfo_IMEI.DeletedUser = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colDeletedDate])) objLotIMEISalesInfo_IMEI.DeletedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_IMEI.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colOrderIndex])) objLotIMEISalesInfo_IMEI.OrderIndex = Convert.ToInt32(reader[LotIMEISalesInfo_IMEI.colOrderIndex]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colIsOutput])) objLotIMEISalesInfo_IMEI.IsOutput = Convert.ToBoolean(reader[LotIMEISalesInfo_IMEI.colIsOutput]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colNote])) objLotIMEISalesInfo_IMEI.Note = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colNote]);
                    if (!Convert.IsDBNull(reader["SalePrice"])) objLotIMEISalesInfo_IMEI.SalePrice = Convert.ToDecimal(reader["SalePrice"]);
                    objLotIMEISalesInfo_IMEI.IsExist = true;
                    lstLotIMEISalesInfo_IMEI.Add(objLotIMEISalesInfo_IMEI);
                }  
                reader.Close();
            }
            catch (Exception objEx)
            {
                throw (objEx);
            }
            return lstLotIMEISalesInfo_IMEI;
        }
		#endregion
		
		
	}
}
