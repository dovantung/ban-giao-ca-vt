
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using LotIMEISalesInfo_ProSpec = ERP.Inventory.BO.PM.LotIMEISalesInfo_ProSpec;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: CAO HỮU VŨ LAM 
	/// Created date 	: 4/2/2014 
	/// 
	/// </summary>	
	public partial class DA_LotIMEISalesInfo_ProSpec
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_ProSpec">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_ProSpec, string strLotIMEISalesInfoID, int intProductSpecID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colLotIMEISalesInfoID, strLotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colProductSpecID, intProductSpecID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objLotIMEISalesInfo_ProSpec = new LotIMEISalesInfo_ProSpec();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_ProSpec.colLotIMEISalesInfoID])) objLotIMEISalesInfo_ProSpec.LotIMEISalesInfoID = Convert.ToString(reader[LotIMEISalesInfo_ProSpec.colLotIMEISalesInfoID]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_ProSpec.colProductSpecID])) objLotIMEISalesInfo_ProSpec.ProductSpecID = Convert.ToInt32(reader[LotIMEISalesInfo_ProSpec.colProductSpecID]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_ProSpec.colProductSpecStatusID])) objLotIMEISalesInfo_ProSpec.ProductSpecStatusID = Convert.ToInt32(reader[LotIMEISalesInfo_ProSpec.colProductSpecStatusID]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_ProSpec.colNote])) objLotIMEISalesInfo_ProSpec.Note = Convert.ToString(reader[LotIMEISalesInfo_ProSpec.colNote]).Trim();
                    objLotIMEISalesInfo_ProSpec.IsExist = true;
                }
 				else
 				{
                    objLotIMEISalesInfo_ProSpec = new LotIMEISalesInfo_ProSpec();
                    objLotIMEISalesInfo_ProSpec.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_PROSPEC -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_PROSPEC">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_PROSPEC)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objLotIMEISalesInfo_PROSPEC);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_PROSPEC -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo_PROSPEC">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_PROSPEC)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colLotIMEISalesInfoID, objLotIMEISalesInfo_PROSPEC.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colProductSpecID, objLotIMEISalesInfo_PROSPEC.ProductSpecID);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colProductSpecStatusID, objLotIMEISalesInfo_PROSPEC.ProductSpecStatusID);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colNote, objLotIMEISalesInfo_PROSPEC.Note);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_PROSPEC">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_PROSPEC)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objLotIMEISalesInfo_PROSPEC);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_PROSPEC -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo_PROSPEC">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_PROSPEC)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colLotIMEISalesInfoID, objLotIMEISalesInfo_PROSPEC.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colProductSpecID, objLotIMEISalesInfo_PROSPEC.ProductSpecID);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colProductSpecStatusID, objLotIMEISalesInfo_PROSPEC.ProductSpecStatusID);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colNote, objLotIMEISalesInfo_PROSPEC.Note);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_PROSPEC">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_PROSPEC)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objLotIMEISalesInfo_PROSPEC);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_PROSPEC -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo_PROSPEC">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_PROSPEC)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colLotIMEISalesInfoID, objLotIMEISalesInfo_PROSPEC.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colProductSpecID, objLotIMEISalesInfo_PROSPEC.ProductSpecID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objLotIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void DeleteByLotIMEISalesInfo(IData objIData, string strLotIMEISalesInfoID)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + LotIMEISalesInfo_ProSpec.colLotIMEISalesInfoID, strLotIMEISalesInfoID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
		#endregion

		
		#region Constructor

		public DA_LotIMEISalesInfo_ProSpec()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_LOTIMEISALESINFO_PRSPEC_ADD";
		public const String SP_UPDATE = "PM_LOTIMEISALESINFO_PRSPEC_UPD";
		public const String SP_DELETE = "PM_LOTIMEISALESINFO_PRSPEC_DEL";
		public const String SP_SELECT = "PM_LOTIMEISALESINFO_PRSPEC_SEL";
		public const String SP_SEARCH = "PM_LOTIMEISALESINFO_PRSPEC_SRH";
		public const String SP_UPDATEINDEX = "PM_LOTIMEISALESINFO_PRSPEC_UPDINDEX";
		#endregion
		
	}
}
