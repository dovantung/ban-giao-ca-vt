
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using LotIMEISalesInfo_Images = ERP.Inventory.BO.PM.LotIMEISalesInfo_Images;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: CAO HỮU VŨ LAM 
	/// Created date 	: 4/2/2014 
	/// 
	/// </summary>	
	public partial class DA_LotIMEISalesInfo_Images
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_ImageS">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref LotIMEISalesInfo_Images objLotIMEISalesInfo_ImageS, string strImageID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colImageID, strImageID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objLotIMEISalesInfo_ImageS = new LotIMEISalesInfo_Images();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colImageID])) objLotIMEISalesInfo_ImageS.ImageID = Convert.ToString(reader[LotIMEISalesInfo_Images.colImageID]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colLotIMEISalesInfoID])) objLotIMEISalesInfo_ImageS.LotIMEISalesInfoID = Convert.ToString(reader[LotIMEISalesInfo_Images.colLotIMEISalesInfoID]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colImageName])) objLotIMEISalesInfo_ImageS.ImageName = Convert.ToString(reader[LotIMEISalesInfo_Images.colImageName]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colDescription])) objLotIMEISalesInfo_ImageS.Description = Convert.ToString(reader[LotIMEISalesInfo_Images.colDescription]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colLargeSizeImage])) objLotIMEISalesInfo_ImageS.LargeSizeImage = Convert.ToString(reader[LotIMEISalesInfo_Images.colLargeSizeImage]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colMediumSizeImage])) objLotIMEISalesInfo_ImageS.MediumSizeImage = Convert.ToString(reader[LotIMEISalesInfo_Images.colMediumSizeImage]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colSmallSizeImage])) objLotIMEISalesInfo_ImageS.SmallSizeImage = Convert.ToString(reader[LotIMEISalesInfo_Images.colSmallSizeImage]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colLargeSizeImageFileID])) objLotIMEISalesInfo_ImageS.LargeSizeImageFileID = Convert.ToString(reader[LotIMEISalesInfo_Images.colLargeSizeImageFileID]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colMediumSizeImageFileID])) objLotIMEISalesInfo_ImageS.MediumSizeImageFileID = Convert.ToString(reader[LotIMEISalesInfo_Images.colMediumSizeImageFileID]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colSmallSizeImageFileID])) objLotIMEISalesInfo_ImageS.SmallSizeImageFileID = Convert.ToString(reader[LotIMEISalesInfo_Images.colSmallSizeImageFileID]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colIsDefault])) objLotIMEISalesInfo_ImageS.IsDefault = Convert.ToBoolean(reader[LotIMEISalesInfo_Images.colIsDefault]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colOrderIndex])) objLotIMEISalesInfo_ImageS.OrderIndex = Convert.ToInt32(reader[LotIMEISalesInfo_Images.colOrderIndex]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colIsActive])) objLotIMEISalesInfo_ImageS.IsActive = Convert.ToBoolean(reader[LotIMEISalesInfo_Images.colIsActive]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colIsSystem])) objLotIMEISalesInfo_ImageS.IsSystem = Convert.ToBoolean(reader[LotIMEISalesInfo_Images.colIsSystem]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colCreatedUser])) objLotIMEISalesInfo_ImageS.CreatedUser = Convert.ToString(reader[LotIMEISalesInfo_Images.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colCreatedDate])) objLotIMEISalesInfo_ImageS.CreatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Images.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colUpdatedUser])) objLotIMEISalesInfo_ImageS.UpdatedUser = Convert.ToString(reader[LotIMEISalesInfo_Images.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colUpdatedDate])) objLotIMEISalesInfo_ImageS.UpdatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Images.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colIsDeleted])) objLotIMEISalesInfo_ImageS.IsDeleted = Convert.ToBoolean(reader[LotIMEISalesInfo_Images.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colDeletedUser])) objLotIMEISalesInfo_ImageS.DeletedUser = Convert.ToString(reader[LotIMEISalesInfo_Images.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_Images.colDeletedDate])) objLotIMEISalesInfo_ImageS.DeletedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_Images.colDeletedDate]);
                    objLotIMEISalesInfo_ImageS.IsExist = true;
                }
 				else
 				{
                    objLotIMEISalesInfo_ImageS = new LotIMEISalesInfo_Images();
                    objLotIMEISalesInfo_ImageS.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_ImageS -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(LotIMEISalesInfo_Images objLotIMEISalesInfo_ImageS)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objLotIMEISalesInfo_ImageS);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_ImageS -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, LotIMEISalesInfo_Images objLotIMEISalesInfo_ImageS)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				//objIData.AddParameter("@" + LotIMEISalesInfo_Images.colImageID, objLotIMEISalesInfo_ImageS.ImageID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colLotIMEISalesInfoID, objLotIMEISalesInfo_ImageS.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colImageName, objLotIMEISalesInfo_ImageS.ImageName);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colDescription, objLotIMEISalesInfo_ImageS.Description);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colLargeSizeImage, objLotIMEISalesInfo_ImageS.LargeSizeImage);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colMediumSizeImage, objLotIMEISalesInfo_ImageS.MediumSizeImage);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colSmallSizeImage, objLotIMEISalesInfo_ImageS.SmallSizeImage);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colLargeSizeImageFileID, objLotIMEISalesInfo_ImageS.LargeSizeImageFileID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colMediumSizeImageFileID, objLotIMEISalesInfo_ImageS.MediumSizeImageFileID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colSmallSizeImageFileID, objLotIMEISalesInfo_ImageS.SmallSizeImageFileID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colIsDefault, objLotIMEISalesInfo_ImageS.IsDefault);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colOrderIndex, objLotIMEISalesInfo_ImageS.OrderIndex);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colIsActive, objLotIMEISalesInfo_ImageS.IsActive);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colIsSystem, objLotIMEISalesInfo_ImageS.IsSystem);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colCreatedUser, objLotIMEISalesInfo_ImageS.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <param name="lstOrderIndex">Danh sách OrderIndex</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(LotIMEISalesInfo_Images objLotIMEISalesInfo_ImageS, List<object> lstOrderIndex)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.BeginTransaction();
				Update(objIData, objLotIMEISalesInfo_ImageS);
				if (lstOrderIndex != null && lstOrderIndex.Count > 0)
				{
					for (int i = 0; i < lstOrderIndex.Count; i++)
					{
						UpdateOrderIndex(objIData, Convert.ToString(lstOrderIndex[i]), i);
					}
				}
				objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_ImageS -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, LotIMEISalesInfo_Images objLotIMEISalesInfo_ImageS)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colImageID, objLotIMEISalesInfo_ImageS.ImageID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colLotIMEISalesInfoID, objLotIMEISalesInfo_ImageS.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colImageName, objLotIMEISalesInfo_ImageS.ImageName);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colDescription, objLotIMEISalesInfo_ImageS.Description);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colLargeSizeImage, objLotIMEISalesInfo_ImageS.LargeSizeImage);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colMediumSizeImage, objLotIMEISalesInfo_ImageS.MediumSizeImage);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colSmallSizeImage, objLotIMEISalesInfo_ImageS.SmallSizeImage);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colLargeSizeImageFileID, objLotIMEISalesInfo_ImageS.LargeSizeImageFileID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colMediumSizeImageFileID, objLotIMEISalesInfo_ImageS.MediumSizeImageFileID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colSmallSizeImageFileID, objLotIMEISalesInfo_ImageS.SmallSizeImageFileID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colIsDefault, objLotIMEISalesInfo_ImageS.IsDefault);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colOrderIndex, objLotIMEISalesInfo_ImageS.OrderIndex);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colIsActive, objLotIMEISalesInfo_ImageS.IsActive);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colIsSystem, objLotIMEISalesInfo_ImageS.IsSystem);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colUpdatedUser, objLotIMEISalesInfo_ImageS.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="intOrderIndex">Thứ tự</param>
		/// <returns>Kết quả trả về</returns>
		public void UpdateOrderIndex(IData objIData, string strImageID, int intOrderIndex)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATEINDEX);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colImageID, strImageID);
                objIData.AddParameter("@OrderIndex", intOrderIndex);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(LotIMEISalesInfo_Images objLotIMEISalesInfo_ImageS)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objLotIMEISalesInfo_ImageS);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_ImageS -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, LotIMEISalesInfo_Images objLotIMEISalesInfo_ImageS)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colImageID, objLotIMEISalesInfo_ImageS.ImageID);
				objIData.AddParameter("@" + LotIMEISalesInfo_Images.colDeletedUser, objLotIMEISalesInfo_ImageS.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objLotIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void DeleteByLotIMEISalesInfo(IData objIData, string strLotIMEISalesInfoID, string strDeletedUser)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + LotIMEISalesInfo_Images.colLotIMEISalesInfoID, strLotIMEISalesInfoID);
                objIData.AddParameter("@" + LotIMEISalesInfo_Images.colDeletedUser, strDeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
		#endregion

		
		#region Constructor

		public DA_LotIMEISalesInfo_Images()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_LOTIMEISALESINFO_IMAGES_ADD";
		public const String SP_UPDATE = "PM_LOTIMEISALESINFO_IMAGES_UPD";
		public const String SP_DELETE = "PM_LOTIMEISALESINFO_IMAGES_DEL";
		public const String SP_SELECT = "PM_LOTIMEISALESINFO_IMAGES_SEL";
		public const String SP_SEARCH = "PM_LOTIMEISALESINFO_IMAGES_SRH";
		public const String SP_UPDATEINDEX = "PM_LOTIMEISALESINFO_IMAGES_UPDINDEX";
		#endregion
		
	}
}
