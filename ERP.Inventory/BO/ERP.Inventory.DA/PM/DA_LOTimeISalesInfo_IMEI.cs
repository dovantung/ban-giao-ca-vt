
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using LotIMEISalesInfo_IMEI = ERP.Inventory.BO.PM.LotIMEISalesInfo_IMEI;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: CAO HỮU VŨ LAM 
	/// Created date 	: 4/2/2014 
	/// 
	/// </summary>	
	public partial class DA_LotIMEISalesInfo_IMEI
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_IMEI">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI, string strIMEI, string strLotIMEISalesInfoID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colIMEI, strIMEI);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colLotIMEISalesInfoID, strLotIMEISalesInfoID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objLotIMEISalesInfo_IMEI = new LotIMEISalesInfo_IMEI();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colLotIMEISalesInfoID])) objLotIMEISalesInfo_IMEI.LotIMEISalesInfoID = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colLotIMEISalesInfoID]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colIMEI])) objLotIMEISalesInfo_IMEI.IMEI = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colIsSystem])) objLotIMEISalesInfo_IMEI.IsSystem = Convert.ToBoolean(reader[LotIMEISalesInfo_IMEI.colIsSystem]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colCreatedUser])) objLotIMEISalesInfo_IMEI.CreatedUser = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colCreatedDate])) objLotIMEISalesInfo_IMEI.CreatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_IMEI.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colUpdatedUser])) objLotIMEISalesInfo_IMEI.UpdatedUser = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colUpdatedDate])) objLotIMEISalesInfo_IMEI.UpdatedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_IMEI.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colIsDeleted])) objLotIMEISalesInfo_IMEI.IsDeleted = Convert.ToBoolean(reader[LotIMEISalesInfo_IMEI.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colDeletedUser])) objLotIMEISalesInfo_IMEI.DeletedUser = Convert.ToString(reader[LotIMEISalesInfo_IMEI.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[LotIMEISalesInfo_IMEI.colDeletedDate])) objLotIMEISalesInfo_IMEI.DeletedDate = Convert.ToDateTime(reader[LotIMEISalesInfo_IMEI.colDeletedDate]);
                    objLotIMEISalesInfo_IMEI.IsExist = true;
                }
 				else
 				{
                    objLotIMEISalesInfo_IMEI = new LotIMEISalesInfo_IMEI();
                    objLotIMEISalesInfo_IMEI.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_IMEI -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_IMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objLotIMEISalesInfo_IMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_IMEI -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo_IMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colLotIMEISalesInfoID, objLotIMEISalesInfo_IMEI.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colIMEI, objLotIMEISalesInfo_IMEI.IMEI);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colIsSystem, objLotIMEISalesInfo_IMEI.IsSystem);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colCreatedUser, objLotIMEISalesInfo_IMEI.CreatedUser);
                objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colIsOutput, objLotIMEISalesInfo_IMEI.IsOutput);
                objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colNote, objLotIMEISalesInfo_IMEI.Note);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_IMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objLotIMEISalesInfo_IMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_IMEI -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo_IMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colLotIMEISalesInfoID, objLotIMEISalesInfo_IMEI.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colIMEI, objLotIMEISalesInfo_IMEI.IMEI);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colIsSystem, objLotIMEISalesInfo_IMEI.IsSystem);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colUpdatedUser, objLotIMEISalesInfo_IMEI.UpdatedUser);
                objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colIsOutput, objLotIMEISalesInfo_IMEI.IsOutput);
                objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colNote, objLotIMEISalesInfo_IMEI.Note);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objLotIMEISalesInfo_IMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objLotIMEISalesInfo_IMEI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_IMEI -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objLotIMEISalesInfo_IMEI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colLotIMEISalesInfoID, objLotIMEISalesInfo_IMEI.LotIMEISalesInfoID);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colIMEI, objLotIMEISalesInfo_IMEI.IMEI);
				objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colDeletedUser, objLotIMEISalesInfo_IMEI.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objLotIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void DeleteByLotIMEISalesInfo(IData objIData, string strLotIMEISalesInfoID, string strDeletedUser)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colLotIMEISalesInfoID, strLotIMEISalesInfoID);
                objIData.AddParameter("@" + LotIMEISalesInfo_IMEI.colDeletedUser, strDeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
		#endregion

		
		#region Constructor

		public DA_LotIMEISalesInfo_IMEI()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_LOTIMEISALESINFO_IMEI_ADD";
		public const String SP_UPDATE = "PM_LOTIMEISALESINFO_IMEI_UPD";
		public const String SP_DELETE = "PM_LOTIMEISALESINFO_IMEI_DEL";
		public const String SP_SELECT = "PM_LOTIMEISALESINFO_IMEI_SEL";
		public const String SP_SEARCH = "PM_LOTIMEISALESINFO_IMEI_SRH";
		public const String SP_UPDATEINDEX = "PM_LOTIMEISALESINFO_IMEI_UPDINDEX";
		#endregion
		
	}
}
