
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO.PM;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 06/20/2018 
	/// Yêu cầu IMEI không FIFO
	/// </summary>	
	public partial class DA_IMEIExcludeFIFORequest
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin yêu cầu imei không fifo
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_IMEIExcludeFIFORequest.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin yêu cầu imei không fifo", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIEXCLUDEFIForeQUEST -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public ResultMessage GetIMEIExcludeFIFORequestNewID(ref string strIMEIExcludeFIFORequestID, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("IMEIEXCLUDEFIFOREQUEST_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strIMEIExcludeFIFORequestID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi lấy mã yêu cầu IMEI vi phạm FIFO", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIExcludeFIFORequestDT -> GetIMEIExcludeFIFORequestNewID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

		#endregion
		
		
	}
}
