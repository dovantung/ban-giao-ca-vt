
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using LotIMEISalesInfo_ProSpec = ERP.Inventory.BO.PM.LotIMEISalesInfo_ProSpec;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: CAO HỮU VŨ LAM 
	/// Created date 	: 4/2/2014 
	/// 
	/// </summary>	
	public partial class DA_LotIMEISalesInfo_ProSpec
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin 
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_LotIMEISalesInfo_ProSpec.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_PROSPEC -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// Tìm kiếm thông tin 
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataToList(ref List<LotIMEISalesInfo_ProSpec> lstLotIMEISalesInfo_ProSpec, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                lstLotIMEISalesInfo_ProSpec = SearchDataToList(objIData, objKeywords);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_PROSPEC -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objLotIMEISalesInfo_ProSpec">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public List<LotIMEISalesInfo_ProSpec> SearchDataToList(IData objIData, params object[] objKeywords)
        {
            List<LotIMEISalesInfo_ProSpec> lstLotIMEISalesInfo_ProSpec = new List<LotIMEISalesInfo_ProSpec>();
            try
            {
                objIData.CreateNewStoredProcedure(SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_ProSpec = new LotIMEISalesInfo_ProSpec();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_ProSpec.colLotIMEISalesInfoID])) objLotIMEISalesInfo_ProSpec.LotIMEISalesInfoID = Convert.ToString(reader[LotIMEISalesInfo_ProSpec.colLotIMEISalesInfoID]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_ProSpec.colProductSpecID])) objLotIMEISalesInfo_ProSpec.ProductSpecID = Convert.ToInt32(reader[LotIMEISalesInfo_ProSpec.colProductSpecID]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_ProSpec.colProductSpecStatusID])) objLotIMEISalesInfo_ProSpec.ProductSpecStatusID = Convert.ToInt32(reader[LotIMEISalesInfo_ProSpec.colProductSpecStatusID]);
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_ProSpec.colNote])) objLotIMEISalesInfo_ProSpec.Note = Convert.ToString(reader[LotIMEISalesInfo_ProSpec.colNote]).Trim();
                    if (!Convert.IsDBNull(reader[LotIMEISalesInfo_ProSpec.colProductSpecName])) objLotIMEISalesInfo_ProSpec.ProductSpecName = Convert.ToString(reader[LotIMEISalesInfo_ProSpec.colProductSpecName]).Trim();
                    objLotIMEISalesInfo_ProSpec.IsExist = true;
                    lstLotIMEISalesInfo_ProSpec.Add(objLotIMEISalesInfo_ProSpec);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
               throw(objEx);
            }
            return lstLotIMEISalesInfo_ProSpec;
        }
		#endregion
		
		
	}
}
