﻿
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO.PM;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
    /// Created by 		: Trần Thị Mi 
    /// Created date 	: 01/12/2014 
    /// 
    /// </summary>	
    public partial class DA_LotIMEISalesInfo_Comment
    {

        #region Methods

        /// <summary>
        /// Tìm kiếm thông tin 
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_LotIMEISalesInfo_Comment.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_Comment -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objLotIMEISalesInfo_Comment">Đối tượng truyền vào</param>
        /// <param name="lstOrderIndex">Danh sách OrderIndex</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage UpdateData(LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment_Update, LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment_Insert)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                if (objLotIMEISalesInfo_Comment_Update != null)
                    Update(objIData, objLotIMEISalesInfo_Comment_Update);
                if (objLotIMEISalesInfo_Comment_Insert != null)
                    Insert(objIData, objLotIMEISalesInfo_Comment_Insert);
                //if (lstOrderIndex != null && lstOrderIndex.Count > 0)
                //{
                //    for (int i = 0; i < lstOrderIndex.Count; i++)
                //    {
                //        UpdateOrderIndex(objIData, Convert.ToInt32(lstOrderIndex[i]), i);
                //    }
                //}
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_LotIMEISalesInfo_Comment -> UpdateData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        
        #endregion


    }
}


