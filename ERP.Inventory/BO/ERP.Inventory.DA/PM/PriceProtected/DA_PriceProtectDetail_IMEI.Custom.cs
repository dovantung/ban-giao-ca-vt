﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ERP.Inventory.BO.PM.PriceProtected;
using System.Data;
using Library.DataAccess;
using Library.WebCore;

namespace ERP.Inventory.DA.PM.PriceProtected
{
    public partial class DA_PriceProtectDetail_IMEI
    {
        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="lstPriceProtectDetail_IMEI">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public List<PriceProtectDetail_IMEI> SearchDataToList(IData objIData, params object[] objKeywords)
        {
            List<PriceProtectDetail_IMEI> lstPriceProtectDetail_IMEI = new List<PriceProtectDetail_IMEI>();
            try
            {
                objIData.CreateNewStoredProcedure("PM_PRICEPROTECTDETAIL_IMEL_SRH");
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    PriceProtectDetail_IMEI objPriceProtectDetail_IMEI = new PriceProtectDetail_IMEI();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colPriceProtectDetailID])) objPriceProtectDetail_IMEI.PriceProtectDetailID = Convert.ToString(reader[PriceProtectDetail_IMEI.colPriceProtectDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colProductIMEI])) objPriceProtectDetail_IMEI.ProductIMEI = Convert.ToString(reader[PriceProtectDetail_IMEI.colProductIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colProductID])) objPriceProtectDetail_IMEI.ProductID = Convert.ToString(reader[PriceProtectDetail_IMEI.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colInputVoucherID])) objPriceProtectDetail_IMEI.InputVoucherID = Convert.ToString(reader[PriceProtectDetail_IMEI.colInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colInputDate])) objPriceProtectDetail_IMEI.InputDate = Convert.ToDateTime(reader[PriceProtectDetail_IMEI.colInputDate]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colAllocateTypeID])) objPriceProtectDetail_IMEI.AllocateTypeID = Convert.ToInt32(reader[PriceProtectDetail_IMEI.colAllocateTypeID]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colStoreID])) objPriceProtectDetail_IMEI.StoreID = Convert.ToInt32(reader[PriceProtectDetail_IMEI.colStoreID]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colStoreName])) objPriceProtectDetail_IMEI.StoreName = Convert.ToString(reader[PriceProtectDetail_IMEI.colStoreName]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colOriginalCostPrice])) objPriceProtectDetail_IMEI.OriginalCostPrice = Convert.ToDecimal(reader[PriceProtectDetail_IMEI.colOriginalCostPrice]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colAdjustCostPrice])) objPriceProtectDetail_IMEI.AdjustCostPrice = Convert.ToDecimal(reader[PriceProtectDetail_IMEI.colAdjustCostPrice]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colCostPrice])) objPriceProtectDetail_IMEI.CostPrice = Convert.ToDecimal(reader[PriceProtectDetail_IMEI.colCostPrice]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colLastInputVoucherDetailID])) objPriceProtectDetail_IMEI.LastInputVoucherDetailID = Convert.ToString(reader[PriceProtectDetail_IMEI.colLastInputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colInputVoucherDetailID])) objPriceProtectDetail_IMEI.InputVoucherDetailID = Convert.ToString(reader[PriceProtectDetail_IMEI.colInputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colOutputVoucherDetailID])) objPriceProtectDetail_IMEI.OutputVoucherDetailID = Convert.ToString(reader[PriceProtectDetail_IMEI.colOutputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colIsCenterBranch])) objPriceProtectDetail_IMEI.IsCenterBranch = Convert.ToBoolean(reader[PriceProtectDetail_IMEI.colIsCenterBranch]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colAllocateDate])) objPriceProtectDetail_IMEI.AllocateDate = Convert.ToDateTime(reader[PriceProtectDetail_IMEI.colAllocateDate]);


                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colNumOfAllocate])) objPriceProtectDetail_IMEI.NumOfAllocate = Convert.ToInt32(reader[PriceProtectDetail_IMEI.colNumOfAllocate]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colVATInvoiceDTID1])) objPriceProtectDetail_IMEI.VATInvoiceDTID1 = Convert.ToString(reader[PriceProtectDetail_IMEI.colVATInvoiceDTID1]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colInputVoucherDetailID1])) objPriceProtectDetail_IMEI.InputVoucherDetailID1 = Convert.ToString(reader[PriceProtectDetail_IMEI.colInputVoucherDetailID1]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colOutputVoucherDetailID1])) objPriceProtectDetail_IMEI.OutputVoucherDetailID1 = Convert.ToString(reader[PriceProtectDetail_IMEI.colOutputVoucherDetailID1]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colAdjustCostPrice1])) objPriceProtectDetail_IMEI.AdjustCostPrice1 = Convert.ToDecimal(reader[PriceProtectDetail_IMEI.colAdjustCostPrice1]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_IMEI.colCostPrice1])) objPriceProtectDetail_IMEI.CostPrice1 = Convert.ToDecimal(reader[PriceProtectDetail_IMEI.colCostPrice1]);


                    objPriceProtectDetail_IMEI.IsValid = true;
                    lstPriceProtectDetail_IMEI.Add(objPriceProtectDetail_IMEI);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                throw (objEx);
            }
            return lstPriceProtectDetail_IMEI;
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objPriceProtectDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void UpdateAllocate(IData objIData, PriceProtectDetail_IMEI objPriceProtectDetail_IMEI)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_PRICEPROTECTDT_IMEL_UPDALC");
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colProductIMEI, objPriceProtectDetail_IMEI.ProductIMEI);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colPriceProtectDetailID, objPriceProtectDetail_IMEI.PriceProtectDetailID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colProductID, objPriceProtectDetail_IMEI.ProductID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colAllocateTypeID, objPriceProtectDetail_IMEI.AllocateTypeID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colStoreID, objPriceProtectDetail_IMEI.StoreID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colOriginalCostPrice, objPriceProtectDetail_IMEI.OriginalCostPrice);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colAdjustCostPrice, objPriceProtectDetail_IMEI.AdjustCostPrice);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colCostPrice, objPriceProtectDetail_IMEI.CostPrice);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colLastInputVoucherDetailID, objPriceProtectDetail_IMEI.LastInputVoucherDetailID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
    }
}
