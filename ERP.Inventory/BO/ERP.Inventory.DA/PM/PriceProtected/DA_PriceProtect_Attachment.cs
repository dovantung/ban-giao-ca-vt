
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using PriceProtect_Attachment = ERP.Inventory.BO.PM.PriceProtected.PriceProtect_Attachment;
#endregion
namespace ERP.Inventory.DA.PM.PriceProtected
{
    /// <summary>
	/// Created by 		: LE VAN DONG 
	/// Created date 	: 7/6/2018 
	/// Danh sách file đính kèm của phiếu hỗ trợ giá
	/// </summary>	
	public partial class DA_PriceProtect_Attachment
	{	

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách file đính kèm của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objPriceProtect_Attachment">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref PriceProtect_Attachment objPriceProtect_Attachment, string strAttachmentID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + PriceProtect_Attachment.colAttachmentID, strAttachmentID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objPriceProtect_Attachment = new PriceProtect_Attachment();
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colAttachmentID])) objPriceProtect_Attachment.AttachmentID = Convert.ToString(reader[PriceProtect_Attachment.colAttachmentID]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colPriceProtectID])) objPriceProtect_Attachment.PriceProtectID = Convert.ToString(reader[PriceProtect_Attachment.colPriceProtectID]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colDescription])) objPriceProtect_Attachment.Description = Convert.ToString(reader[PriceProtect_Attachment.colDescription]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colFilePath])) objPriceProtect_Attachment.FilePath = Convert.ToString(reader[PriceProtect_Attachment.colFilePath]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colFileName])) objPriceProtect_Attachment.FileName = Convert.ToString(reader[PriceProtect_Attachment.colFileName]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colCreatedUser])) objPriceProtect_Attachment.CreatedUser = Convert.ToString(reader[PriceProtect_Attachment.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colCreatedUserName])) objPriceProtect_Attachment.CreatedUserName = Convert.ToString(reader[PriceProtect_Attachment.colCreatedUserName]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colCreatedDate])) objPriceProtect_Attachment.CreatedDate = Convert.ToDateTime(reader[PriceProtect_Attachment.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colIsDeleted])) objPriceProtect_Attachment.IsDeleted = Convert.ToBoolean(reader[PriceProtect_Attachment.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colDeletedUser])) objPriceProtect_Attachment.DeletedUser = Convert.ToString(reader[PriceProtect_Attachment.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colDeletedDate])) objPriceProtect_Attachment.DeletedDate = Convert.ToDateTime(reader[PriceProtect_Attachment.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colFileID])) objPriceProtect_Attachment.FileID = Convert.ToString(reader[PriceProtect_Attachment.colFileID]).Trim();
 				}
 				else
 				{
 					objPriceProtect_Attachment = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách file đính kèm của phiếu hỗ trợ giá", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Attachment -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách file đính kèm của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objPriceProtect_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(PriceProtect_Attachment objPriceProtect_Attachment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objPriceProtect_Attachment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách file đính kèm của phiếu hỗ trợ giá", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Attachment -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách file đính kèm của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objPriceProtect_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public string Insert(IData objIData, PriceProtect_Attachment objPriceProtect_Attachment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + PriceProtect_Attachment.colPriceProtectID, objPriceProtect_Attachment.PriceProtectID);
				objIData.AddParameter("@" + PriceProtect_Attachment.colDescription, objPriceProtect_Attachment.Description);
				objIData.AddParameter("@" + PriceProtect_Attachment.colFilePath, objPriceProtect_Attachment.FilePath);
				objIData.AddParameter("@" + PriceProtect_Attachment.colFileName, objPriceProtect_Attachment.FileName);
				objIData.AddParameter("@" + PriceProtect_Attachment.colCreatedUser, objPriceProtect_Attachment.CreatedUser);
				objIData.AddParameter("@" + PriceProtect_Attachment.colFileID, objPriceProtect_Attachment.FileID);
                return objIData.ExecStoreToString();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin danh sách file đính kèm của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objPriceProtect_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(PriceProtect_Attachment objPriceProtect_Attachment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objPriceProtect_Attachment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách file đính kèm của phiếu hỗ trợ giá", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Attachment -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách file đính kèm của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objPriceProtect_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, PriceProtect_Attachment objPriceProtect_Attachment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + PriceProtect_Attachment.colAttachmentID, objPriceProtect_Attachment.AttachmentID);
				objIData.AddParameter("@" + PriceProtect_Attachment.colPriceProtectID, objPriceProtect_Attachment.PriceProtectID);
				objIData.AddParameter("@" + PriceProtect_Attachment.colDescription, objPriceProtect_Attachment.Description);
				objIData.AddParameter("@" + PriceProtect_Attachment.colFilePath, objPriceProtect_Attachment.FilePath);
				objIData.AddParameter("@" + PriceProtect_Attachment.colFileName, objPriceProtect_Attachment.FileName);
				objIData.AddParameter("@" + PriceProtect_Attachment.colFileID, objPriceProtect_Attachment.FileID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin danh sách file đính kèm của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objPriceProtect_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(PriceProtect_Attachment objPriceProtect_Attachment)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objPriceProtect_Attachment);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách file đính kèm của phiếu hỗ trợ giá", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Attachment -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách file đính kèm của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objPriceProtect_Attachment">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, PriceProtect_Attachment objPriceProtect_Attachment)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + PriceProtect_Attachment.colAttachmentID, objPriceProtect_Attachment.AttachmentID);
				objIData.AddParameter("@" + PriceProtect_Attachment.colDeletedUser, objPriceProtect_Attachment.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        public ResultMessage SearchDataToList(ref List<PriceProtect_Attachment> lstPriceProtect_Attachment, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            if (lstPriceProtect_Attachment == null)
                lstPriceProtect_Attachment = new List<PriceProtect_Attachment>();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_PriceProtect_Attachment.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    PriceProtect_Attachment objPriceProtect_Attachment = new PriceProtect_Attachment();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colAttachmentID])) objPriceProtect_Attachment.AttachmentID = Convert.ToString(reader[PriceProtect_Attachment.colAttachmentID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colPriceProtectID])) objPriceProtect_Attachment.PriceProtectID = Convert.ToString(reader[PriceProtect_Attachment.colPriceProtectID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colDescription])) objPriceProtect_Attachment.Description = Convert.ToString(reader[PriceProtect_Attachment.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colFilePath])) objPriceProtect_Attachment.FilePath = Convert.ToString(reader[PriceProtect_Attachment.colFilePath]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colFileName])) objPriceProtect_Attachment.FileName = Convert.ToString(reader[PriceProtect_Attachment.colFileName]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colCreatedUser])) objPriceProtect_Attachment.CreatedUser = Convert.ToString(reader[PriceProtect_Attachment.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colCreatedUserName])) objPriceProtect_Attachment.CreatedUserName = Convert.ToString(reader[PriceProtect_Attachment.colCreatedUserName]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colCreatedDate])) objPriceProtect_Attachment.CreatedDate = Convert.ToDateTime(reader[PriceProtect_Attachment.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colIsDeleted])) objPriceProtect_Attachment.IsDeleted = Convert.ToBoolean(reader[PriceProtect_Attachment.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colDeletedUser])) objPriceProtect_Attachment.DeletedUser = Convert.ToString(reader[PriceProtect_Attachment.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colDeletedDate])) objPriceProtect_Attachment.DeletedDate = Convert.ToDateTime(reader[PriceProtect_Attachment.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect_Attachment.colFileID])) objPriceProtect_Attachment.FileID = Convert.ToString(reader[PriceProtect_Attachment.colFileID]).Trim();
                    lstPriceProtect_Attachment.Add(objPriceProtect_Attachment);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi tìm kiếm thông tin tập tin đính kèm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Attachment -> SearchDataToList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion


        #region Constructor

        public DA_PriceProtect_Attachment()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_PRICEPROTECT_ATTACHMENT_ADD";
		public const String SP_UPDATE = "PM_PRICEProtect_ATTACHment_UPD";
		public const String SP_DELETE = "PM_PRICEProtect_ATTACHment_DEL";
		public const String SP_SELECT = "PM_PRICEProtect_ATTACHment_SEL";
		public const String SP_SEARCH = "PM_PRICEProtect_ATTACHment_SRH";
		public const String SP_UPDATEINDEX = "PM_PRICEProtect_ATTACHment_UPDINDEX";
		#endregion
		
	}
}
