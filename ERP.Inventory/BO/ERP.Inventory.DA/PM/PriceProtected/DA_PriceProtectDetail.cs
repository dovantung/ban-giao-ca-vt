﻿using ERP.Inventory.BO.PM.PriceProtected;
using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.PM.PriceProtected
{
    public partial class DA_PriceProtectDetail
    {
        public DA_PriceProtectDetail()
        {
        }

        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        public ResultMessage Insert(PriceProtectDetail objPriProtectDetail, string strPriceProtectID, ref string strPriceProtectDetailID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objPriProtectDetail, strPriceProtectID, ref strPriceProtectDetailID);
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thêm Chi tiết biên bản hỗ trợ hàng tồn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtectDetail -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void Insert(IData objIData, PriceProtectDetail objPriceProtectDetail, string strPriceProtectID,ref string strPriceProtectDetailID)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + PriceProtectDetail.colPriceProtectID, strPriceProtectID);
                objIData.AddParameter("@" + PriceProtectDetail.colProductID, objPriceProtectDetail.ProductID);
                objIData.AddParameter("@" + PriceProtectDetail.colProductName, objPriceProtectDetail.ProductName);
                objIData.AddParameter("@" + PriceProtectDetail.colQuantity, objPriceProtectDetail.Quantity);
                objIData.AddParameter("@" + PriceProtectDetail.colInStockQuantity, objPriceProtectDetail.InStockQuantity);
                objIData.AddParameter("@" + PriceProtectDetail.colVat, objPriceProtectDetail.Vat);
                objIData.AddParameter("@" + PriceProtectDetail.colVatPercent, objPriceProtectDetail.VatPercent);
                objIData.AddParameter("@" + PriceProtectDetail.colVatTypeID, objPriceProtectDetail.VatTypeID);
                objIData.AddParameter("@" + PriceProtectDetail.colPrice, objPriceProtectDetail.Price);
                objIData.AddParameter("@" + PriceProtectDetail.colPriceNoVat, objPriceProtectDetail.PriceNoVat);
                objIData.AddParameter("@" + PriceProtectDetail.colTotalAmountNoVat, objPriceProtectDetail.TotalAmountNoVat);
                objIData.AddParameter("@" + PriceProtectDetail.colTotalAmount, objPriceProtectDetail.TotalAmount);

                objIData.AddParameter("@" + PriceProtectDetail.colCreatedUser, objPriceProtectDetail.CreatedUser);
                objIData.AddParameter("@" + PriceProtectDetail.colInputVoucherID, objPriceProtectDetail.InputVoucherID);
                objIData.AddParameter("@" + PriceProtectDetail.colInVoiceID, objPriceProtectDetail.InVoiceID);
                
                strPriceProtectDetailID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objPriceProtectDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(PriceProtectDetail objPriceProtectDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objPriceProtectDetail, false);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStock -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objPriceProtectDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, PriceProtectDetail objPriceProtectDetail, bool bolIsHasAllocateByPostdate)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + PriceProtectDetail.colPriceProtectDetailID, objPriceProtectDetail.PriceProtectDetailID);
                objIData.AddParameter("@" + PriceProtectDetail.colPriceProtectID, objPriceProtectDetail.PriceProtectID);
                objIData.AddParameter("@" + PriceProtectDetail.colProductID, objPriceProtectDetail.ProductID);
                objIData.AddParameter("@" + PriceProtectDetail.colProductName, objPriceProtectDetail.ProductName);
                objIData.AddParameter("@" + PriceProtectDetail.colQuantity, objPriceProtectDetail.Quantity);
                objIData.AddParameter("@" + PriceProtectDetail.colInStockQuantity, objPriceProtectDetail.InStockQuantity);
                objIData.AddParameter("@" + PriceProtectDetail.colVat, objPriceProtectDetail.Vat);
                objIData.AddParameter("@" + PriceProtectDetail.colVatPercent, objPriceProtectDetail.VatPercent);
                objIData.AddParameter("@" + PriceProtectDetail.colVatTypeID, objPriceProtectDetail.VatTypeID);
                objIData.AddParameter("@" + PriceProtectDetail.colPrice, objPriceProtectDetail.Price);
                objIData.AddParameter("@" + PriceProtectDetail.colPriceNoVat, objPriceProtectDetail.PriceNoVat);
                objIData.AddParameter("@" + PriceProtectDetail.colTotalAmountNoVat, objPriceProtectDetail.TotalAmountNoVat);
                objIData.AddParameter("@" + PriceProtectDetail.colTotalAmount, objPriceProtectDetail.TotalAmount);
                
                objIData.AddParameter("@" + PriceProtectDetail.colInputVoucherID, objPriceProtectDetail.InputVoucherID);
                objIData.AddParameter("@" + PriceProtectDetail.colInVoiceID, objPriceProtectDetail.InVoiceID);

                if(bolIsHasAllocateByPostdate)
                {
                    objIData.AddParameter("@" + PriceProtectDetail.colPriceVoucher, objPriceProtectDetail.PriceVoucher);
                    objIData.AddParameter("@" + PriceProtectDetail.colPriceNoVatVoucher, objPriceProtectDetail.PriceNoVatVoucher);
                    objIData.AddParameter("@" + PriceProtectDetail.colTotalAmountNoVatVoucher, objPriceProtectDetail.TotalAmountNoVatVoucher);
                    objIData.AddParameter("@" + PriceProtectDetail.colTotalAmountVoucher, objPriceProtectDetail.TotalAmountVoucher);
                }

                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public const String SP_ADD = "PM_PRICEPROTECTDETAIL_ADD";
        public const String SP_DEL = "PM_PRICEPROTECTDETAIL_DEL";
        public const String SP_SEARCH = "PM_PRICEPROTECTDETAIL_SRH";
        public const String SP_SELECT = "PM_PRICEPROTECTDETAIL_SEL";
        public const String SP_UPDATE = "PM_PRICEPROTECTDETAIL_UPD";
    }
}
