
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using PriceProtect_Customer = ERP.Inventory.BO.PM.PriceProtected.PriceProtect_Customer;
#endregion
namespace ERP.Inventory.DA.PM.PriceProtected
{
    /// <summary>
	/// Created by 		: LE VAN DONG 
	/// Created date 	: 7/6/2018 
	/// Danh sách nhà cung cấp của phiếu hỗ trợ giá
	/// </summary>	
	public partial class DA_PriceProtect_Customer
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objPriceProtect_Customer">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref PriceProtect_Customer objPriceProtect_Customer, int intCustomerID, string strPriceProtectID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerID, intCustomerID);
				objIData.AddParameter("@" + PriceProtect_Customer.colPriceProtectID, strPriceProtectID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objPriceProtect_Customer = new PriceProtect_Customer();
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerID])) objPriceProtect_Customer.CustomerID = Convert.ToInt32(reader[PriceProtect_Customer.colCustomerID]);
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colPriceProtectID])) objPriceProtect_Customer.PriceProtectID = Convert.ToString(reader[PriceProtect_Customer.colPriceProtectID]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colIsDefault])) objPriceProtect_Customer.IsDefault = Convert.ToBoolean(reader[PriceProtect_Customer.colIsDefault]);
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCreatedUser])) objPriceProtect_Customer.CreatedUser = Convert.ToString(reader[PriceProtect_Customer.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCreatedDate])) objPriceProtect_Customer.CreatedDate = Convert.ToDateTime(reader[PriceProtect_Customer.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colUpdatedUser])) objPriceProtect_Customer.UpdatedUser = Convert.ToString(reader[PriceProtect_Customer.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colUpdatedDate])) objPriceProtect_Customer.UpdatedDate = Convert.ToDateTime(reader[PriceProtect_Customer.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colIsDeleted])) objPriceProtect_Customer.IsDeleted = Convert.ToBoolean(reader[PriceProtect_Customer.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colDeletedUser])) objPriceProtect_Customer.DeletedUser = Convert.ToString(reader[PriceProtect_Customer.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colDeletedDate])) objPriceProtect_Customer.DeletedDate = Convert.ToDateTime(reader[PriceProtect_Customer.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerName])) objPriceProtect_Customer.CustomerName = Convert.ToString(reader[PriceProtect_Customer.colCustomerName]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerAddress])) objPriceProtect_Customer.CustomerAddress = Convert.ToString(reader[PriceProtect_Customer.colCustomerAddress]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerTaxID])) objPriceProtect_Customer.CustomerTaxID = Convert.ToString(reader[PriceProtect_Customer.colCustomerTaxID]).Trim();
 					if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerPhone])) objPriceProtect_Customer.CustomerPhone = Convert.ToString(reader[PriceProtect_Customer.colCustomerPhone]).Trim();
 				}
 				else
 				{
 					objPriceProtect_Customer = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Customer -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objPriceProtect_Customer">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(PriceProtect_Customer objPriceProtect_Customer)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objPriceProtect_Customer);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Customer -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objPriceProtect_Customer">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, PriceProtect_Customer objPriceProtect_Customer)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerID, objPriceProtect_Customer.CustomerID);
				objIData.AddParameter("@" + PriceProtect_Customer.colPriceProtectID, objPriceProtect_Customer.PriceProtectID);
				objIData.AddParameter("@" + PriceProtect_Customer.colIsDefault, objPriceProtect_Customer.IsDefault);
				objIData.AddParameter("@" + PriceProtect_Customer.colCreatedUser, objPriceProtect_Customer.CreatedUser);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerName, objPriceProtect_Customer.CustomerName);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerAddress, objPriceProtect_Customer.CustomerAddress);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerTaxID, objPriceProtect_Customer.CustomerTaxID);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerPhone, objPriceProtect_Customer.CustomerPhone);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objPriceProtect_Customer">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(PriceProtect_Customer objPriceProtect_Customer)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objPriceProtect_Customer);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Customer -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objPriceProtect_Customer">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, PriceProtect_Customer objPriceProtect_Customer)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerID, objPriceProtect_Customer.CustomerID);
				objIData.AddParameter("@" + PriceProtect_Customer.colPriceProtectID, objPriceProtect_Customer.PriceProtectID);
				objIData.AddParameter("@" + PriceProtect_Customer.colIsDefault, objPriceProtect_Customer.IsDefault);
				objIData.AddParameter("@" + PriceProtect_Customer.colUpdatedUser, objPriceProtect_Customer.UpdatedUser);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerName, objPriceProtect_Customer.CustomerName);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerAddress, objPriceProtect_Customer.CustomerAddress);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerTaxID, objPriceProtect_Customer.CustomerTaxID);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerPhone, objPriceProtect_Customer.CustomerPhone);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objPriceProtect_Customer">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(PriceProtect_Customer objPriceProtect_Customer)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objPriceProtect_Customer);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Customer -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách nhà cung cấp của phiếu hỗ trợ giá
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objPriceProtect_Customer">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, PriceProtect_Customer objPriceProtect_Customer)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + PriceProtect_Customer.colCustomerID, objPriceProtect_Customer.CustomerID);
				objIData.AddParameter("@" + PriceProtect_Customer.colPriceProtectID, objPriceProtect_Customer.PriceProtectID);
				objIData.AddParameter("@" + PriceProtect_Customer.colDeletedUser, objPriceProtect_Customer.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


        public ResultMessage SearchDataToList(ref List<PriceProtect_Customer> lstPriceProtect_Customer, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            if (lstPriceProtect_Customer == null)
                lstPriceProtect_Customer = new List<PriceProtect_Customer>();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_PriceProtect_Customer.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    PriceProtect_Customer objPriceProtect_Customer = new PriceProtect_Customer();
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerID])) objPriceProtect_Customer.CustomerID = Convert.ToInt32(reader[PriceProtect_Customer.colCustomerID]);
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colPriceProtectID])) objPriceProtect_Customer.PriceProtectID = Convert.ToString(reader[PriceProtect_Customer.colPriceProtectID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colIsDefault])) objPriceProtect_Customer.IsDefault = Convert.ToBoolean(reader[PriceProtect_Customer.colIsDefault]);
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCreatedUser])) objPriceProtect_Customer.CreatedUser = Convert.ToString(reader[PriceProtect_Customer.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCreatedDate])) objPriceProtect_Customer.CreatedDate = Convert.ToDateTime(reader[PriceProtect_Customer.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colUpdatedUser])) objPriceProtect_Customer.UpdatedUser = Convert.ToString(reader[PriceProtect_Customer.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colUpdatedDate])) objPriceProtect_Customer.UpdatedDate = Convert.ToDateTime(reader[PriceProtect_Customer.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colIsDeleted])) objPriceProtect_Customer.IsDeleted = Convert.ToBoolean(reader[PriceProtect_Customer.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colDeletedUser])) objPriceProtect_Customer.DeletedUser = Convert.ToString(reader[PriceProtect_Customer.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colDeletedDate])) objPriceProtect_Customer.DeletedDate = Convert.ToDateTime(reader[PriceProtect_Customer.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerName])) objPriceProtect_Customer.CustomerName = Convert.ToString(reader[PriceProtect_Customer.colCustomerName]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerAddress])) objPriceProtect_Customer.CustomerAddress = Convert.ToString(reader[PriceProtect_Customer.colCustomerAddress]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerTaxID])) objPriceProtect_Customer.CustomerTaxID = Convert.ToString(reader[PriceProtect_Customer.colCustomerTaxID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect_Customer.colCustomerPhone])) objPriceProtect_Customer.CustomerPhone = Convert.ToString(reader[PriceProtect_Customer.colCustomerPhone]).Trim();
                    lstPriceProtect_Customer.Add(objPriceProtect_Customer);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi tìm kiếm thông tin danh sách nhà cung cấp", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect_Customer -> SearchDataToList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion


        #region Constructor

        public DA_PriceProtect_Customer()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_PRICEProtect_CUSTOMER_ADD";
		public const String SP_UPDATE = "PM_PRICEProtect_CUSTOMER_UPD";
		public const String SP_DELETE = "PM_PRICEProtect_CUSTOMER_DEL";
		public const String SP_SELECT = "PM_PRICEProtect_CUSTOMER_SEL";
		public const String SP_SEARCH = "PM_PRICEProtect_CUSTOMER_SRH";
		public const String SP_UPDATEINDEX = "PM_PRICEProtect_CUSTOMER_UPDINDEX";
		#endregion
		
	}
}
