﻿using ERP.Inventory.BO.PM.PriceProtected;
using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.PM.PriceProtected
{
    public partial class DA_PriceProtectDetail_IMEI
    {
        public DA_PriceProtectDetail_IMEI()
        {
        }

        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        public ResultMessage Insert(PriceProtectDetail_IMEI objPriceProtectDetail_IMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                Insert(objIData, objPriceProtectDetail_IMEI);
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thêm Chi tiết biên bản hỗ trợ hàng tồn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtectDetail -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void Insert(IData objIData, PriceProtectDetail_IMEI objPriceProtectDetail_IMEI)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colPriceProtectDetailID, objPriceProtectDetail_IMEI.PriceProtectDetailID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colProductID, objPriceProtectDetail_IMEI.ProductID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colProductIMEI, objPriceProtectDetail_IMEI.ProductIMEI);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colInputVoucherID, objPriceProtectDetail_IMEI.InputVoucherID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colInputDate, objPriceProtectDetail_IMEI.InputDate);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colAllocateTypeID, objPriceProtectDetail_IMEI.AllocateTypeID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colStoreID, objPriceProtectDetail_IMEI.StoreID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colOriginalCostPrice, objPriceProtectDetail_IMEI.OriginalCostPrice);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colAdjustCostPrice, objPriceProtectDetail_IMEI.AdjustCostPrice);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colCostPrice, objPriceProtectDetail_IMEI.CostPrice);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colLastInputVoucherDetailID, objPriceProtectDetail_IMEI.LastInputVoucherDetailID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colNumOfAllocate, objPriceProtectDetail_IMEI.NumOfAllocate);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objPriceProtectDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(PriceProtectDetail_IMEI objPriceProtectDetail_IMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objPriceProtectDetail_IMEI);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStock -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objPriceProtectDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, PriceProtectDetail_IMEI objPriceProtectDetail_IMEI)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colProductIMEI, objPriceProtectDetail_IMEI.ProductIMEI);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colPriceProtectDetailID, objPriceProtectDetail_IMEI.PriceProtectDetailID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colProductID, objPriceProtectDetail_IMEI.ProductID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colInputVoucherID, objPriceProtectDetail_IMEI.InputVoucherID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colInputDate, objPriceProtectDetail_IMEI.InputDate);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colAllocateTypeID, objPriceProtectDetail_IMEI.AllocateTypeID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colStoreID, objPriceProtectDetail_IMEI.StoreID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colOriginalCostPrice, objPriceProtectDetail_IMEI.OriginalCostPrice);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colAdjustCostPrice, objPriceProtectDetail_IMEI.AdjustCostPrice);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colCostPrice, objPriceProtectDetail_IMEI.CostPrice);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colLastInputVoucherDetailID, objPriceProtectDetail_IMEI.LastInputVoucherDetailID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objPriceProtectDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, string strPriceProtectDetailID, int intNumOfAllocate)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DEL);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colPriceProtectDetailID, strPriceProtectDetailID);
                objIData.AddParameter("@" + PriceProtectDetail_IMEI.colNumOfAllocate, intNumOfAllocate);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public const String SP_ADD = "PM_PRICEPROTECTDETAIL_IMEL_ADD";
        public const String SP_DEL = "PM_PRICEPROTECTDETAIL_IMEL_DEL";
        public const String SP_SEARCH = "PM_PRICEPROTECTDETAIL_IMEL_SRH";
        public const String SP_SELECT = "PM_PRICEPROTECTDETAIL_IMEL_SEL";
        public const String SP_UPDATE = "PM_PRICEPROTECTDETAIL_IMEL_UPD";
    }
}
