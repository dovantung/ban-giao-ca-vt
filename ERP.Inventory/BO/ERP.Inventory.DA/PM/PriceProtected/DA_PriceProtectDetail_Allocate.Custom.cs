﻿using ERP.Inventory.BO.PM.PriceProtected;
using Library.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.PM.PriceProtected
{
    public partial class DA_PriceProtectDetail_Allocate
    {
        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="lstPriceProtectDetail_Allocate">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public List<PriceProtectDetail_Allocate> SearchDataToList(IData objIData, params object[] objKeywords)
        {
            List<PriceProtectDetail_Allocate> lstPriceProtectDetail_Allocate = new List<PriceProtectDetail_Allocate>();
            try
            {
                objIData.CreateNewStoredProcedure("PM_PRICEPROTECTDT_ALLOCATE_SRH");
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    PriceProtectDetail_Allocate objPriceProtectDetail_Allocate = new PriceProtectDetail_Allocate();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colPriceProtectDetailAllocateID])) objPriceProtectDetail_Allocate.PriceProtectDetailAllocateID = Convert.ToString(reader[PriceProtectDetail_Allocate.colPriceProtectDetailAllocateID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colPriceProtectDetailID])) objPriceProtectDetail_Allocate.PriceProtectDetailID = Convert.ToString(reader[PriceProtectDetail_Allocate.colPriceProtectDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colQuantity])) objPriceProtectDetail_Allocate.Quantity = Convert.ToInt32(reader[PriceProtectDetail_Allocate.colQuantity]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colTotalAmount])) objPriceProtectDetail_Allocate.TotalAmount = Convert.ToDecimal(reader[PriceProtectDetail_Allocate.colTotalAmount]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colVat])) objPriceProtectDetail_Allocate.Vat = Convert.ToDecimal(reader[PriceProtectDetail_Allocate.colVat]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colPrice])) objPriceProtectDetail_Allocate.Price = Convert.ToDecimal(reader[PriceProtectDetail_Allocate.colPrice]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colPriceNoVat])) objPriceProtectDetail_Allocate.PriceNoVat = Convert.ToDecimal(reader[PriceProtectDetail_Allocate.colPriceNoVat]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colAllocateTypeID])) objPriceProtectDetail_Allocate.AllocateTypeID = Convert.ToInt32(reader[PriceProtectDetail_Allocate.colAllocateTypeID]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colInVoiceTransTypeID])) objPriceProtectDetail_Allocate.InVoiceTransTypeID = Convert.ToInt16(reader[PriceProtectDetail_Allocate.colInVoiceTransTypeID]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colTotalAmountNoVat])) objPriceProtectDetail_Allocate.TotalAmountNoVat = Convert.ToDecimal(reader[PriceProtectDetail_Allocate.colTotalAmountNoVat]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colStoreID])) objPriceProtectDetail_Allocate.StoreID = Convert.ToInt32(reader[PriceProtectDetail_Allocate.colStoreID]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail_Allocate.colStoreName])) objPriceProtectDetail_Allocate.StoreName = Convert.ToString(reader[PriceProtectDetail_Allocate.colStoreName]).Trim();
                    lstPriceProtectDetail_Allocate.Add(objPriceProtectDetail_Allocate);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                throw (objEx);
            }
            return lstPriceProtectDetail_Allocate;
        }
    }
}
