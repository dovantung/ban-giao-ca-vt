﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ERP.Inventory.BO.PM.PriceProtected;
using Library.DataAccess;
using System.Data;
namespace ERP.Inventory.DA.PM.PriceProtected
{
    public partial class DA_PriceProtectDetail
    {
        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="lstPriceProtectDetail">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public List<PriceProtectDetail> SearchDataToList(IData objIData, params object[] objKeywords)
        {
            List<PriceProtectDetail> lstPriceProtectDetail = new List<PriceProtectDetail>();
            try
            {
                objIData.CreateNewStoredProcedure("PM_PRICEPROTECTDETAIL_SRH");
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    PriceProtectDetail objPriceProtectDetail = new PriceProtectDetail();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colPriceProtectDetailID])) objPriceProtectDetail.PriceProtectDetailID = Convert.ToString(reader[PriceProtectDetail.colPriceProtectDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colPriceProtectID])) objPriceProtectDetail.PriceProtectID = Convert.ToString(reader[PriceProtectDetail.colPriceProtectID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colProductID])) objPriceProtectDetail.ProductID = Convert.ToString(reader[PriceProtectDetail.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colProductName])) objPriceProtectDetail.ProductName = Convert.ToString(reader[PriceProtectDetail.colProductName]).Trim();

                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colInStockQuantity])) objPriceProtectDetail.InStockQuantity = Convert.ToInt32(reader[PriceProtectDetail.colInStockQuantity]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colPrice])) objPriceProtectDetail.Price = Convert.ToDecimal(reader[PriceProtectDetail.colPrice]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colPriceNoVat])) objPriceProtectDetail.PriceNoVat = Convert.ToDecimal(reader[PriceProtectDetail.colPriceNoVat]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colVat])) objPriceProtectDetail.Vat = Convert.ToDecimal(reader[PriceProtectDetail.colVat]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colVatTypeID])) objPriceProtectDetail.VatTypeID = Convert.ToInt32(reader[PriceProtectDetail.colVatTypeID]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colVatPercent])) objPriceProtectDetail.VatPercent = Convert.ToDecimal(reader[PriceProtectDetail.colVatPercent]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colTotalAmountNoVat])) objPriceProtectDetail.TotalAmountNoVat = Convert.ToDecimal(reader[PriceProtectDetail.colTotalAmountNoVat]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colTotalAmount])) objPriceProtectDetail.TotalAmount = Convert.ToDecimal(reader[PriceProtectDetail.colTotalAmount]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colVatAmount])) objPriceProtectDetail.VATAmount = Convert.ToDecimal(reader[PriceProtectDetail.colVatAmount]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colInVoiceID])) objPriceProtectDetail.InVoiceID = Convert.ToString(reader[PriceProtectDetail.colInVoiceID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colInputVoucherID])) objPriceProtectDetail.InputVoucherID = Convert.ToString(reader[PriceProtectDetail.colInputVoucherID]).Trim();

                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colVatPInvoiceDTID])) objPriceProtectDetail.VatPInvoiceDTID = Convert.ToString(reader[PriceProtectDetail.colVatPInvoiceDTID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colIsRequestIMEI])) objPriceProtectDetail.IsRequestIMEI = Convert.ToBoolean(reader[PriceProtectDetail.colIsRequestIMEI]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colCreatedUser])) objPriceProtectDetail.CreatedUser = Convert.ToString(reader[PriceProtectDetail.colCreatedUser]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colCreatedDate])) objPriceProtectDetail.CreatedDate = Convert.ToDateTime(reader[PriceProtectDetail.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colDeletedUser])) objPriceProtectDetail.DeletedUser = Convert.ToString(reader[PriceProtectDetail.colDeletedUser]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colDeletedDate])) objPriceProtectDetail.DeletedDate = Convert.ToDateTime(reader[PriceProtectDetail.colDeletedDate]);


                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colQuantity])) objPriceProtectDetail.Quantity = Convert.ToInt32(reader[PriceProtectDetail.colQuantity]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colPriceVoucher])) objPriceProtectDetail.PriceVoucher = Convert.ToDecimal(reader[PriceProtectDetail.colPriceVoucher]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colPriceNoVatVoucher])) objPriceProtectDetail.PriceNoVatVoucher = Convert.ToDecimal(reader[PriceProtectDetail.colPriceNoVatVoucher]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colTotalAmountNoVatVoucher])) objPriceProtectDetail.TotalAmountNoVatVoucher = Convert.ToInt32(reader[PriceProtectDetail.colTotalAmountNoVatVoucher]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colTotalAmountVoucher])) objPriceProtectDetail.TotalAmountVoucher = Convert.ToDecimal(reader[PriceProtectDetail.colTotalAmountVoucher]);
                    if (!Convert.IsDBNull(reader[PriceProtectDetail.colVATAmountVoucher])) objPriceProtectDetail.VatAmountVoucher = Convert.ToDecimal(reader[PriceProtectDetail.colVATAmountVoucher]);

                    if(objPriceProtectDetail.Quantity == null)
                    {
                        objPriceProtectDetail.Quantity = objPriceProtectDetail.InStockQuantity;
                        objPriceProtectDetail.PriceVoucher = objPriceProtectDetail.Price;
                        objPriceProtectDetail.PriceNoVatVoucher = objPriceProtectDetail.PriceNoVat;
                        objPriceProtectDetail.TotalAmountNoVatVoucher = objPriceProtectDetail.TotalAmountNoVat;
                        objPriceProtectDetail.TotalAmountVoucher = objPriceProtectDetail.TotalAmount;
                        objPriceProtectDetail.VatAmountVoucher = objPriceProtectDetail.VATAmount;

                    }
                    
                    objPriceProtectDetail.QuantityDifference = objPriceProtectDetail.Quantity - objPriceProtectDetail.InStockQuantity;
                    objPriceProtectDetail.PriceDifference = objPriceProtectDetail.PriceVoucher - objPriceProtectDetail.Price;
                    objPriceProtectDetail.PriceNoVatDifference =  objPriceProtectDetail.PriceNoVatVoucher -objPriceProtectDetail.PriceNoVat;
                    objPriceProtectDetail.TotalAmountNoVatDifference = objPriceProtectDetail.TotalAmountNoVatVoucher - objPriceProtectDetail.TotalAmountNoVat;
                    objPriceProtectDetail.TotalAmountDifference = objPriceProtectDetail.TotalAmountVoucher - objPriceProtectDetail.TotalAmount;
                    objPriceProtectDetail.VatAmountDifference = objPriceProtectDetail.VatAmountVoucher - objPriceProtectDetail.VATAmount;

                    lstPriceProtectDetail.Add(objPriceProtectDetail);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                throw (objEx);
            }
            return lstPriceProtectDetail;
        }
    }
}
