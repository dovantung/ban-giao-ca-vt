﻿using ERP.Inventory.BO.PM.PriceProtected;
using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.PM.PriceProtected
{
    public partial class DA_PriceProtectDetail_Allocate
    {
        public DA_PriceProtectDetail_Allocate()
        {

        }
        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        public ResultMessage Insert(PriceProtectDetail_Allocate objPriceProtectDetail_Allocate)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                Insert(objIData, objPriceProtectDetail_Allocate);
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thêm Chi tiết biên bản hỗ trợ hàng tồn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtectDetail -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            return objResultMessage;
        }

        public void Insert(IData objIData, PriceProtectDetail_Allocate objPriceProtectDetail_Allocate)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colPriceProtectDetailID, objPriceProtectDetail_Allocate.PriceProtectDetailID);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colVatPinVoicedID, objPriceProtectDetail_Allocate.VatPinVoiceID);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colInVoiceTransTypeID, objPriceProtectDetail_Allocate.InVoiceTransTypeID);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colAllocateTypeID, objPriceProtectDetail_Allocate.AllocateTypeID);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colQuantity, (objPriceProtectDetail_Allocate.Quantity == null ? 0 : objPriceProtectDetail_Allocate.Quantity));
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colPrice, objPriceProtectDetail_Allocate.Price);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colVat, objPriceProtectDetail_Allocate.Vat);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colTotalAmount, objPriceProtectDetail_Allocate.TotalAmount);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colPriceNoVat, objPriceProtectDetail_Allocate.PriceNoVat);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colTotalAmountNoVat, objPriceProtectDetail_Allocate.TotalAmountNoVat);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colStoreID, objPriceProtectDetail_Allocate.StoreID);

                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objPriceProtectDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(PriceProtectDetail_Allocate objPriceProtectDetail_Allocate)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objPriceProtectDetail_Allocate);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStock -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objPriceProtectDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, PriceProtectDetail_Allocate objPriceProtectDetail_Allocate)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colPriceProtectDetailAllocateID, objPriceProtectDetail_Allocate.PriceProtectDetailAllocateID);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colPriceProtectDetailID, objPriceProtectDetail_Allocate.PriceProtectDetailID);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colVatPinVoicedID, objPriceProtectDetail_Allocate.VatPinVoiceID);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colInVoiceTransTypeID, objPriceProtectDetail_Allocate.InVoiceTransTypeID);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colAllocateTypeID, objPriceProtectDetail_Allocate.AllocateTypeID);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colQuantity, objPriceProtectDetail_Allocate.Quantity);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colPrice, objPriceProtectDetail_Allocate.Price);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colVat, objPriceProtectDetail_Allocate.Vat);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colTotalAmount, objPriceProtectDetail_Allocate.TotalAmount);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colPriceNoVat, objPriceProtectDetail_Allocate.PriceNoVat);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colTotalAmountNoVat, objPriceProtectDetail_Allocate.TotalAmountNoVat);
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colStoreID, objPriceProtectDetail_Allocate.StoreID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objPriceProtectDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, string strPriceProtectDetailID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_PRICEPROTECTDT_AC_DELALL");
                objIData.AddParameter("@" + PriceProtectDetail_Allocate.colPriceProtectDetailID, strPriceProtectDetailID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public const String SP_ADD = "PM_PRICEPROTECTDT_ALLOCATE_ADD";
        public const String SP_DEL = "PM_PRICEPROTECTDT_ALLOCATE_DEL";
        public const String SP_SEARCH = "PM_PRICEPROTECTDT_ALLOCATE_SRH";
        public const String SP_SELECT = "PM_PRICEPROTECTDT_ALLOCATE_SEL";
        public const String SP_UPDATE = "PM_PRICEPROTECTDT_ALLOCATE_UPD";
    }
}
