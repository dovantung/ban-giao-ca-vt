﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ERP.Inventory.BO.PM.PriceProtected;
using System.Data;
namespace ERP.Inventory.DA.PM.PriceProtected
{
    /// <summary>
    /// Created by 		: LE VAN DONG
    /// Created date 	: 02/07/2017
    /// 
    /// </summary>
    public partial class DA_PriceProtect
    {
        public DA_PriceProtect()
        {
        }

        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion



        /// <summary>
        /// Nạp thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIMEISalesInfo">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref PriceProtect objPriceProtect, string strPriceProtectID)
        {
            objPriceProtect = new PriceProtect();
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + PriceProtect.colPriceProtectID, strPriceProtectID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader[PriceProtect.colPriceProtectID])) objPriceProtect.PriceProtectID = Convert.ToString(reader[PriceProtect.colPriceProtectID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colPriceProtectNO])) objPriceProtect.PriceProtectNO = Convert.ToString(reader[PriceProtect.colPriceProtectNO]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colPriceProtectDate])) objPriceProtect.PriceProtectDate = Convert.ToDateTime(reader[PriceProtect.colPriceProtectDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colStockDate])) objPriceProtect.StockDate = Convert.ToDateTime(reader[PriceProtect.colStockDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colCustomerID])) objPriceProtect.CustomerID = Convert.ToInt32(reader[PriceProtect.colCustomerID]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colCustomerName])) objPriceProtect.CustomerName = Convert.ToString(reader[PriceProtect.colCustomerName]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colCustomerAddress])) objPriceProtect.CustomerAddress = Convert.ToString(reader[PriceProtect.colCustomerAddress]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colStatus])) objPriceProtect.Status = Convert.ToUInt16(reader[PriceProtect.colStatus]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colNote])) objPriceProtect.Note = Convert.ToString(reader[PriceProtect.colNote]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colIsInvoice])) objPriceProtect.IsInvoice = Convert.ToBoolean(reader[PriceProtect.colIsInvoice]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colVATPINVOICEID])) objPriceProtect.VatPinVoiceID = Convert.ToString(reader[PriceProtect.colVATPINVOICEID]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colCreatedUser])) objPriceProtect.CreatedUser = Convert.ToString(reader[PriceProtect.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colCreatedDate])) objPriceProtect.CreatedDate = Convert.ToDateTime(reader[PriceProtect.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colDeletedUser])) objPriceProtect.DeletedUser = Convert.ToString(reader[PriceProtect.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colDeletedDate])) objPriceProtect.DeletedDate = Convert.ToDateTime(reader[PriceProtect.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colIsRoadTrip])) objPriceProtect.IsRoadTrip = Convert.ToInt16(reader[PriceProtect.colIsRoadTrip]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colInVoiceTransTypeID])) objPriceProtect.InVoiceTransTypeID = Convert.ToInt16(reader[PriceProtect.colInVoiceTransTypeID]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colPriceProtectTypeID])) objPriceProtect.PriceProtectTypeID = Convert.ToInt16(reader[PriceProtect.colPriceProtectTypeID]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colVoucherDate])) objPriceProtect.VoucherDate = Convert.ToDateTime(reader[PriceProtect.colVoucherDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colCreatedStoreID])) objPriceProtect.CreatedStoreID = Convert.ToInt32(reader[PriceProtect.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colStoreID])) objPriceProtect.StoreID = Convert.ToInt32(reader[PriceProtect.colStoreID]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colCompanyID])) objPriceProtect.CompanyID = Convert.ToInt32(reader[PriceProtect.colCompanyID]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colUpdatedDate])) objPriceProtect.UpdatedDate = Convert.ToDateTime(reader[PriceProtect.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colUpdatedUser])) objPriceProtect.UpdatedUser = Convert.ToString(reader[PriceProtect.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colIsHasInternalInvoice])) objPriceProtect.IsHasInternalInvoice = Convert.ToBoolean(reader[PriceProtect.colIsHasInternalInvoice]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colIsHasAdjustVoucher])) objPriceProtect.IsHasAdjustVoucher = Convert.ToBoolean(reader[PriceProtect.colIsHasAdjustVoucher]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colIsSupplierCal])) objPriceProtect.IsSupplierCal = Convert.ToBoolean(reader[PriceProtect.colIsSupplierCal]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colInputFromDate])) objPriceProtect.InputFromDate = Convert.ToDateTime(reader[PriceProtect.colInputFromDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colInputToDate])) objPriceProtect.InputToDate = Convert.ToDateTime(reader[PriceProtect.colInputToDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colPostDate])) objPriceProtect.PostDate = Convert.ToDateTime(reader[PriceProtect.colPostDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colIsHasInternalInvoice_PostDate])) objPriceProtect.IsHasInternalInvoice_Postdate = Convert.ToBoolean(reader[PriceProtect.colIsHasInternalInvoice_PostDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colIsHasAdjustVoucher_PostDate])) objPriceProtect.IsHasAdjustVoucher_Postdate = Convert.ToBoolean(reader[PriceProtect.colIsHasAdjustVoucher_PostDate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colIsHasAllocateByPostdate])) objPriceProtect.IsHasAllocateByPostdate = Convert.ToBoolean(reader[PriceProtect.colIsHasAllocateByPostdate]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colIsApproved])) objPriceProtect.IsApproved = Convert.ToBoolean(reader[PriceProtect.colIsApproved]);
                    if (!Convert.IsDBNull(reader[PriceProtect.colApprovedUser])) objPriceProtect.ApprovedUser = Convert.ToString(reader[PriceProtect.colApprovedUser]).Trim();
                    if (!Convert.IsDBNull(reader[PriceProtect.colApprovedDate])) objPriceProtect.ApprovedDate = Convert.ToDateTime(reader[PriceProtect.colApprovedDate]);


                    objPriceProtect.LstPriceProtectDetail = new PriceProtected.DA_PriceProtectDetail().SearchDataToList(objIData, new object[] { "@PriceProtectID", objPriceProtect.PriceProtectID });
                    objPriceProtect.LstPriceProtectDetail_IMEI = new PriceProtected.DA_PriceProtectDetail_IMEI().SearchDataToList(objIData, "@PriceProtectID", objPriceProtect.PriceProtectID);
                    foreach (var objDetail in objPriceProtect.LstPriceProtectDetail)
                    {
                        objDetail.PriceProtectDetail_ImeiList =
                            objPriceProtect.LstPriceProtectDetail_IMEI.Where(r => r.PriceProtectDetailID.Trim() == objDetail.PriceProtectDetailID.Trim() && r.NumOfAllocate == 1
                            ).ToList();
                        objDetail.PriceProtectDetail_Allocate = new PriceProtected.DA_PriceProtectDetail_Allocate().SearchDataToList(objIData,
                            new object[] { "@PRICEPROTECTDETAILID", objDetail.PriceProtectDetailID, "@NUMOFALLOCATE", 1 });

                        if (objDetail.PriceProtectDetail_Allocate != null && objDetail.PriceProtectDetail_Allocate.Count() > 0)
                        {
                            objDetail.QuantityNoInput = objDetail.PriceProtectDetail_Allocate.Where(s => s.AllocateTypeID == 0).Sum(s => s.Quantity);

                            objDetail.QuantityStock = objDetail.PriceProtectDetail_Allocate.Where(s => s.AllocateTypeID == 2).Sum(s => s.Quantity);
                            objDetail.TotalAmountStock = objDetail.QuantityStock * objDetail.PriceNoVat;

                            objDetail.QuantityBuy = objDetail.PriceProtectDetail_Allocate.Where(s => s.AllocateTypeID == 1).Sum(s => s.Quantity);
                            objDetail.TotalAmountBuy = objDetail.QuantityBuy * objDetail.PriceNoVat;

                            objDetail.QuantityInStockNonIsCenter = objDetail.PriceProtectDetail_Allocate.Where(s => s.AllocateTypeID == 3).Sum(s => s.Quantity);
                            objDetail.TotalAmountStockNonIsCenter = objDetail.QuantityInStockNonIsCenter * objDetail.PriceNoVat;

                            objDetail.QuantityBuyNonIsCenter = objDetail.PriceProtectDetail_Allocate.Where(s => s.AllocateTypeID == 4).Sum(s => s.Quantity);
                            objDetail.TotalAmountBuyNonIsCenter = objDetail.QuantityBuyNonIsCenter * objDetail.PriceNoVat;

                            objDetail.QuantityOtherIncome = objDetail.PriceProtectDetail_Allocate.Where(s => s.AllocateTypeID == 5).Sum(s => s.Quantity);
                            objDetail.TotalAmountOtherIncome = objDetail.QuantityOtherIncome * objDetail.PriceNoVat;

                            //objDetail.TotalAmountNoVat = objDetail.TotalAmountStock + objDetail.TotalAmountBuy + objDetail.TotalAmountStockNonIsCenter + objDetail.TotalAmountBuyNonIsCenter + objDetail.TotalAmountOtherIncome;
                        }
                        if (objPriceProtect.IsHasAllocateByPostdate)
                        {
                            objDetail.PriceProtectDetail_ImeiList1 =
                               objPriceProtect.LstPriceProtectDetail_IMEI.Where(r => r.PriceProtectDetailID.Trim() == objDetail.PriceProtectDetailID.Trim() && r.NumOfAllocate == 2
                               ).ToList();
                            if (objDetail.PriceProtectDetail_ImeiList1 == null || objDetail.PriceProtectDetail_ImeiList1.Count == 0)
                                objDetail.PriceProtectDetail_ImeiList1 = objDetail.PriceProtectDetail_ImeiList;
                            objDetail.PriceProtectDetail_Allocate2 = new PriceProtected.DA_PriceProtectDetail_Allocate().SearchDataToList(objIData,
                                                       new object[] { "@PRICEPROTECTDETAILID", objDetail.PriceProtectDetailID, "@NUMOFALLOCATE", 2 });
                            if (objDetail.PriceProtectDetail_Allocate2 != null && objDetail.PriceProtectDetail_Allocate2.Count() > 0)
                            {
                                objDetail.QuantityNoInputVoucher = objDetail.PriceProtectDetail_Allocate2.Where(s => s.AllocateTypeID == 0).Sum(s => s.Quantity);

                                objDetail.QuantityStockVoucher = objDetail.PriceProtectDetail_Allocate2.Where(s => s.AllocateTypeID == 2).Sum(s => s.Quantity);
                                objDetail.TotalAmountStockVoucher = objDetail.QuantityStockVoucher * objDetail.PriceNoVatVoucher;

                                objDetail.QuantityBuyVoucher = objDetail.PriceProtectDetail_Allocate2.Where(s => s.AllocateTypeID == 1).Sum(s => s.Quantity);
                                objDetail.TotalAmountBuyVoucher = objDetail.QuantityBuyVoucher * objDetail.PriceNoVatVoucher;

                                objDetail.QuantityInStockNonIsCenterVoucher = objDetail.PriceProtectDetail_Allocate2.Where(s => s.AllocateTypeID == 3).Sum(s => s.Quantity);
                                objDetail.TotalAmountStockNonIsCenterVoucher = objDetail.QuantityInStockNonIsCenterVoucher * objDetail.PriceNoVatVoucher;

                                objDetail.QuantityBuyNonIsCenterVoucher = objDetail.PriceProtectDetail_Allocate2.Where(s => s.AllocateTypeID == 4).Sum(s => s.Quantity);
                                objDetail.TotalAmountBuyNonIsCenterVoucher = objDetail.QuantityBuyNonIsCenterVoucher * objDetail.PriceNoVatVoucher;

                                objDetail.QuantityOtherIncomeVoucher = objDetail.PriceProtectDetail_Allocate2.Where(s => s.AllocateTypeID == 5).Sum(s => s.Quantity);
                                objDetail.TotalAmountOtherIncomeVoucher = objDetail.QuantityOtherIncomeVoucher * objDetail.PriceNoVatVoucher;

                                //objDetail.TotalAmountNoVatVoucher = objDetail.TotalAmountStockVoucher + objDetail.TotalAmountBuyVoucher
                                //        + objDetail.TotalAmountStockNonIsCenterVoucher + objDetail.TotalAmountBuyNonIsCenterVoucher + objDetail.TotalAmountOtherIncomeVoucher;

                                objDetail.QuantityNoInputDifference = objDetail.QuantityNoInputVoucher - objDetail.QuantityNoInput;

                                objDetail.QuantityStockDifference = objDetail.QuantityStockVoucher - objDetail.QuantityStock;
                                objDetail.TotalAmountStockDifference = objDetail.TotalAmountStockVoucher - objDetail.TotalAmountStock;

                                objDetail.QuantityBuyDifference = objDetail.QuantityBuyVoucher - objDetail.QuantityBuy;
                                objDetail.TotalAmountBuyDifference = objDetail.TotalAmountBuyVoucher - objDetail.TotalAmountBuy;

                                objDetail.QuantityInStockNonIsCenterDifference = objDetail.QuantityInStockNonIsCenterVoucher - objDetail.QuantityInStockNonIsCenter;
                                objDetail.TotalAmountStockNonIsCenterDifference = objDetail.TotalAmountStockNonIsCenterVoucher - objDetail.TotalAmountStockNonIsCenter;

                                objDetail.QuantityBuyNonIsCenterDifference = objDetail.QuantityBuyNonIsCenterVoucher - objDetail.QuantityBuyNonIsCenter;
                                objDetail.TotalAmountBuyNonIsCenterDifference = objDetail.TotalAmountBuyNonIsCenterVoucher - objDetail.TotalAmountBuyNonIsCenter;

                                objDetail.QuantityOtherIncomeDifference = objDetail.QuantityOtherIncomeVoucher - objDetail.QuantityOtherIncome;
                                objDetail.TotalAmountOtherIncomeDifference = objDetail.TotalAmountOtherIncomeVoucher - objDetail.TotalAmountOtherIncome;

                            }
                        }
                    }

                    List<PriceProtect_Attachment> listPriceProtect_Attachment = null;
                    objResultMessage = new DA_PriceProtect_Attachment().SearchDataToList(ref listPriceProtect_Attachment, new object[] { "@" + PriceProtect_Attachment.colPriceProtectID, objPriceProtect.PriceProtectID });
                    objPriceProtect.ListPriceProtect_Attachment = listPriceProtect_Attachment;
                    if (objResultMessage.IsError)
                        return objResultMessage;

                    List<PriceProtect_Customer> listPriceProtect_Customer = null;
                    objResultMessage = new DA_PriceProtect_Customer().SearchDataToList(ref listPriceProtect_Customer, new object[] { "@" + PriceProtect_Attachment.colPriceProtectID, objPriceProtect.PriceProtectID });
                    objPriceProtect.ListPriceProtect_Customer = listPriceProtect_Customer;
                    if (objResultMessage.IsError)
                        return objResultMessage;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin phiếu hỗ trợ hàng tồn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// </summary>
        /// <param name="objPriceProtect"></param>
        /// <returns></returns>
        public ResultMessage Insert(ref PriceProtect objPriceProtect)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            string strPriceProtectID = string.Empty;
            string strPriceProtectDetailID = string.Empty;
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                Insert(objIData, objPriceProtect, ref strPriceProtectID);
                objPriceProtect.PriceProtectID = strPriceProtectID;

                DA.PM.PriceProtected.DA_PriceProtectDetail objDA_PriceProtectDetail = new DA_PriceProtectDetail();
                DA.PM.PriceProtected.DA_PriceProtect_Customer objDA_PriceProtect_Customer = new DA_PriceProtect_Customer();
                DA.PM.PriceProtected.DA_PriceProtect_Attachment objDA_PriceProtect_Attachment = new DA_PriceProtect_Attachment();
                DA.PM.PriceProtected.DA_PriceProtectDetail_IMEI objDA_PriceProtectDetail_IMEI = new DA_PriceProtectDetail_IMEI();

                foreach (PriceProtectDetail objDetail in objPriceProtect.LstPriceProtectDetail)
                {
                    objDetail.CreatedUser = objPriceProtect.CreatedUser;
                    objDA_PriceProtectDetail.Insert(objIData, objDetail, strPriceProtectID, ref strPriceProtectDetailID);
                    if (objDetail.PriceProtectDetail_ImeiList != null && objDetail.PriceProtectDetail_ImeiList.Count > 0)
                    {
                        foreach (PriceProtectDetail_IMEI objIMEI in objDetail.PriceProtectDetail_ImeiList)
                        {
                            objIMEI.PriceProtectDetailID = strPriceProtectDetailID.Trim();
                            objIMEI.NumOfAllocate = 1;
                            objDA_PriceProtectDetail_IMEI.Insert(objIData, objIMEI);
                        }
                    }
                }


                if (objPriceProtect.ListPriceProtect_Customer != null && objPriceProtect.ListPriceProtect_Customer.Count > 0)
                {
                    foreach (var item in objPriceProtect.ListPriceProtect_Customer)
                    {
                        item.CreatedUser = objPriceProtect.CreatedUser;
                        item.PriceProtectID = strPriceProtectID;
                        objDA_PriceProtect_Customer.Insert(objIData, item);
                    }
                }

                if (objPriceProtect.ListPriceProtect_Attachment != null && objPriceProtect.ListPriceProtect_Attachment.Count > 0)
                {
                    foreach (var item in objPriceProtect.ListPriceProtect_Attachment)
                    {
                        item.CreatedUser = objPriceProtect.CreatedUser;
                        item.PriceProtectID = strPriceProtectID;
                        item.AttachmentID = objDA_PriceProtect_Attachment.Insert(objIData, item);
                    }
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thêm thông tin hỗ trợ hàng tồn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private void Insert(IData objIData, PriceProtect objPriceProtect, ref string strPriceProtectID)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + PriceProtect.colPriceProtectDate, objPriceProtect.PriceProtectDate);
                objIData.AddParameter("@" + PriceProtect.colPriceProtectNO, objPriceProtect.PriceProtectNO);
                objIData.AddParameter("@" + PriceProtect.colCustomerID, objPriceProtect.CustomerID);
                objIData.AddParameter("@" + PriceProtect.colCustomerName, objPriceProtect.CustomerName);
                objIData.AddParameter("@" + PriceProtect.colCustomerAddress, objPriceProtect.CustomerAddress);
                objIData.AddParameter("@" + PriceProtect.colStockDate, objPriceProtect.StockDate);
                objIData.AddParameter("@" + PriceProtect.colStatus, objPriceProtect.Status);
                objIData.AddParameter("@" + PriceProtect.colNote, objPriceProtect.Note);
                objIData.AddParameter("@" + PriceProtect.colIsInvoice, objPriceProtect.IsInvoice);
                objIData.AddParameter("@" + PriceProtect.colVATPINVOICEID, objPriceProtect.VatPinVoiceID);
                objIData.AddParameter("@" + PriceProtect.colCreatedUser, objPriceProtect.CreatedUser);
                objIData.AddParameter("@" + PriceProtect.colIsRoadTrip, objPriceProtect.IsRoadTrip);
                objIData.AddParameter("@" + PriceProtect.colInVoiceTransTypeID, objPriceProtect.InVoiceTransTypeID);
                objIData.AddParameter("@" + PriceProtect.colPriceProtectTypeID, objPriceProtect.PriceProtectTypeID);
                objIData.AddParameter("@" + PriceProtect.colVoucherDate, objPriceProtect.VoucherDate);
                objIData.AddParameter("@" + PriceProtect.colCreatedStoreID, objPriceProtect.CreatedStoreID);
                objIData.AddParameter("@" + PriceProtect.colStoreID, objPriceProtect.StoreID);
                objIData.AddParameter("@" + PriceProtect.colCompanyID, objPriceProtect.CompanyID);
                objIData.AddParameter("@" + PriceProtect.colIsHasAdjustVoucher, objPriceProtect.IsHasAdjustVoucher);
                objIData.AddParameter("@" + PriceProtect.colIsSupplierCal, objPriceProtect.IsSupplierCal);
                objIData.AddParameter("@" + PriceProtect.colInputFromDate, objPriceProtect.InputFromDate);
                objIData.AddParameter("@" + PriceProtect.colInputToDate, objPriceProtect.InputToDate);

                objIData.AddParameter("@" + PriceProtect.colPostDate, objPriceProtect.PostDate);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LOGINLOGID", objLogObject.LoginLogID);

                strPriceProtectID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Tìm kiếm thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_PriceProtect.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin hỗ trợ hàng tồn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objPriceProtect">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(PriceProtect objPriceProtect)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                Update(objIData, objPriceProtect);
                DA_PriceProtectDetail objDA_PriceProtectDetail = new DA_PriceProtectDetail();
                DA_PriceProtectDetail_IMEI objDA_PriceProtectDetail_IMEI = new DA_PriceProtectDetail_IMEI();
                DA_PriceProtectDetail_Allocate objDA_PriceProtectDetail_Allocate = new DA_PriceProtectDetail_Allocate();
                DA_PriceProtect_Customer objDA_PriceProtect_Customer = new DA_PriceProtect_Customer();
                DA_PriceProtect_Attachment objDA_PriceProtect_Attachment = new DA_PriceProtect_Attachment();
                foreach (PriceProtectDetail objDetail in objPriceProtect.LstPriceProtectDetail)
                {
                    objDA_PriceProtectDetail.Update(objIData, objDetail, objPriceProtect.IsHasAllocateByPostdate);
                    //// Xóa imei add lại
                    //objDA_PriceProtectDetail_IMEI.Delete(objIData, objDetail.PriceProtectDetailID.Trim());
                    //if (objDetail.PriceProtectDetail_ImeiList != null && objDetail.PriceProtectDetail_ImeiList.Count > 0)
                    //{
                    //    foreach (PriceProtectDetail_IMEI objIMEI in objDetail.PriceProtectDetail_ImeiList)
                    //    {
                    //        objIMEI.PriceProtectDetailID = objDetail.PriceProtectDetailID;
                    //        objDA_PriceProtectDetail_IMEI.Insert(objIData, objIMEI);
                    //    }
                    //}
                    if (objPriceProtect.PriceProtectTypeID == 4)
                    {
                        if (!objPriceProtect.IsApproved)
                        {
                            objDA_PriceProtectDetail_IMEI.Delete(objIData, objDetail.PriceProtectDetailID.Trim(), 1);
                            if (objDetail.PriceProtectDetail_ImeiList != null && objDetail.PriceProtectDetail_ImeiList.Count > 0)
                            {
                                foreach (PriceProtectDetail_IMEI objIMEI in objDetail.PriceProtectDetail_ImeiList)
                                {
                                    objIMEI.PriceProtectDetailID = objDetail.PriceProtectDetailID;
                                    objIMEI.NumOfAllocate = 1;
                                    objDA_PriceProtectDetail_IMEI.Insert(objIData, objIMEI);
                                }
                            }
                        }
                        else
                        {
                            objDA_PriceProtectDetail_IMEI.Delete(objIData, objDetail.PriceProtectDetailID.Trim(), 2);
                            if (objDetail.PriceProtectDetail_ImeiList1 != null && objDetail.PriceProtectDetail_ImeiList1.Count > 0)
                            {
                                foreach (PriceProtectDetail_IMEI objIMEI in objDetail.PriceProtectDetail_ImeiList1)
                                {
                                    objIMEI.PriceProtectDetailID = objDetail.PriceProtectDetailID;
                                    objIMEI.NumOfAllocate = 2;
                                    objDA_PriceProtectDetail_IMEI.Insert(objIData, objIMEI);
                                }
                            }
                        }
                    }
                }

                foreach (var objCustomer in objPriceProtect.ListPriceProtect_Customer)
                {
                    if (objCustomer.IsUpdate)
                    {
                        objCustomer.UpdatedUser = objPriceProtect.UpdatedUser;
                        objDA_PriceProtect_Customer.Update(objIData, objCustomer);
                    }
                }

                foreach (var objAttach in objPriceProtect.ListPriceProtect_Attachment)
                {
                    if (string.IsNullOrEmpty(objAttach.AttachmentID))
                    {
                        objAttach.CreatedUser = objPriceProtect.CreatedUser;
                        objAttach.PriceProtectID = objPriceProtect.PriceProtectID;
                        objAttach.AttachmentID = objDA_PriceProtect_Attachment.Insert(objIData, objAttach);
                    }
                    else if (objAttach.IsUpdate)
                    {
                        objDA_PriceProtect_Attachment.Update(objIData, objAttach);
                    }
                }


                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin hỗ trợ giá", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objPriceProtect">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, PriceProtect objPriceProtect)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + PriceProtect.colPriceProtectID, objPriceProtect.PriceProtectID);
                objIData.AddParameter("@" + PriceProtect.colCustomerID, objPriceProtect.CustomerID);
                objIData.AddParameter("@" + PriceProtect.colCustomerName, objPriceProtect.CustomerName);
                objIData.AddParameter("@" + PriceProtect.colCustomerAddress, objPriceProtect.CustomerAddress);
                objIData.AddParameter("@" + PriceProtect.colVATPINVOICEID, objPriceProtect.VatPinVoiceID);
                objIData.AddParameter("@" + PriceProtect.colIsInvoice, objPriceProtect.IsInvoice);
                objIData.AddParameter("@" + PriceProtect.colStatus, objPriceProtect.Status);
                objIData.AddParameter("@" + PriceProtect.colNote, objPriceProtect.Note);
                objIData.AddParameter("@" + PriceProtect.colPriceProtectNO, objPriceProtect.PriceProtectNO);
                objIData.AddParameter("@" + PriceProtect.colPriceProtectDate, objPriceProtect.PriceProtectDate);
                objIData.AddParameter("@" + PriceProtect.colStockDate, objPriceProtect.StockDate);
                objIData.AddParameter("@" + PriceProtect.colIsRoadTrip, objPriceProtect.IsRoadTrip);
                objIData.AddParameter("@" + PriceProtect.colInVoiceTransTypeID, objPriceProtect.InVoiceTransTypeID);
                objIData.AddParameter("@" + PriceProtect.colPriceProtectTypeID, objPriceProtect.PriceProtectTypeID);
                objIData.AddParameter("@" + PriceProtect.colVoucherDate, objPriceProtect.VoucherDate);
                objIData.AddParameter("@" + PriceProtect.colUpdatedUser, objPriceProtect.UpdatedUser);
                objIData.AddParameter("@" + PriceProtect.colCreatedStoreID, objPriceProtect.CreatedStoreID);
                objIData.AddParameter("@" + PriceProtect.colStoreID, objPriceProtect.StoreID);
                objIData.AddParameter("@" + PriceProtect.colCompanyID, objPriceProtect.CompanyID);
                objIData.AddParameter("@" + PriceProtect.colIsHasInternalInvoice, objPriceProtect.IsHasInternalInvoice);
                objIData.AddParameter("@" + PriceProtect.colIsHasAdjustVoucher, objPriceProtect.IsHasAdjustVoucher);
                objIData.AddParameter("@" + PriceProtect.colIsSupplierCal, objPriceProtect.IsSupplierCal);
                objIData.AddParameter("@" + PriceProtect.colInputFromDate, objPriceProtect.InputFromDate);
                objIData.AddParameter("@" + PriceProtect.colInputToDate, objPriceProtect.InputToDate);
                objIData.AddParameter("@" + PriceProtect.colPostDate, objPriceProtect.PostDate);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Xóa thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objPriceProtect">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(PriceProtect objPriceProtect)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objPriceProtect);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi hủy phiếu hỗ trợ hàng tồn", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, PriceProtect objPriceProtect)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DEL);
                objIData.AddParameter("@" + PriceProtect.colPriceProtectID, objPriceProtect.PriceProtectID);
                objIData.AddParameter("@DeletedUser", objPriceProtect.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public const String SP_ADD = "PM_PRICEPROTECT_ADD";
        public const String SP_DEL = "PM_PRICEPROTECT_DEL";
        public const String SP_SEARCH = "PM_PRICEPROTECT_SRH";
        public const String SP_SELECT = "PM_PRICEPROTECT_SEL";
        public const String SP_UPDATE = "PM_PRICEPROTECT_UPD";
    }
}
