﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ERP.Inventory.BO.PM.PriceProtected;

namespace ERP.Inventory.DA.PM.PriceProtected
{
    public partial class DA_PriceProtect
    {
        public ResultMessage CalInStock(ref DataTable dtbInStock, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("GETSTOCK_BY_CUSTOMER");
                objIData.AddParameter(objKeywords);
                dtbInStock = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tính tồn kho theo nhà cung cấp", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> CalInStock", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        public ResultMessage CalculatorQuantity(ref List<ERP.Inventory.BO.PM.PriceProtected.PriceProtectDetail> lstPriceProtectDetail, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                foreach (ERP.Inventory.BO.PM.PriceProtected.PriceProtectDetail objDetail in lstPriceProtectDetail)
                {
                    DataTable dtbData = null;
                    CalculatorQuantity(objIData, ref dtbData, objDetail.PriceProtectDetailID.Trim(), objKeywords);

                    if (dtbData != null && dtbData.Rows.Count > 0)
                    {
                        objDetail.QuantityBuy = 0;
                        objDetail.QuantityStock = 0;
                        objDetail.QuantityBuyNonIsCenter = 0;
                        objDetail.QuantityInStockNonIsCenter = 0;
                        if (dtbData.Rows.Count > 1)
                        {
                            DataRow[] rowIMEI = dtbData.Select("ALLOCATETYPE > 0");
                            for (int i = 0; i < rowIMEI.Count(); i++)
                            {
                                var objImei = objDetail.PriceProtectDetail_ImeiList.FirstOrDefault(r => r.ProductIMEI.Trim() == rowIMEI[i]["IMEI"].ToString().Trim());
                                if (objImei != null)
                                {
                                    objImei.AllocateTypeID = Convert.ToInt32(rowIMEI[i]["ALLOCATETYPE"]);
                                    objImei.StoreID = Convert.ToInt32(rowIMEI[i]["STOREID"]);
                                    objImei.StoreName = rowIMEI[i]["STORENAME"].ToString();
                                    objImei.OriginalCostPrice = Convert.ToDecimal(rowIMEI[i]["COSTPRICE"]);
                                    objImei.LastInputVoucherDetailID = rowIMEI[i]["INPUTVOUCHERDETAILID"].ToString();
                                    //--1. Bán tại trung tâm, 2. Tồn tại trung tâm, 3. Tồn tại tỉnh, 4. Bán tại tỉnh
                                    switch (objImei.AllocateTypeID)
                                    {
                                        case 1:
                                            objDetail.QuantityBuy += 1;
                                            break;
                                        case 2:
                                            objDetail.QuantityStock += 1;
                                            break;
                                        case 3:
                                            objDetail.QuantityInStockNonIsCenter += 1;
                                            break;
                                        case 4:
                                            objDetail.QuantityBuyNonIsCenter += 1;
                                            break;
                                    }
                                }
                            }
                        }
                    }                    
                }
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tính số lượng phân bổ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> CalculatorQuantity", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private void CalculatorQuantity(IData objIData, ref DataTable dtbInStock, string strPriceProtectDetailID, object[] objKeywords)
        {
            dtbInStock = null;
            try
            {
                //objIData.CreateNewStoredProcedure("CALQUANTITY_AGAIN_TYPESTORE");
                objIData.CreateNewStoredProcedure("CALIMEI_AGAIN_TYPESTORE");
                objIData.AddParameter(objKeywords);
                objIData.AddParameter("@PRICEPROTECTDETAILID", strPriceProtectDetailID.Trim());
                dtbInStock = objIData.ExecStoreToDataTable();
                
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Tạo hóa đơn giảm trừ nội bộ hỗ trợ giá
        /// </summary>
        /// <param name="strPriceProtectID"></param>
        /// <param name="strCreatedUser"></param>
        /// <param name="intOutInvoiceTransTypeID"></param>
        /// <param name="intInInvoiceTransTypeID"></param>
        /// <returns></returns>
        public ResultMessage CreateInternalInvoice(string strPriceProtectID, string strCreatedUser, int intOutInvoiceTransTypeID, int intInInvoiceTransTypeID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                CreateInternalInvoice(objIData, strPriceProtectID, strCreatedUser, intOutInvoiceTransTypeID, intInInvoiceTransTypeID);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi tạo hóa đơn giảm trừ nội bộ hỗ trợ giá", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> CreateInternalInvoice", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tạo hóa đơn giảm trừ nội bộ hỗ trợ giá
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="strPriceProtectID"></param>
        /// <param name="strCreatedUser"></param>
        /// <param name="intOutInvoiceTransTypeID"></param>
        /// <param name="intInInvoiceTransTypeID"></param>
        public void CreateInternalInvoice(IData objIData, string strPriceProtectID, string strCreatedUser, int intOutInvoiceTransTypeID, int intInInvoiceTransTypeID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("VAT_INVOICE_PRICEPROTECTAUTO");
                objIData.AddParameter("@" + PriceProtect.colPriceProtectID, strPriceProtectID);
                objIData.AddParameter("@" + PriceProtect.colCreatedUser, strCreatedUser); 
                objIData.AddParameter("@OUTINVOICETRANSTYPEID", intOutInvoiceTransTypeID);
                objIData.AddParameter("@ININVOICETRANSTYPEID", intInInvoiceTransTypeID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Tạo phiếu nhập xuất điều chỉnh giá vốn
        /// </summary>
        /// <param name="strPriceProtectID"></param>
        /// <param name="strCreatedUser"></param>
        /// <param name="intOutputTypeID"></param>
        /// <param name="intInputTypeID"></param>
        /// <returns></returns>
        public ResultMessage CreateAdjustVoucher(string strPriceProtectID, string strCreatedUser, int intOutputTypeID, int intInputTypeID, DateTime dteVoucherDate)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                CreateAdjustVoucher(objIData, strPriceProtectID, strCreatedUser, intOutputTypeID, intInputTypeID, dteVoucherDate);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi tạo chứng từ giảm trừ giá vốn hỗ trợ giá", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> CreateAdjustVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="strPriceProtectID"></param>
        /// <param name="strCreatedUser"></param>
        /// <param name="intOutputTypeID"></param>
        /// <param name="intInputTypeID"></param>
        public void CreateAdjustVoucher(IData objIData, string strPriceProtectID, string strCreatedUser, int intOutputTypeID, int intInputTypeID, DateTime dteVoucherDate)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_PRICEPROTECT_ADJUSTVOUCHER");
                objIData.AddParameter("@" + PriceProtect.colPriceProtectID, strPriceProtectID);
                objIData.AddParameter("@" + PriceProtect.colCreatedUser, strCreatedUser);
                objIData.AddParameter("@" + PriceProtect.colVoucherDate, dteVoucherDate);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@InputTypeID", intInputTypeID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public ResultMessage Appove(string strPriceProtectID,int intIsApproved, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_PRICEPROTECT_APPROVE");
                objIData.AddParameter("@" + PriceProtect.colPriceProtectID, strPriceProtectID);
                objIData.AddParameter("@" + PriceProtect.colIsApproved, intIsApproved);
                objIData.AddParameter("@" + PriceProtect.colApprovedUser, strUserName);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi duyệt phiếu bảo vệ giá", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_PriceProtect -> Appove", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
    }
}
