
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using IMEIExcludeFIFORequest = ERP.Inventory.BO.PM.IMEIExcludeFIFORequest;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 06/20/2018 
	/// Yêu cầu IMEI không FIFO
	/// </summary>	
	public partial class DA_IMEIExcludeFIFORequest
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIMEIExcludeFIFORequest">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref IMEIExcludeFIFORequest objIMEIExcludeFIFORequest, string strIMEIExcludeFIFORequestID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colIMEIExcludeFIFORequestID, strIMEIExcludeFIFORequestID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objIMEIExcludeFIFORequest = new IMEIExcludeFIFORequest();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colIMEIExcludeFIFORequestID])) objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID = Convert.ToString(reader[IMEIExcludeFIFORequest.colIMEIExcludeFIFORequestID]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colNote])) objIMEIExcludeFIFORequest.Note = Convert.ToString(reader[IMEIExcludeFIFORequest.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colRequestUser])) objIMEIExcludeFIFORequest.RequestUser = Convert.ToString(reader[IMEIExcludeFIFORequest.colRequestUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colRequestStoreID])) objIMEIExcludeFIFORequest.RequestStoreID = Convert.ToInt32(reader[IMEIExcludeFIFORequest.colRequestStoreID]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colCreatedStoreID])) objIMEIExcludeFIFORequest.CreatedStoreID = Convert.ToInt32(reader[IMEIExcludeFIFORequest.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colCreatedUser])) objIMEIExcludeFIFORequest.CreatedUser = Convert.ToString(reader[IMEIExcludeFIFORequest.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colCreatedDate])) objIMEIExcludeFIFORequest.CreatedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequest.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colUpdatedUser])) objIMEIExcludeFIFORequest.UpdatedUser = Convert.ToString(reader[IMEIExcludeFIFORequest.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colUpdatedDate])) objIMEIExcludeFIFORequest.UpdatedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequest.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colIsDeleted])) objIMEIExcludeFIFORequest.IsDeleted = Convert.ToBoolean(reader[IMEIExcludeFIFORequest.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colDeletedUser])) objIMEIExcludeFIFORequest.DeletedUser = Convert.ToString(reader[IMEIExcludeFIFORequest.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colDeletedDate])) objIMEIExcludeFIFORequest.DeletedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequest.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colReviewedNote])) objIMEIExcludeFIFORequest.ReviewedNote = Convert.ToString(reader[IMEIExcludeFIFORequest.colReviewedNote]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colReviewedUser])) objIMEIExcludeFIFORequest.ReviewedUser = Convert.ToString(reader[IMEIExcludeFIFORequest.colReviewedUser]).Trim();
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colReviewedDate])) objIMEIExcludeFIFORequest.ReviewedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequest.colReviewedDate]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colReviewedStatus])) objIMEIExcludeFIFORequest.ReviewedStatus = Convert.ToInt32(reader[IMEIExcludeFIFORequest.colReviewedStatus]);
 					if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colDeletedReason])) objIMEIExcludeFIFORequest.DeletedReason = Convert.ToString(reader[IMEIExcludeFIFORequest.colDeletedReason]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequest.colIMEIExcludeFIFORequestTypeID])) objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestTypeID = Convert.ToInt32(reader[IMEIExcludeFIFORequest.colIMEIExcludeFIFORequestTypeID]);

                    objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList = new DA_IMEIExcludeFIFORequestDT().SearchDataToList(objIData, new object[] { "@IMEIEXCLUDEFIForeQUESTID", objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID });
                
                }
 				else
 				{
 					objIMEIExcludeFIFORequest = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin yêu cầu imei không fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIExcludeFIFORequest -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIMEIExcludeFIFORequest">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
				Insert(objIData, objIMEIExcludeFIFORequest);

                if(objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList != null)
                {
                    DA_IMEIExcludeFIFORequestDT objDA_IMEIExcludeFIFORequestDT = new DA_IMEIExcludeFIFORequestDT();
                    objDA_IMEIExcludeFIFORequestDT.objLogObject.CertificateString = objLogObject.CertificateString;
                    objDA_IMEIExcludeFIFORequestDT.objLogObject.UserHostAddress = objLogObject.UserHostAddress;
                    objDA_IMEIExcludeFIFORequestDT.objLogObject.LoginLogID = objLogObject.LoginLogID;
                    foreach(ERP.Inventory.BO.PM.IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT in objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList)
                    {
                        objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestID = objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID;
                        objIMEIExcludeFIFORequestDT.CreatedUser = objIMEIExcludeFIFORequest.CreatedUser;
                        objDA_IMEIExcludeFIFORequestDT.Insert(objIData, objIMEIExcludeFIFORequestDT);
                    }
                }

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin yêu cầu imei không fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIExcludeFIFORequest -> Insert", InventoryGlobals.ModuleName);
                objIData.RollBackTransaction();
                return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEIExcludeFIFORequest">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colNote, objIMEIExcludeFIFORequest.Note);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colRequestUser, objIMEIExcludeFIFORequest.RequestUser);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colRequestStoreID, objIMEIExcludeFIFORequest.RequestStoreID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colCreatedStoreID, objIMEIExcludeFIFORequest.CreatedStoreID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colCreatedUser, objIMEIExcludeFIFORequest.CreatedUser);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colReviewedNote, objIMEIExcludeFIFORequest.ReviewedNote);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colReviewedUser, objIMEIExcludeFIFORequest.ReviewedUser);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colReviewedDate, objIMEIExcludeFIFORequest.ReviewedDate);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colReviewedStatus, objIMEIExcludeFIFORequest.ReviewedStatus);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colDeletedReason, objIMEIExcludeFIFORequest.DeletedReason);
                objIData.AddParameter("@" + IMEIExcludeFIFORequest.colIMEIExcludeFIFORequestTypeID, objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestTypeID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID = objIData.ExecStoreToString();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIMEIExcludeFIFORequest">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
				Update(objIData, objIMEIExcludeFIFORequest);

                DA_IMEIExcludeFIFORequestDT objDA_IMEIExcludeFIFORequestDT = new DA_IMEIExcludeFIFORequestDT();
                objDA_IMEIExcludeFIFORequestDT.objLogObject.CertificateString = objLogObject.CertificateString;
                objDA_IMEIExcludeFIFORequestDT.objLogObject.UserHostAddress = objLogObject.UserHostAddress;
                objDA_IMEIExcludeFIFORequestDT.objLogObject.LoginLogID = objLogObject.LoginLogID;

                if (objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList != null)
                {
                    foreach (ERP.Inventory.BO.PM.IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT in objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList)
                    {
                        if (string.IsNullOrEmpty(objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestDTID))
                        {
                            objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestID = objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID;
                            objIMEIExcludeFIFORequestDT.CreatedUser = objIMEIExcludeFIFORequest.CreatedUser;
                            objDA_IMEIExcludeFIFORequestDT.Insert(objIData, objIMEIExcludeFIFORequestDT);
                        }
                        else
                        {
                            objIMEIExcludeFIFORequestDT.UpdatedUser = objIMEIExcludeFIFORequest.UpdatedUser;
                            objDA_IMEIExcludeFIFORequestDT.Update(objIData, objIMEIExcludeFIFORequestDT);
                        }
                    }
                }

                if(objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDT_Del_List != null)
                {
                    foreach (ERP.Inventory.BO.PM.IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT in objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDT_Del_List)
                    {
                        objIMEIExcludeFIFORequestDT.DeletedUser = objIMEIExcludeFIFORequest.UpdatedUser;
                        objDA_IMEIExcludeFIFORequestDT.Delete(objIData, objIMEIExcludeFIFORequestDT);
                    }
                }

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin yêu cầu imei không fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIExcludeFIFORequest -> Update", InventoryGlobals.ModuleName);
                objIData.RollBackTransaction();
                return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEIExcludeFIFORequest">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colIMEIExcludeFIFORequestID, objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colNote, objIMEIExcludeFIFORequest.Note);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colRequestUser, objIMEIExcludeFIFORequest.RequestUser);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colRequestStoreID, objIMEIExcludeFIFORequest.RequestStoreID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colCreatedStoreID, objIMEIExcludeFIFORequest.CreatedStoreID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colUpdatedUser, objIMEIExcludeFIFORequest.UpdatedUser);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colReviewedNote, objIMEIExcludeFIFORequest.ReviewedNote);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colReviewedUser, objIMEIExcludeFIFORequest.ReviewedUser);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colReviewedDate, objIMEIExcludeFIFORequest.ReviewedDate);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colReviewedStatus, objIMEIExcludeFIFORequest.ReviewedStatus);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colDeletedReason, objIMEIExcludeFIFORequest.DeletedReason);
                objIData.AddParameter("@" + IMEIExcludeFIFORequest.colIMEIExcludeFIFORequestTypeID, objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestTypeID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIMEIExcludeFIFORequest">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
				Delete(objIData, objIMEIExcludeFIFORequest);

                DA_IMEIExcludeFIFORequestDT objDA_IMEIExcludeFIFORequestDT = new DA_IMEIExcludeFIFORequestDT();
                objDA_IMEIExcludeFIFORequestDT.objLogObject.CertificateString = objLogObject.CertificateString;
                objDA_IMEIExcludeFIFORequestDT.objLogObject.UserHostAddress = objLogObject.UserHostAddress;
                objDA_IMEIExcludeFIFORequestDT.objLogObject.LoginLogID = objLogObject.LoginLogID;
                if (objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList != null)
                {
                    foreach (ERP.Inventory.BO.PM.IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT in objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList)
                    {
                        objIMEIExcludeFIFORequestDT.DeletedUser = objIMEIExcludeFIFORequest.DeletedUser;
                        objDA_IMEIExcludeFIFORequestDT.Delete(objIData, objIMEIExcludeFIFORequestDT);
                    }
                }

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin yêu cầu imei không fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIExcludeFIFORequest -> Delete", InventoryGlobals.ModuleName);
                objIData.RollBackTransaction();
                return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin yêu cầu imei không fifo
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objIMEIExcludeFIFORequest">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colIMEIExcludeFIFORequestID, objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID);
				objIData.AddParameter("@" + IMEIExcludeFIFORequest.colDeletedUser, objIMEIExcludeFIFORequest.DeletedUser);
                objIData.AddParameter("@" + IMEIExcludeFIFORequest.colDeletedReason, objIMEIExcludeFIFORequest.DeletedReason);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion
		
		#region Constructor

		public DA_IMEIExcludeFIFORequest()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_IMEIExcludeFIFORequest_ADD";
		public const String SP_UPDATE = "PM_IMEIExcludeFIFORequest_UPD";
		public const String SP_DELETE = "PM_IMEIExcludeFIFORequest_DEL";
		public const String SP_SELECT = "PM_IMEIExcludeFIFORequest_SEL";
		public const String SP_SEARCH = "PM_IMEIExcludeFIFORequest_SRH";
		#endregion
		
	}
}
