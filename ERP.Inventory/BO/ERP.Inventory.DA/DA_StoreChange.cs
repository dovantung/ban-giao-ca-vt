
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using StoreChange = ERP.Inventory.BO.StoreChange;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 10/1/2012 
	/// Bảng xuất chuyển kho
	/// </summary>	
	public partial class DA_StoreChange
	{
        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion
		#region Methods			
		
		/// <summary>
		/// Nạp thông tin bảng xuất chuyển kho
		/// </summary>
		/// <param name="objStoreChange">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref StoreChange objStoreChange, string strStoreChangeID = null, string strOutputVoucherID = null, string strInputVoucherID = null, DateTime? storechangedate = null)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + StoreChange.colStoreChangeID, strStoreChangeID);
                objIData.AddParameter("@" + StoreChange.colOutputVoucherID, strOutputVoucherID);
                objIData.AddParameter("@" + StoreChange.colInputVoucherID, strInputVoucherID);
                objIData.AddParameter("@" + StoreChange.colStoreChangeDate, storechangedate);
                IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objStoreChange = new StoreChange();
 					if (!Convert.IsDBNull(reader[StoreChange.colStoreChangeID])) objStoreChange.StoreChangeID = Convert.ToString(reader[StoreChange.colStoreChangeID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colStoreChangeOrderID])) objStoreChange.StoreChangeOrderID = Convert.ToString(reader[StoreChange.colStoreChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colStoreChangeTypeID])) objStoreChange.StoreChangeTypeID = Convert.ToInt32(reader[StoreChange.colStoreChangeTypeID]);
 					if (!Convert.IsDBNull(reader[StoreChange.colTransportTypeID])) objStoreChange.TransportTypeID = Convert.ToInt32(reader[StoreChange.colTransportTypeID]);
 					if (!Convert.IsDBNull(reader[StoreChange.colFromStoreID])) objStoreChange.FromStoreID = Convert.ToInt32(reader[StoreChange.colFromStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChange.colToStoreID])) objStoreChange.ToStoreID = Convert.ToInt32(reader[StoreChange.colToStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChange.colCreatedStoreID])) objStoreChange.CreatedStoreID = Convert.ToInt32(reader[StoreChange.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChange.colStoreChangeDate])) objStoreChange.StoreChangeDate = Convert.ToDateTime(reader[StoreChange.colStoreChangeDate]);
 					if (!Convert.IsDBNull(reader[StoreChange.colIsNew])) objStoreChange.IsNew = Convert.ToBoolean(reader[StoreChange.colIsNew]);
                    if (!Convert.IsDBNull(reader[StoreChange.colInStockStatusID])) objStoreChange.InStockStatusID = Convert.ToInt32(reader[StoreChange.colInStockStatusID]);
 					if (!Convert.IsDBNull(reader[StoreChange.colInvoiceID])) objStoreChange.InvoiceID = Convert.ToString(reader[StoreChange.colInvoiceID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colCASKCode])) objStoreChange.CaskCode = Convert.ToString(reader[StoreChange.colCASKCode]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colTotalPacking])) objStoreChange.TotalPacking = Convert.ToInt32(reader[StoreChange.colTotalPacking]);
 					if (!Convert.IsDBNull(reader[StoreChange.colToUser1])) objStoreChange.ToUser1 = Convert.ToString(reader[StoreChange.colToUser1]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colToUser2])) objStoreChange.ToUser2 = Convert.ToString(reader[StoreChange.colToUser2]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colTotalWEIGHT])) objStoreChange.TotalWeight = Convert.ToDecimal(reader[StoreChange.colTotalWEIGHT]);
 					if (!Convert.IsDBNull(reader[StoreChange.colTotalSize])) objStoreChange.TotalSize = Convert.ToDecimal(reader[StoreChange.colTotalSize]);
 					if (!Convert.IsDBNull(reader[StoreChange.colTotalShippingCost])) objStoreChange.TotalShippingCost = Convert.ToDecimal(reader[StoreChange.colTotalShippingCost]);
 					if (!Convert.IsDBNull(reader[StoreChange.colTransportVoucherID])) objStoreChange.TransportVoucherID = Convert.ToString(reader[StoreChange.colTransportVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colContent])) objStoreChange.Content = Convert.ToString(reader[StoreChange.colContent]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colStoreChangeUser])) objStoreChange.StoreChangeUser = Convert.ToString(reader[StoreChange.colStoreChangeUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colOutputVoucherID])) objStoreChange.OutputVoucherID = Convert.ToString(reader[StoreChange.colOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colInputVoucherID])) objStoreChange.InputVoucherID = Convert.ToString(reader[StoreChange.colInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colINVoucherID])) objStoreChange.InVoucherID = Convert.ToString(reader[StoreChange.colINVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colOutVoucherID])) objStoreChange.OutVoucherID = Convert.ToString(reader[StoreChange.colOutVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colISReceive])) objStoreChange.IsReceive = Convert.ToBoolean(reader[StoreChange.colISReceive]);
 					if (!Convert.IsDBNull(reader[StoreChange.colReceiveNote])) objStoreChange.ReceiveNote = Convert.ToString(reader[StoreChange.colReceiveNote]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colUserReceive])) objStoreChange.UserReceive = Convert.ToString(reader[StoreChange.colUserReceive]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colDateReceive])) objStoreChange.DateReceive = Convert.ToDateTime(reader[StoreChange.colDateReceive]);
 					if (!Convert.IsDBNull(reader[StoreChange.colISURGENT])) objStoreChange.IsUrgent = Convert.ToBoolean(reader[StoreChange.colISURGENT]);
 					if (!Convert.IsDBNull(reader[StoreChange.colIsTransferED])) objStoreChange.IsTransfered = Convert.ToBoolean(reader[StoreChange.colIsTransferED]);
 					if (!Convert.IsDBNull(reader[StoreChange.colTransferEDDate])) objStoreChange.TransferedDate = Convert.ToDateTime(reader[StoreChange.colTransferEDDate]);
 					if (!Convert.IsDBNull(reader[StoreChange.colTransferEDUser])) objStoreChange.TransferedUser = Convert.ToString(reader[StoreChange.colTransferEDUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colCreatedUser])) objStoreChange.CreatedUser = Convert.ToString(reader[StoreChange.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colCreatedDate])) objStoreChange.CreatedDate = Convert.ToDateTime(reader[StoreChange.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChange.colUpdatedUser])) objStoreChange.UpdatedUser = Convert.ToString(reader[StoreChange.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colUpdatedDate])) objStoreChange.UpdatedDate = Convert.ToDateTime(reader[StoreChange.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChange.colIsDeleted])) objStoreChange.IsDeleted = Convert.ToBoolean(reader[StoreChange.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[StoreChange.colDeletedUser])) objStoreChange.DeletedUser = Convert.ToString(reader[StoreChange.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChange.colDeletedDate])) objStoreChange.DeletedDate = Convert.ToDateTime(reader[StoreChange.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[StoreChange.colIsReceivedTransfer])) objStoreChange.IsReceivedTransfer = Convert.ToBoolean(reader[StoreChange.colIsReceivedTransfer]);
                    if (!Convert.IsDBNull(reader[StoreChange.colReceivedTransferUser])) objStoreChange.ReceivedTransferUser = Convert.ToString(reader[StoreChange.colReceivedTransferUser]);
                    if (!Convert.IsDBNull(reader[StoreChange.colReceivedTransferDate])) objStoreChange.ReceivedTransferDate = Convert.ToDateTime(reader[StoreChange.colReceivedTransferDate]);
                    if (!Convert.IsDBNull(reader[StoreChange.colIsSignReceive])) objStoreChange.IsSignReceive = Convert.ToBoolean(reader[StoreChange.colIsSignReceive]);
                    if (!Convert.IsDBNull(reader[StoreChange.colSignReceiveUser])) objStoreChange.SignReceiveUser = Convert.ToString(reader[StoreChange.colSignReceiveUser]);
                    if (!Convert.IsDBNull(reader[StoreChange.colSignReceiveDate])) objStoreChange.SignReceiveDate = Convert.ToDateTime(reader[StoreChange.colSignReceiveDate]);
                    if (!Convert.IsDBNull(reader[StoreChange.colSignReceiveNote])) objStoreChange.SignReceiveNote = Convert.ToString(reader[StoreChange.colSignReceiveNote]);
                    if (!Convert.IsDBNull(reader[StoreChange.colInvoiceSymbol])) objStoreChange.InvoiceSymbol = Convert.ToString(reader[StoreChange.colInvoiceSymbol]);
                    if(!Convert.IsDBNull(reader["STORECHANGEUSERFULLNAME"])) objStoreChange.StoreChangeUserFullName = Convert.ToString(reader["STORECHANGEUSERFULLNAME"]);
                    if (!Convert.IsDBNull(reader["USERRECEIVEFULLNAME"])) objStoreChange.UserReciveFullName = Convert.ToString(reader["USERRECEIVEFULLNAME"]);
                    if (!Convert.IsDBNull(reader["TRANSFEREDUSERFULLNAME"])) objStoreChange.TransferUserFullName = Convert.ToString(reader["TRANSFEREDUSERFULLNAME"]);
                    if (!Convert.IsDBNull(reader["RECEIVEDTRANSFERUSERFULLNAME"])) objStoreChange.RecivedTransferUserFullName = Convert.ToString(reader["RECEIVEDTRANSFERUSERFULLNAME"]);
                    if (!Convert.IsDBNull(reader["SIGNRECEIVEUSERFULLNAME"])) objStoreChange.SignReciveUserFullName = Convert.ToString(reader["SIGNRECEIVEUSERFULLNAME"]);

                }
 				else
 				{
 					objStoreChange = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin bảng xuất chuyển kho", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin bảng xuất chuyển kho
		/// </summary>
		/// <param name="objStoreChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(StoreChange objStoreChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				//Insert(objIData, objStoreChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin bảng xuất chuyển kho", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin bảng xuất chuyển kho
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, StoreChange objStoreChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + StoreChange.colStoreChangeID, objStoreChange.StoreChangeID);
				objIData.AddParameter("@" + StoreChange.colStoreChangeOrderID, objStoreChange.StoreChangeOrderID);
				objIData.AddParameter("@" + StoreChange.colStoreChangeTypeID, objStoreChange.StoreChangeTypeID);
				objIData.AddParameter("@" + StoreChange.colTransportTypeID, objStoreChange.TransportTypeID);
				objIData.AddParameter("@" + StoreChange.colFromStoreID, objStoreChange.FromStoreID);
				objIData.AddParameter("@" + StoreChange.colToStoreID, objStoreChange.ToStoreID);
				objIData.AddParameter("@" + StoreChange.colCreatedStoreID, objStoreChange.CreatedStoreID);
				objIData.AddParameter("@" + StoreChange.colStoreChangeDate, objStoreChange.StoreChangeDate);
				objIData.AddParameter("@" + StoreChange.colIsNew, objStoreChange.IsNew);
                objIData.AddParameter("@" + StoreChange.colInStockStatusID, objStoreChange.InStockStatusID);
				objIData.AddParameter("@" + StoreChange.colInvoiceID, objStoreChange.InvoiceID);
				objIData.AddParameter("@" + StoreChange.colCASKCode, objStoreChange.CaskCode);
				objIData.AddParameter("@" + StoreChange.colTotalPacking, objStoreChange.TotalPacking);
				objIData.AddParameter("@" + StoreChange.colToUser1, objStoreChange.ToUser1);
				objIData.AddParameter("@" + StoreChange.colToUser2, objStoreChange.ToUser2);
				objIData.AddParameter("@" + StoreChange.colTotalWEIGHT, objStoreChange.TotalWeight);
				objIData.AddParameter("@" + StoreChange.colTotalSize, objStoreChange.TotalSize);
				objIData.AddParameter("@" + StoreChange.colTotalShippingCost, objStoreChange.TotalShippingCost);
				objIData.AddParameter("@" + StoreChange.colTransportVoucherID, objStoreChange.TransportVoucherID);
				objIData.AddParameter("@" + StoreChange.colContent, objStoreChange.Content);
				objIData.AddParameter("@" + StoreChange.colStoreChangeUser, objStoreChange.StoreChangeUser);
				objIData.AddParameter("@" + StoreChange.colOutputVoucherID, objStoreChange.OutputVoucherID);
				objIData.AddParameter("@" + StoreChange.colInputVoucherID, objStoreChange.InputVoucherID);
				objIData.AddParameter("@" + StoreChange.colINVoucherID, objStoreChange.InVoucherID);
				objIData.AddParameter("@" + StoreChange.colOutVoucherID, objStoreChange.OutVoucherID);
				objIData.AddParameter("@" + StoreChange.colISReceive, objStoreChange.IsReceive);
				objIData.AddParameter("@" + StoreChange.colReceiveNote, objStoreChange.ReceiveNote);
				objIData.AddParameter("@" + StoreChange.colUserReceive, objStoreChange.UserReceive);
				objIData.AddParameter("@" + StoreChange.colDateReceive, objStoreChange.DateReceive);
				objIData.AddParameter("@" + StoreChange.colISURGENT, objStoreChange.IsUrgent);
				objIData.AddParameter("@" + StoreChange.colIsTransferED, objStoreChange.IsTransfered);
				objIData.AddParameter("@" + StoreChange.colTransferEDDate, objStoreChange.TransferedDate);
				objIData.AddParameter("@" + StoreChange.colTransferEDUser, objStoreChange.TransferedUser);
				objIData.AddParameter("@" + StoreChange.colCreatedUser, objStoreChange.CreatedUser);
                objIData.AddParameter("@" + StoreChange.colInvoiceSymbol, objStoreChange.InvoiceSymbol);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin bảng xuất chuyển kho
		/// </summary>
		/// <param name="objStoreChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(StoreChange objStoreChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objStoreChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin bảng xuất chuyển kho", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin bảng xuất chuyển kho
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, StoreChange objStoreChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + StoreChange.colStoreChangeID, objStoreChange.StoreChangeID);
				objIData.AddParameter("@" + StoreChange.colStoreChangeOrderID, objStoreChange.StoreChangeOrderID);
				objIData.AddParameter("@" + StoreChange.colStoreChangeTypeID, objStoreChange.StoreChangeTypeID);
				objIData.AddParameter("@" + StoreChange.colTransportTypeID, objStoreChange.TransportTypeID);
				objIData.AddParameter("@" + StoreChange.colFromStoreID, objStoreChange.FromStoreID);
				objIData.AddParameter("@" + StoreChange.colToStoreID, objStoreChange.ToStoreID);
				objIData.AddParameter("@" + StoreChange.colCreatedStoreID, objStoreChange.CreatedStoreID);
				objIData.AddParameter("@" + StoreChange.colStoreChangeDate, objStoreChange.StoreChangeDate);
				objIData.AddParameter("@" + StoreChange.colIsNew, objStoreChange.IsNew);
                objIData.AddParameter("@" + StoreChange.colInStockStatusID, objStoreChange.InStockStatusID);
				objIData.AddParameter("@" + StoreChange.colInvoiceID, objStoreChange.InvoiceID);
				objIData.AddParameter("@" + StoreChange.colCASKCode, objStoreChange.CaskCode);
				objIData.AddParameter("@" + StoreChange.colTotalPacking, objStoreChange.TotalPacking);
				objIData.AddParameter("@" + StoreChange.colToUser1, objStoreChange.ToUser1);
				objIData.AddParameter("@" + StoreChange.colToUser2, objStoreChange.ToUser2);
				objIData.AddParameter("@" + StoreChange.colTotalWEIGHT, objStoreChange.TotalWeight);
				objIData.AddParameter("@" + StoreChange.colTotalSize, objStoreChange.TotalSize);
				objIData.AddParameter("@" + StoreChange.colTotalShippingCost, objStoreChange.TotalShippingCost);
				objIData.AddParameter("@" + StoreChange.colTransportVoucherID, objStoreChange.TransportVoucherID);
				objIData.AddParameter("@" + StoreChange.colContent, objStoreChange.Content);
				objIData.AddParameter("@" + StoreChange.colStoreChangeUser, objStoreChange.StoreChangeUser);
				objIData.AddParameter("@" + StoreChange.colOutputVoucherID, objStoreChange.OutputVoucherID);
				objIData.AddParameter("@" + StoreChange.colInputVoucherID, objStoreChange.InputVoucherID);
				objIData.AddParameter("@" + StoreChange.colINVoucherID, objStoreChange.InVoucherID);
				objIData.AddParameter("@" + StoreChange.colOutVoucherID, objStoreChange.OutVoucherID);
				objIData.AddParameter("@" + StoreChange.colISReceive, objStoreChange.IsReceive);
				objIData.AddParameter("@" + StoreChange.colReceiveNote, objStoreChange.ReceiveNote);
				objIData.AddParameter("@" + StoreChange.colUserReceive, objStoreChange.UserReceive);
				objIData.AddParameter("@" + StoreChange.colDateReceive, objStoreChange.DateReceive);
				objIData.AddParameter("@" + StoreChange.colISURGENT, objStoreChange.IsUrgent);
				objIData.AddParameter("@" + StoreChange.colIsTransferED, objStoreChange.IsTransfered);
				objIData.AddParameter("@" + StoreChange.colTransferEDDate, objStoreChange.TransferedDate);
				objIData.AddParameter("@" + StoreChange.colTransferEDUser, objStoreChange.TransferedUser);
				objIData.AddParameter("@" + StoreChange.colUpdatedUser, objStoreChange.UpdatedUser);
                objIData.AddParameter("@" + StoreChange.colUpdatedUser, objStoreChange.UpdatedUser);
                objIData.AddParameter("@" + StoreChange.colIsReceivedTransfer, objStoreChange.IsReceivedTransfer);
                objIData.AddParameter("@" + StoreChange.colReceivedTransferUser, objStoreChange.ReceivedTransferUser);
                objIData.AddParameter("@" + StoreChange.colReceivedTransferDate, objStoreChange.ReceivedTransferDate);
                objIData.AddParameter("@" + StoreChange.colIsReceivedCheckinByFinger, objStoreChange.IsReceivedCheckinByFinger);
                objIData.AddParameter("@" + StoreChange.colInvoiceSymbol, objStoreChange.InvoiceSymbol);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin bảng xuất chuyển kho
		/// </summary>
		/// <param name="objStoreChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(StoreChange objStoreChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objStoreChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin bảng xuất chuyển kho", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin bảng xuất chuyển kho
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, StoreChange objStoreChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + StoreChange.colStoreChangeID, objStoreChange.StoreChangeID);
				objIData.AddParameter("@" + StoreChange.colDeletedUser, objStoreChange.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_StoreChange()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_STORECHANGE_ADD";
		public const String SP_UPDATE = "PM_STORECHANGE_UPD";
		public const String SP_DELETE = "PM_STORECHANGE_DEL";
		public const String SP_SELECT = "PM_STORECHANGE_SEL";
		public const String SP_SEARCH = "PM_STORECHANGE_SRH";
		public const String SP_UPDATEINDEX = "PM_STORECHANGE_UPDINDEX";
		#endregion
		
	}
}
