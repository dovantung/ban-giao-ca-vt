
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using IMEISalesInfo_ProSpec = ERP.Inventory.BO.IMEISalesInfo_ProSpec;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Quang Tú 
	/// Created date 	: 17/06/2013 
	/// Thông tin về thuộc tính sản phẩm
	/// </summary>	
	public partial class DA_IMEISalesInfo_ProSpec
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_IMEISalesInfo_ProSpec.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin thông tin về thuộc tính sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ProSpec -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}
        /// <summary>
		/// Tìm kiếm thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchDataSpecStatus(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_IMEISALES_SPECSTATUS_SRH");
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin trạng thái của sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ProSpec -> SearchDataSpecStatus", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}        
        /// <summary>
		/// Tìm kiếm thông tin thông tin về thuộc tính sản phẩm
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchDataSpec(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_IMEISALES_PROSPEC_SRH");
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin trạng thái của sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ProSpec -> SearchDataSpec", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}


        /// <summary>
        /// Tìm kiếm thông tin dạng list
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataToList(ref List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                lstIMEISalesInfo_ProSpec = SearchDataToList(objIData, objKeywords);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_PROSPEC -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objIMEISalesInfo_ProSpec">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public List<IMEISalesInfo_ProSpec> SearchDataToList(IData objIData, params object[] objKeywords)
        {
            List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec = new List<IMEISalesInfo_ProSpec>();
            try
            {
                objIData.CreateNewStoredProcedure("PM_IMEISALESINFO_PRSPEC_SRH");
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec = new IMEISalesInfo_ProSpec();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colProductID])) objIMEISalesInfo_ProSpec.ProductID = Convert.ToString(reader[IMEISalesInfo_ProSpec.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colIMEI])) objIMEISalesInfo_ProSpec.IMEI = Convert.ToString(reader[IMEISalesInfo_ProSpec.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colProductSpecID])) objIMEISalesInfo_ProSpec.ProductSpecID = Convert.ToInt32(reader[IMEISalesInfo_ProSpec.colProductSpecID]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colProductSpecStatusID])) objIMEISalesInfo_ProSpec.ProductSpecStatusID = Convert.ToInt32(reader[IMEISalesInfo_ProSpec.colProductSpecStatusID]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colNote])) objIMEISalesInfo_ProSpec.Note = Convert.ToString(reader[IMEISalesInfo_ProSpec.colNote]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colProductSpecName])) objIMEISalesInfo_ProSpec.ProductSpecName = Convert.ToString(reader[IMEISalesInfo_ProSpec.colProductSpecName]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colSetPriceProductSpecStatusID])) objIMEISalesInfo_ProSpec.SetPriceProductSpecStatusID = Convert.ToInt32(reader[IMEISalesInfo_ProSpec.colSetPriceProductSpecStatusID]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colSetPriceProductSpecStatusName])) objIMEISalesInfo_ProSpec.SetPriceProductSpecStatusName = Convert.ToString(reader[IMEISalesInfo_ProSpec.colSetPriceProductSpecStatusName]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colIsUsedBySalesInfo])) objIMEISalesInfo_ProSpec.IsUsedBySalesInfo = Convert.ToBoolean(reader[IMEISalesInfo_ProSpec.colIsUsedBySalesInfo]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_ProSpec.colIsCanPrint])) objIMEISalesInfo_ProSpec.IsCanPrint = Convert.ToBoolean(reader[IMEISalesInfo_ProSpec.colIsCanPrint]);
                    objIMEISalesInfo_ProSpec.IsExist = true;
                    lstIMEISalesInfo_ProSpec.Add(objIMEISalesInfo_ProSpec);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                throw (objEx);
            }
            return lstIMEISalesInfo_ProSpec;
        }
		#endregion
		
		
	}
}
