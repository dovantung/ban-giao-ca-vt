
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
#endregion
namespace ERP.Inventory.DA.ProductChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/1/2012 
	/// 
	/// </summary>	
	public partial class DA_ProductChange
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objProductChange">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref ERP.Inventory.BO.ProductChange.ProductChange objProductChange, string strProductChangeID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeID, strProductChangeID);
				IDataReader reader = objIData.ExecStoreToDataReader();
                if (objProductChange == null)
                    objProductChange = new BO.ProductChange.ProductChange();
				if (reader.Read())
 				{
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeID])) objProductChange.ProductChangeID = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeID]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeOrderID])) objProductChange.ProductChangeOrderID = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colStoreID])) objProductChange.StoreID = Convert.ToInt32(reader[ERP.Inventory.BO.ProductChange.ProductChange.colStoreID]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colIsNew])) objProductChange.IsNew = Convert.ToBoolean(reader[ERP.Inventory.BO.ProductChange.ProductChange.colIsNew]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colOutputVoucherID])) objProductChange.OutputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colInputVoucherID])) objProductChange.InputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeUser])) objProductChange.ProductChangeUser = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colInVoiceDate])) objProductChange.InVoiceDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colInVoiceDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeDate])) objProductChange.ProductChangeDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colContent])) objProductChange.Content = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colContent]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedStoreID])) objProductChange.CreatedStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedUser])) objProductChange.CreatedUser = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedDate])) objProductChange.CreatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colUpdatedUser])) objProductChange.UpdatedUser = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colUpdatedDate])) objProductChange.UpdatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colIsDeleted])) objProductChange.IsDeleted = Convert.ToBoolean(reader[ERP.Inventory.BO.ProductChange.ProductChange.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colDeletedUser])) objProductChange.DeletedUser = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colDeletedDate])) objProductChange.DeletedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colContentDeleted])) objProductChange.ContentDeleted = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colContentDeleted]).Trim();
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChange -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objProductChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(ERP.Inventory.BO.ProductChange.ProductChange objProductChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objProductChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChange -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProductChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, ERP.Inventory.BO.ProductChange.ProductChange objProductChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeID, objProductChange.ProductChangeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeOrderID, objProductChange.ProductChangeOrderID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colStoreID, objProductChange.StoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colIsNew, objProductChange.IsNew);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colOutputVoucherID, objProductChange.OutputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colInputVoucherID, objProductChange.InputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeUser, objProductChange.ProductChangeUser);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colInVoiceDate, objProductChange.InVoiceDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeDate, objProductChange.ProductChangeDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colContent, objProductChange.Content);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colCreatedStoreID, objProductChange.CreatedStoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colCreatedUser, objProductChange.CreatedUser);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colContentDeleted, objProductChange.ContentDeleted);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objProductChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(ERP.Inventory.BO.ProductChange.ProductChange objProductChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objProductChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChange -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProductChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, ERP.Inventory.BO.ProductChange.ProductChange objProductChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeID, objProductChange.ProductChangeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeOrderID, objProductChange.ProductChangeOrderID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colStoreID, objProductChange.StoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colIsNew, objProductChange.IsNew);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colOutputVoucherID, objProductChange.OutputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colInputVoucherID, objProductChange.InputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeUser, objProductChange.ProductChangeUser);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colInVoiceDate, objProductChange.InVoiceDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeDate, objProductChange.ProductChangeDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colContent, objProductChange.Content);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colCreatedStoreID, objProductChange.CreatedStoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colUpdatedUser, objProductChange.UpdatedUser);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colContentDeleted, objProductChange.ContentDeleted);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objProductChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(ERP.Inventory.BO.ProductChange.ProductChange objProductChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objProductChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChange -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProductChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, ERP.Inventory.BO.ProductChange.ProductChange objProductChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeID, objProductChange.ProductChangeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.ProductChange.ProductChange.colDeletedUser, objProductChange.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_ProductChange()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_PRODUCTCHANGE_ADD";
		public const String SP_UPDATE = "PM_PRODUCTCHANGE_UPD";
		public const String SP_DELETE = "PM_PRODUCTCHANGE_DEL";
		public const String SP_SELECT = "PM_PRODUCTCHANGE_SEL";
		public const String SP_SEARCH = "PM_PRODUCTCHANGE_SRH";
		public const String SP_UPDATEINDEX = "PM_PRODUCTCHANGE_UPDINDEX";
		#endregion
		
	}
}
