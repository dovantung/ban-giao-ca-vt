
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using ProductChangeOrder_RVL = ERP.Inventory.BO.ProductChange.ProductChangeOrder_RVL;
#endregion
namespace ERP.Inventory.DA.ProductChange
{
    /// <summary>
	/// Created by 		: Phan Tiến Lộc 
	/// Created date 	: 2016-11-15 
	/// Yêu cầu xuất đổi - Mức duyệt
	/// </summary>	
	public partial class DA_ProductChangeOrder_RVL
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objProductChangeOrder_RVL">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref ProductChangeOrder_RVL objProductChangeOrder_RVL, string strProductChangeOrderRVLID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colProductChangeOrderRVLID, strProductChangeOrderRVLID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objProductChangeOrder_RVL = new ProductChangeOrder_RVL();
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colProductChangeOrderRVLID])) objProductChangeOrder_RVL.ProductChangeOrderRVLID = Convert.ToString(reader[ProductChangeOrder_RVL.colProductChangeOrderRVLID]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colProductChangeOrderID])) objProductChangeOrder_RVL.ProductChangeOrderID = Convert.ToString(reader[ProductChangeOrder_RVL.colProductChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colReviewLevelID])) objProductChangeOrder_RVL.ReviewLevelID = Convert.ToInt32(reader[ProductChangeOrder_RVL.colReviewLevelID]);
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colUserName])) objProductChangeOrder_RVL.UserName = Convert.ToString(reader[ProductChangeOrder_RVL.colUserName]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colReviewedStatus])) objProductChangeOrder_RVL.ReviewedStatus = Convert.ToInt32(reader[ProductChangeOrder_RVL.colReviewedStatus]);
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colReviewedDate])) objProductChangeOrder_RVL.ReviewedDate = Convert.ToDateTime(reader[ProductChangeOrder_RVL.colReviewedDate]);
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colNote])) objProductChangeOrder_RVL.Note = Convert.ToString(reader[ProductChangeOrder_RVL.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colOrderIndex])) objProductChangeOrder_RVL.OrderIndex = Convert.ToInt32(reader[ProductChangeOrder_RVL.colOrderIndex]);
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colCreatedUser])) objProductChangeOrder_RVL.CreatedUser = Convert.ToString(reader[ProductChangeOrder_RVL.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colCreatedDate])) objProductChangeOrder_RVL.CreatedDate = Convert.ToDateTime(reader[ProductChangeOrder_RVL.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colIsDeleted])) objProductChangeOrder_RVL.IsDeleted = Convert.ToBoolean(reader[ProductChangeOrder_RVL.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colDeletedUser])) objProductChangeOrder_RVL.DeletedUser = Convert.ToString(reader[ProductChangeOrder_RVL.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colDeletedDate])) objProductChangeOrder_RVL.DeletedDate = Convert.ToDateTime(reader[ProductChangeOrder_RVL.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colApprovePermission])) objProductChangeOrder_RVL.ApprovePermission = Convert.ToString(reader[ProductChangeOrder_RVL.colApprovePermission]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colRejectPermission])) objProductChangeOrder_RVL.RejectPermission = Convert.ToString(reader[ProductChangeOrder_RVL.colRejectPermission]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colStorePermissionType])) objProductChangeOrder_RVL.StorePermissionType = Convert.ToInt32(reader[ProductChangeOrder_RVL.colStorePermissionType]);
                }
 				else
 				{
 					objProductChangeOrder_RVL = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin yêu cầu xuất đổi - mức duyệt", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder_RVL -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


        /// <summary>
        /// Nạp thông tin yêu cầu xuất đổi - mức duyệt
        /// </summary>
        /// <param name="objProductChangeOrder_RVL">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadAllInfo(ref List<ProductChangeOrder_RVL> listProductChangeOrder_RVL, string strProductChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECTBYPARENT);
                objIData.AddParameter("@" + ProductChangeOrder_RVL.colProductChangeOrderID, strProductChangeOrderID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    if (listProductChangeOrder_RVL == null)
                    {
                        listProductChangeOrder_RVL = new List<ProductChangeOrder_RVL>();
                    }
                    ProductChangeOrder_RVL objProductChangeOrder_RVL = new ProductChangeOrder_RVL();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colProductChangeOrderRVLID])) objProductChangeOrder_RVL.ProductChangeOrderRVLID = Convert.ToString(reader[ProductChangeOrder_RVL.colProductChangeOrderRVLID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colProductChangeOrderID])) objProductChangeOrder_RVL.ProductChangeOrderID = Convert.ToString(reader[ProductChangeOrder_RVL.colProductChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colReviewLevelID])) objProductChangeOrder_RVL.ReviewLevelID = Convert.ToInt32(reader[ProductChangeOrder_RVL.colReviewLevelID]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colReviewLevelName])) objProductChangeOrder_RVL.ReviewLevelName = Convert.ToString(reader[ProductChangeOrder_RVL.colReviewLevelName]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colUserName])) objProductChangeOrder_RVL.UserName = Convert.ToString(reader[ProductChangeOrder_RVL.colUserName]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colFullName])) objProductChangeOrder_RVL.FullName = Convert.ToString(reader[ProductChangeOrder_RVL.colFullName]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colReviewType])) objProductChangeOrder_RVL.ReviewType = Convert.ToString(reader[ProductChangeOrder_RVL.colReviewType]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colReviewedStatus])) objProductChangeOrder_RVL.ReviewedStatus = Convert.ToInt32(reader[ProductChangeOrder_RVL.colReviewedStatus]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colReviewedDate])) objProductChangeOrder_RVL.ReviewedDate = Convert.ToDateTime(reader[ProductChangeOrder_RVL.colReviewedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colNote])) objProductChangeOrder_RVL.Note = Convert.ToString(reader[ProductChangeOrder_RVL.colNote]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colOrderIndex])) objProductChangeOrder_RVL.OrderIndex = Convert.ToInt32(reader[ProductChangeOrder_RVL.colOrderIndex]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colCreatedUser])) objProductChangeOrder_RVL.CreatedUser = Convert.ToString(reader[ProductChangeOrder_RVL.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colCreatedDate])) objProductChangeOrder_RVL.CreatedDate = Convert.ToDateTime(reader[ProductChangeOrder_RVL.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colIsDeleted])) objProductChangeOrder_RVL.IsDeleted = Convert.ToBoolean(reader[ProductChangeOrder_RVL.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colDeletedUser])) objProductChangeOrder_RVL.DeletedUser = Convert.ToString(reader[ProductChangeOrder_RVL.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colDeletedDate])) objProductChangeOrder_RVL.DeletedDate = Convert.ToDateTime(reader[ProductChangeOrder_RVL.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colSequenceReview])) objProductChangeOrder_RVL.SequenceReview = Convert.ToInt32(reader[ProductChangeOrder_RVL.colSequenceReview]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colApprovePermission])) objProductChangeOrder_RVL.ApprovePermission = Convert.ToString(reader[ProductChangeOrder_RVL.colApprovePermission]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colRejectPermission])) objProductChangeOrder_RVL.RejectPermission = Convert.ToString(reader[ProductChangeOrder_RVL.colRejectPermission]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder_RVL.colStorePermissionType])) objProductChangeOrder_RVL.StorePermissionType = Convert.ToInt32(reader[ProductChangeOrder_RVL.colStorePermissionType]);
                    listProductChangeOrder_RVL.Add(objProductChangeOrder_RVL);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin yêu cầu xuất đổi - mức duyệt", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder_RVL -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        /// <summary>
        /// Thêm thông tin yêu cầu xuất đổi - mức duyệt
        /// </summary>
        /// <param name="objProductChangeOrder_RVL">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ProductChangeOrder_RVL objProductChangeOrder_RVL)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objProductChangeOrder_RVL);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin yêu cầu xuất đổi - mức duyệt", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder_RVL -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProductChangeOrder_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, ProductChangeOrder_RVL objProductChangeOrder_RVL)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colProductChangeOrderID, objProductChangeOrder_RVL.ProductChangeOrderID);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colReviewLevelID, objProductChangeOrder_RVL.ReviewLevelID);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colUserName, objProductChangeOrder_RVL.UserName);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colReviewedStatus, objProductChangeOrder_RVL.ReviewedStatus);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colReviewedDate, objProductChangeOrder_RVL.ReviewedDate);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colNote, objProductChangeOrder_RVL.Note);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colOrderIndex, objProductChangeOrder_RVL.OrderIndex);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colCreatedUser, objProductChangeOrder_RVL.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objProductChangeOrder_RVL">Đối tượng truyền vào</param>
		/// <param name="lstOrderIndex">Danh sách OrderIndex</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(ProductChangeOrder_RVL objProductChangeOrder_RVL, List<object> lstOrderIndex)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.BeginTransaction();
				Update(objIData, objProductChangeOrder_RVL);
				//if (lstOrderIndex != null && lstOrderIndex.Count > 0)
				//{
				//	for (int i = 0; i < lstOrderIndex.Count; i++)
				//	{
				//		UpdateOrderIndex(objIData, Convert.ToInt32(lstOrderIndex[i]), i);
				//	}
				//}
				objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin yêu cầu xuất đổi - mức duyệt", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder_RVL -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProductChangeOrder_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, ProductChangeOrder_RVL objProductChangeOrder_RVL)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colProductChangeOrderRVLID, objProductChangeOrder_RVL.ProductChangeOrderRVLID);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colProductChangeOrderID, objProductChangeOrder_RVL.ProductChangeOrderID);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colReviewLevelID, objProductChangeOrder_RVL.ReviewLevelID);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colUserName, objProductChangeOrder_RVL.UserName);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colReviewedStatus, objProductChangeOrder_RVL.ReviewedStatus);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colReviewedDate, objProductChangeOrder_RVL.ReviewedDate);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colNote, objProductChangeOrder_RVL.Note);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colOrderIndex, objProductChangeOrder_RVL.OrderIndex);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="intOrderIndex">Thứ tự</param>
		/// <returns>Kết quả trả về</returns>
		public void UpdateOrderIndex(IData objIData, string strProductChangeOrderRVLID, int intOrderIndex)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATEINDEX);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colProductChangeOrderRVLID, strProductChangeOrderRVLID);
                objIData.AddParameter("@OrderIndex", intOrderIndex);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objProductChangeOrder_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(ProductChangeOrder_RVL objProductChangeOrder_RVL)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objProductChangeOrder_RVL);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin yêu cầu xuất đổi - mức duyệt", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder_RVL -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin yêu cầu xuất đổi - mức duyệt
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProductChangeOrder_RVL">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, ProductChangeOrder_RVL objProductChangeOrder_RVL)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colProductChangeOrderRVLID, objProductChangeOrder_RVL.ProductChangeOrderRVLID);
				objIData.AddParameter("@" + ProductChangeOrder_RVL.colDeletedUser, objProductChangeOrder_RVL.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_ProductChangeOrder_RVL()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_PRODUCTCHANGEORDER_RVL_ADD";
		public const String SP_UPDATE = "PM_PRODUCTCHANGEORDER_RVL_UPD";
		public const String SP_DELETE = "PM_PRODUCTCHANGEORDER_RVL_DEL";
		public const String SP_SELECT = "PM_PRODUCTCHANGEORDER_RVL_SEL";
        public const String SP_SELECTBYPARENT = "PM_PRODUCTORDER_RVL_SELPAR";
        public const String SP_SEARCH = "PM_PRODUCTCHANGEORDER_RVL_SRH";
		public const String SP_UPDATEINDEX = "PM_PRODUCTORDER_RVL_UPDINDEX";
		#endregion
		
	}
}
