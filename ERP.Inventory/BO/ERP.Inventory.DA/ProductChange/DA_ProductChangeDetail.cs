
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using ProductChangeDetail = ERP.Inventory.BO.ProductChange.ProductChangeDetail;
#endregion
namespace ERP.Inventory.DA.ProductChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/1/2012 
	/// 
	/// </summary>	
	public partial class DA_ProductChangeDetail
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objProductChangeDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref ProductChangeDetail objProductChangeDetail, string strProductChangeDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + ProductChangeDetail.colProductChangeDetailID, strProductChangeDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
 				if (objProductChangeDetail == null) 
 					objProductChangeDetail = new ProductChangeDetail();
				if (reader.Read())
 				{
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colProductChangeDetailID])) objProductChangeDetail.ProductChangeDetailID = Convert.ToString(reader[ProductChangeDetail.colProductChangeDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colProductChangeID])) objProductChangeDetail.ProductChangeID = Convert.ToString(reader[ProductChangeDetail.colProductChangeID]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colStoreID])) objProductChangeDetail.StoreID = Convert.ToInt32(reader[ProductChangeDetail.colStoreID]);
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colProductChangeDate])) objProductChangeDetail.ProductChangeDate = Convert.ToDateTime(reader[ProductChangeDetail.colProductChangeDate]);
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colProductID_Out])) objProductChangeDetail.ProductID_Out = Convert.ToString(reader[ProductChangeDetail.colProductID_Out]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colIMEI_Out])) objProductChangeDetail.IMEI_Out = Convert.ToString(reader[ProductChangeDetail.colIMEI_Out]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colProductID_In])) objProductChangeDetail.ProductID_In = Convert.ToString(reader[ProductChangeDetail.colProductID_In]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colIMEI_In])) objProductChangeDetail.IMEI_In = Convert.ToString(reader[ProductChangeDetail.colIMEI_In]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colQuantity])) objProductChangeDetail.Quantity = Convert.ToDecimal(reader[ProductChangeDetail.colQuantity]);
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colOutputVoucherDetailID])) objProductChangeDetail.OutputVoucherDetailID = Convert.ToString(reader[ProductChangeDetail.colOutputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colInputVoucherDetailID])) objProductChangeDetail.InputVoucherDetailID = Convert.ToString(reader[ProductChangeDetail.colInputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colOldInputVoucherDetailID])) objProductChangeDetail.OldInputVoucherDetailID = Convert.ToString(reader[ProductChangeDetail.colOldInputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colCreatedStoreID])) objProductChangeDetail.CreatedStoreID = Convert.ToInt32(reader[ProductChangeDetail.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colCreatedUser])) objProductChangeDetail.CreatedUser = Convert.ToString(reader[ProductChangeDetail.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colCreatedDate])) objProductChangeDetail.CreatedDate = Convert.ToDateTime(reader[ProductChangeDetail.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colUpdatedUser])) objProductChangeDetail.UpdatedUser = Convert.ToString(reader[ProductChangeDetail.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colUpdatedDate])) objProductChangeDetail.UpdatedDate = Convert.ToDateTime(reader[ProductChangeDetail.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colIsDeleted])) objProductChangeDetail.IsDeleted = Convert.ToBoolean(reader[ProductChangeDetail.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colDeletedUser])) objProductChangeDetail.DeletedUser = Convert.ToString(reader[ProductChangeDetail.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colDeletedDate])) objProductChangeDetail.DeletedDate = Convert.ToDateTime(reader[ProductChangeDetail.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[ProductChangeDetail.colContentDeleted])) objProductChangeDetail.ContentDeleted = Convert.ToString(reader[ProductChangeDetail.colContentDeleted]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeDetail.colInStockStatusID_in])) objProductChangeDetail.InStockStatusID_in = Convert.ToInt32(reader[ProductChangeDetail.colInStockStatusID_in]);
                    if (!Convert.IsDBNull(reader[ProductChangeDetail.colInStockStatusID_Out])) objProductChangeDetail.InStockStatusID_Out = Convert.ToInt32(reader[ProductChangeDetail.colInStockStatusID_Out]);
                }
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objProductChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(ProductChangeDetail objProductChangeDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objProductChangeDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProductChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, ProductChangeDetail objProductChangeDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + ProductChangeDetail.colProductChangeID, objProductChangeDetail.ProductChangeID);
				objIData.AddParameter("@" + ProductChangeDetail.colStoreID, objProductChangeDetail.StoreID);
				objIData.AddParameter("@" + ProductChangeDetail.colProductChangeDate, objProductChangeDetail.ProductChangeDate);
				objIData.AddParameter("@" + ProductChangeDetail.colProductID_Out, objProductChangeDetail.ProductID_Out);
				objIData.AddParameter("@" + ProductChangeDetail.colIMEI_Out, objProductChangeDetail.IMEI_Out);
				objIData.AddParameter("@" + ProductChangeDetail.colProductID_In, objProductChangeDetail.ProductID_In);
				objIData.AddParameter("@" + ProductChangeDetail.colIMEI_In, objProductChangeDetail.IMEI_In);
				objIData.AddParameter("@" + ProductChangeDetail.colQuantity, objProductChangeDetail.Quantity);
				objIData.AddParameter("@" + ProductChangeDetail.colOutputVoucherDetailID, objProductChangeDetail.OutputVoucherDetailID);
				objIData.AddParameter("@" + ProductChangeDetail.colInputVoucherDetailID, objProductChangeDetail.InputVoucherDetailID);
				objIData.AddParameter("@" + ProductChangeDetail.colOldInputVoucherDetailID, objProductChangeDetail.OldInputVoucherDetailID);
				objIData.AddParameter("@" + ProductChangeDetail.colCreatedStoreID, objProductChangeDetail.CreatedStoreID);
				objIData.AddParameter("@" + ProductChangeDetail.colCreatedUser, objProductChangeDetail.CreatedUser);
				objIData.AddParameter("@" + ProductChangeDetail.colContentDeleted, objProductChangeDetail.ContentDeleted);
                objIData.AddParameter("@" + ProductChangeDetail.colInStockStatusID_in, objProductChangeDetail.InStockStatusID_in);
                objIData.AddParameter("@" + ProductChangeDetail.colInStockStatusID_Out, objProductChangeDetail.InStockStatusID_Out);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objProductChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(ProductChangeDetail objProductChangeDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objProductChangeDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProductChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, ProductChangeDetail objProductChangeDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + ProductChangeDetail.colProductChangeDetailID, objProductChangeDetail.ProductChangeDetailID);
				objIData.AddParameter("@" + ProductChangeDetail.colProductChangeID, objProductChangeDetail.ProductChangeID);
				objIData.AddParameter("@" + ProductChangeDetail.colStoreID, objProductChangeDetail.StoreID);
				objIData.AddParameter("@" + ProductChangeDetail.colProductChangeDate, objProductChangeDetail.ProductChangeDate);
				objIData.AddParameter("@" + ProductChangeDetail.colProductID_Out, objProductChangeDetail.ProductID_Out);
				objIData.AddParameter("@" + ProductChangeDetail.colIMEI_Out, objProductChangeDetail.IMEI_Out);
				objIData.AddParameter("@" + ProductChangeDetail.colProductID_In, objProductChangeDetail.ProductID_In);
				objIData.AddParameter("@" + ProductChangeDetail.colIMEI_In, objProductChangeDetail.IMEI_In);
				objIData.AddParameter("@" + ProductChangeDetail.colQuantity, objProductChangeDetail.Quantity);
				objIData.AddParameter("@" + ProductChangeDetail.colOutputVoucherDetailID, objProductChangeDetail.OutputVoucherDetailID);
				objIData.AddParameter("@" + ProductChangeDetail.colInputVoucherDetailID, objProductChangeDetail.InputVoucherDetailID);
				objIData.AddParameter("@" + ProductChangeDetail.colOldInputVoucherDetailID, objProductChangeDetail.OldInputVoucherDetailID);
				objIData.AddParameter("@" + ProductChangeDetail.colCreatedStoreID, objProductChangeDetail.CreatedStoreID);
				objIData.AddParameter("@" + ProductChangeDetail.colUpdatedUser, objProductChangeDetail.UpdatedUser);
				objIData.AddParameter("@" + ProductChangeDetail.colContentDeleted, objProductChangeDetail.ContentDeleted);
                objIData.AddParameter("@" + ProductChangeDetail.colInStockStatusID_in, objProductChangeDetail.InStockStatusID_in);
                objIData.AddParameter("@" + ProductChangeDetail.colInStockStatusID_Out, objProductChangeDetail.InStockStatusID_Out);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objProductChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(ProductChangeDetail objProductChangeDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objProductChangeDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objProductChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, ProductChangeDetail objProductChangeDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + ProductChangeDetail.colProductChangeDetailID, objProductChangeDetail.ProductChangeDetailID);
				objIData.AddParameter("@" + ProductChangeDetail.colDeletedUser, objProductChangeDetail.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_ProductChangeDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_PRODUCTCHANGEDETAIL_ADD";
		public const String SP_UPDATE = "PM_PRODUCTCHANGEDETAIL_UPD";
		public const String SP_DELETE = "PM_PRODUCTCHANGEDETAIL_DEL";
		public const String SP_SELECT = "PM_PRODUCTCHANGEDETAIL_SEL";
		public const String SP_SEARCH = "PM_PRODUCTCHANGEDETAIL_SRH";
		public const String SP_UPDATEINDEX = "PM_PRODUCTCHANGEDETAIL_UPDINDEX";
		#endregion
		
	}
}
