
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using ProductChangeOrder = ERP.Inventory.BO.ProductChange.ProductChangeOrder;
#endregion
namespace ERP.Inventory.DA.ProductChange
{
    /// <summary>
	/// Created by 		: Phan Tiến Lộc 
	/// Created date 	: 2016-11-12 
	/// Yêu cầu xuất đổi hàng
	/// </summary>	
	public partial class DA_ProductChangeOrder
    {


        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods			


        /// <summary>
        /// Nạp thông tin yêu cầu xuất đổi hàng
        /// </summary>
        /// <param name="objProductChangeOrder">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref ProductChangeOrder objProductChangeOrder, string strProductChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeOrderID, strProductChangeOrderID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objProductChangeOrder = new ProductChangeOrder();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colProductChangeOrderID])) objProductChangeOrder.ProductChangeOrderID = Convert.ToString(reader[ProductChangeOrder.colProductChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colProductChangeTypeID])) objProductChangeOrder.ProductChangeTypeID = Convert.ToInt32(reader[ProductChangeOrder.colProductChangeTypeID]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colDescription])) objProductChangeOrder.Description = Convert.ToString(reader[ProductChangeOrder.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colProductChangeStoreID])) objProductChangeOrder.ProductChangeStoreID = Convert.ToInt32(reader[ProductChangeOrder.colProductChangeStoreID]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colReviewedStatus])) objProductChangeOrder.ReviewedStatus = Convert.ToInt32(reader[ProductChangeOrder.colReviewedStatus]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colReviewedUser])) objProductChangeOrder.ReviewedUser = Convert.ToString(reader[ProductChangeOrder.colReviewedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colReviewedDate])) objProductChangeOrder.ReviewedDate = Convert.ToDateTime(reader[ProductChangeOrder.colReviewedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colISChangeD])) objProductChangeOrder.IsChanged = Convert.ToBoolean(reader[ProductChangeOrder.colISChangeD]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colChangeDUser])) objProductChangeOrder.ChangedUser = Convert.ToString(reader[ProductChangeOrder.colChangeDUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colChangeDDate])) objProductChangeOrder.ChangedDate = Convert.ToDateTime(reader[ProductChangeOrder.colChangeDDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colCurrentReviewLevelID])) objProductChangeOrder.CurrentReviewLevelID = Convert.ToInt32(reader[ProductChangeOrder.colCurrentReviewLevelID]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colCreatedStoreID])) objProductChangeOrder.CreatedStoreID = Convert.ToInt32(reader[ProductChangeOrder.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colCreatedUser])) objProductChangeOrder.CreatedUser = Convert.ToString(reader[ProductChangeOrder.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colCreatedDate])) objProductChangeOrder.CreatedDate = Convert.ToDateTime(reader[ProductChangeOrder.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colUpdatedUser])) objProductChangeOrder.UpdatedUser = Convert.ToString(reader[ProductChangeOrder.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colUpdatedDate])) objProductChangeOrder.UpdatedDate = Convert.ToDateTime(reader[ProductChangeOrder.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colIsDeleted])) objProductChangeOrder.IsDeleted = Convert.ToBoolean(reader[ProductChangeOrder.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colDeletedUser])) objProductChangeOrder.DeletedUser = Convert.ToString(reader[ProductChangeOrder.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colDeletedDate])) objProductChangeOrder.DeletedDate = Convert.ToDateTime(reader[ProductChangeOrder.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colDeletedReason])) objProductChangeOrder.DeletedReason = Convert.ToString(reader[ProductChangeOrder.colDeletedReason]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colExpiryDate])) objProductChangeOrder.ExpiryDate = Convert.ToDateTime(reader[ProductChangeOrder.colExpiryDate]);
                }
                else
                {
                    objProductChangeOrder = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin yêu cầu xuất đổi hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage Approve(string productChangeOrderId, int reviewLevelId, string userName, bool unApprove, string Reason)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_ProductChangeOrder.SP_APPROVE);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeOrderID, productChangeOrderId);
                objIData.AddParameter("@" + ProductChangeOrder.colCurrentReviewLevelID, reviewLevelId);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedUser, userName);
                objIData.AddParameter("@Approve", unApprove == true ? 0 : 1);
                objIData.AddParameter("@Reason", Reason);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi duyệt thông tin loại yêu cầu xuất đổi";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.LoadInfo, strMessage, objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeType -> Approve", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Nạp thông tin yêu cầu xuất đổi hàng
        /// </summary>
        /// <param name="objProductChangeOrder">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadAllInfo(ref ProductChangeOrder objProductChangeOrder, string strProductChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            DA_ProductChangeOrderDT objDA_ProductChangeOrderDT = new DA_ProductChangeOrderDT();
            DA_ProductChangeOrder_RVL objDA_ProductChangeOrder_RVL = new DA_ProductChangeOrder_RVL();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeOrderID, strProductChangeOrderID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objProductChangeOrder = new ProductChangeOrder();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colProductChangeOrderID])) objProductChangeOrder.ProductChangeOrderID = Convert.ToString(reader[ProductChangeOrder.colProductChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colProductChangeTypeID])) objProductChangeOrder.ProductChangeTypeID = Convert.ToInt32(reader[ProductChangeOrder.colProductChangeTypeID]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colDescription])) objProductChangeOrder.Description = Convert.ToString(reader[ProductChangeOrder.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colProductChangeStoreID])) objProductChangeOrder.ProductChangeStoreID = Convert.ToInt32(reader[ProductChangeOrder.colProductChangeStoreID]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colReviewedStatus])) objProductChangeOrder.ReviewedStatus = Convert.ToInt32(reader[ProductChangeOrder.colReviewedStatus]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colReviewedUser])) objProductChangeOrder.ReviewedUser = Convert.ToString(reader[ProductChangeOrder.colReviewedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colReviewedDate])) objProductChangeOrder.ReviewedDate = Convert.ToDateTime(reader[ProductChangeOrder.colReviewedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colISChangeD])) objProductChangeOrder.IsChanged = Convert.ToBoolean(reader[ProductChangeOrder.colISChangeD]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colChangeDUser])) objProductChangeOrder.ChangedUser = Convert.ToString(reader[ProductChangeOrder.colChangeDUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colChangeDDate])) objProductChangeOrder.ChangedDate = Convert.ToDateTime(reader[ProductChangeOrder.colChangeDDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colCurrentReviewLevelID])) objProductChangeOrder.CurrentReviewLevelID = Convert.ToInt32(reader[ProductChangeOrder.colCurrentReviewLevelID]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colCreatedStoreID])) objProductChangeOrder.CreatedStoreID = Convert.ToInt32(reader[ProductChangeOrder.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colCreatedUser])) objProductChangeOrder.CreatedUser = Convert.ToString(reader[ProductChangeOrder.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colCreatedDate])) objProductChangeOrder.CreatedDate = Convert.ToDateTime(reader[ProductChangeOrder.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colUpdatedUser])) objProductChangeOrder.UpdatedUser = Convert.ToString(reader[ProductChangeOrder.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colUpdatedDate])) objProductChangeOrder.UpdatedDate = Convert.ToDateTime(reader[ProductChangeOrder.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colIsDeleted])) objProductChangeOrder.IsDeleted = Convert.ToBoolean(reader[ProductChangeOrder.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colDeletedUser])) objProductChangeOrder.DeletedUser = Convert.ToString(reader[ProductChangeOrder.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colDeletedDate])) objProductChangeOrder.DeletedDate = Convert.ToDateTime(reader[ProductChangeOrder.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colDeletedReason])) objProductChangeOrder.DeletedReason = Convert.ToString(reader[ProductChangeOrder.colDeletedReason]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colExpiryDate])) objProductChangeOrder.ExpiryDate = Convert.ToDateTime(reader[ProductChangeOrder.colExpiryDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colChangedUserFull])) objProductChangeOrder.ChangedUserFull = Convert.ToString(reader[ProductChangeOrder.colChangedUserFull]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colCreatedUserFull])) objProductChangeOrder.CreatedUserFull = Convert.ToString(reader[ProductChangeOrder.colCreatedUserFull]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrder.colReviewedUserFull])) objProductChangeOrder.ReviewedUserFull = Convert.ToString(reader[ProductChangeOrder.colReviewedUserFull]).Trim();

                    List<BO.ProductChange.ProductChangeOrderDT> objDetai = null;
                    List<BO.ProductChange.ProductChangeOrder_RVL> objRVL = null;
                    objResultMessage = objDA_ProductChangeOrderDT.LoadInfoByParent(ref objDetai, objProductChangeOrder.ProductChangeOrderID);
                    if (objResultMessage.IsError == false)
                    {
                        objProductChangeOrder.ProductChangeOrderDT = new List<BO.ProductChange.ProductChangeOrderDT>();
                        objProductChangeOrder.ProductChangeOrderDT.AddRange(objDetai);
                        objResultMessage = objDA_ProductChangeOrder_RVL.LoadAllInfo(ref objRVL, objProductChangeOrder.ProductChangeOrderID);
                        if (objResultMessage.IsError == false)
                        {
                            objProductChangeOrder.ProductChangeOrder_RVL = new List<BO.ProductChange.ProductChangeOrder_RVL>();
                            objProductChangeOrder.ProductChangeOrder_RVL.AddRange(objRVL);
                        }
                    }
                    else
                    {
                        return objResultMessage;
                    }
                }
                else
                {
                    objProductChangeOrder = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin yêu cầu xuất đổi hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin yêu cầu xuất đổi hàng
        /// </summary>
        /// <param name="objProductChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ProductChangeOrder objProductChangeOrder)
        {
            ResultMessage objResultMessage = new ResultMessage();
            DA_ProductChangeOrderDT objDA_ProductChangeOrderDT = new DA_ProductChangeOrderDT();
            DA_ProductChangeOrder_RVL objDA_ProductChangeOrder_RVL = new DA_ProductChangeOrder_RVL();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                string strProductChangeOrderID = "";
                Insert(objIData, objProductChangeOrder, ref strProductChangeOrderID);
                foreach (var item in objProductChangeOrder.ProductChangeOrderDT)
                {
                    item.ProductChangeOrderID = strProductChangeOrderID;
                    item.CreatedUser = objProductChangeOrder.CreatedUser;
                    objDA_ProductChangeOrderDT.Insert(objIData, item);
                }
                foreach (var item in objProductChangeOrder.ProductChangeOrder_RVL)
                {
                    item.ProductChangeOrderID = strProductChangeOrderID;
                    item.CreatedUser = objProductChangeOrder.CreatedUser;
                    objDA_ProductChangeOrder_RVL.Insert(objIData, item);
                }

                InsertAttachment(objIData, strProductChangeOrderID, objProductChangeOrder.CreatedUser, objProductChangeOrder.ProductChangeOrder_ATTList);

                objIData.CommitTransaction();

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin yêu cầu xuất đổi hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin yêu cầu xuất đổi hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objProductChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, ProductChangeOrder objProductChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeOrderID, objProductChangeOrder.ProductChangeOrderID);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeTypeID, objProductChangeOrder.ProductChangeTypeID);
                objIData.AddParameter("@" + ProductChangeOrder.colDescription, objProductChangeOrder.Description);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeStoreID, objProductChangeOrder.ProductChangeStoreID);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedStatus, objProductChangeOrder.ReviewedStatus);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedUser, objProductChangeOrder.ReviewedUser);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedDate, objProductChangeOrder.ReviewedDate);
                objIData.AddParameter("@" + ProductChangeOrder.colISChangeD, objProductChangeOrder.IsChanged);
                objIData.AddParameter("@" + ProductChangeOrder.colChangeDUser, objProductChangeOrder.ChangedUser);
                objIData.AddParameter("@" + ProductChangeOrder.colChangeDDate, objProductChangeOrder.ChangedDate);
                objIData.AddParameter("@" + ProductChangeOrder.colCurrentReviewLevelID, objProductChangeOrder.CurrentReviewLevelID);
                objIData.AddParameter("@" + ProductChangeOrder.colCreatedStoreID, objProductChangeOrder.CreatedStoreID);
                objIData.AddParameter("@" + ProductChangeOrder.colCreatedUser, objProductChangeOrder.CreatedUser);
                objIData.AddParameter("@" + ProductChangeOrder.colDeletedReason, objProductChangeOrder.DeletedReason);
                objIData.AddParameter("@" + ProductChangeOrder.colExpiryDate, objProductChangeOrder.ExpiryDate);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Thêm thông tin yêu cầu xuất đổi hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objProductChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, ProductChangeOrder objProductChangeOrder, ref string ProductChangeOrderID)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeTypeID, objProductChangeOrder.ProductChangeTypeID);
                objIData.AddParameter("@" + ProductChangeOrder.colDescription, objProductChangeOrder.Description);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeStoreID, objProductChangeOrder.ProductChangeStoreID);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedStatus, objProductChangeOrder.ReviewedStatus);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedUser, objProductChangeOrder.ReviewedUser);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedDate, objProductChangeOrder.ReviewedDate);
                objIData.AddParameter("@" + ProductChangeOrder.colISChangeD, objProductChangeOrder.IsChanged);
                objIData.AddParameter("@" + ProductChangeOrder.colChangeDUser, objProductChangeOrder.ChangedUser);
                objIData.AddParameter("@" + ProductChangeOrder.colChangeDDate, objProductChangeOrder.ChangedDate);
                objIData.AddParameter("@" + ProductChangeOrder.colCurrentReviewLevelID, objProductChangeOrder.CurrentReviewLevelID);
                objIData.AddParameter("@" + ProductChangeOrder.colCreatedStoreID, objProductChangeOrder.CreatedStoreID);
                objIData.AddParameter("@" + ProductChangeOrder.colCreatedUser, objProductChangeOrder.CreatedUser);
                objIData.AddParameter("@" + ProductChangeOrder.colDeletedReason, objProductChangeOrder.DeletedReason);
                objIData.AddParameter("@" + ProductChangeOrder.colExpiryDate, objProductChangeOrder.ExpiryDate);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                string strRefID = objIData.ExecStoreToString();
                ProductChangeOrderID = strRefID;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objCurrentInStockDetail">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage CheckExistIMEI(ref bool bolIsResult, string strIMEI, string strProductID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_CURRENTINSTOCKDETAIL_CHK");
                objIData.AddParameter("@IMEI", strIMEI);
                objIData.AddParameter("@PRODUCTID", strProductID);
                bolIsResult = Convert.ToBoolean(Convert.ToInt32(objIData.ExecStoreToString()));
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm tra tồn kho IMEI-Sản phẩm ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CurrentInStockDetail -> CheckExist", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        /// <summary>
        /// Cập nhật thông tin yêu cầu xuất đổi hàng
        /// </summary>
        /// <param name="objProductChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(ProductChangeOrder objProductChangeOrder)
        {
            ResultMessage objResultMessage = new ResultMessage();
            DA_ProductChangeOrderDT objDA_ProductChangeOrderDT = new DA_ProductChangeOrderDT();
            DA_ProductChangeOrder_RVL objDA_ProductChangeOrder_RVL = new DA_ProductChangeOrder_RVL();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
               // objIData.BeginTransaction();
                Update(objIData, objProductChangeOrder);
                InsertAttachment(objIData, objProductChangeOrder.ProductChangeOrderID, objProductChangeOrder.UpdatedUser, objProductChangeOrder.ProductChangeOrder_ATTList);

                foreach (var item in objProductChangeOrder.ProductChangeOrderDT)
                {
                    objDA_ProductChangeOrderDT.Update(objIData, item);
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin yêu cầu xuất đổi hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin yêu cầu xuất đổi hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objProductChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, ProductChangeOrder objProductChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeOrderID, objProductChangeOrder.ProductChangeOrderID);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeTypeID, objProductChangeOrder.ProductChangeTypeID);
                objIData.AddParameter("@" + ProductChangeOrder.colDescription, objProductChangeOrder.Description);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeStoreID, objProductChangeOrder.ProductChangeStoreID);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedStatus, objProductChangeOrder.ReviewedStatus);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedUser, objProductChangeOrder.ReviewedUser);
                objIData.AddParameter("@" + ProductChangeOrder.colReviewedDate, objProductChangeOrder.ReviewedDate);
                objIData.AddParameter("@" + ProductChangeOrder.colISChangeD, objProductChangeOrder.IsChanged);
                objIData.AddParameter("@" + ProductChangeOrder.colChangeDUser, objProductChangeOrder.ChangedUser);
                objIData.AddParameter("@" + ProductChangeOrder.colChangeDDate, objProductChangeOrder.ChangedDate);
                objIData.AddParameter("@" + ProductChangeOrder.colCurrentReviewLevelID, objProductChangeOrder.CurrentReviewLevelID);
                objIData.AddParameter("@" + ProductChangeOrder.colCreatedStoreID, objProductChangeOrder.CreatedStoreID);
                objIData.AddParameter("@" + ProductChangeOrder.colUpdatedUser, objProductChangeOrder.UpdatedUser);
                objIData.AddParameter("@" + ProductChangeOrder.colDeletedReason, objProductChangeOrder.DeletedReason);
                objIData.AddParameter("@" + ProductChangeOrder.colExpiryDate, objProductChangeOrder.ExpiryDate);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin yêu cầu xuất đổi hàng
        /// </summary>
        /// <param name="objProductChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(ProductChangeOrder objProductChangeOrder)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objProductChangeOrder);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin yêu cầu xuất đổi hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin yêu cầu xuất đổi hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objProductChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, ProductChangeOrder objProductChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeOrderID, objProductChangeOrder.ProductChangeOrderID);
                objIData.AddParameter("@" + ProductChangeOrder.colDeletedUser, objProductChangeOrder.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// kiểm tra tồn tại mã yêu cầu xuất đổi
        /// </summary>
        /// <param name="bolResult"></param>
        /// <param name="objProductChangeType"></param>
        /// <returns></returns>
        public ResultMessage CheckExist(ref bool bolResult, string ProductChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_ProductChangeOrder.SP_CHECKEXIST);
                objIData.AddParameter("@" + ProductChangeOrder.colProductChangeOrderID, ProductChangeOrderID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    bolResult = true;
                }
                else
                {
                    bolResult = false;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi kiểm tra tồn tại thông tin loại yêu cầu xuất đổi";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.LoadInfo, strMessage, objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeType -> CheckExist", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion


        #region Constructor

        public DA_ProductChangeOrder()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "PM_PRODUCTCHANGEORDER_ADD";
        public const String SP_UPDATE = "PM_PRODUCTCHANGEORDER_UPD";
        public const String SP_DELETE = "PM_PRODUCTCHANGEORDER_DEL";
        public const String SP_SELECT = "PM_PRODUCTCHANGEORDER_SEL";
        public const String SP_SEARCH = "PM_PRODUCTCHANGEORDER_SRH";
        public const String SP_UPDATEINDEX = "PM_PRODUCTCHANGEORDER_UPDINDEX";
        public const String SP_CHECKEXIST = "PM_PRODUCTCHANGEORDER_CHKEXIST";
        public const String SP_APPROVE = "PM_PRODUCTCHANGEORDER_APPROVE";
        public const String SP_SEARCHPARAM = "PM_PRODUCTCHANGEORDER_SRHPARAM";
        #endregion

    }
}
