
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.ProductChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/1/2012 
	/// 
	/// </summary>	
	public partial class DA_ProductChange
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin 
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_ProductChange.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChange -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public ResultMessage GetListStoreInStock(ref DataTable dtbData, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_PrdChange_GetStoreInStock");
                objIData.AddParameter("@StoreID", intStoreID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy thông tin tồn kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChange -> GetListStoreInStock", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public string GetProductChangeNewID(IData objIData, int intStoreID)
        {
            string strProductChangeNewID = string.Empty;
            try
            {
                objIData.CreateNewStoredProcedure("PM_PRODUCTCHANGE_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strProductChangeNewID = objIData.ExecStoreToString();
                return strProductChangeNewID;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage InsertMasterAndDetail(ERP.Inventory.BO.ProductChange.ProductChange objProductChange)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();

                objProductChange.OutputVoucherBO.CreatedUser = objProductChange.CreatedUser;
                objDA_OutputVoucher.Insert(objIData, objProductChange.OutputVoucherBO);

                DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();

                objProductChange.InputVoucherBO.CreatedUser = objProductChange.CreatedUser;
                objDA_InputVoucher.Insert(objIData, objProductChange.InputVoucherBO);

                objProductChange.InputVoucherID = objProductChange.InputVoucherBO.InputVoucherID;
                objProductChange.OutputVoucherID = objProductChange.OutputVoucherBO.OutputVoucherID;

                objProductChange.ProductChangeID = GetProductChangeNewID(objIData, objProductChange.CreatedStoreID);

                this.Insert(objIData, objProductChange);

                DA_ProductChangeDetail objDA_ProductChangeDetail = new DA_ProductChangeDetail();

                for (int i = 0; i < objProductChange.ProductChangeDetailList.Count; i++)
                {
                    BO.ProductChange.ProductChangeDetail objProductChangeDetail = objProductChange.ProductChangeDetailList[i];
                    objProductChangeDetail.ProductChangeID = objProductChange.ProductChangeID;
                    objProductChangeDetail.CreatedUser = objProductChange.CreatedUser;

                    objIData.CreateNewStoredProcedure("PM_PRODUCTCHANGEDETAIL_ADD");
                    objIData.AddParameter("@OutputVoucherID", objProductChange.OutputVoucherID);
                    objIData.AddParameter("@InputVoucherID", objProductChange.InputVoucherID);
                    objIData.AddParameter("@ProductChangeID", objProductChangeDetail.ProductChangeID);
                    objIData.AddParameter("@StoreID", objProductChangeDetail.StoreID);
                    objIData.AddParameter("@ProductChangeDate", objProductChangeDetail.ProductChangeDate);
                    objIData.AddParameter("@ProductID_Out", objProductChangeDetail.ProductID_Out);
                    objIData.AddParameter("@IMEI_Out", objProductChangeDetail.IMEI_Out);
                    objIData.AddParameter("@ProductID_IN", objProductChangeDetail.ProductID_In);
                    objIData.AddParameter("@IMEI_IN", objProductChangeDetail.IMEI_In);
                    objIData.AddParameter("@Quantity", objProductChangeDetail.Quantity);
                    objIData.AddParameter("@OLDInputVoucherDetailID", objProductChangeDetail.OldInputVoucherDetailID);
                    objIData.AddParameter("@CreatedStoreID", objProductChangeDetail.CreatedStoreID);
                    objIData.AddParameter("@CreatedUser", objProductChangeDetail.CreatedUser);
                    objIData.AddParameter("@ContentDeleted", objProductChangeDetail.ContentDeleted);

                    objIData.AddParameter("@Quantity_IN", objProductChangeDetail.Quantity);
                    objIData.AddParameter("@IsNew_IN", objProductChange.InputVoucherBO.InputVoucherDetailList[0].IsNew);
                    objIData.AddParameter("@InputTypeID_IN", objProductChange.InputVoucherBO.InputTypeID);
                    objIData.AddParameter("@CustomerID_IN", objProductChange.InputVoucherBO.CustomerID);
                    objIData.AddParameter("@InputDate_IN", objProductChangeDetail.ProductChangeDate);
                    objIData.AddParameter("@ProductChangeDate_IN", objProductChangeDetail.ProductChangeDate);
                    for (int j = 0; j < objProductChange.InputVoucherBO.InputVoucherDetailList.Count; j++)
                    {
                        if (objProductChange.InputVoucherBO.InputVoucherDetailList[j].ProductID == objProductChangeDetail.ProductID_In)
                        {
                            objIData.AddParameter("@ENDWarrantyDate_IN", objProductChange.InputVoucherBO.InputVoucherDetailList[j].ENDWarrantyDate);
                            break;
                        }
                    }
                    if (objProductChangeDetail.ProductID_Out == objProductChangeDetail.ProductID_In
                        && objProductChangeDetail.IMEI_Out != string.Empty && objProductChangeDetail.IMEI_Out == objProductChangeDetail.IMEI_In)
                    {
                        objIData.AddParameter("@ChangeToOLDDate_IN", objProductChangeDetail.ProductChangeDate);
                    }

                    objIData.AddParameter("@Quantity_Out", objProductChangeDetail.Quantity);
                    objIData.AddParameter("@IsNew_Out", objProductChange.OutputVoucherBO.OutputVoucherDetailList[0].IsNew);
                    objIData.AddParameter("@OutputTypeID_Out", objProductChangeDetail.OutputTypeID);
                    objIData.AddParameter("@OutputDate_Out", objProductChangeDetail.ProductChangeDate);
                    objIData.AddParameter("@InStockStatusID_in", objProductChangeDetail.InStockStatusID_in);
                    objIData.AddParameter("@InStockStatusID_Out", objProductChangeDetail.InStockStatusID_Out);
                    objIData.ExecNonQuery();
                }

                //02/11/2016  Thiên bổ sung Cập nhật Tổng tiền Phiếu nhập, Phiếu xuất
                #region Tổng tiền phiếu nhập
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_UPDATETOTAL");
                objIData.AddParameter("@InputVoucherID", objProductChange.InputVoucherID);
                objIData.ExecNonQuery();
                #endregion

                #region Tổng tiền phiếu xuất
                objIData.CreateNewStoredProcedure("PM_OUTPUTVOUCHER_UPDATETOTAL");
                objIData.AddParameter("@OutputVoucherID", objProductChange.OutputVoucherID);
                objIData.ExecNonQuery();
                #endregion

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi xuất đổi hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChange -> InsertMasterAndDetail", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="strInputOrOutputVoucherID">Mã phiếu nhập hoặc phiếu xuất</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfoByInputOrOutputVoucherID(ref ERP.Inventory.BO.ProductChange.ProductChange objProductChange, string strInputOrOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@InputOrOutputVoucherID", strInputOrOutputVoucherID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (objProductChange == null)
                    objProductChange = new BO.ProductChange.ProductChange();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeID])) objProductChange.ProductChangeID = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeOrderID])) objProductChange.ProductChangeOrderID = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colStoreID])) objProductChange.StoreID = Convert.ToInt32(reader[ERP.Inventory.BO.ProductChange.ProductChange.colStoreID]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colIsNew])) objProductChange.IsNew = Convert.ToBoolean(reader[ERP.Inventory.BO.ProductChange.ProductChange.colIsNew]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colOutputVoucherID])) objProductChange.OutputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colInputVoucherID])) objProductChange.InputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeUser])) objProductChange.ProductChangeUser = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colInVoiceDate])) objProductChange.InVoiceDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colInVoiceDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeDate])) objProductChange.ProductChangeDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colProductChangeDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colContent])) objProductChange.Content = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colContent]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedStoreID])) objProductChange.CreatedStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedUser])) objProductChange.CreatedUser = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedDate])) objProductChange.CreatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colUpdatedUser])) objProductChange.UpdatedUser = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colUpdatedDate])) objProductChange.UpdatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colIsDeleted])) objProductChange.IsDeleted = Convert.ToBoolean(reader[ERP.Inventory.BO.ProductChange.ProductChange.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colDeletedUser])) objProductChange.DeletedUser = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colDeletedDate])) objProductChange.DeletedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.ProductChange.ProductChange.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[ERP.Inventory.BO.ProductChange.ProductChange.colContentDeleted])) objProductChange.ContentDeleted = Convert.ToString(reader[ERP.Inventory.BO.ProductChange.ProductChange.colContentDeleted]).Trim();
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin bởi mã phiếu nhập hoặc mã phiếu xuất ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChange -> LoadInfoByInputOrOutputVoucherID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
		#endregion
		
		
	}
}
