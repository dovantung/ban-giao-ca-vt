
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.ProductChange
{
    /// <summary>
	/// Created by 		: Phan Tiến Lộc 
	/// Created date 	: 2016-11-12 
	/// Yêu cầu xuất đổi hàng
	/// </summary>	
	public partial class DA_ProductChangeOrder
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin yêu cầu xuất đổi hàng
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_ProductChangeOrder.SP_SEARCHPARAM);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin yêu cầu xuất đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrder -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}


        public ResultMessage GetAttachment(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_PRODUCTCHANGEORDER_ATT_SRH");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "AttachmentList";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy tập tin đính kèm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeOrder -> GetAttachment", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private void InsertAttachment(IData objIData, string strProductChangeOrderID, string strUserName, List<BO.ProductChange.ProductChangeOrder_ATT> lstProductChangeOrder_ATT)
        {
            if (lstProductChangeOrder_ATT == null)
                return;
            for (int i = 0; i < lstProductChangeOrder_ATT.Count; i++)
            {
                if (lstProductChangeOrder_ATT[i].IsAddNew)
                {
                    lstProductChangeOrder_ATT[i].ProductChangeOrderID = strProductChangeOrderID;
                    try
                    {
                        objIData.CreateNewStoredProcedure("PM_PRODUCTCHANGEORDER_ATT_ADD");
                        objIData.AddParameter("@AttachmentName", lstProductChangeOrder_ATT[i].AttachMentName);
                        objIData.AddParameter("@AttachmentPath", lstProductChangeOrder_ATT[i].AttachMentPath);
                        objIData.AddParameter("@CreatedUser", strUserName);
                        objIData.AddParameter("@Description", lstProductChangeOrder_ATT[i].Description);
                        objIData.AddParameter("@ProductChangeOrderID", strProductChangeOrderID);
                        objIData.AddParameter("@FileID", lstProductChangeOrder_ATT[i].FileID);
                        objIData.ExecNonQuery();
                    }
                    catch (Exception objEx)
                    {
                        objIData.RollBackTransaction();
                        throw (objEx);
                    }
                }
                else
                {
                    if (lstProductChangeOrder_ATT[i].IsDeleted)
                    {
                        try
                        {
                            objIData.CreateNewStoredProcedure("PM_PRODUCTCHANGEORDER_ATT_DEL");
                            objIData.AddParameter("@AttachmentID", lstProductChangeOrder_ATT[i].AttachMentID);
                            objIData.AddParameter("@DeletedUser", strUserName);
                            objIData.ExecNonQuery();
                        }
                        catch (Exception objEx)
                        {
                            objIData.RollBackTransaction();
                            throw (objEx);
                        }
                    }
                    else
                    {
                        try
                        {
                            objIData.CreateNewStoredProcedure("PM_PRODUCTCHANGEORDER_ATT_UPD");
                            objIData.AddParameter("@AttachmentID", lstProductChangeOrder_ATT[i].AttachMentID);
                            objIData.AddParameter("@Description", lstProductChangeOrder_ATT[i].Description);
                            objIData.ExecNonQuery();
                        }
                        catch (Exception objEx)
                        {
                            objIData.RollBackTransaction();
                            throw (objEx);
                        }
                    }
                }
            }
        }

        #endregion


    }
}
