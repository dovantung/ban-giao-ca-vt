
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using ProductChangeOrderDT = ERP.Inventory.BO.ProductChange.ProductChangeOrderDT;
#endregion
namespace ERP.Inventory.DA.ProductChange
{
    /// <summary>
	/// Created by 		: Phan Tiến Lộc 
	/// Created date 	: 2016-11-12 
	/// Chi tiết yêu cầu xuất đổi
    /// 
    /// Update by       : Phạm Hải Đăng
    /// Created date 	: 2016-12-15
    /// Thêm 1 cột ghi chú
	/// </summary>	
	public partial class DA_ProductChangeOrderDT
    {



        #region Methods			


        /// <summary>
        /// Nạp thông tin chi tiết yêu cầu xuất đổi
        /// </summary>
        /// <param name="objProductChangeOrderDT">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref ProductChangeOrderDT objProductChangeOrderDT, string strProductChangeOrderDTID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductChangeOrderDTID, strProductChangeOrderDTID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objProductChangeOrderDT = new ProductChangeOrderDT();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductChangeOrderDTID])) objProductChangeOrderDT.ProductChangeOrderDTID = Convert.ToString(reader[ProductChangeOrderDT.colProductChangeOrderDTID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductChangeOrderID])) objProductChangeOrderDT.ProductChangeOrderID = Convert.ToString(reader[ProductChangeOrderDT.colProductChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colNPUTVoucherConcernID])) objProductChangeOrderDT.NputVoucherConcernID = Convert.ToString(reader[ProductChangeOrderDT.colNPUTVoucherConcernID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductID_Out])) objProductChangeOrderDT.ProductID_Out = Convert.ToString(reader[ProductChangeOrderDT.colProductID_Out]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIMEI_Out])) objProductChangeOrderDT.IMEI_Out = Convert.ToString(reader[ProductChangeOrderDT.colIMEI_Out]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIsNew_Out])) objProductChangeOrderDT.IsNew_Out = Convert.ToBoolean(reader[ProductChangeOrderDT.colIsNew_Out]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductID_IN])) objProductChangeOrderDT.ProductID_IN = Convert.ToString(reader[ProductChangeOrderDT.colProductID_IN]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIMEI_IN])) objProductChangeOrderDT.IMEI_IN = Convert.ToString(reader[ProductChangeOrderDT.colIMEI_IN]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIsNew_IN])) objProductChangeOrderDT.IsNew_IN = Convert.ToBoolean(reader[ProductChangeOrderDT.colIsNew_IN]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colQuantity])) objProductChangeOrderDT.Quantity = Convert.ToDecimal(reader[ProductChangeOrderDT.colQuantity]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colCreatedUser])) objProductChangeOrderDT.CreatedUser = Convert.ToString(reader[ProductChangeOrderDT.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colCreatedDate])) objProductChangeOrderDT.CreatedDate = Convert.ToDateTime(reader[ProductChangeOrderDT.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIsDeleted])) objProductChangeOrderDT.IsDeleted = Convert.ToBoolean(reader[ProductChangeOrderDT.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colDeletedUser])) objProductChangeOrderDT.DeletedUser = Convert.ToString(reader[ProductChangeOrderDT.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colDeletedDate])) objProductChangeOrderDT.DeletedDate = Convert.ToDateTime(reader[ProductChangeOrderDT.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colChangeNote])) objProductChangeOrderDT.ChangeNote = Convert.ToString(reader[ProductChangeOrderDT.colChangeNote]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colInstockStatusID_In])) objProductChangeOrderDT.InStockStatusID_In = Convert.ToInt32(reader[ProductChangeOrderDT.colInstockStatusID_In]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colInstockStatusID_Out])) objProductChangeOrderDT.InStockStatusID_Out = Convert.ToInt32(reader[ProductChangeOrderDT.colInstockStatusID_Out]);
                }
                else
                {
                    objProductChangeOrderDT = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết yêu cầu xuất đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrderDT -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public ResultMessage LoadInfoByParent(ref List<ProductChangeOrderDT> listProductChangeOrderDT, string strProductChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECTBYPARENT);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductChangeOrderID, strProductChangeOrderID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    if (listProductChangeOrderDT == null)
                    {
                        listProductChangeOrderDT = new List<ProductChangeOrderDT>();
                    }
                    ProductChangeOrderDT objProductChangeOrderDT = new ProductChangeOrderDT();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductChangeOrderDTID])) objProductChangeOrderDT.ProductChangeOrderDTID = Convert.ToString(reader[ProductChangeOrderDT.colProductChangeOrderDTID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductChangeOrderID])) objProductChangeOrderDT.ProductChangeOrderID = Convert.ToString(reader[ProductChangeOrderDT.colProductChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colNPUTVoucherConcernID])) objProductChangeOrderDT.NputVoucherConcernID = Convert.ToString(reader[ProductChangeOrderDT.colNPUTVoucherConcernID]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductID_Out])) objProductChangeOrderDT.ProductID_Out = Convert.ToString(reader[ProductChangeOrderDT.colProductID_Out]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIMEI_Out])) objProductChangeOrderDT.IMEI_Out = Convert.ToString(reader[ProductChangeOrderDT.colIMEI_Out]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIsNew_Out])) objProductChangeOrderDT.IsNew_Out = Convert.ToBoolean(reader[ProductChangeOrderDT.colIsNew_Out]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductID_IN])) objProductChangeOrderDT.ProductID_IN = Convert.ToString(reader[ProductChangeOrderDT.colProductID_IN]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIMEI_IN])) objProductChangeOrderDT.IMEI_IN = Convert.ToString(reader[ProductChangeOrderDT.colIMEI_IN]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIsNew_IN])) objProductChangeOrderDT.IsNew_IN = Convert.ToBoolean(reader[ProductChangeOrderDT.colIsNew_IN]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colQuantity])) objProductChangeOrderDT.Quantity = Convert.ToDecimal(reader[ProductChangeOrderDT.colQuantity]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colCreatedUser])) objProductChangeOrderDT.CreatedUser = Convert.ToString(reader[ProductChangeOrderDT.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colCreatedDate])) objProductChangeOrderDT.CreatedDate = Convert.ToDateTime(reader[ProductChangeOrderDT.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colIsDeleted])) objProductChangeOrderDT.IsDeleted = Convert.ToBoolean(reader[ProductChangeOrderDT.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colDeletedUser])) objProductChangeOrderDT.DeletedUser = Convert.ToString(reader[ProductChangeOrderDT.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colDeletedDate])) objProductChangeOrderDT.DeletedDate = Convert.ToDateTime(reader[ProductChangeOrderDT.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductID_OutName])) objProductChangeOrderDT.ProductID_OutName = Convert.ToString(reader[ProductChangeOrderDT.colProductID_OutName]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colProductID_INName])) objProductChangeOrderDT.ProductID_INName = Convert.ToString(reader[ProductChangeOrderDT.colProductID_INName]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colChangeNote])) objProductChangeOrderDT.ChangeNote = Convert.ToString(reader[ProductChangeOrderDT.colChangeNote]).Trim();
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colInstockStatusID_In])) objProductChangeOrderDT.InStockStatusID_In = Convert.ToInt32(reader[ProductChangeOrderDT.colInstockStatusID_In]);
                    if (!Convert.IsDBNull(reader[ProductChangeOrderDT.colInstockStatusID_Out])) objProductChangeOrderDT.InStockStatusID_Out = Convert.ToInt32(reader[ProductChangeOrderDT.colInstockStatusID_Out]);

                    if (!Convert.IsDBNull(reader["MACHINEERRORGROUPNAME"])) objProductChangeOrderDT.MachineErrorGroupName = Convert.ToString(reader["MACHINEERRORGROUPNAME"]).Trim();
                    if (!Convert.IsDBNull(reader["MACHINEERRORNAME"])) objProductChangeOrderDT.MachineErrorName = Convert.ToString(reader["MACHINEERRORNAME"]).Trim();
                    if (!Convert.IsDBNull(reader["MACHINEERRORNAME"])) objProductChangeOrderDT.MachineErrorID = Convert.ToString(reader["MACHINEERRORID"]).Trim();
                    listProductChangeOrderDT.Add(objProductChangeOrderDT);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết yêu cầu xuất đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrderDT -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        /// <summary>
        /// Thêm thông tin chi tiết yêu cầu xuất đổi
        /// </summary>
        /// <param name="objProductChangeOrderDT">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ProductChangeOrderDT objProductChangeOrderDT)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objProductChangeOrderDT);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết yêu cầu xuất đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrderDT -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin chi tiết yêu cầu xuất đổi
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objProductChangeOrderDT">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, ProductChangeOrderDT objProductChangeOrderDT)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductChangeOrderID, objProductChangeOrderDT.ProductChangeOrderID);
                objIData.AddParameter("@" + ProductChangeOrderDT.colNPUTVoucherConcernID, objProductChangeOrderDT.NputVoucherConcernID);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductID_Out, objProductChangeOrderDT.ProductID_Out);
                objIData.AddParameter("@" + ProductChangeOrderDT.colIMEI_Out, objProductChangeOrderDT.IMEI_Out);
                objIData.AddParameter("@" + ProductChangeOrderDT.colIsNew_Out, objProductChangeOrderDT.IsNew_Out);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductID_IN, objProductChangeOrderDT.ProductID_IN);
                objIData.AddParameter("@" + ProductChangeOrderDT.colIMEI_IN, objProductChangeOrderDT.IMEI_IN);
                objIData.AddParameter("@" + ProductChangeOrderDT.colIsNew_IN, objProductChangeOrderDT.IsNew_IN);
                objIData.AddParameter("@" + ProductChangeOrderDT.colQuantity, objProductChangeOrderDT.Quantity);
                objIData.AddParameter("@" + ProductChangeOrderDT.colCreatedUser, objProductChangeOrderDT.CreatedUser);
                objIData.AddParameter("@" + ProductChangeOrderDT.colChangeNote, objProductChangeOrderDT.ChangeNote.Trim());
                objIData.AddParameter("@" + ProductChangeOrderDT.colInstockStatusID_In, objProductChangeOrderDT.InStockStatusID_In);
                objIData.AddParameter("@" + ProductChangeOrderDT.colInstockStatusID_Out, objProductChangeOrderDT.InStockStatusID_Out);
                objIData.AddParameter("@MACHINEERRORIDLIST", objProductChangeOrderDT.MachineErrorID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin chi tiết yêu cầu xuất đổi
        /// </summary>
        /// <param name="objProductChangeOrderDT">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(ProductChangeOrderDT objProductChangeOrderDT)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objProductChangeOrderDT);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết yêu cầu xuất đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrderDT -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin chi tiết yêu cầu xuất đổi
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objProductChangeOrderDT">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, ProductChangeOrderDT objProductChangeOrderDT)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductChangeOrderDTID, objProductChangeOrderDT.ProductChangeOrderDTID);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductChangeOrderID, objProductChangeOrderDT.ProductChangeOrderID);
                objIData.AddParameter("@" + ProductChangeOrderDT.colNPUTVoucherConcernID, objProductChangeOrderDT.NputVoucherConcernID);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductID_Out, objProductChangeOrderDT.ProductID_Out);
                objIData.AddParameter("@" + ProductChangeOrderDT.colIMEI_Out, objProductChangeOrderDT.IMEI_Out);
                objIData.AddParameter("@" + ProductChangeOrderDT.colIsNew_Out, objProductChangeOrderDT.IsNew_Out);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductID_IN, objProductChangeOrderDT.ProductID_IN);
                objIData.AddParameter("@" + ProductChangeOrderDT.colIMEI_IN, objProductChangeOrderDT.IMEI_IN);
                objIData.AddParameter("@" + ProductChangeOrderDT.colIsNew_IN, objProductChangeOrderDT.IsNew_IN);
                objIData.AddParameter("@" + ProductChangeOrderDT.colQuantity, objProductChangeOrderDT.Quantity);
                objIData.AddParameter("@" + ProductChangeOrderDT.colChangeNote, objProductChangeOrderDT.ChangeNote.Trim());
                objIData.AddParameter("@" + ProductChangeOrderDT.colInstockStatusID_In, objProductChangeOrderDT.InStockStatusID_In);
                objIData.AddParameter("@" + ProductChangeOrderDT.colInstockStatusID_Out, objProductChangeOrderDT.InStockStatusID_Out);
                objIData.AddParameter("@MACHINEERRORIDLIST", objProductChangeOrderDT.MachineErrorID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin chi tiết yêu cầu xuất đổi
        /// </summary>
        /// <param name="objProductChangeOrderDT">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(ProductChangeOrderDT objProductChangeOrderDT)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objProductChangeOrderDT);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết yêu cầu xuất đổi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductChangeOrderDT -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin chi tiết yêu cầu xuất đổi
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objProductChangeOrderDT">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, ProductChangeOrderDT objProductChangeOrderDT)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + ProductChangeOrderDT.colProductChangeOrderDTID, objProductChangeOrderDT.ProductChangeOrderDTID);
                objIData.AddParameter("@" + ProductChangeOrderDT.colDeletedUser, objProductChangeOrderDT.DeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_ProductChangeOrderDT()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "PM_PRODUCTCHANGEORDERDT_ADD";
        public const String SP_UPDATE = "PM_PRODUCTCHANGEORDERDT_UPD";
        public const String SP_DELETE = "PM_PRODUCTCHANGEORDERDT_DEL";
        public const String SP_SELECT = "PM_PRODUCTCHANGEORDERDT_SEL";
        public const String SP_SELECTBYPARENT = "PM_PRODUCTCHANGEORDERDT_SELPAR";
        public const String SP_SEARCH = "PM_PRODUCTCHANGEORDERDT_SRH";
        public const String SP_UPDATEINDEX = "PM_PRODUCTCHANGEORDERDT_UPDINDEX";

        #endregion

    }
}
