
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using OutputVoucher = ERP.Inventory.BO.OutputVoucher;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Trương Trung Lợi 
	/// Created date 	: 11/09/2012 
	/// Phiếu xuất
    /// 
    /// 01/03/2017 : Hải Đăng bổ sung 1 trường kiểm tra tồn tại vat_invoice_product
	/// </summary>	
	public partial class DA_OutputVoucher
	{

        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

		#region Methods
		

		/// <summary>
		/// Nạp thông tin phiếu xuất
		/// </summary>
		/// <param name="objOutputVoucher">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref OutputVoucher objOutputVoucher, string strOutputVoucherID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + OutputVoucher.colOutputVoucherID, strOutputVoucherID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objOutputVoucher = new OutputVoucher();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colOutputVoucherID])) objOutputVoucher.OutputVoucherID = Convert.ToString(reader[OutputVoucher.colOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colOrderID])) objOutputVoucher.OrderID = Convert.ToString(reader[OutputVoucher.colOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colInvoiceID])) objOutputVoucher.InvoiceID = Convert.ToString(reader[OutputVoucher.colInvoiceID]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colInvoiceSymbol])) objOutputVoucher.InvoiceSymbol = Convert.ToString(reader[OutputVoucher.colInvoiceSymbol]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colDenominator])) objOutputVoucher.Denominator = Convert.ToString(reader[OutputVoucher.colDenominator]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCreatedStoreID])) objOutputVoucher.CreatedStoreID = Convert.ToInt32(reader[OutputVoucher.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colOutputStoreID])) objOutputVoucher.OutputStoreID = Convert.ToInt32(reader[OutputVoucher.colOutputStoreID]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCustomerID])) objOutputVoucher.CustomerID = Convert.ToInt32(reader[OutputVoucher.colCustomerID]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCustomerName])) objOutputVoucher.CustomerName = Convert.ToString(reader[OutputVoucher.colCustomerName]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCustomerAddress])) objOutputVoucher.CustomerAddress = Convert.ToString(reader[OutputVoucher.colCustomerAddress]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCustomerPhone])) objOutputVoucher.CustomerPhone = Convert.ToString(reader[OutputVoucher.colCustomerPhone]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCustomerTaxID])) objOutputVoucher.CustomerTaxID = Convert.ToString(reader[OutputVoucher.colCustomerTaxID]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCurrencyUnitID])) objOutputVoucher.CurrencyUnitID = Convert.ToInt32(reader[OutputVoucher.colCurrencyUnitID]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colPayableTypeID])) objOutputVoucher.PayableTypeID = Convert.ToInt32(reader[OutputVoucher.colPayableTypeID]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colDiscountReasonID])) objOutputVoucher.DiscountReasonID = Convert.ToInt32(reader[OutputVoucher.colDiscountReasonID]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colOutputContent])) objOutputVoucher.OutputContent = Convert.ToString(reader[OutputVoucher.colOutputContent]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colInvoiceDate])) objOutputVoucher.InvoiceDate = Convert.ToDateTime(reader[OutputVoucher.colInvoiceDate]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colOutputDate])) objOutputVoucher.OutputDate = Convert.ToDateTime(reader[OutputVoucher.colOutputDate]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colPayableDate])) objOutputVoucher.PayableDate = Convert.ToDateTime(reader[OutputVoucher.colPayableDate]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colStaffUser])) objOutputVoucher.StaffUser = Convert.ToString(reader[OutputVoucher.colStaffUser]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCurrencyExchange])) objOutputVoucher.CurrencyExchange = Convert.ToDecimal(reader[OutputVoucher.colCurrencyExchange]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colDiscount])) objOutputVoucher.Discount = Convert.ToDecimal(reader[OutputVoucher.colDiscount]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colTotalAmountBFT])) objOutputVoucher.TotalAmountBFT = Convert.ToDecimal(reader[OutputVoucher.colTotalAmountBFT]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colTotalVAT])) objOutputVoucher.TotalVAT = Convert.ToDecimal(reader[OutputVoucher.colTotalVAT]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colTotalAmount])) objOutputVoucher.TotalAmount = Convert.ToDecimal(reader[OutputVoucher.colTotalAmount]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colPromotionDiscount])) objOutputVoucher.PromotionDiscount = Convert.ToDecimal(reader[OutputVoucher.colPromotionDiscount]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colTotalCardSpend])) objOutputVoucher.TotalCardSpend = Convert.ToDecimal(reader[OutputVoucher.colTotalCardSpend]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colIsStoreChange])) objOutputVoucher.IsStoreChange = Convert.ToBoolean(reader[OutputVoucher.colIsStoreChange]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colIsPosted])) objOutputVoucher.IsPosted = Convert.ToBoolean(reader[OutputVoucher.colIsPosted]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colIsError])) objOutputVoucher.IsError = Convert.ToBoolean(reader[OutputVoucher.colIsError]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colErrorContent])) objOutputVoucher.ErrorContent = Convert.ToString(reader[OutputVoucher.colErrorContent]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colErrorOutputVoucherID])) objOutputVoucher.ErrorOutputVoucherID = Convert.ToString(reader[OutputVoucher.colErrorOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCreatedUser])) objOutputVoucher.CreatedUser = Convert.ToString(reader[OutputVoucher.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colCreatedDate])) objOutputVoucher.CreatedDate = Convert.ToDateTime(reader[OutputVoucher.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colUpdatedUser])) objOutputVoucher.UpdatedUser = Convert.ToString(reader[OutputVoucher.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colUpdatedDate])) objOutputVoucher.UpdatedDate = Convert.ToDateTime(reader[OutputVoucher.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colIsDeleted])) objOutputVoucher.IsDeleted = Convert.ToBoolean(reader[OutputVoucher.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[OutputVoucher.colDeletedUser])) objOutputVoucher.DeletedUser = Convert.ToString(reader[OutputVoucher.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[OutputVoucher.colDeletedDate])) objOutputVoucher.DeletedDate = Convert.ToDateTime(reader[OutputVoucher.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[OutputVoucher.colOutputNote])) objOutputVoucher.OutputNote = Convert.ToString(reader[OutputVoucher.colOutputNote]);
                    //Đăng bổ sung 1 cột địa chỉ khách hàng
                    if (!Convert.IsDBNull(reader[OutputVoucher.colTaxCustomerAddress])) objOutputVoucher.TaxCustomerAddress = Convert.ToString(reader[OutputVoucher.colTaxCustomerAddress]);
                    if (!Convert.IsDBNull(reader[OutputVoucher.colIsSInvoice])) objOutputVoucher.IsSInvoice = Convert.ToBoolean(reader[OutputVoucher.colIsSInvoice]);
                    if (!Convert.IsDBNull(reader[OutputVoucher.colBrokerUser])) objOutputVoucher.BrokerUser = Convert.ToString(reader[OutputVoucher.colBrokerUser]);
                }
 				else
 				{
 					objOutputVoucher = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin phiếu xuất", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin phiếu xuất
		/// </summary>
		/// <param name="objOutputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(OutputVoucher objOutputVoucher)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objOutputVoucher);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin phiếu xuất", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin phiếu xuất
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objOutputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, OutputVoucher objOutputVoucher)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + OutputVoucher.colOrderID, objOutputVoucher.OrderID);
				objIData.AddParameter("@" + OutputVoucher.colInvoiceID, objOutputVoucher.InvoiceID);
				objIData.AddParameter("@" + OutputVoucher.colInvoiceSymbol, objOutputVoucher.InvoiceSymbol);
				objIData.AddParameter("@" + OutputVoucher.colDenominator, objOutputVoucher.Denominator);
				objIData.AddParameter("@" + OutputVoucher.colCreatedStoreID, objOutputVoucher.CreatedStoreID);
				objIData.AddParameter("@" + OutputVoucher.colOutputStoreID, objOutputVoucher.OutputStoreID);
				objIData.AddParameter("@" + OutputVoucher.colCustomerID, objOutputVoucher.CustomerID);
				objIData.AddParameter("@" + OutputVoucher.colCustomerName, objOutputVoucher.CustomerName);
				objIData.AddParameter("@" + OutputVoucher.colCustomerAddress, objOutputVoucher.CustomerAddress);
				objIData.AddParameter("@" + OutputVoucher.colCustomerPhone, objOutputVoucher.CustomerPhone);
				objIData.AddParameter("@" + OutputVoucher.colCustomerTaxID, objOutputVoucher.CustomerTaxID);
				objIData.AddParameter("@" + OutputVoucher.colCurrencyUnitID, objOutputVoucher.CurrencyUnitID);
				objIData.AddParameter("@" + OutputVoucher.colPayableTypeID, objOutputVoucher.PayableTypeID);
				objIData.AddParameter("@" + OutputVoucher.colDiscountReasonID, objOutputVoucher.DiscountReasonID);
				objIData.AddParameter("@" + OutputVoucher.colOutputContent, objOutputVoucher.OutputContent);
				objIData.AddParameter("@" + OutputVoucher.colInvoiceDate, objOutputVoucher.InvoiceDate);
				objIData.AddParameter("@" + OutputVoucher.colOutputDate, objOutputVoucher.OutputDate);
				objIData.AddParameter("@" + OutputVoucher.colPayableDate, objOutputVoucher.PayableDate);
				objIData.AddParameter("@" + OutputVoucher.colStaffUser, objOutputVoucher.StaffUser);
				objIData.AddParameter("@" + OutputVoucher.colCurrencyExchange, objOutputVoucher.CurrencyExchange);
				objIData.AddParameter("@" + OutputVoucher.colDiscount, objOutputVoucher.Discount);
				objIData.AddParameter("@" + OutputVoucher.colTotalAmountBFT, objOutputVoucher.TotalAmountBFT);
				objIData.AddParameter("@" + OutputVoucher.colTotalVAT, objOutputVoucher.TotalVAT);
				objIData.AddParameter("@" + OutputVoucher.colTotalAmount, objOutputVoucher.TotalAmount);
				objIData.AddParameter("@" + OutputVoucher.colPromotionDiscount, objOutputVoucher.PromotionDiscount);
				objIData.AddParameter("@" + OutputVoucher.colTotalCardSpend, objOutputVoucher.TotalCardSpend);
				objIData.AddParameter("@" + OutputVoucher.colIsStoreChange, objOutputVoucher.IsStoreChange);
				objIData.AddParameter("@" + OutputVoucher.colIsPosted, objOutputVoucher.IsPosted);
				objIData.AddParameter("@" + OutputVoucher.colIsError, objOutputVoucher.IsError);
				objIData.AddParameter("@" + OutputVoucher.colErrorContent, objOutputVoucher.ErrorContent);
				objIData.AddParameter("@" + OutputVoucher.colErrorOutputVoucherID, objOutputVoucher.ErrorOutputVoucherID);
				objIData.AddParameter("@" + OutputVoucher.colCreatedUser, objOutputVoucher.CreatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + OutputVoucher.colOutputNote, objOutputVoucher.OutputNote);
                objIData.AddParameter("@" + OutputVoucher.colCreatedDate, objOutputVoucher.CreatedDate);
                objIData.AddParameter("@" + OutputVoucher.colTaxCustomerAddress, objOutputVoucher.TaxCustomerAddress);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objOutputVoucher.OutputVoucherID = reader["OutputVoucherID"].ToString().Trim();
                    objOutputVoucher.IsSInvoice = Convert.ToBoolean(Convert.ToInt32(reader["ISSINVOICE"]));
                }
            }
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin phiếu xuất
		/// </summary>
		/// <param name="objOutputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(OutputVoucher objOutputVoucher)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objOutputVoucher);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin phiếu xuất", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin phiếu xuất
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objOutputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, OutputVoucher objOutputVoucher)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + OutputVoucher.colOutputVoucherID, objOutputVoucher.OutputVoucherID);
				objIData.AddParameter("@" + OutputVoucher.colOrderID, objOutputVoucher.OrderID);
				objIData.AddParameter("@" + OutputVoucher.colInvoiceID, objOutputVoucher.InvoiceID);
				objIData.AddParameter("@" + OutputVoucher.colInvoiceSymbol, objOutputVoucher.InvoiceSymbol);
				objIData.AddParameter("@" + OutputVoucher.colDenominator, objOutputVoucher.Denominator);
				objIData.AddParameter("@" + OutputVoucher.colCreatedStoreID, objOutputVoucher.CreatedStoreID);
				objIData.AddParameter("@" + OutputVoucher.colOutputStoreID, objOutputVoucher.OutputStoreID);
				objIData.AddParameter("@" + OutputVoucher.colCustomerID, objOutputVoucher.CustomerID);
				objIData.AddParameter("@" + OutputVoucher.colCustomerName, objOutputVoucher.CustomerName);
				objIData.AddParameter("@" + OutputVoucher.colCustomerAddress, objOutputVoucher.CustomerAddress);
				objIData.AddParameter("@" + OutputVoucher.colCustomerPhone, objOutputVoucher.CustomerPhone);
				objIData.AddParameter("@" + OutputVoucher.colCustomerTaxID, objOutputVoucher.CustomerTaxID);
				objIData.AddParameter("@" + OutputVoucher.colCurrencyUnitID, objOutputVoucher.CurrencyUnitID);
				objIData.AddParameter("@" + OutputVoucher.colPayableTypeID, objOutputVoucher.PayableTypeID);
				objIData.AddParameter("@" + OutputVoucher.colDiscountReasonID, objOutputVoucher.DiscountReasonID);
				objIData.AddParameter("@" + OutputVoucher.colOutputContent, objOutputVoucher.OutputContent);
				objIData.AddParameter("@" + OutputVoucher.colInvoiceDate, objOutputVoucher.InvoiceDate);
				objIData.AddParameter("@" + OutputVoucher.colOutputDate, objOutputVoucher.OutputDate);
				objIData.AddParameter("@" + OutputVoucher.colPayableDate, objOutputVoucher.PayableDate);
				objIData.AddParameter("@" + OutputVoucher.colStaffUser, objOutputVoucher.StaffUser);
				objIData.AddParameter("@" + OutputVoucher.colCurrencyExchange, objOutputVoucher.CurrencyExchange);
				objIData.AddParameter("@" + OutputVoucher.colDiscount, objOutputVoucher.Discount);
				objIData.AddParameter("@" + OutputVoucher.colTotalAmountBFT, objOutputVoucher.TotalAmountBFT);
				objIData.AddParameter("@" + OutputVoucher.colTotalVAT, objOutputVoucher.TotalVAT);
				objIData.AddParameter("@" + OutputVoucher.colTotalAmount, objOutputVoucher.TotalAmount);
				objIData.AddParameter("@" + OutputVoucher.colPromotionDiscount, objOutputVoucher.PromotionDiscount);
				objIData.AddParameter("@" + OutputVoucher.colTotalCardSpend, objOutputVoucher.TotalCardSpend);
				objIData.AddParameter("@" + OutputVoucher.colIsStoreChange, objOutputVoucher.IsStoreChange);
				objIData.AddParameter("@" + OutputVoucher.colIsPosted, objOutputVoucher.IsPosted);
				objIData.AddParameter("@" + OutputVoucher.colIsError, objOutputVoucher.IsError);
				objIData.AddParameter("@" + OutputVoucher.colErrorContent, objOutputVoucher.ErrorContent);
				objIData.AddParameter("@" + OutputVoucher.colErrorOutputVoucherID, objOutputVoucher.ErrorOutputVoucherID);
				objIData.AddParameter("@" + OutputVoucher.colUpdatedUser, objOutputVoucher.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + OutputVoucher.colOutputNote, objOutputVoucher.OutputNote);
                objIData.AddParameter("@" + OutputVoucher.colTaxCustomerAddress, objOutputVoucher.TaxCustomerAddress);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin phiếu xuất
		/// </summary>
		/// <param name="objOutputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(OutputVoucher objOutputVoucher)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objOutputVoucher);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin phiếu xuất", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin phiếu xuất
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objOutputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, OutputVoucher objOutputVoucher)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + OutputVoucher.colOutputVoucherID, objOutputVoucher.OutputVoucherID);
				objIData.AddParameter("@" + OutputVoucher.colDeletedUser, objOutputVoucher.DeletedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_OutputVoucher()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_OUTPUTVOUCHER_ADD";
		public const String SP_UPDATE = "PM_OUTPUTVOUCHER_UPD";
		public const String SP_DELETE = "PM_OUTPUTVOUCHER_DEL";
		public const String SP_SELECT = "PM_OUTPUTVOUCHER_SEL";
		public const String SP_SEARCH = "PM_OUTPUTVOUCHER_SRH";
		public const String SP_UPDATEINDEX = "PM_OUTPUTVOUCHER_UPDINDEX";
		#endregion
		
	}
}
