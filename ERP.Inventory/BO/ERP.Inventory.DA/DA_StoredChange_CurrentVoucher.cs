﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using ERP.Inventory.BO;

namespace ERP.Inventory.DA
{
    public class DA_StoredChange_CurrentVoucher
    {
        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion
        /// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objStoredChange_CurrentVoucher">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref StoredChange_CurrentVoucher objStoredChange_CurrentVoucher, int intStoreID, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colStoreID, intStoreID);
                objIData.AddParameter("@UserName", strUserName);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objStoredChange_CurrentVoucher = new StoredChange_CurrentVoucher();
                    if (!Convert.IsDBNull(reader[StoredChange_CurrentVoucher.colStoreID])) objStoredChange_CurrentVoucher.StoreID = Convert.ToInt32(reader[StoredChange_CurrentVoucher.colStoreID]);
                    if (!Convert.IsDBNull(reader[StoredChange_CurrentVoucher.colUserName])) objStoredChange_CurrentVoucher.UserName = Convert.ToString(reader[StoredChange_CurrentVoucher.colUserName]).Trim();
                    if (!Convert.IsDBNull(reader[StoredChange_CurrentVoucher.colInvoiceID])) objStoredChange_CurrentVoucher.InvoiceID = Convert.ToDecimal(reader[StoredChange_CurrentVoucher.colInvoiceID]);
                    if (!Convert.IsDBNull(reader[StoredChange_CurrentVoucher.colInvoiceSymbol])) objStoredChange_CurrentVoucher.InvoiceSymbol = Convert.ToString(reader[StoredChange_CurrentVoucher.colInvoiceSymbol]).Trim();
                    if (!Convert.IsDBNull(reader[StoredChange_CurrentVoucher.colDenominator])) objStoredChange_CurrentVoucher.Denominator = Convert.ToString(reader[StoredChange_CurrentVoucher.colDenominator]).Trim();
                    if (!Convert.IsDBNull(reader["InvoiceIDStart"])) objStoredChange_CurrentVoucher.InvoiceIdStart = Convert.ToInt32(reader["InvoiceIDStart"]);

                    if (!Convert.IsDBNull(reader["STORECHANGECURVOUCHERID"])) objStoredChange_CurrentVoucher.STORECHANGECURVOUCHERID = Convert.ToString(reader["STORECHANGECURVOUCHERID"]).Trim();
                    if (!Convert.IsDBNull(reader["INVOICEEND"])) objStoredChange_CurrentVoucher.INVOICEEND = Convert.ToDecimal(reader["INVOICEEND"]);
                    if (!Convert.IsDBNull(reader["FROMDATE"])) objStoredChange_CurrentVoucher.FROMDATE = Convert.ToDateTime(reader["FROMDATE"]);
                    if (!Convert.IsDBNull(reader["TODATE"])) objStoredChange_CurrentVoucher.TODATE = Convert.ToDateTime(reader["TODATE"]);
                    if (!Convert.IsDBNull(reader["CREATEDDATE"])) objStoredChange_CurrentVoucher.TODATE = Convert.ToDateTime(reader["CREATEDDATE"]);
                    if (!Convert.IsDBNull(reader["UPDATEDDATE"])) objStoredChange_CurrentVoucher.TODATE = Convert.ToDateTime(reader["UPDATEDDATE"]);
                }
                else
                {
                    objStoredChange_CurrentVoucher = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoredChange_CurrentVoucher -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objStoredChange_CurrentVoucher">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(StoredChange_CurrentVoucher objStoredChange_CurrentVoucher)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objStoredChange_CurrentVoucher);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoredChange_CurrentVoucher -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objStoredChange_CurrentVoucher">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, StoredChange_CurrentVoucher objStoredChange_CurrentVoucher)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colStoreID, objStoredChange_CurrentVoucher.StoreID);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colUserName, objStoredChange_CurrentVoucher.UserName);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colInvoiceID, objStoredChange_CurrentVoucher.InvoiceID);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colInvoiceSymbol, objStoredChange_CurrentVoucher.InvoiceSymbol);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colDenominator, objStoredChange_CurrentVoucher.Denominator);

                objIData.AddParameter("@STORECHANGECURVOUCHERID", objStoredChange_CurrentVoucher.STORECHANGECURVOUCHERID);
                objIData.AddParameter("@INVOICEEND", objStoredChange_CurrentVoucher.INVOICEEND);
                objIData.AddParameter("@FROMDATE", objStoredChange_CurrentVoucher.FROMDATE);
                objIData.AddParameter("@TODATE", objStoredChange_CurrentVoucher.TODATE);
                objIData.AddParameter("@INVOICEIDSTART", objStoredChange_CurrentVoucher.InvoiceIdStart);

                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objStoredChange_CurrentVoucher">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(StoredChange_CurrentVoucher objStoredChange_CurrentVoucher)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objStoredChange_CurrentVoucher);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoredChange_CurrentVoucher -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objStoredChange_CurrentVoucher">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, StoredChange_CurrentVoucher objStoredChange_CurrentVoucher)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colStoreID, objStoredChange_CurrentVoucher.StoreID);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colUserName, objStoredChange_CurrentVoucher.UserName);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colInvoiceID, objStoredChange_CurrentVoucher.InvoiceID);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colInvoiceSymbol, objStoredChange_CurrentVoucher.InvoiceSymbol);
                objIData.AddParameter("@" + StoredChange_CurrentVoucher.colDenominator, objStoredChange_CurrentVoucher.Denominator);

                objIData.AddParameter("@STORECHANGECURVOUCHERID", objStoredChange_CurrentVoucher.STORECHANGECURVOUCHERID);
                objIData.AddParameter("@INVOICEEND", objStoredChange_CurrentVoucher.INVOICEEND);
                objIData.AddParameter("@INVOICEIDSTART", objStoredChange_CurrentVoucher.InvoiceIdStart);
                objIData.AddParameter("@FROMDATE", objStoredChange_CurrentVoucher.FROMDATE);
                objIData.AddParameter("@TODATE", objStoredChange_CurrentVoucher.TODATE);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage UpdateVoucher(StoreChange_CurrentVoucherUpdate objStoreChange_CurrentVoucherUpdate, ref bool bolIsDuplicate)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                UpdateVoucher(objIData, objStoreChange_CurrentVoucherUpdate, ref bolIsDuplicate);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoredChange_CurrentVoucher -> UpdateVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        public void UpdateVoucher(IData objIData, StoreChange_CurrentVoucherUpdate objStoreChange_CurrentVoucherUpdate, ref bool bolIsDuplicate)
        {
            try
            {
                objIData.CreateNewStoredProcedure("STORECHANGE_UPDATEVOUCHER");
                objIData.AddParameter("@STORECHANGEID", objStoreChange_CurrentVoucherUpdate.StoreChangeId.Trim());
                objIData.AddParameter("@OUTPUTVOUCHERID", objStoreChange_CurrentVoucherUpdate.OutputVoucherID.Trim());
                objIData.AddParameter("@Denominator", objStoreChange_CurrentVoucherUpdate.Denominator);
                objIData.AddParameter("@InvoiceSymbol", objStoreChange_CurrentVoucherUpdate.InvoiceSymbol);
                objIData.AddParameter("@InvoiceId", objStoreChange_CurrentVoucherUpdate.InvoiceId);
                objIData.AddParameter("@StoreId", objStoreChange_CurrentVoucherUpdate.StoreId);
                objIData.AddParameter("@PrintedReason", objStoreChange_CurrentVoucherUpdate.PrintedReason);
                objIData.AddParameter("@UserName", objStoreChange_CurrentVoucherUpdate.UserName);
                objIData.AddParameter("@InvoiceIdNew", objStoreChange_CurrentVoucherUpdate.InvoiceIdNew);
                objIData.AddParameter("@IsReNewPrint", objStoreChange_CurrentVoucherUpdate.IsReNewPrint);
                objIData.AddParameter("@IsReOldPrint", objStoreChange_CurrentVoucherUpdate.IsReOldPrint);
                objIData.AddParameter("@STORECHANGECURVOUCHERID", objStoreChange_CurrentVoucherUpdate.STORECHANGECURVOUCHERID);

                objIData.AddParameter("@STORECHANGECURVOUCHERIDOLD", objStoreChange_CurrentVoucherUpdate.STORECHANGECURVOUCHERIDOld);
                objIData.AddParameter("@DenominatorOld", objStoreChange_CurrentVoucherUpdate.DenominatorOld);
                objIData.AddParameter("@InvoiceSymbolOld", objStoreChange_CurrentVoucherUpdate.InvoiceSymbolOld);
                bolIsDuplicate = Convert.ToInt32(objIData.ExecStoreToString()) > 0;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage GetHistoryPrint(ref DataTable dtbData, string strStoreChangeId)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("CURRENTVOUCHER_PRINT_SEL");
                objIData.AddParameter("@StoreChangeId", strStoreChangeId);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy thông tin lịch sử in kim", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoredChange_CurrentVoucher -> GetHistoryPrint", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("STORECHANGE_CURRENTVOUCHER_SR");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin khu vực", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoredChange_CurrentVoucher -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        #region Constructor

        public DA_StoredChange_CurrentVoucher()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "STORECHANGE_CURRENTVOUCHER_AD";
        public const String SP_UPDATE = "STORECHANGE_CURRENTVOUCHER_UP";
        public const String SP_SELECT = "STORECHANGE_CURRENTVOUCHER_SE";
        #endregion
    }
}
