
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using StoreChangeCommandDTImei = ERP.Inventory.BO.StoreChangeCommandDTImei;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 11/22/2012 
	/// 
	/// </summary>	
	public partial class DA_StoreChangeCommandDTimeI
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objStoreChangeCommandDTimeI">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref StoreChangeCommandDTImei objStoreChangeCommandDTimeI, string strIMEI, string strStoreChangeCommandDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colIMEI, strIMEI);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colStoreChangeCommandDetailID, strStoreChangeCommandDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objStoreChangeCommandDTimeI = new StoreChangeCommandDTImei();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colStoreChangeCommandDetailID])) objStoreChangeCommandDTimeI.StoreChangeCommandDetailID = Convert.ToString(reader[StoreChangeCommandDTImei.colStoreChangeCommandDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colIMEI])) objStoreChangeCommandDTimeI.IMEI = Convert.ToString(reader[StoreChangeCommandDTImei.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colIsStoreChange])) objStoreChangeCommandDTimeI.IsStoreChange = Convert.ToBoolean(reader[StoreChangeCommandDTImei.colIsStoreChange]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colCreatedStoreID])) objStoreChangeCommandDTimeI.CreatedStoreID = Convert.ToInt32(reader[StoreChangeCommandDTImei.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colCreatedUser])) objStoreChangeCommandDTimeI.CreatedUser = Convert.ToString(reader[StoreChangeCommandDTImei.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colCreatedDate])) objStoreChangeCommandDTimeI.CreatedDate = Convert.ToDateTime(reader[StoreChangeCommandDTImei.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colUpdatedUser])) objStoreChangeCommandDTimeI.UpdatedUser = Convert.ToString(reader[StoreChangeCommandDTImei.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colUpdatedDate])) objStoreChangeCommandDTimeI.UpdatedDate = Convert.ToDateTime(reader[StoreChangeCommandDTImei.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colIsDeleted])) objStoreChangeCommandDTimeI.IsDeleted = Convert.ToBoolean(reader[StoreChangeCommandDTImei.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colDeletedUser])) objStoreChangeCommandDTimeI.DeletedUser = Convert.ToString(reader[StoreChangeCommandDTImei.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommandDTImei.colDeletedDate])) objStoreChangeCommandDTimeI.DeletedDate = Convert.ToDateTime(reader[StoreChangeCommandDTImei.colDeletedDate]);
 				}
 				else
 				{
 					objStoreChangeCommandDTimeI = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommandDTimeI -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objStoreChangeCommandDTimeI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(StoreChangeCommandDTImei objStoreChangeCommandDTimeI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objStoreChangeCommandDTimeI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommandDTimeI -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeCommandDTimeI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, StoreChangeCommandDTImei objStoreChangeCommandDTimeI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colStoreChangeCommandDetailID, objStoreChangeCommandDTimeI.StoreChangeCommandDetailID);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colIMEI, objStoreChangeCommandDTimeI.IMEI);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colIsStoreChange, objStoreChangeCommandDTimeI.IsStoreChange);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colCreatedStoreID, objStoreChangeCommandDTimeI.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colCreatedUser, objStoreChangeCommandDTimeI.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objStoreChangeCommandDTimeI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(StoreChangeCommandDTImei objStoreChangeCommandDTimeI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objStoreChangeCommandDTimeI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommandDTimeI -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeCommandDTimeI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, StoreChangeCommandDTImei objStoreChangeCommandDTimeI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colStoreChangeCommandDetailID, objStoreChangeCommandDTimeI.StoreChangeCommandDetailID);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colIMEI, objStoreChangeCommandDTimeI.IMEI);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colIsStoreChange, objStoreChangeCommandDTimeI.IsStoreChange);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colCreatedStoreID, objStoreChangeCommandDTimeI.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colUpdatedUser, objStoreChangeCommandDTimeI.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objStoreChangeCommandDTimeI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(StoreChangeCommandDTImei objStoreChangeCommandDTimeI)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objStoreChangeCommandDTimeI);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommandDTimeI -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeCommandDTimeI">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, StoreChangeCommandDTImei objStoreChangeCommandDTimeI)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colStoreChangeCommandDetailID, objStoreChangeCommandDTimeI.StoreChangeCommandDetailID);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colIMEI, objStoreChangeCommandDTimeI.IMEI);
				objIData.AddParameter("@" + StoreChangeCommandDTImei.colDeletedUser, objStoreChangeCommandDTimeI.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_StoreChangeCommandDTimeI()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_STORECHANGECOMMANDDTIMEI_ADD";
		public const String SP_UPDATE = "PM_STORECHANGECOMMANDDTIMEI_UPD";
		public const String SP_DELETE = "PM_STORECHANGECOMMANDDTIMEI_DEL";
		public const String SP_SELECT = "PM_STORECHANGECOMMANDDTIMEI_SEL";
		public const String SP_SEARCH = "PM_STORECHANGECOMMANDDTIMEI_SRH";
		public const String SP_UPDATEINDEX = "PM_STORECHANGECOMMANDDTIMEI_UPDINDEX";
		#endregion
		
	}
}
