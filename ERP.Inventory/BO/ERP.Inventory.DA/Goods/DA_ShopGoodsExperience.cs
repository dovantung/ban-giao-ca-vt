﻿
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using SHOP_GOODS_EXPERIENCE = ERP.Inventory.BO.Goods.ShopGoodsExperience;
#endregion
namespace ERP.Inventory.DA.Goods
{
    /// <summary>
	/// Created by 		: TruongTX 
	/// Created date 	: 9/18/2018 
	/// 
	/// </summary>	
	public partial class DA_ShopGoodsExperience
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objSHOP_GOODS_EXPERIENCE">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref SHOP_GOODS_EXPERIENCE objSHOP_GOODS_EXPERIENCE)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objSHOP_GOODS_EXPERIENCE = new SHOP_GOODS_EXPERIENCE();
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colSHOP_GOODS_EXPERIENCE_ID])) objSHOP_GOODS_EXPERIENCE.SHOP_GOODS_EXPERIENCE_ID = Convert.ToDecimal(reader[SHOP_GOODS_EXPERIENCE.colSHOP_GOODS_EXPERIENCE_ID]);
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colSHOP_ID])) objSHOP_GOODS_EXPERIENCE.SHOP_ID = Convert.ToDecimal(reader[SHOP_GOODS_EXPERIENCE.colSHOP_ID]);
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colGOODS_ID])) objSHOP_GOODS_EXPERIENCE.GOODS_ID = Convert.ToDecimal(reader[SHOP_GOODS_EXPERIENCE.colGOODS_ID]);
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colGOODS_Group_ID])) objSHOP_GOODS_EXPERIENCE.GOODS_Group_ID = Convert.ToDecimal(reader[SHOP_GOODS_EXPERIENCE.colGOODS_Group_ID]);
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colProduct_Type])) objSHOP_GOODS_EXPERIENCE.Product_Type = Convert.ToDecimal(reader[SHOP_GOODS_EXPERIENCE.colProduct_Type]);
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colFrom_Date])) objSHOP_GOODS_EXPERIENCE.From_Date = Convert.ToDateTime(reader[SHOP_GOODS_EXPERIENCE.colFrom_Date]);
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colTo_Date])) objSHOP_GOODS_EXPERIENCE.To_Date = Convert.ToDateTime(reader[SHOP_GOODS_EXPERIENCE.colTo_Date]);
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colUser_ID])) objSHOP_GOODS_EXPERIENCE.User_ID = Convert.ToString(reader[SHOP_GOODS_EXPERIENCE.colUser_ID]).Trim();
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colUser_SHOP_ID])) objSHOP_GOODS_EXPERIENCE.User_SHOP_ID = Convert.ToDecimal(reader[SHOP_GOODS_EXPERIENCE.colUser_SHOP_ID]);
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colCreate_Date])) objSHOP_GOODS_EXPERIENCE.Create_Date = Convert.ToDateTime(reader[SHOP_GOODS_EXPERIENCE.colCreate_Date]);
 					if (!Convert.IsDBNull(reader[SHOP_GOODS_EXPERIENCE.colCOUPLE_GOODS_Code])) objSHOP_GOODS_EXPERIENCE.COUPLE_GOODS_Code = Convert.ToString(reader[SHOP_GOODS_EXPERIENCE.colCOUPLE_GOODS_Code]).Trim();
 				}
 				else
 				{
 					objSHOP_GOODS_EXPERIENCE = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SHOP_GOODS_EXPERIENCE -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objSHOP_GOODS_EXPERIENCE">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(SHOP_GOODS_EXPERIENCE objSHOP_GOODS_EXPERIENCE)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objSHOP_GOODS_EXPERIENCE);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SHOP_GOODS_EXPERIENCE -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objSHOP_GOODS_EXPERIENCE">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, SHOP_GOODS_EXPERIENCE objSHOP_GOODS_EXPERIENCE)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colSHOP_GOODS_EXPERIENCE_ID, objSHOP_GOODS_EXPERIENCE.SHOP_GOODS_EXPERIENCE_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colSHOP_ID, objSHOP_GOODS_EXPERIENCE.SHOP_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colGOODS_ID, objSHOP_GOODS_EXPERIENCE.GOODS_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colGOODS_Group_ID, objSHOP_GOODS_EXPERIENCE.GOODS_Group_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colProduct_Type, objSHOP_GOODS_EXPERIENCE.Product_Type);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colFrom_Date, objSHOP_GOODS_EXPERIENCE.From_Date);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colTo_Date, objSHOP_GOODS_EXPERIENCE.To_Date);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colUser_ID, objSHOP_GOODS_EXPERIENCE.User_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colUser_SHOP_ID, objSHOP_GOODS_EXPERIENCE.User_SHOP_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colCreate_Date, objSHOP_GOODS_EXPERIENCE.Create_Date);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colCOUPLE_GOODS_Code, objSHOP_GOODS_EXPERIENCE.COUPLE_GOODS_Code);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objSHOP_GOODS_EXPERIENCE">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(SHOP_GOODS_EXPERIENCE objSHOP_GOODS_EXPERIENCE)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objSHOP_GOODS_EXPERIENCE);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SHOP_GOODS_EXPERIENCE -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objSHOP_GOODS_EXPERIENCE">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, SHOP_GOODS_EXPERIENCE objSHOP_GOODS_EXPERIENCE)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colSHOP_GOODS_EXPERIENCE_ID, objSHOP_GOODS_EXPERIENCE.SHOP_GOODS_EXPERIENCE_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colSHOP_ID, objSHOP_GOODS_EXPERIENCE.SHOP_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colGOODS_ID, objSHOP_GOODS_EXPERIENCE.GOODS_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colGOODS_Group_ID, objSHOP_GOODS_EXPERIENCE.GOODS_Group_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colProduct_Type, objSHOP_GOODS_EXPERIENCE.Product_Type);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colFrom_Date, objSHOP_GOODS_EXPERIENCE.From_Date);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colTo_Date, objSHOP_GOODS_EXPERIENCE.To_Date);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colUser_ID, objSHOP_GOODS_EXPERIENCE.User_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colUser_SHOP_ID, objSHOP_GOODS_EXPERIENCE.User_SHOP_ID);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colCreate_Date, objSHOP_GOODS_EXPERIENCE.Create_Date);
				objIData.AddParameter("@" + SHOP_GOODS_EXPERIENCE.colCOUPLE_GOODS_Code, objSHOP_GOODS_EXPERIENCE.COUPLE_GOODS_Code);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objSHOP_GOODS_EXPERIENCE">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(SHOP_GOODS_EXPERIENCE objSHOP_GOODS_EXPERIENCE)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objSHOP_GOODS_EXPERIENCE);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SHOP_GOODS_EXPERIENCE -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objSHOP_GOODS_EXPERIENCE">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, SHOP_GOODS_EXPERIENCE objSHOP_GOODS_EXPERIENCE)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_ShopGoodsExperience()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "SHOP_GOODS_EXPERIENCE_ADD";
		public const String SP_UPDATE = "SHOP_GOODS_EXPERIENCE_UPD";
		public const String SP_DELETE = "SHOP_GOODS_EXPERIENCE_DEL";
		public const String SP_SELECT = "SHOP_GOODS_EXPERIENCE_SEL";
		public const String SP_SEARCH = "SHOP_GOODS_EXPERIENCE_SRH";
		public const String SP_UPDATEINDEX = "SHOP_GOODS_EXPERIENCE_UPDINDEX";
		#endregion
    }
}


