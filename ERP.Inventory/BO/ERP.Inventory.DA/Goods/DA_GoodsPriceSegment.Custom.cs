﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.Goods
{
    public partial class DA_GoodsPriceSegment
    {
        #region Methods

        public ResultMessage LoadDataProductType(ref DataTable dtbData)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.load_nganh_hang");
                dtbData = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsPriceSegment -> LoadDataGoodsDMTB", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm thông tin định mức trưng bày
        /// </summary>
        /// <param name="dtbData">Dữ liệu danh sách định mức trưng bày</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadDataGoodsPriceSegment(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.get_price_segment");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsPriceSegment -> LoadDataGoodsDMTB", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ref string objValue, params object[] objKeywords)
        {

            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.add_price_segment");
                objIData.AddParameter(objKeywords);
                objValue = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Thêm mới thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsPriceSegment -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;

        }

        /// <summary>
        /// Sửa thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(params object[] objKeywords)
        {

            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.update_price_segment");
                objIData.AddParameter(objKeywords);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi sửa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsPriceSegment -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;

        }

        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objKeywords">Đối tượng truyền vào</param>
        public ResultMessage Delete(params object[] objKeywords)
        {

            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.delete_price_segment");
                objIData.AddParameter(objKeywords);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi sửa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsPriceSegment -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;

        }

        #endregion

    }
}

