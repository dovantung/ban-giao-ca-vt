﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.Goods
{
    public partial class DA_SameLevelDividing
    {
        #region Methods
        public ResultMessage SearchData(ref DataTable dtResult, params  object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("COMMON_SELECT");
                objIData.AddParameter(objKeywords);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SameLevelDividing -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage SearchRotatingHistory(ref DataTable dtRedundance, ref DataTable dtLack,
            ref DataTable dtGoods, ref DataTable dtShop, ref DataTable dtResult,
            String goods_rotate_history_id, String size)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("erp.same_level_dividing_common");
                object[] objKeywords = new object[]
                    {
                      "@sql_type","sql_shop_rotate_list",
                      "@param", ":goods_rotate_history_id|'"+goods_rotate_history_id+"'|:type|'THUA'",
                    };
                objIData.AddParameter(objKeywords);
                dtRedundance = objIData.ExecStoreToDataTable();

                objIData.CreateNewStoredProcedure("erp.same_level_dividing_common");
                objKeywords = new object[]
                    {
                      "@sql_type","sql_shop_rotate_list",
                      "@param", ":goods_rotate_history_id|'"+goods_rotate_history_id+"'|:type|'THIEU'",
                    };
                objIData.AddParameter(objKeywords);
                dtLack = objIData.ExecStoreToDataTable();

                objIData.CreateNewStoredProcedure("erp.same_level_dividing_common");
                objKeywords = new object[]
                    {
                      "@sql_type","sql_goods_rotate_param",
                      "@param", ":goods_rotate_history_id|'"+goods_rotate_history_id+"'",
                    };
                objIData.AddParameter(objKeywords);
                dtGoods = objIData.ExecStoreToDataTable();

                objIData.CreateNewStoredProcedure("erp.same_level_dividing_common");
                objKeywords = new object[]
                    {
                      "@sql_type","sql_shop_rotate_source_list",
                      "@param", ":goods_rotate_history_id|'"+goods_rotate_history_id+"'",
                    };
                objIData.AddParameter(objKeywords);
                dtShop = objIData.ExecStoreToDataTable();

                objIData.CreateNewStoredProcedure("erp.same_level_dividing_common");
                if (size != null && !"".Equals(size))
                {
                    objKeywords = new object[]
                    {
                      "@sql_type","sql_goods_rotate_result",
                      "@param", ":goods_rotate_history_id|'"+goods_rotate_history_id+"'|:size_sql| and rownum <= '"+size+"'",
                    };
                }
                else
                {
                    objKeywords = new object[]
                    {
                      "@sql_type","sql_goods_rotate_result",
                      "@param", ":goods_rotate_history_id|'"+goods_rotate_history_id+"'|:size_sql| ",
                    };
                }
                objIData.AddParameter(objKeywords);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SameLevelDividing -> SearchRotatingHistory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GoodsRotate(ref DataTable dtResult, object[] objKeywords, object[] objGoods, object[] objShops)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.add_goods_rotate_history");
                objIData.AddParameter(objKeywords);
                string goods_rotate_history_id = objIData.ExecStoreToString();
                object[] objectParameter;
                if (objGoods != null && objGoods.Length > 0) { 
                    for (int i = 0; i < objGoods.Length; i++)
                    {
                        objectParameter = new object[]{
                            "@goods_rotate_history_id",goods_rotate_history_id,
                            "@value_id", objGoods[i],
                            "@type", "0",
                           };
                        objIData.CreateNewStoredProcedure("ERP.pck_dividing.add_goods_rotate_param_list");
                        objIData.AddParameter(objectParameter);
                        objIData.ExecNonQuery();

                    }
                }
                if (objShops != null && objShops.Length > 0)
                {
                    for (int i = 0; i < objShops.Length; i++)
                    {
                        objectParameter = new object[]{
                            "@goods_rotate_history_id",goods_rotate_history_id,
                            "@value_id", objShops[i],
                            "@type", "1",
                           };
                        objIData.CreateNewStoredProcedure("ERP.pck_dividing.add_goods_rotate_param_list");
                        objIData.AddParameter(objectParameter);
                        objIData.ExecNonQuery();

                    }
                }

                objectParameter = new object[]{
                    "@goods_div_his_id",goods_rotate_history_id,
                    "@cencode", "-1",
                    "@province", "-1",
                   };
                objIData.CreateNewStoredProcedure("ERP.pck_goods_rotate.get_goods_rotate");
                objIData.AddParameter(objectParameter);
                objIData.ExecNonQuery();

                objectParameter = new object[]{
                    "@goods_rotate_history_id",goods_rotate_history_id,
                   };
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.goods_serial_rotate_his_infor");
                objIData.AddParameter(objectParameter);
                dtResult = objIData.ExecStoreToDataTable();

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SameLevelDividing -> SearchRotatingHistory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage RecyclingRotateGoods(ref DataTable dtResult, String goods_rotate_history_id)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_goods_rotate.cal_goods_rotate");
                object[] objKeywords = new object[]
                    {
                      "@goods_div_his_id",goods_rotate_history_id,
                      };
                objIData.AddParameter(objKeywords);
                objIData.ExecNonQuery();

                objIData.CreateNewStoredProcedure("erp.same_level_dividing_common");
                objKeywords = new object[]
                    {
                      "@sql_type","sql_goods_rotate_result",
                      "@param", ":goods_rotate_history_id|'"+goods_rotate_history_id+"'|:size_sql| ",
                    };
                objIData.AddParameter(objKeywords);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SameLevelDividing -> SearchRotatingHistory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion

    }
}
