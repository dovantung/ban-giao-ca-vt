﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.Goods
{
    public partial class DA_Holidays
    {
        public Library.WebCore.ResultMessage LoadHolidays(ref System.Data.DataTable dtResult)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.load_event_list");

                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi lấy thông tin ngày nghỉ ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Holidays -> LoadHolidays", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public Library.WebCore.ResultMessage LoadGoods(ref System.Data.DataTable dtResult, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.load_good_event");
                objIData.AddParameter(objKeywords);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi lấy dữ liệu mặt hàng ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Holidays -> LoadGoods", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage LoadShopByShopId(ref System.Data.DataTable dtResult, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.load_shop_event");
                objIData.AddParameter(objKeywords);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi lấy thông tin siêu thị ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Holidays -> LoadShopByShopId", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage AddEventHolidays(string strGoods, string strShops, string fromDate, string toDate,
            string nameEvent, string strDays, String rate, String userId, String shopId, ref string eventId)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.insert_event_list");
                object[] objectParameter = new object[]{
                        "@name", nameEvent,
                        "@from_date", fromDate,
                        "@to_date", toDate,
                        "@rate", rate,
                        "@user_id", userId,
                        "@shop_id", shopId,
                        "@days", strDays
                    };
                objIData.AddParameter(objectParameter);
                eventId = objIData.ExecStoreToString();
                
                String[] arrShop = strShops.Split('|');
                foreach(String shop in arrShop){
                    if (shop != null && !String.IsNullOrEmpty(shop))
                    {
                        objIData.CreateNewStoredProcedure("ERP.pck_dividing.inser_shop_event_list");
                        objectParameter = new object[]{
                            "@event_id", eventId,
                            "@shop_id", shop, 
                        };
                        objIData.AddParameter(objectParameter);
                        objIData.ExecNonQuery();

                    }
                }
                String[] arrGoods = strGoods.Split('|');
                foreach (String goods in arrGoods)
                {
                    if (goods != null && !String.IsNullOrEmpty(goods))
                    {
                        objIData.CreateNewStoredProcedure("ERP.pck_dividing.inser_good_event_list");
                        objectParameter = new object[]{
                            "@event_id", eventId,
                            "@couple_code", goods,
                        };
                        objIData.AddParameter(objectParameter);
                        objIData.ExecNonQuery();
                    }
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi thêm ngày nghỉ lễ ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Holidays -> AddEventHolidays", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage DeleteEventHoliday(object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.delete_event_list");
                objIData.AddParameter(objKeywords);
                objIData.ExecNonQuery();
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi xóa ngày nghỉ lễ ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Holidays -> DeleteEventHoliday", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateEventHoliday(string strGoods, string strShops, string fromDate,
            string toDate, string nameEvent, string strDays, string rate, string userId, string shopId, string eventID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.update_event_list");
                object[] objectParameter = new object[]{
                        "@event_id", eventID,
                        "@name", nameEvent,
                        "@from_date", fromDate,
                        "@to_date", toDate,
                        "@rate", rate,
                        "@user_id", userId,
                        "@shop_id", shopId,
                        "@days", strDays,
                    };
                objIData.AddParameter(objectParameter);
                objIData.ExecNonQuery();

                String[] arrShop = strShops.Split('|');
                foreach (String shop in arrShop)
                {
                    if (shop != null && !String.IsNullOrEmpty(shop))
                    {
                        objIData.CreateNewStoredProcedure("ERP.pck_dividing.inser_shop_event_list");
                        objectParameter = new object[]{
                            "@event_id", eventID,
                            "@shop_id", shop, 
                        };
                        objIData.AddParameter(objectParameter);
                        objIData.ExecNonQuery();

                    }
                }
                String[] arrGoods = strGoods.Split('|');
                foreach (String goods in arrGoods)
                {
                    if (goods != null && !String.IsNullOrEmpty(goods))
                    {
                        objIData.CreateNewStoredProcedure("ERP.pck_dividing.inser_good_event_list");
                        objectParameter = new object[]{
                            "@event_id", eventID,
                            "@couple_code", goods,
                        };
                        objIData.AddParameter(objectParameter);
                        objIData.ExecNonQuery();
                    }
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi thêm ngày nghỉ lễ ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Holidays -> AddEventHolidays", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetGoodsByEvent(ref System.Data.DataTable dtResult, string eventId)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                object[] objectParameter = new object[]{
                            "@event_list_id", eventId,
                        };
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.get_good_event");
                objIData.AddParameter(objectParameter);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi lấy thông tin ngày nghỉ ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Holidays -> LoadHolidays", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
    }


}
