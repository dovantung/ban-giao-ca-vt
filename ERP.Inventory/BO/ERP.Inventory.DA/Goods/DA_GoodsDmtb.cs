﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using GOODS_DMTB = ERP.Inventory.BO.Goods.GoodsDmtb;
#endregion

namespace ERP.Inventory.DA.Goods
{
    /// <summary>
	/// Created by 		: TruongTX 
	/// Created date 	: 8/28/2018 
	/// 
	/// </summary>	
	public partial class DA_GoodsDmtb
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objGOODS_DMTB">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref GOODS_DMTB objGOODS_DMTB, decimal decGOODS_DMTB_ID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + GOODS_DMTB.colGOODS_DMTB_ID, decGOODS_DMTB_ID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objGOODS_DMTB = new GOODS_DMTB();
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colGOODS_DMTB_ID])) objGOODS_DMTB.GOODS_DMTB_ID = Convert.ToDecimal(reader[GOODS_DMTB.colGOODS_DMTB_ID]);
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colGOODS_ID])) objGOODS_DMTB.GOODS_ID = Convert.ToDecimal(reader[GOODS_DMTB.colGOODS_ID]);
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colValue])) objGOODS_DMTB.Value = Convert.ToInt32(reader[GOODS_DMTB.colValue]);
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colFrom_Date])) objGOODS_DMTB.From_Date = Convert.ToDateTime(reader[GOODS_DMTB.colFrom_Date]);
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colTo_Date])) objGOODS_DMTB.To_Date = Convert.ToDateTime(reader[GOODS_DMTB.colTo_Date]);
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colCreate_DateTime])) objGOODS_DMTB.Create_DateTime = Convert.ToDateTime(reader[GOODS_DMTB.colCreate_DateTime]);
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colUser_ID])) objGOODS_DMTB.User_ID = Convert.ToDecimal(reader[GOODS_DMTB.colUser_ID]);
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colUser_SHOP_ID])) objGOODS_DMTB.User_SHOP_ID = Convert.ToDecimal(reader[GOODS_DMTB.colUser_SHOP_ID]);
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colStock_ID])) objGOODS_DMTB.Stock_ID = Convert.ToInt32(reader[GOODS_DMTB.colStock_ID]);
 					if (!Convert.IsDBNull(reader[GOODS_DMTB.colCOUPLE_GOODS_Code])) objGOODS_DMTB.COUPLE_GOODS_Code = Convert.ToString(reader[GOODS_DMTB.colCOUPLE_GOODS_Code]).Trim();
 				}
 				else
 				{
 					objGOODS_DMTB = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODS_DMTB -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(GOODS_DMTB objGOODS_DMTB)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objGOODS_DMTB);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODS_DMTB -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, GOODS_DMTB objGOODS_DMTB)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + GOODS_DMTB.colGOODS_DMTB_ID, objGOODS_DMTB.GOODS_DMTB_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colGOODS_ID, objGOODS_DMTB.GOODS_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colValue, objGOODS_DMTB.Value);
				objIData.AddParameter("@" + GOODS_DMTB.colFrom_Date, objGOODS_DMTB.From_Date);
				objIData.AddParameter("@" + GOODS_DMTB.colTo_Date, objGOODS_DMTB.To_Date);
				objIData.AddParameter("@" + GOODS_DMTB.colCreate_DateTime, objGOODS_DMTB.Create_DateTime);
				objIData.AddParameter("@" + GOODS_DMTB.colUser_ID, objGOODS_DMTB.User_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colUser_SHOP_ID, objGOODS_DMTB.User_SHOP_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colStock_ID, objGOODS_DMTB.Stock_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colCOUPLE_GOODS_Code, objGOODS_DMTB.COUPLE_GOODS_Code);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(GOODS_DMTB objGOODS_DMTB)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objGOODS_DMTB);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODS_DMTB -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, GOODS_DMTB objGOODS_DMTB)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + GOODS_DMTB.colGOODS_DMTB_ID, objGOODS_DMTB.GOODS_DMTB_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colGOODS_ID, objGOODS_DMTB.GOODS_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colValue, objGOODS_DMTB.Value);
				objIData.AddParameter("@" + GOODS_DMTB.colFrom_Date, objGOODS_DMTB.From_Date);
				objIData.AddParameter("@" + GOODS_DMTB.colTo_Date, objGOODS_DMTB.To_Date);
				objIData.AddParameter("@" + GOODS_DMTB.colCreate_DateTime, objGOODS_DMTB.Create_DateTime);
				objIData.AddParameter("@" + GOODS_DMTB.colUser_ID, objGOODS_DMTB.User_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colUser_SHOP_ID, objGOODS_DMTB.User_SHOP_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colStock_ID, objGOODS_DMTB.Stock_ID);
				objIData.AddParameter("@" + GOODS_DMTB.colCOUPLE_GOODS_Code, objGOODS_DMTB.COUPLE_GOODS_Code);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(GOODS_DMTB objGOODS_DMTB)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objGOODS_DMTB);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODS_DMTB -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, GOODS_DMTB objGOODS_DMTB)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + GOODS_DMTB.colGOODS_DMTB_ID, objGOODS_DMTB.GOODS_DMTB_ID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_GoodsDmtb()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "GOODS_DMTB_ADD";
		public const String SP_UPDATE = "GOODS_DMTB_UPD";
		public const String SP_DELETE = "GOODS_DMTB_DEL";
		public const String SP_SELECT = "GOODS_DMTB_SEL";
		public const String SP_SEARCH = "GOODS_DMTB_SRH";
		public const String SP_UPDATEINDEX = "GOODS_DMTB_UPDINDEX";
		#endregion
		
	}
}
