﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.Goods
{
    public partial class DA_Goods
    {
        #region Methods

        public ResultMessage GetShops(ref DataTable dtbData)
        {
            object[] objKeywords = new object[]{
                    "@shop_type", "10",
                    "@shop_type_vtt", "1",
            };
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.get_shop_list");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Goods -> GetShops", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetCoupleGoods(ref DataTable dtbData)
        {
           
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.get_couple_goods_list");
                dtbData = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Goods -> GetCoupleGoods", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage ImportByExcel(params object[][] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();

            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                for (int i = 0; i < objKeywords.Length; i++)
                {
                    object[] objectParameter = objKeywords[i];
                    objIData.CreateNewStoredProcedure("ERP.goods_dividing_category_update");
                    objIData.AddParameter(objectParameter);
                    objIData.ExecNonQuery();

                }
               
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi importExcel ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsDmtb -> ImportByExcel", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm thông tin siêu thị của user
        /// </summary>
        /// <param name="dtbData">Dữ liệu danh sách siêu thị trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataDividing(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("erp.get_list_divide_his");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Goods -> SearchDataDividing", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm thông tin siêu thị của user
        /// </summary>
        /// <param name="dtbData">Dữ liệu danh sách siêu thị trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataDivideCategory(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("erp.get_divide_category");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Goods -> SearchDataDivideCategory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage InsertGoodsDivisionHistory(ref DataTable dtResult, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                objIData.CreateNewStoredProcedure("erp.insert_goods_division_history");
                objIData.AddParameter(objKeywords);
                string id = objIData.ExecStoreToString();

                object[] objKey = new object[]{
                    "@goods_division_history_id ", id,
                    "@date", DateTime.Now,
                };
                objIData.CreateNewStoredProcedure("erp.create_danhmuc_list");
                objIData.AddParameter(objKey);
                objIData.ExecNonQuery();

                objKey = new object[]{
                    "@goods_division_history_id ", id,
                    "@type", "3",
                };
                objIData.CreateNewStoredProcedure("erp.get_goods_division_history_id");
                objIData.AddParameter(objKey);
                dtResult = objIData.ExecStoreToDataTable();
                objIData.CommitTransaction();

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Goods -> SearchDataDivideCategory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objKeywords">Đối tượng truyền vào</param>
        public ResultMessage DeleteCoupleGood(params object[] objKeywords)
        {

            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.delete_couple_goods_list");
                objIData.AddParameter(objKeywords);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi sửa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsDmtb -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;

        }


        #endregion
    }
}
