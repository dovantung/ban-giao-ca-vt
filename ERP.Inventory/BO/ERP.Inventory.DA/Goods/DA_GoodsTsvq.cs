﻿
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using GOODS_TSVQ = ERP.Inventory.BO.Goods.GoodsTsvq;
using ERP.Inventory.DA;
#endregion
namespace ERP.Inventory.BO.Goods
{
    /// <summary>
    /// Created by 		: TruongTX 
    /// Created date 	: 9/19/2018 
    /// 
    /// </summary>	
    public partial class DA_GoodsTsvq
    {



        #region Methods


        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objGOODS_TSVQ">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref GOODS_TSVQ objGOODS_TSVQ, decimal decGOODS_TSVQ_ID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + GOODS_TSVQ.colGOODS_TSVQ_ID, decGOODS_TSVQ_ID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objGOODS_TSVQ = new GOODS_TSVQ();
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colGOODS_TSVQ_ID])) objGOODS_TSVQ.GOODS_TSVQ_ID = Convert.ToDecimal(reader[GOODS_TSVQ.colGOODS_TSVQ_ID]);
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colGOODS_ID])) objGOODS_TSVQ.GOODS_ID = Convert.ToDecimal(reader[GOODS_TSVQ.colGOODS_ID]);
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colTSVQ])) objGOODS_TSVQ.TSVQ = Convert.ToDecimal(reader[GOODS_TSVQ.colTSVQ]);
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colFrom_Date])) objGOODS_TSVQ.From_Date = Convert.ToDateTime(reader[GOODS_TSVQ.colFrom_Date]);
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colTo_Date])) objGOODS_TSVQ.To_Date = Convert.ToDateTime(reader[GOODS_TSVQ.colTo_Date]);
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colCreate_DateTime])) objGOODS_TSVQ.Create_DateTime = Convert.ToDateTime(reader[GOODS_TSVQ.colCreate_DateTime]);
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colUser_ID])) objGOODS_TSVQ.User_ID = Convert.ToDecimal(reader[GOODS_TSVQ.colUser_ID]);
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colUser_SHOP_ID])) objGOODS_TSVQ.User_SHOP_ID = Convert.ToDecimal(reader[GOODS_TSVQ.colUser_SHOP_ID]);
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colStock_ID])) objGOODS_TSVQ.Stock_ID = Convert.ToInt32(reader[GOODS_TSVQ.colStock_ID]);
                    if (!Convert.IsDBNull(reader[GOODS_TSVQ.colCOUPLE_GOODS_Code])) objGOODS_TSVQ.COUPLE_GOODS_Code = Convert.ToString(reader[GOODS_TSVQ.colCOUPLE_GOODS_Code]).Trim();
                }
                else
                {
                    objGOODS_TSVQ = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODS_TSVQ -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objGOODS_TSVQ">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(GOODS_TSVQ objGOODS_TSVQ)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objGOODS_TSVQ);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODS_TSVQ -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODS_TSVQ">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, GOODS_TSVQ objGOODS_TSVQ)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + GOODS_TSVQ.colGOODS_TSVQ_ID, objGOODS_TSVQ.GOODS_TSVQ_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colGOODS_ID, objGOODS_TSVQ.GOODS_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colTSVQ, objGOODS_TSVQ.TSVQ);
                objIData.AddParameter("@" + GOODS_TSVQ.colFrom_Date, objGOODS_TSVQ.From_Date);
                objIData.AddParameter("@" + GOODS_TSVQ.colTo_Date, objGOODS_TSVQ.To_Date);
                objIData.AddParameter("@" + GOODS_TSVQ.colCreate_DateTime, objGOODS_TSVQ.Create_DateTime);
                objIData.AddParameter("@" + GOODS_TSVQ.colUser_ID, objGOODS_TSVQ.User_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colUser_SHOP_ID, objGOODS_TSVQ.User_SHOP_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colStock_ID, objGOODS_TSVQ.Stock_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colCOUPLE_GOODS_Code, objGOODS_TSVQ.COUPLE_GOODS_Code);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objGOODS_TSVQ">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(GOODS_TSVQ objGOODS_TSVQ)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objGOODS_TSVQ);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODS_TSVQ -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODS_TSVQ">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, GOODS_TSVQ objGOODS_TSVQ)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + GOODS_TSVQ.colGOODS_TSVQ_ID, objGOODS_TSVQ.GOODS_TSVQ_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colGOODS_ID, objGOODS_TSVQ.GOODS_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colTSVQ, objGOODS_TSVQ.TSVQ);
                objIData.AddParameter("@" + GOODS_TSVQ.colFrom_Date, objGOODS_TSVQ.From_Date);
                objIData.AddParameter("@" + GOODS_TSVQ.colTo_Date, objGOODS_TSVQ.To_Date);
                objIData.AddParameter("@" + GOODS_TSVQ.colCreate_DateTime, objGOODS_TSVQ.Create_DateTime);
                objIData.AddParameter("@" + GOODS_TSVQ.colUser_ID, objGOODS_TSVQ.User_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colUser_SHOP_ID, objGOODS_TSVQ.User_SHOP_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colStock_ID, objGOODS_TSVQ.Stock_ID);
                objIData.AddParameter("@" + GOODS_TSVQ.colCOUPLE_GOODS_Code, objGOODS_TSVQ.COUPLE_GOODS_Code);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objGOODS_TSVQ">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(GOODS_TSVQ objGOODS_TSVQ)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objGOODS_TSVQ);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODS_TSVQ -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODS_TSVQ">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, GOODS_TSVQ objGOODS_TSVQ)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + GOODS_TSVQ.colGOODS_TSVQ_ID, objGOODS_TSVQ.GOODS_TSVQ_ID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_GoodsTsvq()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "GOODS_TSVQ_ADD";
        public const String SP_UPDATE = "GOODS_TSVQ_UPD";
        public const String SP_DELETE = "GOODS_TSVQ_DEL";
        public const String SP_SELECT = "GOODS_TSVQ_SEL";
        public const String SP_SEARCH = "GOODS_TSVQ_SRH";
        public const String SP_UPDATEINDEX = "GOODS_TSVQ_UPDINDEX";
        #endregion

    }
}


