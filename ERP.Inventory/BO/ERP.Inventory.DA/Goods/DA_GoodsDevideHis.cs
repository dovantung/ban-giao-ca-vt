﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.Goods
{
    public partial class DA_GoodsDevideHis
    {
        public Library.WebCore.ResultMessage Delete(object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.delete_couple_goods_list");
                objIData.AddParameter(objKeywords);
                objIData.ExecNonQuery(); 
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi xóa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsDevideHis -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public Library.WebCore.ResultMessage DeleteShop(object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.delete_good_divide_param");
                objIData.AddParameter(objKeywords);
                objIData.ExecNonQuery();

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi xóa thông tin siêu thị", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsDevideHis -> DeleteShop", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        public ResultMessage AddShopGoodsDevide(ref string result, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.add_goods_divide_param");
                objIData.AddParameter(objKeywords);
                result = objIData.ExecStoreToString();

                objIData.CommitTransaction();

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thêm siêu thị ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "AddShopGoodsDevide -> LoadGoodsDevideHis", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage AddGoodsDivisionHistory(ref System.Data.DataTable dtResult, object[] objKeywords, 
            string stockIds, string goodsGroupIds, string type)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.add_goods_division_history");
                objIData.AddParameter(objKeywords);
                string result = objIData.ExecStoreToString();
                if (!string.IsNullOrEmpty(stockIds))
                {
                    string[] arrayStockIds = stockIds.Split(',');
                    foreach (string stockId in arrayStockIds)
                    {
                        if (!string.IsNullOrEmpty(stockId))
                        {
                            objIData.CreateNewStoredProcedure("ERP.add_goods_divide_param");
                        object[] addGoodsDevideParam = new object[]{
                        "@goods_division_history_id", result,
                        "@param", stockId, 
                         "@type", 1,
                        };
                        objIData.AddParameter(addGoodsDevideParam);
                        objIData.ExecStoreToString();
                        }
                        
                    }
                }
                if (!string.IsNullOrEmpty(goodsGroupIds))
                {
                    goodsGroupIds = goodsGroupIds.Replace("><", ",").Replace("<", "").Replace(">", "");
                    string[] arrayGoodsGroupIds = goodsGroupIds.Split(',');
                    foreach (string goodsId in arrayGoodsGroupIds)
                    {
                        if (!string.IsNullOrEmpty(goodsId))
                        {
                            objIData.CreateNewStoredProcedure("ERP.add_goods_divide_param");
                            object[] addGoodsDevideParam = new object[]{
                                "@goods_division_history_id", result,
                                "@param", goodsId, 
                                 "@type", 0,
                            };
                            objIData.AddParameter(addGoodsDevideParam);
                            objIData.ExecStoreToString();
                        }
                    }
                }
                objKeywords = new object[]{
                        "@goods_division_history_id", result,
                        };
                if (type.Equals("0"))
                {
                    objIData.CreateNewStoredProcedure("ERP.create_dividing_list");
                } else if (type.Equals("1"))
                {
                    objIData.CreateNewStoredProcedure("ERP.create_dividing_list_pk");
                } 
                
                objIData.AddParameter(objKeywords);
                String error = objIData.ExecStoreToString();
                if (!string.IsNullOrEmpty(error))
                {
                    throw new Exception(error);
                }
                objIData.CommitTransaction();
                objKeywords = new object[]{
                        "@goods_division_history_id", result,
                         "@type", type,
                        };
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.get_good_division_his_infor");
                objIData.AddParameter(objKeywords);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thêm siêu thị ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsDevideHis -> AddGoodsDivisionHistory", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage DeleteGoodsDivisionItem(string goodsDivisionItems, String goodsDivisionHistoryId)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                if (!string.IsNullOrEmpty(goodsDivisionItems))
                {
                    objIData.Connect();
                    String[] arrGoodsDivisionItem = goodsDivisionItems.Split('|');
                    foreach(string goodsDivisionItem in arrGoodsDivisionItem) {
                        if (!string.IsNullOrEmpty(goodsDivisionItem))
                        {
                            objIData.CreateNewStoredProcedure("ERP.pck_dividing.DELETE_GOODS_DIVISION_ITEM");
                            String[] tmp = goodsDivisionItem.Split(',');

                            object[] objKeywords = new object[]{
                                "@goods_division_history_id", goodsDivisionHistoryId,
                                "@goods_id", tmp[0],
                                "@stock_id", tmp[1],
                            };
                            objIData.AddParameter(objKeywords);
                            objIData.ExecNonQuery();
                        }
                    }
                    objIData.CommitTransaction();
                }
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thêm siêu thị ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "AddShopGoodsDevide -> LoadGoodsDevideHis", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage ImportSerialByExcel(object[][] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();

            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                for (int i = 0; i < objKeywords.Length; i++)
                {

                    object[] objectParameter = new object[]{
                                "@tsvq", objKeywords[i][15],
                                "@goods_div_his_id", objKeywords[i][21],
                                "@couple_goods", objKeywords[i][1],
                                "@stock_id", objKeywords[i][17],
                     };
                    objIData.CreateNewStoredProcedure("ERP.pck_goods_div.calc_goods_need");
                    objIData.AddParameter(objectParameter);
                    String inhucau = objIData.ExecStoreToString();
                    objectParameter = new object[]{
                                "@goods_division_history_id", objKeywords[i][21],
                                "@stock_id", objKeywords[i][17],
                                "@couple_goods", objKeywords[i][1],
                                "@item_code", "SN_SLBH",
                                "@value2", objKeywords[i][5],
                                "@value3", objKeywords[i][7],
                                "@value4", objKeywords[i][9],
                                "@value5", objKeywords[i][11],
                                "@value6", objKeywords[i][13],
                                "@value7", objKeywords[i][15],
                                "@value8", inhucau,
                            };
                    objIData.CreateNewStoredProcedure("ERP.pck_dividing.import_dividing_update");
                    objIData.AddParameter(objectParameter);
                    string result = objIData.ExecStoreToString();

                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi importExcel ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsTsvq -> ImportByExcel", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        public ResultMessage ImportPKByExcel(object[][] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();

            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                for (int i = 0; i < objKeywords.Length; i++)
                {
                    object[] objectParameter = objKeywords[i];
                    objIData.CreateNewStoredProcedure("ERP.pck_dividing.import_dividing_update_item");
                    objIData.AddParameter(objectParameter);
                    String inhucau = objIData.ExecStoreToString();
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi importExcel ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsTsvq -> ImportByExcel", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
       
    }
}
