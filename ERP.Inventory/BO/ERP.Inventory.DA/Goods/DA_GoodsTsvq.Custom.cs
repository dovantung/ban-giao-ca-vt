﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.Goods
{
    public partial class DA_GoodsTsvq
    {
        #region Methods

        /// <summary>
        /// Tìm kiếm thông tin định mức trưng bày
        /// </summary>
        /// <param name="dtbData">Dữ liệu danh sách định mức trưng bày</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadDataGoodsTsvq(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.load_goods_tsvq");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsTsvq -> LoadDataGoodsDMTB", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Check thỏa mãn điều kiện tồn tại thông tin định mức trưng bày
        /// </summary>
        /// <param name="objValueCheck">check tồn tại, nếu = true là thỏa mãn, = false là không thỏa mãn</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage CheckGoodsTsvqExists(ref string objValueCheck, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.CHECK_GOODS_DMTB_EXISTS");
                objIData.AddParameter(objKeywords);
                objValueCheck = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.CheckData, "Lỗi kiểm tra thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsTsvq -> CheckGoodsDmtbExists", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ref string objValue, params object[] objKeywords)
        {

            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.goods_tsvq_add");
                objIData.AddParameter(objKeywords);
                objValue = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Thêm mới thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsTsvq -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;

        }

        /// <summary>
        /// Sửa thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODS_DMTB">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(ref string objValue, params object[] objKeywords)
        {

            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.goods_tsvq_upd");
                objIData.AddParameter(objKeywords);
                objValue = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi sửa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsTsvq -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;

        }

        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objKeywords">Đối tượng truyền vào</param>
        public ResultMessage Delete(params object[] objKeywords)
        {

            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.GOODS_TSVQ_DEL");
                objIData.AddParameter(objKeywords);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi sửa thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsTsvq -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;

        }

        /// <summary>
        /// Tìm kiếm thông tin định mức trưng bày
        /// </summary>
        /// <param name="dtbData">Dữ liệu danh sách định mức trưng bày</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage ImportByExcel(params object[][] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();

            try
            {
                objIData.Connect(); // đây này thiếu cái này
                objIData.BeginTransaction();
                for (int i = 0; i < objKeywords.Length; i++)
                {
                    object[] objectParameter = objKeywords[i];
                    objIData.CreateNewStoredProcedure("ERP.goods_tsvq_import_add");
                    objIData.AddParameter(objectParameter);
                    objIData.ExecNonQuery();

                }
                objIData.CreateNewStoredProcedure("ERP.goods_tsvq_import_merge");
                objIData.ExecStoreToString();
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi importExcel ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodsTsvq -> ImportByExcel", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage SearchData(ref DataTable dtResult, params  object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.COMMON_SELECT");
                objIData.AddParameter(objKeywords);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GoodDivideProperties -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion

    }
}

