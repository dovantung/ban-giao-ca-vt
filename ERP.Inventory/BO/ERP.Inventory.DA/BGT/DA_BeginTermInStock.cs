
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using BeginTermInStock = ERP.Inventory.BO.BGT.BeginTermInStock;
#endregion
namespace ERP.Inventory.DA.BGT
{
    /// <summary>
	/// Created by 		: Trương Trung Lợi 
	/// Created date 	: 11/28/2012 
	/// 
	/// </summary>	
	public partial class DA_BeginTermInStock
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objBeginTermInStock">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref BeginTermInStock objBeginTermInStock, DateTime? dtmBeginTermDate)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + BeginTermInStock.colBeginTermDate, dtmBeginTermDate);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objBeginTermInStock = new BeginTermInStock();
 					if (!Convert.IsDBNull(reader[BeginTermInStock.colBeginTermDate])) objBeginTermInStock.BeginTermDate = Convert.ToDateTime(reader[BeginTermInStock.colBeginTermDate]);
 					if (!Convert.IsDBNull(reader[BeginTermInStock.colBeginTermTable])) objBeginTermInStock.BeginTermTable = Convert.ToString(reader[BeginTermInStock.colBeginTermTable]).Trim();
 					if (!Convert.IsDBNull(reader[BeginTermInStock.colBeginTermTableDetail])) objBeginTermInStock.BeginTermTableDetail = Convert.ToString(reader[BeginTermInStock.colBeginTermTableDetail]).Trim();
 					if (!Convert.IsDBNull(reader[BeginTermInStock.colCreatedUser])) objBeginTermInStock.CreatedUser = Convert.ToString(reader[BeginTermInStock.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BeginTermInStock.colCreatedDate])) objBeginTermInStock.CreatedDate = Convert.ToDateTime(reader[BeginTermInStock.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[BeginTermInStock.colUpdatedUser])) objBeginTermInStock.UpdatedUser = Convert.ToString(reader[BeginTermInStock.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[BeginTermInStock.colUpdatedDate])) objBeginTermInStock.UpdatedDate = Convert.ToDateTime(reader[BeginTermInStock.colUpdatedDate]);
 				}
 				else
 				{
 					objBeginTermInStock = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermInStock -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objBeginTermInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(BeginTermInStock objBeginTermInStock)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objBeginTermInStock);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermInStock -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBeginTermInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, BeginTermInStock objBeginTermInStock)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + BeginTermInStock.colBeginTermDate, objBeginTermInStock.BeginTermDate);
				objIData.AddParameter("@" + BeginTermInStock.colBeginTermTable, objBeginTermInStock.BeginTermTable);
				objIData.AddParameter("@" + BeginTermInStock.colBeginTermTableDetail, objBeginTermInStock.BeginTermTableDetail);
				objIData.AddParameter("@" + BeginTermInStock.colCreatedUser, objBeginTermInStock.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objBeginTermInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(BeginTermInStock objBeginTermInStock)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objBeginTermInStock);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermInStock -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBeginTermInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, BeginTermInStock objBeginTermInStock)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + BeginTermInStock.colBeginTermDate, objBeginTermInStock.BeginTermDate);
				objIData.AddParameter("@" + BeginTermInStock.colCreatedUser, objBeginTermInStock.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objBeginTermInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(BeginTermInStock objBeginTermInStock)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objBeginTermInStock);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermInStock -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBeginTermInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, BeginTermInStock objBeginTermInStock)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + BeginTermInStock.colBeginTermDate, objBeginTermInStock.BeginTermDate);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_BeginTermInStock()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "BGT_BEGINTERMINSTOCK_ADD";
		public const String SP_UPDATE = "BGT_BEGINTERMINSTOCK_UPD";
		public const String SP_DELETE = "BGT_BEGINTERMINSTOCK_DEL";
		public const String SP_SELECT = "BGT_BEGINTERMINSTOCK_SEL";
		public const String SP_SEARCH = "BGT_BEGINTERMINSTOCK_SRH";
		public const String SP_UPDATEINDEX = "BGT_BEGINTERMINSTOCK_UPDINDEX";
		#endregion
		
	}
}
