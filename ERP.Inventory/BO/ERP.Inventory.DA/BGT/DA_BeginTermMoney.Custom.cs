
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.BGT
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 6/25/2013 
	/// 
	/// </summary>	
	public partial class DA_BeginTermMoney
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin 
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_BeginTermMoney.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermMoney -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}


        /// <summary>
        /// Tính số dư tiền đầu ngày
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage CalcAll(bool bolIsCalAll, DateTime dtBeginTermMoneyDate, string strUpdatedUser, string strUserHostAddress, string strCertificateString, string strLoginLogID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                //objIData.CreateNewStoredProcedure("BGT_BEGINTERMMONEY_CalcAll");
                DateTime dtCalNow = dtBeginTermMoneyDate;
                int i = 0;
                int intTotalDay = 0;
                if (bolIsCalAll)
                {
                    DateTime dtNow = DateTime.Now;
                    TimeSpan tspCal = dtNow.Subtract(dtCalNow);
                    intTotalDay = tspCal.Days;
                }      
                while (i <= intTotalDay)
                {
                    object[] objKeyWords = new object[] 
                    {
                        "@BEGINTERMMONEYDATE", dtCalNow,
                        "@UPDATEDUSER", strUpdatedUser,
                        "@USERHOSTADDRESS", strUserHostAddress,
                        "@CERTIFICATESTRING", strCertificateString,
                        "@LOGINLOGID", strLoginLogID
                    };
                    CalcAll(objIData, objKeyWords);
                    dtCalNow = dtCalNow.AddDays(1);
                    i++;
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Others, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermMoney -> CalcAll", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tính số dư tiền đầu ngày
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        private void CalcAll(IData objIData, params object[] objKeywords)
        {
            try
            {
                objIData.CreateNewStoredProcedure("BGT_BEGINTERMMONEY_CalcAll");
                objIData.AddParameter(objKeywords);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Tìm kiếm thông tin 
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataMoneyInfo(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("BGT_BEGINTERMMONEYINFO_SRH");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermMoney -> SearchDataMoneyInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tìm kiếm thông tin Log
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataMoneyInfoLog(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("BGT_BEGINTERMMONEYINFO_LOG_SRH");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermMoney -> SearchDataMoneyInfoLog", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
		#endregion
		
		
	}
}
