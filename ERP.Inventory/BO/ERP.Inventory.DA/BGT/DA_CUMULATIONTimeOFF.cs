
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using CumulationTimeOFF = ERP.Inventory.BO.BGT.CumulationTimeOff;
#endregion
namespace ERP.Inventory.DA.BGT
{
    /// <summary>
	/// Created by 		: CAO HUU VU LAM 
	/// Created date 	: 10-Mar-15 
	/// 
	/// </summary>	
	public partial class DA_CumulationTimeOff
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objCumulationTimeOFF">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref CumulationTimeOFF objCumulationTimeOFF, int intNewYear, string strUserName)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + CumulationTimeOFF.colNewYear, intNewYear);
				objIData.AddParameter("@" + CumulationTimeOFF.colUserName, strUserName);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objCumulationTimeOFF = new CumulationTimeOFF();
 					if (!Convert.IsDBNull(reader[CumulationTimeOFF.colNewYear])) objCumulationTimeOFF.NewYear = Convert.ToInt32(reader[CumulationTimeOFF.colNewYear]);
 					if (!Convert.IsDBNull(reader[CumulationTimeOFF.colUserName])) objCumulationTimeOFF.UserName = Convert.ToString(reader[CumulationTimeOFF.colUserName]).Trim();
 					if (!Convert.IsDBNull(reader[CumulationTimeOFF.colCumulationTimeOffDays])) objCumulationTimeOFF.CumulationTimeOffDays = Convert.ToDecimal(reader[CumulationTimeOFF.colCumulationTimeOffDays]);
 					if (!Convert.IsDBNull(reader[CumulationTimeOFF.colUsedTimeOffDays])) objCumulationTimeOFF.UsedTimeOffDays = Convert.ToDecimal(reader[CumulationTimeOFF.colUsedTimeOffDays]);
 					if (!Convert.IsDBNull(reader[CumulationTimeOFF.colRemainTimeOffDays])) objCumulationTimeOFF.RemainTimeOffDays = Convert.ToDecimal(reader[CumulationTimeOFF.colRemainTimeOffDays]);
 					if (!Convert.IsDBNull(reader[CumulationTimeOFF.colIsActive])) objCumulationTimeOFF.IsActive = Convert.ToBoolean(reader[CumulationTimeOFF.colIsActive]);
 				}
 				else
 				{
 					objCumulationTimeOFF = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CumulationTimeOFF -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objCumulationTimeOFF">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(CumulationTimeOFF objCumulationTimeOFF)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objCumulationTimeOFF);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CumulationTimeOFF -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objCumulationTimeOFF">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, CumulationTimeOFF objCumulationTimeOFF)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + CumulationTimeOFF.colNewYear, objCumulationTimeOFF.NewYear);
				objIData.AddParameter("@" + CumulationTimeOFF.colUserName, objCumulationTimeOFF.UserName);
				objIData.AddParameter("@" + CumulationTimeOFF.colCumulationTimeOffDays, objCumulationTimeOFF.CumulationTimeOffDays);
				objIData.AddParameter("@" + CumulationTimeOFF.colUsedTimeOffDays, objCumulationTimeOFF.UsedTimeOffDays);
				objIData.AddParameter("@" + CumulationTimeOFF.colRemainTimeOffDays, objCumulationTimeOFF.RemainTimeOffDays);
				objIData.AddParameter("@" + CumulationTimeOFF.colIsActive, objCumulationTimeOFF.IsActive);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objCumulationTimeOFF">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(CumulationTimeOFF objCumulationTimeOFF)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objCumulationTimeOFF);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CumulationTimeOFF -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objCumulationTimeOFF">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, CumulationTimeOFF objCumulationTimeOFF)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + CumulationTimeOFF.colNewYear, objCumulationTimeOFF.NewYear);
				objIData.AddParameter("@" + CumulationTimeOFF.colUserName, objCumulationTimeOFF.UserName);
				objIData.AddParameter("@" + CumulationTimeOFF.colCumulationTimeOffDays, objCumulationTimeOFF.CumulationTimeOffDays);
				objIData.AddParameter("@" + CumulationTimeOFF.colUsedTimeOffDays, objCumulationTimeOFF.UsedTimeOffDays);
				objIData.AddParameter("@" + CumulationTimeOFF.colRemainTimeOffDays, objCumulationTimeOFF.RemainTimeOffDays);
				objIData.AddParameter("@" + CumulationTimeOFF.colIsActive, objCumulationTimeOFF.IsActive);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objCumulationTimeOFF">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(CumulationTimeOFF objCumulationTimeOFF)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objCumulationTimeOFF);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CumulationTimeOFF -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objCumulationTimeOFF">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, CumulationTimeOFF objCumulationTimeOFF)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + CumulationTimeOFF.colNewYear, objCumulationTimeOFF.NewYear);
				objIData.AddParameter("@" + CumulationTimeOFF.colUserName, objCumulationTimeOFF.UserName);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_CumulationTimeOff()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "BGT_CumulationTIMEOFF_ADD";
		public const String SP_UPDATE = "BGT_CumulationTIMEOFF_UPD";
		public const String SP_DELETE = "BGT_CumulationTIMEOFF_DEL";
		public const String SP_SELECT = "BGT_CumulationTIMEOFF_SEL";
		public const String SP_SEARCH = "BGT_CumulationTIMEOFF_SRH";
		public const String SP_UPDATEINDEX = "BGT_CumulationTIMEOFF_UPDINDEX";
		#endregion
		
	}
}
