
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using BeginTermMoney = ERP.Inventory.BO.BGT.BeginTermMoney;
#endregion
namespace ERP.Inventory.DA.BGT
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 6/25/2013 
	/// 
	/// </summary>	
	public partial class DA_BeginTermMoney
	{	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objBeginTermMoney">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref BeginTermMoney objBeginTermMoney, DateTime? dtmBeginTermMoneyDate, int intStoreID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + BeginTermMoney.colBeginTermMoneyDate, dtmBeginTermMoneyDate);
				objIData.AddParameter("@" + BeginTermMoney.colStoreID, intStoreID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objBeginTermMoney = new BeginTermMoney();
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colBeginTermMoneyDate])) objBeginTermMoney.BeginTermMoneyDate = Convert.ToDateTime(reader[BeginTermMoney.colBeginTermMoneyDate]);
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colStoreID])) objBeginTermMoney.StoreID = Convert.ToInt32(reader[BeginTermMoney.colStoreID]);
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colVNDCash])) objBeginTermMoney.VNDCash = Convert.ToDecimal(reader[BeginTermMoney.colVNDCash]);
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colForeignCash])) objBeginTermMoney.ForeignCash = Convert.ToDecimal(reader[BeginTermMoney.colForeignCash]);
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colForeignCashExchange])) objBeginTermMoney.ForeignCashExchange = Convert.ToDecimal(reader[BeginTermMoney.colForeignCashExchange]);
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colPaymentCardAmount])) objBeginTermMoney.PaymentCardAmount = Convert.ToDecimal(reader[BeginTermMoney.colPaymentCardAmount]);
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colPaymentCardSpend])) objBeginTermMoney.PaymentCardSpend = Convert.ToDecimal(reader[BeginTermMoney.colPaymentCardSpend]);
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colGiftVoucherAmount])) objBeginTermMoney.GiftVoucherAmount = Convert.ToDecimal(reader[BeginTermMoney.colGiftVoucherAmount]);
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colUnEventPriceGiftVoucher])) objBeginTermMoney.UnEventPriceGiftVoucher = Convert.ToDecimal(reader[BeginTermMoney.colUnEventPriceGiftVoucher]);
 					if (!Convert.IsDBNull(reader[BeginTermMoney.colRefundAmount])) objBeginTermMoney.RefundAmount = Convert.ToDecimal(reader[BeginTermMoney.colRefundAmount]);
 				}
 				else
 				{
 					objBeginTermMoney = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermMoney -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objBeginTermMoney">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(BeginTermMoney objBeginTermMoney)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objBeginTermMoney);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermMoney -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBeginTermMoney">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, BeginTermMoney objBeginTermMoney)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + BeginTermMoney.colBeginTermMoneyDate, objBeginTermMoney.BeginTermMoneyDate);
				objIData.AddParameter("@" + BeginTermMoney.colStoreID, objBeginTermMoney.StoreID);
				objIData.AddParameter("@" + BeginTermMoney.colVNDCash, objBeginTermMoney.VNDCash);
				objIData.AddParameter("@" + BeginTermMoney.colForeignCash, objBeginTermMoney.ForeignCash);
				objIData.AddParameter("@" + BeginTermMoney.colForeignCashExchange, objBeginTermMoney.ForeignCashExchange);
				objIData.AddParameter("@" + BeginTermMoney.colPaymentCardAmount, objBeginTermMoney.PaymentCardAmount);
				objIData.AddParameter("@" + BeginTermMoney.colPaymentCardSpend, objBeginTermMoney.PaymentCardSpend);
				objIData.AddParameter("@" + BeginTermMoney.colGiftVoucherAmount, objBeginTermMoney.GiftVoucherAmount);
				objIData.AddParameter("@" + BeginTermMoney.colUnEventPriceGiftVoucher, objBeginTermMoney.UnEventPriceGiftVoucher);
				objIData.AddParameter("@" + BeginTermMoney.colRefundAmount, objBeginTermMoney.RefundAmount);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objBeginTermMoney">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(BeginTermMoney objBeginTermMoney)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objBeginTermMoney);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermMoney -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBeginTermMoney">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, BeginTermMoney objBeginTermMoney)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + BeginTermMoney.colBeginTermMoneyDate, objBeginTermMoney.BeginTermMoneyDate);
				objIData.AddParameter("@" + BeginTermMoney.colStoreID, objBeginTermMoney.StoreID);
				objIData.AddParameter("@" + BeginTermMoney.colVNDCash, objBeginTermMoney.VNDCash);
				objIData.AddParameter("@" + BeginTermMoney.colForeignCash, objBeginTermMoney.ForeignCash);
				objIData.AddParameter("@" + BeginTermMoney.colForeignCashExchange, objBeginTermMoney.ForeignCashExchange);
				objIData.AddParameter("@" + BeginTermMoney.colPaymentCardAmount, objBeginTermMoney.PaymentCardAmount);
				objIData.AddParameter("@" + BeginTermMoney.colPaymentCardSpend, objBeginTermMoney.PaymentCardSpend);
				objIData.AddParameter("@" + BeginTermMoney.colGiftVoucherAmount, objBeginTermMoney.GiftVoucherAmount);
				objIData.AddParameter("@" + BeginTermMoney.colUnEventPriceGiftVoucher, objBeginTermMoney.UnEventPriceGiftVoucher);
				objIData.AddParameter("@" + BeginTermMoney.colRefundAmount, objBeginTermMoney.RefundAmount);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objBeginTermMoney">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(BeginTermMoney objBeginTermMoney)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objBeginTermMoney);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_BeginTermMoney -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objBeginTermMoney">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, BeginTermMoney objBeginTermMoney)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + BeginTermMoney.colBeginTermMoneyDate, objBeginTermMoney.BeginTermMoneyDate);
				objIData.AddParameter("@" + BeginTermMoney.colStoreID, objBeginTermMoney.StoreID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_BeginTermMoney()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "BGT_BEGINTERMMONEY_ADD";
		public const String SP_UPDATE = "BGT_BEGINTERMMONEY_UPD";
		public const String SP_DELETE = "BGT_BEGINTERMMONEY_DEL";
		public const String SP_SELECT = "BGT_BEGINTERMMONEY_SEL";
		public const String SP_SEARCH = "BGT_BEGINTERMMONEY_SRH";
		public const String SP_UPDATEINDEX = "BGT_BEGINTERMMONEY_UPDINDEX";
		#endregion
		
	}
}
