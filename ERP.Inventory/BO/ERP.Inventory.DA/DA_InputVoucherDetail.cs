
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputVoucherDetail = ERP.Inventory.BO.InputVoucherDetail;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// Chi tiết phiếu nhập
	/// </summary>	
	public partial class DA_InputVoucherDetail
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết phiếu nhập
		/// </summary>
		/// <param name="objInputVoucherDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InputVoucherDetail objInputVoucherDetail, string strInputVoucherDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherDetailID, strInputVoucherDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					if (objInputVoucherDetail == null) 
 						objInputVoucherDetail = new InputVoucherDetail();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputVoucherDetailID])) objInputVoucherDetail.InputVoucherDetailID = Convert.ToString(reader[InputVoucherDetail.colInputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputVoucherID])) objInputVoucherDetail.InputVoucherID = Convert.ToString(reader[InputVoucherDetail.colInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colProductID])) objInputVoucherDetail.ProductID = Convert.ToString(reader[InputVoucherDetail.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colQuantity])) objInputVoucherDetail.Quantity = Convert.ToDecimal(reader[InputVoucherDetail.colQuantity]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colVAT])) objInputVoucherDetail.VAT = Convert.ToInt32(reader[InputVoucherDetail.colVAT]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colVATPercent])) objInputVoucherDetail.VATPercent = Convert.ToInt32(reader[InputVoucherDetail.colVATPercent]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputPrice])) objInputVoucherDetail.InputPrice = Convert.ToDecimal(reader[InputVoucherDetail.colInputPrice]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colORIGINALInputPrice])) objInputVoucherDetail.ORIGINALInputPrice = Convert.ToDecimal(reader[InputVoucherDetail.colORIGINALInputPrice]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colCostPrice])) objInputVoucherDetail.CostPrice = Convert.ToDecimal(reader[InputVoucherDetail.colCostPrice]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstPrice])) objInputVoucherDetail.FirstPrice = Convert.ToDecimal(reader[InputVoucherDetail.colFirstPrice]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colReturnFee])) objInputVoucherDetail.ReturnFee = Convert.ToDecimal(reader[InputVoucherDetail.colReturnFee]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colAdjustPrice])) objInputVoucherDetail.AdjustPrice = Convert.ToDecimal(reader[InputVoucherDetail.colAdjustPrice]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colAdjustPriceContent])) objInputVoucherDetail.AdjustPriceContent = Convert.ToString(reader[InputVoucherDetail.colAdjustPriceContent]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colAdjustPriceUser])) objInputVoucherDetail.AdjustPriceUser = Convert.ToString(reader[InputVoucherDetail.colAdjustPriceUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputDate])) objInputVoucherDetail.InputDate = Convert.ToDateTime(reader[InputVoucherDetail.colInputDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colENDWarrantyDate])) objInputVoucherDetail.ENDWarrantyDate = Convert.ToDateTime(reader[InputVoucherDetail.colENDWarrantyDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colProductIONDate])) objInputVoucherDetail.ProductIONDate = Convert.ToDateTime(reader[InputVoucherDetail.colProductIONDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colProductChangeDate])) objInputVoucherDetail.ProductChangeDate = Convert.ToDateTime(reader[InputVoucherDetail.colProductChangeDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colReturnDate])) objInputVoucherDetail.ReturnDate = Convert.ToDateTime(reader[InputVoucherDetail.colReturnDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colChangeToOLDDate])) objInputVoucherDetail.ChangeToOLDDate = Convert.ToDateTime(reader[InputVoucherDetail.colChangeToOLDDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInputDate])) objInputVoucherDetail.FirstInputDate = Convert.ToDateTime(reader[InputVoucherDetail.colFirstInputDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInVoiceDate])) objInputVoucherDetail.FirstInVoiceDate = Convert.ToDateTime(reader[InputVoucherDetail.colFirstInVoiceDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstCustomerID])) objInputVoucherDetail.FirstCustomerID = Convert.ToInt32(reader[InputVoucherDetail.colFirstCustomerID]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInputTypeID])) objInputVoucherDetail.FirstInputTypeID = Convert.ToDecimal(reader[InputVoucherDetail.colFirstInputTypeID]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInputVoucherID])) objInputVoucherDetail.FirstInputVoucherID = Convert.ToString(reader[InputVoucherDetail.colFirstInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInputVoucherDetailID])) objInputVoucherDetail.FirstInputVoucherDetailID = Convert.ToString(reader[InputVoucherDetail.colFirstInputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsNew])) objInputVoucherDetail.IsNew = Convert.ToBoolean(reader[InputVoucherDetail.colIsNew]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsShowProduct])) objInputVoucherDetail.IsShowProduct = Convert.ToBoolean(reader[InputVoucherDetail.colIsShowProduct]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsReturnProduct])) objInputVoucherDetail.IsReturnProduct = Convert.ToBoolean(reader[InputVoucherDetail.colIsReturnProduct]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsReturnWithFee])) objInputVoucherDetail.IsReturnWithFee = Convert.ToBoolean(reader[InputVoucherDetail.colIsReturnWithFee]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsCheckRealInput])) objInputVoucherDetail.IsCheckRealInput = Convert.ToBoolean(reader[InputVoucherDetail.colIsCheckRealInput]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colISProductChange])) objInputVoucherDetail.ISProductChange = Convert.ToBoolean(reader[InputVoucherDetail.colISProductChange]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colISAddInStock])) objInputVoucherDetail.ISAddInStock = Convert.ToBoolean(reader[InputVoucherDetail.colISAddInStock]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colCreatedStoreID])) objInputVoucherDetail.CreatedStoreID = Convert.ToInt32(reader[InputVoucherDetail.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputStoreID])) objInputVoucherDetail.InputStoreID = Convert.ToInt32(reader[InputVoucherDetail.colInputStoreID]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colCreatedUser])) objInputVoucherDetail.CreatedUser = Convert.ToString(reader[InputVoucherDetail.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colCreatedDate])) objInputVoucherDetail.CreatedDate = Convert.ToDateTime(reader[InputVoucherDetail.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colUpdatedUser])) objInputVoucherDetail.UpdatedUser = Convert.ToString(reader[InputVoucherDetail.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colUpdatedDate])) objInputVoucherDetail.UpdatedDate = Convert.ToDateTime(reader[InputVoucherDetail.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsDeleted])) objInputVoucherDetail.IsDeleted = Convert.ToBoolean(reader[InputVoucherDetail.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colDeletedUser])) objInputVoucherDetail.DeletedUser = Convert.ToString(reader[InputVoucherDetail.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucherDetail.colDeletedDate])) objInputVoucherDetail.DeletedDate = Convert.ToDateTime(reader[InputVoucherDetail.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colTaxedFee])) objInputVoucherDetail.TaxedFee = Convert.ToDecimal(reader[InputVoucherDetail.colTaxedFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colPurchaseFee])) objInputVoucherDetail.PurchaseFee = Convert.ToDecimal(reader[InputVoucherDetail.colPurchaseFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colNote])) objInputVoucherDetail.Note = Convert.ToString(reader[InputVoucherDetail.colNote]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colOrderIndex])) objInputVoucherDetail.OrderIndex = Convert.ToInt32(reader[InputVoucherDetail.colOrderIndex]);
                    objInputVoucherDetail.IsExist = true;
 				}
 				else
 				{
 					objInputVoucherDetail.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết phiếu nhập
		/// </summary>
		/// <param name="objInputVoucherDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InputVoucherDetail objInputVoucherDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInputVoucherDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucherDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InputVoucherDetail objInputVoucherDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherDetailID, objInputVoucherDetail.InputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherID, objInputVoucherDetail.InputVoucherID);
                objIData.AddParameter("@" + InputVoucherDetail.colProductID, objInputVoucherDetail.ProductID);
                objIData.AddParameter("@" + InputVoucherDetail.colQuantity, objInputVoucherDetail.Quantity);
                objIData.AddParameter("@" + InputVoucherDetail.colVAT, objInputVoucherDetail.VAT);
                objIData.AddParameter("@" + InputVoucherDetail.colVATPercent, objInputVoucherDetail.VATPercent);
                objIData.AddParameter("@" + InputVoucherDetail.colInputPrice, objInputVoucherDetail.InputPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colORIGINALInputPrice, objInputVoucherDetail.ORIGINALInputPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colCostPrice, objInputVoucherDetail.CostPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstPrice, objInputVoucherDetail.FirstPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colReturnFee, objInputVoucherDetail.ReturnFee);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPrice, objInputVoucherDetail.AdjustPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceContent, objInputVoucherDetail.AdjustPriceContent);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceUser, objInputVoucherDetail.AdjustPriceUser);
                objIData.AddParameter("@" + InputVoucherDetail.colInputDate, objInputVoucherDetail.InputDate);
                objIData.AddParameter("@" + InputVoucherDetail.colENDWarrantyDate, objInputVoucherDetail.ENDWarrantyDate);
                objIData.AddParameter("@" + InputVoucherDetail.colProductIONDate, objInputVoucherDetail.ProductIONDate);
                objIData.AddParameter("@" + InputVoucherDetail.colProductChangeDate, objInputVoucherDetail.ProductChangeDate);
                objIData.AddParameter("@" + InputVoucherDetail.colReturnDate, objInputVoucherDetail.ReturnDate);
                objIData.AddParameter("@" + InputVoucherDetail.colChangeToOLDDate, objInputVoucherDetail.ChangeToOLDDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputDate, objInputVoucherDetail.FirstInputDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInVoiceDate, objInputVoucherDetail.FirstInVoiceDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstCustomerID, objInputVoucherDetail.FirstCustomerID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputTypeID, objInputVoucherDetail.FirstInputTypeID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherID, objInputVoucherDetail.FirstInputVoucherID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherDetailID, objInputVoucherDetail.FirstInputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colIsNew, objInputVoucherDetail.IsNew);
                objIData.AddParameter("@" + InputVoucherDetail.colIsShowProduct, objInputVoucherDetail.IsShowProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colIsReturnProduct, objInputVoucherDetail.IsReturnProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colIsReturnWithFee, objInputVoucherDetail.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucherDetail.colIsCheckRealInput, objInputVoucherDetail.IsCheckRealInput);
                objIData.AddParameter("@" + InputVoucherDetail.colISProductChange, objInputVoucherDetail.ISProductChange);
                objIData.AddParameter("@" + InputVoucherDetail.colISAddInStock, objInputVoucherDetail.ISAddInStock);
                objIData.AddParameter("@" + InputVoucherDetail.colCreatedStoreID, objInputVoucherDetail.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucherDetail.colInputStoreID, objInputVoucherDetail.InputStoreID);
                objIData.AddParameter("@" + InputVoucherDetail.colCreatedUser, objInputVoucherDetail.CreatedUser);
                objIData.AddParameter("@" + InputVoucherDetail.colOrderDetailID, objInputVoucherDetail.OrderDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colIsErrorProduct, objInputVoucherDetail.IsErrorProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colTaxedFee, objInputVoucherDetail.TaxedFee);
                objIData.AddParameter("@" + InputVoucherDetail.colPurchaseFee, objInputVoucherDetail.PurchaseFee);
                objIData.AddParameter("@" + InputVoucherDetail.colNote, objInputVoucherDetail.Note);
                objIData.AddParameter("@" + InputVoucherDetail.colOrderIndex, objInputVoucherDetail.OrderIndex);
                objIData.AddParameter("@REMAINQUANTITY", objInputVoucherDetail.QuantityRemain);
                if (objInputVoucherDetail.InvoiceQuantity > 0)
                    objIData.AddParameter("@" + InputVoucherDetail.colInvoiceQuantity, objInputVoucherDetail.InvoiceQuantity);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objInputVoucherDetail.InputVoucherDetailID = objIData.ExecStoreToString();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
        /// Thêm một chi tiết phiếu nhập từ một phiếu xuất nhập trả hàng
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="objInputVoucherDetailBO"></param>
        /// <returns></returns>
        public bool InsertReturnOP(BO.InputVoucherDetail objInputVoucherDetail,
                                   BO.InputVoucherDetailIMEI objInputVoucherDetailIMEI, IData objIData)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherDetailID, objInputVoucherDetail.InputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherID, objInputVoucherDetail.InputVoucherID);
                objIData.AddParameter("@" + InputVoucherDetail.colProductID, objInputVoucherDetail.ProductID);
                objIData.AddParameter("@" + InputVoucherDetail.colQuantity, objInputVoucherDetail.Quantity);
                objIData.AddParameter("@" + InputVoucherDetail.colVAT, objInputVoucherDetail.VAT);
                objIData.AddParameter("@" + InputVoucherDetail.colVATPercent, objInputVoucherDetail.VATPercent);
                objIData.AddParameter("@" + InputVoucherDetail.colInputPrice, objInputVoucherDetail.InputPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colORIGINALInputPrice, objInputVoucherDetail.ORIGINALInputPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colCostPrice, objInputVoucherDetail.CostPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstPrice, objInputVoucherDetail.FirstPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colReturnFee, objInputVoucherDetail.ReturnFee);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPrice, objInputVoucherDetail.AdjustPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceContent, objInputVoucherDetail.AdjustPriceContent);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceUser, objInputVoucherDetail.AdjustPriceUser);
                objIData.AddParameter("@" + InputVoucherDetail.colInputDate, objInputVoucherDetail.InputDate);
                objIData.AddParameter("@" + InputVoucherDetail.colENDWarrantyDate, objInputVoucherDetail.ENDWarrantyDate);
                objIData.AddParameter("@" + InputVoucherDetail.colProductIONDate, objInputVoucherDetail.ProductIONDate);
                objIData.AddParameter("@" + InputVoucherDetail.colProductChangeDate, objInputVoucherDetail.ProductChangeDate);
                objIData.AddParameter("@" + InputVoucherDetail.colReturnDate, objInputVoucherDetail.ReturnDate);
                objIData.AddParameter("@" + InputVoucherDetail.colChangeToOLDDate, objInputVoucherDetail.ChangeToOLDDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputDate, objInputVoucherDetail.FirstInputDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInVoiceDate, objInputVoucherDetail.FirstInVoiceDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstCustomerID, objInputVoucherDetail.FirstCustomerID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputTypeID, objInputVoucherDetail.FirstInputTypeID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherID, objInputVoucherDetail.FirstInputVoucherID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherDetailID, objInputVoucherDetail.FirstInputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colIsNew, objInputVoucherDetail.IsNew);
                objIData.AddParameter("@" + InputVoucherDetail.colIsShowProduct, objInputVoucherDetail.IsShowProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colIsReturnProduct, objInputVoucherDetail.IsReturnProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colIsReturnWithFee, objInputVoucherDetail.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucherDetail.colIsCheckRealInput, objInputVoucherDetail.IsCheckRealInput);
                objIData.AddParameter("@" + InputVoucherDetail.colISProductChange, objInputVoucherDetail.ISProductChange);
                objIData.AddParameter("@" + InputVoucherDetail.colISAddInStock, objInputVoucherDetail.ISAddInStock);
                objIData.AddParameter("@" + InputVoucherDetail.colCreatedStoreID, objInputVoucherDetail.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucherDetail.colInputStoreID, objInputVoucherDetail.InputStoreID);
                objIData.AddParameter("@" + InputVoucherDetail.colCreatedUser, objInputVoucherDetail.CreatedUser);
                objIData.AddParameter("@" + InputVoucherDetail.colNote, objInputVoucherDetail.Note);
                objIData.AddParameter("@" + InputVoucherDetail.colOrderIndex, objInputVoucherDetail.OrderIndex);
                objIData.AddParameter("@REMAINQUANTITY", objInputVoucherDetail.QuantityRemain);
                if (objInputVoucherDetail.InvoiceQuantity > 0)
                    objIData.AddParameter("@" + InputVoucherDetail.colInvoiceQuantity, objInputVoucherDetail.InvoiceQuantity);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
            }
            catch (Exception objEx)
            {
                throw objEx;
                return false;
            }
            return true;
        }

		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu nhập
		/// </summary>
		/// <param name="objInputVoucherDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InputVoucherDetail objInputVoucherDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInputVoucherDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucherDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InputVoucherDetail objInputVoucherDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherDetailID, objInputVoucherDetail.InputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherID, objInputVoucherDetail.InputVoucherID);
                objIData.AddParameter("@" + InputVoucherDetail.colProductID, objInputVoucherDetail.ProductID);
                objIData.AddParameter("@" + InputVoucherDetail.colQuantity, objInputVoucherDetail.Quantity);
                objIData.AddParameter("@" + InputVoucherDetail.colVAT, objInputVoucherDetail.VAT);
                objIData.AddParameter("@" + InputVoucherDetail.colVATPercent, objInputVoucherDetail.VATPercent);
                objIData.AddParameter("@" + InputVoucherDetail.colInputPrice, objInputVoucherDetail.InputPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colORIGINALInputPrice, objInputVoucherDetail.ORIGINALInputPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colCostPrice, objInputVoucherDetail.CostPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstPrice, objInputVoucherDetail.FirstPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colReturnFee, objInputVoucherDetail.ReturnFee);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPrice, objInputVoucherDetail.AdjustPrice);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceContent, objInputVoucherDetail.AdjustPriceContent);
                objIData.AddParameter("@" + InputVoucherDetail.colAdjustPriceUser, objInputVoucherDetail.AdjustPriceUser);
                objIData.AddParameter("@" + InputVoucherDetail.colInputDate, objInputVoucherDetail.InputDate);
                objIData.AddParameter("@" + InputVoucherDetail.colENDWarrantyDate, objInputVoucherDetail.ENDWarrantyDate);
                objIData.AddParameter("@" + InputVoucherDetail.colProductIONDate, objInputVoucherDetail.ProductIONDate);
                objIData.AddParameter("@" + InputVoucherDetail.colProductChangeDate, objInputVoucherDetail.ProductChangeDate);
                objIData.AddParameter("@" + InputVoucherDetail.colReturnDate, objInputVoucherDetail.ReturnDate);
                objIData.AddParameter("@" + InputVoucherDetail.colChangeToOLDDate, objInputVoucherDetail.ChangeToOLDDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputDate, objInputVoucherDetail.FirstInputDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInVoiceDate, objInputVoucherDetail.FirstInVoiceDate);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstCustomerID, objInputVoucherDetail.FirstCustomerID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputTypeID, objInputVoucherDetail.FirstInputTypeID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherID, objInputVoucherDetail.FirstInputVoucherID);
                objIData.AddParameter("@" + InputVoucherDetail.colFirstInputVoucherDetailID, objInputVoucherDetail.FirstInputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colIsNew, objInputVoucherDetail.IsNew);
                objIData.AddParameter("@" + InputVoucherDetail.colIsShowProduct, objInputVoucherDetail.IsShowProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colIsReturnProduct, objInputVoucherDetail.IsReturnProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colIsReturnWithFee, objInputVoucherDetail.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucherDetail.colIsCheckRealInput, objInputVoucherDetail.IsCheckRealInput);
                objIData.AddParameter("@" + InputVoucherDetail.colISProductChange, objInputVoucherDetail.ISProductChange);
                objIData.AddParameter("@" + InputVoucherDetail.colISAddInStock, objInputVoucherDetail.ISAddInStock);
                objIData.AddParameter("@" + InputVoucherDetail.colCreatedStoreID, objInputVoucherDetail.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucherDetail.colInputStoreID, objInputVoucherDetail.InputStoreID);
                objIData.AddParameter("@" + InputVoucherDetail.colUpdatedUser, objInputVoucherDetail.UpdatedUser);
                objIData.AddParameter("@" + InputVoucherDetail.colOrderDetailID, objInputVoucherDetail.OrderDetailID);
                objIData.AddParameter("@" + InputVoucherDetail.colIsErrorProduct, objInputVoucherDetail.IsErrorProduct);
                objIData.AddParameter("@" + InputVoucherDetail.colTaxedFee, objInputVoucherDetail.TaxedFee);
                objIData.AddParameter("@" + InputVoucherDetail.colPurchaseFee, objInputVoucherDetail.PurchaseFee);
                objIData.AddParameter("@" + InputVoucherDetail.colNote, objInputVoucherDetail.Note);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết phiếu nhập
		/// </summary>
		/// <param name="objInputVoucherDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InputVoucherDetail objInputVoucherDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInputVoucherDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucherDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InputVoucherDetail objInputVoucherDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherDetailID, objInputVoucherDetail.InputVoucherDetailID);
				objIData.AddParameter("@" + InputVoucherDetail.colDeletedUser, objInputVoucherDetail.DeletedUser);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
        /// Xóa thông tin chi tiết phiếu nhập
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void DeleteByInputVoucherID(IData objIData, string strInputVoucherID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTDETAIL_DELBYINPUTID");
                objIData.AddParameter("@" + InputVoucherDetail.colInputVoucherID,strInputVoucherID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
		#endregion

		
		#region Constructor

		public DA_InputVoucherDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_INPUTVOUCHERDETAIL_ADD";
		public const String SP_UPDATE = "PM_INPUTVOUCHERDETAIL_UPD";
		public const String SP_DELETE = "PM_INPUTVOUCHERDETAIL_DEL";
		public const String SP_SELECT = "PM_INPUTVOUCHERDETAIL_SEL";
        public const String SP_GETLISTBYINPUTVOUCHERID = "PM_INPUTDETAIL_INPUTVOUCHERID";
		public const String SP_SEARCH = "PM_INPUTVOUCHERDETAIL_SRH";
		public const String SP_UPDATEINDEX = "PM_INPUTVOUCHERDETAIL_UPDINDEX";
		#endregion
		
	}
}
