﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.DataAccess;
using Library.WebCore;
using System.Data;
using ProductInStock = ERP.Inventory.BO.ProductInStock;
using System.Xml;

namespace ERP.Inventory.DA
{
    public class DA_ProductInStock
    {
        /// <summary>
        /// Nạp thông tin yêu cầu xuất hàng
        /// </summary>
        /// <param name="objSaleOrder">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfoAndUpdateIsOrdering(ref ProductInStock objProductInStock, string strBarcode, int intStoreID, int intOutputTypeID,
            string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID, bool IsUpdateOrdering, int intCustomerID = -1)
        {
            bool bolIsExist = false;
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_PRODUCT_SELINSTOCK");
                objIData.AddParameter("@Barcode", strBarcode);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@AutoGetIMEIList", strAutoGetIMEIList);
                objIData.AddParameter("@IsAutoGetIMEI", bolIsAutoGetIMEI);
                objIData.AddParameter("@InstockStatusID", intInstockStatusID);
                objIData.AddParameter("@CustomerID", intCustomerID);
                objIData.AddParameter("@IsUpdateOrdering", IsUpdateOrdering);
                IDataReader drReader = objIData.ExecStoreToDataReader();
                if (drReader.Read())
                {
                    if (objProductInStock == null)
                        objProductInStock = new ProductInStock();
                    objProductInStock.ProductID = Convert.ToString(drReader["ProductID"]).Trim();
                    objProductInStock.ProductName = Convert.ToString(drReader["ProductName"]).Trim();
                    objProductInStock.SMSProductCode = Convert.ToString(drReader["SMSProductCode"]).Trim();
                    objProductInStock.VAT = Convert.ToInt32(drReader["VAT"]);
                    objProductInStock.IsRequestIMEI = (Convert.ToInt32(drReader["ISREQUESTIMEI"]) > 0);
                    objProductInStock.IsInstock = (Convert.ToInt32(drReader["IsInStock"]) > 0);
                    objProductInStock.IMEI = Convert.ToString(drReader["IMEI"]).Trim();
                    objProductInStock.Quantity = Convert.ToDecimal(drReader["Quantity"]);
                    if (!Convert.IsDBNull(drReader["EndWarrantyDate"]))
                        objProductInStock.EndWarrantyDate = Convert.ToDateTime(drReader["EndWarrantyDate"]);

                    objProductInStock.VATPercent = Convert.ToInt32(drReader["VATPercent"]);
                    objProductInStock.IsService = (Convert.ToInt32(drReader["IsService"]) > 0);

                    objProductInStock.FirstCustomerID = Convert.ToInt32(drReader["FirstCustomerID"]);
                    objProductInStock.CostPrice = Convert.ToDecimal(drReader["CostPrice"]);
                    objProductInStock.FirstPrice = Convert.ToDecimal(drReader["FirstPrice"]);
                    objProductInStock.FirstInputTypeID = Convert.ToInt32(drReader["FirstInputTypeID"]);
                    objProductInStock.ReturnOutputTypeID = Convert.ToInt32(drReader["ReturnOutputTypeID"]);
                    objProductInStock.IsNew = Convert.ToBoolean(drReader["IsNew"]);
                    objProductInStock.IsShowProduct = Convert.ToBoolean(drReader["IsShowProduct"]);
                    objProductInStock.InputVoucherDetailID = Convert.ToString(drReader["INPUTVOUCHERDETAILID"]).Trim();
                    objProductInStock.FirstInputVoucherID = Convert.ToString(drReader["FirstInputVoucherID"]).Trim();
                    if (!Convert.IsDBNull(drReader["InputVoucherDate"]))
                        objProductInStock.InputVoucherDate = Convert.ToDateTime(drReader["InputVoucherDate"]);
                    if (!Convert.IsDBNull(drReader["FirstInvoiceDate"]))
                        objProductInStock.FirstInvoiceDate = Convert.ToDateTime(drReader["FirstInvoiceDate"]);
                    objProductInStock.IsOrder = Convert.ToInt32(drReader["IsOrder"]) > 0;
                    objProductInStock.IsOrdering = Convert.ToInt32(drReader["IsOrdering"]) > 0;
                    objProductInStock.IsDelivery = Convert.ToInt32(drReader["IsDelivery"]) > 0;
                    objProductInStock.OrderID = Convert.ToString(drReader["OrderID"]).Trim();
                    objProductInStock.IsHasWarranty = Convert.ToInt32(drReader["IsHasWarranty"]) > 0;
                    objProductInStock.Note = Convert.ToString(drReader["Note"]).Trim();
                    objProductInStock.IsReturnProduct = Convert.ToInt32(drReader["IsReturnProduct"]) > 0;
                    objProductInStock.InputStaffUser = Convert.ToString(drReader["InputStaffUser"]).Trim();
                    objProductInStock.IsCheckRealInput = Convert.ToBoolean(drReader["IsCheckRealInput"]);
                    objProductInStock.IsReturnWithFee = Convert.ToBoolean(drReader["IsReturnWithFee"]);
                    objProductInStock.IsAutoGetIMEI = Convert.ToBoolean(drReader["IsAutoGetIMEI"]);
                    objProductInStock.IsCheckStockQuantity = Convert.ToBoolean(drReader["ISCHECKSTOCKQUANTITY"]);
                    objProductInStock.IsCanReturnOutput = Convert.ToBoolean(drReader["ISCANRETURNOUTPUT"]);
                    objProductInStock.Price = Convert.ToDecimal(drReader["Price"]);
                    objProductInStock.IsAllowDecimal = Convert.ToBoolean(drReader["ISALLOWDECIMAL"]);
                    objProductInStock.BrandID = Convert.ToInt32(drReader["BrandID"]);
                    objProductInStock.SubGroupID = Convert.ToInt32(drReader["SubGroupID"]);
                    objProductInStock.MainGroupID = Convert.ToInt32(drReader["MainGroupID"]);
                    objProductInStock.SubGroupName = Convert.ToString(drReader["SubGroupName"]).Trim();
                    objProductInStock.MainGroupName = Convert.ToString(drReader["MainGroupName"]).Trim();
                    objProductInStock.RetailPrice = Convert.ToDecimal(drReader["RetailPrice"]);
                    objProductInStock.IsPriceOfProduct = Convert.ToBoolean(drReader["IsPriceOfProduct"]);
                    objProductInStock.MinInStock = Convert.ToDecimal(drReader["MinInStock"]);
                    objProductInStock.QuantityUnitName = Convert.ToString(drReader["QuantityUnitName"]).Trim();
                    objProductInStock.IsPriceOfOutputType = Convert.ToBoolean(drReader["IsPriceOfOutputType"]);
                    objProductInStock.RealQuantity = Convert.ToDecimal(drReader["RealQuantity"]);
                    objProductInStock.GetIMEIType = Convert.ToInt32(drReader["GetIMEIType"]);
                    objProductInStock.InvoiceNote = Convert.ToString(drReader["InvoiceNote"]).Trim();
                    objProductInStock.RewardPoint = Convert.ToDecimal(drReader["RewardPoint"]);
                    objProductInStock.PriceProtect = Convert.ToDecimal(drReader["PriceProtect"]);
                    objProductInStock.InStockStatusID = Convert.ToInt32(drReader["InStockStatusID"]);
                    objProductInStock.RealQuantityProduct = Convert.ToDecimal(drReader["RealQuantityProduct"]);
                    objProductInStock.ConsignmentType = Convert.ToInt32(drReader["CONSIGNMENTTYPE"]);
                    objProductInStock.StatusFIFOID = Convert.ToInt32(drReader["STATUSFIFOID"]);
                    objProductInStock.StatusFIFO = (drReader["STATUSFIFONAME"] ?? "").ToString().Trim();
                    bolIsExist = true;
                }
                drReader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm lấy thông tin barcode", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> LoadInfo", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            if (!bolIsExist)
                objProductInStock = null;
            return objResultMessage;
        }

        /// <summary>
        /// Không lấy theo trạng thái sản phẩm mặc định mới =1
        /// </summary>
        /// <param name="objProductInStock"></param>
        /// <param name="strBarcode"></param>
        /// <param name="intStoreID"></param>
        /// <param name="intOutputTypeID"></param>
        /// <param name="strAutoGetIMEIList"></param>
        /// <param name="bolIsAutoGetIMEI"></param>
        /// <returns></returns>
        public ResultMessage LoadInfoAndUpdateIsOrdering(ref ProductInStock objProductInStock, string strBarcode, int intStoreID, int intOutputTypeID,
      string strAutoGetIMEIList, bool bolIsAutoGetIMEI, bool IsUpdateOrdering)
        {
            bool bolIsExist = false;
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_PRODUCT_SELINSTOCK");
                objIData.AddParameter("@Barcode", strBarcode);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@AutoGetIMEIList", strAutoGetIMEIList);
                objIData.AddParameter("@IsAutoGetIMEI", bolIsAutoGetIMEI);
                objIData.AddParameter("@IsUpdateOrdering", IsUpdateOrdering);
                IDataReader drReader = objIData.ExecStoreToDataReader();
                if (drReader.Read())
                {
                    if (objProductInStock == null)
                        objProductInStock = new ProductInStock();
                    objProductInStock.ProductID = Convert.ToString(drReader["ProductID"]).Trim();
                    objProductInStock.ProductName = Convert.ToString(drReader["ProductName"]).Trim();
                    objProductInStock.SMSProductCode = Convert.ToString(drReader["SMSProductCode"]).Trim();
                    objProductInStock.VAT = Convert.ToInt32(drReader["VAT"]);
                    objProductInStock.IsRequestIMEI = (Convert.ToInt32(drReader["ISREQUESTIMEI"]) > 0);
                    objProductInStock.IsInstock = (Convert.ToInt32(drReader["IsInStock"]) > 0);
                    objProductInStock.IMEI = Convert.ToString(drReader["IMEI"]).Trim();
                    objProductInStock.Quantity = Convert.ToDecimal(drReader["Quantity"]);
                    if (!Convert.IsDBNull(drReader["EndWarrantyDate"]))
                        objProductInStock.EndWarrantyDate = Convert.ToDateTime(drReader["EndWarrantyDate"]);

                    objProductInStock.VATPercent = Convert.ToInt32(drReader["VATPercent"]);
                    objProductInStock.IsService = (Convert.ToInt32(drReader["IsService"]) > 0);

                    objProductInStock.FirstCustomerID = Convert.ToInt32(drReader["FirstCustomerID"]);
                    objProductInStock.CostPrice = Convert.ToDecimal(drReader["CostPrice"]);
                    objProductInStock.FirstPrice = Convert.ToDecimal(drReader["FirstPrice"]);
                    objProductInStock.FirstInputTypeID = Convert.ToInt32(drReader["FirstInputTypeID"]);
                    objProductInStock.ReturnOutputTypeID = Convert.ToInt32(drReader["ReturnOutputTypeID"]);
                    objProductInStock.IsNew = Convert.ToBoolean(drReader["IsNew"]);
                    objProductInStock.IsShowProduct = Convert.ToBoolean(drReader["IsShowProduct"]);
                    objProductInStock.InputVoucherDetailID = Convert.ToString(drReader["INPUTVOUCHERDETAILID"]).Trim();
                    objProductInStock.FirstInputVoucherID = Convert.ToString(drReader["FirstInputVoucherID"]).Trim();
                    if (!Convert.IsDBNull(drReader["InputVoucherDate"]))
                        objProductInStock.InputVoucherDate = Convert.ToDateTime(drReader["InputVoucherDate"]);
                    if (!Convert.IsDBNull(drReader["FirstInvoiceDate"]))
                        objProductInStock.FirstInvoiceDate = Convert.ToDateTime(drReader["FirstInvoiceDate"]);
                    objProductInStock.IsOrder = Convert.ToInt32(drReader["IsOrder"]) > 0;
                    objProductInStock.IsOrdering = Convert.ToInt32(drReader["IsOrdering"]) > 0;
                    objProductInStock.IsDelivery = Convert.ToInt32(drReader["IsDelivery"]) > 0;
                    objProductInStock.OrderID = Convert.ToString(drReader["OrderID"]).Trim();
                    objProductInStock.IsHasWarranty = Convert.ToInt32(drReader["IsHasWarranty"]) > 0;
                    objProductInStock.Note = Convert.ToString(drReader["Note"]).Trim();
                    objProductInStock.IsReturnProduct = Convert.ToInt32(drReader["IsReturnProduct"]) > 0;
                    objProductInStock.InputStaffUser = Convert.ToString(drReader["InputStaffUser"]).Trim();
                    objProductInStock.IsCheckRealInput = Convert.ToBoolean(drReader["IsCheckRealInput"]);
                    objProductInStock.IsReturnWithFee = Convert.ToBoolean(drReader["IsReturnWithFee"]);
                    objProductInStock.IsAutoGetIMEI = Convert.ToBoolean(drReader["IsAutoGetIMEI"]);
                    objProductInStock.IsCheckStockQuantity = Convert.ToBoolean(drReader["ISCHECKSTOCKQUANTITY"]);
                    objProductInStock.IsCanReturnOutput = Convert.ToBoolean(drReader["ISCANRETURNOUTPUT"]);
                    objProductInStock.Price = Convert.ToDecimal(drReader["Price"]);
                    objProductInStock.IsAllowDecimal = Convert.ToBoolean(drReader["ISALLOWDECIMAL"]);
                    objProductInStock.BrandID = Convert.ToInt32(drReader["BrandID"]);
                    objProductInStock.SubGroupID = Convert.ToInt32(drReader["SubGroupID"]);
                    objProductInStock.MainGroupID = Convert.ToInt32(drReader["MainGroupID"]);
                    objProductInStock.SubGroupName = Convert.ToString(drReader["SubGroupName"]).Trim();
                    objProductInStock.MainGroupName = Convert.ToString(drReader["MainGroupName"]).Trim();
                    objProductInStock.RetailPrice = Convert.ToDecimal(drReader["RetailPrice"]);
                    objProductInStock.IsPriceOfProduct = Convert.ToBoolean(drReader["IsPriceOfProduct"]);
                    objProductInStock.MinInStock = Convert.ToDecimal(drReader["MinInStock"]);
                    objProductInStock.QuantityUnitName = Convert.ToString(drReader["QuantityUnitName"]).Trim();
                    objProductInStock.IsPriceOfOutputType = Convert.ToBoolean(drReader["IsPriceOfOutputType"]);
                    objProductInStock.RealQuantity = Convert.ToDecimal(drReader["RealQuantity"]);
                    objProductInStock.GetIMEIType = Convert.ToInt32(drReader["GetIMEIType"]);
                    objProductInStock.InvoiceNote = Convert.ToString(drReader["InvoiceNote"]).Trim();
                    objProductInStock.RewardPoint = Convert.ToDecimal(drReader["RewardPoint"]);
                    objProductInStock.PriceProtect = Convert.ToDecimal(drReader["PriceProtect"]);
                    objProductInStock.InStockStatusID = Convert.ToInt32(drReader["InStockStatusID"]);
                    objProductInStock.RealQuantityProduct = Convert.ToDecimal(drReader["REALQUANTITYPRODUCT"]);
                    objProductInStock.ConsignmentType = Convert.ToInt32(drReader["CONSIGNMENTTYPE"]);
                    objProductInStock.StatusFIFOID = Convert.ToInt32(drReader["STATUSFIFOID"]);
                    objProductInStock.StatusFIFO = (drReader["STATUSFIFONAME"] ?? "").ToString().Trim();
                    bolIsExist = true;
                }
                drReader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm lấy thông tin barcode", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> LoadInfo", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            if (!bolIsExist)
                objProductInStock = null;
            return objResultMessage;
        }

        #region Tuan Anh: Bo sung ham ban barcode coa update ordering
        public ResultMessage LoadInfo(ref ProductInStock objProductInStock, string strBarcode, int intStoreID, int intOutputTypeID,
           string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID, int intCustomerID = -1)
        {
            return LoadInfoAndUpdateIsOrdering(ref  objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                               strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, false, intCustomerID);
        }

        public ResultMessage LoadInfo(ref ProductInStock objProductInStock, string strBarcode, int intStoreID, int intOutputTypeID,
     string strAutoGetIMEIList, bool bolIsAutoGetIMEI)
        {
            return LoadInfoAndUpdateIsOrdering(ref  objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                              strAutoGetIMEIList, bolIsAutoGetIMEI, false);
        }
        #endregion

        /// <summary>
        /// Nạp thông tin sản phẩm và số lượng tồn kho
        /// </summary>
        public ResultMessage GetQuantity(ref ProductInStock objProductInStock, string strProductID, int intStoreID, int intIsNew, int intIsShowProduct, int intIsCheckRealInput)
        {
            bool bolIsExist = false;
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_PRODUCT_INSTOCK_GETQUANTITY");
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@IsNew", intIsNew);
                objIData.AddParameter("@ISSHOWPRODUCT", intIsShowProduct);
                objIData.AddParameter("@ISCHECKREALINPUT", intIsCheckRealInput);
                IDataReader drReader = objIData.ExecStoreToDataReader();
                if (drReader.Read())
                {
                    if (objProductInStock == null)
                        objProductInStock = new ProductInStock();
                    objProductInStock.ProductID = Convert.ToString(drReader["ProductID"]).Trim();
                    objProductInStock.ProductName = Convert.ToString(drReader["ProductName"]).Trim();
                    objProductInStock.VAT = Convert.ToInt32(drReader["VAT"]);
                    objProductInStock.IsRequestIMEI = (Convert.ToInt32(drReader["ISREQUESTIMEI"]) > 0);
                    objProductInStock.Quantity = Convert.ToInt32(drReader["Quantity"]);
                    objProductInStock.SMSProductCode = Convert.ToString(drReader["SMSProductCode"]).Trim();
                    objProductInStock.VATPercent = Convert.ToInt32(drReader["VATPercent"]);
                    objProductInStock.IsService = (Convert.ToInt32(drReader["IsService"]) > 0);
                    objProductInStock.IsCheckStockQuantity = Convert.ToBoolean(drReader["ISCHECKSTOCKQUANTITY"]);
                    objProductInStock.IsCanReturnOutput = Convert.ToBoolean(drReader["ISCANRETURNOUTPUT"]);
                    objProductInStock.IsAllowDecimal = Convert.ToBoolean(drReader["ISALLOWDECIMAL"]);
                    objProductInStock.BrandID = Convert.ToInt32(drReader["BrandID"]);
                    objProductInStock.SubGroupID = Convert.ToInt32(drReader["SubGroupID"]);
                    objProductInStock.MainGroupID = Convert.ToInt32(drReader["MainGroupID"]);
                    objProductInStock.SubGroupName = Convert.ToString(drReader["SubGroupName"]).Trim();
                    objProductInStock.MainGroupName = Convert.ToString(drReader["MainGroupName"]).Trim();
                    objProductInStock.MinInStock = Convert.ToDecimal(drReader["MinInStock"]);
                    objProductInStock.QuantityUnitName = Convert.ToString(drReader["QuantityUnitName"]).Trim();
                    bolIsExist = true;
                }
                drReader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm lấy thông tin barcode", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> LoadInfo", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            if (!bolIsExist)
                objProductInStock = null;
            return objResultMessage;
        }

        /// <summary>
        /// Kiểm tra tồn tại IMEI
        /// </summary>
        /// <param name="strProductID"></param>
        /// <param name="strIMEI"></param>
        /// <param name="intStoreID"></param>
        /// <param name="intIsNew"></param>
        /// <param name="intOutputTypeID"></param>
        /// <returns></returns>
        public ResultMessage CheckIMEIExists(ref Boolean bolResult, String strProductID, String strIMEI, int intStoreID, int intIsNew, int intOutputTypeID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            string strProductID1 = string.Empty;
            string strIMEI1 = string.Empty;
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_CURRENTINSTOCK_CHKIMEI");
                objIData.AddParameter("@IMEI", strIMEI.Trim());
                objIData.AddParameter("@ProductID", strProductID.Trim());
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@IsNew", intIsNew);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                IDataReader drReader = objIData.ExecStoreToDataReader();
                if (drReader.Read())
                {
                    strProductID1 = Convert.ToString(drReader["ProductID"]).Trim();
                    strIMEI1 = Convert.ToString(drReader["IMEI"]).Trim();
                    bolResult = true;
                }
                else
                    bolResult = false;
                drReader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm tra tồn kho hiện tại IMEI", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> CheckIMEIExists", InventoryGlobals.ModuleName);
                bolResult = false;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage LoadProductStoreInSock(String strProductID, int intAreaID, int intIsSaleStore, ref DataTable tblResult)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            string strProductID1 = string.Empty;
            string strIMEI1 = string.Empty;
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_CURRENTINSTOCK_STOREINSTOCK");
                objIData.AddParameter("@ProductID", strProductID.Trim());
                objIData.AddParameter("@AreaID", intAreaID);
                objIData.AddParameter("@IsSaleStore", intIsSaleStore);
                tblResult = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi lấy dữ liệu tồn kho theo kho của một sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> LoadProductStoreInSock", InventoryGlobals.ModuleName);
                tblResult = null;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Lấy giá của imei máy cũ (for MOBILE)    
        /// </summary>
        /// <param name="tblResult"></param>
        /// <param name="strProductID"></param>
        /// <param name="intOutputTypeID"></param>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        public ResultMessage GetIMEIPrice_ForMobile(ref DataTable tblResult, String strProductID, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            string strProductID1 = string.Empty;
            string strIMEI1 = string.Empty;
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_GETIMEIPRICE_FORMOBILE");
                objIData.AddParameter("@PRODUCTID", strProductID.Trim());
                objIData.AddParameter("@STOREID", intStoreID);
                tblResult = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi Lấy giá của imei máy cũ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> GetIMEIPrice_ForMobile", InventoryGlobals.ModuleName);
                tblResult = null;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Lấy sản phẩm tồn kho theo Model (for MOBILE)    
        /// </summary>
        /// <param name="tblResult"></param>
        /// <param name="strProductID"></param>
        /// <param name="intOutputTypeID"></param>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        public ResultMessage GetProductInstock_ByModel(ref DataTable tblResult, String strModelID, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            string strProductID1 = string.Empty;
            string strIMEI1 = string.Empty;
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_PRODUCTINSTOCK_BYMODEL");
                objIData.AddParameter("@MODELID", strModelID.Trim());
                objIData.AddParameter("@STOREID", intStoreID);
                tblResult = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi Lấy sản phẩm tồn kho theo Model (for MOBILE)  ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> GetProductInstock_ByModel", InventoryGlobals.ModuleName);
                tblResult = null;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public ResultMessage Update_IsOrdering(DataTable tblImei, int inIsOrdering, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            string strProductID1 = string.Empty;
            string xmlIMEI = CreateXMLFromDataTable(tblImei, "IMEI");
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INSTOCK_UPDATEORDERING");
                objIData.AddParameter("@ISORDERING", inIsOrdering);
                objIData.AddParameter("@STOREID", intStoreID);
                objIData.AddParameter("@IMEI", xmlIMEI, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.ExecNonQuery();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi cập nhật trạng thái đặt hàng tồn kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> Update_IsOrdering", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage Update_IsOrdering(string xmlIMEI, int inIsOrdering, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            string strProductID1 = string.Empty;
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INSTOCK_UPDATEORDERING");
                objIData.AddParameter("@ISORDERING", inIsOrdering);
                objIData.AddParameter("@STOREID", intStoreID);
                objIData.AddParameter("@IMEI", xmlIMEI, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.ExecNonQuery();

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi cập nhật trạng thái đặt hàng tồn kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> Update_IsOrdering", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private static string CreateXMLFromDataTable(DataTable dtb, string tableName)
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(tableName))
            {
                return string.Empty;
            }
            var sb = new StringBuilder();
            var xtx = XmlWriter.Create(sb);
            xtx.WriteStartDocument();
            xtx.WriteStartElement("", tableName.ToUpper() + "LIST", "");
            foreach (DataRow dr in dtb.Rows)
            {
                xtx.WriteStartElement("", tableName.ToUpper(), "");
                foreach (DataColumn dc in dtb.Columns)
                {
                    xtx.WriteAttributeString(dc.ColumnName.ToUpper(), dr[dc.ColumnName].ToString());
                }
                xtx.WriteEndElement();
            }
            xtx.WriteEndElement();
            xtx.Flush();
            xtx.Close();
            return sb.ToString();
        }
        public ResultMessage GetFIFOList_ByProductID(ref DataTable dtbFIFO, string strProductID, string strIMEI, int intStoreID, int intOutputTypeID,
         int intMaxIMEICanShow = 4)
        {
            bool bolIsExist = false;
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_PRODUCT_SELFIFO");
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@IMEI", strIMEI);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@MaxIMEI", intMaxIMEICanShow);
                dtbFIFO = objIData.ExecStoreToDataTable();
                if (dtbFIFO != null)
                {
                    dtbFIFO.TableName = "FIFO_DATA";
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi lấy danh sách fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> GetFIFOList_ByProductID", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }



        public ResultMessage GetFIFOList_ByIMEI(ref DataTable dtbFIFO, string xmlIMEI, string strProductID, int intStoreID, int intOutputTypeID,
         int intMaxIMEICanShow = 4)
        {
            bool bolIsExist = false;
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_PRODUCT_SELFIFO_BYIMEILIST");
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@MaxIMEI", intMaxIMEICanShow);
                objIData.AddParameter("@IMEI", xmlIMEI, Library.DataAccess.Globals.DATATYPE.NCLOB);
                dtbFIFO = objIData.ExecStoreToDataTable();
                if (dtbFIFO != null)
                {
                    dtbFIFO.TableName = "FIFO_DATA";
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi lấy danh sách fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> GetFIFOList_ByProductID", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetFIFOList_ByIMEI(ref DataTable dtbFIFO, DataTable dtbIMEI, string strProductID, int intStoreID, int intOutputTypeID,
         int intMaxIMEICanShow = 4)
        {
            string xmlIMEI = CreateXMLFromDataTable(dtbIMEI, "IMEI");
            return GetFIFOList_ByIMEI(ref dtbFIFO, xmlIMEI, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
        }

        public ResultMessage GetFIFOList_ByIMEI_Product(ref DataTable dtbFIFO, string xmlIMEI, string xmlProduct, int intStoreID, int intOutputTypeID,
         int intMaxIMEICanShow = 4)
        {
            bool bolIsExist = false;
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_PRODUCT_SELFIFO_BYPROLIST");
                objIData.AddParameter("@Product", xmlProduct, Library.DataAccess.Globals.DATATYPE.NCLOB);
                objIData.AddParameter("@StoreID", intStoreID);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@MaxIMEI", intMaxIMEICanShow);
                objIData.AddParameter("@IMEI", xmlIMEI, Library.DataAccess.Globals.DATATYPE.NCLOB);
                dtbFIFO = objIData.ExecStoreToDataTable();
                if (dtbFIFO != null)
                {
                    dtbFIFO.TableName = "FIFO_DATA";
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi lấy danh sách fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ProductInStock -> GetFIFOList_ByProductID", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetFIFOList_ByIMEI_Product(ref DataTable dtbFIFO, DataTable dtbIMEI, DataTable dtbProduct, int intStoreID, int intOutputTypeID,
         int intMaxIMEICanShow = 4)
        {
            string xmlIMEI = CreateXMLFromDataTable(dtbIMEI, "IMEI");
            string xmlProduct = CreateXMLFromDataTable(dtbProduct, "IMEI");
            return GetFIFOList_ByIMEI_Product(ref dtbFIFO, xmlIMEI, xmlProduct, intStoreID, intOutputTypeID, intMaxIMEICanShow);
        }
    }
}
