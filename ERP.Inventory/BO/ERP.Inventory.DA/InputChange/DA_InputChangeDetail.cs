
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputChangeDetail = ERP.Inventory.BO.InputChange.InputChangeDetail;
#endregion
namespace ERP.Inventory.DA.InputChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/17/2012 
	/// Chi tiết phiếu nhập đổi hàng
	/// </summary>	
	public partial class DA_InputChangeDetail
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objInputChangeDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref InputChangeDetail objInputChangeDetail, string strInputChangeDetailID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InputChangeDetail.colInputChangeDetailID, strInputChangeDetailID);
				IDataReader reader = objIData.ExecStoreToDataReader();
 				if (objInputChangeDetail == null) 
 					objInputChangeDetail = new InputChangeDetail();
				if (reader.Read())
 				{
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colInputChangeDetailID])) objInputChangeDetail.InputChangeDetailID = Convert.ToString(reader[InputChangeDetail.colInputChangeDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colInputChangeID])) objInputChangeDetail.InputChangeID = Convert.ToString(reader[InputChangeDetail.colInputChangeID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colInputChangeDate])) objInputChangeDetail.InputChangeDate = Convert.ToDateTime(reader[InputChangeDetail.colInputChangeDate]);
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colInputChangeStoreID])) objInputChangeDetail.InputChangeStoreID = Convert.ToInt32(reader[InputChangeDetail.colInputChangeStoreID]);
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colOldOutputVoucherDetailID])) objInputChangeDetail.OldOutputVoucherDetailID = Convert.ToString(reader[InputChangeDetail.colOldOutputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colProductID_In])) objInputChangeDetail.ProductID_In = Convert.ToString(reader[InputChangeDetail.colProductID_In]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colQuantity])) objInputChangeDetail.Quantity = Convert.ToDecimal(reader[InputChangeDetail.colQuantity]);
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colIMEI_In])) objInputChangeDetail.IMEI_In = Convert.ToString(reader[InputChangeDetail.colIMEI_In]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colProductID_Out])) objInputChangeDetail.ProductID_Out = Convert.ToString(reader[InputChangeDetail.colProductID_Out]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colIMEI_Out])) objInputChangeDetail.IMEI_Out = Convert.ToString(reader[InputChangeDetail.colIMEI_Out]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colNewInputVoucherDetailID])) objInputChangeDetail.NewInputVoucherDetailID = Convert.ToString(reader[InputChangeDetail.colNewInputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colNewOutputVoucherDetailID])) objInputChangeDetail.NewOutputVoucherDetailID = Convert.ToString(reader[InputChangeDetail.colNewOutputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colCreatedStoreID])) objInputChangeDetail.CreatedStoreID = Convert.ToInt32(reader[InputChangeDetail.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colCreatedUser])) objInputChangeDetail.CreatedUser = Convert.ToString(reader[InputChangeDetail.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colCreatedDate])) objInputChangeDetail.CreatedDate = Convert.ToDateTime(reader[InputChangeDetail.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colUpdatedUser])) objInputChangeDetail.UpdatedUser = Convert.ToString(reader[InputChangeDetail.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colUpdatedDate])) objInputChangeDetail.UpdatedDate = Convert.ToDateTime(reader[InputChangeDetail.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colIsDeleted])) objInputChangeDetail.IsDeleted = Convert.ToBoolean(reader[InputChangeDetail.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colDeletedUser])) objInputChangeDetail.DeletedUser = Convert.ToString(reader[InputChangeDetail.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputChangeDetail.colDeletedDate])) objInputChangeDetail.DeletedDate = Convert.ToDateTime(reader[InputChangeDetail.colDeletedDate]);
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết phiếu nhập đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objInputChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InputChangeDetail objInputChangeDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInputChangeDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết phiếu nhập đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InputChangeDetail objInputChangeDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + InputChangeDetail.colInputChangeID, objInputChangeDetail.InputChangeID);
				objIData.AddParameter("@" + InputChangeDetail.colInputChangeDate, objInputChangeDetail.InputChangeDate);
				objIData.AddParameter("@" + InputChangeDetail.colInputChangeStoreID, objInputChangeDetail.InputChangeStoreID);
				objIData.AddParameter("@" + InputChangeDetail.colOldOutputVoucherDetailID, objInputChangeDetail.OldOutputVoucherDetailID);
				objIData.AddParameter("@" + InputChangeDetail.colProductID_In, objInputChangeDetail.ProductID_In);
				objIData.AddParameter("@" + InputChangeDetail.colQuantity, objInputChangeDetail.Quantity);
				objIData.AddParameter("@" + InputChangeDetail.colIMEI_In, objInputChangeDetail.IMEI_In);
				objIData.AddParameter("@" + InputChangeDetail.colProductID_Out, objInputChangeDetail.ProductID_Out);
				objIData.AddParameter("@" + InputChangeDetail.colIMEI_Out, objInputChangeDetail.IMEI_Out);
				objIData.AddParameter("@" + InputChangeDetail.colNewInputVoucherDetailID, objInputChangeDetail.NewInputVoucherDetailID);
				objIData.AddParameter("@" + InputChangeDetail.colNewOutputVoucherDetailID, objInputChangeDetail.NewOutputVoucherDetailID);
                objIData.AddParameter("@" + InputChangeDetail.colCreatedStoreID, objInputChangeDetail.CreatedStoreID);
				objIData.AddParameter("@" + InputChangeDetail.colCreatedUser, objInputChangeDetail.CreatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objInputChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InputChangeDetail objInputChangeDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInputChangeDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết phiếu nhập đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InputChangeDetail objInputChangeDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InputChangeDetail.colInputChangeDetailID, objInputChangeDetail.InputChangeDetailID);
				objIData.AddParameter("@" + InputChangeDetail.colInputChangeID, objInputChangeDetail.InputChangeID);
				objIData.AddParameter("@" + InputChangeDetail.colInputChangeDate, objInputChangeDetail.InputChangeDate);
				objIData.AddParameter("@" + InputChangeDetail.colInputChangeStoreID, objInputChangeDetail.InputChangeStoreID);
				objIData.AddParameter("@" + InputChangeDetail.colOldOutputVoucherDetailID, objInputChangeDetail.OldOutputVoucherDetailID);
				objIData.AddParameter("@" + InputChangeDetail.colProductID_In, objInputChangeDetail.ProductID_In);
				objIData.AddParameter("@" + InputChangeDetail.colQuantity, objInputChangeDetail.Quantity);
				objIData.AddParameter("@" + InputChangeDetail.colIMEI_In, objInputChangeDetail.IMEI_In);
				objIData.AddParameter("@" + InputChangeDetail.colProductID_Out, objInputChangeDetail.ProductID_Out);
				objIData.AddParameter("@" + InputChangeDetail.colIMEI_Out, objInputChangeDetail.IMEI_Out);
				objIData.AddParameter("@" + InputChangeDetail.colNewInputVoucherDetailID, objInputChangeDetail.NewInputVoucherDetailID);
				objIData.AddParameter("@" + InputChangeDetail.colNewOutputVoucherDetailID, objInputChangeDetail.NewOutputVoucherDetailID);
				objIData.AddParameter("@" + InputChangeDetail.colCreatedStoreID, objInputChangeDetail.CreatedStoreID);
				objIData.AddParameter("@" + InputChangeDetail.colUpdatedUser, objInputChangeDetail.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin chi tiết phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objInputChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(InputChangeDetail objInputChangeDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInputChangeDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết phiếu nhập đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChangeDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChangeDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, InputChangeDetail objInputChangeDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InputChangeDetail.colInputChangeDetailID, objInputChangeDetail.InputChangeDetailID);
				objIData.AddParameter("@" + InputChangeDetail.colDeletedUser, objInputChangeDetail.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InputChangeDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_InPUTCHANGEDETAIL_ADD";
		public const String SP_UPDATE = "PM_InPUTCHANGEDETAIL_UPD";
		public const String SP_DELETE = "PM_InPUTCHANGEDETAIL_DEL";
		public const String SP_SELECT = "PM_InPUTCHANGEDETAIL_SEL";
		public const String SP_SEARCH = "PM_InPUTCHANGEDETAIL_SRH";
		public const String SP_UPDATEINDEX = "PM_InPUTCHANGEDETAIL_UPDINDEX";
		#endregion
		
	}
}
