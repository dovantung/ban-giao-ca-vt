
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.InputChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/17/2012 
	/// Bảng phiếu nhập đổi hàng
	/// </summary>	
	public partial class DA_InputChange
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin bảng phiếu nhập đổi hàng
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_InputChange.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin bảng phiếu nhập đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChange -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public ResultMessage CheckInputChangeByOutputVoucher(ref string strInputChangeID, string strOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            IDataReader reader = null;
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INPUTCHANGE_GETBYOUTPUVOUCHER");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["InputChangeID"]))
                    {
                        strInputChangeID = Convert.ToString(reader["InputChangeID"]).Trim();
                    }
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi kiểm tra phiếu nhập đổi hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChange -> CheckInputChangeByOutputVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CheckInputChangeByNewOutputVoucher(ref string strInputChangeID, string strNewOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            IDataReader reader = null;
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INPUCHANG_GETBYNEWOUTPUVOUCHER");
                objIData.AddParameter("@NewOutputVoucherID", strNewOutputVoucherID);
                reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["InputChangeID"]))
                    {
                        strInputChangeID = Convert.ToString(reader["InputChangeID"]).Trim();
                    }
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi kiểm tra phiếu nhập đổi hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChange -> CheckInputChangeByNewOutputVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public string GetInputChangeNewID(IData objIData, int intStoreID)
        {
            string strInputChangeNewID = string.Empty;
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTCHANGE_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strInputChangeNewID = objIData.ExecStoreToString();
                return strInputChangeNewID;
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        public ResultMessage InsertMasterAndDetail(ERP.Inventory.BO.InputChange.InputChange objInputChange)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                DA_OutputVoucher objDA_OutputVoucher = new DA_OutputVoucher();

                objInputChange.OutputVoucherBO.CreatedUser = objInputChange.CreatedUser;
                objDA_OutputVoucher.objLogObject = this.objLogObject;
                objDA_OutputVoucher.Insert(objIData, objInputChange.OutputVoucherBO);

                DA_InputVoucher objDA_InputVoucher = new DA_InputVoucher();

                objInputChange.InputVoucherBO.CreatedUser = objInputChange.CreatedUser;
                objDA_InputVoucher.objLogObject = this.objLogObject;
                objDA_InputVoucher.Insert(objIData, objInputChange.InputVoucherBO);

                objInputChange.NewInputVoucherID = objInputChange.InputVoucherBO.InputVoucherID;
                objInputChange.NewOutputVoucherID = objInputChange.OutputVoucherBO.OutputVoucherID;

                objInputChange.InputChangeID = GetInputChangeNewID(objIData, objInputChange.CreatedStoreID);

                this.Insert(objIData, objInputChange);

                DA_InputChangeDetail objDA_InputChangeDetail = new DA_InputChangeDetail();

                for (int i = 0; i < objInputChange.InputChangeDetailList.Count; i++)
                {
                    BO.InputChange.InputChangeDetail objInputChangeDetail = objInputChange.InputChangeDetailList[i];
                    objInputChangeDetail.InputChangeID = objInputChange.InputChangeID;
                    objInputChangeDetail.CreatedUser = objInputChange.CreatedUser;

                    objIData.CreateNewStoredProcedure("PM_InPUTCHANGEDETAIL_ADD");
                    objIData.AddParameter("@OutputVoucherID", objInputChange.NewOutputVoucherID);
                    objIData.AddParameter("@InputVoucherID", objInputChange.NewInputVoucherID);
                    objIData.AddParameter("@InputChangeID", objInputChangeDetail.InputChangeID);
                    objIData.AddParameter("@InputChangeDate", objInputChangeDetail.InputChangeDate);
                    objIData.AddParameter("@InputChangeStoreID", objInputChangeDetail.InputChangeStoreID);
                    objIData.AddParameter("@OldOutputVoucherDetailID", objInputChangeDetail.OldOutputVoucherDetailID);
                    objIData.AddParameter("@ProductID_In", objInputChangeDetail.ProductID_In);
                    objIData.AddParameter("@Quantity", objInputChangeDetail.Quantity);
                    objIData.AddParameter("@IMEI_In", objInputChangeDetail.IMEI_In);
                    objIData.AddParameter("@ProductID_Out", objInputChangeDetail.ProductID_Out);
                    objIData.AddParameter("@IMEI_Out", objInputChangeDetail.IMEI_Out);
                    objIData.AddParameter("@CreatedStoreID", objInputChangeDetail.CreatedStoreID);
                    objIData.AddParameter("@CreatedUser", objInputChangeDetail.CreatedUser);
                    objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                    objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                    objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);

                    objIData.AddParameter("@Quantity_IN", objInputChangeDetail.Quantity);
                    objIData.AddParameter("@IsNew_IN", objInputChange.InputVoucherBO.InputVoucherDetailList[0].IsNew);
                    objIData.AddParameter("@InputTypeID_IN", objInputChange.InputVoucherBO.InputTypeID);
                    objIData.AddParameter("@CustomerID_IN", objInputChange.InputVoucherBO.CustomerID);
                    objIData.AddParameter("@InputDate_IN", objInputChangeDetail.InputChangeDate);

                    for (int j = 0; j < objInputChange.InputVoucherBO.InputVoucherDetailList.Count; j++)
                    {
                        if (objInputChange.InputVoucherBO.InputVoucherDetailList[j].ProductID == objInputChangeDetail.ProductID_In)
                        {
                            objIData.AddParameter("@CostPrice_IN", objInputChange.InputVoucherBO.InputVoucherDetailList[j].CostPrice);
                            objIData.AddParameter("@ENDWarrantyDate_IN", objInputChange.InputVoucherBO.InputVoucherDetailList[j].ENDWarrantyDate);
                            for (int k = 0; k < objInputChange.InputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList.Count; k++)
                            {
                                if (objInputChange.InputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList[k].IMEI == objInputChangeDetail.IMEI_In)
                                {
                                    objIData.AddParameter("@IsHasWarranty_IN", objInputChange.InputVoucherBO.InputVoucherDetailList[j].InputVoucherDetailIMEIList[k].IsHasWarranty);
                                    break;
                                }
                            }
                            break;
                        }
                    }


                    objIData.AddParameter("@Quantity_Out", objInputChangeDetail.Quantity);
                    objIData.AddParameter("@IsNew_Out", objInputChange.OutputVoucherBO.OutputVoucherDetailList[0].IsNew);
                    objIData.AddParameter("@OutputTypeID_Out", objInputChangeDetail.OutputTypeID);
                    objIData.AddParameter("@OutputDate_Out", objInputChangeDetail.InputChangeDate);
                    objIData.AddParameter("@InputChangeDate_Out", objInputChangeDetail.InputChangeDate);
                    for (int j = 0; j < objInputChange.OutputVoucherBO.OutputVoucherDetailList.Count; j++)
                    {
                        if (objInputChange.OutputVoucherBO.OutputVoucherDetailList[j].ProductID == objInputChangeDetail.ProductID_Out)
                        {
                            objIData.AddParameter("@SalePrice_Out", objInputChange.OutputVoucherBO.OutputVoucherDetailList[j].SalePrice);
                            objIData.AddParameter("@CostPrice_Out", objInputChange.OutputVoucherBO.OutputVoucherDetailList[j].CostPrice);
                            break;
                        }
                    }

                    objIData.ExecNonQuery();
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi nhập đổi hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChange -> InsertMasterAndDetail", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

		#endregion
		
		
	}
}
