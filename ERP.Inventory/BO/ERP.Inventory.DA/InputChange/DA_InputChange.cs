
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputChange = ERP.Inventory.BO.InputChange.InputChange;
#endregion
namespace ERP.Inventory.DA.InputChange
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 11/17/2012 
	/// Bảng phiếu nhập đổi hàng
	/// </summary>	
	public partial class DA_InputChange
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin bảng phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objInputChange">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref ERP.Inventory.BO.InputChange.InputChange objInputChange, string strInputChangeID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeID, strInputChangeID);
				IDataReader reader = objIData.ExecStoreToDataReader();
                if (objInputChange == null)
                    objInputChange = new BO.InputChange.InputChange();
				if (reader.Read())
 				{
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colInputChangeID])) objInputChange.InputChangeID = Convert.ToString(reader[ERP.Inventory.BO.InputChange.InputChange.colInputChangeID]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colInputChangeDate])) objInputChange.InputChangeDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChange.InputChange.colInputChangeDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colInputChangeStoreID])) objInputChange.InputChangeStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.InputChange.InputChange.colInputChangeStoreID]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colOldOutputVoucherID])) objInputChange.OldOutputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.InputChange.InputChange.colOldOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colNewInputVoucherID])) objInputChange.NewInputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.InputChange.InputChange.colNewInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colNewOutputVoucherID])) objInputChange.NewOutputVoucherID = Convert.ToString(reader[ERP.Inventory.BO.InputChange.InputChange.colNewOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colInputChangeTypeID])) objInputChange.InputChangeTypeID = Convert.ToInt32(reader[ERP.Inventory.BO.InputChange.InputChange.colInputChangeTypeID]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colCreatedStoreID])) objInputChange.CreatedStoreID = Convert.ToInt32(reader[ERP.Inventory.BO.InputChange.InputChange.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colCreatedUser])) objInputChange.CreatedUser = Convert.ToString(reader[ERP.Inventory.BO.InputChange.InputChange.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colCreatedDate])) objInputChange.CreatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChange.InputChange.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colUpdatedUser])) objInputChange.UpdatedUser = Convert.ToString(reader[ERP.Inventory.BO.InputChange.InputChange.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colUpdatedDate])) objInputChange.UpdatedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChange.InputChange.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colIsDeleted])) objInputChange.IsDeleted = Convert.ToBoolean(reader[ERP.Inventory.BO.InputChange.InputChange.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colDeletedUser])) objInputChange.DeletedUser = Convert.ToString(reader[ERP.Inventory.BO.InputChange.InputChange.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[ERP.Inventory.BO.InputChange.InputChange.colDeletedDate])) objInputChange.DeletedDate = Convert.ToDateTime(reader[ERP.Inventory.BO.InputChange.InputChange.colDeletedDate]);
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin bảng phiếu nhập đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChange -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin bảng phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objInputChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(ERP.Inventory.BO.InputChange.InputChange objInputChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInputChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin bảng phiếu nhập đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChange -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin bảng phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, ERP.Inventory.BO.InputChange.InputChange objInputChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeID, objInputChange.InputChangeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeDate, objInputChange.InputChangeDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeStoreID, objInputChange.InputChangeStoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colOldOutputVoucherID, objInputChange.OldOutputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colNewInputVoucherID, objInputChange.NewInputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colNewOutputVoucherID, objInputChange.NewOutputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeTypeID, objInputChange.InputChangeTypeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colCreatedStoreID, objInputChange.CreatedStoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colCreatedUser, objInputChange.CreatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin bảng phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objInputChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Update(ERP.Inventory.BO.InputChange.InputChange objInputChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objInputChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin bảng phiếu nhập đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChange -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin bảng phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, ERP.Inventory.BO.InputChange.InputChange objInputChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeID, objInputChange.InputChangeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeDate, objInputChange.InputChangeDate);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeStoreID, objInputChange.InputChangeStoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colOldOutputVoucherID, objInputChange.OldOutputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colNewInputVoucherID, objInputChange.NewInputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colNewOutputVoucherID, objInputChange.NewOutputVoucherID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeTypeID, objInputChange.InputChangeTypeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colCreatedStoreID, objInputChange.CreatedStoreID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colUpdatedUser, objInputChange.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin bảng phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objInputChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(ERP.Inventory.BO.InputChange.InputChange objInputChange)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objInputChange);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin bảng phiếu nhập đổi hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputChange -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin bảng phiếu nhập đổi hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputChange">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, ERP.Inventory.BO.InputChange.InputChange objInputChange)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colInputChangeID, objInputChange.InputChangeID);
				objIData.AddParameter("@" + ERP.Inventory.BO.InputChange.InputChange.colDeletedUser, objInputChange.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_InputChange()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_INPUTCHANGE_ADD";
		public const String SP_UPDATE = "PM_INPUTCHANGE_UPD";
		public const String SP_DELETE = "PM_INPUTCHANGE_DEL";
		public const String SP_SELECT = "PM_INPUTCHANGE_SEL";
		public const String SP_SEARCH = "PM_INPUTCHANGE_SRH";
		public const String SP_UPDATEINDEX = "PM_INPUTCHANGE_UPDINDEX";
		#endregion
		
	}
}
