﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
    /// Create: Nguyễn Linh Tuấn
    /// Date: 14/11/2012
    /// </summary>
    public class DA_AutoSplitProduct
    {
        #region Chia hàng từ đơn hàng
        public ResultMessage CheckOrderReviewStatus(ref int intResult, string strOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("POM_Order_Check");
                objIData.AddParameter("@OrderID", strOrderID);
                intResult = Convert.ToInt32(objIData.ExecStoreToString());
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi kiểm tra đơn đặt hàng";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.CheckData, strMessage, objEx);
                objIData.RollBackTransaction();
                new SystemError(objIData, strMessage, objEx.ToString(), "AutoSplitProductDAO -> CheckOrderReviewStatus", "TGDD.ERP.Sale");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetInStockByOrder(ref DataTable dtbData, string strOrderID, string strAreaIDList, string strStoreIDList,int intIsCheckRealInput)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_Rpt_InStock_ListByOrder");
                objIData.AddParameter("@OrderID", strOrderID);
                objIData.AddParameter("@AreaIDList", strAreaIDList);
                objIData.AddParameter("@StoreIDList", strStoreIDList);
                objIData.AddParameter("@IsSaleStore", 1);
                objIData.AddParameter("@IsCheckRealInput", intIsCheckRealInput);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi nạp thông tin chia hàng tồn từ đơn hàng!";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.CheckData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_AutoSplitProduct-> GetInStockByOrder", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetAverageSale(ref DataTable dtbData, string strOrderID, int intNumDay, string strUserName, string strAreaIDList, string strStoreIDList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_AutoSplitProduct_LstAveSale");
                objIData.AddParameter("@OrderID", strOrderID);
                objIData.AddParameter("@NumDay", intNumDay);
                objIData.AddParameter("@username", strUserName);
                objIData.AddParameter("@AreaIDList", strAreaIDList);
                objIData.AddParameter("@StoreIDList", strStoreIDList);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi đọc thông tin số lượng bán bình quân";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_AutoSplitProduct -> GetAverageSale", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetProductAverageSale(ref DataTable dtbData, string strProductID, int intNumDay, string strUserName, string strAreaIDList, string strStoreIDList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_AutoSplitProduct_ProAveSale");
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@NumDay", intNumDay);
                objIData.AddParameter("@username", strUserName);
                objIData.AddParameter("@AreaIDList", strAreaIDList);
                objIData.AddParameter("@StoreIDList", strStoreIDList);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi đọc thông tin số lượng bán bình quân của một sản phẩm";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_AutoSplitProduct -> GetProductAverageSale", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetProductListByOrderID(ref DataTable dtbData, string strOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("POM_ORDERDETAIL_SPLITPRODUCT");
                objIData.AddParameter("@OrderID", strOrderID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi đọc danh sách sản phẩm";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_AutoSplitProduct-> GetProductListByOrderID", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        } 
        #endregion

        #region Chia hàng tự động từ các kho
        /// <summary>
        /// lấy dữ liệu tồn kho theo kho
        /// RPT_ReportStoreInStock_GetList
        /// </summary>
        public ResultMessage GetStoreInStock(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("RPT_ReportStoreInStock_GetList");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy dữ liệu tồn kho theo kho chia hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AutoSplitProduct -> GetStoreInStock", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

       
        /// <summary>
        /// lấy dữ liệu tồn kho theo kho
        /// PM_INPUTVOUCHER_GETSTOCHANGE
        /// </summary>
        public ResultMessage GetInputVoucherByStorechange(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_GETSTOCHANGE");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy số lượng nhập từ xuất chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AutoSplitProduct -> GetInputVoucherByStorechange", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tính số lượng bán trung bình theo kho
        /// PM_RPT_STOREINSTOCK_AVGSALE
        /// </summary>
        public ResultMessage GetAvgSaleStoreInStock(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_RPT_STOREINSTOCK_AVGSALE");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy số lượng nhập từ xuất chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AutoSplitProduct -> GetInputVoucherByStorechange", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// nạp số lượng đã tạo lệnh chuyển kho, tạo yêu cầu
        /// GETAUTOSTORECHANGEQUANTITY
        /// </summary>
        public ResultMessage GetAutoStoreChangeQuantity(ref DataTable dtbData,string strPruductIDList,string strFromStoreIDList,string strToStoreIDList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("GETAUTOSTORECHANGEQUANTITY");
                objIData.AddParameter("@ProductIDList",strPruductIDList);
                objIData.AddParameter("@FromStoreIDList", strFromStoreIDList);
                objIData.AddParameter("@ToStoreIDList", strToStoreIDList);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp số lượng đã tạo lệnh chuyển kho, tạo yêu cầu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AutoSplitProduct -> GetAutoStoreChangeQuantity", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion
    }
}
