
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.Warranty
{
    /// <summary>
    /// Created by 		: Nguyễn Linh Tuấn
	/// Created date 	: 02/05/2013 
	/// Quản lý đăng ký bảo hành
	/// </summary>	
	public partial class DA_WarrantyRegister
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin quản lý đăng ký bảo hành
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_WarrantyRegister.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin quản lý đăng ký bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyRegister -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// Thêm nhiều dòng thông tin quản lý đăng ký bảo hành
        /// </summary>
        public ResultMessage Insert(List<BO.Warranty.WarrantyRegister> objWarrantyRegisterList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                foreach (var item in objWarrantyRegisterList)
                {
                    Insert(objIData, item);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thêm nhiều dòng đăng ký bảo hành", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyRegister -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Đăng ký lại bảo hành
        /// </summary>
        public ResultMessage ReRegister(int intRegisterID, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_WARRANTY_REREGISTER");
                objIData.AddParameter("@OLDREGISTERID", intRegisterID);
                objIData.AddParameter("@USERREGISTER", strUserName);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi đăng ký lại bảo hành", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyRegister -> ReRegister", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
		#endregion
		
		
	}
}
