
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using WarrantyRegister = ERP.Inventory.BO.Warranty.WarrantyRegister;
#endregion
namespace ERP.Inventory.DA.Warranty
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn
	/// Created date 	: 02/05/2013 
	/// Quản lý đăng ký bảo hành
	/// </summary>	
	public partial class DA_WarrantyRegister
	{	
	
		#region Methods			
		

		/// <summary>
		/// Nạp thông tin quản lý đăng ký bảo hành
		/// </summary>
		/// <param name="objWarrantyRegister">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref WarrantyRegister objWarrantyRegister, decimal decWarrantyRegisterID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + WarrantyRegister.colWarrantyRegisterID, decWarrantyRegisterID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objWarrantyRegister = new WarrantyRegister();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colWarrantyRegisterID])) objWarrantyRegister.WarrantyRegisterID = Convert.ToDecimal(reader[WarrantyRegister.colWarrantyRegisterID]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colWarrantyRegisterTypeID])) objWarrantyRegister.WarrantyRegisterTypeID = Convert.ToInt32(reader[WarrantyRegister.colWarrantyRegisterTypeID]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colOutputDate])) objWarrantyRegister.OutputDate = Convert.ToDateTime(reader[WarrantyRegister.colOutputDate]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colOutputVoucherID])) objWarrantyRegister.OutputVoucherID = Convert.ToString(reader[WarrantyRegister.colOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colOutputVoucherDetailID])) objWarrantyRegister.OutputVoucherDetailID = Convert.ToString(reader[WarrantyRegister.colOutputVoucherDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colSaleOrderID])) objWarrantyRegister.SaleOrderID = Convert.ToString(reader[WarrantyRegister.colSaleOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colSaleOrderDetailID])) objWarrantyRegister.SaleOrderDetailID = Convert.ToString(reader[WarrantyRegister.colSaleOrderDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colProductID])) objWarrantyRegister.ProductID = Convert.ToString(reader[WarrantyRegister.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colIMEI])) objWarrantyRegister.IMEI = Convert.ToString(reader[WarrantyRegister.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colCustomerPhone])) objWarrantyRegister.CustomerPhone = Convert.ToString(reader[WarrantyRegister.colCustomerPhone]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colCustomerName])) objWarrantyRegister.CustomerName = Convert.ToString(reader[WarrantyRegister.colCustomerName]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colCustomerAddress])) objWarrantyRegister.CustomerAddress = Convert.ToString(reader[WarrantyRegister.colCustomerAddress]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colRegisterStoreID])) objWarrantyRegister.RegisterStoreID = Convert.ToDecimal(reader[WarrantyRegister.colRegisterStoreID]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colISRegisterFromOrder])) objWarrantyRegister.IsRegisterFromOrder = Convert.ToBoolean(reader[WarrantyRegister.colISRegisterFromOrder]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colISRegisterED])) objWarrantyRegister.IsRegistered = Convert.ToBoolean(reader[WarrantyRegister.colISRegisterED]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colRegisterEDTime])) objWarrantyRegister.RegisterEDTime = Convert.ToDateTime(reader[WarrantyRegister.colRegisterEDTime]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colReplyStatusID])) objWarrantyRegister.ReplyStatusID = Convert.ToDecimal(reader[WarrantyRegister.colReplyStatusID]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colReplyMessage])) objWarrantyRegister.ReplyMessage = Convert.ToString(reader[WarrantyRegister.colReplyMessage]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colReplyTime])) objWarrantyRegister.ReplyTime = Convert.ToDateTime(reader[WarrantyRegister.colReplyTime]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colISRegisterError])) objWarrantyRegister.ISRegisterError = Convert.ToBoolean(reader[WarrantyRegister.colISRegisterError]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colCreatedUser])) objWarrantyRegister.CreatedUser = Convert.ToString(reader[WarrantyRegister.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colCreatedDate])) objWarrantyRegister.CreatedDate = Convert.ToDateTime(reader[WarrantyRegister.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colUpdatedUser])) objWarrantyRegister.UpdatedUser = Convert.ToString(reader[WarrantyRegister.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colUpdatedDate])) objWarrantyRegister.UpdatedDate = Convert.ToDateTime(reader[WarrantyRegister.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colIsDeleted])) objWarrantyRegister.IsDeleted = Convert.ToBoolean(reader[WarrantyRegister.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colDeletedUser])) objWarrantyRegister.DeletedUser = Convert.ToString(reader[WarrantyRegister.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyRegister.colDeletedDate])) objWarrantyRegister.DeletedDate = Convert.ToDateTime(reader[WarrantyRegister.colDeletedDate]);
 				}
 				else
 				{
 					objWarrantyRegister = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin quản lý đăng ký bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyRegister -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin quản lý đăng ký bảo hành
		/// </summary>
		/// <param name="objWarrantyRegister">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(WarrantyRegister objWarrantyRegister)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objWarrantyRegister);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin quản lý đăng ký bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyRegister -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin quản lý đăng ký bảo hành
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objWarrantyRegister">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, WarrantyRegister objWarrantyRegister)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + WarrantyRegister.colWarrantyRegisterID, objWarrantyRegister.WarrantyRegisterID);
				objIData.AddParameter("@" + WarrantyRegister.colWarrantyRegisterTypeID, objWarrantyRegister.WarrantyRegisterTypeID);
				objIData.AddParameter("@" + WarrantyRegister.colOutputDate, objWarrantyRegister.OutputDate);
				objIData.AddParameter("@" + WarrantyRegister.colOutputVoucherID, objWarrantyRegister.OutputVoucherID);
				objIData.AddParameter("@" + WarrantyRegister.colOutputVoucherDetailID, objWarrantyRegister.OutputVoucherDetailID);
				objIData.AddParameter("@" + WarrantyRegister.colSaleOrderID, objWarrantyRegister.SaleOrderID);
				objIData.AddParameter("@" + WarrantyRegister.colSaleOrderDetailID, objWarrantyRegister.SaleOrderDetailID);
				objIData.AddParameter("@" + WarrantyRegister.colProductID, objWarrantyRegister.ProductID);
				objIData.AddParameter("@" + WarrantyRegister.colIMEI, objWarrantyRegister.IMEI);
				objIData.AddParameter("@" + WarrantyRegister.colCustomerPhone, objWarrantyRegister.CustomerPhone);
				objIData.AddParameter("@" + WarrantyRegister.colCustomerName, objWarrantyRegister.CustomerName);
				objIData.AddParameter("@" + WarrantyRegister.colCustomerAddress, objWarrantyRegister.CustomerAddress);
				objIData.AddParameter("@" + WarrantyRegister.colRegisterStoreID, objWarrantyRegister.RegisterStoreID);
				objIData.AddParameter("@" + WarrantyRegister.colISRegisterFromOrder, objWarrantyRegister.IsRegisterFromOrder);
				objIData.AddParameter("@" + WarrantyRegister.colISRegisterED, objWarrantyRegister.IsRegistered);
				objIData.AddParameter("@" + WarrantyRegister.colRegisterEDTime, objWarrantyRegister.RegisterEDTime);
				objIData.AddParameter("@" + WarrantyRegister.colReplyStatusID, objWarrantyRegister.ReplyStatusID);
				objIData.AddParameter("@" + WarrantyRegister.colReplyMessage, objWarrantyRegister.ReplyMessage);
				objIData.AddParameter("@" + WarrantyRegister.colReplyTime, objWarrantyRegister.ReplyTime);
				objIData.AddParameter("@" + WarrantyRegister.colISRegisterError, objWarrantyRegister.ISRegisterError);
				objIData.AddParameter("@" + WarrantyRegister.colCreatedUser, objWarrantyRegister.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin quản lý đăng ký bảo hành
		/// </summary>
		/// <param name="objWarrantyRegister">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(WarrantyRegister objWarrantyRegister)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objWarrantyRegister);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin quản lý đăng ký bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyRegister -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin quản lý đăng ký bảo hành
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objWarrantyRegister">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, WarrantyRegister objWarrantyRegister)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + WarrantyRegister.colWarrantyRegisterID, objWarrantyRegister.WarrantyRegisterID);
				objIData.AddParameter("@" + WarrantyRegister.colWarrantyRegisterTypeID, objWarrantyRegister.WarrantyRegisterTypeID);
				objIData.AddParameter("@" + WarrantyRegister.colOutputDate, objWarrantyRegister.OutputDate);
				objIData.AddParameter("@" + WarrantyRegister.colOutputVoucherID, objWarrantyRegister.OutputVoucherID);
				objIData.AddParameter("@" + WarrantyRegister.colOutputVoucherDetailID, objWarrantyRegister.OutputVoucherDetailID);
				objIData.AddParameter("@" + WarrantyRegister.colSaleOrderID, objWarrantyRegister.SaleOrderID);
				objIData.AddParameter("@" + WarrantyRegister.colSaleOrderDetailID, objWarrantyRegister.SaleOrderDetailID);
				objIData.AddParameter("@" + WarrantyRegister.colProductID, objWarrantyRegister.ProductID);
				objIData.AddParameter("@" + WarrantyRegister.colIMEI, objWarrantyRegister.IMEI);
				objIData.AddParameter("@" + WarrantyRegister.colCustomerPhone, objWarrantyRegister.CustomerPhone);
				objIData.AddParameter("@" + WarrantyRegister.colCustomerName, objWarrantyRegister.CustomerName);
				objIData.AddParameter("@" + WarrantyRegister.colCustomerAddress, objWarrantyRegister.CustomerAddress);
				objIData.AddParameter("@" + WarrantyRegister.colRegisterStoreID, objWarrantyRegister.RegisterStoreID);
				objIData.AddParameter("@" + WarrantyRegister.colISRegisterFromOrder, objWarrantyRegister.IsRegisterFromOrder);
				objIData.AddParameter("@" + WarrantyRegister.colISRegisterED, objWarrantyRegister.IsRegistered);
				objIData.AddParameter("@" + WarrantyRegister.colRegisterEDTime, objWarrantyRegister.RegisterEDTime);
				objIData.AddParameter("@" + WarrantyRegister.colReplyStatusID, objWarrantyRegister.ReplyStatusID);
				objIData.AddParameter("@" + WarrantyRegister.colReplyMessage, objWarrantyRegister.ReplyMessage);
				objIData.AddParameter("@" + WarrantyRegister.colReplyTime, objWarrantyRegister.ReplyTime);
				objIData.AddParameter("@" + WarrantyRegister.colISRegisterError, objWarrantyRegister.ISRegisterError);
				objIData.AddParameter("@" + WarrantyRegister.colUpdatedUser, objWarrantyRegister.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin quản lý đăng ký bảo hành
		/// </summary>
		/// <param name="objWarrantyRegister">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(WarrantyRegister objWarrantyRegister)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objWarrantyRegister);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin quản lý đăng ký bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyRegister -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin quản lý đăng ký bảo hành
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objWarrantyRegister">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, WarrantyRegister objWarrantyRegister)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + WarrantyRegister.colWarrantyRegisterID, objWarrantyRegister.WarrantyRegisterID);
				objIData.AddParameter("@" + WarrantyRegister.colDeletedUser, objWarrantyRegister.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_WarrantyRegister()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_WARRANTYREGISTER_ADD";
		public const String SP_UPDATE = "PM_WARRANTYREGISTER_UPD";
		public const String SP_DELETE = "PM_WARRANTYREGISTER_DEL";
		public const String SP_SELECT = "PM_WARRANTYREGISTER_SEL";
		public const String SP_SEARCH = "PM_WARRANTYREGISTER_SRH";
		public const String SP_UPDATEINDEX = "PM_WARRANTYREGISTER_UPDINDEX";
		#endregion
		
	}
}
