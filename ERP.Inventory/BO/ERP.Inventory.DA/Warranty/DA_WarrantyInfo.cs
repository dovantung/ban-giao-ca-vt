
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using WarrantyInfo = ERP.Inventory.BO.Warranty.WarrantyInfo;
#endregion
namespace ERP.Inventory.DA.Warranty
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 22/04/2013 
	/// Thông tin bảo hành
	/// </summary>	
	public partial class DA_WarrantyInfo
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin thông tin bảo hành
		/// </summary>
		/// <param name="objWarrantyInfo">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref WarrantyInfo objWarrantyInfo, string strIMEI, string strProductID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + WarrantyInfo.colIMEI, strIMEI);
				objIData.AddParameter("@" + WarrantyInfo.colProductID, strProductID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objWarrantyInfo = new WarrantyInfo();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colProductID])) objWarrantyInfo.ProductID = Convert.ToString(reader[WarrantyInfo.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colIMEI])) objWarrantyInfo.IMEI = Convert.ToString(reader[WarrantyInfo.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colInputDate])) objWarrantyInfo.InputDate = Convert.ToDateTime(reader[WarrantyInfo.colInputDate]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colInputVoucherID])) objWarrantyInfo.InputVoucherID = Convert.ToString(reader[WarrantyInfo.colInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colInputStoreID])) objWarrantyInfo.InputStoreID = Convert.ToInt32(reader[WarrantyInfo.colInputStoreID]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colIsActive])) objWarrantyInfo.IsActive = Convert.ToBoolean(reader[WarrantyInfo.colIsActive]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colActiveTime])) objWarrantyInfo.ActiveTime = Convert.ToDateTime(reader[WarrantyInfo.colActiveTime]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colOutputVoucherID])) objWarrantyInfo.OutputVoucherID = Convert.ToString(reader[WarrantyInfo.colOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colOutputDate])) objWarrantyInfo.OutputDate = Convert.ToDateTime(reader[WarrantyInfo.colOutputDate]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colWarrantyMonth])) objWarrantyInfo.WarrantyMonth = Convert.ToDecimal(reader[WarrantyInfo.colWarrantyMonth]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colBeginWarrantyDate])) objWarrantyInfo.BeginWarrantyDate = Convert.ToDateTime(reader[WarrantyInfo.colBeginWarrantyDate]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colEndWarrantyDate])) objWarrantyInfo.EndWarrantyDate = Convert.ToDateTime(reader[WarrantyInfo.colEndWarrantyDate]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colCustomerID])) objWarrantyInfo.CustomerID = Convert.ToInt32(reader[WarrantyInfo.colCustomerID]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colOutputStoreID])) objWarrantyInfo.OutputStoreID = Convert.ToInt32(reader[WarrantyInfo.colOutputStoreID]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colCustomerName])) objWarrantyInfo.CustomerName = Convert.ToString(reader[WarrantyInfo.colCustomerName]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colCustomerAddress])) objWarrantyInfo.CustomerAddress = Convert.ToString(reader[WarrantyInfo.colCustomerAddress]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colCustomerPhone])) objWarrantyInfo.CustomerPhone = Convert.ToString(reader[WarrantyInfo.colCustomerPhone]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colIsSendSMS])) objWarrantyInfo.IsSendSMS = Convert.ToBoolean(reader[WarrantyInfo.colIsSendSMS]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colSENTTime])) objWarrantyInfo.SENTTime = Convert.ToDateTime(reader[WarrantyInfo.colSENTTime]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colSMSContent])) objWarrantyInfo.SMSContent = Convert.ToString(reader[WarrantyInfo.colSMSContent]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colExtendWarrantyMonth])) objWarrantyInfo.ExtendWarrantyMonth = Convert.ToInt32(reader[WarrantyInfo.colExtendWarrantyMonth]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colWarrantyExtendID])) objWarrantyInfo.WarrantyExtendID = Convert.ToDecimal(reader[WarrantyInfo.colWarrantyExtendID]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colCreatedDate])) objWarrantyInfo.CreatedDate = Convert.ToDateTime(reader[WarrantyInfo.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colIsDeleted])) objWarrantyInfo.IsDeleted = Convert.ToBoolean(reader[WarrantyInfo.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colDeletedUser])) objWarrantyInfo.DeletedUser = Convert.ToString(reader[WarrantyInfo.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[WarrantyInfo.colDeletedDate])) objWarrantyInfo.DeletedDate = Convert.ToDateTime(reader[WarrantyInfo.colDeletedDate]);
 				}
 				else
 				{
 					objWarrantyInfo = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông tin bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyInfo -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin thông tin bảo hành
		/// </summary>
		/// <param name="objWarrantyInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(WarrantyInfo objWarrantyInfo)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objWarrantyInfo);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin thông tin bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyInfo -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin thông tin bảo hành
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objWarrantyInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, WarrantyInfo objWarrantyInfo)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + WarrantyInfo.colProductID, objWarrantyInfo.ProductID);
				objIData.AddParameter("@" + WarrantyInfo.colIMEI, objWarrantyInfo.IMEI);
				objIData.AddParameter("@" + WarrantyInfo.colInputDate, objWarrantyInfo.InputDate);
				objIData.AddParameter("@" + WarrantyInfo.colInputVoucherID, objWarrantyInfo.InputVoucherID);
				objIData.AddParameter("@" + WarrantyInfo.colInputStoreID, objWarrantyInfo.InputStoreID);
				objIData.AddParameter("@" + WarrantyInfo.colIsActive, objWarrantyInfo.IsActive);
				objIData.AddParameter("@" + WarrantyInfo.colActiveTime, objWarrantyInfo.ActiveTime);
				objIData.AddParameter("@" + WarrantyInfo.colOutputVoucherID, objWarrantyInfo.OutputVoucherID);
				objIData.AddParameter("@" + WarrantyInfo.colOutputDate, objWarrantyInfo.OutputDate);
				objIData.AddParameter("@" + WarrantyInfo.colWarrantyMonth, objWarrantyInfo.WarrantyMonth);
				objIData.AddParameter("@" + WarrantyInfo.colBeginWarrantyDate, objWarrantyInfo.BeginWarrantyDate);
				objIData.AddParameter("@" + WarrantyInfo.colEndWarrantyDate, objWarrantyInfo.EndWarrantyDate);
				objIData.AddParameter("@" + WarrantyInfo.colCustomerID, objWarrantyInfo.CustomerID);
				objIData.AddParameter("@" + WarrantyInfo.colOutputStoreID, objWarrantyInfo.OutputStoreID);
				objIData.AddParameter("@" + WarrantyInfo.colCustomerName, objWarrantyInfo.CustomerName);
				objIData.AddParameter("@" + WarrantyInfo.colCustomerAddress, objWarrantyInfo.CustomerAddress);
				objIData.AddParameter("@" + WarrantyInfo.colCustomerPhone, objWarrantyInfo.CustomerPhone);
				objIData.AddParameter("@" + WarrantyInfo.colIsSendSMS, objWarrantyInfo.IsSendSMS);
				objIData.AddParameter("@" + WarrantyInfo.colSENTTime, objWarrantyInfo.SENTTime);
				objIData.AddParameter("@" + WarrantyInfo.colSMSContent, objWarrantyInfo.SMSContent);
				objIData.AddParameter("@" + WarrantyInfo.colExtendWarrantyMonth, objWarrantyInfo.ExtendWarrantyMonth);
				objIData.AddParameter("@" + WarrantyInfo.colWarrantyExtendID, objWarrantyInfo.WarrantyExtendID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin thông tin bảo hành
		/// </summary>
		/// <param name="objWarrantyInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(WarrantyInfo objWarrantyInfo)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objWarrantyInfo);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin thông tin bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyInfo -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin thông tin bảo hành
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objWarrantyInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, WarrantyInfo objWarrantyInfo)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + WarrantyInfo.colProductID, objWarrantyInfo.ProductID);
				objIData.AddParameter("@" + WarrantyInfo.colIMEI, objWarrantyInfo.IMEI);
				objIData.AddParameter("@" + WarrantyInfo.colInputDate, objWarrantyInfo.InputDate);
				objIData.AddParameter("@" + WarrantyInfo.colInputVoucherID, objWarrantyInfo.InputVoucherID);
				objIData.AddParameter("@" + WarrantyInfo.colInputStoreID, objWarrantyInfo.InputStoreID);
				objIData.AddParameter("@" + WarrantyInfo.colIsActive, objWarrantyInfo.IsActive);
				objIData.AddParameter("@" + WarrantyInfo.colActiveTime, objWarrantyInfo.ActiveTime);
				objIData.AddParameter("@" + WarrantyInfo.colOutputVoucherID, objWarrantyInfo.OutputVoucherID);
				objIData.AddParameter("@" + WarrantyInfo.colOutputDate, objWarrantyInfo.OutputDate);
				objIData.AddParameter("@" + WarrantyInfo.colWarrantyMonth, objWarrantyInfo.WarrantyMonth);
				objIData.AddParameter("@" + WarrantyInfo.colBeginWarrantyDate, objWarrantyInfo.BeginWarrantyDate);
				objIData.AddParameter("@" + WarrantyInfo.colEndWarrantyDate, objWarrantyInfo.EndWarrantyDate);
				objIData.AddParameter("@" + WarrantyInfo.colCustomerID, objWarrantyInfo.CustomerID);
				objIData.AddParameter("@" + WarrantyInfo.colOutputStoreID, objWarrantyInfo.OutputStoreID);
				objIData.AddParameter("@" + WarrantyInfo.colCustomerName, objWarrantyInfo.CustomerName);
				objIData.AddParameter("@" + WarrantyInfo.colCustomerAddress, objWarrantyInfo.CustomerAddress);
				objIData.AddParameter("@" + WarrantyInfo.colCustomerPhone, objWarrantyInfo.CustomerPhone);
				objIData.AddParameter("@" + WarrantyInfo.colIsSendSMS, objWarrantyInfo.IsSendSMS);
				objIData.AddParameter("@" + WarrantyInfo.colSENTTime, objWarrantyInfo.SENTTime);
				objIData.AddParameter("@" + WarrantyInfo.colSMSContent, objWarrantyInfo.SMSContent);
				objIData.AddParameter("@" + WarrantyInfo.colExtendWarrantyMonth, objWarrantyInfo.ExtendWarrantyMonth);
				objIData.AddParameter("@" + WarrantyInfo.colWarrantyExtendID, objWarrantyInfo.WarrantyExtendID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin thông tin bảo hành
		/// </summary>
		/// <param name="objWarrantyInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(WarrantyInfo objWarrantyInfo)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objWarrantyInfo);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin thông tin bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyInfo -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin thông tin bảo hành
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objWarrantyInfo">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, WarrantyInfo objWarrantyInfo)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + WarrantyInfo.colProductID, objWarrantyInfo.ProductID);
				objIData.AddParameter("@" + WarrantyInfo.colIMEI, objWarrantyInfo.IMEI);
				objIData.AddParameter("@" + WarrantyInfo.colDeletedUser, objWarrantyInfo.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_WarrantyInfo()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_WARRANTYINFO_ADD";
		public const String SP_UPDATE = "PM_WARRANTYINFO_UPD";
		public const String SP_DELETE = "PM_WARRANTYINFO_DEL";
		public const String SP_SELECT = "PM_WARRANTYINFO_SEL";
		public const String SP_SEARCH = "PM_WARRANTYINFO_SRH";
		public const String SP_UPDATEINDEX = "PM_WARRANTYINFO_UPDINDEX";
		#endregion
		
	}
}
