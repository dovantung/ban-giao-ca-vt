
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.Warranty
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 22/04/2013 
	/// Thông tin bảo hành
	/// </summary>	
	public partial class DA_WarrantyInfo
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin thông tin bảo hành
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_WarrantyInfo.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin thông tin bảo hành", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyInfo -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// Thêm thông tin thông tin bảo hành
        /// </summary>
        /// <param name="objWarrantyInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage InsertWarrantyInfo(string strIMEI, string strOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_WARRANTYINFO_IMPV2");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                objIData.AddParameter("@IMEI", strIMEI);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin thông tin bảo hành", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyInfo -> InsertWarrantyInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Tính lại thời gian bảo hành
        /// </summary>
        /// <param name="objWarrantyInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage CalWarrantyTime(string strIMEI, string strProductID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_WARRANTYINFO_CALENDDATE");
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@IMEI", strIMEI);
                objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Others, "Lỗi tính lại thời gian bảo hành", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_WarrantyInfo -> CalWarrantyTime", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
		#endregion
		
		
	}
}
