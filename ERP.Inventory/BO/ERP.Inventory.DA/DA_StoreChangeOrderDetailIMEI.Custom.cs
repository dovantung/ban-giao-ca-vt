
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 12/5/2012 
	/// Chi tiết yêu cầu chuyển kho theo IMEI
	/// </summary>	
	public partial class DA_StoreChangeOrderDetailIMEI
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin chi tiết yêu cầu chuyển kho theo imei
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_StoreChangeOrderDetailIMEI.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chi tiết yêu cầu chuyển kho theo imei", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrderDetailIMEI -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}
		#endregion
		
		
	}
}
