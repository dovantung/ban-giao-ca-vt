
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using CURRENTInStock = ERP.Inventory.BO.CurrentInStock;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// 
	/// </summary>	
	public partial class DA_CurrentInStock
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin 
		/// </summary>
		/// <param name="objCURRENTInStock">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref CURRENTInStock objCURRENTInStock, string strProductID, int intStoreID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + CURRENTInStock.colProductID, strProductID);
				objIData.AddParameter("@" + CURRENTInStock.colStoreID, intStoreID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					if (objCURRENTInStock == null) 
 						objCURRENTInStock = new CURRENTInStock();
 					if (!Convert.IsDBNull(reader[CURRENTInStock.colStoreID])) objCURRENTInStock.StoreID = Convert.ToInt32(reader[CURRENTInStock.colStoreID]);
 					if (!Convert.IsDBNull(reader[CURRENTInStock.colProductID])) objCURRENTInStock.ProductID = Convert.ToString(reader[CURRENTInStock.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[CURRENTInStock.colQuantity])) objCURRENTInStock.Quantity = Convert.ToDecimal(reader[CURRENTInStock.colQuantity]);
 					if (!Convert.IsDBNull(reader[CURRENTInStock.colISOLDQuantity])) objCURRENTInStock.ISOLDQuantity = Convert.ToDecimal(reader[CURRENTInStock.colISOLDQuantity]);
 					if (!Convert.IsDBNull(reader[CURRENTInStock.colIsShowProductQuantity])) objCURRENTInStock.IsShowProductQuantity = Convert.ToDecimal(reader[CURRENTInStock.colIsShowProductQuantity]);
 					if (!Convert.IsDBNull(reader[CURRENTInStock.colOLDShowProductQuantity])) objCURRENTInStock.OLDShowProductQuantity = Convert.ToDecimal(reader[CURRENTInStock.colOLDShowProductQuantity]);
 					objCURRENTInStock.IsExist = true;
 				}
 				else
 				{
 					objCURRENTInStock.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStock -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objCURRENTInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(CURRENTInStock objCURRENTInStock)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objCURRENTInStock);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStock -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objCURRENTInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, CURRENTInStock objCURRENTInStock)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + CURRENTInStock.colStoreID, objCURRENTInStock.StoreID);
				objIData.AddParameter("@" + CURRENTInStock.colProductID, objCURRENTInStock.ProductID);
				objIData.AddParameter("@" + CURRENTInStock.colQuantity, objCURRENTInStock.Quantity);
				objIData.AddParameter("@" + CURRENTInStock.colISOLDQuantity, objCURRENTInStock.ISOLDQuantity);
				objIData.AddParameter("@" + CURRENTInStock.colIsShowProductQuantity, objCURRENTInStock.IsShowProductQuantity);
				objIData.AddParameter("@" + CURRENTInStock.colOLDShowProductQuantity, objCURRENTInStock.OLDShowProductQuantity);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objCURRENTInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(CURRENTInStock objCURRENTInStock)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objCURRENTInStock);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStock -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objCURRENTInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, CURRENTInStock objCURRENTInStock)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + CURRENTInStock.colStoreID, objCURRENTInStock.StoreID);
				objIData.AddParameter("@" + CURRENTInStock.colProductID, objCURRENTInStock.ProductID);
				objIData.AddParameter("@" + CURRENTInStock.colQuantity, objCURRENTInStock.Quantity);
				objIData.AddParameter("@" + CURRENTInStock.colISOLDQuantity, objCURRENTInStock.ISOLDQuantity);
				objIData.AddParameter("@" + CURRENTInStock.colIsShowProductQuantity, objCURRENTInStock.IsShowProductQuantity);
				objIData.AddParameter("@" + CURRENTInStock.colOLDShowProductQuantity, objCURRENTInStock.OLDShowProductQuantity);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objCURRENTInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(CURRENTInStock objCURRENTInStock)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objCURRENTInStock);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStock -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin 
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objCURRENTInStock">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, CURRENTInStock objCURRENTInStock)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + CURRENTInStock.colStoreID, objCURRENTInStock.StoreID);
				objIData.AddParameter("@" + CURRENTInStock.colProductID, objCURRENTInStock.ProductID);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_CurrentInStock()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_CURRENTINSTOCK_ADD";
		public const String SP_UPDATE = "PM_CURRENTINSTOCK_UPD";
		public const String SP_DELETE = "PM_CURRENTINSTOCK_DEL";
		public const String SP_SELECT = "PM_CURRENTINSTOCK_SEL";
		public const String SP_SEARCH = "PM_CURRENTINSTOCK_SRH";
		public const String SP_UPDATEINDEX = "PM_CURRENTINSTOCK_UPDINDEX";
		#endregion
		
	}
}
