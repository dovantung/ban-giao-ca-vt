
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using CurrentInStockDetail = ERP.Inventory.BO.CurrentInStockDetail;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// 
	/// </summary>	
	public partial class DA_CurrentInStockDetail
    {

        #region Methods			

        /// <summary>
        /// Tìm kiếm thông tin 
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_CurrentInStockDetail.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStockDetail -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objCurrentInStockDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage UpdateOrderIndex(ref List<CurrentInStockDetail> lstCurrentInStockDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                foreach (var objCurrentInStockDetail in lstCurrentInStockDetail)
                {
                    objCurrentInStockDetail.IsSuccess = UpdateOrderIndex(objIData, objCurrentInStockDetail);
                }
                objIData.CommitTransaction();

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin thứ tự", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CurrentInStockDetail -> UpdateOrderIndex", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Cập nhật thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objCurrentInStockDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        private bool UpdateOrderIndex(IData objIData, CurrentInStockDetail objCurrentInStockDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_CURRENTINSTOCKDT_UPD_ORDIDX");
                objIData.AddParameter("@" + CurrentInStockDetail.colOrderIndex, objCurrentInStockDetail.OrderIndex);
                objIData.AddParameter("@" + CurrentInStockDetail.colProductID, objCurrentInStockDetail.ProductID);
                objIData.AddParameter("@" + CurrentInStockDetail.colIMEI, objCurrentInStockDetail.IMEI);
                objIData.ExecNonQuery();
                return true;
            }
            catch (Exception objEx)
            {
                return false;
                //objIData.RollBackTransaction();
                //throw (objEx);
            }
        }

        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objCurrentInStockDetail">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage CheckExist(ref bool bolIsResult, string strIMEI, string strProductID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_CURRENTINSTOCKDETAIL_CHK");
                objIData.AddParameter("@" + CurrentInStockDetail.colIMEI, strIMEI);
                objIData.AddParameter("@" + CurrentInStockDetail.colProductID, strProductID);
                bolIsResult = Convert.ToBoolean(Convert.ToInt32(objIData.ExecStoreToString()));
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm tra tồn kho IMEI-Sản phẩm ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CurrentInStockDetail -> CheckExist", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion

        public ResultMessage CheckAutoGenIMEI(ref bool bolIsResult, string strProductID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_PRODUCT_CHECK_AUTOGENIMEI");
                objIData.AddParameter("@PRODUCTID", strProductID);
                bolIsResult = Convert.ToBoolean(Convert.ToInt32(objIData.ExecStoreToString()));
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi kiểm tra tự động lấy IMEI ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CurrentInStockDetail -> CheckAutoGenIMEI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetIMEIByQuantity(ref DataTable dtbResult, string strProductID, int intStoreID, int intQuantity, int intInStockStatusID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PRODUCT_GETIMEI_BYNUMBER");
                objIData.AddParameter("@NUMBER", intQuantity);
                objIData.AddParameter("@STOREID", intStoreID);
                objIData.AddParameter("@PRODUCTID", strProductID);
                objIData.AddParameter("@INSTOCKSTATUSID", intInStockStatusID);
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy danh sách IMEI theo số lượng cần ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStockDetail -> GetIMEIByQuantity", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetIMEIByQuantityCustom(ref DataTable dtbResult, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PRODUCT_GETIMEI_BYNUMBER");
                objIData.AddParameter(objKeywords);
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy danh sách IMEI theo số lượng cần ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStockDetail -> GetIMEIByQuantityCustom", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CheckIMEICustom(ref string strProductID, string strIMEI)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_CURRENINSTOCK_CHECKGENIMEI");
                objIData.AddParameter("@IMEI", strIMEI);
                DataTable dtb = objIData.ExecStoreToDataTable();
                if (dtb != null && dtb.Rows.Count > 0)
                    strProductID = dtb.Rows[0]["PRODUCTID"].ToString().Trim();
                else
                    strProductID = string.Empty;
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi check IMEI có sản phẩm ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CURRENTInStockDetail -> CheckIMEICustom", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Chỉnh sửa GetDataSource - Hàm Kiểm tra IMEi 
        /// </summary>
     
        public ResultMessage CheckIMEIInStock1(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("CHECKIMEI_INSTOCK_1");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi kiểm tra IMEI trong Store ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_CurrentInStock -> CheckIMEIInStock1", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
    }
}
