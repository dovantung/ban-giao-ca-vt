
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO;
using System.Linq;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
    /// Created by 		: Trương Trung Lợi 
    /// Created date 	: 11/09/2012 
    /// Phiếu xuất
    /// 
    /// 01/03/2017 : Hải Đăng kiểm tra xem có tôn tại trong sản phẩm đã toạn hóa đơn
    /// </summary>	
    public partial class DA_OutputVoucher
    {

        #region Methods

        /// <summary>
        /// Tìm kiếm thông tin phiếu xuất
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_OutputVoucher_SrhByKeywords");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin phiếu xuất", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }



        /// <summary>
        /// Tìm kiếm thông tin PVI
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchDataPVI(ref DataTable dtbData, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_PVIReports_SrhByKeywords");
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin PVI", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> SearchDataPVI", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CreateOutputVoucher(OutputVoucher objOutputVoucher)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                List<OutputVoucherDetail> lstOutputVoucherDetail_WarExt = null;
                CreateOutputVoucher(objIData, objOutputVoucher, ref lstOutputVoucherDetail_WarExt);
                //UpdateControlPromotion(objIData, objOutputVoucher.OutputVoucherDetailList);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tạo phiếu xuất", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> CreateOutputVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void UpdateControlPromotion(IData objIData, List<OutputVoucherDetail> lstOutputVoucherDetail)
        {

            //int intCountAddpromotion = lstOutputVoucherDetail.Where(x => x.IsAddPromotionDetail == true).Count();
            //if (intCountAddpromotion == lstOutputVoucherDetail.Count)
            //{
            //    var lst = from p in lstOutputVoucherDetail
            //              group p by new
            //              {
            //                  p.ProductID,
            //                  p.PromotionOfferID,
            //                  p.ApplySaleOrderDetailID,
            //                  p.SalePrice
            //              } into g
            //              select new
            //              {
            //                  ProductID = g.Key.ProductID,
            //                  PromotionOfferID = g.Key.PromotionOfferID,
            //                  ApplySaleOrderDetailID = g.Key.ApplySaleOrderDetailID,
            //                  SalePrice = g.Key.SalePrice,
            //                  SumQuantity = g.Sum(x => x.Quantity)
            //              };

            //    var lstCustom = lst.Where(x => !string.IsNullOrEmpty(x.ApplySaleOrderDetailID)).Select(x => new OutputVoucherDetail_Custom()
            //    {
            //        ApplySaleOrderDetailId = x.ApplySaleOrderDetailID,
            //        ProductId = x.ProductID,
            //        PromotionOfferId = x.PromotionOfferID,
            //        SalePrice = x.SalePrice,
            //        SumQuantity = x.SumQuantity
            //    }).ToList();
            //    if (lstCustom != null && lstCustom.Count > 0)
            //    {
            //        foreach (var item in lstCustom)
            //        {
            //            try
            //            {
            //                objIData.CreateNewStoredProcedure("RPT_CONTROL_PROMOTION_UPD");
            //                objIData.AddParameter("@ProductID", item.ProductId);
            //                objIData.AddParameter("@ApplySaleOrderDetailID", item.ApplySaleOrderDetailId);
            //                objIData.AddParameter("@PromotionOfferId", item.PromotionOfferId);
            //                objIData.AddParameter("@Quantity", item.SumQuantity);
            //                objIData.AddParameter("@PromotionPrice", item.SalePrice);
            //                objIData.ExecNonQuery();                            
            //            }
            //            catch (Exception objEx)
            //            {
            //                objIData.RollBackTransaction();
            //                throw (objEx);
            //            }
            //        }
            //    }
            //}
        }

        public void CreateOutputVoucher(IData objIData, OutputVoucher objOutputVoucher)
        {
            List<OutputVoucherDetail> lstOutputVoucherDetail_WarExt = null;
            CreateOutputVoucher(objIData, objOutputVoucher, ref lstOutputVoucherDetail_WarExt);
        }

        /// <summary>
        /// Tạo phiếu xuất
        /// Lấy về chi tiết phiếu xuất ( có bảo hành mở rộng)
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="objOutputVoucher"></param>
        /// <param name="lstOutputVoucherDetail_WarExt"></param>
        public void CreateOutputVoucher(IData objIData, OutputVoucher objOutputVoucher, ref List<OutputVoucherDetail> lstOutputVoucherDetail_WarExt)
        {
            this.Insert(objIData, objOutputVoucher);
            DA_OutputVoucherDetail objDA_OutputVoucherDetail = new DA_OutputVoucherDetail();
            objDA_OutputVoucherDetail.objLogObject = this.objLogObject;
            foreach (OutputVoucherDetail objOutputVoucherDetail in objOutputVoucher.OutputVoucherDetailList)
            {
                objOutputVoucherDetail.OutputVoucherID = objOutputVoucher.OutputVoucherID;
                objOutputVoucherDetail.SaleOrderID = objOutputVoucher.OrderID;
                objOutputVoucherDetail.InvoiceID = objOutputVoucher.InvoiceID;
                objOutputVoucherDetail.OutputDate = objOutputVoucher.OutputDate;
                objOutputVoucherDetail.OutputStoreID = objOutputVoucher.OutputStoreID;
                objDA_OutputVoucherDetail.Insert(objIData, objOutputVoucherDetail);
                if (lstOutputVoucherDetail_WarExt != null)
                {
                    if (!string.IsNullOrEmpty(objOutputVoucherDetail.ExtendWarrantyIMEI))
                    {
                        lstOutputVoucherDetail_WarExt.Add(objOutputVoucherDetail);
                    }
                }
            }
        }

        /// <summary>
        /// Tạo phiếu xuất v2
        /// Lấy về chi tiết phiếu xuất ( có bảo hành mở rộng)
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="objOutputVoucher"></param>
        /// <param name="lstOutputVoucherDetail_WarExt"></param>
        public void CreateOutputVoucher_V2(IData objIData, OutputVoucher objOutputVoucher, ref List<OutputVoucherDetail> lstOutputVoucherDetail_WarExt)
        {
            this.Insert(objIData, objOutputVoucher);
            DA_OutputVoucherDetail objDA_OutputVoucherDetail = new DA_OutputVoucherDetail();
            objDA_OutputVoucherDetail.objLogObject = this.objLogObject;
            foreach (OutputVoucherDetail objOutputVoucherDetail in objOutputVoucher.OutputVoucherDetailList)
            {
                objOutputVoucherDetail.OutputVoucherID = objOutputVoucher.OutputVoucherID;
                objOutputVoucherDetail.SaleOrderID = objOutputVoucher.OrderID;
                objOutputVoucherDetail.InvoiceID = objOutputVoucher.InvoiceID;
                objOutputVoucherDetail.OutputDate = objOutputVoucher.OutputDate;
                objOutputVoucherDetail.OutputStoreID = objOutputVoucher.OutputStoreID;
                objDA_OutputVoucherDetail.Insert_V2(objIData, objOutputVoucherDetail);
                if (lstOutputVoucherDetail_WarExt != null)
                {
                    if (!string.IsNullOrEmpty(objOutputVoucherDetail.ExtendWarrantyIMEI))
                    {
                        lstOutputVoucherDetail_WarExt.Add(objOutputVoucherDetail);
                    }
                }
            }
        }



        public ResultMessage CheckVoucherByOrderID(ref int intVoucherTypeID, string strSaleOrderID, string strOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("CM_VOUCHER_GETBYORDERID");
                objIData.AddParameter("@SaleOrderID", strSaleOrderID);
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                intVoucherTypeID = Convert.ToInt32(objIData.ExecStoreToString());
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi kiểm tra phiếu thu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> CheckVoucherByOrderID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetTotalPaidByOrderID(ref string strVoucherID, ref decimal decTotalPaid, ref decimal decDebt, string strSaleOrderID, string strOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("CMVouche_GetPaidByVouchConcern");
                objIData.AddParameter("@SaleOrderID", strSaleOrderID);
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                String strReturnValue = objIData.ExecStoreToString().Trim();
                String[] arrReturnValue = strReturnValue.Split(':');
                strVoucherID = arrReturnValue[0].Trim();
                decTotalPaid = Convert.ToDecimal(arrReturnValue[1]);
                decDebt = Convert.ToDecimal(arrReturnValue[2]);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi kiểm tra phiếu thu", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> CheckVoucherByOrderID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateCustomer(OutputVoucher objOutputVoucher, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                objIData.CreateNewStoredProcedure("PM_OUTPUTVOUCHER_UPDCUSTOMER");
                objIData.AddParameter("@OutputVoucherID", objOutputVoucher.OutputVoucherID);
                objIData.AddParameter("@CustomerID", objOutputVoucher.CustomerID);
                objIData.AddParameter("@CustomerName", objOutputVoucher.CustomerName);
                objIData.AddParameter("@CustomerAddress", objOutputVoucher.CustomerAddress);
                objIData.AddParameter("@CustomerPhone", objOutputVoucher.CustomerPhone);
                objIData.AddParameter("@CustomerTaxID", objOutputVoucher.CustomerTaxID);
                objIData.AddParameter("@OutputContent", objOutputVoucher.OutputContent);

                objIData.AddParameter("@UpdatedUser", strUserName);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@OutputNote", objOutputVoucher.OutputNote);
                objIData.ExecNonQuery();

                objIData.CreateNewStoredProcedure("PM_OUTPUTVOUCHER_UPDINVOICE");
                objIData.AddParameter("@OutputVoucherID", objOutputVoucher.OutputVoucherID);
                objIData.AddParameter("@InvoiceID", objOutputVoucher.InvoiceID);
                objIData.AddParameter("@InvoiceSymbol", objOutputVoucher.InvoiceSymbol);
                objIData.AddParameter("@Denominator", objOutputVoucher.Denominator);
                objIData.AddParameter("@InvoiceDate", objOutputVoucher.InvoiceDate);
                objIData.AddParameter("@PayableDate", objOutputVoucher.PayableDate);
                objIData.ExecNonQuery();
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi cập nhật thông tin khách hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> UpdateCustomer", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetList(ref DataTable dtbData, string strOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_OUTPUTVOUCHER_GETBYORDER");
                objIData.AddParameter("@OrderID", strOrderID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp danh sách phiếu xuất qua đơn hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> GetList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        public ResultMessage CheckOutputType_IsSubDebtOutput(ref bool bolIsError, string strOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_OUTPUTVOUCHER_CHKOUTPUTTYPE");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                string strResult = objIData.ExecStoreToString();
                if (strResult.Trim() == "0")
                    bolIsError = false;
                else
                    bolIsError = true;

            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp kiểm tra hình thức xuất", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> CheckOutputType_IsSubDebtOutput", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CheckOV_Order(ref DataTable dtbData, string strOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_OV_ORDER_CHK");
                objIData.AddParameter("@OrderID", strOrderID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi kiểm tra đơn hàng phiếu xuất", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_OutputVoucher -> CheckOV_Order", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tạo phiếu xuất PVI - Dang bo sung
        /// Lấy về chi tiết phiếu xuất ( có bảo hành mở rộng)
        /// </summary>
        /// <param name="objIData"></param>
        /// <param name="objOutputVoucher"></param>
        /// <param name="lstOutputVoucherDetail_WarExt"></param>
        public void CreateOutputVoucherPVI(IData objIData,
            string strSaleOrderID,
            OutputVoucher objOutputVoucher, int intInvoiceStoreID,
            MasterData.BO.MD.StoreChangeType objStoreChangeType,
            ref List<OutputVoucherDetail> lstOutputVoucherDetail_WarExt)
        {
            try
            {
                DA_OutputVoucherDetail objDA_OutputVoucherDetail = new DA_OutputVoucherDetail();
                #region Tạo chuyển kho master
                ResultMessage objResultMessage = new ResultMessage();
                string strStoreChangeID = new DA_StoreChange().CreateStoreChangeID(ref objResultMessage, objIData, objOutputVoucher.CreatedStoreID);
                string strInputVoucherID = string.Empty;
                objOutputVoucher.OrderID = string.Empty;
                objOutputVoucher.IsStoreChange = true;
                this.Insert(objIData, objOutputVoucher);
                string strContentInput = "MPX: " + objOutputVoucher.OutputVoucherID + " - Nội dung: xuất chuyển kho khác kho tạo";
                this.InsertInput(objIData, ref strInputVoucherID, objOutputVoucher, objStoreChangeType.InputTypeID, strContentInput, intInvoiceStoreID);
                string strContentOutput = "MPN: " + strInputVoucherID + " - Nội dung: xuất chuyển kho khác kho tạo";
                objIData.CreateNewStoredProcedure("ERP.PM_OUTPUTVOUCHER_UPDCONTENT");
                objIData.AddParameter("@OutputVoucherID", objOutputVoucher.OutputVoucherID);
                objIData.AddParameter("@OutputContent", strContentOutput);
                objIData.ExecNonQuery();
                decimal decQuantity = objOutputVoucher.OutputVoucherDetailList.Sum(x => x.Quantity);
                InsertStoreChange(objIData, strStoreChangeID, objOutputVoucher, objStoreChangeType, strInputVoucherID, intInvoiceStoreID, decQuantity);
                #endregion

                #region Tạo chuyển kho detail
                foreach (OutputVoucherDetail objOutputVoucherDetail in objOutputVoucher.OutputVoucherDetailList)
                {
                    int intOutputTypeID = objOutputVoucherDetail.OutputTypeID;
                    objOutputVoucherDetail.OutputVoucherID = objOutputVoucher.OutputVoucherID;
                    objOutputVoucherDetail.SaleOrderID = objOutputVoucher.OrderID;
                    objOutputVoucherDetail.InvoiceID = objOutputVoucher.InvoiceID;
                    objOutputVoucherDetail.OutputDate = objOutputVoucher.OutputDate;
                    objOutputVoucherDetail.OutputStoreID = objOutputVoucher.OutputStoreID;
                    objOutputVoucherDetail.OutputTypeID = Convert.ToInt32(objStoreChangeType.OutputTypeID);
                    objOutputVoucherDetail.CostPrice = 0;
                    objDA_OutputVoucherDetail.Insert(objIData, objOutputVoucherDetail);
                    objOutputVoucherDetail.OutputTypeID = intOutputTypeID;
                    CreateStoreChangePVI(objIData, objOutputVoucherDetail.OutputVoucherDetailID, objOutputVoucher.OutputVoucherID, strInputVoucherID, strStoreChangeID, intInvoiceStoreID, objStoreChangeType.StoreChangeTypeID);
                }

                #endregion
                #region Tạo phiếu xuất từ kho hóa đơn

                objOutputVoucher.OrderID = strSaleOrderID;
                objOutputVoucher.OutputStoreID = intInvoiceStoreID;
                objOutputVoucher.IsStoreChange = false;
                objOutputVoucher.OutputDate = ((DateTime)objOutputVoucher.OutputDate).AddSeconds(3);
                this.Insert(objIData, objOutputVoucher);
                
                objDA_OutputVoucherDetail.objLogObject = this.objLogObject;
                foreach (OutputVoucherDetail objOutputVoucherDetail in objOutputVoucher.OutputVoucherDetailList)
                {
                    objOutputVoucherDetail.OutputVoucherID = objOutputVoucher.OutputVoucherID;
                    objOutputVoucherDetail.SaleOrderID = objOutputVoucher.OrderID;
                    objOutputVoucherDetail.InvoiceID = objOutputVoucher.InvoiceID;
                    objOutputVoucherDetail.OutputDate = objOutputVoucher.OutputDate;
                    objOutputVoucherDetail.OutputStoreID = objOutputVoucher.OutputStoreID;
                    objOutputVoucherDetail.CostPrice = 0;
                    objDA_OutputVoucherDetail.Insert(objIData, objOutputVoucherDetail);
                    if (lstOutputVoucherDetail_WarExt != null)
                    {
                        if (!string.IsNullOrEmpty(objOutputVoucherDetail.ExtendWarrantyIMEI))
                        {
                            lstOutputVoucherDetail_WarExt.Add(objOutputVoucherDetail);
                        }
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw ex;
            }
        }

        /// <summary>
		/// Thêm thông tin phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void InsertInput(IData objIData, ref string strInputVoucherID, OutputVoucher objOutputVoucher,
            decimal intInputTypeID, string strContentInput, int intInvoiceStoreID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_ADD");
                objIData.AddParameter("@" + InputVoucher.colOrderID, objOutputVoucher.OrderID);
                objIData.AddParameter("@" + InputVoucher.colInVoiceID, objOutputVoucher.InvoiceID);
                objIData.AddParameter("@" + InputVoucher.colInVoiceSymbol, objOutputVoucher.InvoiceSymbol);
                objIData.AddParameter("@" + InputVoucher.colDENOMinAToR, objOutputVoucher.Denominator);
                objIData.AddParameter("@" + InputVoucher.colCustomerID, objOutputVoucher.CustomerID);
                objIData.AddParameter("@" + InputVoucher.colCustomerName, objOutputVoucher.CustomerName);
                objIData.AddParameter("@" + InputVoucher.colCustomerAddress, objOutputVoucher.CustomerAddress);
                objIData.AddParameter("@" + InputVoucher.colCustomerPhone, objOutputVoucher.CustomerPhone);
                objIData.AddParameter("@" + InputVoucher.colCustomerTaxID, objOutputVoucher.CustomerTaxID);
                objIData.AddParameter("@" + InputVoucher.colContent, strContentInput);
                objIData.AddParameter("@" + InputVoucher.colCreatedStoreID, objOutputVoucher.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucher.colInputStoreID, intInvoiceStoreID);
                //objIData.AddParameter("@" + InputVoucher.colAutoStoreChangeToStoreID, objInputVoucher.AutoStoreChangeToStoreID);
                objIData.AddParameter("@" + InputVoucher.colInputTypeID, intInputTypeID);
                objIData.AddParameter("@" + InputVoucher.colPayableTypeID, objOutputVoucher.PayableTypeID);
                objIData.AddParameter("@" + InputVoucher.colCurrencyUnitID, objOutputVoucher.CurrencyUnitID);
                objIData.AddParameter("@" + InputVoucher.colDiscountReasonID, objOutputVoucher.DiscountReasonID);
                objIData.AddParameter("@" + InputVoucher.colCreateDate, objOutputVoucher.CreatedDate);
                objIData.AddParameter("@" + InputVoucher.colInVoiceDate, objOutputVoucher.InvoiceDate);
                objIData.AddParameter("@" + InputVoucher.colInputDate, ((DateTime)objOutputVoucher.OutputDate).AddSeconds(1));
                objIData.AddParameter("@" + InputVoucher.colPayABLedate, objOutputVoucher.PayableDate);
                //objIData.AddParameter("@" + InputVoucher.colTaxMonth, objOutputVoucher.TaxMonth);
                objIData.AddParameter("@" + InputVoucher.colCurrencyExchange, objOutputVoucher.CurrencyExchange);
                objIData.AddParameter("@" + InputVoucher.colDiscount, objOutputVoucher.Discount);
                //objIData.AddParameter("@" + InputVoucher.colPROTECTPriceDiscount, objOutputVoucher.ProtectPriceDiscount);
                objIData.AddParameter("@" + InputVoucher.colTotalAmountBFT, objOutputVoucher.TotalAmountBFT);
                objIData.AddParameter("@" + InputVoucher.colTotalVAT, objOutputVoucher.TotalVAT);
                objIData.AddParameter("@" + InputVoucher.colTotalAmount, objOutputVoucher.TotalAmount);
                objIData.AddParameter("@" + InputVoucher.colCreatedUser, objOutputVoucher.CreatedUser);
                objIData.AddParameter("@" + InputVoucher.colStaffUser, objOutputVoucher.StaffUser);
                objIData.AddParameter("@" + InputVoucher.colIsNew, 1);
                //objIData.AddParameter("@" + InputVoucher.colIsReturnWithFee, objOutputVoucher.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucher.colISStoreChange, 1);
                objIData.AddParameter("@" + InputVoucher.colIsCheckRealInput, 1);
                objIData.AddParameter("@" + InputVoucher.colISPOSTED, 0);
                //objIData.AddParameter("@" + InputVoucher.colCheckRealInputNote, objInputVoucher.CheckRealInputNote);
                //objIData.AddParameter("@" + InputVoucher.colCheckRealInputUser, objInputVoucher.CheckRealInputUser);
                //objIData.AddParameter("@" + InputVoucher.colCheckRealInputTime, objInputVoucher.CheckRealInputTime);
                //objIData.AddParameter("@" + InputVoucher.colContentDeleted, objInputVoucher.ContentDeleted);
                objIData.AddParameter("@" + InputVoucher.colIDCardIssueDate, null);//objOutputVoucher.IdCardIssueDate);
                objIData.AddParameter("@" + InputVoucher.colIDCardIssuePlace, null);//objOutputVoucher.IdCardIssuePlace);
                //objIData.AddParameter("@" + InputVoucher.colIsErrorProduct, objInputVoucher.IsErrorProduct);
                //objIData.AddParameter("@" + InputVoucher.colErrorProductNote, objInputVoucher.ErrorProductNote);
                //objIData.AddParameter("@" + InputVoucher.colTransportCustomerID, objInputVoucher.TransportCustomerID);
                //objIData.AddParameter("@" + InputVoucher.colIsAutoCreateInvoice, objInputVoucher.IsAutoCreateInvoice);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + InputVoucher.colNote, objOutputVoucher.OutputNote);

                strInputVoucherID = objIData.ExecStoreToString().Trim();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        
        private void InsertStoreChange(IData objIData, string strStoreChangeID, OutputVoucher objOutputVoucher,
            MasterData.BO.MD.StoreChangeType objStoreChangeType,
            string strInputVoucherID, int intInvoiceStoreID, decimal decTotalQuantity)
        {
            try
            {

                objIData.CreateNewStoredProcedure("PM_STORECHANGE_Add");
                objIData.AddParameter("@STORECHANGEID", strStoreChangeID);
                objIData.AddParameter("@STORECHANGEORDERID", "");
                objIData.AddParameter("@FROMSTOREID", objOutputVoucher.OutputStoreID);
                objIData.AddParameter("@TOSTOREID", intInvoiceStoreID);
                objIData.AddParameter("@TRANSPORTTYPEID", 1);
                objIData.AddParameter("@STORECHANGETYPEID", objStoreChangeType.StoreChangeTypeID);
                objIData.AddParameter("@CONTENT", objOutputVoucher.OutputNote);
                objIData.AddParameter("@USERCREATE", objOutputVoucher.CreatedUser);
                objIData.AddParameter("@STORECHANGEDATE", objOutputVoucher.OutputDate);
                objIData.AddParameter("@OUTPUTVOUCHERID", objOutputVoucher.OutputVoucherID);
                objIData.AddParameter("@INPUTVOUCHERID", strInputVoucherID);
                objIData.AddParameter("@STORECHANGEUSER", objOutputVoucher.CreatedUser);
                objIData.AddParameter("@CreatedStoreID", objOutputVoucher.CreatedStoreID);
                objIData.AddParameter("@IsNew", 1);
                objIData.AddParameter("@INSTOCKSTATUSID", 1);
                objIData.AddParameter("@InvoiceID", "");
                objIData.AddParameter("@TotalQuantity", decTotalQuantity);
                //objIData.AddParameter("@TotalPacking", objStoreChangeBO.TotalPacking);
                //if (objStoreChangeBO.ToUser1 != "-1") objIData.AddParameter("@ToUser1", objStoreChangeBO.ToUser1);
                //if (objStoreChangeBO.ToUser2 != "-1") objIData.AddParameter("@ToUser2", objStoreChangeBO.ToUser2);
                objIData.AddParameter("@ISRECEIVE", 1);
                //if (objStoreChangeBO.InVoucherBO != null)
                //    objIData.AddParameter("@InVoucherID", objStoreChangeBO.InVoucherBO.VoucherID);
                //else
                objIData.AddParameter("@InVoucherID", string.Empty);
                //if (objStoreChangeBO.OutVoucherBO != null)
                //    objIData.AddParameter("@OutVoucherID", objStoreChangeBO.OutVoucherBO.VoucherID);
                //else
                objIData.AddParameter("@OutVoucherID", string.Empty);
                //objIData.AddParameter("@TotalWeight", objStoreChangeBO.TotalWeight);
                //objIData.AddParameter("@TotalSize", objStoreChangeBO.TotalSize);
                //objIData.AddParameter("@IsUrgent", objStoreChangeBO.IsUrgent);
                //objIData.AddParameter("@TotalShippingCost", objStoreChangeBO.TotalShippingCost);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                //objIData.AddParameter("@TransportVoucherID", objStoreChangeBO.TransportVoucherID);
                objIData.AddParameter("@InvoiceSymbol", objOutputVoucher.InvoiceSymbol);

                objIData.AddParameter("@IsCheckRealInput", 0);

                //objIData.AddParameter("@ISTRANSFERED", objStoreChangeBO.IsTransfered);
                //objIData.AddParameter("@TRANSFEREDUSER", objStoreChangeBO.TransferedUser);

                //objIData.AddParameter("@ISRECEIVEDTRANSFER", objStoreChangeBO.IsReceivedTransfer);
                //objIData.AddParameter("@RECEIVEDTRANSFERUSER", objStoreChangeBO.ReceivedTransferUser);

                //objIData.AddParameter("@ISSIGNRECEIVE", objStoreChangeBO.IsSignReceive);
                //objIData.AddParameter("@SIGNRECEIVEUSER", objStoreChangeBO.SignReceiveUser);
                //objIData.AddParameter("@SIGNRECEIVENOTE", objStoreChangeBO.SignReceiveNote);

                //objIData.AddParameter("@USERRECEIVE", objStoreChangeBO.UserReceive);
                //objIData.AddParameter("@RECEIVENOTE", objStoreChangeBO.ReceiveNote);

                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw ex;
            }
        }


        public void CreateStoreChangePVI(IData objIData,string strOutputVoucherDetailID, string strOutputVoucherID,
            string strInputVoucherID, string strStoreChangeID,
            int intInvoiceStoreID, int intStoreChangeTypeID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_OUTPUTVOUCHER_CREATESC");
                objIData.AddParameter("@OutputVoucherDetailID", strOutputVoucherDetailID);
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                objIData.AddParameter("@InputVoucherID", strInputVoucherID);
                objIData.AddParameter("@StoreChangeID", strStoreChangeID);
                objIData.AddParameter("@InvoiceStoreID", intInvoiceStoreID);
                objIData.AddParameter("@StoreChangeTypeID", intStoreChangeTypeID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw objEx;
            }
        }

        #endregion


    }
    public class OutputVoucherDetail_Custom
    {
        public string ProductId { get; set; }
        public string ApplySaleOrderDetailId { get; set; }
        public int PromotionOfferId { get; set; }
        public decimal SumQuantity { get; set; }
        public decimal SalePrice { get; set; }
    }
}
