
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using StoreChangeOrderDetail =  ERP.Inventory.BO.StoreChangeOrderDetail;
#endregion
namespace  ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Hoang Nhu Phong 
	/// Created date 	: 9/28/2012 
	/// Chi tiết yêu cầu chuyển kho
	/// </summary>	
	public partial class DA_StoreChangeOrderDetail
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin chi tiết yêu cầu chuyển kho
		/// </summary>
		/// <param name="objStoreChangeOrderDetail">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref StoreChangeOrderDetail objStoreChangeOrderDetail, string strStoreChangeOrderDetailID, string strStoreChangeOrderID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeOrderDetailID, strStoreChangeOrderDetailID);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeOrderID, strStoreChangeOrderID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					if (objStoreChangeOrderDetail == null) 
 						objStoreChangeOrderDetail = new StoreChangeOrderDetail();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colStoreChangeOrderDetailID])) objStoreChangeOrderDetail.StoreChangeOrderDetailID = Convert.ToString(reader[StoreChangeOrderDetail.colStoreChangeOrderDetailID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colStoreChangeOrderID])) objStoreChangeOrderDetail.StoreChangeOrderID = Convert.ToString(reader[StoreChangeOrderDetail.colStoreChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colOrderDate])) objStoreChangeOrderDetail.OrderDate = Convert.ToDateTime(reader[StoreChangeOrderDetail.colOrderDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colProductID])) objStoreChangeOrderDetail.ProductID = Convert.ToString(reader[StoreChangeOrderDetail.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colCommandQuantity])) objStoreChangeOrderDetail.CommandQuantity = Convert.ToDecimal(reader[StoreChangeOrderDetail.colCommandQuantity]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colQuantity])) objStoreChangeOrderDetail.Quantity = Convert.ToDecimal(reader[StoreChangeOrderDetail.colQuantity]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colStoreChangeQuantity])) objStoreChangeOrderDetail.StoreChangeQuantity = Convert.ToDecimal(reader[StoreChangeOrderDetail.colStoreChangeQuantity]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colIsManualAdd])) objStoreChangeOrderDetail.IsManualAdd = Convert.ToBoolean(reader[StoreChangeOrderDetail.colIsManualAdd]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colISAttachProduct])) objStoreChangeOrderDetail.ISAttachProduct = Convert.ToBoolean(reader[StoreChangeOrderDetail.colISAttachProduct]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colStoreChangeCommandIDList])) objStoreChangeOrderDetail.StoreChangeCommandIDList = Convert.ToString(reader[StoreChangeOrderDetail.colStoreChangeCommandIDList]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colNote])) objStoreChangeOrderDetail.Note = Convert.ToString(reader[StoreChangeOrderDetail.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colCreatedStoreID])) objStoreChangeOrderDetail.CreatedStoreID = Convert.ToInt32(reader[StoreChangeOrderDetail.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colCreatedUser])) objStoreChangeOrderDetail.CreatedUser = Convert.ToString(reader[StoreChangeOrderDetail.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colCreatedDate])) objStoreChangeOrderDetail.CreatedDate = Convert.ToDateTime(reader[StoreChangeOrderDetail.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colUpdatedUser])) objStoreChangeOrderDetail.UpdatedUser = Convert.ToString(reader[StoreChangeOrderDetail.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colUpdatedDate])) objStoreChangeOrderDetail.UpdatedDate = Convert.ToDateTime(reader[StoreChangeOrderDetail.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colIsDeleted])) objStoreChangeOrderDetail.IsDeleted = Convert.ToBoolean(reader[StoreChangeOrderDetail.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colDeletedUser])) objStoreChangeOrderDetail.DeletedUser = Convert.ToString(reader[StoreChangeOrderDetail.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeOrderDetail.colDeletedDate])) objStoreChangeOrderDetail.DeletedDate = Convert.ToDateTime(reader[StoreChangeOrderDetail.colDeletedDate]);
 					objStoreChangeOrderDetail.IsExist = true;
 				}
 				else
 				{
 					objStoreChangeOrderDetail.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrderDetail -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin chi tiết yêu cầu chuyển kho
		/// </summary>
		/// <param name="objStoreChangeOrderDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(StoreChangeOrderDetail objStoreChangeOrderDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objStoreChangeOrderDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết yêu cầu chuyển kho", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrderDetail -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin chi tiết yêu cầu chuyển kho
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrderDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
        public string Insert(IData objIData, StoreChangeOrderDetail objStoreChangeOrderDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                //objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeOrderDetailID, objStoreChangeOrderDetail.StoreChangeOrderDetailID);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeOrderID, objStoreChangeOrderDetail.StoreChangeOrderID);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colOrderDate, objStoreChangeOrderDetail.OrderDate);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colProductID, objStoreChangeOrderDetail.ProductID);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colCommandQuantity, objStoreChangeOrderDetail.CommandQuantity);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colQuantity, objStoreChangeOrderDetail.Quantity);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeQuantity, objStoreChangeOrderDetail.StoreChangeQuantity);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colIsManualAdd, objStoreChangeOrderDetail.IsManualAdd);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colISAttachProduct, objStoreChangeOrderDetail.ISAttachProduct);
                if (string.IsNullOrEmpty(objStoreChangeOrderDetail.StoreChangeCommandIDList))
                    objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeCommandIDList, objStoreChangeOrderDetail.StoreChangeCommandIDList);
                else
                    objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeCommandIDList, objStoreChangeOrderDetail.StoreChangeCommandIDList, Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colNote, objStoreChangeOrderDetail.Note);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colCreatedStoreID, objStoreChangeOrderDetail.CreatedStoreID);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colCreatedUser, objStoreChangeOrderDetail.CreatedUser);
                return objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


		/// <summary>
		/// Cập nhật thông tin chi tiết yêu cầu chuyển kho
		/// </summary>
		/// <param name="objStoreChangeOrderDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(StoreChangeOrderDetail objStoreChangeOrderDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objStoreChangeOrderDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết yêu cầu chuyển kho", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrderDetail -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin chi tiết yêu cầu chuyển kho
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrderDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, StoreChangeOrderDetail objStoreChangeOrderDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeOrderDetailID, objStoreChangeOrderDetail.StoreChangeOrderDetailID);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeOrderID, objStoreChangeOrderDetail.StoreChangeOrderID);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colOrderDate, objStoreChangeOrderDetail.OrderDate);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colProductID, objStoreChangeOrderDetail.ProductID);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colCommandQuantity, objStoreChangeOrderDetail.CommandQuantity);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colQuantity, objStoreChangeOrderDetail.Quantity);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeQuantity, objStoreChangeOrderDetail.StoreChangeQuantity);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colIsManualAdd, objStoreChangeOrderDetail.IsManualAdd);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colISAttachProduct, objStoreChangeOrderDetail.ISAttachProduct);
                objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeCommandIDList, objStoreChangeOrderDetail.StoreChangeCommandIDList, Library.DataAccess.Globals.DATATYPE.CLOB);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colNote, objStoreChangeOrderDetail.Note);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colCreatedStoreID, objStoreChangeOrderDetail.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colUpdatedUser, objStoreChangeOrderDetail.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
        /// Xóa thông tin chi tiết yêu cầu chuyển kho
        /// </summary>
        /// <param name="objStoreChangeOrderDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, BO.StoreChangeOrder objStoreChangeOrder)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE_BY_SCORDERID);
                objIData.AddParameter("@" + BO.StoreChangeOrder.colStoreChangeOrderID, objStoreChangeOrder.StoreChangeOrderID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
		/// <summary>
		/// Xóa thông tin chi tiết yêu cầu chuyển kho
		/// </summary>
		/// <param name="objStoreChangeOrderDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(StoreChangeOrderDetail objStoreChangeOrderDetail)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objStoreChangeOrderDetail);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết yêu cầu chuyển kho", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrderDetail -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin chi tiết yêu cầu chuyển kho
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeOrderDetail">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, StoreChangeOrderDetail objStoreChangeOrderDetail)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeOrderDetailID, objStoreChangeOrderDetail.StoreChangeOrderDetailID);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colStoreChangeOrderID, objStoreChangeOrderDetail.StoreChangeOrderID);
				objIData.AddParameter("@" + StoreChangeOrderDetail.colDeletedUser, objStoreChangeOrderDetail.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_StoreChangeOrderDetail()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_STORECHANGEORDERDETAIL_ADD";
		public const String SP_UPDATE = "PM_STORECHANGEORDERDETAIL_UPD";
        public const String SP_DELETE = "PM_STORECHANGEORDERDETAIL_DEL";
        public const String SP_DELETE_BY_SCORDERID = "PM_SCORDERDETAIL_DEL_BYORDERID";
		public const String SP_SELECT = "PM_STORECHANGEORDERDETAIL_SEL";
		public const String SP_SEARCH = "PM_STORECHANGEORDERDETAIL_SRH";
		public const String SP_UPDATEINDEX = "PM_STORECHANGEORDERDETAIL_UPDINDEX";
		#endregion
		
	}
}
