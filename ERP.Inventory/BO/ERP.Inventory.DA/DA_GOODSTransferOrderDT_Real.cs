﻿using ERP.SalesAndServices.SaleOrders.BO;
using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.SalesAndServices.SaleOrders.DA
{
    /// <summary>
	/// Created by 		: Do Van Tung 
	/// Created date 	: 06/03/2020 
	/// Bàn giao thực tế
	/// </summary>	
	public partial class DA_GOODSTransferOrderDT_Real
    {

        #region Methods			


        /// <summary>
        /// Nạp thông tin bàn giao thực tế
        /// </summary>
        /// <param name="objGOODSTransferOrderDT_Real">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref GOODSTransferOrderDT_Real objGOODSTransferOrderDT_Real, string strGOODSTransferOrderDetailID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colGOODSTransferOrderDetailID, strGOODSTransferOrderDetailID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objGOODSTransferOrderDT_Real = new GOODSTransferOrderDT_Real();
                    if (!Convert.IsDBNull(reader[GOODSTransferOrderDT_Real.colGOODSTransferOrderDetailID])) objGOODSTransferOrderDT_Real.GOODSTransferOrderDetailID = Convert.ToString(reader[GOODSTransferOrderDT_Real.colGOODSTransferOrderDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[GOODSTransferOrderDT_Real.colRealTransferID])) objGOODSTransferOrderDT_Real.RealTransferID = Convert.ToString(reader[GOODSTransferOrderDT_Real.colRealTransferID]).Trim();
                    if (!Convert.IsDBNull(reader[GOODSTransferOrderDT_Real.colProductID])) objGOODSTransferOrderDT_Real.ProductID = Convert.ToString(reader[GOODSTransferOrderDT_Real.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[GOODSTransferOrderDT_Real.colQUANTITTYTransfer])) objGOODSTransferOrderDT_Real.QUANTITTYTransfer = Convert.ToInt32(reader[GOODSTransferOrderDT_Real.colQUANTITTYTransfer]);
                    if (!Convert.IsDBNull(reader[GOODSTransferOrderDT_Real.colQuantityRealTransfer])) objGOODSTransferOrderDT_Real.QuantityRealTransfer = Convert.ToInt32(reader[GOODSTransferOrderDT_Real.colQuantityRealTransfer]);
                    if (!Convert.IsDBNull(reader[GOODSTransferOrderDT_Real.colUpdatedUser])) objGOODSTransferOrderDT_Real.UpdatedUser = Convert.ToString(reader[GOODSTransferOrderDT_Real.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[GOODSTransferOrderDT_Real.colUpdatedDate])) objGOODSTransferOrderDT_Real.UpdatedDate = Convert.ToDateTime(reader[GOODSTransferOrderDT_Real.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[GOODSTransferOrderDT_Real.colInStockStatusID])) objGOODSTransferOrderDT_Real.InStockStatusID = Convert.ToBoolean(reader[GOODSTransferOrderDT_Real.colInStockStatusID]);
                }
                else
                {
                    objGOODSTransferOrderDT_Real = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin bàn giao thực tế", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODSTransferOrderDT_Real -> LoadInfo", Globals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin bàn giao thực tế
        /// </summary>
        /// <param name="objGOODSTransferOrderDT_Real">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(GOODSTransferOrderDT_Real objGOODSTransferOrderDT_Real)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objGOODSTransferOrderDT_Real);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin bàn giao thực tế", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODSTransferOrderDT_Real -> Insert", Globals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin bàn giao thực tế
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODSTransferOrderDT_Real">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, GOODSTransferOrderDT_Real objGOODSTransferOrderDT_Real)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colGOODSTransferOrderDetailID, objGOODSTransferOrderDT_Real.GOODSTransferOrderDetailID);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colRealTransferID, objGOODSTransferOrderDT_Real.RealTransferID);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colProductID, objGOODSTransferOrderDT_Real.ProductID);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colQUANTITTYTransfer, objGOODSTransferOrderDT_Real.QUANTITTYTransfer);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colQuantityRealTransfer, objGOODSTransferOrderDT_Real.QuantityRealTransfer);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colInStockStatusID, objGOODSTransferOrderDT_Real.InStockStatusID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin bàn giao thực tế
        /// </summary>
        /// <param name="objGOODSTransferOrderDT_Real">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(GOODSTransferOrderDT_Real objGOODSTransferOrderDT_Real)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objGOODSTransferOrderDT_Real);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin bàn giao thực tế", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODSTransferOrderDT_Real -> Update", Globals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin bàn giao thực tế
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODSTransferOrderDT_Real">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, GOODSTransferOrderDT_Real objGOODSTransferOrderDT_Real)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colGOODSTransferOrderDetailID, objGOODSTransferOrderDT_Real.GOODSTransferOrderDetailID);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colRealTransferID, objGOODSTransferOrderDT_Real.RealTransferID);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colProductID, objGOODSTransferOrderDT_Real.ProductID);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colQUANTITTYTransfer, objGOODSTransferOrderDT_Real.QUANTITTYTransfer);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colQuantityRealTransfer, objGOODSTransferOrderDT_Real.QuantityRealTransfer);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colUpdatedUser, objGOODSTransferOrderDT_Real.UpdatedUser);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colInStockStatusID, objGOODSTransferOrderDT_Real.InStockStatusID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin bàn giao thực tế
        /// </summary>
        /// <param name="objGOODSTransferOrderDT_Real">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(GOODSTransferOrderDT_Real objGOODSTransferOrderDT_Real)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objGOODSTransferOrderDT_Real);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin bàn giao thực tế", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODSTransferOrderDT_Real -> Delete", Globals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin bàn giao thực tế
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objGOODSTransferOrderDT_Real">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, GOODSTransferOrderDT_Real objGOODSTransferOrderDT_Real)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + GOODSTransferOrderDT_Real.colGOODSTransferOrderDetailID, objGOODSTransferOrderDT_Real.GOODSTransferOrderDetailID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        #region Methods			

        /// <summary>
        /// Tìm kiếm thông tin bàn giao thực tế
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin bàn giao thực tế", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_GOODSTransferOrderDT_Real -> SearchData", Globals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion
        #endregion


        #region Constructor

        public DA_GOODSTransferOrderDT_Real()
        {
        }
        #endregion


        #region Stored Procedure Names
        public const String SP_ADD = "PM_GOODSTRANORDERDT_REAL_ADD";
        public const String SP_UPDATE = "PM_GOODSTRANORDERDT_REAL_UP";
        public const String SP_DELETE = "PM_GOODSTRANORDERDT_REAL_DEL";
        public const String SP_SELECT = "PM_GOODSTRANORDERDT_REAL_SEL";
        public const String SP_SEARCH = "PM_GOODSTRANORDERDT_REAL_SHR";
        #endregion

    }
}


