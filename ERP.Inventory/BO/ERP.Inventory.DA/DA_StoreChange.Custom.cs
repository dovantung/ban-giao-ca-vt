
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using System.Linq;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
    /// Created by 		: Nguyễn Linh Tuấn 
    /// Created date 	: 10/3/2012 
    /// Bảng xuất chuyển kho
    /// </summary>	
    public partial class DA_StoreChange
    {

        #region Methods

        /// <summary>
        /// Tìm kiếm thông tin bảng xuất chuyển kho
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_StoreChange.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin bảng xuất chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateStoreChange(BO.StoreChange objStoreChangeBO, bool bolIsUpdateAll)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {

                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_STORECHANGE_UPDATE");
                objIData.AddParameter("@StoreChangeID", objStoreChangeBO.StoreChangeID);
                objIData.AddParameter("@IsReceive", objStoreChangeBO.IsReceive);
                objIData.AddParameter("@ReceiveNote", objStoreChangeBO.ReceiveNote);
                objIData.AddParameter("@UserReceive", objStoreChangeBO.UserReceive);
                objIData.AddParameter("@InputVoucherID", objStoreChangeBO.InputVoucherID);
                objIData.AddParameter("@TransportVoucherID", objStoreChangeBO.TransportVoucherID);

                objIData.AddParameter("@ReceivedTransferUser", objStoreChangeBO.ReceivedTransferUser);
                objIData.AddParameter("@IsReceivedTransfer", objStoreChangeBO.IsReceivedTransfer);
                objIData.AddParameter("@IsReceivedCheckinByFinger", objStoreChangeBO.IsReceivedCheckinByFinger);
                objIData.AddParameter("@TransferedUser", objStoreChangeBO.TransferedUser);
                objIData.AddParameter("@IsTransfered", objStoreChangeBO.IsTransfered);
                objIData.AddParameter("@IsTransferedCheckinByFinger", objStoreChangeBO.IsTransferedCheckinByFinger);
                objIData.AddParameter("@IsSignReceive", objStoreChangeBO.IsSignReceive);
                objIData.AddParameter("@SignReceiveUer", objStoreChangeBO.SignReceiveUser);
                objIData.AddParameter("@SignReceiveNote", objStoreChangeBO.SignReceiveNote);

                objIData.AddParameter("@ToUser1", objStoreChangeBO.ToUser1);
                objIData.AddParameter("@ToUser2", objStoreChangeBO.ToUser2);
                objIData.AddParameter("@updateduser", objStoreChangeBO.UpdatedUser);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@InvoiceID", objStoreChangeBO.InvoiceID);
                objIData.AddParameter("@InvoiceSymbol", objStoreChangeBO.InvoiceSymbol);
                objIData.AddParameter("@IsUpdateAll", bolIsUpdateAll);
                //12/12/2017: Dang bo sung
                objIData.AddParameter("@ISSIGNRECEIVEDCHECKBYFINGER", objStoreChangeBO.IsSignReceivedCheckinByFinger);
                objIData.AddParameter("@ISREALRECEIVEDCHECKBYFINGER", objStoreChangeBO.IsReadReceivedCheckinByFinger);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi cập nhật phiếu chuyển kho.", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> UpdateStoreChange", InventoryGlobals.ModuleName);
                objIData.RollBackTransaction();
            }
            finally
            {
                objIData.Disconnect();
            }
            //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
            //objNotification.NotificationID = Guid.NewGuid().ToString();
            //objNotification.NotificationTypeID = 8;
            ////objNotification.TaskParameters = objStoreChangeBO.StoreChangeID;
            //objNotification.TaskParameters = "{\"ID\": \"" + objStoreChangeBO.StoreChangeID + "\"}";
            //objNotification.ClickTaskParameters = objStoreChangeBO.StoreChangeID;
            //objNotification.NotificationName = "Xuất chuyển kho";
            //objNotification.Content = "Từ kho " + objStoreChangeBO.FromStoreName + " -> " + objStoreChangeBO.ToStoreName + ". Nội dung: " + objStoreChangeBO.Content;
            //objNotification.CreatedUser = objStoreChangeBO.CreatedUser;
            //objNotification.CreatedDate = DateTime.Now;
            //ERP.Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
            //obj.UserName = objStoreChangeBO.ToUser1.Trim();
            //ERP.Notification.BO.Notification_User obj1 = new Notification.BO.Notification_User();
            //obj1.UserName = objStoreChangeBO.ToUser2.Trim();
            //ERP.Notification.BO.Notification_User obj2 = new Notification.BO.Notification_User();
            //obj2.UserName = objStoreChangeBO.TransferedUser;
            //objNotification.Notification_UserList = new List<Notification.BO.Notification_User>();
            //if (obj.UserName.Trim() != string.Empty)
            //    objNotification.Notification_UserList.Add(obj);
            //if (obj1.UserName.Trim() != string.Empty && !objNotification.Notification_UserList.Exists(o => o.UserName == obj1.UserName))
            //    objNotification.Notification_UserList.Add(obj1);
            //if (obj2.UserName.Trim() != string.Empty && !objNotification.Notification_UserList.Exists(o => o.UserName == obj2.UserName))
            //    objNotification.Notification_UserList.Add(obj2);
            //objNotification.StartDate = DateTime.Now;
            ////objNotification.LinkURL = System.Configuration.ConfigurationManager.AppSettings["WebHostURL"] + "/EOffice/Inform/Details/" + objEO_INFORM_COMMENT.INFormID;
            //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
            //ResultMessage objResultMessage1 = new ResultMessage();
            //objResultMessage1 = objDA_Notification.Broadcast(objNotification);
            return objResultMessage;
        }

        /// <summary>
        /// Lấy mã phiếu xuất chuyển kho
        /// </summary>
        public ResultMessage GetStoreChangeNewID(ref string strStoreChangeID, int intStoreID)
        {

            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                strStoreChangeID = CreateStoreChangeID(ref objResultMessage, objIData, intStoreID);
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi lấy mã xuất chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> GetStoreChangeNewID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm chuyển kho
        /// </summary>
        /// <param name="objResultMessage"></param>
        /// <param name="tblStoreChangeDetail"></param>
        /// <param name="objStoreChangeBO"></param>
        /// <returns></returns>
        public bool InsertStoreChange(ref ResultMessage objResultMessage, DataTable tblStoreChangeDetail, BO.StoreChange objStoreChangeBO, ref string strStoreChangeID, ref string strOutPutVoucherID, int intStoreChangeOrderTypeID = 0, int intCreateVATInvoice = 0)
        {
            //IData objIData = Data.CreateData();
            //try
            //{
            //    objIData.Connect();
            //    objIData.BeginTransaction();

            //    //if (objStoreChangeBO.TotalQuantity <= 0 & objStoreChangeBO.IsInputFromOrder)
            //    //{
            //    //    if (!SaveUserNote(ref objResultMessage, objIData, tblStoreChangeDetail, objStoreChangeBO))
            //    //    {
            //    //        objIData.RollBackTransaction();
            //    //        return false;
            //    //    }
            //    //}

            //    //Tạo phiếu xuất chuyển kho
            //    if (!CreateOuputInputVoucher(ref objResultMessage, objIData, ref objStoreChangeBO))
            //    {
            //        objIData.RollBackTransaction();
            //        return false;
            //    }
            //    //Đăng bổ sung lấy mã phiếu xuất
            //    if (!string.IsNullOrWhiteSpace(objStoreChangeBO.OutputVoucherBO.OutputVoucherID))
            //        strOutPutVoucherID = objStoreChangeBO.OutputVoucherBO.OutputVoucherID;

            //    if (objStoreChangeBO.IsCreateInOutVoucher)
            //    {
            //        if (!CreateInOutVoucher(ref objResultMessage, objIData, objStoreChangeBO))
            //        {
            //            objIData.RollBackTransaction();
            //            return false;
            //        }
            //    }

            //    //Thêm chi tiết xuất chuyển kho
            //    if (!AddStoreChangeDetail1(ref objResultMessage, objIData, tblStoreChangeDetail, objStoreChangeBO))
            //    {

            //        objIData.RollBackTransaction();
            //        return false;
            //    }

            //    //Lưu ghi chú và trạng thái yc chuyển kho
            //    if (objStoreChangeBO.IsInputFromOrder)
            //    {
            //        if (!SaveUserNote(ref objResultMessage, objIData, tblStoreChangeDetail, objStoreChangeBO))
            //        {
            //            objIData.RollBackTransaction();
            //            return false;
            //        }
            //        if (!UpdateStoreChangeOrderStatus(ref objResultMessage, objIData, objStoreChangeBO.StoreChangeOrderID))
            //        {
            //            objIData.RollBackTransaction();
            //            return false;
            //        }
            //    }
            //    strStoreChangeID = objStoreChangeBO.StoreChangeID;

            //    /*
            //    if (objStoreChangeBO.StoreChangeType.IsPrintVAT)
            //    {
            //        if (!UpdatePrintVATStatus(objIData, objStoreChangeBO.OutputVoucherBO.OutputVoucherID, objStoreChangeBO.InVoiceID, objStoreChangeBO.InputVoucherBO.InVoiceSymbol, objStoreChangeBO.OriginateStoreID, objStoreChangeBO.UserCreate))
            //            return false;
            //    }
            //    */
            //    if (intStoreChangeOrderTypeID > 0 && intCreateVATInvoice == 2)
            //    {
            //        PrintVAT.DA.PrintVAT.DA_VAT_Invoice objDA_VAT_Invoice = new PrintVAT.DA.PrintVAT.DA_VAT_Invoice();
            //        objResultMessage = objDA_VAT_Invoice.CreateVATInvoiceStoreChange(objIData, objStoreChangeBO.InputVoucherBO.InputVoucherID, objStoreChangeBO.OutputVoucherBO.OutputVoucherID, intStoreChangeOrderTypeID, objStoreChangeBO.OutputVoucherBO.IsSInvoice);
            //        if (objResultMessage.IsError)
            //        {
            //            objIData.RollBackTransaction();
            //            new SystemError(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "StoreChangeDAO -> AddStoreChange -> CreateVATInvoiceStoreChange", InventoryGlobals.ModuleName);
            //            ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "StoreChangeDAO -> AddStoreChange -> CreateVATInvoiceStoreChange", InventoryGlobals.ModuleName);
            //            return false;
            //        }
            //    }
            //    objIData.CommitTransaction();


            //}
            //catch (Exception objEx)
            //{
            //    string strMessage = "Lỗi lưu xuất chuyển kho";
            //    objIData.RollBackTransaction();
            //    objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
            //    new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> AddStoreChange", "ERP.Inventory.DA.DA_StoreChange");
            //    ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "StoreChangeDAO -> AddStoreChange -> CreateVATInvoiceStoreChange", InventoryGlobals.ModuleName);

            //}
            //finally
            //{
            //    objIData.Disconnect();
            //}

            //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
            //List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
            //if (objStoreChangeBO.UserNotifyData != null && objStoreChangeBO.UserNotifyData.Rows.Count > 0)
            //{
            //    foreach (DataRow item in objStoreChangeBO.UserNotifyData.Rows)
            //    {
            //        Notification.BO.Notification_User objNotification_User = new Notification.BO.Notification_User();
            //        objNotification_User.UserName = item["UserName"].ToString();
            //        if (!objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
            //        {
            //            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
            //            {
            //                bool bolResult = false;
            //                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, objNotification_User.UserName);
            //                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
            //                    objNotification_UserList.Add(objNotification_User);
            //            }
            //            else
            //                objNotification_UserList.Add(objNotification_User);
            //        }
            //    }
            //    ERP.Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
            //    obj.UserName = objStoreChangeBO.ToUser1.Trim();
            //    ERP.Notification.BO.Notification_User obj1 = new Notification.BO.Notification_User();
            //    obj1.UserName = objStoreChangeBO.ToUser2.Trim();
            //    ERP.Notification.BO.Notification_User obj2 = new Notification.BO.Notification_User();
            //    obj2.UserName = objStoreChangeBO.TransferedUser;
            //    objNotification.Notification_UserList = objNotification_UserList;
            //    if (obj.UserName.Trim() != string.Empty)
            //        objNotification.Notification_UserList.Add(obj);
            //    if (obj1.UserName.Trim() != string.Empty && !objNotification.Notification_UserList.Exists(o => o.UserName == obj1.UserName))
            //        objNotification.Notification_UserList.Add(obj1);
            //    if (obj2.UserName.Trim() != string.Empty && !objNotification.Notification_UserList.Exists(o => o.UserName == obj2.UserName))
            //        objNotification.Notification_UserList.Add(obj2);
            //}
            //objNotification.NotificationID = Guid.NewGuid().ToString();
            //objNotification.NotificationTypeID = 8;
            //objNotification.ClickTaskParameters = objStoreChangeBO.StoreChangeID;
            //objNotification.TaskParameters = "{\"ID\": \"" + objStoreChangeBO.StoreChangeID + "\"}";
            //objNotification.NotificationName = "Xuất chuyển kho";
            //objNotification.Content = "Từ kho " + objStoreChangeBO.FromStoreName + " -> " + objStoreChangeBO.ToStoreName + ". Nội dung: " + objStoreChangeBO.Content;
            //objNotification.CreatedUser = objStoreChangeBO.CreatedUser;
            //objNotification.CreatedDate = DateTime.Now;

            //objNotification.StartDate = DateTime.Now;
            ////objNotification.LinkURL = System.Configuration.ConfigurationManager.AppSettings["WebHostURL"] + "/EOffice/Inform/Details/" + objEO_INFORM_COMMENT.INFormID;
            //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
            //ResultMessage objResultMessage1 = new ResultMessage();
            //objResultMessage1 = objDA_Notification.Broadcast(objNotification);
            return false;
        }

        #region PM_SCORDERDETAIL_UpdateNote (Lưu ghi chú chi tiết yêu cầu chuyển kho)
        private bool SaveUserNote(ref ResultMessage objResultMessage, IData objIData, DataTable tblStoreChangeDetail, BO.StoreChange objStoreChangeBO)
        {
            DataRow[] arrRow = tblStoreChangeDetail.Select("UserNote is not null");
            foreach (DataRow objRow in arrRow)
            {
                String strStoreChangeOrderDetailID = Convert.ToString(objRow["StoreChangeOrderDetailID"]).Trim();
                String strNote = Convert.ToString(objRow["UserNote"]).Trim();
                String strIMEI = Convert.ToString(objRow["IMEI"]).Trim();
                decimal decOrderQuantity = Convert.ToDecimal(objRow["OrderQuantity"]);
                decimal decStoreChangeQuantity = Convert.ToDecimal(objRow["Quantity"]);
                bool bolIsStoreChange = Convert.ToBoolean(objRow["IsSelect"]);
                if (!SaveUserNote(ref objResultMessage, objIData, strStoreChangeOrderDetailID, strIMEI, strNote,
                    decOrderQuantity, decStoreChangeQuantity, bolIsStoreChange, objStoreChangeBO))
                    return false;
            }
            return true;
        }

        private bool SaveUserNote(ref ResultMessage objResultMessage, IData objIData, String strStoreChangeOrderDetailID, String strIMEI, String strNote,
            decimal decOrderQuantity, decimal decStoreChangeQuantity, bool bolIsStoreChange, BO.StoreChange objStoreChangeBO)
        {

            if (strNote.Length > 2000) strNote = strNote.Substring(0, 2000);
            try
            {
                objIData.CreateNewStoredProcedure("PM_SCORDERDETAIL_UpdateNote");
                objIData.AddParameter("@STORECHANGEORDERID", objStoreChangeBO.StoreChangeOrderID);
                objIData.AddParameter("@STORECHANGEORDERDETAILID", strStoreChangeOrderDetailID);
                objIData.AddParameter("@IMEI", strIMEI);
                objIData.AddParameter("@Note", strNote);
                objIData.AddParameter("@OrderQuantity", decOrderQuantity);
                objIData.AddParameter("@StoreChangeQuantity", decStoreChangeQuantity);
                objIData.AddParameter("@IsStoreChange", bolIsStoreChange);
                objIData.AddParameter("@StoreChangeID", objStoreChangeBO.StoreChangeID);
                objIData.AddParameter("@UserName", objStoreChangeBO.CreatedUser);
                objIData.ExecNonQuery();
                return true;
            }
            catch (Exception objEx)
            {

                string strMessage = "Lỗi lưu ghi chú";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Update, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange -> SaveUserNote", "PM_SCORDERDETAIL_UpdateNote");
                return false;
            }
        }

        /// <summary>
        /// Cập nhật trạng thái yêu cầu chuyển kho khi nhập hàng từ đơn hàng
        /// </summary>
        /// <param name="objResultMessage"></param>
        /// <param name="objIData"></param>
        /// <param name="strStoreChangeOrderID"></param>
        /// <returns></returns>
        private bool UpdateStoreChangeOrderStatus(ref ResultMessage objResultMessage, IData objIData, String strStoreChangeOrderID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_STORECHANGE_UPDSTATUS");
                objIData.AddParameter("@STORECHANGEORDERID", strStoreChangeOrderID);
                objIData.ExecNonQuery();
                return true;
            }
            catch (Exception objEx)
            {

                string strMessage = "Lỗi cập nhật trạng thái của yêu cầu chuyên kho khi nhập hàng";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Update, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange-> UpdateStoreChangeOrderStatus", "PM_STORECHANGE_UPDSTATUS");
                return false;
            }
        }
        #endregion

        #region Tạo phiếu Xuất, Nhập , Chuyển kho
        public bool CreateOuputInputVoucher(ref ResultMessage objResultMessage, IData objIData, ref BO.StoreChange objStoreChangeBO)
        {
            objStoreChangeBO.StoreChangeID = CreateStoreChangeID(ref objResultMessage, objIData, objStoreChangeBO.CreatedStoreID);

            try
            {
                DA_OutputVoucher objOutputVoucherDAO = new DA_OutputVoucher();
                objOutputVoucherDAO.Insert(objIData, objStoreChangeBO.OutputVoucherBO);
            }
            catch (Exception objEx)
            {

                string strMessage = "Lỗi khi thêm mới một phiếu xuất hàng";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateOuputInputVoucher", "PM_OUTPUTVOUCHER_ADD");
                return false;
            }

            try
            {
                objStoreChangeBO.InputVoucherBO.Content = "Chuyển từ " + objStoreChangeBO.FromStoreName + " - MPX: " + objStoreChangeBO.OutputVoucherBO.OutputVoucherID + " - Nội dung: " + objStoreChangeBO.Content;
                DA_InputVoucher objInputVoucherDAO = new DA_InputVoucher();
                objInputVoucherDAO.objLogObject = this.objLogObject;
                objInputVoucherDAO.Insert(objIData, objStoreChangeBO.InputVoucherBO);
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi khi thêm mới một phiếu nhập hàng";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateOuputInputVoucher", "PM_INPUTVOUCHER_ADD");
                return false;
            }

            try
            {
                if (objStoreChangeBO.Content.Length > 400)
                    objStoreChangeBO.OutputVoucherBO.OutputContent = "Chuyển đến " + objStoreChangeBO.ToStoreName + " - MPN: " + objStoreChangeBO.InputVoucherBO.InputVoucherID + " - Nội dung: " + objStoreChangeBO.Content.Substring(0, 400);
                else
                    objStoreChangeBO.OutputVoucherBO.OutputContent = "Chuyển đến " + objStoreChangeBO.ToStoreName + " - MPN: " + objStoreChangeBO.InputVoucherBO.InputVoucherID + " - Nội dung: " + objStoreChangeBO.Content;

                objIData.CreateNewStoredProcedure("ERP.PM_OUTPUTVOUCHER_UPDCONTENT");
                objIData.AddParameter("@OutputVoucherID", objStoreChangeBO.OutputVoucherBO.OutputVoucherID);
                objIData.AddParameter("@OutputContent", objStoreChangeBO.OutputVoucherBO.OutputContent);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi cập nhật nội dung phiếu xuất";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateOuputInputVoucher", "PM_SHIPPINGCOST_SEL");
                return false;
            }


            // Tính chi phí van chuyển của phiếu chuyển kho này
            try
            {
                objIData.CreateNewStoredProcedure("ERP.MD_SHIPPINGCOST_SEL");
                objIData.AddParameter("@TransportTypeID", objStoreChangeBO.TransportTypeID);
                objIData.AddParameter("@FromStoreID", objStoreChangeBO.FromStoreID);
                objIData.AddParameter("@ToStoreID", objStoreChangeBO.ToStoreID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objStoreChangeBO.TotalShippingCost = (objStoreChangeBO.TotalWeight * Convert.ToDecimal(reader["SHIPPINGCOST"]));
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi khi lấy chi phí vận chuyển giữa 2 kho";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateOuputInputVoucher", "PM_SHIPPINGCOST_SEL");
                return false;
            }

            try
            {

                objIData.CreateNewStoredProcedure("PM_STORECHANGE_ADD");
                objIData.AddParameter("@STORECHANGEID", objStoreChangeBO.StoreChangeID);
                objIData.AddParameter("@STORECHANGEORDERID", objStoreChangeBO.StoreChangeOrderID);
                objIData.AddParameter("@FROMSTOREID", objStoreChangeBO.FromStoreID);
                objIData.AddParameter("@TOSTOREID", objStoreChangeBO.ToStoreID);
                objIData.AddParameter("@TRANSPORTTYPEID", objStoreChangeBO.TransportTypeID);
                objIData.AddParameter("@STORECHANGETYPEID", objStoreChangeBO.StoreChangeTypeID);
                objIData.AddParameter("@CONTENT", objStoreChangeBO.Content);
                objIData.AddParameter("@USERCREATE", objStoreChangeBO.CreatedUser);
                objIData.AddParameter("@STORECHANGEDATE", objStoreChangeBO.StoreChangeDate);
                objIData.AddParameter("@OUTPUTVOUCHERID", objStoreChangeBO.OutputVoucherBO.OutputVoucherID);
                objIData.AddParameter("@INPUTVOUCHERID", objStoreChangeBO.InputVoucherBO.InputVoucherID);
                objIData.AddParameter("@STORECHANGEUSER", objStoreChangeBO.StoreChangeUser);
                objIData.AddParameter("@CreatedStoreID", objStoreChangeBO.CreatedStoreID);
                objIData.AddParameter("@IsNew", objStoreChangeBO.IsNew);
                objIData.AddParameter("@INSTOCKSTATUSID", objStoreChangeBO.InStockStatusID);
                objIData.AddParameter("@InvoiceID", objStoreChangeBO.InvoiceID);
                objIData.AddParameter("@TotalQuantity", objStoreChangeBO.TotalQuantity);
                objIData.AddParameter("@TotalPacking", objStoreChangeBO.TotalPacking);
                if (objStoreChangeBO.ToUser1 != "-1") objIData.AddParameter("@ToUser1", objStoreChangeBO.ToUser1);
                if (objStoreChangeBO.ToUser2 != "-1") objIData.AddParameter("@ToUser2", objStoreChangeBO.ToUser2);
                objIData.AddParameter("@ISRECEIVE", objStoreChangeBO.IsReceive);
                if (objStoreChangeBO.InVoucherBO != null)
                    objIData.AddParameter("@InVoucherID", objStoreChangeBO.InVoucherBO.VoucherID);
                else
                    objIData.AddParameter("@InVoucherID", string.Empty);
                if (objStoreChangeBO.OutVoucherBO != null)
                    objIData.AddParameter("@OutVoucherID", objStoreChangeBO.OutVoucherBO.VoucherID);
                else
                    objIData.AddParameter("@OutVoucherID", string.Empty);
                objIData.AddParameter("@TotalWeight", objStoreChangeBO.TotalWeight);
                objIData.AddParameter("@TotalSize", objStoreChangeBO.TotalSize);
                objIData.AddParameter("@IsUrgent", objStoreChangeBO.IsUrgent);
                objIData.AddParameter("@TotalShippingCost", objStoreChangeBO.TotalShippingCost);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@TransportVoucherID", objStoreChangeBO.TransportVoucherID);
                objIData.AddParameter("@InvoiceSymbol", objStoreChangeBO.InvoiceSymbol);

                objIData.AddParameter("@IsCheckRealInput", 0);

                objIData.AddParameter("@ISTRANSFERED", objStoreChangeBO.IsTransfered);
                objIData.AddParameter("@TRANSFEREDUSER", objStoreChangeBO.TransferedUser);

                objIData.AddParameter("@ISRECEIVEDTRANSFER", objStoreChangeBO.IsReceivedTransfer);
                objIData.AddParameter("@RECEIVEDTRANSFERUSER", objStoreChangeBO.ReceivedTransferUser);

                objIData.AddParameter("@ISSIGNRECEIVE", objStoreChangeBO.IsSignReceive);
                objIData.AddParameter("@SIGNRECEIVEUSER", objStoreChangeBO.SignReceiveUser);
                objIData.AddParameter("@SIGNRECEIVENOTE", objStoreChangeBO.SignReceiveNote);

                objIData.AddParameter("@USERRECEIVE", objStoreChangeBO.UserReceive);
                objIData.AddParameter("@RECEIVENOTE", objStoreChangeBO.ReceiveNote);
                objIData.AddParameter("@CCDC", objStoreChangeBO.IsCCDC);

                objIData.ExecNonQuery();
                HungDT_GhiLog(objStoreChangeBO.StoreChangeID + "---" + objStoreChangeBO.StoreChangeDetailData.Rows.Count.ToString());
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi thêm thông tin chuyển kho";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateOuputInputVoucher", "PM_STORECHANGE_Add");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Tạo mã phiếu nhập
        /// </summary>
        /// <param name="objResultMessage"></param>
        /// <param name="objIData"></param>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        public string CreateInputVoucherID(ref ResultMessage objResultMessage, IData objIData, int intStoreID)
        {
            string strResult = "";
            if (intStoreID < 0) intStoreID = 0;
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strResult = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi khi tạo mã phiếu nhập";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateInputVoucherID", "PM_InputVoucher_NewID");
                return string.Empty;
            }
            return strResult;
        }

        /// <summary>
        /// Tạo mã phiếu xuất
        /// </summary>
        /// <param name="objResultMessage"></param>
        /// <param name="objIData"></param>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        public string CreateStoreChangeID(ref ResultMessage objResultMessage, IData objIData, int intStoreID)
        {
            string strResult = "";
            if (intStoreID < 0)
                intStoreID = 0;
            try
            {
                objIData.CreateNewStoredProcedure("PM_STORECHANGE_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strResult = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi khi tạo mã chuyển kho";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateStoreChangeID", "PM_STORECHANGE_NEWID");
                return string.Empty;
            }
            return strResult;
        }

        #endregion

        #region Tạo chi tiết cho phiếu xuất chuyển kho và nhập chuyển kho
        private bool AddStoreChangeDetail(IData objIData, DataTable tblStoreChangeDetail, BO.StoreChange objStoreChangeBO)
        {
            DataRow[] arrRow = tblStoreChangeDetail.Select("IsSelect = 1");
            DA_StoreChangeDetail objDA_StoreChangeDetail = new DA_StoreChangeDetail();
            foreach (DataRow objRow in arrRow)
            {
                String strProductID = Convert.ToString(objRow["ProductID"]).Trim();
                decimal decQuantity = Convert.ToDecimal(objRow["Quantity"]);
                DateTime dtEndWarrantyTime = Convert.ToDateTime(objRow["EndWarrantyDate"]);
                String strIMEI = Convert.ToString(objRow["IMEI"]).Trim();
                bool bolIsHasWarranty = Convert.ToBoolean(objRow["IsHasWarranty"]);
                decimal decSalePrice = Convert.ToDecimal(objRow["Price"]);
                int intPackingNumber = Convert.ToInt32(objRow["PackingNumber"]);
                decimal decCostPrice = Convert.ToDecimal(objRow["CostPrice"]);
                int intVAT = Convert.ToInt32(objRow["VAT"]);
                int intVATPercent = Convert.ToInt32(objRow["VATPercent"]);
                DateTime dtInputVoucherDate = Convert.ToDateTime(objRow["InputVoucherDate"]);
                int intInputCustomerID = Convert.ToInt32(objRow["InputCustomerID"]);
                String strInputUserID = Convert.ToString(objRow["InputUserID"]).Trim();
                int intInputTypeID = Convert.ToInt32(objRow["InputTypeID"]);
                bool bolIsShowProductInput = Convert.ToBoolean(objRow["IsShowProduct"]);
                String strNote = Convert.ToString(objRow["Note"]).Trim();
                bool bolIsReturnProduct = Convert.ToBoolean(objRow["IsReturnProduct"]);
                String strStoreChangeOrderDetailID = Convert.ToString(objRow["StoreChangeOrderDetailID"]).Trim();
                decimal decOrderQuantity = Convert.ToDecimal(objRow["OrderQuantity"]);
                String strUserNote = Convert.ToString(objRow["UserNote"]).Trim();
                decimal decOutputPrice = Convert.ToDecimal(objRow["OutputPrice"]);
                decimal decInputPrice = Convert.ToDecimal(objRow["InputPrice"]);
                int intOutputVAT = Convert.ToInt32(objRow["OutputVAT"]);
                int intInputVAT = Convert.ToInt32(objRow["InputVAT"]);
                if (strUserNote.Length > 2000) strUserNote = strUserNote.Substring(0, 2000);
                if (!AddStoreChangeDetail(objIData, strProductID, strIMEI, decQuantity, dtEndWarrantyTime, bolIsHasWarranty,
                    intPackingNumber, intVAT, dtInputVoucherDate, intInputCustomerID, objStoreChangeBO.InputVoucherBO.InputTypeID, objStoreChangeBO.OutputTypeID, bolIsReturnProduct,
                    bolIsShowProductInput, intVATPercent, strNote, strStoreChangeOrderDetailID,
                    decOrderQuantity, strUserNote, decOutputPrice, decInputPrice, intOutputVAT, intInputVAT, decCostPrice, objStoreChangeBO))
                {
                    return false;
                }
            }
            return true;
        }
        #endregion
        #region Tạo phiếu chi/ thu tương ứng
        //Tạo phiếu thu và phiếu chi tương ứng
        public bool CreateInOutVoucher(ref ResultMessage objResultMessage, IData objIData, BO.StoreChange objStoreChangeBO)
        {
            try
            {

                ERP.SalesAndServices.Payment.DA.DA_Voucher objVoucherDAO = new SalesAndServices.Payment.DA.DA_Voucher();
                //tạo phiếu thu
                objStoreChangeBO.InVoucherBO.VoucherConcern = objStoreChangeBO.OutputVoucherBO.OutputVoucherID;
                objStoreChangeBO.InVoucherBO.Content = "MPX:" + objStoreChangeBO.OutputVoucherBO.OutputVoucherID;
                objStoreChangeBO.InVoucherBO.InvoiceDate = DateTime.Now;
                objStoreChangeBO.InVoucherBO.CreatedUser = objStoreChangeBO.CreatedUser;
                objVoucherDAO.CreateVoucher(objIData, objStoreChangeBO.InVoucherBO);

                //tạo phiếu chi
                objStoreChangeBO.OutVoucherBO.VoucherConcern = objStoreChangeBO.InputVoucherBO.InputVoucherID;
                objStoreChangeBO.OutVoucherBO.Content = "MPN:" + objStoreChangeBO.InputVoucherBO.InputVoucherID;
                objStoreChangeBO.OutVoucherBO.InvoiceDate = DateTime.Now;
                objStoreChangeBO.InVoucherBO.CreatedUser = objStoreChangeBO.CreatedUser;
                objVoucherDAO.CreateVoucher(objIData, objStoreChangeBO.OutVoucherBO);
                //cập nhật phiếu chuyển kho
                if (!UpdateVoucher(ref objResultMessage, objIData, objStoreChangeBO))
                    return false;
                return true;
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi khi tạo phiếu thu chi";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> CreateInOutVoucher", "PM_OUTPUTVOUCHER_ADD");
                return false;
            }
        }

        public bool UpdateVoucher(ref ResultMessage objResultMessage, IData objIData, BO.StoreChange objStoreChangeBO)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_STORECHANGE_UpdateVoucher");
                objIData.AddParameter("@STORECHANGEID", objStoreChangeBO.StoreChangeID);
                objIData.AddParameter("@INVOUCHERID", objStoreChangeBO.InVoucherBO.VoucherID);
                objIData.AddParameter("@OUTVOUCHERID", objStoreChangeBO.OutVoucherBO.VoucherID);
                objIData.ExecNonQuery();
                return true;

            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi cập nhật phiếu chuyển kho";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Update, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange-> CreateInOutVoucher", "PM_STORECHANGE_UpdateVoucher");
                return false;
            }
        }
        #endregion
        public bool AddStoreChangeDetail(IData objIData, string strProductID, string strIMEI, decimal decQuantity, DateTime dtEndWarrantyDate,
          bool bolIsHasWarranty, int intPackingNumber, int intVAT, DateTime dtInputVoucherDate, int intInputCustomerID,
            int intInputTypeID, int intOutputTypeID, bool bolIsReturnProduct, bool bolIsShowProductInput, int intVATPercent,
          String strNote, String strStoreChangeOrderDetailID, decimal decOrderQuantity,
          String strUserNote, decimal decOutputPrice, decimal decInputPrice, int intOutputVAT, int intInputVAT, decimal decCostPrice, BO.StoreChange objStoreChangeBO)
        {

            try
            {

                objIData.CreateNewStoredProcedure("PM_StoreChangeDetail_Add1");
                objIData.AddParameter("@InputVoucherID", objStoreChangeBO.InputVoucherBO.InputVoucherID);
                objIData.AddParameter("@OutputVoucherID", objStoreChangeBO.OutputVoucherBO.OutputVoucherID);
                objIData.AddParameter("@StoreID", objStoreChangeBO.FromStoreID);
                objIData.AddParameter("@StoreID_In", objStoreChangeBO.ToStoreID);
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@IMEI", strIMEI);
                objIData.AddParameter("@Quantity", decQuantity);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@PackingNumber", intPackingNumber);
                objIData.AddParameter("@VATPercent", intVATPercent);
                objIData.AddParameter("@Note", strNote);
                objIData.AddParameter("@IsCheckRealInput", objStoreChangeBO.InputVoucherBO.IsCheckRealInput);
                objIData.AddParameter("@StoreChangeOrderDetailID", strStoreChangeOrderDetailID);
                objIData.AddParameter("@StoreChangeID", objStoreChangeBO.StoreChangeID);
                objIData.AddParameter("@OrderQuantity", decOrderQuantity);
                objIData.AddParameter("@UserNote", strUserNote);
                objIData.AddParameter("@CreatedStoreID", objStoreChangeBO.CreatedStoreID);

                objIData.AddParameter("@CreatedUser", objStoreChangeBO.CreatedUser);
                objIData.AddParameter("@OutputPrice", decOutputPrice);
                objIData.AddParameter("@OutputVAT", intOutputVAT);
                objIData.AddParameter("@InputPrice", decInputPrice);
                objIData.AddParameter("@CostPrice", decCostPrice);
                objIData.AddParameter("@InputVAT", intInputVAT);
                objIData.AddParameter("@IsHasWarranty", bolIsHasWarranty);
                objIData.AddParameter("@IsNew", objStoreChangeBO.IsNew);
                objIData.AddParameter("@INSTOCKSTATUDID", objStoreChangeBO.InStockStatusID);
                objIData.AddParameter("@InputVoucherDate", dtInputVoucherDate);
                objIData.AddParameter("@InputCustomerID", intInputCustomerID);
                objIData.AddParameter("@InputTypeID", intInputTypeID);

                objIData.AddParameter("@IsShowProduct", bolIsShowProductInput);
                objIData.AddParameter("@IsReturnProduct", bolIsReturnProduct);
                objIData.AddParameter("@EndWarrantyDate", dtEndWarrantyDate);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw objEx;
            }
            return true;

        }


        /// <summary>
        /// Lấy thông tin hàng hóa có kích thước tối đa của phiếu chuyển kho
        /// </summary>
        /// <param name="lstStoreChangeID"></param>
        /// <returns></returns>
        public ResultMessage GetProductInfoMaxSize(ref DataTable dtbDataResult, string strStoreChangeIDList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_StoreChange_TransferedList");
                objIData.AddParameter("@StoreChangeIDList", strStoreChangeIDList);
                dtbDataResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi lấy thông tin hàng hóa có kích thước tối đa của phiếu chuyển kho";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange -> GetProductInfo", "ERP.Enventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage LoadStoreChangeDetail(ref DataTable dtbData, string strStoreChangeID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_STORECHANGEDETAIL_LIST");
                objIData.AddParameter("@StoreChangeID", strStoreChangeID);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi lấy thông tin chi tiết xuất chuyển kho";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.LoadInfo, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> LoadInfo", "PM_STORECHANGEDETAIL_LIST");
                return null;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateStoreChangeList(string strUsername, List<BO.StoreChange> lstStoreChange)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                foreach (BO.StoreChange item in lstStoreChange)
                {
                    objIData.CreateNewStoredProcedure("PM_STORECHANGE_Update");
                    objIData.AddParameter("@StoreChangeID", item.StoreChangeID);
                    objIData.AddParameter("@IsReceive", item.IsReceive);
                    objIData.AddParameter("@ReceiveNote", item.ReceiveNote);
                    objIData.AddParameter("@UserReceive", strUsername);
                    objIData.AddParameter("@InputVoucherID", item.InputVoucherID);
                    objIData.AddParameter("@IsTransfered", item.IsTransfered);
                    objIData.AddParameter("@TransferedUser", item.TransferedUser);
                    objIData.AddParameter("@TransportVoucherID", item.TransportVoucherID);
                    objIData.AddParameter("@TransportVoucherUser", strUsername);
                    objIData.AddParameter("@IsTransferedCheckinByFinger", item.IsTransferedCheckinByFinger);
                    objIData.AddParameter("@IsReceivedCheckinByFinger", item.IsReceivedCheckinByFinger);
                    objIData.AddParameter("@IsSignReceive", item.IsSignReceive);
                    objIData.AddParameter("@SignReceiveUer", item.SignReceiveUser);
                    objIData.AddParameter("@SignReceiveNote", item.SignReceiveNote);
                    objIData.AddParameter("@IsReceivedTransfer", item.IsReceivedTransfer);
                    objIData.AddParameter("@ReceivedTransferUser", item.ReceivedTransferUser);
                    objIData.AddParameter("@UpdatedUser", strUsername);
                    objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                    objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                    objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                    objIData.AddParameter("@InvoiceID", item.InvoiceID);
                    objIData.AddParameter("@InvoiceSymbol", item.InvoiceSymbol);
                    objIData.AddParameter("@IsUpdateList", 1);
                    objIData.ExecNonQuery();
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi Quản lý phiếu chuyển kho - Cập nhật trạng thái phiếu";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Update, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange-> UpdateVoucherList", "PM_STORECHANGE_Update");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;

        }

        /// <summary>
        /// Lấy thông tin tồn kho chính và kho trưng bày
        /// </summary>
        /// <param name="lstStoreChangeID"></param>
        /// <returns></returns>
        public ResultMessage GetStoreChange_GetInstock(ref DataSet dsDataResult, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.PM_STORECHANGE_GETINSTOCK");
                objIData.AddParameter(objKeywords);
                dsDataResult = objIData.ExecStoreToDataSet("v_Out", "V_Out1");
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi Lấy thông tin tồn kho chính và kho trưng bày";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange -> GetStoreChange_GetInstock", "ERP.Enventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Lấy thông tin tồn kho chính mà không có ở kho trưng bày
        /// </summary>
        /// <param name="lstStoreChangeID"></param>
        /// <returns></returns>
        public ResultMessage GetStoreChange_GetInstock_1(ref DataTable dtDataResult, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.PM_STORECHANGE_GETINSTOCK_1");
                objIData.AddParameter(objKeywords);
                dtDataResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi Lấy thông tin tồn kho chính mà không có ở kho trưng bày";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange -> GetStoreChange_GetInstock_1", "ERP.Enventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetStoreChange_BySCOID(ref DataTable dtbResult, string strStoreChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_STORECHANGE_GETBYSCOID");
                objIData.AddParameter("@STORECHANGEORDERID", strStoreChangeOrderID.Trim());
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi Lấy thông tin phiếu xuất chuyển";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange -> GetStoreChange_BySCOID", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetStoreChangeOrder_GetCode(ref DataTable dtbResult, string strStoreChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_STORECHANGEORDER_GETCODE");
                objIData.AddParameter("@STORECHANGEORDERID", strStoreChangeOrderID.Trim());
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi Lấy mã code in biên bản giao hàng";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange -> GetStoreChangeOrder_GetCode", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetStoreChangeRPT_GetInfo(ref DataTable dtbResult, string strStoreChangeID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_RPT_STORECHANGE_PRT_GETINFO");
                objIData.AddParameter("@STORECHANGEID", strStoreChangeID.Trim());
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi Lấy thông tin in phiếu xuất";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange -> GetStoreChangeRPT_GetInfo", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetStoreChangeRPT_GetData(ref DataTable dtbResult, string strStoreChangeID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_RPT_STORECHANGE_PRINT");
                objIData.AddParameter("@STORECHANGEID", strStoreChangeID.Trim());
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi Lấy danh sách hàng chuyển in phiếu xuất";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChange -> GetStoreChangeRPT_GetData", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage Report_GetOutputVoucherTranferData(ref DataTable dtbData, string strStoreChangeID, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_RPT_OUTPUTVOUCHERTRANSFER");
                objIData.AddParameter("@STORECHANGEID", strStoreChangeID);
                objIData.AddParameter("@UserName", strUserName);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi thông tin phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> Report_GetOutputVoucherTranferData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public Library.WebCore.ResultMessage GetFormTypebyStoreChangeOrderType(ref DataTable dtbFormType, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("VAT_FORMTYPE_SCOTYPE_SRHALL");
                objIData.AddParameter(objKeywords);
                dtbFormType = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi nạp chi tiết đơn hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> GetFormTypebyStoreChangeOrderType", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private bool AddStoreChangeDetail1(ref ResultMessage objResultMessage, IData objIData, DataTable tblStoreChangeDetail, BO.StoreChange objStoreChangeBO)
        {
            DataRow[] arrRow = tblStoreChangeDetail.Select("IsSelect = 1");
            //if (!arrRow.Any())
            //{

            //    objIData.RollBackTransaction();
            //    return false;
            //}
            DataTable dtbStoreChage = CreateStoreChangeDetail();
            DA_StoreChangeDetail objDA_StoreChangeDetail = new DA_StoreChangeDetail();
            foreach (DataRow objRow in arrRow)
            {
                String strProductID = Convert.ToString(objRow["ProductID"]).Trim();
                decimal decQuantity = Convert.ToDecimal(objRow["Quantity"]);
                DateTime dtEndWarrantyTime = Convert.ToDateTime(objRow["EndWarrantyDate"]);
                String strIMEI = Convert.ToString(objRow["IMEI"]).Trim();
                bool bolIsHasWarranty = Convert.ToBoolean(objRow["IsHasWarranty"]);
                decimal decSalePrice = Convert.ToDecimal(objRow["Price"]);
                int intPackingNumber = Convert.ToInt32(objRow["PackingNumber"]);
                decimal decCostPrice = Convert.ToDecimal(objRow["CostPrice"]);
                int intVAT = Convert.ToInt32(objRow["VAT"]);
                int intVATPercent = Convert.ToInt32(objRow["VATPercent"]);
                DateTime dtInputVoucherDate = Convert.ToDateTime(objRow["InputVoucherDate"]);
                int intInputCustomerID = Convert.ToInt32(objRow["InputCustomerID"]);
                String strInputUserID = Convert.ToString(objRow["InputUserID"]).Trim();
                int intInputTypeID = Convert.ToInt32(objRow["InputTypeID"]);
                bool bolIsShowProductInput = Convert.ToBoolean(objRow["IsShowProduct"]);
                String strNote = Convert.ToString(objRow["Note"]).Trim();
                bool bolIsReturnProduct = Convert.ToBoolean(objRow["IsReturnProduct"]);
                String strStoreChangeOrderDetailID = Convert.ToString(objRow["StoreChangeOrderDetailID"]).Trim();
                decimal decOrderQuantity = Convert.ToDecimal(objRow["OrderQuantity"]);
                String strUserNote = Convert.ToString(objRow["UserNote"]).Trim();
                decimal decOutputPrice = Convert.ToDecimal(objRow["OutputPrice"]);
                decimal decInputPrice = Convert.ToDecimal(objRow["InputPrice"]);
                int intOutputVAT = Convert.ToInt32(objRow["OutputVAT"]);
                int intInputVAT = Convert.ToInt32(objRow["InputVAT"]);
                if (strUserNote.Length > 2000) strUserNote = strUserNote.Substring(0, 2000);
                if (!InsertDataStoreChange(dtbStoreChage, strProductID, strIMEI, decQuantity, dtEndWarrantyTime, bolIsHasWarranty,
                    intPackingNumber, intVAT, dtInputVoucherDate, intInputCustomerID, objStoreChangeBO.InputVoucherBO.InputTypeID, objStoreChangeBO.OutputTypeID, bolIsReturnProduct,
                    bolIsShowProductInput, intVATPercent, strNote, strStoreChangeOrderDetailID,
                    decOrderQuantity, strUserNote, decOutputPrice, decInputPrice, intOutputVAT, intInputVAT, decCostPrice, objStoreChangeBO))
                {
                    return false;
                }
            }

            if (dtbStoreChage == null)
                return false;
            if (dtbStoreChage.Rows.Count < 1)
                return false;
            if (dtbStoreChage != null && dtbStoreChage.Rows.Count > 0)
            {
                string strXml = CreateXMLFromDataTable(dtbStoreChage, "STORECHANGEDETAIL");
                if (!AddStoreChangeDetailxml(ref objResultMessage, objIData, strXml, objStoreChangeBO.InputVoucherBO.InputVoucherID, objStoreChangeBO.StoreChangeID, objStoreChangeBO.OutputVoucherBO.OutputVoucherID))
                {

                    return false;
                }
                HungDT_GhiLog(objStoreChangeBO.StoreChangeID + ">>>" + dtbStoreChage.Rows.Count.ToString() + ">>>" + strXml.Length.ToString());
                //    }
                //    iCount += 1000;
                //}
            }

            return true;
        }

        public bool InsertFromTempTable(ref ResultMessage objResultMessage, IData objIData, string strINPUTVOUCHERID, string strSTORECHANGEID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_STORECHANGEDETAIL_ADD5");
                objIData.AddParameter("@INPUTVOUCHERID", strINPUTVOUCHERID);
                objIData.AddParameter("@STORECHANGEID", strSTORECHANGEID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                ErrorLog.Add(objIData, objEx.Message, objEx.ToString(), "DA_InputVoucher -> InsertData", InventoryGlobals.ModuleName);

                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Update, "Lỗi thêm thông tin", objEx);
                return false;
            }
            return true;

        }
        public bool AddStoreChangeDetailxml(ref ResultMessage objResultMessage, IData objIData, string xml, string strINPUTVOUCHERID, string strSTORECHANGEID, string strOUTPUTVOUCHERID)
        {
            try
            {

                objIData.CreateNewStoredProcedure("PM_STORECHANGEDETAIL_ADD6");
                objIData.AddParameter("@xml", xml, Library.DataAccess.Globals.DATATYPE.CLOB);
                objIData.AddParameter("@INPUTVOUCHERID", strINPUTVOUCHERID);
                objIData.AddParameter("@STORECHANGEID", strSTORECHANGEID);
                objIData.AddParameter("@OUTPUTVOUCHERID", strOUTPUTVOUCHERID);
                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                HungDT_GhiLog("Lỗi ADD detail" + strSTORECHANGEID + " " + ex.Message);
                //IData objIData2 = Data.CreateData();
                //objIData2.Connect();
                //objIData2.CreateNewStoredProcedure("A_GHILOG_STORE_ADD");
                //objIData2.AddParameter("@CONTENT", xml, Library.DataAccess.Globals.DATATYPE.CLOB);
                //objIData2.ExecNonQuery();
                //ErrorLog.Add(objIData2, ex.Message, ex.ToString(), "DA_InputVoucher -> InsertData", InventoryGlobals.ModuleName);
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Update, "Lỗi thêm thông tin1111", ex);
                objIData.RollBackTransaction();
                return false;
            }
            return true;

        }
        public ResultMessage DeleteTemp()
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                DeleteTemp(objIData);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết imei phiếu xuất tạm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> DeleteTemp", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public void DeleteTemp(IData objIData)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_STORECHANGEDETAIL_TMP_DEL");
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        public bool BulkCopy(DataTable data, string toTableName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                BulkCopy(objIData, data, toTableName);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi đẩy dữ liệu dùng bullcopy", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChange -> BulkCopy", InventoryGlobals.ModuleName);
                return false;
            }
            finally
            {
                objIData.Disconnect();
            }
            return true;
        }
        public void BulkCopy(IData objIData, DataTable data, string toTableName)
        {
            try
            {
                //objIData.BulkCopy(data, toTableName);
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        private DataTable CreateStoreChangeDetail()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("INPUTVOUCHERID", typeof(string));
            dt.Columns.Add("OUTPUTVOUCHERID", typeof(string));
            dt.Columns.Add("STOREID", typeof(int));
            dt.Columns.Add("STOREID_IN", typeof(int));
            dt.Columns.Add("PRODUCTID", typeof(string));
            dt.Columns.Add("IMEI", typeof(string));
            dt.Columns.Add("QUANTITY", typeof(decimal));
            dt.Columns.Add("OUTPUTTYPEID", typeof(int));
            dt.Columns.Add("PACKINGNUMBER", typeof(int));
            dt.Columns.Add("VATPERCENT", typeof(int));
            dt.Columns.Add("NOTE", typeof(string));
            dt.Columns.Add("ISCHECKREALINPUT", typeof(int));
            dt.Columns.Add("STORECHANGEORDERDETAILID", typeof(string));
            dt.Columns.Add("STORECHANGEID", typeof(string));
            dt.Columns.Add("ORDERQUANTITY", typeof(decimal));
            dt.Columns.Add("USERNOTE", typeof(string));
            dt.Columns.Add("CREATEDSTOREID", typeof(int));
            dt.Columns.Add("CREATEDUSER", typeof(string));
            dt.Columns.Add("OUTPUTPRICE", typeof(decimal));
            dt.Columns.Add("OUTPUTVAT", typeof(int));
            dt.Columns.Add("INPUTPRICE", typeof(decimal));
            dt.Columns.Add("COSTPRICE", typeof(decimal));
            dt.Columns.Add("INPUTVAT", typeof(int));
            dt.Columns.Add("ISHASWARRANTY", typeof(int));
            dt.Columns.Add("ISNEW", typeof(int));
            dt.Columns.Add("INPUTVOUCHERDATE", typeof(string));
            dt.Columns.Add("INPUTCUSTOMERID", typeof(int));
            dt.Columns.Add("INPUTTYPEID", typeof(int));
            dt.Columns.Add("ISSHOWPRODUCT", typeof(int));
            dt.Columns.Add("ISRETURNPRODUCT", typeof(int));
            dt.Columns.Add("ENDWARRANTYDATE", typeof(string));
            dt.Columns.Add("INSTOCKSTATUSID", typeof(int));
            return dt;
        }

        public bool InsertDataStoreChange(DataTable dt, string strProductID, string strIMEI, decimal decQuantity, DateTime dtEndWarrantyDate,
          bool bolIsHasWarranty, int intPackingNumber, int intVAT, DateTime dtInputVoucherDate, int intInputCustomerID,
            int intInputTypeID, int intOutputTypeID, bool bolIsReturnProduct, bool bolIsShowProductInput, int intVATPercent,
          String strNote, String strStoreChangeOrderDetailID, decimal decOrderQuantity,
          String strUserNote, decimal decOutputPrice, decimal decInputPrice, int intOutputVAT, int intInputVAT, decimal decCostPrice, BO.StoreChange objStoreChangeBO)
        {
            try
            {
                dt.Rows.Add(
                    objStoreChangeBO.InputVoucherBO.InputVoucherID
                    , objStoreChangeBO.OutputVoucherBO.OutputVoucherID
                    , objStoreChangeBO.FromStoreID
                    , objStoreChangeBO.ToStoreID
                    , strProductID
                    , strIMEI
                    , decQuantity
                    , intOutputTypeID
                    , intPackingNumber
                    , intVATPercent
                    , strNote
                    , objStoreChangeBO.InputVoucherBO.IsCheckRealInput ? 1 : 0
                    , strStoreChangeOrderDetailID
                    , objStoreChangeBO.StoreChangeID
                    , decOrderQuantity
                    , strUserNote
                    , objStoreChangeBO.CreatedStoreID
                    , objStoreChangeBO.CreatedUser
                    , decOutputPrice
                    , intOutputVAT
                    , decInputPrice
                    , decCostPrice
                    , intInputVAT
                    , bolIsHasWarranty ? 1 : 0
                    , objStoreChangeBO.IsNew ? 1 : 0
                    , dtInputVoucherDate == null ? "" : dtInputVoucherDate.AddMinutes(5).ToString("yyyy/MM/dd HH:mm:ss")//, dtInputVoucherDate.AddMinutes(5)//
                    , intInputCustomerID
                    , intInputTypeID
                    , bolIsShowProductInput ? 1 : 0
                    , bolIsReturnProduct ? 1 : 0
                    , dtEndWarrantyDate == null ? "" : dtEndWarrantyDate.ToString("yyyy/MM/dd HH:mm:ss")//, dtEndWarrantyDate == null ? DateTime.Now : dtEndWarrantyDate//
                    , objStoreChangeBO.InStockStatusID
                    );
            }
            catch (Exception objEx)
            {
                throw objEx;
            }
            return true;
        }

        private string CreateXMLFromDataTable(DataTable dtb, string tableName)
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(tableName))
            {
                return string.Empty;
            }
            var sb = new System.Text.StringBuilder();
            var xtx = System.Xml.XmlWriter.Create(sb);
            xtx.WriteStartDocument();
            xtx.WriteStartElement("", tableName.ToUpper() + "LIST", "");
            foreach (DataRow dr in dtb.Rows)
            {
                xtx.WriteStartElement("", tableName.ToUpper(), "");
                foreach (DataColumn dc in dtb.Columns)
                {
                    xtx.WriteAttributeString(dc.ColumnName.ToUpper(), dr[dc.ColumnName].ToString());
                }
                xtx.WriteEndElement();
            }
            xtx.WriteEndElement();
            xtx.Flush();
            xtx.Close();
            return sb.ToString();
        }

        private void HungDT_GhiLog(string content)
        {
            IData objIData2 = Data.CreateData();
            try
            {
                objIData2.Connect();
                objIData2.CreateNewStoredProcedure("A_GHILOG_STORECHANGE_ADD");
                objIData2.AddParameter("@CONTENT", content);
                objIData2.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData2.RollBackTransaction();
                throw (objEx);
            }
            finally
            {
                objIData2.Disconnect();
            }
        }

        /// <summary>
        /// Thêm chuyển kho
        /// </summary>
        /// <param name="objResultMessage"></param>
        /// <param name="tblStoreChangeDetail"></param>
        /// <param name="objStoreChangeBO"></param>
        /// <returns></returns>
        public bool InsertStoreChangeNew(ref ResultMessage objResultMessage, DataTable tblStoreChangeDetail, BO.StoreChange objStoreChangeBO, ref string strStoreChangeID, ref string strOutPutVoucherID, ref List<string> lstStoreChangeID, ref List<string> lstVATInvoiceID, int intStoreChangeOrderTypeID = 0, int intCreateVATInvoice = 0, int intPrepareVAT = 15)
        {
            IData objIData = Data.CreateData();
            lstStoreChangeID = new List<string>();
            lstVATInvoiceID = new List<string>();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                List<DataTable> lstDataStoreChangeDetail = PrepareStoreChange(tblStoreChangeDetail, intPrepareVAT);

                foreach (DataTable tblDataSource in lstDataStoreChangeDetail)
                {
                    //if(tblDataSource.Rows.Count > 15)
                    //{
                    //    objResultMessage.IsError = true;
                    //    objResultMessage.Message = objResultMessage.MessageDetail = "Lỗi detail > 15 dòng";
                    //    objIData.RollBackTransaction();
                    //    return false;
                    //}
                    DataRow[] arrRow = tblDataSource.Select("IsSelect = 1");
                    if (arrRow.Any())
                    {
                        var lstData = tblDataSource.AsEnumerable();
                        decimal decTotalQuantity = 0;
                        decimal decTotalAmountBF_Out = 0;
                        decimal decTotalVAT_Out = 0;
                        decimal decTotalAmount_Out = 0;
                        decimal decTotalAmountBF_In = 0;
                        decimal decTotalVAT_In = 0;
                        decimal decTotalAmount_In = 0;
                        GetTotalValues(tblDataSource, ref decTotalQuantity, ref decTotalAmountBF_Out, ref decTotalVAT_Out, ref decTotalAmount_Out, ref decTotalAmountBF_In, ref decTotalVAT_In, ref decTotalAmount_In);

                        #region cap nhat lai so tien tron phieu nhap
                        objStoreChangeBO.InputVoucherBO.TotalAmountBFT = decTotalAmountBF_In;
                        objStoreChangeBO.InputVoucherBO.TotalVAT = decTotalVAT_In;
                        objStoreChangeBO.InputVoucherBO.TotalAmount = decTotalAmount_In;
                        #endregion

                        #region cap nhat lai so tien tron phieu xuat
                        objStoreChangeBO.OutputVoucherBO.TotalAmountBFT = decTotalAmountBF_Out;
                        objStoreChangeBO.OutputVoucherBO.TotalVAT = decTotalVAT_Out;
                        objStoreChangeBO.OutputVoucherBO.TotalAmount = decTotalAmount_Out;

                        #endregion
                        if (objStoreChangeBO.IsCreateInOutVoucher)
                        {
                            #region InVoucherBO
                            objStoreChangeBO.InVoucherBO.TotalMoney = decTotalAmount_Out;
                            objStoreChangeBO.InVoucherBO.TotalLiquidate = decTotalAmount_Out;
                            objStoreChangeBO.InVoucherBO.Debt = decTotalAmount_Out;
                            #endregion
                            #region OutVoucherBO
                            objStoreChangeBO.OutVoucherBO.TotalMoney = decTotalAmount_In;
                            objStoreChangeBO.OutVoucherBO.TotalLiquidate = decTotalAmount_In;
                            objStoreChangeBO.OutVoucherBO.Debt = decTotalAmount_In;
                            #endregion
                        }

                        //Tạo phiếu xuất chuyển kho
                        if (!CreateOuputInputVoucher(ref objResultMessage, objIData, ref objStoreChangeBO))
                        {
                            objIData.RollBackTransaction();
                            return false;
                        }
                        //Đăng bổ sung lấy mã phiếu xuất
                        if (!string.IsNullOrWhiteSpace(objStoreChangeBO.OutputVoucherBO.OutputVoucherID))
                            strOutPutVoucherID = objStoreChangeBO.OutputVoucherBO.OutputVoucherID;

                        if (objStoreChangeBO.IsCreateInOutVoucher)
                        {
                            if (!CreateInOutVoucher(ref objResultMessage, objIData, objStoreChangeBO))
                            {
                                objIData.RollBackTransaction();
                                return false;
                            }
                        }

                        //if (tblDataSource.Rows.Count > 200)
                        //{ //Thêm chi tiết xuất chuyển kho - anh Lộc
                        //if(tblDataSource.Rows.Count < 1)
                        //{
                        //    HungDT_GhiLog("HUNGDT check tblDataSource 1478  " + arrRow.Count().ToString());
                        //    objIData.RollBackTransaction();
                        //    return false;
                        //}
                        if (!AddStoreChangeDetail1(ref objResultMessage, objIData, tblDataSource, objStoreChangeBO))
                        {
                            objIData.RollBackTransaction();
                            return false;
                        }
                        //}
                        //else
                        //{
                        //    //Thêm chi tiết xuất chuyển kho - anh Tuấn
                        //    if (!AddStoreChangeDetail(objIData, tblDataSource, objStoreChangeBO))
                        //    {
                        //        objIData.RollBackTransaction();
                        //        return false;
                        //    }
                        //}

                        //Lưu ghi chú và trạng thái yc chuyển kho
                        if (objStoreChangeBO.IsInputFromOrder)
                        {
                            if (!SaveUserNote(ref objResultMessage, objIData, tblDataSource, objStoreChangeBO))
                            {
                                objIData.RollBackTransaction();
                                return false;
                            }
                            if (!UpdateStoreChangeOrderStatus(ref objResultMessage, objIData, objStoreChangeBO.StoreChangeOrderID))
                            {
                                objIData.RollBackTransaction();
                                return false;
                            }
                        }
                        strStoreChangeID = objStoreChangeBO.StoreChangeID;
                        lstStoreChangeID.Add(strStoreChangeID);
                        if (intStoreChangeOrderTypeID > 0 && intCreateVATInvoice == 2)
                        {
                            string strVATInvoceID = string.Empty;
                            PrintVAT.DA.PrintVAT.DA_VAT_Invoice objDA_VAT_Invoice = new PrintVAT.DA.PrintVAT.DA_VAT_Invoice();
                            objResultMessage = objDA_VAT_Invoice.CreateVATInvoiceStoreChange(objIData, objStoreChangeBO.InputVoucherBO.InputVoucherID, objStoreChangeBO.OutputVoucherBO.OutputVoucherID, intStoreChangeOrderTypeID, ref strVATInvoceID, objStoreChangeBO.OutputVoucherBO.IsSInvoice);
                            if (objResultMessage.IsError)
                            {
                                objIData.RollBackTransaction();
                                new SystemError(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "StoreChangeDAO -> AddStoreChange -> CreateVATInvoiceStoreChange", InventoryGlobals.ModuleName);
                                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "StoreChangeDAO -> AddStoreChange -> CreateVATInvoiceStoreChange", InventoryGlobals.ModuleName);
                                return false;
                            }
                            lstVATInvoiceID.Add(strVATInvoceID);
                        }
                    }
                }

                objIData.CommitTransaction();


            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi lưu xuất chuyển kho";
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.Insert, objEx.Message, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> AddStoreChange", "ERP.Inventory.DA.DA_StoreChange");
                ErrorLog.Add(objIData, strMessage, objEx.ToString(), "StoreChangeDAO -> AddStoreChange", "ERP.Inventory.DA.DA_StoreChange");
            }
            finally
            {
                objIData.Disconnect();
            }
            return true;
        }

        private List<DataTable> PrepareStoreChange(DataTable tblStoreChangeDetail, int intPrepareVAT)
        {
            try
            {
                DataTable dtbTemp = tblStoreChangeDetail.Clone();
                List<DataTable> lstDataStoreChangeDetail = new List<DataTable>();
                var lstDataTable = tblStoreChangeDetail.Select("IsSelect = 1").GroupBy(x => new
                {
                    keyProductID = x.Field<string>("PRODUCTID"),
                    keyOutputPrice = x.Field<decimal>("OutputPrice"),
                    keyRownumber = Convert.ToInt32(x.Field<string>("NUMBERLINE"))
                });
                if (lstDataTable != null)
                {
                    int intTotalNumberLine = lstDataTable.Sum(x => x.Key.keyRownumber);
                    if (intTotalNumberLine > intPrepareVAT)
                    {
                        int intIndex = 0;
                        foreach (var item in lstDataTable)
                        {
                            intIndex += item.Key.keyRownumber;
                            if (intIndex <= intPrepareVAT)
                            {
                                DataRow[] drData = tblStoreChangeDetail.Copy().Select("PRODUCTID='" + item.Key.keyProductID + "' and OutputPrice = " + item.Key.keyOutputPrice + "");
                                if (drData != null && drData.Count() > 0)
                                {
                                    foreach (DataRow dr in drData)
                                    {
                                        dtbTemp.ImportRow(dr);
                                    }
                                }
                            }
                            else
                            {
                                lstDataStoreChangeDetail.Add(dtbTemp);
                                dtbTemp = tblStoreChangeDetail.Clone();
                                intIndex = 0;
                                intIndex += item.Key.keyRownumber;
                                DataRow[] drData = tblStoreChangeDetail.Copy().Select("PRODUCTID='" + item.Key.keyProductID + "' and OutputPrice = " + item.Key.keyOutputPrice + "");
                                if (drData != null && drData.Count() > 0)
                                {
                                    foreach (DataRow dr in drData)
                                    {
                                        dtbTemp.ImportRow(dr);
                                    }
                                }
                            }
                        }
                        if (dtbTemp != null && dtbTemp.Rows.Count > 0)
                            lstDataStoreChangeDetail.Add(dtbTemp);
                    }
                    else
                    {
                        lstDataStoreChangeDetail.Add(tblStoreChangeDetail);
                    }
                }
                return lstDataStoreChangeDetail;
            }
            catch (Exception objEx)
            {
                throw (objEx);
            }
        }


        private void GetTotalValues(DataTable dtbStoreChangeDetail, ref decimal decTotalQuantity, ref decimal decTotalAmountBF_Out,
            ref decimal decTotalVAT_Out, ref decimal decTotalAmount_Out, ref decimal decTotalAmountBF_In,
            ref decimal decTotalVAT_In, ref decimal decTotalAmount_In)
        {
            decTotalQuantity = 0;
            decTotalAmountBF_Out = 0;
            decTotalVAT_Out = 0;
            decTotalAmount_Out = 0;

            decTotalAmountBF_In = 0;
            decTotalVAT_In = 0;
            decTotalAmount_In = 0;
            DataRow[] arrRow = dtbStoreChangeDetail.Select("ISSELECT= 1");
            foreach (DataRow objRow in arrRow)
            {
                decTotalQuantity += Convert.ToDecimal(objRow["Quantity"]);
                decimal decTotalCost_Out = Convert.ToDecimal(objRow["Quantity"]) * Convert.ToDecimal(objRow["OutputPrice"]);
                decimal decTotalCost_In = Convert.ToDecimal(objRow["Quantity"]) * Convert.ToDecimal(objRow["InputPrice"]);
                decimal decOutputVAT = Convert.ToDecimal(objRow["OutputVAT"]) * Convert.ToDecimal(objRow["VATPercent"]) / 10000;
                decimal decInputVAT = Convert.ToDecimal(objRow["InputVAT"]) * Convert.ToDecimal(objRow["VATPercent"]) / 10000;
                decTotalAmountBF_Out += decTotalCost_Out;
                decTotalVAT_Out += decTotalCost_Out * decOutputVAT;
                decTotalAmount_Out += decTotalCost_Out * (1 + decOutputVAT);

                decTotalAmountBF_In += decTotalCost_In;
                decTotalVAT_In += decTotalCost_In * decInputVAT;
                decTotalAmount_In += decTotalCost_In * (1 + decInputVAT);
            }
        }

        #endregion


    }
}
