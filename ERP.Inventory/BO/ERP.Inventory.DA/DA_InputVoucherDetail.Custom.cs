
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
    /// Created by 		: Nguyễn Linh Tuấn 
    /// Created date 	: 8/13/2012 
    /// Chi tiết phiếu nhập
    /// 
    /// Modifield by    : Đặng Thế Hùng
    /// Date            : 26/06/2018
    /// Bổ sung thêm ProductStatusName
    /// </summary>	
    public partial class DA_InputVoucherDetail
    {

        #region Methods

        /// <summary>
        /// Tìm kiếm thông tin chi tiết phiếu nhập
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InputVoucherDetail.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chi tiết phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherDetail -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Nạp thông tin Chi tiết phiếu nhập 
        /// StoreName: PM_GETLISTBYINPUTVOUCHERID
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public List<InputVoucherDetail> GetListByInputVoucherID(string strInputVoucherID)
        {
            List<InputVoucherDetail> objInputVoucherDetailList = null;
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InputVoucherDetail.SP_GETLISTBYINPUTVOUCHERID);
                objIData.AddParameter("@" + InputVoucher.colInputVoucherID, strInputVoucherID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    InputVoucherDetail objInputVoucherDetail = new InputVoucherDetail();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputVoucherDetailID])) objInputVoucherDetail.InputVoucherDetailID = Convert.ToString(reader[InputVoucherDetail.colInputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputVoucherID])) objInputVoucherDetail.InputVoucherID = Convert.ToString(reader[InputVoucherDetail.colInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colProductID])) objInputVoucherDetail.ProductID = Convert.ToString(reader[InputVoucherDetail.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader["ProductName"])) objInputVoucherDetail.ProductName = Convert.ToString(reader["ProductName"]).Trim();
                    if (!Convert.IsDBNull(reader["QUANTITYUNITNAME"])) objInputVoucherDetail.QuantityUnitName = Convert.ToString(reader["QUANTITYUNITNAME"]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colQuantity])) objInputVoucherDetail.Quantity = Convert.ToDecimal(reader[InputVoucherDetail.colQuantity]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colVAT])) objInputVoucherDetail.VAT = Convert.ToInt32(reader[InputVoucherDetail.colVAT]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colVATPercent])) objInputVoucherDetail.VATPercent = Convert.ToInt32(reader[InputVoucherDetail.colVATPercent]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputPrice])) objInputVoucherDetail.InputPrice = Convert.ToDecimal(reader[InputVoucherDetail.colInputPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colORIGINALInputPrice])) objInputVoucherDetail.ORIGINALInputPrice = Convert.ToDecimal(reader[InputVoucherDetail.colORIGINALInputPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colCostPrice])) objInputVoucherDetail.CostPrice = Convert.ToDecimal(reader[InputVoucherDetail.colCostPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstPrice])) objInputVoucherDetail.FirstPrice = Convert.ToDecimal(reader[InputVoucherDetail.colFirstPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colReturnFee])) objInputVoucherDetail.ReturnFee = Convert.ToDecimal(reader[InputVoucherDetail.colReturnFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colAdjustPrice])) objInputVoucherDetail.AdjustPrice = Convert.ToDecimal(reader[InputVoucherDetail.colAdjustPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colAdjustPriceContent])) objInputVoucherDetail.AdjustPriceContent = Convert.ToString(reader[InputVoucherDetail.colAdjustPriceContent]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colAdjustPriceUser])) objInputVoucherDetail.AdjustPriceUser = Convert.ToString(reader[InputVoucherDetail.colAdjustPriceUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputDate])) objInputVoucherDetail.InputDate = Convert.ToDateTime(reader[InputVoucherDetail.colInputDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colENDWarrantyDate])) objInputVoucherDetail.ENDWarrantyDate = Convert.ToDateTime(reader[InputVoucherDetail.colENDWarrantyDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colProductIONDate])) objInputVoucherDetail.ProductIONDate = Convert.ToDateTime(reader[InputVoucherDetail.colProductIONDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colProductChangeDate])) objInputVoucherDetail.ProductChangeDate = Convert.ToDateTime(reader[InputVoucherDetail.colProductChangeDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colReturnDate])) objInputVoucherDetail.ReturnDate = Convert.ToDateTime(reader[InputVoucherDetail.colReturnDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colChangeToOLDDate])) objInputVoucherDetail.ChangeToOLDDate = Convert.ToDateTime(reader[InputVoucherDetail.colChangeToOLDDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInputDate])) objInputVoucherDetail.FirstInputDate = Convert.ToDateTime(reader[InputVoucherDetail.colFirstInputDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInVoiceDate])) objInputVoucherDetail.FirstInVoiceDate = Convert.ToDateTime(reader[InputVoucherDetail.colFirstInVoiceDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstCustomerID])) objInputVoucherDetail.FirstCustomerID = Convert.ToInt32(reader[InputVoucherDetail.colFirstCustomerID]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInputTypeID])) objInputVoucherDetail.FirstInputTypeID = Convert.ToDecimal(reader[InputVoucherDetail.colFirstInputTypeID]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInputVoucherID])) objInputVoucherDetail.FirstInputVoucherID = Convert.ToString(reader[InputVoucherDetail.colFirstInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colFirstInputVoucherDetailID])) objInputVoucherDetail.FirstInputVoucherDetailID = Convert.ToString(reader[InputVoucherDetail.colFirstInputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsNew])) objInputVoucherDetail.IsNew = Convert.ToBoolean(reader[InputVoucherDetail.colIsNew]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsShowProduct])) objInputVoucherDetail.IsShowProduct = Convert.ToBoolean(reader[InputVoucherDetail.colIsShowProduct]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsReturnProduct])) objInputVoucherDetail.IsReturnProduct = Convert.ToBoolean(reader[InputVoucherDetail.colIsReturnProduct]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsReturnWithFee])) objInputVoucherDetail.IsReturnWithFee = Convert.ToBoolean(reader[InputVoucherDetail.colIsReturnWithFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsCheckRealInput])) objInputVoucherDetail.IsCheckRealInput = Convert.ToBoolean(reader[InputVoucherDetail.colIsCheckRealInput]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colISProductChange])) objInputVoucherDetail.ISProductChange = Convert.ToBoolean(reader[InputVoucherDetail.colISProductChange]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colISAddInStock])) objInputVoucherDetail.ISAddInStock = Convert.ToBoolean(reader[InputVoucherDetail.colISAddInStock]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colCreatedStoreID])) objInputVoucherDetail.CreatedStoreID = Convert.ToInt32(reader[InputVoucherDetail.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colInputStoreID])) objInputVoucherDetail.InputStoreID = Convert.ToInt32(reader[InputVoucherDetail.colInputStoreID]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colCreatedUser])) objInputVoucherDetail.CreatedUser = Convert.ToString(reader[InputVoucherDetail.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colCreatedDate])) objInputVoucherDetail.CreatedDate = Convert.ToDateTime(reader[InputVoucherDetail.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colUpdatedUser])) objInputVoucherDetail.UpdatedUser = Convert.ToString(reader[InputVoucherDetail.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colUpdatedDate])) objInputVoucherDetail.UpdatedDate = Convert.ToDateTime(reader[InputVoucherDetail.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colIsDeleted])) objInputVoucherDetail.IsDeleted = Convert.ToBoolean(reader[InputVoucherDetail.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colDeletedUser])) objInputVoucherDetail.DeletedUser = Convert.ToString(reader[InputVoucherDetail.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colDeletedDate])) objInputVoucherDetail.DeletedDate = Convert.ToDateTime(reader[InputVoucherDetail.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colTaxedFee])) objInputVoucherDetail.TaxedFee = Convert.ToDecimal(reader[InputVoucherDetail.colTaxedFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colPurchaseFee])) objInputVoucherDetail.PurchaseFee = Convert.ToDecimal(reader[InputVoucherDetail.colPurchaseFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colOrderIndex])) objInputVoucherDetail.OrderIndex = Convert.ToInt32(reader[InputVoucherDetail.colOrderIndex]);
                    if (!Convert.IsDBNull(reader[InputVoucherDetail.colProductStatusName])) objInputVoucherDetail.ProductStatusName = Convert.ToString(reader[InputVoucherDetail.colProductStatusName]).Trim();
                    //if (!Convert.Is)
                    //Nguyen Van Tai bo sung
                    try
                    {
                        if (!Convert.IsDBNull(reader["IsRequestImei"])) objInputVoucherDetail.IsRequestIMEI = Convert.ToBoolean(reader["IsRequestImei"]);
                    }
                    catch { }
                    //End
                    objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * (objInputVoucherDetail.InputPrice * (1 + (objInputVoucherDetail.VAT * objInputVoucherDetail.VATPercent) / Convert.ToDecimal(10000)));
                    if (objInputVoucherDetailList == null)
                        objInputVoucherDetailList = new List<InputVoucherDetail>();
                    objInputVoucherDetailList.Add(objInputVoucherDetail);
                }
            }
            catch (Exception objEx)
            {
                throw objEx;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objInputVoucherDetailList;
        }
        #endregion


    }
}
