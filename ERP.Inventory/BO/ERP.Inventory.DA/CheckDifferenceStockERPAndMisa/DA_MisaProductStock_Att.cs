﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using MisaProductStock_Att = ERP.Inventory.BO.CheckDifferenceStockERPAndMisa.MisaProductStock_Att;
#endregion

namespace ERP.Inventory.DA.CheckDifferenceStockERPAndMisa
{
    public class DA_MisaProductStock_Att
    {
        public DA_MisaProductStock_Att(){}
    
        public ResultMessage LoadInfo(ref MisaProductStock_Att objMisaProductStock_Att, string strMISAPRODUCTSTOCKID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + objMisaProductStock_Att.MISAPRODUCTSTOCKID, strMISAPRODUCTSTOCKID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    objMisaProductStock_Att = new MisaProductStock_Att();
                    if (!Convert.IsDBNull(reader[MisaProductStock_Att.colMISAPRODUCTSTOCKID]))  
                        objMisaProductStock_Att.MISAPRODUCTSTOCKID = Convert.ToString(reader[MisaProductStock_Att.colMISAPRODUCTSTOCKID]).Trim();
                    if (!Convert.IsDBNull(reader[MisaProductStock_Att.colDESCRIPTION]))
                        objMisaProductStock_Att.DESCRIPTION = Convert.ToString(reader[MisaProductStock_Att.colMISAPRODUCTSTOCKID]).Trim();
                    if (!Convert.IsDBNull(reader[MisaProductStock_Att.colCREATEDUSER]))
                        objMisaProductStock_Att.CREATEDUSER = Convert.ToString(reader[MisaProductStock_Att.colCREATEDUSER]);
                    if (!Convert.IsDBNull(reader[MisaProductStock_Att.colCREATEDDATE]))
                        objMisaProductStock_Att.CREATEDDATE = Convert.ToDateTime(reader[MisaProductStock_Att.colCREATEDDATE]);
                    if (!Convert.IsDBNull(reader[MisaProductStock_Att.colISDELETED]))
                        objMisaProductStock_Att.ISDELETED = Convert.ToBoolean(reader[MisaProductStock_Att.colISDELETED]);
                    if (!Convert.IsDBNull(reader[MisaProductStock_Att.colDELETEDUSER]))
                        objMisaProductStock_Att.DELETEDUSER = Convert.ToString(reader[MisaProductStock_Att.colDELETEDUSER]);
                    if (!Convert.IsDBNull(reader[MisaProductStock_Att.colDELETEDDATE]))
                        objMisaProductStock_Att.DELETEDDATE = Convert.ToDateTime(reader[MisaProductStock_Att.colDELETEDDATE]);
                    if (!Convert.IsDBNull(reader[MisaProductStock_Att.colFILEID]))
                        objMisaProductStock_Att.FILEID = Convert.ToString(reader[MisaProductStock_Att.colFILEID]).Trim();
                }
                else
                {
                    objMisaProductStock_Att = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin lịch sử chênh lệch", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_MisaProductStock_Att -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        
        public ResultMessage Insert(MisaProductStock_Att objMisaProductStock_Att)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                Insert(objIData, objMisaProductStock_Att);
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi cập nhật thông tin lịch sử chênh lệch", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_MisaProductStock_Att -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


       
        public void Insert(IData objIData, MisaProductStock_Att objMisaProductStock_Att)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + MisaProductStock_Att.colDESCRIPTION, objMisaProductStock_Att.DESCRIPTION);
                objIData.AddParameter("@" + MisaProductStock_Att.colCREATEDUSER, objMisaProductStock_Att.CREATEDUSER);
                objIData.AddParameter("@" + MisaProductStock_Att.colFILEID, objMisaProductStock_Att.FILEID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin lịch sử chênh lệch", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_MisaProductStock_Att -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        #region Stored Procedure Names

        public const String SP_ADD = "PN_MISAPRODUCTSTOCK_ATT_ADD";
        public const String SP_UPDATE = "PN_MISAPRODUCTSTOCK_ATT_UPD";
        public const String SP_DELETE = "PN_MISAPRODUCTSTOCK_ATT_DEL";
        public const String SP_SELECT = "PN_MISAPRODUCTSTOCK_ATT_SEL";
        public const String SP_SEARCH = "PN_MISAPRODUCTSTOCK_ATT_SRH";
        public const String SP_UPDATEINDEX = "";
        #endregion
    }
}
