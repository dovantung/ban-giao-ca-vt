
#region Using
using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// Danh sách file đính kèm của phiếu nhập
	/// </summary>	
	public partial class DA_InputVoucher_Attachment
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin danh sách file đính kèm của phiếu nhập
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_InputVoucher_Attachment.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin danh sách file đính kèm của phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher_Attachment -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        /// <summary>
        /// PM_INPUTATTACHMENT_INPUTID
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public ResultMessage GetListByInputVoucherID(ref List<InputVoucher_Attachment>  objInputVoucher_AttachmentList, string strInputVoucherID)
        {
            IData objIData = Library.DataAccess.Data.CreateData();
            ResultMessage objResultMessage = new ResultMessage();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InputVoucher_Attachment.SP_GETLISTBYINPUTVOUCHERID);
                objIData.AddParameter("@" + InputVoucher_Attachment.colInputVoucherID, strInputVoucherID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    InputVoucher_Attachment objInputVoucher_Attachment = new InputVoucher_Attachment();
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colAttachMENTID])) objInputVoucher_Attachment.AttachmentID = Convert.ToString(reader[BO.InputVoucher_Attachment.colAttachMENTID]).Trim();
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colInputVoucherID])) objInputVoucher_Attachment.InputVoucherID = Convert.ToString(reader[BO.InputVoucher_Attachment.colInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colDescription])) objInputVoucher_Attachment.Description = Convert.ToString(reader[BO.InputVoucher_Attachment.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colFilePath])) objInputVoucher_Attachment.FilePath = Convert.ToString(reader[BO.InputVoucher_Attachment.colFilePath]).Trim();
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colFileName])) objInputVoucher_Attachment.FileName = Convert.ToString(reader[BO.InputVoucher_Attachment.colFileName]).Trim();
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colCreatedUser])) objInputVoucher_Attachment.CreatedUser = Convert.ToString(reader[BO.InputVoucher_Attachment.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colCreatedDate])) objInputVoucher_Attachment.CreatedDate = Convert.ToDateTime(reader[BO.InputVoucher_Attachment.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colIsDeleted])) objInputVoucher_Attachment.IsDeleted = Convert.ToBoolean(reader[BO.InputVoucher_Attachment.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colDeletedUser])) objInputVoucher_Attachment.DeletedUser = Convert.ToString(reader[BO.InputVoucher_Attachment.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.InputVoucher_Attachment.colDeletedDate])) objInputVoucher_Attachment.DeletedDate = Convert.ToDateTime(reader[BO.InputVoucher_Attachment.colDeletedDate]);
                    if (!Convert.IsDBNull(reader["FileID"])) objInputVoucher_Attachment.FileID = Convert.ToString(reader["FileID"]);
                    objInputVoucher_Attachment.IsNew = false;
                    if (objInputVoucher_AttachmentList == null)
                        objInputVoucher_AttachmentList = new List<InputVoucher_Attachment>();
                    objInputVoucher_AttachmentList.Add(objInputVoucher_Attachment);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi lấy danh sách file đính kèm theo mã phiếu nhập", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher_Attachment -> GetListByInputVoucherID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
		#endregion
	}
}
