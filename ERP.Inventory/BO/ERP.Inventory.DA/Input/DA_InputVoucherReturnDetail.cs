﻿
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputVoucherReturnDetail = ERP.Inventory.BO.Input.InputVoucherReturnDetail;
#endregion
namespace ERP.Inventory.DA.Input
{
    /// <summary>
    /// Created by 		: Võ Minh Hiếu 
    /// Created date 	: 12/10/2012 
    /// Chi tiết phiếu nhập trả hàng
    /// </summary>	
    public partial class DA_InputVoucherReturnDetail
    {



        #region Methods


        /// <summary>
        /// Nạp thông tin chi tiết phiếu nhập trả hàng
        /// </summary>
        /// <param name="objInputVoucherReturnDetail">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref InputVoucherReturnDetail objInputVoucherReturnDetail, string strInputVoucherReturnDetailID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colInputVoucherReturnDetailID, strInputVoucherReturnDetailID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (objInputVoucherReturnDetail == null)
                    objInputVoucherReturnDetail = new InputVoucherReturnDetail();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colInputVoucherReturnDetailID])) objInputVoucherReturnDetail.InputVoucherReturnDetailID = Convert.ToString(reader[InputVoucherReturnDetail.colInputVoucherReturnDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colInputVoucherReturnID])) objInputVoucherReturnDetail.InputVoucherReturnID = Convert.ToString(reader[InputVoucherReturnDetail.colInputVoucherReturnID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colReturnStoreID])) objInputVoucherReturnDetail.ReturnStoreID = Convert.ToInt32(reader[InputVoucherReturnDetail.colReturnStoreID]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colReturnDate])) objInputVoucherReturnDetail.ReturnDate = Convert.ToDateTime(reader[InputVoucherReturnDetail.colReturnDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colOutputVoucherID])) objInputVoucherReturnDetail.OutputVoucherID = Convert.ToString(reader[InputVoucherReturnDetail.colOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colOutputVoucherDetailID])) objInputVoucherReturnDetail.OutputVoucherDetailID = Convert.ToString(reader[InputVoucherReturnDetail.colOutputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colInputVoucherDetailID])) objInputVoucherReturnDetail.InputVoucherDetailID = Convert.ToString(reader[InputVoucherReturnDetail.colInputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colProductID])) objInputVoucherReturnDetail.ProductID = Convert.ToString(reader[InputVoucherReturnDetail.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colQuantity])) objInputVoucherReturnDetail.Quantity = Convert.ToDecimal(reader[InputVoucherReturnDetail.colQuantity]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colVAT])) objInputVoucherReturnDetail.VAT = Convert.ToInt32(reader[InputVoucherReturnDetail.colVAT]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colVATPercent])) objInputVoucherReturnDetail.VATPercent = Convert.ToInt32(reader[InputVoucherReturnDetail.colVATPercent]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colIMEI])) objInputVoucherReturnDetail.IMEI = Convert.ToString(reader[InputVoucherReturnDetail.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colIsReturnWithFee])) objInputVoucherReturnDetail.IsReturnWithFee = Convert.ToBoolean(reader[InputVoucherReturnDetail.colIsReturnWithFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colSalePrice])) objInputVoucherReturnDetail.SalePrice = Convert.ToDecimal(reader[InputVoucherReturnDetail.colSalePrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colReturnFee])) objInputVoucherReturnDetail.ReturnFee = Convert.ToDecimal(reader[InputVoucherReturnDetail.colReturnFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colOriginalReturnPrice])) objInputVoucherReturnDetail.OriginalReturnPrice = Convert.ToDecimal(reader[InputVoucherReturnDetail.colOriginalReturnPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colTotalVATLost])) objInputVoucherReturnDetail.TotalVATLost = Convert.ToDecimal(reader[InputVoucherReturnDetail.colTotalVATLost]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colAdjustPrice])) objInputVoucherReturnDetail.AdjustPrice = Convert.ToDecimal(reader[InputVoucherReturnDetail.colAdjustPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colReturnPrice])) objInputVoucherReturnDetail.ReturnPrice = Convert.ToDecimal(reader[InputVoucherReturnDetail.colReturnPrice]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colCreatedStoreID])) objInputVoucherReturnDetail.CreatedStoreID = Convert.ToInt32(reader[InputVoucherReturnDetail.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colCreatedUser])) objInputVoucherReturnDetail.CreatedUser = Convert.ToString(reader[InputVoucherReturnDetail.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colCreatedDate])) objInputVoucherReturnDetail.CreatedDate = Convert.ToDateTime(reader[InputVoucherReturnDetail.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colUpdatedUser])) objInputVoucherReturnDetail.UpdatedUser = Convert.ToString(reader[InputVoucherReturnDetail.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colUpdatedDate])) objInputVoucherReturnDetail.UpdatedDate = Convert.ToDateTime(reader[InputVoucherReturnDetail.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colIsDeleted])) objInputVoucherReturnDetail.IsDeleted = Convert.ToBoolean(reader[InputVoucherReturnDetail.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colDeletedUser])) objInputVoucherReturnDetail.DeletedUser = Convert.ToString(reader[InputVoucherReturnDetail.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturnDetail.colDeletedDate])) objInputVoucherReturnDetail.DeletedDate = Convert.ToDateTime(reader[InputVoucherReturnDetail.colDeletedDate]);
                    objInputVoucherReturnDetail.IsExist = true;
                }
                else
                {
                    objInputVoucherReturnDetail.IsExist = false;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin chi tiết phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturnDetail -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin chi tiết phiếu nhập trả hàng
        /// </summary>
        /// <param name="objInputVoucherReturnDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(InputVoucherReturnDetail objInputVoucherReturnDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objInputVoucherReturnDetail);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturnDetail -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin chi tiết phiếu nhập trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherReturnDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, InputVoucherReturnDetail objInputVoucherReturnDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHERRETURNDT_ADD");
                objIData.AddParameter("@" + InputVoucherReturnDetail.colInputVoucherReturnID, objInputVoucherReturnDetail.InputVoucherReturnID);
                objIData.AddParameter("@INPUTVOUCHERID", objInputVoucherReturnDetail.InputVoucherID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colReturnStoreID, objInputVoucherReturnDetail.ReturnStoreID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colReturnDate, objInputVoucherReturnDetail.ReturnDate);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colOutputVoucherID, objInputVoucherReturnDetail.OutputVoucherID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colOutputVoucherDetailID, objInputVoucherReturnDetail.OutputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colInputVoucherDetailID, objInputVoucherReturnDetail.InputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colProductID, objInputVoucherReturnDetail.ProductID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colQuantity, objInputVoucherReturnDetail.Quantity);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colVAT, objInputVoucherReturnDetail.VAT);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colVATPercent, objInputVoucherReturnDetail.VATPercent);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colIMEI, objInputVoucherReturnDetail.IMEI);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colIsReturnWithFee, objInputVoucherReturnDetail.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colSalePrice, objInputVoucherReturnDetail.SalePrice);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colReturnFee, objInputVoucherReturnDetail.ReturnFee);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colOriginalReturnPrice, objInputVoucherReturnDetail.OriginalReturnPrice);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colTotalVATLost, objInputVoucherReturnDetail.TotalVATLost);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colAdjustPrice, objInputVoucherReturnDetail.AdjustPrice);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colReturnPrice, objInputVoucherReturnDetail.ReturnPrice);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colCreatedStoreID, objInputVoucherReturnDetail.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colCreatedUser, objInputVoucherReturnDetail.CreatedUser);

                objIData.AddParameter("@ISNEW", objInputVoucherReturnDetail.IsNew);
                objIData.AddParameter("@ISSHOWPRODUCT", objInputVoucherReturnDetail.IsShowProduct);
                objIData.AddParameter("@ADJUSTPRICECONTENT", objInputVoucherReturnDetail.AdjustPriceContent);
                objIData.AddParameter("@ADJUSTPRICEUSER", objInputVoucherReturnDetail.AdjustPriceUser);
                objIData.AddParameter("@COSTPRICE", objInputVoucherReturnDetail.CostPrice);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin chi tiết phiếu nhập trả hàng
        /// </summary>
        /// <param name="objInputVoucherReturnDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(InputVoucherReturnDetail objInputVoucherReturnDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objInputVoucherReturnDetail);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin chi tiết phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturnDetail -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin chi tiết phiếu nhập trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherReturnDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, InputVoucherReturnDetail objInputVoucherReturnDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colInputVoucherReturnDetailID, objInputVoucherReturnDetail.InputVoucherReturnDetailID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colInputVoucherReturnID, objInputVoucherReturnDetail.InputVoucherReturnID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colReturnStoreID, objInputVoucherReturnDetail.ReturnStoreID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colReturnDate, objInputVoucherReturnDetail.ReturnDate);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colOutputVoucherID, objInputVoucherReturnDetail.OutputVoucherID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colOutputVoucherDetailID, objInputVoucherReturnDetail.OutputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colInputVoucherDetailID, objInputVoucherReturnDetail.InputVoucherDetailID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colProductID, objInputVoucherReturnDetail.ProductID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colQuantity, objInputVoucherReturnDetail.Quantity);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colVAT, objInputVoucherReturnDetail.VAT);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colVATPercent, objInputVoucherReturnDetail.VATPercent);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colIMEI, objInputVoucherReturnDetail.IMEI);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colIsReturnWithFee, objInputVoucherReturnDetail.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colSalePrice, objInputVoucherReturnDetail.SalePrice);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colReturnFee, objInputVoucherReturnDetail.ReturnFee);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colOriginalReturnPrice, objInputVoucherReturnDetail.OriginalReturnPrice);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colTotalVATLost, objInputVoucherReturnDetail.TotalVATLost);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colAdjustPrice, objInputVoucherReturnDetail.AdjustPrice);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colReturnPrice, objInputVoucherReturnDetail.ReturnPrice);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colCreatedStoreID, objInputVoucherReturnDetail.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colUpdatedUser, objInputVoucherReturnDetail.UpdatedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin chi tiết phiếu nhập trả hàng
        /// </summary>
        /// <param name="objInputVoucherReturnDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(InputVoucherReturnDetail objInputVoucherReturnDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objInputVoucherReturnDetail);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin chi tiết phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturnDetail -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin chi tiết phiếu nhập trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherReturnDetail">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, InputVoucherReturnDetail objInputVoucherReturnDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colInputVoucherReturnDetailID, objInputVoucherReturnDetail.InputVoucherReturnDetailID);
                objIData.AddParameter("@" + InputVoucherReturnDetail.colDeletedUser, objInputVoucherReturnDetail.DeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_InputVoucherReturnDetail()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "PM_INPUTVOUCHERRETURNDETAIL_ADD";
        public const String SP_UPDATE = "PM_INPUTVOUCHERRETURNDETAIL_UPD";
        public const String SP_DELETE = "PM_INPUTVOUCHERRETURNDETAIL_DEL";
        public const String SP_SELECT = "PM_INPUTVOUCHERRETURNDETAIL_SEL";
        public const String SP_SEARCH = "PM_INPUTVOUCHERRETURNDETAIL_SRH";
        public const String SP_UPDATEINDEX = "PM_INPUTVOUCHERRETURNDETAIL_UPDINDEX";
        #endregion

    }
}


