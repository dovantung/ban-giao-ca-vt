﻿
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
#endregion
namespace ERP.Inventory.DA.Input
{
    /// <summary>
    /// Created by 		: Võ Minh Hiếu 
    /// Created date 	: 12/10/2012 
    /// Bảng phiếu nhập trả hàng
    /// </summary>	
    public partial class DA_InputVoucherReturn
    {

        #region Methods

        /// <summary>
        /// Tìm kiếm thông tin bảng phiếu nhập trả hàng
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_InputVoucherReturn.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin bảng phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        public ResultMessage LoadOutputVoucherDetailForReturn(String strOutputVoucherID, bool bolIsReturnWithFee, bool bolIsAllowReturnAllProduct, ref DataTable dtbData)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_OutputVoucherDetail_Return");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                objIData.AddParameter("@IsReturnWithFee", bolIsReturnWithFee);
                objIData.AddParameter("@IsAllowReturnAllProduct", bolIsAllowReturnAllProduct);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "OutputVoucherDetail";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy chi tiết phiếu xuất để nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> LoadOutputVoucherDetailForReturn", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage LoadOutputVoucherWarrantyExtend(String strOutputVoucherID, String strIMEI, ref DataTable dtbData)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_OUPUVOUCHDT_GetAllWarrtyExt");
                objIData.AddParameter("@OutputVoucherID", strOutputVoucherID);
                objIData.AddParameter("@IMEI", strIMEI);
                dtbData = objIData.ExecStoreToDataTable();
                dtbData.TableName = "OutputVoucherDetailWarrantyExt";
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy phiếu xuất bảo hành mở rộng của imei nhập trả", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> LoadOutputVoucherWarrantyExtend", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage InsertMasterAndDetailAndVoucher(BO.Input.InputVoucherReturn objInputVoucherReturn, ERP.SalesAndServices.Payment.BO.Voucher objVoucher)
        {
            
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();

                this.Insert(objIData, objInputVoucherReturn);
                DA_InputVoucherReturnDetail objDA_InputVoucherReturnDetail = new DA_InputVoucherReturnDetail();

                foreach (BO.Input.InputVoucherReturnDetail objInputVoucherReturnDetail in objInputVoucherReturn.InputVoucherReturnDetailList)
                {
                    objInputVoucherReturnDetail.InputVoucherReturnID = objInputVoucherReturn.InputVoucherReturnID;
                    objInputVoucherReturnDetail.InputVoucherID = objInputVoucherReturn.InputVoucherID;
                    objDA_InputVoucherReturnDetail.Insert(objIData, objInputVoucherReturnDetail);
                }

                if (objVoucher.VoucherTypeID > 0)
                {
                    objVoucher.VoucherConcern = objInputVoucherReturn.InputVoucherID;
                    ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new SalesAndServices.Payment.DA.DA_Voucher();
                    objDA_Voucher.objLogObject = this.objLogObject;
                    objDA_Voucher.Insert(objIData, objVoucher);

                    ERP.SalesAndServices.Payment.DA.DA_VoucherDetail objDA_VoucherDetail = new SalesAndServices.Payment.DA.DA_VoucherDetail();
                    foreach (ERP.SalesAndServices.Payment.BO.VoucherDetail objVoucherDetail in objVoucher.VoucherDetailList)
                    {
                        objVoucherDetail.VoucherID = objVoucher.VoucherID;
                        if (objVoucherDetail.VNDCash != 0 || objVoucherDetail.ForeignCash != 0 || objVoucherDetail.PaymentCardAmount != 0 || objVoucherDetail.ForeignCashExchange != 0)
                        {
                            objDA_VoucherDetail.objLogObject = this.objLogObject;
                            objDA_VoucherDetail.Insert(objIData, objVoucherDetail);
                        }
                    }
                }

                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm phiếu nhập trả hàng và phiếu chi", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> InsertMasterAndDetailAndVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage CheckInputVoucherReturnByOutputVoucher(ref string strInputVoucherReturnID, string strOutputVoucherID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            IDataReader reader = null;
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("INPUVOUCHRETUR_GETBYOUTPUVOUCH");
                objIData.AddParameter("@OUTPUTVOUCHERID", strOutputVoucherID);
                reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["INPUTVOUCHERRETURNID"]))
                    {
                        strInputVoucherReturnID = Convert.ToString(reader["INPUTVOUCHERRETURNID"]);
                    }
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.GetData, "Lỗi kiểm tra phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> CheckInputVoucherReturnByOutputVoucher", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        //16/04/2014 Trần Thị Mi
        public ResultMessage InsertListInputVoucherReturn(List<BO.Input.InputVoucherReturn> lstInputVoucherReturn, ERP.SalesAndServices.Payment.BO.Voucher objVoucher)
        {

            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                DA_InputVoucherReturnDetail objDA_InputVoucherReturnDetail = new DA_InputVoucherReturnDetail();
                foreach (BO.Input.InputVoucherReturn objInputVoucherReturn in lstInputVoucherReturn)
                {
                    Insert(objIData, objInputVoucherReturn);
                    foreach (BO.Input.InputVoucherReturnDetail objInputVoucherReturnDetail in objInputVoucherReturn.InputVoucherReturnDetailList)
                    {
                        objInputVoucherReturnDetail.InputVoucherReturnID = objInputVoucherReturn.InputVoucherReturnID;
                        objInputVoucherReturnDetail.InputVoucherID = objInputVoucherReturn.InputVoucherID;
                        objDA_InputVoucherReturnDetail.Insert(objIData, objInputVoucherReturnDetail);
                    }

                }

                if (objVoucher!=null && objVoucher.VoucherTypeID > 0)
                {
                    ERP.SalesAndServices.Payment.DA.DA_Voucher objDA_Voucher = new SalesAndServices.Payment.DA.DA_Voucher();
                    objDA_Voucher.Insert(objIData, objVoucher);
                    ERP.SalesAndServices.Payment.DA.DA_VoucherDetail objDA_VoucherDetail = new SalesAndServices.Payment.DA.DA_VoucherDetail();

                    foreach (ERP.SalesAndServices.Payment.BO.VoucherDetail objVoucherDetail in objVoucher.VoucherDetailList)
                    {
                        objVoucherDetail.VoucherID = objVoucher.VoucherID;
                        if (objVoucherDetail.VNDCash != 0 || objVoucherDetail.ForeignCash != 0 || objVoucherDetail.PaymentCardAmount != 0 || objVoucherDetail.ForeignCashExchange != 0)
                        {
                            objDA_VoucherDetail.Insert(objIData, objVoucherDetail);
                        }
                    }
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm phiếu nhập trả hàng cho YCSC bảo hành", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> InsertListInputVoucherReturn", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetVoucherAlert(String strSaleOrderID, ref string strVoucherAlert)
        {
            strVoucherAlert = string.Empty;
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("CM_Voucher_GetAlert");
                objIData.AddParameter("@SaleOrderID", strSaleOrderID);
                strVoucherAlert = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi lấy thông tin cảnh báo đơn hàng thanh toán thẻ", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> GetVoucherAlert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        #endregion


    }
}


