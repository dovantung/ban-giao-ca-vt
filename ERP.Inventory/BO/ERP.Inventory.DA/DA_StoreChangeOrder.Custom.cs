
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using StoreChangeOrder = ERP.Inventory.BO.StoreChangeOrder;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Hoang Nhu Phong 
	/// Created date 	: 9/28/2012 
	/// Yêu cầu chuyển kho
	/// </summary>	
	public partial class DA_StoreChangeOrder
    {

        #region Methods			

        /// <summary>
        /// Tìm kiếm thông tin yêu cầu chuyển kho
        /// </summary>
        /// <param name="dtbData">Dữ liệu trả về</param>
        /// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_StoreChangeOrder.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        /// <summary>
		/// Tìm kiếm thông tin yêu cầu chuyển kho hàng hóa khác
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchDataOtherTransfer(ref DataTable dtbData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_StoreChangeOrder.SP_ORTHERSEARCH);
                objIData.AddParameter(objKeywords);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetStoreChangeOrderNewID(ref string strInputVoucherID, int intStoreID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_STORECHANGEORDER_NEWID");
                objIData.AddParameter("@StoreID", intStoreID);
                strInputVoucherID = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi lấy mã yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> GetInputVoucherNewID", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        /// <summary>
        /// Nạp thông tin yêu cầu chuyển kho
        /// </summary>
        /// <param name="objStoreChangeOrder">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref StoreChangeOrder objStoreChangeOrder, string strStoreChangeOrderID, int intStoreChangeOrderTypeID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            if (string.IsNullOrWhiteSpace(strStoreChangeOrderID))
            {
                if (objStoreChangeOrder == null)
                    objStoreChangeOrder = new StoreChangeOrder();
                objStoreChangeOrder.StoreChangeOrderID = "-2";
                objStoreChangeOrder.StoreChangeOrderTypeID = intStoreChangeOrderTypeID;
                objResultMessage = GetDataRelation(objStoreChangeOrder);
                return objResultMessage;
            }
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeOrderID, strStoreChangeOrderID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    if (objStoreChangeOrder == null)
                        objStoreChangeOrder = new StoreChangeOrder();
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colStoreChangeOrderID])) objStoreChangeOrder.StoreChangeOrderID = Convert.ToString(reader[StoreChangeOrder.colStoreChangeOrderID]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colStoreChangeOrderTypeID])) objStoreChangeOrder.StoreChangeOrderTypeID = Convert.ToInt32(reader[StoreChangeOrder.colStoreChangeOrderTypeID]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colStoreChangeTypeID])) objStoreChangeOrder.StoreChangeTypeID = Convert.ToInt32(reader[StoreChangeOrder.colStoreChangeTypeID]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colTransportTypeID])) objStoreChangeOrder.TransportTypeID = Convert.ToInt32(reader[StoreChangeOrder.colTransportTypeID]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colTransportCompanyID])) objStoreChangeOrder.TransportCompanyID = Convert.ToInt32(reader[StoreChangeOrder.colTransportCompanyID]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colTransportServicesID])) objStoreChangeOrder.TransportServicesID = Convert.ToInt32(reader[StoreChangeOrder.colTransportServicesID]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colFromStoreID])) objStoreChangeOrder.FromStoreID = Convert.ToInt32(reader[StoreChangeOrder.colFromStoreID]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colToStoreID])) objStoreChangeOrder.ToStoreID = Convert.ToInt32(reader[StoreChangeOrder.colToStoreID]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colContent])) objStoreChangeOrder.Content = Convert.ToString(reader[StoreChangeOrder.colContent]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colOrderDate])) objStoreChangeOrder.OrderDate = Convert.ToDateTime(reader[StoreChangeOrder.colOrderDate]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colExpiryDate])) objStoreChangeOrder.ExpiryDate = Convert.ToDateTime(reader[StoreChangeOrder.colExpiryDate]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colIsReviewed])) objStoreChangeOrder.IsReviewed = Convert.ToBoolean(reader[StoreChangeOrder.colIsReviewed]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colReviewedUser])) objStoreChangeOrder.ReviewedUser = Convert.ToString(reader[StoreChangeOrder.colReviewedUser]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colReviewedDate])) objStoreChangeOrder.ReviewedDate = Convert.ToDateTime(reader[StoreChangeOrder.colReviewedDate]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colCreatedStoreID])) objStoreChangeOrder.CreatedStoreID = Convert.ToInt32(reader[StoreChangeOrder.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colStoreChangeStatus])) objStoreChangeOrder.StoreChangeStatus = Convert.ToInt32(reader[StoreChangeOrder.colStoreChangeStatus]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colIsNew])) objStoreChangeOrder.IsNew = Convert.ToBoolean(reader[StoreChangeOrder.colIsNew]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colInStockStatusID])) objStoreChangeOrder.InStockStatusID = Convert.ToInt32(reader[StoreChangeOrder.colInStockStatusID]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colTotalCommandQuantity])) objStoreChangeOrder.TotalCommandQuantity = Convert.ToDecimal(reader[StoreChangeOrder.colTotalCommandQuantity]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colTotalQuantity])) objStoreChangeOrder.TotalQuantity = Convert.ToDecimal(reader[StoreChangeOrder.colTotalQuantity]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colTotalStoreChangeQuantity])) objStoreChangeOrder.TotalStoreChangeQuantity = Convert.ToDecimal(reader[StoreChangeOrder.colTotalStoreChangeQuantity]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colIsUrgent])) objStoreChangeOrder.IsUrgent = Convert.ToBoolean(reader[StoreChangeOrder.colIsUrgent]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colUpdatedIsUrgentIUser])) objStoreChangeOrder.UpdatedIsUrgentIUser = Convert.ToString(reader[StoreChangeOrder.colUpdatedIsUrgentIUser]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colUpdatedIsUrgentDate])) objStoreChangeOrder.UpdatedIsUrgentDate = Convert.ToDateTime(reader[StoreChangeOrder.colUpdatedIsUrgentDate]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colCreatedUser])) objStoreChangeOrder.CreatedUser = Convert.ToString(reader[StoreChangeOrder.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colCreatedDate])) objStoreChangeOrder.CreatedDate = Convert.ToDateTime(reader[StoreChangeOrder.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colUpdatedUser])) objStoreChangeOrder.UpdatedUser = Convert.ToString(reader[StoreChangeOrder.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colUpdatedDate])) objStoreChangeOrder.UpdatedDate = Convert.ToDateTime(reader[StoreChangeOrder.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colIsDeleted])) objStoreChangeOrder.IsDeleted = Convert.ToBoolean(reader[StoreChangeOrder.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colDeletedUser])) objStoreChangeOrder.DeletedUser = Convert.ToString(reader[StoreChangeOrder.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colDeletedDate])) objStoreChangeOrder.DeletedDate = Convert.ToDateTime(reader[StoreChangeOrder.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colContentDeleted])) objStoreChangeOrder.ContentDeleted = Convert.ToString(reader[StoreChangeOrder.colContentDeleted]).Trim();
                    if (!Convert.IsDBNull(reader[StoreChangeOrder.colIsExpired])) objStoreChangeOrder.IsExpired = Convert.ToBoolean(reader[StoreChangeOrder.colIsExpired]);
                    objResultMessage = GetDataRelation(objStoreChangeOrder);
                }
                else
                {
                    objStoreChangeOrder = null;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        private ResultMessage GetDataRelation(StoreChangeOrder objStoreChangeOrder)
        {
            ResultMessage objResultMessage = new ResultMessage();
            DataTable dtb = null;
            objResultMessage = new DA_StoreChangeOrderDetail().SearchData(ref dtb, new object[] { "@STORECHANGEORDERID", objStoreChangeOrder.StoreChangeOrderID });
            if (objResultMessage.IsError)
            {
                return objResultMessage;
            }
            objStoreChangeOrder.DtbStoreChangeOrderDetail = dtb;
            dtb = null;
            objResultMessage = new DA_StoreChangeOrder_RVList().SearchData(ref dtb, new object[] { "@STORECHANGEORDERTYPEID", objStoreChangeOrder.StoreChangeOrderTypeID, "@STORECHANGEORDERID", objStoreChangeOrder.StoreChangeOrderID, "@REVIEWLEVELID", "-1" });
            if (objResultMessage.IsError)
            {
                return objResultMessage;
            }
            objStoreChangeOrder.DtbReviewLevel = dtb;
            dtb = null;
            objResultMessage = new DA_StoreChangeOrderAttachment().SearchData(ref dtb, new object[] { "@StoreChangeOrderID", objStoreChangeOrder.StoreChangeOrderID });
            if (objResultMessage.IsError) { return objResultMessage; }
            objStoreChangeOrder.DtbAttachment = dtb;
            dtb = null;
            return objResultMessage;
        }
        /// <summary>
        /// Thêm thông tin yêu cầu chuyển kho
        /// </summary>
        /// <param name="objStoreChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(StoreChangeOrder objStoreChangeOrder, ref string strStoreChangeOrderID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                Insert(objIData, objStoreChangeOrder);
                strStoreChangeOrderID = objStoreChangeOrder.StoreChangeOrderID;
                objResultMessage = InsertDetail(objIData, objStoreChangeOrder);
                if (objResultMessage.IsError) return objResultMessage;
                objResultMessage = InsertReview(objIData, objStoreChangeOrder);
                if (objResultMessage.IsError) return objResultMessage;
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        /// <summary>
        /// Thêm thông tin yêu cầu chuyển kho
        /// </summary>
        /// <param name="objStoreChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public bool ReturnInstock(StoreChangeOrder objStoreChangeOrder)
        {
            bool bolResult = false;
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_STORECHANGEINSTOCKCHECK");
                objIData.AddParameter("@StoreChangeOrderID", objStoreChangeOrder.StoreChangeOrderID);
                int Result =Convert.ToInt32(objIData.ExecStoreToString()) ;
                bolResult = Convert.ToBoolean(Result);
            }
            catch (Exception objEx)
            {
                objIData.Disconnect();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi lấy mã yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> GetInputVoucherNewID", InventoryGlobals.ModuleName);
                return bolResult;
            }
            finally
            {
                objIData.Disconnect();
             
            }
            return bolResult;
        }

        private ResultMessage InsertDetail(IData objIData, StoreChangeOrder objStoreChangeOrder)
        {
            ResultMessage objResultMessage = new ResultMessage();
            if (objStoreChangeOrder.DtbStoreChangeOrderDetail == null) return new ResultMessage();
            try
            {
                new DA_StoreChangeOrderDetail().Delete(objIData, objStoreChangeOrder);
                foreach (DataRow row in objStoreChangeOrder.DtbStoreChangeOrderDetail.Rows)
                {
                    BO.StoreChangeOrderDetail objStoreChangeOrderDetail = new BO.StoreChangeOrderDetail();
                    objStoreChangeOrderDetail.CreatedStoreID = objStoreChangeOrder.CreatedStoreID;
                    objStoreChangeOrderDetail.CreatedUser = objStoreChangeOrder.CreatedUser;
                    objStoreChangeOrderDetail.Note = Convert.ToString(row["NOTE"]);
                    objStoreChangeOrderDetail.OrderDate = objStoreChangeOrder.OrderDate;
                    objStoreChangeOrderDetail.ProductID = Convert.ToString(row["PRODUCTID"]);
                    objStoreChangeOrderDetail.Quantity = Convert.ToDecimal(row["QUANTITY"]);
                    objStoreChangeOrderDetail.StoreChangeQuantity = Convert.ToDecimal(row["STORECHANGEQUANTITY"]);
                    objStoreChangeOrderDetail.StoreChangeOrderID = objStoreChangeOrder.StoreChangeOrderID;
                    objStoreChangeOrderDetail.FromStoreID = objStoreChangeOrder.FromStoreID;
                    objStoreChangeOrderDetail.ToStoreID = objStoreChangeOrder.ToStoreID;
                    objStoreChangeOrderDetail.StoreChangeOrderDetailID = new DA_StoreChangeOrderDetail().Insert(objIData, objStoreChangeOrderDetail);
                    //Delete Imei
                    //DeleteIMEI(objIData, objStoreChangeOrderDetail); -- Xóa trong store (Thiên bỏ 07/09/2016)
                    //Insert Imei
                    string strImei = Convert.ToString(row["IMEI"]).Replace("\r\n", ",").Replace("\n", ",").Replace(";", ",");
                    foreach (string imei in strImei.Split(','))
                    {
                        if (!string.IsNullOrWhiteSpace(imei))
                            InsertImei(objIData, objStoreChangeOrderDetail, imei);
                    }
                }
            }
            catch (Exception ex)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin chi tiết yêu cầu chuyển kho", ex.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> InsertDetail", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            return objResultMessage;
        }
        private ResultMessage InsertReview(IData objIData, StoreChangeOrder objStoreChangeOrder)
        {
            ResultMessage objResultMessage = new ResultMessage();
            if (objStoreChangeOrder.DtbReviewLevel == null) return new ResultMessage();
            try
            {
                //BO.StoreChangeOrder_RVList objStoreChangeOrder_RVList_Del = new BO.StoreChangeOrder_RVList();
                //objStoreChangeOrder_RVList_Del.StoreChangeOrderID = objStoreChangeOrder.StoreChangeOrderID;
                //objStoreChangeOrder_RVList_Del.ReviewLevelID = -1;
                //new DA_StoreChangeOrder_RVList().Delete(objIData, objStoreChangeOrder_RVList_Del);
                foreach (DataRow row in objStoreChangeOrder.DtbReviewLevel.Rows)
                {
                    BO.StoreChangeOrder_RVList objStoreChangeOrder_RVList = new BO.StoreChangeOrder_RVList();
                    objStoreChangeOrder_RVList.IsReviewed = Convert.ToBoolean(row["ISREVIEWED"]);
                    objStoreChangeOrder_RVList.Note = Convert.ToString(row["NOTE"]);
                    objStoreChangeOrder_RVList.OrderDate = objStoreChangeOrder.OrderDate;
                    if (string.IsNullOrEmpty(Convert.ToString(row["REVIEWEDDATE"])))
                    {
                        objStoreChangeOrder_RVList.ReviewedDate = null;
                    }
                    else
                        objStoreChangeOrder_RVList.ReviewedDate = Convert.ToDateTime(row["REVIEWEDDATE"]); //DateTime.Now;
                    if (string.IsNullOrEmpty(Convert.ToString(row["REVIEWEDUSER"])))
                    {
                        objStoreChangeOrder_RVList.ReviewedUser = null;
                    }
                    else
                        objStoreChangeOrder_RVList.ReviewedUser = Convert.ToString(row["REVIEWEDUSER"]);
                    if (string.IsNullOrEmpty(Convert.ToString(row["REVIEWSTATUS"])))
                    {
                        objStoreChangeOrder_RVList.ReviewStatus = 0;
                    }
                    else
                        objStoreChangeOrder_RVList.ReviewStatus = Convert.ToInt32(row["REVIEWSTATUS"]);

                    objStoreChangeOrder_RVList.ReviewLevelID = Convert.ToInt32(row["REVIEWLEVELID"]);
                    objStoreChangeOrder_RVList.StoreChangeOrderID = objStoreChangeOrder.StoreChangeOrderID;
                    new DA_StoreChangeOrder_RVList().Insert(objIData, objStoreChangeOrder_RVList);
                }
            }
            catch (Exception ex)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm mức duyệt yêu cầu chuyển kho", ex.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> InsertReview", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            return objResultMessage;
        }
        private void DeleteIMEI(IData objIData, ERP.Inventory.BO.StoreChangeOrderDetail objStoreChangeOrderDetail)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_SCODETAILIMEI_DEL");
                objIData.AddParameter("@STORECHANGEORDERDETAILID", objStoreChangeOrderDetail.StoreChangeOrderDetailID);
                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw ex;
            }

        }
        private void InsertImei(IData objIData, ERP.Inventory.BO.StoreChangeOrderDetail objStoreChangeOrderDetail, string strImei)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_SCODETAILIMEI_ADD");
                objIData.AddParameter("@STORECHANGEORDERDETAILID", objStoreChangeOrderDetail.StoreChangeOrderDetailID);
                objIData.AddParameter("@IMEI", strImei);
                objIData.AddParameter("@ORDERDATE", objStoreChangeOrderDetail.OrderDate);
                objIData.AddParameter("@ISSTORECHANGE", 0);
                objIData.AddParameter("@NOTE", string.Empty);
                objIData.AddParameter("@CREATEDSTOREID", objStoreChangeOrderDetail.CreatedStoreID);
                objIData.AddParameter("@FROMSTORE", objStoreChangeOrderDetail.FromStoreID);
                objIData.AddParameter("@CREATEDUSER", objStoreChangeOrderDetail.CreatedUser);
                objIData.AddParameter("@STORECHANGEORDERID", objStoreChangeOrderDetail.StoreChangeOrderID);
                objIData.AddParameter("@PRODUCTID", objStoreChangeOrderDetail.ProductID);

                objIData.ExecNonQuery();
            }
            catch (Exception ex)
            {
                objIData.RollBackTransaction();
                throw (ex);
            }
        }
        /// <summary>
        /// Cập nhật thông tin yêu cầu chuyển kho
        /// </summary>
        /// <param name="objStoreChangeOrder">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(StoreChangeOrder objStoreChangeOrder)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                Update(objIData, objStoreChangeOrder);
                objResultMessage = InsertDetail(objIData, objStoreChangeOrder);
                if (objResultMessage.IsError) return objResultMessage;
                objResultMessage = InsertReview(objIData, objStoreChangeOrder);
                if (objResultMessage.IsError) return objResultMessage;
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }


            //List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
            //if (objStoreChangeOrder.UserNotifyData != null && objStoreChangeOrder.UserNotifyData.Rows.Count > 0)
            //{
            //    foreach (DataRow item in objStoreChangeOrder.UserNotifyData.Rows)
            //    {
            //        Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
            //        obj.UserName = item["UserName"].ToString();
            //        if (!objNotification_UserList.Exists(o => o.UserName == obj.UserName))
            //        {
            //            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
            //            {
            //                bool bolResult = false;
            //                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, obj.UserName);
            //                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == obj.UserName))
            //                    objNotification_UserList.Add(obj);
            //            }
            //            else
            //                objNotification_UserList.Add(obj);
            //        }
            //    }
            //}
            //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
            //objNotification.NotificationID = Guid.NewGuid().ToString();
            //objNotification.NotificationTypeID = 9;
            //objNotification.TaskParameters = objStoreChangeOrder.StoreChangeOrderID;
            //objNotification.NotificationName = "Yêu cầu chuyển kho";
            //objNotification.Content = "Từ kho " + objStoreChangeOrder.FromStoreName + " -> " + objStoreChangeOrder.ToStoreName + ". Nội dung: " + objStoreChangeOrder.Content;
            //objNotification.CreatedUser = objStoreChangeOrder.CreatedUser;
            //objNotification.CreatedDate = DateTime.Now;
            //objNotification.Notification_UserList = objNotification_UserList;
            //objNotification.StartDate = DateTime.Now;
            //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
            //objDA_Notification.Broadcast(objNotification);
            return objResultMessage;
        }


        public ResultMessage CreateMainGroupMenuItem(ref DataTable dtbResult, string strStoreChangeOrderID, string strUserName)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.PM_STORECHANGEORDER_ListMG");
                objIData.AddParameter("@StoreChangeOrderID", strStoreChangeOrderID);
                objIData.AddParameter("@UserName", strUserName);
                dtbResult = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi tạo danh sách ngành hàng";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChangeOrder -> CreateMainGroupMenuItem", "ERP.Inventoty.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Lấy danh sách mặt hàng chưa chuyển cho xuất chuyển kho
        /// </summary>
        /// <returns></returns>
        public ResultMessage LoadDetailForStoreChangeByList(ref DataTable dtbResult, string strMainGroupIDList, string strStoreChangeOrderID, int intInputTypeID, int intOutputTypeID, string strUsername)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_SCO_DETAIL_LSC");
                objIData.AddParameter("@StoreChangeOrderID", strStoreChangeOrderID);
                objIData.AddParameter("@MAINGROUPIDLIST", strMainGroupIDList);
                objIData.AddParameter("@InputTypeID", intInputTypeID);
                objIData.AddParameter("@OutputTypeID", intOutputTypeID);
                objIData.AddParameter("@Username", strUsername);
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi lấy danh sách mặt hàng chưa chuyển cho xuất chuyển kho";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DAStoreChangeOrder -> LoadDetailForStoreChangeByList", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        public ResultMessage CreateStoreChangeOrder(BO.StoreChangeOrder StoreChangeOrder, DataTable tblStoreChangeCommand, DataTable tblFromStore)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                DA_StoreChangeOrder objDA_StoreChangeOrder = new DA_StoreChangeOrder();
                foreach (DataRow objFromStore in tblFromStore.Rows)
                {
                    int intFromStoreID = Convert.ToInt32(objFromStore["FromStoreID"]);
                    DataTable tblToStore = Library.WebCore.Globals.SelectDistinct(tblStoreChangeCommand, "ToStoreID", "FromStoreID = " + intFromStoreID.ToString());
                    if (tblToStore.Rows.Count > 0)
                    {
                        foreach (DataRow objToStore in tblToStore.Rows)
                        {
                            int intToStoreID = Convert.ToInt32(objToStore["ToStoreID"]);
                            String strStoreChangeCommandIDList = GetStoreChangeCommandIDList(tblStoreChangeCommand, intFromStoreID, intToStoreID);
                            DataTable dtbStoreChangeOrderDetail = LoadStoreChangeOrderDetailFromCommand(objIData, strStoreChangeCommandIDList);
                            if (dtbStoreChangeOrderDetail != null && dtbStoreChangeOrderDetail.Rows.Count > 0)
                            {

                                StoreChangeOrder.FromStoreID = intFromStoreID;
                                StoreChangeOrder.ToStoreID = intToStoreID;
                                if (dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", "") == null)
                                    StoreChangeOrder.TotalCommandQuantity = 0;
                                else
                                    StoreChangeOrder.TotalCommandQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(CommandQuantity)", ""));
                                if (dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", "") == null)
                                    StoreChangeOrder.TotalQuantity = 0;
                                else
                                    StoreChangeOrder.TotalQuantity = Convert.ToDecimal(dtbStoreChangeOrderDetail.Compute("sum(OrderQuantity)", ""));
                                objDA_StoreChangeOrder.Insert(objIData, StoreChangeOrder);
                                if (!StoreChangeOrder.IsReviewed)
                                {
                                    objDA_StoreChangeOrder.InsertStoreChangeOrder_ReviewList(objIData, StoreChangeOrder.StoreChangeOrderID, StoreChangeOrder.StoreChangeOrderTypeID);
                                }
                                DA_StoreChangeOrderDetail objDA_StoreChangeOrderDetail = new DA_StoreChangeOrderDetail();
                                DA_StoreChangeOrderDetailIMEI objDA_StoreChangeOrderDetailIMEI = new DA_StoreChangeOrderDetailIMEI();
                                foreach (DataRow dtrStoreChangeOrderDetail in dtbStoreChangeOrderDetail.Rows)
                                {
                                    BO.StoreChangeOrderDetail objStoreChangeOrderDetailBO = new BO.StoreChangeOrderDetail();
                                    BO.StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEIBO = new BO.StoreChangeOrderDetailIMEI();
                                    objStoreChangeOrderDetailBO.ProductID = dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim();
                                    objStoreChangeOrderDetailBO.CommandQuantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["CommandQuantity"]);
                                    objStoreChangeOrderDetailBO.Quantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["OrderQuantity"]);
                                    objStoreChangeOrderDetailBO.CreatedStoreID = intFromStoreID;
                                    objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                                    objStoreChangeOrderDetailBO.IsManualAdd = Convert.ToBoolean(dtrStoreChangeOrderDetail["IsManualAdd"]);
                                    objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                                    objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                    objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                                    objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                                    //Lưu db
                                    string strStoreChangeOrderDetailID = objDA_StoreChangeOrderDetail.Insert(objIData, objStoreChangeOrderDetailBO);
                                    if (dtrStoreChangeOrderDetail["IMEI"].ToString().Trim() != string.Empty)
                                    {
                                        objStoreChangeOrderDetailIMEIBO.CreatedStoreID = objStoreChangeOrderDetailBO.CreatedStoreID;
                                        objStoreChangeOrderDetailIMEIBO.CreatedUser = objStoreChangeOrderDetailBO.CreatedUser;
                                        objStoreChangeOrderDetailIMEIBO.IMEI = dtrStoreChangeOrderDetail["IMEI"].ToString().Trim();
                                        objStoreChangeOrderDetailIMEIBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                                        objStoreChangeOrderDetailIMEIBO.StoreChangeOrderDetailID = strStoreChangeOrderDetailID;
                                        objDA_StoreChangeOrderDetailIMEI.Insert(objIData, objStoreChangeOrderDetailIMEIBO);
                                    }
                                }
                            }
                        }
                    }
                }
                objIData.CommitTransaction();
                //DataSet dsUserNotify = null;
                //List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
                //if (dsUserNotify != null && dsUserNotify.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow item in dsUserNotify.Tables[0].Rows)
                //    {
                //        Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
                //        obj.UserName = item["UserName"].ToString();
                //        if (!objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                //        {
                //            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
                //            {
                //                bool bolResult = false;
                //                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, obj.UserName);
                //                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                //                    objNotification_UserList.Add(obj);
                //            }
                //            else
                //                objNotification_UserList.Add(obj);
                //        }
                //    }
                //}
                //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
                //objNotification.NotificationID = Guid.NewGuid().ToString();
                //objNotification.NotificationTypeID = 9;
                //objNotification.TaskParameters = StoreChangeOrder.StoreChangeOrderID;
                //objNotification.NotificationName = "Yêu cầu chuyển kho";
                //objNotification.Content = "Từ kho " + StoreChangeOrder.FromStoreName + " -> " + StoreChangeOrder.ToStoreName + ". Nội dung: " + StoreChangeOrder.Content;
                //objNotification.CreatedUser = StoreChangeOrder.CreatedUser;
                //objNotification.CreatedDate = DateTime.Now;
                //objNotification.Notification_UserList = objNotification_UserList;
                //objNotification.StartDate = DateTime.Now;
                //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
                //objDA_Notification.Broadcast(objNotification);
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi tạo danh sách loại lệnh chuyển kho được tính lúc chia hàng";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChangeOrder -> CreateStoreChangeOrder", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Chuyển kho đảo hàng giữa kho chính và kho bày mẫu
        /// </summary>
        /// <param name="StoreChangeOrder"></param>
        /// <param name="tblStoreChangeDetail"></param>
        /// <returns></returns>
        public ResultMessage CreateStoreChangeOrderShowProduct(BO.StoreChangeOrder StoreChangeOrder, DataTable tblStoreChangeDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                if (tblStoreChangeDetail.Rows.Count > 0)
                {
                    int intRealStoreID = Convert.ToInt32(tblStoreChangeDetail.Rows[0]["RealStoreID"]);
                    int intAuxiliaryStoreID = Convert.ToInt32(tblStoreChangeDetail.Rows[0]["AuxiliaryStoreID"]);
                    if (tblStoreChangeDetail != null && tblStoreChangeDetail.Rows.Count > 0)
                    {
                        StoreChangeOrder.FromStoreID = intRealStoreID;
                        StoreChangeOrder.ToStoreID = intAuxiliaryStoreID;
                        StoreChangeOrder.TotalQuantity = tblStoreChangeDetail.Rows.Count;
                        AddStoreChangeOrderShowProduct(objIData, StoreChangeOrder, tblStoreChangeDetail, "REALIMEI");//Chuyển từ kho chính qua kho bày mẫu
                        StoreChangeOrder.FromStoreID = intAuxiliaryStoreID;
                        StoreChangeOrder.ToStoreID = intRealStoreID;
                        AddStoreChangeOrderShowProduct(objIData, StoreChangeOrder, tblStoreChangeDetail, "AUXILIARYIMEI"); //Chuyển từ kho bày mẫu sang kho chính
                    }
                }
                objIData.CommitTransaction();
                //DataSet dsUserNotify = null;
                //List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
                //if (dsUserNotify != null && dsUserNotify.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow item in dsUserNotify.Tables[0].Rows)
                //    {
                //        Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
                //        obj.UserName = item["UserName"].ToString();
                //        if (!objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                //        {
                //            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
                //            {
                //                bool bolResult = false;
                //                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, obj.UserName);
                //                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                //                    objNotification_UserList.Add(obj);
                //            }
                //            else
                //                objNotification_UserList.Add(obj);
                //        }
                //    }
                //}
                //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
                //objNotification.NotificationID = Guid.NewGuid().ToString();
                //objNotification.NotificationTypeID = 9;
                //objNotification.TaskParameters = StoreChangeOrder.StoreChangeOrderID;
                //objNotification.NotificationName = "Yêu cầu chuyển kho";
                //objNotification.Content = "Từ kho " + StoreChangeOrder.FromStoreName + " -> " + StoreChangeOrder.ToStoreName + ". Nội dung: " + StoreChangeOrder.Content;
                //objNotification.CreatedUser = StoreChangeOrder.CreatedUser;
                //objNotification.CreatedDate = DateTime.Now;
                //objNotification.Notification_UserList = objNotification_UserList;
                //objNotification.StartDate = DateTime.Now;
                //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
                //objDA_Notification.Broadcast(objNotification);
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi tạo yêu cầu chuyển kho từ đảo hàng bày mẫu";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChangeOrder -> CreateStoreChangeOrderShowProduct", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Chuyển từ kho chính sang kho bày mẫu
        /// </summary>
        /// <param name="StoreChangeOrder"></param>
        /// <param name="tblStoreChangeDetail"></param>
        /// <returns></returns>
        public ResultMessage CreateStoreChangeOrderShowProduct_1(BO.StoreChangeOrder StoreChangeOrder, DataTable tblStoreChangeDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                if (tblStoreChangeDetail.Rows.Count > 0)
                {
                    if (tblStoreChangeDetail != null && tblStoreChangeDetail.Rows.Count > 0)
                    {
                        StoreChangeOrder.TotalQuantity = tblStoreChangeDetail.Rows.Count;
                        AddStoreChangeOrderShowProduct(objIData, StoreChangeOrder, tblStoreChangeDetail, "IMEI");//Chuyển từ kho chính qua kho bày mẫu
                    }
                }
                objIData.CommitTransaction();
                //DataSet dsUserNotify = null;
                //List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
                //if (dsUserNotify != null && dsUserNotify.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow item in dsUserNotify.Tables[0].Rows)
                //    {
                //        Notification.BO.Notification_User obj = new Notification.BO.Notification_User();
                //        obj.UserName = item["UserName"].ToString();
                //        if (!objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                //        {
                //            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
                //            {
                //                bool bolResult = false;
                //                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, obj.UserName);
                //                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == obj.UserName))
                //                    objNotification_UserList.Add(obj);
                //            }
                //            else
                //                objNotification_UserList.Add(obj);
                //        }
                //    }
                //}
                //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
                //objNotification.NotificationID = Guid.NewGuid().ToString();
                //objNotification.NotificationTypeID = 9;
                //objNotification.TaskParameters = StoreChangeOrder.StoreChangeOrderID;
                //objNotification.NotificationName = "Yêu cầu chuyển kho";
                //objNotification.Content = "Từ kho " + StoreChangeOrder.FromStoreName + " -> " + StoreChangeOrder.ToStoreName + ". Nội dung: " + StoreChangeOrder.Content;
                //objNotification.CreatedUser = StoreChangeOrder.CreatedUser;
                //objNotification.CreatedDate = DateTime.Now;
                //objNotification.Notification_UserList = objNotification_UserList;
                //objNotification.StartDate = DateTime.Now;
                //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
                //objDA_Notification.Broadcast(objNotification);
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                string strMessage = "Lỗi tạo yêu cầu chuyển kho từ đảo hàng bày mẫu";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx.ToString());
                new SystemError(objIData, strMessage, objEx.ToString(), "DA_StoreChangeOrder -> CreateStoreChangeOrderShowProduct", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        private void AddStoreChangeOrderShowProduct(IData objIData, BO.StoreChangeOrder StoreChangeOrder, DataTable tblStoreChangeDetail, string strIMEITYPE)
        {
            try
            {
                DA_StoreChangeOrder objDA_StoreChangeOrder = new DA_StoreChangeOrder();
                objDA_StoreChangeOrder.Insert(objIData, StoreChangeOrder);//Chuyển từ kho chính sang kho phụ
                if (!StoreChangeOrder.IsReviewed)
                {
                    objDA_StoreChangeOrder.InsertStoreChangeOrder_ReviewList(objIData, StoreChangeOrder.StoreChangeOrderID, StoreChangeOrder.StoreChangeOrderTypeID);
                }
                DA_StoreChangeOrderDetail objDA_StoreChangeOrderDetail = new DA_StoreChangeOrderDetail();
                DA_StoreChangeOrderDetailIMEI objDA_StoreChangeOrderDetailIMEI = new DA_StoreChangeOrderDetailIMEI();
                foreach (DataRow dtrStoreChangeOrderDetail in tblStoreChangeDetail.Rows)
                {
                    BO.StoreChangeOrderDetail objStoreChangeOrderDetailBO = new BO.StoreChangeOrderDetail();
                    BO.StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEIBO = new BO.StoreChangeOrderDetailIMEI();
                    objStoreChangeOrderDetailBO.ProductID = dtrStoreChangeOrderDetail["PRODUCTID"].ToString().Trim();
                    //objStoreChangeOrderDetailBO.CommandQuantity = Convert.ToDecimal(dtrStoreChangeOrderDetail["CommandQuantity"]);
                    objStoreChangeOrderDetailBO.Quantity = 1;
                    objStoreChangeOrderDetailBO.CreatedStoreID = StoreChangeOrder.CreatedStoreID;
                    objStoreChangeOrderDetailBO.FromStoreID = StoreChangeOrder.FromStoreID;
                    objStoreChangeOrderDetailBO.ToStoreID = StoreChangeOrder.ToStoreID;
                    objStoreChangeOrderDetailBO.CreatedUser = StoreChangeOrder.CreatedUser;
                    objStoreChangeOrderDetailBO.IsManualAdd = false;
                    //objStoreChangeOrderDetailBO.StoreChangeCommandIDList = strStoreChangeCommandIDList;
                    //objStoreChangeOrderDetailBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                    objStoreChangeOrderDetailBO.StoreChangeOrderID = StoreChangeOrder.StoreChangeOrderID;
                    objStoreChangeOrderDetailBO.OrderDate = StoreChangeOrder.OrderDate;
                    //Lưu db
                    string strStoreChangeOrderDetailID = objDA_StoreChangeOrderDetail.Insert(objIData, objStoreChangeOrderDetailBO);
                    if (dtrStoreChangeOrderDetail[strIMEITYPE].ToString().Trim() != string.Empty)
                    {
                        objStoreChangeOrderDetailIMEIBO.CreatedStoreID = objStoreChangeOrderDetailBO.CreatedStoreID;
                        objStoreChangeOrderDetailIMEIBO.CreatedUser = objStoreChangeOrderDetailBO.CreatedUser;
                        objStoreChangeOrderDetailIMEIBO.OrderDate = DateTime.Now;
                        objStoreChangeOrderDetailIMEIBO.IMEI = dtrStoreChangeOrderDetail[strIMEITYPE].ToString().Trim();
                        //objStoreChangeOrderDetailIMEIBO.Note = dtrStoreChangeOrderDetail["Note"].ToString();
                        objStoreChangeOrderDetailIMEIBO.StoreChangeOrderDetailID = strStoreChangeOrderDetailID;
                        objDA_StoreChangeOrderDetailIMEI.Insert(objIData, objStoreChangeOrderDetailIMEIBO);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private String GetStoreChangeCommandIDList(DataTable tblStoreChangeCommand, int intFromStoreID, int intToStoreID)
        {
            String strResult = "";
            String strSelectExp = "FromStoreID = " + intFromStoreID.ToString() + " and ToStoreID = " + intToStoreID.ToString();
            DataRow[] arrRow = tblStoreChangeCommand.Select(strSelectExp);
            foreach (DataRow objRow in arrRow)
            {
                strResult += "'" + Convert.ToString(objRow["StoreChangeCommandID"]).Trim() + "',";
            }
            if (strResult.Length > 0)
                strResult = strResult.Substring(0, strResult.Length - 1);
            return strResult;
        }

        private DataTable LoadStoreChangeOrderDetailFromCommand(IData objIData, String strStoreChangeCommandIDList)
        {

            DataTable dtbResult = null;
            try
            {
                objIData.CreateNewStoredProcedure("PM_STORECHANGEORDER_IDFRCMD");
                objIData.AddParameter("@StoreChangeCommandIDList", strStoreChangeCommandIDList, Library.DataAccess.Globals.DATATYPE.CLOB);
                dtbResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objExc)
            {
                new SystemError(objIData, "Lỗi nạp danh sách sản phẩm từ lệnh chuyển kho", objExc);
            }
            return dtbResult;
        }

        public ResultMessage LoadStoreChangeOrderDetailFromCommand(ref DataTable dtbData, String strStoreChangeCommandIDList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Data.CreateData();
            try
            {
                objIData.Connect();
                dtbData = LoadStoreChangeOrderDetailFromCommand(objIData, strStoreChangeCommandIDList);
            }
            catch (Exception objEx)
            {
                string strMessage = "Lỗi lấy danh sách chi tiết yêu cầu chuyển kho từ lệnh chuyển kho";
                objResultMessage = new ResultMessage(true, SystemError.ErrorTypes.GetData, strMessage, objEx);
                new SystemError(objIData, strMessage, objEx.ToString(), "DAStoreChangeOrder -> LoadStoreChangeOrderDetailFromCommand", "ERP.Inventory.DA");
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public void InsertStoreChangeOrder_ReviewList(IData objIData, string strStoreChangeOrderID, int intStoreChangeOrderTypeID)
        {
            try
            {
                objIData.CreateNewStoredProcedure("PM_SCORDER_RVLIST_ADDMULTI");
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeOrderID, strStoreChangeOrderID);
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeOrderTypeID, intStoreChangeOrderTypeID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion
        public ResultMessage ReviewStoreChangeOrder(string strStoreChangeOrderID, string strReviewUser)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_STORECHANGEORDER_UPDREVIEW");
                objIData.AddParameter("@" + StoreChangeOrder.colStoreChangeOrderID, strStoreChangeOrderID);
                objIData.AddParameter("@" + StoreChangeOrder.colReviewedUser, strReviewUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi duyệt thông tin yêu cầu chuyển kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> ReviewStoreChangeOrder", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        public ResultMessage CheckStorePermission(int intFromStoreID, int intToStoreID, string strUserName, ref bool bolFromStorePermission, ref bool bolToStorePermission)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_SCO_CHECKSTOREPERMISSION");
                objIData.AddParameter("@FROMSTOREID", intFromStoreID);
                objIData.AddParameter("@TOSTOREID", intToStoreID);
                objIData.AddParameter("@USERNAME", strUserName);
                string strResult = objIData.ExecStoreToString();
                string[] arr = strResult.Split(',');
                if (arr.Length == 2)
                {
                    bolFromStorePermission = arr[0].Equals("1");
                    bolToStorePermission = arr[1].Equals("1");
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi kiểm tra quyền trên kho", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> CheckStorePermission", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage LoadMainGroupPermission(string strUserName, ref DataTable dtbData)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("MD_MAINGROUP_CACHE");
                objIData.AddParameter("@USERNAME", strUserName);
                dtbData = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi nạp thông tin quyền trên ngành hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeOrder -> CheckMainGroupPermission", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
    }
}
