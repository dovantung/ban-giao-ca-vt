
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using IMEISalesInfo_Images = ERP.Inventory.BO.IMEISalesInfo_Images;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Cao Hữu Vũ Lam 
	/// Created date 	: 3/7/2014 
	/// 
	/// </summary>	
	public partial class DA_IMEISalesInfo_Images
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin 
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_IMEISalesInfo_Images.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin ", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo_ImageS -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}


        /// <summary>
        /// Nạp thông tin 
        /// </summary>
        /// <param name="objIMEISalesInfo_Images">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public List<IMEISalesInfo_Images> LoadInfoToListByIMEI(IData objIData, string strProductID, string strIMEI)
        {
            //ResultMessage objResultMessage = new ResultMessage();
            List<IMEISalesInfo_Images> lstIMEISalesInfo_Images = new List<IMEISalesInfo_Images>();
            //IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                //objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_IMEISalesInfo_Images.SP_SEARCH);
                objIData.AddParameter("@" + IMEISalesInfo_Images.colProductID, strProductID);
                objIData.AddParameter("@" + IMEISalesInfo_Images.colIMEI, strIMEI);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while(reader.Read())
                {
                    IMEISalesInfo_Images objIMEISalesInfo_Images = new IMEISalesInfo_Images();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colImageID])) objIMEISalesInfo_Images.ImageID = Convert.ToString(reader[IMEISalesInfo_Images.colImageID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colProductID])) objIMEISalesInfo_Images.ProductID = Convert.ToString(reader[IMEISalesInfo_Images.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colIMEI])) objIMEISalesInfo_Images.IMEI = Convert.ToString(reader[IMEISalesInfo_Images.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colImageName])) objIMEISalesInfo_Images.ImageName = Convert.ToString(reader[IMEISalesInfo_Images.colImageName]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colDescription])) objIMEISalesInfo_Images.Description = Convert.ToString(reader[IMEISalesInfo_Images.colDescription]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colLargeSizeImage])) objIMEISalesInfo_Images.LargeSizeImage = Convert.ToString(reader[IMEISalesInfo_Images.colLargeSizeImage]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colMediumSizeImage])) objIMEISalesInfo_Images.MediumSizeImage = Convert.ToString(reader[IMEISalesInfo_Images.colMediumSizeImage]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colSmallSizeImage])) objIMEISalesInfo_Images.SmallSizeImage = Convert.ToString(reader[IMEISalesInfo_Images.colSmallSizeImage]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colLargeSizeImageFileID])) objIMEISalesInfo_Images.LargeSizeImageFileID = Convert.ToString(reader[IMEISalesInfo_Images.colLargeSizeImageFileID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colMediumSizeImageFileID])) objIMEISalesInfo_Images.MediumSizeImageFileID = Convert.ToString(reader[IMEISalesInfo_Images.colMediumSizeImageFileID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colSmallSizeImageFileID])) objIMEISalesInfo_Images.SmallSizeImageFileID = Convert.ToString(reader[IMEISalesInfo_Images.colSmallSizeImageFileID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colIsDefault])) objIMEISalesInfo_Images.IsDefault = Convert.ToBoolean(reader[IMEISalesInfo_Images.colIsDefault]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colOrderIndex])) objIMEISalesInfo_Images.OrderIndex = Convert.ToInt32(reader[IMEISalesInfo_Images.colOrderIndex]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colIsActive])) objIMEISalesInfo_Images.IsActive = Convert.ToBoolean(reader[IMEISalesInfo_Images.colIsActive]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colIsSystem])) objIMEISalesInfo_Images.IsSystem = Convert.ToBoolean(reader[IMEISalesInfo_Images.colIsSystem]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colCreatedUser])) objIMEISalesInfo_Images.CreatedUser = Convert.ToString(reader[IMEISalesInfo_Images.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colCreatedDate])) objIMEISalesInfo_Images.CreatedDate = Convert.ToDateTime(reader[IMEISalesInfo_Images.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colUpdatedUser])) objIMEISalesInfo_Images.UpdatedUser = Convert.ToString(reader[IMEISalesInfo_Images.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colUpdatedDate])) objIMEISalesInfo_Images.UpdatedDate = Convert.ToDateTime(reader[IMEISalesInfo_Images.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colIsDeleted])) objIMEISalesInfo_Images.IsDeleted = Convert.ToBoolean(reader[IMEISalesInfo_Images.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colDeletedUser])) objIMEISalesInfo_Images.DeletedUser = Convert.ToString(reader[IMEISalesInfo_Images.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo_Images.colDeletedDate])) objIMEISalesInfo_Images.DeletedDate = Convert.ToDateTime(reader[IMEISalesInfo_Images.colDeletedDate]);
                    lstIMEISalesInfo_Images.Add(objIMEISalesInfo_Images);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
            return lstIMEISalesInfo_Images;
        }

        /// <summary>
        /// Xóa thông tin 
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objIMEISalesInfo_ImageS">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, string strDeletedUser, string strProductID, string strIMEI)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + IMEISalesInfo_Images.colDeletedUser, strDeletedUser);
                objIData.AddParameter("@" + IMEISalesInfo_Images.colProductID, strProductID);
                objIData.AddParameter("@" + IMEISalesInfo_Images.colIMEI, strIMEI);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
		#endregion
		
		
	}
}
