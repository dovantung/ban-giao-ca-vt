
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.InputChangeOrder
{
    /// <summary>
	/// Created by 		: Đặng Minh Hùng 
	/// Created date 	: 02/07/2014 
	/// 
    /// Modified by : Nguyen Van Tai
    /// Modified date : 30/11/2017
    /// Desc : Bo sung Nhom loi, loi
	/// </summary>	
	public class InputChangeOrderDetail
    {


        #region Member Variables

        private string strInputChangeOrderDetailID = string.Empty;
        private string strInputChangeOrderID = string.Empty;
        private DateTime? dtmInputChangeOrderDate;
        private int intInputChangeOrderStoreID = 0;
        private string strOldOutputVoucherDetailID = string.Empty;
        private string strProductID_In = string.Empty;
        private decimal decQuantity;
        private string strIMEI_In = string.Empty;
        private decimal decInputPrice;
        private string strProductID_Out = string.Empty;
        private string strIMEI_Out = string.Empty;
        private decimal decSalePrice;
        private string strInputChangeDetailID = string.Empty;
        private string strNewInputVoucherDetailID = string.Empty;
        private string strNewOutputVoucherDetailID = string.Empty;
        private int intCreatedStoreID = 0;
        private string strCreatedUser = string.Empty;
        private DateTime? dtmCreatedDate;
        private string strUpdatedUser = string.Empty;
        private DateTime? dtmUpdatedDate;
        private bool bolIsDeleted = false;
        private string strDeletedUser = string.Empty;
        private DateTime? dtmDeletedDate;
        private decimal decReturnFee;
        private int intReturnInputTypeID = 0;
        private bool bolIsNew_In = false;
        private int intVAT_In = 0;
        private int intVATPercent_In = 0;
        private bool bolIsHasWarranty_In = false;
        private int intOutputTypeID_Out = 0;
        private DateTime? dtmEndWarrantyDate;
        private int intMachineErrorId = 0;
        private int intMachineErrorGroupId = 0;
        private string strMachineErrorContent = string.Empty;
        private int intInStockStatusID_in = -1;
        private int? intInStockStatusID_Out;

        #endregion


        #region Properties 

        /// <summary>
        /// InputChangeOrderDetailID
        /// 
        /// </summary>
        public string InputChangeOrderDetailID
        {
            get { return strInputChangeOrderDetailID; }
            set { strInputChangeOrderDetailID = value; }
        }

        /// <summary>
        /// InputChangeOrderID
        /// 
        /// </summary>
        public string InputChangeOrderID
        {
            get { return strInputChangeOrderID; }
            set { strInputChangeOrderID = value; }
        }

        /// <summary>
        /// InputChangeOrderDate
        /// 
        /// </summary>
        public DateTime? InputChangeOrderDate
        {
            get { return dtmInputChangeOrderDate; }
            set { dtmInputChangeOrderDate = value; }
        }

        /// <summary>
        /// InputChangeOrderStoreID
        /// 
        /// </summary>
        public int InputChangeOrderStoreID
        {
            get { return intInputChangeOrderStoreID; }
            set { intInputChangeOrderStoreID = value; }
        }

        /// <summary>
        /// OldOutputVoucherDetailID
        /// 
        /// </summary>
        public string OldOutputVoucherDetailID
        {
            get { return strOldOutputVoucherDetailID; }
            set { strOldOutputVoucherDetailID = value; }
        }

        /// <summary>
        /// ProductID_In
        /// 
        /// </summary>
        public string ProductID_In
        {
            get { return strProductID_In; }
            set { strProductID_In = value; }
        }

        /// <summary>
        /// Quantity
        /// 
        /// </summary>
        public decimal Quantity
        {
            get { return decQuantity; }
            set { decQuantity = value; }
        }

        /// <summary>
        /// IMEI_In
        /// 
        /// </summary>
        public string IMEI_In
        {
            get { return strIMEI_In; }
            set { strIMEI_In = value; }
        }

        /// <summary>
        /// InputPrice
        /// 
        /// </summary>
        public decimal InputPrice
        {
            get { return decInputPrice; }
            set { decInputPrice = value; }
        }

        /// <summary>
        /// ProductID_Out
        /// 
        /// </summary>
        public string ProductID_Out
        {
            get { return strProductID_Out; }
            set { strProductID_Out = value; }
        }

        /// <summary>
        /// IMEI_Out
        /// 
        /// </summary>
        public string IMEI_Out
        {
            get { return strIMEI_Out; }
            set { strIMEI_Out = value; }
        }

        /// <summary>
        /// SalePrice
        /// 
        /// </summary>
        public decimal SalePrice
        {
            get { return decSalePrice; }
            set { decSalePrice = value; }
        }

        /// <summary>
        /// InputChangeDetailID
        /// 
        /// </summary>
        public string InputChangeDetailID
        {
            get { return strInputChangeDetailID; }
            set { strInputChangeDetailID = value; }
        }

        /// <summary>
        /// NewInputVoucherDetailID
        /// 
        /// </summary>
        public string NewInputVoucherDetailID
        {
            get { return strNewInputVoucherDetailID; }
            set { strNewInputVoucherDetailID = value; }
        }

        /// <summary>
        /// NewOutputVoucherDetailID
        /// 
        /// </summary>
        public string NewOutputVoucherDetailID
        {
            get { return strNewOutputVoucherDetailID; }
            set { strNewOutputVoucherDetailID = value; }
        }

        /// <summary>
        /// ReturnFee
        /// Phí trả
        /// </summary>
        public decimal ReturnFee
        {
            get { return decReturnFee; }
            set { decReturnFee = value; }
        }

        /// <summary>
        /// ReturnInputTypeID
        /// Hình thức nhập lại
        /// </summary>
        public int ReturnInputTypeID
        {
            get { return intReturnInputTypeID; }
            set { intReturnInputTypeID = value; }
        }

        /// <summary>
        /// IsNew
        /// Mới/Cũ của SP nhập
        /// </summary>
        public bool IsNew_In
        {
            get { return bolIsNew_In; }
            set { bolIsNew_In = value; }
        }

        /// <summary>
        /// VAT_In
        /// VAT SP Nhập
        /// </summary>
        public int VAT_In
        {
            get { return intVAT_In; }
            set { intVAT_In = value; }
        }

        /// <summary>
        /// VATPercent_In
        /// Phần trăm nộp SP nhập
        /// </summary>
        public int VATPercent_In
        {
            get { return intVATPercent_In; }
            set { intVATPercent_In = value; }
        }

        /// <summary>
        /// IsHasWarranty_In
        /// Có bảo hành SP nhập
        /// </summary>
        public bool IsHasWarranty_In
        {
            get { return bolIsHasWarranty_In; }
            set { bolIsHasWarranty_In = value; }
        }

        /// <summary>
        /// OutputTypeID_Out
        /// Hình thức xuất ra
        /// </summary>
        public int OutputTypeID_Out
        {
            get { return intOutputTypeID_Out; }
            set { intOutputTypeID_Out = value; }
        }

        /// <summary>
        /// EndWarrantyDate
        /// Ngày bảo hành SP nhập
        /// </summary>
        public DateTime? EndWarrantyDate
        {
            get { return dtmEndWarrantyDate; }
            set { dtmEndWarrantyDate = value; }
        }

        /// <summary>
        /// CreatedStoreID
        /// 
        /// </summary>
        public int CreatedStoreID
        {
            get { return intCreatedStoreID; }
            set { intCreatedStoreID = value; }
        }

        /// <summary>
        /// CreatedUser
        /// 
        /// </summary>
        public string CreatedUser
        {
            get { return strCreatedUser; }
            set { strCreatedUser = value; }
        }

        /// <summary>
        /// CreatedDate
        /// 
        /// </summary>
        public DateTime? CreatedDate
        {
            get { return dtmCreatedDate; }
            set { dtmCreatedDate = value; }
        }

        /// <summary>
        /// UpdatedUser
        /// 
        /// </summary>
        public string UpdatedUser
        {
            get { return strUpdatedUser; }
            set { strUpdatedUser = value; }
        }

        /// <summary>
        /// UpdatedDate
        /// 
        /// </summary>
        public DateTime? UpdatedDate
        {
            get { return dtmUpdatedDate; }
            set { dtmUpdatedDate = value; }
        }

        /// <summary>
        /// IsDeleted
        /// 
        /// </summary>
        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }

        /// <summary>
        /// DeletedUser
        /// 
        /// </summary>
        public string DeletedUser
        {
            get { return strDeletedUser; }
            set { strDeletedUser = value; }
        }

        /// <summary>
        /// DeletedDate
        /// 
        /// </summary>
        public DateTime? DeletedDate
        {
            get { return dtmDeletedDate; }
            set { dtmDeletedDate = value; }
        }

        public int OutputTypeID { get; set; }

        /// <summary>
        /// MachineErrorId
        /// </summary>
        public int MachineErrorId
        {
            get { return intMachineErrorId; }
            set { intMachineErrorId = value; }
        }

        /// <summary>
        /// MachineErrorGroupId
        /// </summary>
        public int MachineErrorGroupId
        {
            get { return intMachineErrorGroupId; }
            set { intMachineErrorGroupId = value; }
        }

        /// <summary>
        /// MachineErrorContent
        /// </summary>
        public string MachineErrorContent
        {
            get { return strMachineErrorContent; }
            set { strMachineErrorContent = value; }
        }

        public int InStockStatusID_In
        {
            get
            {
                return intInStockStatusID_in;
            }

            set
            {
                intInStockStatusID_in = value;
            }
        }

        public int? InStockStatusID_Out
        {
            get
            {
                return intInStockStatusID_Out;
            }

            set
            {
                intInStockStatusID_Out = value;
            }
        }
        private bool bolIsUpdateConcern = false;

        public bool IsUpdateConcern
        {
            get { return bolIsUpdateConcern; }
            set { bolIsUpdateConcern = value; }
        }
        #endregion


        #region Constructor

        public InputChangeOrderDetail()
        {
        }
        #endregion


        #region Column Names

        public const String colInputChangeOrderDetailID = "InputChangeOrderDetailID";
        public const String colInputChangeOrderID = "InputChangeOrderID";
        public const String colInputChangeOrderDate = "InputChangeOrderDate";
        public const String colInputChangeOrderStoreID = "InputChangeOrderStoreID";
        public const String colOLDOutputVoucherDetailID = "OLDOutputVoucherDetailID";
        public const String colProductID_In = "ProductID_In";
        public const String colQuantity = "Quantity";
        public const String colIMEI_In = "IMEI_In";
        public const String colInputPrice = "InputPrice";
        public const String colProductID_Out = "ProductID_Out";
        public const String colIMEI_Out = "IMEI_Out";
        public const String colSalePrice = "SalePrice";
        public const String colInputChangeDetailID = "InputChangeDetailID";
        public const String colNewInputVoucherDetailID = "NewInputVoucherDetailID";
        public const String colNewOutputVoucherDetailID = "NewOutputVoucherDetailID";
        public const String colReturnFee = "ReturnFee";
        public const String colReturnInputTypeID = "ReturnInputTypeID";
        public const String colIsNew_In = "IsNew_In";
        public const String colVAT_In = "VAT_In";
        public const String colVATPercent_In = "VATPercent_In";
        public const String colIsHasWarranty_In = "IsHasWarranty_In";
        public const String colOutputTypeID_Out = "OutputTypeID_Out";
        public const String colEndWarrantyDate = "EndWarrantyDate";
        public const String colCreatedStoreID = "CreatedStoreID";
        public const String colCreatedUser = "CreatedUser";
        public const String colCreatedDate = "CreatedDate";
        public const String colUpdatedUser = "UpdatedUser";
        public const String colUpdatedDate = "UpdatedDate";
        public const String colIsDeleted = "IsDeleted";
        public const String colDeletedUser = "DeletedUser";
        public const String colDeletedDate = "DeletedDate";
        public const String colMachineErrorId = "MachineErrorId";
        public const String colMachineErrorGroupId = "MachineErrorGroupId";

        public const String colMachineErrorContent = "MACHINEERRORCONTENT";
        public const String colInStockStatusID_in = "InStockStatusID_in";
        public const String colInStockStatusID_Out = "InStockStatusID_Out";
        

        #endregion //Column names


    }
}
