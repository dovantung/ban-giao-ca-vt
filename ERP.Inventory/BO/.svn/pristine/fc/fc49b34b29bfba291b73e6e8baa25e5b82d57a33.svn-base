﻿using Library.DataAccess;
using Library.WebCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.Inventory.DA.Accessory
{
    public partial class DA_AccessoryDividing
    {
        #region Methods
        public ResultMessage SearchData(ref DataTable dtResult, params  object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.COMMON_SELECT");
                objIData.AddParameter(objKeywords);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AccessoryDividing -> SearchData", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetListDivideHis(ref DataTable dtResult)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.get_list_divide_his");
                object[] objKeywords = new object[]
                    {
                      "@type","1",
                    };
                objIData.AddParameter(objKeywords);
                dtResult = objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AccessoryDividing -> GetListDivideHis", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetDivisionItemList(ref DataTable dtSource, ref DataTable dtList, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.get_access_divide_result");
                objIData.AddParameter(objKeywords);
                dtList = objIData.ExecStoreToDataTable();

                objIData.CreateNewStoredProcedure("ERP.pck_dividing.get_divide_source_list");
                objIData.AddParameter(objKeywords);
                dtSource = objIData.ExecStoreToDataTable();

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AccessoryDividing -> GetDivisionItemList", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage GetStockCardAccessory(ref DataTable dtSource, object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                object[] objectParameter = new object[]{
                            "@serial_or_pk","1",
                           };
                objIData.CreateNewStoredProcedure("ERP.pck_division_goods.reset_stock_card_fromNC");
                objIData.AddParameter(objectParameter);
                string v_out = objIData.ExecStoreToString();

                if ("1".Equals(v_out))
                {
                    objIData.CreateNewStoredProcedure("ERP.pck_dividing.get_access_sum_divide_item");
                    objIData.AddParameter(objKeywords);
                    dtSource = objIData.ExecStoreToDataTable();
                }
                else
                {
                    objIData.RollBackTransaction();
                    objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", "Error when reset Stock card: " + v_out);
                    ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AccessoryDividing -> GetSourceFromStockCard", InventoryGlobals.ModuleName);
                    return objResultMessage;
                }

                objIData.CommitTransaction();

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AccessoryDividing -> GetSourceFromStockCard", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateSourceForDividing(String goods_division_history_id, object[][] objSourceList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                object[] objectParameter = new object[]{
                            "@goods_division_history_id",goods_division_history_id,
                           };
                objResultMessage = UpdateSourceForDividing(objIData, objectParameter, objSourceList);
                if (objResultMessage.IsError)
                {
                    objIData.RollBackTransaction();
                }
                objIData.CommitTransaction();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AccessoryDividing -> UpdateSourceForDividing", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;

        }
        private ResultMessage UpdateSourceForDividing(IData objIData, object[] objKeywords, object[][] objSourceList)
        {
                 ResultMessage objResultMessage = new ResultMessage();
                 objIData.CreateNewStoredProcedure("ERP.pck_dividing.del_goods_division_source_list");
                objIData.AddParameter(objKeywords);
                objIData.ExecStoreToString();

                object[] objectParameter = new object[]{
                            "@command","select shop_code, stock_id from shop_stock where shop_code in ('VTMB','VTMN','NTST')"
                           };

                DataTable dtShop = null;
                SearchData(ref dtShop, objectParameter);
                string shop_code,stock_id;

                for (int i = 0; i < objSourceList.Length; i++)
                {
                    shop_code = ""; stock_id = "";
                    for (int j = 0; j < objSourceList[i].Length; j++)
                    {
                        if ("@shop_code".Equals(objSourceList[i][j]))
                        {
                            shop_code = objSourceList[i][j + 1].ToString();
                            for (int k = 0; k < dtShop.Rows.Count; k++)
                            {
                                if (dtShop.Rows[k]["shop_code"].Equals(shop_code))
                                {
                                    stock_id = dtShop.Rows[k]["stock_id"].ToString();
                                    break;
                                }
                            }

                            if (!String.IsNullOrEmpty(shop_code))
                            {
                                if (!String.IsNullOrEmpty(stock_id))
                                {
                                    objSourceList[i][j] = "@stock_id";
                                    objSourceList[i][j + 1] = stock_id;
                                    objIData.CreateNewStoredProcedure("ERP.pck_dividing.add_goods_division_source_list");
                                    objIData.AddParameter(objSourceList[i]);
                                    objIData.ExecStoreToString();
                                    break;
                                }
                                else { 
                                
                                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi thông tin", "Không tồn tại kho có mã : " + shop_code);
                                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AccessoryDividing -> UpdateSourceForDividing", InventoryGlobals.ModuleName);
                                return objResultMessage;
                                }
                            }
                        }
                    }
                }
                

           
            return objResultMessage;
        }

        public ResultMessage OnDividing(String goods_division_history_id, object[][] objSourceList)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.BeginTransaction();
                object[] objectParameter = new object[]{
                            "@goods_division_history_id",goods_division_history_id,
                           };
                objResultMessage = UpdateSourceForDividing(objIData, objectParameter, objSourceList);
                if (objResultMessage.IsError)
                {
                    objIData.RollBackTransaction();
                    return objResultMessage;
                }

                objIData.CreateNewStoredProcedure("ERP.pck_dividing.update_goods_division_history");
                objIData.AddParameter(objectParameter);
                objIData.ExecNonQuery();

                objIData.CreateNewStoredProcedure("ERP.pck_dividing.get_goods_dividing_source_pk");
                objIData.AddParameter(objectParameter);
                objIData.ExecNonQuery();
                //DataTable dtbSource = objIData.ExecStoreToDataTable();

                //if (dtbSource.Rows.Count > 0)
                //{
                //    for (int i = 0; i < dtbSource.Rows.Count; i++)
                //    {
                //        objectParameter = new object[]{
                //              "@goods_id"         , dtbSource.Rows[i]["goods_id"],
                //              "@div_type"         , "0",
                //              "@sl"               , dtbSource.Rows[i]["VALUE"],
                //              "@goods_div_his_id" , goods_division_history_id,
                //              "@par_shop_code"    , dtbSource.Rows[i]["shop_code"],
                //           };
                //        objIData.CreateNewStoredProcedure("pck_goods_div.goods_div");
                //        objIData.AddParameter(objectParameter);
                //        objIData.ExecNonQuery();
                //    }

                //}
                objIData.CommitTransaction();

            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AccessoryDividing -> GetSourceFromStockCard", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        public ResultMessage UpdateAdjustmentNumber(object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("ERP.pck_dividing.update_division_source_list");
                objIData.AddParameter(objKeywords);
                string v_out = objIData.ExecStoreToString();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi update thông tin", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_AccessoryDividing -> UpdateAdjustmentNumber", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }
        #endregion

    }
}
