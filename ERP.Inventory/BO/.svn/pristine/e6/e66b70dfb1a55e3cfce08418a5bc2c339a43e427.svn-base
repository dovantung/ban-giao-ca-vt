
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.WebCore;
using Library.DataAccess;
using ERP.Inventory.BO.PM;
#endregion
namespace ERP.Inventory.DA.PM
{
    /// <summary>
	/// Created by 		: Hồ Tấn Tài 
	/// Created date 	: 06/20/2018 
	/// Chi tiết yêu cầu IMEI không FIFO
	/// </summary>	
	public partial class DA_IMEIExcludeFIFORequestDT
	{	
		
		#region Methods			
		
		/// <summary>
		/// Tìm kiếm thông tin chi tiết yêu cầu imei không fifo
		/// </summary>
		/// <param name="dtbData">Dữ liệu trả về</param>
		/// <param name="objKeywords">Điều kiện tìm kiếm truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage SearchData(ref DataTable dtbData, params object[] objKeywords)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(DA_IMEIExcludeFIFORequestDT.SP_SEARCH);
				objIData.AddParameter(objKeywords);
				dtbData = objIData.ExecStoreToDataTable();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chi tiết yêu cầu imei không fifo", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEIEXCLUDEFIForeQUESTDT -> SearchData", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
			{
        		objIData.Disconnect();
			}
    		return objResultMessage;
		}

        public List<IMEIExcludeFIFORequestDT> SearchDataToList(IData objIData, params object[] objKeywords)
        {
            ResultMessage objResultMessage = new ResultMessage();
            List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = new List<IMEIExcludeFIFORequestDT>();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(DA_IMEIExcludeFIFORequestDT.SP_SEARCH);
                objIData.AddParameter(objKeywords);
                IDataReader reader = objIData.ExecStoreToDataReader();
                while (reader.Read())
                {
                    IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT = new IMEIExcludeFIFORequestDT();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestDTID])) objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestDTID = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestDTID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestID])) objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestID = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colIMEIExcludeFIFORequestID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colProductID])) objIMEIExcludeFIFORequestDT.ProductID = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colIMEI])) objIMEIExcludeFIFORequestDT.IMEI = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colInStockStatusID])) objIMEIExcludeFIFORequestDT.InStockStatusID = Convert.ToInt32(reader[IMEIExcludeFIFORequestDT.colInStockStatusID]);
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colFirstInputDate])) objIMEIExcludeFIFORequestDT.FirstInputDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequestDT.colFirstInputDate]);
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colNote])) objIMEIExcludeFIFORequestDT.Note = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colNote]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colCreatedUser])) objIMEIExcludeFIFORequestDT.CreatedUser = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colCreatedDate])) objIMEIExcludeFIFORequestDT.CreatedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequestDT.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colUpdatedUser])) objIMEIExcludeFIFORequestDT.UpdatedUser = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colUpdatedDate])) objIMEIExcludeFIFORequestDT.UpdatedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequestDT.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colIsDeleted])) objIMEIExcludeFIFORequestDT.IsDeleted = Convert.ToBoolean(reader[IMEIExcludeFIFORequestDT.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colDeletedUser])) objIMEIExcludeFIFORequestDT.DeletedUser = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colDeletedDate])) objIMEIExcludeFIFORequestDT.DeletedDate = Convert.ToDateTime(reader[IMEIExcludeFIFORequestDT.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[IMEIExcludeFIFORequestDT.colVoucherConcern])) objIMEIExcludeFIFORequestDT.VoucherConcern = Convert.ToString(reader[IMEIExcludeFIFORequestDT.colVoucherConcern]).Trim();
                    if (!Convert.IsDBNull(reader["PRODUCTNAME"])) objIMEIExcludeFIFORequestDT.ProductName = Convert.ToString(reader["PRODUCTNAME"]).Trim();
                    lstIMEIExcludeFIFORequestDT.Add(objIMEIExcludeFIFORequestDT);
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.SearchData, "Lỗi tìm kiếm thông tin chi tiết yêu cầu imei không fifo", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_ACC_WORDHandover_Detail -> SearchData", InventoryGlobals.ModuleName);
                return null;
            }

            return lstIMEIExcludeFIFORequestDT;
        }

		#endregion
		
		
	}
}
