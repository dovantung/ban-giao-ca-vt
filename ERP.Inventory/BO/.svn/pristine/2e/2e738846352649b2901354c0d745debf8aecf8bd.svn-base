
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using Inventory = ERP.Inventory.BO.Inventory.Inventory;
#endregion
namespace ERP.Inventory.DA.Inventory
{
    /// <summary>
    /// Created by 		: Đặng Minh Hùng 
    /// Created date 	: 12/26/2012 
    /// Phiếu kiểm kê
    /// </summary>	
    public partial class DA_Inventory
    {


        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods


        /// <summary>
        /// Nạp thông tin phiếu kiểm kê
        /// </summary>
        /// <param name="objInventory">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref BO.Inventory.Inventory objInventory, string strInventoryID, ref DataTable dtbInventoryDetail)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryID, strInventoryID);
                IDataReader reader = objIData.ExecStoreToDataReader();

                if (reader.Read())
                {
                    if (objInventory == null)
                        objInventory = new BO.Inventory.Inventory();

                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryID])) objInventory.InventoryID = Convert.ToString(reader[BO.Inventory.Inventory.colInventoryID]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryTermID])) objInventory.InventoryTermID = Convert.ToInt32(reader[BO.Inventory.Inventory.colInventoryTermID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryStoreID])) objInventory.InventoryStoreID = Convert.ToInt32(reader[BO.Inventory.Inventory.colInventoryStoreID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colMainGroupID])) objInventory.MainGroupIDList = Convert.ToString(reader[BO.Inventory.Inventory.colMainGroupID]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colSubGroupID])) objInventory.SubGroupIDList = Convert.ToString(reader[BO.Inventory.Inventory.colSubGroupID]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsNew])) objInventory.IsNew = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsNew]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsConnected])) objInventory.IsConnected = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsConnected]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsParent])) objInventory.IsParent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsParent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colProductStatusID])) objInventory.ProductStatusID = Convert.ToInt32(reader[BO.Inventory.Inventory.colProductStatusID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryDate])) objInventory.InventoryDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colInventoryDate]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colInventoryUser])) objInventory.InventoryUser = Convert.ToString(reader[BO.Inventory.Inventory.colInventoryUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colBeginInventoryTime])) objInventory.BeginInventoryTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colBeginInventoryTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colEndInventoryTime])) objInventory.EndInventoryTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colEndInventoryTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colContent])) objInventory.Content = Convert.ToString(reader[BO.Inventory.Inventory.colContent]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsReviewed])) objInventory.IsReviewed = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsReviewed]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsUpdateUnEvent])) objInventory.IsUpdateUnEvent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsUpdateUnEvent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colUpdateUnEventUser])) objInventory.UpdateUnEventUser = Convert.ToString(reader[BO.Inventory.Inventory.colUpdateUnEventUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colUpdateUnEventTime])) objInventory.UpdateUnEventTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colUpdateUnEventTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsLock])) objInventory.IsLock = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsLock]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colLockUser])) objInventory.LockUser = Convert.ToString(reader[BO.Inventory.Inventory.colLockUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colLockTime])) objInventory.LockTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colLockTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsExplainUnEvent])) objInventory.IsExplainUnEvent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsExplainUnEvent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colExplainUnEventUser])) objInventory.ExplainUnEventUser = Convert.ToString(reader[BO.Inventory.Inventory.colExplainUnEventUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colExplainUnEventTime])) objInventory.ExplainUnEventTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colExplainUnEventTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsAdjustUnEvent])) objInventory.IsAdjustUnEvent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsAdjustUnEvent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colAdjustUnEventUser])) objInventory.AdjustUnEventUser = Convert.ToString(reader[BO.Inventory.Inventory.colAdjustUnEventUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colAdjustUnEventTime])) objInventory.AdjustUnEventTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colAdjustUnEventTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsReviewUnEvent])) objInventory.IsReviewUnEvent = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsReviewUnEvent]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colReviewUnEventUser])) objInventory.ReviewUnEventUser = Convert.ToString(reader[BO.Inventory.Inventory.colReviewUnEventUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colReviewUnEventTime])) objInventory.ReviewUnEventTime = Convert.ToDateTime(reader[BO.Inventory.Inventory.colReviewUnEventTime]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colCreatedStoreID])) objInventory.CreatedStoreID = Convert.ToInt32(reader[BO.Inventory.Inventory.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colCreatedUser])) objInventory.CreatedUser = Convert.ToString(reader[BO.Inventory.Inventory.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colCreatedDate])) objInventory.CreatedDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colUpdatedUser])) objInventory.UpdatedUser = Convert.ToString(reader[BO.Inventory.Inventory.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colUpdatedDate])) objInventory.UpdatedDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsDeleted])) objInventory.IsDeleted = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colDeletedUser])) objInventory.DeletedUser = Convert.ToString(reader[BO.Inventory.Inventory.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colDeletedDate])) objInventory.DeletedDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colDeletedDate]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colDeletedNote])) objInventory.DeletedNote = Convert.ToString(reader[BO.Inventory.Inventory.colDeletedNote]).Trim();

                    if (!Convert.IsDBNull(reader["CurrentReviewLevelID"])) objInventory.CurrentReviewLevelID = Convert.ToInt32(reader["CurrentReviewLevelID"]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colReviewStatusName])) objInventory.ReviewStatusName = Convert.ToString(reader[BO.Inventory.Inventory.colReviewStatusName]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colReviewStatusID])) objInventory.ReviewStatusID = Convert.ToInt32(reader[BO.Inventory.Inventory.colReviewStatusID]);


                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colIsExplainAccess])) objInventory.IsExplainAccess = Convert.ToBoolean(reader[BO.Inventory.Inventory.colIsExplainAccess]);
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colExplainAccessUser])) objInventory.ExplainAccessUser = Convert.ToString(reader[BO.Inventory.Inventory.colExplainAccessUser]).Trim();
                    if (!Convert.IsDBNull(reader[BO.Inventory.Inventory.colExplainAccessDate])) objInventory.ExplainAccessDate = Convert.ToDateTime(reader[BO.Inventory.Inventory.colExplainAccessDate]);

                }

                reader.Close();

                dtbInventoryDetail = LoadInventoryDetail(objIData, strInventoryID);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin phiếu kiểm kê
        /// </summary>
        /// <param name="objInventory">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(BO.Inventory.Inventory objInventory)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objInventory);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin phiếu kiểm kê
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInventory">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, BO.Inventory.Inventory objInventory)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryID, objInventory.InventoryID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryTermID, objInventory.InventoryTermID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryStoreID, objInventory.InventoryStoreID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colMainGroupID, objInventory.MainGroupIDList);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colSubGroupID, objInventory.SubGroupIDList);
               // objIData.AddParameter("@" + BO.Inventory.Inventory.colIsNew, objInventory.IsNew);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colProductStatusID, objInventory.ProductStatusID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryDate, objInventory.InventoryDate);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryUser, objInventory.InventoryUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colBeginInventoryTime, objInventory.BeginInventoryTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colEndInventoryTime, objInventory.EndInventoryTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colContent, objInventory.Content);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsReviewed, objInventory.IsReviewed);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsUpdateUnEvent, objInventory.IsUpdateUnEvent);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colUpdateUnEventUser, objInventory.UpdateUnEventUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colUpdateUnEventTime, objInventory.UpdateUnEventTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsLock, objInventory.IsLock);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colLockUser, objInventory.LockUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colLockTime, objInventory.LockTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsExplainUnEvent, objInventory.IsExplainUnEvent);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colExplainUnEventUser, objInventory.ExplainUnEventUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colExplainUnEventTime, objInventory.ExplainUnEventTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsAdjustUnEvent, objInventory.IsAdjustUnEvent);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colAdjustUnEventUser, objInventory.AdjustUnEventUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colAdjustUnEventTime, objInventory.AdjustUnEventTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsReviewUnEvent, objInventory.IsReviewUnEvent);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colReviewUnEventUser, objInventory.ReviewUnEventUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colReviewUnEventTime, objInventory.ReviewUnEventTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colCreatedStoreID, objInventory.CreatedStoreID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colCreatedUser, objInventory.CreatedUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colDeletedNote, objInventory.DeletedNote);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin phiếu kiểm kê
        /// </summary>
        /// <param name="objInventory">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(BO.Inventory.Inventory objInventory)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objInventory);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin phiếu kiểm kê
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInventory">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, BO.Inventory.Inventory objInventory)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryID, objInventory.InventoryID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryTermID, objInventory.InventoryTermID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryStoreID, objInventory.InventoryStoreID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colMainGroupID, objInventory.MainGroupIDList);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colSubGroupID, objInventory.SubGroupIDList);
                //objIData.AddParameter("@" + BO.Inventory.Inventory.colIsNew, objInventory.IsNew);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colProductStatusID, objInventory.ProductStatusID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryDate, objInventory.InventoryDate);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryUser, objInventory.InventoryUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colBeginInventoryTime, objInventory.BeginInventoryTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colEndInventoryTime, objInventory.EndInventoryTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colContent, objInventory.Content);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsReviewed, objInventory.IsReviewed);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsUpdateUnEvent, objInventory.IsUpdateUnEvent);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colUpdateUnEventUser, objInventory.UpdateUnEventUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colUpdateUnEventTime, objInventory.UpdateUnEventTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsLock, objInventory.IsLock);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colLockUser, objInventory.LockUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colLockTime, objInventory.LockTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsExplainUnEvent, objInventory.IsExplainUnEvent);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colExplainUnEventUser, objInventory.ExplainUnEventUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colExplainUnEventTime, objInventory.ExplainUnEventTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsAdjustUnEvent, objInventory.IsAdjustUnEvent);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colAdjustUnEventUser, objInventory.AdjustUnEventUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colAdjustUnEventTime, objInventory.AdjustUnEventTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colIsReviewUnEvent, objInventory.IsReviewUnEvent);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colReviewUnEventUser, objInventory.ReviewUnEventUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colReviewUnEventTime, objInventory.ReviewUnEventTime);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colCreatedStoreID, objInventory.CreatedStoreID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colUpdatedUser, objInventory.UpdatedUser);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colDeletedNote, objInventory.DeletedNote);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin phiếu kiểm kê
        /// </summary>
        /// <param name="objInventory">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(BO.Inventory.Inventory objInventory)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objInventory);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin phiếu kiểm kê", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_Inventory -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin phiếu kiểm kê
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInventory">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, BO.Inventory.Inventory objInventory)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colInventoryID, objInventory.InventoryID);
                objIData.AddParameter("@" + BO.Inventory.Inventory.colDeletedUser, objInventory.DeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_Inventory()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "Inventory_ADD";
        public const String SP_UPDATE = "Inventory_UPD";
        public const String SP_DELETE = "Inventory_DEL";
        public const String SP_SELECT = "INV_Inventory_SEL";
        public const String SP_SEARCH = "Inventory_SRH";
        public const String SP_UPDATEINDEX = "Inventory_UPDINDEX";
        #endregion

    }
}
