
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using IMEISalesInfo = ERP.Inventory.BO.IMEISalesInfo;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
    /// Created by 		: Nguyễn Quang Tú 
    /// Created date 	: 17/06/2013 
    /// Thông tin bán hàng của IMEI(dùng cho máy cũ, hàng trưng bày)
    /// </summary>	
    public partial class DA_IMEISalesInfo
    {


        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods


        /// <summary>
        /// Nạp thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIMEISalesInfo">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref IMEISalesInfo objIMEISalesInfo, string strIMEI, string strProductID)
        {
            objIMEISalesInfo = new IMEISalesInfo();
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + IMEISalesInfo.colIMEI, strIMEI);
                objIData.AddParameter("@" + IMEISalesInfo.colProductID, strProductID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colProductID])) objIMEISalesInfo.ProductID = Convert.ToString(reader[IMEISalesInfo.colProductID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colIMEI])) objIMEISalesInfo.IMEI = Convert.ToString(reader[IMEISalesInfo.colIMEI]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colInputVoucherDetailID])) objIMEISalesInfo.InputVoucherDetailID = Convert.ToString(reader[IMEISalesInfo.colInputVoucherDetailID]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colStoreID])) objIMEISalesInfo.StoreID = Convert.ToInt32(reader[IMEISalesInfo.colStoreID]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colIsBrandNewWarranty])) objIMEISalesInfo.IsBrandNewWarranty = Convert.ToBoolean(reader[IMEISalesInfo.colIsBrandNewWarranty]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colEndWarrantyDate])) objIMEISalesInfo.EndWarrantyDate = Convert.ToDateTime(reader[IMEISalesInfo.colEndWarrantyDate]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colNote])) objIMEISalesInfo.Note = Convert.ToString(reader[IMEISalesInfo.colNote]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colPrintVATContent])) objIMEISalesInfo.PrintVATContent = Convert.ToString(reader[IMEISalesInfo.colPrintVATContent]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colIsConfirm])) objIMEISalesInfo.IsConfirm = Convert.ToBoolean(reader[IMEISalesInfo.colIsConfirm]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colIsReviewed])) objIMEISalesInfo.IsReviewed = Convert.ToBoolean(reader[IMEISalesInfo.colIsReviewed]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colReviewedUser])) objIMEISalesInfo.ReviewedUser = Convert.ToString(reader[IMEISalesInfo.colReviewedUser]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colReviewedDate])) objIMEISalesInfo.ReviewedDate = Convert.ToDateTime(reader[IMEISalesInfo.colReviewedDate]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colCreatedUser])) objIMEISalesInfo.CreatedUser = Convert.ToString(reader[IMEISalesInfo.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colCreatedDate])) objIMEISalesInfo.CreatedDate = Convert.ToDateTime(reader[IMEISalesInfo.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colUpdatedUser])) objIMEISalesInfo.UpdatedUser = Convert.ToString(reader[IMEISalesInfo.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colUpdatedDate])) objIMEISalesInfo.UpdatedDate = Convert.ToDateTime(reader[IMEISalesInfo.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[IMEISalesInfo.colWebProductSpecContent])) objIMEISalesInfo.WebProductSpecContent = Convert.ToString(reader[IMEISalesInfo.colWebProductSpecContent]).Trim();
                    objIMEISalesInfo.IsExist = true;
                    objIMEISalesInfo.IMEISalesInfo_ProSpec = new DA_IMEISalesInfo_ProSpec().SearchDataToList(objIData, "@" + IMEISalesInfo.colProductID, objIMEISalesInfo.ProductID, "@" + IMEISalesInfo.colIMEI, objIMEISalesInfo.IMEI);
                    objIMEISalesInfo.IMEISalesInfo_ImagesList = new DA_IMEISalesInfo_Images().LoadInfoToListByIMEI(objIData, objIMEISalesInfo.ProductID, objIMEISalesInfo.IMEI);
                }
                else
                {
                    objIMEISalesInfo.IsExist = false;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(IMEISalesInfo objIMEISalesInfo)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objIMEISalesInfo);
                if (objIMEISalesInfo.IMEISalesInfo_ProSpec != null)
                {
                    DA.DA_IMEISalesInfo_ProSpec objDA_IMEISalesInfo_ProSpec = new DA_IMEISalesInfo_ProSpec();
                    foreach (BO.IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec in objIMEISalesInfo.IMEISalesInfo_ProSpec)
                    {
                        objIMEISalesInfo_ProSpec.IMEI = objIMEISalesInfo.IMEI;
                        objIMEISalesInfo_ProSpec.ProductID = objIMEISalesInfo.ProductID;
                        objDA_IMEISalesInfo_ProSpec.objLogObject.UserHostAddress = objLogObject.UserHostAddress;
                        objDA_IMEISalesInfo_ProSpec.objLogObject.CertificateString = objLogObject.CertificateString;
                        objDA_IMEISalesInfo_ProSpec.objLogObject.LoginLogID = objLogObject.LoginLogID;
                        objDA_IMEISalesInfo_ProSpec.Insert(objIData, objIMEISalesInfo_ProSpec);
                    }
                }
                if (objIMEISalesInfo.IMEISalesInfo_ImagesList != null)
                {
                    DA_IMEISalesInfo_Images objDA_IMEISalesInfo_Images = new DA_IMEISalesInfo_Images();
                    foreach (BO.IMEISalesInfo_Images objIMEISalesInfo_Images in objIMEISalesInfo.IMEISalesInfo_ImagesList)
                    {
                        objDA_IMEISalesInfo_Images.Insert(objIData, objIMEISalesInfo_Images);
                    }
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, IMEISalesInfo objIMEISalesInfo)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + IMEISalesInfo.colProductID, objIMEISalesInfo.ProductID);
                objIData.AddParameter("@" + IMEISalesInfo.colIMEI, objIMEISalesInfo.IMEI);
                objIData.AddParameter("@" + IMEISalesInfo.colInputVoucherDetailID, objIMEISalesInfo.InputVoucherDetailID);
                objIData.AddParameter("@" + IMEISalesInfo.colStoreID, objIMEISalesInfo.StoreID);
                objIData.AddParameter("@" + IMEISalesInfo.colIsBrandNewWarranty, objIMEISalesInfo.IsBrandNewWarranty);
                objIData.AddParameter("@" + IMEISalesInfo.colEndWarrantyDate, objIMEISalesInfo.EndWarrantyDate);
                objIData.AddParameter("@" + IMEISalesInfo.colNote, objIMEISalesInfo.Note);
                objIData.AddParameter("@" + IMEISalesInfo.colPrintVATContent, objIMEISalesInfo.PrintVATContent);
                objIData.AddParameter("@" + IMEISalesInfo.colIsConfirm, objIMEISalesInfo.IsConfirm);
                objIData.AddParameter("@" + IMEISalesInfo.colIsReviewed, objIMEISalesInfo.IsReviewed);
                objIData.AddParameter("@" + IMEISalesInfo.colReviewedUser, objIMEISalesInfo.ReviewedUser);
                objIData.AddParameter("@" + IMEISalesInfo.colReviewedDate, objIMEISalesInfo.ReviewedDate);
                objIData.AddParameter("@" + IMEISalesInfo.colCreatedUser, objIMEISalesInfo.CreatedUser);
                objIData.AddParameter("@" + IMEISalesInfo.colWebProductSpecContent, objIMEISalesInfo.WebProductSpecContent);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(IMEISalesInfo objIMEISalesInfo)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objIMEISalesInfo);
                if (objIMEISalesInfo.IMEISalesInfo_ProSpec != null)
                {
                    DA.DA_IMEISalesInfo_ProSpec objDA_IMEISalesInfo_ProSpec = new DA_IMEISalesInfo_ProSpec();
                    foreach (BO.IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec in objIMEISalesInfo.IMEISalesInfo_ProSpec)
                    {
                        objIMEISalesInfo_ProSpec.IMEI = objIMEISalesInfo.IMEI;
                        objIMEISalesInfo_ProSpec.ProductID = objIMEISalesInfo.ProductID;
                        objDA_IMEISalesInfo_ProSpec.objLogObject.UserHostAddress = objLogObject.UserHostAddress;
                        objDA_IMEISalesInfo_ProSpec.objLogObject.CertificateString = objLogObject.CertificateString;
                        objDA_IMEISalesInfo_ProSpec.objLogObject.LoginLogID = objLogObject.LoginLogID;
                        objDA_IMEISalesInfo_ProSpec.Insert(objIData, objIMEISalesInfo_ProSpec);
                    }
                }
                if (objIMEISalesInfo.IMEISalesInfo_ImagesList != null)
                {
                    DA_IMEISalesInfo_Images objDA_IMEISalesInfo_Images = new DA_IMEISalesInfo_Images();
                    objDA_IMEISalesInfo_Images.Delete(objIData, string.Empty, objIMEISalesInfo.ProductID, objIMEISalesInfo.IMEI);
                    foreach (BO.IMEISalesInfo_Images objIMEISalesInfo_Images in objIMEISalesInfo.IMEISalesInfo_ImagesList)
                    {
                        objDA_IMEISalesInfo_Images.Insert(objIData, objIMEISalesInfo_Images);
                    }
                }
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, IMEISalesInfo objIMEISalesInfo)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + IMEISalesInfo.colProductID, objIMEISalesInfo.ProductID);
                objIData.AddParameter("@" + IMEISalesInfo.colIMEI, objIMEISalesInfo.IMEI);
                objIData.AddParameter("@" + IMEISalesInfo.colInputVoucherDetailID, objIMEISalesInfo.InputVoucherDetailID);
                objIData.AddParameter("@" + IMEISalesInfo.colStoreID, objIMEISalesInfo.StoreID);
                objIData.AddParameter("@" + IMEISalesInfo.colIsBrandNewWarranty, objIMEISalesInfo.IsBrandNewWarranty);
                objIData.AddParameter("@" + IMEISalesInfo.colEndWarrantyDate, objIMEISalesInfo.EndWarrantyDate);
                objIData.AddParameter("@" + IMEISalesInfo.colNote, objIMEISalesInfo.Note);
                objIData.AddParameter("@" + IMEISalesInfo.colPrintVATContent, objIMEISalesInfo.PrintVATContent);
                objIData.AddParameter("@" + IMEISalesInfo.colIsConfirm, objIMEISalesInfo.IsConfirm);
                objIData.AddParameter("@" + IMEISalesInfo.colIsReviewed, objIMEISalesInfo.IsReviewed);
                objIData.AddParameter("@" + IMEISalesInfo.colReviewedUser, objIMEISalesInfo.ReviewedUser);
                objIData.AddParameter("@" + IMEISalesInfo.colReviewedDate, objIMEISalesInfo.ReviewedDate);
                objIData.AddParameter("@" + IMEISalesInfo.colUpdatedUser, objIMEISalesInfo.UpdatedUser);
                objIData.AddParameter("@" + IMEISalesInfo.colWebProductSpecContent, objIMEISalesInfo.WebProductSpecContent);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(IMEISalesInfo objIMEISalesInfo)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objIMEISalesInfo);

                DA.DA_IMEISalesInfo_ProSpec objDA_IMEISalesInfo_ProSpec = new DA_IMEISalesInfo_ProSpec();
                objDA_IMEISalesInfo_ProSpec.objLogObject.UserHostAddress = objLogObject.UserHostAddress;
                objDA_IMEISalesInfo_ProSpec.objLogObject.CertificateString = objLogObject.CertificateString;
                objDA_IMEISalesInfo_ProSpec.objLogObject.LoginLogID = objLogObject.LoginLogID;
                objDA_IMEISalesInfo_ProSpec.Delete(objIData, objIMEISalesInfo.ProductID, objIMEISalesInfo.IMEI, objIMEISalesInfo.UpdatedUser);

                DA_IMEISalesInfo_Images objDA_IMEISalesInfo_Images = new DA_IMEISalesInfo_Images();
                objDA_IMEISalesInfo_Images.Delete(objIData, string.Empty, objIMEISalesInfo.ProductID, objIMEISalesInfo.IMEI);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_IMEISalesInfo -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin thông tin bán hàng của imei(dùng cho máy cũ, hàng trưng bày)
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objIMEISalesInfo">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, IMEISalesInfo objIMEISalesInfo)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + IMEISalesInfo.colProductID, objIMEISalesInfo.ProductID);
                objIData.AddParameter("@" + IMEISalesInfo.colIMEI, objIMEISalesInfo.IMEI);
                objIData.AddParameter("@UpdatedUser", objIMEISalesInfo.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_IMEISalesInfo()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "PM_IMEISALESINFO_ADD";
        public const String SP_UPDATE = "PM_IMEISALESINFO_UPD";
        public const String SP_DELETE = "PM_IMEISALESINFO_DEL";
        public const String SP_SELECT = "PM_IMEISALESINFO_SEL";
        public const String SP_SEARCH = "PM_IMEISALESINFO_SRH";
        public const String SP_UPDATEINDEX = "PM_IMEISALESINFO_UPDINDEX";
        #endregion

    }
}
