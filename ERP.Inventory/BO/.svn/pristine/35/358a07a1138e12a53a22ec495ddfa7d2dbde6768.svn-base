﻿
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputVoucherReturn = ERP.Inventory.BO.Input.InputVoucherReturn;
#endregion
namespace ERP.Inventory.DA.Input
{
    /// <summary>
    /// Created by 		: Võ Minh Hiếu 
    /// Created date 	: 12/10/2012 
    /// Bảng phiếu nhập trả hàng
    /// </summary>	
    public partial class DA_InputVoucherReturn
    {


        #region Log Property
        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
        #endregion

        #region Methods


        /// <summary>
        /// Nạp thông tin bảng phiếu nhập trả hàng
        /// </summary>
        /// <param name="objInputVoucherReturn">Đối tượng trả về</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage LoadInfo(ref InputVoucherReturn objInputVoucherReturn, string strInputVoucherReturnID)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure(SP_SELECT);
                objIData.AddParameter("@" + InputVoucherReturn.colInputVoucherReturnID, strInputVoucherReturnID);
                IDataReader reader = objIData.ExecStoreToDataReader();
                if (objInputVoucherReturn == null)
                    objInputVoucherReturn = new InputVoucherReturn();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colInputVoucherReturnID])) objInputVoucherReturn.InputVoucherReturnID = Convert.ToString(reader[InputVoucherReturn.colInputVoucherReturnID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colOutputVoucherID])) objInputVoucherReturn.OutputVoucherID = Convert.ToString(reader[InputVoucherReturn.colOutputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colInputVoucherID])) objInputVoucherReturn.InputVoucherID = Convert.ToString(reader[InputVoucherReturn.colInputVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colOutVoucherID])) objInputVoucherReturn.OutVoucherID = Convert.ToString(reader[InputVoucherReturn.colOutVoucherID]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colReturnStoreID])) objInputVoucherReturn.ReturnStoreID = Convert.ToInt32(reader[InputVoucherReturn.colReturnStoreID]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colReturnDate])) objInputVoucherReturn.ReturnDate = Convert.ToDateTime(reader[InputVoucherReturn.colReturnDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colIsReturnWithFee])) objInputVoucherReturn.IsReturnWithFee = Convert.ToBoolean(reader[InputVoucherReturn.colIsReturnWithFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colReturnReason])) objInputVoucherReturn.ReturnReason = Convert.ToString(reader[InputVoucherReturn.colReturnReason]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colReturnNote])) objInputVoucherReturn.ReturnNote = Convert.ToString(reader[InputVoucherReturn.colReturnNote]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colDiscount])) objInputVoucherReturn.Discount = Convert.ToDecimal(reader[InputVoucherReturn.colDiscount]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colTotalAmountBFT])) objInputVoucherReturn.TotalAmountBFT = Convert.ToDecimal(reader[InputVoucherReturn.colTotalAmountBFT]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colTotalVAT])) objInputVoucherReturn.TotalVAT = Convert.ToDecimal(reader[InputVoucherReturn.colTotalVAT]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colTotalAmount])) objInputVoucherReturn.TotalAmount = Convert.ToDecimal(reader[InputVoucherReturn.colTotalAmount]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colTotalVATLost])) objInputVoucherReturn.TotalVATLost = Convert.ToDecimal(reader[InputVoucherReturn.colTotalVATLost]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colTotalReturnFee])) objInputVoucherReturn.TotalReturnFee = Convert.ToDecimal(reader[InputVoucherReturn.colTotalReturnFee]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colCreatedStoreID])) objInputVoucherReturn.CreatedStoreID = Convert.ToInt32(reader[InputVoucherReturn.colCreatedStoreID]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colCreatedUser])) objInputVoucherReturn.CreatedUser = Convert.ToString(reader[InputVoucherReturn.colCreatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colCreatedDate])) objInputVoucherReturn.CreatedDate = Convert.ToDateTime(reader[InputVoucherReturn.colCreatedDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colUpdatedUser])) objInputVoucherReturn.UpdatedUser = Convert.ToString(reader[InputVoucherReturn.colUpdatedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colUpdatedDate])) objInputVoucherReturn.UpdatedDate = Convert.ToDateTime(reader[InputVoucherReturn.colUpdatedDate]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colIsDeleted])) objInputVoucherReturn.IsDeleted = Convert.ToBoolean(reader[InputVoucherReturn.colIsDeleted]);
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colDeletedUser])) objInputVoucherReturn.DeletedUser = Convert.ToString(reader[InputVoucherReturn.colDeletedUser]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucherReturn.colDeletedDate])) objInputVoucherReturn.DeletedDate = Convert.ToDateTime(reader[InputVoucherReturn.colDeletedDate]);
                    objInputVoucherReturn.IsExist = true;
                }
                else
                {
                    objInputVoucherReturn.IsExist = false;
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin bảng phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> LoadInfo", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Thêm thông tin bảng phiếu nhập trả hàng
        /// </summary>
        /// <param name="objInputVoucherReturn">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Insert(InputVoucherReturn objInputVoucherReturn)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Insert(objIData, objInputVoucherReturn);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin bảng phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> Insert", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Thêm thông tin bảng phiếu nhập trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherReturn">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Insert(IData objIData, InputVoucherReturn objInputVoucherReturn)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);               
                objIData.AddParameter("@" + InputVoucherReturn.colOutputVoucherID, objInputVoucherReturn.OutputVoucherID);                
                objIData.AddParameter("@" + InputVoucherReturn.colOutVoucherID, objInputVoucherReturn.OutVoucherID);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnStoreID, objInputVoucherReturn.ReturnStoreID);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnDate, objInputVoucherReturn.ReturnDate);
                objIData.AddParameter("@" + InputVoucherReturn.colIsReturnWithFee, objInputVoucherReturn.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnReason, objInputVoucherReturn.ReturnReason);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnNote, objInputVoucherReturn.ReturnNote);
                objIData.AddParameter("@" + InputVoucherReturn.colDiscount, objInputVoucherReturn.Discount);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalAmountBFT, objInputVoucherReturn.TotalAmountBFT);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalVAT, objInputVoucherReturn.TotalVAT);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalAmount, objInputVoucherReturn.TotalAmount);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalVATLost, objInputVoucherReturn.TotalVATLost);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalReturnFee, objInputVoucherReturn.TotalReturnFee);


                objIData.AddParameter("@INVOICEID", objInputVoucherReturn.InVoiceID);//nn
                objIData.AddParameter("@INVOICESYMBOL", objInputVoucherReturn.InVoiceSymbol);//nn
                objIData.AddParameter("@DENOMINATOR", objInputVoucherReturn.Denominator);//nn
                objIData.AddParameter("@CUSTOMERID" , objInputVoucherReturn.CustomerID);//nn
                objIData.AddParameter("@CUSTOMERNAME", objInputVoucherReturn.CustomerName);
                objIData.AddParameter("@CUSTOMERADDRESS", objInputVoucherReturn.CustomerAddress);
                objIData.AddParameter("@CUSTOMERPHONE", objInputVoucherReturn.CustomerPhone);
                objIData.AddParameter("@CUSTOMERTAXID", objInputVoucherReturn.CustomerTaxID);

                objIData.AddParameter("@INPUTTYPEID", objInputVoucherReturn.InputTypeID);
                objIData.AddParameter("@PAYABLETYPEID", objInputVoucherReturn.PayableTypeID);
                objIData.AddParameter("@CURRENCYUNITID", objInputVoucherReturn.CurrencyUnitID);
                objIData.AddParameter("@CURRENCYEXCHANGE", objInputVoucherReturn.CurrencyExchange);
                objIData.AddParameter("@DISCOUNTREASONID", objInputVoucherReturn.DiscountReasonID);

                objIData.AddParameter("@ISNEW", objInputVoucherReturn.IsNew);

                objIData.AddParameter("@" + InputVoucherReturn.colCreatedStoreID, objInputVoucherReturn.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucherReturn.colCreatedUser, objInputVoucherReturn.CreatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);

                objIData.AddParameter("@InputVoucherID", objInputVoucherReturn.InputVoucherID);
                String strReturnValue = objIData.ExecStoreToString().Trim();
                String[] arrReturnValue = strReturnValue.Split(':');
                objInputVoucherReturn.InputVoucherReturnID = arrReturnValue[0].Trim();
                objInputVoucherReturn.InputVoucherID = arrReturnValue[1].Trim();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }

        /// <summary>
        /// Thêm thông tin bảng phiếu nhập trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherReturn">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void InsertViettel(IData objIData,ref BO.Input.InputVoucherReturn objInputVoucherReturn)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + InputVoucherReturn.colOutputVoucherID, objInputVoucherReturn.OutputVoucherID);
                objIData.AddParameter("@" + InputVoucherReturn.colOutVoucherID, objInputVoucherReturn.OutVoucherID);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnStoreID, objInputVoucherReturn.ReturnStoreID);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnDate, objInputVoucherReturn.ReturnDate);
                objIData.AddParameter("@" + InputVoucherReturn.colIsReturnWithFee, objInputVoucherReturn.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnReason, objInputVoucherReturn.ReturnReason);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnNote, objInputVoucherReturn.ReturnNote);
                objIData.AddParameter("@" + InputVoucherReturn.colDiscount, objInputVoucherReturn.Discount);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalAmountBFT, objInputVoucherReturn.TotalAmountBFT);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalVAT, objInputVoucherReturn.TotalVAT);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalAmount, objInputVoucherReturn.TotalAmount);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalVATLost, objInputVoucherReturn.TotalVATLost);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalReturnFee, objInputVoucherReturn.TotalReturnFee);


                objIData.AddParameter("@INVOICEID", objInputVoucherReturn.InVoiceID);//nn
                objIData.AddParameter("@INVOICESYMBOL", objInputVoucherReturn.InVoiceSymbol);//nn
                objIData.AddParameter("@DENOMINATOR", objInputVoucherReturn.Denominator);//nn
                objIData.AddParameter("@CUSTOMERID", objInputVoucherReturn.CustomerID);//nn
                objIData.AddParameter("@CUSTOMERNAME", objInputVoucherReturn.CustomerName);
                objIData.AddParameter("@CUSTOMERADDRESS", objInputVoucherReturn.CustomerAddress);
                objIData.AddParameter("@CUSTOMERPHONE", objInputVoucherReturn.CustomerPhone);
                objIData.AddParameter("@CUSTOMERTAXID", objInputVoucherReturn.CustomerTaxID);

                objIData.AddParameter("@INPUTTYPEID", objInputVoucherReturn.InputTypeID);
                objIData.AddParameter("@PAYABLETYPEID", objInputVoucherReturn.PayableTypeID);
                objIData.AddParameter("@CURRENCYUNITID", objInputVoucherReturn.CurrencyUnitID);
                objIData.AddParameter("@CURRENCYEXCHANGE", objInputVoucherReturn.CurrencyExchange);
                objIData.AddParameter("@DISCOUNTREASONID", objInputVoucherReturn.DiscountReasonID);

                objIData.AddParameter("@ISNEW", objInputVoucherReturn.IsNew);

                objIData.AddParameter("@" + InputVoucherReturn.colCreatedStoreID, objInputVoucherReturn.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucherReturn.colCreatedUser, objInputVoucherReturn.CreatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);

                objIData.AddParameter("@InputVoucherID", objInputVoucherReturn.InputVoucherID);
                String strReturnValue = objIData.ExecStoreToString().Trim();
                String[] arrReturnValue = strReturnValue.Split(':');
                objInputVoucherReturn.InputVoucherReturnID = arrReturnValue[0].Trim();
                objInputVoucherReturn.InputVoucherID = arrReturnValue[1].Trim();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Cập nhật thông tin bảng phiếu nhập trả hàng
        /// </summary>
        /// <param name="objInputVoucherReturn">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Update(InputVoucherReturn objInputVoucherReturn)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Update(objIData, objInputVoucherReturn);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin bảng phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> Update", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Cập nhật thông tin bảng phiếu nhập trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherReturn">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Update(IData objIData, InputVoucherReturn objInputVoucherReturn)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_UPDATE);
                objIData.AddParameter("@" + InputVoucherReturn.colInputVoucherReturnID, objInputVoucherReturn.InputVoucherReturnID);
                objIData.AddParameter("@" + InputVoucherReturn.colOutputVoucherID, objInputVoucherReturn.OutputVoucherID);
                objIData.AddParameter("@" + InputVoucherReturn.colInputVoucherID, objInputVoucherReturn.InputVoucherID);
                objIData.AddParameter("@" + InputVoucherReturn.colOutVoucherID, objInputVoucherReturn.OutVoucherID);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnStoreID, objInputVoucherReturn.ReturnStoreID);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnDate, objInputVoucherReturn.ReturnDate);
                objIData.AddParameter("@" + InputVoucherReturn.colIsReturnWithFee, objInputVoucherReturn.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnReason, objInputVoucherReturn.ReturnReason);
                objIData.AddParameter("@" + InputVoucherReturn.colReturnNote, objInputVoucherReturn.ReturnNote);
                objIData.AddParameter("@" + InputVoucherReturn.colDiscount, objInputVoucherReturn.Discount);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalAmountBFT, objInputVoucherReturn.TotalAmountBFT);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalVAT, objInputVoucherReturn.TotalVAT);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalAmount, objInputVoucherReturn.TotalAmount);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalVATLost, objInputVoucherReturn.TotalVATLost);
                objIData.AddParameter("@" + InputVoucherReturn.colTotalReturnFee, objInputVoucherReturn.TotalReturnFee);
                objIData.AddParameter("@" + InputVoucherReturn.colCreatedStoreID, objInputVoucherReturn.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucherReturn.colUpdatedUser, objInputVoucherReturn.UpdatedUser);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }


        /// <summary>
        /// Xóa thông tin bảng phiếu nhập trả hàng
        /// </summary>
        /// <param name="objInputVoucherReturn">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public ResultMessage Delete(InputVoucherReturn objInputVoucherReturn)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                Delete(objIData, objInputVoucherReturn);
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin bảng phiếu nhập trả hàng", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucherReturn -> Delete", InventoryGlobals.ModuleName);
                return objResultMessage;
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }


        /// <summary>
        /// Xóa thông tin bảng phiếu nhập trả hàng
        /// </summary>
        /// <param name="objIData">Đối tượng Kết nối CSDL</param>
        /// <param name="objInputVoucherReturn">Đối tượng truyền vào</param>
        /// <returns>Kết quả trả về</returns>
        public void Delete(IData objIData, InputVoucherReturn objInputVoucherReturn)
        {
            try
            {
                objIData.CreateNewStoredProcedure(SP_DELETE);
                objIData.AddParameter("@" + InputVoucherReturn.colInputVoucherReturnID, objInputVoucherReturn.InputVoucherReturnID);
                objIData.AddParameter("@" + InputVoucherReturn.colDeletedUser, objInputVoucherReturn.DeletedUser);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_InputVoucherReturn()
        {
        }
        #endregion


        #region Stored Procedure Names

        public const String SP_ADD = "PM_INPUTVOUCHERRETURN_ADD";
        public const String SP_UPDATE = "PM_INPUTVOUCHERRETURN_UPD";
        public const String SP_DELETE = "PM_INPUTVOUCHERRETURN_DEL";
        public const String SP_SELECT = "PM_INPUTVOUCHERRETURN_SEL";
        public const String SP_SEARCH = "PM_INPUTVOUCHERRETURN_SRH";
        public const String SP_UPDATEINDEX = "PM_INPUTVOUCHERRETURN_UPDINDEX";
        #endregion

    }
}


