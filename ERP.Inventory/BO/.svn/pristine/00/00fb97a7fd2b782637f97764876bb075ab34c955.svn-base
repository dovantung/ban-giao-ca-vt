
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using StoreChangeCommand = ERP.Inventory.BO.StoreChangeCommand;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 11/22/2012 
	/// Danh sách lệnh chuyển kho được tính lúc chia hàng
	/// </summary>	
	public partial class DA_StoreChangeCommand
	{	
	
		

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin danh sách lệnh chuyển kho được tính lúc chia hàng
		/// </summary>
		/// <param name="objStoreChangeCommand">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref StoreChangeCommand objStoreChangeCommand, string strStoreChangeCommandID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + StoreChangeCommand.colStoreChangeCommandID, strStoreChangeCommandID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objStoreChangeCommand = new StoreChangeCommand();
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colStoreChangeCommandID])) objStoreChangeCommand.StoreChangeCommandID = Convert.ToString(reader[StoreChangeCommand.colStoreChangeCommandID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colStoreChangeCommandTypeID])) objStoreChangeCommand.StoreChangeCommandTypeID = Convert.ToInt32(reader[StoreChangeCommand.colStoreChangeCommandTypeID]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colFromStoreID])) objStoreChangeCommand.FromStoreID = Convert.ToInt32(reader[StoreChangeCommand.colFromStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colToStoreID])) objStoreChangeCommand.ToStoreID = Convert.ToInt32(reader[StoreChangeCommand.colToStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colContent])) objStoreChangeCommand.Content = Convert.ToString(reader[StoreChangeCommand.colContent]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colNote])) objStoreChangeCommand.Note = Convert.ToString(reader[StoreChangeCommand.colNote]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colISURGENT])) objStoreChangeCommand.IsUrgent = Convert.ToBoolean(reader[StoreChangeCommand.colISURGENT]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colIsCreateStoreChangeOrder])) objStoreChangeCommand.IsCreateStoreChangeOrder = Convert.ToBoolean(reader[StoreChangeCommand.colIsCreateStoreChangeOrder]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colStoreChangeOrderID])) objStoreChangeCommand.StoreChangeOrderID = Convert.ToString(reader[StoreChangeCommand.colStoreChangeOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colCreatedStoreID])) objStoreChangeCommand.CreatedStoreID = Convert.ToInt32(reader[StoreChangeCommand.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colCreatedUser])) objStoreChangeCommand.CreatedUser = Convert.ToString(reader[StoreChangeCommand.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colCreatedDate])) objStoreChangeCommand.CreatedDate = Convert.ToDateTime(reader[StoreChangeCommand.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colUpdatedUser])) objStoreChangeCommand.UpdatedUser = Convert.ToString(reader[StoreChangeCommand.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colUpdatedDate])) objStoreChangeCommand.UpdatedDate = Convert.ToDateTime(reader[StoreChangeCommand.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colIsDeleted])) objStoreChangeCommand.IsDeleted = Convert.ToBoolean(reader[StoreChangeCommand.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colDeletedUser])) objStoreChangeCommand.DeletedUser = Convert.ToString(reader[StoreChangeCommand.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[StoreChangeCommand.colDeletedDate])) objStoreChangeCommand.DeletedDate = Convert.ToDateTime(reader[StoreChangeCommand.colDeletedDate]);
 				}
 				else
 				{
 					objStoreChangeCommand = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin danh sách lệnh chuyển kho được tính lúc chia hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommand -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin danh sách lệnh chuyển kho được tính lúc chia hàng
		/// </summary>
		/// <param name="objStoreChangeCommand">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(StoreChangeCommand objStoreChangeCommand)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objStoreChangeCommand);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin danh sách lệnh chuyển kho được tính lúc chia hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommand -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin danh sách lệnh chuyển kho được tính lúc chia hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeCommand">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, StoreChangeCommand objStoreChangeCommand)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + StoreChangeCommand.colStoreChangeCommandID, objStoreChangeCommand.StoreChangeCommandID);
				objIData.AddParameter("@" + StoreChangeCommand.colStoreChangeCommandTypeID, objStoreChangeCommand.StoreChangeCommandTypeID);
				objIData.AddParameter("@" + StoreChangeCommand.colFromStoreID, objStoreChangeCommand.FromStoreID);
				objIData.AddParameter("@" + StoreChangeCommand.colToStoreID, objStoreChangeCommand.ToStoreID);
				objIData.AddParameter("@" + StoreChangeCommand.colContent, objStoreChangeCommand.Content);
				objIData.AddParameter("@" + StoreChangeCommand.colNote, objStoreChangeCommand.Note);
				objIData.AddParameter("@" + StoreChangeCommand.colISURGENT, objStoreChangeCommand.IsUrgent);
				objIData.AddParameter("@" + StoreChangeCommand.colIsCreateStoreChangeOrder, objStoreChangeCommand.IsCreateStoreChangeOrder);
				objIData.AddParameter("@" + StoreChangeCommand.colStoreChangeOrderID, objStoreChangeCommand.StoreChangeOrderID);
				objIData.AddParameter("@" + StoreChangeCommand.colCreatedStoreID, objStoreChangeCommand.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeCommand.colCreatedUser, objStoreChangeCommand.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin danh sách lệnh chuyển kho được tính lúc chia hàng
		/// </summary>
		/// <param name="objStoreChangeCommand">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(StoreChangeCommand objStoreChangeCommand)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objStoreChangeCommand);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin danh sách lệnh chuyển kho được tính lúc chia hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommand -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin danh sách lệnh chuyển kho được tính lúc chia hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeCommand">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, StoreChangeCommand objStoreChangeCommand)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + StoreChangeCommand.colStoreChangeCommandID, objStoreChangeCommand.StoreChangeCommandID);
				objIData.AddParameter("@" + StoreChangeCommand.colStoreChangeCommandTypeID, objStoreChangeCommand.StoreChangeCommandTypeID);
				objIData.AddParameter("@" + StoreChangeCommand.colFromStoreID, objStoreChangeCommand.FromStoreID);
				objIData.AddParameter("@" + StoreChangeCommand.colToStoreID, objStoreChangeCommand.ToStoreID);
				objIData.AddParameter("@" + StoreChangeCommand.colContent, objStoreChangeCommand.Content);
				objIData.AddParameter("@" + StoreChangeCommand.colNote, objStoreChangeCommand.Note);
				objIData.AddParameter("@" + StoreChangeCommand.colISURGENT, objStoreChangeCommand.IsUrgent);
				objIData.AddParameter("@" + StoreChangeCommand.colIsCreateStoreChangeOrder, objStoreChangeCommand.IsCreateStoreChangeOrder);
				objIData.AddParameter("@" + StoreChangeCommand.colStoreChangeOrderID, objStoreChangeCommand.StoreChangeOrderID);
				objIData.AddParameter("@" + StoreChangeCommand.colCreatedStoreID, objStoreChangeCommand.CreatedStoreID);
				objIData.AddParameter("@" + StoreChangeCommand.colUpdatedUser, objStoreChangeCommand.UpdatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin danh sách lệnh chuyển kho được tính lúc chia hàng
		/// </summary>
		/// <param name="objStoreChangeCommand">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(StoreChangeCommand objStoreChangeCommand)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objStoreChangeCommand);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin danh sách lệnh chuyển kho được tính lúc chia hàng", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_StoreChangeCommand -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin danh sách lệnh chuyển kho được tính lúc chia hàng
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objStoreChangeCommand">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, StoreChangeCommand objStoreChangeCommand)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + StoreChangeCommand.colStoreChangeCommandID, objStoreChangeCommand.StoreChangeCommandID);
                objIData.AddParameter("@" + StoreChangeCommand.colNote, objStoreChangeCommand.Note);
				objIData.AddParameter("@" + StoreChangeCommand.colDeletedUser, objStoreChangeCommand.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_StoreChangeCommand()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_STORECHANGECOMMAND_ADD";
		public const String SP_UPDATE = "PM_STORECHANGECOMMAND_UPD";
		public const String SP_DELETE = "PM_STORECHANGECOMMAND_DEL";
		public const String SP_SELECT = "PM_STORECHANGECOMMAND_SEL";
		public const String SP_SEARCH = "PM_STORECHANGECOMMAND_SRH";
		public const String SP_SEARCHCOMPANY = "MD_TRANSPORTCOMPANY_SHRBYID";
        public const String SP_UPDATEINDEX = "PM_STORECHANGECOMMAND_UPDINDEX";
		#endregion
		
	}
}
