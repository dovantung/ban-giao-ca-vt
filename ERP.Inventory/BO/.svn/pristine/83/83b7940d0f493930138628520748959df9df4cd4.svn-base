
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using SplitProduct = ERP.Inventory.BO.SplitProduct;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 11/6/14 
	/// Tách linh kiện từ sản phẩm
	/// </summary>	
	public partial class DA_SplitProduct
	{


        public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#region Methods			
		

		/// <summary>
		/// Nạp thông tin tách linh kiện từ sản phẩm
		/// </summary>
		/// <param name="objSplitProduct">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage LoadInfo(ref SplitProduct objSplitProduct, string strSplitProductID)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + SplitProduct.colSplitProductID, strSplitProductID);
				IDataReader reader = objIData.ExecStoreToDataReader();
				if (reader.Read())
 				{
 					objSplitProduct = new SplitProduct();
 					if (!Convert.IsDBNull(reader[SplitProduct.colSplitProductID])) objSplitProduct.SplitProductID = Convert.ToString(reader[SplitProduct.colSplitProductID]).Trim();
 					if (!Convert.IsDBNull(reader[SplitProduct.colProductID])) objSplitProduct.ProductID = Convert.ToString(reader[SplitProduct.colProductID]).Trim();
 					if (!Convert.IsDBNull(reader[SplitProduct.colIMEI])) objSplitProduct.IMEI = Convert.ToString(reader[SplitProduct.colIMEI]).Trim();
 					if (!Convert.IsDBNull(reader[SplitProduct.colPrice])) objSplitProduct.Price = Convert.ToDecimal(reader[SplitProduct.colPrice]);
 					if (!Convert.IsDBNull(reader[SplitProduct.colSplitContent])) objSplitProduct.SplitContent = Convert.ToString(reader[SplitProduct.colSplitContent]).Trim();
 					if (!Convert.IsDBNull(reader[SplitProduct.colOutputVoucherID])) objSplitProduct.OutputVoucherID = Convert.ToString(reader[SplitProduct.colOutputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[SplitProduct.colInputVoucherID1])) objSplitProduct.InputVoucherID1 = Convert.ToString(reader[SplitProduct.colInputVoucherID1]).Trim();
 					if (!Convert.IsDBNull(reader[SplitProduct.colInputVoucherID2])) objSplitProduct.InputVoucherID2 = Convert.ToString(reader[SplitProduct.colInputVoucherID2]).Trim();
 					if (!Convert.IsDBNull(reader[SplitProduct.colCreatedStoreID])) objSplitProduct.CreatedStoreID = Convert.ToInt32(reader[SplitProduct.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[SplitProduct.colCreatedUser])) objSplitProduct.CreatedUser = Convert.ToString(reader[SplitProduct.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[SplitProduct.colCreatedDate])) objSplitProduct.CreatedDate = Convert.ToDateTime(reader[SplitProduct.colCreatedDate]);
 					if (!Convert.IsDBNull(reader[SplitProduct.colIsDeleted])) objSplitProduct.IsDeleted = Convert.ToBoolean(reader[SplitProduct.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[SplitProduct.colDeletedUser])) objSplitProduct.DeletedUser = Convert.ToString(reader[SplitProduct.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[SplitProduct.colDeletedDate])) objSplitProduct.DeletedDate = Convert.ToDateTime(reader[SplitProduct.colDeletedDate]);
 				}
 				else
 				{
 					objSplitProduct = null;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.LoadInfo, "Lỗi nạp thông tin tách linh kiện từ sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SplitProduct -> LoadInfo", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}

		/// <summary>
		/// Thêm thông tin tách linh kiện từ sản phẩm
		/// </summary>
		/// <param name="objSplitProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(SplitProduct objSplitProduct)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objSplitProduct);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin tách linh kiện từ sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SplitProduct -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin tách linh kiện từ sản phẩm
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objSplitProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, SplitProduct objSplitProduct)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
				objIData.AddParameter("@" + SplitProduct.colSplitProductID, objSplitProduct.SplitProductID);
				objIData.AddParameter("@" + SplitProduct.colProductID, objSplitProduct.ProductID);
				objIData.AddParameter("@" + SplitProduct.colIMEI, objSplitProduct.IMEI);
				objIData.AddParameter("@" + SplitProduct.colPrice, objSplitProduct.Price);
				objIData.AddParameter("@" + SplitProduct.colSplitContent, objSplitProduct.SplitContent);
				objIData.AddParameter("@" + SplitProduct.colOutputVoucherID, objSplitProduct.OutputVoucherID);
				objIData.AddParameter("@" + SplitProduct.colInputVoucherID1, objSplitProduct.InputVoucherID1);
				objIData.AddParameter("@" + SplitProduct.colInputVoucherID2, objSplitProduct.InputVoucherID2);
				objIData.AddParameter("@" + SplitProduct.colCreatedStoreID, objSplitProduct.CreatedStoreID);
				objIData.AddParameter("@" + SplitProduct.colCreatedUser, objSplitProduct.CreatedUser);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Cập nhật thông tin tách linh kiện từ sản phẩm
		/// </summary>
		/// <param name="objSplitProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(SplitProduct objSplitProduct)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Update(objIData, objSplitProduct);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin tách linh kiện từ sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SplitProduct -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin tách linh kiện từ sản phẩm
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objSplitProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, SplitProduct objSplitProduct)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + SplitProduct.colSplitProductID, objSplitProduct.SplitProductID);
				objIData.AddParameter("@" + SplitProduct.colProductID, objSplitProduct.ProductID);
				objIData.AddParameter("@" + SplitProduct.colIMEI, objSplitProduct.IMEI);
				objIData.AddParameter("@" + SplitProduct.colPrice, objSplitProduct.Price);
				objIData.AddParameter("@" + SplitProduct.colSplitContent, objSplitProduct.SplitContent);
				objIData.AddParameter("@" + SplitProduct.colOutputVoucherID, objSplitProduct.OutputVoucherID);
				objIData.AddParameter("@" + SplitProduct.colInputVoucherID1, objSplitProduct.InputVoucherID1);
				objIData.AddParameter("@" + SplitProduct.colInputVoucherID2, objSplitProduct.InputVoucherID2);
				objIData.AddParameter("@" + SplitProduct.colCreatedStoreID, objSplitProduct.CreatedStoreID);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin tách linh kiện từ sản phẩm
		/// </summary>
		/// <param name="objSplitProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(SplitProduct objSplitProduct)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Delete(objIData, objSplitProduct);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin tách linh kiện từ sản phẩm", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_SplitProduct -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin tách linh kiện từ sản phẩm
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objSplitProduct">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Delete(IData objIData, SplitProduct objSplitProduct)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + SplitProduct.colSplitProductID, objSplitProduct.SplitProductID);
				objIData.AddParameter("@" + SplitProduct.colDeletedUser, objSplitProduct.DeletedUser);
 				objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}
		#endregion

		
		#region Constructor

		public DA_SplitProduct()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_SplitPRODUCT_ADD";
		public const String SP_UPDATE = "PM_SplitPRODUCT_UPD";
		public const String SP_DELETE = "PM_SplitPRODUCT_DEL";
		public const String SP_SELECT = "PM_SplitPRODUCT_SEL";
		public const String SP_SEARCH = "PM_SplitPRODUCT_SRH";
		public const String SP_UPDATEINDEX = "PM_SplitPRODUCT_UPDINDEX";
		#endregion
		
	}
}
