
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
namespace ERP.Inventory.BO.ProductChange
{
    /// <summary>
	/// Created by 		: Phan Tiến Lộc 
	/// Created date 	: 2016-11-15 
	/// Yêu cầu xuất đổi - Mức duyệt
	/// </summary>	
	public class ProductChangeOrder_RVL
	{	
	
	
		#region Member Variables

		private string strProductChangeOrderRVLID = string.Empty;
		private string strProductChangeOrderID;
		private int intReviewLevelID = 0;
		private string strUserName = string.Empty;
		private int intReviewedStatus = 0;
		private DateTime? dtmReviewedDate;
		private string strNote = string.Empty;
		private int intOrderIndex = 0;
		private string strCreatedUser = string.Empty;
		private DateTime? dtmCreatedDate;
		private bool bolIsDeleted = false;
		private string strDeletedUser = string.Empty;
		private DateTime? dtmDeletedDate;
        private int intSequenceReview;
        private string strApprovePermission = string.Empty;
        private string strRejectPermission = string.Empty;
        private int intStorePermissionType = 0;



        public int StorePermissionType
        {
            get { return intStorePermissionType; }
            set { intStorePermissionType = value; }
        }

        public string RejectPermission
        {
            get { return strRejectPermission; }
            set { strRejectPermission = value; }
        }

        public string ApprovePermission
        {
            get { return strApprovePermission; }
            set { strApprovePermission = value; }
        }
        public int SequenceReview
        {
            get { return intSequenceReview; }
            set { intSequenceReview = value; }
        }

        private string strReviewType;

        public string ReviewType
        {
            get { return strReviewType; }
            set { strReviewType = value; }
        }


        private bool bolCheck;

        public bool Check
        {
            get { return bolCheck; }
            set { bolCheck = value; }
        }

        private string strReviewLevelName;        
        public string ReviewLevelName
        {
            get { return strReviewLevelName; }
            set { strReviewLevelName = value; }
        }
        private string strFullName;

        public string FullName
        {
            get { return strFullName; }
            set { strFullName = value; }
        }

        #endregion


        #region Properties 

        /// <summary>
        /// ProductChangeOrderRVLID
        /// Mã chi tiết - Mức duyệt
        /// </summary>
        public string ProductChangeOrderRVLID
		{
			get { return  strProductChangeOrderRVLID; }
			set { strProductChangeOrderRVLID = value; }
		}

		/// <summary>
		/// ProductChangeOrderID
		/// Mã yêu cầu
		/// </summary>
		public string ProductChangeOrderID
		{
			get { return  strProductChangeOrderID; }
			set { strProductChangeOrderID = value; }
		}

		/// <summary>
		/// ReviewLevelID
		/// Mã mức duyệt
		/// </summary>
		public int ReviewLevelID
		{
			get { return  intReviewLevelID; }
			set { intReviewLevelID = value; }
		}

		/// <summary>
		/// UserName
		/// Nhân viên duyệt
		/// </summary>
		public string UserName
		{
			get { return  strUserName; }
			set { strUserName = value; }
		}

		/// <summary>
		/// ReviewedStatus
		/// Trạng thái duyệt: -1-Đang sử lý; 0-Từ chối; 1-Đồng ý
		/// </summary>
		public int ReviewedStatus
		{
			get { return  intReviewedStatus; }
			set { intReviewedStatus = value; }
		}

		/// <summary>
		/// ReviewedDate
		/// Ngày duyệt
		/// </summary>
		public DateTime? ReviewedDate
		{
			get { return  dtmReviewedDate; }
			set { dtmReviewedDate = value; }
		}

		/// <summary>
		/// Note
		/// Ghi chú
		/// </summary>
		public string Note
		{
			get { return  strNote; }
			set { strNote = value; }
		}

		/// <summary>
		/// OrderIndex
		/// Thứ tự duyệt
		/// </summary>
		public int OrderIndex
		{
			get { return  intOrderIndex; }
			set { intOrderIndex = value; }
		}

		/// <summary>
		/// CreatedUser
		/// Người tạo
		/// </summary>
		public string CreatedUser
		{
			get { return  strCreatedUser; }
			set { strCreatedUser = value; }
		}

		/// <summary>
		/// CreatedDate
		/// Ngày tạo
		/// </summary>
		public DateTime? CreatedDate
		{
			get { return  dtmCreatedDate; }
			set { dtmCreatedDate = value; }
		}

		/// <summary>
		/// IsDeleted
		/// Đã xóa
		/// </summary>
		public bool IsDeleted
		{
			get { return  bolIsDeleted; }
			set { bolIsDeleted = value; }
		}

		/// <summary>
		/// DeletedUser
		/// Nhân viên xóa
		/// </summary>
		public string DeletedUser
		{
			get { return  strDeletedUser; }
			set { strDeletedUser = value; }
		}

		/// <summary>
		/// DeletedDate
		/// Ngày xóa
		/// </summary>
		public DateTime? DeletedDate
		{
			get { return  dtmDeletedDate; }
			set { dtmDeletedDate = value; }
		}


		#endregion			
		
		
		#region Constructor

		public ProductChangeOrder_RVL()
		{
		}
		#endregion


		#region Column Names

		public const String colProductChangeOrderRVLID = "ProductChangeOrderRVLID";
		public const String colProductChangeOrderID = "ProductChangeOrderID";
		public const String colReviewLevelID = "ReviewLevelID";
		public const String colUserName = "UserName"; 
        public const String colFullName = "FullName";
        public const String colReviewLevelName = "ReviewLevelName";
        public const String colReviewType = "ReviewType";
        public const String colReviewedStatus = "ReviewedStatus";
		public const String colReviewedDate = "ReviewedDate";
		public const String colNote = "Note";
		public const String colOrderIndex = "OrderIndex";
		public const String colCreatedUser = "CreatedUser";
		public const String colCreatedDate = "CreatedDate";
		public const String colIsDeleted = "IsDeleted";
		public const String colDeletedUser = "DeletedUser";
		public const String colDeletedDate = "DeletedDate";
        public const String colSequenceReview = "SequenceReview";
        public const String colApprovePermission = "ApprovePermission";
        public const String colRejectPermission = "RejectPermission";
        public const String colStorePermissionType = "StorePermissionType";

        #endregion //Column names


    }
}
