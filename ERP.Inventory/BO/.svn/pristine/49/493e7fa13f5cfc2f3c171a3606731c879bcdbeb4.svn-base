
#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Library.DataAccess;
using Library.WebCore;
using InputVoucher = ERP.Inventory.BO.InputVoucher;
#endregion
namespace ERP.Inventory.DA
{
    /// <summary>
	/// Created by 		: Nguyễn Linh Tuấn 
	/// Created date 	: 8/13/2012 
	/// Phiếu nhập
	/// </summary>	
	public partial class DA_InputVoucher
	{	
	
		
		#region Log Property
		public Library.WebCore.LogObject objLogObject = new Library.WebCore.LogObject();
		#endregion

		#region Methods			
		

		/// <summary>
		/// Nạp thông tin phiếu nhập
		/// </summary>
		/// <param name="objInputVoucher">Đối tượng trả về</param>
		/// <returns>Kết quả trả về</returns>
		public InputVoucher LoadInfo(string strInputVoucherID)
		{
			IData objIData = Library.DataAccess.Data.CreateData();
            InputVoucher objInputVoucher = new InputVoucher();
			try 
			{
				objIData.Connect();
				objIData.CreateNewStoredProcedure(SP_SELECT);
				objIData.AddParameter("@" + InputVoucher.colInputVoucherID, strInputVoucherID);
				IDataReader reader = objIData.ExecStoreToDataReader();
                
				if (reader.Read())
 				{
                    
 					if (objInputVoucher == null) 
 						objInputVoucher = new InputVoucher();
 					if (!Convert.IsDBNull(reader[InputVoucher.colInputVoucherID])) objInputVoucher.InputVoucherID = Convert.ToString(reader[InputVoucher.colInputVoucherID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colOrderID])) objInputVoucher.OrderID = Convert.ToString(reader[InputVoucher.colOrderID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colInVoiceID])) objInputVoucher.InVoiceID = Convert.ToString(reader[InputVoucher.colInVoiceID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colInVoiceSymbol])) objInputVoucher.InVoiceSymbol = Convert.ToString(reader[InputVoucher.colInVoiceSymbol]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colDENOMinAToR])) objInputVoucher.Denominator = Convert.ToString(reader[InputVoucher.colDENOMinAToR]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colCustomerID])) objInputVoucher.CustomerID = Convert.ToInt32(reader[InputVoucher.colCustomerID]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colCustomerName])) objInputVoucher.CustomerName = Convert.ToString(reader[InputVoucher.colCustomerName]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colCustomerAddress])) objInputVoucher.CustomerAddress = Convert.ToString(reader[InputVoucher.colCustomerAddress]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colCustomerPhone])) objInputVoucher.CustomerPhone = Convert.ToString(reader[InputVoucher.colCustomerPhone]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colCustomerTaxID])) objInputVoucher.CustomerTaxID = Convert.ToString(reader[InputVoucher.colCustomerTaxID]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colCustomerEmail])) objInputVoucher.CustomerEmail = Convert.ToString(reader[InputVoucher.colCustomerEmail]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colCustomerBirthday])) objInputVoucher.CustomerBirthday = Convert.ToDateTime(reader[InputVoucher.colCustomerBirthday]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colCustomerIDCard])) objInputVoucher.CustomerIDCard = Convert.ToString(reader[InputVoucher.colCustomerIDCard]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colContent])) objInputVoucher.Content = Convert.ToString(reader[InputVoucher.colContent]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colCreatedStoreID])) objInputVoucher.CreatedStoreID = Convert.ToInt32(reader[InputVoucher.colCreatedStoreID]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colInputStoreID])) objInputVoucher.InputStoreID = Convert.ToInt32(reader[InputVoucher.colInputStoreID]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colAutoStoreChangeToStoreID])) objInputVoucher.AutoStoreChangeToStoreID = Convert.ToInt32(reader[InputVoucher.colAutoStoreChangeToStoreID]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colInputTypeID])) objInputVoucher.InputTypeID = Convert.ToInt32(reader[InputVoucher.colInputTypeID]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colPayableTypeID])) objInputVoucher.PayableTypeID = Convert.ToInt32(reader[InputVoucher.colPayableTypeID]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colCurrencyUnitID])) objInputVoucher.CurrencyUnitID = Convert.ToInt32(reader[InputVoucher.colCurrencyUnitID]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colDiscountReasonID])) objInputVoucher.DiscountReasonID = Convert.ToInt32(reader[InputVoucher.colDiscountReasonID]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colCreateDate])) objInputVoucher.CreateDate = Convert.ToDateTime(reader[InputVoucher.colCreateDate]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colInVoiceDate])) objInputVoucher.InVoiceDate = Convert.ToDateTime(reader[InputVoucher.colInVoiceDate]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colInputDate])) objInputVoucher.InputDate = Convert.ToDateTime(reader[InputVoucher.colInputDate]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colPayABLedate])) objInputVoucher.PayableDate = Convert.ToDateTime(reader[InputVoucher.colPayABLedate]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colTaxMonth])) objInputVoucher.TaxMonth = Convert.ToDateTime(reader[InputVoucher.colTaxMonth]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colCurrencyExchange])) objInputVoucher.CurrencyExchange = Convert.ToDecimal(reader[InputVoucher.colCurrencyExchange]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colDiscount])) objInputVoucher.Discount = Convert.ToDecimal(reader[InputVoucher.colDiscount]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colPROTECTPriceDiscount])) objInputVoucher.ProtectPriceDiscount = Convert.ToDecimal(reader[InputVoucher.colPROTECTPriceDiscount]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colTotalAmountBFT])) objInputVoucher.TotalAmountBFT = Convert.ToDecimal(reader[InputVoucher.colTotalAmountBFT]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colTotalVAT])) objInputVoucher.TotalVAT = Convert.ToDecimal(reader[InputVoucher.colTotalVAT]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colTotalAmount])) objInputVoucher.TotalAmount = Convert.ToDecimal(reader[InputVoucher.colTotalAmount]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colCreatedUser])) objInputVoucher.CreatedUser = Convert.ToString(reader[InputVoucher.colCreatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colStaffUser])) objInputVoucher.StaffUser = Convert.ToString(reader[InputVoucher.colStaffUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colIsNew])) objInputVoucher.IsNew = Convert.ToBoolean(reader[InputVoucher.colIsNew]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colIsReturnWithFee])) objInputVoucher.IsReturnWithFee = Convert.ToBoolean(reader[InputVoucher.colIsReturnWithFee]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colISStoreChange])) objInputVoucher.ISStoreChange = Convert.ToBoolean(reader[InputVoucher.colISStoreChange]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colIsCheckRealInput])) objInputVoucher.IsCheckRealInput = Convert.ToBoolean(reader[InputVoucher.colIsCheckRealInput]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colISPOSTED])) objInputVoucher.IsPosted = Convert.ToBoolean(reader[InputVoucher.colISPOSTED]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colCheckRealInputNote])) objInputVoucher.CheckRealInputNote = Convert.ToString(reader[InputVoucher.colCheckRealInputNote]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colCheckRealInputUser])) objInputVoucher.CheckRealInputUser = Convert.ToString(reader[InputVoucher.colCheckRealInputUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colCheckRealInputTime])) objInputVoucher.CheckRealInputTime = Convert.ToDateTime(reader[InputVoucher.colCheckRealInputTime]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colUpdatedUser])) objInputVoucher.UpdatedUser = Convert.ToString(reader[InputVoucher.colUpdatedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colUpdatedDate])) objInputVoucher.UpdatedDate = Convert.ToDateTime(reader[InputVoucher.colUpdatedDate]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colIsDeleted])) objInputVoucher.IsDeleted = Convert.ToBoolean(reader[InputVoucher.colIsDeleted]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colDeletedUser])) objInputVoucher.DeletedUser = Convert.ToString(reader[InputVoucher.colDeletedUser]).Trim();
 					if (!Convert.IsDBNull(reader[InputVoucher.colDeletedDate])) objInputVoucher.DeletedDate = Convert.ToDateTime(reader[InputVoucher.colDeletedDate]);
 					if (!Convert.IsDBNull(reader[InputVoucher.colContentDeleted])) objInputVoucher.ContentDeleted = Convert.ToString(reader[InputVoucher.colContentDeleted]).Trim();

                    if (!Convert.IsDBNull(reader[InputVoucher.colIdCardIssueDate])) objInputVoucher.IdCardIssueDate = Convert.ToDateTime(reader[InputVoucher.colIdCardIssueDate]);
                    if (!Convert.IsDBNull(reader[InputVoucher.colIdCardIssuePlace])) objInputVoucher.IdCardIssuePlace = Convert.ToString(reader[InputVoucher.colIdCardIssuePlace]).Trim();
                    if (!Convert.IsDBNull(reader[InputVoucher.colTransportCustomerID])) objInputVoucher.TransportCustomerID = Convert.ToInt32(reader[InputVoucher.colTransportCustomerID]);
                    if (!Convert.IsDBNull(reader[InputVoucher.colIsAutoCreateInvoice])) objInputVoucher.IsAutoCreateInvoice = Convert.ToBoolean(reader[InputVoucher.colIsAutoCreateInvoice]);
                    if (!Convert.IsDBNull(reader[InputVoucher.colNote])) objInputVoucher.Note = Convert.ToString(reader[InputVoucher.colNote]);
                    if (!Convert.IsDBNull(reader[InputVoucher.colDeliveryNoteDate])) objInputVoucher.DeliveryNoteDate = Convert.ToDateTime(reader[InputVoucher.colDeliveryNoteDate]);
                    
                    objInputVoucher.IsExist = true;
 				}
 				else
 				{
 					objInputVoucher.IsExist = false;
 				}
				reader.Close();
			}
			catch (Exception objEx)
			{
                throw objEx;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objInputVoucher;
		}

		/// <summary>
		/// Thêm thông tin phiếu nhập
		/// </summary>
		/// <param name="objInputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Insert(InputVoucher objInputVoucher)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
				Insert(objIData, objInputVoucher);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Insert, "Lỗi thêm thông tin phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> Insert", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
    		return objResultMessage;
		}


		/// <summary>
		/// Thêm thông tin phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Insert(IData objIData, InputVoucher objInputVoucher)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_ADD);
                objIData.AddParameter("@" + InputVoucher.colInputVoucherID, objInputVoucher.InputVoucherID);
                objIData.AddParameter("@" + InputVoucher.colOrderID, objInputVoucher.OrderID);
                objIData.AddParameter("@" + InputVoucher.colInVoiceID, objInputVoucher.InVoiceID);
                objIData.AddParameter("@" + InputVoucher.colInVoiceSymbol, objInputVoucher.InVoiceSymbol);
                objIData.AddParameter("@" + InputVoucher.colDENOMinAToR, objInputVoucher.Denominator);
                objIData.AddParameter("@" + InputVoucher.colCustomerID, objInputVoucher.CustomerID);
                objIData.AddParameter("@" + InputVoucher.colCustomerName, objInputVoucher.CustomerName);
                objIData.AddParameter("@" + InputVoucher.colCustomerAddress, objInputVoucher.CustomerAddress);
                objIData.AddParameter("@" + InputVoucher.colCustomerPhone, objInputVoucher.CustomerPhone);
                objIData.AddParameter("@" + InputVoucher.colCustomerTaxID, objInputVoucher.CustomerTaxID);
                objIData.AddParameter("@" + InputVoucher.colCustomerEmail, objInputVoucher.CustomerEmail);
                objIData.AddParameter("@" + InputVoucher.colCustomerBirthday, objInputVoucher.CustomerBirthday);
                objIData.AddParameter("@" + InputVoucher.colCustomerIDCard, objInputVoucher.CustomerIDCard);
                objIData.AddParameter("@" + InputVoucher.colContent, objInputVoucher.Content);
                objIData.AddParameter("@" + InputVoucher.colCreatedStoreID, objInputVoucher.CreatedStoreID);
                objIData.AddParameter("@" + InputVoucher.colInputStoreID, objInputVoucher.InputStoreID);
                objIData.AddParameter("@" + InputVoucher.colAutoStoreChangeToStoreID, objInputVoucher.AutoStoreChangeToStoreID);
                objIData.AddParameter("@" + InputVoucher.colInputTypeID, objInputVoucher.InputTypeID);
                objIData.AddParameter("@" + InputVoucher.colPayableTypeID, objInputVoucher.PayableTypeID);
                objIData.AddParameter("@" + InputVoucher.colCurrencyUnitID, objInputVoucher.CurrencyUnitID);
                objIData.AddParameter("@" + InputVoucher.colDiscountReasonID, objInputVoucher.DiscountReasonID);
                objIData.AddParameter("@" + InputVoucher.colCreateDate, objInputVoucher.CreateDate);
                objIData.AddParameter("@" + InputVoucher.colInVoiceDate, objInputVoucher.InVoiceDate);
                objIData.AddParameter("@" + InputVoucher.colInputDate, objInputVoucher.InputDate);
                objIData.AddParameter("@" + InputVoucher.colPayABLedate, objInputVoucher.PayableDate);
                objIData.AddParameter("@" + InputVoucher.colTaxMonth, objInputVoucher.TaxMonth);
                objIData.AddParameter("@" + InputVoucher.colCurrencyExchange, objInputVoucher.CurrencyExchange);
                objIData.AddParameter("@" + InputVoucher.colDiscount, objInputVoucher.Discount);
                objIData.AddParameter("@" + InputVoucher.colPROTECTPriceDiscount, objInputVoucher.ProtectPriceDiscount);
                objIData.AddParameter("@" + InputVoucher.colTotalAmountBFT, objInputVoucher.TotalAmountBFT);
                objIData.AddParameter("@" + InputVoucher.colTotalVAT, objInputVoucher.TotalVAT);
                objIData.AddParameter("@" + InputVoucher.colTotalAmount, objInputVoucher.TotalAmount);
                objIData.AddParameter("@" + InputVoucher.colCreatedUser, objInputVoucher.CreatedUser);
                objIData.AddParameter("@" + InputVoucher.colStaffUser, objInputVoucher.StaffUser);
                objIData.AddParameter("@" + InputVoucher.colIsNew, objInputVoucher.IsNew);
                objIData.AddParameter("@" + InputVoucher.colIsReturnWithFee, objInputVoucher.IsReturnWithFee);
                objIData.AddParameter("@" + InputVoucher.colISStoreChange, objInputVoucher.ISStoreChange);
                objIData.AddParameter("@" + InputVoucher.colIsCheckRealInput, objInputVoucher.IsCheckRealInput);
                objIData.AddParameter("@" + InputVoucher.colISPOSTED, objInputVoucher.IsPosted);
                objIData.AddParameter("@" + InputVoucher.colCheckRealInputNote, objInputVoucher.CheckRealInputNote);
                objIData.AddParameter("@" + InputVoucher.colCheckRealInputUser, objInputVoucher.CheckRealInputUser);
                objIData.AddParameter("@" + InputVoucher.colCheckRealInputTime, objInputVoucher.CheckRealInputTime);
                objIData.AddParameter("@" + InputVoucher.colContentDeleted, objInputVoucher.ContentDeleted);
                objIData.AddParameter("@" + InputVoucher.colIDCardIssueDate, objInputVoucher.IdCardIssueDate);
                objIData.AddParameter("@" + InputVoucher.colIDCardIssuePlace, objInputVoucher.IdCardIssuePlace);
                objIData.AddParameter("@" + InputVoucher.colIsErrorProduct, objInputVoucher.IsErrorProduct);
                objIData.AddParameter("@" + InputVoucher.colErrorProductNote, objInputVoucher.ErrorProductNote);
                objIData.AddParameter("@" + InputVoucher.colTransportCustomerID, objInputVoucher.TransportCustomerID);
                objIData.AddParameter("@" + InputVoucher.colIsAutoCreateInvoice, objInputVoucher.IsAutoCreateInvoice);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + InputVoucher.colNote, objInputVoucher.Note);
                objIData.AddParameter("@" + InputVoucher.colDeliveryNoteDate, objInputVoucher.DeliveryNoteDate);

                objInputVoucher.InputVoucherID = objIData.ExecStoreToString().Trim();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        /// <summary>
        /// Kiểm tra số lương IMEI cần thêm vào có vượt quá giớ hạn không
        /// </summary>
        /// <param name="strProductID"></param>
        /// <param name="intQuantity"></param>
        /// <returns></returns>
        public ResultMessage GetMaxImei(ref int intMaxIMEI ,string strProductID, int intQuantity)
        {
            ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_GETMAXIMEI");
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@Quantity", intQuantity);
                intMaxIMEI= Convert.ToInt32(objIData.ExecStoreToString());
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi kiểm tra số lương IMEI cần thêm vào có vượt quá giớ hạn không", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> GetMaxImei", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

        /// <summary>
        /// Tạo IMEI phát sinh tự động
        /// </summary>
        /// <param name="strProductID"></param>
        /// <param name="intQuantity"></param>
        /// <returns></returns>
        public ResultMessage GenAutoImei(ref DataTable dtbResult, string strProductID, int intQuantity)
        {
            ResultMessage objResultMessage = new ResultMessage();
            IData objIData = Library.DataAccess.Data.CreateData();
            try
            {
                objIData.Connect();
                objIData.CreateNewStoredProcedure("PM_INPUTVOUCHER_GENAUTOIMEI");
                objIData.AddParameter("@ProductID", strProductID);
                objIData.AddParameter("@Quantity", intQuantity);
                dtbResult= objIData.ExecStoreToDataTable();
            }
            catch (Exception objEx)
            {
                objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi tạo IMEI phát sinh tự động", objEx.ToString());
                ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> GenAutoImei", InventoryGlobals.ModuleName);
            }
            finally
            {
                objIData.Disconnect();
            }
            return objResultMessage;
        }

		/// <summary>
		/// Cập nhật thông tin phiếu nhập
		/// </summary>
		/// <param name="objInputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Update(InputVoucher objInputVoucher)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
                objIData.BeginTransaction();
				Update(objIData, objInputVoucher);
                if (objInputVoucher.InputVoucher_AttachmnetList != null && objInputVoucher.InputVoucher_AttachmnetList.Count > 0)
                {
                    foreach (var item in objInputVoucher.InputVoucher_AttachmnetList)
                    {
                        if (item.IsNew!=true)
                        {
                            new DA_InputVoucher_Attachment().Update(objIData, item);
                        }
                        else
                        {
                            item.InputVoucherID = objInputVoucher.InputVoucherID;
                            new DA_InputVoucher_Attachment().Insert(objIData, item);
                        }
                    }
                }

                if (objInputVoucher.InputVoucher_AttachmnetDelList != null && objInputVoucher.InputVoucher_AttachmnetDelList.Count > 0)
                {
                    foreach (var item1 in objInputVoucher.InputVoucher_AttachmnetDelList)
                    {
                        new DA_InputVoucher_Attachment().Delete(objIData,item1);
                    }
                }
                if (objInputVoucher.IsAutoCreateInvoice)
                {
                    PrintVAT.DA.PrintVAT.DA_VAT_Invoice objDA_VAT_Invoice = new PrintVAT.DA.PrintVAT.DA_VAT_Invoice();
                    objResultMessage = objDA_VAT_Invoice.CreateVATInvoiceInputVoucher(objIData, objInputVoucher.InputVoucherID);
                    if (objResultMessage.IsError)
                    {
                        objIData.RollBackTransaction();
                        new SystemError(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceInputVoucher", InventoryGlobals.ModuleName);
                        ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_VAT_Invoice -> CreateVATInvoiceInputVoucher", InventoryGlobals.ModuleName);
                        return objResultMessage;
                    }
                }

                UpdatePostDate_Pinvoice(objIData, objInputVoucher);

                objIData.CommitTransaction();
			}
			catch (Exception objEx)
			{
                objIData.RollBackTransaction();
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Update, "Lỗi cập nhật thông tin phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> Update", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
            //ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
            //List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
            //if (objInputVoucher.UserNotifyData != null && objInputVoucher.UserNotifyData.Rows.Count > 0)
            //{
            //    foreach (DataRow item in objInputVoucher.UserNotifyData.Rows)
            //    {
            //        Notification.BO.Notification_User objNotification_User = new Notification.BO.Notification_User();
            //        objNotification_User.UserName = item["UserName"].ToString();
            //        if (!objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
            //        {
            //            if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
            //            {
            //                bool bolResult = false;
            //                new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, objNotification_User.UserName);
            //                if (bolResult && !objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
            //                    objNotification_UserList.Add(objNotification_User);
            //            }
            //            else
            //                objNotification_UserList.Add(objNotification_User);
            //        }
            //    }
            //    objNotification.Notification_UserList = objNotification_UserList;
            //}
            //objNotification.NotificationID = Guid.NewGuid().ToString();
            //objNotification.NotificationTypeID = 8;
            //objNotification.ClickTaskParameters = objInputVoucher.InputVoucherID;
            //objNotification.TaskParameters = "{\"ID\": \"" + objInputVoucher.InputVoucherID + "\"}";
            //objNotification.NotificationName = "Cập nhật phiếu nhập";
            //objNotification.Content = "Nội dung: " + objInputVoucher.Content;
            //objNotification.CreatedUser = objInputVoucher.CreatedUser;
            //objNotification.CreatedDate = DateTime.Now;
            //objNotification.StartDate = DateTime.Now;
            //ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
            //ResultMessage objResultMessage1 = new ResultMessage();
            //objResultMessage1 = objDA_Notification.Broadcast(objNotification);
    		return objResultMessage;
		}


		/// <summary>
		/// Cập nhật thông tin phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public void Update(IData objIData, InputVoucher objInputVoucher)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_UPDATE);
				objIData.AddParameter("@" + InputVoucher.colInputVoucherID, objInputVoucher.InputVoucherID);
				objIData.AddParameter("@" + InputVoucher.colOrderID, objInputVoucher.OrderID);
				objIData.AddParameter("@" + InputVoucher.colInVoiceID, objInputVoucher.InVoiceID);
				objIData.AddParameter("@" + InputVoucher.colInVoiceSymbol, objInputVoucher.InVoiceSymbol);
				objIData.AddParameter("@" + InputVoucher.colDENOMinAToR, objInputVoucher.Denominator);
				objIData.AddParameter("@" + InputVoucher.colCustomerID, objInputVoucher.CustomerID);
				objIData.AddParameter("@" + InputVoucher.colCustomerName, objInputVoucher.CustomerName);
				objIData.AddParameter("@" + InputVoucher.colCustomerAddress, objInputVoucher.CustomerAddress);
				objIData.AddParameter("@" + InputVoucher.colCustomerPhone, objInputVoucher.CustomerPhone);
				objIData.AddParameter("@" + InputVoucher.colCustomerTaxID, objInputVoucher.CustomerTaxID);
				objIData.AddParameter("@" + InputVoucher.colCustomerEmail, objInputVoucher.CustomerEmail);
				objIData.AddParameter("@" + InputVoucher.colCustomerBirthday, objInputVoucher.CustomerBirthday);
				objIData.AddParameter("@" + InputVoucher.colCustomerIDCard, objInputVoucher.CustomerIDCard);
				objIData.AddParameter("@" + InputVoucher.colContent, objInputVoucher.Content);
				objIData.AddParameter("@" + InputVoucher.colCreatedStoreID, objInputVoucher.CreatedStoreID);
				objIData.AddParameter("@" + InputVoucher.colInputStoreID, objInputVoucher.InputStoreID);
				objIData.AddParameter("@" + InputVoucher.colAutoStoreChangeToStoreID, objInputVoucher.AutoStoreChangeToStoreID);
				objIData.AddParameter("@" + InputVoucher.colInputTypeID, objInputVoucher.InputTypeID);
				objIData.AddParameter("@" + InputVoucher.colPayableTypeID, objInputVoucher.PayableTypeID);
				objIData.AddParameter("@" + InputVoucher.colCurrencyUnitID, objInputVoucher.CurrencyUnitID);
				objIData.AddParameter("@" + InputVoucher.colDiscountReasonID, objInputVoucher.DiscountReasonID);
				objIData.AddParameter("@" + InputVoucher.colCreateDate, objInputVoucher.CreateDate);
				objIData.AddParameter("@" + InputVoucher.colInVoiceDate, objInputVoucher.InVoiceDate);
				objIData.AddParameter("@" + InputVoucher.colInputDate, objInputVoucher.InputDate);
				objIData.AddParameter("@" + InputVoucher.colPayABLedate, objInputVoucher.PayableDate);
				objIData.AddParameter("@" + InputVoucher.colTaxMonth, objInputVoucher.TaxMonth);
				objIData.AddParameter("@" + InputVoucher.colCurrencyExchange, objInputVoucher.CurrencyExchange);
				objIData.AddParameter("@" + InputVoucher.colDiscount, objInputVoucher.Discount);
				objIData.AddParameter("@" + InputVoucher.colPROTECTPriceDiscount, objInputVoucher.ProtectPriceDiscount);
				objIData.AddParameter("@" + InputVoucher.colTotalAmountBFT, objInputVoucher.TotalAmountBFT);
				objIData.AddParameter("@" + InputVoucher.colTotalVAT, objInputVoucher.TotalVAT);
				objIData.AddParameter("@" + InputVoucher.colTotalAmount, objInputVoucher.TotalAmount);
                objIData.AddParameter("@" + InputVoucher.colCreatedUser, objInputVoucher.CreatedUser);
				objIData.AddParameter("@" + InputVoucher.colStaffUser, objInputVoucher.StaffUser);
				objIData.AddParameter("@" + InputVoucher.colIsNew, objInputVoucher.IsNew);
				objIData.AddParameter("@" + InputVoucher.colIsReturnWithFee, objInputVoucher.IsReturnWithFee);
				objIData.AddParameter("@" + InputVoucher.colISStoreChange, objInputVoucher.ISStoreChange);
				objIData.AddParameter("@" + InputVoucher.colIsCheckRealInput, objInputVoucher.IsCheckRealInput);
				objIData.AddParameter("@" + InputVoucher.colISPOSTED, objInputVoucher.IsPosted);
				objIData.AddParameter("@" + InputVoucher.colCheckRealInputNote, objInputVoucher.CheckRealInputNote);
				objIData.AddParameter("@" + InputVoucher.colCheckRealInputUser, objInputVoucher.CheckRealInputUser);
				objIData.AddParameter("@" + InputVoucher.colCheckRealInputTime, objInputVoucher.CheckRealInputTime);
				objIData.AddParameter("@" + InputVoucher.colUpdatedUser, objInputVoucher.UpdatedUser);
                objIData.AddParameter("@" + InputVoucher.colIdCardIssueDate, objInputVoucher.IdCardIssueDate);
                objIData.AddParameter("@" + InputVoucher.colIdCardIssuePlace, objInputVoucher.IdCardIssuePlace);
                objIData.AddParameter("@" + InputVoucher.colIsAutoCreateInvoice, objInputVoucher.IsAutoCreateInvoice);
                objIData.AddParameter("@TransportCustomerID", objInputVoucher.TransportCustomerID);
                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
                objIData.AddParameter("@" + InputVoucher.colNote, objInputVoucher.Note);
                objIData.AddParameter("@" + InputVoucher.colDeliveryNoteDate, objInputVoucher.DeliveryNoteDate);
                objIData.ExecNonQuery();
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}


		/// <summary>
		/// Xóa thông tin phiếu nhập
		/// </summary>
		/// <param name="objInputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public ResultMessage Delete(ref int intResult, InputVoucher objInputVoucher)
		{
			ResultMessage objResultMessage = new ResultMessage();
			IData objIData = Library.DataAccess.Data.CreateData();
			try 
			{
				objIData.Connect();
			   intResult = Delete(objIData, objInputVoucher);
			}
			catch (Exception objEx)
			{
				objResultMessage = new ResultMessage(true, Library.WebCore.SystemError.ErrorTypes.Delete, "Lỗi xóa thông tin phiếu nhập", objEx.ToString());
				ErrorLog.Add(objIData, objResultMessage.Message, objResultMessage.MessageDetail, "DA_InputVoucher -> Delete", InventoryGlobals.ModuleName);
				return objResultMessage;
			}
			finally
    		{
        		objIData.Disconnect();
    		}
            /*
            ERP.Notification.BO.Notification objNotification = new Notification.BO.Notification();
            List<Notification.BO.Notification_User> objNotification_UserList = new List<Notification.BO.Notification_User>();
            if (objInputVoucher.UserNotifyData != null && objInputVoucher.UserNotifyData.Rows.Count > 0)
            {
                foreach (DataRow item in objInputVoucher.UserNotifyData.Rows)
                {
                    Notification.BO.Notification_User objNotification_User = new Notification.BO.Notification_User();
                    objNotification_User.UserName = item["UserName"].ToString();
                    if (!objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
                    {
                        if (Convert.ToBoolean(item["ISSHIFTCONSIDER"]))
                        {
                            bool bolResult = false;
                            new ERP.HRM.Payroll.DA.DA_HR_AttendanceMark().AttendanceMark_CheckTime(ref bolResult, objNotification_User.UserName);
                            if (bolResult && !objNotification_UserList.Exists(o => o.UserName == objNotification_User.UserName))
                                objNotification_UserList.Add(objNotification_User);
                        }
                        else
                            objNotification_UserList.Add(objNotification_User);
                    }
                }
                objNotification.Notification_UserList = objNotification_UserList;
            }
            objNotification.NotificationID = Guid.NewGuid().ToString();
            objNotification.NotificationTypeID = 8;
            objNotification.ClickTaskParameters = objInputVoucher.InputVoucherID;
            objNotification.TaskParameters = "{\"ID\": \"" + objInputVoucher.InputVoucherID + "\"}";
            objNotification.NotificationName = "Hủy phiếu nhập";
            objNotification.Content = "Nội dung: " + objInputVoucher.Content;
            objNotification.CreatedUser = objInputVoucher.CreatedUser;
            objNotification.CreatedDate = DateTime.Now;
            objNotification.StartDate = DateTime.Now;
            ERP.Notification.DA.DA_Notification objDA_Notification = new Notification.DA.DA_Notification();
            ResultMessage objResultMessage1 = new ResultMessage();
            objResultMessage1 = objDA_Notification.Broadcast(objNotification);
            */
    		return objResultMessage;
		}


		/// <summary>
		/// Xóa thông tin phiếu nhập
		/// </summary>
		/// <param name="objIData">Đối tượng Kết nối CSDL</param>
		/// <param name="objInputVoucher">Đối tượng truyền vào</param>
		/// <returns>Kết quả trả về</returns>
		public int Delete(IData objIData, InputVoucher objInputVoucher)
		{
			try 
			{
				objIData.CreateNewStoredProcedure(SP_DELETE);
				objIData.AddParameter("@" + InputVoucher.colInputVoucherID, objInputVoucher.InputVoucherID);
				objIData.AddParameter("@" + InputVoucher.colDeletedUser, objInputVoucher.DeletedUser);
                objIData.AddParameter("@" + InputVoucher.colContentDeleted, objInputVoucher.ContentDeleted);

                objIData.AddParameter("@UserHostAddress", objLogObject.UserHostAddress);
                objIData.AddParameter("@CertificateString", objLogObject.CertificateString);
                objIData.AddParameter("@LoginLogID", objLogObject.LoginLogID);
 				return Convert.ToInt32(objIData.ExecStoreToString());
			}
			catch (Exception objEx)
			{
				objIData.RollBackTransaction();
				throw (objEx);
			}
		}

        public void UpdatePostDate_Pinvoice(IData objIData, InputVoucher objInputVoucher)
        {
            try
            {
                objIData.CreateNewStoredProcedure("VAT_PINVOICE_UPDPOSTDATE");
                objIData.AddParameter("@" + InputVoucher.colInputVoucherID, objInputVoucher.InputVoucherID);
                objIData.ExecNonQuery();
            }
            catch (Exception objEx)
            {
                objIData.RollBackTransaction();
                throw (objEx);
            }
        }
        #endregion


        #region Constructor

        public DA_InputVoucher()
		{
		}
		#endregion


		#region Stored Procedure Names

		public const String SP_ADD = "PM_INPUTVOUCHER_ADD";
		public const String SP_UPDATE = "PM_INPUTVOUCHER_UPD";
		public const String SP_DELETE = "PM_INPUTVOUCHER_DELETE";
		public const String SP_SELECT = "PM_INPUTVOUCHER_SEL";
		public const String SP_SEARCH = "PM_INPUTVOUCHER_SRH";
		#endregion
		
	}
}
