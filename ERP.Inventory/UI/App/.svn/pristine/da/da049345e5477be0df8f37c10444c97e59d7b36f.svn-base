﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.InputChange
{
    public class PLCInputChange
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/InputChange/WSInputChange.asmx";
        private WSInputChange.WSInputChange objWSInputChangeProxy = null;
        private WSInputChange.ResultMessage objResultMessage = new WSInputChange.ResultMessage();
        private ResultMessageApp objResultMessageApp = new ResultMessageApp();

        #endregion
        #region Property
        private WSInputChange.WSInputChange WSInputChangeProxy
        {
            get
            {
                if (objWSInputChangeProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSInputChangeProxy = new WSInputChange.WSInputChange();
                    objWSInputChangeProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSInputChangeProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSInputChangeProxy.EnableDecompression = true;
                    objWSInputChangeProxy.Timeout = 180000;
                }
                return objWSInputChangeProxy;
            }
        }

        public ResultMessageApp ResultMessageApp
        {
            get { return objResultMessageApp; }
            set { objResultMessageApp = value; }
        }
        #endregion
        #region Phương thức

        public bool InsertMasterAndDetail(WSInputChange.InputChange objInputChange)
        {
            try
            {
                objResultMessage = WSInputChangeProxy.InsertMasterAndDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChange);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeProxy.InsertMasterAndDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChange);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChange.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nhập đổi hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public string CheckInputChangeByOutputVoucher(string strOutputVoucherID)
        {
            string strInputChangeID = string.Empty;
            try
            {
                objResultMessage = WSInputChangeProxy.CheckInputChangeByOutputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputChangeID, strOutputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeProxy.CheckInputChangeByOutputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputChangeID, strOutputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChange.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra phiếu nhập đổi hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strInputChangeID;
        }

        public string CheckInputChangeByNewOutputVoucher(string strNewOutputVoucherID)
        {
            string strInputChangeID = string.Empty;
            try
            {
                objResultMessage = WSInputChangeProxy.CheckInputChangeByNewOutputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputChangeID, strNewOutputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeProxy.CheckInputChangeByNewOutputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputChangeID, strNewOutputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChange.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra phiếu nhập đổi hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strInputChangeID;
        }

        public DataTable GetListByOutputVoucher(string strOutputVoucherID)
        {
            DataTable dtbInputChangeDetail = null;
            try
            {
                objResultMessage = WSInputChangeProxy.GetListByOutputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbInputChangeDetail, strOutputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeProxy.GetListByOutputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbInputChangeDetail, strOutputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy danh sách chi tiết nhập đổi hàng qua mã phiếu xuất";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbInputChangeDetail;
        }

        public DataTable GetFirstPriceInputVoucherDetail(string strOutputVoucherID)
        {
            DataTable dtbFirstPriceInputVoucherDetail = null;
            try
            {
                objResultMessage = WSInputChangeProxy.GetFirstPriceInputVoucherDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFirstPriceInputVoucherDetail, strOutputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeProxy.GetFirstPriceInputVoucherDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFirstPriceInputVoucherDetail, strOutputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy giá nhập đầu tiên";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbFirstPriceInputVoucherDetail;
        }

        public WSInputChange.InputChange LoadInfo(string strInputChangeID)
        {
            WSInputChange.InputChange objInputChange = null;
            try
            {
                objResultMessage = WSInputChangeProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objInputChange, strInputChangeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objInputChange, strInputChangeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChange.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Nạp thông tin bảng phiếu nhập đổi hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objInputChange;
        }

        #endregion;
    }
}
