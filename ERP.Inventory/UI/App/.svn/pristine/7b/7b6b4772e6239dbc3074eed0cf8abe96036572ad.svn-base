﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using ERP.Inventory.PLC.WSIMEIExcludeFIFORequest;
using Library.AppCore;
using Library.AppCore.Forms;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmIMEIExcludeFIFORequestImportExcel : Form
    {
        private DataTable dtbData;
        public DataTable dtbSub { get; set; }

        private DataTable dtbResultData = null;
        private int intOutputStoreID;
        private bool bolIsFromStoreChange;
        private int intOutputTypeID;
        private int intInputTypeID;
        private int intInStockStatusID = 1;
        private int intCustomerID = -1;
        private int intIMEIExcludeFIFORequestType = 0;
        public List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = new List<IMEIExcludeFIFORequestDT>();
        private int intRequestStoreID = 0;
        public string strRequestID = string.Empty;
        public int IMEIExcludeFIFORequestType
        {
            set { intIMEIExcludeFIFORequestType = value; }
        }

        public int RequestStoreID
        {
            set { intRequestStoreID = value; }
        }

        public frmIMEIExcludeFIFORequestImportExcel()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(gridControl1);
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Không thể xuất file Excel mẫu!", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Không thể xuất file Excel mẫu!");
                return;
            }
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang kiểm tra IMEI");
        }

        private bool ValidTemplatesExcel(DataTable dtbImeiImport)
        {
            if (dtbImeiImport == null || dtbImeiImport.Columns.Count == 0 || dtbImeiImport.Rows.Count == 0)
                return false;

            if (dtbImeiImport.Rows[0][0].ToString().Trim().ToLower() != "imei")
            {
                MessageBox.Show("File nhập không đúng định dạng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            bool bolInputDataColumn = false;

            for (int i = 0; i < dtbImeiImport.Columns.Count; i++)
            {
                string strProductID = dtbImeiImport.Rows[0][i].ToString();
                if (!string.IsNullOrEmpty(strProductID))
                {
                    if (dtbImeiImport.AsEnumerable().Where(x => !string.IsNullOrEmpty(x[i].ToString())).Count() <= 1)
                    {
                        bolInputDataColumn = true;
                        continue;
                    }
                }
            }
            if (bolInputDataColumn)
            {
                MessageBox.Show(this, "Vui lòng nhập dữ liệu đầy đủ vào file import!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            DataTable dtbImeiImport = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(1);
            DataTable dtbGrid = grdData.DataSource as DataTable;
            if (!ValidTemplatesExcel(dtbImeiImport))
                return;
            dtbResultData = null;

            string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbImeiImport, "IMEI");
            object[] objKeywords = new object[]{"@StoreID", intRequestStoreID,
                                                "@IMEI", xmlIMEI,
                                                "@IMEIEXCLUDEFIFOREQUESTTYPEID", intIMEIExcludeFIFORequestType,
                                                    "@IMEIEXCLUDEFIFOREQUESTID", strRequestID.Trim()};
            DataTable dtData = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("CHECKIMEI_EXCLUEFIFOREQUEST", objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }

            if (dtData == null)
                return;

            //Thread objThread = new Thread(new ThreadStart(Excute));
            //objThread.Start();

            DataRow[] arrDataRowCheck = null;
            foreach (DataRow rowImport in dtData.Rows)
            {
                if (Convert.ToInt32(rowImport["InStockStatusID"]) != 1)
                {
                    rowImport["ISERROR"] = 1;
                    rowImport["NOTE"] = "IMEI này không phải IMEI mới";
                    continue;
                }

                arrDataRowCheck = dtData.Select(string.Format(@"IMEI = '{0}'", rowImport["IMEI"].ToString().Trim()));
                if(arrDataRowCheck != null && arrDataRowCheck.Length > 1)
                {
                    rowImport["ISERROR"] = 1;
                    rowImport["NOTE"] = "IMEI này bị trùng";
                    continue;
                }

                if (Convert.ToInt32(rowImport["ISEXIST"]) == 1)
                {
                    rowImport["ISERROR"] = 1;
                    rowImport["NOTE"] = "IMEI này đang đợi duyệt. Không thể tạo yêu cầu.";
                    continue;
                }

                if (Convert.ToInt32(rowImport["ISEXIST"]) == 2)
                {
                    rowImport["ISERROR"] = 1;
                    rowImport["NOTE"] = "IMEI này đã được duyệt và chứng từ chưa được hủy. Không thể tạo yêu cầu.";
                    continue;
                }
            }

            if (dtbImeiImport != null && dtbImeiImport.Rows.Count > 1)
            {
                for(int i = 1; i < dtbImeiImport.Rows.Count; i++)
                {
                    if (!dtData.AsEnumerable().Any(x => x["IMEI"].ToString().Trim() == dtbImeiImport.Rows[i][0].ToString().Trim()))
                    {
                        DataRow rowNew = dtData.NewRow();
                        rowNew["IMEI"] = dtbImeiImport.Rows[i][0];
                        rowNew["ISERROR"] = 1;
                        rowNew["NOTE"] = "Không tìm thấy IMEI";
                        dtData.Rows.Add(rowNew);
                    }
                }
            }

            //frmWaitDialog.Close();
            //Thread.Sleep(0);
            //objThread.Abort();

            //ValidateData(dtbGrid);
            grdData.DataSource = dtData;
            grdData.RefreshDataSource();
            btnUpdate.Enabled = true;
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!CheckValiadate())
            {
                return;
            }
            if (!InsertDataToInput()) return;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private bool InsertDataToInput()
        {
            try
            {
                DataTable dtData = grdData.DataSource as DataTable;
                if (dtData == null)
                    return false;

                foreach(DataRow row in dtData.Rows)
                {
                    if (Convert.ToInt32(row["ISERROR"]) == 1)
                        continue;
                    IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT = new IMEIExcludeFIFORequestDT();
                    objIMEIExcludeFIFORequestDT.ProductID = row["PRODUCTID"].ToString().Trim();
                    objIMEIExcludeFIFORequestDT.ProductName = row["PRODUCTNAME"].ToString().Trim();
                    objIMEIExcludeFIFORequestDT.IMEI = row["IMEI"].ToString().Trim();
                    objIMEIExcludeFIFORequestDT.InStockStatusID = Convert.ToInt32(row["INSTOCKSTATUSID"]);
                    objIMEIExcludeFIFORequestDT.FirstInputDate = Convert.ToDateTime(row["FIRSTINPUTDATE"]);
                    lstIMEIExcludeFIFORequestDT.Add(objIMEIExcludeFIFORequestDT);
                }
                
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool CheckValiadate()
        {
            DataTable dtData = grdData.DataSource as DataTable;
            if (dtData == null)
                return false;

            if (dtData.AsEnumerable().Any(x => Convert.ToInt32(x["ISERROR"]) == 1))
            {
                if (MessageBox.Show(this, "Tồn tại Imei không hợp lệ.\nBạn có muốn tiếp tục?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
            }

            return true;
        }

        private void grdViewData_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            DataRow row = grdViewData.GetDataRow(e.RowHandle);
            if (row != null && row["IsError"] != null && !string.IsNullOrEmpty(row["IsError"].ToString()))
            {
                if (Convert.ToInt32(row["ISERROR"]) == 1)
                    e.Appearance.BackColor = Color.Pink;
            }
        }

        private void repoViewDetail_Click(object sender, EventArgs e)
        {
            grdData_DoubleClick(null, null);
        }

        private void frmStoreChangeImportExcelImei_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            if (!bolIsFromStoreChange)
            {
            }
        }

        private void frmStoreChangeImportExcelImei_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

    }


}
