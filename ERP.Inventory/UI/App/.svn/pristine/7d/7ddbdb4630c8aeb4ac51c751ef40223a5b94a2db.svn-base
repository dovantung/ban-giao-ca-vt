﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.AppCore;
using System.Data;

namespace ERP.Inventory.PLC.PM
{
    public class PLCLotIMEISalesInfo
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/PM/WSLotIMEISalesInfo.asmx";
        private WSLotIMEISalesInfo.WSLotIMEISalesInfo objWSLotIMEISalesInfoProxy = null;
        private WSLotIMEISalesInfo.ResultMessage objResultMessage = new WSLotIMEISalesInfo.ResultMessage();
        #endregion
        #region Property
        private WSLotIMEISalesInfo.WSLotIMEISalesInfo WSLotIMEISalesInfoProxy
        {
            get
            {
                if (objWSLotIMEISalesInfoProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSLotIMEISalesInfoProxy = new WSLotIMEISalesInfo.WSLotIMEISalesInfo();
                    objWSLotIMEISalesInfoProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSLotIMEISalesInfoProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSLotIMEISalesInfoProxy.EnableDecompression = true;
                    objWSLotIMEISalesInfoProxy.Timeout = 180000;
                }
                return objWSLotIMEISalesInfoProxy;
            }
        }
        #endregion
        #region Method
        public WSLotIMEISalesInfo.LotIMEISalesInfo LoadInfo(string strLotIMEISalesInfoID)
        {
            WSLotIMEISalesInfo.LotIMEISalesInfo objLotIMEISalesInfo = null;
            try
            {
                objResultMessage = WSLotIMEISalesInfoProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objLotIMEISalesInfo, strLotIMEISalesInfoID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfoProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objLotIMEISalesInfo, strLotIMEISalesInfoID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objLotIMEISalesInfo;
        }

        public bool Insert(WSLotIMEISalesInfo.LotIMEISalesInfo objLotIMEISalesInfo)
        {
            string strInputVoucherID = string.Empty;
            try
            {
                objResultMessage = WSLotIMEISalesInfoProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfoProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Update(WSLotIMEISalesInfo.LotIMEISalesInfo objLotIMEISalesInfo)
        {
            try
            {
                objResultMessage = WSLotIMEISalesInfoProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfoProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Delete(WSLotIMEISalesInfo.LotIMEISalesInfo objLotIMEISalesInfo)
        {
            int intResult = 0;
            try
            {
                objResultMessage = WSLotIMEISalesInfoProxy.Detete(ref intResult, PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfoProxy.Detete(ref intResult, PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa thông tin bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSLotIMEISalesInfoProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfoProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        public List<WSLotIMEISalesInfo.LotIMEISalesInfo_ProSpec> SearchData_LotIMEISalesInfo_ProSpec_ToList(object[] objKeywords)
        {
            WSLotIMEISalesInfo.LotIMEISalesInfo_ProSpec[] dtbData = null;
            try
            {
                objResultMessage = WSLotIMEISalesInfoProxy.SearchData_LotIMEISalesInfo_ProSpec_ToList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfoProxy.SearchData_LotIMEISalesInfo_ProSpec_ToList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm tính năng sản phẩm của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData.ToList();
        }

        public bool Review(List<WSLotIMEISalesInfo.LotIMEISalesInfo> lstLotIMEISalesInfo)
        {
            try
            {
                objResultMessage = WSLotIMEISalesInfoProxy.Review(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstLotIMEISalesInfo.ToArray());
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfoProxy.Review(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstLotIMEISalesInfo.ToArray());
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi duyệt thông tin bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Delete(List<WSLotIMEISalesInfo.LotIMEISalesInfo> lstLotIMEISalesInfo)
        {
            try
            {
                objResultMessage = WSLotIMEISalesInfoProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstLotIMEISalesInfo.ToArray());
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfoProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstLotIMEISalesInfo.ToArray());
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa thông tin bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        #endregion
    }
}
