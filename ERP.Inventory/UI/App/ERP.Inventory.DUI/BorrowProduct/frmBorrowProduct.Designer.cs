﻿namespace ERP.Inventory.DUI.BorrowProduct
{
    partial class frmBorrowProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBorrowProduct));
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.cboBorrowProductType = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.cboReviewStatus = new System.Windows.Forms.ComboBox();
            this.cboBorrowStoreIDList = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.dtToDate = new System.Windows.Forms.DateTimePicker();
            this.dtFromDate = new System.Windows.Forms.DateTimePicker();
            this.cboBorrowStatus = new System.Windows.Forms.ComboBox();
            this.cboSearchBy = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkIsDeleted = new System.Windows.Forms.CheckBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemView = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBorrowProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBorrowProductTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBorrowDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReturnPlanDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoreName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBorrowStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReviewedStatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReviewUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cilReviewDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsFinished = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repIsFinished = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colFinishedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBorrowNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCreatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsDeleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repIsDeleted = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDeletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeletedUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBorrowStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeleteReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.mnuAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsFinished)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.cboBorrowProductType);
            this.panel3.Controls.Add(this.cboReviewStatus);
            this.panel3.Controls.Add(this.cboBorrowStoreIDList);
            this.panel3.Controls.Add(this.dtToDate);
            this.panel3.Controls.Add(this.dtFromDate);
            this.panel3.Controls.Add(this.cboBorrowStatus);
            this.panel3.Controls.Add(this.cboSearchBy);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.chkIsDeleted);
            this.panel3.Controls.Add(this.btnSearch);
            this.panel3.Controls.Add(this.txtKeywords);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1020, 62);
            this.panel3.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 16);
            this.label6.TabIndex = 15;
            this.label6.Text = "Loại yêu cầu:";
            // 
            // cboBorrowProductType
            // 
            this.cboBorrowProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBorrowProductType.Location = new System.Drawing.Point(94, 4);
            this.cboBorrowProductType.Margin = new System.Windows.Forms.Padding(0);
            this.cboBorrowProductType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBorrowProductType.Name = "cboBorrowProductType";
            this.cboBorrowProductType.Size = new System.Drawing.Size(217, 24);
            this.cboBorrowProductType.TabIndex = 14;
            // 
            // cboReviewStatus
            // 
            this.cboReviewStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReviewStatus.FormattingEnabled = true;
            this.cboReviewStatus.Items.AddRange(new object[] {
            "-- Trạng thái duyệt --",
            "Đang xử lý",
            "Từ chối",
            "Đồng ý"});
            this.cboReviewStatus.Location = new System.Drawing.Point(822, 4);
            this.cboReviewStatus.Name = "cboReviewStatus";
            this.cboReviewStatus.Size = new System.Drawing.Size(159, 24);
            this.cboReviewStatus.TabIndex = 12;
            // 
            // cboBorrowStoreIDList
            // 
            this.cboBorrowStoreIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBorrowStoreIDList.Location = new System.Drawing.Point(387, 4);
            this.cboBorrowStoreIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboBorrowStoreIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBorrowStoreIDList.Name = "cboBorrowStoreIDList";
            this.cboBorrowStoreIDList.Size = new System.Drawing.Size(209, 24);
            this.cboBorrowStoreIDList.TabIndex = 1;
            // 
            // dtToDate
            // 
            this.dtToDate.CustomFormat = "dd/MM/yyyy";
            this.dtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtToDate.Location = new System.Drawing.Point(201, 31);
            this.dtToDate.Name = "dtToDate";
            this.dtToDate.Size = new System.Drawing.Size(110, 22);
            this.dtToDate.TabIndex = 5;
            // 
            // dtFromDate
            // 
            this.dtFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFromDate.Location = new System.Drawing.Point(94, 31);
            this.dtFromDate.Name = "dtFromDate";
            this.dtFromDate.Size = new System.Drawing.Size(101, 22);
            this.dtFromDate.TabIndex = 4;
            // 
            // cboBorrowStatus
            // 
            this.cboBorrowStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBorrowStatus.FormattingEnabled = true;
            this.cboBorrowStatus.Items.AddRange(new object[] {
            "-- Trạng thái mượn --",
            "Tạo mới",
            "Đã mượn",
            "Đã trả"});
            this.cboBorrowStatus.Location = new System.Drawing.Point(676, 4);
            this.cboBorrowStatus.Name = "cboBorrowStatus";
            this.cboBorrowStatus.Size = new System.Drawing.Size(140, 24);
            this.cboBorrowStatus.TabIndex = 2;
            // 
            // cboSearchBy
            // 
            this.cboSearchBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchBy.FormattingEnabled = true;
            this.cboSearchBy.Items.AddRange(new object[] {
            "Nhân viên duyệt",
            "Nhân viên nhận",
            "Nhân viên giao",
            "Nhân viên tạo",
            "Mã sản phẩm",
            "IMEI"});
            this.cboSearchBy.Location = new System.Drawing.Point(676, 31);
            this.cboSearchBy.Name = "cboSearchBy";
            this.cboSearchBy.Size = new System.Drawing.Size(140, 24);
            this.cboSearchBy.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(599, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Tìm theo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(599, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Trạng thái:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ngày mượn:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(314, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Kho mượn:";
            // 
            // chkIsDeleted
            // 
            this.chkIsDeleted.AutoSize = true;
            this.chkIsDeleted.Location = new System.Drawing.Point(822, 33);
            this.chkIsDeleted.Name = "chkIsDeleted";
            this.chkIsDeleted.Size = new System.Drawing.Size(68, 20);
            this.chkIsDeleted.TabIndex = 3;
            this.chkIsDeleted.Text = "Đã hủy";
            this.chkIsDeleted.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(896, 31);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 25);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtKeywords
            // 
            this.txtKeywords.Location = new System.Drawing.Point(387, 31);
            this.txtKeywords.MaxLength = 100;
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(209, 22);
            this.txtKeywords.TabIndex = 6;
            this.txtKeywords.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeywords_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(317, 34);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 16);
            this.label22.TabIndex = 0;
            this.label22.Text = "Từ khóa:";
            // 
            // grdData
            // 
            this.grdData.ContextMenuStrip = this.mnuAction;
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(0, 62);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repIsDeleted,
            this.repIsFinished,
            this.repositoryItemCheckEdit8,
            this.repNote});
            this.grdData.Size = new System.Drawing.Size(1020, 460);
            this.grdData.TabIndex = 9;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            this.grdData.DoubleClick += new System.EventHandler(this.grdData_DoubleClick);
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemView,
            this.mnuItemExportExcel});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(128, 48);
            this.mnuAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAction_Opening);
            // 
            // mnuItemView
            // 
            this.mnuItemView.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemView.Image")));
            this.mnuItemView.Name = "mnuItemView";
            this.mnuItemView.Size = new System.Drawing.Size(127, 22);
            this.mnuItemView.Text = "Xem";
            this.mnuItemView.Click += new System.EventHandler(this.mnuItemView_Click);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExportExcel.Image")));
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(127, 22);
            this.mnuItemExportExcel.Text = "Xuất Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBorrowProductID,
            this.colBorrowProductTypeName,
            this.colBorrowDate,
            this.colReturnPlanDate,
            this.colStoreName,
            this.colFullName,
            this.colBorrowStatus,
            this.colReviewedStatusName,
            this.colReviewUser,
            this.cilReviewDate,
            this.colIsFinished,
            this.colFinishedDate,
            this.colBorrowNote,
            this.colCreatedDate,
            this.colCreatedUser,
            this.colUpdatedDate,
            this.colUpdatedUser,
            this.colIsDeleted,
            this.colDeletedDate,
            this.colDeletedUser,
            this.colBorrowStatusID,
            this.colDeleteReason});
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.AllowHtmlDrawHeaders = true;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            // 
            // colBorrowProductID
            // 
            this.colBorrowProductID.AppearanceCell.Options.UseTextOptions = true;
            this.colBorrowProductID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colBorrowProductID.AppearanceHeader.Options.UseTextOptions = true;
            this.colBorrowProductID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBorrowProductID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBorrowProductID.Caption = "Mã chứng từ";
            this.colBorrowProductID.FieldName = "BORROWPRODUCTID";
            this.colBorrowProductID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBorrowProductID.Name = "colBorrowProductID";
            this.colBorrowProductID.OptionsColumn.AllowEdit = false;
            this.colBorrowProductID.OptionsColumn.AllowMove = false;
            this.colBorrowProductID.OptionsColumn.ReadOnly = true;
            this.colBorrowProductID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colBorrowProductID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PROVIDERPRODUCTCODETYPEID", "Tổng số dòng : {0}")});
            this.colBorrowProductID.Visible = true;
            this.colBorrowProductID.VisibleIndex = 0;
            this.colBorrowProductID.Width = 120;
            // 
            // colBorrowProductTypeName
            // 
            this.colBorrowProductTypeName.Caption = "Loại yêu cầu";
            this.colBorrowProductTypeName.FieldName = "BORROWPRODUCTTYPENAME";
            this.colBorrowProductTypeName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBorrowProductTypeName.Name = "colBorrowProductTypeName";
            this.colBorrowProductTypeName.OptionsColumn.AllowEdit = false;
            this.colBorrowProductTypeName.Visible = true;
            this.colBorrowProductTypeName.VisibleIndex = 1;
            this.colBorrowProductTypeName.Width = 250;
            // 
            // colBorrowDate
            // 
            this.colBorrowDate.AppearanceCell.Options.UseTextOptions = true;
            this.colBorrowDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colBorrowDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colBorrowDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBorrowDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBorrowDate.Caption = "Ngày mượn";
            this.colBorrowDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colBorrowDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colBorrowDate.FieldName = "BORROWDATE";
            this.colBorrowDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBorrowDate.Name = "colBorrowDate";
            this.colBorrowDate.OptionsColumn.AllowEdit = false;
            this.colBorrowDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colBorrowDate.Visible = true;
            this.colBorrowDate.VisibleIndex = 2;
            this.colBorrowDate.Width = 120;
            // 
            // colReturnPlanDate
            // 
            this.colReturnPlanDate.AppearanceCell.Options.UseTextOptions = true;
            this.colReturnPlanDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colReturnPlanDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colReturnPlanDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnPlanDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colReturnPlanDate.Caption = "Ngày hẹn trả";
            this.colReturnPlanDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colReturnPlanDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colReturnPlanDate.FieldName = "RETURNPLANDATE";
            this.colReturnPlanDate.Name = "colReturnPlanDate";
            this.colReturnPlanDate.OptionsColumn.AllowEdit = false;
            this.colReturnPlanDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colReturnPlanDate.Visible = true;
            this.colReturnPlanDate.VisibleIndex = 3;
            this.colReturnPlanDate.Width = 120;
            // 
            // colStoreName
            // 
            this.colStoreName.AppearanceCell.Options.UseTextOptions = true;
            this.colStoreName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colStoreName.AppearanceHeader.Options.UseTextOptions = true;
            this.colStoreName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStoreName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStoreName.Caption = "Kho mượn";
            this.colStoreName.FieldName = "STORENAME";
            this.colStoreName.Name = "colStoreName";
            this.colStoreName.OptionsColumn.AllowEdit = false;
            this.colStoreName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colStoreName.Visible = true;
            this.colStoreName.VisibleIndex = 4;
            this.colStoreName.Width = 220;
            // 
            // colFullName
            // 
            this.colFullName.AppearanceCell.Options.UseTextOptions = true;
            this.colFullName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFullName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFullName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFullName.Caption = "Nhân viên mượn";
            this.colFullName.FieldName = "FULLNAME";
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.AllowEdit = false;
            this.colFullName.OptionsColumn.ReadOnly = true;
            this.colFullName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 5;
            this.colFullName.Width = 200;
            // 
            // colBorrowStatus
            // 
            this.colBorrowStatus.AppearanceCell.Options.UseTextOptions = true;
            this.colBorrowStatus.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colBorrowStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colBorrowStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBorrowStatus.Caption = "Trạng thái mượn";
            this.colBorrowStatus.FieldName = "BORROWSTATUSNAME";
            this.colBorrowStatus.Name = "colBorrowStatus";
            this.colBorrowStatus.OptionsColumn.AllowEdit = false;
            this.colBorrowStatus.OptionsColumn.ReadOnly = true;
            this.colBorrowStatus.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colBorrowStatus.Visible = true;
            this.colBorrowStatus.VisibleIndex = 6;
            this.colBorrowStatus.Width = 120;
            // 
            // colReviewedStatusName
            // 
            this.colReviewedStatusName.Caption = "Trạng thái duyệt";
            this.colReviewedStatusName.FieldName = "REVIEWEDSTATUSNAME";
            this.colReviewedStatusName.Name = "colReviewedStatusName";
            this.colReviewedStatusName.OptionsColumn.AllowEdit = false;
            this.colReviewedStatusName.Visible = true;
            this.colReviewedStatusName.VisibleIndex = 7;
            this.colReviewedStatusName.Width = 120;
            // 
            // colReviewUser
            // 
            this.colReviewUser.Caption = "Người duyệt";
            this.colReviewUser.FieldName = "REVIEW_FULLNAME";
            this.colReviewUser.Name = "colReviewUser";
            this.colReviewUser.OptionsColumn.AllowEdit = false;
            this.colReviewUser.Visible = true;
            this.colReviewUser.VisibleIndex = 8;
            this.colReviewUser.Width = 200;
            // 
            // cilReviewDate
            // 
            this.cilReviewDate.Caption = "Ngày duyệt";
            this.cilReviewDate.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm:sss";
            this.cilReviewDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.cilReviewDate.FieldName = "REVIEWEDDATE";
            this.cilReviewDate.Name = "cilReviewDate";
            this.cilReviewDate.OptionsColumn.AllowEdit = false;
            this.cilReviewDate.Visible = true;
            this.cilReviewDate.VisibleIndex = 9;
            this.cilReviewDate.Width = 120;
            // 
            // colIsFinished
            // 
            this.colIsFinished.AppearanceCell.Options.UseTextOptions = true;
            this.colIsFinished.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colIsFinished.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsFinished.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsFinished.Caption = "Đã hoàn thành";
            this.colIsFinished.ColumnEdit = this.repIsFinished;
            this.colIsFinished.FieldName = "ISFINISHED";
            this.colIsFinished.Name = "colIsFinished";
            this.colIsFinished.OptionsColumn.AllowEdit = false;
            this.colIsFinished.Visible = true;
            this.colIsFinished.VisibleIndex = 10;
            this.colIsFinished.Width = 110;
            // 
            // repIsFinished
            // 
            this.repIsFinished.AutoHeight = false;
            this.repIsFinished.Name = "repIsFinished";
            this.repIsFinished.ValueChecked = ((short)(1));
            this.repIsFinished.ValueUnchecked = ((short)(0));
            // 
            // colFinishedDate
            // 
            this.colFinishedDate.AppearanceCell.Options.UseTextOptions = true;
            this.colFinishedDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFinishedDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colFinishedDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFinishedDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFinishedDate.Caption = "Ngày hoàn thành";
            this.colFinishedDate.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm:sss";
            this.colFinishedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colFinishedDate.FieldName = "FINISHEDDATE";
            this.colFinishedDate.Name = "colFinishedDate";
            this.colFinishedDate.OptionsColumn.AllowEdit = false;
            this.colFinishedDate.Visible = true;
            this.colFinishedDate.VisibleIndex = 11;
            this.colFinishedDate.Width = 150;
            // 
            // colBorrowNote
            // 
            this.colBorrowNote.AppearanceCell.Options.UseTextOptions = true;
            this.colBorrowNote.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colBorrowNote.AppearanceHeader.Options.UseTextOptions = true;
            this.colBorrowNote.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBorrowNote.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBorrowNote.Caption = "Ghi chú";
            this.colBorrowNote.ColumnEdit = this.repNote;
            this.colBorrowNote.FieldName = "BORROWNOTE";
            this.colBorrowNote.Name = "colBorrowNote";
            this.colBorrowNote.OptionsColumn.AllowEdit = false;
            this.colBorrowNote.Visible = true;
            this.colBorrowNote.VisibleIndex = 12;
            this.colBorrowNote.Width = 200;
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 499;
            this.repNote.Name = "repNote";
            // 
            // colCreatedDate
            // 
            this.colCreatedDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreatedDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreatedDate.Caption = "Ngày tạo";
            this.colCreatedDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colCreatedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colCreatedDate.FieldName = "CREATEDDATE";
            this.colCreatedDate.Name = "colCreatedDate";
            this.colCreatedDate.OptionsColumn.AllowEdit = false;
            this.colCreatedDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCreatedDate.Visible = true;
            this.colCreatedDate.VisibleIndex = 13;
            this.colCreatedDate.Width = 120;
            // 
            // colCreatedUser
            // 
            this.colCreatedUser.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreatedUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreatedUser.Caption = "Người tạo";
            this.colCreatedUser.FieldName = "CREATEDUSER";
            this.colCreatedUser.Name = "colCreatedUser";
            this.colCreatedUser.OptionsColumn.AllowEdit = false;
            this.colCreatedUser.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCreatedUser.Visible = true;
            this.colCreatedUser.VisibleIndex = 14;
            this.colCreatedUser.Width = 200;
            // 
            // colUpdatedDate
            // 
            this.colUpdatedDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdatedDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdatedDate.Caption = "Ngày cập nhật";
            this.colUpdatedDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colUpdatedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colUpdatedDate.FieldName = "UPDATEDDATE";
            this.colUpdatedDate.Name = "colUpdatedDate";
            this.colUpdatedDate.OptionsColumn.AllowEdit = false;
            this.colUpdatedDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colUpdatedDate.Visible = true;
            this.colUpdatedDate.VisibleIndex = 15;
            this.colUpdatedDate.Width = 120;
            // 
            // colUpdatedUser
            // 
            this.colUpdatedUser.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdatedUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdatedUser.Caption = "Người cập nhật";
            this.colUpdatedUser.FieldName = "UPDATEDUSER";
            this.colUpdatedUser.Name = "colUpdatedUser";
            this.colUpdatedUser.OptionsColumn.AllowEdit = false;
            this.colUpdatedUser.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colUpdatedUser.Visible = true;
            this.colUpdatedUser.VisibleIndex = 16;
            this.colUpdatedUser.Width = 200;
            // 
            // colIsDeleted
            // 
            this.colIsDeleted.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsDeleted.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsDeleted.Caption = "Đã hủy";
            this.colIsDeleted.ColumnEdit = this.repIsDeleted;
            this.colIsDeleted.FieldName = "ISDELETED";
            this.colIsDeleted.Name = "colIsDeleted";
            this.colIsDeleted.OptionsColumn.AllowEdit = false;
            // 
            // repIsDeleted
            // 
            this.repIsDeleted.AutoHeight = false;
            this.repIsDeleted.Name = "repIsDeleted";
            this.repIsDeleted.ValueChecked = ((short)(1));
            this.repIsDeleted.ValueUnchecked = ((short)(0));
            // 
            // colDeletedDate
            // 
            this.colDeletedDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDeletedDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeletedDate.Caption = "Ngày hủy";
            this.colDeletedDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colDeletedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colDeletedDate.FieldName = "DELETEDDATE";
            this.colDeletedDate.Name = "colDeletedDate";
            this.colDeletedDate.OptionsColumn.AllowEdit = false;
            this.colDeletedDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDeletedDate.Width = 120;
            // 
            // colDeletedUser
            // 
            this.colDeletedUser.AppearanceHeader.Options.UseTextOptions = true;
            this.colDeletedUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeletedUser.Caption = "Người hủy";
            this.colDeletedUser.FieldName = "DELETEDUSER";
            this.colDeletedUser.Name = "colDeletedUser";
            this.colDeletedUser.OptionsColumn.AllowEdit = false;
            this.colDeletedUser.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDeletedUser.Width = 200;
            // 
            // colBorrowStatusID
            // 
            this.colBorrowStatusID.Caption = "Trang thai";
            this.colBorrowStatusID.FieldName = "BORROWSATUS";
            this.colBorrowStatusID.Name = "colBorrowStatusID";
            // 
            // colDeleteReason
            // 
            this.colDeleteReason.Caption = "Lý do hủy";
            this.colDeleteReason.FieldName = "DELETEDREASON";
            this.colDeleteReason.Name = "colDeleteReason";
            this.colDeleteReason.OptionsColumn.AllowEdit = false;
            this.colDeleteReason.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDeleteReason.Width = 250;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit8.ValueUnchecked = ((short)(0));
            // 
            // frmBorrowProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 522);
            this.Controls.Add(this.grdData);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(960, 560);
            this.Name = "frmBorrowProduct";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý chứng từ mượn hàng hóa";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBorrowProduct_FormClosing);
            this.Load += new System.EventHandler(this.frmBorrowProduct_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmBorrowProduct_KeyUp);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.mnuAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsFinished)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker dtToDate;
        private System.Windows.Forms.DateTimePicker dtFromDate;
        private System.Windows.Forms.ComboBox cboSearchBy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkIsDeleted;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtKeywords;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraGrid.Columns.GridColumn colBorrowProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colBorrowDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStoreName;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colBorrowStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIsFinished;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repIsFinished;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colBorrowNote;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedUser;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedUser;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsDeleted;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repIsDeleted;
        private DevExpress.XtraGrid.Columns.GridColumn colDeletedUser;
        private DevExpress.XtraGrid.Columns.GridColumn colDeletedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemView;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private System.Windows.Forms.ComboBox cboBorrowStatus;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboBorrowStoreIDList;
        private DevExpress.XtraGrid.Columns.GridColumn colBorrowStatusID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnPlanDate;
        private DevExpress.XtraGrid.Columns.GridColumn colBorrowProductTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colReviewUser;
        private DevExpress.XtraGrid.Columns.GridColumn cilReviewDate;
        private DevExpress.XtraGrid.Columns.GridColumn colReviewedStatusName;
        private System.Windows.Forms.ComboBox cboReviewStatus;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboBorrowProductType;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraGrid.Columns.GridColumn colDeleteReason;
    }
}