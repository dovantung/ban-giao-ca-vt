﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.BorrowProduct
{
    public partial class frmBorrowProduct_Note : Form
    {
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.BorrowProduct_Delivery objBorrowProduct_Delivery = new PLC.BorrowProduct.WSBorrowProduct.BorrowProduct_Delivery();
        private PLC.BorrowProduct.PLCBorrowProduct_Delivery objPLCBorrowProduct_Delivery = new PLC.BorrowProduct.PLCBorrowProduct_Delivery();
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct_Delivery.ResultMessage objResultMessageDelivery = null;
        private string strBorrowProductID = string.Empty;
        private string strNote = string.Empty;

        public string Note
        {
            get { return strNote; }
            set { strNote = value; }
        }
        public string BorrowProductID
        {
            get { return strBorrowProductID; }
            set { strBorrowProductID = value; }
        }
        public frmBorrowProduct_Note()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtNote.Text.Trim() == "")
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập ghi chú!");
                return;
            }

            Note = txtNote.Text.Trim();
            this.DialogResult = DialogResult.OK;
        }

        private void frmBorrowProduct_Note_Load(object sender, EventArgs e)
        {
            //PLC.BorrowProduct.WSBorrowProduct_Delivery.BorrowProduct_Delivery objBorrowProduct_Delivery = new PLC.BorrowProduct.WSBorrowProduct_Delivery.BorrowProduct_Delivery();
            //objResultMessageDelivery = objPLCBorrowProduct_Delivery.LoadInfo(ref objBorrowProduct_Delivery, strBorrowProductID);
            //if (objBorrowProduct_Delivery.Note == null)
                txtNote.Text = string.Empty;
                //txtNote.Text = objBorrowProduct_Delivery.Note;
        }

        private void txtNote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (btnOk.Enabled)
                    btnOk_Click(null, null);
            }
        }

        private void frmBorrowProduct_Note_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
    }
}
