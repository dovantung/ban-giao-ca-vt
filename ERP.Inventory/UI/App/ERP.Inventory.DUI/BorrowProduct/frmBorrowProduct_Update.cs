﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using DevExpress.XtraGrid.Views.Grid;
using ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct;
using ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct_Delivery;
using ERP.Inventory.PLC.BorrowProduct.WSBorrowProductDT;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors;

namespace ERP.Inventory.DUI.BorrowProduct
{
    public partial class frmBorrowProduct_Update : Form
    {
        public enum ActionType
        {
            NEW = 0,
            EDIT = 1,
            READ_ONLY = 2
        }
        public ActionType eActionType;




        frmBorrowProduct frmBorrow;
        #region Variable
        private string strPermission_Edit = "PM_BORROWPRODUCT_DELIVERY";
        private PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new PLC.BorrowProduct.PLCBorrowProduct();
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.ResultMessage objResultMessage = null;
        private PLC.BorrowProduct.PLCBorrowProduct_Delivery objPLCBorrowProduct_Delivery = new PLC.BorrowProduct.PLCBorrowProduct_Delivery();
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct_Delivery.ResultMessage objResultMessageDelivery = null;
        private PLC.BorrowProduct.PLCBorrowProductDT objPLCBorrowProductDT = new PLC.BorrowProduct.PLCBorrowProductDT();
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProductDT.ResultMessage objResultMessageDT = null;
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.BorrowProduct objBorrowProduct = new PLC.BorrowProduct.WSBorrowProduct.BorrowProduct();
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.BorrowProduct_Delivery objBorrowProduct_Delivery = new PLC.BorrowProduct.WSBorrowProduct.BorrowProduct_Delivery();
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.BorrowProductDT objBorrowProductDT = new PLC.BorrowProduct.WSBorrowProduct.BorrowProductDT();
        private List<ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.BorrowProduct_Delivery> lstBorrowProduct_Delivery = new List<ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.BorrowProduct_Delivery>();
        private List<ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.BorrowProductDT> lstBorrowProductDT = new List<ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.BorrowProductDT>();
        private List<PLC.BorrowProduct.WSBorrowProductDT.BorrowProductDT> lstBorrowProductDT_Del = new List<PLC.BorrowProduct.WSBorrowProductDT.BorrowProductDT>();
        private ERP.MasterData.SYS.PLC.PLCUser objPLCUser = new MasterData.SYS.PLC.PLCUser();
        private bool cancelLeaving = false;
        DataTable dtbProductDetail = null;
        private bool bolIsRequestIMEI = false;
        private string strImeiCheck = string.Empty;
        private string strImeiProductID = string.Empty;
        private string strImeiProductName = string.Empty;
        public int intBorrowStoreID = 0;
        
        private String strPermission_BorrowProduct = string.Empty;
        private String strRecieveuser = string.Empty;
        private String strDeliveryser = string.Empty;
        private string strUserCurrent = string.Empty;

        private bool bolIsUpdate = false;
        private bool bolIsRefreshData = false;
        private bool bolIsOnlyCertifyFinger = false;
        private string strBorrowProductID = string.Empty;
        private bool bolIsDeleted = false;
        private int intBorrowSatus = 0;
        private string strCreateUser = string.Empty;
        private bool bolIsEnableLogin = true;
        private string strBorrowProductTypeID = string.Empty;

        // Le van Dong
        public bool bolIsNewFromWarranty;
        public string strRepairOrderId = string.Empty;
        public DateTime dteAppoimentReturnDate;
        //public delegate void DeleteBorrowProductDelegate(string strBorrowProductId, string strReasonDelete);
        private DataTable dtbAccessory = null;
        //public Delegate DeleteBorrowProductMethod { get; set; }

        public bool IsRefreshData
        {
            get { return bolIsRefreshData; }
            set { bolIsRefreshData = value; }
        }

        public bool IsUpdate
        {
            get { return bolIsUpdate; }
            set { bolIsUpdate = value; }
        }
        public bool IsOnlyCertifyFinger
        {
            get { return bolIsOnlyCertifyFinger; }
            set { bolIsOnlyCertifyFinger = value; }
        }
        public string BorrowProductID
        {
            get { return strBorrowProductID; }
            set { strBorrowProductID = value; }
        }


        public bool IsDeleted
        {
            get { return bolIsDeleted; }
            set { bolIsDeleted = value; }
        }
        public int BorrowSatus
        {
            get { return intBorrowSatus; }
            set { intBorrowSatus = value; }
        }
        public string CreateUser
        {
            get { return strCreateUser; }
            set { strCreateUser = value; }
        }
        public string BorrowProductTypeID
        {
            get { return strBorrowProductTypeID; }
            set { strBorrowProductTypeID = value; }
        }
        #endregion

        #region Constructor
        public frmBorrowProduct_Update(frmBorrowProduct _frmBorrow)
        {
            InitializeComponent();
            frmBorrow = _frmBorrow;
        }

        public frmBorrowProduct_Update()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        #endregion



        #region Event

        private void btnSearchProduct_Click(object sender, EventArgs e)
        {
            if (!ValidateStore()) return;
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch = MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch.IsMultiSelect = true;
            frmProductSearch.ShowDialog();
            if (frmProductSearch.ProductList != null && frmProductSearch.ProductList.Count > 0)
            {
                foreach (ERP.MasterData.PLC.MD.WSProduct.Product objProduct in frmProductSearch.ProductList)
                {
                    AddProduct(objProduct.ProductID);
                }
                txtBarcode.Focus();
            }
            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (!btnDelete.Enabled)
                return;
            if (!DeleteData())
                return;
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!btnUpdate.Enabled)
                return;
            if (!UpdateData())
                return;
            this.Cursor = Cursors.WaitCursor;
            btnUpdate.Enabled = false;
            this.Refresh();
            try
            {
            }
            catch { }
            finally
            {
                this.Cursor = Cursors.Default;
                btnUpdate.Enabled = true;
                if (bolIsNewFromWarranty)
                    this.Close();
            }
            if (eActionType == ActionType.EDIT
                || eActionType == ActionType.READ_ONLY)
            {
                IsUpdate = true;
                this.Close();
                return;
            }
            else
                frmBorrowProduct_Update_Load(null, null);
            ClearValueControl();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (eActionType == ActionType.EDIT
                || eActionType == ActionType.READ_ONLY)
            {
                this.Close();
                return;
            }
            else
            {
                if (MessageBox.Show(this, "Bạn có chắc chắn muốn bỏ qua?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (bolIsNewFromWarranty)
                    {
                        this.Close();
                        return;
                    }

                    //this.Close();
                    //return;
                    frmBorrowProduct_Update_Load(null, null);
                    ClearValueControl();
                }
            }
        }

        private void btnBorrow_Click(object sender, EventArgs e)
        {
            if (!btnBorrow.Enabled)
                return;
            if (!ValidateStore())
                return;
            if (!ValidateProductCheck())
                return;
            if (bolIsOnlyCertifyFinger)
            {
                List<string> lstDeliveryUser = new List<string>();
                lstDeliveryUser.Add(SystemConfig.objSessionUser.UserName);
                //lstPermission.Add(strPermission_BorrowProduct);
                Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên giao hàng cho mượn", "cho mượn", null,
                    null, !bolIsOnlyCertifyFinger, true);
                frmCertifyLogin1.ShowDialog(this);
                if (!frmCertifyLogin1.IsCorrect)
                {
                    return;
                }
                else
                {
                    if (!frmCertifyLogin1.UserName.Equals(objBorrowProduct.CreatedUser))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Chỉ nhân viên tạo mới được giao phiếu này");
                        return;
                    }
                }
                strDeliveryser = frmCertifyLogin1.UserName;
            }
            else
            {
                if (SystemConfig.objSessionUser.UserName.Equals(objBorrowProduct.CreatedUser))
                {
                    strDeliveryser = SystemConfig.objSessionUser.UserName;
                }
                else
                {
                    Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên giao hàng cho mượn", "cho mượn", null,
        null, !bolIsOnlyCertifyFinger, true);
                    frmCertifyLogin1.ShowDialog(this);
                    if (!frmCertifyLogin1.IsCorrect)
                    {
                        return;
                    }
                    else
                    {
                        if (!frmCertifyLogin1.UserName.Equals(objBorrowProduct.CreatedUser))
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Chỉ nhân viên tạo mới được giao phiếu này");
                            return;
                        }
                    }
                    strDeliveryser = frmCertifyLogin1.UserName;
                }
            }

            List<string> lstReceiverUser = new List<string>();
            lstReceiverUser.Add(ctrlUser.UserName);
            Library.Login.frmCertifyLogin frmCertifyLogin12 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên mượn", "mượn", null,
                null, !bolIsOnlyCertifyFinger, true);
            frmCertifyLogin12.ShowDialog(this);
            if (!frmCertifyLogin12.IsCorrect)
            {
                return;
            }
            else
            {
                if (!frmCertifyLogin12.UserName.Equals(ctrlUser.UserName))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Nhân viên xác nhận không phải là nhân viên mượn");
                    return;
                }
            }
            strRecieveuser = frmCertifyLogin12.UserName;
            if (SetStatusBorrow())
            {
                btnBorrow.Enabled = false;
                btnDelete.Enabled = false;
                btnUpdate.Enabled = false;
                mnuAction.Enabled = false;
                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    return;
                }
                else
                {
                    MessageBox.Show(" Mượn hàng thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                EditData();
                IsUpdate = true;
                IsRefreshData = true;
                dtReturnPlanDate.Enabled = false;
            }

            
        }
        private bool SetStatusBorrow()
        {
            if (objBorrowProduct == null)
                objBorrowProduct = new PLC.BorrowProduct.WSBorrowProduct.BorrowProduct();

            objBorrowProduct.BorrowStatus = 1;
            objBorrowProduct.BorrowNote = txtBorrowNote.Text.Trim();
            objBorrowProduct.BorrowProductTypeID = Convert.ToInt32(strBorrowProductTypeID);
            DataTable dtbData = null;
            frmBorrowProduct_Note frm = new frmBorrowProduct_Note();
            frm.BorrowProductID = strBorrowProductID;
            DialogResult dialog = frm.ShowDialog();
            if (dialog != DialogResult.OK)
                return false;
            objResultMessage = objPLCBorrowProduct.Update(objBorrowProduct, ref dtbData);
            if (dtbData != null)
            {
                grdProduct.DataSource = dtbData;
                return true;
            }


            PLC.BorrowProduct.WSBorrowProduct_Delivery.BorrowProduct_Delivery objBorrowProduct_Delivery = new PLC.BorrowProduct.WSBorrowProduct_Delivery.BorrowProduct_Delivery();

            objBorrowProduct_Delivery.Note = frm.Note;
            objBorrowProduct_Delivery.BorrowProductID = strBorrowProductID;
            objBorrowProduct_Delivery.DeliveryDate = DateTime.Now;
            objBorrowProduct_Delivery.DeliveryType = false;
            objBorrowProduct_Delivery.RECIEVEUser = strRecieveuser;
            objBorrowProduct_Delivery.DeliverySER = strDeliveryser;
            objResultMessageDelivery = objPLCBorrowProduct_Delivery.Insert(objBorrowProduct_Delivery, ref strBorrowProductID);
            strDeliveryser = string.Empty;
            strRecieveuser = string.Empty;

            return true;
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            if (!btnReturn.Enabled)
                return;
            if (!ValidateStore())
                return;
            List<string> lstDeliveryUser = new List<string>();
            //lstDeliveryUser.Add(ctrlUser.UserName);
            Library.Login.frmCertifyLogin frmCertifyLoginR1 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên mượn trả hàng hóa", "trả", null,
                null, !bolIsOnlyCertifyFinger, true);
            frmCertifyLoginR1.ShowDialog(this);
            if (!frmCertifyLoginR1.IsCorrect)
            {
                return;
            }
            else
            {
                if (!frmCertifyLoginR1.UserName.Equals(ctrlUser.UserName))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Nhân viên xác nhận không phải là nhân viên mượn");
                    return;
                }
            }

            strDeliveryser = frmCertifyLoginR1.UserName;
            List<string> lstReceiverUser = new List<string>();
            //lstReceiverUser.Add(SystemConfig.objSessionUser.UserName);
            frmCertifyLoginR1 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên nhận hàng hóa cho mượn", "cho mượn", null,
               null, !bolIsOnlyCertifyFinger, true);
            frmCertifyLoginR1.ShowDialog(this);
            if (!frmCertifyLoginR1.IsCorrect)
            {
                return;
            }
            else
            {
                if (!bolIsNewFromWarranty && frmCertifyLoginR1.UserName == ctrlUser.UserName)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Nhân viên trả hàng và nhận hàng không được trùng nhau !");
                    return;
                }
            }
            strRecieveuser = frmCertifyLoginR1.UserName;
            DataTable dtbNote = objPLCUser.GetDataUser_Store(strRecieveuser);
            dtbNote.DefaultView.RowFilter = "ISCANOUTPUT = 1 AND STOREID = " + Convert.ToInt32(cboBorrowStoreID.StoreID);
            dtbNote = dtbNote.DefaultView.ToTable();
            if (dtbNote.Rows.Count == 0)
            {
                MessageBox.Show(this, "Chỉ nhân viên có quyền xuất kho này mới được nhận lại !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                strRecieveuser = string.Empty;
                return;
            }

            if (SetStatusReturn())
            {
                btnReturn.Enabled = false;
                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    return;
                }
                else
                {
                    MessageBox.Show(" Trả hàng thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                IsUpdate = true;
                IsRefreshData = true;
                EditData();
                grdViewProduct.OptionsBehavior.Editable = false;
            }

            
        }
        private bool SetStatusReturn()
        {

            if (objBorrowProduct == null)
                objBorrowProduct = new PLC.BorrowProduct.WSBorrowProduct.BorrowProduct();

            objBorrowProduct.BorrowStatus = 2;
            objBorrowProduct.IsFinishED = true;
            objBorrowProduct.FinishEDDate = DateTime.Now;
            objBorrowProduct.BorrowNote = txtBorrowNote.Text.Trim();
            objBorrowProduct.BorrowProductTypeID = Convert.ToInt32(strBorrowProductTypeID);
            DataTable dtbProduct = (DataTable)grdProduct.DataSource;
            dtbProduct.AcceptChanges();
            objBorrowProduct.DtbBorrowProductDT = dtbProduct;

            DataTable dtbData = null;
            frmBorrowProduct_Note frm = new frmBorrowProduct_Note();
            frm.BorrowProductID = strBorrowProductID;
            DialogResult dialog = frm.ShowDialog();
           
            if (dialog != DialogResult.OK)
                return false;
            objResultMessage = objPLCBorrowProduct.Update(objBorrowProduct, ref dtbData);
            if (dtbData != null)
            {
                grdProduct.DataSource = dtbData;
                return true;
            }
            PLC.BorrowProduct.WSBorrowProduct_Delivery.BorrowProduct_Delivery objBorrowProduct_Delivery = new PLC.BorrowProduct.WSBorrowProduct_Delivery.BorrowProduct_Delivery();

            objBorrowProduct_Delivery.Note = frm.Note;
            objBorrowProduct_Delivery.BorrowProductID = strBorrowProductID;
            objBorrowProduct_Delivery.DeliveryDate = DateTime.Now;
            objBorrowProduct_Delivery.DeliveryType = true;
            objBorrowProduct_Delivery.DeliverySER = strDeliveryser;
            objBorrowProduct_Delivery.RECIEVEUser = strRecieveuser; ;
            objResultMessageDelivery = objPLCBorrowProduct_Delivery.Insert(objBorrowProduct_Delivery, ref strBorrowProductID);
            strDeliveryser = string.Empty;
            strRecieveuser = string.Empty;

            return true;
        }

        private void mnuItemDeleted_Click(object sender, EventArgs e)
        {
            if (grdViewProduct.FocusedRowHandle < 0) return;
            grdViewProduct.DeleteRow(grdViewProduct.FocusedRowHandle);
            grdViewProduct.CloseEditor();
            grdViewProduct.UpdateCurrentRow();

            colStatus.Visible = grdViewProduct.RowCount > 0 && colStatus.Visible;


        }
        #endregion

        #region Method
        private void frmBorrowProduct_Update_Load(object sender, EventArgs e)
        {
            objPLCBorrowProductType = new MasterData.PLC.MD.PLCBorrowProductType();
            if (bolIsOnlyCertifyFinger)
                bolIsEnableLogin = false;
            InitControl();
            LoadCombobox();
            ctrlUser.IsOnlyActived = true;
            if (IsHaveTagForm())
            {
                SetActionTypeForm(0);
            }
            if (string.IsNullOrEmpty(strBorrowProductTypeID))
            {
                MessageBox.Show("Không tìm thấy loại yêu cầu mượn hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                intClosedForm = 0;
                return;
            }

            if (!string.IsNullOrEmpty(strBorrowProductTypeID))
            {
                LoadBorrowProductTypeInfor(strBorrowProductTypeID);
                if (eActionType == ActionType.NEW && !IsPermission_BorrowProductType_Addfunction)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Bạn không có quyền tạo mới phiếu mượn hàng!");
                    intClosedForm = 0;
                    return;
                }
                LoadDataReview();
                CheckButton();

            }
            cboBorrowStoreID.SetValue(intBorrowStoreID);
            lblCreateUser.Visible = lblCaptionCreateUser.Visible = eActionType != ActionType.NEW;
            lblCreateUser.Text = string.IsNullOrEmpty(strCreateUser) ? SystemConfig.objSessionUser.UserName + "-" + SystemConfig.objSessionUser.FullName : strCreateUser;
           
            
        }

        public bool IsHaveTagForm()
        {
            // Lay Thong tin loai xuat muong hang 
            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    if (hstbParam.ContainsKey("BORROWPRODUCTTYPEID"))
                    {
                        strBorrowProductTypeID = (hstbParam["BORROWPRODUCTTYPEID"] ?? "").ToString().Trim();
                        return true;
                    }

                }
                catch { }
            }
            return false;
        }

        public void SetActionTypeForm(int intActionType = 0)
        {
            // Neu form duoc mở từ Bảo hành thì không cần set lại Defauld Kho
            if (!bolIsNewFromWarranty)
                intBorrowStoreID = SystemConfig.intDefaultStoreID;

            // Check Da Ton Tai Muc Duyet chua 
            switch (intActionType)
            {
                case 0:
                    this.Text = "Thêm mới chứng từ mượn hàng hóa";

                    eActionType = ActionType.NEW;
                    strBorrowProductID = objPLCBorrowProduct.GetBorrowProductNewID(SystemConfig.intDefaultStoreID);
                    EnableControl(true);
                    ClearValueControl();
                    grbTransferDetail.Visible = false;
                    colMachineStatusReturn.Visible = false;

                    //Thêm ghi chú mượn hàng
                    if (bolIsNewFromWarranty)
                    {
                        try
                        {
                            txtBorrowNote.Text = Library.AppCore.AppConfig.GetConfigValue("PM_BORROWPRODUCT_DEFAULTNOTE").ToString();
                        }
                        catch
                        {
                            txtBorrowNote.Text = string.Empty;
                        }

                    }
                    //grbReviewDetail.Height = 167 + grbTransferDetail.Height;
                    break;
                default:
                    if (!EditData())
                        return;

                    EnableControl(false);
                    grbTransferDetail.Visible = true;
                    //grbDetailProduct.Dock = DockStyle.None;

                    break;
            }
            if (intActionType == 1)
            {
                eActionType = ActionType.EDIT;
                this.Text = "Cập nhật chứng từ mượn hàng hóa";
            }
            if (intActionType == 2)
            {
                eActionType = ActionType.READ_ONLY;
                this.Text = "Duyệt chứng từ mượn hàng hóa";
                //grdViewProduct.OptionsBehavior.Editable = false;
                txtBorrowNote.ReadOnly = true;
                dtReturnPlanDate.Enabled = false;
                // Kiem tra nguoi duyet dau tien la ai 
            }


        }


        private void InitControl()
        {
            btnDelete.Enabled = bolIsUpdate;
            if (SystemConfig.objSessionUser.UserName != "administrator" && strPermission_Edit != string.Empty)
            {
                btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
                btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
            }
            else
            {
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
            }
            cboBorrowStoreID.KeyPress += new KeyPressEventHandler(ChangeFocus);
            dtBorrowDate.KeyPress += new KeyPressEventHandler(ChangeFocus);
            dtReturnPlanDate.KeyPress += new KeyPressEventHandler(ChangeFocus);
            txtBorrowNote.KeyUp += new KeyEventHandler(SelectAll);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewDelivery);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewProduct);

            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grvReViewLevel,
                "STT",
                "REVIEWLEVELNAME",
                "REVIEWTYPE",
                "ISSELECT",
                "ISREADONLY",
                "REVIEW_USER",
                "REVIEW_FULLNAME",
                "ISREVIEWED",
                "REVIEWEDSTATUS",
                "REVIEWEDDATE",
                "ISREVIEWEDCHECKINBYFINGER",
                "NOTE"
                );
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvReViewLevel);
            FormatGridReivew();
        }

        private void ChangeFocus(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                this.SelectNextControl(this.ActiveControl, true, true, true, true);
        }
        private void SelectAll(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == System.Windows.Forms.Keys.A))
            {
                (sender as TextBox).SelectAll();
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }

        private void LoadCombobox()
        {
            System.Windows.Forms.ComboBox cboTMP = new System.Windows.Forms.ComboBox();
            Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboTMP, true, -1, -1, -1, Library.AppCore.Constant.EnumType.StorePermissionType.OUTPUT, Library.AppCore.Constant.EnumType.AreaType.AREA, "ISCANOUTPUT = 1 and IsActive = 1");
            DataTable dtbStore = cboTMP.DataSource as DataTable;
            dtbStore.Rows.RemoveAt(0);
            cboBorrowStoreID.InitControl(false, dtbStore);
            cboBorrowStoreID.Title = "-- Chọn kho mượn --";
            //cboBorrowStoreID.InitControl(false, false);
            cboBorrowStoreID.SetValue(intBorrowStoreID);

            DataTable dtbMachineStatus = null;
            objPLCBorrowProduct.GetDataMachineStatusReturn(ref dtbMachineStatus);
            //if (dtbMachineStatus != null)
            //    dtbMachineStatus.Columns[0].Caption = "Tình trạng";
            //DataTable dtbMachineStatusTemp = dtbMachineStatus.DefaultView.ToTable();
            //DataColumn[] col = new DataColumn[]{new DataColumn("MACHINESTATUSNAME", typeof(String), "Tình trạng")};
            //dtbMachineStatusTemp.Columns.AddRange(col);
            //DataRow drStatusTemp = dtbMachineStatusTemp.NewRow();
            //drStatusTemp["MACHINESTATUSNAME"] = "--Chọn tình trạng--";
            //dtbMachineStatusTemp.Rows.InsertAt(drStatusTemp, 0);

            //repLookUpMachineStatus.DataSource = dtbMachineStatus;
            //repLookUpMachineStatus.ValueMember = "MACHINESTATUSNAME";
            //repLookUpMachineStatus.DisplayMember = "MACHINESTATUSNAME";
            if (dtbMachineStatus != null)
            {
                foreach (DataRow row in dtbMachineStatus.Rows)
                {
                    repComboMachineStatusReturn.Items.Add(row["MACHINESTATUSNAME"]);
                }
            }
        }

        private void ClearValueControl()
        {
            dtBorrowDate.Value = Library.AppCore.Globals.GetServerDateTime();
            if (bolIsNewFromWarranty) // Neu form duoc mở tu Bảo hành thì gán ngày hẹn trả
                dtReturnPlanDate.EditValue = dteAppoimentReturnDate;
            else
                dtReturnPlanDate.EditValue = null;

            txtBorrowNote.Text = string.Empty;
            txtBarcode.Text = string.Empty;
            if (bolIsNewFromWarranty)
                ctrlUser.UserName = SystemConfig.objSessionUser.UserName;
            DataTable dtbDT = null; //(grdProduct.DataSource as DataTable).Clone();
            grdProduct.DataSource = dtbDT;
            grdViewProduct.RefreshData();
            DataTable dtbDE = null; //(grdDelivery.DataSource as DataTable).Clone();
            grdProduct.DataSource = dtbDE;
            grdViewProduct.RefreshData();

            if (dtbAccessory != null)
                dtbAccessory.Clear();

        }

        private void EnableControl(bool bolValue)
        {
            //ERP.MasterData.DUI.Common.CommonFunction.SetReadOnly(txtBorrowNote, !bolValue);
            ERP.MasterData.DUI.Common.CommonFunction.SetReadOnly(txtBarcode, !bolValue);
            cboBorrowStoreID.Enabled = bolValue;
            if (bolIsNewFromWarranty)
                cboBorrowStoreID.Enabled = false;

            //dtBorrowDate.Enabled = bolValue;
            if (bolIsUpdate)
            {
                dtReturnPlanDate.Enabled = !bolIsDeleted && intBorrowSatus == 0;
            }
            else
            {
                dtReturnPlanDate.Enabled = bolValue;
            }
            ctrlUser.Enabled = bolValue;
            btnSearchProduct.Enabled = bolValue;
            if (bolIsDeleted)
                ERP.MasterData.DUI.Common.CommonFunction.SetReadOnly(txtBorrowNote, !bolValue);
            if (intBorrowSatus == 2)
                ERP.MasterData.DUI.Common.CommonFunction.SetReadOnly(txtBorrowNote, !bolValue);
        }

        private bool ValidateStore()
        {
            if (cboBorrowStoreID.StoreID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho mượn !");
                cboBorrowStoreID.Focus();
                return false;
            }
            return true;
        }

        private bool ValidateData()
        {

            if (cboBorrowStoreID.StoreID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho mượn !");
                cboBorrowStoreID.Focus();
                return false;
            }

            if (ctrlUser.UserName == string.Empty)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn nhân viên mượn !");
                ctrlUser.Focus();
                return false;
            }

            if ((!bolIsNewFromWarranty) && ctrlUser.UserName.Equals(SystemConfig.objSessionUser.UserName))
            {
                MessageBox.Show(this, "Nhân viên mượn không được trùng nhân viên tạo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ctrlUser.UserName = string.Empty;
                return false;
            }

            if (dtReturnPlanDate.EditValue == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn ngày hẹn trả !");
                dtReturnPlanDate.Focus();
                return false;
            }

            if (dtReturnPlanDate.EditValue != null && (dtReturnPlanDate.DateTime < dtBorrowDate.Value))
            {
                MessageBoxObject.ShowWarningMessage(this, "Ngày hẹn trả không thể nhỏ hơn ngày mượn !");
                dtReturnPlanDate.Focus();
                return false;
            }

            //if (dtReturnPlanDate.EditValue != null && (dtReturnPlanDate.DateTime.Day < dtBorrowDate.Value.Day || dtReturnPlanDate.DateTime.Month < dtBorrowDate.Value.Month || dtReturnPlanDate.DateTime.Year < dtBorrowDate.Value.Year))
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Ngày hẹn trả không thể nhỏ hơn ngày mượn !");
            //    dtReturnPlanDate.Focus();
            //    return false;
            //}
            if (!CheckReviewUser()) return false;
            if (grdProduct.DataSource == null || grdViewProduct.RowCount < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập sản phẩm mượn !");
                grdProduct.Focus();
                return false;
            }
            if (eActionType == ActionType.NEW)
            {
                dtbProductDetail = grdProduct.DataSource as DataTable;
                dtbProductDetail.AcceptChanges();
                DataRow[] rowIMEI = null;
                for (int i = 0; i < grdViewProduct.RowCount; i++)
                {
                    String strBorrowProductID = Convert.ToString(dtbProductDetail.Rows[i]["BORROWPRODUCTID"]).Trim();
                    String strProductID = Convert.ToString(dtbProductDetail.Rows[i]["PRODUCTID"]).Trim();
                    String strProductName = Convert.ToString(dtbProductDetail.Rows[i]["PRODUCTNAME"]).Trim();
                    String strImei = Convert.ToString(dtbProductDetail.Rows[i]["IMEI"]).Trim();
                    bool bolIsNew = Convert.ToBoolean(dtbProductDetail.Rows[i]["ISNEW"]);
                    int decQuantity = Convert.ToInt32(Globals.IsNull(dtbProductDetail.Rows[i]["QUANTITY"], 1));
                    String strNote = Convert.ToString(dtbProductDetail.Rows[i]["NOTE"]).Trim();
                    String strMachineStatus = Convert.ToString(dtbProductDetail.Rows[i]["MACHINESTATUS"]).Trim();
                    bolIsRequestIMEI = Convert.ToBoolean(dtbProductDetail.Rows[i]["ISREQUESTIMEI"]);
                    rowIMEI = dtbProductDetail.Select("IMEI = '" + strImei + "'"); ;



                    if (bolIsRequestIMEI)
                    {
                        if (bolIsRequestIMEI && strImei.Length == 0)
                        {
                            MessageBox.Show("Vui lòng nhập IMEI sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            grdProduct.Select();
                            grdViewProduct.FocusedRowHandle = i;
                            grdViewProduct.FocusedColumn = grdViewProduct.Columns["IMEI"];
                            //grdViewProduct.FocusedColumn = grdViewProduct.VisibleColumns[0];
                            //grdViewProduct.ShowEditor();
                            return false;
                        }
                        if (!string.IsNullOrEmpty(strImei))
                        {
                            if (rowIMEI.Count() > 1)
                            {
                                MessageBox.Show("IMEI đã bị trùng .Vui lòng nhập  IMEI khác ! ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                grdProduct.Select();
                                grdViewProduct.FocusedRowHandle = i;
                                grdViewProduct.FocusedColumn = grdViewProduct.Columns["IMEI"];
                                return false;
                            }
                            //if (!Library.AppCore.Other.CheckObject.CheckIMEI(strImei))
                            //{
                            //    MessageBox.Show(this, "IMEI không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //    grdProduct.Select();
                            //    grdViewProduct.FocusedRowHandle = i;
                            //    grdViewProduct.FocusedColumn = grdViewProduct.Columns["IMEI"];
                            //    return false;
                            //}
                            //PLC.WSProductInStock.ProductInStock objIMEI = new ERP.Inventory.PLC.PLCProductInStock().LoadInfo(strImei, cboBorrowStoreID.StoreID);
                            //if (objIMEI == null)
                            //{
                            //    MessageBox.Show(this, "Số IMEI không tồn tại trong kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //    grdProduct.Select();
                            //    grdViewProduct.FocusedRowHandle = i;
                            //    grdViewProduct.FocusedColumn = grdViewProduct.Columns["IMEI"];
                            //    return false;
                            //}

                        }
                    }




                    //int intStatus = -1;
                    //if (!CheckExsitAndQuantity(strBorrowProductID,
                    //                       strProductID,
                    //                       bolIsNew,
                    //                       strImei,
                    //                       decQuantity, out intStatus))
                    //{




                    //    grdProduct.Focus();
                    //    grdViewProduct.FocusedRowHandle = i;
                    //    grdViewProduct.FocusedColumn = grdViewProduct.Columns["QUANTITY"];
                    //    grdViewProduct.SetRowCellValue(i, grdViewProduct.Columns["STATUS"], intStatus);
                    //    grdViewProduct.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewProduct.ShowEditor(); });
                    //    return false;
                    //}
                    //grdViewProduct.SetRowCellValue(i, grdViewProduct.Columns["STATUS"], intStatus);

                }
            }
            return true;
        }

        public bool ValidateProductCheck()
        {
            grdViewProduct.CloseEditor();
            grdViewProduct.UpdateCurrentRow();
            dtbProductDetail = grdProduct.DataSource as DataTable;
            dtbProductDetail.AcceptChanges();

            DataRow[] rowIMEI = null;
            int intStatus = -1;
            bool IsERROR = false;
            for (int i = 0; i < grdViewProduct.RowCount; i++)
            {
                DataRow drProduct = grdViewProduct.GetDataRow(i);
                String strBorrowProductID = Convert.ToString(drProduct["BORROWPRODUCTID"]).Trim();
                String strProductID = Convert.ToString(drProduct["PRODUCTID"]).Trim();
                String strProductName = Convert.ToString(drProduct["PRODUCTNAME"]).Trim();
                String strImei = Convert.ToString(drProduct["IMEI"]).Trim();
                bool bolIsNew = Convert.ToBoolean(drProduct["ISNEW"]);
                int decQuantity = Convert.ToInt32(Globals.IsNull(drProduct["QUANTITY"], 1));
                String strNote = Convert.ToString(drProduct["NOTE"]).Trim();
                bolIsRequestIMEI = Convert.ToBoolean(drProduct["ISREQUESTIMEI"]);
                rowIMEI = dtbProductDetail.Select("IMEI = '" + strImei + "'");
                if (bolIsRequestIMEI) // Bat buoc nhap IMEI
                {
                    if (bolIsRequestIMEI && strImei.Length == 0)
                    {
                        MessageBox.Show("Vui lòng nhập IMEI sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        grdProduct.Select();
                        grdViewProduct.FocusedRowHandle = i;
                        grdViewProduct.FocusedColumn = grdViewProduct.Columns["IMEI"];
                        return false;
                    }
                    if (!string.IsNullOrEmpty(strImei))
                    {
                        if (rowIMEI.Count() > 1)
                        {
                            MessageBox.Show("IMEI đã bị trùng .Vui lòng nhập  IMEI khác ! ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            grdProduct.Select();
                            grdViewProduct.FocusedRowHandle = i;
                            grdViewProduct.FocusedColumn = grdViewProduct.Columns["IMEI"];
                            return false;
                        }

                        PLC.WSProductInStock.ProductInStock objIMEI = new ERP.Inventory.PLC.PLCProductInStock().LoadInfo(strImei, cboBorrowStoreID.StoreID);
                        if (objIMEI == null)
                        {
                            MessageBox.Show(this, "Số IMEI không tồn tại trong kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            grdProduct.Select();
                            grdViewProduct.FocusedRowHandle = i;
                            grdViewProduct.FocusedColumn = grdViewProduct.Columns["IMEI"];
                            return false;
                        }
                        //if (objIMEI.IsOrder)// kiểm tra IMEI có được đặt hàng hay chưa
                        //{
                        //    if (MessageBox.Show(this, "Số IMEI " + objIMEI.IMEI + " đã được đặt hàng trong đơn hàng: [" + objIMEI.OrderID + "]. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
                        //    {
                        //        grdProduct.Select();
                        //        grdViewProduct.FocusedRowHandle = i;
                        //        grdViewProduct.FocusedColumn = grdViewProduct.Columns["IMEI"];
                        //        return false;
                        //    }
                        //}
                    }
                }

                if (!CheckExsitAndQuantity(strBorrowProductID,
                                            strProductID,
                                            bolIsNew,
                                            strImei,
                                            decQuantity, out intStatus, false))
                {

                    IsERROR = true;
                    grdViewProduct.SetRowCellValue(i, grdViewProduct.Columns["STATUS"], intStatus);
                    continue;
                }
                grdViewProduct.SetRowCellValue(i, grdViewProduct.Columns["STATUS"], 0);
            }
            colStatus.Visible = IsERROR;
            if (IsERROR)
            {
                MessageBox.Show("Tồn tại sản phẩm đã tạo phiếu yêu cầu hoặc vượt giới hạn tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return false;
            }
            return true;

        }

        private bool ValidateProduct()
        {

            for (int i = 0; i < grdViewProduct.RowCount; i++)
            {
                int Quantity = 0;
                Int32.TryParse(grdViewProduct.GetRowCellValue(i, "QUANTITY").ToString(), out Quantity);
                if (Quantity <= 0)
                {
                    grdViewProduct.SelectRow(i);
                    MessageBoxObject.ShowWarningMessage(this, "Số lượng sản phẩm yêu cầu chuyển phải lớn hơn 0.");
                    return false;
                }
            }
            return true;
        }
        private bool UpdateData()
        {

            if (!ValidateProduct()) return false;
            string strMessage = string.Empty;
            DataTable dtbData = null;
            try
            {
                if (!ValidateData())
                    return false;
                SetData();
                if (eActionType == ActionType.NEW)
                {
                    objBorrowProduct.BorrowDate = Library.AppCore.Globals.GetServerDateTime();
                    objResultMessage = objPLCBorrowProduct.Insert(objBorrowProduct, ref strBorrowProductID, ref dtbData);

                    strMessage = "Thêm";
                    strBorrowProductID = string.Empty;
                }
                if (eActionType == ActionType.EDIT)
                {
                    objResultMessage = objPLCBorrowProduct.Update(objBorrowProduct, ref dtbData);
                    strMessage = "Cập nhật";
                }
                if (objResultMessage.IsError)
                {
                    if (eActionType == ActionType.NEW)
                    {
                        if (dtbData != null)
                            grdProduct.DataSource = dtbData;
                        colStatus.Visible = dtbData != null && eActionType == ActionType.NEW;
                    }
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    return false;
                }
                else
                {

                    MessageBox.Show(strMessage + " thông tin chứng từ mượn hàng hóa thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    IsRefreshData = true;
                    colStatus.Visible = dtbData != null && eActionType == ActionType.NEW;
                }
            }
            catch (Exception objExce)
            {
                if (dtbData != null)
                    grdProduct.DataSource = dtbData;
                MessageBoxObject.ShowWarningMessage(this, "Lỗi" + strMessage.ToLower() + " danh sách cập nhật chứng từ mượn hàng hóa", objExce.ToString());
                SystemErrorWS.Insert("Lỗi" + strMessage.ToLower() + " danh sách cập nhật chứng từ mượn hàng hóa", objExce.ToString(), this.Name + "-> UpdateData", DUIInventory_Globals.ModuleName);
                return false;
            }
            txtBarcode.Text = string.Empty;
            return true;
        }

        public bool CheckReviewUser()
        {
            DataTable dtb = (DataTable)gridReViewLevel.DataSource;
            if (dtb.Rows.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy mức duyệt nào. Vui lòng kiểm tra lại");
                return false;
            }
            
            Dictionary<int, int> ListTest = new Dictionary<int, int>();
            foreach (DataRow drv in dtb.Rows)
            {
                int intStt = Convert.ToInt32(drv["STT"]);
                int intIsSelect = Convert.ToInt32(drv["ISSELECT"]);
                if (!ListTest.ContainsKey(intStt))
                    ListTest.Add(intStt, intIsSelect);
                else
                {
                    if (intIsSelect == 1)
                        ListTest[intStt] = intIsSelect;
                }
            }
            var varResult = ListTest.Where(row => row.Value == 0);
            if (varResult.Any())
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn nhân viên duyệt cho mức duyệt [" + varResult.FirstOrDefault().Key + "]");
                return false;
            }
            return true;
        }

        private void SetData()
        {
            if (objBorrowProduct == null)
                objBorrowProduct = new PLC.BorrowProduct.WSBorrowProduct.BorrowProduct();
            objBorrowProduct.BorrowProductID = strBorrowProductID;
            objBorrowProduct.BorrowStoreID = cboBorrowStoreID.StoreID;
            objBorrowProduct.BorrowDate = dtBorrowDate.Value;
            if (dtReturnPlanDate != null)
                objBorrowProduct.ReturnPlanDate = dtReturnPlanDate.DateTime;
            objBorrowProduct.BorrowUser = ctrlUser.UserName;
            objBorrowProduct.BorrowProductTypeID = Convert.ToInt32(strBorrowProductTypeID);
            objBorrowProduct.BorrowNote = txtBorrowNote.Text.Trim();
            objBorrowProduct.CreatedStoreID = SystemConfig.intDefaultStoreID;
            DataTable dtbData = (grdProduct.DataSource as DataTable);
            dtbData.AcceptChanges();
            objBorrowProduct.DtbBorrowProductDT = dtbData;

            List<BorrowProduct_RVL> BorrowProduct_RVLList = new List<BorrowProduct_RVL>();
            // Dua Muc Duyet Vao 
            if (eActionType == ActionType.READ_ONLY) return;
            grvReViewLevel.CloseEditor();
            grvReViewLevel.UpdateCurrentRow();
            if (gridReViewLevel.DataSource != null)
            {
                DataTable dtb = (DataTable)gridReViewLevel.DataSource;
                DataView dtv = dtb.AsDataView();
                dtv.RowFilter = "ISSELECT = 1";
                foreach (DataRowView drv in dtv)
                {
                    BorrowProduct_RVL objBorrowProduct_RVL = new BorrowProduct_RVL();
                    objBorrowProduct_RVL.ReviewLevelID = Convert.ToInt32(drv["REVIEWLEVELID"]);
                    objBorrowProduct_RVL.ReviewType = Convert.ToInt32(drv["REVIEWTYPE"]);
                    objBorrowProduct_RVL.UserName = (drv["REVIEW_USER"] ?? "").ToString().Trim();
                    objBorrowProduct_RVL.ReviewedStatus = -1;
                    objBorrowProduct_RVL.SequenceReview = Convert.ToInt32(drv["STT"]);
                    BorrowProduct_RVLList.Add(objBorrowProduct_RVL);
                }
            }
            objBorrowProduct.BorrowProduct_RVLList = BorrowProduct_RVLList.ToArray();
            //objBorrowProduct.DtbBorrowProduct_Delivery = grdDelivery.DataSource as DataTable;
            //objBorrowProduct.DtbBorrowProduct_Delivery.AcceptChanges();

            // LE VAN DONG
            objBorrowProduct.DtbBorrowProductDT_Accesory = dtbAccessory;
            if (bolIsNewFromWarranty)
                objBorrowProduct.RepairOrderID = strRepairOrderId;
        }
        private bool DeleteData()
        {
            try
            {
                //if (MessageBox.Show("Bạn có chắc chắn muốn hủy?", "Xác nhận hủy", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                //    return false;
                string strReson = ShowReasion("Nhập lý do hủy", 800);
                if (strReson == "-1") return false;
                objBorrowProduct.DeletedReason = strReson;


                    objPLCBorrowProduct.Delete(objBorrowProduct);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                    MessageBox.Show("Hủy chứng từ mượn hàng hóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    IsRefreshData = true;
                    IsUpdate = true;

                
                this.Close();
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi hủy chứng từ mượn hàng hóa");
                SystemErrorWS.Insert("Lỗi hủy chứng từ mượn hàng hóa", objExce.ToString(), this.Name + " -> DeleteData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private string GetIMEI(string strProduct)
        {
            string strImeiList = string.Empty;
            for (int i = 0; i < grdViewProduct.RowCount; i++)
            {
                string productID = grdViewProduct.GetRowCellValue(i, "PRODUCTID").ToString().Trim();
                if (productID == strProduct)
                {
                    string imei = grdViewProduct.GetRowCellValue(i, "IMEI").ToString().Trim();
                    if (!string.IsNullOrEmpty(imei))
                        strImeiList += "<" + imei + ">";
                }
            }
            return strImeiList;
        }
        private bool CheckProduct(ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock)
        {
            if (objProductInStock == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Sản phẩm không tồn tại trong hệ thống hoặc số imei không có trong kho.");
                return false;
            }
            if (!objProductInStock.IsInstock || objProductInStock.Quantity <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " không tồn kho trong kho " + cboBorrowStoreID.StoreName);
                return false;
            }

            if (objProductInStock.IsService && !objProductInStock.IsCheckStockQuantity)
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn không được tạo yêu cầu trên ngành hàng này!");
                return false;
            }
            return true;
        }
        private bool CheckImei(ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock)
        {

            if (!Library.AppCore.Other.CheckObject.CheckIMEI(objProductInStock.IMEI))
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI không đúng định dạng.");
                return false;
            }

            else if (!objProductInStock.IsCheckRealInput && objProductInStock.IMEI.Length > 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI này chưa được xác nhận nhập kho");
                return false;
            }


            if (objProductInStock.IsOrder)	// kiểm tra IMEI có được đặt hàng hay chưa
            {
                if (MessageBox.Show(this, "Số IMEI " + objProductInStock.IMEI + " đã được đặt hàng trong đơn hàng: [" + objProductInStock.OrderID + "]. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
                {
                    return false;
                }
            }
            return true;
        }


        public bool CheckExsitAndQuantity(string strBORROWPRODUCTID, string strPRODUCTID, bool bolISNEW, string strIMEI, decimal decQuantity, out int intStatus, bool isShowMessenger = true)
        {
            // Check Hang Muon
            if (!string.IsNullOrEmpty(strIMEI)) decQuantity = 1;
            else if (decQuantity == 0) decQuantity = 1;
            intStatus = 0;
            if (cboBorrowStoreID.StoreID == -1)
            {

                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho!");
                cboBorrowStoreID.Focus();
                return false;
            }
            intStatus = objPLCBorrowProductDT.CheckImeiExist(strBORROWPRODUCTID, strPRODUCTID, bolISNEW, strIMEI, decQuantity, cboBorrowStoreID.StoreID);
            string strTile = "IMEI";
            if (string.IsNullOrEmpty(strIMEI.Trim())) strTile = "Sản phẩm";
            if (intStatus == -1)
            {
                if (isShowMessenger)
                    MessageBoxObject.ShowWarningMessage(this, "Có lỗi xãy ra trong lúc kiểm tra " + strTile + " !");
                return false;
            }
            if ((intStatus == 1 || intStatus == 3) && !string.IsNullOrEmpty(strIMEI))
            {
                if (isShowMessenger)
                    MessageBoxObject.ShowWarningMessage(this, strTile + " đang tồn tại phiếu mượn hàng!");
                return false;
            }
            if (intStatus == 2 && !string.IsNullOrEmpty(strIMEI))
            {
                if (isShowMessenger)
                    MessageBoxObject.ShowWarningMessage(this, strTile + " đang cho mượn chưa trả!");
                return false;
            }

            //if (intStatus == 3)
            //{
            //    //DialogResult drs = MessageBox.Show("Sản phẩm này đang trong quá trình duyệt hoặc đã duyệt xong! Bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    //if (drs == System.Windows.Forms.DialogResult.Yes) return true;
            //    //else return false;
            //    //MessageBoxObject.ShowWarningMessage(this, "Sản phẩm này đang trong quá trình duyệt hoặc đã duyệt xong!");
            //    //return false;
            //    MessageBoxObject.ShowWarningMessage(this, strTile + " đang trong quá trình duyệt hoặc đã duyệt xong!");
            //    return false;
            //}
            if ((intStatus == 4 && !string.IsNullOrEmpty(strIMEI))
                || (intStatus == -2 && string.IsNullOrEmpty(strIMEI))
                || (intStatus > 0 && string.IsNullOrEmpty(strIMEI)))
            {
                if (isShowMessenger)
                    MessageBoxObject.ShowWarningMessage(this, "Số lượng " + strTile + " này không đủ để cho mượn!");
                return false;
            }
            return true;
        }


        private bool AddProduct(string strProduct)
        {
            if (string.IsNullOrWhiteSpace(strProduct)) return false;
            int intBorrowStoreID = cboBorrowStoreID.StoreID;
            string strImeiList = GetIMEI(strProduct);

            ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = new ERP.Inventory.PLC.PLCProductInStock().LoadInfo(strProduct, intBorrowStoreID, 0, strImeiList, false);
            if (!CheckProduct(objProductInStock)) return false;
            if (!CheckImei(objProductInStock)) return false;
            int intStatus = -1;
            if (!CheckExsitAndQuantity(strBorrowProductID, objProductInStock.ProductID, objProductInStock.IsNew, objProductInStock.IMEI, 0, out intStatus)) return false;

            DataTable dtbProduct = grdProduct.DataSource as DataTable;

            DataRow[] rowProduct = null;
            if (dtbProduct != null)
            {
                dtbProduct.AcceptChanges();
                rowProduct = dtbProduct.Select("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "'");
                if (rowProduct.Count() > 0)
                {
                    //Trường hợp không cần nhập IMEI, thông báo ra và nhập số lượng
                    if (!objProductInStock.IsRequestIMEI)
                    {
                        rowProduct[0]["QUANTITY"] = Convert.ToInt32(rowProduct[0]["QUANTITY"]) + 1;
                        if (Convert.ToInt32(rowProduct[0]["QUANTITY"]) > objProductInStock.Quantity)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Số lượng vượt quá số lượng tồn trong kho !");
                            rowProduct[0]["QUANTITY"] = objProductInStock.Quantity;
                            return false;
                        }
                        //MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " đã tồn tại rồi. Vui lòng nhập sản phẩm khác.");
                        grdProduct.RefreshDataSource();
                        return true;
                    }
                }
                rowProduct = null;
                //lay lai row product , khi do ham kiem tra nhieu imei ko con tac dung
                if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI))
                {
                    rowProduct = dtbProduct.Select("IMEI = '" + objProductInStock.IMEI.Trim() + "'");
                    if (rowProduct.Count() > 0)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "IMEI " + objProductInStock.IMEI + " đã tồn tại rồi. Vui lòng nhập IMEI khác.");
                        return false;
                    }

                }
                if (strProduct.Trim() == objProductInStock.IMEI && objProductInStock.IsRequestIMEI)
                {
                    DataRow[] rowlist = dtbProduct.Select("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "' and IMEI = ''");
                    if (rowlist.Length > 0)
                    {
                        rowlist[0]["IMEI"] = objProductInStock.IMEI;
                        rowlist[0]["QUANTITY"] = 1;
                        return true;
                    }
                }
            }
            string strNote = string.Empty;
            //if (!bolIsNewFromWarranty)
            //{
            //    try
            //    {
            //        strNote = AppConfig.GetConfigValue("PM_BORROWPRODUCT_DEFAULTNOTE").ToString();
            //    }
            //    catch
            //    {
            //        strNote = "";
            //        //MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy cấu hình ứng dụng");
            //        //return false;
            //    }
            //}
            grdProduct.DataSource = CreateTableProduct(dtbProduct, rowProduct, objProductInStock.IsRequestIMEI, objProductInStock.IsNew, objProductInStock.ProductID, objProductInStock.ProductName, objProductInStock.IMEI, 1, strNote, intStatus, string.Empty, "--Chọn phụ kiện--", objProductInStock.MainGroupID);
            return true;
        }

        private DataTable CreateTableProduct(DataTable dtbProduct, DataRow[] rowProduct, bool IsReQuestIMEI, bool IsNew, string strProductID, string strProductName, string strImei, int intQuantity, string strNote, int intSTATUS, string strMachineStatus, string strAccessoryList, int intMainGroupId)
        {
            DataTable dtb = dtbProduct;
            if (dtb == null)
            {
                dtb = new DataTable("TableProduct");
                DataColumn[] col = new DataColumn[]{new DataColumn("BORROWPRODUCTID", typeof(String))
                                                ,new DataColumn("PRODUCTID", typeof(String))
                                                ,new DataColumn("PRODUCTNAME", typeof(String))
                                                ,new DataColumn("IMEI", typeof(String))
                                                ,new DataColumn("ISNEW", typeof(Int16))
                                                ,new DataColumn("QUANTITY", typeof(Int32))
                                                ,new DataColumn("NOTE", typeof(String))
                                                ,new DataColumn("STATUS", typeof(int))
                                                ,new DataColumn("ISREQUESTIMEI", typeof(Boolean))
                                                ,new DataColumn("MACHINESTATUS", typeof(String))
                                                ,new DataColumn("ACCESSORYLIST", typeof(String))
                                                ,new DataColumn("MAINGROUPID", typeof(Int32))
                                                ,new DataColumn("GUIDID", typeof(Guid))
                                                };
                dtb.Columns.AddRange(col);
            }

            if (rowProduct == null || rowProduct.Count() == 0)
            {
                if (IsReQuestIMEI)
                {
                    intQuantity = 1;// ERP.MasterData.DUI.Common.CommonFunction.GetQuantityFromIMEI(strImei);
                }

                DataRow row = dtb.NewRow();
                row["BORROWPRODUCTID"] = strBorrowProductID;
                row["PRODUCTID"] = strProductID;
                row["PRODUCTNAME"] = strProductName;
                row["IMEI"] = strImei;
                row["ISNEW"] = IsNew;
                row["QUANTITY"] = intQuantity;
                row["NOTE"] = strNote;
                row["ISREQUESTIMEI"] = IsReQuestIMEI;
                row["STATUS"] = intSTATUS;
                row["MACHINESTATUS"] = strMachineStatus;
                row["ACCESSORYLIST"] = strAccessoryList;
                row["MAINGROUPID"] = intMainGroupId;
                row["GUIDID"] = Guid.NewGuid();
                dtb.Rows.Add(row);
            }
            //nhap nhieu imei tren 1 dong
            else if (IsReQuestIMEI)
            {
                try
                {
                    int index = dtb.Rows.IndexOf(rowProduct[0]);
                    string strOldImei = Convert.ToString(grdViewProduct.GetRowCellValue(index, "IMEI")).Trim();
                    if (!string.IsNullOrWhiteSpace(strImei) && strOldImei.Contains(strImei))
                    {
                        //Hien thi thong bao imei da ton tai
                        MessageBoxObject.ShowWarningMessage(this, "IMEI " + strImei + " đã tồn tại.Vui lòng nhập IMEI khác.");
                        return dtb;
                    }
                    if (string.IsNullOrEmpty(strOldImei))
                        strImei = strOldImei;
                    else
                        strImei = strOldImei + "\r\n" + strImei;
                    int iQuantity = ERP.MasterData.DUI.Common.CommonFunction.GetQuantityFromIMEI(strImei);
                    grdViewProduct.SetRowCellValue(index, "IMEI", strImei);
                    grdViewProduct.SetRowCellValue(index, "QUANTITY", iQuantity);
                }
                catch (Exception ex)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy thông tin IMEI.", ex.ToString());
                }
            }
            return dtb;
        }

        private void cboBorrowStoreID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (grdProduct.DataSource != null && grdViewProduct.RowCount > 0)
            {
                if (MessageBox.Show(this, "Thay đổi kho mượn sẽ làm thay đổi dữ liệu trên lưới, bạn có muốn tiếp tục không.", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    DataTable dtb = (grdProduct.DataSource as DataTable).Clone();
                    grdProduct.DataSource = dtb;
                    grdViewProduct.RefreshData();
                }
                else
                    cboBorrowStoreID.SetValue(intBorrowStoreID);
            }
            intBorrowStoreID = cboBorrowStoreID.StoreID;
        }
        private bool EditData()
        {
            try
            {

                objResultMessage = objPLCBorrowProduct.LoadInfoAll(ref objBorrowProduct, strBorrowProductID);
                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Không tìm thấy thông tin phiếu yêu cầu mượn!");
                    intClosedForm = 0;
                    return false;
                }
                if (objBorrowProduct == null)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Không tìm thấy thông tin phiếu yêu cầu mượn!");
                    intClosedForm = 0;
                    return false;
                }

                intBorrowStoreID = objBorrowProduct.BorrowStoreID;
                cboBorrowStoreID.SetValue(intBorrowStoreID);
                dtBorrowDate.Value = objBorrowProduct.BorrowDate.Value;
                dtReturnPlanDate.EditValue = objBorrowProduct.ReturnPlanDate;
                ctrlUser.UserName = (objBorrowProduct.BorrowUser ?? "").ToString();
                strUserCurrent = (objBorrowProduct.BorrowUser ?? "").ToString();
                if (!string.IsNullOrEmpty((objBorrowProduct.BorrowNote ?? "").ToString()))
                    txtBorrowNote.Lines = (objBorrowProduct.BorrowNote ?? "").ToString().Split('\n');
                if (objBorrowProduct.DtbBorrowProductDT != null)
                    grdProduct.DataSource = objBorrowProduct.DtbBorrowProductDT;
                if (objBorrowProduct.DtbBorrowProduct_Delivery != null)
                    grdDelivery.DataSource = objBorrowProduct.DtbBorrowProduct_Delivery;
                if (objBorrowProduct.DtbBorrowProductDT_Accesory != null)
                    dtbAccessory = objBorrowProduct.DtbBorrowProductDT_Accesory;

                if (objBorrowProduct.IsDeleted)
                    EnableControl(false);
                bolIsUpdate = true;
                if (objBorrowProduct.RepairOrderID != null || objBorrowProduct.RepairOrderID != "")
                    bolIsNewFromWarranty = true;

                colMachineStatusReturn.Visible = objBorrowProduct.BorrowStatus >= 1;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin chứng từ mượn hàng hóa");
                SystemErrorWS.Insert("Lỗi nạp thông tin chứng từ mượn hàng hóa", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        #endregion

        private void grdViewProduct_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedRowHandle < 0) return;
            if (view.FocusedColumn == view.Columns["IMEI"])
            {
                if (!string.IsNullOrWhiteSpace(e.Value.ToString()))
                {
                    if (!Library.AppCore.Other.CheckObject.CheckIMEI(e.Value.ToString().Trim()))
                    {
                        //e.Valid = false;
                        //e.ErrorText = "IMEI không đúng định dạng.";
                        MessageBoxObject.ShowWarningMessage(this, "IMEI không đúng định dạng.");
                        return;
                    }
                    strImeiProductID = view.GetFocusedRowCellValue("PRODUCTID").ToString().Trim();
                    strImeiProductName = view.GetFocusedRowCellValue("PRODUCTNAME").ToString().Trim();
                    strImeiCheck = e.Value.ToString();
                    string strError = ValidateImei(strImeiCheck, strImeiProductID, strImeiProductName);
                    if (!string.IsNullOrEmpty(strError))
                    {
                        //e.Valid = false;
                        //e.ErrorText = strError;
                        MessageBoxObject.ShowWarningMessage(this, strError);
                        e.Value = null;
                        return;
                    }
                }
            }
        }
        private string ValidateImei(string strIMEI, string strProductID, string strProductName)
        {
            DataTable dtbTmp = (grdProduct.DataSource as DataTable);
            dtbTmp.AcceptChanges();
            DataTable dtb = dtbTmp.Copy();
            string strImeiNull = string.Empty;
            int intBorrowStoreID = cboBorrowStoreID.StoreID;
            if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI))
            {
                //MessageBoxObject.ShowWarningMessage(this, "IMEI không đúng định dạng");
                return "Imei " + strIMEI + "không đúng định dạng";
            }
            if (!string.IsNullOrWhiteSpace(strIMEI))
            {
                //lay nhung imei khong thuoc san pham da chon
                string strImeiNotSameProductID = string.Empty;
                strIMEI = strIMEI.Replace("\r\n", ",").Replace("\n", ",").Replace(";", ",");
                foreach (string imei in strIMEI.Split(','))
                {
                    if (!string.IsNullOrEmpty(imei))
                    {
                        //imei khong trung voi ma san pham
                        if (string.Equals(imei, strProductID))
                        {
                            //MessageBoxObject.ShowWarningMessage(this, "Imei: " + imei + " đã trùng với mã sản phẩm. Vui lòng nhập imei khác.");
                            return "Imei: " + imei + " đã trùng với mã sản phẩm. Vui lòng nhập imei khác.";
                        }

                        if (grdViewProduct.FocusedRowHandle >= 0)
                            dtb.Rows.RemoveAt(grdViewProduct.FocusedRowHandle);
                        DataRow[] rowImei = dtb.Select("IMEI = '" + imei.Trim() + "'");
                        if (rowImei.Count() > 0)
                        {
                            return "Imei: " + imei + " đã tồn tại trên lưới. Vui lòng nhập imei khác.";
                        }
                    }
                }

            }
            if (!string.IsNullOrWhiteSpace(strImeiNull))
            {
                if (strImeiNull[0] == ',') strImeiNull = strImeiNull.Remove(0, 1);
                //MessageBoxObject.ShowWarningMessage(this, "Imei: " + strImeiNull + " không tồn tại trong hệ thống.");
                return "Imei: " + strImeiNull + " không tồn tại trong hệ thống.";
            }
            return string.Empty;
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            if (bolIsDeleted)
                return;
            //if (intBorrowSatus == 0)
            //{
            //    if (grdViewProduct.RowCount == 0)
            //        mnuAction.Enabled = false;
            //    else
            //        mnuAction.Enabled = true;
            //}
            //else
            //    mnuAction.Enabled = false;
            //if (!IsHaveAllChuaDuyet)
            mnuAction.Enabled = eActionType == ActionType.NEW && grdViewProduct.RowCount > 0;
        }

        private void frmBorrowProduct_Update_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (frmBorrow == null)
                return;
            //frmBorrow.RefreshView();
        }

        private void grdViewProduct_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (bolIsUpdate)
            {
                if (!bolIsDeleted)
                {
                    if (intBorrowSatus == 0)
                    {
                        if (grdViewProduct.FocusedRowHandle >= 0)
                        {
                            DataRow row = (DataRow)grdViewProduct.GetDataRow(grdViewProduct.FocusedRowHandle);
                            if (!string.IsNullOrEmpty(Convert.ToString(row["IMEI"])))
                            {
                                if (grdViewProduct.FocusedColumn.FieldName.ToLower() != "note")
                                    e.Cancel = true;
                            }
                            if (bolIsRequestIMEI)
                            {
                                if (string.IsNullOrEmpty(Convert.ToString(row["IMEI"])))
                                {
                                    e.Cancel = false;
                                    if (grdViewProduct.FocusedColumn.FieldName.ToLower() != "imei" && grdViewProduct.FocusedColumn.FieldName.ToLower() != "note" && grdViewProduct.FocusedColumn.FieldName.ToLower() != "machinestatus" && grdViewProduct.FocusedColumn.FieldName.ToLower() != "machinestatusreturn")
                                        e.Cancel = true;
                                }
                            }
                            else
                            {
                                if (grdViewProduct.FocusedColumn.FieldName.ToLower() == "imei")
                                    e.Cancel = true;
                            }
                        }
                    }
                    else if (intBorrowSatus == 1)
                    {
                        GridView view = sender as GridView;
                        if (grdViewProduct.FocusedColumn.FieldName.ToLower() != "machinestatusreturn")
                            e.Cancel = true;
                    }
                    else
                    {
                        GridView view = sender as GridView;
                        e.Cancel = true;
                    }
                }
                else
                {
                    GridView view = sender as GridView;
                    e.Cancel = true;
                }
                //if (intBorrowSatus == 0)
                //{
                //    if (grdViewProduct.FocusedRowHandle >= 0)
                //    {
                //        DataRow row = (DataRow)grdViewProduct.GetDataRow(grdViewProduct.FocusedRowHandle);
                //        if (Convert.ToBoolean(row["IsRequestIMEI"]))
                //        {
                //            if (grdViewProduct.FocusedColumn.FieldName.ToLower() != "note")
                //                e.Cancel = true;
                //        }
                //        if (string.IsNullOrEmpty(Convert.ToString(row["IMEI"])))
                //        {
                //            e.Cancel = false;
                //            if (grdViewProduct.FocusedColumn.FieldName.ToLower() != "quantity" && grdViewProduct.FocusedColumn.FieldName.ToLower() != "note")
                //                e.Cancel = true;
                //        }
                //    }
                //}
                //else
                //{
                //    GridView view = sender as GridView;
                //    e.Cancel = true;
                //}
            }
            else
                if (grdViewProduct.FocusedRowHandle >= 0)
                {
                    DataRow row = (DataRow)grdViewProduct.GetDataRow(grdViewProduct.FocusedRowHandle);
                    if (Convert.ToBoolean(row["IsRequestIMEI"]))
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(row["IMEI"])))
                        {
                            e.Cancel = false;
                            if (grdViewProduct.FocusedColumn.FieldName.ToLower() != "imei" && grdViewProduct.FocusedColumn.FieldName.ToLower() != "note" && grdViewProduct.FocusedColumn.FieldName.ToLower() != "machinestatus" && grdViewProduct.FocusedColumn.FieldName.ToLower() != "machinestatusreturn")
                                e.Cancel = true;
                        }
                    }
                    else
                    {
                        if (grdViewProduct.FocusedColumn.FieldName.ToLower() == "imei")
                            e.Cancel = true;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(row["IMEI"])))
                    {
                        if (grdViewProduct.FocusedColumn.FieldName.ToLower() == "quantity")
                            e.Cancel = true;
                    }
                }
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 13) return;
            if (!ValidateStore()) return;
            txtBarcode.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtBarcode.Text.Trim());
            AddProduct(txtBarcode.Text.Trim());
            txtBarcode.Text = string.Empty;
            txtBarcode.Focus();
        }

        private void grdViewProduct_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            string strColName = grdViewProduct.FocusedColumn.FieldName.ToUpper();
            if (grdViewProduct.FocusedRowHandle < 0)
                return;
            if (strColName == "QUANTITY")
            {
                DataRow row = (DataRow)grdViewProduct.GetDataRow(grdViewProduct.FocusedRowHandle);
                try
                {
                    string strProductID = row["ProductID"].ToString().Trim();
                    int decQuantity = Convert.ToInt32(Globals.IsNull(row["QUANTITY"], 1));
                    PLC.WSProductInStock.ProductInStock objProductInStock = new PLC.PLCProductInStock().LoadInfo(strProductID, cboBorrowStoreID.StoreID);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    if (decQuantity > objProductInStock.Quantity)
                    {
                        MessageBox.Show(this, "Số lượng vượt quá số lượng tồn trong kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        row["Quantity"] = objProductInStock.Quantity;
                    }
                }
                catch { }
            }

            DataRow rowi = (DataRow)grdViewProduct.GetDataRow(grdViewProduct.FocusedRowHandle);
            bool bolRequestIMEI = Convert.ToBoolean(rowi["ISREQUESTIMEI"]);
            if (bolRequestIMEI)
            {
                if (strColName == "IMEI")
                {
                    string strIMei = rowi["IMEI"].ToString().Trim();
                    if (!string.IsNullOrEmpty(strIMei))
                    {
                        if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMei))
                        {
                            MessageBox.Show(this, "IMEI không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            rowi["IMEI"] = string.Empty;
                            return;
                        }
                        PLC.WSProductInStock.ProductInStock objIMEI = new ERP.Inventory.PLC.PLCProductInStock().LoadInfo(strIMei, cboBorrowStoreID.StoreID);
                        if (objIMEI == null)
                        {
                            MessageBox.Show(this, "Số IMEI không tồn tại trong kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            rowi["IMEI"] = string.Empty;
                            return;
                        }

                        if (objIMEI.IsOrder)	// kiểm tra IMEI có được đặt hàng hay chưa
                        {
                            if (MessageBox.Show(this, "Số IMEI " + objIMEI.IMEI + " đã được đặt hàng trong đơn hàng: [" + objIMEI.OrderID + "]. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
                            {
                                rowi["IMEI"] = string.Empty;
                                return;
                            }
                        }
                    }
                }
            }
        }

        private void ctrlUser_Leave(object sender, EventArgs e)
        {
            // Nếu form được mở từ Bảo hành thì không cần check điều kiện dưới - LÊ VĂN ĐÔNG
            if (bolIsNewFromWarranty) return; 

            if (!string.IsNullOrEmpty(ctrlUser.FullName) && ctrlUser.Enabled)
            {
                if ((!bolIsUpdate) && ctrlUser.UserName.Equals(SystemConfig.objSessionUser.UserName))
                {
                    MessageBox.Show(this, "Nhân viên mượn không được trùng nhân viên tạo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ctrlUser.UserName = string.Empty;
                    return;
                }
                else
                {
                    if (objBorrowProduct != null && !string.IsNullOrEmpty(objBorrowProduct.CreatedUser))
                    {
                        if (ctrlUser.UserName.Equals(objBorrowProduct.CreatedUser))
                        {
                            MessageBox.Show(this, "Nhân viên mượn không được trùng nhân viên tạo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            ctrlUser.UserName = string.Empty;
                            return;
                        }
                    }
                }
            }
        }


        #region Load loai muon hang
        ERP.MasterData.PLC.MD.PLCBorrowProductType objPLCBorrowProductType = null;
        string strBorrowProductType_AddfunctionID = string.Empty;
        string strBorrowProductType_EditAllFunctionID = string.Empty;
        string strBorrowProductType_DeletedAllFunctionID = string.Empty;
        string strBorrowProductType_DeletedReviewedFunctionID = string.Empty;

        public bool IsPermission_BorrowProductType_Addfunction = false;
        public bool IsPermission_BorrowProductType_EditAllFunction = false;
        public bool IsPermission_BorrowProductType_DeletedAllFunction = false;
        public bool IsPermission_BorrowProductType_DeletedReviewedFunction = false;

        public void LoadBorrowProductTypeInfor(string _strBorrowProductTypeID)
        {
            if (objPLCBorrowProductType == null)
                objPLCBorrowProductType = new MasterData.PLC.MD.PLCBorrowProductType();
            if (string.IsNullOrEmpty(_strBorrowProductTypeID))
                _strBorrowProductTypeID = strBorrowProductTypeID;
            ERP.MasterData.PLC.WSBorrowProductType.BORRowProductType objBORRowProductType = null;
            objPLCBorrowProductType.LoadInfo(ref objBORRowProductType, Convert.ToInt32(_strBorrowProductTypeID));
            if (objBORRowProductType == null) return;
            strBorrowProductType_AddfunctionID = objBORRowProductType.AddFunctionID;
            strBorrowProductType_EditAllFunctionID = objBORRowProductType.EditAllFunctionID;
            strBorrowProductType_DeletedAllFunctionID = objBORRowProductType.DeleteAllFunctionID;
            strBorrowProductType_DeletedReviewedFunctionID = objBORRowProductType.DeleteFunctionReviewedID;

            IsPermission_BorrowProductType_Addfunction = false;
            IsPermission_BorrowProductType_EditAllFunction = false;
            IsPermission_BorrowProductType_DeletedAllFunction = false;
            IsPermission_BorrowProductType_DeletedReviewedFunction = false;

            if (SystemConfig.objSessionUser.UserName == "administrator")
            {
                IsPermission_BorrowProductType_Addfunction = true;
                IsPermission_BorrowProductType_EditAllFunction = true;
                IsPermission_BorrowProductType_DeletedAllFunction = true;
                IsPermission_BorrowProductType_DeletedReviewedFunction = true;
                return;
            }
            else
            {
                IsPermission_BorrowProductType_Addfunction = SystemConfig.objSessionUser.IsPermission(strBorrowProductType_AddfunctionID);
                IsPermission_BorrowProductType_EditAllFunction = SystemConfig.objSessionUser.IsPermission(strBorrowProductType_EditAllFunctionID);
                IsPermission_BorrowProductType_DeletedAllFunction = SystemConfig.objSessionUser.IsPermission(strBorrowProductType_DeletedAllFunctionID);
                IsPermission_BorrowProductType_DeletedReviewedFunction = SystemConfig.objSessionUser.IsPermission(strBorrowProductType_DeletedReviewedFunctionID);
            }
        }



        #endregion
        #region Muc Duyet
        DataTable dtbReviewStatus()
        {
            DataTable dtb = new DataTable();
            dtb.Columns.AddRange(
                new DataColumn[] {
                    new DataColumn("ID",typeof(Int32)),
                    new DataColumn("NAME",typeof(string))
                });
            dtb.Rows.Add(-1, "Chưa duyệt");
            dtb.Rows.Add(0, "Từ chối");
            dtb.Rows.Add(1, "Đồng ý");
            return dtb;
        }

        void FormatGridReivew()
        {
            GridView view = grvReViewLevel;
            view.OptionsView.ColumnAutoWidth = false;
            view.OptionsBehavior.Editable = true;

            RepositoryItemCheckEdit ReCheckChon = new RepositoryItemCheckEdit();
            ReCheckChon.ValueChecked = (decimal)1;
            ReCheckChon.ValueUnchecked = (decimal)0;
            ReCheckChon.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            ReCheckChon.CheckedChanged += ReCheckChon_CheckedChanged;
            RepositoryItemCheckEdit ReCheckReviewed = new RepositoryItemCheckEdit();
            ReCheckReviewed.ValueChecked = (decimal)1;
            ReCheckReviewed.ValueUnchecked = (decimal)0;
            ReCheckReviewed.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;

            RepositoryItemDateEdit ReDateTime = new RepositoryItemDateEdit();
            ReDateTime.Mask.EditMask = "dd/MM/yyyy hh:mm:sss";
            ReDateTime.Mask.UseMaskAsDisplayFormat = true;

            RepositoryItemLookUpEdit ReLoopupReviewStatus = new RepositoryItemLookUpEdit();
            ReLoopupReviewStatus.ValueMember = "ID";
            ReLoopupReviewStatus.DisplayMember = "NAME";
            ReLoopupReviewStatus.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Tình trạng", 100));
            ReLoopupReviewStatus.DataSource = dtbReviewStatus();


            view.Columns["STT"].Caption = "STT";
            view.Columns["STT"].Width = 50;
            view.Columns["STT"].OptionsColumn.AllowEdit = false;
            view.Columns["STT"].OptionsColumn.FixedWidth = true;
            view.Columns["STT"].AppearanceCell.Options.UseTextOptions = true;
            view.Columns["STT"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            view.Columns["REVIEWLEVELNAME"].Caption = "Mức duyệt";
            view.Columns["REVIEWLEVELNAME"].Width = 200;
            view.Columns["REVIEWLEVELNAME"].OptionsColumn.AllowEdit = false;
            view.Columns["REVIEWLEVELNAME"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;

            view.Columns["ISSELECT"].Caption = "Chọn";
            view.Columns["ISSELECT"].Width = 50;
            view.Columns["ISSELECT"].ColumnEdit = ReCheckChon;
            view.Columns["ISSELECT"].OptionsColumn.FixedWidth = true;

            view.Columns["REVIEW_FULLNAME"].Caption = "Nhân viên duyệt";
            view.Columns["REVIEW_FULLNAME"].Width = 200;
            view.Columns["REVIEW_FULLNAME"].OptionsColumn.AllowEdit = false;
            view.Columns["REVIEW_FULLNAME"].OptionsColumn.FixedWidth = true;
            view.Columns["REVIEW_FULLNAME"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;

            view.Columns["REVIEWEDSTATUS"].Caption = "Trạng thái duyệt";
            view.Columns["REVIEWEDSTATUS"].Width = 120;
            view.Columns["REVIEWEDSTATUS"].ColumnEdit = ReLoopupReviewStatus;
            view.Columns["REVIEWEDSTATUS"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            view.Columns["REVIEWEDSTATUS"].OptionsColumn.AllowEdit = false;

            view.Columns["ISREVIEWED"].Caption = "Đã duyệt";
            view.Columns["ISREVIEWED"].Width = 75;
            view.Columns["ISREVIEWED"].ColumnEdit = ReCheckReviewed;
            view.Columns["ISREVIEWED"].OptionsColumn.AllowEdit = false;

            view.Columns["REVIEWEDDATE"].Caption = "Ngày duyệt";
            view.Columns["REVIEWEDDATE"].Width = 135;
            view.Columns["REVIEWEDDATE"].ColumnEdit = ReDateTime;
            view.Columns["REVIEWEDDATE"].OptionsColumn.AllowEdit = false;

            view.Columns["ISREVIEWEDCHECKINBYFINGER"].Caption = "Hình thức duyệt";
            view.Columns["ISREVIEWEDCHECKINBYFINGER"].Width = 175;
            view.Columns["ISREVIEWEDCHECKINBYFINGER"].OptionsColumn.AllowEdit = false;


            view.Columns["NOTE"].Caption = "Ghi chú duyệt";
            view.Columns["NOTE"].Width = 250;
            view.Columns["NOTE"].OptionsColumn.AllowEdit = false;

            view.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            view.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            view.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            view.OptionsView.ShowAutoFilterRow = true;
            view.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            view.OptionsView.ShowFooter = false;
            view.OptionsView.ShowGroupPanel = false;
            for (int i = 0; i < view.Columns.Count; i++)
            {
                view.Columns[i].AppearanceHeader.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
                view.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                view.Columns[i].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;


                view.Columns[i].AppearanceHeader.Options.UseTextOptions = true;
                view.Columns[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                string strFieldName = view.Columns[i].FieldName;
                if (strFieldName == "REVIEWTYPE"
                    || strFieldName == "ISREADONLY"
                    || strFieldName == "REVIEW_USER") view.Columns[i].Visible = false;
            }
        }

        void ReCheckChon_CheckedChanged(object sender, EventArgs e)
        {
            DataRow dr = grvReViewLevel.GetFocusedDataRow();
            if (dr == null) return;
            CheckEdit chk = (CheckEdit)sender;
            if (chk.CheckState == CheckState.Checked)
            {
                string strReviewLevelID = (dr["REVIEWLEVELID"] ?? "").ToString().Trim();
                string strReviewUser = (dr["REVIEW_USER"] ?? "").ToString().Trim();
                DataTable dtb = (DataTable)gridReViewLevel.DataSource;
                var varResult = dtb.AsEnumerable().Where(row => (row.Field<object>("REVIEWLEVELID") ?? "").ToString().Trim() == strReviewLevelID
                                                               && (row.Field<object>("REVIEW_USER") ?? "").ToString().Trim() != strReviewUser);
                if (varResult.Any())
                {
                    foreach (DataRow drResult in varResult)
                        drResult["ISSELECT"] = 0;
                }
                grvReViewLevel.CloseEditor();
                grvReViewLevel.UpdateCurrentRow();
            }
        }

        string strREVIEWLEVELID = string.Empty;
        string strSEQUENCEREVIEW = string.Empty;
        List<string> strUserList;
        void LoadDataReview()
        {
            DataTable dtbReview = null;
            int IsEdit = 0;
            if (eActionType == ActionType.NEW) IsEdit = 1;
            PLC.BorrowProduct.WSBorrowProduct.ResultMessage objResultMessage = objPLCBorrowProduct.SearchDataReview(ref dtbReview,
                "@BORROWPRODUCTID", strBorrowProductID,
                "@BORROWPRODUCTTYPEID", strBorrowProductTypeID,
                "@ISEDIT", IsEdit
                );
            if (objResultMessage.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return;
            }
            bool IsShowDetailReview = true;
            DataRow[] drs = dtbReview.Select("REVIEWEDSTATUS = -1");
            if (drs != null)
            {
                if (drs.Count() == dtbReview.Rows.Count)
                    IsShowDetailReview = false;
            }
            if (IsEdit == 0) IsShowDetailReview = true;
            string[] ColumnsListAn = null;
            if (!IsShowDetailReview)
                ColumnsListAn = new string[] { "REVIEWEDSTATUS", "ISREVIEWED", "REVIEWEDDATE", "ISREVIEWEDCHECKINBYFINGER", "NOTE" };
            else
                ColumnsListAn = new string[] { "ISSELECT" };
            if (ColumnsListAn != null)
            {
                for (int i = 0; i < grvReViewLevel.Columns.Count; i++)
                {
                    string strFieldName = grvReViewLevel.Columns[i].FieldName;
                    if (ColumnsListAn.Contains(strFieldName))
                        grvReViewLevel.Columns[i].Visible = false;
                }
            }
            if (eActionType == ActionType.NEW)
            {
                grvReViewLevel.Columns["REVIEWLEVELNAME"].Width =
                    (colProductID.Width + colProductName.Width) - grvReViewLevel.Columns["STT"].Width;
                grvReViewLevel.Columns["REVIEW_FULLNAME"].Width =
                    (colIMEI.Width + colIsNew.Width + colQuantity.Width + colNote.Width) - grvReViewLevel.Columns["ISSELECT"].Width;
            }
            gridReViewLevel.DataSource = dtbReview;
            strUserList = new List<string>(); ;
            // Kiem tra nguoi duyet ke tiep la ai 
            var varResult = dtbReview.AsEnumerable().
                    Where(row => (row.Field<object>("REVIEWEDSTATUS") ?? "-1").ToString().Trim() == "-1");
            if (IsShowDetailReview)
            {
                //var varResult2 = dtbReview.AsEnumerable().
                //    Where(row => (row.Field<object>("REVIEWEDSTATUS") ?? "-1").ToString().Trim() == "0");
                //if (varResult2.Any())
                //{
                if (varResult.Any())
                {
                    DataTable dtbResult = varResult.CopyToDataTable();
                    DataView dtvResult = dtbResult.AsDataView();
                    dtvResult.Sort = "STT";
                    string strSTT = string.Empty;
                    foreach (DataRowView drV in dtvResult)
                    {
                        if (string.IsNullOrEmpty(strSTT))
                        {
                            strSTT = (drV["STT"] ?? "").ToString().Trim();
                            strSEQUENCEREVIEW = strSTT;
                            strREVIEWLEVELID = (drV["REVIEWLEVELID"] ?? "").ToString().Trim();
                        }
                        if (strSTT == (drV["STT"] ?? "").ToString().Trim())
                            strUserList.Add((drV["REVIEW_USER"] ?? "").ToString().Trim());
                        else break;
                    }
                }
            }
        }
        #endregion

        private void grvReViewLevel_CellMerge(object sender, CellMergeEventArgs e)
        {
            if (e.Column.FieldName == "STT")
            {
                string protectionId1 = grvReViewLevel.GetRowCellValue(e.RowHandle1, "STT").ToString();
                string protectionId2 = grvReViewLevel.GetRowCellValue(e.RowHandle2, "STT").ToString();
                if (protectionId1 == protectionId2)
                {
                    e.Merge = true;
                    e.Handled = false;
                }
                else
                {
                    e.Merge = false;
                    e.Handled = true;
                }
                return;
            }
            if (e.Column.FieldName == "REVIEWLEVELNAME")
            {
                string protectionId1 = grvReViewLevel.GetRowCellValue(e.RowHandle1, "STT").ToString();
                string protectionId2 = grvReViewLevel.GetRowCellValue(e.RowHandle2, "STT").ToString();
                if (protectionId1 == protectionId2)
                {
                    e.Merge = true;
                    e.Handled = false;
                }
                else
                {
                    e.Merge = false;
                    e.Handled = true;
                }
                return;
            }
            e.Handled = true;
        }

        private void grvReViewLevel_ShowingEditor(object sender, CancelEventArgs e)
        {
            DataRow dr = grvReViewLevel.GetFocusedDataRow();
            if (dr == null) return;
            if (grvReViewLevel.FocusedColumn.FieldName == "ISSELECT")
            {
                if ((dr["ISREADONLY"] ?? "0").ToString() == "1")
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void btnTuChoi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //DialogResult drResult = MessageBox.Show("Bạn có muốn duyệt phiếu mượn hàng này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //if (drResult == System.Windows.Forms.DialogResult.No)
            //    return;
            //bool IsShowForm = true;
            //if (strUserList.Contains(SystemConfig.objSessionUser.UserName) && !bolIsOnlyCertifyFinger)
            //    IsShowForm = false;
            string strUserReview = SystemConfig.objSessionUser.UserName;
            //if (IsShowForm)
            //{
            //    Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên duyệt phiếu mượn hàng", "Duyệt phiếu mượn hàng", strUserList,
            //       null, !bolIsOnlyCertifyFinger, bolIsOnlyCertifyFinger);
            //    frmCertifyLogin1.ShowDialog(this);
            //    if (!frmCertifyLogin1.IsCorrect)
            //        return;
            //    strUserReview = frmCertifyLogin1.UserName;
            //}
            int intReviewStatus = (int)e.Item.Tag;
            string strTitle = "Nhập ghi chú duyệt - Đồng ý";
            string strTitleCertify = "Xác nhận duyệt đồng ý";
            if (intReviewStatus == 0)
            {
                strTitle = "Nhập ghi chú duyệt - Từ chối";
                strTitleCertify = "Xác nhận duyệt từ chối";
            }

            Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin(strTitleCertify, strTitleCertify, strUserList,
                 null, !bolIsOnlyCertifyFinger, bolIsOnlyCertifyFinger);
            frmCertifyLogin1.ShowDialog(this);
            if (!frmCertifyLogin1.IsCorrect)
                return;
            strUserReview = frmCertifyLogin1.UserName;


            string strNode = ShowReasion(strTitle, 600);
            if (strNode == "-1")
                return;
            if (!ValidateStore())
                return;
            if (!ValidateProductCheck())
                return;
            BorrowProduct_RVL objBorrowProduct_RVL = new BorrowProduct_RVL();
            objBorrowProduct_RVL.BorrowProductID = strBorrowProductID;
            objBorrowProduct_RVL.ReviewLevelID = Convert.ToInt32(strREVIEWLEVELID);
            objBorrowProduct_RVL.Note = strNode;
            objBorrowProduct_RVL.IsReviewedCheckInByFinger = bolIsOnlyCertifyFinger;
            objBorrowProduct_RVL.UserName = strUserReview;
            objBorrowProduct_RVL.ReviewedStatus = intReviewStatus;
            objBorrowProduct_RVL.SequenceReview = Convert.ToInt32(strSEQUENCEREVIEW);
            PLC.BorrowProduct.WSBorrowProduct.ResultMessage objResultMessage = objPLCBorrowProduct.UserReview(objBorrowProduct_RVL);
            if (objResultMessage.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, objResultMessage.Message, objResultMessage.MessageDetail);
                LoadDataReview();
                return;
            }
            MessageBoxObject.ShowInfoMessage(this, "Đã duyệt phiếu mượn hàng thành công!");
            IsRefreshData = true;
            IsUpdate = true;
            SetActionTypeForm(2);
            LoadDataReview();
            CheckButton();
            //this.Close();
        }

        string ShowReasion(string strTitle, int MaxLengthString)
        {

            string strReasion = "-1";
            Library.AppControl.FormCommon.frmInputString frm = new Library.AppControl.FormCommon.frmInputString();
            frm.MaxLengthString = MaxLengthString;
            frm.IsAllowEmpty = false;
            frm.Text = strTitle;
            frm.ShowDialog();
            if (frm.IsAccept)
                strReasion = frm.ContentString;
            return strReasion;
        }

        private void grdViewProduct_HiddenEditor(object sender, EventArgs e)
        {
            //GridView view = (GridView)sender;
            //if (view.FocusedColumn.FieldName == "QUANTITY"
            //    && (eActionType == ActionType.EDIT || eActionType == ActionType.NEW))
            //{
            //    int intStatus = 0;
            //    decimal decQuantity = Convert.ToDecimal((view.GetFocusedRowCellValue(view.FocusedColumn.FieldName) ?? "0").ToString().Trim());
            //    if (!CheckExsitAndQuantity(strBorrowProductID,
            //                                (view.GetFocusedRowCellValue("PRODUCTID") ?? "").ToString().Trim(),
            //                                Convert.ToBoolean((view.GetFocusedRowCellValue("ISNEW") ?? "0").ToString().Trim()),
            //                                (view.GetFocusedRowCellValue("IMEI") ?? "").ToString().Trim(),
            //                                decQuantity, out intStatus))
            //    {
            //        int intRowHanle = view.FocusedRowHandle;
            //        view.Focus();

            //        view.FocusedRowHandle = intRowHanle;
            //        view.FocusedColumn = view.Columns["QUANTITY"];
            //        view.GridControl.BeginInvoke((MethodInvoker)delegate { view.ShowEditor(); });
            //        return;
            //    }
            //}
        }
        bool IsHaveAllChuaDuyet = false;
        public void CheckButton()
        {

            bool IsHaveTuChoi = false;
            bool IsHaveChuaDuyet = false;
            IsHaveAllChuaDuyet = false;
            if (gridReViewLevel.DataSource != null)
            {
                var varResultSearch = ((DataTable)gridReViewLevel.DataSource).AsEnumerable().
                       Where(row => (row.Field<object>("REVIEWEDSTATUS") ?? "-1").ToString().Trim() == "0");
                IsHaveTuChoi = varResultSearch.Any();
                var varResultSearch2 = ((DataTable)gridReViewLevel.DataSource).AsEnumerable().
                       Where(row => (row.Field<object>("REVIEWEDSTATUS") ?? "-1").ToString().Trim() == "-1");
                IsHaveChuaDuyet = varResultSearch2.Any();
                if (IsHaveChuaDuyet)
                {
                    if (varResultSearch2.Count() == ((DataTable)gridReViewLevel.DataSource).Rows.Count)
                        IsHaveAllChuaDuyet = true;
                }

                // btnDelete.Visible = varResultSearch.Any() && !objBorrowProduct.IsDeleted && (eActionType == ActionType.EDIT || eActionType == ActionType.READ_ONLY);
            }

            if (eActionType == ActionType.NEW)
                grdViewProduct.OptionsBehavior.Editable = true;
            else
                grdViewProduct.OptionsBehavior.Editable = !objBorrowProduct.IsDeleted && objBorrowProduct.BorrowStatus == 1;
            mnuAction.Enabled = eActionType == ActionType.NEW;

            btnBorrow.Visible = btnReturn.Visible = true;
            btnBorrow.Enabled = !objBorrowProduct.IsDeleted && objBorrowProduct.BorrowStatus == 0 && objBorrowProduct.ReviewedStatus == 1;
            btnReturn.Enabled = !objBorrowProduct.IsDeleted && objBorrowProduct.BorrowStatus == 1;

            btnUpdate.Visible = !objBorrowProduct.IsDeleted
                                && objBorrowProduct.BorrowStatus == 0;

            btnDelete.Visible = !objBorrowProduct.IsDeleted
                                && objBorrowProduct.BorrowStatus == 0
                                && (eActionType == ActionType.EDIT || eActionType == ActionType.READ_ONLY);


            btnAction.Visible = !objBorrowProduct.IsDeleted && grvReViewLevel.RowCount > 0
                                && (eActionType == ActionType.EDIT || eActionType == ActionType.READ_ONLY)
                                && objBorrowProduct.ReviewedStatus == -1
                                && objBorrowProduct.BorrowStatus == 0
                                && !IsHaveTuChoi;


            dtReturnPlanDate.Enabled = IsHaveAllChuaDuyet;

            btnUpdate.Enabled = mnuAction.Enabled = eActionType == ActionType.NEW
                               || (eActionType == ActionType.EDIT
                               && (IsPermission_BorrowProductType_EditAllFunction
                               || (objBorrowProduct.CreatedUser == (SystemConfig.objSessionUser.UserName))))
                               && objBorrowProduct.ReviewedStatus == -1;

            btnDelete.Enabled = !objBorrowProduct.IsDeleted
                                && objBorrowProduct.BorrowStatus == 0
                                && ((IsPermission_BorrowProductType_DeletedReviewedFunction
                                    && objBorrowProduct.BorrowStatus == 0)
                                    || ((IsPermission_BorrowProductType_DeletedAllFunction && IsHaveAllChuaDuyet)
                                    || (IsHaveAllChuaDuyet && objBorrowProduct.CreatedUser == (SystemConfig.objSessionUser.UserName))));

        }
        int intClosedForm = 1;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (intClosedForm == 0)
            {
                intClosedForm = 1;
                //if (strMessageCloseForm != string.Empty)
                //    MessageBoxObject.ShowWarningMessage(this, strMessageCloseForm);
                System.Threading.Thread.Sleep(500);
                this.Close();
            }
        }

        private void grdViewProduct_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (grdViewProduct.RowCount == 0) return;
            e.HighPriority = true;
            string strStatus = (grdViewProduct.GetRowCellValue(e.RowHandle, "STATUS") ?? "").ToString().Trim();
            if (strStatus != "0")
                e.Appearance.BackColor = Color.Pink;
        }

        private void grdViewProduct_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "STATUS")
            {
                string IMEI = (grdViewProduct.GetRowCellValue(e.RowHandle, "IMEI") ?? "").ToString();
                string strTile = "Sản phẩm";
                if (!string.IsNullOrEmpty(IMEI)) strTile = "IMEI";
                string strStatus = (e.Value ?? "").ToString().Trim();
                if (strStatus == "0" || string.IsNullOrEmpty(strStatus))
                {
                    e.DisplayText = string.Empty;
                    return;
                }
                if ((strStatus == "1" || strStatus == "3") && !string.IsNullOrEmpty(IMEI))
                {
                    e.DisplayText = strTile + " đang tồn tại phiếu mượn hàng!";
                    return;
                }
                if (strStatus == "2" && !string.IsNullOrEmpty(IMEI))
                {
                    e.DisplayText = strTile + " đang cho mượn chưa trả!";
                    return;
                }
                if (strStatus == "4" && !string.IsNullOrEmpty(IMEI))
                {
                    e.DisplayText = strTile + " không còn tồn trong kho!";
                    return;
                }
                if (strStatus == "-2" && string.IsNullOrEmpty(IMEI))
                {
                    e.DisplayText = "SL mượn tối đa là 0";
                    return;
                }
                if (strStatus != "0" && string.IsNullOrEmpty(IMEI))
                {
                    e.DisplayText = "SL mượn tối đa là " + strStatus;
                    return;

                }
            }
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            btnAction.ShowDropDown();
        }

        private void grdProduct_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void AddAccessory(DataTable dtbSelected, string strBorrowProductDTId, ref string strAccessoryList)
        {
            if (dtbAccessory == null)
            {
                dtbAccessory = new DataTable("TableAccessory");
                DataColumn[] col = new DataColumn[]{new DataColumn("BORROWPRODUCTDTID", typeof(String))
                                                ,new DataColumn("MACHINEACCESSORYID", typeof(String))
                                                ,new DataColumn("NOTE", typeof(String))
                                                ,new DataColumn("GUIDID", typeof(Guid))
                                                };
                dtbAccessory.Columns.AddRange(col);
            }

            DataRow[] drr = dtbAccessory.Select("BORROWPRODUCTDTID='" + strBorrowProductDTId + "'");
            for (int i = 0; i < drr.Length; i++)
                dtbAccessory.Rows.Remove(drr[i]);
            dtbAccessory.AcceptChanges();

            if (dtbSelected != null && dtbSelected.Rows.Count > 0)
            {
                // Add row
                foreach (DataRow rowSelect in dtbSelected.Rows)
                {
                    DataRow row = dtbAccessory.NewRow();
                    row["BORROWPRODUCTDTID"] = strBorrowProductDTId;
                    row["MACHINEACCESSORYID"] = rowSelect["MACHINEACCESSORYID"].ToString();
                    row["NOTE"] = rowSelect["NOTE"].ToString(); ;
                    row["GUIDID"] = Guid.NewGuid();
                    dtbAccessory.Rows.Add(row);
                    if (strAccessoryList == string.Empty || strAccessoryList == "")
                        strAccessoryList += rowSelect["MACHINEACCESSORYNAME"].ToString();
                    else
                        strAccessoryList += ", " + rowSelect["MACHINEACCESSORYNAME"].ToString();
                }
                
            }
            else
                strAccessoryList = "--Chọn phụ kiện--";
        }

        private void grdViewProduct_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (grdViewProduct.FocusedRowHandle < 0 | bolIsUpdate) return;
            GridView view = (GridView)sender;

            Point pt = view.GridControl.PointToClient(Control.MousePosition);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(pt);
            if (info.InRow || info.InRowCell)
            {
                string colName = info.Column == null ? "N/A" : info.Column.Name;
                if (colName == "colAccessoryList")
                {
                    DataRow row = (DataRow)view.GetDataRow(view.FocusedRowHandle);
                    string strGuid = row["GUIDID"].ToString();
                    string strMainGroupId = row["MAINGROUPID"].ToString();
                    string strProductId = row["PRODUCTID"].ToString();
                    string strProductName = row["PRODUCTNAME"].ToString();


                    DataTable dtbOldSelect = null;
                    if (dtbAccessory != null)
                    {
                        dtbOldSelect = dtbAccessory.Clone();
                        DataRow[] rArr = dtbAccessory.Select("BORROWPRODUCTDTID='" + strGuid + "'");
                        foreach (DataRow r in rArr)
                        {
                            dtbOldSelect.ImportRow(r);
                        }
                    }


                    ERP.Inventory.DUI.Search.frmMachineAccessorySearch frm = new Search.frmMachineAccessorySearch(Convert.ToInt32(strMainGroupId));
                    frm.Text = "Phụ kiện thiết bị máy: " + strProductId + "-" + strProductName;
                    frm.dtbOldSelect = dtbOldSelect;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        string strAccessoryList = string.Empty;
                        AddAccessory(frm.dtbSelect, strGuid, ref strAccessoryList);
                        DataTable dtbProduct = grdProduct.DataSource as DataTable;
                        DataRow[] rowChange = dtbProduct.Select("GUIDID='" + strGuid + "'");
                        row["ACCESSORYLIST"] = strAccessoryList;

                        grdProduct.RefreshDataSource();
                    }
                }
            }
        }
    }
}
