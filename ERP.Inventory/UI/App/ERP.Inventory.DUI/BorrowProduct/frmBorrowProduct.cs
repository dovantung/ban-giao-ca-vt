﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.MasterData.DUI.Common;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Popup;
using DevExpress.XtraLayout;
using DevExpress.Utils.Win;


namespace ERP.Inventory.DUI.BorrowProduct
{
    public partial class frmBorrowProduct : Form
    {
        #region Variable
        private string strPermission_View = "PM_BORROWPRODUCT_VIEW";
        private ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new PLC.BorrowProduct.PLCBorrowProduct();
        private ERP.MasterData.PLC.MD.PLCBorrowProductType objPLCBorrowProductType = new ERP.MasterData.PLC.MD.PLCBorrowProductType();
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.ResultMessage objResultMessage = null;
        private ERP.Inventory.PLC.BorrowProduct.WSBorrowProduct.BorrowProduct objBorrowProduct = new PLC.BorrowProduct.WSBorrowProduct.BorrowProduct();
        private string strKeywords = string.Empty;
        private int intIRowHandle = -999997;
        private object[] objKeywords = null;
        private bool boolStatusBeforeReLoad = false;
        private DataTable dtbFullList = null;
        private DateTime dtmFromTime;
        private DateTime dtmToTime;
        private DataTable dtbSearch = null;
        private string strBorrowProductID = string.Empty;
        private bool bolIsOnlyCertifyFinger = false;
        #endregion

        #region Constructor
        public frmBorrowProduct()
        {
            InitializeComponent();
        }
        #endregion

        #region Event

        private void ChangeFocus(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                this.SelectNextControl(this.ActiveControl, true, true, true, true);
        }

        private void txtKeywords_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (btnSearch.Enabled)
                    btnSearch_Click(null, null);
                else return;
            }
        }

        private int intID = 0;
        void btnSearch_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;
            else
            {
                btnSearch.Enabled = false;
                boolStatusBeforeReLoad = chkIsDeleted.Checked;
                LoadData();
                btnSearch.Enabled = true;
                
                colDeletedUser.Visible = chkIsDeleted.Checked;
                colDeletedDate.Visible = chkIsDeleted.Checked;
                colDeleteReason.Visible = chkIsDeleted.Checked;
                if(chkIsDeleted.Checked){
                    colDeletedDate.VisibleIndex = colDeletedUser.VisibleIndex = colDeleteReason.VisibleIndex =
                        colUpdatedUser.VisibleIndex + 1;
                }


                //if (boolStatusBeforeReLoad)
                //{
                //    mnuItemView.Enabled = false;
                //}
            }
        }

        private void FocusRow(string strID)
        {
            try
            {
                if (strID.Trim().Length > 0)
                {
                    for (int i = 0; i < grdViewData.RowCount; i++)
                    {
                        string strIDTemp = Convert.ToString(grdViewData.GetRowCellValue(i, "BORROWPRODUCTID"));
                        if (strID.Trim() == strIDTemp.Trim())
                        {
                            grdViewData.FocusedRowHandle = i;
                            return;
                        }
                    }
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi focus trên lưới");
                SystemErrorWS.Insert("Lỗi focus trên lưới", objExce.ToString(), this.Name + " -> FocusRow", DUIInventory_Globals.ModuleName);
                return;
            }
        }
        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            mnuItemView_Click(null, null);
        }
        private void mnuItemView_Click(object sender, EventArgs e)
        {
            if (!mnuItemView.Enabled)
                return;
            if (grdViewData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            ERP.Inventory.DUI.BorrowProduct.frmBorrowProduct_Update frm = new frmBorrowProduct_Update();
            frm.BorrowProductID = row["BORROWPRODUCTID"].ToString();
            frm.IsOnlyCertifyFinger = bolIsOnlyCertifyFinger;
            strBorrowProductID = row["BORROWPRODUCTID"].ToString();
            frm.BorrowProductTypeID = row["BORROWPRODUCTTYPEID"].ToString();
            frm.IsDeleted=boolStatusBeforeReLoad;
            frm.BorrowSatus = Convert.ToInt32((row["BORROWSTATUS"]));
            frm.CreateUser = Convert.ToString(row["CREATEDUSER"]);
            //frm.IsUpdate = true;
            // Kiem tra thong tin duyet 
            frm.LoadBorrowProductTypeInfor(frm.BorrowProductTypeID);
            int intActionType = 2;
            if (frm.IsPermission_BorrowProductType_Addfunction
                && frm.CreateUser.Trim() == (SystemConfig.objSessionUser.UserName + "-" + SystemConfig.objSessionUser.FullName))
                intActionType = 1;
            if (frm.IsPermission_BorrowProductType_EditAllFunction)
                intActionType = 1;
            if (Convert.ToInt32(row["REVIEWEDSTATUS"]) > -1)
                intActionType = 2;
            frm.SetActionTypeForm(intActionType);
            frm.ShowDialog();
            if (frm.IsUpdate && frm.IsRefreshData)
                btnSearch_Click(null, null);
        }

        private void frmBorrowProduct_Load(object sender, EventArgs e)
        {
            InitControl();
            LoadCombobox();
        }

        private void frmBorrowProduct_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc muốn đóng màn hình này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
        }

        private void frmBorrowProduct_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.W && e.Modifiers == Keys.Control)
            {
                mnuItemView_Click(null, null);
            }
            else if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control)
            {
                mnuItemExportExcel_Click(null, null);
            }
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            if (!mnuItemExportExcel.Enabled)
                return;
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        #endregion

        #region Method

        private void InitControl()
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            this.chkIsDeleted.KeyPress += new KeyPressEventHandler(delegate(object sender, KeyPressEventArgs e) { CommonFunction.ChangeFocus(this, sender, e); });
            this.txtKeywords.KeyPress += new KeyPressEventHandler(txtKeywords_KeyPress);
            if (SystemConfig.objSessionUser.UserName != "administrator")
                mnuItemView.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_View);
            else
                mnuItemView.Enabled = true;

            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    bolIsOnlyCertifyFinger = Convert.ToBoolean(hstbParam["ISONLYCERTIFYFINGER"]);
                }
                catch { }
            }
        }

        private void LoadCombobox()
        {
            ComboBox cboTMP = new ComboBox();
            Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboTMP, true, -1, -1, -1, Library.AppCore.Constant.EnumType.StorePermissionType.OUTPUT, Library.AppCore.Constant.EnumType.AreaType.AREA, "ISCANOUTPUT = 1 and IsActive = 1");
            DataTable dtbStore = cboTMP.DataSource as DataTable;
            dtbStore.Rows.RemoveAt(0);
            LoadDataDM();
            cboBorrowStoreIDList.InitControl(true, dtbStore);
            cboBorrowStoreIDList.Title = "-- Chọn kho mượn --";
            //if (dtbStore.Rows.Count > 0)
            //{
            //    cboBorrowStoreIDList.SetValue(Convert.ToInt32(dtbStore.Rows[0]["StoreID"]));
            //    if (cboBorrowStoreIDList.StoreID != SystemConfig.intDefaultStoreID)
            //    {
            //        MessageBox.Show(this, "Bạn không có quyền xem tại kho này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        btnSearch.Enabled = false;
            //    }
            //}
            cboSearchBy.SelectedIndex = 1;
            cboBorrowStatus.SelectedIndex = 0;
            cboReviewStatus.SelectedIndex = 0;
        }

        private bool ValidateData()
        {
            //if (cboBorrowStatus.SelectedIndex == 0)
            //{
            //    MessageBox.Show(this, "Vui lòng chọn trạng thái mượn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    cboBorrowStatus.Focus();
            //    return false;
            //}
            //if (cboBorrowStatus.SelectedIndex == 0)
            //{
            //    MessageBox.Show(this, "Vui lòng chọn trạng thái mượn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    cboBorrowStatus.Focus();
            //    return false;
            //}
            if (!ERP.MasterData.DUI.Common.CommonFunction.EndDateValidation(dtFromDate.Value, dtToDate.Value, false))
            {
                dtToDate.Focus();
                MessageBox.Show(this, "Đến ngày không thể nhỏ hơn ngày mượn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (dtFromDate.Value.Date.AddMonths(12) < dtToDate.Value.Date)
            {
                MessageBox.Show(this, "Vui lòng xem trong vòng 12 tháng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtToDate.Focus();
                return false;
            }

            return true;
        }
        public bool LoadDataDM()
        {
            objKeywords = new object[] { "@Keywords", string.Empty,
                                         "@IsDeleted",0};
            DataTable dtbData = null;
            ERP.MasterData.PLC.WSBorrowProductType.ResultMessage objResultMessageType = objPLCBorrowProductType.SearchData(ref dtbData, objKeywords);
            if (objResultMessageType.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessageType.Message, objResultMessageType.MessageDetail);
                return false;
            }
            cboBorrowProductType.InitControl(true, dtbData, "BORROWPRODUCTTYPEID", "BORROWPRODUCTTYPENAME", "-- Tất cả --");
            return true;
        }
        private bool LoadData()
        {
            objKeywords = new object[] { "@Keywords", this.txtKeywords.Text.Trim(),
                                         "@IsDeleted", Convert.ToInt32(boolStatusBeforeReLoad), 
                                         "@UserName", SystemConfig.objSessionUser.UserName,
                                         "@BorrowStoreIDList", cboBorrowStoreIDList.StoreIDList,
                                         "@BorrowProductTypeIDList", cboBorrowProductType.ColumnIDList,
                                         "@BorrowStatus", cboBorrowStatus.SelectedIndex - 1,
                                         "@ReviewedStatus", cboReviewStatus.SelectedIndex - 2,
                                         "@Searchby", cboSearchBy.SelectedIndex,
                                         "@FromDate", dtFromDate.Value,
                                         "@ToDate", dtToDate.Value
                                        };
            DataTable dtbData = null;
            objResultMessage = objPLCBorrowProduct.SearchData(ref dtbData, objKeywords);
            if (objResultMessage.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return false;
            }
            grdData.DataSource = dtbData;
            mnuItemView.Enabled = grdViewData.RowCount > 0;
            mnuItemExportExcel.Enabled = grdViewData.RowCount > 0;
            return true;
        }

        public void RefreshView()
        {
            objResultMessage = objPLCBorrowProduct.SearchData(ref dtbSearch, string.Empty);
            if (objResultMessage.IsError)
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
            grdData.DataSource = dtbSearch;
            grdData.RefreshDataSource();
        }

        #endregion

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            mnuItemExportExcel.Enabled = mnuItemView.Enabled = grdViewData.RowCount > 0;
        }
    }
}
