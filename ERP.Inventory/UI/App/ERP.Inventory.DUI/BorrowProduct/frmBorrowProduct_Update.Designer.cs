﻿namespace ERP.Inventory.DUI.BorrowProduct
{
    partial class frmBorrowProduct_Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBorrowProduct_Update));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAction = new DevExpress.XtraEditors.DropDownButton();
            this.pMnu = new DevExpress.XtraBars.PopupMenu();
            this.btnTuChoi = new DevExpress.XtraBars.BarButtonItem();
            this.btnDongY = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.btnBorrow = new System.Windows.Forms.Button();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip();
            this.mnuItemDeleted = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblCreateUser = new System.Windows.Forms.Label();
            this.lblCaptionCreateUser = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtReturnPlanDate = new DevExpress.XtraEditors.DateEdit();
            this.cboBorrowStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.ctrlUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.dtBorrowDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBorrowNote = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.panDetail = new System.Windows.Forms.Panel();
            this.grbReviewDetail = new System.Windows.Forms.GroupBox();
            this.gridReViewLevel = new DevExpress.XtraGrid.GridControl();
            this.grvReViewLevel = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grbTransferDetail = new System.Windows.Forms.GroupBox();
            this.grdDelivery = new DevExpress.XtraGrid.GridControl();
            this.grdViewDelivery = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDeliveryTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliveryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliveryFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiveFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.grbDetailProduct = new System.Windows.Forms.GroupBox();
            this.grdProduct = new DevExpress.XtraGrid.GridControl();
            this.grdViewProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsNew = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repIsNew = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colMachineStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repMachineStatus = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAccessoryList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineStatusReturn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repComboMachineStatusReturn = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colProductNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repLookUpMachineStatus = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.btnSearchProduct = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pMnu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.mnuAction.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtReturnPlanDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReturnPlanDate.Properties)).BeginInit();
            this.panDetail.SuspendLayout();
            this.grbReviewDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridReViewLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvReViewLevel)).BeginInit();
            this.grbTransferDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            this.grbDetailProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMachineStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repComboMachineStatusReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLookUpMachineStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.btnAction);
            this.panel1.Controls.Add(this.btnUndo);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnReturn);
            this.panel1.Controls.Add(this.btnBorrow);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 532);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(944, 38);
            this.panel1.TabIndex = 8;
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAction.Appearance.Options.UseFont = true;
            this.btnAction.DropDownControl = this.pMnu;
            this.btnAction.Location = new System.Drawing.Point(508, 7);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(125, 25);
            this.btnAction.TabIndex = 15;
            this.btnAction.Text = "Chưa duyệt";
            this.btnAction.Visible = false;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // pMnu
            // 
            this.pMnu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTuChoi),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDongY)});
            this.pMnu.Manager = this.barManager1;
            this.pMnu.Name = "pMnu";
            // 
            // btnTuChoi
            // 
            this.btnTuChoi.Caption = "Từ chối";
            this.btnTuChoi.Glyph = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnTuChoi.Id = 0;
            this.btnTuChoi.Name = "btnTuChoi";
            this.btnTuChoi.Tag = 0;
            this.btnTuChoi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTuChoi_ItemClick);
            // 
            // btnDongY
            // 
            this.btnDongY.Caption = "Đồng ý";
            this.btnDongY.Glyph = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnDongY.Id = 1;
            this.btnDongY.Name = "btnDongY";
            this.btnDongY.Tag = 1;
            this.btnDongY.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTuChoi_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnTuChoi,
            this.btnDongY});
            this.barManager1.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(944, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 570);
            this.barDockControlBottom.Size = new System.Drawing.Size(944, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 570);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(944, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 570);
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.Image")));
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(841, 7);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(95, 25);
            this.btnUndo.TabIndex = 14;
            this.btnUndo.Text = "   Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(740, 7);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 13;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(639, 7);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(95, 25);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "Hủy";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReturn.Image = global::ERP.Inventory.DUI.Properties.Resources.mnu_export_16;
            this.btnReturn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReturn.Location = new System.Drawing.Point(117, 7);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(103, 25);
            this.btnReturn.TabIndex = 11;
            this.btnReturn.Text = "   Trả hàng";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Visible = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // btnBorrow
            // 
            this.btnBorrow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBorrow.Image = global::ERP.Inventory.DUI.Properties.Resources.mnu_import_16;
            this.btnBorrow.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBorrow.Location = new System.Drawing.Point(12, 7);
            this.btnBorrow.Name = "btnBorrow";
            this.btnBorrow.Size = new System.Drawing.Size(99, 25);
            this.btnBorrow.TabIndex = 10;
            this.btnBorrow.Text = "     Mượn hàng";
            this.btnBorrow.UseVisualStyleBackColor = true;
            this.btnBorrow.Visible = false;
            this.btnBorrow.Click += new System.EventHandler(this.btnBorrow_Click);
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemDeleted});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(95, 26);
            this.mnuAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAction_Opening);
            // 
            // mnuItemDeleted
            // 
            this.mnuItemDeleted.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDeleted.Image")));
            this.mnuItemDeleted.Name = "mnuItemDeleted";
            this.mnuItemDeleted.Size = new System.Drawing.Size(94, 22);
            this.mnuItemDeleted.Text = "Xóa";
            this.mnuItemDeleted.Click += new System.EventHandler(this.mnuItemDeleted_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblCreateUser);
            this.panel3.Controls.Add(this.lblCaptionCreateUser);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.dtReturnPlanDate);
            this.panel3.Controls.Add(this.cboBorrowStoreID);
            this.panel3.Controls.Add(this.ctrlUser);
            this.panel3.Controls.Add(this.dtBorrowDate);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.txtBorrowNote);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(944, 76);
            this.panel3.TabIndex = 0;
            // 
            // lblCreateUser
            // 
            this.lblCreateUser.AutoSize = true;
            this.lblCreateUser.Location = new System.Drawing.Point(718, 7);
            this.lblCreateUser.Name = "lblCreateUser";
            this.lblCreateUser.Size = new System.Drawing.Size(58, 16);
            this.lblCreateUser.TabIndex = 15;
            this.lblCreateUser.Text = "NV Tạo:";
            // 
            // lblCaptionCreateUser
            // 
            this.lblCaptionCreateUser.AutoSize = true;
            this.lblCaptionCreateUser.Location = new System.Drawing.Point(619, 7);
            this.lblCaptionCreateUser.Name = "lblCaptionCreateUser";
            this.lblCaptionCreateUser.Size = new System.Drawing.Size(93, 16);
            this.lblCaptionCreateUser.TabIndex = 14;
            this.lblCaptionCreateUser.Text = "Nhân viên tạo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "Ngày hẹn trả:";
            // 
            // dtReturnPlanDate
            // 
            this.dtReturnPlanDate.EditValue = null;
            this.dtReturnPlanDate.Location = new System.Drawing.Point(109, 51);
            this.dtReturnPlanDate.Name = "dtReturnPlanDate";
            this.dtReturnPlanDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.dtReturnPlanDate.Properties.Appearance.Options.UseFont = true;
            this.dtReturnPlanDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtReturnPlanDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.dtReturnPlanDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtReturnPlanDate.Properties.EditFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.dtReturnPlanDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtReturnPlanDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtReturnPlanDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtReturnPlanDate.Size = new System.Drawing.Size(126, 22);
            this.dtReturnPlanDate.TabIndex = 4;
            // 
            // cboBorrowStoreID
            // 
            this.cboBorrowStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBorrowStoreID.Location = new System.Drawing.Point(109, 3);
            this.cboBorrowStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboBorrowStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBorrowStoreID.Name = "cboBorrowStoreID";
            this.cboBorrowStoreID.Size = new System.Drawing.Size(270, 24);
            this.cboBorrowStoreID.TabIndex = 0;
            this.cboBorrowStoreID.SelectionChangeCommitted += new System.EventHandler(this.cboBorrowStoreID_SelectionChangeCommitted);
            // 
            // ctrlUser
            // 
            this.ctrlUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrlUser.IsOnlyShowRealStaff = false;
            this.ctrlUser.IsValidate = true;
            this.ctrlUser.Location = new System.Drawing.Point(109, 27);
            this.ctrlUser.Margin = new System.Windows.Forms.Padding(4);
            this.ctrlUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ctrlUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ctrlUser.Name = "ctrlUser";
            this.ctrlUser.Size = new System.Drawing.Size(270, 22);
            this.ctrlUser.TabIndex = 2;
            this.ctrlUser.UserName = "";
            this.ctrlUser.WorkingPositionID = 0;
            this.ctrlUser.Leave += new System.EventHandler(this.ctrlUser_Leave);
            // 
            // dtBorrowDate
            // 
            this.dtBorrowDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtBorrowDate.Enabled = false;
            this.dtBorrowDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtBorrowDate.Location = new System.Drawing.Point(470, 4);
            this.dtBorrowDate.Name = "dtBorrowDate";
            this.dtBorrowDate.Size = new System.Drawing.Size(143, 22);
            this.dtBorrowDate.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Nhân viên mượn:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(384, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ngày mượn:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "Kho mượn:";
            // 
            // txtBorrowNote
            // 
            this.txtBorrowNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBorrowNote.Location = new System.Drawing.Point(470, 27);
            this.txtBorrowNote.MaxLength = 450;
            this.txtBorrowNote.Multiline = true;
            this.txtBorrowNote.Name = "txtBorrowNote";
            this.txtBorrowNote.Size = new System.Drawing.Size(466, 42);
            this.txtBorrowNote.TabIndex = 3;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(384, 30);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 16);
            this.label22.TabIndex = 0;
            this.label22.Text = "Ghi chú:";
            // 
            // panDetail
            // 
            this.panDetail.Controls.Add(this.grbReviewDetail);
            this.panDetail.Controls.Add(this.grbTransferDetail);
            this.panDetail.Controls.Add(this.grbDetailProduct);
            this.panDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panDetail.Location = new System.Drawing.Point(0, 76);
            this.panDetail.Name = "panDetail";
            this.panDetail.Size = new System.Drawing.Size(944, 456);
            this.panDetail.TabIndex = 1;
            this.panDetail.TabStop = true;
            // 
            // grbReviewDetail
            // 
            this.grbReviewDetail.BackColor = System.Drawing.SystemColors.Control;
            this.grbReviewDetail.Controls.Add(this.gridReViewLevel);
            this.grbReviewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbReviewDetail.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.grbReviewDetail.Location = new System.Drawing.Point(0, 0);
            this.grbReviewDetail.Name = "grbReviewDetail";
            this.grbReviewDetail.Size = new System.Drawing.Size(944, 153);
            this.grbReviewDetail.TabIndex = 5;
            this.grbReviewDetail.TabStop = false;
            this.grbReviewDetail.Text = "Mức duyệt";
            // 
            // gridReViewLevel
            // 
            this.gridReViewLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridReViewLevel.Location = new System.Drawing.Point(3, 18);
            this.gridReViewLevel.MainView = this.grvReViewLevel;
            this.gridReViewLevel.Name = "gridReViewLevel";
            this.gridReViewLevel.Size = new System.Drawing.Size(938, 132);
            this.gridReViewLevel.TabIndex = 11;
            this.gridReViewLevel.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvReViewLevel});
            // 
            // grvReViewLevel
            // 
            this.grvReViewLevel.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvReViewLevel.Appearance.FocusedRow.Options.UseFont = true;
            this.grvReViewLevel.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvReViewLevel.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvReViewLevel.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvReViewLevel.Appearance.Preview.Options.UseFont = true;
            this.grvReViewLevel.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvReViewLevel.Appearance.Row.Options.UseFont = true;
            this.grvReViewLevel.ColumnPanelRowHeight = 20;
            this.grvReViewLevel.GridControl = this.gridReViewLevel;
            this.grvReViewLevel.Name = "grvReViewLevel";
            this.grvReViewLevel.OptionsBehavior.KeepGroupExpandedOnSorting = false;
            this.grvReViewLevel.OptionsCustomization.AllowFilter = false;
            this.grvReViewLevel.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grvReViewLevel.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvReViewLevel.OptionsMenu.EnableColumnMenu = false;
            this.grvReViewLevel.OptionsMenu.EnableFooterMenu = false;
            this.grvReViewLevel.OptionsMenu.EnableGroupPanelMenu = false;
            this.grvReViewLevel.OptionsNavigation.UseTabKey = false;
            this.grvReViewLevel.OptionsView.AllowCellMerge = true;
            this.grvReViewLevel.OptionsView.ColumnAutoWidth = false;
            this.grvReViewLevel.OptionsView.ShowAutoFilterRow = true;
            this.grvReViewLevel.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvReViewLevel.OptionsView.ShowGroupPanel = false;
            this.grvReViewLevel.CellMerge += new DevExpress.XtraGrid.Views.Grid.CellMergeEventHandler(this.grvReViewLevel_CellMerge);
            this.grvReViewLevel.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvReViewLevel_ShowingEditor);
            // 
            // grbTransferDetail
            // 
            this.grbTransferDetail.BackColor = System.Drawing.SystemColors.Control;
            this.grbTransferDetail.Controls.Add(this.grdDelivery);
            this.grbTransferDetail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grbTransferDetail.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.grbTransferDetail.Location = new System.Drawing.Point(0, 153);
            this.grbTransferDetail.Name = "grbTransferDetail";
            this.grbTransferDetail.Size = new System.Drawing.Size(944, 98);
            this.grbTransferDetail.TabIndex = 3;
            this.grbTransferDetail.TabStop = false;
            this.grbTransferDetail.Text = "Chi tiết giao nhận";
            // 
            // grdDelivery
            // 
            this.grdDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDelivery.Location = new System.Drawing.Point(3, 18);
            this.grdDelivery.MainView = this.grdViewDelivery;
            this.grdDelivery.Name = "grdDelivery";
            this.grdDelivery.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit3});
            this.grdDelivery.Size = new System.Drawing.Size(938, 77);
            this.grdDelivery.TabIndex = 11;
            this.grdDelivery.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewDelivery});
            // 
            // grdViewDelivery
            // 
            this.grdViewDelivery.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDelivery.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewDelivery.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewDelivery.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewDelivery.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDelivery.Appearance.Preview.Options.UseFont = true;
            this.grdViewDelivery.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDelivery.Appearance.Row.Options.UseFont = true;
            this.grdViewDelivery.ColumnPanelRowHeight = 20;
            this.grdViewDelivery.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDeliveryTypeName,
            this.colDeliveryDate,
            this.colDeliveryFullName,
            this.colReceiveFullName,
            this.colNote});
            this.grdViewDelivery.GridControl = this.grdDelivery;
            this.grdViewDelivery.Name = "grdViewDelivery";
            this.grdViewDelivery.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewDelivery.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewDelivery.OptionsNavigation.UseTabKey = false;
            this.grdViewDelivery.OptionsView.ColumnAutoWidth = false;
            this.grdViewDelivery.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewDelivery.OptionsView.ShowGroupPanel = false;
            // 
            // colDeliveryTypeName
            // 
            this.colDeliveryTypeName.AppearanceCell.Options.UseTextOptions = true;
            this.colDeliveryTypeName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDeliveryTypeName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDeliveryTypeName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeliveryTypeName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDeliveryTypeName.Caption = "Loại";
            this.colDeliveryTypeName.FieldName = "DELIVERYTYPENAME";
            this.colDeliveryTypeName.Name = "colDeliveryTypeName";
            this.colDeliveryTypeName.OptionsColumn.AllowEdit = false;
            this.colDeliveryTypeName.OptionsColumn.ReadOnly = true;
            this.colDeliveryTypeName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDeliveryTypeName.Visible = true;
            this.colDeliveryTypeName.VisibleIndex = 0;
            this.colDeliveryTypeName.Width = 90;
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.AppearanceCell.Options.UseTextOptions = true;
            this.colDeliveryDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDeliveryDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDeliveryDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeliveryDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDeliveryDate.Caption = "Ngày giao nhận";
            this.colDeliveryDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colDeliveryDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colDeliveryDate.FieldName = "DELIVERYDATE";
            this.colDeliveryDate.Name = "colDeliveryDate";
            this.colDeliveryDate.OptionsColumn.AllowEdit = false;
            this.colDeliveryDate.OptionsColumn.ReadOnly = true;
            this.colDeliveryDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDeliveryDate.Visible = true;
            this.colDeliveryDate.VisibleIndex = 1;
            this.colDeliveryDate.Width = 130;
            // 
            // colDeliveryFullName
            // 
            this.colDeliveryFullName.AppearanceCell.Options.UseTextOptions = true;
            this.colDeliveryFullName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDeliveryFullName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDeliveryFullName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeliveryFullName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDeliveryFullName.Caption = "Nhân viên giao";
            this.colDeliveryFullName.FieldName = "DELIVERYSER";
            this.colDeliveryFullName.Name = "colDeliveryFullName";
            this.colDeliveryFullName.OptionsColumn.AllowEdit = false;
            this.colDeliveryFullName.OptionsColumn.ReadOnly = true;
            this.colDeliveryFullName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDeliveryFullName.Visible = true;
            this.colDeliveryFullName.VisibleIndex = 2;
            this.colDeliveryFullName.Width = 200;
            // 
            // colReceiveFullName
            // 
            this.colReceiveFullName.AppearanceCell.Options.UseTextOptions = true;
            this.colReceiveFullName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colReceiveFullName.AppearanceHeader.Options.UseTextOptions = true;
            this.colReceiveFullName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReceiveFullName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colReceiveFullName.Caption = "Nhân viên nhận";
            this.colReceiveFullName.FieldName = "RECIEVEUSER";
            this.colReceiveFullName.Name = "colReceiveFullName";
            this.colReceiveFullName.OptionsColumn.AllowEdit = false;
            this.colReceiveFullName.OptionsColumn.ReadOnly = true;
            this.colReceiveFullName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colReceiveFullName.Visible = true;
            this.colReceiveFullName.VisibleIndex = 3;
            this.colReceiveFullName.Width = 200;
            // 
            // colNote
            // 
            this.colNote.AppearanceCell.Options.UseTextOptions = true;
            this.colNote.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNote.AppearanceHeader.Options.UseTextOptions = true;
            this.colNote.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "NOTE";
            this.colNote.Name = "colNote";
            this.colNote.OptionsColumn.AllowEdit = false;
            this.colNote.OptionsColumn.ReadOnly = true;
            this.colNote.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 4;
            this.colNote.Width = 260;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit2.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit1.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit3.ValueUnchecked = ((short)(0));
            // 
            // grbDetailProduct
            // 
            this.grbDetailProduct.Controls.Add(this.grdProduct);
            this.grbDetailProduct.Controls.Add(this.txtBarcode);
            this.grbDetailProduct.Controls.Add(this.btnSearchProduct);
            this.grbDetailProduct.Controls.Add(this.label15);
            this.grbDetailProduct.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grbDetailProduct.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.grbDetailProduct.Location = new System.Drawing.Point(0, 251);
            this.grbDetailProduct.Name = "grbDetailProduct";
            this.grbDetailProduct.Size = new System.Drawing.Size(944, 205);
            this.grbDetailProduct.TabIndex = 4;
            this.grbDetailProduct.TabStop = false;
            this.grbDetailProduct.Text = "Chi tiết sản phẩm";
            // 
            // grdProduct
            // 
            this.grdProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProduct.ContextMenuStrip = this.mnuAction;
            this.grdProduct.Location = new System.Drawing.Point(3, 48);
            this.grdProduct.MainView = this.grdViewProduct;
            this.grdProduct.Name = "grdProduct";
            this.grdProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repIsNew,
            this.repNote,
            this.repQuantity,
            this.repositoryItemTextEdit1,
            this.repMachineStatus,
            this.repLookUpMachineStatus,
            this.repComboMachineStatusReturn});
            this.grdProduct.Size = new System.Drawing.Size(938, 151);
            this.grdProduct.TabIndex = 2;
            this.grdProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProduct});
            this.grdProduct.DoubleClick += new System.EventHandler(this.grdProduct_DoubleClick);
            // 
            // grdViewProduct
            // 
            this.grdViewProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProduct.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewProduct.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProduct.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProduct.Appearance.Preview.Options.UseFont = true;
            this.grdViewProduct.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProduct.Appearance.Row.Options.UseFont = true;
            this.grdViewProduct.ColumnPanelRowHeight = 20;
            this.grdViewProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colProductID,
            this.colProductName,
            this.colIMEI,
            this.colIsNew,
            this.colQuantity,
            this.colMachineStatus,
            this.colAccessoryList,
            this.colMachineStatusReturn,
            this.colProductNote,
            this.gridColumn1,
            this.colStatus});
            this.grdViewProduct.GridControl = this.grdProduct;
            this.grdViewProduct.Name = "grdViewProduct";
            this.grdViewProduct.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewProduct.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProduct.OptionsNavigation.UseTabKey = false;
            this.grdViewProduct.OptionsView.ColumnAutoWidth = false;
            this.grdViewProduct.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProduct.OptionsView.ShowFooter = true;
            this.grdViewProduct.OptionsView.ShowGroupPanel = false;
            this.grdViewProduct.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grdViewProduct_RowCellClick);
            this.grdViewProduct.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grdViewProduct_RowStyle);
            this.grdViewProduct.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewProduct_ShowingEditor);
            this.grdViewProduct.HiddenEditor += new System.EventHandler(this.grdViewProduct_HiddenEditor);
            this.grdViewProduct.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdViewProduct_CellValueChanged);
            this.grdViewProduct.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.grdViewProduct_CustomColumnDisplayText);
            this.grdViewProduct.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.grdViewProduct_ValidatingEditor);
            // 
            // colProductID
            // 
            this.colProductID.AppearanceCell.Options.UseTextOptions = true;
            this.colProductID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colProductID.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductID.Caption = "Mã sản phẩm";
            this.colProductID.FieldName = "PRODUCTID";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.AllowMove = false;
            this.colProductID.OptionsColumn.ReadOnly = true;
            this.colProductID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colProductID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "PROVIDERPRODUCTCODETYPEID", "Tổng số lượng : {0}")});
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 0;
            this.colProductID.Width = 100;
            // 
            // colProductName
            // 
            this.colProductName.AppearanceCell.Options.UseTextOptions = true;
            this.colProductName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colProductName.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductName.Caption = "Tên sản phẩm";
            this.colProductName.FieldName = "PRODUCTNAME";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colProductName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0}")});
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 1;
            this.colProductName.Width = 250;
            // 
            // colIMEI
            // 
            this.colIMEI.AppearanceCell.Options.UseTextOptions = true;
            this.colIMEI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colIMEI.AppearanceHeader.Options.UseTextOptions = true;
            this.colIMEI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIMEI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.ColumnEdit = this.repositoryItemTextEdit1;
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 2;
            this.colIMEI.Width = 160;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 20;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colIsNew
            // 
            this.colIsNew.AppearanceCell.Options.UseTextOptions = true;
            this.colIsNew.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colIsNew.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsNew.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsNew.Caption = "Mới";
            this.colIsNew.ColumnEdit = this.repIsNew;
            this.colIsNew.FieldName = "ISNEW";
            this.colIsNew.Name = "colIsNew";
            this.colIsNew.OptionsColumn.AllowEdit = false;
            this.colIsNew.OptionsColumn.ReadOnly = true;
            this.colIsNew.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIsNew.Visible = true;
            this.colIsNew.VisibleIndex = 3;
            this.colIsNew.Width = 51;
            // 
            // repIsNew
            // 
            this.repIsNew.AutoHeight = false;
            this.repIsNew.Name = "repIsNew";
            this.repIsNew.ValueChecked = ((short)(1));
            this.repIsNew.ValueUnchecked = ((short)(0));
            // 
            // colQuantity
            // 
            this.colQuantity.AppearanceCell.Options.UseTextOptions = true;
            this.colQuantity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantity.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantity.Caption = "SL";
            this.colQuantity.ColumnEdit = this.repQuantity;
            this.colQuantity.FieldName = "QUANTITY";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 4;
            this.colQuantity.Width = 50;
            // 
            // repQuantity
            // 
            this.repQuantity.AutoHeight = false;
            this.repQuantity.DisplayFormat.FormatString = "#,##0";
            this.repQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repQuantity.Mask.EditMask = "N00";
            this.repQuantity.MaxLength = 4;
            this.repQuantity.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.repQuantity.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repQuantity.Name = "repQuantity";
            this.repQuantity.NullText = "1";
            // 
            // colMachineStatus
            // 
            this.colMachineStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colMachineStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMachineStatus.Caption = "Tình trạng thiết bị khi mượn";
            this.colMachineStatus.ColumnEdit = this.repMachineStatus;
            this.colMachineStatus.FieldName = "MACHINESTATUS";
            this.colMachineStatus.Name = "colMachineStatus";
            this.colMachineStatus.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colMachineStatus.Visible = true;
            this.colMachineStatus.VisibleIndex = 5;
            this.colMachineStatus.Width = 200;
            // 
            // repMachineStatus
            // 
            this.repMachineStatus.AutoHeight = false;
            this.repMachineStatus.MaxLength = 400;
            this.repMachineStatus.Name = "repMachineStatus";
            // 
            // colAccessoryList
            // 
            this.colAccessoryList.AppearanceHeader.Options.UseTextOptions = true;
            this.colAccessoryList.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAccessoryList.Caption = "Phụ kiện mượn";
            this.colAccessoryList.FieldName = "ACCESSORYLIST";
            this.colAccessoryList.Name = "colAccessoryList";
            this.colAccessoryList.OptionsColumn.AllowEdit = false;
            this.colAccessoryList.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colAccessoryList.Visible = true;
            this.colAccessoryList.VisibleIndex = 6;
            this.colAccessoryList.Width = 180;
            // 
            // colMachineStatusReturn
            // 
            this.colMachineStatusReturn.Caption = "Tình trạng thiết bị khi trả";
            this.colMachineStatusReturn.ColumnEdit = this.repComboMachineStatusReturn;
            this.colMachineStatusReturn.FieldName = "MACHINESTATUSRETURN";
            this.colMachineStatusReturn.Name = "colMachineStatusReturn";
            this.colMachineStatusReturn.Visible = true;
            this.colMachineStatusReturn.VisibleIndex = 7;
            this.colMachineStatusReturn.Width = 200;
            // 
            // repComboMachineStatusReturn
            // 
            this.repComboMachineStatusReturn.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repComboMachineStatusReturn.Appearance.Options.UseFont = true;
            this.repComboMachineStatusReturn.AutoHeight = false;
            this.repComboMachineStatusReturn.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repComboMachineStatusReturn.MaxLength = 400;
            this.repComboMachineStatusReturn.Name = "repComboMachineStatusReturn";
            this.repComboMachineStatusReturn.NullText = "--Tình trạng--";
            // 
            // colProductNote
            // 
            this.colProductNote.AppearanceCell.Options.UseTextOptions = true;
            this.colProductNote.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colProductNote.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductNote.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductNote.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductNote.Caption = "Ghi chú";
            this.colProductNote.ColumnEdit = this.repNote;
            this.colProductNote.FieldName = "NOTE";
            this.colProductNote.Name = "colProductNote";
            this.colProductNote.Visible = true;
            this.colProductNote.VisibleIndex = 8;
            this.colProductNote.Width = 250;
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 499;
            this.repNote.Name = "repNote";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "colIsRequestIMEI";
            this.gridColumn1.FieldName = "ISREQUESTIMEI";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // colStatus
            // 
            this.colStatus.AppearanceCell.Options.UseTextOptions = true;
            this.colStatus.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStatus.Caption = "Ghi chú lỗi";
            this.colStatus.FieldName = "STATUS";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.Width = 300;
            // 
            // repLookUpMachineStatus
            // 
            this.repLookUpMachineStatus.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repLookUpMachineStatus.Appearance.Options.UseFont = true;
            this.repLookUpMachineStatus.AutoHeight = false;
            this.repLookUpMachineStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLookUpMachineStatus.MaxLength = 400;
            this.repLookUpMachineStatus.Name = "repLookUpMachineStatus";
            this.repLookUpMachineStatus.NullText = "--Tình trạng--";
            this.repLookUpMachineStatus.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(74, 20);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(196, 22);
            this.txtBarcode.TabIndex = 0;
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // btnSearchProduct
            // 
            this.btnSearchProduct.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchProduct.Image")));
            this.btnSearchProduct.Location = new System.Drawing.Point(276, 19);
            this.btnSearchProduct.Name = "btnSearchProduct";
            this.btnSearchProduct.Size = new System.Drawing.Size(29, 24);
            this.btnSearchProduct.TabIndex = 1;
            this.btnSearchProduct.Click += new System.EventHandler(this.btnSearchProduct_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label15.Location = new System.Drawing.Point(11, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 16);
            this.label15.TabIndex = 39;
            this.label15.Text = "Barcode:";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmBorrowProduct_Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 570);
            this.Controls.Add(this.panDetail);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(960, 560);
            this.Name = "frmBorrowProduct_Update";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cập nhật chứng từ mượn hàng hóa";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBorrowProduct_Update_FormClosing);
            this.Load += new System.EventHandler(this.frmBorrowProduct_Update_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pMnu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.mnuAction.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtReturnPlanDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReturnPlanDate.Properties)).EndInit();
            this.panDetail.ResumeLayout(false);
            this.grbReviewDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridReViewLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvReViewLevel)).EndInit();
            this.grbTransferDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            this.grbDetailProduct.ResumeLayout(false);
            this.grbDetailProduct.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMachineStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repComboMachineStatusReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLookUpMachineStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Button btnBorrow;
        private System.Windows.Forms.Panel panel3;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ctrlUser;
        private System.Windows.Forms.DateTimePicker dtBorrowDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBorrowNote;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDeleted;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboBorrowStoreID;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.DateEdit dtReturnPlanDate;
        private System.Windows.Forms.Panel panDetail;
        private System.Windows.Forms.GroupBox grbDetailProduct;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Button btnSearchProduct;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraGrid.GridControl grdProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewProduct;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colIsNew;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repIsNew;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colProductNote;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private System.Windows.Forms.GroupBox grbTransferDetail;
        private DevExpress.XtraGrid.GridControl grdDelivery;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewDelivery;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliveryTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliveryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliveryFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiveFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private System.Windows.Forms.GroupBox grbReviewDetail;
        private DevExpress.XtraGrid.GridControl gridReViewLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView grvReViewLevel;
        private DevExpress.XtraEditors.DropDownButton btnAction;
        private DevExpress.XtraBars.PopupMenu pMnu;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnTuChoi;
        private DevExpress.XtraBars.BarButtonItem btnDongY;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblCreateUser;
        private System.Windows.Forms.Label lblCaptionCreateUser;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessoryList;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repMachineStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineStatusReturn;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLookUpMachineStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repComboMachineStatusReturn;
    }
}