﻿namespace ERP.Inventory.DUI.CM
{
    partial class frmExchangeRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtTotalVND = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalUSD = new DevExpress.XtraEditors.TextEdit();
            this.txtExchangeRate125 = new DevExpress.XtraEditors.TextEdit();
            this.txtTotal125 = new DevExpress.XtraEditors.TextEdit();
            this.txtExchangeRate1020 = new DevExpress.XtraEditors.TextEdit();
            this.txtTotal1020 = new DevExpress.XtraEditors.TextEdit();
            this.txtExchangeRate50100 = new DevExpress.XtraEditors.TextEdit();
            this.txtTotal50100 = new DevExpress.XtraEditors.TextEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.lblTotalVND = new System.Windows.Forms.Label();
            this.lblTotalUSD = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboCurrencyUnit = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalVND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalUSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate125.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal125.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate1020.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal1020.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate50100.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal50100.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.Controls.Add(this.txtTotalVND);
            this.groupControl1.Controls.Add(this.txtTotalUSD);
            this.groupControl1.Controls.Add(this.txtExchangeRate125);
            this.groupControl1.Controls.Add(this.txtTotal125);
            this.groupControl1.Controls.Add(this.txtExchangeRate1020);
            this.groupControl1.Controls.Add(this.txtTotal1020);
            this.groupControl1.Controls.Add(this.txtExchangeRate50100);
            this.groupControl1.Controls.Add(this.txtTotal50100);
            this.groupControl1.Controls.Add(this.btnCancel);
            this.groupControl1.Controls.Add(this.btnOK);
            this.groupControl1.Controls.Add(this.lblTotalVND);
            this.groupControl1.Controls.Add(this.lblTotalUSD);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.cboCurrencyUnit);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(484, 209);
            this.groupControl1.TabIndex = 39;
            this.groupControl1.Text = "TÍNH TỈ GIÁ";
            // 
            // txtTotalVND
            // 
            this.txtTotalVND.Location = new System.Drawing.Point(332, 141);
            this.txtTotalVND.Name = "txtTotalVND";
            this.txtTotalVND.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalVND.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalVND.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalVND.Properties.Appearance.Options.UseFont = true;
            this.txtTotalVND.Properties.DisplayFormat.FormatString = "###,###,###,##0";
            this.txtTotalVND.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalVND.Properties.Mask.EditMask = "###,###,###,##0";
            this.txtTotalVND.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalVND.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalVND.Properties.NullText = "0";
            this.txtTotalVND.Properties.ReadOnly = true;
            this.txtTotalVND.Size = new System.Drawing.Size(136, 22);
            this.txtTotalVND.TabIndex = 52;
            // 
            // txtTotalUSD
            // 
            this.txtTotalUSD.Location = new System.Drawing.Point(103, 141);
            this.txtTotalUSD.Name = "txtTotalUSD";
            this.txtTotalUSD.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalUSD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalUSD.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalUSD.Properties.Appearance.Options.UseFont = true;
            this.txtTotalUSD.Properties.DisplayFormat.FormatString = "###,###,###,##0";
            this.txtTotalUSD.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalUSD.Properties.Mask.EditMask = "###,###,###,##0";
            this.txtTotalUSD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalUSD.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalUSD.Properties.NullText = "0";
            this.txtTotalUSD.Properties.NullValuePrompt = "0";
            this.txtTotalUSD.Properties.ReadOnly = true;
            this.txtTotalUSD.Size = new System.Drawing.Size(136, 22);
            this.txtTotalUSD.TabIndex = 52;
            // 
            // txtExchangeRate125
            // 
            this.txtExchangeRate125.Location = new System.Drawing.Point(332, 115);
            this.txtExchangeRate125.Name = "txtExchangeRate125";
            this.txtExchangeRate125.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtExchangeRate125.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeRate125.Properties.Appearance.Options.UseBackColor = true;
            this.txtExchangeRate125.Properties.Appearance.Options.UseFont = true;
            this.txtExchangeRate125.Properties.DisplayFormat.FormatString = "###,###,###,##0";
            this.txtExchangeRate125.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExchangeRate125.Properties.Mask.EditMask = "###,###,###,##0";
            this.txtExchangeRate125.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtExchangeRate125.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtExchangeRate125.Properties.NullText = "0";
            this.txtExchangeRate125.Properties.ReadOnly = true;
            this.txtExchangeRate125.Size = new System.Drawing.Size(136, 22);
            this.txtExchangeRate125.TabIndex = 52;
            // 
            // txtTotal125
            // 
            this.txtTotal125.Location = new System.Drawing.Point(103, 115);
            this.txtTotal125.Name = "txtTotal125";
            this.txtTotal125.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal125.Properties.Appearance.Options.UseFont = true;
            this.txtTotal125.Properties.DisplayFormat.FormatString = "###,###,###,###";
            this.txtTotal125.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotal125.Properties.Mask.EditMask = "###,###,###,##0";
            this.txtTotal125.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotal125.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotal125.Properties.NullText = "0";
            this.txtTotal125.Properties.NullValuePrompt = "0";
            this.txtTotal125.Size = new System.Drawing.Size(136, 22);
            this.txtTotal125.TabIndex = 52;
            this.txtTotal125.EditValueChanged += new System.EventHandler(this.txtTotal50100_EditValueChanged);
            // 
            // txtExchangeRate1020
            // 
            this.txtExchangeRate1020.Location = new System.Drawing.Point(332, 89);
            this.txtExchangeRate1020.Name = "txtExchangeRate1020";
            this.txtExchangeRate1020.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtExchangeRate1020.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeRate1020.Properties.Appearance.Options.UseBackColor = true;
            this.txtExchangeRate1020.Properties.Appearance.Options.UseFont = true;
            this.txtExchangeRate1020.Properties.DisplayFormat.FormatString = "###,###,###,##0";
            this.txtExchangeRate1020.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExchangeRate1020.Properties.Mask.EditMask = "###,###,###,##0";
            this.txtExchangeRate1020.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtExchangeRate1020.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtExchangeRate1020.Properties.NullText = "0";
            this.txtExchangeRate1020.Properties.ReadOnly = true;
            this.txtExchangeRate1020.Size = new System.Drawing.Size(136, 22);
            this.txtExchangeRate1020.TabIndex = 52;
            // 
            // txtTotal1020
            // 
            this.txtTotal1020.Location = new System.Drawing.Point(103, 89);
            this.txtTotal1020.Name = "txtTotal1020";
            this.txtTotal1020.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal1020.Properties.Appearance.Options.UseFont = true;
            this.txtTotal1020.Properties.DisplayFormat.FormatString = "###,###,###,###";
            this.txtTotal1020.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotal1020.Properties.Mask.EditMask = "###,###,###,##0";
            this.txtTotal1020.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotal1020.Properties.NullText = "0";
            this.txtTotal1020.Properties.NullValuePrompt = "0";
            this.txtTotal1020.Size = new System.Drawing.Size(136, 22);
            this.txtTotal1020.TabIndex = 52;
            this.txtTotal1020.EditValueChanged += new System.EventHandler(this.txtTotal50100_EditValueChanged);
            // 
            // txtExchangeRate50100
            // 
            this.txtExchangeRate50100.Location = new System.Drawing.Point(332, 63);
            this.txtExchangeRate50100.Name = "txtExchangeRate50100";
            this.txtExchangeRate50100.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtExchangeRate50100.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeRate50100.Properties.Appearance.Options.UseBackColor = true;
            this.txtExchangeRate50100.Properties.Appearance.Options.UseFont = true;
            this.txtExchangeRate50100.Properties.DisplayFormat.FormatString = "###,###,###,##0";
            this.txtExchangeRate50100.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExchangeRate50100.Properties.Mask.EditMask = "###,###,###,##0";
            this.txtExchangeRate50100.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtExchangeRate50100.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtExchangeRate50100.Properties.NullText = "0";
            this.txtExchangeRate50100.Properties.ReadOnly = true;
            this.txtExchangeRate50100.Size = new System.Drawing.Size(136, 22);
            this.txtExchangeRate50100.TabIndex = 52;
            // 
            // txtTotal50100
            // 
            this.txtTotal50100.Location = new System.Drawing.Point(103, 63);
            this.txtTotal50100.Name = "txtTotal50100";
            this.txtTotal50100.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal50100.Properties.Appearance.Options.UseFont = true;
            this.txtTotal50100.Properties.DisplayFormat.FormatString = "###,###,###,###";
            this.txtTotal50100.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotal50100.Properties.Mask.EditMask = "###,###,###,##0";
            this.txtTotal50100.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotal50100.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotal50100.Properties.NullText = "0";
            this.txtTotal50100.Size = new System.Drawing.Size(136, 22);
            this.txtTotal50100.TabIndex = 52;
            this.txtTotal50100.EditValueChanged += new System.EventHandler(this.txtTotal50100_EditValueChanged);
            this.txtTotal50100.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotal50100_KeyPress);
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Image = global::ERP.Inventory.DUI.Properties.Resources.close;
            this.btnCancel.Location = new System.Drawing.Point(373, 177);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(95, 23);
            this.btnCancel.TabIndex = 51;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Appearance.Options.UseFont = true;
            this.btnOK.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnOK.Location = new System.Drawing.Point(272, 177);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(95, 23);
            this.btnOK.TabIndex = 51;
            this.btnOK.Text = "Đồng ý";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblTotalVND
            // 
            this.lblTotalVND.AutoSize = true;
            this.lblTotalVND.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVND.Location = new System.Drawing.Point(257, 144);
            this.lblTotalVND.Name = "lblTotalVND";
            this.lblTotalVND.Size = new System.Drawing.Size(74, 16);
            this.lblTotalVND.TabIndex = 50;
            this.lblTotalVND.Text = "Tổng VNĐ:";
            // 
            // lblTotalUSD
            // 
            this.lblTotalUSD.AutoSize = true;
            this.lblTotalUSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalUSD.Location = new System.Drawing.Point(15, 144);
            this.lblTotalUSD.Name = "lblTotalUSD";
            this.lblTotalUSD.Size = new System.Drawing.Size(75, 16);
            this.lblTotalUSD.TabIndex = 49;
            this.lblTotalUSD.Text = "Tổng USD:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(257, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 16);
            this.label5.TabIndex = 48;
            this.label5.Text = "Tỉ giá:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 16);
            this.label6.TabIndex = 47;
            this.label6.Text = "Tiền(1, 2, 5):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(257, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 16);
            this.label3.TabIndex = 45;
            this.label3.Text = "Tỉ giá:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 43;
            this.label4.Text = "Tiền(10, 20):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(257, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 16);
            this.label2.TabIndex = 42;
            this.label2.Text = "Tỉ giá:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 41;
            this.label1.Text = "Tiền(50, 100):";
            // 
            // cboCurrencyUnit
            // 
            this.cboCurrencyUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCurrencyUnit.Location = new System.Drawing.Point(103, 34);
            this.cboCurrencyUnit.Name = "cboCurrencyUnit";
            this.cboCurrencyUnit.Size = new System.Drawing.Size(136, 24);
            this.cboCurrencyUnit.TabIndex = 39;
            this.cboCurrencyUnit.SelectionChangeCommitted += new System.EventHandler(this.cboCurrencyUnit_SelectionChangeCommitted);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 16);
            this.label9.TabIndex = 40;
            this.label9.Text = "Loại tiền tệ:";
            // 
            // frmExchangeRate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 209);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExchangeRate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tính tỉ giá";
            this.Load += new System.EventHandler(this.frmExchangeRate_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmExchangeRate_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalVND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalUSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate125.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal125.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate1020.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal1020.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate50100.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal50100.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtTotalVND;
        private DevExpress.XtraEditors.TextEdit txtTotalUSD;
        private DevExpress.XtraEditors.TextEdit txtExchangeRate125;
        private DevExpress.XtraEditors.TextEdit txtTotal125;
        private DevExpress.XtraEditors.TextEdit txtExchangeRate1020;
        private DevExpress.XtraEditors.TextEdit txtTotal1020;
        private DevExpress.XtraEditors.TextEdit txtExchangeRate50100;
        private DevExpress.XtraEditors.TextEdit txtTotal50100;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private System.Windows.Forms.Label lblTotalVND;
        private System.Windows.Forms.Label lblTotalUSD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboCurrencyUnit;
        private System.Windows.Forms.Label label9;
    }
}