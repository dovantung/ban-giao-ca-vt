﻿using GemBox.Spreadsheet;
using Library.AppCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Accessory
{
    public partial class DUISameLevelAccessoryDividing : Form
    {
        private PLC.Accessory.PLCSameLevelAccessoryDividing plcObject = new PLC.Accessory.PLCSameLevelAccessoryDividing();
        private PLC.Accessory.WSSameLevelAccessoryDividing.ResultMessage objResultMessage = null;

        private SQL_COMMON sql_common;

        private DataTable dtbResourceRotateList = null;
        private DataTable dtbResourceAccessoryGroup = null;
        private DataTable dtbResourceAccessory = null;
        private DataTable dtbResourceAccessoryAll = null;
        private DataTable dtbResourceShops = null;

        private DataTable dtbResourceRedundance = null;
        private DataTable dtbResourceLack = null;
        private DataTable dtbResourceAccessoryList = null;
        private DataTable dtbResourceShopList = null;
        private DataTable dtbResourceResult = null;

        private DataTable dtbResourceRedundanceBackUp = null;
        private DataTable dtbResourceLackBackUp = null;
        private DataTable dtbResourceAccessoryListBackUp = null;
        private DataTable dtbResourceShopListBackUp = null;
        private DataTable dtbResourceResultBackUp = null;
        private bool bolIsLoadComplete = false;
        public DUISameLevelAccessoryDividing()
        {
            try
            {
                InitializeComponent();
                DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
                sql_common = new SQL_COMMON();

                loadDataAndMapActionInForm();
                cboPage.SelectedValue = "50";

                if (dtbResourceRotateList.Rows.Count > 0)
                {
                    grdRotateList.SelectRow(0);
                    tblRotateList_Click(null, null);
                }

                plcObject.SearchRotatingHistory(ref dtbResourceRedundance, ref dtbResourceLack,
                        ref dtbResourceAccessoryList, ref dtbResourceShopList, ref dtbResourceResult,
                        "", "50");
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }
                tblRedundanceShop.DataSource = dtbResourceRedundance;
                tblLackShop.DataSource = dtbResourceLack;
                tblList.DataSource = dtbResourceResult;

                tblAccessoryList.DataSource = dtbResourceAccessoryList;
                //pnlShopList.setDataList(response.getVector("Out.shop_list"));
                tblShopList.DataSource = dtbResourceShopList;

                btnCancelRotate.Visible = false;
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                   e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
            }

        }

        private void loadDataAndMapActionInForm()
        {

            dtbResourceRotateList = plcObject.GetAccessRotate();
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            tblRotateList.DataSource = dtbResourceRotateList;

            dtbResourceShops = SearchCommon(sql_common.SQL_SHOP, null);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            lovShop.InitControl(false, dtbResourceShops, "STOCK_ID", "NAME", "");

            dtbResourceAccessoryGroup = SearchCommon(sql_common.SQL_GOODS_GROUP, null);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }


            cboAccessoryGroup.InitControl(false, dtbResourceAccessoryGroup, "goods_group_id", "NAME", "");

            cboAccessoryGroup.SelectionChangeCommitted += new System.EventHandler(cboAccessoryGroup_SelectionChangeCommitted);
        }

        private void cboAccessoryGroup_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            if (false)
            {
                dtbResourceAccessory = null;
            }
            else
            {
               
                Dictionary<string, string> map = new Dictionary<string, string>() {
                                                {":goods_group_id", cboAccessoryGroup.ColumnID},
                                              };
                dtbResourceAccessory = SearchCommon(sql_common.SQL_GOODS, map);
                if (String.IsNullOrEmpty(cboAccessoryGroup.ColumnID))
                {
                    dtbResourceAccessoryAll = dtbResourceAccessory;
                }
            }
            lovAccessory.InitControl(false, dtbResourceAccessory, "code", "name", "");
        }

        private void viewDetail()
        {
            try
            {
                if (grdRotateList.GetFocusedDataRow() == null) return;
                DataRow data = grdRotateList.GetFocusedDataRow();
                loadActionStatus();

                txtRecyclingName.Text = data["name"].ToString();

                txtRedundanceDays.Text = data["shop_redundance_days"].ToString();
                txtLackDays.Text = data["shop_lack_days"].ToString();
                String Accessory_div_his_id = data["goods_rotate_history_id"].ToString();
                String page_size = "";
                if (cboPage.SelectedItem != null)
                {
                    page_size = cboPage.SelectedItem.ToString();

                }
                plcObject.SearchRotatingHistory(ref dtbResourceRedundance, ref dtbResourceLack,
                    ref dtbResourceAccessoryList, ref dtbResourceShopList, ref dtbResourceResult,
                    Accessory_div_his_id, page_size);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }
                tblRedundanceShop.DataSource = dtbResourceRedundance;
                tblLackShop.DataSource = dtbResourceLack;
                tblList.DataSource = dtbResourceResult;

                tblAccessoryList.DataSource = dtbResourceAccessoryList;
                //pnlShopList.setDataList(response.getVector("Out.shop_list"));
                tblShopList.DataSource = dtbResourceShopList;
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                   e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        private void tblRotateList_Click(object sender, EventArgs e)
        {
            viewDetail();
        }
        private DataTable SearchCommon(StringBuilder sqlSelect, Dictionary<string, string> dictParam)
        {
            StringBuilder sql = new StringBuilder(sqlSelect.ToString());
            if (dictParam != null && dictParam.Count > 0)
            {
                foreach (KeyValuePair<string, string> item in dictParam)
                {
                    sql.Replace(item.Key, "'" + item.Value + "'");
                }
            }

            object[] objKeywords = new object[]
            {
              "@command",sql.ToString()
            };
            return plcObject.SearchData(objKeywords);
        }

        public class SQL_COMMON
        {
            public StringBuilder SQL_GOODS_GROUP;
            public StringBuilder SQL_GOODS;
            public StringBuilder SQL_SHOP;

            public SQL_COMMON()
            {

                SQL_GOODS_GROUP = new StringBuilder();
                SQL_GOODS_GROUP.Append(" select '' goods_group_id, 'TRUE', '' goods_group_code, 'Tất cả' name	from dual union all		");
                SQL_GOODS_GROUP.Append(" select to_char(goods_group_id), 'TRUE', to_char(goods_group_code), to_char(name)			");
                SQL_GOODS_GROUP.Append(" from goods_group where status = '1'                            ");
                SQL_GOODS_GROUP.Append(" and product_type = 16    ");

                SQL_GOODS = new StringBuilder();
                SQL_GOODS.Append("	select distinct d.value code, d.value name, d.value  					");
                SQL_GOODS.Append("	from goods g, goods_dividing_properties d where 1=1                     ");
                SQL_GOODS.Append("	and g.goods_id = d.goods_id and d.code = 'couple_goods'                 ");
                SQL_GOODS.Append("	and (g.goods_group_id  = :goods_group_id or :goods_group_id is null)    ");
                SQL_GOODS.Append("	and g.check_serial = '0' and d.value is not null                          ");

                SQL_SHOP = new StringBuilder("select s.shop_code, s.shop_name name, s.shop_id as stock_id, s.shop_id, s.PAR_SHOP_CODE from shop s  order by s.shop_code");

                
            }

        }

        private void tmrFormatFlex_Tick(object sender, EventArgs e)
        {
            if (bolIsLoadComplete)
            {
                bolIsLoadComplete = false;

                pnSearchStatus.Visible = false;
            }
        }

        private void loadActionStatus()
        {
            pnSearchStatus.Visible = true;
            pnSearchStatus.Refresh();
        }

        private void btnCancelRotate_Click(object sender, EventArgs e)
        {
            btnNewRotate.Visible = (true);
            btnCancelRotate.Visible = (false);
            btnClose.Visible = (true);
            btnRemove.Visible = (true);
            tblRotateList.Enabled = (true);
            if (grdAccessoryList.GetFocusedDataRow() == null) return;
            DataRow data = grdAccessoryList.GetFocusedDataRow();

            txtRecyclingName.Text = data["name"].ToString();

            tblRedundanceShop.DataSource = dtbResourceRedundanceBackUp;
            tblLackShop.DataSource = dtbResourceLackBackUp;
            tblList.DataSource = dtbResourceResultBackUp;

            tblAccessoryList.DataSource = dtbResourceAccessoryListBackUp;
            //pnlShopList.setDataList(response.getVector("Out.shop_list"));
            tblShopList.DataSource = dtbResourceShopListBackUp;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNewRotate_Click(object sender, EventArgs e)
        {
            dtbResourceLackBackUp = dtbResourceLack.Copy();
            dtbResourceRedundanceBackUp = dtbResourceRedundance.Copy();
            dtbResourceAccessoryListBackUp = dtbResourceAccessoryList.Copy();
            dtbResourceResultBackUp = dtbResourceResult.Copy();
            dtbResourceShopListBackUp = dtbResourceShopList.Copy();

            tblRotateList.Enabled = (false);


            btnNewRotate.Visible = (false);
            btnCancelRotate.Visible = (true);
            btnClose.Visible = (false);
            clearDetailValue();
            txtRecyclingName.Focus();
            dtbResourceResult.Clear();
            dtbResourceLack.Clear();
            dtbResourceRedundance.Clear();
            dtbResourceAccessoryList.Clear();
            dtbResourceShopList.Clear();

            dtbResourceResult.AcceptChanges();
            dtbResourceLack.AcceptChanges();
            dtbResourceRedundance.AcceptChanges();
            dtbResourceAccessoryList.AcceptChanges();
            dtbResourceShopList.AcceptChanges();
            tabDetail.SelectedTab = tabPage1;
        }

        private void clearDetailValue()
        {
            txtRecyclingName.Text = ("");
            txtRedundanceDays.Text = ("20");
            txtLackDays.Text = ("5");
            cboAccessoryGroup.Refresh();
        }

        private void btnAddAccessory_Click(object sender, EventArgs e)
        {
            if (lovAccessory.ColumnID == null || lovAccessory.ColumnID.ToString() == "")
            {
                MessageBox.Show("Bạn cần chọn mặt hàng để thực hiện", "Thông báo",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            if (dtbResourceAccessoryList.Rows.Count > 0)
            {
                for (int i = 0; i < dtbResourceAccessoryList.Rows.Count; i++)
                {
                    if (dtbResourceAccessoryList.Rows[i]["value_id"].ToString().Equals(lovAccessory.ColumnID.ToString()))
                    {
                        MessageBox.Show("Hàng đã có trong danh sách", "Thông báo",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            string redundance_days = txtRedundanceDays.Text;
            string lack_days = txtLackDays.Text;

            DataRow data = dtbResourceAccessoryList.NewRow();
            data["value_id"] = lovAccessory.ColumnID.ToString();
            data["goods_code"] = lovAccessory.ColumnID.ToString();
            data["name"] = lovAccessory.ColumnID.ToString();
            data["REDUNDANCE_DAYS"] = redundance_days;
            data["LACK_DAYS"] = lack_days;
            dtbResourceAccessoryList.Rows.Add(data);
            dtbResourceAccessoryList.AcceptChanges();
            lovAccessory.Refresh();


        }

        private void btnAllAccessory_Click(object sender, EventArgs e)
        {
            string redundance_days = txtRedundanceDays.Text;
            string lack_days = txtLackDays.Text;
            dtbResourceAccessoryList.Clear();
            if (dtbResourceAccessory.Rows.Count > 0)
            {
                for (int i = 0; i < dtbResourceAccessory.Rows.Count; i++)
                {
                    DataRow data = dtbResourceAccessoryList.NewRow();
                    string value = dtbResourceAccessory.Rows[i]["value"].ToString();
                    data["value_id"] = value;
                    data["goods_code"] = value;
                    data["name"] = value;
                    data["REDUNDANCE_DAYS"] = redundance_days;
                    data["LACK_DAYS"] = lack_days;
                    dtbResourceAccessoryList.Rows.Add(data);
                }

            }
            dtbResourceAccessoryList.AcceptChanges();

        }

        private void btnRemoveAccessory_Click(object sender, EventArgs e)
        {
            if (grdAccessoryList.GetFocusedDataRow() == null || grdAccessoryList.GetFocusedDataRow()["value_id"].ToString() == "")
            {
                MessageBox.Show("Bạn cần chọn một bản ghi để thực hiện", "Thông báo",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            dtbResourceAccessoryList.Rows.Remove(grdAccessoryList.GetFocusedDataRow());
            dtbResourceAccessoryList.AcceptChanges();
        }

        private void btnRemoveAllAccessory_Click(object sender, EventArgs e)
        {
            dtbResourceAccessoryList.Clear();
            dtbResourceAccessoryList.AcceptChanges();
        }

        private void btnImportAccessory_Click(object sender, EventArgs e)
        {
            loadActionStatus();
            Thread thdSyncRead = new Thread(new ThreadStart(importGoods));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }

        private void importGoods()
        {
            try
            {
                if (dtbResourceAccessoryAll == null || dtbResourceAccessoryAll.Rows.Count <= 0)
                {
                    Dictionary<string, string> map = new Dictionary<string, string>() {
                                                {":goods_group_id", ""},
                                              };
                    dtbResourceAccessoryAll = SearchCommon(sql_common.SQL_GOODS, map);
                }

                OpenFileDialog Openfile = new OpenFileDialog();
                Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
                //Openfile.Filter = "Excel Files|*.xls;*.xlsx";
                //Openfile.Title = "Select a Cursor File";
                //Openfile.InitialDirectory = "C:";
                DialogResult result = Openfile.ShowDialog();
                if (result == DialogResult.Cancel) return;
                try
                {
                    DataTable dt = new DataTable();
                    dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);

                    frmSameLevelAccessoryDividingImportExcel frm = new frmSameLevelAccessoryDividingImportExcel(false);
                    frm.SourceTable = dt;
                    frm.DtbGoodsResource = dtbResourceAccessoryAll.Copy();

                    frm.ShowDialog();
                    if (frm.IsUpdate)
                    {
                        if (frm.SourceTable.Rows.Count > 0)
                        {
                            dtbResourceAccessoryList.Clear();
                            dtbResourceAccessoryList.AcceptChanges();
                            DataRow row;
                            DataRow data;
                            for (int i = 0; i < frm.SourceTable.Rows.Count; i++)
                            {
                                if (!Convert.ToBoolean(frm.SourceTable.Rows[i]["IsError"]))
                                {
                                    row = frm.SourceTable.Rows[i];
                                    data = dtbResourceAccessoryList.NewRow();
                                    data["value_id"] = row["value_id"];
                                    data["goods_code"] = row["goods_code"];
                                    data["name"] = row["name"];
                                    data["REDUNDANCE_DAYS"] = row["REDUNDANCE_DAYS"];
                                    data["LACK_DAYS"] = row["LACK_DAYS"];
                                    dtbResourceAccessoryList.Rows.Add(data);
                                    dtbResourceAccessoryList.AcceptChanges();
                                }
                            }
                        }
                    }

                }
                catch (Exception objex)
                {
                    MessageBox.Show(objex.Message.ToString(), "Lỗi nạp thông tin từ file Excel!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                   ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            loadActionStatus();
            Thread thdSyncRead = new Thread(new ThreadStart(importShops));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }

        private void importShops()
        {
            try
            {

                OpenFileDialog Openfile = new OpenFileDialog();
                Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
                //Openfile.Filter = "Excel Files|*.xls;*.xlsx";
                //Openfile.Title = "Select a Cursor File";
                //Openfile.InitialDirectory = "C:";
                DialogResult result = Openfile.ShowDialog();
                if (result == DialogResult.Cancel) return;
                try
                {
                    DataTable dt = new DataTable();
                    dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);

                    frmSameLevelAccessoryDividingImportExcel frm = new frmSameLevelAccessoryDividingImportExcel(true);
                    frm.SourceTable = dt;
                    frm.DtbShopResource = dtbResourceShops.Copy();

                    frm.ShowDialog();
                    if (frm.IsUpdate)
                    {
                        if (frm.SourceTable.Rows.Count > 0)
                        {
                            dtbResourceShopList.Clear();
                            dtbResourceShopList.AcceptChanges();
                            DataRow row;
                            DataRow data;
                            for (int i = 0; i < frm.SourceTable.Rows.Count; i++)
                            {
                                if (!Convert.ToBoolean(frm.SourceTable.Rows[i]["IsError"]))
                                {
                                    row = frm.SourceTable.Rows[i];
                                    data = dtbResourceShopList.NewRow();
                                    data["shop_code"] = row["shop_code"].ToString();
                                    data["shop_name"] = row["shop_name"].ToString();
                                    data["stock_id"] =  row["stock_id"].ToString();
                                    dtbResourceShopList.Rows.Add(data);
                                    dtbResourceShopList.AcceptChanges();
                                }
                            }
                        }
                    }

                }
                catch (Exception objex)
                {
                    MessageBox.Show(objex.Message.ToString(), "Lỗi nạp thông tin từ file Excel!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                   ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            dtbResourceShopList.Clear();
            if (dtbResourceShops.Rows.Count > 0)
            {
                for (int i = 0; i < dtbResourceShops.Rows.Count; i++)
                {
                    DataRow data = dtbResourceShopList.NewRow();
                    data["shop_code"] = dtbResourceShops.Rows[i]["shop_code"].ToString();
                    data["shop_name"] = dtbResourceShops.Rows[i]["name"].ToString();
                    data["stock_id"] = dtbResourceShops.Rows[i]["stock_id"].ToString();
                    dtbResourceShopList.Rows.Add(data);
                }

            }
            dtbResourceShopList.AcceptChanges();
        }

        private void toolStripAll_Click(object sender, EventArgs e)
        {
            btnAll_Click(null, null);
        }

        private void testToolStripMB_Click(object sender, EventArgs e)
        {
            dtbResourceShopList.Clear();
            if (dtbResourceShops.Rows.Count > 0)
            {
                for (int i = 0; i < dtbResourceShops.Rows.Count; i++)
                {

                    DataRow data = dtbResourceShopList.NewRow();
                    if (dtbResourceShops.Rows[i]["PAR_SHOP_CODE"].Equals("VTMB"))
                    {
                        data["shop_code"] = dtbResourceShops.Rows[i]["shop_code"].ToString();
                        data["shop_name"] = dtbResourceShops.Rows[i]["name"].ToString();
                        data["stock_id"] = dtbResourceShops.Rows[i]["stock_id"].ToString();
                        dtbResourceShopList.Rows.Add(data);
                    }
                }

            }
            dtbResourceShopList.AcceptChanges();

        }

        private void toolStripMN_Click(object sender, EventArgs e)
        {
            dtbResourceShopList.Clear();
            if (dtbResourceShops.Rows.Count > 0)
            {
                for (int i = 0; i < dtbResourceShops.Rows.Count; i++)
                {

                    DataRow data = dtbResourceShopList.NewRow();
                    if (dtbResourceShops.Rows[i]["PAR_SHOP_CODE"].Equals("VTMN"))
                    {
                        data["shop_code"] = dtbResourceShops.Rows[i]["shop_code"].ToString();
                        data["shop_name"] = dtbResourceShops.Rows[i]["name"].ToString();
                        data["stock_id"] = dtbResourceShops.Rows[i]["stock_id"].ToString();
                        dtbResourceShopList.Rows.Add(data);
                    }
                }

            }
            dtbResourceShopList.AcceptChanges();

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (lovShop.ColumnID == null || lovShop.ColumnID.ToString() == "")
            {
                MessageBox.Show("Bạn phải chọn siêu thị", "Thông báo",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (dtbResourceShopList.Rows.Count > 0)
            {
                for (int i = 0; i < dtbResourceShopList.Rows.Count; i++)
                {
                    if (dtbResourceShopList.Rows[i]["stock_id"].ToString().Equals(lovShop.ColumnID.ToString()))
                    {
                        MessageBox.Show("Siêu thị đã có trong danh sách", "Thông báo",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }

            DataRow data = dtbResourceShopList.NewRow();
            for (int i = 0; i < dtbResourceShops.Rows.Count; i++)
            {
                if (dtbResourceShops.Rows[i]["stock_id"].ToString().Equals(lovShop.ColumnID.ToString()))
                {
                    data["shop_code"] = dtbResourceShops.Rows[i]["shop_code"].ToString();
                    data["shop_name"] = dtbResourceShops.Rows[i]["name"].ToString();
                    data["stock_id"] = dtbResourceShops.Rows[i]["stock_id"].ToString();
                    break;
                }
            }
            dtbResourceShopList.Rows.Add(data);
            dtbResourceShopList.AcceptChanges();
            lovShop.Refresh();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (grdShopList.GetFocusedDataRow() == null || grdShopList.GetFocusedDataRow()["value_id"].ToString() == "")
            {
                MessageBox.Show("Bạn cần chọn một bản ghi để thực hiện", "Thông báo",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            dtbResourceShopList.Rows.Remove(grdShopList.GetFocusedDataRow());
            dtbResourceShopList.AcceptChanges();
        }

        private void btnRemoveAll_Click(object sender, EventArgs e)
        {
            dtbResourceShopList.Clear();
            dtbResourceShopList.AcceptChanges();
        }

        private void btnCompute_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtRecyclingName.Text))
                {
                    MessageBox.Show("Bạn cần nhập Tên lần điều chuyển", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
               

                if (btnNewRotate.Visible)
                {
                    MessageBox.Show("Bạn cần tạo mới lần thực hiện!", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return;
                }

                loadActionStatus();

                string name, shop_redundance_days, shop_lack_days, user_shop_id, user_id, status, type, over_days_redundance_shop;

                user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                name = txtRecyclingName.Text;
                shop_redundance_days = "0";
                shop_lack_days = "0";
                status = "1";
                type = "1";
                over_days_redundance_shop = "0";

                object[] objKeywords = new object[]{
                            "@user_shop_id",user_shop_id,
                            "@user_id", user_id,
                            "@name", name,
                            "@status",status,
                            "@type", type,
                            "@shop_lack_days",shop_lack_days,
                            "@shop_redundance_days",shop_redundance_days,
                            "@over_days_redundance_shop",over_days_redundance_shop,
                           };

                List<object[]> lsAccessory = new List<object[]>();
                object[] objKeywordsGoods;
                for (int i = 0; i < dtbResourceAccessoryList.Rows.Count; i++)
                {
                    objKeywordsGoods = new object[]{
                            "@value_id",dtbResourceAccessoryList.Rows[i]["value_id"].ToString(),
                            "@redundance_days", dtbResourceAccessoryList.Rows[i]["redundance_days"].ToString(),
                            "@lack_days", dtbResourceAccessoryList.Rows[i]["lack_days"].ToString(),
                           
                           };
                    lsAccessory.Add(objKeywordsGoods);

                }
                List<String> lsShops = new List<String>();
                for (int i = 0; i < dtbResourceShopList.Rows.Count; i++)
                {
                    lsShops.Add(dtbResourceShopList.Rows[i]["stock_id"].ToString());

                }

                string goods_rotate_history_id = plcObject.AccessRotate(objKeywords, lsAccessory.ToArray(), lsShops.ToArray());
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }


                DataRow rowClone = dtbResourceRotateList.NewRow();
                rowClone["goods_rotate_history_id"] = goods_rotate_history_id;
                rowClone["name"] = txtRecyclingName.Text;
                dtbResourceRotateList.Rows.InsertAt(rowClone, 0);
                dtbResourceRotateList.AcceptChanges();
                grdRotateList.SelectRow(0);
                tblRotateList_Click(null, null);
                tblRotateList.Enabled = (true);
                btnNewRotate.Visible = (true);
                btnCancelRotate.Visible = (false);
                btnClose.Visible = (true);

                
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                   ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        private void btnRecycling_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdRotateList.GetFocusedDataRow() == null) return;
                DataRow data = grdRotateList.GetFocusedDataRow();
                loadActionStatus();

                string goods_rotate_history_id = data["goods_rotate_history_id"].ToString();

                dtbResourceResult = plcObject.RecyclingRotateAccess(goods_rotate_history_id);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                tblList.DataSource = dtbResourceResult;
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                   ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Common.frmExportExcel frmExportExcel = new frmExport()
            {
                StrFileTemplateName = "goods_rotate_result_report",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\goods_rotate_result_report.xlsx",
                DtbResourceSource = dtbResourceResult
            };
            frmExportExcel.btnExport_Click();
        }

        class frmExport : Common.frmExportExcel
        {

            public override void AddRowDetail(ref ExcelWorksheet sheet, DataRow row, ref int iRow, ref int iSTT)
            {
                InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
                InsertValueCell(ref sheet, "B", iRow, row["FROM_SHOP"].ToString(), false, false);
                InsertValueCell(ref sheet, "C", iRow, row["TO_SHOP"].ToString(), false, false);
                InsertValueCell(ref sheet, "D", iRow, row["GOODS_CODE"].ToString(), false, false);
                InsertValueCell(ref sheet, "E", iRow, row["GOODS_NAME"].ToString(), false, false);
                InsertValueCell(ref sheet, "F", iRow, row["ORG_VALUE"].ToString(), false, false);
                InsertValueCell(ref sheet, "G", iRow, row["VALUE"].ToString(), false, false);
                InsertValueCell(ref sheet, "H", iRow, row["DISTANCE"].ToString(), false, false);
                iSTT++;
                iRow++;
            }
        }

        private void btnDownTemplate_Click(object sender, EventArgs e)
        {
            Common.frmDownloadTemplateImport frmExportExcel = new Common.frmDownloadTemplateImport()
            {
                StrFileTemplateName = "import_shop_dieuchuyennganghangPK",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\import_shop_dieuchuyennganghangPK.xlsx",
            };
            frmExportExcel.btnExportTemplate_Click();
        }

        private void btnDownTemplateAccessory_Click(object sender, EventArgs e)
        {
            Common.frmDownloadTemplateImport frmExportExcel = new Common.frmDownloadTemplateImport()
            {
                StrFileTemplateName = "import_good_dieuchuyennganghangPK",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\import_good_dieuchuyennganghangPK.xlsx",
            };
            frmExportExcel.btnExportTemplate_Click();
        }
    }
}
