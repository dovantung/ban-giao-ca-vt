﻿using GemBox.Spreadsheet;
using Library.AppCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Accessory
{
    public partial class DUIAccessoryDividing : Form
    {
        private PLC.Accessory.PLCAccessoryDiving plcObject = new PLC.Accessory.PLCAccessoryDiving();
        private PLC.Accessory.WSAccessoryDividing.ResultMessage objResultMessage = null;

        private DataTable dtbResourceDividingTime = null;
        private DataTable dtbResourceSource = null;
        private DataTable dtbResourceList = null;

        private SQL_COMMON sql_common;
        private DataTable dtbResourceGoods = null;
        private DataRow dataSelect = null;
        private bool bolIsLoadComplete = false;

        public DUIAccessoryDividing()
        {
            sql_common = new SQL_COMMON();
            InitializeComponent();
            loadDataAndMapActionInForm();
            DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
        }

        private void loadDataAndMapActionInForm()
        {
            cboDividingTimes.SelectionChangeCommitted += new System.EventHandler(cboDividingTimes_SelectionChangeCommitted);

            this.repositoryItemTextEdit3.KeyPress += new KeyPressEventHandler(delegate(object sender, KeyPressEventArgs e)
            {
                dataSelect = grdSource.GetFocusedDataRow();
                if (e.KeyChar == (char)Keys.Enter)
                {
                    UpdateAdjustmentNumber();
                }

            });
            this.repositoryItemTextEdit3.Leave += new System.EventHandler(delegate(object sender, EventArgs e)
            {
                UpdateAdjustmentNumber();
            });
        }

        private void UpdateAdjustmentNumber()
        {
            if (dataSelect != null &&
                !String.IsNullOrEmpty(dataSelect["goods_division_source_list_id"].ToString())
                && !dataSelect["ORG_VALUE"].Equals(dataSelect["VALUE"].ToString()))
            {
                string user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                string user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                object[] objKeywords = new object[]{
                            "@division_source_list_id",dataSelect["goods_division_source_list_id"].ToString(),
                            "@user_id", user_id,
                            "@user_shop_id", user_shop_id,
                            "@value",dataSelect["VALUE"].ToString(),
                           };
                plcObject.UpdateAdjustmentNumber(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                dataSelect["ORG_VALUE"] = dataSelect["VALUE"].ToString();
                dataSelect["username"] = user_id;
                dataSelect["modify_date"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                dtbResourceSource.AcceptChanges();
            }
        } 

        private void cboDividingTimes_Load(object sender, EventArgs e)
        {
            dtbResourceDividingTime = plcObject.GetListDivideHis();
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            cboDividingTimes.InitControl(false, dtbResourceDividingTime, "goods_division_history_id", "name", "");

        }
        private void cboDividingTimes_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            if (cboDividingTimes.ColumnID < 1)
            {
                // dtbResourceGoodsGroup = null;
            }
            else
            {
                object[] objKeywords = new object[]
            {
              "@goods_division_history_id",cboDividingTimes.ColumnID
            };
                plcObject.GetDivisionItemList(ref dtbResourceSource, ref dtbResourceList, objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }
                tblSource.DataSource = dtbResourceSource;
                tblList.DataSource = dtbResourceList;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGetStockCard_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboDividingTimes.ColumnID < 1)
                {
                    // dtbResourceGoodsGroup = null;
                }
                else
                {
                    loadActionStatus();
                    object[] objKeywords = new object[]
                    {
                      "@goods_division_history_id",cboDividingTimes.ColumnID
                    };
                    dtbResourceSource = plcObject.GetStockCardAccessory(objKeywords);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    tblSource.DataSource = dtbResourceSource;
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                   ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            loadActionStatus();
            Thread thdSyncRead = new Thread(new ThreadStart(import));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }

        private void import()
        {
            try
            {
                if (cboDividingTimes.ColumnID < 1)
                {
                    return;
                }

                DataRow rowSelect = null;
                for (int i = 0; i < dtbResourceDividingTime.Rows.Count; i++)
                {
                    if (dtbResourceDividingTime.Rows[i]["goods_division_history_id"].ToString().Equals(cboDividingTimes.ColumnID.ToString()))
                    {
                        if (dtbResourceDividingTime.Rows[i]["division_status"].ToString().Equals("2"))
                        {
                            MessageBox.Show("Lần chia hàng này đã thực hiện tạo lệnh xuất kho nên không thể thực hiện lại", "Thông báo",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        rowSelect = dtbResourceDividingTime.Rows[i];
                        break;
                    }
                }

                if (dtbResourceGoods == null || dtbResourceGoods.Rows.Count <= 0)
                {
                    Dictionary<string, string> map = new Dictionary<string, string>() {
                                                {":goods_division_history_id", cboDividingTimes.ColumnID.ToString()},
                                               };
                    dtbResourceGoods = SearchCommon(sql_common.SQL_GOODS, map);
                }

                OpenFileDialog Openfile = new OpenFileDialog();
                Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
                //Openfile.Filter = "Excel Files|*.xls;*.xlsx";
                //Openfile.Title = "Select a Cursor File";
                //Openfile.InitialDirectory = "C:";
                DialogResult result = Openfile.ShowDialog();
                if (result == DialogResult.Cancel) return;
                try
                {
                    DataTable dt = new DataTable();
                    dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);

                    frmAccessoryDividingImportExcel frm = new frmAccessoryDividingImportExcel();
                    frm.SourceTable = dt;
                    frm.DtbGoodsResource = dtbResourceGoods.Copy();

                    frm.ShowDialog();
                    if (frm.IsUpdate)
                    {
                        if (frm.SourceTable.Rows.Count > 0)
                        {
                            dtbResourceSource.Clear();
                            dtbResourceSource.AcceptChanges();
                            DataRow row;
                            DataRow data;
                            for (int i = 0; i < frm.SourceTable.Rows.Count; i++)
                            {
                                if (!Convert.ToBoolean(frm.SourceTable.Rows[i]["IsError"]))
                                {
                                    row = frm.SourceTable.Rows[i];
                                    data = dtbResourceSource.NewRow();
                                    data["GOODS_DIVISION_SOURCE_LIST_ID"] = row["GOODS_DIVISION_SOURCE_LIST_ID"];
                                    data["NAME"] = row["NAME"].ToString();
                                    data["GOODS_ID"] = row["GOODS_ID"].ToString();
                                    data["ORG_VALUE"] = row["ORG_VALUE"].ToString();
                                    data["OLD_VALUE"] = row["OLD_VALUE"].ToString();
                                    data["GOODS_CODE"] = row["GOODS_CODE"].ToString();
                                    data["VALUE"] = row["VALUE"].ToString();
                                    data["SHOP_CODE"] = row["SHOP_CODE"].ToString();
                                    data["USERNAME"] = "";
                                    data["MODIFY_DATE"] = "";
                                    dtbResourceSource.Rows.Add(data);
                                    dtbResourceSource.AcceptChanges();
                                }
                            }
                        }
                    }

                }
                catch (Exception objex)
                {
                    MessageBox.Show(objex.Message.ToString(), "Lỗi nạp thông tin từ file Excel!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                   ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        private object[][] convertTableResourceToObject()
        {
            List<object[]> lsObject = new List<object[]>();
            string user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
            string user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
            object[] objKeywords;
            string goods_division_history_id = "", goods_id = "", shop_code = "", value = "", org_value = "";
            for (int i = 0; i < dtbResourceSource.Rows.Count; i++)
            {
                goods_division_history_id = cboDividingTimes.ColumnID.ToString();
                goods_id = dtbResourceSource.Rows[i]["GOODS_ID"].ToString();
                shop_code = dtbResourceSource.Rows[i]["SHOP_CODE"].ToString();
                value = dtbResourceSource.Rows[i]["VALUE"].ToString();
                org_value = dtbResourceSource.Rows[i]["ORG_VALUE"].ToString();
                objKeywords = new object[]{
                           		"@goods_division_history_id",goods_division_history_id,    
                                "@goods_id"                 ,goods_id,    
                                "@user_id"                  ,user_id,    
                                "@user_shop_id"             ,user_shop_id,    
                                "@shop_code"                ,shop_code,     
                                "@value"                    ,value,    
                                "@org_value"                ,org_value, 
                        };
                lsObject.Add(objKeywords);
            }
            return lsObject.ToArray();
        }

       



        private void btnApplyStockCard_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboDividingTimes.ColumnID < 1)
                {
                    // dtbResourceGoodsGroup = null;
                }
                else
                {
                    loadActionStatus();
                    plcObject.UpdateSourceForDividing(cboDividingTimes.ColumnID.ToString(), convertTableResourceToObject());
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                   ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        private void btnExportERP_Click(object sender, EventArgs e)
        {

        }

        private void btnDividing_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboDividingTimes.ColumnID < 1)
                {
                    // dtbResourceGoodsGroup = null;
                }
                else
                {
                    loadActionStatus();
                    DataRow rowSelect = null;
                    for (int i = 0; i < dtbResourceDividingTime.Rows.Count; i++)
                    {
                        if (dtbResourceDividingTime.Rows[i]["goods_division_history_id"].ToString().Equals(cboDividingTimes.ColumnID.ToString()))
                        {
                            if (dtbResourceDividingTime.Rows[i]["division_status"].ToString().Equals("2"))
                            {
                                MessageBox.Show("Lần chia hàng này đã thực hiện tạo lệnh xuất kho nên không thể thực hiện lại", "Thông báo",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            rowSelect = dtbResourceDividingTime.Rows[i];
                            break;
                        }
                    }
                    plcObject.OnDividing(cboDividingTimes.ColumnID.ToString(), convertTableResourceToObject());
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    rowSelect["division_status"] = "1";
                    cboDividingTimes_SelectionChangeCommitted(null, null);

                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                   ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        private DataTable SearchCommon(StringBuilder sqlSelect, Dictionary<string, string> dictParam)
        {
            StringBuilder sql = new StringBuilder(sqlSelect.ToString());
            if (dictParam != null && dictParam.Count > 0)
            {
                foreach (KeyValuePair<string, string> item in dictParam)
                {
                    sql.Replace(item.Key, "'" + item.Value + "'");
                }
            }

            object[] objKeywords = new object[]
            {
              "@command",sql.ToString()
            };
            return plcObject.SearchData(objKeywords);
        }
        public class SQL_COMMON
        {
            public StringBuilder SQL_GOODS;

            public SQL_COMMON()
            {

                SQL_GOODS = new StringBuilder();
                SQL_GOODS.Append("	select goods_code, goods_id, name from goods where status = '1' 			");
                SQL_GOODS.Append("	 and goods_group_id in (select value_id from goods_division_param_list pl   ");
                SQL_GOODS.Append("	 where pl.type = 0                                                          ");
                SQL_GOODS.Append("	 and pl.goods_division_history_id = :goods_division_history_id)             ");
            }

        }

        private void tmrFormatFlex_Tick(object sender, EventArgs e)
        {
            if (bolIsLoadComplete)
            {
                bolIsLoadComplete = false;

                pnSearchStatus.Visible = false;
            }
        }

        private void loadActionStatus()
        {
            pnSearchStatus.Visible = true;
            pnSearchStatus.Refresh();
        }

        private void btnExportStockCard_Click(object sender, EventArgs e)
        {
            Common.frmExportExcel frmExportExcel = new frmExportStockCard()
            {
                StrFileTemplateName = "StockCard_PK_report",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\StockCard_PK_report.xlsx",
                DtbResourceSource = dtbResourceSource   
            };
            frmExportExcel.btnExport_Click();
            
            
            
        }

        class frmExportStockCard : Common.frmExportExcel
        {

            public override void AddRowDetail(ref ExcelWorksheet sheet, DataRow row, ref int iRow, ref int iSTT)
            {
                InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
                InsertValueCell(ref sheet, "B", iRow, row["GOODS_CODE"].ToString(), false, false);
                InsertValueCell(ref sheet, "C", iRow, row["NAME"].ToString(), false, false);
                InsertValueCell(ref sheet, "D", iRow, row["SHOP_CODE"].ToString(), false, false);
                InsertValueCell(ref sheet, "E", iRow, row["ORG_VALUE"].ToString(), false, false);
                InsertValueCell(ref sheet, "F", iRow, row["VALUE"].ToString(), false, false);
                InsertValueCell(ref sheet, "G", iRow, row["USERNAME"].ToString(), false, false);
                InsertValueCell(ref sheet, "H", iRow, row["MODIFY_DATE"].ToString(), false, false);
                iSTT++;
                iRow++;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Common.frmExportExcel frmExportExcel = new frmExport()
            {
                StrFileTemplateName = "AccessoryDividing_PK_report",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\AccessoryDividing_PK_report.xlsx",
                DtbResourceSource = dtbResourceList
            };
            frmExportExcel.btnExport_Click();
        }

        class frmExport : Common.frmExportExcel
        {

            public override void AddRowDetail(ref ExcelWorksheet sheet, DataRow row, ref int iRow, ref int iSTT)
            {
                InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
                InsertValueCell(ref sheet, "B", iRow, row["GOODS_CODE"].ToString(), false, false);
                InsertValueCell(ref sheet, "C", iRow, row["GOODS_NAME"].ToString(), false, false);
                InsertValueCell(ref sheet, "D", iRow, row["COUPLE_GOODS"].ToString(), false, false);
                InsertValueCell(ref sheet, "E", iRow, row["FROM_STOCK"].ToString(), false, false);
                InsertValueCell(ref sheet, "F", iRow, row["STOCK_NAME"].ToString(), false, false);
                InsertValueCell(ref sheet, "G", iRow, row["BLOCK"].ToString(), false, false);
                InsertValueCell(ref sheet, "H", iRow, row["ORG_VALUE"].ToString(), false, false);
                InsertValueCell(ref sheet, "I", iRow, row["VALUE"].ToString(), false, false);
                InsertValueCell(ref sheet, "J", iRow, row["FROM_STOCK"].ToString(), false, false);
                iSTT++;
                iRow++;
            }
        }

        private void btnDownTemplate_Click(object sender, EventArgs e)
        {
            Common.frmDownloadTemplateImport frmExportExcel = new Common.frmDownloadTemplateImport()
            {
                StrFileTemplateName = "import_chiahangPK",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\import_chiahangPK.xlsx",
            };
            frmExportExcel.btnExportTemplate_Click();
        }
    }
}
