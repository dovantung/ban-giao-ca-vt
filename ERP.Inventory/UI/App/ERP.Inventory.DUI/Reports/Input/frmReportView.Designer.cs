﻿namespace ERP.Inventory.DUI.Reports.Input
{
    partial class frmReportView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowInputVoucher = new DevExpress.XtraEditors.SimpleButton();
            this.btnShowInputVoucherDetail = new DevExpress.XtraEditors.SimpleButton();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.cboLien = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnShowInputVoucher
            // 
            this.btnShowInputVoucher.Location = new System.Drawing.Point(491, 2);
            this.btnShowInputVoucher.Name = "btnShowInputVoucher";
            this.btnShowInputVoucher.Size = new System.Drawing.Size(111, 23);
            this.btnShowInputVoucher.TabIndex = 1;
            this.btnShowInputVoucher.Text = "Xem phiếu nhập";
            this.btnShowInputVoucher.Click += new System.EventHandler(this.btnShowInputVoucher_Click);
            // 
            // btnShowInputVoucherDetail
            // 
            this.btnShowInputVoucherDetail.Location = new System.Drawing.Point(616, 2);
            this.btnShowInputVoucherDetail.Name = "btnShowInputVoucherDetail";
            this.btnShowInputVoucherDetail.Size = new System.Drawing.Size(111, 23);
            this.btnShowInputVoucherDetail.TabIndex = 1;
            this.btnShowInputVoucherDetail.Text = "Xem chi tiết";
            this.btnShowInputVoucherDetail.Click += new System.EventHandler(this.btnShowInputVoucherDetail_Click);
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.ShowGroupTreeButton = false;
            this.crystalReportViewer1.ShowParameterPanelButton = false;
            this.crystalReportViewer1.Size = new System.Drawing.Size(883, 550);
            this.crystalReportViewer1.TabIndex = 2;
            this.crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // cboLien
            // 
            this.cboLien.FormattingEnabled = true;
            this.cboLien.Items.AddRange(new object[] {
            "Liên 1",
            "Liên 2",
            "Liên 3"});
            this.cboLien.Location = new System.Drawing.Point(411, 4);
            this.cboLien.Name = "cboLien";
            this.cboLien.Size = new System.Drawing.Size(74, 21);
            this.cboLien.TabIndex = 3;
            this.cboLien.SelectedIndexChanged += new System.EventHandler(this.cboLien_SelectedIndexChanged);
            // 
            // frmReportView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 550);
            this.Controls.Add(this.cboLien);
            this.Controls.Add(this.btnShowInputVoucherDetail);
            this.Controls.Add(this.btnShowInputVoucher);
            this.Controls.Add(this.crystalReportViewer1);
            this.Name = "frmReportView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmReportView";
            this.Load += new System.EventHandler(this.frmReportView_Load);
            this.Shown += new System.EventHandler(this.frmReportView_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnShowInputVoucher;
        private DevExpress.XtraEditors.SimpleButton btnShowInputVoucherDetail;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.ComboBox cboLien;
    }
}