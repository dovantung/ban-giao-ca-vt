﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.Reports.Input
{
    public partial class frmReportView : Form
    {
        public frmReportView()
        {
            InitializeComponent();
        }

        private string strInputVoucherID = string.Empty;
        bool isGetData = false;
        public string InputVoucherID
        {
            get { return strInputVoucherID; }
            set { strInputVoucherID = value; }
        }
        private CrystalDecisions.CrystalReports.Engine.ReportDocument crDoc = null;
        
        public int intIsInputVoucherReportVAT = 0;
        public bool bolIsPriceHide = true;
        public int intIsInputVoucherInKim = 0;
        bool closing = false;
        private void frmReportView_Load(object sender, EventArgs e)
        {
            cboLien.SelectedIndex = 0;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            if (!GetInputVoucherData(1))
            {
                if (closing)
                {
              
                    this.Close();
                }
                else
                {
                    MessageBox.Show(this, "Lỗi xem phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }

        }
        private DataTable dtData = null;
        
        private bool GetInputVoucherData(int Lien)
        {
            try
            {
                
                string strMoney = string.Empty;
                this.Text = "In phiếu nhập " + strInputVoucherID;
                crystalReportViewer1.ReportSource = null;
                String strPathReport = "\\Input\\rptInputVoucherReport.rpt";

                if (intIsInputVoucherReportVAT == 1)
                    strPathReport = "\\Input\\rptInputVoucherReportVAT.rpt";
                if (intIsInputVoucherInKim == 1)
                {
                    strPathReport = "\\Input\\rptInputVoucher.rpt";
                }
                if (crDoc != null)
                {
                    crDoc.Close();
                    crDoc.Dispose();
                }
                crDoc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                crDoc.Load(DUIInventory_Globals.ReportPath + strPathReport);
                if (dtData == null || dtData.Rows.Count == 0)
                {
                    dtData = new PLC.Input.PLCInputVoucher().Report_GetInputVoucherData(strInputVoucherID);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                }

                if (intIsInputVoucherInKim == 1)
                {

                    int countRow = 0;
                    double sumMoney = 0;
                    for (int i = 0; i < dtData.Rows.Count; i++)
                    {
                        //string strInputDate = dtData.Rows[i]["INPUTDATE"].ToString();
                        string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                        if (string.IsNullOrEmpty(strProductName)) break;
                        //int price = Convert.ToInt32(dtData.Rows[i]["PRICE"].ToString().Trim());
                        double price = 0;
                        double.TryParse(dtData.Rows[i]["PRICE"].ToString().Trim(), out price);
                        int quantity = Convert.ToInt32(dtData.Rows[i]["QUANTITY"].ToString().Trim());
                        Font font = new Font("Arial", 9.0f);
                        int rowProductName = GetNumOfLines(strProductName, 160, new Font(font, FontStyle.Bold));
                        countRow += rowProductName;
                        sumMoney = sumMoney + (price * quantity);

                        string checkReal = dtData.Rows[i]["ISCHECKREALINPUT"].ToString().Trim();
                        if (checkReal == "0")
                        {
                            dtData.Rows[i]["QUANTITYCHECKREAL"] = 0;
                        }
                        if (string.IsNullOrEmpty(dtData.Rows[i]["InvoiceID"].ToString()))
                        {
                            dtData.Rows[i]["InvoiceID"] = "";
                        }
                        if (string.IsNullOrEmpty(dtData.Rows[i]["ORDERID"].ToString()))
                        {
                            dtData.Rows[i]["ORDERID"] = "";
                        }
                    }
                    strMoney = Library.Common.Number.MoneyToString.ReadMoney(sumMoney);

                    for (int i = 0; i < dtData.Rows.Count; i++)
                    {
                        // dtData.Rows[i]["STT"] = i + 1;




                        dtData.Rows[i]["totalMoneyText"] = strMoney;
                        //dtData.Rows[i]["storename"] = "haha";
                    }
                    if (!isGetData)
                    {
                        if (countRow < 15)
                        {
                            int padding = 15 - countRow;
                            for (int i = 0; i < padding; i++)
                            {
                                DataRow rowPadding = dtData.NewRow();
                                dtData.Rows.Add(rowPadding);
                            }
                            isGetData = true;
                        }
                    }
                    


                    //string strMoney = Library.Common.Number.MoneyToString.ReadMoney(sumMoney);
                    //dtData.Rows[0]["totalMoneyText"] = strMoney;
                }

                crDoc.SetDataSource(dtData);
                if (intIsInputVoucherInKim == 1)
                {
                    cboLien.Visible = true;
                    crDoc.SetParameterValue("TOTALMONEYTEXT", strMoney);
                    if (Lien == 1) crDoc.SetParameterValue("Lien", "1: Lưu Kho");
                    if (Lien == 2) crDoc.SetParameterValue("Lien", "2: Lưu kế toán");
                    if (Lien == 3) crDoc.SetParameterValue("Lien", "3: Nội bộ");
                }
                else
                {
                    cboLien.Visible = false;
                }
                crDoc.SetParameterValue("IsPriceHide", Convert.ToInt32(bolIsPriceHide));
                crDoc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                crDoc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
                crystalReportViewer1.ReportSource = crDoc;
                btnShowInputVoucher.Enabled = false;
                btnShowInputVoucherDetail.Enabled = true;
                return true;
            }
            catch
            {
                return false;
            }
        }
        private DataTable dtDataDetail = null;
        private void btnShowInputVoucherDetail_Click(object sender, EventArgs e)
        {
            this.Text = "In chi tiết phiếu nhập " + strInputVoucherID;
            crystalReportViewer1.ReportSource = null;
            crDoc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            crDoc.Load(DUIInventory_Globals.ReportPath + "\\Input\\rptInputVoucherDetailReport.rpt");
            if (dtDataDetail == null || dtDataDetail.Rows.Count == 0)
            {
                dtDataDetail = new PLC.Input.PLCInputVoucher().Report_GetInputVoucherDetailData(strInputVoucherID);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
            }
            crDoc.SetDataSource(dtDataDetail);
            int intCompanyID = 0;
            if (dtDataDetail.Rows.Count > 0)
                intCompanyID = Convert.ToInt32(dtDataDetail.Rows[0]["CompanyID"]);
            ERP.MasterData.PLC.MD.WSCompany.Company objCompany = new MasterData.PLC.MD.PLCCompany().LoadInfo(intCompanyID);
            crDoc.SetParameterValue("CompanyName", objCompany.CompanyName);
            crDoc.SetParameterValue("Address", objCompany.Address);
            crDoc.SetParameterValue("Website", objCompany.Website);
            crDoc.SetParameterValue("Email", objCompany.Email);
            crDoc.SetParameterValue("Phone", objCompany.Phone);
            crDoc.SetParameterValue("Fax", objCompany.Fax);
            crDoc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
            crDoc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
            crystalReportViewer1.ReportSource = crDoc;
            btnShowInputVoucher.Enabled = true;
            btnShowInputVoucherDetail.Enabled = false;
        }

        private void btnShowInputVoucher_Click(object sender, EventArgs e)
        {
            if (!GetInputVoucherData(1))
            {
                MessageBox.Show(this, "Lỗi xem phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public int GetNumOfLines(string multiPageString, int wrapWidth, Font fnt)
        {
            StringFormat sfFmt = new StringFormat(StringFormatFlags.LineLimit);
            using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
            {
                int iHeight = (int)(Math.Ceiling(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height));
                int iOneLineHeight = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Height;
                int oneChar = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Width;
                int oneLine = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Width;
                int oneLineHeight = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height;
                //var vaRound = Math.Ceiling((double)(iHeight / iOneLineHeight));
                //return int.Parse(vaRound.ToString());
                //return (int)(iHeight / iOneLineHeight);

                return (int)(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height / g.MeasureString("W", fnt, wrapWidth, sfFmt).Height);
                //return iHeight;
            }
        }

        private void cboLien_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboLien.SelectedIndex == 0)
            {
                GetInputVoucherData(1);
            }
            if (cboLien.SelectedIndex == 1)
            {
                GetInputVoucherData(2);
            }
            if (cboLien.SelectedIndex == 2)
            {
                GetInputVoucherData(3);
            }
        }

        private void frmReportView_Shown(object sender, EventArgs e)
        {
            this.BringToFront();
            this.TopMost = true;
        }
    }
}
