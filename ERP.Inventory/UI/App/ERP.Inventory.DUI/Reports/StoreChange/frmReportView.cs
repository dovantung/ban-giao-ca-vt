﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.Reports.StoreChange
{
    public partial class frmReportView : Form
    {
        public frmReportView()
        {
            InitializeComponent();
            intIsOutputVoucherTransferInKim = 0;
            intIsStoreChangePrintInKim = 0;
        }
        private string strInputVoucherID = string.Empty;
        private string strStoreChangeOrderID = string.Empty;
        private string strStoreChangeID = string.Empty;
        private DataTable dtbSource = new DataTable();

        //public string 
        private CrystalDecisions.CrystalReports.Engine.ReportDocument crDoc = null;
        public int intIsInputVoucherReportVAT = 0;
        public bool bolIsPriceHide = true;
        public int intIsOutputVoucherTransferInKim = 0;
        public int intIsStoreChangePrintInKim = 0;
        public bool bolIsPrintStoreChangeHandovers = false;
        public bool isLoadForm = false;
        public string StoreChangeOrderID
        {
            get { return strStoreChangeOrderID; }
            set { strStoreChangeOrderID = value; }
        }

        public string StoreChangeID
        {
            get { return strStoreChangeID; }
            set { strStoreChangeID = value; }
        }

        public DataTable DataTableSouce
        {
            get { return dtbSource; }
            set { dtbSource = value; }
        }



        private void frmReportView_Load(object sender, EventArgs e)
        {
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            isLoadForm = true;
            cboLien.SelectedIndex = 0;
            if (intIsStoreChangePrintInKim == 1)
            {
                if (!GetStoreChangePrintData(1))
                {
                    MessageBox.Show(this, "Lỗi xem phiếu xuất (mẫu in kim)!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else if (intIsOutputVoucherTransferInKim == 1)
            {
                cboLien.Items.Add("Liên 4");
                if (!GetOutputVouchertranferData(1))
                {
                    MessageBox.Show(this, "Lỗi xem phiếu xuất (mẫu in kim)!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else if (bolIsPrintStoreChangeHandovers)
            {
                if (!GetStoreChangeHandoversPrint())
                {
                    MessageBox.Show(this, "Lỗi xem biên bản bàn giao!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                if (!GetInputVoucherData())
                {
                    MessageBox.Show(this, "Lỗi xem phiếu xuất!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            isLoadForm = false;
            //intIsStoreChangePrintInKim = 0;
        }

        private bool GetStoreChangeHandoversPrint()
        {
            try
            {
                btnShowInputVoucher.Visible = false;
                btnShowInputVoucherDetail.Visible = false;
                cboLien.Visible = false;

                this.Text = "In biên bản bàn giao " + strStoreChangeID;
                crystalReportViewer1.ReportSource = null;
                String strPathReport = "\\StoreChange\\rptProductOrderRecords.rpt";

                if (crDoc != null)
                {
                    crDoc.Close();
                    crDoc.Dispose();
                }
                crDoc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                crDoc.Load(DUIInventory_Globals.ReportPath + strPathReport);

                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                crDoc.SetDataSource(DataTableSouce);

                //Lấy info
                object[] objKeywords = new object[] { "@STORECHANGEID", strStoreChangeID };
                DataTable dtbInfo = new ERP.Report.PLC.PLCReportDataSource().GetHeavyDataSource("RPT_STORECHANGE_HANDOVERS_INFO", objKeywords);
                if (dtbInfo != null && dtbInfo.Rows.Count > 0)
                {
                    crDoc.SetParameterValue("COMPANYNAME", SystemConfig.DefaultCompany.CompanyName);
                    crDoc.SetParameterValue("COMPANYADDRESS", SystemConfig.DefaultCompany.Address);
                    crDoc.SetParameterValue("ORDERREASON", dtbInfo.Rows[0]["CONTENT"].ToString().Trim());
                    crDoc.SetParameterValue("STORENAME", dtbInfo.Rows[0]["STORENAME"].ToString().Trim());
                    crDoc.SetParameterValue("STOREADDRESS", dtbInfo.Rows[0]["STOREADDRESS"].ToString().Trim());
                    crDoc.SetParameterValue("PRINTDATE", Convert.ToDateTime(dtbInfo.Rows[0]["STORECHANGEDATE"]));
                    crDoc.SetParameterValue("TOUSER", dtbInfo.Rows[0]["FULLNAME"].ToString().Trim());
                    crDoc.SetParameterValue("CODENUMBER", dtbInfo.Rows[0]["CODE"].ToString().Trim());
                    crDoc.SetParameterValue("V_STORECHANGEORDERID", strStoreChangeID);
                }
                else
                {
                    crDoc.SetParameterValue("COMPANYNAME", SystemConfig.DefaultCompany.CompanyName);
                    crDoc.SetParameterValue("COMPANYADDRESS", SystemConfig.DefaultCompany.Address);
                    crDoc.SetParameterValue("ORDERREASON", "");
                    crDoc.SetParameterValue("STORENAME", "");
                    crDoc.SetParameterValue("STOREADDRESS", "");
                    crDoc.SetParameterValue("PRINTDATE", DateTime.Now);
                    crDoc.SetParameterValue("TOUSER", "");
                    crDoc.SetParameterValue("CODENUMBER", "");
                    crDoc.SetParameterValue("V_STORECHANGEORDERID", "");
                }

                crDoc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                crDoc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
                crystalReportViewer1.ReportSource = crDoc;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private bool GetStoreChangePrintData(int Lien)
        {
            try
            {
                btnShowInputVoucher.Visible = false;
                btnShowInputVoucherDetail.Visible = false;
                this.Text = "In phiếu xuất kho " + strStoreChangeID;
                crystalReportViewer1.ReportSource = null;
                string strPathReport = "\\StoreChange\\rptStoreChangePrint.rpt";
                if (crDoc != null)
                {
                    crDoc.Close();
                    crDoc.Dispose();
                }
                crDoc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                crDoc.Load(DUIInventory_Globals.ReportPath + strPathReport);
                //Lấy dữ liệu
                //DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(strStoreChangeID);
                //Lấy thông tin
                DataTable dtInfo = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetInfo(strStoreChangeID);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }

                crDoc.SetDataSource(DataTableSouce);
                DateTime _datetime = Convert.ToDateTime(dtInfo.Rows[0]["STORECHANGEDATE"].ToString());
                if (dtInfo.Rows.Count > 0 && dtInfo != null)
                {

                    crDoc.SetParameterValue("DD", _datetime.ToString("dd"));
                    crDoc.SetParameterValue("MM", _datetime.ToString("MM"));
                    crDoc.SetParameterValue("YY", _datetime.ToString("yyyy").Substring(2, 2).ToString());
                    crDoc.SetParameterValue("TOUSER", dtInfo.Rows[0]["FULLNAME"].ToString());
                    crDoc.SetParameterValue("ADDRESS", dtInfo.Rows[0]["TOSTOREADDRESS"].ToString());
                    crDoc.SetParameterValue("CONTENT", dtInfo.Rows[0]["CONTENT"].ToString());
                    crDoc.SetParameterValue("STORENAME", dtInfo.Rows[0]["FROMSTORENAME"].ToString());

                    crDoc.SetParameterValue("STOREADDRESS", dtInfo.Rows[0]["FROMSTOREADDRESS"].ToString());
                    crDoc.SetParameterValue("OUTPUTVOUCHERID", dtInfo.Rows[0]["OUTPUTVOUCHERID"].ToString());
                    crDoc.SetParameterValue("COMPANYNAME", SystemConfig.DefaultCompany.CompanyName);
                    crDoc.SetParameterValue("COMPANYADDRESS", SystemConfig.DefaultCompany.Address);
                    crDoc.SetParameterValue("COMPANYFAX", SystemConfig.DefaultCompany.Fax);
                    crDoc.SetParameterValue("COMPANYPHONE", SystemConfig.DefaultCompany.Fax);

                }
                else
                {
                    crDoc.SetParameterValue("DD", string.Empty);
                    crDoc.SetParameterValue("MM", string.Empty);
                    crDoc.SetParameterValue("YY", string.Empty);
                    crDoc.SetParameterValue("TOUSER", string.Empty);
                    crDoc.SetParameterValue("ADDRESS", string.Empty);
                    crDoc.SetParameterValue("CONTENT", string.Empty);
                    crDoc.SetParameterValue("STORENAME", string.Empty);
                    crDoc.SetParameterValue("STOREADDRESS", string.Empty);
                    crDoc.SetParameterValue("OUTPUTVOUCHERID", string.Empty);
                    crDoc.SetParameterValue("COMPANYNAME", string.Empty);
                    crDoc.SetParameterValue("COMPANYADDRESS", string.Empty);
                    crDoc.SetParameterValue("COMPANYFAX", string.Empty);
                    crDoc.SetParameterValue("COMPANYPHONE", string.Empty);

                }
                switch (Lien)
                {
                    case 1:
                        crDoc.SetParameterValue("Lien", "1: Lưu Kho");
                        break;
                    case 2:
                        crDoc.SetParameterValue("Lien", "2: Lưu kế toán");
                        break;
                    case 3:
                        crDoc.SetParameterValue("Lien", "3: Nội bộ");
                        break;
                    default:
                        break;
                }
                crDoc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                crDoc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
                crystalReportViewer1.ReportSource = crDoc;
                btnShowInputVoucher.Enabled = false;
                btnShowInputVoucherDetail.Enabled = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool GetOutputVouchertranferData(int Lien)
        {
            try
            {
                this.Text = "In phiếu xuất kho " + strStoreChangeOrderID;
                crystalReportViewer1.ReportSource = null;
                // crystalReportViewer1.Refresh();
                String strPathReport = "\\StoreChange\\rptOutputVoucherTranfer.rpt";



                if (crDoc != null)
                {
                    crDoc.Close();
                    crDoc.Dispose();
                }
                crDoc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                crDoc.Load(DUIInventory_Globals.ReportPath + strPathReport);

                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }



                crDoc.SetDataSource(DataTableSouce);
                DataTable dtInfo = new PLC.StoreChange.PLCStoreChange().Report_GetOutputVoucherTransferData(strStoreChangeID);
                DateTime _datetime = Convert.ToDateTime(dtInfo.Rows[0]["STORECHANGEDATE"].ToString());
                DateTime _OutputDate = Convert.ToDateTime(dtInfo.Rows[0]["OUTPUTDATE"].ToString());
                if (dtInfo != null && dtInfo.Rows.Count > 0)
                {
                    crDoc.SetParameterValue("DD", _datetime.ToString("dd"));
                    crDoc.SetParameterValue("MM", _datetime.ToString("MM"));
                    crDoc.SetParameterValue("YY", _datetime.ToString("yyyy").Substring(2, 2).ToString());

                    crDoc.SetParameterValue("COMPANYNAME", SystemConfig.DefaultCompany.CompanyName);
                    crDoc.SetParameterValue("COMPANYADDRESS", SystemConfig.DefaultCompany.Address);
                    crDoc.SetParameterValue("COMPANYTAX", SystemConfig.DefaultCompany.TaxID);
                    crDoc.SetParameterValue("TOSTORE", dtInfo.Rows[0]["TOSTORE"].ToString());
                    crDoc.SetParameterValue("FROMSTORE", dtInfo.Rows[0]["FROMSTORE"].ToString());
                    crDoc.SetParameterValue("TransferedUserFullName", dtInfo.Rows[0]["TransferedUserFullName"].ToString());
                    crDoc.SetParameterValue("TransportTypeName", dtInfo.Rows[0]["TransportTypeName"].ToString());
                    crDoc.SetParameterValue("INVOICEID", dtInfo.Rows[0]["INVOICEID"].ToString());
                    crDoc.SetParameterValue("STORECHANGETYPENAME", dtInfo.Rows[0]["STORECHANGETYPENAME"].ToString());
                    crDoc.SetParameterValue("STORECHANGEDATETEXT", dtInfo.Rows[0]["STORECHANGEDATETEXT"].ToString());
                    crDoc.SetParameterValue("TransferedDate", dtInfo.Rows[0]["TransferedDate"].ToString());
                    crDoc.SetParameterValue("StoreChangeUserFullName", dtInfo.Rows[0]["StoreChangeUserFullName"].ToString());
                    crDoc.SetParameterValue("BRANCHNAME", dtInfo.Rows[0]["BRANCHNAME"].ToString());
                    crDoc.SetParameterValue("BRANCHADDRESS", dtInfo.Rows[0]["BRANCHADDRESS"].ToString());
                    crDoc.SetParameterValue("BRANCHTAX", dtInfo.Rows[0]["BRANCHTAX"].ToString());
                    crDoc.SetParameterValue("ORDERID", dtInfo.Rows[0]["ORDERID"].ToString());

                    crDoc.SetParameterValue("OutDD", _OutputDate.ToString("dd"));
                    crDoc.SetParameterValue("OutMM", _OutputDate.ToString("MM"));
                    crDoc.SetParameterValue("OutYY", _OutputDate.ToString("yyyy"));

                }
                else
                {
                    crDoc.SetParameterValue("DD", _datetime.ToString("dd"));
                    crDoc.SetParameterValue("MM", _datetime.ToString("MM"));
                    crDoc.SetParameterValue("YY", _datetime.ToString("yyyy").Substring(2, 2).ToString());
                    crDoc.SetParameterValue("COMPANYNAME", string.Empty);
                    crDoc.SetParameterValue("COMPANYADDRESS", string.Empty);
                    crDoc.SetParameterValue("COMPANYTAX", string.Empty);
                    crDoc.SetParameterValue("TOSTORE", string.Empty);
                    crDoc.SetParameterValue("FROMSTORE", string.Empty);
                    crDoc.SetParameterValue("TransferedUserFullName", string.Empty);
                    crDoc.SetParameterValue("TransportTypeName", string.Empty);
                    crDoc.SetParameterValue("INVOICEID", string.Empty);
                    crDoc.SetParameterValue("STORECHANGETYPENAME", string.Empty);
                    crDoc.SetParameterValue("STORECHANGEDATETEXT", string.Empty);
                    crDoc.SetParameterValue("TransferedDate", string.Empty);
                    crDoc.SetParameterValue("StoreChangeUserFullName", string.Empty);
                    crDoc.SetParameterValue("BRANCHNAME", string.Empty);
                    crDoc.SetParameterValue("BRANCHADDRESS", string.Empty);
                    crDoc.SetParameterValue("BRANCHTAX", string.Empty);
                    crDoc.SetParameterValue("ORDERID", string.Empty);
                    crDoc.SetParameterValue("OutDD", string.Empty);
                    crDoc.SetParameterValue("OutMM", string.Empty);
                    crDoc.SetParameterValue("OutYY", string.Empty);
                }
                switch (Lien)
                {
                    case 1:
                        crDoc.SetParameterValue("Lien", "1: Lưu Kho");
                        break;
                    case 2:
                        crDoc.SetParameterValue("Lien", "2: Dùng để vận chuyển hàng");
                        break;
                    case 3:
                        crDoc.SetParameterValue("Lien", "3: Lưu kế toán");
                        break;
                    case 4:
                        crDoc.SetParameterValue("Lien", "4: Nội bộ");
                        break;
                    default:
                        break;
                }
                //if (Lien == 1) crDoc.SetParameterValue("Lien", "1: Lưu Kho");
                //if (Lien == 2) crDoc.SetParameterValue("Lien", "2: Dùng để vận chuyển hàng");
                //if (Lien == 3) crDoc.SetParameterValue("Lien", "3: Lưu kế toán");
                //if (Lien == 4) crDoc.SetParameterValue("Lien", "4: Nội bộ");

                //crDoc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.;
                crDoc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
                System.Drawing.Printing.PaperSize oPaperSize = new System.Drawing.Printing.PaperSize();
                crDoc.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;
                oPaperSize.RawKind = (int)System.Drawing.Printing.PaperKind.Custom;
                oPaperSize.Height = 210;
                oPaperSize.Width = 277;
                crDoc.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)oPaperSize.Kind;
                crDoc.PrintToPrinter(1, true, 0, 0);
                crystalReportViewer1.ReportSource = crDoc;
                btnShowInputVoucher.Enabled = false;
                btnShowInputVoucherDetail.Enabled = true;
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool GetInputVoucherData()
        {
            try
            {
                this.Text = "In phiếu xuất kho " + strStoreChangeOrderID;
                crystalReportViewer1.ReportSource = null;
                String strPathReport = "\\StoreChange\\rptOutputVouchertranfer.rpt";

                if (intIsInputVoucherReportVAT == 1)
                    strPathReport = "\\Input\\rptInputVoucherReportVAT.rpt";
                if (intIsOutputVoucherTransferInKim == 1)
                {
                    strPathReport = "\\StoreChange\\rptOutputVouchertranfer.rpt";
                }
                if (intIsStoreChangePrintInKim == 1)
                {

                }
                if (crDoc != null)
                {
                    crDoc.Close();
                    crDoc.Dispose();
                }
                crDoc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                crDoc.Load(DUIInventory_Globals.ReportPath + strPathReport);
                DataTable dtData = new PLC.Input.PLCInputVoucher().Report_GetInputVoucherData(strStoreChangeOrderID);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }

                if (intIsOutputVoucherTransferInKim == 1)
                {
                    int countRow = 0;
                    double sumMoney = 0;
                    for (int i = 0; i < dtData.Rows.Count; i++)
                    {
                        //string strInputDate = dtData.Rows[i]["INPUTDATE"].ToString();
                        string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                        int price = Convert.ToInt32(dtData.Rows[i]["PRICE"].ToString().Trim());
                        int quantity = Convert.ToInt32(dtData.Rows[i]["QUANTITY"].ToString().Trim());
                        Font font = new Font("Arial", 9.0f);
                        int rowProductName = GetNumOfLines(strProductName, 160, new Font(font, FontStyle.Bold));
                        countRow += rowProductName;
                        sumMoney = sumMoney + (price * quantity);
                        if (countRow > 15)
                        {
                            MessageBox.Show(this, "Vui lòng nhập ít sản phẩm lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                        string checkReal = dtData.Rows[i]["ISCHECKREALINPUT"].ToString().Trim();
                        if (checkReal == "0")
                        {
                            dtData.Rows[i]["QUANTITYCHECKREAL"] = "";
                        }

                    }
                    string strMoney = Library.Common.Number.MoneyToString.ReadMoney(sumMoney);

                    for (int i = 0; i < dtData.Rows.Count; i++)
                    {
                        // dtData.Rows[i]["STT"] = i + 1;




                        dtData.Rows[i]["totalMoneyText"] = strMoney;
                        //dtData.Rows[i]["storename"] = "haha";
                    }

                    if (countRow < 15)
                    {
                        int padding = 15 - countRow;
                        for (int i = 0; i < padding; i++)
                        {
                            DataRow rowPadding = dtData.NewRow();
                            dtData.Rows.Add(rowPadding);
                        }

                    }

                    //crDoc.SetParameterValue("TOTALMONEYTEXT", strMoney);
                    //string strMoney = Library.Common.Number.MoneyToString.ReadMoney(sumMoney);
                    //dtData.Rows[0]["totalMoneyText"] = strMoney;
                }

                crDoc.SetDataSource(dtData);
                crDoc.SetParameterValue("IsPriceHide", Convert.ToInt32(bolIsPriceHide));
                crDoc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                crDoc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
                crystalReportViewer1.ReportSource = crDoc;
                btnShowInputVoucher.Enabled = false;
                btnShowInputVoucherDetail.Enabled = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void btnShowInputVoucherDetail_Click(object sender, EventArgs e)
        {
            this.Text = "In chi tiết phiếu nhập " + strInputVoucherID;
            crystalReportViewer1.ReportSource = null;
            crDoc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            crDoc.Load(DUIInventory_Globals.ReportPath + "\\Input\\rptInputVoucherDetailReport.rpt");
            DataTable dtData = new PLC.Input.PLCInputVoucher().Report_GetInputVoucherDetailData(strInputVoucherID);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            crDoc.SetDataSource(dtData);
            int intCompanyID = 0;
            if (dtData.Rows.Count > 0)
                intCompanyID = Convert.ToInt32(dtData.Rows[0]["CompanyID"]);
            ERP.MasterData.PLC.MD.WSCompany.Company objCompany = new MasterData.PLC.MD.PLCCompany().LoadInfo(intCompanyID);
            crDoc.SetParameterValue("CompanyName", objCompany.CompanyName);
            crDoc.SetParameterValue("Address", objCompany.Address);
            crDoc.SetParameterValue("Website", objCompany.Website);
            crDoc.SetParameterValue("Email", objCompany.Email);
            crDoc.SetParameterValue("Phone", objCompany.Phone);
            crDoc.SetParameterValue("Fax", objCompany.Fax);
            crDoc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
            crDoc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
            crystalReportViewer1.ReportSource = crDoc;
            btnShowInputVoucher.Enabled = true;
            btnShowInputVoucherDetail.Enabled = false;
        }

        private void btnShowInputVoucher_Click(object sender, EventArgs e)
        {
            if (!GetInputVoucherData())
            {
                MessageBox.Show(this, "Lỗi xem phiếu xuất!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public int GetNumOfLines(string multiPageString, int wrapWidth, Font fnt)
        {
            StringFormat sfFmt = new StringFormat(StringFormatFlags.LineLimit);
            using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
            {
                int iHeight = (int)(Math.Ceiling(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height));
                int iOneLineHeight = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Height;
                int oneChar = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Width;
                int oneLine = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Width;
                int oneLineHeight = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height;
                return (int)(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height / g.MeasureString("W", fnt, wrapWidth, sfFmt).Height);
            }
        }

        private void frmReportView_FormClosing(object sender, FormClosingEventArgs e)
        {
            //intIsStoreChangePrintInKim = 0;
            StoreChangeID = string.Empty;
            DataTableSouce.Clear();
        }

        private void cboLien_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoadForm) return;
            int Lien = 0;
            int indexCbo = cboLien.SelectedIndex;
            switch (indexCbo)
            {
                case 0:
                    Lien = 1;
                    break;
                case 1:
                    Lien = 2;
                    break;
                case 2:
                    Lien = 3;
                    break;
                case 3:
                    Lien = 4;
                    break;
                default:
                    break;
            }
            if (intIsOutputVoucherTransferInKim == 1) GetOutputVouchertranferData(Lien);
            else if (intIsStoreChangePrintInKim == 1) GetStoreChangePrintData(Lien);
        }
    }
}
