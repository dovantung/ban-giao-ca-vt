﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Reports.ProductChange
{
    public partial class frmReportProductChangeOrder : Form
    {
        private object[] objKeywords = null;
        ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        #region Properties
        public object[] Keywords
        {
            get { return objKeywords; }
            set { objKeywords = value; }
        }
        #endregion

        public frmReportProductChangeOrder()
        {
            InitializeComponent();
        }

        private void frmReportProductChangeOrder_Load(object sender, EventArgs e)
        {
            //this.Size = new Size(800, 600);
            //this.StartPosition = FormStartPosition.CenterParent;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.WindowState = FormWindowState.Maximized;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            LoadData();
        }
        private void LoadData()
        {
            DataTable dtbData = null;
            if (objKeywords != null)
            {
                dtbData=objPLCReportDataSource.GetDataSource("RPT_PRODUCTCHANGEORDER", objKeywords);
            }
            if (dtbData != null && dtbData.Rows.Count > 0)
            {
                grdData.DataSource = dtbData;

            }
            else
            {
                dtbData = new DataTable();
                grdData.DataSource = dtbData;
            }
            grdData.RefreshDataSource();
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }
    }
}
