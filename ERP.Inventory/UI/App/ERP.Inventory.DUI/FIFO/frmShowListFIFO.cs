﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.FIFO
{
    public partial class frmShowListFIFO : Form
    {
        public string Content { get; set; }
        public DataTable dtbFIFO { get; set; }
        private bool bolIsShowPriceNoneVAT = false;
        private bool bolIsShowOK = false;

        public bool IsShowOK
        {
            get { return bolIsShowOK; }
            set { bolIsShowOK = value; }
        }

        public bool IsShowPriceNoneVAT
        {
            get { return bolIsShowPriceNoneVAT; }
            set { bolIsShowPriceNoneVAT = value; }
        }

        private bool bolIsShowPricHaveVAT = false;
        public bool IsShowPriceHaveVAT
        {
            get { return bolIsShowPricHaveVAT; }
            set { bolIsShowPricHaveVAT = value; }
        }

        public frmShowListFIFO()
        {
            InitializeComponent();
        }

        private void frmShowListFIFO_Load(object sender, EventArgs e)
        {
            lblContent.Text = Content;
            if (dtbFIFO.Rows.Count == 0)
                linkLblFIFO.Visible = false;
            flexUnEvent.DataSource = dtbFIFO;
            btnOK.Visible = bolIsShowOK;
            CustomFlex();
        }

        #region Fomart Grid C1
        private void CustomFlex()
        {
            //Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowMerging(flexUnEvent, "IMEI,FIRSTINPUTDATE,SALEPRICE,SALEPRICE_HAVEVAT", true);
            if (bolIsShowPriceNoneVAT)
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexUnEvent, false, "PRODUCTID,PRODUCTNAME,SALEPRICE_HAVEVAT");
            if (bolIsShowPricHaveVAT)
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexUnEvent, false, "PRODUCTID,PRODUCTNAME,SALEPRICE");
            if (bolIsShowPriceNoneVAT && bolIsShowPricHaveVAT)
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexUnEvent, false, "PRODUCTID,PRODUCTNAME");
            if (!bolIsShowPriceNoneVAT && !bolIsShowPricHaveVAT)
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexUnEvent, false, "PRODUCTID,PRODUCTNAME,SALEPRICE,SALEPRICE_HAVEVAT");

            flexUnEvent.Cols["IMEI"].Caption = "IMEI";
            flexUnEvent.Cols["FIRSTINPUTDATE"].Caption = "Ngày nhập";
            flexUnEvent.Cols["SALEPRICE"].Caption = "Giá Bán (Không VAT)";
            flexUnEvent.Cols["SALEPRICE_HAVEVAT"].Caption = "Giá bán (Có VAT)";

            flexUnEvent.Cols["IMEI"].Width = 200;
            flexUnEvent.Cols["FIRSTINPUTDATE"].Width = 120;
            flexUnEvent.Cols["SALEPRICE"].Width = 100;
            flexUnEvent.Cols["SALEPRICE_HAVEVAT"].Width = 100;

            flexUnEvent.Cols["FIRSTINPUTDATE"].Format = "dd/MM/yyyy HH:mm";
            flexUnEvent.Cols["SALEPRICE"].Format = "##0";
            flexUnEvent.Cols["SALEPRICE_HAVEVAT"].Format = "##0";
            for (int i = 1; i < flexUnEvent.Cols.Count; i++)
            {
                flexUnEvent.Cols[i].AllowEditing = false;
            }
            C1.Win.C1FlexGrid.CellStyle style = flexUnEvent.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexUnEvent.GetCellRange(0, 0, 0, flexUnEvent.Cols.Count - 1);
            range.Style = style;
            //flexUnEvent.Rows[0].Height = 50;
            
            //flexUnEvent.Rows[1].Height = 50;

            //   Library.AppCore.LoadControls.C1FlexGridObject.AddFormSearchProduct(flexUnEvent, flexUnEvent.Cols["IMEI"].Index);

        }
        #endregion



        bool ShowList = false;
        private void linkLblFIFO_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!ShowList)
            {
                this.Height = 240;
                ShowList = true;
            }
            else
            {
                this.Height = 130;
                ShowList = false;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }
    }
}
