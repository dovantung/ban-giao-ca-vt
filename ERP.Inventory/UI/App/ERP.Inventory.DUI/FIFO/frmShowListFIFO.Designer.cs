﻿namespace ERP.Inventory.DUI.FIFO
{
    partial class frmShowListFIFO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmShowListFIFO));
            this.lblContent = new System.Windows.Forms.Label();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.linkLblFIFO = new System.Windows.Forms.LinkLabel();
            this.flexUnEvent = new C1.Win.C1FlexGrid.C1FlexGrid();
            ((System.ComponentModel.ISupportInitialize)(this.flexUnEvent)).BeginInit();
            this.SuspendLayout();
            // 
            // lblContent
            // 
            this.lblContent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.lblContent.Location = new System.Drawing.Point(13, 9);
            this.lblContent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblContent.Name = "lblContent";
            this.lblContent.Size = new System.Drawing.Size(374, 46);
            this.lblContent.TabIndex = 23;
            this.lblContent.Text = "label1";
            // 
            // btnClose
            // 
            this.btnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Image = global::ERP.Inventory.DUI.Properties.Resources.close;
            this.btnClose.Location = new System.Drawing.Point(301, 58);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(86, 23);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Appearance.Options.UseFont = true;
            this.btnOK.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnOK.Location = new System.Drawing.Point(209, 58);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(86, 23);
            this.btnOK.TabIndex = 24;
            this.btnOK.Text = "Đồng ý";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // linkLblFIFO
            // 
            this.linkLblFIFO.AutoSize = true;
            this.linkLblFIFO.Location = new System.Drawing.Point(17, 65);
            this.linkLblFIFO.Name = "linkLblFIFO";
            this.linkLblFIFO.Size = new System.Drawing.Size(136, 16);
            this.linkLblFIFO.TabIndex = 26;
            this.linkLblFIFO.TabStop = true;
            this.linkLblFIFO.Text = "Danh sách gợi ý FIFO";
            this.linkLblFIFO.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLblFIFO_LinkClicked);
            // 
            // flexUnEvent
            // 
            this.flexUnEvent.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexUnEvent.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexUnEvent.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexUnEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexUnEvent.AutoClipboard = true;
            this.flexUnEvent.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexUnEvent.Location = new System.Drawing.Point(12, 93);
            this.flexUnEvent.Name = "flexUnEvent";
            this.flexUnEvent.Rows.DefaultSize = 21;
            this.flexUnEvent.Size = new System.Drawing.Size(375, 0);
            this.flexUnEvent.StyleInfo = resources.GetString("flexUnEvent.StyleInfo");
            this.flexUnEvent.TabIndex = 27;
            this.flexUnEvent.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            // 
            // frmShowListFIFO
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(399, 90);
            this.Controls.Add(this.flexUnEvent);
            this.Controls.Add(this.linkLblFIFO);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblContent);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmShowListFIFO";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông báo FIFO";
            this.Load += new System.EventHandler(this.frmShowListFIFO_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flexUnEvent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblContent;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private System.Windows.Forms.LinkLabel linkLblFIFO;
        private C1.Win.C1FlexGrid.C1FlexGrid flexUnEvent;
    }
}