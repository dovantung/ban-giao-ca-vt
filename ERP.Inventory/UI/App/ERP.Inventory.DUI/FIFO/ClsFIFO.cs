﻿using Library.AppCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.FIFO
{
    public static class ClsFIFO
    {
        public static string Get_DataFIFO(ref DataTable dtbFIFO, string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            dtbFIFO = new PLC.PLCProductInStock().GetFIFOList_ByProductID(strProductID, strIMEI, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                dtbFIFO = new DataTable();
                dtbFIFO.Columns.Add("PRODUCTID", typeof(string));
                dtbFIFO.Columns.Add("PRODUCTNAME", typeof(string));
                dtbFIFO.Columns.Add("IMEI", typeof(string));
                dtbFIFO.Columns.Add("FIRSTINPUTDATE", typeof(DateTime));
                dtbFIFO.Columns.Add("SALEPRICE", typeof(decimal));
                dtbFIFO.Columns.Add("SALEPRICE_HAVEVAT", typeof(decimal));
                return SystemConfig.objSessionUser.ResultMessageApp.MessageDetail;
            }
            if (dtbFIFO == null)
            {
                dtbFIFO = new DataTable();
                dtbFIFO.Columns.Add("PRODUCTID", typeof(string));
                dtbFIFO.Columns.Add("PRODUCTNAME", typeof(string));
                dtbFIFO.Columns.Add("IMEI", typeof(string));
                dtbFIFO.Columns.Add("FIRSTINPUTDATE", typeof(DateTime));
                dtbFIFO.Columns.Add("SALEPRICE", typeof(decimal));
                dtbFIFO.Columns.Add("SALEPRICE_HAVEVAT", typeof(decimal));
            }
            return string.Empty;
        }

        public static string Get_DataFIFO_ByIMEI(ref DataTable dtbFIFO, DataTable dtbIMEI, string strProductID, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            dtbFIFO = new PLC.PLCProductInStock().GetFIFOList_ByIMEI(dtbIMEI, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                dtbFIFO = new DataTable();
                dtbFIFO.Columns.Add("PRODUCTID", typeof(string));
                dtbFIFO.Columns.Add("PRODUCTNAME", typeof(string));
                dtbFIFO.Columns.Add("IMEI", typeof(string));
                dtbFIFO.Columns.Add("FIRSTINPUTDATE", typeof(DateTime));
                dtbFIFO.Columns.Add("SALEPRICE", typeof(decimal));
                dtbFIFO.Columns.Add("SALEPRICE_HAVEVAT", typeof(decimal));
                return SystemConfig.objSessionUser.ResultMessageApp.MessageDetail;
            }
            if (dtbFIFO == null)
            {
                dtbFIFO = new DataTable();
                dtbFIFO.Columns.Add("PRODUCTID", typeof(string));
                dtbFIFO.Columns.Add("PRODUCTNAME", typeof(string));
                dtbFIFO.Columns.Add("IMEI", typeof(string));
                dtbFIFO.Columns.Add("FIRSTINPUTDATE", typeof(DateTime));
                dtbFIFO.Columns.Add("SALEPRICE", typeof(decimal));
                dtbFIFO.Columns.Add("SALEPRICE_HAVEVAT", typeof(decimal));
            }
            return string.Empty;
        }

        public static string Get_DataFIFO_ByIMEI(ref DataTable dtbFIFO, string xmlIMEI, string strProductID, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            dtbFIFO = new PLC.PLCProductInStock().GetFIFOList_ByIMEI(xmlIMEI, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                dtbFIFO = new DataTable();
                dtbFIFO.Columns.Add("PRODUCTID", typeof(string));
                dtbFIFO.Columns.Add("PRODUCTNAME", typeof(string));
                dtbFIFO.Columns.Add("IMEI", typeof(string));
                dtbFIFO.Columns.Add("FIRSTINPUTDATE", typeof(DateTime));
                dtbFIFO.Columns.Add("SALEPRICE", typeof(decimal));
                dtbFIFO.Columns.Add("SALEPRICE_HAVEVAT", typeof(decimal));
                return SystemConfig.objSessionUser.ResultMessageApp.MessageDetail;
            }
            if (dtbFIFO == null)
            {
                dtbFIFO = new DataTable();
                dtbFIFO.Columns.Add("PRODUCTID", typeof(string));
                dtbFIFO.Columns.Add("PRODUCTNAME", typeof(string));
                dtbFIFO.Columns.Add("IMEI", typeof(string));
                dtbFIFO.Columns.Add("FIRSTINPUTDATE", typeof(DateTime));
                dtbFIFO.Columns.Add("SALEPRICE", typeof(decimal));
                dtbFIFO.Columns.Add("SALEPRICE_HAVEVAT", typeof(decimal));
            }
            return string.Empty;
        }


        public static DialogResult ShowMessengerFIFOList(string strContent,
            string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, bool IsShowPriceNoneVAT, bool IsShowPricHaveVAT, int intMaxIMEICanShow = 4, bool bolIsShowOK = false)
        {
            DataTable dtbFIFO = null;
            string strMessenger = Get_DataFIFO(ref dtbFIFO, strProductID, strIMEI, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            if (string.IsNullOrEmpty(strMessenger))
                strMessenger = strContent;
            frmShowListFIFO frm = new frmShowListFIFO();
            frm.Content = strMessenger;
            frm.dtbFIFO = dtbFIFO;
            frm.IsShowPriceHaveVAT = IsShowPricHaveVAT;
            frm.IsShowPriceNoneVAT = IsShowPriceNoneVAT;
            frm.IsShowOK = bolIsShowOK;
            DialogResult dresult = frm.ShowDialog();
            return dresult;
        }

        public static DialogResult ShowMessengerFIFOList(string strContent,
            string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, bool IsShowPriceNoneVAT, int intMaxIMEICanShow = 4)
        {
            return ShowMessengerFIFOList(strContent, strProductID, strIMEI, intStoreID, intOutputTypeID, IsShowPriceNoneVAT, false, intMaxIMEICanShow);
        }

        public static DialogResult ShowMessengerFIFOList(string strContent,
            string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            return ShowMessengerFIFOList(strContent, strProductID, strIMEI, intStoreID, intOutputTypeID, false, false, intMaxIMEICanShow);
        }


        #region Show IMEI FIFO by List
        public static DialogResult ShowMessengerFIFOList_ByIMEI(DataTable dtbIMEI, string strContent,
            string strProductID, int intStoreID, int intOutputTypeID, bool IsShowPriceNoneVAT, bool IsShowPricHaveVAT, int intMaxIMEICanShow = 4)
        {
            DataTable dtbFIFO = null;
            string strMessenger = Get_DataFIFO_ByIMEI(ref dtbFIFO, dtbIMEI, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            if (string.IsNullOrEmpty(strMessenger))
                strMessenger = strContent;
            frmShowListFIFO frm = new frmShowListFIFO();
            frm.Content = strMessenger;
            frm.dtbFIFO = dtbFIFO;
            frm.IsShowPriceHaveVAT = IsShowPricHaveVAT;
            frm.IsShowPriceNoneVAT = IsShowPriceNoneVAT;
            DialogResult dresult = frm.ShowDialog();
            return dresult;
        }

        public static DialogResult ShowMessengerFIFOList_ByIMEI(DataTable dtbIMEI, string strContent,
            string strProductID, int intStoreID, int intOutputTypeID, bool IsShowPriceNoneVAT, int intMaxIMEICanShow = 4)
        {
            return ShowMessengerFIFOList_ByIMEI(dtbIMEI, strContent, strProductID, intStoreID, intOutputTypeID, IsShowPriceNoneVAT, false, intMaxIMEICanShow);
        }

        public static DialogResult ShowMessengerFIFOList_ByIMEI(DataTable dtbIMEI, string strContent,
            string strProductID, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            return ShowMessengerFIFOList_ByIMEI(dtbIMEI, strContent, strProductID, intStoreID, intOutputTypeID, false, false, intMaxIMEICanShow);
        }


        public static DialogResult ShowMessengerFIFOList_ByIMEI(string xmlIMEI, string strContent,
            string strProductID, int intStoreID, int intOutputTypeID, bool IsShowPriceNoneVAT, bool IsShowPricHaveVAT, int intMaxIMEICanShow = 4)
        {
            DataTable dtbFIFO = null;
            string strMessenger = Get_DataFIFO_ByIMEI(ref dtbFIFO, xmlIMEI, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
            if (string.IsNullOrEmpty(strMessenger))
                strMessenger = strContent;
            frmShowListFIFO frm = new frmShowListFIFO();
            frm.Content = strMessenger;
            frm.dtbFIFO = dtbFIFO;
            frm.IsShowPriceHaveVAT = IsShowPricHaveVAT;
            frm.IsShowPriceNoneVAT = IsShowPriceNoneVAT;
            DialogResult dresult = frm.ShowDialog();
            return dresult;
        }

        public static DialogResult ShowMessengerFIFOList_ByIMEI(string xmlIMEI, string strContent,
            string strProductID, int intStoreID, int intOutputTypeID, bool IsShowPriceNoneVAT, int intMaxIMEICanShow = 4)
        {
            return ShowMessengerFIFOList_ByIMEI(xmlIMEI, strContent, strProductID, intStoreID, intOutputTypeID, IsShowPriceNoneVAT, false, intMaxIMEICanShow);
        }

        public static DialogResult ShowMessengerFIFOList_ByIMEI(string xmlIMEI, string strContent,
            string strProductID, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            return ShowMessengerFIFOList_ByIMEI(xmlIMEI, strContent, strProductID, intStoreID, intOutputTypeID, false, false, intMaxIMEICanShow);
        }
        #endregion
    }
}
