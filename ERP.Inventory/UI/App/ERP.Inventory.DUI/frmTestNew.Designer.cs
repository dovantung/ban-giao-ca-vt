﻿namespace ERP.Inventory.DUI
{
    partial class frmTestNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.chứcNăngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tạoDanhMụcChiaHàngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.chiaHàngSerialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.điềuChuyểnNgangHàngSerialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phụKiệnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhuCầuHàngPhụKiệnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chiaHàngPhụKiệnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chứcNăngToolStripMenuItem,
            this.serialToolStripMenuItem,
            this.phụKiệnToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(560, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // chứcNăngToolStripMenuItem
            // 
            this.chứcNăngToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem,
            this.quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem,
            this.quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem,
            this.tạoDanhMụcChiaHàngToolStripMenuItem,});
            this.chứcNăngToolStripMenuItem.Name = "chứcNăngToolStripMenuItem";
            this.chứcNăngToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.chứcNăngToolStripMenuItem.Text = "Chức năng";
            // 
            // quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem
            // 
            this.quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem.Name = "quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem";
            this.quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem.Size = new System.Drawing.Size(351, 22);
            this.quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem.Text = "8.\tQuản lý các hàng trải nghiệm của bàn trải nghiệm ";
            this.quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem.Click += new System.EventHandler(this.quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem_Click);
            // 
            // quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem
            // 
            this.quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem.Name = "quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem";
            this.quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem.Size = new System.Drawing.Size(351, 22);
            this.quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem.Text = "9.\tQuản lý danh sách tham số vòng quay chia hàng";
            this.quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem.Click += new System.EventHandler(this.quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem_Click);
            // 
            // quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem
            // 
            this.quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem.Name = "quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem";
            this.quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem.Size = new System.Drawing.Size(351, 22);
            this.quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem.Text = "10.\tQuản lý danh sách định mức trưng bày ";
            this.quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem.Click += new System.EventHandler(this.quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem_Click);
            // 
            // serialToolStripMenuItem
            // 
            this.serialToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.chiaHàngSerialToolStripMenuItem,
            this.điềuChuyểnNgangHàngSerialToolStripMenuItem});
            this.serialToolStripMenuItem.Name = "serialToolStripMenuItem";
            this.serialToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.serialToolStripMenuItem.Text = "Hàng Serial";
            // 
            // tạoDanhMụcChiaHàngToolStripMenuItem
            // 
            this.tạoDanhMụcChiaHàngToolStripMenuItem.Name = "tạoDanhMụcChiaHàngToolStripMenuItem";
            this.tạoDanhMụcChiaHàngToolStripMenuItem.Size = new System.Drawing.Size(293, 22);
            this.tạoDanhMụcChiaHàngToolStripMenuItem.Text = "11.Tạo danh mục chia hàng";
            this.tạoDanhMụcChiaHàngToolStripMenuItem.Click += new System.EventHandler(this.tạoDanhMụcChiaHàngToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(293, 22);
            this.toolStripMenuItem1.Text = "12.Quản lý danh sách nhu cầu hàng serial";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // chiaHàngSerialToolStripMenuItem
            // 
            this.chiaHàngSerialToolStripMenuItem.Name = "chiaHàngSerialToolStripMenuItem";
            this.chiaHàngSerialToolStripMenuItem.Size = new System.Drawing.Size(293, 22);
            this.chiaHàngSerialToolStripMenuItem.Text = "13.Chia hàng serial";
            this.chiaHàngSerialToolStripMenuItem.Click += new System.EventHandler(this.chiaHàngSerialToolStripMenuItem_Click);
            // 
            // điềuChuyểnNgangHàngSerialToolStripMenuItem
            // 
            this.điềuChuyểnNgangHàngSerialToolStripMenuItem.Name = "điềuChuyểnNgangHàngSerialToolStripMenuItem";
            this.điềuChuyểnNgangHàngSerialToolStripMenuItem.Size = new System.Drawing.Size(293, 22);
            this.điềuChuyểnNgangHàngSerialToolStripMenuItem.Text = "14.Điều chuyển ngang hàng serial";
            this.điềuChuyểnNgangHàngSerialToolStripMenuItem.Click += new System.EventHandler(this.điềuChuyểnNgangHàngSerialToolStripMenuItem_Click);
            // 
            // phụKiệnToolStripMenuItem
            // 
            this.phụKiệnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nhuCầuHàngPhụKiệnToolStripMenuItem,
            this.chiaHàngPhụKiệnToolStripMenuItem,
            this.điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem});
            this.phụKiệnToolStripMenuItem.Name = "phụKiệnToolStripMenuItem";
            this.phụKiệnToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.phụKiệnToolStripMenuItem.Text = "Phụ kiện";
            // 
            // nhuCầuHàngPhụKiệnToolStripMenuItem
            // 
            this.nhuCầuHàngPhụKiệnToolStripMenuItem.Name = "nhuCầuHàngPhụKiệnToolStripMenuItem";
            this.nhuCầuHàngPhụKiệnToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.nhuCầuHàngPhụKiệnToolStripMenuItem.Text = "16.Nhu cầu hàng phụ kiện";
            this.nhuCầuHàngPhụKiệnToolStripMenuItem.Click += new System.EventHandler(this.nhuCầuHàngPhụKiệnToolStripMenuItem_Click);
            // 
            // chiaHàngPhụKiệnToolStripMenuItem
            // 
            this.chiaHàngPhụKiệnToolStripMenuItem.Name = "chiaHàngPhụKiệnToolStripMenuItem";
            this.chiaHàngPhụKiệnToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.chiaHàngPhụKiệnToolStripMenuItem.Text = "17.Chia hàng phụ kiện";
            this.chiaHàngPhụKiệnToolStripMenuItem.Click += new System.EventHandler(this.chiaHàngPhụKiệnToolStripMenuItem_Click);
            // 
            // điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem
            // 
            this.điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem.Name = "điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem";
            this.điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem.Text = "18.Điều chuyển ngang hàng phụ kiện";
            this.điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem.Click += new System.EventHandler(this.điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem_Click);
            // 
            // frmTestNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 295);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmTestNew";
            this.Text = "Module đảm bảo hàng hóa";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem chứcNăngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tạoDanhMụcChiaHàngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chiaHàngSerialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem điềuChuyểnNgangHàngSerialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem phụKiệnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhuCầuHàngPhụKiệnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chiaHàngPhụKiệnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serialToolStripMenuItem;
    }
}