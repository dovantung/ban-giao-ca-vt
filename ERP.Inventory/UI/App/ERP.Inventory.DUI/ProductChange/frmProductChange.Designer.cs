﻿namespace ERP.Inventory.DUI.ProductChange
{
    partial class frmProductChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductChange));
            this.dtpPrChangeDate = new System.Windows.Forms.DateTimePicker();
            this.dtpInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.btnSearch = new System.Windows.Forms.Button();
            this.radOutput = new System.Windows.Forms.RadioButton();
            this.radInput = new System.Windows.Forms.RadioButton();
            this.radNew = new System.Windows.Forms.RadioButton();
            this.radOld = new System.Windows.Forms.RadioButton();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.flexData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.txtBarCode = new System.Windows.Forms.TextBox();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).BeginInit();
            this.mnuAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtpPrChangeDate
            // 
            this.dtpPrChangeDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpPrChangeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPrChangeDate.Location = new System.Drawing.Point(74, 29);
            this.dtpPrChangeDate.Name = "dtpPrChangeDate";
            this.dtpPrChangeDate.Size = new System.Drawing.Size(139, 22);
            this.dtpPrChangeDate.TabIndex = 0;
            // 
            // dtpInvoiceDate
            // 
            this.dtpInvoiceDate.CustomFormat = "dd/MM/yyyy";
            this.dtpInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInvoiceDate.Location = new System.Drawing.Point(308, 29);
            this.dtpInvoiceDate.Name = "dtpInvoiceDate";
            this.dtpInvoiceDate.Size = new System.Drawing.Size(97, 22);
            this.dtpInvoiceDate.TabIndex = 1;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(410, 142);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(36, 24);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // radOutput
            // 
            this.radOutput.AutoSize = true;
            this.radOutput.Location = new System.Drawing.Point(467, 67);
            this.radOutput.Name = "radOutput";
            this.radOutput.Size = new System.Drawing.Size(52, 20);
            this.radOutput.TabIndex = 4;
            this.radOutput.TabStop = true;
            this.radOutput.Text = "Xuất";
            this.radOutput.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radOutput.UseVisualStyleBackColor = true;
            // 
            // radInput
            // 
            this.radInput.AutoSize = true;
            this.radInput.Location = new System.Drawing.Point(527, 67);
            this.radInput.Name = "radInput";
            this.radInput.Size = new System.Drawing.Size(59, 20);
            this.radInput.TabIndex = 28;
            this.radInput.TabStop = true;
            this.radInput.Text = "Nhập";
            this.radInput.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radInput.UseVisualStyleBackColor = true;
            // 
            // radNew
            // 
            this.radNew.AutoSize = true;
            this.radNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNew.ForeColor = System.Drawing.Color.Red;
            this.radNew.Location = new System.Drawing.Point(10, 13);
            this.radNew.Name = "radNew";
            this.radNew.Size = new System.Drawing.Size(51, 20);
            this.radNew.TabIndex = 0;
            this.radNew.TabStop = true;
            this.radNew.Text = "Mới";
            this.radNew.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radNew.UseVisualStyleBackColor = true;
            // 
            // radOld
            // 
            this.radOld.AutoSize = true;
            this.radOld.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radOld.ForeColor = System.Drawing.Color.Red;
            this.radOld.Location = new System.Drawing.Point(67, 13);
            this.radOld.Name = "radOld";
            this.radOld.Size = new System.Drawing.Size(44, 20);
            this.radOld.TabIndex = 28;
            this.radOld.TabStop = true;
            this.radOld.Text = "Cũ";
            this.radOld.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radOld.UseVisualStyleBackColor = true;
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(74, 57);
            this.txtContent.MaxLength = 2000;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(330, 71);
            this.txtContent.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 30;
            this.label1.Text = "Ngày đổi:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(215, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 16);
            this.label2.TabIndex = 31;
            this.label2.Text = "Ngày hóa đơn:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(426, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 16);
            this.label3.TabIndex = 32;
            this.label3.Text = "Kho:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 16);
            this.label6.TabIndex = 34;
            this.label6.Text = "Barcode:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 33;
            this.label5.Text = "Nội dung:";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(466, 141);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 25);
            this.btnUpdate.TabIndex = 8;
            this.btnUpdate.Text = "   Cập &nhật";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // flexData
            // 
            this.flexData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flexData.AutoClipboard = true;
            this.flexData.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexData.ContextMenuStrip = this.mnuAction;
            this.flexData.Location = new System.Drawing.Point(5, 27);
            this.flexData.Name = "flexData";
            this.flexData.Rows.DefaultSize = 21;
            this.flexData.Size = new System.Drawing.Size(1055, 279);
            this.flexData.StyleInfo = resources.GetString("flexData.StyleInfo");
            this.flexData.TabIndex = 36;
            this.flexData.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexData.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexData_BeforeEdit);
            this.flexData.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexData_AfterEdit);
            this.flexData.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexData_SetupEditor);
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemDelete});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(95, 26);
            this.mnuAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAction_Opening);
            // 
            // mnuItemDelete
            // 
            this.mnuItemDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelete.Image")));
            this.mnuItemDelete.Name = "mnuItemDelete";
            this.mnuItemDelete.Size = new System.Drawing.Size(94, 22);
            this.mnuItemDelete.Text = "Xóa";
            this.mnuItemDelete.Click += new System.EventHandler(this.mnuItemDelete_Click);
            // 
            // txtBarCode
            // 
            this.txtBarCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarCode.Location = new System.Drawing.Point(74, 143);
            this.txtBarCode.Name = "txtBarCode";
            this.txtBarCode.Size = new System.Drawing.Size(330, 22);
            this.txtBarCode.TabIndex = 6;
            this.txtBarCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarCode_KeyPress);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseImage = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.Controls.Add(this.cboStore);
            this.groupControl1.Controls.Add(this.GroupBox2);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.txtBarCode);
            this.groupControl1.Controls.Add(this.dtpPrChangeDate);
            this.groupControl1.Controls.Add(this.dtpInvoiceDate);
            this.groupControl1.Controls.Add(this.btnUpdate);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.radOutput);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.radInput);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.txtContent);
            this.groupControl1.Location = new System.Drawing.Point(-1, 0);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1062, 180);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin xuất đổi hàng";
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(464, 27);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(306, 24);
            this.cboStore.TabIndex = 2;
            this.cboStore.SelectionChangeCommitted += new System.EventHandler(this.cboStore_SelectionChangeCommitted);
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.radNew);
            this.GroupBox2.Controls.Add(this.radOld);
            this.GroupBox2.Location = new System.Drawing.Point(644, 54);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(126, 46);
            this.GroupBox2.TabIndex = 5;
            this.GroupBox2.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Options.UseImage = true;
            this.groupControl2.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl2.Controls.Add(this.flexData);
            this.groupControl2.Location = new System.Drawing.Point(-1, 180);
            this.groupControl2.LookAndFeel.SkinName = "Blue";
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1062, 307);
            this.groupControl2.TabIndex = 40;
            this.groupControl2.Text = "Thông tin sản phẩm ";
            // 
            // frmProductChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 488);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1072, 515);
            this.Name = "frmProductChange";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xuất đổi hàng";
            this.Load += new System.EventHandler(this.frmProductChange_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).EndInit();
            this.mnuAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpPrChangeDate;
        private System.Windows.Forms.DateTimePicker dtpInvoiceDate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.RadioButton radOutput;
        private System.Windows.Forms.RadioButton radInput;
        private System.Windows.Forms.RadioButton radNew;
        private System.Windows.Forms.RadioButton radOld;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnUpdate;
        private C1.Win.C1FlexGrid.C1FlexGrid flexData;
        private System.Windows.Forms.TextBox txtBarCode;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.GroupBox GroupBox2;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelete;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStore;
    }
}