﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;

namespace ERP.Inventory.DUI.ProductChange
{
    partial class frmProductChangeOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductChangeOrder));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.dtpExpiryDate = new DevExpress.XtraEditors.DateEdit();
            this.txtReviewedStatus = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpDateChange = new DevExpress.XtraEditors.DateEdit();
            this.dtpDateApprove = new DevExpress.XtraEditors.DateEdit();
            this.dtpCreateDate = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUserChange = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUserApprove = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkIsChanged = new System.Windows.Forms.CheckBox();
            this.txtProductChangeTypeId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label7 = new System.Windows.Forms.Label();
            this.tabApprove = new System.Windows.Forms.TabPage();
            this.grdControlApprove = new DevExpress.XtraGrid.GridControl();
            this.grdViewApprove = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnChoose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditChoose = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumnReviewedStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumnReviewedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabAttachment = new System.Windows.Forms.TabPage();
            this.grdAttachment = new DevExpress.XtraGrid.GridControl();
            this.mnuAttachment = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuAddAttachment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelAttachment = new System.Windows.Forms.ToolStripMenuItem();
            this.grvAttachment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDownload = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDownload = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemExportExcelTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewData = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnDataNputVoucherConcernID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.grdColIMEI_Out = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColInputDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.grdColIMEI_IN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnIsNew_IN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repoSelectMachineError = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.grdColQuantity = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.grdColNote = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdExcelMau = new DevExpress.XtraGrid.GridControl();
            this.grdViewExcelMau = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnApprove = new DevExpress.XtraBars.BarButtonItem();
            this.btnUnApprove = new DevExpress.XtraBars.BarButtonItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dropDownBtnApprove = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCreateOutChange = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cboStatus = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.lblStatus = new System.Windows.Forms.Label();
            this.cboInStockStatus_srh = new System.Windows.Forms.ComboBox();
            this.lblTooltip = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtBarCode = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.timerClose = new System.Windows.Forms.Timer(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblInstruction = new System.Windows.Forms.Label();
            this.flexDetail = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblTitlePayment = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSupportCare = new System.Windows.Forms.Button();
            this.lblReviewStatus = new System.Windows.Forms.Label();
            this.txtReturnAmount = new System.Windows.Forms.TextBox();
            this.btnReviewList = new DevExpress.XtraEditors.DropDownButton();
            this.button2 = new System.Windows.Forms.Button();
            this.btnPrint = new DevExpress.XtraEditors.DropDownButton();
            this.btnCreateConcern = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageOutputVoucher = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtReasonNoteOV = new System.Windows.Forms.TextBox();
            this.cboReasonOV = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.lnkCustomer = new System.Windows.Forms.LinkLabel();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.comboBoxStore1 = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.txtCustomerPhone = new System.Windows.Forms.TextBox();
            this.cboInputTypeID = new Library.AppControl.ComboBoxControl.ComboBoxInputType();
            this.txtStaffUser = new System.Windows.Forms.TextBox();
            this.txtTaxID = new System.Windows.Forms.TextBox();
            this.ctrlStaffUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.txtCustomerAddress = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtCustomerIDCard = new System.Windows.Forms.TextBox();
            this.txtContentIOT = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtContentOV = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lblOutputContent = new System.Windows.Forms.Label();
            this.lblStore = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.dtOutputDate = new System.Windows.Forms.DateTimePicker();
            this.lblOutputDate = new System.Windows.Forms.Label();
            this.lblTaxID = new System.Windows.Forms.Label();
            this.lblInVoiceID = new System.Windows.Forms.Label();
            this.txtOutputVoucherID = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblOutputVoucherID = new System.Windows.Forms.Label();
            this.txtInVoiceID = new System.Windows.Forms.TextBox();
            this.txtInvoiceSymbol = new System.Windows.Forms.TextBox();
            this.txtInputTypeName = new System.Windows.Forms.TextBox();
            this.txtStoreName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chkCreateSaleOrder = new System.Windows.Forms.CheckBox();
            this.lblInvoiceSymbol = new System.Windows.Forms.Label();
            this.lblTotalLiquidate = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPageInputChangeOrder = new System.Windows.Forms.TabPage();
            this.chkApplyErrorType = new System.Windows.Forms.CheckBox();
            this.txtReasonNote = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtInputChangeOrderID = new System.Windows.Forms.TextBox();
            this.txtSaleOrderID = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.dtpInputchangeDate = new DevExpress.XtraEditors.DateEdit();
            this.dtpReviewedDate = new DevExpress.XtraEditors.DateEdit();
            this.txtCreateUser = new System.Windows.Forms.TextBox();
            this.txtInputChangeOrderStoreName = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.cboInputChangeOrderStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.dtpInputChangeOrderDate = new System.Windows.Forms.DateTimePicker();
            this.txtOutVoucherID = new System.Windows.Forms.TextBox();
            this.txtInVoucherID = new System.Windows.Forms.TextBox();
            this.ucCreateUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.txtNewInputVoucherID = new System.Windows.Forms.TextBox();
            this.txtNewOutputVoucherID = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.lnkSaleOrderID = new System.Windows.Forms.LinkLabel();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.chkIsInputchange = new System.Windows.Forms.CheckBox();
            this.chkIsReviewed = new System.Windows.Forms.CheckBox();
            this.lblContent = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.cboReason = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.tabPageReviewList = new System.Windows.Forms.TabPage();
            this.flexReviewLevel = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.tabPageWorkflow = new System.Windows.Forms.TabPage();
            this.flexWorkFlow = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.tabPageAttach = new System.Windows.Forms.TabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExpiryDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExpiryDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateChange.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateChange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateApprove.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateApprove.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreateDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreateDate.Properties)).BeginInit();
            this.tabApprove.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdControlApprove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewApprove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditChoose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.tabAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).BeginInit();
            this.mnuAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.mnuAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoSelectMachineError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdExcelMau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewExcelMau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexDetail)).BeginInit();
            this.panel6.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageOutputVoucher.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageInputChangeOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInputchangeDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInputchangeDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpReviewedDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpReviewedDate.Properties)).BeginInit();
            this.tabPageReviewList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexReviewLevel)).BeginInit();
            this.tabPageWorkflow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexWorkFlow)).BeginInit();
            this.tabPageAttach.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 232);
            this.panel1.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabGeneral);
            this.tabControl.Controls.Add(this.tabApprove);
            this.tabControl.Controls.Add(this.tabAttachment);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(984, 232);
            this.tabControl.TabIndex = 0;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.dtpExpiryDate);
            this.tabGeneral.Controls.Add(this.txtReviewedStatus);
            this.tabGeneral.Controls.Add(this.label12);
            this.tabGeneral.Controls.Add(this.dtpDateChange);
            this.tabGeneral.Controls.Add(this.dtpDateApprove);
            this.tabGeneral.Controls.Add(this.dtpCreateDate);
            this.tabGeneral.Controls.Add(this.label3);
            this.tabGeneral.Controls.Add(this.label9);
            this.tabGeneral.Controls.Add(this.txtUserChange);
            this.tabGeneral.Controls.Add(this.label10);
            this.tabGeneral.Controls.Add(this.txtDescription);
            this.tabGeneral.Controls.Add(this.label8);
            this.tabGeneral.Controls.Add(this.label5);
            this.tabGeneral.Controls.Add(this.txtUserApprove);
            this.tabGeneral.Controls.Add(this.label4);
            this.tabGeneral.Controls.Add(this.label1);
            this.tabGeneral.Controls.Add(this.chkIsChanged);
            this.tabGeneral.Controls.Add(this.txtProductChangeTypeId);
            this.tabGeneral.Controls.Add(this.label6);
            this.tabGeneral.Controls.Add(this.label32);
            this.tabGeneral.Controls.Add(this.label2);
            this.tabGeneral.Controls.Add(this.cboStore);
            this.tabGeneral.Controls.Add(this.label7);
            this.tabGeneral.Location = new System.Drawing.Point(4, 25);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneral.Size = new System.Drawing.Size(976, 203);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "Tổng quát";
            this.tabGeneral.UseVisualStyleBackColor = true;
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.EditValue = null;
            this.dtpExpiryDate.Location = new System.Drawing.Point(797, 8);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtpExpiryDate.Properties.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.dtpExpiryDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpExpiryDate.Properties.Appearance.Options.UseBackColor = true;
            this.dtpExpiryDate.Properties.Appearance.Options.UseFont = true;
            this.dtpExpiryDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpExpiryDate.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpExpiryDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtpExpiryDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpExpiryDate.Size = new System.Drawing.Size(139, 22);
            this.dtpExpiryDate.TabIndex = 2;
            // 
            // txtReviewedStatus
            // 
            this.txtReviewedStatus.BackColor = System.Drawing.SystemColors.Info;
            this.txtReviewedStatus.Location = new System.Drawing.Point(560, 40);
            this.txtReviewedStatus.MaxLength = 256;
            this.txtReviewedStatus.Name = "txtReviewedStatus";
            this.txtReviewedStatus.ReadOnly = true;
            this.txtReviewedStatus.Size = new System.Drawing.Size(376, 22);
            this.txtReviewedStatus.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(777, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 16);
            this.label12.TabIndex = 89;
            this.label12.Text = "*";
            // 
            // dtpDateChange
            // 
            this.dtpDateChange.EditValue = null;
            this.dtpDateChange.Location = new System.Drawing.Point(560, 168);
            this.dtpDateChange.Name = "dtpDateChange";
            this.dtpDateChange.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.dtpDateChange.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateChange.Properties.Appearance.Options.UseBackColor = true;
            this.dtpDateChange.Properties.Appearance.Options.UseFont = true;
            this.dtpDateChange.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpDateChange.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDateChange.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpDateChange.Properties.ReadOnly = true;
            this.dtpDateChange.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDateChange.Size = new System.Drawing.Size(133, 22);
            this.dtpDateChange.TabIndex = 10;
            // 
            // dtpDateApprove
            // 
            this.dtpDateApprove.EditValue = null;
            this.dtpDateApprove.Location = new System.Drawing.Point(560, 70);
            this.dtpDateApprove.Name = "dtpDateApprove";
            this.dtpDateApprove.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.dtpDateApprove.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateApprove.Properties.Appearance.Options.UseBackColor = true;
            this.dtpDateApprove.Properties.Appearance.Options.UseFont = true;
            this.dtpDateApprove.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpDateApprove.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDateApprove.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpDateApprove.Properties.ReadOnly = true;
            this.dtpDateApprove.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDateApprove.Size = new System.Drawing.Size(133, 22);
            this.dtpDateApprove.TabIndex = 6;
            // 
            // dtpCreateDate
            // 
            this.dtpCreateDate.EditValue = new System.DateTime(2016, 11, 15, 14, 32, 20, 0);
            this.dtpCreateDate.Location = new System.Drawing.Point(560, 8);
            this.dtpCreateDate.Name = "dtpCreateDate";
            this.dtpCreateDate.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.dtpCreateDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCreateDate.Properties.Appearance.Options.UseBackColor = true;
            this.dtpCreateDate.Properties.Appearance.Options.UseFont = true;
            this.dtpCreateDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpCreateDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpCreateDate.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpCreateDate.Properties.ReadOnly = true;
            this.dtpCreateDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpCreateDate.Size = new System.Drawing.Size(133, 22);
            this.dtpCreateDate.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(696, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 86;
            this.label3.Text = "Hạn xuất đổi:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(462, 171);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 16);
            this.label9.TabIndex = 85;
            this.label9.Text = "Ngày xuất đổi:";
            // 
            // txtUserChange
            // 
            this.txtUserChange.BackColor = System.Drawing.SystemColors.Info;
            this.txtUserChange.Location = new System.Drawing.Point(242, 168);
            this.txtUserChange.MaxLength = 256;
            this.txtUserChange.Name = "txtUserChange";
            this.txtUserChange.ReadOnly = true;
            this.txtUserChange.Size = new System.Drawing.Size(214, 22);
            this.txtUserChange.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(143, 171);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 16);
            this.label10.TabIndex = 83;
            this.label10.Text = "Nhân viên đổi:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(143, 100);
            this.txtDescription.MaxLength = 256;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(793, 59);
            this.txtDescription.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 16);
            this.label8.TabIndex = 81;
            this.label8.Text = "Mô tả:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(412, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 79;
            this.label5.Text = "Ngày duyệt:";
            // 
            // txtUserApprove
            // 
            this.txtUserApprove.BackColor = System.Drawing.SystemColors.Info;
            this.txtUserApprove.Location = new System.Drawing.Point(143, 70);
            this.txtUserApprove.MaxLength = 256;
            this.txtUserApprove.Name = "txtUserApprove";
            this.txtUserApprove.ReadOnly = true;
            this.txtUserApprove.Size = new System.Drawing.Size(261, 22);
            this.txtUserApprove.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 16);
            this.label4.TabIndex = 77;
            this.label4.Text = "Người duyệt:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(412, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 75;
            this.label1.Text = "Trạng thái duyệt:";
            // 
            // chkIsChanged
            // 
            this.chkIsChanged.AutoSize = true;
            this.chkIsChanged.Enabled = false;
            this.chkIsChanged.Location = new System.Drawing.Point(8, 170);
            this.chkIsChanged.Name = "chkIsChanged";
            this.chkIsChanged.Size = new System.Drawing.Size(93, 20);
            this.chkIsChanged.TabIndex = 8;
            this.chkIsChanged.Text = "Đã xuất đổi";
            this.chkIsChanged.UseVisualStyleBackColor = true;
            // 
            // txtProductChangeTypeId
            // 
            this.txtProductChangeTypeId.BackColor = System.Drawing.SystemColors.Info;
            this.txtProductChangeTypeId.Location = new System.Drawing.Point(143, 8);
            this.txtProductChangeTypeId.MaxLength = 256;
            this.txtProductChangeTypeId.Name = "txtProductChangeTypeId";
            this.txtProductChangeTypeId.ReadOnly = true;
            this.txtProductChangeTypeId.Size = new System.Drawing.Size(261, 22);
            this.txtProductChangeTypeId.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 16);
            this.label6.TabIndex = 72;
            this.label6.Text = "Mã yêu cầu xuất đổi:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(90, 42);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(14, 16);
            this.label32.TabIndex = 70;
            this.label32.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(412, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 16);
            this.label2.TabIndex = 68;
            this.label2.Text = "Ngày yêu cầu xuất đổi:";
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(143, 38);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(261, 24);
            this.cboStore.TabIndex = 3;
            this.cboStore.SelectionChangeCommitted += new System.EventHandler(this.cboStore_SelectionChangeCommitted);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 16);
            this.label7.TabIndex = 65;
            this.label7.Text = "Kho xuất đổi:";
            // 
            // tabApprove
            // 
            this.tabApprove.Controls.Add(this.grdControlApprove);
            this.tabApprove.Location = new System.Drawing.Point(4, 25);
            this.tabApprove.Name = "tabApprove";
            this.tabApprove.Padding = new System.Windows.Forms.Padding(3);
            this.tabApprove.Size = new System.Drawing.Size(976, 203);
            this.tabApprove.TabIndex = 1;
            this.tabApprove.Text = "Duyệt";
            this.tabApprove.UseVisualStyleBackColor = true;
            // 
            // grdControlApprove
            // 
            this.grdControlApprove.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdControlApprove.Location = new System.Drawing.Point(3, 3);
            this.grdControlApprove.MainView = this.grdViewApprove;
            this.grdControlApprove.Name = "grdControlApprove";
            this.grdControlApprove.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditChoose,
            this.repositoryItemLookUpEdit});
            this.grdControlApprove.Size = new System.Drawing.Size(970, 197);
            this.grdControlApprove.TabIndex = 12;
            this.grdControlApprove.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewApprove,
            this.gridView3,
            this.gridView4});
            // 
            // grdViewApprove
            // 
            this.grdViewApprove.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewApprove.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewApprove.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewApprove.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.grdViewApprove.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewApprove.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewApprove.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewApprove.Appearance.Preview.Options.UseFont = true;
            this.grdViewApprove.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewApprove.Appearance.Row.Options.UseFont = true;
            this.grdViewApprove.ColumnPanelRowHeight = 50;
            this.grdViewApprove.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumnChoose,
            this.gridColumnReviewedStatus,
            this.gridColumnReviewedDate,
            this.gridColumnNote});
            this.grdViewApprove.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewApprove.GridControl = this.grdControlApprove;
            this.grdViewApprove.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grdViewApprove.Name = "grdViewApprove";
            this.grdViewApprove.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewApprove.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewApprove.OptionsMenu.EnableFooterMenu = false;
            this.grdViewApprove.OptionsNavigation.UseTabKey = false;
            this.grdViewApprove.OptionsView.ShowAutoFilterRow = true;
            this.grdViewApprove.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewApprove.OptionsView.ShowGroupPanel = false;
            this.grdViewApprove.CellMerge += new DevExpress.XtraGrid.Views.Grid.CellMergeEventHandler(this.grdViewApprove_CellMerge);
            this.grdViewApprove.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewApprove_ShowingEditor);
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "STT";
            this.gridColumn11.FieldName = "SequenceReview";
            this.gridColumn11.MaxWidth = 50;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn11.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 48;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "Mức duyệt";
            this.gridColumn12.FieldName = "ReviewLevelName";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn12.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 1;
            this.gridColumn12.Width = 135;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "Người duyệt";
            this.gridColumn13.FieldName = "FullName";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn13.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 2;
            this.gridColumn13.Width = 135;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "Hình thức duyệt";
            this.gridColumn14.FieldName = "ReviewType";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn14.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 3;
            this.gridColumn14.Width = 140;
            // 
            // gridColumnChoose
            // 
            this.gridColumnChoose.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnChoose.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnChoose.Caption = "Chọn";
            this.gridColumnChoose.ColumnEdit = this.repositoryItemCheckEditChoose;
            this.gridColumnChoose.FieldName = "Check";
            this.gridColumnChoose.MaxWidth = 40;
            this.gridColumnChoose.MinWidth = 40;
            this.gridColumnChoose.Name = "gridColumnChoose";
            this.gridColumnChoose.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnChoose.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnChoose.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumnChoose.Visible = true;
            this.gridColumnChoose.VisibleIndex = 4;
            this.gridColumnChoose.Width = 40;
            // 
            // repositoryItemCheckEditChoose
            // 
            this.repositoryItemCheckEditChoose.AutoHeight = false;
            this.repositoryItemCheckEditChoose.Name = "repositoryItemCheckEditChoose";
            this.repositoryItemCheckEditChoose.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEditChoose.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEditChoose_CheckedChanged);
            // 
            // gridColumnReviewedStatus
            // 
            this.gridColumnReviewedStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnReviewedStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnReviewedStatus.Caption = "Trạng thái duyệt";
            this.gridColumnReviewedStatus.ColumnEdit = this.repositoryItemLookUpEdit;
            this.gridColumnReviewedStatus.FieldName = "ReviewedStatus";
            this.gridColumnReviewedStatus.Name = "gridColumnReviewedStatus";
            this.gridColumnReviewedStatus.OptionsColumn.AllowEdit = false;
            this.gridColumnReviewedStatus.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnReviewedStatus.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnReviewedStatus.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumnReviewedStatus.Visible = true;
            this.gridColumnReviewedStatus.VisibleIndex = 5;
            this.gridColumnReviewedStatus.Width = 171;
            // 
            // repositoryItemLookUpEdit
            // 
            this.repositoryItemLookUpEdit.AutoHeight = false;
            this.repositoryItemLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("REVIEWTYPEID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("REVIEWTYPENAME", 100, "Tên")});
            this.repositoryItemLookUpEdit.DisplayMember = "REVIEWTYPENAME";
            this.repositoryItemLookUpEdit.Name = "repositoryItemLookUpEdit";
            this.repositoryItemLookUpEdit.ValueMember = "REVIEWTYPEID";
            // 
            // gridColumnReviewedDate
            // 
            this.gridColumnReviewedDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnReviewedDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnReviewedDate.Caption = "Ngày duyệt";
            this.gridColumnReviewedDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumnReviewedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnReviewedDate.FieldName = "ReviewedDate";
            this.gridColumnReviewedDate.MaxWidth = 110;
            this.gridColumnReviewedDate.MinWidth = 110;
            this.gridColumnReviewedDate.Name = "gridColumnReviewedDate";
            this.gridColumnReviewedDate.OptionsColumn.AllowEdit = false;
            this.gridColumnReviewedDate.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnReviewedDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnReviewedDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumnReviewedDate.Visible = true;
            this.gridColumnReviewedDate.VisibleIndex = 6;
            this.gridColumnReviewedDate.Width = 110;
            // 
            // gridColumnNote
            // 
            this.gridColumnNote.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnNote.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnNote.Caption = "Ghi chú duyệt";
            this.gridColumnNote.FieldName = "Note";
            this.gridColumnNote.Name = "gridColumnNote";
            this.gridColumnNote.OptionsColumn.AllowEdit = false;
            this.gridColumnNote.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnNote.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnNote.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumnNote.Visible = true;
            this.gridColumnNote.VisibleIndex = 7;
            this.gridColumnNote.Width = 244;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.grdControlApprove;
            this.gridView3.Name = "gridView3";
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.grdControlApprove;
            this.gridView4.Name = "gridView4";
            // 
            // tabAttachment
            // 
            this.tabAttachment.Controls.Add(this.grdAttachment);
            this.tabAttachment.Location = new System.Drawing.Point(4, 25);
            this.tabAttachment.Name = "tabAttachment";
            this.tabAttachment.Padding = new System.Windows.Forms.Padding(3);
            this.tabAttachment.Size = new System.Drawing.Size(976, 203);
            this.tabAttachment.TabIndex = 2;
            this.tabAttachment.Text = "Tập tin đính kèm";
            this.tabAttachment.UseVisualStyleBackColor = true;
            // 
            // grdAttachment
            // 
            this.grdAttachment.ContextMenuStrip = this.mnuAttachment;
            this.grdAttachment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdAttachment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdAttachment.Location = new System.Drawing.Point(3, 3);
            this.grdAttachment.MainView = this.grvAttachment;
            this.grdAttachment.Name = "grdAttachment";
            this.grdAttachment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDownload,
            this.repositoryItemTextEdit4});
            this.grdAttachment.Size = new System.Drawing.Size(970, 197);
            this.grdAttachment.TabIndex = 5;
            this.grdAttachment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvAttachment});
            // 
            // mnuAttachment
            // 
            this.mnuAttachment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAddAttachment,
            this.mnuDelAttachment});
            this.mnuAttachment.Name = "mnuFlex";
            this.mnuAttachment.Size = new System.Drawing.Size(142, 48);
            this.mnuAttachment.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAttachment_Opening);
            // 
            // mnuAddAttachment
            // 
            this.mnuAddAttachment.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.mnuAddAttachment.Name = "mnuAddAttachment";
            this.mnuAddAttachment.Size = new System.Drawing.Size(141, 22);
            this.mnuAddAttachment.Text = "Thêm tập tin";
            this.mnuAddAttachment.Click += new System.EventHandler(this.mnuAddAttachment_Click);
            // 
            // mnuDelAttachment
            // 
            this.mnuDelAttachment.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuDelAttachment.Name = "mnuDelAttachment";
            this.mnuDelAttachment.Size = new System.Drawing.Size(141, 22);
            this.mnuDelAttachment.Text = "Xóa tập tin";
            this.mnuDelAttachment.Click += new System.EventHandler(this.mnuDelAttachment_Click);
            // 
            // grvAttachment
            // 
            this.grvAttachment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvAttachment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvAttachment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn4,
            this.gridColumn9,
            this.gridColumn15,
            this.colDownload});
            this.grvAttachment.GridControl = this.grdAttachment;
            this.grvAttachment.Name = "grvAttachment";
            this.grvAttachment.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn8.Caption = "Tên tập tin";
            this.gridColumn8.FieldName = "AttachMentName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.TabStop = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 200;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.Caption = "Thời gian tạo";
            this.gridColumn4.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn4.FieldName = "CreatedDate";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.TabStop = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 120;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.Caption = "Mô tả";
            this.gridColumn9.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn9.FieldName = "Description";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            this.gridColumn9.Width = 300;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.MaxLength = 400;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn15.Caption = "Nhân viên tạo";
            this.gridColumn15.FieldName = "CreatedFullName";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.TabStop = false;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 3;
            this.gridColumn15.Width = 250;
            // 
            // colDownload
            // 
            this.colDownload.AppearanceHeader.Options.UseTextOptions = true;
            this.colDownload.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDownload.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDownload.Caption = "Tải";
            this.colDownload.ColumnEdit = this.btnDownload;
            this.colDownload.FieldName = "colDownload";
            this.colDownload.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colDownload.Name = "colDownload";
            this.colDownload.OptionsColumn.AllowIncrementalSearch = false;
            this.colDownload.OptionsColumn.AllowMove = false;
            this.colDownload.OptionsColumn.AllowShowHide = false;
            this.colDownload.OptionsColumn.AllowSize = false;
            this.colDownload.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDownload.OptionsColumn.FixedWidth = true;
            this.colDownload.OptionsColumn.ReadOnly = true;
            this.colDownload.OptionsColumn.TabStop = false;
            this.colDownload.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colDownload.Visible = true;
            this.colDownload.VisibleIndex = 4;
            this.colDownload.Width = 30;
            // 
            // btnDownload
            // 
            this.btnDownload.AutoHeight = false;
            this.btnDownload.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnDownload.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Tải tập tin", null, null, true)});
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 232);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(984, 300);
            this.panel2.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.grdData);
            this.panel5.Controls.Add(this.grdExcelMau);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 58);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(984, 206);
            this.panel5.TabIndex = 2;
            // 
            // grdData
            // 
            this.grdData.ContextMenuStrip = this.mnuAction;
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(0, 0);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemLookUpEdit1,
            this.repoSelectMachineError});
            this.grdData.Size = new System.Drawing.Size(984, 206);
            this.grdData.TabIndex = 12;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData,
            this.gridView5,
            this.gridView1});
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemDelete,
            this.mnuItemExportExcel,
            this.toolStripSeparator1,
            this.mnuItemExportExcelTemplate,
            this.mnuItemImportExcel});
            this.mnuAction.Name = "mnuAction";
            this.mnuAction.Size = new System.Drawing.Size(172, 98);
            this.mnuAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAction_Opening);
            // 
            // mnuItemDelete
            // 
            this.mnuItemDelete.Checked = true;
            this.mnuItemDelete.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mnuItemDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelete.Image")));
            this.mnuItemDelete.Name = "mnuItemDelete";
            this.mnuItemDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuItemDelete.Size = new System.Drawing.Size(171, 22);
            this.mnuItemDelete.Text = "Xóa";
            this.mnuItemDelete.Click += new System.EventHandler(this.mnuItemDelete_Click);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExportExcel.Image")));
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnuItemExportExcel.Size = new System.Drawing.Size(171, 22);
            this.mnuItemExportExcel.Text = "Xuất  Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(168, 6);
            // 
            // mnuItemExportExcelTemplate
            // 
            this.mnuItemExportExcelTemplate.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcelTemplate.Name = "mnuItemExportExcelTemplate";
            this.mnuItemExportExcelTemplate.Size = new System.Drawing.Size(171, 22);
            this.mnuItemExportExcelTemplate.Tag = "Xuất Excel mẫu";
            this.mnuItemExportExcelTemplate.Text = "Xuất Excel mẫu";
            this.mnuItemExportExcelTemplate.Click += new System.EventHandler(this.mnuItemExportExcelTemplate_Click);
            // 
            // mnuItemImportExcel
            // 
            this.mnuItemImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemImportExcel.Name = "mnuItemImportExcel";
            this.mnuItemImportExcel.Size = new System.Drawing.Size(171, 22);
            this.mnuItemImportExcel.Text = "Nhập từ Excel";
            this.mnuItemImportExcel.Click += new System.EventHandler(this.mnuItemImportExcel_Click);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.BandPanel.Options.UseFont = true;
            this.grdViewData.Appearance.BandPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.BandPanelRowHeight = 30;
            this.grdViewData.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand2,
            this.gridBand3,
            this.gridBand5,
            this.gridBand13});
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumnDataNputVoucherConcernID,
            this.gridColumn2,
            this.gridColumn3,
            this.grdColIMEI_Out,
            this.gridColumn5,
            this.gridColInputDate,
            this.gridColumn6,
            this.gridColumn7,
            this.grdColIMEI_IN,
            this.gridColumnIsNew_IN,
            this.grdColQuantity,
            this.grdColNote,
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn4});
            this.grdViewData.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewData.OptionsMenu.EnableColumnMenu = false;
            this.grdViewData.OptionsMenu.EnableFooterMenu = false;
            this.grdViewData.OptionsMenu.EnableGroupPanelMenu = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowColumnHeaders = false;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewData_ShowingEditor);
            this.grdViewData.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdViewData_CellValueChanged);
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand4.Caption = "Số phiếu nhập gốc";
            this.gridBand4.Columns.Add(this.gridColumnDataNputVoucherConcernID);
            this.gridBand4.MinWidth = 120;
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.OptionsBand.AllowSize = false;
            this.gridBand4.OptionsBand.FixedWidth = true;
            this.gridBand4.Width = 120;
            // 
            // gridColumnDataNputVoucherConcernID
            // 
            this.gridColumnDataNputVoucherConcernID.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnDataNputVoucherConcernID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnDataNputVoucherConcernID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnDataNputVoucherConcernID.Caption = "Số phiếu nhập gốc";
            this.gridColumnDataNputVoucherConcernID.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumnDataNputVoucherConcernID.FieldName = "NputVoucherConcernID";
            this.gridColumnDataNputVoucherConcernID.Name = "gridColumnDataNputVoucherConcernID";
            this.gridColumnDataNputVoucherConcernID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnDataNputVoucherConcernID.Visible = true;
            this.gridColumnDataNputVoucherConcernID.Width = 120;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "[^ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềề" +
    "ểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]{0,20}";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemTextEdit2.MaxLength = 20;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Thông tin xuất";
            this.gridBand2.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8,
            this.gridBand17});
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.OptionsBand.AllowSize = false;
            this.gridBand2.Width = 627;
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Mã SP";
            this.gridBand1.Columns.Add(this.gridColumn2);
            this.gridBand1.MinWidth = 100;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.OptionsBand.AllowSize = false;
            this.gridBand1.OptionsBand.FixedWidth = true;
            this.gridBand1.Width = 115;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Mã SP xuất";
            this.gridColumn2.FieldName = "ProductID_Out";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.Visible = true;
            this.gridColumn2.Width = 115;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "Tên SP";
            this.gridBand6.Columns.Add(this.gridColumn3);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Width = 180;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Tên SP xuất";
            this.gridColumn3.FieldName = "ProductID_OutName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.Visible = true;
            this.gridColumn3.Width = 180;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "IMEI";
            this.gridBand7.Columns.Add(this.grdColIMEI_Out);
            this.gridBand7.MinWidth = 100;
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.OptionsBand.AllowSize = false;
            this.gridBand7.OptionsBand.FixedWidth = true;
            this.gridBand7.Width = 125;
            // 
            // grdColIMEI_Out
            // 
            this.grdColIMEI_Out.AppearanceHeader.Options.UseTextOptions = true;
            this.grdColIMEI_Out.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdColIMEI_Out.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdColIMEI_Out.Caption = "IMEI xuất";
            this.grdColIMEI_Out.FieldName = "IMEI_Out";
            this.grdColIMEI_Out.Name = "grdColIMEI_Out";
            this.grdColIMEI_Out.OptionsColumn.AllowEdit = false;
            this.grdColIMEI_Out.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.grdColIMEI_Out.Visible = true;
            this.grdColIMEI_Out.Width = 125;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Trạng thái";
            this.gridBand8.Columns.Add(this.gridColumn5);
            this.gridBand8.MinWidth = 30;
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.OptionsBand.AllowSize = false;
            this.gridBand8.OptionsBand.FixedWidth = true;
            this.gridBand8.Width = 100;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "TTSP xuất";
            this.gridColumn5.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.gridColumn5.FieldName = "InStockStatusID_Out";
            this.gridColumn5.MinWidth = 30;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn5.Visible = true;
            this.gridColumn5.Width = 100;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("INSTOCKSTATUSNAME", "Tên trạng thái")});
            this.repositoryItemLookUpEdit1.DisplayMember = "INSTOCKSTATUSNAME";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ValueMember = "INSTOCKSTATUSID";
            // 
            // gridBand17
            // 
            this.gridBand17.Caption = "Ngày nhập";
            this.gridBand17.Columns.Add(this.gridColInputDate);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.Width = 107;
            // 
            // gridColInputDate
            // 
            this.gridColInputDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColInputDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColInputDate.Caption = "Ngày nhập";
            this.gridColInputDate.FieldName = "InputDate";
            this.gridColInputDate.Name = "gridColInputDate";
            this.gridColInputDate.OptionsColumn.AllowEdit = false;
            this.gridColInputDate.OptionsColumn.ReadOnly = true;
            this.gridColInputDate.Visible = true;
            this.gridColInputDate.Width = 107;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Thông tin nhập";
            this.gridBand3.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand9,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand14,
            this.gridBand15,
            this.gridBand16});
            this.gridBand3.MinWidth = 20;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.OptionsBand.AllowSize = false;
            this.gridBand3.Width = 795;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "Mã SP";
            this.gridBand9.Columns.Add(this.gridColumn6);
            this.gridBand9.MinWidth = 100;
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.OptionsBand.AllowSize = false;
            this.gridBand9.OptionsBand.FixedWidth = true;
            this.gridBand9.Width = 115;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Mã SP nhập";
            this.gridColumn6.FieldName = "ProductID_IN";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn6.Visible = true;
            this.gridColumn6.Width = 115;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "Tên SP";
            this.gridBand10.Columns.Add(this.gridColumn7);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.Width = 180;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.Caption = "Tên SP nhập";
            this.gridColumn7.FieldName = "ProductID_INName";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn7.Visible = true;
            this.gridColumn7.Width = 180;
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "IMEI";
            this.gridBand11.Columns.Add(this.grdColIMEI_IN);
            this.gridBand11.MinWidth = 100;
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.OptionsBand.AllowSize = false;
            this.gridBand11.OptionsBand.FixedWidth = true;
            this.gridBand11.Width = 125;
            // 
            // grdColIMEI_IN
            // 
            this.grdColIMEI_IN.AppearanceHeader.Options.UseTextOptions = true;
            this.grdColIMEI_IN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdColIMEI_IN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdColIMEI_IN.Caption = "IMEI nhập";
            this.grdColIMEI_IN.FieldName = "IMEI_IN";
            this.grdColIMEI_IN.Name = "grdColIMEI_IN";
            this.grdColIMEI_IN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.grdColIMEI_IN.Visible = true;
            this.grdColIMEI_IN.Width = 125;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "Trạng thái";
            this.gridBand12.Columns.Add(this.gridColumnIsNew_IN);
            this.gridBand12.MinWidth = 30;
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.OptionsBand.AllowSize = false;
            this.gridBand12.OptionsBand.FixedWidth = true;
            this.gridBand12.Width = 100;
            // 
            // gridColumnIsNew_IN
            // 
            this.gridColumnIsNew_IN.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnIsNew_IN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnIsNew_IN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumnIsNew_IN.Caption = "TTSP nhập";
            this.gridColumnIsNew_IN.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.gridColumnIsNew_IN.FieldName = "InStockStatusID_In";
            this.gridColumnIsNew_IN.MinWidth = 30;
            this.gridColumnIsNew_IN.Name = "gridColumnIsNew_IN";
            this.gridColumnIsNew_IN.OptionsColumn.AllowEdit = false;
            this.gridColumnIsNew_IN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnIsNew_IN.Visible = true;
            this.gridColumnIsNew_IN.Width = 100;
            // 
            // gridBand14
            // 
            this.gridBand14.Caption = "Nhóm lỗi";
            this.gridBand14.Columns.Add(this.bandedGridColumn1);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.Width = 136;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.bandedGridColumn1.Caption = "Nhóm lỗi";
            this.bandedGridColumn1.FieldName = "MachineErrorGroupName";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.ReadOnly = true;
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 136;
            // 
            // gridBand15
            // 
            this.gridBand15.Caption = "Lỗi";
            this.gridBand15.Columns.Add(this.bandedGridColumn2);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.Width = 139;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.bandedGridColumn2.Caption = "Lỗi";
            this.bandedGridColumn2.ColumnEdit = this.repoSelectMachineError;
            this.bandedGridColumn2.FieldName = "MachineErrorName";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.ReadOnly = true;
            this.bandedGridColumn2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 139;
            // 
            // repoSelectMachineError
            // 
            this.repoSelectMachineError.AutoHeight = false;
            this.repoSelectMachineError.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repoSelectMachineError.Name = "repoSelectMachineError";
            this.repoSelectMachineError.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repoSelectMachineError_ButtonClick);
            // 
            // gridBand16
            // 
            this.gridBand16.Caption = "gridBand16";
            this.gridBand16.Columns.Add(this.bandedGridColumn4);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.Visible = false;
            this.gridBand16.Width = 150;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "bandedGridColumn4";
            this.bandedGridColumn4.FieldName = "MachineErrorID";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Số Lượng";
            this.gridBand5.Columns.Add(this.grdColQuantity);
            this.gridBand5.MinWidth = 30;
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.OptionsBand.AllowSize = false;
            this.gridBand5.OptionsBand.FixedWidth = true;
            this.gridBand5.Width = 80;
            // 
            // grdColQuantity
            // 
            this.grdColQuantity.AppearanceCell.Options.UseTextOptions = true;
            this.grdColQuantity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.grdColQuantity.AppearanceHeader.Options.UseTextOptions = true;
            this.grdColQuantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdColQuantity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdColQuantity.Caption = "SL";
            this.grdColQuantity.ColumnEdit = this.repositoryItemTextEdit1;
            this.grdColQuantity.FieldName = "Quantity";
            this.grdColQuantity.MinWidth = 30;
            this.grdColQuantity.Name = "grdColQuantity";
            this.grdColQuantity.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.grdColQuantity.Visible = true;
            this.grdColQuantity.Width = 80;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "###,###,##0";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.NullText = "0";
            this.repositoryItemTextEdit1.NullValuePrompt = "0";
            this.repositoryItemTextEdit1.NullValuePromptShowForEmptyValue = true;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand13.AppearanceHeader.Options.UseFont = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Ghi Chú";
            this.gridBand13.Columns.Add(this.grdColNote);
            this.gridBand13.MinWidth = 30;
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.Width = 248;
            // 
            // grdColNote
            // 
            this.grdColNote.AppearanceHeader.Options.UseTextOptions = true;
            this.grdColNote.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdColNote.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdColNote.Caption = "Note";
            this.grdColNote.ColumnEdit = this.repositoryItemTextEdit3;
            this.grdColNote.FieldName = "ChangeNote";
            this.grdColNote.Name = "grdColNote";
            this.grdColNote.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.grdColNote.Visible = true;
            this.grdColNote.Width = 248;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // gridView5
            // 
            this.gridView5.GridControl = this.grdData;
            this.gridView5.Name = "gridView5";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.grdData;
            this.gridView1.Name = "gridView1";
            // 
            // grdExcelMau
            // 
            this.grdExcelMau.Location = new System.Drawing.Point(172, 183);
            this.grdExcelMau.MainView = this.grdViewExcelMau;
            this.grdExcelMau.MenuManager = this.barManager1;
            this.grdExcelMau.Name = "grdExcelMau";
            this.grdExcelMau.Size = new System.Drawing.Size(400, 53);
            this.grdExcelMau.TabIndex = 13;
            this.grdExcelMau.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewExcelMau});
            this.grdExcelMau.Visible = false;
            // 
            // grdViewExcelMau
            // 
            this.grdViewExcelMau.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grdViewExcelMau.ColumnPanelRowHeight = 30;
            this.grdViewExcelMau.GridControl = this.grdExcelMau;
            this.grdViewExcelMau.Name = "grdViewExcelMau";
            this.grdViewExcelMau.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.grdViewExcelMau.OptionsCustomization.AllowColumnMoving = false;
            this.grdViewExcelMau.OptionsView.ColumnAutoWidth = false;
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnApprove,
            this.btnUnApprove});
            this.barManager1.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(984, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 532);
            this.barDockControlBottom.Size = new System.Drawing.Size(984, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 532);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(984, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 532);
            // 
            // btnApprove
            // 
            this.btnApprove.Caption = "Đồng ý";
            this.btnApprove.Id = 0;
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnApprove_ItemClick);
            // 
            // btnUnApprove
            // 
            this.btnUnApprove.Caption = "Từ chối";
            this.btnUnApprove.Id = 1;
            this.btnUnApprove.Name = "btnUnApprove";
            this.btnUnApprove.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUnApprove_ItemClick);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dropDownBtnApprove);
            this.panel4.Controls.Add(this.btnDelete);
            this.panel4.Controls.Add(this.btnCreateOutChange);
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 264);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(984, 36);
            this.panel4.TabIndex = 1;
            // 
            // dropDownBtnApprove
            // 
            this.dropDownBtnApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownBtnApprove.Appearance.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dropDownBtnApprove.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropDownBtnApprove.Appearance.Options.UseBackColor = true;
            this.dropDownBtnApprove.Appearance.Options.UseFont = true;
            this.dropDownBtnApprove.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.dropDownBtnApprove.DropDownControl = this.popupMenu1;
            this.dropDownBtnApprove.Image = ((System.Drawing.Image)(resources.GetObject("dropDownBtnApprove.Image")));
            this.dropDownBtnApprove.Location = new System.Drawing.Point(705, 7);
            this.dropDownBtnApprove.Name = "dropDownBtnApprove";
            this.dropDownBtnApprove.Size = new System.Drawing.Size(92, 23);
            this.dropDownBtnApprove.TabIndex = 1;
            this.dropDownBtnApprove.Text = "Duyệt";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnApprove),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnUnApprove)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(915, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(58, 24);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Hủy";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCreateOutChange
            // 
            this.btnCreateOutChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateOutChange.Image = ((System.Drawing.Image)(resources.GetObject("btnCreateOutChange.Image")));
            this.btnCreateOutChange.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreateOutChange.Location = new System.Drawing.Point(803, 6);
            this.btnCreateOutChange.Name = "btnCreateOutChange";
            this.btnCreateOutChange.Size = new System.Drawing.Size(106, 24);
            this.btnCreateOutChange.TabIndex = 2;
            this.btnCreateOutChange.Text = "Tạo xuất đổi";
            this.btnCreateOutChange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateOutChange.UseVisualStyleBackColor = true;
            this.btnCreateOutChange.Click += new System.EventHandler(this.btnCreateOutChange_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(613, 6);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(86, 24);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cboStatus);
            this.panel3.Controls.Add(this.lblStatus);
            this.panel3.Controls.Add(this.cboInStockStatus_srh);
            this.panel3.Controls.Add(this.lblTooltip);
            this.panel3.Controls.Add(this.btnSearch);
            this.panel3.Controls.Add(this.txtBarCode);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(984, 58);
            this.panel3.TabIndex = 0;
            // 
            // cboStatus
            // 
            this.cboStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStatus.Location = new System.Drawing.Point(419, 28);
            this.cboStatus.Margin = new System.Windows.Forms.Padding(0);
            this.cboStatus.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(220, 24);
            this.cboStatus.TabIndex = 85;
            this.cboStatus.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(336, 31);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(71, 16);
            this.lblStatus.TabIndex = 83;
            this.lblStatus.Text = "Trạng thái:";
            this.lblStatus.Visible = false;
            // 
            // cboInStockStatus_srh
            // 
            this.cboInStockStatus_srh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInStockStatus_srh.FormattingEnabled = true;
            this.cboInStockStatus_srh.Location = new System.Drawing.Point(762, 25);
            this.cboInStockStatus_srh.Name = "cboInStockStatus_srh";
            this.cboInStockStatus_srh.Size = new System.Drawing.Size(210, 24);
            this.cboInStockStatus_srh.TabIndex = 83;
            this.cboInStockStatus_srh.Visible = false;
            // 
            // lblTooltip
            // 
            this.lblTooltip.AutoSize = true;
            this.lblTooltip.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTooltip.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblTooltip.Location = new System.Drawing.Point(12, 10);
            this.lblTooltip.Name = "lblTooltip";
            this.lblTooltip.Size = new System.Drawing.Size(177, 16);
            this.lblTooltip.TabIndex = 82;
            this.lblTooltip.Text = "Nhập sản phẩm cần xuất";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(265, 29);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(42, 24);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtBarCode
            // 
            this.txtBarCode.Location = new System.Drawing.Point(75, 30);
            this.txtBarCode.MaxLength = 256;
            this.txtBarCode.Name = "txtBarCode";
            this.txtBarCode.Size = new System.Drawing.Size(190, 22);
            this.txtBarCode.TabIndex = 0;
            this.txtBarCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarCode_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 16);
            this.label11.TabIndex = 79;
            this.label11.Text = "Barcode:";
            // 
            // timerClose
            // 
            this.timerClose.Tick += new System.EventHandler(this.timerClose_Tick);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox2.Controls.Add(this.lblInstruction);
            this.groupBox2.Controls.Add(this.flexDetail);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 439);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(970, 0);
            this.groupBox2.TabIndex = 75;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chi tiết nhập đổi/trả";
            // 
            // lblInstruction
            // 
            this.lblInstruction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInstruction.AutoSize = true;
            this.lblInstruction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstruction.ForeColor = System.Drawing.Color.Blue;
            this.lblInstruction.Location = new System.Drawing.Point(550, 2);
            this.lblInstruction.Name = "lblInstruction";
            this.lblInstruction.Size = new System.Drawing.Size(416, 16);
            this.lblInstruction.TabIndex = 64;
            this.lblInstruction.Text = "Cho phép nhập trực tiếp IMEI xuất nếu không quan tâm FIFO";
            this.lblInstruction.Visible = false;
            // 
            // flexDetail
            // 
            this.flexDetail.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexDetail.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexDetail.AutoClipboard = true;
            this.flexDetail.ColumnInfo = "1,1,0,0,0,95,Columns:";
            this.flexDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexDetail.Location = new System.Drawing.Point(3, 16);
            this.flexDetail.Name = "flexDetail";
            this.flexDetail.Rows.Count = 1;
            this.flexDetail.Rows.DefaultSize = 19;
            this.flexDetail.Size = new System.Drawing.Size(964, 0);
            this.flexDetail.TabIndex = 37;
            this.flexDetail.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Info;
            this.panel6.Controls.Add(this.lblTitlePayment);
            this.panel6.Controls.Add(this.btnAdd);
            this.panel6.Controls.Add(this.button1);
            this.panel6.Controls.Add(this.btnSupportCare);
            this.panel6.Controls.Add(this.lblReviewStatus);
            this.panel6.Controls.Add(this.txtReturnAmount);
            this.panel6.Controls.Add(this.btnReviewList);
            this.panel6.Controls.Add(this.button2);
            this.panel6.Controls.Add(this.btnPrint);
            this.panel6.Controls.Add(this.btnCreateConcern);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(3, 141);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(970, 59);
            this.panel6.TabIndex = 76;
            // 
            // lblTitlePayment
            // 
            this.lblTitlePayment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTitlePayment.AutoSize = true;
            this.lblTitlePayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitlePayment.ForeColor = System.Drawing.Color.Blue;
            this.lblTitlePayment.Location = new System.Drawing.Point(390, 27);
            this.lblTitlePayment.Name = "lblTitlePayment";
            this.lblTitlePayment.Size = new System.Drawing.Size(75, 20);
            this.lblTitlePayment.TabIndex = 54;
            this.lblTitlePayment.Text = "Phải thu";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.BackColor = System.Drawing.SystemColors.Control;
            this.btnAdd.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(858, 25);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(106, 25);
            this.btnAdd.TabIndex = 32;
            this.btnAdd.Text = "Tạo yêu cầu";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.Enabled = false;
            this.button1.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(858, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 25);
            this.button1.TabIndex = 49;
            this.button1.Text = "Cập nhật";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // btnSupportCare
            // 
            this.btnSupportCare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSupportCare.BackColor = System.Drawing.SystemColors.Control;
            this.btnSupportCare.Image = global::ERP.Inventory.DUI.Properties.Resources.sentuser;
            this.btnSupportCare.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSupportCare.Location = new System.Drawing.Point(655, 25);
            this.btnSupportCare.Name = "btnSupportCare";
            this.btnSupportCare.Size = new System.Drawing.Size(116, 25);
            this.btnSupportCare.TabIndex = 64;
            this.btnSupportCare.Text = "Hỗ trợ trả Care";
            this.btnSupportCare.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSupportCare.UseVisualStyleBackColor = false;
            this.btnSupportCare.Visible = false;
            // 
            // lblReviewStatus
            // 
            this.lblReviewStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblReviewStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReviewStatus.ForeColor = System.Drawing.Color.Blue;
            this.lblReviewStatus.Location = new System.Drawing.Point(7, 3);
            this.lblReviewStatus.Name = "lblReviewStatus";
            this.lblReviewStatus.Size = new System.Drawing.Size(286, 54);
            this.lblReviewStatus.TabIndex = 42;
            this.lblReviewStatus.Text = "Trạng thái duyệt";
            this.lblReviewStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtReturnAmount
            // 
            this.txtReturnAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtReturnAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtReturnAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReturnAmount.Location = new System.Drawing.Point(471, 24);
            this.txtReturnAmount.MaxLength = 20;
            this.txtReturnAmount.Name = "txtReturnAmount";
            this.txtReturnAmount.ReadOnly = true;
            this.txtReturnAmount.Size = new System.Drawing.Size(128, 26);
            this.txtReturnAmount.TabIndex = 44;
            this.txtReturnAmount.Text = "0";
            // 
            // btnReviewList
            // 
            this.btnReviewList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReviewList.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReviewList.Appearance.Options.UseFont = true;
            this.btnReviewList.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnReviewList.Location = new System.Drawing.Point(299, 24);
            this.btnReviewList.Name = "btnReviewList";
            this.btnReviewList.Size = new System.Drawing.Size(85, 25);
            this.btnReviewList.TabIndex = 41;
            this.btnReviewList.Text = "Duyệt";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(777, 25);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 25);
            this.button2.TabIndex = 43;
            this.button2.Text = "      Hủy";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Appearance.Options.UseFont = true;
            this.btnPrint.Location = new System.Drawing.Point(605, 25);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(91, 25);
            this.btnPrint.TabIndex = 59;
            this.btnPrint.Text = "In";
            this.btnPrint.Visible = false;
            // 
            // btnCreateConcern
            // 
            this.btnCreateConcern.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCreateConcern.BackColor = System.Drawing.SystemColors.Control;
            this.btnCreateConcern.Enabled = false;
            this.btnCreateConcern.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnCreateConcern.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreateConcern.Location = new System.Drawing.Point(605, 25);
            this.btnCreateConcern.Name = "btnCreateConcern";
            this.btnCreateConcern.Size = new System.Drawing.Size(91, 25);
            this.btnCreateConcern.TabIndex = 40;
            this.btnCreateConcern.Text = "   Tạo phiếu";
            this.btnCreateConcern.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateConcern.UseVisualStyleBackColor = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageOutputVoucher);
            this.tabControl1.Controls.Add(this.tabPageInputChangeOrder);
            this.tabControl1.Controls.Add(this.tabPageReviewList);
            this.tabControl1.Controls.Add(this.tabPageWorkflow);
            this.tabControl1.Controls.Add(this.tabPageAttach);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(970, 436);
            this.tabControl1.TabIndex = 74;
            // 
            // tabPageOutputVoucher
            // 
            this.tabPageOutputVoucher.BackColor = System.Drawing.SystemColors.Window;
            this.tabPageOutputVoucher.Controls.Add(this.groupBox1);
            this.tabPageOutputVoucher.Location = new System.Drawing.Point(4, 22);
            this.tabPageOutputVoucher.Name = "tabPageOutputVoucher";
            this.tabPageOutputVoucher.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOutputVoucher.Size = new System.Drawing.Size(962, 410);
            this.tabPageOutputVoucher.TabIndex = 0;
            this.tabPageOutputVoucher.Text = "Thông tin phiếu xuất";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtReasonNoteOV);
            this.groupBox1.Controls.Add(this.cboReasonOV);
            this.groupBox1.Controls.Add(this.lnkCustomer);
            this.groupBox1.Controls.Add(this.txtCustomerID);
            this.groupBox1.Controls.Add(this.comboBoxStore1);
            this.groupBox1.Controls.Add(this.txtCustomerPhone);
            this.groupBox1.Controls.Add(this.cboInputTypeID);
            this.groupBox1.Controls.Add(this.txtStaffUser);
            this.groupBox1.Controls.Add(this.txtTaxID);
            this.groupBox1.Controls.Add(this.ctrlStaffUser);
            this.groupBox1.Controls.Add(this.txtCustomerAddress);
            this.groupBox1.Controls.Add(this.txtCustomerName);
            this.groupBox1.Controls.Add(this.txtCustomerIDCard);
            this.groupBox1.Controls.Add(this.txtContentIOT);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtContentOV);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.lblOutputContent);
            this.groupBox1.Controls.Add(this.lblStore);
            this.groupBox1.Controls.Add(this.lblPhone);
            this.groupBox1.Controls.Add(this.dtOutputDate);
            this.groupBox1.Controls.Add(this.lblOutputDate);
            this.groupBox1.Controls.Add(this.lblTaxID);
            this.groupBox1.Controls.Add(this.lblInVoiceID);
            this.groupBox1.Controls.Add(this.txtOutputVoucherID);
            this.groupBox1.Controls.Add(this.lblAddress);
            this.groupBox1.Controls.Add(this.lblOutputVoucherID);
            this.groupBox1.Controls.Add(this.txtInVoiceID);
            this.groupBox1.Controls.Add(this.txtInvoiceSymbol);
            this.groupBox1.Controls.Add(this.txtInputTypeName);
            this.groupBox1.Controls.Add(this.txtStoreName);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.chkCreateSaleOrder);
            this.groupBox1.Controls.Add(this.lblInvoiceSymbol);
            this.groupBox1.Controls.Add(this.lblTotalLiquidate);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(938, 404);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(591, 103);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Nhân viên xuất: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(490, 307);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "Ghi chú";
            // 
            // txtReasonNoteOV
            // 
            this.txtReasonNoteOV.Location = new System.Drawing.Point(573, 285);
            this.txtReasonNoteOV.MaxLength = 300;
            this.txtReasonNoteOV.Multiline = true;
            this.txtReasonNoteOV.Name = "txtReasonNoteOV";
            this.txtReasonNoteOV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReasonNoteOV.Size = new System.Drawing.Size(351, 106);
            this.txtReasonNoteOV.TabIndex = 37;
            // 
            // cboReasonOV
            // 
            this.cboReasonOV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboReasonOV.Location = new System.Drawing.Point(573, 258);
            this.cboReasonOV.Margin = new System.Windows.Forms.Padding(0);
            this.cboReasonOV.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboReasonOV.Name = "cboReasonOV";
            this.cboReasonOV.Size = new System.Drawing.Size(351, 24);
            this.cboReasonOV.TabIndex = 34;
            // 
            // lnkCustomer
            // 
            this.lnkCustomer.AutoSize = true;
            this.lnkCustomer.Location = new System.Drawing.Point(7, 23);
            this.lnkCustomer.Name = "lnkCustomer";
            this.lnkCustomer.Size = new System.Drawing.Size(68, 13);
            this.lnkCustomer.TabIndex = 0;
            this.lnkCustomer.TabStop = true;
            this.lnkCustomer.Text = "Khách hàng:";
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerID.Location = new System.Drawing.Point(135, 21);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.ReadOnly = true;
            this.txtCustomerID.Size = new System.Drawing.Size(449, 20);
            this.txtCustomerID.TabIndex = 1;
            this.txtCustomerID.TabStop = false;
            // 
            // comboBoxStore1
            // 
            this.comboBoxStore1.Enabled = false;
            this.comboBoxStore1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxStore1.Location = new System.Drawing.Point(134, 229);
            this.comboBoxStore1.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxStore1.MinimumSize = new System.Drawing.Size(24, 24);
            this.comboBoxStore1.Name = "comboBoxStore1";
            this.comboBoxStore1.Size = new System.Drawing.Size(331, 24);
            this.comboBoxStore1.TabIndex = 26;
            // 
            // txtCustomerPhone
            // 
            this.txtCustomerPhone.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerPhone.Location = new System.Drawing.Point(695, 47);
            this.txtCustomerPhone.MaxLength = 9;
            this.txtCustomerPhone.Name = "txtCustomerPhone";
            this.txtCustomerPhone.ReadOnly = true;
            this.txtCustomerPhone.Size = new System.Drawing.Size(229, 20);
            this.txtCustomerPhone.TabIndex = 7;
            // 
            // cboInputTypeID
            // 
            this.cboInputTypeID.Enabled = false;
            this.cboInputTypeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputTypeID.Location = new System.Drawing.Point(573, 229);
            this.cboInputTypeID.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputTypeID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputTypeID.Name = "cboInputTypeID";
            this.cboInputTypeID.Size = new System.Drawing.Size(351, 24);
            this.cboInputTypeID.TabIndex = 28;
            // 
            // txtStaffUser
            // 
            this.txtStaffUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtStaffUser.Location = new System.Drawing.Point(695, 101);
            this.txtStaffUser.MaxLength = 20;
            this.txtStaffUser.Name = "txtStaffUser";
            this.txtStaffUser.ReadOnly = true;
            this.txtStaffUser.Size = new System.Drawing.Size(229, 20);
            this.txtStaffUser.TabIndex = 13;
            // 
            // txtTaxID
            // 
            this.txtTaxID.BackColor = System.Drawing.SystemColors.Info;
            this.txtTaxID.Location = new System.Drawing.Point(695, 75);
            this.txtTaxID.MaxLength = 20;
            this.txtTaxID.Name = "txtTaxID";
            this.txtTaxID.ReadOnly = true;
            this.txtTaxID.Size = new System.Drawing.Size(229, 20);
            this.txtTaxID.TabIndex = 11;
            // 
            // ctrlStaffUser
            // 
            this.ctrlStaffUser.Enabled = false;
            this.ctrlStaffUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrlStaffUser.IsOnlyShowRealStaff = false;
            this.ctrlStaffUser.IsValidate = true;
            this.ctrlStaffUser.Location = new System.Drawing.Point(833, 101);
            this.ctrlStaffUser.Margin = new System.Windows.Forms.Padding(4);
            this.ctrlStaffUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ctrlStaffUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ctrlStaffUser.Name = "ctrlStaffUser";
            this.ctrlStaffUser.Size = new System.Drawing.Size(91, 22);
            this.ctrlStaffUser.TabIndex = 14;
            this.ctrlStaffUser.UserName = "";
            this.ctrlStaffUser.Visible = false;
            this.ctrlStaffUser.WorkingPositionID = 0;
            // 
            // txtCustomerAddress
            // 
            this.txtCustomerAddress.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerAddress.Location = new System.Drawing.Point(135, 75);
            this.txtCustomerAddress.MaxLength = 400;
            this.txtCustomerAddress.Multiline = true;
            this.txtCustomerAddress.Name = "txtCustomerAddress";
            this.txtCustomerAddress.ReadOnly = true;
            this.txtCustomerAddress.Size = new System.Drawing.Size(449, 52);
            this.txtCustomerAddress.TabIndex = 9;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerName.Location = new System.Drawing.Point(135, 47);
            this.txtCustomerName.MaxLength = 200;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.ReadOnly = true;
            this.txtCustomerName.Size = new System.Drawing.Size(449, 20);
            this.txtCustomerName.TabIndex = 5;
            // 
            // txtCustomerIDCard
            // 
            this.txtCustomerIDCard.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerIDCard.Location = new System.Drawing.Point(695, 21);
            this.txtCustomerIDCard.MaxLength = 9;
            this.txtCustomerIDCard.Name = "txtCustomerIDCard";
            this.txtCustomerIDCard.ReadOnly = true;
            this.txtCustomerIDCard.Size = new System.Drawing.Size(229, 20);
            this.txtCustomerIDCard.TabIndex = 3;
            // 
            // txtContentIOT
            // 
            this.txtContentIOT.Location = new System.Drawing.Point(134, 258);
            this.txtContentIOT.MaxLength = 200;
            this.txtContentIOT.Multiline = true;
            this.txtContentIOT.Name = "txtContentIOT";
            this.txtContentIOT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContentIOT.Size = new System.Drawing.Size(331, 104);
            this.txtContentIOT.TabIndex = 31;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 280);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "nhập đổi trả:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(591, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Số CMND:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 261);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "Nội dung yêu cầu";
            // 
            // txtContentOV
            // 
            this.txtContentOV.BackColor = System.Drawing.SystemColors.Info;
            this.txtContentOV.Location = new System.Drawing.Point(135, 159);
            this.txtContentOV.MaxLength = 200;
            this.txtContentOV.Multiline = true;
            this.txtContentOV.Name = "txtContentOV";
            this.txtContentOV.ReadOnly = true;
            this.txtContentOV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContentOV.Size = new System.Drawing.Size(789, 64);
            this.txtContentOV.TabIndex = 24;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 50);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Họ tên KH:";
            // 
            // lblOutputContent
            // 
            this.lblOutputContent.AutoSize = true;
            this.lblOutputContent.Location = new System.Drawing.Point(7, 162);
            this.lblOutputContent.Name = "lblOutputContent";
            this.lblOutputContent.Size = new System.Drawing.Size(105, 13);
            this.lblOutputContent.TabIndex = 23;
            this.lblOutputContent.Text = "Nội dung phiếu xuất:";
            // 
            // lblStore
            // 
            this.lblStore.AutoSize = true;
            this.lblStore.Location = new System.Drawing.Point(7, 232);
            this.lblStore.Name = "lblStore";
            this.lblStore.Size = new System.Drawing.Size(56, 13);
            this.lblStore.TabIndex = 25;
            this.lblStore.Text = "Kho nhập:";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(591, 53);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(58, 13);
            this.lblPhone.TabIndex = 6;
            this.lblPhone.Text = "Điện thoại:";
            // 
            // dtOutputDate
            // 
            this.dtOutputDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtOutputDate.Enabled = false;
            this.dtOutputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtOutputDate.Location = new System.Drawing.Point(440, 132);
            this.dtOutputDate.Name = "dtOutputDate";
            this.dtOutputDate.Size = new System.Drawing.Size(144, 20);
            this.dtOutputDate.TabIndex = 18;
            // 
            // lblOutputDate
            // 
            this.lblOutputDate.AutoSize = true;
            this.lblOutputDate.Location = new System.Drawing.Point(370, 134);
            this.lblOutputDate.Name = "lblOutputDate";
            this.lblOutputDate.Size = new System.Drawing.Size(58, 13);
            this.lblOutputDate.TabIndex = 17;
            this.lblOutputDate.Text = "Ngày xuất:";
            // 
            // lblTaxID
            // 
            this.lblTaxID.AutoSize = true;
            this.lblTaxID.Location = new System.Drawing.Point(591, 81);
            this.lblTaxID.Name = "lblTaxID";
            this.lblTaxID.Size = new System.Drawing.Size(63, 13);
            this.lblTaxID.TabIndex = 10;
            this.lblTaxID.Text = "Mã số thuế:";
            // 
            // lblInVoiceID
            // 
            this.lblInVoiceID.AutoSize = true;
            this.lblInVoiceID.Location = new System.Drawing.Point(591, 134);
            this.lblInVoiceID.Name = "lblInVoiceID";
            this.lblInVoiceID.Size = new System.Drawing.Size(69, 13);
            this.lblInVoiceID.TabIndex = 19;
            this.lblInVoiceID.Text = "Hóa đơn số: ";
            // 
            // txtOutputVoucherID
            // 
            this.txtOutputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputVoucherID.Location = new System.Drawing.Point(135, 132);
            this.txtOutputVoucherID.Name = "txtOutputVoucherID";
            this.txtOutputVoucherID.ReadOnly = true;
            this.txtOutputVoucherID.Size = new System.Drawing.Size(229, 20);
            this.txtOutputVoucherID.TabIndex = 16;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(7, 78);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(43, 13);
            this.lblAddress.TabIndex = 8;
            this.lblAddress.Text = "Địa chỉ:";
            // 
            // lblOutputVoucherID
            // 
            this.lblOutputVoucherID.AutoSize = true;
            this.lblOutputVoucherID.Location = new System.Drawing.Point(7, 134);
            this.lblOutputVoucherID.Name = "lblOutputVoucherID";
            this.lblOutputVoucherID.Size = new System.Drawing.Size(77, 13);
            this.lblOutputVoucherID.TabIndex = 15;
            this.lblOutputVoucherID.Text = "Mã phiếu xuất:";
            // 
            // txtInVoiceID
            // 
            this.txtInVoiceID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInVoiceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtInVoiceID.ForeColor = System.Drawing.Color.Red;
            this.txtInVoiceID.Location = new System.Drawing.Point(695, 132);
            this.txtInVoiceID.MaxLength = 20;
            this.txtInVoiceID.Name = "txtInVoiceID";
            this.txtInVoiceID.ReadOnly = true;
            this.txtInVoiceID.Size = new System.Drawing.Size(75, 22);
            this.txtInVoiceID.TabIndex = 20;
            // 
            // txtInvoiceSymbol
            // 
            this.txtInvoiceSymbol.BackColor = System.Drawing.SystemColors.Info;
            this.txtInvoiceSymbol.Location = new System.Drawing.Point(849, 132);
            this.txtInvoiceSymbol.MaxLength = 20;
            this.txtInvoiceSymbol.Name = "txtInvoiceSymbol";
            this.txtInvoiceSymbol.ReadOnly = true;
            this.txtInvoiceSymbol.Size = new System.Drawing.Size(75, 20);
            this.txtInvoiceSymbol.TabIndex = 22;
            // 
            // txtInputTypeName
            // 
            this.txtInputTypeName.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputTypeName.Location = new System.Drawing.Point(692, 229);
            this.txtInputTypeName.MaxLength = 20;
            this.txtInputTypeName.Name = "txtInputTypeName";
            this.txtInputTypeName.ReadOnly = true;
            this.txtInputTypeName.Size = new System.Drawing.Size(226, 20);
            this.txtInputTypeName.TabIndex = 29;
            this.txtInputTypeName.Visible = false;
            // 
            // txtStoreName
            // 
            this.txtStoreName.BackColor = System.Drawing.SystemColors.Info;
            this.txtStoreName.Location = new System.Drawing.Point(134, 229);
            this.txtStoreName.MaxLength = 20;
            this.txtStoreName.Name = "txtStoreName";
            this.txtStoreName.ReadOnly = true;
            this.txtStoreName.Size = new System.Drawing.Size(242, 20);
            this.txtStoreName.TabIndex = 8;
            this.txtStoreName.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(468, 232);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Loại phiếu nhập:";
            // 
            // chkCreateSaleOrder
            // 
            this.chkCreateSaleOrder.AutoSize = true;
            this.chkCreateSaleOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCreateSaleOrder.ForeColor = System.Drawing.Color.Red;
            this.chkCreateSaleOrder.Location = new System.Drawing.Point(135, 367);
            this.chkCreateSaleOrder.Name = "chkCreateSaleOrder";
            this.chkCreateSaleOrder.Size = new System.Drawing.Size(312, 24);
            this.chkCreateSaleOrder.TabIndex = 38;
            this.chkCreateSaleOrder.Text = "Đổi hình thức xuất - Lấy khuyến mãi";
            this.chkCreateSaleOrder.UseVisualStyleBackColor = true;
            // 
            // lblInvoiceSymbol
            // 
            this.lblInvoiceSymbol.AutoSize = true;
            this.lblInvoiceSymbol.Location = new System.Drawing.Point(772, 134);
            this.lblInvoiceSymbol.Name = "lblInvoiceSymbol";
            this.lblInvoiceSymbol.Size = new System.Drawing.Size(64, 13);
            this.lblInvoiceSymbol.TabIndex = 21;
            this.lblInvoiceSymbol.Text = "Ký hiệu HĐ:";
            // 
            // lblTotalLiquidate
            // 
            this.lblTotalLiquidate.AutoSize = true;
            this.lblTotalLiquidate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalLiquidate.ForeColor = System.Drawing.Color.Blue;
            this.lblTotalLiquidate.Location = new System.Drawing.Point(131, 262);
            this.lblTotalLiquidate.Name = "lblTotalLiquidate";
            this.lblTotalLiquidate.Size = new System.Drawing.Size(306, 24);
            this.lblTotalLiquidate.TabIndex = 39;
            this.lblTotalLiquidate.Text = "Tổng tiền thanh toán của đơn hàng";
            this.lblTotalLiquidate.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(491, 323);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "lý do đổi trả:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(490, 262);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "Lý do đổi trả:";
            // 
            // tabPageInputChangeOrder
            // 
            this.tabPageInputChangeOrder.Controls.Add(this.chkApplyErrorType);
            this.tabPageInputChangeOrder.Controls.Add(this.txtReasonNote);
            this.tabPageInputChangeOrder.Controls.Add(this.label21);
            this.tabPageInputChangeOrder.Controls.Add(this.txtInputChangeOrderID);
            this.tabPageInputChangeOrder.Controls.Add(this.txtSaleOrderID);
            this.tabPageInputChangeOrder.Controls.Add(this.label23);
            this.tabPageInputChangeOrder.Controls.Add(this.dtpInputchangeDate);
            this.tabPageInputChangeOrder.Controls.Add(this.dtpReviewedDate);
            this.tabPageInputChangeOrder.Controls.Add(this.txtCreateUser);
            this.tabPageInputChangeOrder.Controls.Add(this.txtInputChangeOrderStoreName);
            this.tabPageInputChangeOrder.Controls.Add(this.label24);
            this.tabPageInputChangeOrder.Controls.Add(this.txtContent);
            this.tabPageInputChangeOrder.Controls.Add(this.label25);
            this.tabPageInputChangeOrder.Controls.Add(this.label26);
            this.tabPageInputChangeOrder.Controls.Add(this.label27);
            this.tabPageInputChangeOrder.Controls.Add(this.cboInputChangeOrderStoreID);
            this.tabPageInputChangeOrder.Controls.Add(this.dtpInputChangeOrderDate);
            this.tabPageInputChangeOrder.Controls.Add(this.txtOutVoucherID);
            this.tabPageInputChangeOrder.Controls.Add(this.txtInVoucherID);
            this.tabPageInputChangeOrder.Controls.Add(this.ucCreateUser);
            this.tabPageInputChangeOrder.Controls.Add(this.txtNewInputVoucherID);
            this.tabPageInputChangeOrder.Controls.Add(this.txtNewOutputVoucherID);
            this.tabPageInputChangeOrder.Controls.Add(this.label28);
            this.tabPageInputChangeOrder.Controls.Add(this.lnkSaleOrderID);
            this.tabPageInputChangeOrder.Controls.Add(this.label29);
            this.tabPageInputChangeOrder.Controls.Add(this.label30);
            this.tabPageInputChangeOrder.Controls.Add(this.chkIsInputchange);
            this.tabPageInputChangeOrder.Controls.Add(this.chkIsReviewed);
            this.tabPageInputChangeOrder.Controls.Add(this.lblContent);
            this.tabPageInputChangeOrder.Controls.Add(this.label31);
            this.tabPageInputChangeOrder.Controls.Add(this.cboReason);
            this.tabPageInputChangeOrder.Location = new System.Drawing.Point(4, 22);
            this.tabPageInputChangeOrder.Name = "tabPageInputChangeOrder";
            this.tabPageInputChangeOrder.Size = new System.Drawing.Size(962, 410);
            this.tabPageInputChangeOrder.TabIndex = 2;
            this.tabPageInputChangeOrder.Text = "Thông tin yêu cầu đổi trả";
            this.tabPageInputChangeOrder.UseVisualStyleBackColor = true;
            // 
            // chkApplyErrorType
            // 
            this.chkApplyErrorType.AutoSize = true;
            this.chkApplyErrorType.Enabled = false;
            this.chkApplyErrorType.Location = new System.Drawing.Point(501, 14);
            this.chkApplyErrorType.Name = "chkApplyErrorType";
            this.chkApplyErrorType.Size = new System.Drawing.Size(87, 17);
            this.chkApplyErrorType.TabIndex = 4;
            this.chkApplyErrorType.Text = "Sản phẩm lỗi";
            this.chkApplyErrorType.UseVisualStyleBackColor = true;
            // 
            // txtReasonNote
            // 
            this.txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
            this.txtReasonNote.Location = new System.Drawing.Point(99, 148);
            this.txtReasonNote.MaxLength = 300;
            this.txtReasonNote.Multiline = true;
            this.txtReasonNote.Name = "txtReasonNote";
            this.txtReasonNote.ReadOnly = true;
            this.txtReasonNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReasonNote.Size = new System.Drawing.Size(706, 89);
            this.txtReasonNote.TabIndex = 27;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(253, 124);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 13);
            this.label21.TabIndex = 24;
            this.label21.Text = "Lý do đổi trả:";
            // 
            // txtInputChangeOrderID
            // 
            this.txtInputChangeOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputChangeOrderID.Location = new System.Drawing.Point(99, 13);
            this.txtInputChangeOrderID.Name = "txtInputChangeOrderID";
            this.txtInputChangeOrderID.ReadOnly = true;
            this.txtInputChangeOrderID.Size = new System.Drawing.Size(141, 20);
            this.txtInputChangeOrderID.TabIndex = 1;
            // 
            // txtSaleOrderID
            // 
            this.txtSaleOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtSaleOrderID.Location = new System.Drawing.Point(99, 121);
            this.txtSaleOrderID.Name = "txtSaleOrderID";
            this.txtSaleOrderID.ReadOnly = true;
            this.txtSaleOrderID.Size = new System.Drawing.Size(141, 20);
            this.txtSaleOrderID.TabIndex = 23;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(501, 43);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 13);
            this.label23.TabIndex = 7;
            this.label23.Text = "NV yêu cầu: ";
            // 
            // dtpInputchangeDate
            // 
            this.dtpInputchangeDate.EditValue = null;
            this.dtpInputchangeDate.Enabled = false;
            this.dtpInputchangeDate.Location = new System.Drawing.Point(99, 94);
            this.dtpInputchangeDate.Name = "dtpInputchangeDate";
            this.dtpInputchangeDate.Properties.Appearance.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dtpInputchangeDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.dtpInputchangeDate.Properties.Appearance.Options.UseBackColor = true;
            this.dtpInputchangeDate.Properties.Appearance.Options.UseFont = true;
            this.dtpInputchangeDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpInputchangeDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.dtpInputchangeDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtpInputchangeDate.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpInputchangeDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtpInputchangeDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtpInputchangeDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpInputchangeDate.Size = new System.Drawing.Size(141, 20);
            this.dtpInputchangeDate.TabIndex = 17;
            // 
            // dtpReviewedDate
            // 
            this.dtpReviewedDate.EditValue = null;
            this.dtpReviewedDate.Enabled = false;
            this.dtpReviewedDate.Location = new System.Drawing.Point(99, 67);
            this.dtpReviewedDate.Name = "dtpReviewedDate";
            this.dtpReviewedDate.Properties.Appearance.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dtpReviewedDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.dtpReviewedDate.Properties.Appearance.Options.UseBackColor = true;
            this.dtpReviewedDate.Properties.Appearance.Options.UseFont = true;
            this.dtpReviewedDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpReviewedDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.dtpReviewedDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtpReviewedDate.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpReviewedDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtpReviewedDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtpReviewedDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpReviewedDate.Size = new System.Drawing.Size(141, 20);
            this.dtpReviewedDate.TabIndex = 11;
            // 
            // txtCreateUser
            // 
            this.txtCreateUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtCreateUser.Location = new System.Drawing.Point(605, 40);
            this.txtCreateUser.Name = "txtCreateUser";
            this.txtCreateUser.ReadOnly = true;
            this.txtCreateUser.Size = new System.Drawing.Size(200, 20);
            this.txtCreateUser.TabIndex = 9;
            this.txtCreateUser.Visible = false;
            // 
            // txtInputChangeOrderStoreName
            // 
            this.txtInputChangeOrderStoreName.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputChangeOrderStoreName.Location = new System.Drawing.Point(99, 40);
            this.txtInputChangeOrderStoreName.Name = "txtInputChangeOrderStoreName";
            this.txtInputChangeOrderStoreName.ReadOnly = true;
            this.txtInputChangeOrderStoreName.Size = new System.Drawing.Size(388, 20);
            this.txtInputChangeOrderStoreName.TabIndex = 6;
            this.txtInputChangeOrderStoreName.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(253, 97);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 13);
            this.label24.TabIndex = 18;
            this.label24.Text = "Phiếu chi mới:";
            // 
            // txtContent
            // 
            this.txtContent.BackColor = System.Drawing.SystemColors.Info;
            this.txtContent.Location = new System.Drawing.Point(99, 242);
            this.txtContent.MaxLength = 200;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ReadOnly = true;
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(706, 89);
            this.txtContent.TabIndex = 29;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(501, 97);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 13);
            this.label25.TabIndex = 20;
            this.label25.Text = "Phiếu nhập mới:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(501, 70);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(79, 13);
            this.label26.TabIndex = 14;
            this.label26.Text = "Phiếu xuất mới:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(253, 70);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(74, 13);
            this.label27.TabIndex = 12;
            this.label27.Text = "Phiếu thu mới:";
            // 
            // cboInputChangeOrderStoreID
            // 
            this.cboInputChangeOrderStoreID.Enabled = false;
            this.cboInputChangeOrderStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputChangeOrderStoreID.Location = new System.Drawing.Point(99, 39);
            this.cboInputChangeOrderStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputChangeOrderStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputChangeOrderStoreID.Name = "cboInputChangeOrderStoreID";
            this.cboInputChangeOrderStoreID.Size = new System.Drawing.Size(389, 24);
            this.cboInputChangeOrderStoreID.TabIndex = 6;
            // 
            // dtpInputChangeOrderDate
            // 
            this.dtpInputChangeOrderDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpInputChangeOrderDate.Enabled = false;
            this.dtpInputChangeOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInputChangeOrderDate.Location = new System.Drawing.Point(346, 13);
            this.dtpInputChangeOrderDate.Name = "dtpInputChangeOrderDate";
            this.dtpInputChangeOrderDate.Size = new System.Drawing.Size(141, 20);
            this.dtpInputChangeOrderDate.TabIndex = 3;
            // 
            // txtOutVoucherID
            // 
            this.txtOutVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutVoucherID.Location = new System.Drawing.Point(346, 94);
            this.txtOutVoucherID.Name = "txtOutVoucherID";
            this.txtOutVoucherID.ReadOnly = true;
            this.txtOutVoucherID.Size = new System.Drawing.Size(141, 20);
            this.txtOutVoucherID.TabIndex = 19;
            // 
            // txtInVoucherID
            // 
            this.txtInVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInVoucherID.Location = new System.Drawing.Point(346, 67);
            this.txtInVoucherID.Name = "txtInVoucherID";
            this.txtInVoucherID.ReadOnly = true;
            this.txtInVoucherID.Size = new System.Drawing.Size(141, 20);
            this.txtInVoucherID.TabIndex = 13;
            // 
            // ucCreateUser
            // 
            this.ucCreateUser.Enabled = false;
            this.ucCreateUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucCreateUser.IsOnlyShowRealStaff = false;
            this.ucCreateUser.IsValidate = true;
            this.ucCreateUser.Location = new System.Drawing.Point(605, 40);
            this.ucCreateUser.Margin = new System.Windows.Forms.Padding(4);
            this.ucCreateUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucCreateUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucCreateUser.Name = "ucCreateUser";
            this.ucCreateUser.Size = new System.Drawing.Size(200, 22);
            this.ucCreateUser.TabIndex = 8;
            this.ucCreateUser.UserName = "";
            this.ucCreateUser.WorkingPositionID = 0;
            // 
            // txtNewInputVoucherID
            // 
            this.txtNewInputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtNewInputVoucherID.Location = new System.Drawing.Point(605, 94);
            this.txtNewInputVoucherID.Name = "txtNewInputVoucherID";
            this.txtNewInputVoucherID.ReadOnly = true;
            this.txtNewInputVoucherID.Size = new System.Drawing.Size(200, 20);
            this.txtNewInputVoucherID.TabIndex = 21;
            // 
            // txtNewOutputVoucherID
            // 
            this.txtNewOutputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtNewOutputVoucherID.Location = new System.Drawing.Point(605, 67);
            this.txtNewOutputVoucherID.Name = "txtNewOutputVoucherID";
            this.txtNewOutputVoucherID.ReadOnly = true;
            this.txtNewOutputVoucherID.Size = new System.Drawing.Size(200, 20);
            this.txtNewOutputVoucherID.TabIndex = 15;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(12, 153);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(72, 13);
            this.label28.TabIndex = 26;
            this.label28.Text = "Ghi chú lý do:";
            // 
            // lnkSaleOrderID
            // 
            this.lnkSaleOrderID.AutoSize = true;
            this.lnkSaleOrderID.Location = new System.Drawing.Point(12, 124);
            this.lnkSaleOrderID.Name = "lnkSaleOrderID";
            this.lnkSaleOrderID.Size = new System.Drawing.Size(71, 13);
            this.lnkSaleOrderID.TabIndex = 22;
            this.lnkSaleOrderID.TabStop = true;
            this.lnkSaleOrderID.Text = "Mã đơn hàng";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(12, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(66, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Mã yêu cầu:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 43);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 13);
            this.label30.TabIndex = 5;
            this.label30.Text = "Kho nhập:";
            // 
            // chkIsInputchange
            // 
            this.chkIsInputchange.AutoSize = true;
            this.chkIsInputchange.Enabled = false;
            this.chkIsInputchange.Location = new System.Drawing.Point(12, 95);
            this.chkIsInputchange.Name = "chkIsInputchange";
            this.chkIsInputchange.Size = new System.Drawing.Size(64, 17);
            this.chkIsInputchange.TabIndex = 16;
            this.chkIsInputchange.Text = "Đã xử lý";
            this.chkIsInputchange.UseVisualStyleBackColor = true;
            // 
            // chkIsReviewed
            // 
            this.chkIsReviewed.AutoSize = true;
            this.chkIsReviewed.Enabled = false;
            this.chkIsReviewed.Location = new System.Drawing.Point(12, 68);
            this.chkIsReviewed.Name = "chkIsReviewed";
            this.chkIsReviewed.Size = new System.Drawing.Size(69, 17);
            this.chkIsReviewed.TabIndex = 10;
            this.chkIsReviewed.Text = "Đã duyệt";
            this.chkIsReviewed.UseVisualStyleBackColor = true;
            // 
            // lblContent
            // 
            this.lblContent.AutoSize = true;
            this.lblContent.Location = new System.Drawing.Point(12, 250);
            this.lblContent.Name = "lblContent";
            this.lblContent.Size = new System.Drawing.Size(53, 13);
            this.lblContent.TabIndex = 28;
            this.lblContent.Text = "Nội dung:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(253, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(76, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "Ngày yêu cầu:";
            // 
            // cboReason
            // 
            this.cboReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboReason.Location = new System.Drawing.Point(346, 120);
            this.cboReason.Margin = new System.Windows.Forms.Padding(0);
            this.cboReason.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboReason.Name = "cboReason";
            this.cboReason.Size = new System.Drawing.Size(459, 24);
            this.cboReason.TabIndex = 25;
            // 
            // tabPageReviewList
            // 
            this.tabPageReviewList.Controls.Add(this.flexReviewLevel);
            this.tabPageReviewList.Location = new System.Drawing.Point(4, 22);
            this.tabPageReviewList.Name = "tabPageReviewList";
            this.tabPageReviewList.Size = new System.Drawing.Size(962, 410);
            this.tabPageReviewList.TabIndex = 3;
            this.tabPageReviewList.Text = "Mức duyệt";
            this.tabPageReviewList.UseVisualStyleBackColor = true;
            // 
            // flexReviewLevel
            // 
            this.flexReviewLevel.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexReviewLevel.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexReviewLevel.ColumnInfo = "1,1,0,0,0,95,Columns:";
            this.flexReviewLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexReviewLevel.Location = new System.Drawing.Point(0, 0);
            this.flexReviewLevel.Name = "flexReviewLevel";
            this.flexReviewLevel.Rows.Count = 1;
            this.flexReviewLevel.Rows.DefaultSize = 19;
            this.flexReviewLevel.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Cell;
            this.flexReviewLevel.Size = new System.Drawing.Size(962, 410);
            this.flexReviewLevel.TabIndex = 4;
            // 
            // tabPageWorkflow
            // 
            this.tabPageWorkflow.Controls.Add(this.flexWorkFlow);
            this.tabPageWorkflow.Location = new System.Drawing.Point(4, 22);
            this.tabPageWorkflow.Name = "tabPageWorkflow";
            this.tabPageWorkflow.Size = new System.Drawing.Size(962, 410);
            this.tabPageWorkflow.TabIndex = 4;
            this.tabPageWorkflow.Text = "Quy trình xử lý";
            this.tabPageWorkflow.UseVisualStyleBackColor = true;
            // 
            // flexWorkFlow
            // 
            this.flexWorkFlow.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexWorkFlow.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexWorkFlow.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexWorkFlow.ColumnInfo = "1,1,0,0,0,95,Columns:";
            this.flexWorkFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexWorkFlow.Location = new System.Drawing.Point(0, 0);
            this.flexWorkFlow.Name = "flexWorkFlow";
            this.flexWorkFlow.Rows.Count = 1;
            this.flexWorkFlow.Rows.DefaultSize = 19;
            this.flexWorkFlow.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Cell;
            this.flexWorkFlow.Size = new System.Drawing.Size(962, 410);
            this.flexWorkFlow.TabIndex = 7;
            // 
            // tabPageAttach
            // 
            this.tabPageAttach.Controls.Add(this.gridControl1);
            this.tabPageAttach.Location = new System.Drawing.Point(4, 22);
            this.tabPageAttach.Name = "tabPageAttach";
            this.tabPageAttach.Size = new System.Drawing.Size(962, 410);
            this.tabPageAttach.TabIndex = 5;
            this.tabPageAttach.Text = "Tập tin đính kèm";
            this.tabPageAttach.UseVisualStyleBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemTextEdit5});
            this.gridControl1.Size = new System.Drawing.Size(962, 410);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn10,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "STT";
            this.gridColumn1.FieldName = "STT";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.TabStop = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn10.Caption = "Tên tập tin";
            this.gridColumn10.FieldName = "AttachmentName";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.TabStop = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 1;
            this.gridColumn10.Width = 194;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn16.Caption = "Mô tả";
            this.gridColumn16.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn16.FieldName = "Description";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 2;
            this.gridColumn16.Width = 273;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.MaxLength = 400;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn17.Caption = "Thời gian tạo";
            this.gridColumn17.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn17.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn17.FieldName = "CreatedDate";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.TabStop = false;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 3;
            this.gridColumn17.Width = 119;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn18.Caption = "Nhân viên tạo";
            this.gridColumn18.FieldName = "CreatedFullName";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.TabStop = false;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 4;
            this.gridColumn18.Width = 241;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn19.Caption = "Tải";
            this.gridColumn19.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn19.FieldName = "colDownload";
            this.gridColumn19.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumn19.OptionsColumn.AllowMove = false;
            this.gridColumn19.OptionsColumn.AllowShowHide = false;
            this.gridColumn19.OptionsColumn.AllowSize = false;
            this.gridColumn19.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn19.OptionsColumn.FixedWidth = true;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.OptionsColumn.TabStop = false;
            this.gridColumn19.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 5;
            this.gridColumn19.Width = 30;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Tải tập tin", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl1.Location = new System.Drawing.Point(3, 3);
            this.barDockControl1.Size = new System.Drawing.Size(0, 197);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(973, 3);
            this.barDockControl2.Size = new System.Drawing.Size(0, 197);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl3.Location = new System.Drawing.Point(3, 200);
            this.barDockControl3.Size = new System.Drawing.Size(970, 0);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl4.Location = new System.Drawing.Point(3, 3);
            this.barDockControl4.Size = new System.Drawing.Size(970, 0);
            // 
            // frmProductChangeOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 532);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmProductChangeOrder";
            this.ShowIcon = false;
            this.Text = "Yêu cầu xuất đổi hàng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmProductChangeOrder_Load);
            this.panel1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExpiryDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExpiryDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateChange.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateChange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateApprove.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDateApprove.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreateDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpCreateDate.Properties)).EndInit();
            this.tabApprove.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdControlApprove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewApprove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditChoose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.tabAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).EndInit();
            this.mnuAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.mnuAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoSelectMachineError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdExcelMau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewExcelMau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexDetail)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageOutputVoucher.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageInputChangeOrder.ResumeLayout(false);
            this.tabPageInputChangeOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInputchangeDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInputchangeDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpReviewedDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpReviewedDate.Properties)).EndInit();
            this.tabPageReviewList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexReviewLevel)).EndInit();
            this.tabPageWorkflow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexWorkFlow)).EndInit();
            this.tabPageAttach.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.TabPage tabApprove;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox chkIsChanged;
        private System.Windows.Forms.TextBox txtProductChangeTypeId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label2;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStore;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtUserChange;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUserApprove;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBarCode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblTooltip;
        private DevExpress.XtraGrid.GridControl grdControlApprove;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewApprove;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnChoose;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditChoose;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnReviewedStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnReviewedDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnNote;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnCreateOutChange;
        private System.Windows.Forms.Button btnUpdate;
        private DevExpress.XtraEditors.DateEdit dtpCreateDate;
        private DevExpress.XtraEditors.DateEdit dtpDateApprove;
        private DevExpress.XtraEditors.DateEdit dtpDateChange;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelete;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private System.Windows.Forms.TextBox txtReviewedStatus;
        private DevExpress.XtraEditors.DateEdit dtpExpiryDate;
        private DevExpress.XtraEditors.DropDownButton dropDownBtnApprove;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem btnApprove;
        private DevExpress.XtraBars.BarButtonItem btnUnApprove;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private System.Windows.Forms.Timer timerClose;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView grdViewData;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnDataNputVoucherConcernID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grdColIMEI_Out;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grdColIMEI_IN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnIsNew_IN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grdColQuantity;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn grdColNote;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportExcel;
        private GridControl grdExcelMau;
        private GridView grdViewExcelMau;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcelTemplate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repoSelectMachineError;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private System.Windows.Forms.TabPage tabAttachment;
        private GridControl grdAttachment;
        private GridView grvAttachment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colDownload;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDownload;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblInstruction;
        private C1.Win.C1FlexGrid.C1FlexGrid flexDetail;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblTitlePayment;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnSupportCare;
        private System.Windows.Forms.Label lblReviewStatus;
        private System.Windows.Forms.TextBox txtReturnAmount;
        private DevExpress.XtraEditors.DropDownButton btnReviewList;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.DropDownButton btnPrint;
        private System.Windows.Forms.Button btnCreateConcern;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageOutputVoucher;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtReasonNoteOV;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboReasonOV;
        private System.Windows.Forms.LinkLabel lnkCustomer;
        private System.Windows.Forms.TextBox txtCustomerID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore comboBoxStore1;
        private System.Windows.Forms.TextBox txtCustomerPhone;
        private Library.AppControl.ComboBoxControl.ComboBoxInputType cboInputTypeID;
        private System.Windows.Forms.TextBox txtStaffUser;
        private System.Windows.Forms.TextBox txtTaxID;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ctrlStaffUser;
        private System.Windows.Forms.TextBox txtCustomerAddress;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.TextBox txtCustomerIDCard;
        private System.Windows.Forms.TextBox txtContentIOT;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtContentOV;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblOutputContent;
        private System.Windows.Forms.Label lblStore;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.DateTimePicker dtOutputDate;
        private System.Windows.Forms.Label lblOutputDate;
        private System.Windows.Forms.Label lblTaxID;
        private System.Windows.Forms.Label lblInVoiceID;
        private System.Windows.Forms.TextBox txtOutputVoucherID;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblOutputVoucherID;
        private System.Windows.Forms.TextBox txtInVoiceID;
        private System.Windows.Forms.TextBox txtInvoiceSymbol;
        private System.Windows.Forms.TextBox txtInputTypeName;
        private System.Windows.Forms.TextBox txtStoreName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkCreateSaleOrder;
        private System.Windows.Forms.Label lblInvoiceSymbol;
        private System.Windows.Forms.Label lblTotalLiquidate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabPage tabPageInputChangeOrder;
        private System.Windows.Forms.CheckBox chkApplyErrorType;
        private System.Windows.Forms.TextBox txtReasonNote;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtInputChangeOrderID;
        private System.Windows.Forms.TextBox txtSaleOrderID;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.DateEdit dtpInputchangeDate;
        private DevExpress.XtraEditors.DateEdit dtpReviewedDate;
        private System.Windows.Forms.TextBox txtCreateUser;
        private System.Windows.Forms.TextBox txtInputChangeOrderStoreName;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboInputChangeOrderStoreID;
        private System.Windows.Forms.DateTimePicker dtpInputChangeOrderDate;
        private System.Windows.Forms.TextBox txtOutVoucherID;
        private System.Windows.Forms.TextBox txtInVoucherID;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucCreateUser;
        private System.Windows.Forms.TextBox txtNewInputVoucherID;
        private System.Windows.Forms.TextBox txtNewOutputVoucherID;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.LinkLabel lnkSaleOrderID;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox chkIsInputchange;
        private System.Windows.Forms.CheckBox chkIsReviewed;
        private System.Windows.Forms.Label lblContent;
        private System.Windows.Forms.Label label31;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboReason;
        private System.Windows.Forms.TabPage tabPageReviewList;
        private C1.Win.C1FlexGrid.C1FlexGrid flexReviewLevel;
        private System.Windows.Forms.TabPage tabPageWorkflow;
        private C1.Win.C1FlexGrid.C1FlexGrid flexWorkFlow;
        private System.Windows.Forms.TabPage tabPageAttach;
        private GridControl gridControl1;
        private GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private System.Windows.Forms.ContextMenuStrip mnuAttachment;
        private System.Windows.Forms.ToolStripMenuItem mnuAddAttachment;
        private System.Windows.Forms.ToolStripMenuItem mnuDelAttachment;
        private System.Windows.Forms.ComboBox cboInStockStatus_srh;
        private System.Windows.Forms.Label lblStatus;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColInputDate;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
    }
}