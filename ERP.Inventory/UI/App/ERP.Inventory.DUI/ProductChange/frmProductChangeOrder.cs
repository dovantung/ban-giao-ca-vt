﻿using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using ERP.Inventory.PLC.ProductChange;
using ERP.Inventory.PLC.WSProductChangeOrder;
using ERP.Inventory.PLC.WSProductInStock;
using ERP.MasterData.DUI.CustomControl.ImportExcel;
using ERP.MasterData.PLC.MD;
using ERP.MasterData.PLC.WSProductChangeType;
using Library.AppCore;
using Library.AppCore.LoadControls;
using Library.AppCore.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;
using ERP.Inventory.PLC.BorrowProduct;
using ERP.Inventory.PLC.StoreChange;

namespace ERP.Inventory.DUI.ProductChange
{
    public partial class frmProductChangeOrder : Form
    {
        #region Variables

        private enum ReviewedStatus
        {
            DANGXULY = -1,
            DONGY = 1,
            TUCHOI = 0
        }

        private enum ReviewType
        {
            AllApprove = 0,
            OnlyOne = 1
        }

        private DateTime dateServer = new DateTime();
        private int _optionType;
        private int _productChangeType;
        public PLCProductChangeOrder objPLCProductChangeOrder = new PLCProductChangeOrder();
        private PLCProductChangeType objPLCProductChangeType = new PLCProductChangeType();
        private ProductChangeType objProductChangeType = new ProductChangeType();
        public ProductChangeOrder objProductChangeOrder = new ProductChangeOrder();
        public PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private bool IsInsert = false;
        private bool IsUpdate = false;
        private bool IsDeleted = false;
        public DataTable dtbStoreInStockNotIMEI = null;
        public ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private string ProductChangeOrderID = "";
        private ERP.Inventory.PLC.ProductChange.PLCProductChange objPLCProductChange = new PLC.ProductChange.PLCProductChange();
        private int intStoreOld = -1;
        private DataTable dtbKhoxuat = null;

        private DataTable dtbInstockStatus = null;//Trạng thái sản phẩm
        private List<ProductChangeType_PStatus> lstProductChangeType_PStatus = null;

        // private DataTable dtbMachineErrorList = null;

        private StringBuilder stbAttachmentLog = new StringBuilder();
        private List<ProductChangeOrder_ATT> lstProductChangeOrder_Attachment = new List<ProductChangeOrder_ATT>();
        private List<ProductChangeOrder_ATT> lstProductChangeOrder_Attachment_Del = null;
        private bool bolIsChange = false;
        private DataTable dtbViewproductStatus = null;
        private PLCStoreChangeOrder objPLCStoreChangeOrder = new PLCStoreChangeOrder();

        public bool IsChange
        {
            get { return bolIsChange; }
            set { bolIsChange = value; }
        }
        #endregion Variables

        #region Contructor

        public frmProductChangeOrder()
        {
            InitializeComponent();
            IsInsert = true;
        }

        public frmProductChangeOrder(string ProductChangeOrderID, string IsDeleted)
        {
            InitializeComponent();
            if (IsDeleted == "1")
            {
                this.IsDeleted = true;
            }
            else
            {
                IsUpdate = true;
            }
            this.ProductChangeOrderID = ProductChangeOrderID;
        }


        #endregion Contructor

        #region Main

        private void LoadControl()
        {
            try
            {
                DataTable dtbStore = Library.AppCore.DataSource.GetDataFilter.GetStore(true);
                if (dtbStore != null && dtbStore.Rows.Count > 0)
                {
                    var lst = dtbStore.Select("ISCANOUTPUT = 1");
                    if (lst.Any())
                    {
                        cboStore.InitControl(false, lst.CopyToDataTable(), "STOREID", "STORENAME", "--Chọn kho xuất đổi--");
                    }
                    else
                    {
                        cboStore.InitControl(false, dtbStore.Clone(), "STOREID", "STORENAME", "--Chọn kho xuất đổi--");
                    }
                }
                dtbViewproductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
                cboStatus.InitControl(false, dtbViewproductStatus, "PRODUCTSTATUSID", "PRODUCTSTATUSNAME", "-- Chọn trạng thái sản phẩm --");
                dtbInstockStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
                dtbInstockStatus.Columns["PRODUCTSTATUSID"].ColumnName = "INSTOCKSTATUSID";
                dtbInstockStatus.Columns["PRODUCTSTATUSNAME"].ColumnName = "INSTOCKSTATUSNAME";
                dtbInstockStatus.AcceptChanges();
                repositoryItemLookUpEdit1.DataSource = dtbInstockStatus;
                CreateColumnGridReview(false);
                FormatGridReview();


            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable CreateDataReviewedStatus()
        {
            DataTable dtb = new DataTable();
            dtb.Columns.Add("REVIEWEDSTATUSID", typeof(int));
            dtb.Columns.Add("REVIEWEDSTATUSNAME", typeof(string));
            dtb.Rows.Add(-1, "Đang xử lý");
            dtb.Rows.Add(0, "Từ chối");
            dtb.Rows.Add(1, "Đồng ý");
            return dtb;
        }

        private void frmProductChangeOrder_Load(object sender, EventArgs e)
        {
            dateServer = Globals.GetServerDateTime();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewApprove);

            CreateDataTable();
            LoadControl();
            dtpExpiryDate.Properties.MinValue = dateServer;
            #region Deleted

            if (this.IsDeleted)
            {
                lblTooltip.Text = "";
                objProductChangeOrder = null;
                objPLCProductChangeOrder.LoadInfo(ref objProductChangeOrder, this.ProductChangeOrderID);
                //objProductChangeType = null;
                //var objResultMessage = objPLCProductChangeType.LoadInfo(ref objProductChangeType, objProductChangeOrder.ProductChangeTypeID);
                //if (objResultMessage.IsError)
                //{
                //    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                //    return;
                //}
                //else
                //{
                //    if (!SystemConfig.objSessionUser.IsPermission(objProductChangeType.AddFunctionID))
                //    {
                //        MessageBoxObject.ShowWarningMessage(this, "Bạn không có quyền thêm yêu cầu xuất đổi");
                //        timerClose.Start();
                //    }
                //}
                repositoryItemLookUpEdit.DataSource = CreateDataReviewedStatus();
                repositoryItemLookUpEdit.DisplayMember = "REVIEWEDSTATUSNAME";
                repositoryItemLookUpEdit.ValueMember = "REVIEWEDSTATUSID";
                gridColumnChoose.Visible = false;
                cboStore.Enabled = false;
                dtpExpiryDate.Properties.ReadOnly = true;
                dtpExpiryDate.BackColor = SystemColors.Info;
                dtpExpiryDate.Properties.Buttons.Clear();
                grdViewApprove.OptionsBehavior.Editable = false;
                grdViewData.OptionsBehavior.Editable = false;
                mnuAction.Enabled = false;
                txtDescription.ReadOnly = true;
                txtDescription.BackColor = SystemColors.Info;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                btnCreateOutChange.Enabled = false;
                dropDownBtnApprove.Enabled = false;
                txtBarCode.ReadOnly = true;
                txtBarCode.BackColor = SystemColors.Info;
                btnSearch.Enabled = false;
                if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                {
                    var lstDetail = objProductChangeOrder.ProductChangeOrderDT.ToList();
                    grdData.DataSource = lstDetail;
                    grdViewData.RefreshData();
                }

                if (objProductChangeOrder.ProductChangeOrder_RVL != null && objProductChangeOrder.ProductChangeOrder_RVL.Count() > 0)
                {
                    var lstRVL = objProductChangeOrder.ProductChangeOrder_RVL.ToList();
                    grdControlApprove.DataSource = lstRVL;
                    grdViewApprove.RefreshData();
                }
                txtUserApprove.Text = objProductChangeOrder.ReviewedUserFull;
                dtpDateApprove.EditValue = objProductChangeOrder.ReviewedDate;
                txtDescription.Text = objProductChangeOrder.Description.Replace("\n", System.Environment.NewLine);
                txtReviewedStatus.Text = objProductChangeOrder.ReviewedStatus == (int)ReviewedStatus.DANGXULY ? "Đang xử lý" : objProductChangeOrder.ReviewedStatus == (int)ReviewedStatus.TUCHOI ? "Từ chối" : "Đồng ý";
                txtUserChange.Text = objProductChangeOrder.ChangedUserFull;
                cboStore.SetValue(objProductChangeOrder.ProductChangeStoreID);
                dtpCreateDate.EditValue = objProductChangeOrder.CreatedDate;
                dtpExpiryDate.EditValue = objProductChangeOrder.ExpiryDate;
                chkIsChanged.Checked = objProductChangeOrder.IsChanged;
                dtpDateChange.EditValue = objProductChangeOrder.ChangedDate;
                txtProductChangeTypeId.Text = objProductChangeOrder.ProductChangeOrderID;
            }

            #endregion Deleted

            #region Insert

            if (IsInsert)
            {
                repositoryItemLookUpEdit.DataSource = CreateDataReviewedStatus();
                repositoryItemLookUpEdit.DisplayMember = "REVIEWEDSTATUSNAME";
                repositoryItemLookUpEdit.ValueMember = "REVIEWEDSTATUSID";
                cboStore.SetValue(SystemConfig.intDefaultStoreID);
                intStoreOld = SystemConfig.intDefaultStoreID;
                System.Collections.Hashtable hstbParam = ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                this._productChangeType = Convert.ToInt32(hstbParam["PRODUCTCHANGETYPEID"]);
                objProductChangeType = null;
                var objResultMessage = objPLCProductChangeType.LoadInfo(ref objProductChangeType, this._productChangeType);
                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    return;
                }
                else
                {
                    if (!SystemConfig.objSessionUser.IsPermission(objProductChangeType.AddFunctionID))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Bạn không có quyền thêm yêu cầu xuất đổi");
                        timerClose.Start();
                    }
                }
                dtpExpiryDate.EditValue = dateServer.AddDays(10);
                if (SystemConfig.objSessionUser.IsPermission("PM_PRODUCTCHANGE_TIMEOFCHANGE"))
                {
                    dtpExpiryDate.Enabled = true;
                }
                else
                {
                    dtpExpiryDate.Enabled = false;
                    dtpExpiryDate.Properties.Appearance.BackColor = SystemColors.Info;
                }
                dtpCreateDate.EditValue = dateServer;
                btnCreateOutChange.Enabled = false;
                btnDelete.Enabled = false;
                dropDownBtnApprove.Enabled = false;
                dtbStoreInStockNotIMEI = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0, "@IsRequestIMEI", false });
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->frmProductChangeOrder_Load");
                }

                if (objProductChangeType != null)
                {
                    _optionType = objProductChangeType.OptionType;
                    //Loại đổi trạng thái sản phẩm nạp danh sách trạng thái được phép chuyển đổi.
                    if (this._optionType == (int)OptionType.DOITRANGTHAI_SANPHAM)
                    {
                        
                        //this.txtBarCode.Location = new System.Drawing.Point(143, 30);
                        //this.btnSearch.Location = new System.Drawing.Point(333, 29);
                        //this.lblStatus.Location = new System.Drawing.Point(404, 31);
                        //this.cboStatus.Location = new System.Drawing.Point(487, 28);
                        objResultMessage = objPLCProductChangeType.LoadProductStatusTemp(ref lstProductChangeType_PStatus, this._productChangeType);
                        if (objResultMessage.IsError)
                        {
                            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, objResultMessage.Message, objResultMessage.MessageDetail);
                            SystemErrorWS.Insert("Lỗi nạp danh sách trạng thái sản phẩm theo loại yêu cầu đổi trạng thái!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->frmProductChangeOrder_Load");
                        }
                        if (objProductChangeType.IsChangeStatus && (lstProductChangeType_PStatus == null || lstProductChangeType_PStatus.Count == 0))
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy danh sách trạng thái nguồn cho phép chuyển đổi! Vui lòng kiểm tra khai báo.");
                            return;
                        }
                        if (objProductChangeType.IsChangeStatus)
                        {
                            //var lstPStatus = from o in lstProductChangeType_PStatus
                            //                 where o.IsSelect == true
                            //                 select o;
                            //lstProductChangeType_PStatus = lstPStatus.ToList();
                            //cboInStockStatus_srh.DataSource = lstProductChangeType_PStatus;
                            //cboInStockStatus_srh.ValueMember = "ProductStatusIDFrom";
                            //cboInStockStatus_srh.DisplayMember = "ProductStatusName";
                            DataTable dtbViewProductStatusTemp = dtbViewproductStatus.Clone();
                            var lstViewProductStatusTemp = from a in dtbViewproductStatus.AsEnumerable()
                                                           join b in lstProductChangeType_PStatus
                                                           on a["PRODUCTSTATUSID"].ToString().Trim() equals b.ProductStatusIDFrom.ToString().Trim()
                                                           where b.IsSelect == true
                                                           select a;
                            if (lstViewProductStatusTemp.Any())
                                dtbViewProductStatusTemp.Merge(lstViewProductStatusTemp.CopyToDataTable());
                            
                            cboStatus.InitControl(false, dtbViewProductStatusTemp, "PRODUCTSTATUSID", "PRODUCTSTATUSNAME", "-- Chọn trạng thái sản phẩm --");
                        }
                        else
                        {
                            //cboInStockStatus_srh.DataSource = dtbInstockStatus;
                            //cboInStockStatus_srh.ValueMember = "INSTOCKSTATUSID";
                            //cboInStockStatus_srh.DisplayMember = "INSTOCKSTATUSNAME";
                        }
                        //cboInStockStatus_srh.Visible = true;
                        cboStatus.Visible = true;
                        lblStatus.Visible = true;

                    }
                    List<ProductChangeOrder_RVL> lstProductChangeOrder_RVL = new List<ProductChangeOrder_RVL>();
                    foreach (var itemLevel in objProductChangeType.ProductChangeType_RVLevel.OrderBy(x => x.SequenceReview))
                    {
                        int i = 0;
                        //foreach (var itemUser in itemLevel.ProductChangeType_RVL_User)
                        //{
                            ProductChangeOrder_RVL obj = new ProductChangeOrder_RVL();
                            obj.ProductChangeOrderRVLID = Guid.NewGuid().ToString();
                            obj.ReviewLevelID = itemLevel.ReviewLevelID;
                            obj.ReviewLevelName = itemLevel.ReviewLevelName;
                           // obj.UserName = itemUser.UserName;
                           // obj.FullName = itemUser.FullName;
                            obj.ApprovePermission = itemLevel.ApprovePermission;
                            obj.RejectPermission = itemLevel.RejectPermission;
                            obj.StorePermissionType = itemLevel.StorePermissionType;
                            obj.ReviewedStatus = -1;
                            obj.SequenceReview = itemLevel.SequenceReview;
                            //if (itemLevel.ProductChangeType_RVL_User.Count(x => x.ReviewLevelID == itemLevel.ReviewLevelID) == 1)
                            //{
                            //    obj.Check = true;
                            //}
                            //else
                            //{
                            //    obj.Check = itemLevel.ReviewType == (int)ReviewType.AllApprove ? true : false;
                            //}
                            obj.ReviewedStatus = (int)ReviewedStatus.DANGXULY;
                            obj.ReviewType = itemLevel.ReviewType == (int)ReviewType.AllApprove ? "Tất cả phải duyệt" : "Chỉ cần một người duyệt";
                            obj.OrderIndex = i;
                            lstProductChangeOrder_RVL.Add(obj);
                        //    i++;
                        //}
                    }
                    gridColumnChoose.Visible = true;
                    gridColumnNote.Visible = false;
                    gridColumnReviewedDate.Visible = false;
                    gridColumnReviewedStatus.Visible = false;
                    if (lstProductChangeOrder_RVL.Count > 0)
                    {
                        objProductChangeOrder.ProductChangeOrder_RVL = lstProductChangeOrder_RVL.ToArray();
                        grdControlApprove.DataSource = lstProductChangeOrder_RVL;
                        grdViewApprove.RefreshData();
                    }
                    else
                    {
                        grdControlApprove.DataSource = null;
                        grdViewApprove.RefreshData();
                    }

                    if (this._optionType == (int)OptionType.DOISANGSANPHAMKHAC)
                    {
                        // gridColumnIsNew_IN.OptionsColumn.AllowEdit = false;
                    }
                    else if (this._optionType == (int)OptionType.DOISANGIMEIKHAC)
                    {
                        // gridColumnIsNew_IN.OptionsColumn.AllowEdit = false;
                        lblTooltip.Text = "Nhập IMEI cần xuất";
                    }
                    if (this._optionType == (int)OptionType.DOITRANGTHAI_SANPHAM)
                    {
                        // gridColumnIsNew_IN.OptionsColumn.AllowEdit = false;
                        lblTooltip.Text = "Nhập sản phẩm/Imei:";
                    }
                    else
                    {
                        //gridColumnIsNew_IN.OptionsColumn.AllowEdit = true;
                        lblTooltip.Text = "Nhập IMEI cần xuất";
                    }
                }
            }

            #endregion Insert

            #region Update

            if (IsUpdate)
            {
                lblTooltip.Text = "";
                objProductChangeOrder = null;
                objPLCProductChangeOrder.LoadInfo(ref objProductChangeOrder, this.ProductChangeOrderID);

                objProductChangeType = null;
                var objResultMessage = objPLCProductChangeType.LoadInfo(ref objProductChangeType, objProductChangeOrder.ProductChangeTypeID);
                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    return;
                }
                else
                {
                    if (objProductChangeOrder.CreatedUser != SystemConfig.objSessionUser.UserName)
                    {
                        if (!SystemConfig.objSessionUser.IsPermission(objProductChangeType.EditAllFunctionID) && !SystemConfig.objSessionUser.IsPermission(objProductChangeType.DeleteAllFunctionID))
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Bạn không có quyền sửa hoặc hủy yêu cầu xuất đổi");
                            timerClose.Start();
                        }
                    }
                }
                repositoryItemLookUpEdit.DataSource = CreateDataReviewedStatus();
                repositoryItemLookUpEdit.DisplayMember = "REVIEWEDSTATUSNAME";
                repositoryItemLookUpEdit.ValueMember = "REVIEWEDSTATUSID";
                gridColumnChoose.Visible = false;
                cboStore.Enabled = false;
                //dtpExpiryDate.Properties.ReadOnly = true;
                //dtpExpiryDate.BackColor = SystemColors.Info;
                //dtpExpiryDate.Properties.Buttons.Clear();
                grdViewApprove.OptionsBehavior.Editable = false;

                //    grdViewData.OptionsBehavior.Editable = false;

                grdViewData.Columns["MachineErrorGroupName"].OptionsColumn.AllowEdit = true;
                grdViewData.Columns["MachineErrorName"].OptionsColumn.AllowEdit = true;
                grdViewData.Columns["MachineErrorID"].OptionsColumn.AllowEdit = true;

                grdViewData.Columns["NputVoucherConcernID"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["ProductID_Out"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["ProductID_OutName"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["IMEI_Out"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["InStockStatusID_Out"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["ProductID_IN"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["ProductID_INName"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["IMEI_IN"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["InStockStatusID_In"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["Quantity"].OptionsColumn.AllowEdit = false;
                grdViewData.Columns["ChangeNote"].OptionsColumn.AllowEdit = false;

                mnuItemExportExcelTemplate.Enabled = false;
                mnuItemImportExcel.Enabled = false;

                if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                {
                    var lstDetail = objProductChangeOrder.ProductChangeOrderDT.ToList();
                    grdData.DataSource = lstDetail;
                    grdViewData.RefreshData();
                }

                if (objProductChangeOrder.ProductChangeOrder_RVL != null && objProductChangeOrder.ProductChangeOrder_RVL.Count() > 0)
                {
                    var lstRVL = objProductChangeOrder.ProductChangeOrder_RVL.ToList();
                    grdControlApprove.DataSource = lstRVL;
                    grdViewApprove.RefreshData();
                    //var q = from n in lstRVL
                    //        where n.ReviewedDate != null
                    //        group n by n.ProductChangeOrderRVLID into g
                    //        select g.OrderByDescending(t => t.ReviewedDate).FirstOrDefault();
                    //if (q.Any())
                    //{
                    //    var obj = q.First();
                    //    txtUserApprove.Text = obj.FullName;
                    //    dtpDateApprove.EditValue = obj.ReviewedDate;
                    //}
                }
                txtUserApprove.Text = objProductChangeOrder.ReviewedUserFull;
                dtpDateApprove.EditValue = objProductChangeOrder.ReviewedDate;
                txtDescription.Text = objProductChangeOrder.Description.Replace("\n", System.Environment.NewLine);
                txtReviewedStatus.Text = objProductChangeOrder.ReviewedStatus == (int)ReviewedStatus.DANGXULY ? "Đang xử lý" : objProductChangeOrder.ReviewedStatus == (int)ReviewedStatus.TUCHOI ? "Từ chối" : "Đồng ý";
                txtUserChange.Text = objProductChangeOrder.ChangedUserFull;
                cboStore.SetValue(objProductChangeOrder.ProductChangeStoreID);
                dtpCreateDate.EditValue = objProductChangeOrder.CreatedDate;
                dtpExpiryDate.EditValue = objProductChangeOrder.ExpiryDate;
                chkIsChanged.Checked = objProductChangeOrder.IsChanged;
                dtpDateChange.EditValue = objProductChangeOrder.ChangedDate;
                txtProductChangeTypeId.Text = objProductChangeOrder.ProductChangeOrderID;
                if (
                    (dateServer.Date.CompareTo(objProductChangeOrder.ExpiryDate.Value.Date) == 1 && !SystemConfig.objSessionUser.IsPermission("PM_PRODUCTCHANGE_TIMEOFCHANGE")) 
                    || //17/01/2018: bổ sung quyền kiểm tra khác ngay ( anh Mỹ yêu cầu)
                    objProductChangeOrder.ReviewedStatus == (int)ReviewedStatus.TUCHOI
                    )
                {
                    txtDescription.BackColor = SystemColors.Info;
                    txtDescription.ReadOnly = true;
                    btnUpdate.Enabled = false;
                    if (objProductChangeOrder.ReviewedStatus != (int)ReviewedStatus.TUCHOI)
                    {
                        btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(objProductChangeType.DeleteAllFunctionID);
                    }
                    else
                    {
                        btnDelete.Enabled = false;
                    }
                    //btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission("INV_PRODUCTCHANGEORDER_DELETE_REVIEWED");

                    btnCreateOutChange.Enabled = false;
                    dropDownBtnApprove.Enabled = false;
                    mnuItemDelete.Enabled = false;
                    btnSearch.Enabled = false;
                    txtBarCode.BackColor = SystemColors.Info;
                    txtBarCode.ReadOnly = true;
                }
                else
                {
                    if (objProductChangeOrder.CreatedUser == SystemConfig.objSessionUser.UserName)
                    {
                        btnUpdate.Enabled = true;
                        btnDelete.Enabled = true;
                    }
                    else
                    {
                        btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(objProductChangeType.EditAllFunctionID);
                        btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(objProductChangeType.DeleteAllFunctionID);
                    }
                    btnCreateOutChange.Enabled = objProductChangeOrder.CreatedUser == SystemConfig.objSessionUser.UserName && objProductChangeOrder.ReviewedStatus == (int)ReviewedStatus.DONGY ? true : false;
                    if (chkIsChanged.Checked)
                    {
                        btnUpdate.Enabled = false;
                        btnDelete.Enabled = false;
                        btnCreateOutChange.Enabled = false;
                    }
                    if (!btnUpdate.Enabled)
                    {
                        txtDescription.ReadOnly = true;
                        txtDescription.BackColor = SystemColors.Info;
                    }
                    txtBarCode.ReadOnly = true;
                    txtBarCode.BackColor = SystemColors.Info;
                    btnSearch.Enabled = false;
                    if (objProductChangeOrder.ReviewedStatus == (int)ReviewedStatus.DONGY)
                    {
                        btnDelete.Enabled = false;
                        btnDelete.Enabled = !objProductChangeOrder.IsChanged && SystemConfig.objSessionUser.IsPermission("INV_PRODUCTCHANGEORDER_DELETE_REVIEWED");
                    }
                   
                    //int countApprove = objProductChangeOrder.ProductChangeOrder_RVL.Count(x => x.ReviewedStatus != (int)ReviewedStatus.DANGXULY);
                    //if (countApprove == objProductChangeOrder.ProductChangeOrder_RVL.Length)
                    //{
                    //    btnDelete.Enabled = false;
                    //}
                    //var check = objProductChangeOrder.ProductChangeOrder_RVL.Where(x => x.UserName == SystemConfig.objSessionUser.UserName && x.ReviewedStatus == (int)ReviewedStatus.DANGXULY && objProductChangeOrder.CurrentReviewLevelID == x.ReviewLevelID);
                    //if (check.Any())
                    //{
                    //    dropDownBtnApprove.Enabled = true;
                    //}
                    //else
                    //{
                    //    dropDownBtnApprove.Enabled = false;
                    //}
                }

                if (objProductChangeOrder.ReviewedStatus == (int)ReviewedStatus.DANGXULY && btnUpdate.Enabled && SystemConfig.objSessionUser.IsPermission("PM_PRODUCTCHANGE_TIMEOFCHANGE"))
                {
                    dtpExpiryDate.Enabled = true;
                }
                else
                {
                    dtpExpiryDate.Enabled = false;
                    dtpExpiryDate.Properties.Appearance.BackColor = SystemColors.Info;
                }
                if(objProductChangeOrder.ReviewedStatus != (int)ReviewedStatus.DANGXULY)
                {
                    dropDownBtnApprove.Enabled = false;
                }
            }
            #endregion Update

            if (!InitFlexGridAttachment())
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khởi tạo lưới tập tin đính kèm");
            }

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch1 = MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch1.IsMultiSelect = false;
            frmProductSearch1.ShowDialog();
            if (frmProductSearch1.Product != null && Convert.ToString(frmProductSearch1.Product.ProductName).Trim() != string.Empty)
            {
                txtBarCode.Text = Convert.ToString(frmProductSearch1.Product.ProductID).Trim();
                txtBarCode.Focus();
            }
        }
        public bool CheckImeiUsed(string Imei, int intStoreID, string productchangeorderid = "")
        {
            DataTable dtb = new DataTable();
            dtb = objPLCReportDataSource.GetDataSource("PM_PRODUCTCHANGEORDERDT_SRH", new object[] { "@IMEI_OUT", Imei, "@PRODUCTCHANGEORDERID", productchangeorderid, "@StoreID", intStoreID });
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->CheckImeiUsed");
            }
            if (dtb != null && dtb.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        private void txtBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strProductID = string.Empty;
            string strIMEI = string.Empty;
            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();

            if (Convert.ToString(txtBarCode.Text).Trim() != string.Empty && e.KeyChar == (char)Keys.Enter)
            {
                if (!CheckInput())
                {
                    return;
                }

                string strBarcode = ConvertObject.ConvertTextToBarcode(txtBarCode.Text.Trim());
                int intOutputStoreID = cboStore.ColumnID;
                if (!CheckObject.CheckIMEI(strBarcode))
                {
                    MessageBox.Show(this, "IMEI không đúng định dạng.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                switch (this._optionType)
                {
                    #region DOISANGSANPHAMKHAC
                    case (int)OptionType.DOISANGSANPHAMKHAC:
                        {
                            bool IsHadIMEI = false;
                            bool IsOut = true;


                            if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                            {
                                var item = objProductChangeOrder.ProductChangeOrderDT[objProductChangeOrder.ProductChangeOrderDT.Count() - 1];
                                if (!string.IsNullOrEmpty(item.ProductID_OutName) && !string.IsNullOrEmpty(item.ProductID_INName))
                                {
                                    IsOut = true;
                                }
                                else if (!string.IsNullOrEmpty(item.ProductID_OutName))
                                {
                                    IsOut = false;
                                }
                                else
                                {
                                    IsOut = true;
                                }
                            }

                            if (IsOut)//xuất
                            {
                                if (CheckImeiUsed(strBarcode, cboStore.ColumnID))
                                {
                                    MessageBox.Show(this, "IMEI đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                PLC.WSProductInStock.ProductInStock objProductInStock = null;
                                if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, strBarcode, intOutputStoreID, 0, -1, true, true))
                                {
                                    objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, intOutputStoreID);
                                    if (objProductInStock != null)
                                    {
                                        objProductInStock.IMEI = strBarcode;
                                        strProductID = objProductInStock.ProductID;
                                        strIMEI = strBarcode;
                                        IsHadIMEI = true;

                                        if (objProductInStock.IsOrder)
                                        {
                                            MessageBox.Show(this, "IMEI đã được đặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                        }
                                    }
                                }

                                if (strProductID == string.Empty)
                                {
                                    strProductID = strBarcode;
                                }
                                if (objProductInStock == null)
                                {
                                    objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, intOutputStoreID);
                                    if (objProductInStock != null)
                                    {
                                        if (objProductInStock.IsRequestIMEI)
                                        {

                                            MessageBox.Show(this, "Mã sản phẩm " + strProductID + " có yêu cầu nhập IMEI. Vui lòng nhập IMEI có tồn kho hệ thống.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            lblTooltip.Text = "Nhập IMEI cần xuất";
                                            return;
                                        }
                                    }
                                }

                                //Validate product phải được xác nhận nhập kho mới cho xuất đổi IMEI
                                if (objProductInStock != null && !objProductInStock.IsCheckRealInput)
                                {
                                    if (!objProductInStock.IsRequestIMEI)
                                    {
                                        if (objProductInStock.Quantity == 0)
                                        {
                                            MessageBox.Show(this, "SP này chưa được xác nhận nhập kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                        }
                                    }
                                    else
                                    {
                                        if (!objProductInStock.IsCheckRealInput)
                                        {
                                            MessageBox.Show(this, "IMEI này chưa được xác nhận nhập kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                        }
                                    }
                                }

                                if( objProductInStock != null && !objProductChangeType.ProductConsignmentType.Contains(objProductInStock.ConsignmentType))
                                {
                                    MessageBox.Show(this, "IMEI này có loại hàng hóa (Consignmenttype) không nằm trong loại yêu cầu đổi hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                }
                                objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                                if (objProduct == null || Convert.ToString(objProduct.ProductName).Trim() == string.Empty)
                                {
                                    MessageBox.Show(this, "Mã sản phẩm hoặc IMEI không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                //Hiệp sửa 13/07/2017
                                //Điều chỉnh phù hợp cho việc Quét Barcode
                                strProductID = objProduct.ProductID;
                                if (objProduct.IsRequestIMEI)
                                {
                                    if (strIMEI == string.Empty)
                                    {
                                        if (objProductInStock != null && objProductInStock.Quantity > 0)
                                        {
                                            MessageBox.Show(this, "Mã sản phẩm " + strProductID + " có yêu cầu nhập IMEI. Vui lòng nhập IMEI có tồn kho hệ thống.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            lblTooltip.Text = "Nhập IMEI cần xuất";
                                            return;
                                        }
                                        else
                                        {
                                            MessageBox.Show(this, "Hiện tại mã sản phẩm " + strProductID + " không có tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                                        {
                                            var itemExist = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.IMEI_Out.Trim() == strIMEI || (x.IMEI_IN != null && x.IMEI_IN.Trim() == strIMEI));
                                            if (itemExist.Any())
                                            {
                                                MessageBox.Show(this, "IMEI " + strIMEI + " xuất đổi đã được nhập. Vui lòng nhập IMEI khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                return;
                                            }
                                        }

                                        ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                                        decimal decOut = 0;
                                        objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, cboStore.ColumnID, objProductInStock.ProductID, objProductInStock.IMEI);
                                        if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI))
                                        {
                                            if (decOut > 0)
                                            {
                                                MessageBox.Show(this, "IMEI [" + objProductInStock.IMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            if (decOut > 0)
                                            {
                                                objProductInStock.Quantity = objProductInStock.Quantity - decOut;
                                            }
                                            if (objProductInStock.Quantity <= 0)
                                            {
                                                MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductID + " - " + objProductInStock.ProductName + " không tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                return;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    DataRow[] rOutputInStock = dtbStoreInStockNotIMEI.Select("StoreID = " + intOutputStoreID + " AND TRIM(ProductID) = '" + strProductID + "' AND Quantity > 0");

                                    //DataRow[] rOutputInStock = dtbStoreInStockNotIMEI.Select("StoreID = " + intOutputStoreID + " AND TRIM(ProductID) = '" + objProduct.ProductID + "' AND Quantity > 0");
                                    if (rOutputInStock.Length < 1)
                                    {
                                        MessageBox.Show(this, "Mã sản phẩm " + strProductID + " không tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                    if (objProductChangeOrder.ProductChangeOrderDT != null)
                                    {
                                        var itemExistPrdOut = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.ProductID_Out.Trim() == strProductID);
                                        if (itemExistPrdOut.Any())
                                        {
                                            MessageBox.Show(this, "Mã sản phẩm " + strProductID + " xuất đổi đã được nhập. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                    }
                                }

                                string strProductName = Convert.ToString(objProduct.ProductName).Trim();
                                List<ProductChangeOrderDT> lstDetail = null;
                                if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                                {
                                    lstDetail = objProductChangeOrder.ProductChangeOrderDT.ToList();
                                }
                                if (lstDetail == null)
                                {
                                    lstDetail = new List<ProductChangeOrderDT>();
                                }
                                ProductChangeOrderDT objNew = new ProductChangeOrderDT();
                                // objNew.MachineErrorName=
                                objNew.ProductID_Out = strProductID;
                                objNew.InStockStatusID_Out = objProductInStock.InStockStatusID;
                                objNew.InStockStatusID_In = objProductInStock.InStockStatusID;
                                objNew.ProductID_OutName = strProductName;
                                objNew.IMEI_Out = strIMEI;
                                objNew.InputDate = objProductInStock.InputVoucherDate;
                                if (IsHadIMEI)
                                {
                                    objNew.IMEI_IN = strIMEI;
                                }
                                if (strIMEI != string.Empty)
                                {
                                    objNew.IsNew_Out = objProductInStock.IsNew;
                                    objNew.IsNew_IN = objProductInStock.IsNew;
                                    objNew.Quantity = 1;
                                }
                                else
                                {
                                    DataRow[] rOutputInStock = dtbStoreInStockNotIMEI.Select("StoreID = " + intOutputStoreID + " AND TRIM(ProductID) = '" + strProductID + "' AND Quantity > 0");
                                    if (rOutputInStock.Any())
                                    {
                                        objNew.IsNew_Out = rOutputInStock[0]["ISNEW"].ToString() == "1" ? true : false;
                                        objNew.IsNew_IN = rOutputInStock[0]["ISNEW"].ToString() == "1" ? true : false;
                                    }
                                }
                                lstDetail.Add(objNew);
                                objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
                                grdData.DataSource = lstDetail;
                                grdViewData.RefreshData();
                                txtBarCode.Text = string.Empty;
                                txtBarCode.Focus();
                                lblTooltip.Text = "Nhập sản phẩm cần nhập";
                            }
                            else //nhập
                            {
                                var lstDetailCheck = objProductChangeOrder.ProductChangeOrderDT.ToList();
                                var itemCheck = lstDetailCheck[lstDetailCheck.Count() - 1];

                                strIMEI = strBarcode;
                                if (strProductID == string.Empty)
                                {
                                    strProductID = strBarcode;
                                }
                                objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                                if (objProduct == null || Convert.ToString(objProduct.ProductName).Trim() == string.Empty)
                                {
                                    MessageBox.Show(this, "Mã sản phẩm không đúng.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                //lấy dữ liệu của dòng cuối cùng

                                var lstDetail = objProductChangeOrder.ProductChangeOrderDT.ToList();
                                var item = lstDetail[lstDetail.Count() - 1];
                                if (!IsHadIMEI)
                                {
                                    if (item.ProductID_Out.Trim() == objProduct.ProductID.Trim())
                                    {
                                        MessageBox.Show(this, "Sản phẩm nhập phải khác sản phẩm xuất đổi. Vui lòng nhập lại sản phẩm nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                                ERP.MasterData.PLC.MD.WSProduct.Product objProduct_Out = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(item.ProductID_Out.Trim());
                                if (objProduct_Out.IsRequestIMEI != objProduct.IsRequestIMEI)
                                {
                                    if (objProduct_Out.IsRequestIMEI)
                                    {
                                        MessageBox.Show(this, "Sản phẩm nhập phải là sản phẩm có yêu cầu IMEI như sản phẩm xuất đổi. Vui lòng nhập lại sản phẩm nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                    else
                                    {
                                        MessageBox.Show(this, "Sản phẩm nhập phải là sản phẩm không có yêu cầu IMEI như sản phẩm xuất đổi. Vui lòng nhập lại sản phẩm nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }

                                string strProductName = Convert.ToString(objProduct.ProductName).Trim();
                                item.ProductID_IN = strProductID;
                                item.ProductID_INName = strProductName;
                                item.InStockStatusID_In = item.InStockStatusID_Out;
                                //DataRow[] rOutputInStock = dtbStoreInStockNotIMEI.Select("StoreID = " + intOutputStoreID + " AND TRIM(ProductID) = '" + strProductID + "' AND Quantity > 0");
                                //if (rOutputInStock.Any())
                                //{
                                //    item.IsNew_IN = rOutputInStock[0]["ISNEW"].ToString() == "1" ? true : false;
                                //}
                                objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
                                grdData.DataSource = lstDetail;
                                grdViewData.RefreshData();
                                txtBarCode.Text = string.Empty;
                                txtBarCode.Focus();
                                lblTooltip.Text = "Nhập sản phẩm cần xuất";
                            }
                            break;
                        }
                    #endregion
                    #region DOISANGIMEIKHAC
                    case (int)OptionType.DOISANGIMEIKHAC:
                        {
                            bool IsOut = true;

                            if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                            {
                                var item = objProductChangeOrder.ProductChangeOrderDT[objProductChangeOrder.ProductChangeOrderDT.Count() - 1];
                                if (string.IsNullOrEmpty(item.IMEI_IN))
                                {
                                    IsOut = false;
                                }
                                else
                                {
                                    IsOut = true;
                                }
                            }

                            if (IsOut)//xuất
                            {
                                if (CheckImeiUsed(strBarcode, cboStore.ColumnID))
                                {
                                    MessageBox.Show(this, "IMEI đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                PLC.WSProductInStock.ProductInStock objProductInStock = null;
                                if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, strBarcode, intOutputStoreID, 0, -1, true, true))
                                {
                                    objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, intOutputStoreID);
                                    if (objProductInStock != null)
                                    {
                                        objProductInStock.IMEI = strBarcode;
                                        strProductID = objProductInStock.ProductID;
                                        strIMEI = strBarcode;
                                        if (objProductInStock.IsOrder)
                                        {
                                            MessageBox.Show(this, "IMEI đã được đặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(this, "IMEI không tồn kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }

                                //Validate product phải được xác nhận nhập kho mới cho xuất đổi IMEI
                                if (objProductInStock != null)
                                {
                                    if (!objProductInStock.IsRequestIMEI)
                                    {
                                        if (objProductInStock.Quantity == 0)
                                        {
                                            MessageBox.Show(this, "SP này chưa được xác nhận nhập kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                        }
                                    }
                                    else
                                    {
                                        if (!objProductInStock.IsCheckRealInput)
                                        {
                                            MessageBox.Show(this, "IMEI này chưa được xác nhận nhập kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                        }
                                    }
                                }

                                if (objProductInStock != null && !objProductChangeType.ProductConsignmentType.Contains(objProductInStock.ConsignmentType))
                                {
                                    MessageBox.Show(this, "IMEI này có loại hàng hóa (Consignmenttype) không nằm trong loại yêu cầu đổi hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                }

                                objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                                if (objProduct == null || Convert.ToString(objProduct.ProductName).Trim() == string.Empty)
                                {
                                    MessageBox.Show(this, "Mã sản phẩm hoặc IMEI không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                if (objProduct.IsRequestIMEI)
                                {
                                    if (strIMEI == string.Empty)
                                    {
                                        if (objProductInStock != null && objProductInStock.Quantity > 0)
                                        {
                                            MessageBox.Show(this, "Mã sản phẩm " + strProductID + " có yêu cầu nhập IMEI. Vui lòng nhập IMEI có tồn kho hệ thống.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                        else
                                        {
                                            MessageBox.Show(this, "Hiện tại mã sản phẩm " + strProductID + " không có tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                                        {
                                            var itemExist = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.IMEI_Out.Trim() == strIMEI || (x.IMEI_IN != null && x.IMEI_IN.Trim() == strIMEI));
                                            if (itemExist.Any())
                                            {
                                                MessageBox.Show(this, "IMEI " + strIMEI + " xuất đổi đã được nhập. Vui lòng nhập IMEI khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                return;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    DataRow[] rOutputInStock = dtbStoreInStockNotIMEI.Select("StoreID = " + intOutputStoreID + " AND TRIM(ProductID) = '" + strProductID + "' AND Quantity > 0");
                                    if (rOutputInStock.Length < 1)
                                    {
                                        MessageBox.Show(this, "Mã sản phẩm " + strProductID + " không tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                    var itemExistPrdOut = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.ProductID_Out.Trim() == strProductID);
                                    if (itemExistPrdOut.Any())
                                    {
                                        MessageBox.Show(this, "Mã sản phẩm " + strProductID + " xuất đổi đã được nhập. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                                ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                                decimal decOut = 0;
                                objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, cboStore.ColumnID, objProductInStock.ProductID, objProductInStock.IMEI);
                                if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI))
                                {
                                    if (decOut > 0)
                                    {
                                        MessageBox.Show(this, "IMEI [" + objProductInStock.IMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                                else
                                {
                                    if (decOut > 0)
                                    {
                                        objProductInStock.Quantity = objProductInStock.Quantity - decOut;
                                    }
                                    if (objProductInStock.Quantity <= 0)
                                    {
                                        MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductID + " - " + objProductInStock.ProductName + " không tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                                string strProductName = Convert.ToString(objProduct.ProductName).Trim();
                                List<ProductChangeOrderDT> lstDetail = null;
                                if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                                {
                                    lstDetail = objProductChangeOrder.ProductChangeOrderDT.ToList();
                                }
                                if (lstDetail == null)
                                {
                                    lstDetail = new List<ProductChangeOrderDT>();
                                }
                                ProductChangeOrderDT objNew = new ProductChangeOrderDT();
                                objNew.ProductID_Out = strProductID;
                                objNew.ProductID_OutName = strProductName;
                                objNew.IMEI_Out = strIMEI;
                                objNew.ProductID_IN = strProductID;
                                objNew.ProductID_INName = strProductName;
                                objNew.IsNew_Out = objProductInStock.IsNew;
                                objNew.IsNew_IN = objProductInStock.IsNew;
                                objNew.InStockStatusID_Out = objProductInStock.InStockStatusID;
                                objNew.InStockStatusID_In = objProductInStock.InStockStatusID;
                                objNew.Quantity = 1;
                                objNew.InputDate = objProductInStock.InputVoucherDate;
                                lstDetail.Add(objNew);
                                objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
                                grdData.DataSource = lstDetail;
                                grdViewData.RefreshData();
                                txtBarCode.Text = string.Empty;
                                txtBarCode.Focus();
                                lblTooltip.Text = "Nhập IMEI cần nhập";
                            }
                            else //nhập
                            {
                                strIMEI = strBarcode.Trim();
                                if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI))
                                {
                                    MessageBox.Show(this, "IMEI không đúng định dạng.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                var lstDetail = objProductChangeOrder.ProductChangeOrderDT.ToList();
                                var item = lstDetail[lstDetail.Count() - 1];
                                //lấy dữ liệu của dòng cuối cùng
                                var itemExistPrdOut = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.IMEI_Out.Trim() == strIMEI || (x.IMEI_IN != null && x.IMEI_IN.Trim() == strIMEI));
                                if (itemExistPrdOut.Any())
                                {
                                    MessageBox.Show(this, "IMEI sản phẩm \"" + strIMEI + "\" xuất đổi đã được nhập. Vui lòng nhập IMEI sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }

                                string IMEIExist = "";
                                var objResultMessage = objPLCProductChangeOrder.CheckExistIMEISingle(ref IMEIExist, strIMEI, "");
                                if (objResultMessage.IsError)
                                {
                                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(IMEIExist))
                                    {
                                        MessageBoxObject.ShowWarningMessage(this, "IMEI \"" + IMEIExist + "\" đã tồn tại trong hệ thống!");
                                        return;
                                    }
                                }


                                item.IMEI_IN = strIMEI;
                                item.InStockStatusID_In = item.InStockStatusID_Out;
                                objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
                                grdData.DataSource = lstDetail;
                                grdViewData.RefreshData();
                                txtBarCode.Text = string.Empty;
                                txtBarCode.Focus();
                                lblTooltip.Text = "Nhập IMEI cần xuất";
                            }
                            break;
                        }
                    #endregion
                    #region DOITRANGTHAIIMEI
                    case (int)OptionType.DOITRANGTHAIIMEI:
                        {
                            if (CheckImeiUsed(strBarcode, cboStore.ColumnID))
                            {
                                MessageBox.Show(this, "IMEI đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            PLC.WSProductInStock.ProductInStock objProductInStock = null;
                            if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, strBarcode, intOutputStoreID, 0, -1, true, true))
                            {
                                objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, intOutputStoreID);
                                if (objProductInStock != null)
                                {
                                    objProductInStock.IMEI = strBarcode;
                                    strProductID = objProductInStock.ProductID;
                                    strIMEI = strBarcode;
                                    if (objProductInStock.IsOrder)
                                    {
                                        MessageBox.Show(this, "IMEI đã được đặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show(this, "IMEI không tồn kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }


                            //Validate product phải được xác nhận nhập kho mới cho xuất đổi IMEI
                            //Validate product phải được xác nhận nhập kho mới cho xuất đổi IMEI
                            if (objProductInStock != null)
                            {
                                if (!objProductInStock.IsRequestIMEI)
                                {
                                    if (objProductInStock.Quantity == 0)
                                    {
                                        MessageBox.Show(this, "SP này chưa được xác nhận nhập kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                                else
                                {
                                    if (!objProductInStock.IsCheckRealInput)
                                    {
                                        MessageBox.Show(this, "IMEI này chưa được xác nhận nhập kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                    //Loại y/c chỉ cho phép đổi cũ->mới|ngược lại
                                    if (objProductInStock.InStockStatusID > 1)
                                    {
                                        MessageBox.Show(this, "Yêu cầu chỉ cho phép chuyển đổi trạng thái sản phẩm mới/cũ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                            }

                            if (objProductInStock != null && !objProductChangeType.ProductConsignmentType.Contains(objProductInStock.ConsignmentType))
                            {
                                MessageBox.Show(this, "IMEI này có loại hàng hóa (Consignmenttype) không nằm trong loại yêu cầu đổi hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                            }

                            objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                            if (objProduct == null || Convert.ToString(objProduct.ProductName).Trim() == string.Empty)
                            {
                                MessageBox.Show(this, "Mã sản phẩm hoặc IMEI không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            if (objProduct.IsRequestIMEI)
                            {
                                if (strIMEI == string.Empty)
                                {
                                    if (objProductInStock != null && objProductInStock.Quantity > 0)
                                    {
                                        MessageBox.Show(this, "Mã sản phẩm " + strProductID + " có yêu cầu nhập IMEI. Vui lòng nhập IMEI có tồn kho hệ thống.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                    else
                                    {
                                        MessageBox.Show(this, "Hiện tại mã sản phẩm " + strProductID + " không có tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                                else
                                {
                                    if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                                    {
                                        var itemExist = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.IMEI_Out.Trim() == strIMEI || (x.IMEI_IN != null && x.IMEI_IN.Trim() == strIMEI));
                                        if (itemExist.Any())
                                        {
                                            MessageBox.Show(this, "IMEI " + strIMEI + " xuất đổi đã được nhập. Vui lòng nhập IMEI khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                DataRow[] rOutputInStock = dtbStoreInStockNotIMEI.Select("StoreID = " + intOutputStoreID + " AND TRIM(ProductID) = '" + strProductID + "' AND Quantity > 0");
                                if (rOutputInStock.Length < 1)
                                {
                                    MessageBox.Show(this, "Mã sản phẩm " + strProductID + " không tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                if (objProductChangeOrder != null && objProductChangeOrder.ProductChangeOrderDT != null &&
                                    objProductChangeOrder.ProductChangeOrderDT.Any(x => x.ProductID_Out.Trim() == strProductID))
                                {
                                    MessageBox.Show(this, "Mã sản phẩm " + strProductID + " xuất đổi đã được nhập. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                            decimal decOut = 0;
                            objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, cboStore.ColumnID, objProductInStock.ProductID, objProductInStock.IMEI);
                            if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI))
                            {
                                if (decOut > 0)
                                {
                                    MessageBox.Show(this, "IMEI [" + objProductInStock.IMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            else
                            {
                                if (decOut > 0)
                                {
                                    objProductInStock.Quantity = objProductInStock.Quantity - decOut;
                                }
                                if (objProductInStock.Quantity <= 0)
                                {
                                    MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductID + " - " + objProductInStock.ProductName + " không tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            string strProductName = Convert.ToString(objProduct.ProductName).Trim();
                            List<ProductChangeOrderDT> lstDetail = null;
                            if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                            {
                                lstDetail = objProductChangeOrder.ProductChangeOrderDT.ToList();
                            }
                            if (lstDetail == null)
                            {
                                lstDetail = new List<ProductChangeOrderDT>();
                            }
                            ProductChangeOrderDT objNew = new ProductChangeOrderDT();
                            objNew.ProductID_Out = strProductID;
                            objNew.ProductID_OutName = strProductName;
                            objNew.IMEI_Out = strIMEI;
                            objNew.IMEI_IN = strIMEI;
                            objNew.ProductID_IN = strProductID;
                            objNew.ProductID_INName = strProductName;
                            objNew.IsNew_Out = objProductInStock.IsNew;
                            objNew.IsNew_IN = !objProductInStock.IsNew;
                            objNew.InStockStatusID_Out = objProductInStock.InStockStatusID;
                            objNew.InStockStatusID_In = (objProductInStock.IsNew) ? 0 : 1;
                            objNew.Quantity = 1;
                            lstDetail.Add(objNew);
                            objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
                            grdData.DataSource = lstDetail;
                            grdViewData.RefreshData();
                            txtBarCode.Text = string.Empty;
                            txtBarCode.Focus();
                            break;
                        }
                    #endregion


                    #region DOITRANGTHAI_SANPHAM - NGUYEN LINH TUAN BO SUNG
                    case (int)OptionType.DOITRANGTHAI_SANPHAM:
                        {
                            bool IsOut = true;
                            PLC.WSProductInStock.ProductInStock objProductInStock = null;
                            if (objProductChangeType.IsChangeStatus && (lstProductChangeType_PStatus == null || lstProductChangeType_PStatus.Count == 0))
                            {
                                MessageBox.Show(this, "Loại yêu cầu xuất đổi trạng thái không được khai báo trạng thái nguồn - trạng thái đích! Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                            {
                                var item = objProductChangeOrder.ProductChangeOrderDT[objProductChangeOrder.ProductChangeOrderDT.Count() - 1];

                                if (!item.IsRequestIMEI && !string.IsNullOrEmpty(item.ProductID_Out) && !string.IsNullOrEmpty(item.ProductID_IN))
                                {
                                    IsOut = true;
                                }
                                else
                                if (item.IsRequestIMEI && !string.IsNullOrEmpty(item.IMEI_IN) && !string.IsNullOrEmpty(item.IMEI_Out))
                                {
                                    IsOut = true;
                                }
                                else
                                    IsOut = false;
                            }
                            if (IsOut)//xuất
                            {
                                if (CheckImeiUsed(strBarcode, cboStore.ColumnID))
                                {
                                    MessageBox.Show(this, "IMEI đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, intOutputStoreID);
                                if (objProductInStock != null)
                                {
                                    
                                    
                                    if (!objProductInStock.IsRequestIMEI)
                                    {
                                        if (cboStatus.ColumnID < 0)
                                        {
                                            MessageBox.Show(this, "Vui lòng chọn trạng thái sản phãm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            cboStatus.Focus();
                                            return;
                                        }
                                        else
                                        {
                                            objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, intOutputStoreID, 0, string.Empty, false, cboStatus.ColumnID);
                                        }
                                    }
                                    else
                                    {
                                        

                                            //MessageBox.Show(this, "Mã sản phẩm " + strProductID + " có yêu cầu nhập IMEI. Vui lòng nhập IMEI có tồn kho hệ thống.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            //lblTooltip.Text = "Nhập IMEI cần xuất";
                                            //return;
                                        
                                    }

                                    if (objProductInStock != null && !objProductChangeType.ProductConsignmentType.Contains(objProductInStock.ConsignmentType))
                                    {
                                        MessageBox.Show(this, "IMEI này có loại hàng hóa (Consignmenttype) không nằm trong loại yêu cầu đổi hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
                                    }
                                }
                                //objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, intOutputStoreID,0,string.Empty,false,Convert.ToInt32(cboInStockStatus_srh.SelectedValue));
                                if(objProductInStock==null)
                                {
                                    MessageBox.Show(this, "Sản phẩm/IMEI không tồn tại hệ thống!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }                              
                               #region validate
                                    if (objProductInStock.IsOrder)
                                {
                                    MessageBox.Show(this, "IMEI đã được đặt hàng ở đơn hàng có mã ["+objProductInStock.OrderID.Trim()+"].", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                if(objProductInStock.IsRequestIMEI && !objProductInStock.IsCheckRealInput)
                                {
                                    MessageBox.Show(this, "Sản phẩm/IMEI chưa xác nhận nhập kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                if (!objProductInStock.IsRequestIMEI && objProductInStock.Quantity<=0)
                                {
                                    MessageBox.Show(this, "Sản phẩm không tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                if (objProductInStock.IsRequestIMEI && objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                                {
                                    var itemExist = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.IMEI_Out.Trim() == objProductInStock.IMEI.Trim());
                                    if (itemExist.Any())
                                    {
                                        MessageBox.Show(this, "IMEI " + strIMEI + " xuất đổi đã được nhập. Vui lòng nhập IMEI khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                                else
                                if (!objProductInStock.IsRequestIMEI && objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                                {
                                    //var itemExist = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.ProductID_Out.Trim() == objProductInStock.ProductID.Trim() && (x.InStockStatusID_Out == objProductInStock.InStockStatusID));
                                    //if (itemExist.Any())
                                    //{
                                    //MessageBox.Show(this,"Sản phẩm đã tồn tại trong danh sách!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                                    //return;

                                    //}
                                    var itemExist = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.ProductID_Out.Trim() == objProductInStock.ProductID.Trim() && (x.InStockStatusID_Out == objProductInStock.InStockStatusID)).FirstOrDefault();
                                    if (itemExist!=null)
                                    {
                                        //MessageBox.Show(this, "Sản phẩm đã tồn tại trong danh sách!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        //return;
                                        if (objProductInStock.Quantity > itemExist.Quantity)
                                        {
                                            itemExist.Quantity++;
                                            grdData.RefreshDataSource();
                                            txtBarCode.Text = string.Empty;
                                            return;
                                        }
                                        else
                                        {
                                            MessageBox.Show(this, "Sản phẩm đã hết số lượng tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;

                                        }
                                    }

                                }
                                decimal decOut = 0;
                                PLC.BorrowProduct.WSBorrowProduct.ResultMessage objResultMessage = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct().LockOutPut_CHKIMEI(ref decOut, cboStore.ColumnID, objProductInStock.ProductID, objProductInStock.IMEI);
                                if(objResultMessage.IsError)
                                {
                                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this,objResultMessage.Message,objResultMessage.MessageDetail);
                                    return;
                                }
                                if (objProductInStock.IsRequestIMEI==true && 
                                    !string.IsNullOrWhiteSpace(objProductInStock.IMEI))
                                {
                                    if (decOut > 0)
                                    {
                                        MessageBox.Show(this, "IMEI [" + objProductInStock.IMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                                if (!objProductInStock.IsRequestIMEI && decOut > 0)
                                {
                                    objProductInStock.Quantity = objProductInStock.Quantity - decOut;
                                }
                                if (objProductInStock.Quantity <= 0)
                                {
                                    MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductID + " - " + objProductInStock.ProductName + " không tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                if (objProductChangeType.IsChangeStatus && lstProductChangeType_PStatus != null)
                                {
                                    var varStatusTo = lstProductChangeType_PStatus.First(o => o.ProductStatusIDFrom == objProductInStock.InStockStatusID);
                                    if (varStatusTo == null || !varStatusTo.IsSelect)
                                    {
                                        //MessageBox.Show(this, "Không tìm thấy trạng thái sản phẩm đích cần chuyển đổi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        MessageBox.Show(this, "Trạng thái Imei không thuộc trạng thái nguồn cần chuyển đổi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                                        return;
                                    }
                                }
                                
                                #endregion

                                List<ProductChangeOrderDT> lstDetail = null;
                                if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                                {
                                    lstDetail = objProductChangeOrder.ProductChangeOrderDT.ToList();
                                }
                                if (lstDetail == null)
                                {
                                    lstDetail = new List<ProductChangeOrderDT>();
                                }                                
                                ProductChangeOrderDT objNew = new ProductChangeOrderDT();
                                objNew.ProductID_Out = objProductInStock.ProductID;
                                objNew.InStockStatusID_Out = objProductInStock.InStockStatusID;
                                objNew.ProductID_OutName = objProductInStock.ProductName;
                                objNew.IMEI_Out = objProductInStock.IMEI;
                                objNew.IMEI_IN = objProductInStock.IMEI;
                                objNew.IsRequestIMEI = objProductInStock.IsRequestIMEI;
                                objNew.ProductID_IN = objProductInStock.ProductID;
                                objNew.ProductID_INName = objProductInStock.ProductName;
                                if (objProductInStock.IMEI != string.Empty)
                                {
                                    objNew.IsNew_Out = objProductInStock.IsNew;
                                    objNew.IsNew_IN = objProductInStock.IsNew;
                                    objNew.Quantity = 1;
                                }
                                if (!objProductInStock.IsRequestIMEI) objNew.Quantity = 1;
                                if (objProductChangeType.IsChangeStatus)
                                {
                                    var varStatusTo = lstProductChangeType_PStatus.First(o => o.ProductStatusIDFrom == objProductInStock.InStockStatusID);
                                    if (varStatusTo != null)
                                        objNew.InStockStatusID_In = varStatusTo.ProductStatusIDTo;
                                }
                                else
                                    objNew.InStockStatusID_In = objNew.InStockStatusID_Out;                                                            
                                lstDetail.Add(objNew);
                                objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
                                grdData.DataSource = lstDetail;
                                grdViewData.RefreshData();
                                txtBarCode.Text = string.Empty;
                                txtBarCode.Focus();
                                //lblTooltip.Text = "Nhập sản phẩm cần nhập";
                            }                            
                            break;
                        }
                    #endregion
                    default:
                        break;
                }
            }
        }

        private bool CheckInput()
        {
            if (cboStore.ColumnID < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn kho.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboStore.Focus();
                return false;
            }
            return true;
        }

        private void repositoryItemCheckEditChoose_CheckedChanged(object sender, EventArgs e)
        {
            if (grdViewApprove.FocusedRowHandle < 0) return;
            int row = grdViewApprove.FocusedRowHandle;
            var check = sender as CheckEdit;
            if (check.Checked)
            {
                ProductChangeOrder_RVL item = (ProductChangeOrder_RVL)grdViewApprove.GetFocusedRow();
                if (item.ReviewType == "Chỉ cần một người duyệt")
                {
                    var objRVLevel = objProductChangeOrder.ProductChangeOrder_RVL.Where(x => x.ReviewLevelID == item.ReviewLevelID);
                    if (objRVLevel.Any())
                    {
                        foreach (var itemRVL in objRVLevel)
                        {
                            if (itemRVL.ProductChangeOrderRVLID != item.ProductChangeOrderRVLID)
                            {
                                itemRVL.Check = false;
                            }
                            else
                            {
                                itemRVL.Check = true;
                            }
                        }
                    }
                    var objOther = objProductChangeOrder.ProductChangeOrder_RVL.Where(x => x.ReviewLevelID != item.ReviewLevelID);
                    List<ProductChangeOrder_RVL> lstProductChangeOrder_RVL = new List<ProductChangeOrder_RVL>();
                    if (objOther.Any())
                    {
                        lstProductChangeOrder_RVL.AddRange(objRVLevel);
                        lstProductChangeOrder_RVL.AddRange(objOther.OrderBy(x => x.OrderIndex));
                    }
                    else
                    {
                        lstProductChangeOrder_RVL.AddRange(objRVLevel);
                    }
                    objProductChangeOrder.ProductChangeOrder_RVL = lstProductChangeOrder_RVL.ToArray();
                    grdControlApprove.DataSource = lstProductChangeOrder_RVL.OrderBy(x => x.OrderIndex).ThenBy(x => x.SequenceReview);
                    grdViewApprove.RefreshData();
                }
            }
            grdViewApprove.FocusedRowHandle = row;
        }

        private void grdViewApprove_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (grdViewApprove.GetFocusedRow() != null)
            {
                string strReviewType = Convert.ToString(grdViewApprove.GetFocusedRowCellValue("ReviewType").ToString());
                if (strReviewType == "Tất cả phải duyệt")
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnApprove_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Approve(false);
        }

        private void btnUnApprove_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Approve(true);
        }

        private void Approve(bool UnApprove)
        {
            try
            {

                string title = "";
                if (UnApprove)
                {
                    title = "Nhập lý do từ chối";
                }
                else
                {
                    //kiểm tra có imei nào được duyệt trước đó chưa
                    foreach (var item in objProductChangeOrder.ProductChangeOrderDT.Where(x => x.IMEI_Out != null && x.IMEI_Out != ""))
                    {
                        if (CheckImeiUsed(item.IMEI_Out, cboStore.ColumnID, item.ProductChangeOrderID.ToString()))
                        {
                            MessageBox.Show(this, "Sản phẩm có IMEI \"" + item.IMEI_Out + "\" đã được duyệt ở yêu cầu xuất đổi khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    title = "Nhập ghi chú duyệt đồng ý";
                }
                string Reason = ShowReason(title, 600);
                if (Reason == "-1" || string.IsNullOrWhiteSpace(Reason))
                    return;
                string ProductChangeOrderId = objProductChangeOrder.ProductChangeOrderID;
                int ReviewLevelId = objProductChangeOrder.CurrentReviewLevelID;
                string UserName = SystemConfig.objSessionUser.UserName;
                int intStoreID = SystemConfig.intDefaultStoreID;
                //Quyen duyet tren kho
                bool bolFromStorePermission = true;
                bool bolToStorePermission = true;
                string strError = string.Empty;
                if (UserName != "administrator")
                {
                    bool bolResult = objPLCStoreChangeOrder.CheckStorePermission(intStoreID, intStoreID, UserName, ref bolFromStorePermission, ref bolToStorePermission);
                    if (!bolResult)
                    {
                        strError = SystemConfig.objSessionUser.ResultMessageApp.Message;
                        return;
                    }
                }
                int intStorePermission = 0;
                bool check = false;
                for (int i = 0; i < grdViewApprove.RowCount; i++)
                {
                    //SystemConfig.objSessionUser.IsPermission(strPermission_Add);
                    int intCurenReview = 0;
                    Int32.TryParse(grdViewApprove.GetRowCellValue(i, "ReviewedStatus").ToString(), out intCurenReview);
                    if(intCurenReview == -1 && !check ) // đang xử lý
                    {
                        string strApprovePermission = string.Empty;
                        if (grdViewApprove.GetRowCellValue(i, "ApprovePermission") != null)
                        {
                            strApprovePermission = grdViewApprove.GetRowCellValue(i, "ApprovePermission").ToString();
                            if (strApprovePermission != "" && !SystemConfig.objSessionUser.IsPermission(strApprovePermission))
                            {
                                MessageBoxObject.ShowErrorMessage(this, UserName + " không có quyền duyệt");
                                return;
                            }
                        }
                        Int32.TryParse(grdViewApprove.GetRowCellValue(i, grdViewApprove.Columns["StorePermissionType"]).ToString(), out intStorePermission);
                        switch (intStorePermission)
                        {
                            case 1://kho xuat
                                if (!bolFromStorePermission)
                                {
                                    MessageBoxObject.ShowErrorMessage(this, "Bạn không có quyền duyệt trên kho xuất  ");
                                    return;
                                }
                                break;
                            case 2: //kho nhap
                                if (!bolToStorePermission)
                                {
                                    MessageBoxObject.ShowErrorMessage(this, "Bạn không có quyền duyệt trên kho nhập  ");
                                    return;
                                }
                                break;
                            case 3: //ca 2
                                if (bolToStorePermission && bolFromStorePermission)
                                {
                                    string strStoreType = string.Empty;
                                    if (!bolToStorePermission)
                                        strStoreType = "nhập";
                                    else
                                        strStoreType = "xuất";
                                    MessageBoxObject.ShowErrorMessage(this, "Bạn không có quyền duyệt trên kho " + strStoreType + ".");
                                    return;
                                }
                                break;
                            default://khong xet
                                break;
                        }
                        check = true;
                    }
                   

                    //if (intCurenReview != 3)
                    //{
                       
                    //}
                }
                var objResultMessage = objPLCProductChangeOrder.Approve(ProductChangeOrderId, ReviewLevelId, UserName, UnApprove, Reason);

                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Duyệt thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    bolIsChange = true;
                    this.Close();
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi duyệt", objExce.ToString());
                SystemErrorWS.Insert("Lỗi duyệt ", objExce.ToString(), this.Name + " -> Approve()", DUIInventory_Globals.ModuleName);
                return;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckValidate())
                {
                    return;
                }
                if (lstProductChangeOrder_Attachment_Del != null)
                {
                    lstProductChangeOrder_Attachment.InsertRange(lstProductChangeOrder_Attachment.Count, lstProductChangeOrder_Attachment_Del);
                }
                if (!UploadAttachment())
                {
                    if (MessageBox.Show(this, "Upload tập tin thất bại\r\nBạn có muốn tiếp tục lưu thông tin không?", "Thông báo",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }
                }
                if (IsInsert)
                {
                    objProductChangeOrder.ProductChangeTypeID = this._productChangeType;
                    objProductChangeOrder.Description = txtDescription.Text.Trim();
                    objProductChangeOrder.ProductChangeStoreID = cboStore.ColumnID;
                    objProductChangeOrder.ReviewedStatus = (int)ReviewedStatus.DANGXULY;
                    objProductChangeOrder.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    objProductChangeOrder.ExpiryDate = dtpExpiryDate.DateTime;
                    int intMinSequenceReview = 0;
                    try
                    {
                        intMinSequenceReview = objProductChangeType.ProductChangeType_RVLevel.Min(x => x.SequenceReview);
                        var CurrentReviewLevel = objProductChangeType.ProductChangeType_RVLevel.FirstOrDefault(x => x.SequenceReview == intMinSequenceReview);
                        objProductChangeOrder.CurrentReviewLevelID = CurrentReviewLevel.ReviewLevelID;
                    }
                    catch (Exception ex)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Lỗi lấy mức duyệt thấp nhất của loại yêu cầu xuất đổi!");
                        SystemErrorWS.Insert("Lỗi cập nhật ", "Lỗi lấy mức duyệt thấp nhất của loại yêu cầu xuất đổi", this.Name + " -> btnUpdate_Click", DUIInventory_Globals.ModuleName);
                        return;
                    }

                    var lst = objProductChangeOrder.ProductChangeOrder_RVL.Where(x => x.Check);
                    if (lst.Any())
                    {
                        objProductChangeOrder.ProductChangeOrder_RVL = lst.ToArray();
                    }

                    objProductChangeOrder.ProductChangeOrder_ATTList = lstProductChangeOrder_Attachment.ToArray();

                    var objResultMessage = objPLCProductChangeOrder.Insert(objProductChangeOrder);

                    if (objResultMessage.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    }
                    else
                    {
                        MessageBox.Show("Thêm mới yêu cầu xuất đổi thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ResetForm();
                    }
                }
                if (IsUpdate)
                {
                    objProductChangeOrder.Description = txtDescription.Text.Trim();
                    objProductChangeOrder.ExpiryDate = dtpExpiryDate.DateTime;
                    objProductChangeOrder.ProductChangeOrder_ATTList = lstProductChangeOrder_Attachment.ToArray();
                    var objResultMessage = objPLCProductChangeOrder.Update(objProductChangeOrder);

                    if (objResultMessage.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    }
                    else
                    {
                        MessageBox.Show("Cập nhật yêu cầu xuất đổi thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        bolIsChange = true;
                        this.Close();
                    }
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi cập nhật", objExce.ToString());
                SystemErrorWS.Insert("Lỗi cập nhật ", objExce.ToString(), this.Name + " -> btnUpdate_Click", DUIInventory_Globals.ModuleName);
                return;
            }
        }

        private void ResetForm()
        {
            dtpExpiryDate.EditValue = null;
            cboStore.ResetDefaultValue();
            txtReviewedStatus.Text = string.Empty;
            dtpDateApprove.EditValue = null;
            txtUserApprove.Text = string.Empty;
            txtUserChange.Text = string.Empty;
            dtpDateChange.EditValue = null;
            chkIsChanged.Checked = false;

            List<ProductChangeOrderDT> lstDetail = new List<ProductChangeOrderDT>();
            objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
            grdData.DataSource = lstDetail;
            grdViewData.RefreshData();
            tabControl.SelectedTab = tabGeneral;
            txtDescription.Text = string.Empty;
            gridColumnChoose.Visible = true;
            gridColumnChoose.VisibleIndex = 4;
            //var lstRVLTemp = objProductChangeOrder.ProductChangeOrder_RVL.ToList();
            //var lstRVL = objProductChangeOrder.ProductChangeOrder_RVL.Where(x => x.ReviewType == "Chỉ cần một người duyệt");
            //foreach (var item in lstRVL)
            //{
            //    item.Check = false;
            //}
            //lstRVLTemp.RemoveAll(x => x.ReviewType == "Chỉ cần một người duyệt");
            //lstRVLTemp.AddRange(lstRVL);
            List<ProductChangeOrder_RVL> lstProductChangeOrder_RVL = new List<ProductChangeOrder_RVL>();
            foreach (var itemLevel in objProductChangeType.ProductChangeType_RVLevel.OrderBy(x => x.SequenceReview))
            {
                int i = 0;
                //foreach (var itemUser in itemLevel.ProductChangeType_RVL_User)
                //{
                    ProductChangeOrder_RVL obj = new ProductChangeOrder_RVL();
                    obj.ProductChangeOrderRVLID = Guid.NewGuid().ToString();
                    obj.ReviewLevelID = itemLevel.ReviewLevelID;
                    obj.ReviewLevelName = itemLevel.ReviewLevelName;
                    //obj.UserName = itemUser.UserName;
                   // obj.FullName = itemUser.FullName;
                    obj.SequenceReview = itemLevel.SequenceReview;
                    //if (itemLevel.ProductChangeType_RVL_User.Count(x => x.ReviewLevelID == itemLevel.ReviewLevelID) == 1)
                    //{
                    //    obj.Check = true;
                    //}
                    //else
                    //{
                    //    obj.Check = itemLevel.ReviewType == (int)ReviewType.AllApprove ? true : false;
                    //}
                    obj.ReviewedStatus = (int)ReviewedStatus.DANGXULY;
                    obj.ReviewType = itemLevel.ReviewType == (int)ReviewType.AllApprove ? "Tất cả phải duyệt" : "Chỉ cần một người duyệt";
                    obj.OrderIndex = i;
                    lstProductChangeOrder_RVL.Add(obj);
                //    i++;
                //}
            }
            objProductChangeOrder.ProductChangeOrder_RVL = lstProductChangeOrder_RVL.ToArray();
            grdControlApprove.DataSource = lstProductChangeOrder_RVL;
            grdViewApprove.RefreshData();

            dtpExpiryDate.EditValue = dateServer.AddDays(10);
            if (!InitFlexGridAttachment())
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khởi tạo lưới tập tin đính kèm");
            }
        }

        private bool CheckValidate()
        {
            if (dtpExpiryDate.EditValue == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn hạn xuất đổi!");
                dtpExpiryDate.Focus();
                return false;
            }
            if (dtpExpiryDate.DateTime.Date <= DateTime.Now.Date)
            {
                MessageBoxObject.ShowWarningMessage(this, "Hạn xuất đổi phải lớn hơn ngày hiện tại!");
                dtpExpiryDate.Focus();
                return false;
            }
            if (dtpExpiryDate.DateTime.Date <= dtpCreateDate.DateTime.Date)
            {
                MessageBoxObject.ShowWarningMessage(this, "Hạn xuất đổi phải lớn hơn ngày yêu cầu xuất đổi!");
                dtpExpiryDate.Focus();
                return false;
            }
            if (cboStore.ColumnID == -1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho xuất đổi!");
                cboStore.Focus();
                return false;
            }
            if (objProductChangeOrder.ProductChangeOrderDT == null || objProductChangeOrder.ProductChangeOrderDT.Count() == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập dữ liệu xuất đổi!");
                txtBarCode.Focus();
                return false;
            }
            else
            {
                var check = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.ProductID_IN == null || x.ProductID_IN == "");
                if (check.Any())
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập sản phẩm cần nhập!");
                    this.tabControl.SelectedTab = tabGeneral;
                    txtBarCode.Focus();
                    return false;
                }

                var check2 = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.IMEI_Out != null && x.IMEI_Out != "" && (x.IMEI_IN == null || x.IMEI_IN == ""));
                if (check2.Any())
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập IMEI sản phẩm cần nhập!");
                    this.tabControl.SelectedTab = tabGeneral;
                    txtBarCode.Focus();
                    return false;
                }

                var productNoIMEI = objProductChangeOrder.ProductChangeOrderDT.Where(x => (x.IMEI_Out == null || x.IMEI_Out == "") && x.Quantity <= 0);
                if (productNoIMEI.Any())
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập số lượng > 0!");
                    this.tabControl.SelectedTab = tabGeneral;
                    grdViewData.FocusedColumn = grdColQuantity;
                    ProductChangeOrderDT[] lstProductNoIMEI= productNoIMEI.ToArray();
                    ProductChangeOrderDT objProductNoIMEI = lstProductNoIMEI[0];
                    int iRow=objProductChangeOrder.ProductChangeOrderDT.ToList().IndexOf(objProductNoIMEI);
                    grdViewData.FocusedRowHandle = iRow;
                    grdViewData.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewData.ShowEditor(); });
                    return false;
                }

                if (this._optionType == (int)OptionType.DOISANGIMEIKHAC && IsInsert)
                {
                    var checkDuplicateIMEIIN = objProductChangeOrder.ProductChangeOrderDT.GroupBy(x => x.IMEI_IN).Select(g => new { Value = g.Key, Count = g.Count() }).Where(x => x.Count > 1);
                    if (checkDuplicateIMEIIN.Any())
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Không được nhập trùng IMEI nhập!");
                        return false;
                    }

                    string IMEIExist = "";
                    var objResultMessage = objPLCProductChangeOrder.CheckExistIMEI(ref IMEIExist, objProductChangeOrder.ProductChangeOrderDT.ToList());

                    if (objResultMessage.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(IMEIExist))
                        {
                            MessageBoxObject.ShowWarningMessage(this, "IMEI \"" + IMEIExist + "\" đã tồn tại trong hệ thống!");
                            return false;
                        }
                    }
                }
                if (this._optionType == (int)OptionType.DOITRANGTHAI_SANPHAM)
                {
                    var checkErrorGroup = objProductChangeOrder.ProductChangeOrderDT.Where(x => (x.InStockStatusID_In == 3 || x.InStockStatusID_In == 5) && string.IsNullOrEmpty(x.MachineErrorGroupName));
                    if (checkErrorGroup.Any() && objProductChangeType.OptionType == 4)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn lỗi, nhóm lỗi");
                        return false;
                    }
                }
                if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                {
                    var lstCheckQuantityNotVoucher = objProductChangeOrder.ProductChangeOrderDT.ToList();
                    for (int i = 0; i < lstCheckQuantityNotVoucher.Count(); i++)
                    {
                        ProductChangeOrderDT obj = lstCheckQuantityNotVoucher[i];
                        if (string.IsNullOrWhiteSpace(obj.ChangeNote))
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập ghi chú!");
                            grdViewData.FocusedColumn = grdColNote;
                            grdViewData.FocusedRowHandle = i;
                            grdViewData.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewData.ShowEditor(); });
                            return false;
                        }
                        else
                        {
                            if (obj.ChangeNote.Length > 1000)
                            {
                                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập lại ghi chú vì vượt quá giới hạn ký tự cho phép!");
                                grdViewData.FocusedColumn = grdColNote;
                                grdViewData.FocusedRowHandle = i;
                                grdViewData.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewData.ShowEditor(); });
                                return false;
                            }
                        }
                        if (string.IsNullOrWhiteSpace(obj.NputVoucherConcernID))
                        {
                            ProductInStock objProductInstock = null;
                            //MessageBoxObject.ShowWarningMessage(this, "Kiểm tra tôn mã sản phẩm " + obj.ProductID_Out
                            //        + " trạng thái tồn " + obj.InStockStatusID_Out.ToString()
                            //        + ", Trang thái 2 " + cboStatus.ColumnID.ToString());
                            if (obj != null && !string.IsNullOrEmpty(obj.IMEI_Out))
                                objProductInstock = objPLCProductInStock.LoadInfo(obj.IMEI_Out, cboStore.ColumnID, obj.InStockStatusID_Out);
                            else if (obj != null && !string.IsNullOrEmpty(obj.ProductID_Out))
                                objProductInstock = objPLCProductInStock.LoadInfo(obj.ProductID_Out, cboStore.ColumnID, obj.InStockStatusID_Out);

                            if (objProductInstock == null)
                            {
                                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy tồn kho mã sản phẩm " + obj.ProductID_Out);
                                return false;
                            }
                            //MessageBoxObject.ShowWarningMessage(this, "Kiểm tra tôn mã sản phẩm " + obj.ProductID_Out
                            //       + " trạng thái tồn " + obj.InStockStatusID_Out.ToString()
                            //       + ", Trang thái 2 " + cboStatus.ColumnID.ToString()
                            //       + ", TON " + objProductInstock.Quantity()
                            //       + ", YC " + obj.Quantity.ToString()
                            //       );

                            if (objProductInstock != null && objProductInstock.Quantity < obj.Quantity)
                            {
                                MessageBoxObject.ShowWarningMessage(this, "Số lượng của sản phẩm có mã \"" + obj.ProductID_Out + "\" phải nhỏ hơn hoặc bằng " + objProductInstock.Quantity + "!");
                                grdViewData.FocusedColumn = grdColQuantity;
                                grdViewData.FocusedRowHandle = i;
                                grdViewData.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewData.ShowEditor(); });
                                return false;
                            }
                        }
                        else
                        {
                            DataTable dtb = new DataTable();
                            if (string.IsNullOrEmpty(obj.IMEI_Out))
                            {
                                dtb = objPLCReportDataSource.GetDataSource("PM_INPUTVOUCHERDETAIL_SRH", new object[] { "@INPUTVOUCHERID", obj.NputVoucherConcernID });
                                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                                {
                                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->CheckValidate");
                                }
                            }
                            else
                            {
                                dtb = objPLCReportDataSource.GetDataSource("PM_INPUTVOUCHERDETAILIMEI_SRH2", new object[] { "@INPUTVOUCHERID", obj.NputVoucherConcernID, "@IMEI", obj.IMEI_Out });
                                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                                {
                                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->CheckValidate");
                                }
                            }
                            if (dtb != null)
                            {
                                var lst = dtb.Select("PRODUCTID = '" + obj.ProductID_Out + "'");
                                if (lst.Any())
                                {
                                    if (this._optionType == (int)OptionType.DOISANGSANPHAMKHAC)
                                    {
                                        int intQuantity = -1;
                                        int.TryParse(lst[0]["QUANTITY"].ToString(), out intQuantity);
                                        if (intQuantity < obj.Quantity)
                                        {
                                            MessageBoxObject.ShowWarningMessage(this, "Số lượng của sản phẩm có mã \"" + obj.ProductID_Out + "\" phải nhỏ hơn hoặc bằng " + intQuantity + "!");
                                            grdViewData.FocusedColumn = grdColQuantity;
                                            grdViewData.FocusedRowHandle = i;
                                            grdViewData.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewData.ShowEditor(); });
                                            return false;
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBoxObject.ShowWarningMessage(this, "IMEI \"" + obj.IMEI_Out + "\" không tồn tại trong số phiếu nhập gốc \"" + obj.NputVoucherConcernID + "\"!");
                                    grdViewData.FocusedColumn = gridColumnDataNputVoucherConcernID;
                                    grdViewData.FocusedRowHandle = i;
                                    grdViewData.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewData.ShowEditor(); });
                                    return false;
                                }
                            }
                            else
                            {
                                if (this._optionType == (int)OptionType.DOISANGSANPHAMKHAC && string.IsNullOrEmpty(obj.IMEI_Out))
                                {
                                    MessageBoxObject.ShowWarningMessage(this, "Số phiếu nhập gốc \"" + obj.NputVoucherConcernID + "\" không tồn tại!");
                                    grdViewData.FocusedColumn = gridColumnDataNputVoucherConcernID;
                                    grdViewData.FocusedRowHandle = i;
                                    grdViewData.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewData.ShowEditor(); });
                                    return false;
                                }
                                else //if (this._optionType == (int)OptionType.DOISANGIMEIKHAC || this._optionType == (int)OptionType.DOITRANGTHAIIMEI)
                                {
                                    MessageBoxObject.ShowWarningMessage(this, "IMEI \"" + obj.IMEI_Out + "\" không tồn tại trong số phiếu nhập gốc \"" + obj.NputVoucherConcernID + "\"!");
                                    grdViewData.FocusedColumn = gridColumnDataNputVoucherConcernID;
                                    grdViewData.FocusedRowHandle = i;
                                    grdViewData.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewData.ShowEditor(); });
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            if (objProductChangeOrder.ProductChangeOrder_RVL != null && objProductChangeOrder.ProductChangeOrder_RVL.Count() > 0)
            {
                if (IsInsert)
                {
                    //var lstReviewTypeOnlyOne = objProductChangeOrder.ProductChangeOrder_RVL.Where(x => x.ReviewType == "Chỉ cần một người duyệt");
                    //if (lstReviewTypeOnlyOne.Any())
                    //{
                    //    var lstReviewIDDistinct = (from n in objProductChangeOrder.ProductChangeOrder_RVL
                    //                               select new
                    //                               {
                    //                                   n.ReviewLevelID
                    //                               }).Distinct();
                    //    if (lstReviewIDDistinct.Any())
                    //    {
                    //        foreach (var item in lstReviewIDDistinct)
                    //        {
                    //            var checkOnlyOne = objProductChangeOrder.ProductChangeOrder_RVL.Where(x => x.ReviewLevelID == item.ReviewLevelID && x.Check == true);
                    //            if (!checkOnlyOne.Any())
                    //            {
                    //                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn người duyệt!");
                    //                tabControl.SelectedTab = tabApprove;
                    //                return false;
                    //            }
                    //        }
                    //    }
                    //}
                }
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Không có dữ liệu người duyệt!");
                tabControl.SelectedTab = tabApprove;
                return false;
            }
            return true;
        }

        private void repositoryItemCheckEditIsNew_IN_CheckedChanged(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0) return;
            int row = grdViewData.FocusedRowHandle;
            var check = sender as CheckEdit;
            ProductChangeOrderDT item = (ProductChangeOrderDT)grdViewData.GetFocusedRow();

            var lst = objProductChangeOrder.ProductChangeOrderDT.ToList();
            if (lst.Any())
            {
                foreach (var itemDetail in lst)
                {
                    if (itemDetail.IMEI_IN == item.IMEI_IN)
                    {
                        if (check.Checked)
                        {
                            itemDetail.IsNew_IN = true;
                            itemDetail.IsNew_Out = false;
                            break;
                        }
                        else
                        {
                            itemDetail.IsNew_IN = false;
                            itemDetail.IsNew_Out = true;
                            break;
                        }
                    }
                }
                objProductChangeOrder.ProductChangeOrderDT = lst.ToArray();
                grdData.DataSource = lst;
                grdViewData.RefreshData();
                grdViewData.FocusedRowHandle = row;
            }
        }

        private void grdViewData_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (grdViewData.GetFocusedRow() != null)
            {
                bool IsHadIMEI = false;
                string strIMEI_Out = Convert.ToString(grdViewData.GetFocusedRowCellValue("IMEI_Out").ToString());
                if (strIMEI_Out != "")
                {
                    IsHadIMEI = true;
                }
                #region OptionType.DOISANGSANPHAMKHAC
                if (this._optionType == (int)OptionType.DOISANGSANPHAMKHAC || _optionType == (int)OptionType.DOITRANGTHAI_SANPHAM)
                {
                    if (IsHadIMEI && (grdViewData.FocusedColumn.FieldName == "Quantity"))
                    {
                        e.Cancel = true;
                    }
                    else if (grdViewData.FocusedColumn.FieldName == "NputVoucherConcernID" || grdViewData.FocusedColumn.FieldName == "Quantity")
                    {
                        e.Cancel = false;
                    }
                    if (grdViewData.FocusedColumn.FieldName == "IMEI_IN")
                    {
                        e.Cancel = true;
                    }
                }
                #endregion

                #region OptionType.DOISANGIMEIKHAC
                else if (this._optionType == (int)OptionType.DOISANGIMEIKHAC)
                {
                    if (grdViewData.FocusedColumn.FieldName == "NputVoucherConcernID")
                    {
                        e.Cancel = false;
                    }
                    if (grdViewData.FocusedColumn.FieldName == "Quantity" || grdViewData.FocusedColumn.FieldName == "IMEI_IN")
                    {
                        e.Cancel = true;
                    }
                }
                #endregion
                #region OptionType.DOITRANGTHAI_SANPHAM
                else if (this._optionType == (int)OptionType.DOITRANGTHAI_SANPHAM)
                {
                    if (grdViewData.FocusedColumn.FieldName == "NputVoucherConcernID")
                    {
                        e.Cancel = false;
                    }
                    if (grdViewData.FocusedColumn.FieldName == "Quantity")
                    {
                        ProductChangeOrderDT obj = (ProductChangeOrderDT)grdViewData.GetFocusedRow();
                        if (!obj.IsRequestIMEI)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
                #endregion
                else
                {
                    if (grdViewData.FocusedColumn.FieldName == "NputVoucherConcernID"
                        )
                    {
                        e.Cancel = false;
                    }
                    if (grdViewData.FocusedColumn.FieldName == "Quantity"
                        || grdViewData.FocusedColumn.FieldName == "IMEI_IN" || grdViewData.FocusedColumn.FieldName == "IsNew_IN")
                    {
                        e.Cancel = true;
                    }
                }
                if (grdViewData.FocusedColumn.FieldName == "MachineErrorName")
                {
                    if ((Convert.ToInt32(grdViewData.GetFocusedRowCellValue("InStockStatusID_In")) != 5 && Convert.ToInt32(grdViewData.GetFocusedRowCellValue("InStockStatusID_In")) != 3) || objProductChangeType.OptionType != (int)OptionType.DOITRANGTHAI_SANPHAM)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private string ShowReason(string strTitle, int MaxLengthString)
        {
            string strReasion = "-1";
            Library.AppControl.FormCommon.frmInputString frm = new Library.AppControl.FormCommon.frmInputString();
            frm.MaxLengthString = MaxLengthString;
            frm.IsAllowEmpty = false;
            frm.Text = strTitle;
            frm.ShowDialog();
            if (frm.IsAccept)
            {
                strReasion = frm.ContentString;
            }

            return strReasion;
        }

        private void btnCreateOutChange_Click(object sender, EventArgs e)
        {
            btnCreateOutChange.Enabled = false;

            try
            {
                if (MessageBox.Show(this, "Bạn có chắc muốn xuất đổi hàng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    btnUpdate.Enabled = true;
                    return;
                }

                var lstCheckQuantityNotVoucher = objProductChangeOrder.ProductChangeOrderDT.Where(x => x.IMEI_Out != null || x.IMEI_Out != "").ToList();
                for (int i = 0; i < lstCheckQuantityNotVoucher.Count(); i++)
                {
                    ProductChangeOrderDT obj = lstCheckQuantityNotVoucher[i];
                    if (!string.IsNullOrWhiteSpace(obj.IMEI_Out))
                    {
                        var objProductInStock = objPLCProductInStock.LoadInfo(obj.IMEI_Out, cboStore.ColumnID);
                        if (objProductInStock == null)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Sản phẩm xuất đã bị xuất đổi trước đó!");
                            this.Close();
                            return;
                        }
                        //if (objProductInStock.IsNew != obj.IsNew_Out)
                        if(objProductInStock.InStockStatusID!= obj.InStockStatusID_Out)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Sản phẩm xuất đã bị thay đổi trước đó!");
                            this.Close();
                            return;
                        }

                        if (objProductInStock.ProductID != obj.ProductID_Out && objProductInStock.IMEI != obj.IMEI_Out)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Sản phẩm xuất đã bị thay đổi trước đó!");
                            this.Close();
                            return;
                        }

                        ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                        decimal decOutPutQuantity = 0;// số lượng sản phẩm cho mượn//
                        objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOutPutQuantity, cboStore.ColumnID, obj.ProductID_Out, obj.IMEI_Out);

                        if (decOutPutQuantity > 0)
                        {
                            MessageBox.Show(this, "IMEI [" + obj.IMEI_Out + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            this.Close();
                            return;
                        }
                    }
                    else
                    {
                        PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(obj.ProductID_Out, cboStore.ColumnID);
                        if (objProductInStock != null)
                        {
                            ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                            decimal decOutPutQuantity = 0;// số lượng sản phẩm cho mượn//
                            objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOutPutQuantity, cboStore.ColumnID, obj.ProductID_Out, obj.IMEI_Out);

                            if (decOutPutQuantity > 0)
                            {
                                decimal decQuantityCanUse = objProductInStock.Quantity - decOutPutQuantity;
                                if (decQuantityCanUse <= 0)
                                {
                                    MessageBox.Show(this, "Sản phẩm " + obj.ProductID_Out + " - " + obj.ProductID_OutName + " không tồn kho. Hoặc không lấy được IMEI tự động!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    this.Close();
                                    return;
                                }
                                if (decQuantityCanUse < obj.Quantity)
                                {
                                    MessageBox.Show(this, "Sản phẩm " + obj.ProductID_Out + " - " + obj.ProductID_OutName + " số lượng cho phép xuất bán tối đa là " + (objProductInStock.Quantity - decOutPutQuantity).ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    this.Close();
                                    return;
                                }
                                //if (obj.Quantity > objProductInStock.Quantity - decOutPutQuantity)
                                //{
                                //    MessageBox.Show(this, "Sản phẩm " + obj.ProductID_Out + " - " + obj.ProductID_OutName + " số lượng cho phép xuất bán tối đa là " + (objProductInStock.Quantity - decOutPutQuantity).ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                //    this.Close();
                                //    return;
                                //}
                                //else if (obj.Quantity < objProductInStock.Quantity - decOutPutQuantit && objProductInStock.Quantity - decOutPutQuantit > 0)
                                //{

                                //}
                                //else
                                //{
                                //    MessageBox.Show(this, "Sản phẩm " + obj.ProductID_Out + " - " + obj.ProductID_OutName + " không tồn kho. Hoặc không lấy được IMEI tự động!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                //    this.Close();
                                //    return;
                                //}
                            }
                        }
                    }
                }

                int intStoreID = cboStore.ColumnID;
                string strUserName = Library.AppCore.SystemConfig.objSessionUser.UserName;
                int intCreateStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                DateTime dtmOutputDate = dateServer;
                DateTime dtmInputDate = dateServer;
                string strContent = txtDescription.Text.Trim();

                int intCustomerID = 0;
                int intPayableTypeID = 0;
                int intInputTypeID = 0;
                int intOutputTypeID = 0;
                int intCurrencyUnitID = 0;
                decimal decCurrencyExchange = 0;
                //string strStaffUser = string.Empty;

                DataTable dtbCustomer = Library.AppCore.DataSource.GetDataSource.GetCustomer();
                if (dtbCustomer != null && dtbCustomer.Rows.Count > 0)
                {
                    var item = dtbCustomer.Select("ISDEFAULT = 1");
                    if (item.Count() < 1)
                    {
                        MessageBox.Show(this, "Không tìm thấy thông tin khách hàng mặc định", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    int.TryParse(item[0]["CUSTOMERID"].ToString(), out intCustomerID);
                }
                intPayableTypeID = -1;//tạm thời lấy hình thức thanh toán mặc định
                intInputTypeID = objProductChangeType.InputTypeID;
                intOutputTypeID = objProductChangeType.OutputTypeID;

                DataTable dtbCurrencyUnit = Library.AppCore.DataSource.GetDataSource.GetCurrencyUnit();
                if (dtbCurrencyUnit != null && dtbCurrencyUnit.Rows.Count > 0)
                {
                    var item = dtbCurrencyUnit.Select("ISDEFAULT = 1");
                    int.TryParse(item[0]["CURRENCYUNITID"].ToString(), out intCurrencyUnitID);
                }
                DataTable dtbOutputType = Library.AppCore.DataSource.GetDataSource.GetOutputType();
                bool IsVATZero = false;
                if (dtbOutputType != null && dtbOutputType.Rows.Count > 0)
                {
                    var item = dtbOutputType.Select("OUTPUTTYPEID = " + objProductChangeType.OutputTypeID);
                    if (item.Any())
                    {
                        IsVATZero = item[0]["ISVATZERO"].ToString() == "1" ? true : false;
                    }
                }

                DataTable dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct();

                ERP.MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit objCurrencyUnit = new MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit();
                ERP.MasterData.PLC.MD.PLCCurrencyUnit objPLCCurrencyUnit = new MasterData.PLC.MD.PLCCurrencyUnit();
                objPLCCurrencyUnit.LoadInfo(ref objCurrencyUnit, intCurrencyUnitID);
                if (objCurrencyUnit != null)
                {
                    decCurrencyExchange = objCurrencyUnit.CurrencyExchange;
                }
                else
                {
                    DataTable tblCurrencyUnit = null;
                    objPLCCurrencyUnit.SearchData(ref tblCurrencyUnit, new object[] { });
                    if (tblCurrencyUnit != null && tblCurrencyUnit.Rows.Count > 0)
                    {
                        DataRow[] rCurrencyUnit = tblCurrencyUnit.Select("CurrencyUnitName = 'VND'");
                        if (rCurrencyUnit.Length > 0)
                        {
                            intCurrencyUnitID = Convert.ToInt32(rCurrencyUnit[0]["CurrencyUnitID"]);
                            decCurrencyExchange = Convert.ToDecimal(rCurrencyUnit[0]["CurrencyExchange"]);
                        }
                    }
                }

                ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                ERP.MasterData.PLC.MD.PLCCustomer objPLCCustomer = new MasterData.PLC.MD.PLCCustomer();
                objPLCCustomer.LoadInfo(ref objCustomer, intCustomerID);

                ERP.Inventory.PLC.ProductChange.WSProductChange.ProductChange objProductChange = new PLC.ProductChange.WSProductChange.ProductChange();
                ERP.Inventory.PLC.ProductChange.WSProductChange.InputVoucher objInputVoucher = new PLC.ProductChange.WSProductChange.InputVoucher();
                ERP.Inventory.PLC.ProductChange.WSProductChange.OutputVoucher objOutputVoucher = new PLC.ProductChange.WSProductChange.OutputVoucher();

                objProductChange.StoreID = intStoreID;
                objProductChange.ProductChangeUser = strUserName;
                objProductChange.ProductChangeDate = dtmOutputDate;
                objProductChange.InVoiceDate = dateServer;
                objProductChange.Content = txtDescription.Text.Trim();
                objProductChange.CreatedStoreID = intCreateStoreID;

                objInputVoucher.IsCheckRealInput = true;
                objInputVoucher.CheckRealInputUser = strUserName;
                objInputVoucher.CheckRealInputTime = dtmInputDate;
                objInputVoucher.InputTypeID = intInputTypeID;
                objInputVoucher.InVoiceDate = dateServer;
                objInputVoucher.InputDate = dtmInputDate;
                objInputVoucher.PayableDate = dtmInputDate;
                objInputVoucher.CustomerID = intCustomerID;
                objInputVoucher.CustomerName = objCustomer.CustomerName;
                objInputVoucher.CustomerAddress = objCustomer.CustomerAddress;
                objInputVoucher.CustomerPhone = objCustomer.CustomerPhone;
                objInputVoucher.CustomerIDCard = objCustomer.CustomerIDCard;
                objInputVoucher.CustomerTaxID = objCustomer.CustomerTaxID;
                objInputVoucher.CustomerEmail = objCustomer.CustomerEmail;
                objInputVoucher.CreatedStoreID = intCreateStoreID;
                objInputVoucher.InputStoreID = intStoreID;
                objInputVoucher.PayableTypeID = intPayableTypeID;
                objInputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objInputVoucher.CurrencyExchange = decCurrencyExchange;
                objProductChange.InputVoucherBO = objInputVoucher;

                objOutputVoucher.CreatedStoreID = intCreateStoreID;
                objOutputVoucher.OutputStoreID = intStoreID;
                objOutputVoucher.CustomerID = intCustomerID;
                objOutputVoucher.CustomerName = objCustomer.CustomerName;
                objOutputVoucher.CustomerAddress = objCustomer.CustomerAddress;
                objOutputVoucher.TaxCustomerAddress = objCustomer.TaxCustomerAddress;
                objOutputVoucher.CustomerPhone = objCustomer.CustomerPhone;
                objOutputVoucher.CustomerTaxID = objCustomer.CustomerTaxID;
                objOutputVoucher.OutputContent = strContent;
                objOutputVoucher.InvoiceDate = dateServer;
                objOutputVoucher.OutputDate = dtmOutputDate;
                objOutputVoucher.PayableTypeID = intPayableTypeID;
                objOutputVoucher.PayableDate = dtmOutputDate;
                objOutputVoucher.StaffUser = strUserName;
                objOutputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objOutputVoucher.CurrencyExchange = decCurrencyExchange;
                objProductChange.OutputVoucherBO = objOutputVoucher;

                List<PLC.ProductChange.WSProductChange.InputVoucherDetail> objInputVoucherDetailList = new List<PLC.ProductChange.WSProductChange.InputVoucherDetail>();
                List<PLC.ProductChange.WSProductChange.OutputVoucherDetail> objOutputVoucherDetailList = new List<PLC.ProductChange.WSProductChange.OutputVoucherDetail>();
                List<PLC.ProductChange.WSProductChange.ProductChangeDetail> objProductChangeDetailList = new List<PLC.ProductChange.WSProductChange.ProductChangeDetail>();

                var lstDetail = objProductChangeOrder.ProductChangeOrderDT.OrderBy(x => x.ProductID_Out).OrderBy(x => x.ProductID_IN).ToList();

                for (int i = 0; i < lstDetail.Count(); i++)
                {
                    ProductChangeOrderDT r = lstDetail[i];
                    string strProductID_Out = string.Empty;
                    string strProductID_In = string.Empty;
                    string strIMEI_Out = string.Empty;
                    string strIMEI_In = string.Empty;
                    decimal decQuantity = 0;
                    int intVAT = 0;
                    int intVATPercent = 0;
                    strProductID_Out = r.ProductID_Out;
                    strProductID_In = r.ProductID_IN;
                    if (r.IMEI_Out != null)
                        strIMEI_Out = r.IMEI_Out;
                    if (r.IMEI_IN != null)
                        strIMEI_In = r.IMEI_IN;
                    decQuantity = r.Quantity;
                    var item = dtbProduct.Select("PRODUCTID = '" + r.ProductID_Out + "'");
                    if (item.Any())
                    {
                        if (IsVATZero)
                        {
                            intVAT = 0;
                        }
                        else
                        {
                            intVAT = Convert.ToInt32(item[0]["VAT"]);
                        }
                        intVATPercent = Convert.ToInt32(item[0]["VATPERCENT"]);
                    }

                    #region ProductChangeDetail

                    PLC.ProductChange.WSProductChange.ProductChangeDetail objProductChangeDetail = new PLC.ProductChange.WSProductChange.ProductChangeDetail();
                    objProductChangeDetail.StoreID = intStoreID;
                    objProductChangeDetail.ProductChangeDate = dtmOutputDate;
                    objProductChangeDetail.ProductID_Out = strProductID_Out;
                    objProductChangeDetail.ProductID_In = strProductID_In;
                    objProductChangeDetail.IMEI_Out = strIMEI_Out;
                    objProductChangeDetail.IMEI_In = strIMEI_In;
                    objProductChangeDetail.Quantity = decQuantity;
                    objProductChangeDetail.OutputTypeID = intOutputTypeID;
                    objProductChangeDetail.InputTypeID = intInputTypeID;
                    objProductChangeDetail.CreatedStoreID = intCreateStoreID;
                    objProductChangeDetail.InStockStatusID_in = r.InStockStatusID_In;
                    objProductChangeDetail.InStockStatusID_Out = r.InStockStatusID_Out;
                    //objProductChangeDetail.MachineErrorID = r.MachineErrorID; Xóa theo yêu cầu Nam 14/03/2018
                    objProductChangeDetailList.Add(objProductChangeDetail);

                    #endregion ProductChangeDetail

                    #region InputVoucherDetail

                    PLC.ProductChange.WSProductChange.InputVoucherDetail objInputVoucherDetail = new PLC.ProductChange.WSProductChange.InputVoucherDetail();
                    objInputVoucherDetail.ProductID = strProductID_In;
                    objInputVoucherDetail.Quantity = decQuantity;
                    objInputVoucherDetail.VAT = intVAT;
                    objInputVoucherDetail.VATPercent = intVATPercent;
                    objInputVoucherDetail.IsNew = r.IsNew_IN;
                    objInputVoucherDetail.CreatedStoreID = intCreateStoreID;
                    objInputVoucherDetail.InputStoreID = intStoreID;
                    objInputVoucherDetail.InputDate = dtmInputDate;
                    objInputVoucherDetail.FirstInputDate = dtmInputDate;
                    objInputVoucherDetail.FirstCustomerID = intCustomerID;
                    objInputVoucherDetail.FirstInputTypeID = intInputTypeID;
                    objInputVoucherDetail.ProductChangeDate = dtmInputDate;
                    int intMonthOfWarranty = ERP.MasterData.PLC.MD.PLCWarrantyMonthByProduct.GetWarrantyMonthByProduct(objInputVoucherDetail.ProductID, objInputVoucherDetail.IsNew);
                    objInputVoucherDetail.ENDWarrantyDate = dateServer.AddMonths(intMonthOfWarranty);

                    if (strIMEI_In != string.Empty)
                    {
                        if (strIMEI_Out == strIMEI_In && strProductID_Out == strProductID_In)
                        {
                            objInputVoucherDetail.ChangeToOLDDate = dtmInputDate;
                        }
                        PLC.ProductChange.WSProductChange.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.ProductChange.WSProductChange.InputVoucherDetailIMEI();
                        objInputVoucherDetailIMEI.IMEI = strIMEI_In;
                        objInputVoucherDetailIMEI.CreatedStoreID = intCreateStoreID;
                        objInputVoucherDetailIMEI.InputDate = dtmInputDate;
                        objInputVoucherDetailIMEI.InputStoreID = intStoreID;

                        List<PLC.ProductChange.WSProductChange.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = new List<PLC.ProductChange.WSProductChange.InputVoucherDetailIMEI>();
                        objInputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                        objInputVoucherDetail.InputVoucherDetailIMEIList = objInputVoucherDetailIMEIList.ToArray();
                        objInputVoucherDetail.Quantity = Convert.ToDecimal(objInputVoucherDetail.InputVoucherDetailIMEIList.Length);
                    }
                    objInputVoucherDetailList.Add(objInputVoucherDetail);

                    #endregion InputVoucherDetail

                    #region OutputVoucherDetail

                    PLC.ProductChange.WSProductChange.OutputVoucherDetail objOutputVoucherDetail = new PLC.ProductChange.WSProductChange.OutputVoucherDetail();
                    objOutputVoucherDetail.ProductID = strProductID_Out;
                    objOutputVoucherDetail.IMEI = strIMEI_Out;
                    objOutputVoucherDetail.Quantity = decQuantity;
                    objOutputVoucherDetail.VAT = intVAT;
                    objOutputVoucherDetail.VATPercent = intVATPercent;
                    objOutputVoucherDetail.IsNew = r.IsNew_Out;
                    objOutputVoucherDetail.CreatedStoreID = intCreateStoreID;
                    objOutputVoucherDetail.OutputStoreID = intStoreID;
                    objOutputVoucherDetail.OutputTypeID = intOutputTypeID;
                    objOutputVoucherDetail.OutputDate = dtmOutputDate;
                    objOutputVoucherDetail.InputChangeDate = dtmOutputDate;

                    objOutputVoucherDetailList.Add(objOutputVoucherDetail);

                    #endregion OutputVoucherDetail
                }

                objProductChange.ProductChangeDetailList = objProductChangeDetailList.ToArray();
                objProductChange.InputVoucherBO.InputVoucherDetailList = objInputVoucherDetailList.ToArray();
                objProductChange.OutputVoucherBO.OutputVoucherDetailList = objOutputVoucherDetailList.ToArray();

                objProductChange.InputVoucherBO.IsNew = objInputVoucherDetailList[0].IsNew;
                objProductChange.IsNew = objInputVoucherDetailList[0].IsNew;
                objProductChange.ProductChangeOrderID = objProductChangeOrder.ProductChangeOrderID;
                if (!objPLCProductChange.InsertMasterAndDetail(objProductChange))
                {
                    btnUpdate.Enabled = true;
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                }
                MessageBox.Show(this, "Xuất đổi hàng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsChange = true;
                this.Close();
                //dtbStoreInStockNotIMEI = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0, "@IsRequestIMEI", false });
                //btnUpdate.Enabled = true;
            }
            catch(Exception objExe)
            {
                MessageBox.Show(this, "Lỗi lấy thông tin xuất đổi hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi lấy thông tin xuất đổi hàng", objExe.ToString(), this.Name + "-> btnCreateOutChange_Click", DUIInventory_Globals.ModuleName);
                btnUpdate.Enabled = true;
            }
            finally
            {
                btnCreateOutChange.Enabled = true;
            }
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (objProductChangeOrder != null && objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
                {
                    Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi xuất excel thông tin sản phấm yêu cầu xuất đổi");
                SystemErrorWS.Insert("Lỗi xuất excel thông tin sản phấm yêu cầu xuất đổi", objExce.ToString(), this.Name + " -> mnuItemExportExcel_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdViewData.FocusedRowHandle < 0)
                {
                    return;
                }

                ProductChangeOrderDT item = (ProductChangeOrderDT)grdViewData.GetFocusedRow();
                var lstDetail = objProductChangeOrder.ProductChangeOrderDT.ToList();
                lstDetail.Remove(item);

                objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
                grdData.DataSource = lstDetail;
                grdViewData.RefreshData();
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi hủy sản phẩm", objExce.ToString());
                SystemErrorWS.Insert("Lỗi hủy sản phẩm ", objExce.ToString(), this.Name + " -> mnuItemDelete_Click", DUIInventory_Globals.ModuleName);
                return;
            }
        }

        private void grdViewApprove_CellMerge(object sender, DevExpress.XtraGrid.Views.Grid.CellMergeEventArgs e)
        {
            GridView view = sender as GridView;
            try
            {
                if ((e.Column.FieldName == "ReviewLevelName"))
                {
                    string value1 = Convert.ToString(view.GetRowCellValue(e.RowHandle1, e.Column));
                    string value2 = Convert.ToString(view.GetRowCellValue(e.RowHandle2, e.Column));
                    if (value1 == value2)
                    {
                        value1 = Convert.ToString(view.GetRowCellValue(e.RowHandle1, view.Columns.ColumnByFieldName("ReviewLevelName")));
                        value2 = Convert.ToString(view.GetRowCellValue(e.RowHandle2, view.Columns.ColumnByFieldName("ReviewLevelName")));
                        e.Merge = (value1 == value2);
                        e.Handled = true;
                        return;
                    }
                }
                if ((e.Column.FieldName == "SequenceReview"))
                {
                    string value1 = Convert.ToString(view.GetRowCellValue(e.RowHandle1, e.Column));
                    string value2 = Convert.ToString(view.GetRowCellValue(e.RowHandle2, e.Column));
                    if (value1 == value2)
                    {
                        value1 = Convert.ToString(view.GetRowCellValue(e.RowHandle1, view.Columns.ColumnByFieldName("SequenceReview")));
                        value2 = Convert.ToString(view.GetRowCellValue(e.RowHandle2, view.Columns.ColumnByFieldName("SequenceReview")));
                        e.Merge = (value1 == value2);
                        e.Handled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            if (IsDeleted || IsUpdate)
            {
                mnuItemDelete.Enabled = false;
                return;
            }
            //if (dateServer.Date.CompareTo(objProductChangeOrder.ExpiryDate.Value.Date) == 1 || objProductChangeOrder.ReviewedStatus == (int)ReviewedStatus.TUCHOI)
            //{
            //    mnuItemDelete.Enabled = false;
            //    return;
            //}
            //if (chkIsChanged.Checked)
            //{
            //    mnuItemDelete.Enabled = false;
            //    mnuItemExportExcel.Enabled = true;
            //    return;
            //}
            if (grdViewData.FocusedRowHandle < 0)
            {
                mnuItemExportExcel.Enabled = false;
                mnuItemDelete.Enabled = false;
            }
            if (objProductChangeOrder != null && objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
            {
                mnuItemExportExcel.Enabled = true;
                mnuItemDelete.Enabled = true;
            }
            else
            {
                mnuItemExportExcel.Enabled = false;
                mnuItemDelete.Enabled = false;
            }
        }

        private void cboStore_SelectionChangeCommitted(object sender, EventArgs e)
        {

            if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Count() > 0)
            {
                if (MessageBox.Show(this, "Bạn có muốn thay đổi kho không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    cboStore.SetValue(intStoreOld);
                    return;
                }
                intStoreOld = cboStore.ColumnID;
                List<ProductChangeOrderDT> lstDetail = new List<ProductChangeOrderDT>();
                objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
                grdData.DataSource = lstDetail;
                grdViewData.RefreshData();
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string strRejectPermission = string.Empty;
                if (grdViewApprove.GetRowCellValue(0, "RejectPermission") != null)
                {
                    strRejectPermission = grdViewApprove.GetRowCellValue(0, "RejectPermission").ToString();
                    if (strRejectPermission != "" && !SystemConfig.objSessionUser.IsPermission(strRejectPermission))
                    {
                        MessageBoxObject.ShowErrorMessage(this, SystemConfig.objSessionUser.UserName + " không có quyền hủy");
                        return;
                    }
                }
                if (MessageBox.Show(this, "Bạn có chắc muốn hủy yêu cầu xuất đổi hàng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    return;
                }
                var objResultMessage = objPLCProductChangeOrder.Delete(objProductChangeOrder);

                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                }
                else
                {
                    MessageBox.Show("Hủy yêu cầu xuất đổi thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    bolIsChange = true;
                    this.Close();
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi hủy yêu cầu xuất đổi", objExce.ToString());
                SystemErrorWS.Insert("Lỗi hủy yêu cầu xuất đổi ", objExce.ToString(), this.Name + " -> Approve()", DUIInventory_Globals.ModuleName);
                return;
            }
        }

        #endregion Main
        private void timerClose_Tick(object sender, EventArgs e)
        {
            this.Close();
        }
        private void mnuItemImportExcel_Click(object sender, EventArgs e)
        {
            if (objProductChangeOrder.ProductChangeOrderDT != null && objProductChangeOrder.ProductChangeOrderDT.Any())
            {
                if (MessageBox.Show("Có dữ liệu tồn tại trên lưới\nBạn có muốn thay đổi không?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
            }

            if (cboStore.ColumnID == -1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho xuất đổi");
                cboStore.Focus();
                return;
            }
            ImportProductChange importer = new ImportProductChange((OptionType)this._optionType, lstProductChangeType_PStatus, objProductChangeType.IsChangeStatus, this, cboStore.ColumnID);

            importer.Import();
            grdData.DataSource = objProductChangeOrder.ProductChangeOrderDT;
        }
        private DataTable ToDataTable<T>(IList<T> list)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor prop = properties[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[properties.Count];
            foreach (T item in list)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = properties[i].GetValue(item) ?? DBNull.Value;
                table.Rows.Add(values);
            }
            return table;
        }


        #region Export excel template
        public void CreateColumnTemplateImport()
        {
            grdViewExcelMau.Columns.Clear();
            List<DataColumnInfor> columns = new List<DataColumnInfor>();
            switch ((OptionType)this._optionType)
            {
                case OptionType.DOISANGIMEIKHAC:
                    {
                        columns = new List<DataColumnInfor>() {
                            new DataColumnInfor("IMEI_Out", "IMEI [xuất]", typeof(string), 160, true, 2, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("IMEI_IN", "IMEI [nhập]", typeof(string), 160, true, 6, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("ChangeNote", "Ghi chú", typeof(string), 250, true, 10, DataColumnInfor.TypeShow.ALL)
                        }; break;
                    }
                case OptionType.DOISANGSANPHAMKHAC:
                    {
                        columns = new List<DataColumnInfor>() {
                            new DataColumnInfor("ProductID_Out", "Mã SP [xuất]", typeof(string), 160, true, 1, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("IMEI_Out", "IMEI [xuất]", typeof(string), 160, true, 2, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("ProductID_IN", "Mã SP [nhập]", typeof(string), 160, true, 5, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("Quantity", "Số lượng", typeof(string), 70, true, 9, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("ChangeNote", "Ghi chú", typeof(string), 250, true, 10, DataColumnInfor.TypeShow.ALL)
                        }; break;
                    }
                case OptionType.DOITRANGTHAIIMEI:
                    {
                        columns = new List<DataColumnInfor>() {
                            new DataColumnInfor("IMEI_Out", "IMEI [xuất]", typeof(string), 160, true, 2, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("ChangeNote", "Ghi chú", typeof(string), 250, true, 10, DataColumnInfor.TypeShow.ALL)

                        }; break;
                    }
                case OptionType.DOITRANGTHAI_SANPHAM:
                    {
                        columns = new List<DataColumnInfor>() {
                            new DataColumnInfor("IMEI_Out", "IMEI [xuất]", typeof(string), 160, true, 2, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("ChangeNote", "Ghi chú", typeof(string), 250, true, 10, DataColumnInfor.TypeShow.ALL)

                        }; break;
                    }
                default: { throw new Exception("Product change type undefine"); }
            }
            foreach (var column in columns)
            {
                CreateColumn(grdViewExcelMau, column);
            }
        }
        private void CreateColumn(GridView objGridView, DataColumnInfor objDataColumnInfor)
        {
            DevExpress.XtraGrid.Columns.GridColumn column;
            column = objGridView.Columns.AddField(objDataColumnInfor.FieldName);
            column.Caption = objDataColumnInfor.Caption;
            column.Width = objDataColumnInfor.Width;
            column.Visible = objDataColumnInfor.Visible;
            column.VisibleIndex = objDataColumnInfor.VisibleIndex;

            if (objDataColumnInfor.ItemRepository != null)
                column.ColumnEdit = objDataColumnInfor.ItemRepository;

            column.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            column.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            column.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            column.AppearanceHeader.Options.UseTextOptions = true;
            column.AppearanceHeader.Options.UseFont = true;

            column.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            column.AppearanceCell.Options.UseFont = true;

            column.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            column.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;

            column.OptionsColumn.AllowEdit = false;
            column.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            column.OptionsColumn.AllowMove = false;
        }
        private void mnuItemExportExcelTemplate_Click(object sender, EventArgs e)
        {
            CreateColumnTemplateImport();
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdExcelMau, DevExpress.XtraPrinting.TextExportMode.Text, true);
        }
        #endregion

        private void repoSelectMachineError_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Search.frmMachineErrorSearch frmMachineErrorSearch = new Search.frmMachineErrorSearch();
            frmMachineErrorSearch.Multiselect = true;
            //if (!Convert.IsDBNull(flexDetail.Rows[1]["MACHINEERRORGROUPID"]))
            //{
            //    int intMachineErrorGroupId = 0;
            //    int.TryParse(flexDetail.Rows[1]["MACHINEERRORGROUPID"].ToString(), out intMachineErrorGroupId);

            //    frmMachineErrorSearch.ListMachineErrorGroupId = intMachineErrorGroupId;
            //}
            if (frmMachineErrorSearch.ShowDialog() == DialogResult.OK)
            {
                string strErrorContent = string.Empty;
                string strErrorId = string.Empty;
                string strErrorGroupContent = string.Empty;
                string strErrorGroupId = string.Empty;
                if (frmMachineErrorSearch.ListErrorGroup != null && frmMachineErrorSearch.ListErrorGroup.Count > 0)
                {
                    foreach (var item in frmMachineErrorSearch.ListErrorGroup)
                    {
                        strErrorGroupContent += item.MACHINEERRORGROUPNAME + "; ";
                        strErrorGroupId += item.MACHINEERRORGROUPID + ";";
                    }
                    strErrorGroupContent = strErrorGroupContent.Substring(0, strErrorGroupContent.Length - 2);
                    if (frmMachineErrorSearch.lstMachineError != null && frmMachineErrorSearch.lstMachineError.Count > 0)
                    {
                        foreach (var er in frmMachineErrorSearch.lstMachineError)
                        {
                            strErrorContent += er.MachineErrorName + "; ";
                            strErrorId += er.MachineErrorId + "; ";
                            //flexDetail[e.Row, "MACHINEERRORGROUPNAME"] = er.MachineErrorGroupName;
                            //flexDetail[e.Row, "MACHINEERRORID"] = er.MachineErrorId;
                            //flexDetail[e.Row, "MACHINEERRORGROUPID"] = er.MachineErrorGroupId;
                        }
                        strErrorContent = strErrorContent.Substring(0, strErrorContent.Length - 2);
                        strErrorId = strErrorId.Substring(0, strErrorId.Length - 2);

                        grdViewData.SetFocusedValue(strErrorContent);
                        ProductChangeOrderDT objProductChangeOrderDT = (ProductChangeOrderDT)grdViewData.GetRow(grdViewData.FocusedRowHandle);
                        objProductChangeOrderDT.MachineErrorGroupName = strErrorGroupContent;
                        objProductChangeOrderDT.MachineErrorID = strErrorId;
                    }
                    grdViewData.RefreshData();
                }
            }
        }

        private bool InitFlexGridAttachment()
        {
            try
            {
                lstProductChangeOrder_Attachment = new List<ProductChangeOrder_ATT>();
                if (ProductChangeOrderID != string.Empty)
                {
                    DataTable tblAttachment = objPLCProductChangeOrder.GetAttachment(new object[] { "@ProductChangeOrderID", ProductChangeOrderID });
                    if (tblAttachment != null && tblAttachment.Rows.Count > 0)
                    {
                        int intSTT = 0;
                        for (int i = 0; i < tblAttachment.Rows.Count; i++)
                        {
                            DataRow rAttachment = tblAttachment.Rows[i];
                            ProductChangeOrder_ATT objAttachment = new ProductChangeOrder_ATT();
                            objAttachment.AttachMentID = Convert.ToString(rAttachment["AttachmentID"]).Trim();
                            objAttachment.ProductChangeOrderID = ProductChangeOrderID;
                            objAttachment.AttachMentName = Convert.ToString(rAttachment["AttachmentName"]);
                            objAttachment.AttachMentPath = Convert.ToString(rAttachment["AttachmentPath"]);
                            objAttachment.FileID = Convert.ToString(rAttachment["FileID"]);
                            objAttachment.Description = Convert.ToString(rAttachment["Description"]);
                            objAttachment.CreatedDate = Convert.ToDateTime(rAttachment["CreatedDate"]);
                            objAttachment.CreatedUser = Convert.ToString(rAttachment["CreatedUser"]);
                            objAttachment.CreatedFullName = Convert.ToString(rAttachment["CreatedFullName"]);
                            objAttachment.STT = ++intSTT;
                            lstProductChangeOrder_Attachment.Add(objAttachment);
                        }
                    }
                }
                else
                    grvAttachment.Columns[grvAttachment.Columns.Count - 1].Visible = false;
                if (btnUpdate.Enabled)
                    mnuAttachment.Enabled = true;
                else
                    mnuAttachment.Enabled = false;
                grdAttachment.DataSource = lstProductChangeOrder_Attachment;
                grdAttachment.RefreshDataSource();
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo tập tin đính kèm", objExce, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            ProductChangeOrder_ATT objAttachment = (ProductChangeOrder_ATT)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
            if (objAttachment.IsAddNew == true)
            {
                MessageBox.Show(this, "File chưa cập nhật trên server!");
                return;
            }
            //if (!bolIsHasPermission_ViewAttachment)
            //{
            //    MessageBox.Show(this, "Bạn không có quyền xem file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}
            DownloadFile(objAttachment.AttachMentName, objAttachment.FileID);
        }

        private bool UploadAttachment()
        {
            if (lstProductChangeOrder_Attachment == null || lstProductChangeOrder_Attachment.Count < 1)
                return true;
            //Tạo trạng thái chờ trong khi đính kèm tập tin
            try
            {
                if (lstProductChangeOrder_Attachment != null && lstProductChangeOrder_Attachment.Count > 0)
                {
                    var CheckIsNew = from o in lstProductChangeOrder_Attachment
                                     where o.IsAddNew == true
                                     select o;
                    if (CheckIsNew.Count() <= 0)
                    {
                        return true;
                    }
                    string strFilePath = string.Empty;
                    string strFileID = string.Empty;
                    foreach (ProductChangeOrder_ATT objAttachment in CheckIsNew)
                    {
                        string strResult = ERP.FMS.DUI.DUIFileManager.UploadFile(this, "FMSAplication_ProERP_ProductChangeOrderAttachment", objAttachment.AttachMentLocalPath, ref strFileID, ref strFilePath);
                        objAttachment.AttachMentPath = strFilePath;
                        objAttachment.FileID = strFileID;
                        if (!string.IsNullOrEmpty(strResult))
                        {
                            MessageBoxObject.ShowWarningMessage(this, strResult);
                            return false;
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool DownloadFile(string strFileName, string strFileID)
        {
            try
            {
                SaveFileDialog dlgSaveFile = new SaveFileDialog();
                dlgSaveFile.Title = "Lưu file tải về";
                dlgSaveFile.FileName = strFileName;
                string strExtension = Path.GetExtension(strFileName);
                switch (strExtension)
                {
                    case ".xls":
                    case ".xlsx":
                        dlgSaveFile.Filter = "Excel Worksheets(*.xls, *.xlsx)|*.xls; *.xlsx";
                        break;
                    case ".doc":
                    case ".docx":
                        dlgSaveFile.Filter = "Word Documents(*.doc, *.docx)|*.doc; *.docx";
                        break;
                    case ".pdf":
                        dlgSaveFile.Filter = "PDF Files(*.pdf)|*.pdf";
                        break;
                    case ".zip":
                    case ".rar":
                        dlgSaveFile.Filter = "Compressed Files(*.zip, *.rar)|*.zip;*.rar";
                        break;
                    case ".jpg":
                    case ".jpe":
                    case ".jpeg":
                    case ".gif":
                    case ".png":
                    case ".bmp":
                    case ".tif":
                    case ".tiff":
                        dlgSaveFile.Filter = "Image Files(*.jpg, *.jpe, *.jpeg, *.gif, *.png, *.bmp, *.tif, *.tiff)|*.jpg; *.jpe; *.jpeg; *.gif; *.png; *.bmp; *.tif; *.tiff";
                        break;
                    default:
                        dlgSaveFile.Filter = "All Files (*)|*";
                        break;
                }
                if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                {
                    if (dlgSaveFile.FileName != string.Empty)
                    {
                        Library.AppCore.FTP.FileAttachment objFileAttachment = new Library.AppCore.FTP.FileAttachment();
                        ERP.FMS.DUI.DUIFileManager.DownloadFile(this, "FMSAplication_ProERP_ProductChangeOrderAttachment", strFileID, string.Empty, dlgSaveFile.FileName);
                        if (stbAttachmentLog.Length > 0)
                            stbAttachmentLog.Append("\r\n");
                        stbAttachmentLog.Append("Tải về tập tin: " + strFileName);
                    }
                }
                return true;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tải file đính kèm", objExce.ToString(), " frmInputChangeOrder-> DownloadFile", DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi tải file đính kèm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        private void mnuAddAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog objOpenFileDialog = new OpenFileDialog();
                objOpenFileDialog.Multiselect = true;
                objOpenFileDialog.Filter = "Office Files|*.doc;*.docx;*.xls;*.xlsx;*.pdf;*.ppt;*.zip;*.rar;*.jpg;*.jpe;*.jpeg;*.gif;*.png;*.bmp;*.tif;*.tiff;*.txt";
                if (objOpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String[] strLocalFilePaths = objOpenFileDialog.FileNames;
                    foreach (string strLocalFilePath in strLocalFilePaths)
                    {
                        FileInfo objFileInfo = new FileInfo(strLocalFilePath);
                        var ListCheck = from o in lstProductChangeOrder_Attachment
                                        where o.AttachMentName == objFileInfo.Name
                                        select o;
                        if (ListCheck.Count() > 0)
                            continue;
                        decimal FileSize = Math.Round(objFileInfo.Length / Convert.ToDecimal((1024 * 1024)), 2); //dung lượng tính theo MB
                        decimal FileSizeLimit = 2;//MB
                        if (FileSize > FileSizeLimit)
                        {
                            MessageBox.Show(this, "Vui lòng chọn tập tin đính kèm dung lượng không vượt quá 2 MB", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        ProductChangeOrder_ATT objInputChangeOrder_Attachment = new ProductChangeOrder_ATT();
                        objInputChangeOrder_Attachment.AttachMentName = objFileInfo.Name;
                        objInputChangeOrder_Attachment.AttachMentLocalPath = strLocalFilePath;
                        objInputChangeOrder_Attachment.AttachMentPath = DateTime.Now.ToString("yyyy_MM") + "/{0}/" + Guid.NewGuid() + "_" + objInputChangeOrder_Attachment.AttachMentName;
                        objInputChangeOrder_Attachment.CreatedDate = Globals.GetServerDateTime();
                        objInputChangeOrder_Attachment.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objInputChangeOrder_Attachment.CreatedFullName = SystemConfig.objSessionUser.UserName + " - " + SystemConfig.objSessionUser.FullName;
                        objInputChangeOrder_Attachment.IsAddNew = true;
                        objInputChangeOrder_Attachment.STT = lstProductChangeOrder_Attachment.Count + 1;
                        lstProductChangeOrder_Attachment.Add(objInputChangeOrder_Attachment);
                    }
                    grdAttachment.RefreshDataSource();
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi thêm tập tin", objExce.ToString(), this.Name + " -> mnuAddAttachment_Click", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi thêm tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuDelAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvAttachment.FocusedRowHandle < 0)
                    return;
                ProductChangeOrder_ATT objAttachment = (ProductChangeOrder_ATT)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
                if (objAttachment != null && lstProductChangeOrder_Attachment != null)
                {
                    if (objAttachment.AttachMentID != null && objAttachment.AttachMentID.Trim() != string.Empty)
                    {
                        if (btnAdd.Enabled || ProductChangeOrderID != string.Empty)
                        {
                            if (!btnUpdate.Enabled)
                            {
                                MessageBox.Show(this, "Bạn không có quyền xóa file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                    if (MessageBox.Show(this, "Bạn muốn xóa file đính kèm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        lstProductChangeOrder_Attachment.Remove(objAttachment);
                        int intSTT = 0;
                        for (int i = 0; i < lstProductChangeOrder_Attachment.Count; i++)
                            lstProductChangeOrder_Attachment[i].STT = ++intSTT;
                        if (objAttachment.AttachMentID != null && objAttachment.AttachMentID.Trim() != string.Empty)
                        {
                            if (lstProductChangeOrder_Attachment_Del == null)
                                lstProductChangeOrder_Attachment_Del = new List<ProductChangeOrder_ATT>();
                            objAttachment.IsAddNew = false;
                            objAttachment.IsDeleted = true;
                            objAttachment.DeletedUser = SystemConfig.objSessionUser.UserName;
                            lstProductChangeOrder_Attachment_Del.Add(objAttachment);
                            if (stbAttachmentLog.Length > 0)
                                stbAttachmentLog.Append("\r\n");
                            stbAttachmentLog.Append("Xóa tập tin: " + objAttachment.AttachMentName);
                        }
                        grdAttachment.RefreshDataSource();
                    }
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi xóa tập tin", objExce.ToString(), this.Name + " -> mnuDelAttachment_Click", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xóa tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuAttachment_Opening(object sender, CancelEventArgs e)
        {
            mnuDelAttachment.Enabled = false;
            mnuAddAttachment.Enabled = btnUpdate.Enabled;
            try
            {
                if (grvAttachment.FocusedRowHandle < 0) return;
                ProductChangeOrder_ATT objAttachmentInfo = lstProductChangeOrder_Attachment[grvAttachment.FocusedRowHandle];
                if (objAttachmentInfo.AttachMentName != null && Convert.ToString(objAttachmentInfo.AttachMentName).Trim() != string.Empty)
                    mnuDelAttachment.Enabled = (btnUpdate.Enabled || ProductChangeOrderID != string.Empty);
            }
            catch { }
        }

        private void grdViewData_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
                return;
            if (this._optionType == (int)OptionType.DOITRANGTHAI_SANPHAM)
            {
                if (grdViewData.FocusedColumn.FieldName == "Quantity")
                {
                    ProductChangeOrderDT obj = (ProductChangeOrderDT)grdViewData.GetFocusedRow();
                    if (obj != null)
                    {
                        //PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(obj.ProductID_Out, cboStore.ColumnID, 0, string.Empty, false, Convert.ToInt32(cboInStockStatus_srh.SelectedValue));
                        PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(obj.ProductID_Out, cboStore.ColumnID, 0, string.Empty, false, obj.InStockStatusID_Out);

                        if (objProductInStock != null &&objProductInStock.Quantity < obj.Quantity)
                        {
                            MessageBox.Show(this,"Số lượng cần đổi vướt quá số lượng tồn!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                            obj.Quantity = objProductInStock.Quantity;
                            grdViewData.RefreshData();
                        }
                    }
                }
            }
        }

        private void CreateDataTable()
        {
            this.dtbKhoxuat = new DataTable();
            this.dtbKhoxuat.Columns.Add("ID", typeof(int));
            this.dtbKhoxuat.Columns.Add("TEN", typeof(string));
            DataRow nr = this.dtbKhoxuat.NewRow();
            nr["ID"] = 0;
            nr["TEN"] = "Không xét";
            this.dtbKhoxuat.Rows.Add(nr);
            DataRow nr1 = this.dtbKhoxuat.NewRow();
            nr1["ID"] = 1;
            nr1["TEN"] = "Kho xuất";
            this.dtbKhoxuat.Rows.Add(nr1);
            DataRow nr2 = this.dtbKhoxuat.NewRow();
            nr2["ID"] = 2;
            nr2["TEN"] = "Kho nhập";
            this.dtbKhoxuat.Rows.Add(nr2);
            DataRow nr3 = this.dtbKhoxuat.NewRow();
            nr3["ID"] = 3;
            nr3["TEN"] = "Cả hai";
            this.dtbKhoxuat.Rows.Add(nr3);
        }

        private void CreateColumnGridReview(bool bolAllowEdit)
        {
            string[] fieldNames = new string[] { "SequenceReview","ReviewType","Check","ReviewLevelName","FullName",
                "ProductChangeOrderRVLID","ProductChangeOrderID",
                 "ReviewLevelID","UserName","ReviewedStatus","ReviewedDate","Note","OrderIndex",
                "CreatedUser","CreatedDate","DeletedUser","DeletedDate", "ApprovePermission", "RejectPermission",
                "StorePermissionType","IsDeleted"};
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdControlApprove, true, bolAllowEdit, true, true, fieldNames);
        }
        private bool FormatGridReview()
        {
            grdViewApprove.Columns["SequenceReview"].Caption = "Thứ tự";
            grdViewApprove.Columns["ReviewLevelName"].Caption = "Tên mức duyệt";
            grdViewApprove.Columns["FullName"].Caption = "Người duyệt";
            grdViewApprove.Columns["ReviewedStatus"].Caption = "Trạng thái duyệt";
            grdViewApprove.Columns["ReviewedDate"].Caption = "Ngày duyệt";
            grdViewApprove.Columns["Note"].Caption = "Ghi chú";
            grdViewApprove.Columns["ApprovePermission"].Caption = "quyền duyệt";
            grdViewApprove.Columns["RejectPermission"].Caption = "Quyền hủy";
            grdViewApprove.Columns["StorePermissionType"].Caption = "Xét quyền trên kho";
            grdViewApprove.Columns["SequenceReview"].Width = 40;
            grdViewApprove.Columns["SequenceReview"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.Columns["SequenceReview"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewApprove.Columns["SequenceReview"].OptionsColumn.AllowEdit = false;
            grdViewApprove.Columns["ReviewLevelName"].Width = 80;
            grdViewApprove.Columns["ReviewLevelName"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.Columns["ReviewLevelName"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewApprove.Columns["ReviewLevelName"].OptionsColumn.AllowEdit = false;
            grdViewApprove.Columns["FullName"].Width = 100;
            grdViewApprove.Columns["FullName"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.Columns["FullName"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewApprove.Columns["FullName"].OptionsColumn.AllowEdit = false;
            grdViewApprove.Columns["ReviewedStatus"].Width = 100;
            grdViewApprove.Columns["ReviewedStatus"].AppearanceHeader.Options.UseTextOptions = true;
            grdViewApprove.Columns["ReviewedStatus"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.Columns["ReviewedStatus"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewApprove.Columns["ReviewedStatus"].ColumnEdit = repositoryItemLookUpEdit;
            grdViewApprove.Columns["ReviewedStatus"].OptionsColumn.AllowEdit = false;
            grdViewApprove.Columns["ReviewedDate"].Width = 100;
            grdViewApprove.Columns["ReviewedDate"].AppearanceHeader.Options.UseTextOptions = true;
            grdViewApprove.Columns["ReviewedDate"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.Columns["ReviewedDate"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewApprove.Columns["ReviewedDate"].OptionsColumn.AllowEdit = false;
            grdViewApprove.Columns["Note"].Width = 100;
            grdViewApprove.Columns["Note"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.Columns["Note"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewApprove.Columns["Note"].OptionsColumn.AllowEdit = false;
            grdViewApprove.Columns["ApprovePermission"].Width = 100;
            grdViewApprove.Columns["ApprovePermission"].AppearanceHeader.Options.UseTextOptions = true;
            grdViewApprove.Columns["ApprovePermission"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.Columns["ApprovePermission"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewApprove.Columns["ApprovePermission"].OptionsColumn.AllowEdit = false;
            grdViewApprove.Columns["RejectPermission"].Width = 100;
            grdViewApprove.Columns["RejectPermission"].AppearanceHeader.Options.UseTextOptions = true;
            grdViewApprove.Columns["RejectPermission"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.Columns["RejectPermission"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewApprove.Columns["RejectPermission"].OptionsColumn.AllowEdit = false;
            grdViewApprove.Columns["StorePermissionType"].Width = 100;
            grdViewApprove.Columns["StorePermissionType"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.Columns["StorePermissionType"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewApprove.Columns["StorePermissionType"].OptionsColumn.AllowEdit = false;
            #region Visible 1 so columns
            grdViewApprove.Columns["ReviewType"].Visible = false;
            grdViewApprove.Columns["Check"].Visible = false;
            grdViewApprove.Columns["ProductChangeOrderRVLID"].Visible = false;
            grdViewApprove.Columns["ProductChangeOrderID"].Visible = false;
            grdViewApprove.Columns["ReviewLevelID"].Visible = false;
            grdViewApprove.Columns["UserName"].Visible = false;
            grdViewApprove.Columns["Note"].Visible = false;
            grdViewApprove.Columns["CreatedUser"].Visible = false;
            grdViewApprove.Columns["CreatedDate"].Visible = false;
            grdViewApprove.Columns["OrderIndex"].Visible = false;
            grdViewApprove.Columns["IsDeleted"].Visible = false;
            grdViewApprove.Columns["DeletedUser"].Visible = false;
            grdViewApprove.Columns["DeletedDate"].Visible = false;
            grdViewApprove.Columns["ApprovePermission"].Visible = false;
            grdViewApprove.Columns["RejectPermission"].Visible = false;
            grdViewApprove.Columns["StorePermissionType"].Visible = false;
            grdViewApprove.Columns["FullName"].Visible = false;
            #endregion

            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboLookUpEditStorePermissionType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
            col.Caption = "Mã";
            col.FieldName = "ID";
            cboLookUpEditStorePermissionType.Columns.Add(col);
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col1 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
            col1.Caption = "Tên";
            col1.FieldName = "TEN";
            cboLookUpEditStorePermissionType.Columns.Add(col1);
            cboLookUpEditStorePermissionType.DisplayMember = "TEN";
            cboLookUpEditStorePermissionType.ValueMember = "ID";
            cboLookUpEditStorePermissionType.DataSource = this.dtbKhoxuat;
            grdViewApprove.Columns["StorePermissionType"].ColumnEdit = cboLookUpEditStorePermissionType;
            //              repositoryItemLookUpEdit

            grdViewApprove.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewApprove.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            grdViewApprove.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            grdViewApprove.OptionsView.ShowAutoFilterRow = false;
            grdViewApprove.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            grdViewApprove.OptionsView.ShowFooter = false;
            grdViewApprove.OptionsView.ShowGroupPanel = false;
            grdViewApprove.ColumnPanelRowHeight = 50;
            //grdViewApprove.OptionsFilter.Reset();
            for (int i = 0; i < this.grdViewApprove.Columns.Count; i++)
            {
                this.grdViewApprove.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            }
            return true;
        }
    }
}
