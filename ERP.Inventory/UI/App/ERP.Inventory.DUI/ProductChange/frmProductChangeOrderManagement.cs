﻿
using ERP.Inventory.PLC.ProductChange;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.ProductChange
{
    public partial class frmProductChangeOrderManagement : Form
    {
        #region Variables

        private DataTable dtbDataGridResult = null;
        private PLCProductChangeOrder objPLCProductChangeOrder = new PLCProductChangeOrder();
        private string strPermissionReport = "RPT_PRODUCTCHANGEORDER_VIEW";
        #endregion

        #region Contructor

        public frmProductChangeOrderManagement()
        {
            InitializeComponent();
            LoadControl();

            btnSearch.Enabled = true;
            chkIsDeleted.Enabled = true;

            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
        }
        #endregion

        #region Main

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtbDataGridResult != null && dtbDataGridResult.Rows.Count > 0)
                {
                    Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi xuất excel thông tin quản lý yêu cầu xuất đổi hàng");
                SystemErrorWS.Insert("Lỗi xuất excel thông tin yêu cầu xuất đổi hàng", objExce.ToString(), this.Name + " -> mnuItemVoucherExport_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (DateTime.Compare(dtpFromDate.Value.Date, dtpToDate.Value.Date) > 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Đến ngày phải lớn hơn từ ngày!");
                return;
            }

            if (cboSearchBy.SelectedIndex != 0 && string.IsNullOrWhiteSpace(txtKeywords.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập từ khóa cần tìm!");
                return;
            }

            // thực hiện insert dữ liệu
            object[] objsKeyword = new object[]
            {
                "@PRODUCTCHANGETYPEID",cboProductChangeType.ColumnIDList,
                "@STOREID", cboStore.ColumnIDList,
                "@REVIEWEDSTATUSID", cboReviewedStatus.SelectedIndex == 0 ? "" : cboReviewedStatus.SelectedIndex == 1 ? "<-1>" : cboReviewedStatus.SelectedIndex == 2 ? "<0>" : "<1>",
                "@FROMDATE", dtpFromDate.Value,
                "@TODATE", dtpToDate.Value,
                "@ISDELETED", chkIsDeleted.Checked ? 1 : 0,
                "@KEYWORDS", cboSearchBy.SelectedIndex == 0 ? "" : txtKeywords.Text.Trim(),
                "@KEYTYPE", cboSearchBy.SelectedIndex,
                "@ISEXPIRE", chkOutTime.Checked ? 1 : 0,
                "@ISDETAIL", chkIsDetail.Checked? 1 : 0
            };

            dtbDataGridResult = null;
            objPLCProductChangeOrder.SearchData(ref dtbDataGridResult, objsKeyword);

            if (chkIsDeleted.Checked)
            {
                gridColumnDELETEDUSER.Visible = true;
                gridColumnDELETEDUSER.VisibleIndex = 99;
                gridColumnDELETEDDATE.Visible = true;
                gridColumnDELETEDDATE.VisibleIndex = 100;
            }
            else
            {
                gridColumnDELETEDDATE.Visible = false;
                gridColumnDELETEDUSER.Visible = false;
            }
            grdViewData.Columns["PRODUCTID_OUT"].Visible =
                grdViewData.Columns["PRODUCTNAME_OUT"].Visible =
                grdViewData.Columns["IMEI_OUT"].Visible =
                grdViewData.Columns["INSTOCKSTATUSNAME_OUT"].Visible =
                grdViewData.Columns["COSTPRICE_OUT"].Visible =
                grdViewData.Columns["PRODUCTID_IN"].Visible =
                grdViewData.Columns["PRODUCTNAME_IN"].Visible =
                grdViewData.Columns["IMEI_IN"].Visible =
                grdViewData.Columns["INSTOCKSTATUSNAME_IN"].Visible =
                grdViewData.Columns["COSTPRICE_IN"].Visible = chkIsDetail.Checked;
            if (chkIsDetail.Checked)
            {
               grdViewData.Columns["PRODUCTID_OUT"].VisibleIndex =
               grdViewData.Columns["PRODUCTNAME_OUT"].VisibleIndex =
               grdViewData.Columns["IMEI_OUT"].VisibleIndex =
               grdViewData.Columns["INSTOCKSTATUSNAME_OUT"].VisibleIndex =
               grdViewData.Columns["COSTPRICE_OUT"].VisibleIndex =
               grdViewData.Columns["PRODUCTID_IN"].VisibleIndex =
               grdViewData.Columns["PRODUCTNAME_IN"].VisibleIndex =
               grdViewData.Columns["IMEI_IN"].VisibleIndex =
               grdViewData.Columns["INSTOCKSTATUSNAME_IN"].VisibleIndex =
               grdViewData.Columns["COSTPRICE_IN"].VisibleIndex = grdViewData.Columns["PRODUCTCHANGEID"].VisibleIndex + 1;
            }
            if (dtbDataGridResult != null && dtbDataGridResult.Rows.Count > 0)
            {
                grdData.DataSource = dtbDataGridResult.DefaultView;
                grdViewData.RefreshData();
            }
            else
            {
                grdData.DataSource = null;
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy dữ liệu!");
            }
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            mnuItemEdit_Click(null, null);
        }

        private void LoadControl()
        {
            cboSearchBy.SelectedIndex = 0;
            cboReviewedStatus.SelectedIndex = 0;
            dtpFromDate.MaxDate = dtpToDate.Value;
            dtpToDate.MinDate = dtpFromDate.Value;
            DataTable dtbStore = Library.AppCore.DataSource.GetDataFilter.GetStore(true);
            if (dtbStore != null && dtbStore.Rows.Count > 0)
            {
                cboStore.InitControl(true, dtbStore, "STOREID", "STORENAME", "--Tất cả--");
            }
            //cboReviewedStatus.InitControl(true, CreateDataReviewedStatus(), "REVIEWEDSTATUSID", "REVIEWEDSTATUSNAME", "--Tất cả--");
            DataTable dtbProductChangeType = Library.AppCore.DataSource.GetDataSource.GetProductChangeType();
            if (dtbProductChangeType != null && dtbProductChangeType.Rows.Count > 0)
            {
                cboProductChangeType.InitControl(true, dtbProductChangeType, "PRODUCTCHANGETYPEID", "PRODUCTCHANGETYPENAME", "--Tất cả--");
            }

        }

        private DataTable CreateDataReviewedStatus()
        {
            DataTable dtb = new DataTable();
            dtb.Columns.Add("REVIEWEDSTATUSID", typeof(int));
            dtb.Columns.Add("REVIEWEDSTATUSNAME", typeof(string));
            dtb.Rows.Add(-1, "Đang xử lý");
            dtb.Rows.Add(0, "Từ chối");
            dtb.Rows.Add(1, "Đồng ý");
            return dtb;
        }

        private void mnuItemEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdData.DataSource == null || grdViewData.RowCount == 0)
                {
                    return;
                }
                if (grdViewData.FocusedRowHandle < 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn yêu cầu xuất đổi hàng để chỉnh sửa!");
                    return;
                }

                DataRow row = grdViewData.GetFocusedDataRow();
                frmProductChangeOrder frm = new frmProductChangeOrder(row["PRODUCTCHANGEORDERID"].ToString(), row["ISDELETED"].ToString());
                frm.ShowDialog();
                if (frm.IsChange)
                    btnSearch_Click(null, null);
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi chỉnh sửa thông tin yêu cầu xuất đổi hàng");
                SystemErrorWS.Insert("Lỗi chỉnh sửa thông tin  yêu cầu xuất đổi hàng", objExce.ToString(), this.Name + " -> mnuItemEdit_Click", DUIInventory_Globals.ModuleName);
            }
        }


        #region CẦN PHẦN QUYỀN KHI OPEN MENUCONTENT

        #endregion CẦN PHẦN QUYỀN KHI OPEN MENUCONTENT

        private void txtKeywords_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch_Click(null, null);
            }
        }
        #endregion

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (dtpFromDate.Value.Year < dtpToDate.Value.Year)
            {
                if (dtpFromDate.Value.Year < 1800)
                {
                    dtpFromDate.Value = new DateTime(1800, dtpFromDate.Value.Month, dtpFromDate.Value.Day);
                }
                dtpToDate.MinDate = new DateTime(1800, 12, 31);
                dtpToDate.Value = new DateTime(dtpFromDate.Value.Year, 12, 31);
            }
            dtpToDate.MinDate = dtpFromDate.Value;

        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            if (dtpFromDate.Value.Year < dtpToDate.Value.Year)
            {
                if (dtpToDate.Value.Year > 9997)
                {
                    dtpToDate.Value = new DateTime(9997, dtpFromDate.Value.Month, dtpFromDate.Value.Day);
                }
                dtpFromDate.MaxDate = DateTime.MaxValue.AddYears(-2);
                dtpFromDate.Value = new DateTime(dtpToDate.Value.Year, 1, 1);
            }
            dtpFromDate.MaxDate = dtpToDate.Value;
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            //if (dtbDataGridResult != null && dtbDataGridResult.Rows.Count > 0)
            if (grdData.DataSource != null && grdViewData.RowCount > 0)
            {
                //sửa

                mnuItemView.Enabled = true;
                mnuItemExportExcel.Enabled = true;
            }
            else
            {
                mnuItemView.Enabled = false;
                mnuItemExportExcel.Enabled = false;
            }
        }

        private void mnuItemView_Click(object sender, EventArgs e)
        {
            try
            {
                mnuItemEdit_Click(null, null);
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi chỉnh sửa thông tin yêu cầu xuất đổi");
                SystemErrorWS.Insert("Lỗi chỉnh sửa thông tin yêu cầu xuất đổi", objExce.ToString(), this.Name + " -> mnuItemView_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtbDataGridResult != null && dtbDataGridResult.Rows.Count > 0)
                {
                    Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi xuất excel thông tin quản lý yêu cầu xuất đổi");
                SystemErrorWS.Insert("Lỗi xuất excel thông tin yêu cầu xuất đổi", objExce.ToString(), this.Name + " -> mnuItemExportExcel_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void mnuItemViewReport_Click(object sender, EventArgs e)
        {
            if (DateTime.Compare(dtpFromDate.Value.Date, dtpToDate.Value.Date) > 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Đến ngày phải lớn hơn từ ngày!");
                return;
            }

            if (cboSearchBy.SelectedIndex != 0 && string.IsNullOrWhiteSpace(txtKeywords.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập từ khóa cần tìm!");
                return;
            }

            // thực hiện insert dữ liệu
            object[] objsKeyword = new object[]
            {
                "@PRODUCTCHANGETYPEID",cboProductChangeType.ColumnIDList,
                "@STOREID", cboStore.ColumnIDList,
                "@REVIEWEDSTATUSID", cboReviewedStatus.SelectedIndex == 0 ? "" : cboReviewedStatus.SelectedIndex == 1 ? "<-1>" : cboReviewedStatus.SelectedIndex == 2 ? "<0>" : "<1>",
                "@FROMDATE", dtpFromDate.Value,
                "@TODATE", dtpToDate.Value,
                "@ISDELETED", chkIsDeleted.Checked ? 1 : 0,
                "@KEYWORDS", cboSearchBy.SelectedIndex == 0 ? "" : txtKeywords.Text.Trim(),
                "@KEYTYPE", cboSearchBy.SelectedIndex,
                "@ISEXPIRE", chkOutTime.Checked ? 1 : 0
            };
            dtbDataGridResult = null;
            //objPLCProductChangeOrder.SearchData(ref dtbDataGridResult, objsKeyword);
            Reports.ProductChange.frmReportProductChangeOrder frmReport = new Reports.ProductChange.frmReportProductChangeOrder();
            frmReport.Keywords = objsKeyword;
            frmReport.ShowDialog();
        }

        private void frmProductChangeOrderManagement_Load(object sender, EventArgs e)
        {
            mnuItemViewReport.Enabled = SystemConfig.objSessionUser.IsPermission(strPermissionReport) && !chkIsDeleted.Checked;
        }
    }
}