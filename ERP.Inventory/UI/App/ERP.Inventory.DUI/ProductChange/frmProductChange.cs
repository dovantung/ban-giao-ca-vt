﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.Other;

namespace ERP.Inventory.DUI.ProductChange
{
    /// <summary>
    /// Created by: Đặng Minh Hùng
    /// Desc: Xuất đổi hàng
    /// </summary>
    public partial class frmProductChange : Form
    {
        public frmProductChange()
        {
            InitializeComponent();
        }

        #region variable
        private ERP.Inventory.PLC.ProductChange.PLCProductChange objPLCProductChange = new PLC.ProductChange.PLCProductChange();
        private DataTable dtbData = null;
        //private DataTable dtbStoreInStock = null;
        private DataTable dtbStoreInStockNotIMEI = null;
        private int intOldStoreID = -1;
        private DataTable tblPrdChangeParam = null;
        private DataRow rPrdChangeParam = null;
        private DataTable dtbStoreMainGroup = null;
        private bool bolIsCheckDuplicateIMEIAllProduct = false;
        private bool bolIsCheckDuplicateIMEIAllStore = false;
        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        #endregion

        #region method

        private void LoadComboBox()
        {
            cboStore.InitControl(false, true);
            dtbStoreMainGroup = Library.AppCore.DataSource.GetDataSource.GetStoreMainGroup().Copy();
        }

        private void InitTable()
        {
            dtbData = new DataTable();
            dtbData.Columns.Add("ProductID_Out", typeof(string));
            dtbData.Columns.Add("ProductName_Out", typeof(string));
            dtbData.Columns.Add("IMEI_Out", typeof(string));
            dtbData.Columns.Add("IsNew_Out", typeof(int));
            dtbData.Columns.Add("ProductID_In", typeof(string));
            dtbData.Columns.Add("ProductName_In", typeof(string));
            dtbData.Columns.Add("IMEI_In", typeof(string));
            dtbData.Columns.Add("IsNew_In", typeof(int));
            dtbData.Columns.Add("Quantity", typeof(decimal));
            dtbData.Columns.Add("VAT", typeof(int));
            dtbData.Columns.Add("VATPercent", typeof(int));
        }

        private void LockControl()
        {
            cboStore.Enabled = false;
            txtBarCode.Enabled = false;
            btnSearch.Enabled = false;
            flexData.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void FormatFlex()
        {
            flexData.Rows.Fixed = 1;
            for (int i = 0; i < flexData.Cols.Count; i++)
            {
                flexData.Cols[i].Visible = false;
                flexData.Cols[i].AllowEditing = false;
            }
            flexData.Cols[0].Visible = true;
            flexData.Cols["ProductID_Out"].Visible = true;
            flexData.Cols["ProductName_Out"].Visible = true;
            flexData.Cols["IMEI_Out"].Visible = true;
            flexData.Cols["ProductID_In"].Visible = true;
            flexData.Cols["ProductName_In"].Visible = true;
            flexData.Cols["IMEI_In"].Visible = true;
            flexData.Cols["Quantity"].Visible = true;
            flexData.Cols["ProductID_Out"].Caption = "Mã SP xuất";
            flexData.Cols["ProductName_Out"].Caption = "Tên SP xuất";
            flexData.Cols["IMEI_Out"].Caption = "IMEI xuất";
            flexData.Cols["ProductID_In"].Caption = "Mã SP nhập";
            flexData.Cols["ProductName_In"].Caption = "Tên SP nhập";
            flexData.Cols["IMEI_In"].Caption = "IMEI nhập";
            flexData.Cols["Quantity"].Caption = "Số lượng";
            flexData.Cols[0].Width = 30;
            flexData.Cols["ProductID_Out"].Width = 120;
            flexData.Cols["ProductName_Out"].Width = 200;
            flexData.Cols["IMEI_Out"].Width = 150;
            flexData.Cols["ProductID_In"].Width = 120;
            flexData.Cols["ProductName_In"].Width = 200;
            flexData.Cols["IMEI_In"].Width = 150;
            flexData.Cols["Quantity"].Width = 70;
            flexData.Cols["Quantity"].Format = "#,##0.##";

            flexData.Cols["IMEI_In"].AllowEditing = true;
            flexData.Cols["Quantity"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexData.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexData.GetCellRange(0, 0, 0, flexData.Cols.Count - 1);
            range.Style = style;
            flexData.Rows[0].Height = 26;
            flexData.Cols.Fixed = 1;
        }

        private bool CheckInput()
        {
            if (cboStore.StoreID < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn kho.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboStore.Focus();
                return false;
            }
            return true;
        }

        private bool CheckUpdate()
        {
            if (dtbData.Rows.Count < 1)
            {
                MessageBox.Show(this, "Vui lòng nhập thông tin xuất đổi hàng.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            DataRow[] rCheckIMEI_In = dtbData.Select("(IMEI_Out IS NOT NULL AND TRIM(IMEI_Out) <> '') AND (IMEI_In IS NULL OR TRIM(IMEI_In) = '')");
            if (rCheckIMEI_In.Length > 0)
            {
                for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
                {
                    if ((!Convert.IsDBNull(flexData.Rows[i]["IMEI_Out"]))
                        && Convert.ToString(flexData.Rows[i]["IMEI_Out"]).Trim() != string.Empty)
                    {
                        if (Convert.IsDBNull(flexData.Rows[i]["IMEI_In"])
                        || Convert.ToString(flexData.Rows[i]["IMEI_In"]).Trim() == string.Empty)
                        {
                            flexData.Select(i, flexData.Cols["IMEI_In"].Index);
                            flexData.Focus();
                            break;
                        }
                    }
                }
                MessageBox.Show(this, "IMEI nhập không được rỗng. Vui lòng nhập IMEI nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            DataRow[] rCheckQuantity = dtbData.Select("Quantity IS NULL OR Quantity <= 0");
            if (rCheckQuantity.Length > 0)
            {
                for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
                {
                    if (Convert.IsDBNull(flexData.Rows[i]["Quantity"]) || Convert.ToDecimal(flexData.Rows[i]["Quantity"]) <= 0)
                    {
                        flexData.Select(i, flexData.Cols["Quantity"].Index);
                        flexData.Focus();
                        break;
                    }
                }
                MessageBox.Show(this, "Vui lòng nhập số lượng xuất đổi.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(flexData.Rows[i]["IMEI_Out"].ToString()))
                {
                    PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(flexData.Rows[i]["ProductID_Out"].ToString(), cboStore.StoreID);
                    if (objProductInStock != null)
                    {
                        ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                        decimal decOutPutQuantity = 0;// số lượng sản phẩm cho mượn//
                        objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOutPutQuantity, cboStore.StoreID, flexData.Rows[i]["ProductID_Out"].ToString(), flexData.Rows[i]["IMEI_Out"].ToString());

                        if (decOutPutQuantity > 0)
                        {
                            if (Convert.ToDecimal(flexData.Rows[i]["Quantity"]) > objProductInStock.Quantity - decOutPutQuantity)
                            {
                                MessageBox.Show(this, "Sản phẩm " + flexData.Rows[i]["ProductID_Out"].ToString() + " - " + flexData.Rows[i]["ProductName_Out"].ToString() + " số lượng cho phép xuất bán tối đa là " + (objProductInStock.Quantity - decOutPutQuantity).ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                flexData.Rows[i]["Quantity"] = 1;
                                return false;
                            }
                        }
                           
                            //else
                            //{
                            //    MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductID + " - " + objProductInStock.ProductName + " không tồn kho. Hoặc không lấy được IMEI tự động!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //    return false;
                            //}
                    }
                }
                else
                {
                    PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(flexData.Rows[i]["IMEI_Out"].ToString(), cboStore.StoreID);
                    if (objProductInStock != null)
                    {
                        ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                        decimal decOutPutQuantity = 0;// số lượng sản phẩm cho mượn//
                        objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOutPutQuantity, cboStore.StoreID, flexData.Rows[i]["ProductID_Out"].ToString(), flexData.Rows[i]["IMEI_Out"].ToString());

                        if (decOutPutQuantity > 0)                            
                            {
                                MessageBox.Show(this, "IMEI [" + flexData.Rows[i]["IMEI_Out"].ToString() + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return false;
                            }
                    }
                }

            }
            return true;
        }

        #endregion

        #region event

        private void frmProductChange_Load(object sender, EventArgs e)
        {
            dtpPrChangeDate.Value = Library.AppCore.Globals.GetServerDateTime();

            LoadComboBox();
            try
            {
                cboStore.SetValue(Library.AppCore.SystemConfig.intDefaultStoreID);
            }
            catch { }
            InitTable();
            flexData.DataSource = dtbData;
            FormatFlex();
            radOutput.Checked = true;
            radNew.Checked = true;

            //dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0, "@IsRequestIMEI", true });
            //if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            //{
            //    LockControl();
            //    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            //    return;
            //}
            dtbStoreInStockNotIMEI = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0, "@IsRequestIMEI", false });
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                LockControl();
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }

            ERP.MasterData.PLC.MD.PLCProductChangeParam objPrdChangeParam = new MasterData.PLC.MD.PLCProductChangeParam();
            objPrdChangeParam.SearchData(ref tblPrdChangeParam);
            if (tblPrdChangeParam == null || tblPrdChangeParam.Rows.Count < 1)
            {
                LockControl();
                MessageBox.Show(this, "Chưa khai báo tham số xuất đổi hàng. Vui lòng khai báo tham số xuất đổi hàng trước.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                DataRow[] drPrdChangeParam = tblPrdChangeParam.Select("IsActive = 1");
                if (drPrdChangeParam.Length < 1)
                {
                    LockControl();
                    MessageBox.Show(this, "Chưa kích hoạt khai báo tham số xuất đổi hàng. Vui lòng kích hoạt khai báo tham số xuất đổi hàng trước.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    int intProductChangeParamID = 0;
                    if (this.Tag != null)
                    {
                        Hashtable hstbParam = ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                        try
                        {
                            intProductChangeParamID = Convert.ToInt32(hstbParam["ProductChangeParamID"]);
                            if (intProductChangeParamID > 0)
                            {
                                for (int i = 0; i < drPrdChangeParam.Length; i++)
                                {
                                    if (Convert.ToInt32(drPrdChangeParam[i]["ProductChangeParamID"]) == intProductChangeParamID)
                                    {
                                        rPrdChangeParam = drPrdChangeParam[i];
                                        break;
                                    }
                                }
                            }
                        }
                        catch { }
                    }
                    if (rPrdChangeParam == null)
                        rPrdChangeParam = drPrdChangeParam[0];
                }
            }

            ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfig_IsCheckDuplicateIMEIAllProduct = null;
            ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfig_IsCheckDuplicateIMEIAllStore = null;
            ERP.MasterData.SYS.PLC.PLCAppConfig objPLCAppConfig = new MasterData.SYS.PLC.PLCAppConfig();
            objPLCAppConfig.LoadInfo(ref objAppConfig_IsCheckDuplicateIMEIAllProduct, "ISCHECKDUPLICATEIMEIALLPRODUCT");
            if (objAppConfig_IsCheckDuplicateIMEIAllProduct != null && objAppConfig_IsCheckDuplicateIMEIAllProduct.ConfigValue != string.Empty)
            {
                try
                {
                    bolIsCheckDuplicateIMEIAllProduct = Convert.ToBoolean(Convert.ToInt32(objAppConfig_IsCheckDuplicateIMEIAllProduct.ConfigValue));
                }
                catch
                {
                }
            }
            objPLCAppConfig.LoadInfo(ref objAppConfig_IsCheckDuplicateIMEIAllStore, "ISCHECKDUPLICATEIMEIALLSTORE");
            if (objAppConfig_IsCheckDuplicateIMEIAllStore != null && objAppConfig_IsCheckDuplicateIMEIAllStore.ConfigValue != string.Empty)
            {
                try
                {
                    bolIsCheckDuplicateIMEIAllStore = Convert.ToBoolean(Convert.ToInt32(objAppConfig_IsCheckDuplicateIMEIAllStore.ConfigValue));
                }
                catch
                {
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch1 = MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch1.IsMultiSelect = false;
            frmProductSearch1.ShowDialog();
            if (frmProductSearch1.Product != null && Convert.ToString(frmProductSearch1.Product.ProductName).Trim() != string.Empty)
            {
                txtBarCode.Text = Convert.ToString(frmProductSearch1.Product.ProductID).Trim();
                txtBarCode.Focus();
            }
        }

        private void cboStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (intOldStoreID > 0 && intOldStoreID != cboStore.StoreID && dtbData.Rows.Count > 0)
            {
                if (MessageBox.Show("Khi thay đổi kho xuất, dữ liệu nhập vào sẽ bị xóa!\n Bạn có chắc muốn thay đổi kho xuất không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    InitTable();
                    flexData.DataSource = dtbData;
                    FormatFlex();

                    GroupBox2.Enabled = true;
                    radOld.Checked = false;
                    radNew.Checked = true;
                }
                else
                {
                    cboStore.SetValue(intOldStoreID);
                }
            }
            intOldStoreID = cboStore.StoreID;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            btnUpdate.Enabled = false;

            try
            {
                dtbData.AcceptChanges();

                if (!CheckUpdate())
                {
                    btnUpdate.Enabled = true;
                    return;
                }

                if (MessageBox.Show(this, "Bạn có chắc muốn xuất đổi hàng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    btnUpdate.Enabled = true;
                    return;
                }

                int intStoreID = cboStore.StoreID;
                string strUserName = Library.AppCore.SystemConfig.objSessionUser.UserName;
                int intCreateStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                dtpPrChangeDate.Value = Library.AppCore.Globals.GetServerDateTime();
                DateTime dtmOutputDate = dtpPrChangeDate.Value;
                DateTime dtmInputDate = dtpPrChangeDate.Value;
                string strContent = txtContent.Text.Trim();

                int intCustomerID = 0;
                int intPayableTypeID = 0;
                int intInputTypeID = 0;
                int intOutputTypeID = 0;
                int intCurrencyUnitID = 0;
                decimal decCurrencyExchange = 0;
                //string strStaffUser = string.Empty;

                intCustomerID = Convert.ToInt32(rPrdChangeParam["CustomerID"]);
                intPayableTypeID = Convert.ToInt32(rPrdChangeParam["PayableTypeID"]);
                intInputTypeID = Convert.ToInt32(rPrdChangeParam["InputTypeID"]);
                intOutputTypeID = Convert.ToInt32(rPrdChangeParam["OutputTypeID"]);
                intCurrencyUnitID = Convert.ToInt32(rPrdChangeParam["CurrencyUnitID"]);
                //strStaffUser = Convert.ToString(rPrdChangeParam["CreatedUser"]).Trim();

                ERP.MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit objCurrencyUnit = new MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit();
                ERP.MasterData.PLC.MD.PLCCurrencyUnit objPLCCurrencyUnit = new MasterData.PLC.MD.PLCCurrencyUnit();
                objPLCCurrencyUnit.LoadInfo(ref objCurrencyUnit, intCurrencyUnitID);
                if (objCurrencyUnit != null)
                {
                    decCurrencyExchange = objCurrencyUnit.CurrencyExchange;
                }
                else
                {
                    DataTable tblCurrencyUnit = null;
                    objPLCCurrencyUnit.SearchData(ref tblCurrencyUnit, new object[] { });
                    if (tblCurrencyUnit != null && tblCurrencyUnit.Rows.Count > 0)
                    {
                        DataRow[] rCurrencyUnit = tblCurrencyUnit.Select("CurrencyUnitName = 'VND'");
                        if (rCurrencyUnit.Length > 0)
                        {
                            intCurrencyUnitID = Convert.ToInt32(rCurrencyUnit[0]["CurrencyUnitID"]);
                            decCurrencyExchange = Convert.ToDecimal(rCurrencyUnit[0]["CurrencyExchange"]);
                        }
                    }
                }

                ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                ERP.MasterData.PLC.MD.PLCCustomer objPLCCustomer = new MasterData.PLC.MD.PLCCustomer();
                objPLCCustomer.LoadInfo(ref objCustomer, intCustomerID);

                ERP.Inventory.PLC.ProductChange.WSProductChange.ProductChange objProductChange = new PLC.ProductChange.WSProductChange.ProductChange();
                ERP.Inventory.PLC.ProductChange.WSProductChange.InputVoucher objInputVoucher = new PLC.ProductChange.WSProductChange.InputVoucher();
                ERP.Inventory.PLC.ProductChange.WSProductChange.OutputVoucher objOutputVoucher = new PLC.ProductChange.WSProductChange.OutputVoucher();

                objProductChange.StoreID = intStoreID;
                objProductChange.ProductChangeUser = strUserName;
                objProductChange.ProductChangeDate = dtmOutputDate;
                objProductChange.InVoiceDate = dtpInvoiceDate.Value;
                objProductChange.Content = txtContent.Text.Trim();
                objProductChange.CreatedStoreID = intCreateStoreID;

                objInputVoucher.IsCheckRealInput = true;
                objInputVoucher.CheckRealInputUser = strUserName;
                objInputVoucher.CheckRealInputTime = dtmInputDate;
                objInputVoucher.InputTypeID = intInputTypeID;
                objInputVoucher.InVoiceDate = dtpInvoiceDate.Value;
                objInputVoucher.InputDate = dtmInputDate;
                objInputVoucher.PayableDate = dtmInputDate;
                objInputVoucher.CustomerID = intCustomerID;
                objInputVoucher.CustomerName = objCustomer.CustomerName;
                objInputVoucher.CustomerAddress = objCustomer.CustomerAddress;
                objInputVoucher.CustomerPhone = objCustomer.CustomerPhone;
                objInputVoucher.CustomerIDCard = objCustomer.CustomerIDCard;
                objInputVoucher.CustomerTaxID = objCustomer.CustomerTaxID;
                objInputVoucher.CustomerEmail = objCustomer.CustomerEmail;
                objInputVoucher.CreatedStoreID = intCreateStoreID;
                objInputVoucher.InputStoreID = intStoreID;
                objInputVoucher.PayableTypeID = intPayableTypeID;
                objInputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objInputVoucher.CurrencyExchange = decCurrencyExchange;
                objProductChange.InputVoucherBO = objInputVoucher;

                objOutputVoucher.CreatedStoreID = intCreateStoreID;
                objOutputVoucher.OutputStoreID = intStoreID;
                objOutputVoucher.CustomerID = intCustomerID;
                objOutputVoucher.CustomerName = objCustomer.CustomerName;
                objOutputVoucher.CustomerAddress = objCustomer.CustomerAddress;
                objOutputVoucher.CustomerPhone = objCustomer.CustomerPhone;
                objOutputVoucher.CustomerTaxID = objCustomer.CustomerTaxID;
                objOutputVoucher.OutputContent = strContent;
                objOutputVoucher.InvoiceDate = dtpInvoiceDate.Value;
                objOutputVoucher.OutputDate = dtmOutputDate;
                objOutputVoucher.PayableTypeID = intPayableTypeID;
                objOutputVoucher.PayableDate = dtmOutputDate;
                objOutputVoucher.StaffUser = strUserName;
                objOutputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objOutputVoucher.CurrencyExchange = decCurrencyExchange;
                objProductChange.OutputVoucherBO = objOutputVoucher;

                List<PLC.ProductChange.WSProductChange.InputVoucherDetail> objInputVoucherDetailList = new List<PLC.ProductChange.WSProductChange.InputVoucherDetail>();
                List<PLC.ProductChange.WSProductChange.OutputVoucherDetail> objOutputVoucherDetailList = new List<PLC.ProductChange.WSProductChange.OutputVoucherDetail>();
                List<PLC.ProductChange.WSProductChange.ProductChangeDetail> objProductChangeDetailList = new List<PLC.ProductChange.WSProductChange.ProductChangeDetail>();

                dtbData.DefaultView.Sort = "ProductID_Out ASC, ProductID_In ASC";
                dtbData = dtbData.DefaultView.ToTable();

                for (int i = 0; i < dtbData.Rows.Count; i++)
                {
                    DataRow r = dtbData.Rows[i];
                    string strProductID_Out = string.Empty;
                    string strProductID_In = string.Empty;
                    string strIMEI_Out = string.Empty;
                    string strIMEI_In = string.Empty;
                    decimal decQuantity = 0;
                    int intVAT = 0;
                    int intVATPercent = 0;
                    strProductID_Out = Convert.ToString(r["ProductID_Out"]).Trim();
                    strProductID_In = Convert.ToString(r["ProductID_In"]).Trim();
                    if (!Convert.IsDBNull(r["IMEI_Out"])) strIMEI_Out = Convert.ToString(r["IMEI_Out"]).Trim();
                    if (!Convert.IsDBNull(r["IMEI_In"])) strIMEI_In = Convert.ToString(r["IMEI_In"]).Trim();
                    decQuantity = Convert.ToDecimal(r["Quantity"]);
                    intVAT = Convert.ToInt32(r["VAT"]);
                    intVATPercent = Convert.ToInt32(r["VATPercent"]);

                    #region ProductChangeDetail
                    PLC.ProductChange.WSProductChange.ProductChangeDetail objProductChangeDetail = new PLC.ProductChange.WSProductChange.ProductChangeDetail();
                    objProductChangeDetail.StoreID = intStoreID;
                    objProductChangeDetail.ProductChangeDate = dtmOutputDate;
                    objProductChangeDetail.ProductID_Out = strProductID_Out;
                    objProductChangeDetail.ProductID_In = strProductID_In;
                    objProductChangeDetail.IMEI_Out = strIMEI_Out;
                    objProductChangeDetail.IMEI_In = strIMEI_In;
                    objProductChangeDetail.Quantity = decQuantity;
                    objProductChangeDetail.OutputTypeID = intOutputTypeID;
                    objProductChangeDetail.InputTypeID = intInputTypeID;
                    objProductChangeDetail.CreatedStoreID = intCreateStoreID;

                    objProductChangeDetailList.Add(objProductChangeDetail);
                    #endregion

                    #region InputVoucherDetail

                    PLC.ProductChange.WSProductChange.InputVoucherDetail objInputVoucherDetail = new PLC.ProductChange.WSProductChange.InputVoucherDetail();
                    objInputVoucherDetail.ProductID = strProductID_In;
                    objInputVoucherDetail.Quantity = decQuantity;
                    objInputVoucherDetail.VAT = intVAT;
                    objInputVoucherDetail.VATPercent = intVATPercent;
                    objInputVoucherDetail.IsNew = Convert.ToBoolean(r["IsNew_In"]);
                    objInputVoucherDetail.CreatedStoreID = intCreateStoreID;
                    objInputVoucherDetail.InputStoreID = intStoreID;
                    objInputVoucherDetail.InputDate = dtmInputDate;
                    objInputVoucherDetail.FirstInputDate = dtmInputDate;
                    objInputVoucherDetail.FirstCustomerID = intCustomerID;
                    objInputVoucherDetail.FirstInputTypeID = intInputTypeID;
                    objInputVoucherDetail.ProductChangeDate = dtmInputDate;
                    int intMonthOfWarranty = ERP.MasterData.PLC.MD.PLCWarrantyMonthByProduct.GetWarrantyMonthByProduct(objInputVoucherDetail.ProductID, objInputVoucherDetail.IsNew);
                    objInputVoucherDetail.ENDWarrantyDate = Library.AppCore.Globals.GetServerDateTime().AddMonths(intMonthOfWarranty);

                    if (strIMEI_In != string.Empty)
                    {
                        if (strIMEI_Out == strIMEI_In && strProductID_Out == strProductID_In)
                        {
                            objInputVoucherDetail.ChangeToOLDDate = dtmInputDate;
                        }
                        PLC.ProductChange.WSProductChange.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.ProductChange.WSProductChange.InputVoucherDetailIMEI();
                        objInputVoucherDetailIMEI.IMEI = strIMEI_In;
                        objInputVoucherDetailIMEI.CreatedStoreID = intCreateStoreID;
                        objInputVoucherDetailIMEI.InputDate = dtmInputDate;
                        objInputVoucherDetailIMEI.InputStoreID = intStoreID;

                        List<PLC.ProductChange.WSProductChange.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = new List<PLC.ProductChange.WSProductChange.InputVoucherDetailIMEI>();
                        objInputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                        objInputVoucherDetail.InputVoucherDetailIMEIList = objInputVoucherDetailIMEIList.ToArray();
                        objInputVoucherDetail.Quantity = Convert.ToDecimal(objInputVoucherDetail.InputVoucherDetailIMEIList.Length);
                    }
                    objInputVoucherDetailList.Add(objInputVoucherDetail);

                    #endregion

                    #region OutputVoucherDetail
                    PLC.ProductChange.WSProductChange.OutputVoucherDetail objOutputVoucherDetail = new PLC.ProductChange.WSProductChange.OutputVoucherDetail();
                    objOutputVoucherDetail.ProductID = strProductID_Out;
                    objOutputVoucherDetail.IMEI = strIMEI_Out;
                    objOutputVoucherDetail.Quantity = decQuantity;
                    objOutputVoucherDetail.VAT = intVAT;
                    objOutputVoucherDetail.VATPercent = intVATPercent;
                    objOutputVoucherDetail.IsNew = Convert.ToBoolean(r["IsNew_Out"]);
                    objOutputVoucherDetail.CreatedStoreID = intCreateStoreID;
                    objOutputVoucherDetail.OutputStoreID = intStoreID;
                    objOutputVoucherDetail.OutputTypeID = intOutputTypeID;
                    objOutputVoucherDetail.OutputDate = dtmOutputDate;
                    objOutputVoucherDetail.InputChangeDate = dtmOutputDate;

                    objOutputVoucherDetailList.Add(objOutputVoucherDetail);
                    #endregion
                }

                objProductChange.ProductChangeDetailList = objProductChangeDetailList.ToArray();
                objProductChange.InputVoucherBO.InputVoucherDetailList = objInputVoucherDetailList.ToArray();
                objProductChange.OutputVoucherBO.OutputVoucherDetailList = objOutputVoucherDetailList.ToArray();

                objProductChange.InputVoucherBO.IsNew = objInputVoucherDetailList[0].IsNew;
                objProductChange.IsNew = objInputVoucherDetailList[0].IsNew;

                if (!objPLCProductChange.InsertMasterAndDetail(objProductChange))
                {
                    btnUpdate.Enabled = true;
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                }

                InitTable();
                flexData.DataSource = dtbData;
                FormatFlex();
                cboStore.SetValue(0);
                cboStore_SelectionChangeCommitted(null, null);
                txtContent.Text = string.Empty;
                MessageBox.Show(this, "Xuất đổi hàng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0, "@IsRequestIMEI", true });
                dtbStoreInStockNotIMEI = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0, "@IsRequestIMEI", false });
                GroupBox2.Enabled = true;
                radOld.Checked = false;
                radNew.Checked = true;

                btnUpdate.Enabled = true;
            }
            catch
            {
                MessageBox.Show(this, "Lỗi lấy thông tin xuất đổi hàng.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnUpdate.Enabled = true;
            }
        }

        private void flexData_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexData.Rows.Count <= flexData.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexData.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexData.Cols["IMEI_In"].Index)
            {
                string strProductID_Out = Convert.ToString(flexData[e.Row, "ProductID_Out"]).Trim();
                string strIMEI_Out = string.Empty;
                if ((!Convert.IsDBNull(flexData[e.Row, "IMEI_Out"])) && Convert.ToString(flexData[e.Row, "IMEI_Out"]).Trim() != string.Empty)
                {
                    strIMEI_Out = Convert.ToString(flexData[e.Row, "IMEI_Out"]).Trim();
                }

                string strProductID = Convert.ToString(flexData[e.Row, "ProductID_In"]).Trim();
                string strIMEI = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(Convert.ToString(flexData[e.Row, "IMEI_In"]).Trim());
                if (strIMEI != string.Empty)
                {
                    ERP.MasterData.PLC.MD.WSProduct.Product objCheck = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strIMEI);
                    if (objCheck != null && !string.IsNullOrEmpty(objCheck.ProductName))
                    {
                        MessageBox.Show(this, "Số IMEI này trùng với mã sản phẩm. Vui lòng nhập IMEI khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexData[e.Row, "IMEI_In"] = string.Empty;
                        return;
                    }
                    int intIsNew = 1;
                    if (strIMEI == strIMEI_Out)
                    {
                        if (strProductID == strProductID_Out)
                        {
                            intIsNew = (radNew.Checked == true ? 1 : 0);
                        }
                        else
                        {
                            intIsNew = Convert.ToInt32(flexData[e.Row, "IsNew_Out"]);
                        }
                    }

                    DataRow[] rExistOut = dtbData.Select("TRIM(IMEI_Out) = '" + strIMEI + "'AND IsNew_Out = " + intIsNew);
                    if (rExistOut.Length > 0)
                    {
                        if (strProductID == Convert.ToString(flexData[e.Row, "ProductID_Out"]).Trim())
                        {
                            if (radNew.Checked)
                            {
                                radOld.Checked = radNew.Checked;
                            }
                            else if (radOld.Checked)
                            {
                                radNew.Checked = radOld.Checked;
                            }
                        }
                    }
                    flexData[e.Row, "IMEI_In"] = strIMEI;
                }

                flexData[e.Row, "IsNew_In"] = (radNew.Checked == true ? 1 : 0);

                if (strIMEI != string.Empty && strIMEI_Out != string.Empty && strIMEI == strIMEI_Out)
                {

                }
                else
                {
                    if ((strProductID_Out != strProductID) || (strIMEI != string.Empty && strIMEI_Out != string.Empty && strIMEI != strIMEI_Out))
                    {
                        if (bolIsCheckDuplicateIMEIAllProduct || bolIsCheckDuplicateIMEIAllStore)
                        {
                            //DataRow[] rCheckDuplicateIMEI;
                            if (bolIsCheckDuplicateIMEIAllProduct && bolIsCheckDuplicateIMEIAllStore)
                            {
                                if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, strIMEI, 0, 0, -1, true, true))
                                //rCheckDuplicateIMEI = dtbStoreInStock.Select("TRIM(IMEI) = '" + strIMEI + "'");
                                //if (rCheckDuplicateIMEI.Length > 0)
                                {
                                    MessageBox.Show(this, "IMEI " + strIMEI + " đang tồn tại trong hệ thống. Vui lòng nhập IMEI khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    flexData[e.Row, "IMEI_In"] = string.Empty;
                                    return;
                                }
                            }
                            else if (bolIsCheckDuplicateIMEIAllProduct)
                            {
                                if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, strIMEI, cboStore.StoreID, 0, -1, true, true))
                                //rCheckDuplicateIMEI = dtbStoreInStock.Select("StoreID = " + Convert.ToInt32(cboStore.SelectedValue) + " AND TRIM(IMEI) = '" + strIMEI + "'");
                                //if (rCheckDuplicateIMEI.Length > 0)
                                {
                                    MessageBox.Show(this, "IMEI " + strIMEI + " đang tồn tại trong hệ thống. Vui lòng nhập IMEI khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    flexData[e.Row, "IMEI_In"] = string.Empty;
                                    return;
                                }
                            }
                            else if (bolIsCheckDuplicateIMEIAllStore)
                            {
                                if (objPLCProductInStock.CheckIsExistIMEI(strProductID, strIMEI, 0, 0, -1, true, true))
                                //rCheckDuplicateIMEI = dtbStoreInStock.Select("TRIM(ProductID) = '" + strProductID + "' AND TRIM(IMEI) = '" + strIMEI + "'");
                                //if (rCheckDuplicateIMEI.Length > 0)
                                {
                                    MessageBox.Show(this, "IMEI " + strIMEI + " đang tồn tại trong hệ thống. Vui lòng nhập IMEI khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    flexData[e.Row, "IMEI_In"] = string.Empty;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (objPLCProductInStock.CheckIsExistIMEI(strProductID, strIMEI, cboStore.StoreID, 0, -1, true, true))
                            //DataRow[] rInput = dtbStoreInStock.Select("StoreID = " + Convert.ToInt32(cboStore.SelectedValue) + " AND TRIM(ProductID) = '" + strProductID + "' AND TRIM(IMEI) = '" + strIMEI + "'");
                            //if (rInput.Length > 0)
                            {
                                MessageBox.Show(this, "IMEI đã tồn tại trong kho. Vui lòng nhập IMEI khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                flexData[e.Row, "IMEI_In"] = string.Empty;
                                return;
                            }
                        }
                    }

                    ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                    decimal decOut = 0;
                    objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, cboStore.StoreID, strProductID, strIMEI);
                    if (decOut > 0)
                    {
                        MessageBox.Show(this, "IMEI [" + strIMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexData[e.Row, "IMEI_In"] = string.Empty;
                        return;
                    }

                }
                dtbData.AcceptChanges();
                DataRow[] rExist = dtbData.Select("TRIM(IMEI_In) = '" + strIMEI + "'");
                if (rExist.Length > 1)
                {
                    MessageBox.Show(this, "IMEI " + strIMEI + " đã được nhập. Vui lòng nhập IMEI khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexData[e.Row, "IMEI_In"] = string.Empty;
                    return;
                }

                if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI))
                {
                    MessageBox.Show(this, "IMEI " + strIMEI + " không đúng định dạng. Vui lòng nhập IMEI khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexData[e.Row, "IMEI_In"] = string.Empty;
                    return;
                }
            }
            if (e.Col == flexData.Cols["Quantity"].Index)
            {
                decimal decQuantity = Convert.ToDecimal(flexData[e.Row, "Quantity"]);
                int intOutputStoreID = cboStore.StoreID;
                string strProductID = Convert.ToString(flexData[e.Row, "ProductID_Out"]).Trim();
                DataRow[] rOutputInStock = dtbStoreInStockNotIMEI.Select("StoreID = " + intOutputStoreID + " AND TRIM(ProductID) = '" + strProductID + "' AND Quantity > 0");
                if (rOutputInStock.Length > 0)
                {
                    if (decQuantity > Convert.ToDecimal(rOutputInStock[0]["Quantity"]))
                    {
                        MessageBox.Show(this, "Mã sản phẩm " + strProductID + " không đủ số lượng tồn để xuất đổi. Vui lòng nhập lại số lượng.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexData[e.Row, "Quantity"] = System.DBNull.Value;
                    }
                }
            }
        }

        private void flexData_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexData.Rows.Count <= flexData.Rows.Fixed)
            {
                return;
            }
            if (flexData.RowSel < flexData.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexData.Cols["IMEI_In"].Index)
            {
                if (Convert.IsDBNull(flexData[flexData.RowSel, "ProductID_In"]) || Convert.ToString(flexData[flexData.RowSel, "ProductID_In"]).Trim() == string.Empty)
                {
                    e.Cancel = true;
                }
                if (Convert.IsDBNull(flexData[flexData.RowSel, "IMEI_Out"]) || Convert.ToString(flexData[flexData.RowSel, "IMEI_Out"]).Trim() == string.Empty)
                {
                    e.Cancel = true;
                }
            }
            if (e.Col == flexData.Cols["Quantity"].Index)
            {
                if (Convert.IsDBNull(flexData[flexData.RowSel, "ProductID_In"]) || Convert.ToString(flexData[flexData.RowSel, "ProductID_In"]).Trim() == string.Empty)
                {
                    e.Cancel = true;
                }
                if ((!Convert.IsDBNull(flexData[flexData.RowSel, "IMEI_Out"])) && Convert.ToString(flexData[flexData.RowSel, "IMEI_Out"]).Trim() != string.Empty)
                {
                    e.Cancel = true;
                }
            }
        }

        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            dtbData.AcceptChanges();
            if (dtbData.Rows.Count > 0 && flexData.RowSel >= flexData.Rows.Fixed)
            {
                dtbData.Rows.RemoveAt(flexData.RowSel - flexData.Rows.Fixed);
                int intSTT = 0;
                for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
                {
                    flexData[i, 0] = ++intSTT;
                }
                if (dtbData.Rows.Count < 1)
                {
                    GroupBox2.Enabled = true;
                    radOld.Checked = false;
                    radNew.Checked = true;
                }

                flexData.DataSource = dtbData;
                FormatFlex();
            }
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            mnuItemDelete.Enabled = false;
            if (flexData.Rows.Count > flexData.Rows.Fixed && flexData.RowSel >= flexData.Rows.Fixed)
            {
                mnuItemDelete.Enabled = true;
            }
        }

        private bool CheckStoreMainGroup(int intStoreID, int intMainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType enStoreMainGroupPermissionType)
        {
            switch (enStoreMainGroupPermissionType)
            {
                case Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.OUTPUT:
                    DataRow[] rCheckOutput = dtbStoreMainGroup.Select("StoreID = " + intStoreID + " AND MainGroupID = " + intMainGroupID + " AND IsCanOutput = 1");
                    if (rCheckOutput.Length > 0)
                    {
                        return true;
                    }
                    break;
                case Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.INPUT:
                    DataRow[] rCheckInput = dtbStoreMainGroup.Select("StoreID = " + intStoreID + " AND MainGroupID = " + intMainGroupID + " AND IsCanInput = 1");
                    if (rCheckInput.Length > 0)
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        private void txtBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strProductID = string.Empty;
            string strIMEI = string.Empty;
            int intVAT = 0;
            int intVATPercent = 0;
            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();

            if (Convert.ToString(txtBarCode.Text).Trim() != string.Empty && e.KeyChar == (char)Keys.Enter)
            {
                if (!CheckInput())
                {
                    return;
                }

                dtbData.AcceptChanges();

                string strBarcode = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtBarCode.Text.Trim());
                int intOutputStoreID = cboStore.StoreID;
                if (!Library.AppCore.Other.CheckObject.CheckIMEI(strBarcode))
                {
                    MessageBox.Show(this, "IMEI không đúng định dạng.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (dtbData.Rows.Count > 0)
                {
                    if (Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["ProductID_Out"]).Trim() != string.Empty
                        && Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["ProductID_In"]).Trim() != string.Empty)
                    {
                        if (Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_Out"]).Trim() != string.Empty
                            && Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_In"]).Trim() == string.Empty)
                        {
                            MessageBox.Show(this, "Vui lòng nhập IMEI nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexData.Select(flexData.Rows.Count - 1, flexData.Cols["IMEI_In"].Index);
                            flexData.Focus();
                            return;
                        }
                        if (Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_In"]).Trim() != string.Empty
                            && Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_Out"]).Trim() == string.Empty)
                        {
                            MessageBox.Show(this, "Vui lòng nhập IMEI xuất.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexData.Select(flexData.Rows.Count - 1, flexData.Cols["IMEI_Out"].Index);
                            flexData.Focus();
                            return;
                        }
                        if (Convert.IsDBNull(dtbData.Rows[dtbData.Rows.Count - 1]["Quantity"]) || Convert.ToDecimal(dtbData.Rows[dtbData.Rows.Count - 1]["Quantity"]) == 0)
                        {
                            MessageBox.Show(this, "Vui lòng nhập số lượng.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexData.Select(flexData.Rows.Count - 1, flexData.Cols["Quantity"].Index);
                            flexData.Focus();
                            return;
                        }
                    }
                }

                if (radOutput.Checked)
                {
                    if (dtbData.Rows.Count > 0)
                    {
                        if (Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["ProductID_In"]).Trim() == string.Empty)
                        {
                            MessageBox.Show(this, "Vui lòng nhập mã SP nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexData.Select(flexData.Rows.Count - 1, flexData.Cols["ProductID_In"].Index);
                            flexData.Focus();
                            return;
                        }
                    }

                    int intIsNew_IMEIInStock = 0;
                    PLC.WSProductInStock.ProductInStock objProductInStock = null;
                    if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, strBarcode, intOutputStoreID, 0, -1, true, true))
                    {
                        objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, intOutputStoreID);
                        if (objProductInStock != null)
                        //DataRow[] rOutputIMEI = dtbStoreInStock.Select("StoreID = " + intOutputStoreID + " AND TRIM(IMEI) = '" + strBarcode + "'");
                        //if (rOutputIMEI.Length > 0)
                        {
                            objProductInStock.IMEI = strBarcode;
                            strProductID = objProductInStock.ProductID;//Convert.ToString(rOutputIMEI[0]["ProductID"]).Trim();
                            strIMEI = strBarcode;
                            intIsNew_IMEIInStock = (objProductInStock.IsNew ? 1 : 0);//Convert.ToInt32(rOutputIMEI[0]["IsNew"]);
                        }
                    }

                    if (strProductID == string.Empty)
                    {
                        strProductID = strBarcode;
                    }
                    if (objProductInStock == null)
                        objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, intOutputStoreID);

                    objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                    if (objProduct == null || Convert.ToString(objProduct.ProductName).Trim() == string.Empty)
                    {
                        MessageBox.Show(this, "Mã sản phẩm hoặc IMEI không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (!CheckStoreMainGroup(cboStore.StoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.OUTPUT))
                    {
                        MessageBox.Show(this, "Kho này không được phép xuất đổi trên ngành hàng " + objProduct.MainGroupName + "!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (objProduct.IsRequestIMEI)
                    {
                        GroupBox2.Enabled = false;

                        if (strIMEI == string.Empty)
                        {
                            if (objProductInStock != null && objProductInStock.Quantity > 0)
                            //DataRow[] rOutputPRD = dtbStoreInStock.Select("StoreID = " + intOutputStoreID + " AND TRIM(ProductID) = '" + strProductID + "' AND IMEI IS NOT NULL");
                            //if (rOutputPRD.Length > 0)
                            {
                                MessageBox.Show(this, "Mã sản phẩm " + strProductID + " có yêu cầu nhập IMEI. Vui lòng nhập IMEI có tồn kho hệ thống.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            else
                            {
                                MessageBox.Show(this, "Mã sản phẩm " + strProductID + " có yêu cầu nhập IMEI. Hiện tại mã sản phẩm " + strProductID + " không có tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                        else
                        {
                            DataRow[] rExist = dtbData.Select("TRIM(IMEI_Out) = '" + strIMEI + "'");
                            if (rExist.Length > 0)
                            {
                                MessageBox.Show(this, "IMEI " + strIMEI + " xuất đổi đã được nhập. Vui lòng nhập IMEI khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                    else
                    {
                        DataRow[] rOutputInStock = dtbStoreInStockNotIMEI.Select("StoreID = " + intOutputStoreID + " AND TRIM(ProductID) = '" + strProductID + "' AND Quantity > 0");
                        if (rOutputInStock.Length < 1)
                        {
                            MessageBox.Show(this, "Mã sản phẩm " + strProductID + " không tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        DataRow[] rExistPrdOut = dtbData.Select("ProductID_Out = '" + strProductID + "'");
                        if (rExistPrdOut.Length > 0)
                        {
                            MessageBox.Show(this, "Mã sản phẩm " + strProductID + " xuất đổi đã được nhập. Vui lòng nhập mã sản phẩm khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }

                    ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                    decimal decOut = 0;
                    objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, cboStore.StoreID, strProductID, strIMEI);
                    if (decOut > 0)
                    {
                        MessageBox.Show(this, "IMEI [" + strIMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    string strProductName = Convert.ToString(objProduct.ProductName).Trim();
                    intVAT = objProduct.VAT;
                    intVATPercent = objProduct.VATPercent;
                    DataRow rNew = dtbData.NewRow();
                    rNew["ProductID_Out"] = strProductID;
                    rNew["ProductName_Out"] = strProductName;
                    rNew["IMEI_Out"] = strIMEI;
                    if (strIMEI != string.Empty)
                    {
                        rNew["IsNew_Out"] = intIsNew_IMEIInStock;
                        if (intIsNew_IMEIInStock == 1)
                        {
                            radNew.Checked = true;
                        }
                        else
                        {
                            radOld.Checked = true;
                        }
                    }
                    else
                    {
                        rNew["IsNew_Out"] = (radNew.Checked == true ? 1 : 0);
                    }
                    if (strIMEI != string.Empty)
                    {
                        rNew["Quantity"] = 1;
                    }
                    rNew["VAT"] = intVAT;
                    rNew["VATPercent"] = intVATPercent;
                    dtbData.Rows.Add(rNew);
                    flexData[flexData.Rows.Count - 1, 0] = dtbData.Rows.Count;
                    flexData.Select(flexData.Rows.Count - 1, 0);

                    txtBarCode.Text = string.Empty;
                    radInput.Checked = true;
                }
                else
                {
                    if (dtbData.Rows.Count < 1)
                    {
                        MessageBox.Show(this, "Vui lòng chọn Xuất trước.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        radOutput.Focus();
                        return;
                    }
                    strIMEI = strBarcode;
                    if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, strIMEI, cboStore.StoreID, 0, -1, true, true))
                    //DataRow[] rInput = dtbStoreInStock.Select("StoreID = " + Convert.ToInt32(cboStore.SelectedValue) + " AND TRIM(IMEI) = '" + strIMEI + "'");
                    //if (rInput.Length > 0)
                    {
                        PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(strIMEI, cboStore.StoreID);
                        if (objProductInStock != null)
                        {
                            objProductInStock.IMEI = strIMEI;
                            strProductID = objProductInStock.ProductID;//Convert.ToString(rInput[0]["ProductID"]).Trim();
                            DataRow[] rExist = dtbData.Select("TRIM(IMEI_Out) = '" + strIMEI + "'AND IsNew_Out = " + (radNew.Checked ? 1 : 0));
                            if (rExist.Length > 0)
                            {
                                if (strProductID == Convert.ToString(rExist[0]["ProductID_Out"]).Trim())
                                {
                                    MessageBox.Show(this, "IMEI nhập không phù hợp.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
                                    {
                                        if (Convert.ToString(flexData.Rows[i]["ProductID_Out"]).Trim() == strProductID
                                            && Convert.ToString(flexData.Rows[i]["IMEI_Out"]).Trim() == strIMEI)
                                        {
                                            flexData.Select(i, flexData.Cols["ProductID_Out"].Index);
                                            flexData.Focus();
                                            break;
                                        }
                                    }
                                    return;
                                }
                            }
                        }
                    }
                    if (strProductID == string.Empty)
                    {
                        strProductID = strBarcode;
                    }
                    objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                    if (objProduct == null || Convert.ToString(objProduct.ProductName).Trim() == string.Empty)
                    {
                        MessageBox.Show(this, "Mã sản phẩm không đúng.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (Convert.IsDBNull(flexData[flexData.RowSel, "IMEI_Out"]) || Convert.ToString(flexData[flexData.RowSel, "IMEI_Out"]).Trim() == string.Empty)
                    {
                        if (Convert.ToString(flexData[flexData.RowSel, "ProductID_Out"]).Trim() == Convert.ToString(objProduct.ProductID).Trim())
                        {
                            MessageBox.Show(this, "Sản phẩm nhập phải khác sản phẩm xuất đổi. Vui lòng nhập lại sản phẩm nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }

                    ERP.MasterData.PLC.MD.WSProduct.Product objProduct_Out = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(Convert.ToString(flexData[flexData.RowSel, "ProductID_Out"]).Trim());
                    if (objProduct_Out.IsRequestIMEI != objProduct.IsRequestIMEI)
                    {
                        if (objProduct_Out.IsRequestIMEI)
                        {
                            MessageBox.Show(this, "Sản phẩm nhập phải là sản phẩm có yêu cầu IMEI như sản phẩm xuất đổi. Vui lòng nhập lại sản phẩm nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else
                        {
                            MessageBox.Show(this, "Sản phẩm nhập phải là sản phẩm không có yêu cầu IMEI như sản phẩm xuất đổi. Vui lòng nhập lại sản phẩm nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }

                    if (!CheckStoreMainGroup(cboStore.StoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.INPUT))
                    {
                        MessageBox.Show(this, "Kho này không được phép nhập đổi trên ngành hàng " + objProduct.MainGroupName + "!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    string strProductName = Convert.ToString(objProduct.ProductName).Trim();
                    intVAT = objProduct.VAT;
                    intVATPercent = objProduct.VATPercent;
                    dtbData.Rows[dtbData.Rows.Count - 1]["ProductID_In"] = strProductID;
                    dtbData.Rows[dtbData.Rows.Count - 1]["ProductName_In"] = strProductName;
                    if ((!Convert.IsDBNull(dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_Out"])) && Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_Out"]).Trim() != string.Empty)
                    {
                        if (strProductID != Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["ProductID_Out"]).Trim())
                        {
                            dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_In"] = Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_Out"]).Trim();
                            dtbData.Rows[dtbData.Rows.Count - 1]["IsNew_In"] = dtbData.Rows[dtbData.Rows.Count - 1]["IsNew_Out"];
                        }
                        else
                        {
                            if (Convert.ToBoolean(dtbData.Rows[dtbData.Rows.Count - 1]["IsNew_Out"]) == radNew.Checked)
                            {
                                dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_In"] = string.Empty;
                                dtbData.Rows[dtbData.Rows.Count - 1]["IsNew_In"] = (radNew.Checked == true ? 1 : 0);
                            }
                        }
                    }
                    else
                    {
                        dtbData.Rows[dtbData.Rows.Count - 1]["IMEI_In"] = string.Empty;
                        dtbData.Rows[dtbData.Rows.Count - 1]["IsNew_In"] = (radNew.Checked == true ? 1 : 0);
                    }
                    dtbData.Rows[dtbData.Rows.Count - 1]["VAT"] = intVAT;
                    dtbData.Rows[dtbData.Rows.Count - 1]["VATPercent"] = intVATPercent;

                    txtBarCode.Text = string.Empty;
                    radOutput.Checked = true;
                }
            }
        }

        #endregion

        private void flexData_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexData.Rows.Count <= flexData.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexData.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexData.Cols["IMEI_In"].Index)
            {
                TextBox txt = (TextBox)flexData.Editor;
                txt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            }
        }

    }
}
