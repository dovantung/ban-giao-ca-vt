﻿using ERP.Inventory.PLC.WSProductChangeOrder;
using ERP.Inventory.PLC.WSProductInStock;
using ERP.MasterData.DUI.CustomControl.ImportExcel;
using Library.AppCore;
using Library.AppCore.Other;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.ProductChange
{
    ///Created by: Dương Đình Tú | DEV ERP
    ///Created on: 04/10/2017
    ///TaskID: 4006
    /// <summary>
    ///     Import danh sách yêu cầu - Đổi IMEI | Đổi mã SP | Đổi trạng thái SP
    /// </summary>
    public class ImportProductChange : IExcelImport
    {
        ClsImportExcel objClsImportExcel;
        string _formTitle = "Import yêu cầu xuất đổi hàng";
        private frmProductChangeOrder _dataContext;
        private OptionType _productChangeType;
        private int _outputStoreID = -1;
        private bool bolIsChangeStatus = false;
        private List<ProductChangeOrderDT> lstDetail;
        private List<ProductChangeOrderDT> listReaded = new List<ProductChangeOrderDT>();
        private List<MasterData.PLC.WSProductChangeType.ProductChangeType_PStatus> lstInStockStatus_By_Type;
        public ImportProductChange(OptionType productChangeType, frmProductChangeOrder parent_Context,
            int outputStoreID)
        {
            _productChangeType = productChangeType;
            _dataContext = parent_Context;
            objClsImportExcel = new ClsImportExcel(this, true);
            objClsImportExcel.OnCleaningCache += onCleanCache;
            objClsImportExcel.e_OpenDataExcel += _e_OpenDataExcel_Click;
            _outputStoreID = outputStoreID;
            lstDetail = new List<ProductChangeOrderDT>();
            CreateColumnImport();
        }

        public ImportProductChange(OptionType productChangeType, List<MasterData.PLC.WSProductChangeType.ProductChangeType_PStatus> lstPStatus, bool bolIsChangeStatus, frmProductChangeOrder parent_Context,
            int outputStoreID)
        {
            _productChangeType = productChangeType;
            _dataContext = parent_Context;
            objClsImportExcel = new ClsImportExcel(this, true);
            objClsImportExcel.OnCleaningCache += onCleanCache;
            objClsImportExcel.e_OpenDataExcel += _e_OpenDataExcel_Click;
            _outputStoreID = outputStoreID;
            this.bolIsChangeStatus = bolIsChangeStatus;
            lstDetail = new List<ProductChangeOrderDT>();
            lstInStockStatus_By_Type = lstPStatus;
            CreateColumnImport();
        }

        private void onCleanCache(bool o) { listReaded = new List<ProductChangeOrderDT>(); lstDetail = new List<ProductChangeOrderDT>(); }
        public string IExcelImport_FormTitle
        {
            get
            {
                return _formTitle;
            }
        }

        public void CreateColumnImport()
        {
            switch (_productChangeType)
            {
                case OptionType.DOISANGIMEIKHAC:
                    {
                        objClsImportExcel.InitializeColumnGridView(
                            new DataColumnInfor("IMEI_Out", "IMEI [xuất]", typeof(string), 160, true, 2, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("IMEI_IN", "IMEI [nhập]", typeof(string), 160, true, 6, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("ChangeNote", "Ghi chú", typeof(string), 250, true, 10, DataColumnInfor.TypeShow.ALL)
                        ); break;
                    }
                case OptionType.DOISANGSANPHAMKHAC:
                    {
                        objClsImportExcel.InitializeColumnGridView(
                            new DataColumnInfor("ProductID_Out", "Mã SP [xuất]", typeof(string), 160, true, 1, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("IMEI_Out", "IMEI [xuất]", typeof(string), 160, true, 2, DataColumnInfor.TypeShow.ALL),

                            new DataColumnInfor("ProductID_IN", "Mã SP [nhập]", typeof(string), 160, true, 5, DataColumnInfor.TypeShow.ALL),

                            new DataColumnInfor("Quantity", "Số lượng", typeof(string), 70, true, 9, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("ChangeNote", "Ghi chú", typeof(string), 250, true, 10, DataColumnInfor.TypeShow.ALL)

                        ); break;
                    }
                case OptionType.DOITRANGTHAIIMEI:
                    {
                        objClsImportExcel.InitializeColumnGridView(
                            new DataColumnInfor("IMEI_Out", "IMEI [xuất]", typeof(string), 160, true, 2, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("ChangeNote", "Ghi chú", typeof(string), 250, true, 10, DataColumnInfor.TypeShow.ALL)

                        ); break;
                    }
                case OptionType.DOITRANGTHAI_SANPHAM:
                    {
                        objClsImportExcel.InitializeColumnGridView(
                            new DataColumnInfor("IMEI_Out", "IMEI [xuất]", typeof(string), 160, true, 2, DataColumnInfor.TypeShow.ALL),
                            new DataColumnInfor("ChangeNote", "Ghi chú", typeof(string), 250, true, 10, DataColumnInfor.TypeShow.ALL)
                        ); break;
                    }
                default: { throw new Exception("Product change type undefine"); }
            }
        }
        public List<object> IExcelImport_ReturnDataImport(DataTable dtbData, DataRow drExcelFile, out bool IsError, out string strMessenger)
        {
            List<object> result = new List<object>();
            IsError = false;
            strMessenger = string.Empty;
            ProductChangeOrderDT obj = null;
            try
            {
                obj = BuildDataImport(dtbData, drExcelFile, out IsError, out strMessenger);
            }
            catch (Exception ex)
            {
                strMessenger = "Error " + ex.Message;
            }
            finally { result = BuildResultStructure(obj); }

            IsError = !string.IsNullOrEmpty(strMessenger);
            return result;
        }
        private List<object> BuildResultStructure(ProductChangeOrderDT obj)
        {
            List<object> result = new List<object>();
            try
            {
                if (obj == null)
                    obj = new ProductChangeOrderDT();
                switch (_productChangeType)
                {
                    case OptionType.DOISANGIMEIKHAC:
                        {
                            result = new List<object>() { obj.IMEI_Out, obj.IMEI_IN, obj.ChangeNote }; break;
                        }
                    case OptionType.DOITRANGTHAIIMEI:
                        {
                            result = new List<object>() { obj.IMEI_Out, obj.ChangeNote }; break;
                        }
                    case OptionType.DOISANGSANPHAMKHAC:
                        {
                            result = new List<object>() { obj.ProductID_Out, obj.IMEI_Out, obj.ProductID_IN, obj.Quantity, obj.ChangeNote }; break;
                        }
                    default:
                        {
                            result = new List<object>()
                            {
                                obj.ProductID_Out, obj.IMEI_Out, obj.ProductID_OutName, obj.IsNew_Out,
                                obj.ProductID_IN, obj.IMEI_IN, obj.ProductID_INName, obj.IsNew_IN,
                                obj.Quantity, obj.ChangeNote }; break;
                        }
                }
            }
            catch (Exception ex) { throw ex; }
            return result;
        }
        private ProductChangeOrderDT BuildDataImport(DataTable dtbData, DataRow drExcelFile, out bool IsError, out string strMessenger)
        {
            ProductChangeOrderDT result = null;
            IsError = false;
            strMessenger = string.Empty;
            try
            {
                isIMEIBelongToProduct = isProductExist_By_IDOut = isProductExist_By_IDIN = isProductExist_By_IMEIOUT = isProductExist_By_IMEIIN = isProductOutRequireIMEI = false;
                objProduct_By_IMEIOUT = objProduct_By_IMEIIN = null;
                objProduct_By_IDOut = objProduct_By_IDIN = null;
                result = GetRowValue(drExcelFile, out strMessenger);
                IsValidObj(ref result, ref strMessenger);
            }
            catch (Exception exception)
            {
                strMessenger = exception.Message;
            };

            IsError = !string.IsNullOrEmpty(strMessenger);
            return result;
        }
        private bool IsIMEIExist(string IMEI)
        {
            try
            {
                var objProductInStock = _dataContext.objPLCProductInStock.LoadInfo(IMEI, _outputStoreID);
                if (objProductInStock == null) return false;
                return true;
            }
            catch { return false; }
        }
        private ProductChangeOrderDT GetRowValue(DataRow drExcelFile, out string message)
        {
            ProductChangeOrderDT result = new ProductChangeOrderDT();
            message = string.Empty;
            try
            {
                if (_outputStoreID < 0)
                    message += "Chưa chọn mã kho, ";
                switch (_productChangeType)
                {
                    case OptionType.DOISANGIMEIKHAC:
                        {
                            result.IMEI_Out = (drExcelFile[0] ?? string.Empty).ToString().Trim();
                            result.IMEI_IN = (drExcelFile[1] ?? string.Empty).ToString().Trim();
                            result.ChangeNote = (drExcelFile[2] ?? string.Empty).ToString().Trim();

                            //Kiểm tra thông tin xuất
                            if (!string.IsNullOrEmpty(result.IMEI_Out))
                            {
                                if (!CheckObject.CheckIMEI(result.IMEI_Out))
                                    message += "IMEI xuất sai định dạng, ";
                                else
                                {
                                    objProduct_By_IMEIOUT = _dataContext.objPLCProductInStock.LoadInfo(result.IMEI_Out, _outputStoreID);
                                    isProductExist_By_IMEIOUT = objProduct_By_IMEIOUT != null;

                                }
                            }
                            else message += "IMEI xuất không được bỏ trống, ";
                            //kiểm tra thông tin nhập
                            if (!string.IsNullOrEmpty(result.IMEI_IN))
                            {
                                if (!CheckObject.CheckIMEI(result.IMEI_IN))
                                    message += "IMEI nhập sai định dạng, ";
                                else
                                {
                                    objProduct_By_IMEIIN = _dataContext.objPLCProductInStock.LoadInfo(result.IMEI_IN, _outputStoreID);
                                    isProductExist_By_IMEIIN = objProduct_By_IMEIIN != null;
                                }
                            }
                            else message += "IMEI nhập không được bỏ trống, ";

                            if (string.IsNullOrEmpty(message.ToString().Trim())
                                && result.IMEI_IN == result.IMEI_Out)
                                message += "IMEI nhập không được giống IMEI xuất, ";
                            //Gán trạng thái sản phẩm
                            if (isProductExist_By_IMEIOUT)
                            {
                                result.InStockStatusID_Out = objProduct_By_IMEIOUT.InStockStatusID;
                                result.InStockStatusID_In = objProduct_By_IMEIOUT.InStockStatusID;
                            }

                            break;
                        }
                    case OptionType.DOISANGSANPHAMKHAC:
                        {
                            result.ProductID_Out = (drExcelFile[0] ?? string.Empty).ToString().Trim();
                            result.IMEI_Out = (drExcelFile[1] ?? string.Empty).ToString().Trim();
                            result.ProductID_IN = (drExcelFile[2] ?? string.Empty).ToString().Trim();
                            string quantity = (drExcelFile[3] ?? string.Empty).ToString().Trim();

                            //Kiểm tra sp xuất
                            if (!string.IsNullOrEmpty(result.ProductID_Out) && !string.IsNullOrEmpty(result.IMEI_Out))
                            {
                                objProduct_By_IMEIOUT = _dataContext.objPLCProductInStock.LoadInfo(result.IMEI_Out, _outputStoreID);
                                objProduct_By_IDOut = MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(result.ProductID_Out);
                                isProductExist_By_IMEIOUT = objProduct_By_IMEIOUT != null;
                                isProductExist_By_IDOut = objProduct_By_IDOut != null;
                                isProductOutRequireIMEI = isProductExist_By_IDOut && objProduct_By_IDOut.IsRequestIMEI;
                                isIMEIBelongToProduct = isProductExist_By_IDOut && isProductExist_By_IMEIOUT && objProduct_By_IMEIOUT.ProductID == objProduct_By_IDOut.ProductID;

                                if (!CheckObject.CheckIMEI(result.IMEI_Out))
                                    message += "IMEI xuất sai định dạng, ";

                                else if (isProductOutRequireIMEI)
                                {
                                    result.Quantity = 1;
                                }
                                else
                                {
                                    message += "Mã SP xuất không yêu cầu IMEI, ";
                                    if (string.IsNullOrEmpty(quantity))
                                        message += "Vui lòng nhập số lượng, ";
                                    else if (!IsStringNumber(quantity))
                                        message += "Số lượng phải là kiểu số, ";
                                    else if (Convert.ToInt32(quantity) <= 0)
                                        message += "Số lượng > 0, ";
                                    else result.Quantity = Convert.ToInt32(quantity);
                                }
                            }
                            else if (!string.IsNullOrEmpty(result.ProductID_Out) && string.IsNullOrEmpty(result.IMEI_Out))
                            {
                                objProduct_By_IDOut = MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(result.ProductID_Out);
                                isProductExist_By_IDOut = objProduct_By_IDOut != null;
                                isProductOutRequireIMEI = (isProductExist_By_IDOut && objProduct_By_IDOut.IsRequestIMEI);
                                if (!isProductExist_By_IDOut) { }
                                else if (isProductOutRequireIMEI)
                                {
                                    result.Quantity = 1;
                                }
                                else if (string.IsNullOrEmpty(quantity))
                                    message += "Vui lòng nhập số lượng, ";
                                else if (!IsStringNumber(quantity))
                                    message += "Số lượng phải là kiểu số, ";
                                else if (Convert.ToInt32(quantity) <= 0)
                                    message += "Số lượng > 0, ";
                                else result.Quantity = Convert.ToInt32(quantity);
                            }
                            else if (string.IsNullOrEmpty(result.ProductID_Out) && !string.IsNullOrEmpty(result.IMEI_Out))
                            {
                                objProduct_By_IMEIOUT = _dataContext.objPLCProductInStock.LoadInfo(result.IMEI_Out, _outputStoreID);
                                isProductExist_By_IMEIOUT = objProduct_By_IMEIOUT != null;

                                if (!CheckObject.CheckIMEI(result.IMEI_Out))
                                    message += "IMEI xuất sai định dạng, ";
                                if (!isProductExist_By_IMEIOUT) { }
                                else result.Quantity = 1;
                            }
                            else
                            {
                                message += "Mã SP xuất hoặc IMEI xuất không được bỏ trống, ";
                                if (string.IsNullOrEmpty(quantity))
                                    message += "Vui lòng nhập số lượng, ";
                                else if (!IsStringNumber(quantity))
                                    message += "Số lượng phải là kiểu số, ";
                                else if (Convert.ToInt32(quantity) <= 0)
                                    message += "Số lượng > 0, ";
                                else result.Quantity = Convert.ToInt32(quantity);
                            }


                            //Kiểm tra sp nhập
                            if (!string.IsNullOrEmpty(result.ProductID_IN))
                            {
                                objProduct_By_IDIN = MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(result.ProductID_IN);
                                isProductExist_By_IDIN = objProduct_By_IDIN != null;
                            }
                            else message += "Mã SP nhập không được bỏ trống, ";
                            if (string.IsNullOrEmpty(message.ToString().Trim())
                                && result.ProductID_IN == result.ProductID_Out)
                                message += "Sản phẩm nhập không được giống sản phẩm xuất, ";

                            result.ChangeNote = (drExcelFile[4] ?? string.Empty).ToString().Trim();
                            if (isProductExist_By_IMEIOUT)
                            {
                                result.InStockStatusID_Out = objProduct_By_IMEIOUT.InStockStatusID;
                                result.InStockStatusID_In = objProduct_By_IMEIOUT.InStockStatusID;
                            }
                            break;
                        }
                    case OptionType.DOITRANGTHAIIMEI:
                        {
                            result.IMEI_Out = (drExcelFile[0] ?? string.Empty).ToString().Trim();
                            result.ChangeNote = (drExcelFile[1] ?? string.Empty).ToString().Trim();

                            if (!string.IsNullOrEmpty(result.IMEI_Out))
                            {
                                objProduct_By_IMEIOUT = _dataContext.objPLCProductInStock.LoadInfo(result.IMEI_Out, _outputStoreID);
                                isProductExist_By_IMEIOUT = objProduct_By_IMEIOUT != null;
                            }
                            else message += "IMEI xuất không được bỏ trống, ";
                            if (isProductExist_By_IMEIOUT)
                            {
                                result.IsNew_Out = objProduct_By_IMEIOUT.IsNew;
                                result.IsNew_IN = !objProduct_By_IMEIOUT.IsNew;
                                result.InStockStatusID_Out = objProduct_By_IMEIOUT.InStockStatusID;
                                result.InStockStatusID_In = (objProduct_By_IMEIOUT.IsNew) ? 0 : 1;
                            }
                            if (string.IsNullOrEmpty(message.ToString().Trim())
                                && result.InStockStatusID_Out == result.InStockStatusID_In)
                                message += "Trạng thái nhập không được giống trạng thái xuất, ";
                            break;
                        }
                    case OptionType.DOITRANGTHAI_SANPHAM:
                        {
                            result.IMEI_Out = (drExcelFile[0] ?? string.Empty).ToString().Trim();
                            result.ChangeNote = (drExcelFile[1] ?? string.Empty).ToString().Trim();

                            if (!string.IsNullOrEmpty(result.IMEI_Out))
                            {
                                objProduct_By_IMEIOUT = _dataContext.objPLCProductInStock.LoadInfo(result.IMEI_Out, _outputStoreID);
                                isProductExist_By_IMEIOUT = objProduct_By_IMEIOUT != null;
                            }
                            else message += "IMEI xuất không được bỏ trống, ";
                            if (isProductExist_By_IMEIOUT)
                            {
                                var objStatus = lstInStockStatus_By_Type.First(o => o.ProductStatusIDFrom == objProduct_By_IMEIOUT.InStockStatusID);
                                if (objStatus.Equals(null) || !objStatus.IsSelect)
                                {
                                    message += "Trạng thái sản phẩm xuất không thuộc khai báo trạng thái nguồn, ";
                                }
                                else
                                {
                                    result.InStockStatusID_Out = objStatus.ProductStatusIDFrom;
                                    result.InStockStatusID_In = objStatus.ProductStatusIDTo;
                                }
                            }
                            if (string.IsNullOrEmpty(message.ToString().Trim())
                                && result.InStockStatusID_Out == result.InStockStatusID_In)
                                message += "Trạng thái nhập không được giống trạng thái xuất, ";
                            break;
                        }
                    default: { break; }
                }
                if (string.IsNullOrEmpty(result.ChangeNote))
                    message += "Ghi chú không được bỏ trống, ";
                else if (result.ChangeNote.Length > 1000)
                    message += "Ghi chú vượt quá độ dài cho phép 1000, ";

                //Kiểm tra đã tồn tại
                if (listReaded.Any(d =>
                    (!string.IsNullOrEmpty(GetString(d.IMEI_IN)) && GetString(result.IMEI_IN) == GetString(d.IMEI_IN))
                    || (!string.IsNullOrEmpty(GetString(d.IMEI_Out)) && GetString(result.IMEI_Out) == GetString(d.IMEI_Out))
                    || (!string.IsNullOrEmpty(GetString(d.IMEI_IN)) && GetString(result.IMEI_Out) == GetString(d.IMEI_IN))
                    || (!string.IsNullOrEmpty(GetString(d.IMEI_Out)) && GetString(result.IMEI_IN) == GetString(d.IMEI_Out))
                    || ((!string.IsNullOrEmpty(GetString(result.ProductID_Out)) && GetString(d.ProductID_Out) == GetString(result.ProductID_Out) &&
                            isProductExist_By_IDOut && !isProductOutRequireIMEI))
                    || ((!string.IsNullOrEmpty(GetString(result.IMEI_Out))) && (!string.IsNullOrEmpty(GetString(result.ProductID_Out)))
                        && (GetString(d.ProductID_Out) == GetString(result.ProductID_Out) && GetString(d.IMEI_Out) == GetString(result.IMEI_Out)))
                    || (!string.IsNullOrEmpty(GetString(result.ProductID_Out)) && string.IsNullOrEmpty(GetString(result.IMEI_Out))
                        && GetString(d.ProductID_Out) == GetString(result.ProductID_Out) && string.IsNullOrEmpty(GetString(d.IMEI_Out)))
                    ))
                {
                    message += "Mã SP hoặc IMEI đã tồn tại trên lưới, ";
                }
            }
            catch (Exception exception)
            {
                FileLogger.LogAction(exception);
            }

            if (result != null)
                listReaded.Add(result);
            return result;
        }
        private bool IsValidObj(ref ProductChangeOrderDT obj, ref string message)
        {
            try
            {
                if (obj == null) { message += "Không đọc được dữ liệu"; goto end; }
                CheckOnBusinessRule(ref obj, ref message);
                // CheckObjectBusinessRuleData(ref obj, ref message);
            }
            catch (Exception ex) { message += ex.ToString(); }
        end:
            return string.IsNullOrEmpty(message);
        }
        public void Import()
        {
            if (objClsImportExcel.DoImportExcel(true))
            {
                if (objClsImportExcel.dtbReturn != null)
                {
                    _dataContext.objProductChangeOrder.ProductChangeOrderDT = lstDetail.ToArray();
                    MessageBox.Show("Import thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void _e_OpenDataExcel_Click(object sender, EventArgs e)
        {
            DataTable dtbExcel = (DataTable)sender;
            string message = string.Empty;
            if (dtbExcel != null && dtbExcel.Rows.Count > 0)
            {
                switch (_productChangeType)
                {
                    case OptionType.DOISANGIMEIKHAC:
                        {
                            #region DOISANGIMEIKHAC
                            
                            dtbExcel.Columns[0].ColumnName = "IMEI_Out";
                            dtbExcel.Columns[1].ColumnName = "IMEI_IN";
                            dtbExcel.Columns[2].ColumnName = "ChangeNote";
                            dtbExcel.Columns.Add("ISERROR", typeof(int));
                            dtbExcel.Columns.Add("NOTE", typeof(string));

                            var lstNote = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["ChangeNote"].ToString()));
                            if (lstNote.Any() && lstNote.Count() > 0)
                            {
                                lstNote.All(c => { c["ISERROR"] = true; c["NOTE"] = "Ghi chú không được bỏ trống "; return true; });
                            }

                            var lstNoteLength = dtbExcel.AsEnumerable().Where(x => x["ChangeNote"].ToString().Trim().Length > 1000);
                            if (lstNoteLength.Any() && lstNoteLength.Count() > 0)
                            {
                                lstNoteLength.All(c => { c["ISERROR"] = true; c["NOTE"] = "Ghi chú vượt quá độ dài cho phép 1000 "; return true; });
                            }
                            var lstIMEI_Out = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()));
                            if (lstIMEI_Out.Any() && lstIMEI_Out.Count() > 0)
                            {
                                lstIMEI_Out.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không được bỏ trống "; return true; });
                            }

                            var lstIMEI_OutFormat = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()) && !CheckObject.CheckIMEI(x["IMEI_Out"].ToString()));
                            if (lstIMEI_OutFormat.Any() && lstIMEI_OutFormat.Count() > 0)
                            {
                                lstIMEI_OutFormat.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất sai định dạng "; return true; });
                            }
                            var lstIMEI_IN = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["IMEI_IN"].ToString()));
                            if (lstIMEI_IN.Any() && lstIMEI_IN.Count() > 0)
                            {
                                lstIMEI_IN.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI nhập không được bỏ trống "; return true; });
                            }

                            var lstIMEI_INFormat = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["IMEI_IN"].ToString()) && !CheckObject.CheckIMEI(x["IMEI_IN"].ToString()));
                            if (lstIMEI_INFormat.Any() && lstIMEI_INFormat.Count() > 0)
                            {
                                lstIMEI_INFormat.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI nhập sai định dạng "; return true; });
                            }

                            var lstClear = dtbExcel.Select("ISERROR <> 1 or ISERROR is null");



                            // Check trùng Imei trên lưới imei xuất
                            Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
                            for (int i = 0; i < lstClear.Count(); i++)
                            {
                                if (dicCheckIMEI.ContainsKey(lstClear[i]["IMEI_Out"].ToString()))
                                {
                                    dicCheckIMEI[lstClear[i]["IMEI_Out"].ToString()].Add(i);
                                }
                                else
                                {
                                    List<int> lstIndex = new List<int>();
                                    lstIndex.Add(i);
                                    dicCheckIMEI.Add(lstClear[i]["IMEI_Out"].ToString(), lstIndex);
                                }
                            }

                            foreach (string strIMEI in dicCheckIMEI.Keys)
                            {
                                if (dicCheckIMEI[strIMEI].Count() > 1)
                                {
                                    for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                                    {
                                        lstClear[dicCheckIMEI[strIMEI][i]]["ISERROR"] = true;
                                        lstClear[dicCheckIMEI[strIMEI][i]]["NOTE"] = lstClear[dicCheckIMEI[strIMEI][i]]["NOTE"].ToString().Replace("Trùng IMEI trên file Excel", "") + "Trùng IMEI trên file Excel";
                                    }
                                }
                            }


                            // Check trùng Imei trên lưới imei nhập
                            Dictionary<string, List<int>> dicCheckIMEI_IN = new Dictionary<string, List<int>>();
                            for (int i = 0; i < lstClear.Count(); i++)
                            {
                                if (dicCheckIMEI_IN.ContainsKey(lstClear[i]["IMEI_IN"].ToString()))
                                {
                                    dicCheckIMEI_IN[lstClear[i]["IMEI_IN"].ToString()].Add(i);
                                }
                                else
                                {
                                    List<int> lstIndex = new List<int>();
                                    lstIndex.Add(i);
                                    dicCheckIMEI_IN.Add(lstClear[i]["IMEI_IN"].ToString(), lstIndex);
                                }
                            }

                            foreach (string strIMEI in dicCheckIMEI_IN.Keys)
                            {
                                if (dicCheckIMEI_IN[strIMEI].Count() > 1)
                                {
                                    for (int i = 1; i < dicCheckIMEI_IN[strIMEI].Count(); i++)
                                    {
                                        lstClear[dicCheckIMEI_IN[strIMEI][i]]["ISERROR"] = true;
                                        lstClear[dicCheckIMEI_IN[strIMEI][i]]["NOTE"] = lstClear[dicCheckIMEI_IN[strIMEI][i]]["NOTE"].ToString().Replace("Trùng IMEI trên file Excel", "") + "Trùng IMEI trên file Excel";
                                    }
                                }
                            }


                            var lstTrim = dtbExcel.Select("ISERROR <> 1  or ISERROR is null");

                            lstTrim.All(c => { c["IMEI_Out"] = c["IMEI_Out"].ToString().Trim(); c["IMEI_IN"] = c["IMEI_IN"].ToString().Trim(); c["ChangeNote"] = c["ChangeNote"].ToString().Trim(); return true; });
                            dtbExcel.AcceptChanges();
                            //var duplicates = lstTrim.GroupBy(i => new { IMEI_Out = i.Field<string>("IMEI_Out"), IMEI_IN = i.Field<string>("IMEI_IN") }).Where(g => g.Count() > 1).Select(g => new { g.Key.IMEI_Out, g.Key.IMEI_IN }).ToList();

                            // Check Imei xuất trong DB
                            DataTable dtDataOut = new DataTable();
                            dtDataOut.Columns.Add("IMEI");
                            foreach (DataRow item in lstTrim)
                            {
                                dtDataOut.Rows.Add(item["IMEI_Out"]);
                            }
                            DataTable dtbTotalImei = null;
                            DataTable dtbTotalImeiNotInStock = null;
                            DataTable dtbTotalImeiUse = null;
                            DataSet dsData = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("CHECKIMEI_INSTOCK_NOTIN_BORROW", new string[] { "v_Out1", "v_Out2", "v_Out3" }, new object[] { "@StoreID", _outputStoreID, "@IMEI", DUIInventoryCommon.CreateXMLFromDataTable(dtDataOut, "IMEI") });
                            if (dsData != null && dsData.Tables.Count > 0)
                            {
                                dtbTotalImei = dsData.Tables[0].Copy();
                            }
                            if (dsData != null && dsData.Tables.Count > 1)
                            {
                                dtbTotalImeiNotInStock = dsData.Tables[1].Copy();
                            }
                            if (dsData != null && dsData.Tables.Count > 2)
                            {
                                dtbTotalImeiUse = dsData.Tables[2].Copy();
                            }


                            DataTable dtDataIN = new DataTable();
                            dtDataIN.Columns.Add("IMEI");
                            foreach (DataRow item in lstTrim)
                            {
                                dtDataIN.Rows.Add(item["IMEI_IN"]);
                            }
                            DataTable dtbTotalImei2 = null;
                            DataTable dtbTotalImeiNotInStock2 = null;
                            DataTable dtbTotalImeiUse2 = null;
                            DataSet dsData2 = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("CHECKIMEI_INSTOCK_NOTIN_BORROW", new string[] { "v_Out1", "v_Out2", "v_Out3" }, new object[] { "@StoreID", _outputStoreID, "@IMEI", DUIInventoryCommon.CreateXMLFromDataTable(dtDataIN, "IMEI") });
                            if (dsData2 != null && dsData2.Tables.Count > 0)
                            {
                                dtbTotalImei2 = dsData2.Tables[0].Copy();
                            }
                            if (dsData2 != null && dsData2.Tables.Count > 1)
                            {
                                dtbTotalImeiNotInStock2 = dsData2.Tables[1].Copy();
                            }
                            if (dsData2 != null && dsData2.Tables.Count > 2)
                            {
                                dtbTotalImeiUse2 = dsData2.Tables[2].Copy();
                            }


                            var LstIMEIInProductStoreIN = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                          join i in dtbTotalImei2.AsEnumerable()
                                                          on o["IMEI_IN"] equals i["IMEI"]
                                                          select o;
                            if (LstIMEIInProductStoreIN.Any())
                            {
                                LstIMEIInProductStoreIN.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI nhập đã tồn tại trong hệ thống"; return true; });
                            }



                            var LstIMEIInProductOrder = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                        join i in dtbTotalImei.Select("ISORDER = 1")
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                        select o;
                            if (LstIMEIInProductOrder.Any())
                            {
                                LstIMEIInProductOrder.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI đã được đặt hàng"; return true; });
                            }




                            List<string> lstIMEI = new List<string>();
                            lstIMEI = lstIMEI.Concat(dtbTotalImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList()).ToList();
                            var filteredRows = dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                .Where(row => !lstIMEI.Contains(row.Field<string>("IMEI_Out")));
                            if (filteredRows.Any())
                            {
                                filteredRows.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không tồn tại"; return true; });
                            }



                            var LstIMEIInProductStore = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                        join i in dtbTotalImei.Select("STOREID <> " + _outputStoreID)
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                        select o;
                            if (LstIMEIInProductStore.Any())
                            {
                                LstIMEIInProductStore.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không tồn trong kho xuất"; return true; });
                            }

                            var LstIMEINotRealInput = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                      join i in dtbTotalImei.Select("IsCheckRealInput = 0")
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                      select o;
                            if (LstIMEINotRealInput.Any())
                            {
                                LstIMEINotRealInput.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất này chưa được xác nhận nhập kho"; return true; });
                            }


                            var LstIMEINotInStock = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                    join i in dtbTotalImeiNotInStock.AsEnumerable()
                                                    on o["IMEI_Out"] equals i["IMEI"]
                                                    select o;
                            if (LstIMEINotInStock.Any())
                            {
                                LstIMEINotInStock.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất đang cho mượn"; return true; });
                            }

                            var LstIMEIUse = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                             join i in dtbTotalImeiUse.AsEnumerable()
                                                    on o["IMEI_Out"] equals i["IMEI"]
                                             select o;
                            if (LstIMEIUse.Any())
                            {
                                LstIMEIUse.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý"; return true; });
                            }
                            var LstIMEIValidate = dtbExcel.Select("ISERROR <> 1  or ISERROR is null");
                            dtbTotalImei.PrimaryKey = new DataColumn[] { dtbTotalImei.Columns["IMEI"] };
                            dtbTotalImei2.PrimaryKey = new DataColumn[] { dtbTotalImei2.Columns["IMEI"] };
                            foreach (DataRow item in LstIMEIValidate)
                            {
                                ProductChangeOrderDT result = new ProductChangeOrderDT();
                                result.IMEI_Out = item["IMEI_Out"].ToString();
                                result.IMEI_IN = item["IMEI_IN"].ToString();
                                result.ChangeNote = item["ChangeNote"].ToString();
                                result.Quantity = 1;
                                DataRow[] productOut = dtbTotalImei.Select("IMEI = '" + result.IMEI_Out + "'");
                                if (productOut.Length > 0)
                                {
                                    result.ProductID_Out = productOut[0]["ProductID"].ToString();
                                    result.ProductID_OutName = productOut[0]["ProductName"].ToString();
                                    result.IsNew_Out = Convert.ToBoolean(productOut[0]["IsNew"]);
                                    result.InStockStatusID_In = Convert.ToInt32(productOut[0]["InStockStatusID"]);
                                    result.InStockStatusID_Out = Convert.ToInt32(productOut[0]["InStockStatusID"]);
                                    result.ProductID_IN = productOut[0]["ProductID"].ToString();
                                    result.ProductID_INName = productOut[0]["ProductName"].ToString();
                                    result.IsNew_IN = Convert.ToBoolean(productOut[0]["IsNew"]);
                                    if ((item["IMEI_Out"] ?? "").ToString().Trim() == (item["IMEI_IN"] ?? "").ToString().Trim())
                                    {
                                        item["ISERROR"] = true; item["NOTE"] = "IMEI nhập không được trùng với IMEI xuất";
                                        continue;
                                    }
                                }
                                lstDetail.Add(result);
                            }

                            //foreach (DataRow item in dtbExcel.Rows)
                            //{
                            //    message = string.Empty;
                            //    ProductChangeOrderDT result = new ProductChangeOrderDT();

                            //    result.IMEI_Out = (item[0] ?? string.Empty).ToString().Trim();
                            //    result.IMEI_IN = (item[1] ?? string.Empty).ToString().Trim();
                            //    result.ChangeNote = (item[2] ?? string.Empty).ToString().Trim();

                            //    //Kiểm tra thông tin xuất
                            //    if (!string.IsNullOrEmpty(result.IMEI_Out))
                            //    {
                            //        if (!CheckObject.CheckIMEI(result.IMEI_Out))
                            //        {
                            //            message += "IMEI xuất sai định dạng, ";
                            //            goto end;
                            //        }
                            //        else
                            //        {
                            //            objProduct_By_IMEIOUT = _dataContext.objPLCProductInStock.LoadInfo(result.IMEI_Out, _outputStoreID);
                            //            isProductExist_By_IMEIOUT = objProduct_By_IMEIOUT != null;
                            //            if (!isProductExist_By_IMEIOUT)
                            //            {
                            //                message += "Không tìm thấy thông tin IMEI " + result.IMEI_Out + ", ";
                            //                goto end;
                            //            }
                            //        }
                            //    }
                            //    else
                            //    {
                            //        message += "IMEI xuất không được bỏ trống, ";
                            //        goto end;
                            //    }

                            //    //kiểm tra thông tin nhập
                            //    if (!string.IsNullOrEmpty(result.IMEI_IN))
                            //    {
                            //        if (!CheckObject.CheckIMEI(result.IMEI_IN))
                            //        {
                            //            message += "IMEI nhập sai định dạng, ";
                            //            goto end;
                            //        }
                            //        else
                            //        {
                            //            objProduct_By_IMEIIN = _dataContext.objPLCProductInStock.LoadInfo(result.IMEI_IN, _outputStoreID);
                            //            isProductExist_By_IMEIIN = objProduct_By_IMEIIN != null;

                            //            if (isProductExist_By_IMEIIN)
                            //            {
                            //                message += "IMEI " + result.IMEI_IN + " đã tồn tại trong hệ thống, ";
                            //                goto end;
                            //            }
                            //        }
                            //    }
                            //    else
                            //    {
                            //        message += "IMEI nhập không được bỏ trống, ";
                            //        goto end;
                            //    }


                            //    if (string.IsNullOrEmpty(result.ChangeNote))
                            //    {
                            //        message += "Ghi chú không được bỏ trống, ";
                            //        goto end;
                            //    }
                            //    else if (result.ChangeNote.Length > 1000)
                            //    {
                            //        message += "Ghi chú vượt quá độ dài cho phép 1000, ";
                            //        goto end;
                            //    }

                            //    //Kiểm tra đã tồn tại
                            //    if (listReaded.Any(d =>
                            //        (!string.IsNullOrEmpty(GetString(d.IMEI_IN)) && GetString(result.IMEI_IN) == GetString(d.IMEI_IN))
                            //        || (!string.IsNullOrEmpty(GetString(d.IMEI_Out)) && GetString(result.IMEI_Out) == GetString(d.IMEI_Out))
                            //        || (!string.IsNullOrEmpty(GetString(d.IMEI_IN)) && GetString(result.IMEI_Out) == GetString(d.IMEI_IN))
                            //        || (!string.IsNullOrEmpty(GetString(d.IMEI_Out)) && GetString(result.IMEI_IN) == GetString(d.IMEI_Out))
                            //        || ((!string.IsNullOrEmpty(GetString(result.ProductID_Out)) && GetString(d.ProductID_Out) == GetString(result.ProductID_Out) &&
                            //                isProductExist_By_IDOut && !isProductOutRequireIMEI))
                            //        || ((!string.IsNullOrEmpty(GetString(result.IMEI_Out))) && (!string.IsNullOrEmpty(GetString(result.ProductID_Out)))
                            //            && (GetString(d.ProductID_Out) == GetString(result.ProductID_Out) && GetString(d.IMEI_Out) == GetString(result.IMEI_Out)))
                            //        || (!string.IsNullOrEmpty(GetString(result.ProductID_Out)) && string.IsNullOrEmpty(GetString(result.IMEI_Out))
                            //            && GetString(d.ProductID_Out) == GetString(result.ProductID_Out) && string.IsNullOrEmpty(GetString(d.IMEI_Out)))
                            //        ))
                            //    {
                            //        message += "Mã SP hoặc IMEI đã tồn tại trên lưới, ";
                            //        goto end;
                            //    }

                            //    if (result != null)
                            //        listReaded.Add(result);
                            //    else
                            //    {
                            //        message += "Không đọc được dữ liệu";
                            //        goto end;
                            //    }
                            //    #region Xuất
                            //    ProductInStock objProductOut = null;
                            //    objProductOut = objProduct_By_IMEIOUT;

                            //    if (objProductOut != null)
                            //    {
                            //        result.ProductID_Out = objProductOut.ProductID;
                            //        result.ProductID_IN = objProductOut.ProductID;
                            //        result.ProductID_OutName = objProductOut.ProductName;
                            //        result.ProductID_INName = objProductOut.ProductName;
                            //        result.IsNew_Out = objProductOut.IsNew;
                            //        result.IsNew_IN = objProductOut.IsNew;
                            //        result.Quantity = 1;

                            //        if (!objProductOut.IsCheckRealInput)
                            //        {
                            //            if (!objProductOut.IsRequestIMEI)
                            //            {
                            //                if (objProductOut.Quantity == 0)
                            //                {
                            //                    message += "SP này chưa được xác nhận nhập kho, ";
                            //                    goto end;
                            //                }
                            //            }
                            //            else
                            //            {
                            //                message += "IMEI này chưa được xác nhận nhập kho, ";
                            //                goto end;
                            //            }
                            //        }

                            //        if (objProductOut.Quantity <= 0)
                            //        {
                            //            message += "IMEI " + objProductOut.IMEI + " không có tồn kho trên hệ thống, ";
                            //            goto end;
                            //        }
                            //        if (!string.IsNullOrEmpty(result.IMEI_Out) && _dataContext.CheckImeiUsed(result.IMEI_Out))
                            //        {
                            //            message += "IMEI đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý, ";
                            //            goto end;
                            //        }
                            //        else
                            //        {
                            //            decimal decOut = 0;
                            //            objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, _outputStoreID, objProductOut.ProductID, result.IMEI_Out);
                            //            if (!string.IsNullOrEmpty(result.IMEI_Out))
                            //                if (decOut > 0) { message += "IMEI [" + result.IMEI_Out + "] đang cho mượn, không được thao tác trên IMEI này, ";
                            //                    goto end;
                            //                }
                            //        }
                            //    }
                            //    #endregion                                
                            //    end:
                            //    if (string.IsNullOrEmpty(message) && result != null)
                            //        lstDetail.Add(result);
                            //    item[3] = !string.IsNullOrEmpty(message);
                            //    item[4] = message;
                            //}
                            #endregion
                            break;
                        }
                    case OptionType.DOISANGSANPHAMKHAC:
                        {
                            #region DOISANGSANPHAMKHAC
                          
                            dtbExcel.Columns[0].ColumnName = "ProductID_Out";
                            dtbExcel.Columns[1].ColumnName = "IMEI_Out";
                            dtbExcel.Columns[2].ColumnName = "ProductID_IN";
                            dtbExcel.Columns[3].ColumnName = "Quantity";
                            dtbExcel.Columns[4].ColumnName = "ChangeNote";
                            dtbExcel.Columns.Add("ISERROR", typeof(int));
                            dtbExcel.Columns.Add("NOTE", typeof(string));
                            var lstNote = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["ChangeNote"].ToString()));
                            if (lstNote.Any() && lstNote.Count() > 0)
                            {
                                lstNote.All(c => { c["ISERROR"] = true; c["NOTE"] = "Ghi chú không được bỏ trống "; return true; });
                            }

                            var lstNoteLength = dtbExcel.AsEnumerable().Where(x => x["ChangeNote"].ToString().Trim().Length > 1000);
                            if (lstNoteLength.Any() && lstNoteLength.Count() > 0)
                            {
                                lstNoteLength.All(c => { c["ISERROR"] = true; c["NOTE"] = "Ghi chú vượt quá độ dài cho phép 1000 "; return true; });
                            }
                            var lstProductOut = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["ProductID_Out"].ToString()) && string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()));
                            if (lstProductOut.Any() && lstProductOut.Count() > 0)
                            {
                                lstProductOut.All(c => { c["ISERROR"] = true; c["NOTE"] = "Mã SP xuất hoặc IMEI xuất không được bỏ trống "; return true; });
                            }
                            var lstProductOut2 = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["ProductID_Out"].ToString()) && !string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()) && !CheckObject.CheckIMEI(x["IMEI_Out"].ToString()));
                            if (lstProductOut2.Any() && lstProductOut2.Count() > 0)
                            {
                                lstProductOut2.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất sai định dạng"; return true; });
                            }
                            var lstProductOut3 = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["ProductID_Out"].ToString()) && string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()) && string.IsNullOrWhiteSpace(x["Quantity"].ToString()));
                            if (lstProductOut3.Any() && lstProductOut3.Count() > 0)
                            {
                                lstProductOut3.All(c => { c["ISERROR"] = true; c["NOTE"] = "Vui lòng nhập số lượng"; return true; });
                            }
                            var lstProductOut4 = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["Quantity"].ToString()) && !IsStringNumber(x["Quantity"].ToString()));
                            if (lstProductOut4.Any() && lstProductOut4.Count() > 0)
                            {
                                lstProductOut4.All(c => { c["ISERROR"] = true; c["NOTE"] = "Số lượng phải là kiểu số"; return true; });
                            }
                            var lstProductOut5 = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["Quantity"].ToString()) && IsStringNumber(x["Quantity"].ToString()) && Convert.ToInt32(x["Quantity"].ToString()) <= 0);
                            if (lstProductOut5.Any() && lstProductOut5.Count() > 0)
                            {
                                lstProductOut5.All(c => { c["ISERROR"] = true; c["NOTE"] = "Số lượng > 0"; return true; });
                            }

                            var lstProductOut6 = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["ProductID_Out"].ToString()) && !string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()) && !CheckObject.CheckIMEI(x["IMEI_Out"].ToString()));
                            if (lstProductOut6.Any() && lstProductOut6.Count() > 0)
                            {
                                lstProductOut6.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất sai định dạng"; return true; });
                            }

                            var lstProductOut7 = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["ProductID_IN"].ToString()));
                            if (lstProductOut7.Any() && lstProductOut7.Count() > 0)
                            {
                                lstProductOut7.All(c => { c["ISERROR"] = true; c["NOTE"] = "Mã SP nhập không được bỏ trống "; return true; });
                            }

                            var lstTrim = dtbExcel.Select("ISERROR <> 1  or ISERROR is null");
                            lstTrim.All(c => { c["IMEI_Out"] = c["IMEI_Out"].ToString().Trim(); c["ProductID_Out"] = c["ProductID_Out"].ToString().Trim(); c["ProductID_IN"] = c["ProductID_IN"].ToString().Trim(); c["ChangeNote"] = c["ChangeNote"].ToString().Trim(); return true; });
                            dtbExcel.AcceptChanges();

                            var lstClear = dtbExcel.Select("ISERROR <> 1 or ISERROR is null");

                            // Check trùng Imei trên lưới imei xuất
                            Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
                            for (int i = 0; i < lstClear.Count(); i++)
                            {
                                if (!string.IsNullOrWhiteSpace(lstClear[i]["IMEI_Out"].ToString()))
                                {
                                    if (dicCheckIMEI.ContainsKey(lstClear[i]["IMEI_Out"].ToString()))
                                    {
                                        dicCheckIMEI[lstClear[i]["IMEI_Out"].ToString()].Add(i);
                                    }
                                    else
                                    {
                                        List<int> lstIndex = new List<int>();
                                        lstIndex.Add(i);
                                        dicCheckIMEI.Add(lstClear[i]["IMEI_Out"].ToString(), lstIndex);
                                    }
                                }
                            }

                            foreach (string strIMEI in dicCheckIMEI.Keys)
                            {
                                if (dicCheckIMEI[strIMEI].Count() > 1)
                                {
                                    for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                                    {
                                        lstClear[dicCheckIMEI[strIMEI][i]]["ISERROR"] = true;
                                        lstClear[dicCheckIMEI[strIMEI][i]]["NOTE"] = lstClear[dicCheckIMEI[strIMEI][i]]["NOTE"].ToString().Replace("Trùng IMEI trên file Excel", "") + "Trùng IMEI trên file Excel";
                                    }
                                }
                            }


                            // Check Imei xuất trong DB
                            var lstCheckDB = dtbExcel.Select("ISERROR <> 1 or ISERROR is null");

                            DataTable dtDataOut = new DataTable();
                            dtDataOut.Columns.Add("IMEI");
                            foreach (DataRow item in lstCheckDB)
                            {
                                if (string.IsNullOrWhiteSpace(item["IMEI_Out"].ToString()))
                                {
                                    continue;
                                }
                                dtDataOut.Rows.Add(item["IMEI_Out"]);
                            }
                            DataTable dtbTotalImei = null;
                            DataTable dtbTotalImeiNotInStock = null;
                            DataTable dtbTotalImeiUse = null;
                            DataSet dsData = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("CHECKIMEI_INSTOCK_NOTIN_BORROW", new string[] { "v_Out1", "v_Out2", "v_Out3" }, new object[] { "@StoreID", _outputStoreID, "@IMEI", DUIInventoryCommon.CreateXMLFromDataTable(dtDataOut, "IMEI") });
                            if (dsData != null && dsData.Tables.Count > 0)
                            {
                                dtbTotalImei = dsData.Tables[0].Copy();
                            }
                            if (dsData != null && dsData.Tables.Count > 1)
                            {
                                dtbTotalImeiNotInStock = dsData.Tables[1].Copy();
                            }
                            if (dsData != null && dsData.Tables.Count > 2)
                            {
                                dtbTotalImeiUse = dsData.Tables[2].Copy();
                            }
                            foreach (DataRow item in dtbExcel.Rows)
                            {
                                DataRow[] lstdr = dtbTotalImei.Select("IMEI = '" + item["IMEI_Out"] + "'");
                                if (lstdr.Any())
                                {
                                    item["ProductID_Out"] = lstdr[0]["PRODUCTID"].ToString();
                                    //if (item["ProductID_OUT"] != lstdr[0]["PRODUCTID"].ToString())
                                    //{
                                    //    item["ISERROR"] = true; item["NOTE"] = "IMEI này không có trong sản phẩm";
                                    //}
                                }
                            }

                            DataTable dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct();


                            List<string> lstProduct = new List<string>();
                            lstProduct = lstProduct.Concat(dtbProduct.AsEnumerable().Select(x => x["PRODUCTID"].ToString().Trim()).ToList()).ToList();
                            var filteredRowsProduct = dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                .Where(row => !lstProduct.Contains(row.Field<string>("PRODUCTID_OUT")));
                            if (filteredRowsProduct.Any())
                            {
                                filteredRowsProduct.All(c => { c["ISERROR"] = true; c["NOTE"] = "Mã sản phẩm xuất không tồn tại"; return true; });
                            }
                            var filteredRowsProduct2 = dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                .Where(row => !lstProduct.Contains(row.Field<string>("PRODUCTID_IN")));
                            if (filteredRowsProduct2.Any())
                            {
                                filteredRowsProduct2.All(c => { c["ISERROR"] = true; c["NOTE"] = "Mã sản phẩm nhập không tồn tại"; return true; });
                            }
                            var lstProductOut8 = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["ProductID_Out"].ToString()) && !string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()));
                            if (lstProductOut8.Any() && lstProductOut8.Count() > 0)
                            {

                                var lstProduct2 = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                  join i in dtbProduct.AsEnumerable()
                                                  on o["ProductID_Out"] equals i["PRODUCTID"]
                                                  where i["ISREQUESTIMEI"].ToString() == "0"
                                                  select o;
                                if (lstProduct2.Any())
                                {
                                    lstProduct2.All(c => { c["ISERROR"] = true; c["NOTE"] = "Sản phẩm xuất không yêu cầu nhập IMEI"; return true; });
                                }
                            }

                            var LstIMEIInProductOrder = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                        join i in dtbTotalImei.Select("ISORDER = 1")
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                        select o;
                            if (LstIMEIInProductOrder.Any())
                            {
                                LstIMEIInProductOrder.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI đã được đặt hàng"; return true; });
                            }

                            var lstProduct3 = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                              select o;
                            foreach (DataRow item in lstProduct3)
                            {
                                var p = dtbProduct.Select("PRODUCTID = '" + item["ProductID_Out"] + "'");
                                var p2 = dtbProduct.Select("PRODUCTID = '" + item["ProductID_IN"] + "'");
                                if (Convert.ToBoolean(p[0]["ISREQUESTIMEI"]) != Convert.ToBoolean(p2[0]["ISREQUESTIMEI"]))
                                {
                                    item["ISERROR"] = true; item["NOTE"] = "Sản phẩm không cùng loại(yêu cầu/không yêu cầu IMEI)";
                                    continue;
                                }
                                if ((item["ProductID_Out"] ?? "").ToString().Trim() == (item["ProductID_IN"] ?? "").ToString().Trim())
                                {
                                    item["ISERROR"] = true; item["NOTE"] = "Sản phẩm nhập không được trùng với sản phẩm xuất";
                                    continue;
                                }
                            }


                            //Dictionary<string, List<int>> dicCheckProductOut = new Dictionary<string, List<int>>();
                            //for (int i = 0; i < lstClear.Count(); i++)
                            //{
                            //    if (dicCheckProductOut.ContainsKey(lstClear[i]["ProductID_Out"].ToString()))
                            //    {
                            //        dicCheckProductOut[lstClear[i]["ProductID_Out"].ToString()].Add(i);
                            //    }
                            //    else
                            //    {
                            //        List<int> lstIndex = new List<int>();
                            //        lstIndex.Add(i);
                            //        dicCheckProductOut.Add(lstClear[i]["ProductID_Out"].ToString(), lstIndex);
                            //    }
                            //}

                            //foreach (string strIMEI in dicCheckProductOut.Keys)
                            //{
                            //    if (dicCheckProductOut[strIMEI].Count() > 1)
                            //    {
                            //        for (int i = 1; i < dicCheckProductOut[strIMEI].Count(); i++)
                            //        {
                            //            lstClear[dicCheckProductOut[strIMEI][i]]["ISERROR"] = true;
                            //            lstClear[dicCheckProductOut[strIMEI][i]]["NOTE"] = lstClear[dicCheckProductOut[strIMEI][i]]["NOTE"].ToString().Replace("Trùng sản phẩm xuất trên file Excel", "") + "Trùng sản phẩm xuất trên file Excel";
                            //        }
                            //    }
                            //}


                            //var duplicates = lstTrim.GroupBy(i => new { IMEI_Out = i.Field<string>("IMEI_Out"), IMEI_IN = i.Field<string>("IMEI_IN") }).Where(g => g.Count() > 1).Select(g => new { g.Key.IMEI_Out, g.Key.IMEI_IN }).ToList();


                            List<string> lstIMEI = new List<string>();
                            lstIMEI = lstIMEI.Concat(dtbTotalImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList()).ToList();
                            var filteredRows = dtbExcel.Select("ISERROR <> 1  or ISERROR is null AND IMEI_OUT <> ''")
                                .Where(row => !lstIMEI.Contains(row.Field<string>("IMEI_Out")));
                            if (filteredRows.Any())
                            {
                                filteredRows.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không tồn tại"; return true; });
                            }
                            List<string> lstProductInStock = new List<string>();
                            lstProductInStock = lstProductInStock.Concat(dtbExcel.Select("ISERROR <> 1  or ISERROR is null").AsEnumerable().Select(x => x["PRODUCTID_OUT"].ToString()).Distinct()).ToList();
                            DataTable dtDataProductID = new DataTable();
                            dtDataProductID.Columns.Add("PRODUCT");
                            foreach (var item in lstProductInStock)
                            {
                                dtDataProductID.Rows.Add(item);
                            }
                            DataTable dtbProductInStock = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("CHECKPRODUCT_INSTOCK", new object[] { "@StoreID", _outputStoreID, "@PRODUCTID", DUIInventoryCommon.CreateXMLFromDataTable(dtDataProductID, "PRODUCT") });

                            //var lstProduct4 = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                            //                  select o;

                            foreach (DataRow item in dtbProductInStock.Rows)
                            {
                                if (!Convert.ToBoolean(item["ISREQUESTIMEI"]))
                                {
                                    var p = dtbExcel.Select("ISERROR <> 1  or ISERROR is null and PRODUCTID_OUT = '" + item["ProductID"] + "'");
                                    if (Convert.ToInt32(item["REALQUANTITY"]) != 0 && Convert.ToInt32(item["REALQUANTITY"]) < Convert.ToInt32(p[0]["QUANTITY"]))
                                    {
                                        p[0]["ISERROR"] = true; p[0]["NOTE"] = "Số lượng của sản phẩm phải nhỏ hơn hoặc bằng " + item["REALQUANTITY"] + "!";
                                    }
                                    for (int i = 1; i < p.Length; i++)
                                    {
                                        if (p[0]["PRODUCTID_IN"].ToString() == p[i]["PRODUCTID_IN"].ToString())
                                        {
                                            p[i]["ISERROR"] = true; p[i]["NOTE"] = "Sản phẩm xuất/nhập bị trùng";
                                        }
                                    }

                                    //result.InStockStatusID_In = Convert.ToInt32(productOut[0]["InStockStatusID"]);
                                    //result.InStockStatusID_Out = Convert.ToInt32(productOut[0]["InStockStatusID"]);

                                }
                            }

                            //foreach (DataRow item in lstProduct4)
                            //{
                            //    var p = dtbProductInStock.Select("PRODUCT = '" + item["ProductID_Out"] + "'");
                            //    if (Convert.ToInt32(p[0]["REALQUANTITY"]) != 0 && Convert.ToInt32(p[0]["REALQUANTITY"]) < Convert.ToInt32(item["QUANTITY"]))
                            //    {
                            //        item["ISERROR"] = true; item["NOTE"] = "Số lượng của sản phẩm phải nhỏ hơn hoặc bằng " + p[0]["REALQUANTITY"] + "!";
                            //    }
                            //}
                            var LstIMEIInProductStore = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                        join i in dtbTotalImei.Select("STOREID <> " + _outputStoreID)
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                        select o;
                            if (LstIMEIInProductStore.Any())
                            {
                                LstIMEIInProductStore.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không tồn trong kho xuất"; return true; });
                            }

                            var LstIMEINotRealInput = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                      join i in dtbTotalImei.Select("IsCheckRealInput = 0")
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                      select o;
                            if (LstIMEINotRealInput.Any())
                            {
                                LstIMEINotRealInput.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất này chưa được xác nhận nhập kho"; return true; });
                            }


                            var LstIMEINotInStock = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                    join i in dtbTotalImeiNotInStock.AsEnumerable()
                                                    on o["IMEI_Out"] equals i["IMEI"]
                                                    select o;
                            if (LstIMEINotInStock.Any())
                            {
                                LstIMEINotInStock.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất đang cho mượn"; return true; });
                            }

                            var LstIMEIUse = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                             join i in dtbTotalImeiUse.AsEnumerable()
                                                    on o["IMEI_Out"] equals i["IMEI"]
                                             select o;
                            if (LstIMEIUse.Any())
                            {
                                LstIMEIUse.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý"; return true; });
                            }
                            var LstIMEIValidate = dtbExcel.Select("ISERROR <> 1  or ISERROR is null");
                            dtbTotalImei.PrimaryKey = new DataColumn[] { dtbTotalImei.Columns["IMEI"] };
                            foreach (DataRow item in LstIMEIValidate)
                            {
                                ProductChangeOrderDT result = new ProductChangeOrderDT();
                                result.IMEI_Out = item["IMEI_Out"].ToString();
                                result.IMEI_IN = item["IMEI_Out"].ToString();
                                result.ChangeNote = item["ChangeNote"].ToString();
                                result.ProductID_Out = item["ProductID_Out"].ToString();
                                DataRow[] product = dtbProduct.Select("ProductID = '" + result.ProductID_Out + "'");
                                if (product.Length > 0)
                                {
                                    result.ProductID_OutName = product[0]["ProductName"].ToString();
                                }
                                result.ProductID_IN = item["ProductID_IN"].ToString();
                                DataRow[] product2 = dtbProduct.Select("ProductID = '" + result.ProductID_IN + "'");
                                if (product.Length > 0)
                                {
                                    result.ProductID_INName = product2[0]["ProductName"].ToString();
                                }
                                DataRow[] productOut = dtbTotalImei.Select("IMEI = '" + result.IMEI_Out + "'");
                                if (productOut.Length > 0)
                                {
                                    result.IsNew_Out = Convert.ToBoolean(productOut[0]["IsNew"]);
                                    result.IsNew_IN = Convert.ToBoolean(productOut[0]["IsNew"]);
                                    result.InStockStatusID_In = Convert.ToInt32(productOut[0]["InStockStatusID"]);
                                    result.InStockStatusID_Out = Convert.ToInt32(productOut[0]["InStockStatusID"]);
                                    result.Quantity = 1;
                                    item["QUANTITY"] = 1;
                                }
                                else
                                {
                                    result.InStockStatusID_In = 1;
                                    result.InStockStatusID_Out = 1;
                                    result.Quantity = Convert.ToInt32(item["QUANTITY"]);
                                }
                                lstDetail.Add(result);
                            }
                            #endregion
                            break;
                        }
                    case OptionType.DOITRANGTHAIIMEI:
                        {
                            #region DOITRANGTHAIIMEI
                           
                            dtbExcel.Columns[0].ColumnName = "IMEI_Out";
                            dtbExcel.Columns[1].ColumnName = "ChangeNote";
                            dtbExcel.Columns.Add("ISERROR", typeof(int));
                            dtbExcel.Columns.Add("NOTE", typeof(string));

                            var lstNote = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["ChangeNote"].ToString()));
                            if (lstNote.Any() && lstNote.Count() > 0)
                            {
                                lstNote.All(c => { c["ISERROR"] = true; c["NOTE"] = "Ghi chú không được bỏ trống "; return true; });
                            }

                            var lstNoteLength = dtbExcel.AsEnumerable().Where(x => x["ChangeNote"].ToString().Trim().Length > 1000);
                            if (lstNoteLength.Any() && lstNoteLength.Count() > 0)
                            {
                                lstNoteLength.All(c => { c["ISERROR"] = true; c["NOTE"] = "Ghi chú vượt quá độ dài cho phép 1000 "; return true; });
                            }
                            var lstIMEI_Out = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()));
                            if (lstIMEI_Out.Any() && lstIMEI_Out.Count() > 0)
                            {
                                lstIMEI_Out.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không được bỏ trống "; return true; });
                            }

                            var lstIMEI_OutFormat = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()) && !CheckObject.CheckIMEI(x["IMEI_Out"].ToString()));
                            if (lstIMEI_OutFormat.Any() && lstIMEI_OutFormat.Count() > 0)
                            {
                                lstIMEI_OutFormat.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất sai định dạng "; return true; });
                            }

                            var lstClear = dtbExcel.Select("ISERROR <> 1 or ISERROR is null");



                            // Check trùng Imei trên lưới imei xuất
                            Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
                            for (int i = 0; i < lstClear.Count(); i++)
                            {
                                if (dicCheckIMEI.ContainsKey(lstClear[i]["IMEI_Out"].ToString()))
                                {
                                    dicCheckIMEI[lstClear[i]["IMEI_Out"].ToString()].Add(i);
                                }
                                else
                                {
                                    List<int> lstIndex = new List<int>();
                                    lstIndex.Add(i);
                                    dicCheckIMEI.Add(lstClear[i]["IMEI_Out"].ToString(), lstIndex);
                                }
                            }

                            foreach (string strIMEI in dicCheckIMEI.Keys)
                            {
                                if (dicCheckIMEI[strIMEI].Count() > 1)
                                {
                                    for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                                    {
                                        lstClear[dicCheckIMEI[strIMEI][i]]["ISERROR"] = true;
                                        lstClear[dicCheckIMEI[strIMEI][i]]["NOTE"] = lstClear[dicCheckIMEI[strIMEI][i]]["NOTE"].ToString().Replace("Trùng IMEI trên file Excel", "") + "Trùng IMEI trên file Excel";
                                    }
                                }
                            }

                            var lstTrim = dtbExcel.Select("ISERROR <> 1  or ISERROR is null");

                            lstTrim.All(c => { c["IMEI_Out"] = c["IMEI_Out"].ToString().Trim(); c["ChangeNote"] = c["ChangeNote"].ToString().Trim(); return true; });
                            dtbExcel.AcceptChanges();
                            //var duplicates = lstTrim.GroupBy(i => new { IMEI_Out = i.Field<string>("IMEI_Out"), IMEI_IN = i.Field<string>("IMEI_IN") }).Where(g => g.Count() > 1).Select(g => new { g.Key.IMEI_Out, g.Key.IMEI_IN }).ToList();

                            // Check Imei xuất trong DB
                            DataTable dtDataOut = new DataTable();
                            dtDataOut.Columns.Add("IMEI");
                            foreach (DataRow item in lstTrim)
                            {
                                dtDataOut.Rows.Add(item["IMEI_Out"]);
                            }
                            DataTable dtbTotalImei = null;
                            DataTable dtbTotalImeiNotInStock = null;
                            DataTable dtbTotalImeiUse = null;
                            DataSet dsData = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("CHECKIMEI_INSTOCK_NOTIN_BORROW", new string[] { "v_Out1", "v_Out2", "v_Out3" }, new object[] { "@StoreID", _outputStoreID, "@IMEI", DUIInventoryCommon.CreateXMLFromDataTable(dtDataOut, "IMEI") });
                            if (dsData != null && dsData.Tables.Count > 0)
                            {
                                dtbTotalImei = dsData.Tables[0].Copy();
                            }
                            if (dsData != null && dsData.Tables.Count > 1)
                            {
                                dtbTotalImeiNotInStock = dsData.Tables[1].Copy();
                            }
                            if (dsData != null && dsData.Tables.Count > 2)
                            {
                                dtbTotalImeiUse = dsData.Tables[2].Copy();
                            }

                            List<string> lstIMEI = new List<string>();
                            lstIMEI = lstIMEI.Concat(dtbTotalImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList()).ToList();
                            var filteredRows = dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                .Where(row => !lstIMEI.Contains(row.Field<string>("IMEI_Out")));
                            if (filteredRows.Any())
                            {
                                filteredRows.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không tồn tại"; return true; });
                            }



                            var LstIMEIInProductStore = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                        join i in dtbTotalImei.Select("STOREID <> " + _outputStoreID)
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                        select o;
                            if (LstIMEIInProductStore.Any())
                            {
                                LstIMEIInProductStore.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không tồn trong kho xuất"; return true; });
                            }

                            var LstIMEINotRealInput = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                      join i in dtbTotalImei.Select("IsCheckRealInput = 0")
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                      select o;
                            if (LstIMEINotRealInput.Any())
                            {
                                LstIMEINotRealInput.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất này chưa được xác nhận nhập kho"; return true; });
                            }

                            var LstIMEIInProductOrder = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                        join i in dtbTotalImei.Select("ISORDER = 1")
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                        select o;
                            if (LstIMEIInProductOrder.Any())
                            {
                                LstIMEIInProductOrder.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI đã được đặt hàng"; return true; });
                            }

                            var LstIMEINotInStock = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                    join i in dtbTotalImeiNotInStock.AsEnumerable()
                                                    on o["IMEI_Out"] equals i["IMEI"]
                                                    select o;
                            if (LstIMEINotInStock.Any())
                            {
                                LstIMEINotInStock.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất đang cho mượn"; return true; });
                            }

                            var LstIMEIUse = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                             join i in dtbTotalImeiUse.AsEnumerable()
                                                    on o["IMEI_Out"] equals i["IMEI"]
                                             select o;
                            if (LstIMEIUse.Any())
                            {
                                LstIMEIUse.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý"; return true; });
                            }
                            var LstIMEIValidate = dtbExcel.Select("ISERROR <> 1  or ISERROR is null");
                            dtbTotalImei.PrimaryKey = new DataColumn[] { dtbTotalImei.Columns["IMEI"] };
                            foreach (DataRow item in LstIMEIValidate)
                            {
                                ProductChangeOrderDT result = new ProductChangeOrderDT();
                                result.IMEI_Out = item["IMEI_Out"].ToString();
                                result.ChangeNote = item["ChangeNote"].ToString();
                                result.Quantity = 1;
                                DataRow[] productOut = dtbTotalImei.Select("IMEI = '" + result.IMEI_Out + "'");
                                if (productOut.Length > 0)
                                {
                                    result.ProductID_Out = productOut[0]["ProductID"].ToString();
                                    result.ProductID_OutName = productOut[0]["ProductName"].ToString();
                                    result.IsNew_Out = Convert.ToBoolean(productOut[0]["IsNew"]);
                                    result.ProductID_IN = productOut[0]["ProductID"].ToString();
                                    result.ProductID_INName = productOut[0]["ProductName"].ToString();
                                    result.IsNew_IN = !Convert.ToBoolean(productOut[0]["IsNew"]);
                                    if (Convert.ToInt32(productOut[0]["InStockStatusID"]) > 1)
                                    {
                                        item["ISERROR"] = 1;
                                        item["NOTE"] = "Sản phẩm đang có trạng thái khác trạng thái sản phẩm cho phép Mới/Cũ!";
                                        dtbExcel.AcceptChanges();
                                        continue;
                                    }
                                    result.InStockStatusID_Out = Convert.ToInt32(productOut[0]["InStockStatusID"]);
                                    result.InStockStatusID_In = (Convert.ToBoolean(productOut[0]["IsNew"])) ? 0 : 1;
                                    result.IMEI_IN = result.IMEI_Out;
                                }
                                lstDetail.Add(result);
                            }
                            //dtbExcel.Columns[0].ColumnName = "IMEI_Out";
                            //dtbExcel.Columns[1].ColumnName = "ChangeNote";
                            //dtbExcel.Columns.Add("ISERROR", typeof(int));
                            //dtbExcel.Columns.Add("NOTE", typeof(string));
                            //foreach (DataRow item in dtbExcel.Rows)
                            //{
                            //    message = string.Empty;
                            //    ProductChangeOrderDT result = new ProductChangeOrderDT();
                            //    result.IMEI_Out = (item[0] ?? string.Empty).ToString().Trim();
                            //    result.ChangeNote = (item[1] ?? string.Empty).ToString().Trim();

                            //    if (!string.IsNullOrEmpty(result.IMEI_Out))
                            //    {
                            //        objProduct_By_IMEIOUT = _dataContext.objPLCProductInStock.LoadInfo(result.IMEI_Out, _outputStoreID);
                            //        isProductExist_By_IMEIOUT = objProduct_By_IMEIOUT != null;
                            //    }
                            //    else { message += "IMEI xuất không được bỏ trống, "; goto end; }


                            //    //Kiểm tra đã tồn tại
                            //    if (listReaded.Any(d =>
                            //        (!string.IsNullOrEmpty(GetString(d.IMEI_IN)) && GetString(result.IMEI_IN) == GetString(d.IMEI_IN))
                            //        || (!string.IsNullOrEmpty(GetString(d.IMEI_Out)) && GetString(result.IMEI_Out) == GetString(d.IMEI_Out))
                            //        || (!string.IsNullOrEmpty(GetString(d.IMEI_IN)) && GetString(result.IMEI_Out) == GetString(d.IMEI_IN))
                            //        || (!string.IsNullOrEmpty(GetString(d.IMEI_Out)) && GetString(result.IMEI_IN) == GetString(d.IMEI_Out))
                            //        || ((!string.IsNullOrEmpty(GetString(result.ProductID_Out)) && GetString(d.ProductID_Out) == GetString(result.ProductID_Out) &&
                            //                isProductExist_By_IDOut && !isProductOutRequireIMEI))
                            //        || ((!string.IsNullOrEmpty(GetString(result.IMEI_Out))) && (!string.IsNullOrEmpty(GetString(result.ProductID_Out)))
                            //            && (GetString(d.ProductID_Out) == GetString(result.ProductID_Out) && GetString(d.IMEI_Out) == GetString(result.IMEI_Out)))
                            //        || (!string.IsNullOrEmpty(GetString(result.ProductID_Out)) && string.IsNullOrEmpty(GetString(result.IMEI_Out))
                            //            && GetString(d.ProductID_Out) == GetString(result.ProductID_Out) && string.IsNullOrEmpty(GetString(d.IMEI_Out)))
                            //        ))
                            //    {
                            //        message += "Mã SP hoặc IMEI đã tồn tại trên lưới, ";
                            //        goto end; ;
                            //    }

                            //    if (result != null)
                            //        listReaded.Add(result);
                            //    else
                            //    {
                            //        message += "Không đọc được dữ liệu";
                            //        goto end; ;
                            //    }
                            //    ProductInStock objProductOut = null;

                            //    if (!isProductExist_By_IMEIOUT)
                            //    { message += "Không tìm thấy thông tin IMEI, "; goto end; }
                            //    else
                            //    {
                            //        objProductOut = objProduct_By_IMEIOUT;
                            //    }
                            //    if (objProductOut != null)
                            //    {
                            //        result.ProductID_Out = objProductOut.ProductID;
                            //        result.ProductID_IN = objProductOut.ProductID;
                            //        result.ProductID_OutName = objProductOut.ProductName;
                            //        result.ProductID_INName = objProductOut.ProductName;
                            //        result.IsNew_IN = !objProductOut.IsNew;
                            //        result.IsNew_Out = objProductOut.IsNew;
                            //        result.IMEI_IN = result.IMEI_Out;
                            //        result.Quantity = 1;

                            //        if (!string.IsNullOrEmpty(result.IMEI_Out) && _dataContext.CheckImeiUsed(result.IMEI_Out))
                            //        { message += "IMEI đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý, "; goto end; }

                            //        if (!objProductOut.IsCheckRealInput)
                            //        {
                            //            if (!objProductOut.IsRequestIMEI)
                            //            {
                            //                if (objProductOut.Quantity == 0)
                            //                { message += "SP này chưa được xác nhận nhập kho, "; goto end; }
                            //                }
                            //            else
                            //            {
                            //                message += "IMEI này chưa được xác nhận nhập kho, "; goto end;
                            //            }
                            //        }

                            //        DataRow[] rOutputInStock = null;
                            //        _dataContext.dtbStoreInStockNotIMEI.Select("StoreID = " + _outputStoreID + " AND TRIM(ProductID) = '" + objProductOut.ProductID + "' AND Quantity > 0");

                            //        if (rOutputInStock != null && !rOutputInStock.Any())
                            //        {
                            //            message += "IMEI " + result.IMEI_Out + " không tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác, "; goto end;
                            //        }
                            //        //if (lstDetail != null && lstDetail.Any(d => d.IMEI_Out == IMEI_OUT))
                            //        //{ message += "IMEI " + IMEI_OUT + " xuất đổi đã được nhập. Vui lòng nhập mã sản phẩm khác, "; }


                            //        decimal decOut = 0;
                            //        objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, _outputStoreID, objProductOut.ProductID, objProductOut.IMEI);
                            //        if (!string.IsNullOrEmpty(objProductOut.IMEI))
                            //        {
                            //            if (decOut > 0)
                            //            { message += "IMEI [" + objProductOut.IMEI + "] đang cho mượn, không được thao tác trên IMEI này, "; goto end; }
                            //        }
                            //        else
                            //        {
                            //            if (decOut > 0)
                            //                objProductOut.Quantity -= decOut;

                            //            if (objProductOut.Quantity <= 0)
                            //            { message += "Sản phẩm " + objProductOut.ProductID + " - " + objProductOut.ProductName + " không tồn kho, "; goto end; }
                            //        }
                            //    }
                            //    end:
                            //    if (string.IsNullOrEmpty(message) && result != null)
                            //        lstDetail.Add(result);
                            //    item[2] = !string.IsNullOrEmpty(message);
                            //    item[3] = message;
                            //}
                            #endregion
                       


                            break;
                        }
                    case OptionType.DOITRANGTHAI_SANPHAM:
                        {

                            dtbExcel.Columns[0].ColumnName = "IMEI_Out";
                            dtbExcel.Columns[1].ColumnName = "ChangeNote";
                            dtbExcel.Columns.Add("ISERROR", typeof(int));
                            dtbExcel.Columns.Add("NOTE", typeof(string));

                            if (bolIsChangeStatus && (lstInStockStatus_By_Type == null || lstInStockStatus_By_Type.Count == 0))
                            {
                                var lstALL = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()));
                                if (lstALL.Any() && lstALL.Count() > 0)
                                {
                                    lstALL.All(c => { c["ISERROR"] = true; c["NOTE"] = "Loại yêu cầu xuất đổi trạng thái không được khai báo trạng thái nguồn - trạng thái đích."; return true; });
                                }
                            }

                            var lstNote = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["ChangeNote"].ToString()));
                            if (lstNote.Any() && lstNote.Count() > 0)
                            {
                                lstNote.All(c => { c["ISERROR"] = true; c["NOTE"] = "Ghi chú không được bỏ trống "; return true; });
                            }

                            var lstNoteLength = dtbExcel.AsEnumerable().Where(x => x["ChangeNote"].ToString().Trim().Length > 1000);
                            if (lstNoteLength.Any() && lstNoteLength.Count() > 0)
                            {
                                lstNoteLength.All(c => { c["ISERROR"] = true; c["NOTE"] = "Ghi chú vượt quá độ dài cho phép 1000 "; return true; });
                            }
                            var lstIMEI_Out = dtbExcel.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()));
                            if (lstIMEI_Out.Any() && lstIMEI_Out.Count() > 0)
                            {
                                lstIMEI_Out.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không được bỏ trống "; return true; });
                            }

                            var lstIMEI_OutFormat = dtbExcel.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["IMEI_Out"].ToString()) && !CheckObject.CheckIMEI(x["IMEI_Out"].ToString()));
                            if (lstIMEI_OutFormat.Any() && lstIMEI_OutFormat.Count() > 0)
                            {
                                lstIMEI_OutFormat.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất sai định dạng "; return true; });
                            }

                            var lstClear = dtbExcel.Select("ISERROR <> 1 or ISERROR is null");



                            // Check trùng Imei trên lưới imei xuất
                            Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
                            for (int i = 0; i < lstClear.Count(); i++)
                            {
                                if (dicCheckIMEI.ContainsKey(lstClear[i]["IMEI_Out"].ToString()))
                                {
                                    dicCheckIMEI[lstClear[i]["IMEI_Out"].ToString()].Add(i);
                                }
                                else
                                {
                                    List<int> lstIndex = new List<int>();
                                    lstIndex.Add(i);
                                    dicCheckIMEI.Add(lstClear[i]["IMEI_Out"].ToString(), lstIndex);
                                }
                            }

                            foreach (string strIMEI in dicCheckIMEI.Keys)
                            {
                                if (dicCheckIMEI[strIMEI].Count() > 1)
                                {
                                    for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                                    {
                                        lstClear[dicCheckIMEI[strIMEI][i]]["ISERROR"] = true;
                                        lstClear[dicCheckIMEI[strIMEI][i]]["NOTE"] = lstClear[dicCheckIMEI[strIMEI][i]]["NOTE"].ToString().Replace("Trùng IMEI trên file Excel", "") + "Trùng IMEI trên file Excel";
                                    }
                                }
                            }

                            var lstTrim = dtbExcel.Select("ISERROR <> 1  or ISERROR is null");

                            lstTrim.All(c => { c["IMEI_Out"] = c["IMEI_Out"].ToString().Trim(); c["ChangeNote"] = c["ChangeNote"].ToString().Trim(); return true; });
                            dtbExcel.AcceptChanges();
                            //var duplicates = lstTrim.GroupBy(i => new { IMEI_Out = i.Field<string>("IMEI_Out"), IMEI_IN = i.Field<string>("IMEI_IN") }).Where(g => g.Count() > 1).Select(g => new { g.Key.IMEI_Out, g.Key.IMEI_IN }).ToList();

                            // Check Imei xuất trong DB
                            DataTable dtDataOut = new DataTable();
                            dtDataOut.Columns.Add("IMEI");
                            foreach (DataRow item in lstTrim)
                            {
                                dtDataOut.Rows.Add(item["IMEI_Out"]);
                            }
                            DataTable dtbTotalImei = null;
                            DataTable dtbTotalImeiNotInStock = null;
                            DataTable dtbTotalImeiUse = null;
                            DataSet dsData = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("CHECKIMEI_INSTOCK_NOTIN_BORROW", new string[] { "v_Out1", "v_Out2", "v_Out3" }, new object[] { "@StoreID", _outputStoreID, "@IMEI", DUIInventoryCommon.CreateXMLFromDataTable(dtDataOut, "IMEI") });
                            if (dsData != null && dsData.Tables.Count > 0)
                            {
                                dtbTotalImei = dsData.Tables[0].Copy();
                            }
                            if (dsData != null && dsData.Tables.Count > 1)
                            {
                                dtbTotalImeiNotInStock = dsData.Tables[1].Copy();
                            }
                            if (dsData != null && dsData.Tables.Count > 2)
                            {
                                dtbTotalImeiUse = dsData.Tables[2].Copy();
                            }

                            List<string> lstIMEI = new List<string>();
                            lstIMEI = lstIMEI.Concat(dtbTotalImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList()).ToList();
                            var filteredRows = dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                .Where(row => !lstIMEI.Contains(row.Field<string>("IMEI_Out")));
                            if (filteredRows.Any())
                            {
                                filteredRows.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không tồn tại"; return true; });
                            }


                            var LstIMEIInProductStore = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                        join i in dtbTotalImei.Select("STOREID <> " + _outputStoreID)
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                        select o;
                            if (LstIMEIInProductStore.Any())
                            {
                                LstIMEIInProductStore.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất không tồn trong kho xuất"; return true; });
                            }

                            var LstIMEINotRealInput = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                      join i in dtbTotalImei.Select("IsCheckRealInput = 0")
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                      select o;
                            if (LstIMEINotRealInput.Any())
                            {
                                LstIMEINotRealInput.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất này chưa được xác nhận nhập kho"; return true; });
                            }

                            var LstIMEIInProductOrder = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                        join i in dtbTotalImei.Select("ISORDER = 1")
                                                        on o["IMEI_Out"] equals i["IMEI"]
                                                        select o;
                            if (LstIMEIInProductOrder.Any())
                            {
                                LstIMEIInProductOrder.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI đã được đặt hàng"; return true; });
                            }

                            var LstIMEINotInStock = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                                    join i in dtbTotalImeiNotInStock.AsEnumerable()
                                                    on o["IMEI_Out"] equals i["IMEI"]
                                                    select o;
                            if (LstIMEINotInStock.Any())
                            {
                                LstIMEINotInStock.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất đang cho mượn"; return true; });
                            }

                            var LstIMEIUse = from o in dtbExcel.Select("ISERROR <> 1  or ISERROR is null")
                                             join i in dtbTotalImeiUse.AsEnumerable()
                                                    on o["IMEI_Out"] equals i["IMEI"]
                                             select o;
                            if (LstIMEIUse.Any())
                            {
                                LstIMEIUse.All(c => { c["ISERROR"] = true; c["NOTE"] = "IMEI xuất đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý"; return true; });
                            }
                            var LstIMEIValidate = dtbExcel.Select("ISERROR <> 1  or ISERROR is null");
                            dtbTotalImei.PrimaryKey = new DataColumn[] { dtbTotalImei.Columns["IMEI"] };
                            foreach (DataRow item in LstIMEIValidate)
                            {
                                ProductChangeOrderDT result = new ProductChangeOrderDT();
                                result.IMEI_Out = item["IMEI_Out"].ToString();
                                result.ChangeNote = item["ChangeNote"].ToString();
                                result.Quantity = 1;
                                DataRow[] productOut = dtbTotalImei.Select("IMEI = '" + result.IMEI_Out + "'");
                                if (productOut.Length > 0)
                                {
                                    result.ProductID_Out = productOut[0]["ProductID"].ToString();
                                    result.ProductID_OutName = productOut[0]["ProductName"].ToString();
                                    result.IsNew_Out = Convert.ToBoolean(productOut[0]["IsNew"]);
                                    result.ProductID_IN = productOut[0]["ProductID"].ToString();
                                    result.ProductID_INName = productOut[0]["ProductName"].ToString();
                                    result.IsNew_IN = Convert.ToBoolean(productOut[0]["IsNew"]);
                                    int intStatusFrom = Convert.ToInt32(productOut[0]["InStockStatusID"]);
                                    int intStatusTo = Convert.ToInt32(productOut[0]["InStockStatusID"]);
                                    if (bolIsChangeStatus)
                                    {
                                        try
                                        {
                                            var varStatusTo = lstInStockStatus_By_Type.First(o => o.ProductStatusIDFrom == intStatusFrom);
                                            if (varStatusTo != null && varStatusTo.IsSelect)
                                                intStatusTo = varStatusTo.ProductStatusIDTo;
                                            else
                                            {
                                                item["ISERROR"] = 1;
                                                item["NOTE"] = "IMEI có trang thái không được khai báo ở trạng thái nguồn";
                                                continue;
                                            }
                                        }
                                        catch
                                        {
                                            item["ISERROR"] = 1;
                                            item["NOTE"] = "IMEI có trang thái không được khai báo ở trạng thái nguồn";
                                            continue;
                                        }
                                    }
                                    result.InStockStatusID_Out = intStatusFrom;
                                    result.InStockStatusID_In = intStatusTo;
                                    result.IMEI_IN = result.IMEI_Out;
                                }
                                lstDetail.Add(result);
                            }
                            dtbExcel.AcceptChanges();
                            break;
                        }
                    default: { break; }
                }
            }
            objClsImportExcel.SetFinalGridData2(dtbExcel);
        }

        PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new PLC.BorrowProduct.PLCBorrowProduct();

        private ProductInStock objProduct_By_IMEIOUT = null;
        private ProductInStock objProduct_By_IMEIIN = null;
        private MasterData.PLC.MD.WSProduct.Product objProduct_By_IDOut = null;
        private MasterData.PLC.MD.WSProduct.Product objProduct_By_IDIN = null;
        private bool isProductExist_By_IDOut = false;
        private bool isProductExist_By_IDIN = false;
        private bool isProductExist_By_IMEIOUT = false;
        private bool isProductExist_By_IMEIIN = false;
        private bool isProductOutRequireIMEI = false;
        private bool isIMEIBelongToProduct = false;
        private bool CheckOnBusinessRule(ref ProductChangeOrderDT obj, ref string message)
        {
            bool result = false;
            string IMEI_OUT = string.Empty;
            string IMEI_IN = string.Empty;
            string PRODUCTID_OUT = string.Empty;
            string PRODUCTID_IN = string.Empty;


            if (obj != null)
            {
                IMEI_OUT = obj.IMEI_Out; IMEI_IN = obj.IMEI_IN; PRODUCTID_OUT = obj.ProductID_Out; PRODUCTID_IN = obj.ProductID_IN;
            }
            else return false;

            try
            {
                switch (_productChangeType)
                {
                    case OptionType.DOISANGSANPHAMKHAC:
                        {
                            ProductInStock objProductOut = null;
                            ProductInStock objProductIN = null;
                            bool requireCheckIMEIUsed = true;

                            #region Xử lý xuất
                            //nếu product id xuất và imei xuất đều được nhập thì load product info theo mã imei xuất
                            if (!string.IsNullOrEmpty(PRODUCTID_OUT) && !string.IsNullOrEmpty(IMEI_OUT))
                            {
                                if (!isProductExist_By_IDOut)
                                    message += "Không tìm thấy mã sản phẩm xuất " + PRODUCTID_OUT + ", ";
                                if (!isProductExist_By_IMEIOUT)
                                    message += "Không tìm thấy IMEI " + IMEI_OUT + ", ";

                                if (isIMEIBelongToProduct)
                                    objProductOut = objProduct_By_IMEIOUT;
                                else
                                {
                                    message += "IMEI " + IMEI_OUT + " không thuộc về mã sản phẩm " + PRODUCTID_OUT + ", ";
                                    requireCheckIMEIUsed = false;
                                    //Nếu IMEI không thuộc về mã sản phẩm thì load theo mã sản phẩm
                                    if (isProductExist_By_IDOut)
                                        objProductOut = _dataContext.objPLCProductInStock.LoadInfo(PRODUCTID_OUT, _outputStoreID);
                                }
                            }
                            //else, nếu imei out được nhập thì load theo imei out
                            else if (!string.IsNullOrEmpty(IMEI_OUT))
                            {
                                if (isProductExist_By_IMEIOUT)
                                    objProductOut = objProduct_By_IMEIOUT;
                                else message += "Không tìm thấy thông tin IMEI " + IMEI_OUT + ", ";
                            }
                            //else, nếu product id out được nhập thì load theo product id out 
                            else if (!string.IsNullOrEmpty(PRODUCTID_OUT))
                            {
                                if (isProductExist_By_IDOut)
                                {
                                    objProductOut = _dataContext.objPLCProductInStock.LoadInfo(PRODUCTID_OUT, _outputStoreID);
                                    if (isProductOutRequireIMEI)
                                        message += "Mã SP xuất có yêu cầu IMEI, ";
                                }
                                else message += "Không tìm thấy thông tin mã SP " + PRODUCTID_OUT + ", ";
                            }

                            //nếu tìm thấy thông tin sản phẩm thì fill thông tin vào obj
                            if (objProductOut != null)
                            {
                                obj.ProductID_OutName = objProductOut.ProductName;
                                obj.ProductID_Out = objProductOut.ProductID;
                                obj.IMEI_IN = obj.IMEI_Out;
                                PRODUCTID_OUT = objProductOut.ProductID;

                                if (objProductOut.IMEI != string.Empty)
                                {
                                    obj.IsNew_Out = objProductOut.IsNew;
                                    obj.IsNew_IN = objProductOut.IsNew;
                                    obj.Quantity = 1;
                                }
                                else
                                {
                                    DataRow[] rOutputInStock = _dataContext.dtbStoreInStockNotIMEI.Select("StoreID = " + _outputStoreID + " AND TRIM(ProductID) = '" + obj.ProductID_Out + "' AND Quantity > 0");
                                    if (rOutputInStock != null && rOutputInStock.Any())
                                    {
                                        obj.IsNew_Out = rOutputInStock[0]["ISNEW"].ToString() == "1" ? true : false;
                                        obj.IsNew_IN = rOutputInStock[0]["ISNEW"].ToString() == "1" ? true : false;
                                    }
                                }

                                if (requireCheckIMEIUsed && !string.IsNullOrEmpty(objProductOut.IMEI) && _dataContext.CheckImeiUsed(objProductOut.IMEI, _outputStoreID))
                                    message += "IMEI xuất đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý, ";
                                else
                                {
                                    decimal decOut = 0;
                                    objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, _outputStoreID, objProductOut.ProductID, IMEI_OUT);
                                    if (!objProductOut.IsRequestIMEI)
                                    {
                                        if (objProductOut.Quantity <= 0)
                                            message += "Mã SP " + objProductOut.ProductID + " chưa được xác nhận nhập kho, ";
                                        else if (string.IsNullOrEmpty(IMEI_OUT))
                                        {
                                            if (decOut > 0) { objProductOut.Quantity = objProductOut.Quantity - decOut; }
                                            if (objProductOut.Quantity <= 0)
                                            { message += "Sản phẩm " + objProductOut.ProductID + " - " + objProductOut.ProductName + " không tồn kho!"; }
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(IMEI_OUT))
                                            if (decOut > 0) { message += "IMEI [" + objProductOut.IMEI + "] đang cho mượn, không được thao tác trên IMEI này, "; }

                                        if (!string.IsNullOrEmpty(IMEI_OUT) && objProductOut.Quantity <= 0)
                                        {
                                            message += "Hiện tại mã sản phẩm " + objProductOut.ProductID + " không có tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác, ";
                                        }
                                        else if (!string.IsNullOrEmpty(IMEI_OUT))
                                        {
                                            if (isProductExist_By_IMEIOUT && !objProduct_By_IMEIOUT.IsCheckRealInput)
                                                message += "IMEI này chưa được xác nhận nhập kho, ";
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region Xử lý nhập
                            if (!isProductExist_By_IDIN)
                            {
                                message += "Mã SP nhập " + PRODUCTID_IN + " không tồn tại, ";
                            }
                            else
                            {
                                if (PRODUCTID_OUT == PRODUCTID_IN)
                                    message += "Mã SP nhập phải khác mã SP xuất đổi, ";
                                else
                                {
                                    objProductIN = _dataContext.objPLCProductInStock.LoadInfo(PRODUCTID_IN, _outputStoreID);
                                }
                            }

                            if (objProductIN != null)
                            {
                                obj.ProductID_INName = objProductIN.ProductName;
                                obj.IMEI_IN = obj.IMEI_Out;
                            }

                            //Kiểm tra cùng loại
                            if (objProductIN != null && objProductOut != null)
                            {
                                bool isRequireImei = objProductOut.IsRequestIMEI;
                                if (objProductIN.IsRequestIMEI != objProductOut.IsRequestIMEI)
                                {
                                    if (isRequireImei) message += "Mã SP nhập phải cùng loại có yêu cầu IMEI như SP xuất đổi, ";
                                    else message += "Mã SP nhập phải cùng loại không yêu cầu IMEI như SP xuất đổi,  ";
                                }
                                if (objProductOut.Quantity < obj.Quantity && objProductOut.Quantity != 0)
                                {
                                    message += "Số lượng của sản phẩm có mã \"" + obj.ProductID_Out + "\" phải nhỏ hơn hoặc bằng " + objProductOut.Quantity + "!";
                                }
                            }
                            #endregion
                            break;
                        }
                    case OptionType.DOISANGIMEIKHAC:
                        {
                            #region Xuất
                            ProductInStock objProductOut = null;
                            if (!isProductExist_By_IMEIOUT)
                                message += "Không tìm thấy thông tin IMEI " + IMEI_OUT + ", ";
                            else
                            {
                                objProductOut = objProduct_By_IMEIOUT;
                            }

                            if (objProductOut != null)
                            {
                                obj.ProductID_Out = objProductOut.ProductID;
                                obj.ProductID_IN = objProductOut.ProductID;
                                obj.ProductID_OutName = objProductOut.ProductName;
                                obj.ProductID_INName = objProductOut.ProductName;
                                obj.IsNew_Out = objProductOut.IsNew;
                                obj.IsNew_IN = objProductOut.IsNew;
                                obj.Quantity = 1;

                                if (!objProductOut.IsCheckRealInput)
                                {
                                    if (!objProductOut.IsRequestIMEI)
                                    {
                                        if (objProductOut.Quantity == 0)
                                            message += "SP này chưa được xác nhận nhập kho, ";
                                    }
                                    else
                                    {
                                        message += "IMEI này chưa được xác nhận nhập kho, ";
                                    }
                                }

                                if (objProductOut.Quantity <= 0)
                                    message += "IMEI " + objProductOut.IMEI + " không có tồn kho trên hệ thống, ";
                                if (!string.IsNullOrEmpty(IMEI_OUT) && _dataContext.CheckImeiUsed(IMEI_OUT, _outputStoreID))
                                { message += "IMEI đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý, "; }
                                else
                                {
                                    decimal decOut = 0;
                                    objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, _outputStoreID, objProductOut.ProductID, IMEI_OUT);
                                    if (!string.IsNullOrEmpty(IMEI_OUT))
                                        if (decOut > 0) { message += "IMEI [" + IMEI_OUT + "] đang cho mượn, không được thao tác trên IMEI này, "; }
                                }
                            }
                            #endregion

                            #region Nhập
                            if (!string.IsNullOrEmpty(IMEI_IN))
                            {
                                if (isProductExist_By_IMEIIN)
                                {
                                    message += "IMEI " + IMEI_IN + " đã tồn tại trong hệ thống, ";
                                }
                            }
                            #endregion
                            break;
                        }
                    case OptionType.DOITRANGTHAIIMEI:
                        {
                            ProductInStock objProductOut = null;

                            if (!isProductExist_By_IMEIOUT)
                                message += "Không tìm thấy thông tin IMEI, ";
                            else
                            {
                                objProductOut = objProduct_By_IMEIOUT;
                            }
                            if (objProductOut != null)
                            {
                                obj.ProductID_Out = objProductOut.ProductID;
                                obj.ProductID_IN = objProductOut.ProductID;
                                obj.ProductID_OutName = objProductOut.ProductName;
                                obj.ProductID_INName = objProductOut.ProductName;
                                obj.IsNew_IN = !objProductOut.IsNew;
                                obj.IsNew_Out = objProductOut.IsNew;
                                obj.IMEI_IN = obj.IMEI_Out;
                                obj.Quantity = 1;

                                if (!string.IsNullOrEmpty(IMEI_OUT) && _dataContext.CheckImeiUsed(IMEI_OUT, _outputStoreID))
                                { message += "IMEI đã tồn tại trong phiếu yêu cầu xuất đổi đang xử lý, "; }

                                if (!objProductOut.IsCheckRealInput)
                                {
                                    if (!objProductOut.IsRequestIMEI)
                                    {
                                        if (objProductOut.Quantity == 0)
                                            message += "SP này chưa được xác nhận nhập kho, ";
                                    }
                                    else
                                    {
                                        message += "IMEI này chưa được xác nhận nhập kho, ";
                                    }
                                }

                                DataRow[] rOutputInStock = null;
                                _dataContext.dtbStoreInStockNotIMEI.Select("StoreID = " + _outputStoreID + " AND TRIM(ProductID) = '" + objProductOut.ProductID + "' AND Quantity > 0");

                                if (rOutputInStock != null && !rOutputInStock.Any())
                                {
                                    message += "IMEI " + IMEI_OUT + " không tồn kho hệ thống. Vui lòng nhập mã sản phẩm khác, ";
                                }
                                //if (lstDetail != null && lstDetail.Any(d => d.IMEI_Out == IMEI_OUT))
                                //{ message += "IMEI " + IMEI_OUT + " xuất đổi đã được nhập. Vui lòng nhập mã sản phẩm khác, "; }


                                decimal decOut = 0;
                                objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, _outputStoreID, objProductOut.ProductID, objProductOut.IMEI);
                                if (!string.IsNullOrEmpty(objProductOut.IMEI))
                                {
                                    if (decOut > 0)
                                    { message += "IMEI [" + objProductOut.IMEI + "] đang cho mượn, không được thao tác trên IMEI này, "; }
                                }
                                else
                                {
                                    if (decOut > 0)
                                        objProductOut.Quantity -= decOut;

                                    if (objProductOut.Quantity <= 0)
                                    { message += "Sản phẩm " + objProductOut.ProductID + " - " + objProductOut.ProductName + " không tồn kho, "; }
                                }
                            }
                            break;
                        }
                }
            }
            catch (Exception exception)
            {
                message += "Lỗi trong quá trình kiểm tra dữ liệu, ";
                FileLogger.LogAction(exception);
            }

            if (string.IsNullOrEmpty(message) && obj != null)
                lstDetail.Add(obj);
            return result;

        }
        private string GetString(string input)
        {
            if (input == null) return string.Empty;
            return input.ToString().Trim();
        }
        private bool IsStringNumber(string number)
        {
            try { Convert.ToInt32(number); return true; }
            catch { return false; }
        }
    }

    public enum OptionType
    {
        DOISANGSANPHAMKHAC = 1,
        DOISANGIMEIKHAC = 2,
        DOITRANGTHAIIMEI = 3,
        DOITRANGTHAI_SANPHAM = 4,
    }
}