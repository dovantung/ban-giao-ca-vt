﻿namespace ERP.Inventory.DUI.BGT
{
    partial class frmBeginTermMoneyCal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.chkCalAll = new System.Windows.Forms.CheckBox();
            this.lblMonth = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.dtpBeginDate = new System.Windows.Forms.DateTimePicker();
            this.cboStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label1 = new System.Windows.Forms.Label();
            this.grpInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpInfo
            // 
            this.grpInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpInfo.Controls.Add(this.cboStoreID);
            this.grpInfo.Controls.Add(this.chkCalAll);
            this.grpInfo.Controls.Add(this.label1);
            this.grpInfo.Controls.Add(this.lblMonth);
            this.grpInfo.Controls.Add(this.btnCalculate);
            this.grpInfo.Controls.Add(this.dtpBeginDate);
            this.grpInfo.Location = new System.Drawing.Point(7, -1);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Size = new System.Drawing.Size(389, 101);
            this.grpInfo.TabIndex = 1;
            this.grpInfo.TabStop = false;
            // 
            // chkCalAll
            // 
            this.chkCalAll.AutoSize = true;
            this.chkCalAll.Location = new System.Drawing.Point(57, 76);
            this.chkCalAll.Name = "chkCalAll";
            this.chkCalAll.Size = new System.Drawing.Size(148, 20);
            this.chkCalAll.TabIndex = 3;
            this.chkCalAll.Text = "Tính tới ngày hiện tại";
            this.chkCalAll.UseVisualStyleBackColor = true;
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.Location = new System.Drawing.Point(7, 49);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(44, 16);
            this.lblMonth.TabIndex = 0;
            this.lblMonth.Text = "Ngày:";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Image = global::ERP.Inventory.DUI.Properties.Resources.cal;
            this.btnCalculate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCalculate.Location = new System.Drawing.Point(162, 44);
            this.btnCalculate.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(178, 25);
            this.btnCalculate.TabIndex = 2;
            this.btnCalculate.Text = "    Tính số dư tiền đầu ngày";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // dtpBeginDate
            // 
            this.dtpBeginDate.CustomFormat = "dd/MM/yyyy";
            this.dtpBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginDate.Location = new System.Drawing.Point(57, 46);
            this.dtpBeginDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpBeginDate.Name = "dtpBeginDate";
            this.dtpBeginDate.ShowUpDown = true;
            this.dtpBeginDate.Size = new System.Drawing.Size(101, 22);
            this.dtpBeginDate.TabIndex = 1;
            // 
            // cboStoreID
            // 
            this.cboStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreID.Location = new System.Drawing.Point(57, 18);
            this.cboStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreID.Name = "cboStoreID";
            this.cboStoreID.Size = new System.Drawing.Size(283, 24);
            this.cboStoreID.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kho:";
            // 
            // frmBeginTermMoneyCal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 107);
            this.Controls.Add(this.grpInfo);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmBeginTermMoneyCal";
            this.ShowIcon = false;
            this.Text = "Tính số dư tiền đầu ngày";
            this.Load += new System.EventHandler(this.frmBeginTermMoneyCal_Load);
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpInfo;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.DateTimePicker dtpBeginDate;
        private System.Windows.Forms.CheckBox chkCalAll;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreID;
        private System.Windows.Forms.Label label1;
    }
}