﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System.Threading;

namespace ERP.Inventory.DUI.BGT
{
    public partial class frmSearchBeginTermMoney : Form
    {
        /// <summary>
        /// Created by : Cao Hữu Vũ Lam
        /// Date : 15/05/2013
        /// Xem lịch sử tính só dư tiền đầu ngày
        /// </summary>

        #region Variable

        private string strPermission_Search = "BGT_BeginTermMoney_Search";
        private string strPermission_ExportExcel = "BGT_BeginTermMoney_ExportExcel";
        private bool bolPermissionExportExcel = false;
        
        private bool bolIsLoadComplete = false;
        private DataTable dtbResource = null;
        private string strAccountIDList = string.Empty;
        #endregion

        #region Constructor

        public frmSearchBeginTermMoney()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
        }

        #endregion

        #region Method

        //private void LoadCombobox()
        //{
        //    try
        //    {
        //        Library.AppCore.LoadControls.SetDataSource.SetMainGroup(this, cboMainGroup);
        //        Library.AppCore.LoadControls.SetDataSource.SetCompany(this, cboCompany);
        //    }
        //    catch (Exception ex)
        //    {
        //        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nạp combobox", ex.ToString());
        //    }
        //}
   
        private bool CheckInput()
        {
            if (!ERP.MasterData.DUI.Common.CommonFunction.EndDateValidation(dtpFromDate.Value, dtpToDate.Value, false))
            {
                dtpToDate.Focus();
                MessageBox.Show(this, "Đến ngày không thể nhỏ hơn từ ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
        
        #endregion

        #region Event

        private void frmSearchDetailExpense_Load(object sender, EventArgs e)
        {
            btnSearch.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Search);
            bolPermissionExportExcel = SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel);
            //LoadCombobox();
            FormState = FormStateType.LIST;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            pnSearchStatus.Visible = true;
            pnSearchStatus.Refresh();
            EnableControl(false);
            //SearchData();
            //return;
            Thread objThread = new Thread(new ThreadStart(SearchData));
            objThread.Start();
            
        }

        private void SearchData()
        {
            string strAccountListSeacrch = string.Empty;
            object[] objKeywords = new object[]
            {
                "@FROMDATE", dtpFromDate.Value,
                "@TODATE", dtpToDate.Value
            };
            dtbResource = new PLC.BGT.PLCBeginTermMoney().SearchDataMoneyInfo(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            bolIsLoadComplete = true;
        }

        private DataTable SearchDataDetail()
        {
            DataTable dtbResourceDetail = null;
            object objDate = grdViewData.GetRowCellValue(grdViewData.FocusedRowHandle, grdViewData.Columns["BEGINTERMMONEYDATE"]);
            object strUpdatedUser = grdViewData.GetRowCellValue(grdViewData.FocusedRowHandle, grdViewData.Columns["UPDATEDUSER"]);
            string strAccountListSeacrch = string.Empty;
            object[] objKeywords = new object[]
            {
                 "@BeginTermMoneyDate", objDate
            };
            dtbResourceDetail = new PLC.BGT.PLCBeginTermMoney().SearchDataMoneyInfoLog(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            grdDetail.DataSource = dtbResourceDetail;
            return dtbResourceDetail;
        }

        private void cmd_Grid_Excel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);

        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void tmrFormatFlex_Tick(object sender, EventArgs e)
        {
            if (bolIsLoadComplete)
            {
                bolIsLoadComplete= false;
                grdData.DataSource = dtbResource;
                btnExcel.Enabled = (bolPermissionExportExcel && dtbResource != null && dtbResource.Rows.Count > 0);
                pnSearchStatus.Visible = false;
                EnableControl(true);
            }            
        }

        private void EnableControl(bool bolIsEnable)
        {
            gpSearch.Enabled = bolIsEnable;
        }

        private void grdViewData_DoubleClick(object sender, EventArgs e)
        {
            DataTable dtbResourceDetail = SearchDataDetail();
            if (dtbResourceDetail!= null && dtbResourceDetail.Rows.Count > 0)
            {
                FormState = FormStateType.DETAIL;
            }
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            FormState = FormStateType.LIST;
        }

        #endregion

        #region Form Status
        /// <summary>
        /// Các trạng thái của Form
        /// </summary>
        enum FormStateType
        {
            LIST,
            DETAIL
        }

        FormStateType intFormState = FormStateType.LIST;
        FormStateType FormState
        {
            set
            {
                intFormState = value;
                if (intFormState == FormStateType.LIST)
                {
                    tabControl.SelectedTab = tabList;
                }
                else
                {
                    tabControl.SelectedTab = tabDetail;
                }
            }
            get
            {
                return intFormState;
            }
        }

        #endregion

        private void gpSearch_Enter(object sender, EventArgs e)
        {

        }

        private void tabDetail_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.LIST)
            {
                tabControl.SelectedTab = tabList;
            }
        }

        private void tabList_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.DETAIL)
            {
                tabControl.SelectedTab = tabDetail;
            }
        }




 
 

  


       

       



       



    }
}
