﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Threading;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.BGT
{
    /// <summary>
    /// Created by : Cao Hữu Vũ Lam
    /// Date : 25/06/2013
    /// Tính số dư tiền đầu ngày
    /// </summary>
    public partial class frmBeginTermMoneyCal : Form
    {
        #region Constructor

        public frmBeginTermMoneyCal()
        {
            InitializeComponent();
        }
        #endregion

        #region Method

        private void CalAll()
        {
            Report.PLC.PLCReportDataSource plcPLCReportDataSource = new Report.PLC.PLCReportDataSource();
            DataTable dtbCalResult = plcPLCReportDataSource.GetDataSource("BGT_BEGINTERMMONEY_CALC_BYSTORE", new object[] {
                "@STOREID", cboStoreID.StoreID,
                "@FROMDATE", dtpBeginDate.Value.Date,
                "@TODATE", chkCalAll.Checked?Globals.GetServerDateTime().Date : dtpBeginDate.Value.Date
            });
            if (dtbCalResult == null)
            {
                if (SystemConfig.objSessionUser.ResultMessageApp.Message.ToLower().Contains("time out"))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Tác vụ vượt quá thời gian chờ, vui lòng kiểm tra kết quả sau!");
                    return;
                }
                else
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi trong quá trình tính số dư tiền đầu ngày!");
                    return;
                }
            }
            else if (dtbCalResult.Rows.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi trong quá trình tính số dư tiền đầu ngày!");
                return;
            }
            else
            {
                if (Convert.ToInt32(dtbCalResult.Rows[0][0]) == 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Tính số dư tiền đầu ngày thất bại!");
                    return;
                }
                else
                {
                    MessageBoxObject.ShowInfoMessage(this, "Đã tính xong!");
                    grpInfo.Enabled = true;
                    return;
                }
            }
            //PLC.BGT.PLCBeginTermMoney pbjPLCBeginTermMoney = new PLC.BGT.PLCBeginTermMoney();
            //bool bolIsCal = pbjPLCBeginTermMoney.CalcAll(chkCalAll.Checked, dtpBeginDate.Value);
            //if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            //{
            //    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
            //        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            //}
            //Library.AppCore.LoadControls.MessageBoxObject.ShowInfoMessage(this, "Tính số dư tiền đầu ngày thành công!");
            //grpInfo.Enabled = true;
        }

        private bool CheckInput()
        {
            DateTime dtNow = Library.AppCore.Globals.GetServerDateTime();
            if (!ERP.MasterData.DUI.Common.CommonFunction.EndDateValidation(dtpBeginDate.Value, dtNow, false))
            {
                dtpBeginDate.Focus();
                MessageBox.Show(this, "Ngày tính không thể lớn hơn ngày hiện tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (dtpBeginDate.Value.Date <= new DateTime(2018, 11, 1))
            {
                dtpBeginDate.Focus();
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn thời gian tính lớn hơn ngày 01/11/2018!");
                return false;
            }
            if (cboStoreID.StoreID <= 0)
            {
                cboStoreID.Focus();
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho cần tính số dư!");
                return false;
            }
            return true;
        }

        #endregion

        #region Event

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            grpInfo.Enabled = false;
            //CalAll();
            //return;
            Thread objThread = new Thread(new ThreadStart(CalAll));
            objThread.Start();
        }
        #endregion

        private void frmBeginTermMoneyCal_Load(object sender, EventArgs e)
        {
            Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboStoreID, false);
            cboStoreID.InitControl(false, false);
            cboStoreID.SelectionChangeCommitted += CboStoreID_SelectionChangeCommitted;
            cboStoreID.SetValue(SystemConfig.intDefaultStoreID);
            CboStoreID_SelectionChangeCommitted(null, null);
        }

        private void CboStoreID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboStoreID.StoreID <= 0)
            {
                btnCalculate.Enabled = false;
                return;
            }
            Report.PLC.PLCReportDataSource plcPLCReportDataSource = new Report.PLC.PLCReportDataSource();
            DataTable dtb = plcPLCReportDataSource.GetDataSource("BGT_BEGINTERMMONEY_ISRUNNING", new object[] {
                "@STOREID", cboStoreID.StoreID
            });
            if (dtb == null)
                btnCalculate.Enabled = false;
            else
            {
                if (dtb.Rows.Count == 0)
                    btnCalculate.Enabled = false;
                else
                {
                    if (Convert.ToInt32(dtb.Rows[0]["ISRUNNING"]) == 0)
                        btnCalculate.Enabled = true;
                    else btnCalculate.Enabled = false;
                }
            }
        }
    }
}
