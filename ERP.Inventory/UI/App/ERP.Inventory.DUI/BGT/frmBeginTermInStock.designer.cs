﻿namespace ERP.Inventory.DUI.BGT
{
    partial class frmBeginTermInStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBeginTermInStock));
            this.flexData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.dtpBeginDate = new System.Windows.Forms.DateTimePicker();
            this.lblMonth = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.cachedrptInputVoucherReport1 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).BeginInit();
            this.grpInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // flexData
            // 
            this.flexData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexData.AllowEditing = false;
            this.flexData.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexData.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexData.Location = new System.Drawing.Point(5, 62);
            this.flexData.Margin = new System.Windows.Forms.Padding(4);
            this.flexData.Name = "flexData";
            this.flexData.Rows.Count = 1;
            this.flexData.Rows.DefaultSize = 21;
            this.flexData.Size = new System.Drawing.Size(960, 439);
            this.flexData.StyleInfo = resources.GetString("flexData.StyleInfo");
            this.flexData.TabIndex = 1;
            this.flexData.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            // 
            // dtpBeginDate
            // 
            this.dtpBeginDate.CustomFormat = "MM/yyyy";
            this.dtpBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginDate.Location = new System.Drawing.Point(62, 17);
            this.dtpBeginDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpBeginDate.Name = "dtpBeginDate";
            this.dtpBeginDate.ShowUpDown = true;
            this.dtpBeginDate.Size = new System.Drawing.Size(92, 22);
            this.dtpBeginDate.TabIndex = 1;
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.Location = new System.Drawing.Point(12, 20);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(50, 16);
            this.lblMonth.TabIndex = 0;
            this.lblMonth.Text = "Tháng:";
            // 
            // btnDelete
            // 
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(825, 133);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(105, 25);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "    Xóa tồn kho";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCalculate
            // 
            this.btnCalculate.Image = global::ERP.Inventory.DUI.Properties.Resources.calculator;
            this.btnCalculate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCalculate.Location = new System.Drawing.Point(178, 16);
            this.btnCalculate.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(106, 25);
            this.btnCalculate.TabIndex = 2;
            this.btnCalculate.Text = "    Tính tồn kho";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // grpInfo
            // 
            this.grpInfo.Controls.Add(this.lblMonth);
            this.grpInfo.Controls.Add(this.btnCalculate);
            this.grpInfo.Controls.Add(this.dtpBeginDate);
            this.grpInfo.Location = new System.Drawing.Point(5, 4);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Size = new System.Drawing.Size(290, 51);
            this.grpInfo.TabIndex = 0;
            this.grpInfo.TabStop = false;
            // 
            // frmBeginTermInStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 514);
            this.Controls.Add(this.grpInfo);
            this.Controls.Add(this.flexData);
            this.Controls.Add(this.btnDelete);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmBeginTermInStock";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lịch sử tính tồn kho đầu kỳ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBeginTermInStock_FormClosing);
            this.Load += new System.EventHandler(this.frmBeginTermInStock_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).EndInit();
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1FlexGrid.C1FlexGrid flexData;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.DateTimePicker dtpBeginDate;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox grpInfo;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport1;
    }
}