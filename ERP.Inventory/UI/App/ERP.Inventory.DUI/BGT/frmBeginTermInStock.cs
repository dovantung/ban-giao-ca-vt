﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Library.AppCore.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.Inventory.PLC.BGT;
using ERP.Inventory.PLC.BGT.WSBeginTermInStock;
namespace ERP.Inventory.DUI.BGT
{
    public partial class frmBeginTermInStock : Form
    {

        #region Variables
        private PLCBeginTermInStock objPLCBeginTermInStock = new PLCBeginTermInStock();

        #endregion

        #region Permission

        private String strPermission_Delete = "BGT_BeginTermInStock_Delete";
        private String strPermission_Calculate = "BGT_BeginTermInStock_Calculate";

        #endregion

        #region Contructors

        public frmBeginTermInStock()
        {
            InitializeComponent();
        }

        #endregion

        #region Support Functions

        private void LoadData()
        {
            DataTable dtbData = objPLCBeginTermInStock.SearchData(new object() { });
            dtpBeginDate.Value = new DateTime(dtpBeginDate.Value.Year, dtpBeginDate.Value.Month, 1);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                return;
            }
            flexData.DataSource = dtbData;
            FormatFlex();
        }

        private void FormatFlex()
        {
            try
            {
                for (int i = 0; i < flexData.Cols.Count; i++)
                {
                    flexData.Cols[i].AllowEditing = false;
                    flexData.Cols[i].AllowDragging = false;
                    flexData.Cols[i].Visible = false;
                }
                C1FlexGridObject.SetColumnVisible(flexData, true, "BeginTermDate,BeginTermTable,BeginTermTableDetail,CreatedUser,CreatedUserFullName,CreatedDate,UpdatedUser,UpdatedUserFullName,UpdatedDate");
                C1FlexGridObject.SetColumnWidth(flexData, "BeginTermDate,60,BeginTermTable,200,BeginTermTableDetail,220,CreatedUser,100,CreatedUserFullName,120,CreatedDate,110,UpdatedUser,100,UpdatedUserFullName,120,UpdatedDate,110");
                C1FlexGridObject.SetColumnCaption(flexData, "BeginTermDate,Tháng,BeginTermTable,Bảng tồn kho đầu kỳ,BeginTermTableDetail,Bảng tồn kho đầu kỳ chi tiết,CreatedUser,Nhân viên tính,CreatedUserFullName,Nhân viên tính,CreatedDate,Ngày tính,UpdatedUser,Nhân viên cập nhật,UpdatedUserFullName,Nhân viên cập nhật,UpdatedDate,Ngày cập nhật");
                flexData.Cols["BeginTermDate"].Format = "MM/yyyy";
                flexData.Cols["CreatedDate"].Format = "dd/MM/yyyy HH:mm";
                flexData.Cols["UpdatedDate"].Format = "dd/MM/yyyy HH:mm";
                flexData.Rows[0].AllowMerging = true;
                C1.Win.C1FlexGrid.CellStyle style = flexData.Styles.Add("flexStyle");
                style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
                style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
                C1.Win.C1FlexGrid.CellRange range = flexData.GetCellRange(0, 0, 0, flexData.Cols.Count - 1);
                range.Style = style;
                flexData.Rows[0].Height = 30;
                flexData.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
                //btnDelete.Enabled = flexData.Rows.Count > flexData.Rows.Fixed && SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
            }
            catch {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới dữ liệu");
            }
        }

        private bool ValidatingData()
        {
            DateTime dtmCurrent = Globals.GetServerDateTime();
            // Kiểm tra tháng < tháng hiện tại
            if (dtpBeginDate.Value.Month > dtmCurrent.Month && dtpBeginDate.Value.Year >= dtmCurrent.Year)
            {
                MessageBoxObject.ShowWarningMessage(this, "Tháng tính tồn kho phải nhỏ hơn hoặc bằng tháng hiện tại!");
                return false;
            }
            // Kiểm tra & Warning dữ liệu tồn kho đã tính rồi
            BeginTermInStock objBeginTermInStock = objPLCBeginTermInStock.LoadInfo(dtpBeginDate.Value);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                return false;
            }
            if (objBeginTermInStock != null)
            {
                if (MessageBox.Show(this, "Dữ liệu tồn kho tháng " + dtpBeginDate.Value.ToString("MM/yyyy") + " đã được tính rồi\nBạn có muốn tính lại không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
            }
            return true;
        }


        private void Calculate()
        {
            frmWaitDialog.Show(string.Empty, "Thực hiện tính tồn kho đầu kỳ");
            BeginTermInStock objBeginTermInStock = new BeginTermInStock();
            objBeginTermInStock.BeginTermDate = dtpBeginDate.Value;
            objPLCBeginTermInStock.Calculate(objBeginTermInStock);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                frmWaitDialog.Close();
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                grpInfo.Enabled = true;
                return;
            }
            frmWaitDialog.Close();
            MessageBoxObject.ShowInfoMessage(this, "Đã tính xong");
            grpInfo.Enabled = true;
            LoadData();
        }

        private void Delete()
        {
            BeginTermInStock objBeginTermInStock = new BeginTermInStock();
            objBeginTermInStock.BeginTermDate = Convert.ToDateTime(flexData[flexData.RowSel, "BeginTermDate"]);
            objPLCBeginTermInStock.Delete(objBeginTermInStock);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                return;
            }
            LoadData();
            MessageBoxObject.ShowInfoMessage(this, "Đã xóa xong");
        }
        private void InitControl()
        {
            btnCalculate.Enabled = false;
            btnDelete.Visible = false;
        }
        #endregion

        #region Event Functions

        private void frmBeginTermInStock_Load(object sender, EventArgs e)
        {
            InitControl();
            btnCalculate.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Calculate);
            LoadData();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            grpInfo.Enabled = false;            
            if (!ValidatingData())
            {                
                grpInfo.Enabled = true;
                return;
            }
            Thread objThread = new Thread(new ThreadStart(Calculate));
            objThread.Start();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //if (flexData.RowSel < flexData.Rows.Fixed)
            //    return;
            //if (MessageBox.Show(this, "Bạn có muốn xóa dữ liệu tồn kho tháng " + Convert.ToDateTime(flexData[flexData.RowSel, "BeginTermDate"]).ToString("MM/yyyy") + " không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            //    return;
            //Delete();
        }

        private void frmBeginTermInStock_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!grpInfo.Enabled)
                if (MessageBox.Show(this, "Đang thực hiện tính tồn kho đầu kỳ\r\nBạn có muốn dừng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    e.Cancel = true;
        }
        #endregion
    }
}