﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System.Threading;
using ERP.Inventory.PLC.BGT;
using ERP.Inventory.PLC.BGT.WSCumulationTimeOff;

namespace ERP.Inventory.DUI.BGT
{
    public partial class frmCumulationTimeOffCal : Form
    {
        #region Variable

        private PLCCumulationTimeOff objPLCCumulationTimeOff = new PLCCumulationTimeOff();

        #endregion

        #region Constructor        

        public frmCumulationTimeOffCal()
        {
            InitializeComponent();
        }

        #endregion

        #region Even        

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            grpInfo.Enabled = false;
            if (!ValidatingData())
            {
                grpInfo.Enabled = true;
                return;
            }
            Calculate();
            return;
            Thread objThread = new Thread(new ThreadStart(Calculate));
            objThread.Start();
        }

        #endregion

        #region Method        

        private bool ValidatingData()
        {
            DateTime dtmCurrent = Globals.GetServerDateTime();
            // Kiểm tra tháng < tháng hiện tại
            if (dtpBeginDate.Value.Month > dtmCurrent.Month && dtpBeginDate.Value.Year >= dtmCurrent.Year)
            {
                MessageBoxObject.ShowWarningMessage(this, "Năm tính số ngày nghỉ tích lũy theo từng nhân viên phải nhỏ hơn hoặc bằng năm hiện tại!");
                return false;
            }
            return true;
        }

        private void Calculate()
        {
            frmWaitDialog.Show(string.Empty, "Thực hiện tính số ngày nghỉ tích lũy theo từng nhân viên");
            
            objPLCCumulationTimeOff.Calculate(dtpBeginDate.Value.Year);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                frmWaitDialog.Close();
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                grpInfo.Enabled = true;
                return;
            }
            frmWaitDialog.Close();
            MessageBoxObject.ShowInfoMessage(this, "Đã tính xong");
            grpInfo.Enabled = true;
            LoadData();
        }

        private void LoadData()
        {
            DataTable dtbData = objPLCCumulationTimeOff.SearchData(new object[] { "@NewYear", dtpBeginDate.Value.Year });
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                return;
            }
            grdData.DataSource = dtbData;
            //FormatGrid();
        }

        private bool FormatGrid()
        {
            try
            {

                grdViewData.Columns["NEWYEAR"].Caption = "Năm";
                grdViewData.Columns["USERNAME"].Caption = "Nhân viên";
                grdViewData.Columns["CUMULATIONTIMEOFFDAYS"].Caption = "Số ngày nghỉ tích lũy";
                grdViewData.Columns["USEDTIMEOFFDAYS"].Caption = "Số ngày nghỉ đã sử dụng";
                grdViewData.Columns["REMAINTIMEOFFDAYS"].Caption = "Số ngày nghỉ còn lại";
                grdViewData.Columns["ISACTIVE"].Caption = "Kích hoạt";

                grdViewData.OptionsView.ColumnAutoWidth = false;
                grdViewData.Columns["NEWYEAR"].Width = 60;
                grdViewData.Columns["USERNAME"].Width = 250;
                grdViewData.Columns["CUMULATIONTIMEOFFDAYS"].Width = 200;
                grdViewData.Columns["USEDTIMEOFFDAYS"].Width = 200;
                grdViewData.Columns["REMAINTIMEOFFDAYS"].Width = 200;
                grdViewData.Columns["ISACTIVE"].Width = 80;

                grdViewData.Columns["CUMULATIONTIMEOFFDAYS"].DisplayFormat.FormatString = "#,###.##";
                grdViewData.Columns["USEDTIMEOFFDAYS"].DisplayFormat.FormatString = "#,###.##";
                grdViewData.Columns["REMAINTIMEOFFDAYS"].DisplayFormat.FormatString = "#,###.##";

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsSelect.ValueChecked = ((Int16)(1));
                chkCheckBoxIsSelect.ValueUnchecked = ((Int16)(0));
                grdViewData.Columns["ISACTIVE"].ColumnEdit = chkCheckBoxIsSelect;

                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = false;
                grdViewData.OptionsView.ShowGroupPanel = false;
                grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewData.ColumnPanelRowHeight = 40;

                grdViewData.Columns["NEWYEAR"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                grdViewData.Columns["USERNAME"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách số ngày nghỉ tích lũy theo từng nhân viên");
                SystemErrorWS.Insert("Định dạng lưới danh sách thông tin số ngày nghỉ tích lũy theo từng nhân viên", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        #endregion

        private void frmCumulationTimeOffCal_Load(object sender, EventArgs e)
        {
            string[] fieldNames = new string[] { "NEWYEAR", "USERNAME", "CUMULATIONTIMEOFFDAYS", "USEDTIMEOFFDAYS", "REMAINTIMEOFFDAYS", "ISACTIVE" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            FormatGrid();
        }

    }

}
