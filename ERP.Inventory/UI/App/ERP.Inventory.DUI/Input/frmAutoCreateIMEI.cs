﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.Input
{
    /// <summary>
    /// Created by: Nguyễn Linh Tuấn
    /// Desc: Tạo imei tự động
    /// </summary>
    public partial class frmAutoCreateIMEI : Form
    {
        public frmAutoCreateIMEI()
        {
            InitializeComponent();
        }
        public frmAutoCreateIMEI(string strProductID)
        {
            InitializeComponent();
            this.strProductID = strProductID;
        }
        #region Khai báo

        private string strProductID = string.Empty;
        private DataTable dtbIMEIList = null;
        private int intQuantity = 0;

        public int Quantity
        {
            get { return intQuantity; }
            set { intQuantity = value; }
        }

        public DataTable IMEIList
        {
            get { return dtbIMEIList; }
            set { dtbIMEIList = value; }
        }
        #endregion


        #region Phương thức

        private int GetMaxImei()
        {
            int intMaxQuantity = 0;
            try
            {
                ERP.Inventory.PLC.Input.PLCInputVoucher objPLCInputVoucher = new PLC.Input.PLCInputVoucher();
                intMaxQuantity = objPLCInputVoucher.GetMaxIMEI(strProductID, Convert.ToInt32(txtQuantity.EditValue));
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return 0;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi lấy số lượng IMEI hiện có trong ngày của sản phẩm", objExce.ToString(), "frmAutoCreateIMEI -> GetMaxQuantity", "ERP.Sales");
                return 0;
            }
            return intMaxQuantity;
        }

        private bool GenAutoImei()
        {
            try
            {
                ERP.Inventory.PLC.Input.PLCInputVoucher objPLCInputVoucher = new PLC.Input.PLCInputVoucher();
                dtbIMEIList = objPLCInputVoucher.GenAutoIMEI(strProductID, Convert.ToInt32(txtQuantity.EditValue));
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tạo IMEI tự động", objExce.ToString(), "frmAutoCreateIMEI -> CreateIMEI", "ERP.Sales");
                return false;
            }
            return true;
        }

        #endregion

        private void frmAutoCreateIMEI_Load(object sender, EventArgs e)
        {
            try
            {
                if (intQuantity > 0)
                {
                    txtQuantity.EditValue = intQuantity.ToString();
                }
             
            }
            catch (Exception objEx)
            {

                SystemErrorWS.Insert("Lỗi khi đọc danh sách tiền tệ", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi khi đọc danh sách tiền tệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtQuantity_EditValueChanged(object sender, EventArgs e)
        {
            if (intQuantity > 0 && Convert.ToInt32(txtQuantity.EditValue) > intQuantity)
            {
                txtQuantity.EditValue = intQuantity;
            }
        }

        private void txtQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (intQuantity > 0 && Convert.ToInt32(txtQuantity.EditValue) > intQuantity)
            {
                txtQuantity.EditValue = intQuantity;
            }
        }

        private void btnCreateIMEI_Click(object sender, EventArgs e)
        {
            int intQuantity = Convert.ToInt32(txtQuantity.EditValue);
            if (intQuantity < 1)
            {
                MessageBox.Show(this, "Số lượng phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int intMaxQuantity = GetMaxImei();
            int intCanExistQuantity = 99999 - intMaxQuantity;
            if (intCanExistQuantity < intQuantity)
            {
                MessageBox.Show(this, "Số lượng IMEI cần tạo quá lơn\nSố lượng IMEI hiện còn trong ngày của sản phẩm là: " + intCanExistQuantity, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!GenAutoImei())
            {
                MessageBox.Show(this, "Lỗi tạo IMEI tự động", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            intQuantity = Convert.ToInt32(txtQuantity.EditValue);
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtQuantity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnCreateIMEI_Click(null, null);
        }
    }
}
