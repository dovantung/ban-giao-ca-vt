﻿using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Input
{
    public partial class frmRealInputChangeConfirmError : Form
    {
        public DataTable DtbData { get; set; }
        public frmRealInputChangeConfirmError()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }

        private void frmRealInputChangeConfirmError_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            string[] fieldNames = new string[DtbData.Columns.Count];
            for (int i = 0; i < DtbData.Columns.Count; i++)
                fieldNames[i] = DtbData.Columns[i].ToString();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            for (int i = 0; i < DtbData.Columns.Count; i++)
                grdViewData.Columns[i].Width = 120;
            ////////////////////
            DtbData = DtbData.Rows
            .Cast<DataRow>()
            .Where(row => !row.ItemArray.All(field => field is DBNull ||
                                             string.IsNullOrWhiteSpace(field as string)))
            .CopyToDataTable();
            ////////////////////
            grdData.DataSource = DtbData;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcelReturnBool(grdData);
            // QC Nguyệt nói bỏ
            //if (Library.AppCore.Other.GridDevExportToExcel.ExportToExcelReturnBool(grdData))
            //    MessageBoxObject.ShowInfoMessage(this, "Xuất Excel thành công!");
        }
    }
}
