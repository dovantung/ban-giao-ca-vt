﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Input
{
    public partial class frmPopupNote :Form
    {
        public frmPopupNote()
        {
            InitializeComponent();
        }
        public bool isAccept = false;
        private void cmdAccept_Click(object sender, EventArgs e)
        {
            if(txtContent.Text.Trim()==string.Empty)
            {
                MessageBox.Show("Bạn chưa nhập nội dung ghi chú!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            isAccept = true;
            this.Close();
        }
        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}