﻿namespace ERP.Inventory.DUI.Input
{
    partial class frmInputVoucher_ValidBarcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colIsError = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chksChon = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.grpProduct = new DevExpress.XtraEditors.GroupControl();
            this.grdIMEI = new DevExpress.XtraGrid.GridControl();
            this.mnuPopUp = new System.Windows.Forms.ContextMenuStrip();
            this.mnuItemDeleteIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.grvIMEI = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPincode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTextPinCode = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsHasWarranty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnExportExcelTemplate = new DevExpress.XtraEditors.SimpleButton();
            this.btnImportExcel = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rbtnEnterListIMEI = new System.Windows.Forms.RadioButton();
            this.lblLastIMEI = new System.Windows.Forms.Label();
            this.lblFirstIMEI = new System.Windows.Forms.Label();
            this.txtLastIMEI = new System.Windows.Forms.TextBox();
            this.lblIsAutoCreateIMEI = new System.Windows.Forms.Label();
            this.txtTotalIMEIInValid = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalIMEIValid = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalIMEI = new DevExpress.XtraEditors.TextEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem7 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.btnAutoGenImei = new DevExpress.XtraEditors.SimpleButton();
            this.rbtnSearchIMEI = new System.Windows.Forms.RadioButton();
            this.rbtnInsertIMEI = new System.Windows.Forms.RadioButton();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.btnValidate = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.chksChon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpProduct)).BeginInit();
            this.grpProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdIMEI)).BeginInit();
            this.mnuPopUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvIMEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextPinCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEIInValid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEIValid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // colIsError
            // 
            this.colIsError.Caption = "IsError";
            this.colIsError.FieldName = "IsError";
            this.colIsError.Name = "colIsError";
            // 
            // chksChon
            // 
            this.chksChon.AutoHeight = false;
            this.chksChon.Name = "chksChon";
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // grpProduct
            // 
            this.grpProduct.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpProduct.AppearanceCaption.Options.UseFont = true;
            this.grpProduct.AppearanceCaption.Options.UseTextOptions = true;
            this.grpProduct.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grpProduct.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.grpProduct.Controls.Add(this.grdIMEI);
            this.grpProduct.Controls.Add(this.panelControl1);
            this.grpProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpProduct.Location = new System.Drawing.Point(0, 0);
            this.grpProduct.Margin = new System.Windows.Forms.Padding(6);
            this.grpProduct.Name = "grpProduct";
            this.grpProduct.Size = new System.Drawing.Size(984, 535);
            this.grpProduct.TabIndex = 0;
            this.grpProduct.Text = "ProductName";
            // 
            // grdIMEI
            // 
            this.grdIMEI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdIMEI.ContextMenuStrip = this.mnuPopUp;
            this.grdIMEI.Location = new System.Drawing.Point(19, 100);
            this.grdIMEI.MainView = this.grvIMEI;
            this.grdIMEI.Name = "grdIMEI";
            this.grdIMEI.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTextPinCode});
            this.grdIMEI.Size = new System.Drawing.Size(945, 398);
            this.grdIMEI.TabIndex = 1;
            this.grdIMEI.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvIMEI});
            // 
            // mnuPopUp
            // 
            this.mnuPopUp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemDeleteIMEI});
            this.mnuPopUp.Name = "mnuPopUp";
            this.mnuPopUp.Size = new System.Drawing.Size(153, 48);
            this.mnuPopUp.Opening += new System.ComponentModel.CancelEventHandler(this.mnuPopUp_Opening);
            // 
            // mnuItemDeleteIMEI
            // 
            this.mnuItemDeleteIMEI.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuItemDeleteIMEI.Name = "mnuItemDeleteIMEI";
            this.mnuItemDeleteIMEI.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.mnuItemDeleteIMEI.ShowShortcutKeys = false;
            this.mnuItemDeleteIMEI.Size = new System.Drawing.Size(152, 22);
            this.mnuItemDeleteIMEI.Text = "Xóa IMEI";
            this.mnuItemDeleteIMEI.Click += new System.EventHandler(this.mnuItemDeleteIMEI_Click);
            // 
            // grvIMEI
            // 
            this.grvIMEI.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvIMEI.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvIMEI.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvIMEI.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvIMEI.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvIMEI.Appearance.Row.Options.UseFont = true;
            this.grvIMEI.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grvIMEI.ColumnPanelRowHeight = 20;
            this.grvIMEI.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.colIMEI,
            this.colPincode,
            this.colIsHasWarranty,
            this.colStatus,
            this.colIsError});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colIsError;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[Status]=true";
            styleFormatCondition1.Value1 = true;
            this.grvIMEI.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.grvIMEI.GridControl = this.grdIMEI;
            this.grvIMEI.Name = "grvIMEI";
            this.grvIMEI.OptionsNavigation.UseTabKey = false;
            this.grvIMEI.OptionsView.ColumnAutoWidth = false;
            this.grvIMEI.OptionsView.ShowAutoFilterRow = true;
            this.grvIMEI.OptionsView.ShowGroupPanel = false;
            this.grvIMEI.OptionsView.ShowIndicator = false;
            this.grvIMEI.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.grvIMEI_RowCellStyle);
            this.grvIMEI.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvIMEI_CellValueChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Chọn";
            this.gridColumn1.ColumnEdit = this.chksChon;
            this.gridColumn1.FieldName = "IsSelect";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Width = 57;
            // 
            // colIMEI
            // 
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 0;
            this.colIMEI.Width = 200;
            // 
            // colPincode
            // 
            this.colPincode.Caption = "Pincode";
            this.colPincode.ColumnEdit = this.repTextPinCode;
            this.colPincode.FieldName = "PINCode";
            this.colPincode.Name = "colPincode";
            this.colPincode.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPincode.Visible = true;
            this.colPincode.VisibleIndex = 1;
            this.colPincode.Width = 218;
            // 
            // repTextPinCode
            // 
            this.repTextPinCode.AutoHeight = false;
            this.repTextPinCode.MaxLength = 50;
            this.repTextPinCode.Name = "repTextPinCode";
            // 
            // colIsHasWarranty
            // 
            this.colIsHasWarranty.Caption = "Bảo hành";
            this.colIsHasWarranty.ColumnEdit = this.chkIsSelect;
            this.colIsHasWarranty.FieldName = "IsHasWarranty";
            this.colIsHasWarranty.Name = "colIsHasWarranty";
            this.colIsHasWarranty.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIsHasWarranty.Visible = true;
            this.colIsHasWarranty.VisibleIndex = 2;
            this.colIsHasWarranty.Width = 70;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Trạng thái";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 3;
            this.colStatus.Width = 241;
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl1.Controls.Add(this.btnExportExcelTemplate);
            this.panelControl1.Controls.Add(this.btnImportExcel);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.pictureBox2);
            this.panelControl1.Controls.Add(this.pictureBox1);
            this.panelControl1.Controls.Add(this.rbtnEnterListIMEI);
            this.panelControl1.Controls.Add(this.lblLastIMEI);
            this.panelControl1.Controls.Add(this.lblFirstIMEI);
            this.panelControl1.Controls.Add(this.txtLastIMEI);
            this.panelControl1.Controls.Add(this.lblIsAutoCreateIMEI);
            this.panelControl1.Controls.Add(this.txtTotalIMEIInValid);
            this.panelControl1.Controls.Add(this.txtTotalIMEIValid);
            this.panelControl1.Controls.Add(this.txtTotalIMEI);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.label29);
            this.panelControl1.Controls.Add(this.btnAutoGenImei);
            this.panelControl1.Controls.Add(this.rbtnSearchIMEI);
            this.panelControl1.Controls.Add(this.rbtnInsertIMEI);
            this.panelControl1.Controls.Add(this.txtBarcode);
            this.panelControl1.Controls.Add(this.btnValidate);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Location = new System.Drawing.Point(12, 34);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(960, 495);
            this.panelControl1.TabIndex = 0;
            // 
            // btnExportExcelTemplate
            // 
            this.btnExportExcelTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportExcelTemplate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcelTemplate.Appearance.Options.UseFont = true;
            this.btnExportExcelTemplate.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcelTemplate.Location = new System.Drawing.Point(594, 7);
            this.btnExportExcelTemplate.Name = "btnExportExcelTemplate";
            this.btnExportExcelTemplate.Size = new System.Drawing.Size(139, 23);
            this.btnExportExcelTemplate.TabIndex = 8;
            this.btnExportExcelTemplate.Text = "Xuất file Excel mẫu";
            this.btnExportExcelTemplate.Click += new System.EventHandler(this.btnExportExcelTemplate_Click);
            // 
            // btnImportExcel
            // 
            this.btnImportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportExcel.Appearance.Options.UseFont = true;
            this.btnImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnImportExcel.Location = new System.Drawing.Point(740, 7);
            this.btnImportExcel.Name = "btnImportExcel";
            this.btnImportExcel.Size = new System.Drawing.Size(120, 23);
            this.btnImportExcel.TabIndex = 9;
            this.btnImportExcel.Text = "Nhập từ Excel";
            this.btnImportExcel.Click += new System.EventHandler(this.btnImportExcel_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(838, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 16);
            this.label4.TabIndex = 13;
            this.label4.Text = "IMEI không hợp lệ";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(702, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 12;
            this.label3.Text = "IMEI hợp lệ";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Pink;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(782, 37);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 23);
            this.pictureBox2.TabIndex = 54;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(646, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 23);
            this.pictureBox1.TabIndex = 55;
            this.pictureBox1.TabStop = false;
            // 
            // rbtnEnterListIMEI
            // 
            this.rbtnEnterListIMEI.AutoSize = true;
            this.rbtnEnterListIMEI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnEnterListIMEI.Location = new System.Drawing.Point(421, 8);
            this.rbtnEnterListIMEI.Name = "rbtnEnterListIMEI";
            this.rbtnEnterListIMEI.Size = new System.Drawing.Size(137, 20);
            this.rbtnEnterListIMEI.TabIndex = 7;
            this.rbtnEnterListIMEI.Text = "Nhập IMEI theo dãy";
            this.rbtnEnterListIMEI.UseVisualStyleBackColor = true;
            this.rbtnEnterListIMEI.CheckedChanged += new System.EventHandler(this.rbtnEnterListIMEI_CheckedChanged);
            // 
            // lblLastIMEI
            // 
            this.lblLastIMEI.AutoSize = true;
            this.lblLastIMEI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastIMEI.Location = new System.Drawing.Point(7, 40);
            this.lblLastIMEI.Name = "lblLastIMEI";
            this.lblLastIMEI.Size = new System.Drawing.Size(84, 16);
            this.lblLastIMEI.TabIndex = 2;
            this.lblLastIMEI.Text = "Số IMEI cuối:";
            this.lblLastIMEI.Visible = false;
            // 
            // lblFirstIMEI
            // 
            this.lblFirstIMEI.AutoSize = true;
            this.lblFirstIMEI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstIMEI.Location = new System.Drawing.Point(7, 10);
            this.lblFirstIMEI.Name = "lblFirstIMEI";
            this.lblFirstIMEI.Size = new System.Drawing.Size(82, 16);
            this.lblFirstIMEI.TabIndex = 0;
            this.lblFirstIMEI.Text = "Số IMEI đầu:";
            // 
            // txtLastIMEI
            // 
            this.txtLastIMEI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastIMEI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastIMEI.Location = new System.Drawing.Point(93, 37);
            this.txtLastIMEI.MaxLength = 50;
            this.txtLastIMEI.Name = "txtLastIMEI";
            this.txtLastIMEI.Size = new System.Drawing.Size(150, 23);
            this.txtLastIMEI.TabIndex = 3;
            this.txtLastIMEI.Visible = false;
            this.txtLastIMEI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLastIMEI_KeyPress);
            // 
            // lblIsAutoCreateIMEI
            // 
            this.lblIsAutoCreateIMEI.AutoSize = true;
            this.lblIsAutoCreateIMEI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsAutoCreateIMEI.Location = new System.Drawing.Point(344, 40);
            this.lblIsAutoCreateIMEI.Name = "lblIsAutoCreateIMEI";
            this.lblIsAutoCreateIMEI.Size = new System.Drawing.Size(147, 16);
            this.lblIsAutoCreateIMEI.TabIndex = 11;
            this.lblIsAutoCreateIMEI.Text = "(Phát sinh IMEI tự động)";
            this.lblIsAutoCreateIMEI.Visible = false;
            // 
            // txtTotalIMEIInValid
            // 
            this.txtTotalIMEIInValid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTotalIMEIInValid.Location = new System.Drawing.Point(633, 468);
            this.txtTotalIMEIInValid.Name = "txtTotalIMEIInValid";
            this.txtTotalIMEIInValid.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalIMEIInValid.Properties.Appearance.Options.UseFont = true;
            this.txtTotalIMEIInValid.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalIMEIInValid.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalIMEIInValid.Properties.DisplayFormat.FormatString = "N0";
            this.txtTotalIMEIInValid.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalIMEIInValid.Properties.EditFormat.FormatString = "N0";
            this.txtTotalIMEIInValid.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalIMEIInValid.Properties.Mask.EditMask = "N0";
            this.txtTotalIMEIInValid.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalIMEIInValid.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalIMEIInValid.Properties.NullText = "0";
            this.txtTotalIMEIInValid.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtTotalIMEIInValid.Properties.ReadOnly = true;
            this.txtTotalIMEIInValid.Size = new System.Drawing.Size(100, 22);
            this.txtTotalIMEIInValid.TabIndex = 17;
            // 
            // txtTotalIMEIValid
            // 
            this.txtTotalIMEIValid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTotalIMEIValid.Location = new System.Drawing.Point(363, 468);
            this.txtTotalIMEIValid.Name = "txtTotalIMEIValid";
            this.txtTotalIMEIValid.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalIMEIValid.Properties.Appearance.Options.UseFont = true;
            this.txtTotalIMEIValid.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalIMEIValid.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalIMEIValid.Properties.DisplayFormat.FormatString = "N0";
            this.txtTotalIMEIValid.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalIMEIValid.Properties.EditFormat.FormatString = "N0";
            this.txtTotalIMEIValid.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalIMEIValid.Properties.Mask.EditMask = "N0";
            this.txtTotalIMEIValid.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalIMEIValid.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalIMEIValid.Properties.NullText = "0";
            this.txtTotalIMEIValid.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtTotalIMEIValid.Properties.ReadOnly = true;
            this.txtTotalIMEIValid.Size = new System.Drawing.Size(100, 22);
            this.txtTotalIMEIValid.TabIndex = 15;
            // 
            // txtTotalIMEI
            // 
            this.txtTotalIMEI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTotalIMEI.Location = new System.Drawing.Point(138, 468);
            this.txtTotalIMEI.MenuManager = this.barManager1;
            this.txtTotalIMEI.Name = "txtTotalIMEI";
            this.txtTotalIMEI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalIMEI.Properties.Appearance.Options.UseFont = true;
            this.txtTotalIMEI.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalIMEI.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalIMEI.Properties.DisplayFormat.FormatString = "N0";
            this.txtTotalIMEI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalIMEI.Properties.EditFormat.FormatString = "N0";
            this.txtTotalIMEI.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalIMEI.Properties.Mask.EditMask = "N0";
            this.txtTotalIMEI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalIMEI.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalIMEI.Properties.NullText = "0";
            this.txtTotalIMEI.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtTotalIMEI.Properties.ReadOnly = true;
            this.txtTotalIMEI.Size = new System.Drawing.Size(100, 22);
            this.txtTotalIMEI.TabIndex = 13;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barStaticItem6,
            this.barStaticItem7});
            this.barManager1.MaxItemId = 7;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar3.Appearance.Options.UseFont = true;
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem7)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Bắn IMEI: Ctrl+B";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Tìm IMEI: Ctrl+F";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "Đồng ý: Ctrl+S";
            this.barStaticItem3.Id = 2;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "Xóa IMEI: Ctrl+Del";
            this.barStaticItem4.Id = 3;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "Phóng to: F11";
            this.barStaticItem5.Id = 4;
            this.barStaticItem5.Name = "barStaticItem5";
            this.barStaticItem5.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "Thu nhỏ: ESC";
            this.barStaticItem6.Id = 5;
            this.barStaticItem6.Name = "barStaticItem6";
            this.barStaticItem6.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem7
            // 
            this.barStaticItem7.Caption = "Đóng lại: Ctrl+F4";
            this.barStaticItem7.Id = 6;
            this.barStaticItem7.Name = "barStaticItem7";
            this.barStaticItem7.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(984, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 535);
            this.barDockControlBottom.Size = new System.Drawing.Size(984, 26);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 535);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(984, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 535);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(472, 471);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 16);
            this.label2.TabIndex = 16;
            this.label2.Text = "Số IMEI không hợp lệ:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(247, 471);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 16);
            this.label1.TabIndex = 14;
            this.label1.Text = "Số IMEI hợp lệ:";
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Blue;
            this.label29.Location = new System.Drawing.Point(29, 471);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(95, 16);
            this.label29.TabIndex = 12;
            this.label29.Text = "Tổng số IMEI:";
            // 
            // btnAutoGenImei
            // 
            this.btnAutoGenImei.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoGenImei.Appearance.Options.UseFont = true;
            this.btnAutoGenImei.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.btnAutoGenImei.Location = new System.Drawing.Point(251, 37);
            this.btnAutoGenImei.Name = "btnAutoGenImei";
            this.btnAutoGenImei.Size = new System.Drawing.Size(86, 23);
            this.btnAutoGenImei.TabIndex = 4;
            this.btnAutoGenImei.Text = "Gen IMEI";
            this.btnAutoGenImei.Visible = false;
            this.btnAutoGenImei.Click += new System.EventHandler(this.btnAutoGenImei_Click);
            // 
            // rbtnSearchIMEI
            // 
            this.rbtnSearchIMEI.AutoSize = true;
            this.rbtnSearchIMEI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnSearchIMEI.Location = new System.Drawing.Point(337, 9);
            this.rbtnSearchIMEI.Name = "rbtnSearchIMEI";
            this.rbtnSearchIMEI.Size = new System.Drawing.Size(77, 20);
            this.rbtnSearchIMEI.TabIndex = 6;
            this.rbtnSearchIMEI.Text = "Tìm IMEI";
            this.rbtnSearchIMEI.UseVisualStyleBackColor = true;
            // 
            // rbtnInsertIMEI
            // 
            this.rbtnInsertIMEI.AutoSize = true;
            this.rbtnInsertIMEI.Checked = true;
            this.rbtnInsertIMEI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnInsertIMEI.Location = new System.Drawing.Point(252, 9);
            this.rbtnInsertIMEI.Name = "rbtnInsertIMEI";
            this.rbtnInsertIMEI.Size = new System.Drawing.Size(76, 20);
            this.rbtnInsertIMEI.TabIndex = 5;
            this.rbtnInsertIMEI.TabStop = true;
            this.rbtnInsertIMEI.Text = "Bắn IMEI";
            this.rbtnInsertIMEI.UseVisualStyleBackColor = true;
            // 
            // txtBarcode
            // 
            this.txtBarcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarcode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Location = new System.Drawing.Point(93, 7);
            this.txtBarcode.MaxLength = 50;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(150, 23);
            this.txtBarcode.TabIndex = 1;
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // btnValidate
            // 
            this.btnValidate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnValidate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidate.Appearance.Options.UseFont = true;
            this.btnValidate.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnValidate.Location = new System.Drawing.Point(866, 7);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(86, 23);
            this.btnValidate.TabIndex = 10;
            this.btnValidate.Text = "Đồng ý";
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Enabled = false;
            this.btnSave.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnSave.Location = new System.Drawing.Point(866, 7);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.TabStop = false;
            this.btnSave.Text = "Đồng ý";
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmInputVoucher_ValidBarcode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.grpProduct);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "frmInputVoucher_ValidBarcode";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Danh sách IMEI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmInputVoucher_ValidBarcode_FormClosing);
            this.Load += new System.EventHandler(this.frmInputVoucher_ValidBarcode_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmInputVoucher_ValidBarcode_KeyDown);
            this.Resize += new System.EventHandler(this.frmInputVoucher_ValidBarcode_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.chksChon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpProduct)).EndInit();
            this.grpProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdIMEI)).EndInit();
            this.mnuPopUp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvIMEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextPinCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEIInValid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEIValid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl grpProduct;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraBars.BarStaticItem barStaticItem7;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnValidate;
        private System.Windows.Forms.RadioButton rbtnSearchIMEI;
        private System.Windows.Forms.RadioButton rbtnInsertIMEI;
        private System.Windows.Forms.TextBox txtBarcode;
        private DevExpress.XtraGrid.GridControl grdIMEI;
        private DevExpress.XtraGrid.Views.Grid.GridView grvIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colPincode;
        private DevExpress.XtraGrid.Columns.GridColumn colIsHasWarranty;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIsError;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraEditors.TextEdit txtTotalIMEIInValid;
        private DevExpress.XtraEditors.TextEdit txtTotalIMEIValid;
        private DevExpress.XtraEditors.TextEdit txtTotalIMEI;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ContextMenuStrip mnuPopUp;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDeleteIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private System.Windows.Forms.Label lblIsAutoCreateIMEI;
        private System.Windows.Forms.TextBox txtLastIMEI;
        private System.Windows.Forms.RadioButton rbtnEnterListIMEI;
        private System.Windows.Forms.Label lblLastIMEI;
        private System.Windows.Forms.Label lblFirstIMEI;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chksChon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton btnAutoGenImei;
        private DevExpress.XtraEditors.SimpleButton btnImportExcel;
        private DevExpress.XtraEditors.SimpleButton btnExportExcelTemplate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTextPinCode;
    }
}