﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Input
{
    /// <summary>
    /// Created by: Nguyễn Linh Tuấn
    /// Desc: xác nhận tổng số tiền phiếu nhập
    /// </summary>
    public partial class frmConfirmInput : Form
    {
        #region Khai báo
        private decimal decTotalAmount = 0;
        public bool bolIsConfirm = false;
        #endregion

        #region Hàm khởi tạo
        public frmConfirmInput()
        {
            InitializeComponent();
        }
        public frmConfirmInput(decimal decTotalAmount)
		{
			InitializeComponent();
			this.decTotalAmount	= decTotalAmount;

        }
        #endregion

        private void frmConfirmInput_Load(object sender, EventArgs e)
        {

        }

        private void cmdConfirm_Click(object sender, EventArgs e)
        {
            if (txtTotalAmount.Text.Trim().Length == 0 && decTotalAmount !=0)
            {
                MessageBox.Show(this, "Vui lòng nhập tổng tiền phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            decimal decConfirm = 0;
            decimal.TryParse(txtTotalAmount.EditValue.ToString(), out decConfirm);
            if (Math.Abs(decConfirm - decTotalAmount) > 1000)
            {
                MessageBox.Show(this, "Tổng tiền phiếu nhập không đúng!\n Vui lòng nhập lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            bolIsConfirm = true;
            this.Close();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            bolIsConfirm = false;
            this.Close();
        }
    }
}