﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraGrid.Views.Grid;
using Library.AppCore;
using ERP.Inventory.PLC;
using Library.AppCore.LoadControls;
using System.Diagnostics;
using System.Data.Linq.SqlClient;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Threading;

namespace ERP.Inventory.DUI.Input
{
    /// <summary>
    /// Created by: Nguyễn Linh Tuấn
    /// Desc: Phiếu nhập
    /// 
    /// LÊ VĂN ĐÔNG: 11/12/2017 -> Bỏ ràng buộc đã hạch toàn thì không được check thực nhập
    /// LE VAN DONG: 20/03/2018 -> Khóa textbox nhập thông tin hóa đơn khi đơn hàng có hóa đơn
    /// Đặng Thế Hùng: 26/06/2018 -> Thêm column ProductStatusName (ẩn hiện theo mode edit)
    /// </summary>
    public partial class frmInputVoucher : Form
    {
        public frmInputVoucher()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvAttachment);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvInputVoucherDetail);
            this.WindowState = FormWindowState.Maximized;
        }


        #region Khai báo quyền
        bool bolPermission_PM_InputVoucher_ImportExcel = false;
        bool bolPermission_PM_InputVoucher_ImportPinCode = false;
        bool bolPermission_PM_InputVoucher_Create = false;
        bool bolPermission_PM_InputVoucher_AutoCreateIMEI = false;
        bool bolPermission_PM_OutInVoucher_OutVoucher_Create = false;
        bool bolPermission_PM_INPUTVOUCHER_PAYMENTINFO_EDIT = false;
        //End Add---------------------------------------------
        //Edit
        bool bolPermission_PM_InputVoucher_EditInvoiceInfo = false;
        bool bolPermission_PM_InputVoucher_EditInputType = false;
        bool bolPermission_PM_InputVoucher_EditAfterPosted = false;//Quyền chỉnh sửa sau khi đã hoạch chuyển

        bool bolPermission_PM_InputVoucher_Attachment_Add = false;
        bool bolPermission_PM_InputVoucher_Attachment_View = false;
        bool bolPermission_PM_InputVoucher_Attachment_DeleteAll = false;
        bool bolPermission_PM_InputVoucher_Attachment_Delete = false;

        //End Edit

        //Nguyễn Văn Tài bổ sung quyền check thực nhập
        private const String strPerrmissionRealInput = "PM_INPUTVOUCHER_INPUTVALIDATION";
        private bool bolPermissionRealInput = false;
        private bool bolIsCheckRealInput = false;
        //End Nguyễn Văn Tài

        //04/12/2017 Đăng bổ sung
        private bool bolIsCheckInvoiceFirst = false;

        public bool bolIsReloadManager = false;
        //End
        #endregion

        #region Khai báo biến
        private PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
        private ERP.MasterData.PLC.MD.WSInputType.InputType objInputType = null;
        private ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objInputVoucher = new PLC.Input.WSInputVoucher.InputVoucher();
        private ERP.Inventory.PLC.Input.WSInputVoucher.Voucher objVoucher = new PLC.Input.WSInputVoucher.Voucher();
        private List<PLC.Input.WSInputVoucher.InputVoucherDetail> InputVoucherDetailList = new List<PLC.Input.WSInputVoucher.InputVoucherDetail>();
        private List<PLC.Input.WSInputVoucher.InputVoucher_Attachment> InputVoucher_AttachmentList = new List<PLC.Input.WSInputVoucher.InputVoucher_Attachment>();
        private List<PLC.Input.WSInputVoucher.InputVoucher_Attachment> InputVoucher_AttachmentDelList = null;
        private bool bolIsInputFromOrder = false; //Nhập hàng từ đơn hàng (Order)
        private string strNoteFromOrder = string.Empty; //Nhập hàng từ đơn hàng (Order)
        private string strContentFromOrder = string.Empty; //Nhập hàng từ đơn hàng (Order)

        private bool bolIsInputFromRetailInput = false;//Nhập hàng từ định giá máy cũ
        private bool isLoad = false;
        private int intDefaultCustomerID = 0;// khách hàng từ Order
        private ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer_FromOrder = null;//Don hang mua kach le

        private int intStoreDefault = 0;    // Kho nhập hàng từ Order
        private int intCurrencyID = 0;// Tỉ giá từ Order
        private decimal decCurrencyExchange = 0;  // Tỉ giá đơn vị tiền từ Order

        private decimal decOldCurrencyExchange = 0;  // Tỉ giá đơn vị tiền chưa chỉnh sửa        
        private int intInputTypeID = 0;
        private int intIntermediateInputStoreID = 0;
        private decimal decTotalAmount = 0;
        private bool isEdit = false;
        private string strInputVoucherID = string.Empty;
        private string strOrderID = string.Empty;
        private string strRetailInputPriceID = string.Empty;
        public bool bolComplete = false;//Lưu trạng thái tạo phiếu nhập
        private int intInputStoreIDOld = 0;//lưu kho cũ
        private DataTable dtbNotification_User = null;

        public event EventHandler evtLoadByProductIMEIHistory = null;
        private int intCurrencyUnitDefaultID = 1;

        //Đăng bổ sung 
        private DataTable dtbDataGridResult = null;
        private ERP.MasterData.PLC.MD.WSPOType.POType objPOType = null;
        private DateTime dtbServerDate = Library.AppCore.Globals.GetServerDateTime();
        DataTable dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
        DataTable dtWarrantyMonthByProduct = Library.AppCore.DataSource.GetDataSource.GetWarrantyMonthByProduct().Copy();
        bool bolChekRealInputPrint = false;
        private int intStoreFromRetailInput = 0;    // Kho định giá
        #endregion

        #region Properties
        public int StoreFromRetailInput
        {
            get { return intStoreFromRetailInput; }
            set { intStoreFromRetailInput = value; }
        }

        //Don hang mua khach le
        public ERP.MasterData.PLC.MD.WSCustomer.Customer Customer_FromOrder
        {
            get { return objCustomer_FromOrder; }
            set { objCustomer_FromOrder = value; }
        }

        public int TransportCustomerID
        {
            get;
            set;
        }
        /// <summary>
        /// Thông tin khách hàng từ định giá máy cũ
        /// </summary>
        public ERP.MasterData.PLC.MD.WSCustomer.Customer CustomerInfo
        {
            get;
            set;
        }
        /// <summary>
        /// Mã định giá máy cũ
        /// </summary>
        public string RetailInputPriceID
        {
            get { return strRetailInputPriceID; }
            set { strRetailInputPriceID = value; }
        }

        /// <summary>
        /// Thu mua máy cũ chuyển sang máy mới
        /// </summary>
        public bool IsNewFromRetailInput
        {
            get;
            set;
        }

        public bool IsInputFromRetailInput
        {
            get { return bolIsInputFromRetailInput; }
            set { bolIsInputFromRetailInput = value; }
        }

        public int DefaultCustomerID
        {
            get { return intDefaultCustomerID; }
            set { intDefaultCustomerID = value; }
        }

        public decimal CurrencyExchange
        {
            get { return decCurrencyExchange; }
            set { decCurrencyExchange = value; }
        }


        public int CurrencyID
        {
            get { return intCurrencyID; }
            set { intCurrencyID = value; }
        }

        public int StoreIDDefault
        {
            get { return intStoreDefault; }
            set { intStoreDefault = value; }
        }

        public bool IsInputFromOrder
        {
            get { return bolIsInputFromOrder; }
            set
            {
                bolIsInputFromOrder = value;
            }
        }
        public string OrderID
        {
            get { return strOrderID; }
            set { strOrderID = value; }
        }

        public int InputTypeID
        {
            get { return intInputTypeID; }
            set { intInputTypeID = value; }
        }

        public DataTable dtbInputVoucherDetail_Order
        {
            get;
            set;
        }

        public string InputVoucherID
        {
            get { return strInputVoucherID; }
            set { strInputVoucherID = value; }
        }

        public bool IsEdit
        {
            get { return isEdit; }
            set { isEdit = value; }
        }

        public int IntermediateInputStoreID
        {
            get { return intIntermediateInputStoreID; }
            set { intIntermediateInputStoreID = value; }
        }

        public ERP.MasterData.PLC.MD.WSPOType.POType POType
        {
            get { return objPOType; }
            set { objPOType = value; }
        }

        public string NoteFromOrder
        {
            get { return strNoteFromOrder; }
            set { strNoteFromOrder = value; }
        }

        public string ContentFromOrder
        {
            get { return strContentFromOrder; }
            set { strContentFromOrder = value; }
        }

        private string strInvoiceIDOld = string.Empty;
        private string strInvoiceSymbolOld = string.Empty;
        private string strDenominatorOld = string.Empty;
        private DateTime dteInvoiceDateOld;
        #endregion

        #region Methord

        private void CreateTableUserNotify()
        {
            dtbNotification_User = new DataTable();
            dtbNotification_User.TableName = "dtbNotification_User";
            dtbNotification_User.Columns.Add("USERNAME", typeof(string));
            dtbNotification_User.Columns.Add("ISSHIFTCONSIDER", typeof(Int16));
        }

        private void GetListNotification_UserList(int intInputTypeID, int intInputVoucherEventID, int intStoreID, int intStoreType)
        {
            try
            {
                DataTable dtbUserNotify = Library.AppCore.DataSource.GetDataSource.GetInputType_Notify().Copy();
                if (dtbNotification_User == null)
                    CreateTableUserNotify();
                string strFilter = @"INPUTTYPEID={0} and INPUTVOUCHEREVENTID={1} and (STOREID={2} and (STORETYPE={3} or STORETYPE=0))";
                strFilter = string.Format(strFilter, intInputTypeID, intInputVoucherEventID, intStoreID, intStoreType);
                if (dtbUserNotify != null && dtbUserNotify.Rows.Count > 0)
                {
                    DataRow[] drowList = dtbUserNotify.Select(strFilter);
                    if (drowList != null && drowList.Length > 0)
                    {
                        foreach (var item in drowList)
                        {
                            if (dtbNotification_User.Select(string.Format("UserName='{0}'", item["UserName"])).Length > 0)
                                continue;
                            DataRow row = dtbNotification_User.NewRow();
                            row["USERNAME"] = item["USERNAME"];
                            row["ISSHIFTCONSIDER"] = item["ISSHIFTCONSIDER"];
                            dtbNotification_User.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy danh sách người nhận thông báo");
                SystemErrorWS.Insert("Lỗi lấy danh sách người nhận thông báo", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        private void LoadInputVoucher()
        {
            try
            {
                if (strInputVoucherID != string.Empty)
                {
                    objInputVoucher = new PLC.Input.PLCInputVoucher().LoadInfo(strInputVoucherID);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    if (objInputVoucher != null)
                    {
                        txtInputVoucherID.Text = objInputVoucher.InputVoucherID;
                        txtInVoiceID.Text = objInputVoucher.InVoiceID;
                        strInvoiceIDOld = objInputVoucher.InVoiceID.Trim();
                        txtOrderID.Text = objInputVoucher.OrderID;

                        strOrderID = objInputVoucher.OrderID;

                        if (!string.IsNullOrEmpty(strOrderID))
                        {
                            ERP.Procurement.PLC.WSProcurement.Order objPOM_Order = null;
                            objPOM_Order = new ERP.Procurement.PLC.PLCProcurement().LoadInfo(strOrderID);
                            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                            {
                                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                                return;
                            }
                            if (objPOM_Order != null)
                            {
                                new ERP.MasterData.PLC.MD.PLCPOType().LoadInfo(ref objPOType, objPOM_Order.POTypeID);
                            }
                        }

                        cboPayableType.SelectedValue = objInputVoucher.PayableTypeID;
                        cboDiscountReason.SelectedValue = objInputVoucher.DiscountReasonID;
                        txtDiscountInput.Text = Convert.ToString(objInputVoucher.Discount);
                        txtInputContent.Text = objInputVoucher.Content;
                        dtInputDate.Value = objInputVoucher.InputDate.Value;
                        txtInvoiceSymbol.Text = objInputVoucher.InVoiceSymbol;
                        strInvoiceSymbolOld = objInputVoucher.InVoiceSymbol.Trim();
                        cboStoreSearch.SetValue(objInputVoucher.InputStoreID);
                        dtPayableTime.Value = objInputVoucher.PayableDate.Value;
                        txtProtectPriceDiscount.Text = Convert.ToString(objInputVoucher.ProtectPriceDiscount);
                        dtInvoiceDate.DateTime = objInputVoucher.InVoiceDate.Value;
                        dteInvoiceDateOld = objInputVoucher.InVoiceDate.Value;
                        txtNote.Text = objInputVoucher.Note;
                        txtDenominator.Text = objInputVoucher.Denominator;
                        strDenominatorOld = objInputVoucher.Denominator.Trim();
                        cboInputTypeSearch.SetValue(objInputVoucher.InputTypeID);
                        cboCurrencyUnit.SelectedValue = objInputVoucher.CurrencyUnitID;
                        txtExchangeRate.Text = objInputVoucher.CurrencyExchange.ToString();
                        radIsNew.Checked = objInputVoucher.IsNew;
                        radOld.Checked = !objInputVoucher.IsNew;
                        cboTransportCustomer.SetValue(objInputVoucher.TransportCustomerID);
                        //Customer
                        txtCustomerID.Text = Convert.ToString(objInputVoucher.CustomerID);
                        txtAddress.Text = objInputVoucher.CustomerAddress;
                        txtCustomerName.Text = objInputVoucher.CustomerName;
                        txtCustomerIDCard.Text = objInputVoucher.CustomerIDCard;
                        txtTaxID.Text = objInputVoucher.CustomerTaxID;
                        txtPhone.Text = objInputVoucher.CustomerPhone;
                        txtIDCardIssuePlace.Text = objInputVoucher.IdCardIssuePlace;
                        chkIsCheckRealInput.Checked = objInputVoucher.IsCheckRealInput;
                        bolChekRealInputPrint = objInputVoucher.IsCheckRealInput;
                        chkCreateAutoInvoice.Checked = objInputVoucher.IsAutoCreateInvoice;
                        dtmDeliveryNoteDate.EditValue = objInputVoucher.DeliveryNoteDate;
                        if (!objInputVoucher.IsCheckRealInput && !objInputVoucher.ISStoreChange)
                        {
                            //btnRealInputConfirm.Enabled = true;
                            //chkIsCheckRealInput.Enabled = true;
                            //txtCheckRealInputNote.Properties.ReadOnly = false;
                            btnRealInputConfirm.Enabled = bolPermissionRealInput;
                            chkIsCheckRealInput.Enabled = bolPermissionRealInput;
                            txtCheckRealInputNote.Properties.ReadOnly = !bolPermissionRealInput;
                        }
                        else
                        {
                            btnRealInputConfirm.Enabled = false;
                            chkIsCheckRealInput.Enabled = false;
                            txtCheckRealInputNote.Properties.ReadOnly = true;
                            txtCheckRealInputNote.Properties.NullText = string.Empty;
                            txtCheckRealInputNote.Text = objInputVoucher.CheckRealInputNote;
                        }

                        if (objInputVoucher.IdCardIssueDate != null)
                            dtIDCardIssueDate.Value = objInputVoucher.IdCardIssueDate.Value;
                        grdInputVoucherDetail.DataSource = objInputVoucher.InputVoucherDetailList;
                        grdInputVoucherDetail.RefreshDataSource();
                        if (objInputVoucher.InputVoucher_AttachmnetList != null)
                        {
                            InputVoucher_AttachmentList = new List<PLC.Input.WSInputVoucher.InputVoucher_Attachment>(objInputVoucher.InputVoucher_AttachmnetList);
                        }
                        else
                            InputVoucher_AttachmentList = new List<PLC.Input.WSInputVoucher.InputVoucher_Attachment>();
                        grdAttachment.DataSource = InputVoucher_AttachmentList;
                        grdAttachment.RefreshDataSource();
                    }
                }
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi load phiếu nhập");
                SystemErrorWS.Insert("Lỗi load phiếu nhập", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        private void LockControl()
        {
            bool bolEnable = false;
            txtInputVoucherID.ReadOnly = !bolEnable;
            cboPayableType.Enabled = bolEnable;
            cboDiscountReason.Enabled = bolEnable;
            txtDiscountInput.Properties.ReadOnly = !bolEnable;
            dtInputDate.Enabled = bolEnable;
            txtInvoiceSymbol.ReadOnly = !bolEnable;
            cboStoreSearch.Enabled = bolEnable;
            dtPayableTime.Enabled = bolEnable;
            txtProtectPriceDiscount.Properties.ReadOnly = !bolEnable;
            dtInvoiceDate.Properties.ReadOnly = !bolEnable;
            cboInputTypeSearch.Enabled = bolEnable;
            cboCurrencyUnit.Enabled = bolEnable;
            txtExchangeRate.Properties.ReadOnly = !bolEnable;
            radIsNew.Enabled = bolEnable;
            radOld.Enabled = bolEnable;
            btnSearchProduct.Enabled = bolEnable;
            txtBarcode.ReadOnly = !bolEnable;
            txtCustomerID.Enabled = bolEnable;
            lblCustomerID.Enabled = bolEnable;
            cboTransportCustomer.Enabled = bolEnable;
            txtOrderID.ReadOnly = true;
            if (!bolEnable)
                tabControl1.TabPages.Remove(tabOutVoucher);
            for (int i = 0; i < grvInputVoucherDetail.Columns.Count; i++)
            {
                grvInputVoucherDetail.Columns[i].OptionsColumn.AllowEdit = bolEnable;
                if (grvInputVoucherDetail.Columns[i].FieldName.ToUpper() == "TAXEDFEE"
                    || grvInputVoucherDetail.Columns[i].FieldName.ToUpper() == "PURCHASEFEE")
                {
                    if ((objInputType != null && !objInputType.IsAllowInputOtherFee) || bolIsInputFromOrder)
                        grvInputVoucherDetail.Columns[i].OptionsColumn.AllowEdit = false;
                }
            }
            grdInputVoucherDetail.RefreshDataSource();
        }

        /// <summary>
        /// Nạp dữ liệu combobox
        /// </summary>
        private void LoadCombobox()
        {
            try
            {
                //Lấy thông tin kho:	
                if (!IsEdit)
                {
                    //ComboBox cboStore_Temp = new ComboBox();
                    //Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboStore_Temp, true, 0, 0, 0, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT, Library.AppCore.Constant.EnumType.AreaType.AREA, "ISINPUTSTORE=1");
                    //DataTable dtbStore = cboStore_Temp.DataSource as DataTable;
                    //dtbStore.Rows.RemoveAt(0);
                    Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                    objStoreFilter.IsCheckPermission = true;
                    objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.INPUT;
                    objStoreFilter.AreaType = Library.AppCore.Constant.EnumType.AreaType.AREA;

                    if (string.IsNullOrEmpty(this.strRetailInputPriceID))
                    {
                        objStoreFilter.OtherCondition = "ISINPUTSTORE=1";
                    }
                    else
                    {
                        // Lấy cấu hình: Loại kho nhập máy lẻ
                        object obj = Library.AppCore.AppConfig.GetConfigValue("PM_INPUTVOUCHER_RETAILINPUTSTORETYPEID");
                        // < LÊ VAN ĐÔNG - BỔ SUNG NGÀY 07/06/2017
                        if (obj == null)
                        {
                            // Nếu chưa cấu hình - thì để như cũ
                            // Lấy các kho nhập mà User đăng nhập có quyền nhập hàng và có Mã Loại kho = 5
                            objStoreFilter.OtherCondition = "ISINPUTSTORE=1 AND STORETYPEID = 5";
                        }
                        else
                        {
                            // Nếu khai báo giá trị <= 0: Lấy tất cả các kho nhập mà User đăng nhập có quyền nhập hàng.
                            // Nếu khai báo giá trị > 0: Lấy các kho nhập mà User đăng nhập có quyền nhập hàng
                            // và có Mã loại kho = giá trị cấu hình.
                            int kq = 0;
                            Int32.TryParse(obj.ToString(), out kq);
                            if (kq > 0)
                                objStoreFilter.OtherCondition = "ISINPUTSTORE=1 AND STORETYPEID = " + kq;
                            else
                                objStoreFilter.OtherCondition = "ISINPUTSTORE=1";
                        }
                    }
                    // LE VAN ĐÔNG/>
                    cboStoreSearch.InitControl(false, objStoreFilter);
                }
                else
                    cboStoreSearch.InitControl(false, false);
                if (!bolIsInputFromOrder && !isEdit)
                {
                    cboInputTypeSearch.InitControl(false, true, "ISREQUIREORDER=0");
                }
                else
                {
                    cboInputTypeSearch.InitControl(false, true);
                }
                //Nạp thông tin hình thức thanh toán
                Library.AppCore.LoadControls.SetDataSource.SetPayableType(this, cboPayableType, true);
                //Nạp thông tin loại tiền
                Library.AppCore.LoadControls.SetDataSource.SetCurrencyUnit(this, cboCurrencyUnit, true);
                cboCurrencyUnit_SelectionChangeCommitted(null, null);
                //Nạp thông tin lý do giảm giá
                Library.AppCore.LoadControls.SetDataSource.SetDiscountReason(this, cboDiscountReason, Library.AppCore.Constant.EnumType.DiscountType.INPUT);
                //Nạp loại thẻ cho thông tin phiếu chi
                Library.AppCore.LoadControls.SetDataSource.SetPaymentCard(this, cboPaymentCard);
                intCurrencyUnitDefaultID = ERP.MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyUnitDefaultID();
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nạp combobox");
                SystemErrorWS.Insert("Lỗi nạp combobox", objExec, DUIInventory_Globals.ModuleName);
            }
        }
        /// <summary>
        /// Xử lý ChangeVAT
        /// </summary>
        private void ChangeVAT()
        {
            if (InputVoucherDetailList != null && InputVoucherDetailList.Count > 0)
            {
                foreach (var item in InputVoucherDetailList)
                {
                    if (objInputType.IsVATZero)
                    {
                        item.VAT = 0;
                    }
                    else
                    {
                        item.VAT = item.VATProduct;
                    }
                }
            }
        }

        /// <summary>
        /// Tải file đính kèm
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="strLocation"></param>
        /// <returns></returns>
        public bool DownloadFile(string strFileID, string strFileName)
        {
            try
            {
                SaveFileDialog dlgSaveFile = new SaveFileDialog();
                dlgSaveFile.Title = "Lưu file tải về";
                dlgSaveFile.FileName = strFileName;
                string strExtension = Path.GetExtension(strFileName);
                switch (strExtension)
                {
                    case ".xls":
                    case ".xlsx":
                        dlgSaveFile.Filter = "Excel Worksheets(*.xls, *.xlsx)|*.xls; *.xlsx";
                        break;
                    case ".doc":
                    case ".docx":
                        dlgSaveFile.Filter = "Word Documents(*.doc, *.docx)|*.doc; *.docx";
                        break;
                    case ".pdf":
                        dlgSaveFile.Filter = "PDF Files(*.pdf)|*.pdf";
                        break;
                    case ".zip":
                    case ".rar":
                        dlgSaveFile.Filter = "Compressed Files(*.zip, *.rar)|*.zip;*.rar";
                        break;
                    case ".jpg":
                    case ".jpe":
                    case ".jpeg":
                    case ".gif":
                    case ".png":
                    case ".bmp":
                    case ".tif":
                    case ".tiff":
                        dlgSaveFile.Filter = "Image Files(*.jpg, *.jpe, *.jpeg, *.gif, *.png, *.bmp, *.tif, *.tiff)|*.jpg; *.jpe; *.jpeg; *.gif; *.png; *.bmp; *.tif; *.tiff";
                        break;
                    default:
                        dlgSaveFile.Filter = "All Files (*)|*";
                        break;
                }
                if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                {
                    if (dlgSaveFile.FileName != string.Empty)
                    {
                        ERP.FMS.DUI.DUIFileManager.DownloadFile(this, "FMSAplication_ProERP_InputVoucherAttachment", strFileID, string.Empty, dlgSaveFile.FileName);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception objExce)
            {
                MessageBox.Show("Lỗi tải file đính kèm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi tải file đính kèm", objExce.ToString(), " frmInputVoucher-> DownloadFile", DUIInventory_Globals.ModuleName);
                return false;
            }
        }

        /// <summary>
        /// Kiểm tra Barcode
        /// </summary>
        private String BarcodeValidation(String strBarcode, decimal decQuantity)
        {
            if (!Library.AppCore.Other.CheckObject.CheckIMEI(strBarcode))
            {
                return "Mã sản phẩm hoặc IMEI không đúng định dạng!";
            }
            try
            {
                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strBarcode);
                if (objProduct == null)
                    return "Mặt hàng này không tồn tại";
                if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(objProduct.MainGroupID, Library.AppCore.Constant.EnumType.MainGroupPermissionType.INPUT))
                {
                    return "Bạn không có quyền nhập trên ngành hàng " + objProduct.MainGroupName + "!";
                }
                Library.AppCore.Constant.EnumType.IsNewPermissionType enIsNewPermission = radIsNew.Checked ? Library.AppCore.Constant.EnumType.IsNewPermissionType.ISNEW : Library.AppCore.Constant.EnumType.IsNewPermissionType.ISOLD;
                String strIsNew = radIsNew.Checked ? "mới" : "cũ";
                if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(objProduct.MainGroupID, Library.AppCore.Constant.EnumType.MainGroupPermissionType.INPUT, enIsNewPermission))
                {
                    return "Bạn không có quyền nhập máy " + strIsNew + " ngành hàng " + objProduct.MainGroupName;
                }
                if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(cboStoreSearch.StoreID, objProduct.MainGroupID,
                     Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.INPUT))
                {
                    return "Kho này không được phép nhập trên ngành hàng " + objProduct.MainGroupName + "!";
                }
                if (objProduct.IsService)
                {
                    return "Bạn không được phép nhập hàng mặt hàng dịch vụ!";
                }

                if (InputVoucherDetailList != null)
                {
                    var objCheck = from InputVoucherDetail in InputVoucherDetailList
                                   where InputVoucherDetail.ProductID == objProduct.ProductID
                                   select InputVoucherDetail;
                    if (objCheck.Count() > 0)
                    {
                        if (objProduct.IsRequestIMEI)
                            return string.Empty;
                        else
                        {
                            ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail obj = objCheck.First<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail>();
                            obj.Quantity = obj.Quantity + decQuantity;
                            obj.Amount = obj.Quantity * obj.InputPrice + (obj.Quantity * obj.InputPrice * obj.VAT / 100 * obj.VATPercent / 100);
                            grdInputVoucherDetail.RefreshDataSource();
                            return string.Empty;
                        }
                    }
                }

                InsertProductToGrid(objProduct, 1);
            }
            catch (Exception objExec)
            {
                SystemErrorWS.Insert("Lỗi khi bắn barcode", objExec, DUIInventory_Globals.ModuleName);
                return "Mã mặt hàng này không tồn tại trong kho \nhoặc đang trong tình trạng không bán!";
            }
            return string.Empty;
        }

        /// <summary>
        /// Lấy số tháng bảo hành theo Mã sản phẩm
        /// </summary>
        private int GetWarrantyByProductID(string strProductID, bool bolIsNew)
        {
            var row = dtWarrantyMonthByProduct.AsEnumerable().Where(x => x.Field<string>("PRODUCTID").Trim() == strProductID).FirstOrDefault();
            if (row != null && row["WARRANTYMONTH"] != DBNull.Value)
                return Convert.ToInt32(row["WARRANTYMONTH"]);

            return -1;
            //return ERP.MasterData.PLC.MD.PLCWarrantyMonthByProduct.GetWarrantyMonthByProduct(strProductID, bolIsNew);
        }

        /// <summary>
        /// Thêm sản phẩm vào lưới
        /// </summary>
        private void InsertProductToGrid(ERP.MasterData.PLC.MD.WSProduct.Product objProduct, decimal decQuantity)
        {
            InsertProductToGrid(objProduct, decQuantity, 0, 0, objProduct.VAT, objProduct.VATPercent, 0, 0, 0, 0, 0, 0, 0);
        }

        #region Kiểm tra

        private bool CheckInput()
        {
            try
            {
                int intLockDataNumDays = 30 * 1;//SaleManager_Globals.LockDataNumMonth;
                DateTime dtLockDate = dtbServerDate.AddDays(-intLockDataNumDays);
                if (dtInputDate.Value.Date < dtLockDate.Date)
                {
                    MessageBox.Show(this, "Ngày nhập nhỏ hơn ngày cho phép.\n Ngày nhập phải lớn hơn ngày: " + dtLockDate.ToString("dd/MM/yyyy"), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabPage1;
                    dtInputDate.Focus();
                    return false;
                }

                if (Library.AppCore.Globals.DateDiff(dtbServerDate, dtInvoiceDate.DateTime, Globals.DateDiffType.YEAR) >= 1000)
                {
                    MessageBox.Show(this, "Vui lòng nhập ngày hóa đơn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabPage1;
                    dtInvoiceDate.Focus();
                    return false;
                }

                //if (dtInvoiceDate.DateTime.Date < dtLockDate.Date)
                //{
                //    MessageBox.Show(this, "Ngày hóa đơn nhỏ hơn ngày cho phép.\n Ngày hóa đơn phải lớn hơn ngày: " + dtLockDate.ToString("dd/MM/yyyy"), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    tabControl1.SelectedTab = tabPage1;
                //    dtInvoiceDate.Focus();
                //    return false;
                //}

                if (dtInvoiceDate.DateTime.Date > dtbServerDate.Date)
                {
                    MessageBox.Show(this, "Ngày hóa đơn phải nhỏ hơn hoặc bằng thời điểm hiện tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabPage1;
                    dtInvoiceDate.Focus();
                    return false;
                }
                if (dtPayableTime.Value.Date < dtLockDate.Date)
                {
                    MessageBox.Show(this, "Ngày thanh toán nhỏ hơn ngày cho phép.\n Ngày thanh toán phải lớn hơn ngày: " + dtLockDate.ToString("dd/MM/yyyy"), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabPage1;
                    dtPayableTime.Focus();
                    return false;
                }

                if (bolIsInputFromOrder && chkCreateAutoInvoice.Checked)
                {
                    if (txtInVoiceID.Text.Trim() == string.Empty)
                    {
                        MessageBox.Show(this, "Bạn chưa nhập số hóa đơn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedTab = tabPage1;
                        txtInVoiceID.Focus();
                        return false;
                    }
                }

                if (cboStoreSearch.StoreID < 1)
                {
                    MessageBox.Show(this, "Bạn chưa chọn kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabPage1;
                    cboStoreSearch.Focus();
                    return false;
                }

                if (cboInputTypeSearch.InputTypeID < 1)
                {
                    MessageBox.Show(this, "Bạn chưa chọn hình thức nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabPage1;
                    cboInputTypeSearch.Focus();
                    return false;
                }
                if (objInputType == null)
                {
                    MessageBox.Show(this, "Không lấy được thông tin hình thức nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabPage1;
                    cboInputTypeSearch.Focus();
                    return false;
                }
                if (cboPayableType.SelectedIndex == 0)
                {
                    MessageBox.Show(this, "Bạn chưa chọn hình thức thanh toán", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabPage1;
                    cboPayableType.Focus();
                    cboPayableType.DroppedDown = true;
                    return false;
                }
                if (cboCurrencyUnit.SelectedIndex == 0)
                {
                    MessageBox.Show(this, "Bạn chưa chọn loại tiền", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabPage1;
                    cboCurrencyUnit.Focus();
                    cboCurrencyUnit.DroppedDown = true;
                    return false;
                }

                if (cboDiscountReason.SelectedIndex > 0)
                {
                    decimal decDiscount = Convert.ToDecimal(Globals.IsNull(txtDiscountInput.Text, 0));
                    if (decDiscount < 0)
                    {
                        MessageBox.Show(this, "Giảm giá phải là số dương!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedTab = tabPage1;
                        txtDiscountInput.Focus();
                        return false;
                    }
                    if (decDiscount > decTotalAmount)
                    {
                        MessageBox.Show(this, "Giảm giá phải nhỏ hơn tổng số tiền", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedTab = tabPage1;
                        txtDiscountInput.Focus();
                        return false;
                    }

                    if (decDiscount != 0 && cboDiscountReason.SelectedIndex == 0)
                    {
                        MessageBox.Show(this, "Bạn chưa chọn lý do giảm giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedTab = tabPage1;
                        cboDiscountReason.Focus();
                        return false;
                    }

                    if (cboDiscountReason.SelectedIndex > 0 && decDiscount <= 0)
                    {
                        MessageBox.Show(this, "Vui lòng nhập số tiền giảm giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedTab = tabPage1;
                        txtDiscountInput.Focus();
                        return false;
                    }

                }
                return true;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowErrorMessage(this, "Lỗi kiểm tra thông tin phiếu nhập!");
                SystemErrorWS.Insert("Lỗi kiểm tra thông tin phiếu nhập", objExce.ToString());
                return false;
            }
        }

        private bool CheckInputCustomer()
        {
            try
            {
                if (txtCustomerID.Text.Trim() == string.Empty)
                {
                    tabControl1.SelectedTab = tabCustomer;
                    lblCustomerID.Focus();
                    MessageBox.Show(this, "Bạn chưa chọn khách hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                if (txtCustomerName.Text.Trim() == "")
                {
                    tabControl1.SelectedTab = tabCustomer;
                    txtCustomerName.Focus();
                    MessageBox.Show(this, "Vui lòng nhập tên khách hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                if (bolIsInputFromRetailInput)
                {
                    if (txtPhone.Text.Trim().Length == 0)
                    {
                        tabControl1.SelectedTab = tabCustomer;
                        txtPhone.Focus();
                        MessageBox.Show(this, "Vui lòng nhập số điện thoại khách hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                if (txtPhone.Text.Trim().Length > 0 && (!Globals.CheckIsPhoneNumber(txtPhone.Text.Trim())
                    || txtPhone.Text.Trim().Length < 10))
                {
                    tabControl1.SelectedTab = tabCustomer;
                    txtPhone.Focus();
                    MessageBox.Show(this, "Số điện thoại khách hàng không đúng định dạng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                if (bolIsInputFromRetailInput)
                {
                    if (txtCustomerIDCard.Text.Trim().Length == 0)
                    {
                        tabControl1.SelectedTab = tabCustomer;
                        txtCustomerIDCard.Focus();
                        MessageBox.Show(this, "Vui lòng nhập số CMND khách hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                //TUAN ANH: Chỉnh sửa text CMND cho phép nhập chữ và tự động viết hoa
                if (!string.IsNullOrEmpty(txtCustomerIDCard.Text.Trim()))
                {
                    string strResultCheckPersonalID = Library.AppCore.Other.CheckObject.CheckPersonalID(txtCustomerIDCard.Text.Trim());
                    if (!string.IsNullOrEmpty(strResultCheckPersonalID))
                    {
                        tabControl1.SelectedTab = tabCustomer;
                        txtCustomerIDCard.Focus();
                        MessageBoxObject.ShowWarningMessage(this, strResultCheckPersonalID);
                        return false;
                    }
                }

                //if (txtCustomerIDCard.Text.Trim().Length > 0 && (!Globals.CheckIsNumber(txtCustomerIDCard.Text.Trim())
                //    || txtCustomerIDCard.Text.Trim().Length < 9))
                //{
                //    tabControl1.SelectedTab = tabCustomer;
                //    txtCustomerIDCard.Focus();
                //    MessageBox.Show(this, "Số CMND khách hàng không đúng định dạng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return false;
                //}
                return true;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowErrorMessage(this, "Lỗi kiểm tra thông tin khách hàng!");
                SystemErrorWS.Insert("Lỗi kiểm tra thông tin khách hàng", objExce.ToString());
                return false;
            }
        }

        private bool CheckInputVoucher()
        {
            try
            {
                if (objInputType.IsCreateOutVoucher)
                {
                    if (Convert.ToInt32(cboVoucherType.SelectedValue) < 1)
                    {
                        MessageBox.Show(this, "Vui lòng chọn hình thức của thông tin phiếu chi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedTab = tabOutVoucher;
                        cboVoucherType.Focus();
                        return false;
                    }
                }
                ERP.MasterData.PLC.MD.WSPayableType.PayableType objPayableType = null;
                new ERP.MasterData.PLC.MD.PLCPayableType().LoadInfo(ref objPayableType, Convert.ToInt32(cboPayableType.SelectedValue));
                if (!objPayableType.IsDebtAllow)
                {
                    CalculateCMVoucher();
                    if (Convert.ToDecimal(txtDebt.EditValue) > 0)
                    {
                        MessageBox.Show(this, "Hình thức thanh toán không cho phép nợ lại! Vui lòng thanh toán hết.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedTab = tabOutVoucher;
                        return false;
                    }
                }
                if (cboPaymentCard.SelectedIndex == 0 && Convert.ToDecimal(txtPaymentCard.EditValue) > 0)
                {
                    MessageBox.Show(this, "Vui lòng chọn loại thẻ thanh toán", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabOutVoucher;
                    txtPaymentCard.Focus();
                    return false;
                }

                if (!Convert.IsDBNull(txtTotalLiquidate.EditValue) && Convert.ToDecimal(txtTotalLiquidate.EditValue) < 0)
                {
                    MessageBox.Show(this, "Trả trước + giảm phải nhỏ hơn hoặc bằng tổng số tiền", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tabControl1.SelectedTab = tabOutVoucher;
                    return false;
                }

                return true;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowErrorMessage(this, "Lỗi kiểm tra thông tin phiếu chi!");
                SystemErrorWS.Insert("Lỗi kiểm tra thông tin phiếu chi", objExce.ToString());
                return false;
            }
        }

        private bool CheckInputVoucherDetail()
        {
            try
            {
                if (InputVoucherDetailList.Count <= 0)
                {
                    MessageBox.Show("Bạn phải nhập tối thiểu một mặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                if (colCheck.Visible)
                {
                    if (InputVoucherDetailList.Sum(x => x.Quantity) <= 0)
                    {
                        MessageBox.Show("Số lượng sản phẩm trên phiếu nhập phải lớn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                foreach (PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail1 in InputVoucherDetailList)
                {
                    //kiểm tra nhập IMEI hay chưa?
                    if (!colCheck.Visible)
                    {
                        if (objInputVoucherDetail1.Quantity == 0)
                        {
                            if (objInputVoucherDetail1.IsRequestIMEI)
                                MessageBox.Show("Vui lòng nhập IMEI sản phẩm " + objInputVoucherDetail1.ProductName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            else
                                MessageBox.Show("Vui lòng nhập số lượng sản phẩm " + objInputVoucherDetail1.ProductName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }

                    //if (!objInputType.IsPriceZero)
                    //{
                    //    if (objInputVoucherDetail1.InputPrice == 0)
                    //    {
                    //        MessageBox.Show("Vui lòng nhập đơn giá " + objInputVoucherDetail1.ProductName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //        return false;
                    //    }
                    //}
                }
                if (!ConfirmTotalAmount())
                    return false;
                return true;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowErrorMessage(this, "Lỗi kiểm tra thông tin chi tiết phiếu nhập!");
                SystemErrorWS.Insert("Lỗi kiểm tra thông tin chi tiết phiếu nhập", objExce.ToString());
                return false;
            }
        }

        /// <summary>
        /// Xác nhận số tiền
        /// </summary>
        /// <returns></returns>
        private bool ConfirmTotalAmount()
        {
            bool bolResult = false;
            decTotalAmount = Math.Round(InputVoucherDetailList.Sum(item => item.Amount), 4);
            decimal decConfirmTotalAmount = decTotalAmount - Math.Round(Convert.ToDecimal(txtDiscountInput.EditValue) + Convert.ToDecimal(txtProtectPriceDiscount.EditValue), 4);
            frmConfirmInput frmConfirmInput1 = new frmConfirmInput(decConfirmTotalAmount);
            frmConfirmInput1.ShowDialog();
            if (frmConfirmInput1.bolIsConfirm)
                bolResult = true;
            return bolResult;
        }
        #endregion

        /// <summary>
        /// Thêm sản phẩm vào lưới
        /// </summary>
        /// </summary>
        /// <param name="objProduct">Sản phẩm</param>
        /// <param name="decQuantity">Số lượng</param>
        /// <param name="decOrderQuantity">Số lượng đặt hàng (từ đơn hàng còn lại)</param>
        /// <param name="decTotalOrderQuantity">Số lượng đặt hàng (từ đơn hàng)</param>
        /// <param name="decPrice">Giá inputPrice</param>
        /// <param name="intVAT"></param>
        /// <param name="intVATPercent"></param>
        private void InsertProductToGrid(ERP.MasterData.PLC.MD.WSProduct.Product objProduct, decimal decQuantity,
            decimal decOrderQuantity, decimal decPrice, int intVAT, int intVATPercent, decimal decTaxedFee, decimal decPurchaseFee, decimal decTotalOrderQuantity, decimal decOrderTaxedFee, decimal decOrderPurchaseFee, decimal decQuantityToInput, decimal decQuantityRemain)
        {
            PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = new PLC.Input.WSInputVoucher.InputVoucherDetail();
            int intMonthOfWarranty = GetWarrantyByProductID(objProduct.ProductID, radIsNew.Checked);
            objInputVoucherDetail.ProductID = objProduct.ProductID;
            objInputVoucherDetail.ProductName = objProduct.ProductName;
            objInputVoucherDetail.QuantityUnitName = objProduct.QuantityUnitName;
            objInputVoucherDetail.ProductIONDate = dtbServerDate;
            if (intMonthOfWarranty == -1)
                objInputVoucherDetail.ENDWarrantyDate = dtbServerDate;
            else
                objInputVoucherDetail.ENDWarrantyDate = dtbServerDate.AddMonths(intMonthOfWarranty);
            objInputVoucherDetail.Quantity = objProduct.IsRequestIMEI ? 0 : decQuantity;
            if (isLoad)
            {
                objInputVoucherDetail.Quantity = 0;

            }
            objInputVoucherDetail.InputPrice = decPrice;
            objInputVoucherDetail.FirstInputTypeID = objInputType.InputTypeID;
            objInputVoucherDetail.ORIGINALInputPrice = decPrice;
            objInputVoucherDetail.VAT = objInputType.IsVATZero ? 0 : intVAT;
            objInputVoucherDetail.VATPercent = intVATPercent;
            objInputVoucherDetail.VATProduct = objProduct.VAT;
            objInputVoucherDetail.IsRequestIMEI = objProduct.IsRequestIMEI;
            objInputVoucherDetail.IsAutoCreateIMEI = !objInputType.IsRequireOrder ? false : objProduct.IsAutoCreateIMEI;
            objInputVoucherDetail.IsAllowDecimal = objProduct.IsAllowDecimal;
            objInputVoucherDetail.IMEIFormatEXP = objProduct.IMEIFormatEXP;
            objInputVoucherDetail.OrderQuantity = decOrderQuantity;
            objInputVoucherDetail.InputStoreID = cboStoreSearch.StoreID;
            objInputVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
            objInputVoucherDetail.TaxedFee = decTaxedFee;
            objInputVoucherDetail.PurchaseFee = decPurchaseFee;
            objInputVoucherDetail.TotalOrderQuantity = decTotalOrderQuantity;
            objInputVoucherDetail.OrderTaxedFee = decOrderTaxedFee;
            objInputVoucherDetail.OrderPurchaseFee = decOrderPurchaseFee;
            objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);
            objInputVoucherDetail.QuantityToInput = decQuantityToInput - objInputVoucherDetail.Quantity;
            objInputVoucherDetail.IsCheckQuantity = objInputVoucherDetail.Quantity > 0 ? true : false;
            objInputVoucherDetail.QuantityRemain = decQuantityRemain;
            objInputVoucherDetail.IsRequirePinCode = objProduct.IsRequirePINCode;
            InputVoucherDetailList.Add(objInputVoucherDetail);
        }

        private void InsertProductToGrid(DataRow rowProduct, decimal decQuantity,
            decimal decOrderQuantity, decimal decPrice, int intVAT, int intVATPercent, decimal decTaxedFee, decimal decPurchaseFee, decimal decTotalOrderQuantity, decimal decOrderTaxedFee, decimal decOrderPurchaseFee, decimal decQuantityToInput, decimal decQuantityRemain, string strOrderDetailID = "")
        {
            PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = new PLC.Input.WSInputVoucher.InputVoucherDetail();
            int intMonthOfWarranty = GetWarrantyByProductID(rowProduct["PRODUCTID"].ToString().Trim(), radIsNew.Checked);
            objInputVoucherDetail.ProductID = rowProduct["PRODUCTID"].ToString().Trim();
            objInputVoucherDetail.ProductName = rowProduct["PRODUCTNAME"].ToString().Trim();
            objInputVoucherDetail.QuantityUnitName = rowProduct["QUANTITYUNITNAME"].ToString().Trim();
            objInputVoucherDetail.OrderDetailID = strOrderDetailID;
            objInputVoucherDetail.ProductIONDate = dtbServerDate;
            if (intMonthOfWarranty == -1)
                objInputVoucherDetail.ENDWarrantyDate = dtbServerDate;
            else
                objInputVoucherDetail.ENDWarrantyDate = dtbServerDate.AddMonths(intMonthOfWarranty);
            objInputVoucherDetail.Quantity = Convert.ToBoolean(rowProduct["ISREQUESTIMEI"]) ? 0 : decQuantity;
            if (isLoad)
            {
                objInputVoucherDetail.Quantity = 0;

            }

            objInputVoucherDetail.FirstInputTypeID = objInputType.InputTypeID;
            objInputVoucherDetail.VAT = objInputType.IsVATZero ? 0 : intVAT;
            objInputVoucherDetail.VATPercent = intVATPercent;
            objInputVoucherDetail.VATProduct = Convert.ToInt32(rowProduct["VAT"]);
            objInputVoucherDetail.IsRequestIMEI = Convert.ToBoolean(rowProduct["ISREQUESTIMEI"]);
            objInputVoucherDetail.IsAutoCreateIMEI = !objInputType.IsRequireOrder ? false : Convert.ToBoolean(rowProduct["ISAUTOCREATEIMEI"]);
            objInputVoucherDetail.IsAllowDecimal = Convert.ToBoolean(rowProduct["ISALLOWDECIMAL"]);
            objInputVoucherDetail.IMEIFormatEXP = rowProduct["IMEIFORMATEXP"].ToString().Trim();
            objInputVoucherDetail.OrderQuantity = decOrderQuantity;
            objInputVoucherDetail.InputStoreID = cboStoreSearch.StoreID;
            objInputVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
            objInputVoucherDetail.TaxedFee = decTaxedFee;
            objInputVoucherDetail.PurchaseFee = decPurchaseFee;
            objInputVoucherDetail.TotalOrderQuantity = decTotalOrderQuantity;
            objInputVoucherDetail.OrderTaxedFee = decOrderTaxedFee;
            objInputVoucherDetail.OrderPurchaseFee = decOrderPurchaseFee;

            if (bolIsInputFromRetailInput)//Tạo phiếu nhập từ màn hình định giá máy cũ
            {
                if (Convert.ToInt32(dtbInputVoucherDetail_Order.Rows[0]["ISHASVAT"]) == 0)//Nếu giá chưa bao gồm thuế
                {
                    objInputVoucherDetail.InputPrice = decPrice;
                    objInputVoucherDetail.ORIGINALInputPrice = decPrice;
                    objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);
                }
                else
                {
                    objInputVoucherDetail.Amount = decPrice;
                    objInputVoucherDetail.InputPrice = objInputVoucherDetail.Amount / (objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100) / objInputVoucherDetail.Quantity;
                    objInputVoucherDetail.ORIGINALInputPrice = objInputVoucherDetail.InputPrice;
                }
            }
            else
            {
                objInputVoucherDetail.InputPrice = decPrice;
                objInputVoucherDetail.ORIGINALInputPrice = decPrice;
                objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);
            }

            objInputVoucherDetail.QuantityToInput = decQuantityToInput - objInputVoucherDetail.Quantity;
            objInputVoucherDetail.IsCheckQuantity = objInputVoucherDetail.Quantity > 0 ? true : false;
            objInputVoucherDetail.QuantityRemain = decQuantityRemain;
            objInputVoucherDetail.IsRequirePinCode = Convert.ToBoolean(rowProduct["ISREQUIREPINCODE"]);
            InputVoucherDetailList.Add(objInputVoucherDetail);
        }

        /// <summary>
        /// Thêm sản phẩm vào lưới
        /// </summary>
        /// </summary>
        /// <param name="objProduct">Sản phẩm</param>
        /// <param name="decQuantity">Số lượng</param>
        /// <param name="decOrderQuantity">Số lượng đặt hàng (từ đơn hàng)</param>
        /// <param name="decPrice">Giá inputPrice</param>
        /// <param name="intVAT"></param>
        /// <param name="intVATPercent"></param>
        private void InsertProductToGrid(ERP.MasterData.PLC.MD.WSProduct.Product objProduct, decimal decQuantity,
            decimal decOrderQuantity, decimal decPrice, int intVAT, int intVATPercent, bool bolBrandNewWarranty, DateTime dtEndWarrantyDate)
        {
            PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = new PLC.Input.WSInputVoucher.InputVoucherDetail();

            objInputVoucherDetail.ProductID = objProduct.ProductID;
            objInputVoucherDetail.ProductName = objProduct.ProductName;
            objInputVoucherDetail.QuantityUnitName = objProduct.QuantityUnitName;
            objInputVoucherDetail.ProductIONDate = dtbServerDate;
            if (!bolBrandNewWarranty)
            {
                int intMonthOfWarranty = GetWarrantyByProductID(objProduct.ProductID, radIsNew.Checked);
                if (intMonthOfWarranty == -1)
                    objInputVoucherDetail.ENDWarrantyDate = dtbServerDate;
                else
                    objInputVoucherDetail.ENDWarrantyDate = dtbServerDate.AddMonths(intMonthOfWarranty);
            }
            else
                objInputVoucherDetail.ENDWarrantyDate = dtEndWarrantyDate;
            objInputVoucherDetail.Quantity = objProduct.IsRequestIMEI ? 0 : decQuantity;
            objInputVoucherDetail.InputPrice = decPrice;
            objInputVoucherDetail.FirstInputTypeID = objInputType.InputTypeID;
            objInputVoucherDetail.ORIGINALInputPrice = decPrice;
            objInputVoucherDetail.VAT = objInputType.IsVATZero ? 0 : intVAT;
            objInputVoucherDetail.VATPercent = intVATPercent;
            objInputVoucherDetail.VATProduct = objProduct.VAT;

            if (bolIsInputFromRetailInput)//Tạo phiếu nhập từ màn hình định giá máy cũ
            {
                if (Convert.ToInt32(dtbInputVoucherDetail_Order.Rows[0]["ISHASVAT"]) == 0)//Nếu giá chưa bao gồm thuế
                {
                    objInputVoucherDetail.InputPrice = decPrice;
                    objInputVoucherDetail.ORIGINALInputPrice = decPrice;
                }
                else
                {
                    objInputVoucherDetail.InputPrice = (decPrice / ((decimal)(objInputVoucherDetail.VAT + objInputVoucherDetail.VATPercent) / 100));
                    objInputVoucherDetail.ORIGINALInputPrice = objInputVoucherDetail.InputPrice;
                }
            }
            else
            {
                objInputVoucherDetail.InputPrice = decPrice;
                objInputVoucherDetail.ORIGINALInputPrice = decPrice;
            }

            objInputVoucherDetail.IsRequestIMEI = objProduct.IsRequestIMEI;
            objInputVoucherDetail.IsAutoCreateIMEI = !objInputType.IsRequireOrder ? false : objProduct.IsAutoCreateIMEI;
            objInputVoucherDetail.IsAllowDecimal = objProduct.IsAllowDecimal;
            objInputVoucherDetail.IMEIFormatEXP = objProduct.IMEIFormatEXP;
            objInputVoucherDetail.OrderQuantity = decOrderQuantity;
            objInputVoucherDetail.InputStoreID = cboStoreSearch.StoreID;
            objInputVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
            objInputVoucherDetail.IsRequirePinCode = objProduct.IsRequirePINCode;
            InputVoucherDetailList.Add(objInputVoucherDetail);
        }

        private void CalculateCMVoucher()
        {
            try
            {
                decimal decProtectPriceDiscount = Convert.ToDecimal(txtProtectPriceDiscount.EditValue);
                decimal decDiscountMoney = Math.Round(Convert.ToDecimal(txtDiscountInput.EditValue) + decProtectPriceDiscount, 4);
                decimal decTotalLiquidate = decTotalAmount - decDiscountMoney;
                txtDiscount.EditValue = decDiscountMoney;
                txtTotalMoney.EditValue = decTotalAmount;
                txtTotalLiquidate.EditValue = decTotalLiquidate;
                txtCashVND_EditValueChanged(null, null);
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowErrorMessage(this, "Lỗi tính tiền phiếu chi!");
                SystemErrorWS.Insert("Lỗi tính tiền phiếu chi", objExce.ToString());
            }
        }

        private void RemoveProduct()
        {
            try
            {
                if (grvInputVoucherDetail.FocusedRowHandle < 0)
                    return;
                PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetail)grvInputVoucherDetail.GetRow(grvInputVoucherDetail.FocusedRowHandle);
                if (objInputVoucherDetail != null && InputVoucherDetailList != null)
                {
                    if (MessageBox.Show(this, "Bạn muốn xóa sản phẩm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        InputVoucherDetailList.Remove(objInputVoucherDetail);
                        grdInputVoucherDetail.RefreshDataSource();
                    }

                }
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi xóa sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi xóa sản phẩm", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
            }
        }



        /// <summary>
        /// cập nhật một hóa đơn nhập hàng
        /// </summary>
        private bool UpdateData(decimal decDiscount, decimal decTotalAmount, ref int intVoucherTypeID, ref string strOutVoucherID)
        {
            //Library.AppCore.Forms.frmLoading.Show("Đang cập nhật dữ liệu!","Vui lòng đợi...");
            btnUpdate.Enabled = false;
            int intInputStoreID = cboStoreSearch.StoreID;
            string strMes = string.Empty;
            if (intIntermediateInputStoreID > 0)
            {
                intInputStoreID = intIntermediateInputStoreID;
            }
            try
            {
                if (!IsEdit)//Thêm mới
                {
                    #region
                    DateTime dtmBeginTime = Library.AppCore.Globals.GetServerDateTime();
                    int intDiscountReasonID = Convert.ToInt32(cboDiscountReason.SelectedValue);
                    objInputVoucher.OrderID = txtOrderID.Text.Trim();
                    objInputVoucher.InVoiceID = txtInVoiceID.Text;
                    objInputVoucher.InVoiceSymbol = txtInvoiceSymbol.Text;
                    objInputVoucher.Denominator = txtDenominator.Text;
                    objInputVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                    objInputVoucher.CustomerName = txtCustomerName.Text;
                    objInputVoucher.CustomerAddress = txtAddress.Text;
                    objInputVoucher.CustomerPhone = txtPhone.Text;
                    objInputVoucher.CustomerTaxID = txtTaxID.Text;
                    objInputVoucher.CustomerEmail = string.Empty;
                    objInputVoucher.CustomerIDCard = txtCustomerIDCard.Text;
                    objInputVoucher.IdCardIssueDate = dtIDCardIssueDate.Value;
                    objInputVoucher.IdCardIssuePlace = txtIDCardIssuePlace.Text;
                    objInputVoucher.Note = txtNote.Text.Trim();
                    //objInputVoucher.Content = txtContent.Text; Thiên rào, lưu sai trường
                    objInputVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    objInputVoucher.InputStoreID = intInputStoreID;
                    objInputVoucher.InputTypeID = objInputType.InputTypeID;
                    objInputVoucher.IdCardIssueDate = dtIDCardIssueDate.Value;
                    objInputVoucher.IdCardIssuePlace = txtIDCardIssuePlace.Text;
                    objInputVoucher.PayableTypeID = Convert.ToInt32(cboPayableType.SelectedValue);
                    objInputVoucher.CurrencyUnitID = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
                    objInputVoucher.DiscountReasonID = intDiscountReasonID;
                    objInputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objInputVoucher.InVoiceDate = dtInvoiceDate.DateTime;
                    dtInputDate.Value = Library.AppCore.Globals.GetServerDateTime();
                    objInputVoucher.InputDate = dtInputDate.Value;
                    objInputVoucher.PayableDate = dtPayableTime.Value;
                    //                objInputVoucher.TaxMonth=
                    objInputVoucher.Discount = decDiscount;
                    objInputVoucher.CurrencyExchange = Convert.ToDecimal(txtExchangeRate.EditValue);
                    objInputVoucher.Content = txtInputContent.Text;
                    objInputVoucher.ProtectPriceDiscount = Convert.ToDecimal(txtProtectPriceDiscount.EditValue);
                    decimal decTotalAmountBF = 0;
                    decimal decTotalVAT = 0;
                    GetTotal(ref decTotalAmountBF, ref decTotalVAT);
                    objInputVoucher.TotalAmountBFT = decTotalAmountBF;
                    objInputVoucher.TotalVAT = decTotalVAT;
                    objInputVoucher.TotalAmount = decTotalAmount;
                    objInputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objInputVoucher.StaffUser = SystemConfig.objSessionUser.UserName;
                    objInputVoucher.IsNew = radIsNew.Checked;
                    objInputVoucher.ProtectPriceDiscount = Convert.ToDecimal(txtProtectPriceDiscount.EditValue);

                    objInputVoucher.IsReturnWithFee = false;
                    objInputVoucher.IsCheckRealInput = objInputType.IsAutoCheckRealInput;
                    objInputVoucher.ISStoreChange = false;
                    objInputVoucher.IsPosted = false;
                    objInputVoucher.IsCreateOutVoucher = objInputType.IsCreateOutVoucher;
                    objInputVoucher.IsAutoCreateInvoice = chkCreateAutoInvoice.Checked;
                    objInputVoucher.DeliveryNoteDate = Convert.ToDateTime(dtmDeliveryNoteDate.EditValue);
                    #endregion
                    if (bolIsInputFromOrder)
                    {
                        #region Ân revert phần không lên live
                        //if (bolIsCheckInvoiceFirst)
                        //{

                        //    InputVoucherDetailList.ForEach(x => { x.QuantityRemain = 0; });
                        //    //InputVoucherDetailList.ForEach(x => { x.QuantityRemain = 0; x.InvoiceQuantity = x.Quantity; });

                        //    List<PLC.Input.WSInputVoucher.InputVoucherDetail> filteredInputVoucherDetailList = InputVoucherDetailList.Where(x => x.IsCheckQuantity == true).ToList();
                        //    objInputVoucher.InputVoucherDetailList = filteredInputVoucherDetailList.ToArray();
                        //}
                        //else
                        //{
                        //    List<PLC.Input.WSInputVoucher.InputVoucherDetail> filteredInputVoucherDetailList = InputVoucherDetailList.Where(x => x.IsCheckQuantity == true).ToList();
                        //objInputVoucher.InputVoucherDetailList = filteredInputVoucherDetailList.ToArray();
                        //}
                        #endregion
                        if (bolIsCheckInvoiceFirst)
                        {
                            //InputVoucherDetailList.ForEach(x => { x.QuantityRemain = 0; });
                            InputVoucherDetailList.ForEach(x => { x.QuantityRemain = 0; x.InvoiceQuantity = x.Quantity; });
                            List<PLC.Input.WSInputVoucher.InputVoucherDetail> filteredInputVoucherDetailList = InputVoucherDetailList.Where(x => x.IsCheckQuantity == true).ToList();
                            objInputVoucher.InputVoucherDetailList = filteredInputVoucherDetailList.ToArray();
                        }
                        else
                        {
                            List<PLC.Input.WSInputVoucher.InputVoucherDetail> filteredInputVoucherDetailList = InputVoucherDetailList.Where(x => x.IsCheckQuantity == true).ToList();
                            objInputVoucher.InputVoucherDetailList = filteredInputVoucherDetailList.ToArray();
                        }
                    }
                    else
                    {
                        objInputVoucher.InputVoucherDetailList = InputVoucherDetailList.ToArray();
                    }
                    objInputVoucher.InputVoucher_AttachmnetList = InputVoucher_AttachmentList.ToArray();
                    objInputVoucher.TransportCustomerID = cboTransportCustomer.CustomerID;
                    //Tạo Phiếu Nhập không có tạo Phiếu Chi
                    if (objInputType.IsCreateOutVoucher)
                    {
                        ERP.MasterData.PLC.MD.PLCCustomer objPCLCustomer = new MasterData.PLC.MD.PLCCustomer();
                        //ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
                        //objPCLCustomer.LoadInfo(ref objCustomer, Convert.ToInt32(cboCustomer.SelectedValue));
                        //Thông tin Phiếu Chi
                        if (objVoucher == null)
                            objVoucher = new PLC.Input.WSInputVoucher.Voucher();
                        objVoucher.InvoiceID = txtInVoiceID.Text.Trim();
                        objVoucher.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                        //objVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text); Thiên rào, đã có ở dưới
                        objVoucher.Discount = decDiscount + Convert.ToDecimal(txtProtectPriceDiscount.EditValue);
                        objVoucher.CurrencyUnitID = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
                        objVoucher.CurrencyExchange = Convert.ToDecimal(txtExchangeRate.EditValue);
                        objVoucher.TotalMoney = decTotalAmount;
                        //objVoucher.OriginateStoreID = SystemConfig.intDefaultStoreID;
                        objVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                        objVoucher.VoucherStoreID = intInputStoreID;
                        objVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objVoucher.CashierUser = SystemConfig.objSessionUser.UserName;
                        objVoucher.VoucherDate = dtSettlementDate.Value;
                        objVoucher.InvoiceDate = dtInvoiceDate.DateTime;
                        objVoucher.VoucherTypeID = Convert.ToInt32(cboVoucherType.SelectedValue);

                        objVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                        objVoucher.CustomerName = txtCustomerName.Text;
                        objVoucher.CustomerAddress = txtAddress.Text;
                        objVoucher.CustomerPhone = txtPhone.Text;
                        objVoucher.CustomerTaxID = txtTaxID.Text;
                        objVoucher.CustomerEmail = string.Empty;
                        objVoucher.CustomerIDCard = txtCustomerIDCard.Text;
                        //objVoucher.IdCardIssueDate = dtIDCardIssueDate.Value;
                        //objVoucher.IdCardIssuePlace = txtIDCardIssuePlace.Text;

                        objVoucher.ProvinceID = 1;
                        objVoucher.DistrictID = 1;
                        objVoucher.Gender = true;
                        objVoucher.AgeRangeID = 1;

                        objVoucher.TotalLiquidate = Convert.ToDecimal(txtTotalLiquidate.EditValue);
                        objVoucher.Debt = Convert.ToDecimal(txtDebt.EditValue);
                        objVoucher.TotalAdvance = Convert.ToDecimal("0");
                        objVoucher.VAT = 10;
                        objVoucher.OrderID = txtOrderID.Text.Trim();

                        //29/11/2016: Thiên bổ sung nội dung phiếu chi
                        objVoucher.Content = txtContent.Text;

                        //Thông tin chi tiết Phiếu Chi
                        decimal decCashVND = 0;
                        decimal decCashUSD = 0;
                        decimal decCashUSDExchange = 0;
                        decimal decMoneyCard = 0;
                        decimal decCardSpend = 0;

                        if (!Convert.IsDBNull(txtCashVND.Text) && txtCashVND.Text.Trim() != string.Empty)
                            decCashVND = Convert.ToDecimal(txtCashVND.EditValue);
                        decCashUSD = Convert.ToDecimal(txtCashUSD.EditValue);
                        decCashUSDExchange = Convert.ToDecimal(txtCashUSDExchange.EditValue);
                        decMoneyCard = Convert.ToDecimal(txtPaymentCard.EditValue);
                        decCardSpend = Convert.ToDecimal(txtCardSpend.EditValue);

                        List<PLC.Input.WSInputVoucher.VoucherDetail> VoucherDetailList = new List<PLC.Input.WSInputVoucher.VoucherDetail>();
                        if (((decCashVND > 0) || (decCashUSDExchange > 0) || (decMoneyCard > 0)))
                        {
                            PLC.Input.WSInputVoucher.VoucherDetail objVoucherDetail = new PLC.Input.WSInputVoucher.VoucherDetail();
                            objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDetail.VNDCash = decCashVND;
                            objVoucherDetail.VoucherDate = objVoucher.VoucherDate;

                            objVoucherDetail.ForeignCash = decCashUSD;
                            objVoucherDetail.ForeignCashExchange = decCashUSDExchange;
                            objVoucherDetail.PaymentCardAmount = decMoneyCard;
                            objVoucherDetail.PaymentCardSpend = decCardSpend;
                            objVoucherDetail.PaymentCardID = Convert.ToInt32(cboPaymentCard.SelectedValue);
                            objVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                            objVoucherDetail.VoucherStoreID = intInputStoreID;
                            VoucherDetailList.Add(objVoucherDetail);
                        }
                        objVoucher.VoucherDetailList = VoucherDetailList.ToArray();
                        objInputVoucher.Voucher = objVoucher;
                    }
                    objInputVoucher.IsInputFromOrder = bolIsInputFromOrder;
                    objInputVoucher.IsInputFromRetailInputPrice = bolIsInputFromRetailInput;
                    objInputVoucher.RetailInputPriceID = RetailInputPriceID;
                    //GetNotifyUser
                    GetListNotification_UserList(objInputType.InputTypeID, 1, objInputVoucher.InputStoreID, 2);
                    GetListNotification_UserList(objInputType.InputTypeID, 1, objInputVoucher.CreatedStoreID, 2);
                    objInputVoucher.UserNotifyData = dtbNotification_User;
                    if (!objInputVoucher.IsInputFromRetailInputPrice)
                    {
                        foreach (var item in objInputVoucher.InputVoucherDetailList)
                        {
                            if (item.InputVoucherDetailIMEIList != null)
                            {
                                item.dtbInputVoucherDetailIMEIDTB = CreateInputVoucherDetailIMEIDTB(item.InputVoucherDetailIMEIList.ToList());
                            }
                        }
                    }

                    if (bolIsInputFromRetailInput)
                    {
                        DataTable dtStoreTemp = Library.AppCore.DataSource.GetDataSource.GetStore();
                        DataTable dtBranchTemp = Library.AppCore.DataSource.GetDataSource.GetBranch();

                        DataRow[] lstStoreInput = dtStoreTemp.Select(string.Format("STOREID = {0}", intInputStoreID));
                        if (lstStoreInput != null && lstStoreInput.Length > 0)
                        {
                            DataRow[] lstBranchSelect = dtBranchTemp.Select(string.Format("BRANCHID = {0} AND ISCENTERBRANCH = 0", lstStoreInput[0]["BRANCHID"].ToString()));
                            if (lstBranchSelect != null && lstBranchSelect.Length > 0)
                            {
                                objInputVoucher.InputStoreID = objInputType.StoreCenterID;
                                if (objInputVoucher.InputVoucherDetailList != null)
                                {
                                    for (int i = 0; i < objInputVoucher.InputVoucherDetailList.Count(); i++)
                                    {
                                        objInputVoucher.InputVoucherDetailList[i].InputStoreID = objInputType.StoreCenterID;
                                    }
                                }

                                objInputVoucher.AutoStoreChangeToStoreID = intInputStoreID;//Dùng tạm AutoStoreChangeToStoreID để lưu siêu thị
                                if (objVoucher != null)
                                {
                                    objVoucher.VoucherStoreID = objInputType.StoreCenterID;
                                }
                            }
                        }
                    }

                    string strInputVoucherID = new PLC.Input.PLCInputVoucher().Insert(objInputVoucher);
                    if (strInputVoucherID != string.Empty)
                    {
                        strMes = "Tạo phiếu nhập thành công";
                        objInputVoucher.InputVoucherID = strInputVoucherID;
                        bolComplete = true;
                        //if (chkCreateAutoInvoice.Checked && objInputType.InvoiceTransTypeID > 0 && !string.IsNullOrWhiteSpace(objInputVoucher.InputVoucherID))
                        //{
                        //    object[] objsKeyword = new object[]
                        //    {
                        //        "@INVOICETYPEID", "1,2",
                        //        "@STOREID", "",
                        //        "@MAINGROUPID", "",
                        //        "@INPUTTYPEID","",
                        //        "@FROMDATE", dtInputDate.Value,
                        //        "@TODATE", dtInputDate.Value,
                        //        "@KEYWORDS", objInputVoucher.InputVoucherID,
                        //        "@SEARCHBY", 2
                        //    };
                        //    dtbDataGridResult = null;
                        //    new ERP.PrintVAT.PLC.PPrintVAT.PLCPrintVAT().SearchDataProductInvoice(ref dtbDataGridResult, objsKeyword);
                        //    if (dtbDataGridResult != null && dtbDataGridResult.Rows.Count > 0)
                        //    {
                        //        DataView dv = dtbDataGridResult.DefaultView;
                        //        dv.Sort = "ORDERID";
                        //        DataTable sortedDT = dv.ToTable();
                        //        int intCustomerID = 0;
                        //        string strInvoiceNo = txtInVoiceID.Text.Trim();
                        //        string strInvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                        //        string strDenominator = txtDenominator.Text.Trim();
                        //        DateTime dtpInvoiceDate = dtInvoiceDate.DateTime;
                        //        int.TryParse(txtCustomerID.Text, out intCustomerID);
                        //        int intCurrencyUnit = 0;
                        //        int.TryParse(cboCurrencyUnit.SelectedValue.ToString(), out intCurrencyUnit);
                        //        ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(intCustomerID);
                        //        ERP.PrintVAT.DUI.InputPrintVAT.frmInvoiceVATNotInvoice frm = new ERP.PrintVAT.DUI.InputPrintVAT.frmInvoiceVATNotInvoice(strInvoiceNo, strInvoiceSymbol, strDenominator, dtpInvoiceDate, intCurrencyUnit);
                        //        frm.InvoiceTransactionTypeIDSelect = objInputType.InvoiceTransTypeID;
                        //        frm.PayableTypeIDSelect = Convert.ToInt32(cboPayableType.SelectedValue);
                        //        frm.StoreIDSelect = cboStoreSearch.StoreID;
                        //        frm.DueDate = dtPayableTime.Value;
                        //        frm.InvoiceType = 8;
                        //        frm.IsGetDataCustomer(null);
                        //        if (objCustomer != null)
                        //        {
                        //            objCustomer.TaxCustomerName = txtCustomerName.Text;
                        //            objCustomer.TaxCustomerAddress = txtAddress.Text;
                        //            objCustomer.CustomerTaxID = txtTaxID.Text;
                        //            objCustomer.CustomerPhone = txtPhone.Text;
                        //            frm.SetCustomer(intCustomerID, objCustomer);
                        //        }
                        //        else
                        //        {
                        //            frm.SetCustomer(intCustomerID);
                        //        }
                        //        frm.InitializeDataProductService(sortedDT, true);
                        //        frm.InitializeDataDataCustomer(sortedDT, false);
                        //        frm.ShowDialog();
                        //        if (frm.bolRefresh)
                        //        {
                        //        }
                        //    }
                        //}
                        //if (chkCreateAutoInvoice.Checked && objInputType.InvoiceTransTypeID > 0 && !string.IsNullOrWhiteSpace(objInputVoucher.InputVoucherID) && objInputType != null)
                        //{
                        //    ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice objVAT_PInvoiceMain = new PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice();
                        //    ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objInputVoucher1 = new PLC.Input.PLCInputVoucher().LoadInfo(strInputVoucherID);
                        //    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        //    {
                        //        //Library.AppCore.Forms.frmWaitDialog.Close();
                        //        bolIsFinish = true;
                        //        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        //        return false;
                        //    }
                        //    CreateInputVATInvoice(this, ref objVAT_PInvoiceMain, objInputVoucher1, cboStoreSearch.StoreID);
                        //    if (objVAT_PInvoiceMain == null)
                        //    {
                        //        //Library.AppCore.Forms.frmWaitDialog.Close();
                        //        bolIsFinish = true;
                        //        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi tạo hóa đơn từ phiếu nhập!");
                        //    }
                        //}
                    }
                    else
                    {
                        //Library.AppCore.Forms.frmWaitDialog.Close();
                        bolIsFinish = true;
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Tạo hóa đơn nhập hàng không thành công!");
                        return false;
                    }

                }
                else//Cập nhật
                {

                    objInputVoucher.InVoiceID = txtInVoiceID.Text;
                    objInputVoucher.InVoiceSymbol = txtInvoiceSymbol.Text;
                    objInputVoucher.Denominator = txtDenominator.Text;
                    objInputVoucher.Content = txtInputContent.Text;
                    objInputVoucher.Note = txtNote.Text.Trim();
                    objInputVoucher.OrderID = txtOrderID.Text;
                    objInputVoucher.CustomerName = txtCustomerName.Text;
                    objInputVoucher.CustomerPhone = txtPhone.Text;
                    objInputVoucher.CustomerTaxID = txtTaxID.Text;
                    objInputVoucher.CustomerAddress = txtAddress.Text;
                    objInputVoucher.CustomerIDCard = txtCustomerIDCard.Text;
                    objInputVoucher.UpdatedUser = SystemConfig.objSessionUser.UserName;
                    objInputVoucher.IdCardIssueDate = dtIDCardIssueDate.Value;
                    objInputVoucher.IdCardIssuePlace = txtIDCardIssuePlace.Text;
                    objInputVoucher.InVoiceDate = dtInvoiceDate.DateTime;
                    objInputVoucher.PayableDate = dtPayableTime.Value;
                    objInputVoucher.IsAutoCreateInvoice = chkCreateAutoInvoice.Checked;
                    if (chkIsCheckRealInput.Enabled || bolIsCheckRealInput)
                    {
                        objInputVoucher.IsCheckRealInput = chkIsCheckRealInput.Checked;
                        if (!(txtCheckRealInputNote.Text == txtCheckRealInputNote.Properties.NullText))
                            objInputVoucher.CheckRealInputNote = txtCheckRealInputNote.Text;
                        objInputVoucher.CheckRealInputUser = SystemConfig.objSessionUser.UserName;
                        objInputVoucher.CheckRealInputTime = Library.AppCore.Globals.GetServerDateTime();
                    }
                    objInputVoucher.InputVoucher_AttachmnetList = InputVoucher_AttachmentList.ToArray();
                    if (InputVoucher_AttachmentDelList != null && InputVoucher_AttachmentDelList.Count > 0)
                        objInputVoucher.InputVoucher_AttachmnetDelList = InputVoucher_AttachmentDelList.ToArray();
                    //GetNotifyUser
                    objInputType = new MasterData.PLC.MD.PLCInputType().LoadInfo(cboInputTypeSearch.InputTypeID);
                    if (objInputType == null)
                    {
                        bolIsFinish = true;
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Không lấy được thông tin hình thức nhập");
                        return false;
                    }
                    GetListNotification_UserList(objInputType.InputTypeID, 2, objInputVoucher.InputStoreID, 2);
                    GetListNotification_UserList(objInputType.InputTypeID, 2, objInputVoucher.CreatedStoreID, 2);
                    objInputVoucher.UserNotifyData = dtbNotification_User;
                    objInputVoucher.DeliveryNoteDate = Convert.ToDateTime(dtmDeliveryNoteDate.EditValue);
                    if (new PLC.Input.PLCInputVoucher().UpdateInputVoucher(objInputVoucher))
                    {
                        strMes = "Cập nhật phiếu nhập thành công";
                        DataTable dtbInvoiceExists = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_INVOICE_SELBYINPUTID", new object[] { "@InputVoucherID", objInputVoucher.InputVoucherID });
                        //var objResult = new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataTable(ref dtbInvoiceExists,"VAT_INVOICE_SELBYINPUTID" , new object[] { "@InputVoucherID", objInputVoucher.InputVoucherID })
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->UpdateData");
                        }
                        if (dtbInvoiceExists != null && dtbInvoiceExists.Rows.Count > 0)
                        {
                            if (chkCreateAutoInvoice.Checked && objInputType != null && objInputType.InvoiceTransTypeID > 0 && !string.IsNullOrWhiteSpace(objInputVoucher.InputVoucherID) && bolPermission_PM_InputVoucher_EditInvoiceInfo)
                            {
                                ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice objVAT_PInvoiceMain = new PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice();
                                DataTable dtbProduct = null;
                                DataTable dtbAttachment = null;
                                new ERP.PrintVAT.PLC.PPrintVAT.PLCPrintVAT().LoadInfo(ref objVAT_PInvoiceMain, ref dtbProduct, ref dtbAttachment, dtbInvoiceExists.Rows[0]["VATPINVOICEID"].ToString());
                                if (objVAT_PInvoiceMain != null
                                    && (txtInVoiceID.Text.Trim() != strInvoiceIDOld || txtInvoiceSymbol.Text.Trim() != strInvoiceSymbolOld
                                    || txtDenominator.Text.Trim() != strDenominatorOld || dteInvoiceDateOld.Date != dtInvoiceDate.DateTime.Date))
                                {
                                    objVAT_PInvoiceMain.InvoiceNO = Convert.ToInt32(txtInVoiceID.Text.Trim());
                                    objVAT_PInvoiceMain.Denominator = txtDenominator.Text.Trim();
                                    objVAT_PInvoiceMain.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                                    objVAT_PInvoiceMain.InvoiceDate = Convert.ToDateTime(dtInvoiceDate.EditValue);
                                    new ERP.PrintVAT.PLC.PPrintVAT.PLCPrintVAT().Update_VAT_Invoice(objVAT_PInvoiceMain);
                                }
                            }
                        }
                        //else
                        //{
                        //    if (chkCreateAutoInvoice.Checked && objInputType != null && objInputType.InvoiceTransTypeID > 0 && !string.IsNullOrWhiteSpace(objInputVoucher.InputVoucherID) && bolPermission_PM_InputVoucher_EditInvoiceInfo)
                        //    {
                        //        ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice objVAT_PInvoiceMain = new PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice();
                        //        ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objInputVoucher1 = new PLC.Input.PLCInputVoucher().LoadInfo(objInputVoucher.InputVoucherID);
                        //        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        //        {
                        //            //Library.AppCore.Forms.frmWaitDialog.Close();
                        //            bolIsFinish = true;
                        //            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        //            return false;
                        //        }
                        //        CreateInputVATInvoice(this, ref objVAT_PInvoiceMain, objInputVoucher1, cboStoreSearch.StoreID);
                        //        if (objVAT_PInvoiceMain == null)
                        //        {
                        //            //Library.AppCore.Forms.frmWaitDialog.Close();
                        //            bolIsFinish = true;
                        //            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Lỗi tạo hóa đơn từ phiếu nhập!");
                        //        }
                        //    }
                        //}
                    }
                }
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    bolIsFinish = true;
                    //Library.AppCore.Forms.frmWaitDialog.Close();
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }

                txtInputVoucherID.Text = objInputVoucher.InputVoucherID;
                bolIsFinish = true;
                //Library.AppCore.Forms.frmWaitDialog.Close();
                MessageBox.Show(this, strMes, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                int intPrintRealInput = 0;
                try
                {
                    intPrintRealInput = Library.AppCore.AppConfig.GetIntConfigValue("PM_INPUTVOUCHER_PRINTREALINPUT");
                }
                catch
                {

                    intPrintRealInput = 0;
                }
                if (intPrintRealInput > 0)
                {
                    if (!isEdit && objInputType.IsAutoCheckRealInput)
                        PrintVoucher(objInputVoucher.InputVoucherID);
                    if (isEdit && !bolChekRealInputPrint && chkIsCheckRealInput.Checked)
                        PrintVoucher(objInputVoucher.InputVoucherID);
                }
                else
                {
                    if (!isEdit)
                        PrintVoucher(objInputVoucher.InputVoucherID);
                }


            }
            catch (Exception objExce)
            {
                //Library.AppCore.Forms.frmLoading.Close();
                bolIsFinish = true;
                SystemErrorWS.Insert("Lỗi khi thêm mới một hóa đơn nhập hàng", objExce, PLCInventory_Globals.ModuleName);
                btnUpdate.Enabled = true;
                return false;
            }
            bolIsReloadManager = true;
            this.Close();
            return true;
        }

        /// <summary>
        /// In hoa don
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        private void PrintVoucher(string strInputVoucherID)
        {
            try
            {
                Reports.Input.frmReportView frmReport = new Reports.Input.frmReportView();
                frmReport.InputVoucherID = strInputVoucherID;
                frmReport.bolIsPriceHide = false;
                frmReport.ShowDialog();
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi in phiếu nhập hàng", objExce, PLCInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi in phiếu nhập hàng " + objExce.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Tính tổng tiền sau thuế, trước thuế
        /// </summary>
        /// <param name="decTotalAmountBF"></param>
        /// <param name="decTotalVAT"></param>
        private void GetTotal(ref decimal decTotalAmountBF, ref decimal decTotalVAT)
        {
            decTotalAmountBF = 0;
            decTotalVAT = 0;
            foreach (var item in InputVoucherDetailList)
            {
                decTotalAmountBF += Convert.ToDecimal(item.Quantity) * Convert.ToDecimal(item.InputPrice);
                decTotalVAT += Convert.ToDecimal(item.Quantity) * Convert.ToDecimal(item.InputPrice)
                    * Convert.ToDecimal(item.VAT)
                    * Convert.ToDecimal(item.VATPercent) / Convert.ToDecimal(10000);
            }
        }

        /// <summary>
        /// Đính kèm tập tin
        /// </summary>
        private bool SaveAttachment()
        {
            //Tạo trạng thái chờ trong khi đính kèm tập tin
            this.Cursor = Cursors.WaitCursor;
            this.Refresh();
            try
            {
                if (InputVoucher_AttachmentList != null && InputVoucher_AttachmentList.Count > 0)
                {
                    var CheckIsNew = from o in InputVoucher_AttachmentList
                                     where o.IsNew == true
                                     select o;
                    if (CheckIsNew.Count() <= 0)
                    {
                        this.Cursor = Cursors.Default;
                        return true;
                    }

                    //List<PLC.Input.WSInputVoucher.InputVoucher_Attachment> objListAfterSave= new PLC.Input.PLCInputVoucher().GetListAttachmentByInputVoucherID(objInputVoucher.InputVoucherID);
                    //if(objListAfterSave==null)
                    //{
                    //    this.Cursor = Cursors.Default;
                    //    return true;
                    //}

                    //Library.AppCore.FTP.FileAttachment.FTPSaveFolder = dtbServerDate.ToString("yyyy_MM") + "/" + objInputVoucher.InputVoucherID;
                    //Library.AppCore.FTP.FileAttachment objFileAttachment = new Library.AppCore.FTP.FileAttachment();
                    //objFileAttachment.CreateFTPFolders(Library.AppCore.FTP.FileAttachment.FTPSaveFolder);
                    foreach (PLC.Input.WSInputVoucher.InputVoucher_Attachment obj in CheckIsNew)
                    {
                        string strFilePath = string.Empty;
                        string strFileID = string.Empty;
                        string strMess = ERP.FMS.DUI.DUIFileManager.UploadFile(this, "FMSAplication_ProERP_InputVoucherAttachment", obj.LocalFilePath, ref strFileID, ref strFilePath);
                        if (strMess.Trim() != string.Empty)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                        obj.FilePath = strFilePath;
                        obj.FileID = strFileID;

                    }
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx, DUIInventory_Globals.ModuleName);
                this.Cursor = Cursors.Default;
                return false;
            }
            //Bỏ trạng thái chờ sau khi đính kèm tập tin			
            this.Cursor = Cursors.Default;
            this.Refresh();
            return true;
        }


        private void GetSumTotalMoney()
        {
            decTotalAmount = 0;
            decTotalAmount = Math.Round(InputVoucherDetailList.Sum(item => item.Amount), 4);
            txtTotalMoney.EditValue = decTotalAmount;
        }

        /// <summary>
        /// Nhập danh sách IMEI
        /// </summary>
        /// <param name="bolIsDoubleClick"></param>
        private void InputIMEI()
        {
            try
            {
                if (grvInputVoucherDetail.FocusedRowHandle < 0 || InputVoucherDetailList == null || InputVoucherDetailList.Count == 0)
                    return;
                PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetail)grvInputVoucherDetail.GetRow(grvInputVoucherDetail.FocusedRowHandle);
                if (objInputVoucherDetail == null || !objInputVoucherDetail.IsRequestIMEI)
                    return;
                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = null;
                new ERP.MasterData.PLC.MD.PLCProduct().LoadInfo(ref objProduct, objInputVoucherDetail.ProductID);
                if (objProduct == null)
                    return;
                List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
                if (InputVoucherDetailList != null && InputVoucherDetailList.Count > 0)
                {
                    foreach (var item in InputVoucherDetailList)
                    {
                        if (objInputVoucherDetail.ProductID.Trim() != item.ProductID.Trim() && item.InputVoucherDetailIMEIList != null && item.InputVoucherDetailIMEIList.Count() > 0)
                            objAllIMEIList.AddRange(item.InputVoucherDetailIMEIList);
                    }
                }
                bool bolIsRequirePinCode = objProduct.IsRequirePINCode;
                frmInputVoucher_ValidBarcode frmInputVoucher_ValidBarcode1 = new frmInputVoucher_ValidBarcode();
                frmInputVoucher_ValidBarcode1.InputStoreID = cboStoreSearch.StoreID;

                if (objInputVoucherDetail.InputVoucherDetailIMEIList != null)
                    frmInputVoucher_ValidBarcode1.InputVoucherDetailIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>(objInputVoucherDetail.InputVoucherDetailIMEIList);
                frmInputVoucher_ValidBarcode1.ProductID = objInputVoucherDetail.ProductID;
                frmInputVoucher_ValidBarcode1.ProductNameText = objInputVoucherDetail.ProductName;
                frmInputVoucher_ValidBarcode1.IMEIFormatEXP = objInputVoucherDetail.IMEIFormatEXP;
                frmInputVoucher_ValidBarcode1.Price = objInputVoucherDetail.InputPrice;
                frmInputVoucher_ValidBarcode1.OrderQuantity = objInputVoucherDetail.OrderQuantity;
                frmInputVoucher_ValidBarcode1.IsAutoCreateIMEI = objInputVoucherDetail.IsAutoCreateIMEI;
                frmInputVoucher_ValidBarcode1.IsRequirePincode = bolIsRequirePinCode;//True: Sản phẩm yêu cầu nhập PinCode
                frmInputVoucher_ValidBarcode1.IsInputFromRetailInputPrice = IsInputFromRetailInput;//Nhập từ máy cũ
                frmInputVoucher_ValidBarcode1.InputVoucherDetailList = InputVoucherDetailList;
                frmInputVoucher_ValidBarcode1.AllIMEIList = objAllIMEIList;
                if (frmInputVoucher_ValidBarcode1.ShowDialog() == DialogResult.OK)
                {
                    decimal OldQuantity = objInputVoucherDetail.Quantity;
                    objInputVoucherDetail.InputVoucherDetailIMEIList = frmInputVoucher_ValidBarcode1.InputVoucherDetailIMEIList.ToArray();
                    objInputVoucherDetail.Quantity = objInputVoucherDetail.InputVoucherDetailIMEIList.Count();
                    //Ân thêm check số lượng cần nhập 03/10/2017
                    if (bolIsInputFromOrder)
                    {
                        objInputVoucherDetail.QuantityToInput = objInputVoucherDetail.QuantityToInput - (objInputVoucherDetail.Quantity - OldQuantity);
                        if (objInputVoucherDetail.Quantity > 0) objInputVoucherDetail.IsCheckQuantity = true;
                    }

                    //if (bolIsInputFromOrder)
                    //{
                    //    objInputVoucherDetail.TaxedFee = objInputVoucherDetail.Quantity / objInputVoucherDetail.TotalOrderQuantity * objInputVoucherDetail.OrderTaxedFee;
                    //    objInputVoucherDetail.PurchaseFee = objInputVoucherDetail.Quantity / objInputVoucherDetail.TotalOrderQuantity * objInputVoucherDetail.OrderPurchaseFee;
                    //}
                    objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);
                    grdInputVoucherDetail.RefreshDataSource();
                    GetSumTotalMoney();
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nhập imei!", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nhập imei!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Nhập danh sách IMEI nhiều sản phẩm cùng lúc
        /// </summary>
        /// <param name="bolIsDoubleClick"></param>
        private void InputIMEIByProduct()
        {
            try
            {
                if (InputVoucherDetailList == null || InputVoucherDetailList.Count == 0)
                    return;

                // Lấy danh sách sản phẩm có yêu cầu nhập Imei
                var objInputVoucherDetailList = from r in InputVoucherDetailList
                                                where r.IsRequestIMEI == true
                                                select r;

                if (objInputVoucherDetailList.Count() == 0)
                    return;

                var rlsProductID = from r in objInputVoucherDetailList
                                   select r.ProductID;

                string strProductIDList = string.Join(",", rlsProductID.ToArray());
                object[] objKeywords = new object[]
                {
                    "@PRODUCTIDLIST",strProductIDList
                };

                // Lấy thông tin yêu cầu nhập PinCode của từng sản phẩm nhập
                DataTable dtbPincode = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("GET_PRODUCT_ISREQUIREPINCODE", objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->InputIMEIByProduct");
                }
                if (dtbPincode != null)
                {
                    foreach (var item in objInputVoucherDetailList)
                    {
                        DataRow[] row = dtbPincode.Select("PRODUCTID = '" + item.ProductID.Trim() + "'");
                        if (row.Length > 0)
                            item.IsRequirePinCode = Convert.ToBoolean(row[0]["ISREQUIREPINCODE"]);
                    }
                }

                List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIList = null;
                objAllIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
                foreach (var item in InputVoucherDetailList)
                {
                    if (item.InputVoucherDetailIMEIList != null && item.InputVoucherDetailIMEIList.Count() > 0)
                        objAllIMEIList.AddRange(item.InputVoucherDetailIMEIList);
                }

                frmInputVoucher_ValidBarcodeByProduct frm = new frmInputVoucher_ValidBarcodeByProduct();
                frm.InputStoreID = cboStoreSearch.StoreID;

                if (objAllIMEIList != null)
                    frm.InputVoucherDetailIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>(objAllIMEIList);

                //frm.IMEIFormatEXP = objInputVoucherDetail.IMEIFormatEXP;
                //frm.IsAutoCreateIMEI = objInputVoucherDetail.IsAutoCreateIMEI;

                frm.OrderQuantity = objInputVoucherDetailList.Sum(r => r.OrderQuantity);
                frm.IsInputFromRetailInputPrice = false;//Nhập từ máy cũ
                frm.InputVoucherDetailList = InputVoucherDetailList.ToList();
                frm.AllIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>(objAllIMEIList);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIResultList = null;
                    objAllIMEIResultList = frm.InputVoucherDetailIMEIList;
                    foreach (var objInputVoucherDetail in objInputVoucherDetailList)
                    {
                        objInputVoucherDetail.InputVoucherDetailIMEIList = objAllIMEIResultList.Where(r => r.ProductID == objInputVoucherDetail.ProductID).ToArray();
                        decimal OldQuantity = objInputVoucherDetail.Quantity;
                        objInputVoucherDetail.Quantity = objInputVoucherDetail.InputVoucherDetailIMEIList.Count();
                        objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);
                        //Ân thêm check số lượng cần nhập 03/10/2017
                        if (bolIsInputFromOrder)
                        {
                            objInputVoucherDetail.QuantityToInput = objInputVoucherDetail.QuantityToInput - (objInputVoucherDetail.Quantity - OldQuantity);
                            if (objInputVoucherDetail.Quantity > 0) objInputVoucherDetail.IsCheckQuantity = true;
                        }
                    }
                    grdInputVoucherDetail.RefreshDataSource();
                    GetSumTotalMoney();
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nhập imei theo sản phẩm!", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nhập imei theo sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Lấy phí cà thể
        /// </summary>
        /// <returns></returns>
        private Decimal GetCardSpend()
        {
            Decimal decResult = 0;
            try
            {
                Decimal decSpendRate = 0;
                decSpendRate = ERP.MasterData.PLC.MD.PLCPaymentCard.GetSpendRate(Convert.ToInt32(cboPaymentCard.SelectedValue));
                decResult = Math.Round((decSpendRate / 100) * Convert.ToDecimal(txtPaymentCard.EditValue), 4);
                decResult = Math.Round(decResult / 1000, 4) * 1000;
            }
            catch
            {
                return 0;
            }
            return decResult;
        }
        #endregion

        #region Events
        private void frmInputVoucher_Load(object sender, EventArgs e)
        {
            bolIsReloadManager = false;
            if (!isEdit && !bolIsInputFromOrder)//tạo mới phiếu nhập mới chạy
                LoadControl();
            if (IsEdit)
            {
                if (!string.IsNullOrEmpty(lblMessage.Text))
                {
                    btnUpdate.Enabled = false;
                }
                if (ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.INPUTVOUCHER,
                    objInputVoucher.InputDate.Value, objInputVoucher.InputStoreID))
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tháng " + objInputVoucher.InputDate.Value.Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa");
                    btnUpdate.Enabled = false;
                    btnDelete.Enabled = false;
                }
                if (evtLoadByProductIMEIHistory != null)
                {
                    evtLoadByProductIMEIHistory(this, e);
                    if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
                    {
                        Hashtable _paramFromProductIMEIHistory = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                        bool bolNotVisibleCustomer = Convert.ToBoolean(_paramFromProductIMEIHistory["NotVisibleCustomer"]);
                        bool bolNotUpdateInput = Convert.ToBoolean(_paramFromProductIMEIHistory["NotUpdateInput"]);
                        if (bolNotVisibleCustomer && bolNotUpdateInput)
                        {
                            tabControl1.TabPages.Remove(tabCustomer);
                            btnUpdate.Enabled = false;
                            if (btnDelete.Enabled)
                                btnDelete.Visible = false;
                        }
                    }
                }
                if (objInputVoucher != null && objInputVoucher.IsAutoCreateInvoice)
                {
                    //txtInVoiceID.Enabled = false;
                    //txtInvoiceSymbol.Enabled = false;
                    //txtDenominator.Enabled = false;
                    //dtInvoiceDate.Enabled = false;
                    chkCreateAutoInvoice.Enabled = false;
                    chkCreateAutoInvoice.Checked = objInputVoucher.IsAutoCreateInvoice;
                    dtPayableTime.Enabled = false;
                    txtProtectPriceDiscount.Properties.ReadOnly = true;
                    txtProtectPriceDiscount.BackColor = SystemColors.Info;
                }

                if (objInputVoucher != null && objInputVoucher.IsInputFromRetailInputPrice)
                    grvInputVoucherDetail.OptionsBehavior.Editable = false;

                grvInputVoucherDetail.Columns["ProductStatusName"].Visible = true;
            }
            else
            {
                grvInputVoucherDetail.Columns["ProductStatusName"].Visible = false;
            }

            if (bolIsInputFromRetailInput)
                grvInputVoucherDetail.OptionsBehavior.Editable = false;
        }

        public void LoadControl()
        {
            bolPermissionRealInput = SystemConfig.objSessionUser.IsPermission(strPerrmissionRealInput);
            txtCardSpend.Text = null;
            txtCashUSD.Text = null;
            txtCashUSDExchange.Text = null;
            txtCashVND.Text = null;
            txtDiscount.Text = null;
            txtProtectPriceDiscount.Text = null;
            txtPaymentCard.Text = null;
            txtTotalLiquidate.Text = null;
            txtTotalMoney.Text = null;
            txtDebt.Text = null;
            txtDiscountInput.Text = null;
            lblMessage.Text = string.Empty;
            dtInputDate.Value = Globals.GetServerDateTime();
            dtInvoiceDate.EditValue = dtInputDate.Value;
            dtmDeliveryNoteDate.EditValue = Globals.GetServerDateTime();

            LoadCombobox();
            if (!isEdit)
            {
                cboDiscountReason_SelectionChangeCommitted(null, null);
                btnRealInputConfirm.Visible = false;
                chkIsCheckRealInput.Visible = false;
                txtCheckRealInputNote.Visible = false;
                btnDelete.Visible = false;
                txtInputVoucherID.Text = new PLC.Input.PLCInputVoucher().GetInputVoucherNewID(SystemConfig.intDefaultStoreID);
                txtVoucherID.Text = new PLC.Input.PLCInputVoucher().GetVoucherNewID(SystemConfig.intDefaultStoreID);
                if (bolIsInputFromOrder)
                {
                    // Nạp dữ liệu Default từ Order
                    isLoad = true;
                    this.Text = "Nhập hàng từ đơn hàng " + strOrderID;
                    SetDefaultValues_Order();
                    txtExchangeRate.Enabled = false;

                    //Hiếu thêm Nhập Nội dung, Ghi chú từ phiếu nhập
                    txtInputContent.Text = NoteFromOrder;
                    txtNote.Text = ContentFromOrder;

                    colCheck.Visible = true;
                    colCheck.VisibleIndex = 0;
                    colQuantityToInput.Visible = true;
                    colQuantityToInput.VisibleIndex = colQuantity.VisibleIndex + 1;
                    colQuantityRemain.Visible = true;
                    colQuantityRemain.VisibleIndex = colNKTBH.VisibleIndex + 1;
                    isLoad = false;

                }
                else
                    if (bolIsInputFromRetailInput)
                    {
                        this.Text = "Nhập hàng từ định giá máy cũ";
                        txtOrderID.BackColor = SystemColors.Window;
                        if (!SetDefaultValues_RetailInput())
                        {
                            this.Close();
                            return;
                        }

                    }
                    else
                    {
                        txtOrderID.BackColor = SystemColors.Window;
                        LoadDefaultCustomerInfo();
                    }
                grdInputVoucherDetail.DataSource = InputVoucherDetailList;
                grdInputVoucherDetail.RefreshDataSource();
                grdAttachment.DataSource = InputVoucher_AttachmentList;
                grvAttachment.Columns[grvAttachment.Columns.Count - 1].Visible = false;
                grdAttachment.RefreshDataSource();
                this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                this.Top = 0;
                txtDiscountInput.BackColor = SystemColors.Window;
            }
            else
            {
                btnRealInputConfirm.Visible = true;
                chkIsCheckRealInput.Visible = true;
                txtCheckRealInputNote.Visible = true;
                btnDelete.Visible = true;
                LoadInputVoucher();
                btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_DELETE");
            }
            //Kiểm tra quyền
            CheckPermission();
        }
        /// <summary>
        /// Nạp thông tin từ đơn hàng
        /// </summary>
        private bool SetDefaultValues_RetailInput()
        {
            try
            {
                intStoreDefault = SystemConfig.intDefaultStoreID;
                DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();

                // < LÊ VAN ĐÔNG - BỔ SUNG NGÀY 07/06/2017
                // Lấy cấu hình: Loại kho nhập máy lẻ
                object obj = Library.AppCore.AppConfig.GetConfigValue("PM_INPUTVOUCHER_RETAILINPUTSTORETYPEID");
                DataRow[] rows;
                if (obj == null)
                {
                    // Nếu chưa cấu hình - thì để như cũ
                    // Lấy các kho nhập mà User đăng nhập có quyền nhập hàng và có Mã Loại kho = 5
                    rows = dtbStore.Select(string.Format("STORETYPEID=5 and (HOSTSTOREID={0} or StoreID={0})", intStoreDefault));
                }
                else
                {
                    // Nếu khai báo giá trị <= 0: Lấy tất cả các kho nhập mà User đăng nhập có quyền nhập hàng.
                    // Nếu khai báo giá trị > 0: Lấy các kho nhập mà User đăng nhập có quyền nhập hàng
                    // và có Mã loại kho = giá trị cấu hình.
                    int kq = 0;
                    Int32.TryParse(obj.ToString(), out kq);
                    if (kq > 0)
                        rows = dtbStore.Select(string.Format("STORETYPEID=" + kq + " and (HOSTSTOREID={0} or StoreID={0})", intStoreDefault));
                    else
                        rows = dtbStore.Select(string.Format("(HOSTSTOREID={0} or StoreID={0})", intStoreDefault));
                }
                // /> LE VAN DONG


                if (rows.Length > 0)
                    cboStoreSearch.SetValue(Convert.ToInt32(rows[0]["StoreID"]));
                else
                {
                    cboStoreSearch.SetValue(intStoreDefault);
                }
                if (InputTypeID > 0)
                {
                    cboInputTypeSearch.SetValue(InputTypeID);
                    if (cboInputTypeSearch.InputTypeID < 1)
                    {
                        ERP.MasterData.PLC.MD.PLCInputType objPLCInputType = new MasterData.PLC.MD.PLCInputType();
                        objInputType = objPLCInputType.LoadInfo(intInputTypeID);
                        if (objInputType != null)
                        {
                            MessageBox.Show(this, "Hình thức " + objInputType.InputTypeName + " chỉ được nhập qua đơn hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                            MessageBox.Show(this, "Thông tin hình thức nhập khai báo không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    cboInputTypeSearch_SelectionChangeCommitted(null, null);
                    cboInputTypeSearch.Enabled = false;
                }
                if (CustomerInfo != null && CustomerInfo.CustomerID > 0)
                {
                    txtCustomerID.Text = Convert.ToString(CustomerInfo.CustomerID);
                    txtCustomerID.Enabled = false;
                    lblCustomerID.Enabled = false;
                    txtCustomerName.Text = CustomerInfo.CustomerName;
                    txtAddress.Text = CustomerInfo.CustomerAddress;
                    txtCustomerIDCard.Text = CustomerInfo.CustomerIDCard;
                    txtTaxID.Text = CustomerInfo.CustomerTaxID;
                    txtPhone.Text = CustomerInfo.CustomerPhone;
                }
                dtInputDate.Value = dtbServerDate;
                dtInputDate.Enabled = false;
                //btnSearchProduct.Visible = false;
                btnSearchProduct.Enabled = false;
                txtBarcode.ReadOnly = true;
                txtBarcode.BackColor = SystemColors.Info;
                if (!InsertRetailInputProductList(dtbInputVoucherDetail_Order))
                {
                    MessageBox.Show(this, "Đơn hàng này đã được nhập hết!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                radIsNew.Checked = IsNewFromRetailInput;
                radOld.Checked = !IsNewFromRetailInput;
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nạp thông tin nhập định giá máy cũ");
                SystemErrorWS.Insert("Lỗi nạp thông tin nhập định giá máy cũ", objExec, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Nạp thông tin từ đơn hàng
        /// </summary>
        private void SetDefaultValues_Order()
        {
            if (intStoreDefault > 0)
            {
                cboStoreSearch.SetValue(intStoreDefault);
                if (cboStoreSearch.StoreID < 1)
                {
                    MessageBox.Show(this, "Bạn không có quyền nhập tại kho " + ERP.MasterData.PLC.MD.PLCStore.GetStoreName(intStoreDefault),
                        "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnUpdate.Enabled = false;
                }
                cboStoreSearch.Enabled = false;
            }
            if (InputTypeID > 0)
            {
                cboInputTypeSearch.SetValue(InputTypeID);
                cboInputTypeSearch_SelectionChangeCommitted(null, null);
                cboInputTypeSearch.Enabled = false;
            }
            if (intDefaultCustomerID > 0)
            {
                txtCustomerID.Enabled = false;
                lblCustomerID.Enabled = false;
                if (objCustomer_FromOrder == null)
                    LoadCustomerInfo(intDefaultCustomerID);
                else
                {
                    if (objCustomer_FromOrder != null)
                    {
                        txtCustomerID.Text = objCustomer_FromOrder.CustomerID.ToString();
                        txtCustomerName.Text = objCustomer_FromOrder.CustomerName;
                        txtAddress.Text = objCustomer_FromOrder.CustomerAddress;
                        txtCustomerIDCard.Text = objCustomer_FromOrder.CustomerIDCard;
                        txtTaxID.Text = objCustomer_FromOrder.CustomerTaxID;
                        txtPhone.Text = objCustomer_FromOrder.CustomerPhone;
                    }
                }
            }


            if (!string.IsNullOrEmpty(strOrderID.Trim()))
            {
                txtOrderID.Text = strOrderID;
                txtOrderID.ReadOnly = true;
            }
            if (intCurrencyID > 0)
            {
                decOldCurrencyExchange = decCurrencyExchange;
                cboCurrencyUnit.SelectedValue = intCurrencyID;
                cboCurrencyUnit.Enabled = false;
                txtExchangeRate.Text = decCurrencyExchange.ToString();
                cboCurrencyUnit_SelectionChangeCommitted(null, null);
            }
            dtInputDate.Value = dtbServerDate;
            dtInputDate.Enabled = false;
            //btnSearchProduct.Visible = false;
            btnSearchProduct.Enabled = false;
            txtBarcode.ReadOnly = true;
            txtBarcode.BackColor = SystemColors.Info;
            if (!InsertOrderProductList(dtbInputVoucherDetail_Order))
            {
                MessageBox.Show(this, "Đơn hàng này đã được nhập hết!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        /// <summary>
        /// Thêm danh sách sản phẩm từ đơn hàng vào lưới MỚI
        /// </summary>
        /// <param name="dtbOrderProductList"></param>
        /// <returns></returns>
        private bool InsertOrderProductList(DataTable dtbOrderProductList)
        {
            // Chỉ lấy những sản phẩm có SL đã nhập < SL thực  
            // Sort theo Orderindex
            DataRow[] arrRow = dtbOrderProductList.Select("[INPUTEDQUANTITY] < [QUANTITY]", "[ORDERINDEX]");
            // Nếu không còn sản phẩm chưa nhập hết thì thoát
            if (arrRow.Length < 1)
                return false;
            StringBuilder arrProductNotExist = new StringBuilder();
            StringBuilder arrMainGroupNotPermission = new StringBuilder();
            DataTable dtProductTemp = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
            dtWarrantyMonthByProduct = Library.AppCore.DataSource.GetDataSource.GetWarrantyMonthByProduct().Copy();
            // Tạo danh sách mã sản phẩm
            foreach (DataRow row in arrRow)
            {
                string strProductID = row["PRODUCTID"].ToString().Trim();
                string strOrderDetailID = row["ORDERDETAILID"].ToString().Trim();
                var rowProduct = dtProductTemp.AsEnumerable().Where(x => x.Field<string>("PRODUCTID").Trim() == strProductID).FirstOrDefault();
                if (rowProduct == null)
                {
                    if (arrProductNotExist.ToString().Length > 0)
                        arrProductNotExist.Append(",");
                    arrProductNotExist.Append(strProductID);
                    continue;
                }
                if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(Convert.ToInt32(rowProduct["MAINGROUPID"])))
                {
                    if (arrMainGroupNotPermission.ToString().Length > 0)
                        arrMainGroupNotPermission.Append(",");
                    arrMainGroupNotPermission.Append(rowProduct["MAINGROUPNAME"]);
                    continue;
                }
                decimal decPrice = Convert.ToDecimal(row["Price"]);
                decimal decOrderQuantity = Convert.ToDecimal(row["QUANTITY"]) - Convert.ToDecimal(row["INPUTEDQUANTITY"]);
                decimal decQuantity = 1;
                int intVAT = Convert.ToInt32(row["VAT"]);
                int intVATPercent = Convert.ToInt32(row["VATPercent"]);
                if (decQuantity > decOrderQuantity)
                    decQuantity = decOrderQuantity;
                //decimal decTaxtFee = decOrderQuantity / Convert.ToDecimal(row["QUANTITY"]) * Convert.ToDecimal(row["TAXEDFEE"]);
                //decimal decPurchaseFee = decOrderQuantity / Convert.ToDecimal(row["QUANTITY"]) * Convert.ToDecimal(row["PURCHASEFEE"]);
                decimal decTaxtFee = Convert.ToDecimal(row["TAXEDFEE"]);
                decimal decPurchaseFee = Convert.ToDecimal(row["PURCHASEFEE"]);
                decimal decTotalOrderQuantity = Convert.ToDecimal(row["QUANTITY"]);
                decimal decOrderTaxtFee = Convert.ToDecimal(row["TAXEDFEE"]);
                decimal decOrderPurchaseFee = Convert.ToDecimal(row["PURCHASEFEE"]);
                decimal decQuantityToInput = decTotalOrderQuantity - Convert.ToDecimal(row["INPUTEDQUANTITY"]);
                decimal decQuantityRemain = decTotalOrderQuantity - Convert.ToDecimal(row["INPUTEDQUANTITY"]);
                InsertProductToGrid(rowProduct, decQuantity, decOrderQuantity, decPrice, intVAT, intVATPercent, decTaxtFee, decPurchaseFee, decTotalOrderQuantity, decOrderTaxtFee, decOrderPurchaseFee, decQuantityToInput, decQuantityRemain, strOrderDetailID);
            }

            if (!string.IsNullOrEmpty(arrProductNotExist.ToString().Trim()))
            {
                MessageBox.Show(this, "Danh sách mã sản phẩm không tồn tại [" + arrProductNotExist.ToString().Trim() + "]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (!string.IsNullOrEmpty(arrMainGroupNotPermission.ToString().Trim()))
            {
                MessageBox.Show(this, "Bạn không có quyền nhập trên ngành hàng [" + arrMainGroupNotPermission.ToString().Trim() + "]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (InputVoucherDetailList == null || InputVoucherDetailList.Count <= 0)
                btnUpdate.Enabled = false;
            GetSumTotalMoney();
            return true;
        }

        /// <summary>
        /// Thêm danh sách sản phẩm cũ từ định giá máy cũ
        /// </summary>
        /// <param name="dtbOrderProductList"></param>
        /// <returns></returns>
        private bool InsertRetailInputProductList(DataTable dtbRetailInputProductList)
        {
            try
            {
                // Nếu không còn sản phẩm chưa nhập hết thì thoát
                if (dtbRetailInputProductList == null || dtbRetailInputProductList.Rows.Count <= 0)
                    return false;
                StringBuilder arrProductNotExist = new StringBuilder();
                StringBuilder arrMainGroupNotPermission = new StringBuilder();
                // Tạo danh sách mã sản phẩm
                foreach (DataRow row in dtbRetailInputProductList.Rows)
                {
                    string strProductID = row["PRODUCTID"].ToString().Trim();
                    string strIMEI = row["IMEI"].ToString().Trim();
                    ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                    if (objProduct == null)
                    {
                        if (arrProductNotExist.ToString().Length > 0)
                            arrProductNotExist.Append(",");
                        arrProductNotExist.Append(strProductID);
                        continue;
                    }
                    if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(objProduct.MainGroupID))
                    {
                        if (arrMainGroupNotPermission.ToString().Length > 0)
                            arrMainGroupNotPermission.Append(",");
                        arrMainGroupNotPermission.Append(objProduct.MainGroupName);
                        continue;
                    }
                    decimal decPrice = Convert.ToDecimal(row["INPUTPRICE"]);
                    int intVAT = 0;
                    int intVATPercent = 0;
                    if (!objInputType.IsVATZero)
                    {
                        intVAT = objProduct.VAT;
                    }
                    intVATPercent = objProduct.VATPercent;
                    decimal decQuantity = 1;
                    //Thêm sản phẩm vào lưới
                    DateTime dtEndDate = Convert.IsDBNull(row["ENDWARRANTYDATE"]) == true ? dtbServerDate : Convert.ToDateTime(row["ENDWARRANTYDATE"]);
                    InsertProductToGrid(objProduct, decQuantity, 0, decPrice, intVAT, intVATPercent, Convert.ToBoolean(row["ISBRANDNEWWARRANTY"]), dtEndDate);
                    //Thêm IMEI vào List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>
                    if (InputVoucherDetailList == null || InputVoucherDetailList.Count <= 0)
                    {
                        MessageBox.Show(this, "Thêm sản phẩm vào lưới bị lỗi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    //Khởi tạo list IMEI cho sản phẩm 
                    List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> InputVoucherDetailIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
                    PLC.Input.WSInputVoucher.InputVoucherDetailIMEI obj = new PLC.Input.WSInputVoucher.InputVoucherDetailIMEI();
                    obj.IsValidate = true;
                    obj.ProductID = strProductID;
                    obj.IMEI = strIMEI;
                    obj.IsHasWarranty = Convert.ToBoolean(row["ISBRANDNEWWARRANTY"]);
                    obj.IsError = false;
                    obj.Status = "Đã xác nhận!";
                    //Thêm IMEI vào list trên
                    InputVoucherDetailIMEIList.Add(obj);
                    PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = InputVoucherDetailList.First<PLC.Input.WSInputVoucher.InputVoucherDetail>();
                    objInputVoucherDetail.InputVoucherDetailIMEIList = InputVoucherDetailIMEIList.ToArray();
                    objInputVoucherDetail.Quantity = objInputVoucherDetail.InputVoucherDetailIMEIList.Count();
                    objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);
                    GetSumTotalMoney();
                }
                return true;
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi khi thêm sản phẩm vào chi tiết phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi khi thêm sản phẩm vào chi tiết phiếu nhập", objExce, PLCInventory_Globals.ModuleName);
                return false;
            }
        }


        private void CheckPermission()
        {
            #region Khởi tạo quyền
            bolPermission_PM_InputVoucher_ImportExcel = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_IMPORTEXCEL");
            bolPermission_PM_InputVoucher_ImportPinCode = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_IMPORTPINCODE");
            bolPermission_PM_InputVoucher_Create = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_CREATE");
            bolPermission_PM_InputVoucher_AutoCreateIMEI = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_AUTOCREATEIMEI");
            bolPermission_PM_OutInVoucher_OutVoucher_Create = SystemConfig.objSessionUser.IsPermission("PM_OUTINVOUCHER_OUTVOUCHER_CREATE");

            bolPermission_PM_InputVoucher_EditInvoiceInfo = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_EDITINVOICEINFO");//Phiếu nhập - Chỉnh thông tin hóa đơn
            bolPermission_PM_InputVoucher_EditInputType = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_EDITINPUTTYPE");//Phiếu nhập - Sửa hình thức nhập
            bolPermission_PM_InputVoucher_Attachment_Add = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_ATTACHMENT_ADD");//Phiếu nhập - Thêm tập tin đính kèm
            bolPermission_PM_InputVoucher_Attachment_View = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_ATTACHMENT_VIEW");//Phiếu nhập - Xem tập tin đính kèm
            bolPermission_PM_InputVoucher_Attachment_DeleteAll = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_ATTACHMENT_DELETEALL");//Phiếu nhập - Xóa tất cả tập tin đính kèm
            bolPermission_PM_InputVoucher_Attachment_Delete = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_ATTACHMENT_DELETE");//Phiếu nhập - Xóa tập tin đính kèm
            bolPermission_PM_InputVoucher_EditAfterPosted = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_EDITAFTERPOSTED");//Phiếu nhập - Chỉnh thông tin phiếu nhập sau khi đã hoạch chuyển
            bolPermission_PM_INPUTVOUCHER_PAYMENTINFO_EDIT = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_PAYMENTINFO_EDIT");//Phiếu nhập - cho phép User thay đổi dữ liệu trên các control Ngày hóa đơn, số hóa đơn, ký hiệu hóa đơn, mẫu hóa đơn, Ngày thanh toán.

            #endregion

            #region Kiểm tra quyền
            if (!isEdit)
            {
                #region Mode Thêm mới

                mnuImportFile.Enabled = bolPermission_PM_InputVoucher_ImportExcel;
                if (!bolPermission_PM_InputVoucher_Create)
                {
                    MessageBox.Show(this, "Bạn không có quyền thêm phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtBarcode.ReadOnly = true;
                    btnSearchProduct.Enabled = false;
                    btnUpdate.Enabled = false;
                    dtmDeliveryNoteDate.EditValue = false;

                }

                if (!bolPermission_PM_OutInVoucher_OutVoucher_Create)
                {
                    MessageBox.Show(this, "Sau khi nhập hàng, bạn phải tạo phiếu chi tương ứng\n cho phiếu nhập này. Nhưng bạn chưa có quyền tạo phiếu chi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtBarcode.ReadOnly = true;
                    btnSearchProduct.Enabled = false;
                    btnUpdate.Enabled = false;
                    dtmDeliveryNoteDate.EditValue = false;
                }


                //this.Text = this.Text + " | " + SystemConfig.objSessionUser.FullName + " - " + SystemConfig.objSessionUser.Function;
                if (!bolPermission_PM_InputVoucher_Create)
                {
                    MessageBox.Show(this, "Bạn không có quyền thêm phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtBarcode.ReadOnly = true;
                    btnSearchProduct.Enabled = false;
                    btnUpdate.Enabled = false;
                    dtmDeliveryNoteDate.EditValue = false;
                }

                if (!bolPermission_PM_OutInVoucher_OutVoucher_Create)
                {
                    MessageBox.Show(this, "Sau khi nhập hàng, bạn phải tạo phiếu chi tương ứng\n cho phiếu nhập này. Nhưng bạn chưa có quyền tạo phiếu chi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtBarcode.ReadOnly = true;
                    btnSearchProduct.Enabled = false;
                    btnUpdate.Enabled = false;
                    dtmDeliveryNoteDate.EditValue = false;
                }

                if (!bolPermission_PM_InputVoucher_Attachment_Add)
                {
                    mnuAddAttachment.Enabled = false;
                    mnuDelAttachment.Enabled = false;
                    mnuItemScanImage.Enabled = false;
                    mnuItemCaptureImage.Enabled = false;
                }
                if (bolIsInputFromRetailInput)
                {
                    radIsNew.Enabled = false;
                    radOld.Enabled = false;
                    for (int i = 0; i < grvInputVoucherDetail.Columns.Count; i++)
                    {
                        if ((grvInputVoucherDetail.Columns[i].FieldName.ToUpper() == "TAXEDFEE"
                            || grvInputVoucherDetail.Columns[i].FieldName.ToUpper() == "PURCHASEFEE") && !objInputType.IsAllowInputOtherFee)
                            grvInputVoucherDetail.Columns[i].OptionsColumn.AllowEdit = false;
                    }
                    grdInputVoucherDetail.RefreshDataSource();
                }

                if (bolIsInputFromOrder)
                {
                    for (int i = 0; i < grvInputVoucherDetail.Columns.Count; i++)
                    {
                        if (grvInputVoucherDetail.Columns[i].FieldName.ToUpper() == "TAXEDFEE"
                            || grvInputVoucherDetail.Columns[i].FieldName.ToUpper() == "PURCHASEFEE")
                            grvInputVoucherDetail.Columns[i].OptionsColumn.AllowEdit = false;
                    }
                    grdInputVoucherDetail.RefreshDataSource();
                    cboTransportCustomer.Enabled = false;
                }

                if (bolPermission_PM_INPUTVOUCHER_PAYMENTINFO_EDIT)
                {
                    dtPayableTime.Enabled = true;
                }
                if (!bolPermission_PM_InputVoucher_EditInvoiceInfo)
                {
                    chkCreateAutoInvoice.Enabled = false;
                    chkCreateAutoInvoice.Checked = false;
                    txtInVoiceID.ReadOnly = true;
                    txtInvoiceSymbol.ReadOnly = true;
                    txtDenominator.ReadOnly = true;
                    dtInvoiceDate.Enabled = false;
                    txtInVoiceID.BackColor = SystemColors.Info;
                    txtInvoiceSymbol.BackColor = SystemColors.Info;
                    txtDenominator.BackColor = SystemColors.Info;
                    dtInvoiceDate.BackColor = SystemColors.Info;
                }
                else
                {
                    chkCreateAutoInvoice.Enabled = true;
                    txtInVoiceID.ReadOnly = false;
                    txtInvoiceSymbol.ReadOnly = false;
                    txtDenominator.ReadOnly = false;
                    dtInvoiceDate.Enabled = true;
                    dtInvoiceDate.BackColor = SystemColors.Window;
                    txtInVoiceID.BackColor = SystemColors.Window;
                    txtInvoiceSymbol.BackColor = SystemColors.Window;
                    txtDenominator.BackColor = SystemColors.Window;
                    dtInvoiceDate.BackColor = SystemColors.Window;
                    cboInputTypeSearch_SelectionChangeCommitted(null, null);
                    if (bolIsInputFromOrder)
                    {
                        // Có hóa đơn về trước (Đơn hàng có hóa đơn)
                        DataTable dtbInvoiceFirst = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_PINVOICE_CHKINVOICEFIRST", new object[] { "@ORDERID", strOrderID });
                        //var objResult = new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataTable(ref dtbInvoiceFirst, "VAT_PINVOICE_CHKINVOICEFIRST", new object[] { "@ORDERID", strOrderID });
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->CheckPermission");
                        }

                        bolIsCheckInvoiceFirst = false;

                        if (dtbInvoiceFirst != null && dtbInvoiceFirst.Rows.Count > 0)
                        {
                            bolIsCheckInvoiceFirst = true;
                            chkCreateAutoInvoice.Checked = false;
                            txtInVoiceID.Text = dtbInvoiceFirst.Rows[0]["INVOICENO"].ToString();
                            txtInvoiceSymbol.Text = dtbInvoiceFirst.Rows[0]["INVOICESYMBOL"].ToString();
                            txtDenominator.Text = dtbInvoiceFirst.Rows[0]["DENOMINATOR"].ToString();
                            dtInvoiceDate.DateTime = Convert.ToDateTime(dtbInvoiceFirst.Rows[0]["INVOICEDATE"].ToString());
                            chkCreateAutoInvoice.Enabled = false;
                            txtInVoiceID.ReadOnly = true;
                            txtInVoiceID.BackColor = SystemColors.Info;
                            txtInvoiceSymbol.ReadOnly = true;
                            txtInvoiceSymbol.BackColor = SystemColors.Info;
                            txtDenominator.ReadOnly = true;
                            txtDenominator.BackColor = SystemColors.Info;
                            dtInvoiceDate.Enabled = false;
                            dtInvoiceDate.BackColor = SystemColors.Info;
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region Mode Chỉnh sửa

                grdInputVoucherDetail.ContextMenuStrip = mnuGridEdit;
                LockControl();
                if (objInputVoucher.IsPosted && !bolPermission_PM_InputVoucher_EditAfterPosted)
                {
                    lblMessage.Text = "Phiếu nhập này đã được kết chuyển. Bạn không được phép chỉnh sửa thông tin";
                    chkCreateAutoInvoice.Enabled = bolPermission_PM_InputVoucher_EditInvoiceInfo;
                    txtInVoiceID.ReadOnly = true;
                    txtInvoiceSymbol.ReadOnly = true;
                    txtDenominator.ReadOnly = true;
                    txtInputContent.ReadOnly = true;
                    txtDiscountInput.Properties.ReadOnly = true;
                    txtCustomerName.ReadOnly = true;
                    txtCustomerIDCard.Properties.ReadOnly = true;
                    txtAddress.ReadOnly = true;
                    txtIDCardIssuePlace.ReadOnly = true;
                    dtIDCardIssueDate.Enabled = false;
                    txtTaxID.Properties.ReadOnly = true;
                    txtPhone.Properties.ReadOnly = true;

                    // LÊ VĂN ĐÔNG: 11/12/2017 -> Bỏ ràng buộc đã hạch toàn thì không được check thực nhập
                    //chkIsCheckRealInput.Enabled = false;
                    //txtCheckRealInputNote.Properties.ReadOnly = true;
                    //btnRealInputConfirm.Enabled = false;

                    mnuAddAttachment.Enabled = false;
                    mnuDelAttachment.Enabled = false;
                    mnuItemScanImage.Enabled = false;
                    mnuItemCaptureImage.Enabled = false;


                }
                else
                {
                    if (objInputVoucher.ISStoreChange)
                    {
                        chkCreateAutoInvoice.Enabled = false;
                        txtInVoiceID.ReadOnly = true;
                        txtInVoiceID.BackColor = SystemColors.Info;
                        txtInvoiceSymbol.ReadOnly = true;
                        txtInvoiceSymbol.BackColor = SystemColors.Info;
                        txtDenominator.ReadOnly = true;
                        txtDenominator.BackColor = SystemColors.Info;
                        dtInvoiceDate.Enabled = false;
                        dtInvoiceDate.BackColor = SystemColors.Info;
                    }
                    else
                    {
                        chkCreateAutoInvoice.Enabled = bolPermission_PM_InputVoucher_EditInvoiceInfo;
                        txtInVoiceID.ReadOnly = !bolPermission_PM_InputVoucher_EditInvoiceInfo;
                        txtInvoiceSymbol.ReadOnly = !bolPermission_PM_InputVoucher_EditInvoiceInfo;
                        txtDenominator.ReadOnly = !bolPermission_PM_InputVoucher_EditInvoiceInfo;
                        txtInputContent.ReadOnly = !bolPermission_PM_InputVoucher_EditInvoiceInfo;
                        dtInvoiceDate.Enabled = bolPermission_PM_InputVoucher_EditInvoiceInfo;
                        dtInvoiceDate.Properties.ReadOnly = !bolPermission_PM_InputVoucher_EditInvoiceInfo;
                        mnuAddAttachment.Enabled = bolPermission_PM_InputVoucher_Attachment_Add;
                        mnuDelAttachment.Enabled = bolPermission_PM_InputVoucher_Attachment_Delete || bolPermission_PM_InputVoucher_Attachment_DeleteAll;
                        mnuItemScanImage.Enabled = bolPermission_PM_InputVoucher_Attachment_Add;
                        mnuItemCaptureImage.Enabled = bolPermission_PM_InputVoucher_Attachment_Add;
                        if (bolPermission_PM_InputVoucher_EditInvoiceInfo && !chkCreateAutoInvoice.Checked)
                        {
                            objInputType = new MasterData.PLC.MD.PLCInputType().LoadInfo(cboInputTypeSearch.InputTypeID);
                            if (objInputType == null)
                            {
                                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Không lấy được thông tin hình thức nhập");
                            }
                            if (!objInputType.IsReturnSale && !objInputType.IsReturnOutput)
                            {
                                chkCreateAutoInvoice.Enabled = true;
                                txtInVoiceID.ReadOnly = false;
                                txtInVoiceID.BackColor = SystemColors.Window;
                                txtInvoiceSymbol.ReadOnly = false;
                                txtInvoiceSymbol.BackColor = SystemColors.Window;
                                txtDenominator.ReadOnly = false;
                                txtDenominator.BackColor = SystemColors.Window;
                                dtInvoiceDate.Enabled = true;
                                dtInvoiceDate.BackColor = SystemColors.Window;
                            }

                            bool bolIsHasVATInvoice = new ERP.Inventory.PLC.Input.PLCInputVoucher().CheckIsHasVatInvoice(objInputVoucher.InputVoucherID.Trim());
                            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                            {
                                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                                SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->CheckPermission");
                            }

                            if (bolIsHasVATInvoice)
                            {
                                chkCreateAutoInvoice.Enabled = false;
                                txtInVoiceID.ReadOnly = true;
                                txtInVoiceID.BackColor = SystemColors.Info;
                                txtInvoiceSymbol.ReadOnly = true;
                                txtInvoiceSymbol.BackColor = SystemColors.Info;
                                txtDenominator.ReadOnly = true;
                                txtDenominator.BackColor = SystemColors.Info;
                                dtInvoiceDate.Enabled = false;
                                dtInvoiceDate.BackColor = SystemColors.Info;
                            }

                            if (objInputVoucher != null && !string.IsNullOrWhiteSpace(objInputVoucher.OrderID) && !objInputVoucher.ISStoreChange)
                            {
                                DataTable dtbInvoiceFirst = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_PINVOICE_CHKINVOICEFIRST", new object[] { "@ORDERID", strOrderID });
                                //var objResult = new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataTable(ref dtbInvoiceFirst, "VAT_PINVOICE_CHKINVOICEFIRST", new object[] { "@ORDERID", strOrderID });
                                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                                {
                                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->LoadControl");
                                }

                                if (dtbInvoiceFirst != null && dtbInvoiceFirst.Rows.Count > 0)
                                {
                                    bolIsCheckInvoiceFirst = true;
                                    chkCreateAutoInvoice.Enabled = false;
                                    txtInVoiceID.ReadOnly = true;
                                    txtInVoiceID.BackColor = SystemColors.Info;
                                    txtInvoiceSymbol.ReadOnly = true;
                                    txtInvoiceSymbol.BackColor = SystemColors.Info;
                                    txtDenominator.ReadOnly = true;
                                    txtDenominator.BackColor = SystemColors.Info;
                                    dtInvoiceDate.Enabled = false;
                                    dtInvoiceDate.BackColor = SystemColors.Info;
                                }
                            }
                        }
                    }
                }
                if (objInputVoucher.IsDeleted)
                {
                    btnDelete.Enabled = false;
                    btnUpdate.Enabled = false;
                    dtmDeliveryNoteDate.EditValue = false;
                }

                //txtInVoiceID.ReadOnly = chkCreateAutoInvoice.Checked;
                //txtInvoiceSymbol.ReadOnly = chkCreateAutoInvoice.Checked;
                //txtDenominator.ReadOnly = chkCreateAutoInvoice.Checked;
                //dtInvoiceDate.Enabled = chkCreateAutoInvoice.Checked;
                //if (chkCreateAutoInvoice.Checked)
                //{
                //    txtInVoiceID.BackColor = SystemColors.Info;
                //    txtInvoiceSymbol.BackColor = SystemColors.Info;
                //    txtDenominator.BackColor = SystemColors.Info;
                //    dtInvoiceDate.BackColor = SystemColors.Info;
                //}


                //if (!bolPermission_PM_INPUTVOUCHER_PAYMENTINFO_EDIT)
                //{
                //    dtInvoiceDate.Enabled = false;
                //    txtInVoiceID.ReadOnly = true;
                //    txtInvoiceSymbol.ReadOnly = true;
                //    txtDenominator.ReadOnly = true;
                //    dtPayableTime.Enabled = false;
                //}
                //else if (bolPermission_PM_INPUTVOUCHER_PAYMENTINFO_EDIT)
                //{
                //    dtInvoiceDate.Enabled = true;
                //    dtInvoiceDate.Properties.ReadOnly = false;
                //    dtPayableTime.Enabled = true;
                //}

                #endregion
            }

            if ((objInputType != null && objInputType.StoreCenterID > 0) || (objPOType != null && objPOType.ISIVTOCTSTORE))
            {
                chkCreateAutoInvoice.Checked = false;
                chkCreateAutoInvoice.Enabled = false;
                txtInVoiceID.ReadOnly = true;
                txtInVoiceID.BackColor = SystemColors.Info;
                txtDenominator.ReadOnly = true;
                txtDenominator.BackColor = SystemColors.Info;
                txtInvoiceSymbol.ReadOnly = true;
                txtInvoiceSymbol.BackColor = SystemColors.Info;
                dtInvoiceDate.Properties.ReadOnly = true;
                dtInvoiceDate.BackColor = SystemColors.Info;
            }

            #endregion
        }

        private void btnSearchProduct_Click(object sender, EventArgs e)
        {
            if (bolIsInputFromOrder)
                return;
            if (cboStoreSearch.StoreID < 1)
            {
                MessageBox.Show(this, "Bạn phải chọn kho nhập trước khi bắn barcode!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboStoreSearch.Focus();
                return;
            }
            if (cboInputTypeSearch.InputTypeID < 1)
            {
                MessageBox.Show(this, "Bạn chưa chọn hình thức nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInputTypeSearch.Focus();
                return;
            }
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch1 = MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch1.MainGroupPermissionType = Library.AppCore.Constant.EnumType.MainGroupPermissionType.INPUT;
            frmProductSearch1.IsMultiSelect = true;
            frmProductSearch1.ShowDialog(this);
            if (frmProductSearch1.ProductList != null)
            {
                foreach (var item in frmProductSearch1.ProductList)
                {
                    if (!string.IsNullOrEmpty(item.ProductID.Trim()))
                        AutoEnterBarcode(item.ProductID.Trim());
                }
                //txtBarcode.Text = frmProductSearch1.Product.ProductID;
                //txtBarcode.Focus();
            }
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (bolIsInputFromOrder)
                return;
            txtBarcode.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtBarcode.Text.Trim());
            if (cboStoreSearch.StoreID < 1)
            {
                MessageBox.Show(this, "Bạn phải chọn kho nhập trước khi bắn barcode!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboStoreSearch.Focus();
                return;
            }
            if (cboInputTypeSearch.InputTypeID < 1)
            {
                MessageBox.Show(this, "Bạn chưa chọn hình thức nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInputTypeSearch.Focus();
                return;
            }
            String strBarcode = txtBarcode.Text;
            if (strBarcode.Length > 0 && e.KeyChar == 13)
            {
                String strRlt = BarcodeValidation(strBarcode, 1);
                if (!string.IsNullOrEmpty(strRlt))
                    MessageBox.Show(this, strRlt, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBarcode.Text = string.Empty;
                txtBarcode.Focus();
            }
            grdInputVoucherDetail.RefreshDataSource();
        }


        private void cboCurrencyUnit_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboCurrencyUnit.DataSource == null || cboCurrencyUnit.SelectedIndex < 1)
            {
                txtExchangeRate.Text = null;
                return;
            }
            else
            {
                decOldCurrencyExchange = ERP.MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyExchange(Convert.ToInt32(cboCurrencyUnit.SelectedValue));
                //cmt ở đây
                if (IsInputFromOrder == false)
                {
                    txtExchangeRate.Text = decOldCurrencyExchange.ToString();
                }
                if (Convert.ToInt32(cboCurrencyUnit.SelectedValue) == 1)
                    txtExchangeRate.Properties.ReadOnly = true;
                else
                    txtExchangeRate.Properties.ReadOnly = false;
            }
            if (Convert.ToInt32(cboCurrencyUnit.SelectedValue) == intCurrencyUnitDefaultID)
            {
                txtCashUSD.Text = null;
                txtCashUSDExchange.Text = null;
                txtCashUSD.Enabled = false;
                txtCashVND.Enabled = true;
                txtPaymentCard.Enabled = true;
                cboPaymentCard.Enabled = true;
            }
            else
            {
                txtCashUSDExchange.Text = null;
                txtCashVND.Text = null;
                txtPaymentCard.Text = null;
                cboPaymentCard.SelectedIndex = 0;
                txtCashUSD.Enabled = true;
                txtCashVND.Enabled = false;
                txtPaymentCard.Enabled = false;
                cboPaymentCard.Enabled = false;
            }
            txtCashVND_EditValueChanged(null, null);
        }
        private void LoadDefaultCustomerInfo()
        {
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = MasterData.PLC.MD.PLCCustomer.GetCustomerDefault();
            if (objCustomer != null)
            {
                txtCustomerID.Text = objCustomer.CustomerID.ToString();
                txtCustomerName.Text = objCustomer.CustomerName;
                txtAddress.Text = objCustomer.CustomerAddress;
                txtCustomerIDCard.Text = objCustomer.CustomerIDCard;
                txtTaxID.Text = objCustomer.CustomerTaxID;
                txtPhone.Text = objCustomer.CustomerPhone;
            }
        }
        private void LoadCustomerInfo(int intCustomerID)
        {
            ERP.MasterData.PLC.MD.PLCCustomer objPLCCustomer = new MasterData.PLC.MD.PLCCustomer();
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
            objPLCCustomer.LoadInfo(ref objCustomer, intCustomerID);
            if (objCustomer != null)
            {
                txtCustomerID.Text = objCustomer.CustomerID.ToString();
                txtCustomerName.Text = objCustomer.CustomerName;
                txtAddress.Text = objCustomer.CustomerAddress;
                txtCustomerIDCard.Text = objCustomer.CustomerIDCard;
                txtTaxID.Text = objCustomer.CustomerTaxID;
                txtPhone.Text = objCustomer.CustomerPhone;
            }
        }
        private void ExportExcel(Form owner, DataTable dtbData)
        {
            DevExpress.XtraGrid.GridControl _grid = new DevExpress.XtraGrid.GridControl();
            DevExpress.XtraGrid.Views.Grid.GridView _gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            _grid.MainView = _gridView;
            _grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { _gridView });
            _gridView.GridControl = _grid;
            owner.Controls.Add(_grid);
            _grid.DataSource = dtbData;
            _gridView.OptionsView.ColumnAutoWidth = true;
            _gridView.Columns.Clear();
            for (int i = 0; i < dtbData.Columns.Count; i++)
            {
                string fieldName = dtbData.Columns[i].ColumnName;
                string strCaption = dtbData.Columns[i].Caption;
                DevExpress.XtraGrid.Columns.GridColumn column = _gridView.Columns.AddField(fieldName);
                column.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                column.OptionsColumn.AllowEdit = false;
                column.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
                column.OptionsColumn.AllowSize = true;
                _gridView.Columns[i].Visible = true;
                _gridView.Columns[i].VisibleIndex = i;
                _gridView.Columns[i].AppearanceHeader.Options.UseTextOptions = true;
                _gridView.Columns[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                _gridView.Columns[i].OptionsColumn.AllowMove = false;
                _gridView.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                _gridView.Columns[i].Caption = strCaption;
            }
            _gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            _gridView.ColumnPanelRowHeight = ERP.MasterData.DUI.Common.CommonFunction.iColumnPanelRowHeight;
            _gridView.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            _gridView.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            _gridView.OptionsFilter.Reset();
            _gridView.OptionsView.ShowAutoFilterRow = true;
            _gridView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            _gridView.OptionsView.ShowFooter = false;
            _gridView.OptionsView.ShowGroupPanel = false;
            _gridView.OptionsNavigation.UseTabKey = false;
            _gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font(_gridView.Appearance.HeaderPanel.Font, System.Drawing.FontStyle.Bold);

            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(_grid);

            owner.Controls.Remove(_grid);
        }

        #region Scan hình, chụp hình
        private void CaptureImage()
        {
            try
            {
                string strFileName = System.Guid.NewGuid().ToString();
                //Library.SCImage.CaptureImage.NCIVideo.frmCapture frm = new Library.SCImage.CaptureImage.NCIVideo.frmCapture();
                //frm.ID = strFileName;
                //frm.ShowDialog();
                try
                {
                    ProcessStartInfo StartInfo1 = new ProcessStartInfo
                    {
                        FileName = Application.StartupPath + @"\Library.SCImage.exe",
                        Arguments = string.Format(@"{0}|{1}", "1", strFileName),//{0}: 1 là NCIVideo, 2 là NCNTCamera, 3 là ScanImage
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    };
                    //string strFileName = Path.GetFileName(objPSRequestAttachment.AttachmentPath);
                    Process myProcess = new Process { StartInfo = StartInfo1 };
                    myProcess.Start();
                    while (!myProcess.HasExited)
                    {
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, "Lỗi khởi tạo máy chụp hình đính kèm!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    SystemErrorWS.Insert("Lỗi khởi tạo máy chụp hình đính kèm", ex.Message, DUIInventory_Globals.ModuleName);
                }

                //if (frm.IsSuccess)
                //{
                string strFileTempPath = Path.GetTempPath() + @"\NC_CaptureImage.txt";
                String strReturn = string.Empty;
                if (System.IO.File.Exists(strFileTempPath))
                {
                    FileStream fs = new FileStream(strFileTempPath, FileMode.Open);
                    StreamReader rd = new StreamReader(fs, Encoding.UTF8);
                    strReturn = rd.ReadLine();// ReadLine() chỉ đọc 1 dòng đầu thoy, ReadToEnd là đọc hết               
                    rd.Close();
                    System.IO.File.Delete(strFileTempPath);
                }
                if (!string.IsNullOrWhiteSpace(strReturn))
                {
                    string[] arrReturn = strReturn.Split('\\');
                    PLC.Input.WSInputVoucher.InputVoucher_Attachment objInputVoucher_Attachment = new PLC.Input.WSInputVoucher.InputVoucher_Attachment();
                    string strLocalFilePath = strReturn;//frm.FileName;
                    string strExtension = Path.GetExtension(strLocalFilePath);
                    objInputVoucher_Attachment.FileName = arrReturn[arrReturn.Length - 1];//strFileName + strExtension;
                    objInputVoucher_Attachment.InputVoucherID = string.Empty;
                    objInputVoucher_Attachment.LocalFilePath = strLocalFilePath;
                    objInputVoucher_Attachment.CreatedDate = dtbServerDate;
                    objInputVoucher_Attachment.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objInputVoucher_Attachment.IsNew = true;
                    InputVoucher_AttachmentList.Add(objInputVoucher_Attachment);
                }
                //}
                grdAttachment.RefreshDataSource();
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, objExce.ToString(), "Lỗi chụp hình đính kèm!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SystemErrorWS.Insert("Lỗi chụp hình đính kèm", objExce.ToString(), DUIInventory_Globals.ModuleName);
            }
        }

        private void ScanImage()
        {
            try
            {
                string strFileName = System.Guid.NewGuid().ToString();
                //Library.SCImage.ScanImage.frmScanImage frm = new Library.SCImage.ScanImage.frmScanImage();
                //frm.ID = strFileName;
                //frm.ShowDialog();
                try
                {
                    ProcessStartInfo StartInfo1 = new ProcessStartInfo
                    {
                        FileName = Application.StartupPath + @"\Library.SCImage.exe",
                        Arguments = string.Format(@"{0}|{1}", "3", strFileName),//{0}: 1 là NCIVideo, 2 là NCNTCamera, 3 là ScanImage
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    };
                    //string strFileName = Path.GetFileName(objPSRequestAttachment.AttachmentPath);
                    Process myProcess = new Process { StartInfo = StartInfo1 };
                    myProcess.Start();
                    while (!myProcess.HasExited)
                    {
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, "Lỗi khởi tạo máy chụp hình đính kèm!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    SystemErrorWS.Insert("Lỗi khởi tạo máy chụp hình đính kèm", ex.Message, DUIInventory_Globals.ModuleName);
                }

                //if (frm.IsSuccess)
                //{
                string strFileTempPath = Path.GetTempPath() + @"\NC_CaptureScanImage.txt";
                String strReturn = string.Empty;
                if (System.IO.File.Exists(strFileTempPath))
                {
                    FileStream fs = new FileStream(strFileTempPath, FileMode.Open);
                    StreamReader rd = new StreamReader(fs, Encoding.UTF8);
                    strReturn = rd.ReadLine();// ReadLine() chỉ đọc 1 dòng đầu thoy, ReadToEnd là đọc hết               
                    rd.Close();
                    System.IO.File.Delete(strFileTempPath);
                }
                if (!string.IsNullOrWhiteSpace(strReturn))
                {
                    string[] arrReturn = strReturn.Split('\\');
                    PLC.Input.WSInputVoucher.InputVoucher_Attachment objInputVoucher_Attachment = new PLC.Input.WSInputVoucher.InputVoucher_Attachment();
                    string strLocalFilePath = strReturn;//frm.FileName;
                    string strExtension = Path.GetExtension(strLocalFilePath);
                    objInputVoucher_Attachment.FileName = arrReturn[arrReturn.Length - 1];//strFileName + strExtension;
                    objInputVoucher_Attachment.InputVoucherID = string.Empty;
                    objInputVoucher_Attachment.LocalFilePath = strLocalFilePath;
                    objInputVoucher_Attachment.CreatedDate = dtbServerDate;
                    objInputVoucher_Attachment.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objInputVoucher_Attachment.IsNew = true;
                    InputVoucher_AttachmentList.Add(objInputVoucher_Attachment);
                }
                grdAttachment.RefreshDataSource();
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, objExce.ToString(), "Lỗi scan hình đính kèm!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SystemErrorWS.Insert("Lỗi scan hình đính kèm", objExce.ToString(), DUIInventory_Globals.ModuleName);
            }
        }

        #endregion
        private void cboStoreSearch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (InputVoucherDetailList.Count <= 0 || cboStoreSearch.StoreID < 1
                || intInputStoreIDOld == cboStoreSearch.StoreID)
            {
                intInputStoreIDOld = cboStoreSearch.StoreID;
                return;
            }


            if (!bolIsInputFromRetailInput)
            {
                if (MessageBox.Show(this, "Khi chuyển kho nhập, dữ liệu nhập vào sẽ bị mất! \nBạn có muốn chuyển qua kho \"" + cboStoreSearch.StoreName + "\" không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    InputVoucherDetailList.Clear();
                    grdInputVoucherDetail.DataSource = InputVoucherDetailList;
                    grdInputVoucherDetail.RefreshDataSource();
                }
                else
                    cboStoreSearch.SetValue(intInputStoreIDOld);
            }
            else
            {
                string strMess = string.Empty;
                foreach (var item in InputVoucherDetailList)
                {
                    ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(item.ProductID);
                    if (objProduct == null)
                    {
                        strMess = "Mặt hàng này không tồn tại";
                        break;
                    }
                    if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(objProduct.MainGroupID, Library.AppCore.Constant.EnumType.MainGroupPermissionType.INPUT))
                    {
                        strMess = "Bạn không có quyền nhập trên ngành hàng " + objProduct.MainGroupName + "!";
                        break;
                    }
                    Library.AppCore.Constant.EnumType.IsNewPermissionType enIsNewPermission = Library.AppCore.Constant.EnumType.IsNewPermissionType.ISOLD;
                    String strIsNew = radIsNew.Checked ? "mới" : "cũ";
                    if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(objProduct.MainGroupID, Library.AppCore.Constant.EnumType.MainGroupPermissionType.INPUT, enIsNewPermission))
                    {
                        strMess = "Bạn không có quyền nhập máy " + strIsNew + " ngành hàng " + objProduct.MainGroupName;
                        break;
                    }
                    if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(cboStoreSearch.StoreID, objProduct.MainGroupID,
                         Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.INPUT))
                    {
                        strMess = "Kho này không được phép nhập trên ngành hàng " + objProduct.MainGroupName + "!";
                        break;
                    }
                }
                if (strMess != string.Empty)
                {
                    MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboStoreSearch.SetValue(intInputStoreIDOld);
                }

            }
            intInputStoreIDOld = cboStoreSearch.StoreID;
        }

        private void cboInputTypeSearch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboInputTypeSearch.InputTypeID > 0)
            {
                objInputType = new MasterData.PLC.MD.PLCInputType().LoadInfo(cboInputTypeSearch.InputTypeID);
                if (objInputType == null)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Không lấy được thông tin hình thức nhập");
                    return;
                }
                intInputTypeID = objInputType.InputTypeID;
                Library.AppCore.LoadControls.SetDataSource.SetVoucherType(this, cboVoucherType, true, Library.AppCore.Constant.EnumType.VoucherType.SPEND);
                if (objInputType.VoucherTypeID > 0)
                {
                    Library.AppCore.LoadControls.ComboBoxObject.SetValue(cboVoucherType, objInputType.VoucherTypeID);
                    cboVoucherType.Enabled = false;
                }

                if (!bolIsInputFromOrder && !bolIsInputFromRetailInput)
                {
                    if (InputVoucherDetailList.Count > 0 && MessageBox.Show(this, "Khi chọn lại hình thức nhập chi tiết phiếu nhập sẽ bị xóa\r\nBạn có chắc muốn chọn lại không?",
                        "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        cboInputTypeSearch.SetValue(intInputTypeID);
                        return;
                    }
                    InputVoucherDetailList = new List<PLC.Input.WSInputVoucher.InputVoucherDetail>();
                    grdInputVoucherDetail.DataSource = InputVoucherDetailList;
                    grvInputVoucherDetail.Columns["TaxedFee"].OptionsColumn.AllowEdit = objInputType.IsAllowInputOtherFee;
                    grvInputVoucherDetail.Columns["PurchaseFee"].OptionsColumn.AllowEdit = objInputType.IsAllowInputOtherFee;
                    cboTransportCustomer.Enabled = objInputType.IsAllowInputTransportCustomer;
                }
                else
                {
                    if (TransportCustomerID > 0 && objInputType.IsAllowInputTransportCustomer)
                    {
                        cboTransportCustomer.SetValue(TransportCustomerID);
                        cboTransportCustomer.Enabled = !objInputType.IsAllowInputTransportCustomer;
                    }
                    grvInputVoucherDetail.Columns["TaxedFee"].OptionsColumn.AllowEdit = false;
                    grvInputVoucherDetail.Columns["PurchaseFee"].OptionsColumn.AllowEdit = false;
                }
                if (!isEdit)
                {
                    if (bolPermission_PM_InputVoucher_EditInvoiceInfo)
                    {
                        if (objInputType.IsReturnSale || objInputType.IsReturnOutput || objInputType.StoreCenterID > 0)
                        {
                            chkCreateAutoInvoice.Checked = false;
                            chkCreateAutoInvoice.Enabled = false;
                        }
                        else
                        {
                            chkCreateAutoInvoice.Checked = true;
                            chkCreateAutoInvoice.Enabled = true;
                        }
                    }
                    else
                    {
                        chkCreateAutoInvoice.Enabled = false;
                        chkCreateAutoInvoice.Checked = false;
                        txtInVoiceID.ReadOnly = true;
                        txtInvoiceSymbol.ReadOnly = true;
                        txtDenominator.ReadOnly = true;
                        dtInvoiceDate.Enabled = false;
                        txtInVoiceID.BackColor = SystemColors.Info;
                        txtInvoiceSymbol.BackColor = SystemColors.Info;
                        txtDenominator.BackColor = SystemColors.Info;
                        dtInvoiceDate.BackColor = SystemColors.Info;
                    }
                }
                ChangeVAT();
            }
        }

        private void grvInputVoucherDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (grvInputVoucherDetail.FocusedRowHandle >= 0)
            {
                PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetail)grvInputVoucherDetail.GetRow(grvInputVoucherDetail.FocusedRowHandle);
                switch (e.Column.FieldName.ToUpper())
                {
                    case "QUANTITY":
                        if (Convert.ToDecimal(objInputVoucherDetail.Quantity) <= 0)
                        {
                            objInputVoucherDetail.Quantity = 0;
                            objInputVoucherDetail.IsCheckQuantity = false;
                        }
                        else
                        {
                            objInputVoucherDetail.IsCheckQuantity = true;
                        }
                        if (bolIsInputFromOrder)
                        {
                            if (objInputVoucherDetail.Quantity > objInputVoucherDetail.OrderQuantity)
                            {
                                objInputVoucherDetail.Quantity = objInputVoucherDetail.OrderQuantity;
                            }
                            //objInputVoucherDetail.TaxedFee = objInputVoucherDetail.Quantity / objInputVoucherDetail.TotalOrderQuantity * objInputVoucherDetail.OrderTaxedFee;
                            //objInputVoucherDetail.PurchaseFee = objInputVoucherDetail.Quantity / objInputVoucherDetail.TotalOrderQuantity * objInputVoucherDetail.OrderPurchaseFee;
                        }

                        break;
                    case "INPUTPRICE":
                        if (objInputVoucherDetail.InputPrice < 0)
                        {
                            MessageBox.Show(this, "Đơn giá phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            objInputVoucherDetail.InputPrice = Math.Abs(Convert.ToDecimal(objInputVoucherDetail.InputPrice));
                            grvInputVoucherDetail.FocusedColumn = colInputPrice;
                        }
                        objInputVoucherDetail.FirstPrice = objInputVoucherDetail.InputPrice;
                        objInputVoucherDetail.ORIGINALInputPrice = objInputVoucherDetail.InputPrice;
                        break;

                    case "PRODUCTIONDATE":
                        int intMonthOfWarranty = GetWarrantyByProductID(objInputVoucherDetail.ProductID, objInputVoucherDetail.IsNew);
                        if (intMonthOfWarranty != -1)
                            objInputVoucherDetail.ENDWarrantyDate = Convert.ToDateTime(objInputVoucherDetail.ProductIONDate).AddMonths(intMonthOfWarranty);
                        else
                            objInputVoucherDetail.ENDWarrantyDate = Convert.ToDateTime(objInputVoucherDetail.ProductIONDate);
                        break;
                    case "ENDWARRANTYDATE":
                        if (objInputVoucherDetail.ENDWarrantyDate == null)
                            objInputVoucherDetail.ENDWarrantyDate = dtbServerDate;
                        break;
                    case "TAXEDFEE":
                        Decimal decTaxedFee = Convert.ToDecimal(grvInputVoucherDetail.GetFocusedRowCellValue(colTaxedFee));
                        if (decTaxedFee < 0)
                        {
                            MessageBox.Show(this, "Chi phí tính thuế phải lớn hơn bằng 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            objInputVoucherDetail.TaxedFee = Math.Abs(decTaxedFee);
                            grvInputVoucherDetail.FocusedColumn = colTaxedFee;
                        }
                        break;
                    case "PURCHASEFEE":
                        Decimal decPurchaseFee = Convert.ToDecimal(grvInputVoucherDetail.GetFocusedRowCellValue(colPurchaseFee));
                        if (decPurchaseFee < 0)
                        {
                            MessageBox.Show(this, "Chi phí nhập hàng phải lớn hơn bằng 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            objInputVoucherDetail.PurchaseFee = Math.Abs(decPurchaseFee);
                            grvInputVoucherDetail.FocusedColumn = colPurchaseFee;
                        }
                        break;
                }
                if (objInputVoucherDetail.ProductIONDate > objInputVoucherDetail.ENDWarrantyDate)
                {
                    MessageBox.Show(this, "Ngày sản xuất không lớn hơn ngày kết thúc bảo hành!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objInputVoucherDetail.ENDWarrantyDate = objInputVoucherDetail.ProductIONDate;
                    grvInputVoucherDetail.FocusedColumn = colNKTBH;
                }
                //"[QUANTITY] * [PRICE] + ([QUANTITY] * [PRICE]) * ([VAT] / 100 * [VATPERCENT] / 100)"
                objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);
            }
            grdInputVoucherDetail.RefreshDataSource();
            GetSumTotalMoney();
            grvInputVoucherDetail.FocusedColumn = colPurchaseFee;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!ValidateUpdate())
                return;
            //UpdateOrInsertData();
            Library.AppCore.Forms.frmWaitDialog.Show(string.Empty, "Đang xử lý...");
            Thread objThread = new Thread(new ThreadStart(UpdateOrInsertData));
            objThread.Start();
        }

        private bool ValidateUpdate()
        {
            if (!isEdit && !CheckInput())
                return false;
            if (InputVoucher_AttachmentList != null && InputVoucher_AttachmentList.Count > 0)
            {
                int intCountCamera1 = 0;
                int intCountCamera2 = 0;
                foreach (var item in InputVoucher_AttachmentList)
                {
                    if (item.FileName.Contains("CAM1") && item.IsDeleted == false)
                    {
                        intCountCamera1++;
                    }
                    if (item.FileName.Contains("CAM2") && item.IsDeleted == false)
                    {
                        intCountCamera2++;
                    }
                }
                if (intCountCamera1 > 0)
                {
                    if (intCountCamera2 == 0)
                    {
                        MessageBox.Show("Vui lòng chụp bằng cả 2 camera!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                else
                {
                    if (intCountCamera2 > 0)
                    {
                        MessageBox.Show("Vui lòng chụp bằng cả 2 camera!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
            }
            //if (!isEdit && !CheckInputCustomer())
            //    return;
            if (!CheckInputCustomer())
                return false;
            if (!CheckInputInvoice())
                return false;
            if (!isEdit && !CheckInputVoucher())
                return false;
            if (!isEdit && !CheckInputVoucherDetail())
                return false;
            if (!isEdit && !Library.AppCore.Other.CheckObject.CheckSystemTime())
            {
                MessageBox.Show("Giờ của máy khác giờ hệ thống. Bạn vui lòng xem lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (!isEdit && MessageBox.Show("Vui lòng kiểm tra hình thức nhập trước khi cập nhật. Bạn có chắc muốn cập nhật phiếu nhập?!", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return false;

            if (bolIsInputFromRetailInput)//Định giá máy cũ hình thức trade in bắt buộc cùng kho
            {
                try
                {
                    if (cboStoreSearch.StoreID != intStoreFromRetailInput)
                    {
                        MessageBox.Show("Kho nhập phải giống kho định giá.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedTab = tabPage1;
                        cboStoreSearch.Focus();
                        return false;
                    }
                }
                catch { }
            }

            if (!isEdit && !objInputType.IsCreateOutVoucher
                && MessageBox.Show(this, "Hình thức nhập này không có tạo phiếu chi\r\nBạn có chắc muốn cập nhật không?", "Thông báo",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                return false;
            }

            return true;
        }
        private void UpdateOrInsertData()
        {
            try
            {
                btnUpdate.Enabled = false;
                bolIsCloseForm = false;
                //tabControl1.SelectedTab = tabOutVoucher;
                if (!isEdit && objInputType.IsCreateOutVoucher)
                {
                    txtContent.Text = "MPN:" + txtInputVoucherID.Text.Trim();
                    CalculateCMVoucher();
                }
                decimal decDiscount = Convert.ToDecimal(txtDiscountInput.EditValue);
                int intVoucherTypeID = 0;
                string strOutVoucherID = string.Empty;
                DateTime dtmBeginTime = Library.AppCore.Globals.GetServerDateTime();
                if (!SaveAttachment())
                {
                    if (MessageBox.Show(this, "Đính kèm tập tin không thành công! Bạn muốn tiếp tục lưu hay không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        bolIsFinish = true;
                        btnUpdate.Enabled = true;
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    else
                    {
                        InputVoucher_AttachmentList = InputVoucher_AttachmentList.FindAll(x => x.IsNew = false);
                    }
                }


                if (objPOType != null)
                {
                    if (objPOType.ISIVTOCTSTORE && !isEdit)
                    {
                        DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
                        ERP.MasterData.PLC.MD.WSStore.Store objStore = null;
                        ERP.MasterData.PLC.MD.WSStore.Store objStoreCenterStore = null;
                        new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objStore, cboStoreSearch.StoreID);
                        if (objStore == null)
                        {
                            bolIsFinish = true;
                            MessageBox.Show(this, "Không tìm thấy kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        if (objStore.CenterStore <= 0)
                        {
                            bolIsFinish = true;
                            MessageBox.Show(this, "Không tìm thấy kho trung tâm trong kho nhập [" + objStore.StoreID + " - " + objStore.StoreName + "]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objStoreCenterStore, objStore.CenterStore);

                        if (objStore.BranchID == objStoreCenterStore.BranchID)
                        {
                            if (!UpdateData(decDiscount, decTotalAmount, ref intVoucherTypeID, ref strOutVoucherID))
                            {
                                bolIsFinish = true;
                                MessageBox.Show(this, "Lỗi khi thêm mới một hóa đơn nhập hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                btnUpdate.Enabled = true;
                                return;
                            }
                            else
                                bolIsReloadManager = true;
                        }
                        else
                        {
                            if (!UpdateDataForOrder(decDiscount, decTotalAmount, objPOType,
                           objStore, objStoreCenterStore,
                           ref intVoucherTypeID, ref strOutVoucherID))
                            {
                                btnUpdate.Enabled = true;
                                bolIsFinish = true;
                                return;
                            }
                            else
                                bolIsReloadManager = true;
                        }

                    }
                    else
                    {
                        if (!UpdateData(decDiscount, decTotalAmount, ref intVoucherTypeID, ref strOutVoucherID))
                        {
                            bolIsFinish = true;
                            MessageBox.Show(this, "Lỗi khi thêm mới một hóa đơn nhập hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            btnUpdate.Enabled = true;
                            return;
                        }
                        else
                            bolIsReloadManager = true;
                    }
                }
                else
                {
                    if (!UpdateData(decDiscount, decTotalAmount, ref intVoucherTypeID, ref strOutVoucherID))
                    {
                        bolIsFinish = true;
                        MessageBox.Show(this, "Lỗi khi thêm mới một hóa đơn nhập hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnUpdate.Enabled = true;
                        return;
                    }
                    else
                        bolIsReloadManager = true;
                }

            }
            catch (Exception ex)
            {
                bolIsFinish = true;
                MessageBoxObject.ShowErrorMessage(this, "Lỗi tạo phiếu nhập!");
                SystemErrorWS.Insert("Lỗi tạo phiếu nhập", ex.ToString());
            }
            finally
            {
                bolIsFinish = true;
            }
        }
        private void cboDiscountReason_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtDiscountInput.Properties.ReadOnly = (cboDiscountReason.SelectedIndex == 0);
            if (cboDiscountReason.SelectedIndex == 0)
                txtDiscountInput.Text = null;
        }

        private void txtCashVND_EditValueChanged(object sender, EventArgs e)
        {
            decimal decCashVND = 0;
            decimal decCashUSD = 0;
            decimal decMoneyCard = 0;
            decimal decTotalAmount = 0;

            if (!string.IsNullOrEmpty(txtTotalLiquidate.Text))
                decTotalAmount = Convert.ToDecimal(txtTotalLiquidate.EditValue);
            if (!string.IsNullOrEmpty(txtCashVND.Text))
                decCashVND = Convert.ToDecimal(txtCashVND.EditValue);
            if (!string.IsNullOrEmpty(txtCashUSD.Text))
                decCashUSD = Convert.ToDecimal(txtCashUSD.EditValue);
            try
            {
                if (!string.IsNullOrEmpty(txtPaymentCard.Text))
                    decMoneyCard = Convert.ToDecimal(txtPaymentCard.EditValue);
            }
            catch
            {
                if (!Convert.IsDBNull(txtPaymentCard.Text))
                {
                    decMoneyCard = Convert.ToDecimal(txtPaymentCard.EditValue);
                    txtPaymentCard.EditValue = decMoneyCard;
                }
            }
            //Số tiền còn nợ lại
            decimal decDebt = decTotalAmount - (decCashVND + decCashUSD + decMoneyCard);
            if (decDebt < 0)
            {
                decCashVND = 0;
                txtCashVND.EditValue = decCashVND;
                decCashUSD = 0;
                txtCashUSD.EditValue = decCashUSD;
                decMoneyCard = 0;
                txtPaymentCard.EditValue = decMoneyCard;
                decDebt = decTotalAmount - (decCashVND + decCashUSD + decMoneyCard);
            }
            txtDebt.EditValue = decDebt;
        }

        private void txtExchangeRate_EditValueChanged(object sender, EventArgs e)
        {
            decimal decExchangeRate = Convert.ToDecimal(txtExchangeRate.EditValue);
            if (IsEdit)
                return;
            if (((decimal)Math.Abs(decExchangeRate - decOldCurrencyExchange)) > ((decimal)0.1) * decOldCurrencyExchange)
            {
                MessageBox.Show(this, "Tỷ giá chỉnh sửa chênh lệch với tỷ giá ERP hơn 10%", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtExchangeRate.EditValue = decOldCurrencyExchange;
                txtExchangeRate.Focus();
            }
        }

        private void tabOutVoucher_Enter(object sender, EventArgs e)
        {
            if (cboInputTypeSearch.InputTypeID < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn hình thức nhập trước khi chuyển sang thông tin phiếu chi.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedTab = tabPage1;
                cboInputTypeSearch.Focus();
                return;
            }
            if (!objInputType.IsCreateOutVoucher)
            {
                tabControl1.SelectedTab = tabPage1;
                return;
            }
            txtContent.Text = "MPN:" + txtInputVoucherID.Text.Trim();
            CalculateCMVoucher();
        }
        private DataTable CreateInputVoucherDetailIMEIDTB(List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> InputVoucherDetailIMEIList)
        {
            DataTable dtb = new DataTable();
            dtb.TableName = "InputVoucherDetailIMEI";
            dtb.Columns.Add("INPUTVOUCHERDETAILID", typeof(string));
            dtb.Columns.Add("IMEI", typeof(string));
            dtb.Columns.Add("PINCODE", typeof(string));
            dtb.Columns.Add("NOTE", typeof(string));
            dtb.Columns.Add("IMEICHANGECHAIN", typeof(string));
            dtb.Columns.Add("ISHASWARRANTY", typeof(int));
            dtb.Columns.Add("CREATEDSTOREID", typeof(int));
            dtb.Columns.Add("INPUTSTOREID", typeof(int));
            dtb.Columns.Add("INPUTDATE", typeof(DateTime));
            dtb.Columns.Add("CREATEDUSER", typeof(string));
            dtb.Columns.Add("CREATEDDATE", typeof(DateTime));
            dtb.Columns.Add("UPDATEDUSER", typeof(string));
            dtb.Columns.Add("UPDATEDDATE", typeof(DateTime));
            dtb.Columns.Add("ISDELETED", typeof(int));
            dtb.Columns.Add("DELETEDUSER", typeof(string));
            dtb.Columns.Add("DELETEDDATE", typeof(DateTime));
            dtb.Columns.Add("COSTPRICE", typeof(decimal));
            dtb.Columns.Add("FIRSTPRICE", typeof(decimal));
            dtb.Columns.Add("INPUTUSER", typeof(string));
            DateTime dt = dtbServerDate;
            foreach (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objIMEI in InputVoucherDetailIMEIList)
            {
                dtb.Rows.Add("", objIMEI.IMEI, objIMEI.PINCode, objIMEI.Note, objIMEI.IMEIChangeCHAIN,
                    objIMEI.IsHasWarranty, objIMEI.CreatedStoreID, objIMEI.InputStoreID, dt, objIMEI.CreatedUser,
                    dt, objIMEI.UpdatedUser, objIMEI.UpdatedDate, objIMEI.IsDeleted, objIMEI.DeletedUser,
                    objIMEI.DeletedDate, objIMEI.CostPrice, objIMEI.FirstPrice, objIMEI.InputUser);
            }
            return dtb;
        }
        private void mnuItemIsAutoCreateIMEI_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvInputVoucherDetail.FocusedRowHandle < 0)
                    return;
                PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetail)grvInputVoucherDetail.GetRow(grvInputVoucherDetail.FocusedRowHandle);
                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = null;
                new ERP.MasterData.PLC.MD.PLCProduct().LoadInfo(ref objProduct, objInputVoucherDetail.ProductID);
                if (objProduct == null)
                {
                    MessageBox.Show(this, "Sản phẩm không tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (objProduct.IsRequirePINCode)
                {
                    MessageBox.Show(this, "Sản phẩm yêu cầu nhập Pincode không cho phép phát sinh IMEI tự động.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                frmAutoCreateIMEI frmAutoCreateIMEI1 = new frmAutoCreateIMEI(objProduct.ProductID);
                if (bolIsInputFromOrder)
                {
                    decimal decOrderQuantity = Math.Round(objInputVoucherDetail.OrderQuantity);
                    decimal decQuantity = Math.Round(objInputVoucherDetail.Quantity);
                    if (decQuantity < decOrderQuantity)
                    {
                        //int intNum = (int)(decOrderQuantity - decQuantity);
                        frmAutoCreateIMEI1.Quantity = (int)decOrderQuantity;
                    }
                    else
                    {
                        return;
                    }
                }
                frmAutoCreateIMEI1.ShowDialog();
                DataTable dtbIMEIList = frmAutoCreateIMEI1.IMEIList;
                if (dtbIMEIList == null || dtbIMEIList.Rows.Count < 1)
                    return;
                List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> InputVoucherDetailIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
                foreach (DataRow row in dtbIMEIList.Rows)
                {
                    PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.Input.WSInputVoucher.InputVoucherDetailIMEI();
                    objInputVoucherDetailIMEI.ProductID = objInputVoucherDetail.ProductID;
                    objInputVoucherDetailIMEI.ProductName = objInputVoucherDetail.ProductName;
                    objInputVoucherDetailIMEI.IMEI = row["IMEI"].ToString().Trim();
                    objInputVoucherDetailIMEI.Status = "";
                    objInputVoucherDetailIMEI.IsValidate = true;
                    objInputVoucherDetailIMEI.IsHasWarranty = true;
                    objInputVoucherDetailIMEI.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    InputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                }
                decimal OldQuantity = objInputVoucherDetail.Quantity;

                //objInputVoucherDetail.Quantity = objInputVoucherDetail.Quantity + dtbIMEIList.Rows.Count;
                //objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);

                objInputVoucherDetail.Quantity = dtbIMEIList.Rows.Count;
                objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);

                //Thêm InputVoucherDetailIMEIList vào InputVoucherDetail trên dòng focus
                var objInputVoucherDetail_Sel = from o in InputVoucherDetailList
                                                where o.ProductID == objInputVoucherDetail.ProductID
                                                select o;
                if (objInputVoucherDetail_Sel.Count() > 0)
                {
                    objInputVoucherDetail_Sel.First<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail>().InputVoucherDetailIMEIList = InputVoucherDetailIMEIList.ToArray();
                }
                //Ân thêm check số lượng cần nhập 03/10/2017
                if (bolIsInputFromOrder)
                {
                    objInputVoucherDetail.QuantityToInput = objInputVoucherDetail.QuantityToInput - (objInputVoucherDetail.Quantity - OldQuantity);
                    if (objInputVoucherDetail.Quantity > 0) objInputVoucherDetail.IsCheckQuantity = true;
                }
                grdInputVoucherDetail.RefreshDataSource();
                GetSumTotalMoney();
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi tạo imei tự động!", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tạo imei tự động!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mnuItemInputIMEI_Click(object sender, EventArgs e)
        {
            InputIMEI();
        }

        private void mnuItemDeleteProduct_Click(object sender, EventArgs e)
        {
            RemoveProduct();
        }

        private void grvInputVoucherDetail_DoubleClick(object sender, EventArgs e)
        {
            InputIMEI();
        }

        private void grvInputVoucherDetail_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName != string.Empty)
            {
                DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                if (Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["IsAutoCreateIMEI"])))
                {
                    e.Appearance.BackColor = Color.Violet;
                }
            }
        }

        private void cboInputTypeSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cboInputTypeSearch.ShowPopup();
        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdInputVoucherDetail);
        }

        private void mnuImportFile_Click(object sender, EventArgs e)
        {

        }


        private void mnuAddAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog objOpenFileDialog = new OpenFileDialog();
                objOpenFileDialog.Multiselect = true;
                objOpenFileDialog.Filter = "Doc Files (*.doc, *.docx)|*.doc; *docx|Excel Files (*.xls, *.xlsx)|*.xls; *.xlsx|PDF Files(*.pdf)|*.pdf|Compressed Files(*.zip, *.rar)|*.zip;*.rar|Image Files(*.jpg, *.jpe, *.jpeg, *.gif, *.png, *.bmp, *.tif, *.tiff)|*.jpg; *.jpe; *.jpeg; *.gif; *.png; *.bmp; *.tif; *.tiff";
                if (objOpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String[] strLocalFilePaths = objOpenFileDialog.FileNames;
                    foreach (string strLocalFilePath in strLocalFilePaths)
                    {
                        PLC.Input.WSInputVoucher.InputVoucher_Attachment objInputVoucher_Attachment = new PLC.Input.WSInputVoucher.InputVoucher_Attachment();
                        FileInfo objFileInfo = new FileInfo(strLocalFilePath);
                        var ListCheck = from o in InputVoucher_AttachmentList
                                        where o.FileName == objFileInfo.Name
                                        select o;
                        if (ListCheck.Count() > 0)
                            continue;
                        decimal FileSize = Math.Round(objFileInfo.Length / Convert.ToDecimal((1024 * 1024)), 2); //dung lượng tính theo MB
                        decimal FileSizeLimit = 2;//MB
                        if (FileSize > FileSizeLimit)
                        {
                            MessageBox.Show(this, "Vui lòng chọn tập tin đính kèm dung lượng không vượt quá 2 MB", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        objInputVoucher_Attachment.FileName = objFileInfo.Name;
                        objInputVoucher_Attachment.InputVoucherID = string.Empty;
                        objInputVoucher_Attachment.LocalFilePath = strLocalFilePath;
                        objInputVoucher_Attachment.CreatedDate = dtbServerDate;
                        objInputVoucher_Attachment.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objInputVoucher_Attachment.IsNew = true;
                        InputVoucher_AttachmentList.Add(objInputVoucher_Attachment);
                    }
                    grdAttachment.RefreshDataSource();
                }
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi thêm tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi thêm tập tin", objExce.ToString(), this.Name + " -> mnuDelAttachment_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void mnuDelAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvAttachment.FocusedRowHandle < 0)
                    return;
                PLC.Input.WSInputVoucher.InputVoucher_Attachment objInputVoucher_Attachment = (PLC.Input.WSInputVoucher.InputVoucher_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
                if (objInputVoucher_Attachment != null && InputVoucher_AttachmentList != null)
                {
                    if (!bolPermission_PM_InputVoucher_Attachment_DeleteAll && bolPermission_PM_InputVoucher_Attachment_Delete)
                    {
                        if (objInputVoucher_Attachment.CreatedUser != SystemConfig.objSessionUser.UserName)
                        {
                            MessageBox.Show(this, "Bạn không có quyền xóa file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    if (MessageBox.Show(this, "Bạn muốn xóa file đính kèm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        InputVoucher_AttachmentList.Remove(objInputVoucher_Attachment);
                        if (objInputVoucher_Attachment.AttachmentID != null && objInputVoucher_Attachment.AttachmentID.Trim() != string.Empty)
                        {
                            if (InputVoucher_AttachmentDelList == null)
                                InputVoucher_AttachmentDelList = new List<PLC.Input.WSInputVoucher.InputVoucher_Attachment>();
                            InputVoucher_AttachmentDelList.Add(objInputVoucher_Attachment);
                        }
                        grdAttachment.RefreshDataSource();
                    }
                }
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi xóa tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi xóa tập tin", objExce.ToString(), this.Name + " -> mnuDelAttachment_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (!bolPermission_PM_InputVoucher_Attachment_View)
            {
                MessageBox.Show(this, "Bạn không có quyền xem file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            PLC.Input.WSInputVoucher.InputVoucher_Attachment obj = (PLC.Input.WSInputVoucher.InputVoucher_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
            if (obj.IsNew == true)
            {
                MessageBox.Show(this, "File chưa cập nhật trên server!");
                return;
            }
            DownloadFile(obj.FileID, obj.FileName);
        }

        private void mnuFlex_Opening(object sender, CancelEventArgs e)
        {
            if (InputVoucherDetailList == null || InputVoucherDetailList.Count <= 0)
            {
                mnuItemIsAutoCreateIMEI.Enabled = false;
                mnuItemInputIMEI.Enabled = false;
                mnuItemDeleteProduct.Enabled = false;
                mnuItemInputIMEIByProduct.Enabled = false;
                mnuItemInputbyExcel.Enabled = false;
            }
            else
            {
                mnuItemIsAutoCreateIMEI.Enabled = true;
                mnuItemInputIMEI.Enabled = true;
                mnuItemDeleteProduct.Enabled = true;
                mnuItemInputIMEIByProduct.Enabled = true;
                //if (bolIsInputFromOrder)
                //Kiểm tra ẩn hiện
                bool bolCheck = false;
                foreach (var item in InputVoucherDetailList)
                {
                    if (item.QuantityRemain == 0 || item.Quantity == 0 || item.QuantityRemain != item.Quantity)
                    {
                        bolCheck = true;
                        break;
                    }
                }
                mnuItemInputbyExcel.Enabled = bolCheck;
            }
            if (grvInputVoucherDetail.FocusedRowHandle < 0)
                return;
            PLC.Input.WSInputVoucher.InputVoucherDetail obj = (PLC.Input.WSInputVoucher.InputVoucherDetail)grvInputVoucherDetail.GetRow(grvInputVoucherDetail.FocusedRowHandle);
            if (obj != null)
            {
                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = null;
                new ERP.MasterData.PLC.MD.PLCProduct().LoadInfo(ref objProduct, obj.ProductID);
                if (!objProduct.IsAutoCreateIMEI || !objInputType.IsRequireAutoCreateIMEI)
                    mnuItemIsAutoCreateIMEI.Enabled = false;
                if (!objProduct.IsRequestIMEI)
                    mnuItemInputIMEI.Enabled = false;
            }
        }

        private void mnuAttachment_Opening(object sender, CancelEventArgs e)
        {
            mnuAddAttachment.Enabled = bolPermission_PM_InputVoucher_Attachment_Add;
            mnuDelAttachment.Enabled = bolPermission_PM_InputVoucher_Attachment_Delete || bolPermission_PM_InputVoucher_Attachment_DeleteAll;
            mnuItemCaptureImage.Enabled = bolPermission_PM_InputVoucher_Attachment_Add;
            mnuItemScanImage.Enabled = bolPermission_PM_InputVoucher_Attachment_Add;
            if (InputVoucher_AttachmentList == null || InputVoucher_AttachmentList.Count <= 0)
            {
                mnuDelAttachment.Enabled = false;
            }
            if (objInputVoucher != null && objInputVoucher.IsPosted && !bolPermission_PM_InputVoucher_EditAfterPosted)
            {
                mnuAddAttachment.Enabled = false;
                mnuDelAttachment.Enabled = false;
                mnuItemScanImage.Enabled = false;
                mnuItemCaptureImage.Enabled = false;
            }
        }

        private void grvInputVoucherDetail_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (grvInputVoucherDetail.FocusedColumn.FieldName == "Quantity")
            {
                if (grvInputVoucherDetail.FocusedRowHandle < 0)
                    return;
                PLC.Input.WSInputVoucher.InputVoucherDetail obj = (PLC.Input.WSInputVoucher.InputVoucherDetail)grvInputVoucherDetail.GetRow(grvInputVoucherDetail.FocusedRowHandle);
                if (obj != null)
                {
                    ERP.MasterData.PLC.MD.WSProduct.Product objProduct = null;
                    new ERP.MasterData.PLC.MD.PLCProduct().LoadInfo(ref objProduct, obj.ProductID);
                    if (objProduct.IsRequestIMEI)
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        if (!obj.IsAllowDecimal)
                        {
                            txtItemQuantity.Mask.EditMask = "###,###,###,##0";
                        }
                        else
                        {
                            txtItemQuantity.Mask.EditMask = "###,###,###,##0.####";
                        }
                    }
                }
            }
            if (grvInputVoucherDetail.FocusedColumn.FieldName == "InputPrice")
            {
                if (bolIsInputFromOrder)//Từ đơn hàng thì không được chỉnh giá
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnSearchCustomer_Click(object sender, EventArgs e)
        {

        }

        private void txtPaymentCard_EditValueChanged(object sender, EventArgs e)
        {
            txtCashVND_EditValueChanged(null, null);
            cboPaymentCard_SelectionChangeCommitted(null, null);
        }

        private void cboPaymentCard_SelectionChangeCommitted(object sender, EventArgs e)
        {
            decimal decPaymentCard = 0;
            try
            {
                decPaymentCard = Convert.ToDecimal(txtPaymentCard.EditValue);
            }
            catch
            {
                decPaymentCard = Convert.ToDecimal(txtPaymentCard.EditValue);
                txtPaymentCard.EditValue = decPaymentCard;
            }

            if (decPaymentCard == 0 && cboPaymentCard.SelectedIndex > 0)
            {
                cboPaymentCard.SelectedIndex = 0;
                txtPaymentCard.Focus();
            }
            txtCardSpend.Properties.ReadOnly = cboPaymentCard.SelectedIndex <= 0;
            // txtCardSpend.EditValue = (Convert.ToDecimal(txtPaymentCard.EditValue) + GetCardSpend());
            txtCardSpend.EditValue = GetCardSpend();
        }


        private void lblCustomerID_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ERP.MasterData.DUI.Search.frmCustomerSearch frm = new MasterData.DUI.Search.frmCustomerSearch(false);
            frm.CustomerType = Library.AppCore.Constant.EnumType.CustomerType.ISPROVIDER;
            frm.ShowDialog();
            if (frm.Customer != null && frm.Customer.CustomerID > 0)
            {
                txtCustomerID.Text = Convert.ToString(frm.Customer.CustomerID);
                txtCustomerName.Text = frm.Customer.CustomerName;
                txtAddress.Text = frm.Customer.CustomerAddress;
                txtCustomerIDCard.Text = frm.Customer.CustomerIDCard;
                txtTaxID.Text = frm.Customer.CustomerTaxID;
                txtPhone.Text = frm.Customer.CustomerPhone;
            }
            else
            {
                if (txtCustomerID.Text != string.Empty)
                {
                    if (MessageBox.Show(this, "Bạn muốn bỏ chọn khách hàng hiện tại không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                }
                txtCustomerID.Text = string.Empty;
                txtCustomerName.Text = string.Empty;
                txtAddress.Text = string.Empty;
                txtCustomerIDCard.Text = string.Empty;
                txtTaxID.Text = string.Empty;
                txtPhone.Text = string.Empty;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            frmPopupNote frmPopupNote1 = new frmPopupNote();
            frmPopupNote1.Text = "Ghi chú hủy phiếu nhập";
            frmPopupNote1.ShowDialog();
            if (!frmPopupNote1.isAccept)
                return;
            objInputVoucher.ContentDeleted = frmPopupNote1.txtContent.Text;
            int intResult = new PLC.Input.PLCInputVoucher().Delete(objInputVoucher);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            if (intResult == 1)
            {
                MessageBox.Show(this, "Hủy phiếu nhập thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsReloadManager = true;
                this.Close();
            }
            else
                if (intResult == 2)
                {
                    MessageBox.Show(this, "Sản phẩm ở phiếu nhập này đã được xuất!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                    if (intResult == 3)
                    {
                        MessageBox.Show(this, "Sản phẩm ở phiếu nhập này đã được xuất chuyển kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                        if (intResult == 4)
                        {
                            MessageBox.Show(this, "Sản phẩm ở phiếu nhập này đã được xuất đổi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                            if (intResult == 5)
                            {
                                MessageBox.Show(this, "Sản phẩm ở phiếu nhập này đã được nhập trả!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                                if (intResult == 6)
                                {
                                    MessageBox.Show(this, "Sản phẩm ở phiếu nhập này đã được nhập đổi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }

        }

        private void grvInputVoucherDetail_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void radOld_CheckedChanged(object sender, EventArgs e)
        {
            if (!isEdit && bolIsInputFromOrder && radOld.Checked)
            {
                if (MessageBox.Show(this, "Bạn chắc chắn muốn chọn nhập sản phẩm cũ không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    radOld.Checked = false;
                    radIsNew.Checked = true;
                }
            }
            if (InputVoucherDetailList != null && InputVoucherDetailList.Count > 0)
            {
                foreach (PLC.Input.WSInputVoucher.InputVoucherDetail obj in InputVoucherDetailList)
                {
                    int intMonthOfWarranty = GetWarrantyByProductID(obj.ProductID, radIsNew.Checked);
                    if (intMonthOfWarranty == -1)
                        obj.ENDWarrantyDate = dtbServerDate;
                    else
                        obj.ENDWarrantyDate = dtbServerDate.AddMonths(intMonthOfWarranty);
                }
                grdInputVoucherDetail.RefreshDataSource();
            }
        }

        private void txtCashUSD_EditValueChanged(object sender, EventArgs e)
        {
            txtCashUSDExchange.EditValue = Convert.ToDecimal(txtCashUSD.EditValue) * Convert.ToDecimal(txtExchangeRate.EditValue);
            txtCashVND_EditValueChanged(null, null);
        }

        private void mnuItemExportData_Click(object sender, EventArgs e)
        {
            if (objInputVoucher != null && objInputVoucher.InputVoucherDetailList != null && objInputVoucher.InputVoucherDetailList.Length > 0)
            {
                DataTable dtbData = new PLC.Input.PLCInputVoucher().GetExportDataToSaleOrder(strInputVoucherID);
                if (dtbData == null)
                {
                    MessageBox.Show(this, "Lỗi lấy dữ liệu export cho đơn hàng bán", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                DataTable dtbExport = new DataTable();
                dtbExport.Columns.Add("Mã sản phẩm", typeof(string));
                dtbExport.Columns.Add("IMEI", typeof(string));
                dtbExport.Columns.Add("SL", typeof(decimal));
                dtbExport.TableName = "InputVoucherDetail";
                try
                {
                    foreach (DataRow row in dtbData.Rows)
                    {
                        DataRow rDetail = dtbExport.NewRow();
                        rDetail[0] = row["ProductID"].ToString().Trim();
                        rDetail[1] = row["IMEI"].ToString().Trim();
                        rDetail[2] = Convert.ToDecimal(row["QUANTITY"]);
                        dtbExport.Rows.Add(rDetail);
                    }
                    ExportExcel(this, dtbExport);
                }
                catch (Exception objExc)
                {
                    MessageBox.Show(this, "Lỗi xuất dữ liệu cho đơn hàng bán", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    SystemErrorWS.Insert("Lỗi xuất dữ liệu cho đơn hàng bán", objExc, DUIInventory_Globals.ModuleName);
                }
            }
        }

        private void mnuItemScanImage_Click(object sender, EventArgs e)
        {
            ScanImage();
        }

        private void mnuItemCaptureImage_Click(object sender, EventArgs e)
        {
            CaptureImage();
        }
        private void AutoEnterBarcode(string strBarcodeInput)
        {

            txtBarcode.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(strBarcodeInput);
            String strBarcode = txtBarcode.Text;
            String strRlt = BarcodeValidation(strBarcode, 1);
            if (!string.IsNullOrEmpty(strRlt))
                MessageBox.Show(this, strRlt, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            txtBarcode.Text = string.Empty;
            txtBarcode.Focus();
            grdInputVoucherDetail.RefreshDataSource();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (!bolPermission_PM_InputVoucher_Attachment_View)
            {
                MessageBox.Show(this, "Bạn không có quyền xem file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            PLC.Input.WSInputVoucher.InputVoucher_Attachment obj = (PLC.Input.WSInputVoucher.InputVoucher_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
            string strFileName = string.Empty;
            if (obj.IsNew == true)
            {
                strFileName = Path.GetFullPath(obj.LocalFilePath);
            }
            else
            {
                if (obj.LocalFilePath.Trim() == string.Empty)
                {
                    strFileName = LoadProcess(obj.FileID, obj.FileName);
                }
                else
                {
                    strFileName = Path.GetFullPath(obj.LocalFilePath);
                }
            }
            if (!string.IsNullOrWhiteSpace(strFileName))
            {
                Process.Start(strFileName);
            }
        }
        #endregion
        // <summary>
        /// Load url Process tu Server len pictureBox
        /// </summary>
        /// <param name="strFileID"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        private string LoadProcess(string strFileID, string strFileName)
        {
            string strURL = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(strFileID))
                {
                    //string[] strList = strURL.Split('.');
                    //strFileName = strList[strList.Length - 1];
                    //if (!Directory.Exists(strFileName))
                    //{
                    //    DirectoryInfo di = Directory.CreateDirectory(strURL);
                    //}
                    strURL = Path.GetTempPath() + @"NC_Attachment";
                    if (!Directory.Exists(strURL))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(strURL);
                    }
                    strURL = Path.GetTempPath() + @"NC_Attachment\" + strFileName;
                    ERP.FMS.DUI.DUIFileManager.DownloadFile(this, "FMSAplication_ProERP_InputVoucherAttachment", strFileID, string.Empty, strURL);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    }
                }
            }
            catch
            {
                MessageBox.Show(this, "Không thể tải hình lên được", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return strURL;
        }

        private void mnuItemInputIMEIByProduct_Click(object sender, EventArgs e)
        {
            InputIMEIByProduct();
        }

        private void repositoryItemTextEdit2_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (bolIsInputFromOrder)
            {
                if (grvInputVoucherDetail.FocusedRowHandle < 0)
                    return;
                PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetail)grvInputVoucherDetail.GetFocusedRow();
                decimal decQuantity = 0;

                if (e.NewValue == null || string.IsNullOrEmpty(e.NewValue.ToString().Trim()))
                {
                    e.NewValue = 0;
                    objInputVoucherDetail.Quantity = 0;
                    decimal.TryParse(e.NewValue.ToString(), out decQuantity);
                }
                else
                {
                    decimal.TryParse(e.NewValue.ToString(), out decQuantity);
                    if (decQuantity < 0)
                    {
                        e.NewValue = 0;
                        objInputVoucherDetail.Quantity = 0;
                        decQuantity = 0;
                    }
                }


                decimal decOldQuantity = 0;
                if (e.OldValue == null || string.IsNullOrEmpty(e.OldValue.ToString().Trim()))
                {
                    decOldQuantity = 0;
                    //objInputVoucherDetail.Quantity = 0;
                }
                else decimal.TryParse(e.OldValue.ToString(), out decOldQuantity);
                if (decQuantity <= 0)
                    objInputVoucherDetail.IsCheckQuantity = false;
                else
                    objInputVoucherDetail.IsCheckQuantity = true;
                if (decQuantity > objInputVoucherDetail.QuantityToInput + objInputVoucherDetail.Quantity)
                {
                    objInputVoucherDetail.Quantity = objInputVoucherDetail.QuantityToInput;
                    objInputVoucherDetail.QuantityToInput = 0;
                    return;
                }
                if (e.NewValue != null)
                {
                    objInputVoucherDetail.QuantityToInput = objInputVoucherDetail.QuantityToInput - decQuantity + decOldQuantity;
                    objInputVoucherDetail.Quantity = decQuantity;
                }
            }

        }

        private void mnuItemInputbyExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (InputVoucherDetailList == null || InputVoucherDetailList.Count == 0)
                    return;

                var objInputVoucherDetailList = InputVoucherDetailList.ToList();

                List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIList = null;
                objAllIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
                foreach (var item in InputVoucherDetailList)
                {
                    if (item.InputVoucherDetailIMEIList != null && item.InputVoucherDetailIMEIList.Count() > 0)
                        objAllIMEIList.AddRange(item.InputVoucherDetailIMEIList);
                }

                List<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail> dtbData = grdInputVoucherDetail.DataSource as List<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail>;

                if (bolIsInputFromOrder)
                {
                    if (dtbData.Where(x => x.QuantityToInput != x.Quantity).Count() == 0)
                    {
                        MessageBox.Show(this, "Số lượng IMEI đã nhập đủ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                frmExcelImport frm = new frmExcelImport(bolIsInputFromOrder, (bolIsInputFromOrder ? dtbData.Where(x => x.OrderQuantity > x.Quantity).ToList() : dtbData.ToList()));
                if (objAllIMEIList != null)
                    frm.InputVoucherDetailIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>(objAllIMEIList);
                frm.IsInputFromRetailInputPrice = false;//Nhập từ máy cũ
                frm.InputVoucherDetailList = InputVoucherDetailList.ToList();
                //var lstBackup = DeepClone(InputVoucherDetailList.ToList());
                frm.AllIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>(objAllIMEIList);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIResultList = null;
                    objAllIMEIResultList = frm.InputVoucherDetailIMEIList;
                    List<PLC.Input.WSInputVoucher.InputVoucherDetail> objAllResultList = null;
                    objAllResultList = frm.InputVoucherDetailList;
                    int i = 0;
                    foreach (var objInputVoucherDetail in objInputVoucherDetailList)
                    {
                        objInputVoucherDetail.InputVoucherDetailIMEIList = objAllIMEIResultList.Where(r => r.ProductID == objInputVoucherDetail.ProductID).ToArray();
                        //decimal OldQuantity = lstBackup[i].Quantity;
                        if (objInputVoucherDetail.IsRequestIMEI == true)
                            objInputVoucherDetail.Quantity = objInputVoucherDetail.InputVoucherDetailIMEIList.Count();
                        else
                        {
                            if (objAllResultList != null && objAllResultList.Count > 0 && objAllResultList[i].Quantity > 0)
                            {
                                if (bolIsInputFromOrder)
                                    objInputVoucherDetail.Quantity = (objInputVoucherDetail.QuantityRemain - objInputVoucherDetail.QuantityToInput) + objAllResultList[i].Quantity;
                                else
                                    objInputVoucherDetail.Quantity = objAllResultList[i].Quantity;
                            }
                        }

                        objInputVoucherDetail.Amount = objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice + (objInputVoucherDetail.Quantity * objInputVoucherDetail.InputPrice * objInputVoucherDetail.VAT / 100 * objInputVoucherDetail.VATPercent / 100);

                        i++;
                        if (bolIsInputFromOrder)
                        {
                            //objInputVoucherDetail.QuantityToInput = objInputVoucherDetail.QuantityToInput - (objInputVoucherDetail.Quantity - OldQuantity);
                            objInputVoucherDetail.QuantityToInput = objInputVoucherDetail.QuantityRemain - objInputVoucherDetail.Quantity;
                            if (objInputVoucherDetail.Quantity > 0)
                                objInputVoucherDetail.IsCheckQuantity = true;
                        }
                    }
                    grdInputVoucherDetail.RefreshDataSource();
                    GetSumTotalMoney();
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nhập imei theo sản phẩm!", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nhập imei theo sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;
                return (T)formatter.Deserialize(ms);
            }
        }

        private void chkCreateAutoInvoice_CheckedChanged(object sender, EventArgs e)
        {
            if (isEdit)
            {
                if (chkCreateAutoInvoice.Checked && bolPermissionRealInput)
                {
                    // LÊ VĂN ĐÔNG:
                    txtInVoiceID.ReadOnly = false;
                    txtInvoiceSymbol.ReadOnly = false;
                    txtDenominator.ReadOnly = false;
                    dtInvoiceDate.Enabled = true;
                    dtInvoiceDate.BackColor = SystemColors.Window;
                    txtInVoiceID.BackColor = SystemColors.Window;
                    txtInvoiceSymbol.BackColor = SystemColors.Window;
                    txtDenominator.BackColor = SystemColors.Window;
                    dtInvoiceDate.BackColor = SystemColors.Window;

                    if (!chkIsCheckRealInput.Checked)
                    {
                        if (MessageBox.Show(this, "Bạn có muốn thực nhập không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            chkIsCheckRealInput.Checked = true;
                            //txtCheckRealInputNote.Enabled = false;
                            txtCheckRealInputNote.Properties.ReadOnly = false;
                            txtCheckRealInputNote.BackColor = SystemColors.Window;
                            chkIsCheckRealInput.Enabled = false;
                            btnRealInputConfirm.Enabled = false;
                            bolIsCheckRealInput = true;
                        }
                        else
                        {
                            chkCreateAutoInvoice.Checked = false;
                        }
                    }
                }
                else
                {
                    if (bolPermissionRealInput && objInputVoucher != null && !objInputVoucher.IsCheckRealInput)
                    {
                        chkIsCheckRealInput.Checked = false;
                        chkIsCheckRealInput.Enabled = true;
                        btnRealInputConfirm.Enabled = true;
                        bolIsCheckRealInput = false;
                    }

                    txtInVoiceID.ReadOnly = true;
                    txtInvoiceSymbol.ReadOnly = true;
                    txtDenominator.ReadOnly = true;
                    dtInvoiceDate.Enabled = false;
                    txtInVoiceID.BackColor = SystemColors.Info;
                    txtInvoiceSymbol.BackColor = SystemColors.Info;
                    txtDenominator.BackColor = SystemColors.Info;
                    dtInvoiceDate.BackColor = SystemColors.Info;
                }
            }
            else
            {
                if (chkCreateAutoInvoice.Checked && bolPermission_PM_InputVoucher_EditInvoiceInfo)
                {
                    txtInVoiceID.ReadOnly = false;
                    txtInvoiceSymbol.ReadOnly = false;
                    txtDenominator.ReadOnly = false;
                    dtInvoiceDate.Enabled = true;
                    dtInvoiceDate.BackColor = SystemColors.Window;
                    txtInVoiceID.BackColor = SystemColors.Window;
                    txtInvoiceSymbol.BackColor = SystemColors.Window;
                    txtDenominator.BackColor = SystemColors.Window;
                }
                else
                {
                    txtInVoiceID.Text = string.Empty;
                    txtInvoiceSymbol.Text = string.Empty;
                    txtDenominator.Text = string.Empty;
                    dtInvoiceDate.EditValue = Globals.GetServerDateTime();

                    txtInVoiceID.ReadOnly = true;
                    txtInvoiceSymbol.ReadOnly = true;
                    txtDenominator.ReadOnly = true;
                    dtInvoiceDate.Enabled = false;
                    dtInvoiceDate.BackColor = SystemColors.Info;
                    txtInVoiceID.BackColor = SystemColors.Info;
                    txtInvoiceSymbol.BackColor = SystemColors.Info;
                    txtDenominator.BackColor = SystemColors.Info;
                }
            }
        }

        private void btnRealInputConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                List<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail> lstData = new List<PLC.Input.WSInputVoucher.InputVoucherDetail>();
                var dtbData = grdInputVoucherDetail.DataSource;
                if (dtbData != null)
                    lstData = (dtbData as ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail[]).ToList();
                foreach (ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail item in lstData)
                {
                    DataTable dtbResult = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_INPUTVOUCHERDETAILIMEI_GET",
                        new object[] { "@INPUTVOUCHERDETAILID", item.InputVoucherDetailID });
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->btnRealInputConfirm_Click");
                    }
                    if (dtbResult != null && dtbResult.Rows.Count > 0)
                    {
                        item.InputVoucherDetailIMEIList = ConvertDataTableToListImei(dtbResult).ToArray();
                    }
                }
                frmRealInputConfirm frm = new frmRealInputConfirm(lstData.Where(x => x.Quantity > 0).ToList());
                frm.InputVoucherDetailList = lstData;
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    chkIsCheckRealInput.Checked = true;
                    txtCheckRealInputNote.Text = frm.Note;
                    txtCheckRealInputNote.Enabled = false;
                    chkIsCheckRealInput.Enabled = false;
                    btnRealInputConfirm.Enabled = false;
                    bolIsCheckRealInput = true;
                    btnUpdate_Click(null, null);
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nhập imei theo sản phẩm!", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nhập imei theo sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> ConvertDataTableToListImei(DataTable dt)
        {

            var convertedList = (from rw in dt.AsEnumerable()
                                 select new ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetailIMEI()
                                 {
                                     //InputVoucherDetailID = Convert.ToString(rw["INPUTVOUCHERDETAILID"]),
                                     IMEI = Convert.ToString(rw["IMEI"])
                                     //,PINCode = Convert.ToString(rw["PINCODE"]),
                                     //Note = Convert.ToString(rw["NOTE"]),
                                     //IMEIChangeCHAIN = Convert.ToString(rw["IMEICHANGECHAIN"]),
                                     //IsHasWarranty = string.IsNullOrEmpty(rw["ISHASWARRANTY"].ToString()) ? false : Convert.ToBoolean(rw["ISHASWARRANTY"]),
                                     //CreatedStoreID = string.IsNullOrEmpty(rw["CREATEDSTOREID"].ToString()) ? 0 : Convert.ToInt32(rw["CREATEDSTOREID"]),
                                     //InputStoreID = string.IsNullOrEmpty(rw["INPUTSTOREID"].ToString()) ? 0 : Convert.ToInt32(rw["INPUTSTOREID"]),
                                     //InputDate = string.IsNullOrEmpty(rw["INPUTDATE"].ToString()) ? (DateTime?)null : Convert.ToDateTime(rw["INPUTDATE"]),
                                     //CreatedUser = Convert.ToString(rw["CREATEDUSER"]),
                                     //CreatedDate = string.IsNullOrEmpty(rw["CREATEDDATE"].ToString()) ? (DateTime?)null : Convert.ToDateTime(rw["CREATEDDATE"]),
                                     //UpdatedUser = Convert.ToString(rw["UPDATEDUSER"]),
                                     //UpdatedDate = string.IsNullOrEmpty(rw["UPDATEDDATE"].ToString()) ? (DateTime?)null : Convert.ToDateTime(rw["UPDATEDDATE"]),
                                     //IsDeleted = string.IsNullOrEmpty(rw["ISDELETED"].ToString()) ? false : Convert.ToBoolean(rw["ISDELETED"]),
                                     //DeletedUser = Convert.ToString(rw["DELETEDUSER"]),
                                     //DeletedDate = string.IsNullOrEmpty(rw["DELETEDDATE"].ToString()) ? (DateTime?)null : Convert.ToDateTime(rw["DELETEDDATE"]),
                                     //CostPrice = string.IsNullOrEmpty(rw["COSTPRICE"].ToString()) ? 0 : Convert.ToDecimal(rw["COSTPRICE"]),
                                     //FirstPrice = string.IsNullOrEmpty(rw["FIRSTPRICE"].ToString()) ? 0 : Convert.ToDecimal(rw["FIRSTPRICE"]),
                                     //InputUser = Convert.ToString(rw["INPUTUSER"])
                                 }).ToList();

            return convertedList;
        }

        private void txtInVoiceID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtInVoiceID_TextChanged(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"^\d$");
            if (!regex.IsMatch(txtInVoiceID.Text.Trim()))
            {
                txtInVoiceID.Text = string.Empty;
            }
        }

        #region Đăng bổ sung tác vụ
        //Đăng bổ sung
        private bool CheckInputInvoice()
        {
            try
            {
                if (chkCreateAutoInvoice.Checked)
                {
                    if (string.IsNullOrWhiteSpace(txtInVoiceID.Text.Trim()))
                    {
                        tabControl1.SelectedTab = tabPage1;
                        txtInVoiceID.Focus();
                        MessageBox.Show(this, "Bạn chưa nhập số hóa đơn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    if (string.IsNullOrWhiteSpace(txtInvoiceSymbol.Text.Trim()))
                    {
                        tabControl1.SelectedTab = tabPage1;
                        txtInvoiceSymbol.Focus();
                        MessageBox.Show(this, "Bạn chưa nhập ký hiệu hóa đơn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    if (string.IsNullOrWhiteSpace(txtDenominator.Text.Trim()))
                    {
                        tabControl1.SelectedTab = tabPage1;
                        txtDenominator.Focus();
                        MessageBox.Show(this, "Bạn chưa nhập mẫu số hóa đơn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    if (dtInvoiceDate.EditValue == null)
                    {
                        tabControl1.SelectedTab = tabPage1;
                        dtInvoiceDate.Focus();
                        MessageBox.Show(this, "Bạn chưa chọn ngày hóa đơn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    if (objInputType != null)
                    {

                        if (objInputType.InvoiceTransTypeID <= 0)
                        {
                            if (MessageBox.Show("Chưa chọn loại nghiệp vụ hóa đơn, không cho phép tạo hóa đơn, bạn có tiếp tục tạo phiếu nhập?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            {
                                return false;
                            }
                        }
                        else
                        {
                            DataTable dtbInvoiceTransactionType = Library.AppCore.DataSource.GetDataSource.GetInvoiceTransactionType().Copy();
                            DataRow[] rowGroup = dtbInvoiceTransactionType.Select("INVOICETRANSTYPEID=" + objInputType.InvoiceTransTypeID);
                            bool bolIsTaxDeclaration = Convert.ToBoolean(rowGroup[0]["ISTAXDECLARATION"]);
                            if (bolIsTaxDeclaration)
                                if (Convert.ToInt32(rowGroup[0]["GROUPSERVICEID"]) <= 0)
                                {
                                    MessageBox.Show(this, "Chưa thiết lập nhóm hàng hóa dịch vụ để tạo hóa đơn về cùng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return false;
                                }
                        }
                    }

                }

                return true;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowErrorMessage(this, "Lỗi kiểm tra thông tin hóa đơn!");
                SystemErrorWS.Insert("Lỗi kiểm tra thông tin hóa đơn", objExce.ToString());
                return false;
            }
        }
        /// <summary>
        /// cập nhật một hóa đơn nhập hàng 
        /// </summary>
        private bool UpdateDataForOrder(decimal decDiscount, decimal decTotalAmount, ERP.MasterData.PLC.MD.WSPOType.POType objPOType,
                ERP.MasterData.PLC.MD.WSStore.Store objStore, ERP.MasterData.PLC.MD.WSStore.Store objStoreCenterStore,
            ref int intVoucherTypeID, ref string strOutVoucherID)
        {
            //Library.AppCore.Forms.frmLoading.Show("Đang cập nhật dữ liệu!","Vui lòng đợi...");
            btnUpdate.Enabled = false;
            int intInputStoreID = cboStoreSearch.StoreID;
            string strMes = string.Empty;
            if (objPOType == null)
            {
                //Library.AppCore.Forms.frmWaitDialog.Close();
                bolIsFinish = true;
                MessageBox.Show(this, "Không tìm thấy loại đơn hàng mua", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;
            //Library.AppCore.DataSource.GetDataSource.stỏe
            objStoreChangeOrderType = new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfoFromCache(objPOType.STORECHANGEORDERTYPEID);
            if (objStoreChangeOrderType == null)
            {
                //Library.AppCore.Forms.frmWaitDialog.Close();
                bolIsFinish = true;
                MessageBox.Show(this, "Không tìm thấy loại yêu cầu xuất chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (!objStoreChangeOrderType.IsAutoReview)
            {
                //Library.AppCore.Forms.frmWaitDialog.Close();
                bolIsFinish = true;
                MessageBox.Show(this, "Loại yêu cầu xuất chuyển kho đang áp dụng mức duyệt. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeTypeForOrder = null;
            objStoreChangeTypeForOrder = new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfoFromCache(Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID));

            if (objStoreChangeTypeForOrder == null)
            {
                //Library.AppCore.Forms.frmWaitDialog.Close();
                bolIsFinish = true;
                MessageBox.Show(this, "Không tìm thấy loại xuất chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
            if (objStoreChangeTypeForOrder.IsStoreCustomer)
            {
                if (objStoreCenterStore.CustomerID <= 0)
                {
                    // Library.AppCore.Forms.frmWaitDialog.Close();
                    bolIsFinish = true;
                    MessageBox.Show("Chưa thiết lập khách hàng cho kho [" + objStoreCenterStore.StoreID + " - " + objStoreCenterStore.StoreName + "]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objStoreCenterStore.CustomerID));
            }
            else
            {
                objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objStoreChangeTypeForOrder.CustomerID));
                if (objCustomer.IsDefault)
                {
                    objCustomer.CustomerName = objStoreCenterStore.StoreName;
                    objCustomer.CustomerPhone = objStoreCenterStore.StorePhoneNumber;
                    objCustomer.CustomerTaxID = objStoreCenterStore.TaxCode;
                    objCustomer.CustomerAddress = objStoreCenterStore.StoreAddress;
                    //Nếu khác công ty
                    //if (objStoreChangeType.StoreChangeTypeID == 2 || objStoreChangeType.StoreChangeTypeID == 4)
                    if (objStoreChangeTypeForOrder.CheckCompanyType == 2)
                        objCustomer.CustomerAddress = objStoreCenterStore.TaxAddress;
                }
            }
            //new ERP.MasterData.PLC.MD.PLCCustomer().LoadInfo(ref objCustomer, objStoreCenterStore.CustomerID);
            //if (objCustomer == null)
            //{
            //    MessageBox.Show("Chưa thiết lập khách hàng cho kho trung tâm [" + objStoreCenterStore.StoreID + " - " + objStoreCenterStore.StoreName + "]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return false;
            //}
            ERP.MasterData.PLC.MD.WSInputType.InputType objInputTypebySCType = new MasterData.PLC.MD.PLCInputType().LoadInfo(Convert.ToInt32(objStoreChangeTypeForOrder.InputTypeID));
            ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputTypeCenter = null;
            new MasterData.PLC.MD.PLCOutputType().LoadInfo(ref objOutputTypeCenter, Convert.ToInt32(objStoreChangeTypeForOrder.OutputTypeID));
            int intTransportTypeID = 0;
            var drTransportTypeID = Library.AppCore.DataSource.GetDataSource.GetTransportType().Select("IsDefault=1");
            if (drTransportTypeID != null && drTransportTypeID.Count() > 0)
            {
                int.TryParse(drTransportTypeID[0]["TransportTypeID"].ToString(), out intTransportTypeID);
            }
            else
            {
                //Library.AppCore.Forms.frmWaitDialog.Close();
                bolIsFinish = true;
                MessageBox.Show("Không tìm thấy phương tiện vận chuyển mặc định!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (!CheckSameProvince(objStoreChangeTypeForOrder, objStoreCenterStore, objStore))
            {
                return false;
            }

            try
            {
                if (!IsEdit)//Thêm mới
                {
                    decimal decTotalQuantity = InputVoucherDetailList.Sum(x => x.Quantity);
                    string strStoreChangeOrderID = new ERP.Inventory.PLC.StoreChange.PLCStoreChangeOrder().GetStoreChangeOrderNewID(SystemConfig.intDefaultStoreID);
                    ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objInputVoucherCenter = InputVoucherCenter(decDiscount, decTotalAmount, objStoreCenterStore.StoreID, objOutputTypeCenter, objInputTypebySCType);
                    objInputVoucher = CreateInputVoucher(decDiscount, decTotalAmount, objCustomer, objStore, objStoreChangeTypeForOrder);
                    ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrder objStoreChangeOrder = CreateStoreChangeOrder(objStore, objStoreCenterStore, decTotalQuantity, objStoreChangeOrderType, strStoreChangeOrderID, intTransportTypeID);
                    PLC.Input.WSInputVoucher.StoreChange objStoreChange = CreateStoreChange(objStore, objStoreCenterStore, objStoreChangeTypeForOrder, strStoreChangeOrderID, intTransportTypeID);
                    PLC.Input.WSInputVoucher.VAT_Invoice objVAT_Invoice = null;
                    PLC.Input.WSInputVoucher.VAT_PInvoice objVAT_PInvoice = null;
                    string strFormTypeBranchID = "";
                    if (objStoreChangeOrderType.CreateVATInvoice == 2)
                    {
                        if (!objInputTypebySCType.IsReturnSale && !objInputTypebySCType.IsReturnOutput && objInputTypebySCType.InvoiceTransTypeID > 0)
                        {
                            objVAT_Invoice = CreateVATInvoice(objStoreChange.OutputVoucherBO, objStoreChangeOrderType, objStoreChangeTypeForOrder, ref strFormTypeBranchID);
                            if (objVAT_Invoice == null)
                                return false;
                            objVAT_PInvoice = CreateNewInputVATInvoice(this, objInputTypebySCType, objInputVoucher);
                            if (objVAT_PInvoice == null)
                                return false;
                        }
                    }
                    string strInputVoucherID = string.Empty;
                    string strInputVoucherCenterID = string.Empty;
                    string strStoreChangeID = string.Empty;
                    string strOutputVoucherID = string.Empty;
                    string strVATInvoiceID = string.Empty;
                    var objResultMessage = new PLC.Input.PLCInputVoucher().InsertAll(objInputVoucher, objInputVoucherCenter, objStoreChangeOrder, objStoreChange, objVAT_Invoice, strFormTypeBranchID, objVAT_PInvoice, ref strInputVoucherID, ref strInputVoucherCenterID, ref strStoreChangeID, ref strStoreChangeOrderID, ref strOutputVoucherID, ref strVATInvoiceID);
                    if (!objResultMessage.IsError)
                    {
                        if (!string.IsNullOrWhiteSpace(strVATInvoiceID))
                        {
                            ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
                            objPLCPrintVAT.CreateVATSInvoiceByVATInvoiceID(strVATInvoiceID);
                        }
                        strMes = "Tạo phiếu nhập thành công";
                        objInputVoucher.InputVoucherID = strInputVoucherID;
                        bolComplete = true;
                        #region hóa đơn
                        //if (objInputType != null && chkCreateAutoInvoice.Checked && objInputType.InvoiceTransTypeID > 0 && !string.IsNullOrWhiteSpace(objInputVoucher.InputVoucherID) && !objInputType.IsReturnSale && !objInputType.IsReturnOutput)
                        //{
                        //    ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice objVAT_PInvoiceMain = new PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice();
                        //    ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objstrInputVoucherCenter = new PLC.Input.PLCInputVoucher().LoadInfo(strInputVoucherCenterID);
                        //    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        //    {
                        //        //Library.AppCore.Forms.frmWaitDialog.Close();
                        //        bolIsFinish = true;
                        //        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        //        return false;
                        //    }

                        //    if (!CreateInputVATInvoice(this, ref objVAT_PInvoiceMain, objstrInputVoucherCenter, objStoreCenterStore.StoreID))
                        //    {
                        //        //Library.AppCore.Forms.frmWaitDialog.Close();
                        //        bolIsFinish = true;
                        //        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Lỗi tạo hóa đơn từ phiếu nhập!");
                        //        return false;
                        //    }
                        //}

                        #endregion
                    }
                    else
                    {
                        bolIsFinish = true;
                        MessageBox.Show(this, "Lỗi khi thêm mới một hóa đơn nhập hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                txtInputVoucherID.Text = objInputVoucher.InputVoucherID;
                Library.AppCore.Forms.frmWaitDialog.Close();
                MessageBox.Show(this, strMes, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (!isEdit)
                    PrintVoucher(objInputVoucher.InputVoucherID);


            }
            catch (Exception objExce)
            {
                //Library.AppCore.Forms.frmLoading.Close();
                bolIsFinish = true;
                SystemErrorWS.Insert("Lỗi khi thêm mới một hóa đơn nhập hàng", objExce, PLCInventory_Globals.ModuleName);
                btnUpdate.Enabled = true;
                return false;
            }
            bolIsReloadManager = true;
            this.Close();
            return true;
        }

        private ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher CreateInputVoucher(decimal decDiscount, decimal decTotalAmount,
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer, ERP.MasterData.PLC.MD.WSStore.Store objStore,
            ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType)
        {
            ERP.MasterData.PLC.MD.WSInputType.InputType objInputTypeStoreChange = new MasterData.PLC.MD.PLCInputType().LoadInfo(Convert.ToInt32(objStoreChangeType.InputTypeID));
            DateTime dtmBeginTime = Library.AppCore.Globals.GetServerDateTime();
            int intDiscountReasonID = Convert.ToInt32(cboDiscountReason.SelectedValue);
            objInputVoucher.OrderID = txtOrderID.Text.Trim();
            objInputVoucher.InVoiceID = "0000000";
            objInputVoucher.InVoiceSymbol = "0000000";
            objInputVoucher.Denominator = "0000000";
            objInputVoucher.CustomerID = objCustomer.CustomerID;
            objInputVoucher.CustomerName = objCustomer.CustomerName;
            objInputVoucher.CustomerAddress = objCustomer.CustomerAddress;
            objInputVoucher.CustomerPhone = objCustomer.CustomerPhone;
            objInputVoucher.CustomerTaxID = objCustomer.CustomerTaxID;
            objInputVoucher.CustomerEmail = objCustomer.CustomerEmail;
            objInputVoucher.CustomerIDCard = objCustomer.CustomerIDCard;
            objInputVoucher.IdCardIssueDate = dtIDCardIssueDate.Value;
            objInputVoucher.IdCardIssuePlace = txtIDCardIssuePlace.Text;
            objInputVoucher.Note = txtNote.Text.Trim();
            objInputVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
            objInputVoucher.InputStoreID = objStore.StoreID;
            objInputVoucher.InputTypeID = Convert.ToInt32(objInputTypeStoreChange.InputTypeID);
            objInputVoucher.IdCardIssueDate = dtIDCardIssueDate.Value;
            objInputVoucher.IdCardIssuePlace = txtIDCardIssuePlace.Text;
            objInputVoucher.PayableTypeID = Convert.ToInt32(cboPayableType.SelectedValue);
            objInputVoucher.CurrencyUnitID = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
            objInputVoucher.DiscountReasonID = intDiscountReasonID;
            objInputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
            objInputVoucher.InVoiceDate = dtInvoiceDate.DateTime;
            dtInputDate.Value = dtbServerDate;
            objInputVoucher.InputDate = dtInputDate.Value;
            objInputVoucher.PayableDate = dtPayableTime.Value;
            objInputVoucher.Discount = decDiscount;
            objInputVoucher.CurrencyExchange = Convert.ToDecimal(txtExchangeRate.EditValue);
            objInputVoucher.Content = txtInputContent.Text;
            objInputVoucher.ProtectPriceDiscount = Convert.ToDecimal(txtProtectPriceDiscount.EditValue);
            decimal decTotalAmountBF = 0;
            decimal decTotalVAT = 0;
            GetTotal(ref decTotalAmountBF, ref decTotalVAT);
            objInputVoucher.TotalAmountBFT = decTotalAmountBF;
            objInputVoucher.TotalVAT = decTotalVAT;
            objInputVoucher.TotalAmount = decTotalAmount;
            objInputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
            objInputVoucher.StaffUser = SystemConfig.objSessionUser.UserName;
            objInputVoucher.IsNew = radIsNew.Checked;
            objInputVoucher.ProtectPriceDiscount = Convert.ToDecimal(txtProtectPriceDiscount.EditValue);
            objInputVoucher.IsReturnWithFee = false;
            objInputVoucher.IsCheckRealInput = objInputTypeStoreChange.IsAutoCheckRealInput;
            objInputVoucher.ISStoreChange = false;
            objInputVoucher.IsPosted = false;
            objInputVoucher.IsCreateOutVoucher = objInputTypeStoreChange.IsCreateOutVoucher;
            objInputVoucher.DeliveryNoteDate = Convert.ToDateTime(dtmDeliveryNoteDate.EditValue);
            //objInputVoucher.IsAutoCreateInvoice = chkCreateAutoInvoice.Checked;
            if (bolIsInputFromOrder)
            {
                List<PLC.Input.WSInputVoucher.InputVoucherDetail> filteredInputVoucherDetailList = InputVoucherDetailList.Where(x => x.IsCheckQuantity == true).ToList();
                objInputVoucher.InputVoucherDetailList = filteredInputVoucherDetailList.ToArray();
            }
            objInputVoucher.InputVoucher_AttachmnetList = InputVoucher_AttachmentList.ToArray();
            objInputVoucher.TransportCustomerID = cboTransportCustomer.CustomerID;
            //Tạo Phiếu Nhập không có tạo Phiếu Chi
            //if (objInputType.IsCreateOutVoucher)
            //{
            //    ERP.MasterData.PLC.MD.PLCCustomer objPCLCustomer = new MasterData.PLC.MD.PLCCustomer();
            //    //ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
            //    //objPCLCustomer.LoadInfo(ref objCustomer, Convert.ToInt32(cboCustomer.SelectedValue));
            //    //Thông tin Phiếu Chi
            //    if (objVoucher == null)
            //        objVoucher = new PLC.Input.WSInputVoucher.Voucher();
            //    objVoucher.InvoiceID = txtInVoiceID.Text.Trim();
            //    objVoucher.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
            //    //objVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text); Thiên rào, đã có ở dưới
            //    objVoucher.Discount = decDiscount + Convert.ToDecimal(txtProtectPriceDiscount.EditValue);
            //    objVoucher.CurrencyUnitID = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
            //    objVoucher.CurrencyExchange = Convert.ToDecimal(txtExchangeRate.EditValue);
            //    objVoucher.TotalMoney = decTotalAmount;
            //    //objVoucher.OriginateStoreID = SystemConfig.intDefaultStoreID;
            //    objVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
            //    objVoucher.VoucherStoreID = intInputStoreID;
            //    objVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
            //    objVoucher.CashierUser = SystemConfig.objSessionUser.UserName;
            //    objVoucher.VoucherDate = dtSettlementDate.Value;
            //    objVoucher.InvoiceDate = dtInvoiceDate.DateTime;
            //    objVoucher.VoucherTypeID = Convert.ToInt32(cboVoucherType.SelectedValue);

            //    objVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text);
            //    objVoucher.CustomerName = txtCustomerName.Text;
            //    objVoucher.CustomerAddress = txtAddress.Text;
            //    objVoucher.CustomerPhone = txtPhone.Text;
            //    objVoucher.CustomerTaxID = txtTaxID.Text;
            //    objVoucher.CustomerEmail = string.Empty;
            //    objVoucher.CustomerIDCard = txtCustomerIDCard.Text;
            //    //objVoucher.IdCardIssueDate = dtIDCardIssueDate.Value;
            //    //objVoucher.IdCardIssuePlace = txtIDCardIssuePlace.Text;

            //    objVoucher.ProvinceID = 1;
            //    objVoucher.DistrictID = 1;
            //    objVoucher.Gender = true;
            //    objVoucher.AgeRangeID = 1;

            //    objVoucher.TotalLiquidate = Convert.ToDecimal(txtTotalLiquidate.EditValue);
            //    objVoucher.Debt = Convert.ToDecimal(txtDebt.EditValue);
            //    objVoucher.TotalAdvance = Convert.ToDecimal("0");
            //    objVoucher.VAT = 10;
            //    objVoucher.VoucherStoreID = intInputStoreID;
            //    objVoucher.OrderID = txtOrderID.Text.Trim();

            //    //29/11/2016: Thiên bổ sung nội dung phiếu chi
            //    objVoucher.Content = txtContent.Text;

            //    //Thông tin chi tiết Phiếu Chi
            //    decimal decCashVND = 0;
            //    decimal decCashUSD = 0;
            //    decimal decCashUSDExchange = 0;
            //    decimal decMoneyCard = 0;
            //    decimal decCardSpend = 0;

            //    if (!Convert.IsDBNull(txtCashVND.Text) && txtCashVND.Text.Trim() != string.Empty)
            //        decCashVND = Convert.ToDecimal(txtCashVND.EditValue);
            //    decCashUSD = Convert.ToDecimal(txtCashUSD.EditValue);
            //    decCashUSDExchange = Convert.ToDecimal(txtCashUSDExchange.EditValue);
            //    decMoneyCard = Convert.ToDecimal(txtPaymentCard.EditValue);
            //    decCardSpend = Convert.ToDecimal(txtCardSpend.EditValue);

            //    List<PLC.Input.WSInputVoucher.VoucherDetail> VoucherDetailList = new List<PLC.Input.WSInputVoucher.VoucherDetail>();
            //    if (((decCashVND > 0) || (decCashUSDExchange > 0) || (decMoneyCard > 0)))
            //    {
            //        PLC.Input.WSInputVoucher.VoucherDetail objVoucherDetail = new PLC.Input.WSInputVoucher.VoucherDetail();
            //        objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
            //        objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
            //        objVoucherDetail.VNDCash = decCashVND;
            //        objVoucherDetail.VoucherDate = objVoucher.VoucherDate;

            //        objVoucherDetail.ForeignCash = decCashUSD;
            //        objVoucherDetail.ForeignCashExchange = decCashUSDExchange;
            //        objVoucherDetail.PaymentCardAmount = decMoneyCard;
            //        objVoucherDetail.PaymentCardSpend = decCardSpend;
            //        objVoucherDetail.PaymentCardID = Convert.ToInt32(cboPaymentCard.SelectedValue);
            //        objVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
            //        objVoucherDetail.VoucherStoreID = intInputStoreID;
            //        VoucherDetailList.Add(objVoucherDetail);
            //    }
            //    objVoucher.VoucherDetailList = VoucherDetailList.ToArray();
            //    objInputVoucher.Voucher = objVoucher;
            //}
            objInputVoucher.IsInputFromOrder = bolIsInputFromOrder;
            objInputVoucher.IsInputFromRetailInputPrice = bolIsInputFromRetailInput;
            objInputVoucher.RetailInputPriceID = RetailInputPriceID;
            //GetNotifyUser
            GetListNotification_UserList(objInputType.InputTypeID, 1, objInputVoucher.InputStoreID, 2);
            GetListNotification_UserList(objInputType.InputTypeID, 1, objInputVoucher.CreatedStoreID, 2);
            objInputVoucher.UserNotifyData = dtbNotification_User;
            if (!objInputVoucher.IsInputFromRetailInputPrice)
            {
                foreach (var item in objInputVoucher.InputVoucherDetailList)
                {
                    if (item.InputVoucherDetailIMEIList != null)
                    {
                        item.dtbInputVoucherDetailIMEIDTB = CreateInputVoucherDetailIMEIDTB(item.InputVoucherDetailIMEIList.ToList());
                    }
                }
            }
            return objInputVoucher;
        }

        private ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher InputVoucherCenter(decimal decDiscount, decimal decTotalAmount, int intCenterStoreID,
            ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputTypeCenter,
            ERP.MasterData.PLC.MD.WSInputType.InputType objInputTypeCenter)
        {
            ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objInputVoucherCenter = new PLC.Input.WSInputVoucher.InputVoucher();
            DateTime dtmBeginTime = Library.AppCore.Globals.GetServerDateTime();
            int intDiscountReasonID = Convert.ToInt32(cboDiscountReason.SelectedValue);
            objInputVoucherCenter.OrderID = txtOrderID.Text.Trim();
            objInputVoucherCenter.InVoiceID = txtInVoiceID.Text;
            objInputVoucherCenter.InVoiceSymbol = txtInvoiceSymbol.Text;
            objInputVoucherCenter.Denominator = txtDenominator.Text;
            objInputVoucherCenter.CustomerID = Convert.ToInt32(txtCustomerID.Text);
            objInputVoucherCenter.CustomerName = txtCustomerName.Text;
            objInputVoucherCenter.CustomerAddress = txtAddress.Text;
            objInputVoucherCenter.CustomerPhone = txtPhone.Text;
            objInputVoucherCenter.CustomerTaxID = txtTaxID.Text;
            objInputVoucherCenter.CustomerEmail = string.Empty;
            objInputVoucherCenter.CustomerIDCard = txtCustomerIDCard.Text;
            objInputVoucherCenter.IdCardIssueDate = dtIDCardIssueDate.Value;
            objInputVoucherCenter.IdCardIssuePlace = txtIDCardIssuePlace.Text;
            objInputVoucherCenter.Note = txtNote.Text.Trim();
            objInputVoucherCenter.CreatedStoreID = SystemConfig.intDefaultStoreID;
            objInputVoucherCenter.InputStoreID = intCenterStoreID;
            objInputVoucherCenter.InputTypeID = objInputType.InputTypeID;
            objInputVoucherCenter.IdCardIssueDate = dtIDCardIssueDate.Value;
            objInputVoucherCenter.IdCardIssuePlace = txtIDCardIssuePlace.Text;
            objInputVoucherCenter.PayableTypeID = Convert.ToInt32(cboPayableType.SelectedValue);
            objInputVoucherCenter.CurrencyUnitID = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
            objInputVoucherCenter.DiscountReasonID = intDiscountReasonID;
            objInputVoucherCenter.CreatedUser = SystemConfig.objSessionUser.UserName;
            objInputVoucherCenter.InVoiceDate = dtInvoiceDate.DateTime;
            dtInputDate.Value = dtbServerDate;
            objInputVoucherCenter.InputDate = dtInputDate.Value;
            objInputVoucherCenter.PayableDate = dtPayableTime.Value;
            objInputVoucherCenter.Discount = decDiscount;
            objInputVoucherCenter.CurrencyExchange = Convert.ToDecimal(txtExchangeRate.EditValue);
            objInputVoucherCenter.Content = txtInputContent.Text;
            objInputVoucherCenter.ProtectPriceDiscount = Convert.ToDecimal(txtProtectPriceDiscount.EditValue);
            decimal decTotalAmountBF = 0;
            decimal decTotalVAT = 0;
            GetTotal(ref decTotalAmountBF, ref decTotalVAT);
            objInputVoucherCenter.TotalAmountBFT = decTotalAmountBF;
            objInputVoucherCenter.TotalVAT = decTotalVAT;
            objInputVoucherCenter.TotalAmount = decTotalAmount;
            objInputVoucherCenter.CreatedUser = SystemConfig.objSessionUser.UserName;
            objInputVoucherCenter.StaffUser = SystemConfig.objSessionUser.UserName;
            objInputVoucherCenter.IsNew = radIsNew.Checked;
            objInputVoucherCenter.ProtectPriceDiscount = Convert.ToDecimal(txtProtectPriceDiscount.EditValue);

            objInputVoucherCenter.IsReturnWithFee = false;
            objInputVoucherCenter.IsCheckRealInput = true;
            objInputVoucherCenter.ISStoreChange = false;
            objInputVoucherCenter.IsPosted = false;
            objInputVoucherCenter.IsCreateOutVoucher = objInputType.IsCreateOutVoucher;
            objInputVoucherCenter.IsAutoCreateInvoice = chkCreateAutoInvoice.Checked;
            objInputVoucherCenter.DeliveryNoteDate = Convert.ToDateTime(dtmDeliveryNoteDate.EditValue);

            if (bolIsInputFromOrder)
            {
                if (bolIsCheckInvoiceFirst)
                {
                    InputVoucherDetailList.ForEach(x => { x.QuantityRemain = 0; x.InvoiceQuantity = x.Quantity; });
                }
                List<PLC.Input.WSInputVoucher.InputVoucherDetail> filteredInputVoucherDetailList = InputVoucherDetailList.Where(x => x.IsCheckQuantity == true).ToList();
                objInputVoucherCenter.InputVoucherDetailList = filteredInputVoucherDetailList.ToArray();
            }
            else
            {
                objInputVoucherCenter.InputVoucherDetailList = InputVoucherDetailList.ToArray();
            }
            objInputVoucherCenter.InputVoucher_AttachmnetList = InputVoucher_AttachmentList.ToArray();
            objInputVoucherCenter.TransportCustomerID = cboTransportCustomer.CustomerID;
            //Tạo Phiếu Nhập không có tạo Phiếu Chi
            if (objInputType.IsCreateOutVoucher)
            {
                ERP.MasterData.PLC.MD.PLCCustomer objPCLCustomer = new MasterData.PLC.MD.PLCCustomer();
                //ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
                //objPCLCustomer.LoadInfo(ref objCustomer, Convert.ToInt32(cboCustomer.SelectedValue));
                //Thông tin Phiếu Chi
                if (objVoucher == null)
                    objVoucher = new PLC.Input.WSInputVoucher.Voucher();
                objVoucher.InvoiceID = txtInVoiceID.Text.Trim();
                objVoucher.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                //objVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text); Thiên rào, đã có ở dưới
                objVoucher.Discount = decDiscount + Convert.ToDecimal(txtProtectPriceDiscount.EditValue);
                objVoucher.CurrencyUnitID = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
                objVoucher.CurrencyExchange = Convert.ToDecimal(txtExchangeRate.EditValue);
                objVoucher.TotalMoney = decTotalAmount;
                //objVoucher.OriginateStoreID = SystemConfig.intDefaultStoreID;
                objVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objVoucher.VoucherStoreID = intCenterStoreID;
                objVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objVoucher.CashierUser = SystemConfig.objSessionUser.UserName;
                objVoucher.VoucherDate = dtSettlementDate.Value;
                objVoucher.InvoiceDate = dtInvoiceDate.DateTime;
                objVoucher.VoucherTypeID = Convert.ToInt32(cboVoucherType.SelectedValue);

                objVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                objVoucher.CustomerName = txtCustomerName.Text;
                objVoucher.CustomerAddress = txtAddress.Text;
                objVoucher.CustomerPhone = txtPhone.Text;
                objVoucher.CustomerTaxID = txtTaxID.Text;
                objVoucher.CustomerEmail = string.Empty;
                objVoucher.CustomerIDCard = txtCustomerIDCard.Text;
                //objVoucher.IdCardIssueDate = dtIDCardIssueDate.Value;
                //objVoucher.IdCardIssuePlace = txtIDCardIssuePlace.Text;

                objVoucher.ProvinceID = 1;
                objVoucher.DistrictID = 1;
                objVoucher.Gender = true;
                objVoucher.AgeRangeID = 1;

                objVoucher.TotalLiquidate = Convert.ToDecimal(txtTotalLiquidate.EditValue);
                objVoucher.Debt = Convert.ToDecimal(txtDebt.EditValue);
                objVoucher.TotalAdvance = Convert.ToDecimal("0");
                objVoucher.VAT = 10;
                objVoucher.VoucherStoreID = intCenterStoreID;
                objVoucher.OrderID = txtOrderID.Text.Trim();

                //29/11/2016: Thiên bổ sung nội dung phiếu chi
                objVoucher.Content = txtContent.Text;

                //Thông tin chi tiết Phiếu Chi
                decimal decCashVND = 0;
                decimal decCashUSD = 0;
                decimal decCashUSDExchange = 0;
                decimal decMoneyCard = 0;
                decimal decCardSpend = 0;

                if (!Convert.IsDBNull(txtCashVND.Text) && txtCashVND.Text.Trim() != string.Empty)
                    decCashVND = Convert.ToDecimal(txtCashVND.EditValue);
                decCashUSD = Convert.ToDecimal(txtCashUSD.EditValue);
                decCashUSDExchange = Convert.ToDecimal(txtCashUSDExchange.EditValue);
                decMoneyCard = Convert.ToDecimal(txtPaymentCard.EditValue);
                decCardSpend = Convert.ToDecimal(txtCardSpend.EditValue);

                List<PLC.Input.WSInputVoucher.VoucherDetail> VoucherDetailList = new List<PLC.Input.WSInputVoucher.VoucherDetail>();
                if (((decCashVND > 0) || (decCashUSDExchange > 0) || (decMoneyCard > 0)))
                {
                    PLC.Input.WSInputVoucher.VoucherDetail objVoucherDetail = new PLC.Input.WSInputVoucher.VoucherDetail();
                    objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                    objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objVoucherDetail.VNDCash = decCashVND;
                    objVoucherDetail.VoucherDate = objVoucher.VoucherDate;

                    objVoucherDetail.ForeignCash = decCashUSD;
                    objVoucherDetail.ForeignCashExchange = decCashUSDExchange;
                    objVoucherDetail.PaymentCardAmount = decMoneyCard;
                    objVoucherDetail.PaymentCardSpend = decCardSpend;
                    objVoucherDetail.PaymentCardID = Convert.ToInt32(cboPaymentCard.SelectedValue);
                    objVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    objVoucherDetail.VoucherStoreID = intCenterStoreID;
                    VoucherDetailList.Add(objVoucherDetail);
                }
                objVoucher.VoucherDetailList = VoucherDetailList.ToArray();
                objInputVoucherCenter.Voucher = objVoucher;
            }
            objInputVoucherCenter.IsInputFromOrder = bolIsInputFromOrder;
            objInputVoucherCenter.IsInputFromRetailInputPrice = bolIsInputFromRetailInput;
            objInputVoucherCenter.RetailInputPriceID = RetailInputPriceID;
            //GetNotifyUser
            GetListNotification_UserList(objInputType.InputTypeID, 1, objInputVoucherCenter.InputStoreID, 2);
            GetListNotification_UserList(objInputType.InputTypeID, 1, objInputVoucherCenter.CreatedStoreID, 2);
            objInputVoucherCenter.UserNotifyData = dtbNotification_User;
            if (!objInputVoucherCenter.IsInputFromRetailInputPrice)
            {
                foreach (var item in objInputVoucherCenter.InputVoucherDetailList)
                {
                    if (item.InputVoucherDetailIMEIList != null)
                    {
                        item.dtbInputVoucherDetailIMEIDTB = CreateInputVoucherDetailIMEIDTB(item.InputVoucherDetailIMEIList.ToList());
                    }
                }
            }
            return objInputVoucherCenter;
        }

        private ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrder CreateStoreChangeOrder(ERP.MasterData.PLC.MD.WSStore.Store objStore, ERP.MasterData.PLC.MD.WSStore.Store objStoreCenterStore, decimal decTotalQuantity,
            ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType, string strStoreChangeOrderID, int intTransportTypeID)
        {
            ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrder objStoreChangeOrder = new ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrder();
            try
            {
                int intTransportCompanyID = 0;
                int intTransportServiceID = 0;
                if (intTransportTypeID > 0)
                {
                    object[] objKeywords = new object[] { "@TRANSPORTTYPEID", intTransportTypeID };
                    var dtbTransportCompany = objPLCStoreChangeCommand.SearchDataTranCompany(objKeywords);
                    if (dtbTransportCompany != null && dtbTransportCompany.Rows.Count > 0)
                    {
                        if (dtbTransportCompany.Select("TransportTypeID=" + intTransportTypeID) != null && dtbTransportCompany.Select("TransportTypeID=" + intTransportTypeID).Count() > 0)
                        {
                            intTransportCompanyID = int.Parse(dtbTransportCompany.Select("TransportTypeID=" + intTransportTypeID).CopyToDataTable().Rows[0]["TransportCompanyID"].ToString());
                        }
                    }
                    var dtbTransportServices = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("TP_TRANSPORTSERVICE_CACHE");
                    if (dtbTransportServices != null && dtbTransportServices.Rows.Count > 0)
                    {
                        if (dtbTransportServices.Select("TransportCompanyID=" + intTransportCompanyID) != null && dtbTransportServices.Select("TransportCompanyID=" + intTransportCompanyID).Count() > 0)
                        {
                            intTransportServiceID = int.Parse(dtbTransportServices.Select("TransportCompanyID=" + intTransportCompanyID).CopyToDataTable().Rows[0]["TransportServiceID"].ToString());
                        }
                    }
                }

                objStoreChangeOrder.StoreChangeOrderID = strStoreChangeOrderID;
                objStoreChangeOrder.StoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                objStoreChangeOrder.StoreChangeTypeID = Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID);
                objStoreChangeOrder.ExpiryDate = dtbServerDate;
                objStoreChangeOrder.OrderDate = dtbServerDate;
                objStoreChangeOrder.ContentDeleted = "";
                objStoreChangeOrder.FromStoreName = objStoreCenterStore.StoreName;
                objStoreChangeOrder.ToStoreName = objStore.StoreName;
                objStoreChangeOrder.Content = txtContent.Text;
                objStoreChangeOrder.FromStoreID = objStoreCenterStore.StoreID;
                objStoreChangeOrder.ToStoreID = objStore.StoreID;
                objStoreChangeOrder.TransportTypeID = intTransportTypeID;
                objStoreChangeOrder.TransportCompanyID = intTransportCompanyID;
                objStoreChangeOrder.TransportServicesID = intTransportServiceID;
                objStoreChangeOrder.StoreChangeStatus = 3;
                objStoreChangeOrder.IsNew = true;
                objStoreChangeOrder.IsUrgent = false;//chuyển gấp
                objStoreChangeOrder.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChangeOrder.TotalQuantity = decTotalQuantity;
                objStoreChangeOrder.TotalStoreChangeQuantity = decTotalQuantity;
                objStoreChangeOrder.InStockStatusID = radIsNew.Checked ? 1 : 0;
                //objStoreChangeOrder.DtbStoreChangeOrderDetail = grdProduct.DataSource as DataTable;
                //objStoreChangeOrder.DtbStoreChangeOrderDetail.AcceptChanges();
                //objStoreChangeOrder.DtbReviewLevel = grdReviewLevel.DataSource as DataTable;
                //objStoreChangeOrder.DtbReviewLevel.AcceptChanges();
                objStoreChangeOrder.IsReviewed = true;
                if (objStoreChangeOrder.IsReviewed)
                {

                    objStoreChangeOrder.ReviewedDate = dtbServerDate;
                    objStoreChangeOrder.ReviewedUser = SystemConfig.objSessionUser.UserName;
                }
                //List<ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrderDetail> objStoreChangeOrderDetailList = new List<PLC.Input.WSInputVoucher.StoreChangeOrderDetail>();
                //if (InputVoucherDetailList != null)
                //{
                //    foreach (ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail in InputVoucherDetailList)
                //    {
                //        ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrderDetail objStoreChangeOrderDetail = new ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrderDetail();
                //        objStoreChangeOrderDetail.CreatedStoreID = objStoreChangeOrder.CreatedStoreID;
                //        objStoreChangeOrderDetail.CreatedUser = objStoreChangeOrder.CreatedUser;
                //        objStoreChangeOrderDetail.Note = "";
                //        objStoreChangeOrderDetail.OrderDate = objStoreChangeOrder.OrderDate;
                //        objStoreChangeOrderDetail.ProductID = objInputVoucherDetail.ProductID;
                //        objStoreChangeOrderDetail.Quantity = objInputVoucherDetail.Quantity;
                //        objStoreChangeOrderDetail.StoreChangeQuantity = objInputVoucherDetail.Quantity;
                //        objStoreChangeOrderDetail.StoreChangeOrderID = objStoreChangeOrder.StoreChangeOrderID;
                //        objStoreChangeOrderDetail.FromStoreID = objStoreChangeOrder.FromStoreID;
                //        objStoreChangeOrderDetail.ToStoreID = objStoreChangeOrder.ToStoreID;
                //        //if (objInputVoucherDetail.InputVoucherDetailIMEIList != null)
                //        //{
                //        //    //objStoreChangeOrderDetail.Quantity = 1;
                //        //    //objStoreChangeOrderDetail.StoreChangeQuantity = 1;
                //        //    List<ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrderDetailIMEI> lstStoreChangeOrderDetailIMEI = new List<PLC.Input.WSInputVoucher.StoreChangeOrderDetailIMEI>();
                //        //    foreach (ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailIMEI in objInputVoucherDetail.InputVoucherDetailIMEIList)
                //        //    {
                //        //        ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrderDetailIMEI objStoreChangeOrderDetailIMEI = new ERP.Inventory.PLC.Input.WSInputVoucher.StoreChangeOrderDetailIMEI();
                //        //        objStoreChangeOrderDetailIMEI.IMEI = objInputVoucherDetailIMEI.IMEI;
                //        //        lstStoreChangeOrderDetailIMEI.Add(objStoreChangeOrderDetailIMEI);
                //        //    }
                //        //    objStoreChangeOrderDetail.StoreChangeOrderDetailIMEILIST = lstStoreChangeOrderDetailIMEI.ToArray();
                //        //}
                //        objStoreChangeOrderDetailList.Add(objStoreChangeOrderDetail);
                //    }
                //}
                //objStoreChangeOrder.StoreChangeOrderDetailList = objStoreChangeOrderDetailList.ToArray();
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy dữ liệu để xử lý.", ex.ToString());
                return null;
            }
            return objStoreChangeOrder;
        }

        private PLC.Input.WSInputVoucher.StoreChange CreateStoreChange(ERP.MasterData.PLC.MD.WSStore.Store objStore, ERP.MasterData.PLC.MD.WSStore.Store objStoreCenterStore,
            ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType, string strStoreChangeOrderID, int intTransportTypeID)
        {

            PLC.Input.WSInputVoucher.StoreChange objStoreChange = new PLC.Input.WSInputVoucher.StoreChange();
            try
            {
                DataTable dtbStoreChangeDetail = InitStoreChangeDetail();
                //Load hình thức xuất chuyển kho
                decimal decTotalQuantity = 0;
                decimal decTotalAmountBF_Out = 0;
                decimal decTotalVAT_Out = 0;
                decimal decTotalAmount_Out = 0;
                decimal decTotalAmountBF_In = 0;
                decimal decTotalVAT_In = 0;
                decimal decTotalAmount_In = 0;
                GetTotalValues(dtbStoreChangeDetail, ref decTotalQuantity, ref decTotalAmountBF_Out, ref decTotalVAT_Out, ref decTotalAmount_Out, ref decTotalAmountBF_In, ref decTotalVAT_In, ref decTotalAmount_In);
                //Lưu ghi chú và Thêm chi tiết xuất chuyển kho
                objStoreChange.StoreChangeDetailData = dtbStoreChangeDetail;
                objStoreChange.IsInputFromOrder = bolIsInputFromOrder;//True: Lưu ghi chú
                objStoreChange.IsCreateInOutVoucher = objStoreChangeType.IsCreateInOutVoucher;//True: Tạo phiếu thu chi
                objStoreChange.StoreChangeTypeID = objStoreChangeType.StoreChangeTypeID;
                objStoreChange.StoreChangeOrderID = strStoreChangeOrderID;
                objStoreChange.ToStoreID = objStore.StoreID;
                objStoreChange.FromStoreID = objStoreCenterStore.StoreID;
                objStoreChange.TransportTypeID = intTransportTypeID;
                objStoreChange.StoreChangeDate = dtbServerDate;
                objStoreChange.StoreChangeUser = Library.AppCore.SystemConfig.objSessionUser.UserName;//txtTransportUser.UserName.Trim();
                objStoreChange.CaskCode = "";
                objStoreChange.IsNew = true;
                objStoreChange.InStockStatusID = radIsNew.Checked ? 1 : 0;
                objStoreChange.InvoiceID = "0000000";
                objStoreChange.InvoiceSymbol = "0000000";
                objStoreChange.OutputTypeID = Convert.ToInt32(objStoreChangeType.OutputTypeID);
                objStoreChange.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChange.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChange.Content = txtContent.Text;
                objStoreChange.TransportVoucherID = "0000000";
                objStoreChange.DateReceive = dtbServerDate.AddDays(1);
                objStoreChange.TransferedDate = dtbServerDate.AddDays(1);
                objStoreChange.IsUrgent = false;
                objStoreChange.FromStoreName = objStoreCenterStore.StoreName;
                objStoreChange.ToStoreName = objStore.StoreName;
                dtbStoreChangeDetail.AcceptChanges();
                DataTable tblPacking = DataTableClass.SelectDistinct(dtbStoreChangeDetail, "PackingNumber", "ISSELECT =1");
                objStoreChange.TotalPacking = tblPacking.Rows.Count;
                //if (chkIsReceive.Checked == true)
                //{
                //    objStoreChange.ReceiveNote = txtReceiveNote.Text;
                //    objStoreChange.UserReceive = SystemConfig.objSessionUser.UserName;
                //}
                objStoreChange.ToUser1 = SystemConfig.objSessionUser.UserName;
                objStoreChange.ToUser2 = SystemConfig.objSessionUser.UserName;
                objStoreChange.TotalSize = Convert.ToDecimal(Globals.IsNull(dtbStoreChangeDetail.Compute("Sum(TotalSize)", "IsSelect= 1"), 0));
                objStoreChange.TotalWeight = Convert.ToDecimal(Globals.IsNull(dtbStoreChangeDetail.Compute("Sum(TotalWeight)", "IsSelect = 1"), 0));
                //Tạo phiếu chi và phiếu thu
                if (objStoreChangeType.IsCreateInOutVoucher)
                {
                    //if (!CreateInOutVoucher(decTotalQuantity, decTotalAmountBF_Out, decTotalVAT_Out, decTotalAmount_Out, decTotalAmountBF_In, decTotalVAT_In, decTotalAmount_In))
                    //{
                    //    btnUpdate.Enabled = true;
                    //    MessageBox.Show(this, "Lỗi Tạo phiếu chi và phiếu thu\n Bạn vui lòng kiểm tra lại thông tin nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //    return;
                    //}
                }
                //Tạo phiếu nhập -phiếu xuất
                if (!CreateOuputInputVoucher(
                    objStore, objStoreCenterStore, objStoreChangeType, objStoreChange,
                    decTotalQuantity, decTotalAmountBF_Out, decTotalVAT_Out, decTotalAmount_Out, decTotalAmountBF_In,
                    decTotalVAT_In, decTotalAmount_In, objInputType.IsAutoCheckRealInput))
                {
                    MessageBox.Show(this, "Lỗi Tạo phiếu nhập -phiếu xuất \n Bạn vui lòng kiểm tra lại thông tin nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }

                //Danh sách user nhận thông báo 
                if (bolIsInputFromOrder)
                    objStoreChange.UserNotifyData = dtbNotification_User;

                //if (strStoreChangeID != string.Empty)
                //{
                //    if (objStoreChangeOrderType != null)
                //    {
                //        //Đăng bổ sung 
                //        if (objStoreChangeOrderType.CreateVATInvoice == 2)
                //        {
                //            if (!string.IsNullOrWhiteSpace(strOutPutVoucherID))
                //            {
                //                ERP.Inventory.PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new ERP.Inventory.PLC.Output.PLCOutputVoucher();
                //                PLC.Output.WSOutputVoucher.OutputVoucher objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutPutVoucherID);
                //                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                //                {
                //                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                //                    return;
                //                }
                //                CreateVATInvoice(objOutputVoucher);
                //            }
                //        }
                //    }
                //    PrintStoreAddress(strStoreChangeID);
                //    //In lệnh điều động nội bộ từ yêu cầu chuyển kho
                //    if (!string.IsNullOrEmpty(strStoreChangeOrderID))
                //        PrintStoreChangeReport(strStoreChangeID);
                //}
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi xuất chuyển kho", objEx.ToString(), Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xuất chuyển kho! \n Bạn vui lòng kiểm tra lại thông tin nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            return objStoreChange;
        }

        public static DataTable InitStoreChangeDetail()
        {
            DataTable tblResult = new DataTable("VoucherDetail");
            DataColumn colIsSelect = new DataColumn("ISSELECT", typeof(Decimal));
            colIsSelect.DefaultValue = 1;
            tblResult.Columns.Add(colIsSelect);
            tblResult.Columns.Add(new DataColumn("PRODUCTID", typeof(String)));
            tblResult.Columns.Add(new DataColumn("PRODUCTNAME", typeof(String)));
            tblResult.Columns.Add(new DataColumn("IMEI", typeof(String)));
            tblResult.Columns.Add(new DataColumn("QUANTITYUNITNAME", typeof(String)));
            tblResult.Columns.Add(new DataColumn("ENDWARRANTYDATE", typeof(DateTime)));
            tblResult.Columns.Add(new DataColumn("QUANTITY", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("PRICE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("VAT", typeof(float)));
            tblResult.Columns.Add(new DataColumn("VATPERCENT", typeof(int)));
            tblResult.Columns.Add(new DataColumn("TOTALCOST", typeof(decimal), "[Quantity] * [Price] + ([Quantity] * [Price]) * [VATPercent]*[VAT]*0.0001"));
            tblResult.Columns.Add(new DataColumn("ISREQUESTIMEI", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("ISHASWARRANTY", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("PACKINGNUMBER", typeof(int)));
            tblResult.Columns.Add(new DataColumn("COSTPRICE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("INPUTVOUCHERDATE", typeof(DateTime)));
            tblResult.Columns.Add(new DataColumn("INPUTCUSTOMERID", typeof(int)));
            tblResult.Columns.Add(new DataColumn("INPUTUSERID", typeof(string)));
            tblResult.Columns.Add(new DataColumn("INPUTTYPEID", typeof(int)));
            tblResult.Columns.Add(new DataColumn("ISSHOWPRODUCT", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("NOTE", typeof(String)));
            tblResult.Columns.Add(new DataColumn("ISRETURNPRODUCT", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("STORECHANGEORDERDETAILID", typeof(String)));
            tblResult.Columns.Add(new DataColumn("ORDERQUANTITY", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("USERNOTE", typeof(String)));
            tblResult.Columns.Add(new DataColumn("OUTPUTPRICE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("INPUTPRICE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("OUTPUTVAT", typeof(int)));
            tblResult.Columns.Add(new DataColumn("INPUTVAT", typeof(int)));
            tblResult.Columns.Add(new DataColumn("MAINGROUPID", typeof(int)));
            tblResult.Columns.Add(new DataColumn("ISALLOWDECIMAL", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("BRANDID", typeof(int)));
            tblResult.Columns.Add(new DataColumn("TOTALWEIGHT", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("TOTALSIZE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("PRODUCTWEIGHT", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("PRODUCTSIZE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("SHIPPINGCOST", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("TOTALSHIPPINGCOST", typeof(decimal)));
            return tblResult;
        }

        private void GetTotalValues(DataTable dtbStoreChangeDetail, ref decimal decTotalQuantity, ref decimal decTotalAmountBF_Out,
           ref decimal decTotalVAT_Out, ref decimal decTotalAmount_Out, ref decimal decTotalAmountBF_In,
           ref decimal decTotalVAT_In, ref decimal decTotalAmount_In)
        {
            decTotalQuantity = 0;
            decTotalAmountBF_Out = 0;
            decTotalVAT_Out = 0;
            decTotalAmount_Out = 0;

            decTotalAmountBF_In = 0;
            decTotalVAT_In = 0;
            decTotalAmount_In = 0;
            DataRow[] arrRow = dtbStoreChangeDetail.Select("ISSELECT= 1");
            foreach (DataRow objRow in arrRow)
            {
                decTotalQuantity += Convert.ToDecimal(objRow["Quantity"]);
                decimal decTotalCost_Out = Convert.ToDecimal(objRow["Quantity"]) * Convert.ToDecimal(objRow["OutputPrice"]);
                decimal decTotalCost_In = Convert.ToDecimal(objRow["Quantity"]) * Convert.ToDecimal(objRow["InputPrice"]);
                decimal decOutputVAT = Convert.ToDecimal(objRow["OutputVAT"]) * Convert.ToDecimal(objRow["VATPercent"]) / 10000;
                decimal decInputVAT = Convert.ToDecimal(objRow["InputVAT"]) * Convert.ToDecimal(objRow["VATPercent"]) / 10000;
                decTotalAmountBF_Out += decTotalCost_Out;
                decTotalVAT_Out += decTotalCost_Out * decOutputVAT;
                decTotalAmount_Out += decTotalCost_Out * (1 + decOutputVAT);

                decTotalAmountBF_In += decTotalCost_In;
                decTotalVAT_In += decTotalCost_In * decInputVAT;
                decTotalAmount_In += decTotalCost_In * (1 + decInputVAT);
            }
        }

        /// <summary>
        /// Kiểm tra dữ liệu nhập vào
        /// </summary>
        private bool CheckSameProvince(ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeTypeForOrder,
            ERP.MasterData.PLC.MD.WSStore.Store objFromStore, ERP.MasterData.PLC.MD.WSStore.Store objToStore)
        {
            bool bolIsSameProvince = (objFromStore.ProvinceID == objToStore.ProvinceID);
            bool bolIsSameCompany = (objFromStore.CompanyID == objToStore.CompanyID);
            bool bolIsSameBranch = (objFromStore.BranchID == objToStore.BranchID);
            if (objStoreChangeTypeForOrder.CheckCompanyType == 1 && !bolIsSameCompany) // kiểm tra cùng công ty
            {
                MessageBox.Show(this, "Loại yêu cầu chuyển kho [" + objStoreChangeTypeForOrder.StoreChangeTypeID + " - " + objStoreChangeTypeForOrder.StoreChangeTypeName + "] đang thiết lập cùng công ty. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (objStoreChangeTypeForOrder.CheckCompanyType == 2 && bolIsSameCompany) // kiểm tra khác công ty
            {
                MessageBox.Show(this, "Loại yêu cầu chuyển kho [" + objStoreChangeTypeForOrder.StoreChangeTypeID + " - " + objStoreChangeTypeForOrder.StoreChangeTypeName + "] đang thiết lập khác công ty. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (objStoreChangeTypeForOrder.CheckProvinceType == 1 && !bolIsSameProvince) // kiểm tra cùng tỉnh/TP
            {
                MessageBox.Show(this, "Loại yêu cầu chuyển kho [" + objStoreChangeTypeForOrder.StoreChangeTypeID + " - " + objStoreChangeTypeForOrder.StoreChangeTypeName + "] đang thiết lập cùng tỉnh/thành phố. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (objStoreChangeTypeForOrder.CheckProvinceType == 2 && bolIsSameProvince)// kiểm tra khác tỉnh/TP
            {
                MessageBox.Show(this, "Loại yêu cầu chuyển kho [" + objStoreChangeTypeForOrder.StoreChangeTypeID + " - " + objStoreChangeTypeForOrder.StoreChangeTypeName + "] đang thiết lập khác tỉnh/thành phố. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (objStoreChangeTypeForOrder.CheckBranchType == 1 && !bolIsSameBranch) // kiểm tra cùng chi nhánh
            {
                MessageBox.Show(this, "Loại yêu cầu chuyển kho [" + objStoreChangeTypeForOrder.StoreChangeTypeID + " - " + objStoreChangeTypeForOrder.StoreChangeTypeName + "] đang thiết lập cùng chi nhánh. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (objStoreChangeTypeForOrder.CheckBranchType == 2 && bolIsSameBranch)// kiểm tra khác chi nhánh
            {
                MessageBox.Show(this, "Loại yêu cầu chuyển kho [" + objStoreChangeTypeForOrder.StoreChangeTypeID + " - " + objStoreChangeTypeForOrder.StoreChangeTypeName + "] đang thiết lập khác chi nhánh. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;

        }

        #region Tạo phiếu xuất chuyển kho và nhập chuyển kho
        /// <summary>
        /// Tạo phiếu Chuyển kho - Phiếu xuất - Phiếu nhập
        /// IsCreateInOutVoucher = true : Tạo Phiếu thu - Phiếu chi
        /// </summary>
        /// <returns></returns>
        public bool CreateOuputInputVoucher(ERP.MasterData.PLC.MD.WSStore.Store objToStore, ERP.MasterData.PLC.MD.WSStore.Store objFromStore,
            ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType, PLC.Input.WSInputVoucher.StoreChange objStoreChange,
            decimal decTotalQuantity, decimal decTotalAmountBF_Out, decimal decTotalVAT_Out, decimal decTotalAmount_Out, decimal decTotalAmountBF_In, decimal decTotalVAT_In, decimal decTotalAmount_In, bool bolIsCheckRealInput)
        {
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomerInput = null;
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomerOutput = null;
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
            String strCustomerName = string.Empty;
            String strStorePhone = string.Empty;
            String strStoreTaxID = string.Empty;
            String strTaxAddress = string.Empty;
            String strCustomerAddress = string.Empty;
            if (objStoreChangeType.IsStoreCustomer)
            {
                if (objFromStore.CustomerID <= 0)
                {
                    MessageBox.Show("Chưa thiết lập khách hàng cho kho [" + objFromStore.StoreID + " - " + objFromStore.StoreName + "]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                if (objToStore.CustomerID <= 0)
                {
                    MessageBox.Show("Chưa thiết lập khách hàng cho kho [" + objToStore.StoreID + " - " + objToStore.StoreName + "]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                objCustomerInput = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objFromStore.CustomerID));
                objCustomerOutput = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objToStore.CustomerID));
            }
            else
            {
                objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objStoreChangeType.CustomerID));
                strCustomerName = objToStore.StoreName;
                strStorePhone = objToStore.StorePhoneNumber;
                strStoreTaxID = objToStore.TaxCode;
                strTaxAddress = objToStore.TaxAddress;
                strCustomerAddress = objToStore.StoreAddress;
                //Nếu khác công ty
                //if (objStoreChangeType.StoreChangeTypeID == 2 || objStoreChangeType.StoreChangeTypeID == 4)
                if (objStoreChangeType.CheckCompanyType == 2)
                    strCustomerAddress = strTaxAddress;
                //Nếu là Xuất chuyển hàng trưng bày thì IsCheckRealInput = true
                //if (objStoreChangeType.StoreChangeTypeID == 3)
                //{
                //    //bolIsCheckRealInput = objInputType.
                //    bolIsCheckRealInput = true;
                //}
            }
            try
            {
                // ****** Thông tin phiếu xuất ****** //
                PLC.Input.WSInputVoucher.OutputVoucher objOutputVoucher = new PLC.Input.WSInputVoucher.OutputVoucher();
                objOutputVoucher.OrderID = objStoreChange.StoreChangeOrderID.Trim();
                objOutputVoucher.CurrencyUnitID = Convert.ToInt32(objStoreChangeType.CurrencyUnitID);
                objOutputVoucher.CurrencyExchange = decCurrencyExchange;
                objOutputVoucher.InvoiceSymbol = "";
                objOutputVoucher.InvoiceDate = objStoreChange.StoreChangeDate;
                objOutputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objOutputVoucher.OutputStoreID = objFromStore.StoreID;
                objOutputVoucher.InvoiceSymbol = "";
                objOutputVoucher.OutputDate = objStoreChange.StoreChangeDate;
                objOutputVoucher.PayableTypeID = Convert.ToInt32(objStoreChangeType.PayableTypeID);
                objOutputVoucher.PayableDate = DateTime.Now;
                objOutputVoucher.Discount = 0;
                objOutputVoucher.DiscountReasonID = Convert.ToInt32(objStoreChangeType.DiscountReasonID);
                decimal decTotalAmountBF = 0;
                decimal decTotalVAT = 0;
                GetTotal(ref decTotalAmountBF, ref decTotalVAT);
                objOutputVoucher.TotalAmountBFT = decTotalAmountBF;
                objOutputVoucher.TotalVAT = decTotalVAT;
                objOutputVoucher.TotalAmount = decTotalAmount;
                objOutputVoucher.Denominator = "";
                objOutputVoucher.StaffUser = Library.AppCore.SystemConfig.objSessionUser.UserName; //txtTransportUser.UserName;
                objOutputVoucher.IsError = false;
                objOutputVoucher.ErrorContent = "";
                if (objCustomerOutput != null)
                {
                    objOutputVoucher.CustomerID = objCustomerOutput.CustomerID;
                    objOutputVoucher.CustomerAddress = objCustomerOutput.CustomerAddress;
                    objOutputVoucher.CustomerName = objCustomerOutput.CustomerName;
                    objOutputVoucher.CustomerPhone = objCustomerOutput.CustomerPhone;
                    objOutputVoucher.CustomerTaxID = objCustomerOutput.CustomerTaxID;
                    objOutputVoucher.TaxCustomerAddress = objCustomerOutput.TaxCustomerAddress;
                }
                else
                {
                    if (objCustomer != null)
                    {
                        if (objCustomer.IsDefault)
                        {
                            objOutputVoucher.CustomerID = objCustomer.CustomerID;
                            objOutputVoucher.CustomerAddress = strTaxAddress;
                            objOutputVoucher.CustomerName = strCustomerName;
                            objOutputVoucher.CustomerPhone = strStorePhone;
                            objOutputVoucher.CustomerTaxID = strStoreTaxID;
                            objOutputVoucher.TaxCustomerAddress = objCustomer.TaxCustomerAddress;
                        }
                        else
                        {
                            objOutputVoucher.CustomerID = objCustomer.CustomerID;
                            objOutputVoucher.CustomerAddress = objCustomer.CustomerAddress;
                            objOutputVoucher.CustomerName = objCustomer.CustomerName;
                            objOutputVoucher.CustomerPhone = objCustomer.CustomerPhone;
                            objOutputVoucher.CustomerTaxID = objCustomer.CustomerTaxID;
                            objOutputVoucher.TaxCustomerAddress = objCustomer.TaxCustomerAddress;
                        }
                    }
                }
                objOutputVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objOutputVoucher.IsStoreChange = true;
                objStoreChange.OutputVoucherBO = objOutputVoucher;
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi lấy thông tin phiếu xuất", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi lấy thông tin phiếu xuất \n Bạn vui lòng kiểm tra lại thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        #endregion
        #region hóa đơn đầu vào
        /// <summary>
        /// Đăng bổ sung hàm insertVATInvoice
        /// </summary>
        public bool CreateInputVATInvoice(IWin32Window hwdOwner, ref ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice objVAT_PInvoiceMain,
            ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objInputVoucherCenter, int intStoreID)
        {
            try
            {
                DataTable dtbInvoiceTransactionType = Library.AppCore.DataSource.GetDataSource.GetInvoiceTransactionType().Copy();
                DataRow[] rowGroup = dtbInvoiceTransactionType.Select("INVOICETRANSTYPEID=" + objInputType.InvoiceTransTypeID);
                bool bolIsTaxDeclaration = Convert.ToBoolean(rowGroup[0]["ISTAXDECLARATION"]);
                //object[] objsKeyword = new object[]
                //{
                //    "@INVOICETYPEID", "1,2",
                //    "@STOREID", "",
                //    "@MAINGROUPID", "",
                //    "@INPUTTYPEID","",
                //    "@FROMDATE", dtInputDate.Value,
                //    "@TODATE", dtInputDate.Value,
                //    "@KEYWORDS", strInputVoucherCenterID,
                //    "@SEARCHBY", 2
                //};
                //DataTable dtbDataProductGet = null;
                //new ERP.PrintVAT.PLC.PPrintVAT.PLCPrintVAT().SearchDataProductInvoice(ref dtbDataProductGet, objsKeyword);
                //DataTable dtbDataProduct = InitializeDataProductService(dtbDataProductGet, true);
                ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
                new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, intStoreID);
                ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice objVATInvoice = new ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice();
                objVATInvoice.InvoiceDate = dtInvoiceDate.DateTime;
                objVATInvoice.FormTypeID = -1;
                objVATInvoice.Denominator = txtDenominator.Text;//Mẫu số
                objVATInvoice.InvoiceSymbol = txtInvoiceSymbol.Text;//ký hiệu
                objVATInvoice.InvoiceNO = Convert.ToInt32(txtInVoiceID.Text);
                objVATInvoice.PurchaseMANID = "";
                objVATInvoice.PurchaseMAN = "";
                ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(txtCustomerID.Text));
                if (!string.IsNullOrWhiteSpace(objCustomer.TaxCustomerName))
                {
                    objCustomer.CustomerName = objCustomer.TaxCustomerName;
                }
                objVATInvoice.CurrencyUnit = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
                objVATInvoice.CurrencyExchange = Convert.ToDecimal(txtExchangeRate.Text);
                objVATInvoice.CustomerID = objCustomer.CustomerID;
                objVATInvoice.CustomerName = objCustomer.CustomerName;
                objVATInvoice.SELLER = objCustomer.ContactName;
                objVATInvoice.TaxCustomerAddress = objCustomer.TaxCustomerAddress.Length == 0 ? objCustomer.CustomerAddress : objCustomer.TaxCustomerAddress;
                objVATInvoice.CustomerTaxID = objCustomer.CustomerTaxID;
                objVATInvoice.CustomerPhone = objCustomer.CustomerPhone;
                objVATInvoice.Description = "";

                objVATInvoice.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                objVATInvoice.InvoiceStoreID = intStoreID;

                //objVATInvoice.TypeID = this.intTypeId;
                objVATInvoice.BranchID = objToStore.BranchID;
                objVATInvoice.InvoiceType = 0;
                objVATInvoice.VATInvoiceMainID = "";
                objVATInvoice.PayableTypeID = Convert.ToInt32(cboPayableType.SelectedValue);
                objVATInvoice.InvoiceDUEDate = dtPayableTime.Value;
                objVATInvoice.TypeID = 2;
                objVATInvoice.InvoiceType = 8;
                objVATInvoice.InvoiceTRANSTypeID = objInputType.InvoiceTransTypeID;
                objVATInvoice.OutputVoucherInfo = objCustomer.CustomerPhone + " - " + objCustomer.CustomerName;
                List<PLC.Input.WSInputVoucher.VAT_Invoice_Product> lstProduct = new List<PLC.Input.WSInputVoucher.VAT_Invoice_Product>();
                var lst = objInputVoucherCenter.InputVoucherDetailList;
                //Cộng tiền chưa VAT
                objVATInvoice.AmountBF = lst.Sum(x => x.InputPrice);
                //Cộng tiền thuế VAT
                objVATInvoice.VATAmount = lst.Sum(x => x.VAT);
                //Cộng tiền có  VAT
                objVATInvoice.TotalAmount = lst.Sum(x => x.Quantity * x.InputPrice + (x.Quantity * x.InputPrice * x.VAT / 100));
                //Cộng tiền hóa đơn trước thuế
                objVATInvoice.AmountInvoiceBF = lst.Sum(x => x.Quantity * x.InputPrice);
                //Cộng tiền thuế VAT hóa đơn
                objVATInvoice.VATInvoice = lst.Sum(x => (x.Quantity * x.InputPrice * x.VAT / 100));
                //Cộng tiền hóa đơn có VAT
                objVATInvoice.TotalAmountInvoice = lst.Sum(x => x.Quantity * x.InputPrice + (x.Quantity * x.InputPrice * x.VAT / 100));
                //Thuế suất hóa đơn GTGT
                objVATInvoice.InvoiceVAT = lst.Max(x => x.VAT);

                // nap du lieu danh sach san pham 
                List<ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice_Product> lstProductInvoice = new List<PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice_Product>();

                foreach (var item in objInputVoucherCenter.InputVoucherDetailList)
                {
                    ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice_Product obj = new ERP.PrintVAT.PLC.WSPrintVAT_Input.VAT_PInvoice_Product();
                    obj.VATPInvoiceDTID = "";
                    obj.VATPInvoiceID = "";

                    obj.InputVoucherDetailID = item.InputVoucherDetailID;
                    obj.OrderDetailId = item.OrderDetailID == string.Empty ? "-1" : item.OrderDetailID;

                    obj.ProductID = (item.ProductID ?? "").ToString();
                    obj.Quantity = Convert.ToDecimal(item.Quantity);
                    obj.PriceInvoice = Convert.ToDecimal(item.InputPrice);
                    obj.VATInvoice = Convert.ToDecimal(item.VAT);
                    DataRow[] objProduct = dtbProduct.Select("PRODUCTID = '" + item.ProductID + "'");
                    if (objProduct != null)
                    {
                        obj.QuantityUnitID = Convert.ToInt32(objProduct[0]["QUANTITYUNITID"]);
                    }

                    obj.DIFFERENCEAmount = 0;
                    if (bolIsTaxDeclaration)
                        obj.GroupServiceId = Convert.ToInt32(rowGroup[0]["GROUPSERVICEID"]);
                    else
                        obj.GroupServiceId = null;

                    lstProductInvoice.Add(obj);
                }
                objVATInvoice.ProductList = lstProductInvoice.ToArray();
                // Thực thi qua trình xuong da ta
                string strVATInvoiceID = string.Empty;
                var objResultMessage = new ERP.PrintVAT.PLC.PPrintVAT.PLCPrintVAT().Insert_VAT_Invoice(objVATInvoice, ref strVATInvoiceID);
                if (objResultMessage.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(hwdOwner, objResultMessage.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi thêm mơi hóa đơn GTGT!", objResultMessage.MessageDetail, DUIInventory_Globals.ModuleName + "->InsertVATInvoice");
                    return false;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi thêm mới hóa đơn mua hàng!", objExce, DUIInventory_Globals.ModuleName + "->CreateInputVATInvoice");
                MessageBoxObject.ShowWarningMessage(hwdOwner, "Lỗi khi thêm mơi hóa đơn mua hàng");
                return false;
            }
            return true;
        }

        private void CreateDataProductService(ref DataTable dtbDataProductService)
        {
            dtbDataProductService = new DataTable();
            dtbDataProductService.Columns.Add("VATPINVOICEID", typeof(string));
            dtbDataProductService.Columns.Add("VATPINVOICEDTID", typeof(string));
            dtbDataProductService.Columns.Add("ORDERID", typeof(string));
            dtbDataProductService.Columns.Add("INPUTVOUCHERID", typeof(string));
            dtbDataProductService.Columns.Add("INPUTVOUCHERDETAILID", typeof(string));
            dtbDataProductService.Columns.Add("ORDERDETAILID", typeof(string));
            dtbDataProductService.Columns.Add("CREATEDDATE", typeof(object));
            dtbDataProductService.Columns.Add("CUSTOMERID", typeof(Int32));
            dtbDataProductService.Columns["CUSTOMERID"].DefaultValue = -1;

            dtbDataProductService.Columns.Add("CUSTOMERNAME", typeof(string));
            dtbDataProductService.Columns.Add("CUSTOMERADDRESS", typeof(string));
            dtbDataProductService.Columns.Add("CUSTOMERPHONE", typeof(string));
            dtbDataProductService.Columns.Add("CUSTOMERTAXID", typeof(string));

            dtbDataProductService.Columns.Add("PRODUCTID", typeof(string));
            dtbDataProductService.Columns.Add("PRODUCTNAME", typeof(string));

            dtbDataProductService.Columns.Add("APPLYMISAPRODUCTID", typeof(string));
            dtbDataProductService.Columns.Add("MISAPRODUCTMISANAME", typeof(string));
            dtbDataProductService.Columns.Add("MISAPRODUCTNAMECODE", typeof(string));

            dtbDataProductService.Columns.Add("QUANTITYUNITID", typeof(Int32));
            dtbDataProductService.Columns["QUANTITYUNITID"].DefaultValue = -1;


            dtbDataProductService.Columns.Add("INQUANTITY", typeof(decimal));
            dtbDataProductService.Columns["INQUANTITY"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("QUANTITY_EXISTS", typeof(decimal));
            dtbDataProductService.Columns["QUANTITY_EXISTS"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("INQUANTITYOLD", typeof(decimal));
            dtbDataProductService.Columns["INQUANTITYOLD"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("IN_QUANTITY", typeof(decimal));
            dtbDataProductService.Columns["IN_QUANTITY"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("REMAINQUANTITY", typeof(decimal));
            dtbDataProductService.Columns["REMAINQUANTITY"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("QUANTITY", typeof(decimal));
            dtbDataProductService.Columns["QUANTITY"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("QUANTITYUNITNAME", typeof(string));

            dtbDataProductService.Columns.Add("COSTPRICE", typeof(decimal));
            dtbDataProductService.Columns["COSTPRICE"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("INPUTPRICE", typeof(decimal));
            dtbDataProductService.Columns["INPUTPRICE"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("VAT", typeof(decimal));
            dtbDataProductService.Columns["VAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("VATPERCENT", typeof(decimal));
            dtbDataProductService.Columns["VATPERCENT"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("VATAMOUNT", typeof(decimal));
            dtbDataProductService.Columns["VATAMOUNT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("AMOUNTWITHVAT", typeof(decimal));
            dtbDataProductService.Columns["AMOUNTWITHVAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("AMOUNTINVOICE", typeof(decimal));
            dtbDataProductService.Columns["AMOUNTINVOICE"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("TOTALAMOUNTWITHOUTVAT", typeof(decimal));
            dtbDataProductService.Columns["TOTALAMOUNTWITHOUTVAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("TOTALAMOUNT", typeof(decimal));
            dtbDataProductService.Columns["TOTALAMOUNT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("TOTALAMOUNTINVOICEWITHOUTVAT", typeof(decimal));
            dtbDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("INVOICEVAT", typeof(decimal));
            dtbDataProductService.Columns["INVOICEVAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("INVOICEAMOUNT", typeof(decimal));
            dtbDataProductService.Columns["INVOICEAMOUNT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("TOTALAMOUNTINVOICE", typeof(decimal));
            dtbDataProductService.Columns["TOTALAMOUNTINVOICE"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("UNEVEN", typeof(decimal));
            dtbDataProductService.Columns["UNEVEN"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("CREATEUSER", typeof(string));
            dtbDataProductService.Columns.Add("CREATEDATE", typeof(object));
            dtbDataProductService.Columns.Add("UPDATEUSER", typeof(string));
            dtbDataProductService.Columns.Add("UPDATEDATE", typeof(object));

            dtbDataProductService.Columns.Add("ISDELETE", typeof(Int32));
            dtbDataProductService.Columns["ISDELETE"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("DELETEUSER", typeof(string));
            dtbDataProductService.Columns.Add("DELETEDATE", typeof(object));
            dtbDataProductService.Columns.Add("DELETEREASON", typeof(string));

            dtbDataProductService.Columns.Add("ISNOTAX", typeof(Int32));
            dtbDataProductService.Columns["ISNOTAX"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("ISCHKCHANGE", typeof(Int32));
            dtbDataProductService.Columns["ISCHKCHANGE"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("ISPROMOTIONPRODUCTMISA", typeof(Int32));
            dtbDataProductService.Columns["ISPROMOTIONPRODUCTMISA"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("INVOICENO", typeof(string));
            dtbDataProductService.Columns.Add("INPUTVOUCHERINFO", typeof(string));
            dtbDataProductService.Columns.Add("PROMOTIONDETAILID", typeof(string));
            dtbDataProductService.Columns.Add("PROMOTIONNAME", typeof(string));

            dtbDataProductService.Columns.Add("VATINVOICENOMAIN", typeof(string));
            dtbDataProductService.Columns.Add("VATINVOICEIDMAIN", typeof(string));
            dtbDataProductService.Columns.Add("GROUPSERVICEID", typeof(Int32));
        }
        public PLC.Input.WSInputVoucher.VAT_PInvoice CreateNewInputVATInvoice(IWin32Window hwdOwner, MasterData.PLC.MD.WSInputType.InputType objInputTypebySCType, ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objInputVoucherCenter)
        {
            try
            {
                DataTable dtbInvoiceTransactionType = Library.AppCore.DataSource.GetDataSource.GetInvoiceTransactionType().Copy();
                DataRow[] rowGroup = dtbInvoiceTransactionType.Select("INVOICETRANSTYPEID=" + objInputTypebySCType.InvoiceTransTypeID);
                bool bolIsTaxDeclaration = Convert.ToBoolean(rowGroup[0]["ISTAXDECLARATION"]);
                ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
                new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, objInputVoucherCenter.InputStoreID);
                PLC.Input.WSInputVoucher.VAT_PInvoice objVATInvoice = new PLC.Input.WSInputVoucher.VAT_PInvoice();
                objVATInvoice.InvoiceDate = Globals.GetServerDateTime();
                objVATInvoice.FormTypeID = -1;
                objVATInvoice.Denominator = "";//Mẫu số
                objVATInvoice.InvoiceSymbol = "";//ký hiệu
                objVATInvoice.InvoiceNO = 0;
                objVATInvoice.PurchaseMANID = "";
                objVATInvoice.PurchaseMAN = "";
                ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objInputVoucherCenter.CustomerID));
                if (!string.IsNullOrWhiteSpace(objCustomer.TaxCustomerName))
                {
                    objCustomer.CustomerName = objCustomer.TaxCustomerName;
                }
                objVATInvoice.CustomerID = objCustomer.CustomerID;
                objVATInvoice.CustomerName = objCustomer.CustomerName;
                objVATInvoice.SELLER = objCustomer.ContactName;
                objVATInvoice.TaxCustomerAddress = objCustomer.TaxCustomerAddress.Length == 0 ? objCustomer.CustomerAddress : objCustomer.TaxCustomerAddress;
                objVATInvoice.CustomerTaxID = objCustomer.CustomerTaxID;
                objVATInvoice.CustomerPhone = objCustomer.CustomerPhone;
                objVATInvoice.Description = "";

                objVATInvoice.CurrencyUnit = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
                objVATInvoice.CurrencyExchange = Convert.ToDecimal(txtExchangeRate.Text);

                objVATInvoice.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                objVATInvoice.InvoiceStoreID = objInputVoucher.InputStoreID;

                //objVATInvoice.TypeID = this.intTypeId;
                objVATInvoice.BranchID = objToStore.BranchID;
                objVATInvoice.InvoiceType = 0;
                objVATInvoice.VATInvoiceMainID = "";
                objVATInvoice.PayableTypeID = Convert.ToInt32(objInputVoucher.PayableTypeID);
                objVATInvoice.InvoiceDUEDate = objInputVoucher.PayableDate;
                objVATInvoice.TypeID = 2;
                objVATInvoice.InvoiceType = 8;
                objVATInvoice.InvoiceTRANSTypeID = objInputTypebySCType.InvoiceTransTypeID;
                objVATInvoice.OutputVoucherInfo = objCustomer.CustomerPhone + " - " + objCustomer.CustomerName;
                List<PLC.Input.WSInputVoucher.VAT_Invoice_Product> lstProduct = new List<PLC.Input.WSInputVoucher.VAT_Invoice_Product>();
                var lst = objInputVoucherCenter.InputVoucherDetailList;
                //Cộng tiền chưa VAT
                objVATInvoice.AmountBF = lst.Sum(x => x.InputPrice);
                //Cộng tiền thuế VAT
                objVATInvoice.VATAmount = lst.Sum(x => x.VAT);
                //Cộng tiền có  VAT
                objVATInvoice.TotalAmount = lst.Sum(x => x.Quantity * x.InputPrice + (x.Quantity * x.InputPrice * x.VAT / 100));
                //Cộng tiền hóa đơn trước thuế
                objVATInvoice.AmountInvoiceBF = lst.Sum(x => x.Quantity * x.InputPrice);
                //Cộng tiền thuế VAT hóa đơn
                objVATInvoice.VATInvoice = lst.Sum(x => (x.Quantity * x.InputPrice * x.VAT / 100));
                //Cộng tiền hóa đơn có VAT
                objVATInvoice.TotalAmountInvoice = lst.Sum(x => x.Quantity * x.InputPrice + (x.Quantity * x.InputPrice * x.VAT / 100));
                //Thuế suất hóa đơn GTGT
                objVATInvoice.InvoiceVAT = lst.Max(x => x.VAT);

                //// nap du lieu danh sach san pham 
                List<PLC.Input.WSInputVoucher.VAT_PInvoice_Product> lstProductInvoice = new List<PLC.Input.WSInputVoucher.VAT_PInvoice_Product>();
                foreach (var item in objInputVoucherCenter.InputVoucherDetailList)
                {
                    PLC.Input.WSInputVoucher.VAT_PInvoice_Product obj = new PLC.Input.WSInputVoucher.VAT_PInvoice_Product();
                    obj.VATPInvoiceDTID = "";
                    obj.VATPInvoiceID = "";

                    obj.InputVoucherDetailID = item.InputVoucherDetailID == string.Empty ? "-1" : item.InputVoucherDetailID;
                    obj.OrderDetailId = item.OrderDetailID == string.Empty ? "-1" : item.OrderDetailID;

                    obj.ProductID = (item.ProductID ?? "").ToString();
                    obj.Quantity = Convert.ToDecimal(item.Quantity);
                    obj.PriceInvoice = Convert.ToDecimal(item.InputPrice);
                    obj.VATInvoice = Convert.ToDecimal(item.VAT);
                    DataRow[] objProduct = dtbProduct.Select("PRODUCTID = '" + item.ProductID + "'");
                    if (objProduct != null)
                    {
                        obj.QuantityUnitID = Convert.ToInt32(objProduct[0]["QUANTITYUNITID"]);
                    }

                    obj.DIFFERENCEAmount = 0;
                    if (bolIsTaxDeclaration)
                        obj.GroupServiceId = Convert.ToInt32(rowGroup[0]["GROUPSERVICEID"]);
                    else
                        obj.GroupServiceId = null;

                    lstProductInvoice.Add(obj);
                }
                //Thuế suất hóa đơn GTGT
                objVATInvoice.ProductList = lstProductInvoice.ToArray();
                return objVATInvoice;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi thêm mới hóa đơn GTGT!", objExce, DUIInventory_Globals.ModuleName + "->InsertVATInvoice");
                MessageBoxObject.ShowWarningMessage(hwdOwner, "Lỗi khi thêm mơi hóa đơn GTGT");
                return null;
            }
        }
        #endregion
        #region hóa đơn xuat chuyen kho
        private PLC.Input.WSInputVoucher.VAT_Invoice CreateVATInvoice(ERP.Inventory.PLC.Input.WSInputVoucher.OutputVoucher objOutputVoucher, ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType, ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType, ref string strFormTypeBranchID)
        {
            object[] objsKeyword = new object[]
                {
                    "@STORECHANGEORDERTYPEID", objStoreChangeOrderType.StoreChangeOrderTypeID,
                    "@StoreID", objOutputVoucher.OutputStoreID
                };
            DataTable dtsFormType = new PLC.StoreChange.PLCStoreChange().GetFormTypebyStoreChangeOrderType(objsKeyword);
            if (dtsFormType != null && dtsFormType.Rows.Count > 0)
            {
                int intFormTypeID = 0;
                string strDenominator = string.Empty;
                string strInvoiceSymbol1 = string.Empty;
                int intCurrentNo = 0;
                int intBranchID = 0;
                string strINVOICENO = string.Empty;
                strFormTypeBranchID = "0";
                int.TryParse(dtsFormType.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                ERP.MasterData.PLC.MD.WSStore.Store objStoreOutput = null;
                new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objStoreOutput, Convert.ToInt32(objOutputVoucher.OutputStoreID));
                intBranchID = objStoreOutput.BranchID;
                strDenominator = dtsFormType.Rows[0]["Denominator"].ToString();
                strInvoiceSymbol1 = dtsFormType.Rows[0]["InvoiceSymbol"].ToString();
                int.TryParse(dtsFormType.Rows[0]["CURRENTNO"].ToString(), out intCurrentNo);
                strFormTypeBranchID = dtsFormType.Rows[0]["FormTypeBranchID"].ToString();
                strINVOICENO = dtsFormType.Rows[0]["INVOICENO"].ToString();
                //DataTable dtbDataProduct = null;
                //object[] objsKeyword1 = new object[]
                //{
                //    "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                //    "@StoreID", objOutputVoucher.OutputStoreID,
                //    "@IsStoreChange", 1
                //};
                //new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                //if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                //{                    
                //if (objVAT_InvoiceMain != null)
                //{
                //    //PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                //    //objPrintVAT_PrepareData_Viettel.PrintVATInvoice(objVAT_InvoiceMain, false, false, "", objStoreChangeOrderType.VATPagesPrint, true, true);
                //}
                return CreateVATInvoice(this, intFormTypeID, strDenominator, strInvoiceSymbol1, intBranchID, intCurrentNo, strFormTypeBranchID, objOutputVoucher, objStoreChangeType);
                //{
                //    Library.AppCore.Forms.frmWaitDialog.Close();
                //    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tạo hóa đơn GTGT không thành công!");
                //}
                //}
            }
            else
            {
                DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
                var drStore = dtbStore.Select("STOREID=" + objOutputVoucher.OutputStoreID);
                if (drStore.Any())
                {
                    Library.AppCore.Forms.frmWaitDialog.Close();
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất: " + objOutputVoucher.OutputStoreID + " - " + drStore[0]["STORENAME"].ToString() + "!");
                    return null;
                }
                else
                {
                    Library.AppCore.Forms.frmWaitDialog.Close();
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất!");
                    return null;
                }
            }
        }

        /// <summary>
        /// Đăng bổ sung hàm insertVATInvoice
        /// </summary>
        public PLC.Input.WSInputVoucher.VAT_Invoice CreateVATInvoice(IWin32Window hwdOwner,
            int intFormTypeID, string strDenominator, string strInvoiceSymbol, int intBranchID,
            int intCurrentNo, string strFormTypeBranchID, PLC.Input.WSInputVoucher.OutputVoucher objOutputVoucher,
            ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType
            )
        {
            try
            {
                ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
                new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, cboStoreSearch.StoreID);
                var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + objToStore.BranchID);
                PLC.Input.WSInputVoucher.VAT_Invoice objVATInvoice = new PLC.Input.WSInputVoucher.VAT_Invoice();
                objVATInvoice.FormTypeID = intFormTypeID;//Mã thiết lập
                objVATInvoice.InvoiceDate = dtbServerDate;
                objVATInvoice.Denominator = strDenominator;//Mẫu số
                objVATInvoice.InvoiceSymbol = strInvoiceSymbol;//ký hiệu
                objVATInvoice.InvoiceNO = intCurrentNo;
                objVATInvoice.SalemanID = objOutputVoucher.StaffUser;
                ERP.MasterData.SYS.PLC.WSUser.User objUser = ERP.MasterData.SYS.PLC.PLCUser.LoadInfoFromCache(objOutputVoucher.StaffUser.Trim());
                objVATInvoice.Saleman = objUser.FullName;
                ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
                if (objStoreChangeType.IsStoreCustomer)
                {
                    objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objOutputVoucher.CustomerID));
                    if (!string.IsNullOrWhiteSpace(objCustomer.TaxCustomerName))
                    {
                        objCustomer.CustomerName = objCustomer.TaxCustomerName;
                    }
                }
                else
                {
                    objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objStoreChangeType.CustomerID));
                    if (objCustomer.IsDefault)
                    {
                        objCustomer.CustomerName = objOutputVoucher.CustomerName;
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(objCustomer.TaxCustomerName))
                        {
                            objCustomer.CustomerName = objCustomer.TaxCustomerName;
                        }
                    }
                }

                objVATInvoice.CustomerID = objCustomer.CustomerID;
                objVATInvoice.CustomerName = objCustomer.CustomerName;
                objVATInvoice.Buyer = string.Empty;
                objVATInvoice.TaxCustomerAddress = objBranch[0]["Address"].ToString().Trim();
                objVATInvoice.CustomerTaxID = objBranch[0]["TaxCode"].ToString().Trim();
                objVATInvoice.CustomerPhone = objBranch[0]["PhoneNumber"].ToString().Trim();
                objVATInvoice.PayableTypeID = objOutputVoucher.PayableTypeID;
                objVATInvoice.Description = "";

                objVATInvoice.CreatedStore = objOutputVoucher.OutputStoreID;
                //objVATInvoice.TypeID = this.intTypeId;
                objVATInvoice.BranchID = intBranchID;
                objVATInvoice.INVOICETYPE = 0;
                objVATInvoice.VATINVOICEMAINID = "";
                objVATInvoice.InvoiceKind = 1;
                objVATInvoice.VATCustomerName = objBranch[0]["BRANCHNAME"].ToString();
                objVATInvoice.Discount = objOutputVoucher.Discount;
                objVATInvoice.DiscountReasonID = objOutputVoucher.DiscountReasonID;
                //List<PLC.Input.WSInputVoucher.VAT_Invoice_Product> lstProduct = new List<PLC.Input.WSInputVoucher.VAT_Invoice_Product>();
                //DataTable dtbProdcut = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
                //DataTable dtbProductNotax = new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().GetAllProductNoTax().Copy();
                //foreach (DataRow dr in dtbDataProduct.Rows)
                //{
                //    PLC.Input.WSInputVoucher.VAT_Invoice_Product obj = new PLC.Input.WSInputVoucher.VAT_Invoice_Product();
                //    obj.OutputVoucherDetailID = dr["OutputVoucherDetailID"].ToString();
                //    obj.MISAProductID = dr["ProductID"].ToString();
                //    obj.ProductID = dr["ProductID"].ToString();
                //    obj.Quantity = Convert.ToDecimal(dr["Quantity"].ToString());
                //    obj.PriceInvoice = Convert.ToDecimal(dr["SalePrice"].ToString());
                //    obj.VATInvoice = Convert.ToDecimal(dr["VAT"].ToString());
                //    if (dtbProductNotax != null && dtbProductNotax.Rows.Count > 0)
                //    {
                //        if (dtbProductNotax.AsEnumerable().Count(x => x.Field<string>("PRODUCTID") == dr["ProductID"].ToString()) > 0)
                //        {
                //            obj.VATInvoice = -1;
                //        }
                //    }
                //    DataRow[] drProduct = dtbProdcut.Select("ProductID='" + dr["ProductID"].ToString() + "'");
                //    if (drProduct.Any())
                //    {
                //        int intQuantityUnitId = 0;
                //        int.TryParse(drProduct[0]["QuantityUnitId"].ToString(), out intQuantityUnitId);
                //        obj.QuantityUnitId = intQuantityUnitId;
                //    }
                //    obj.DifferenceAmount = 0;
                //    obj.ISPROMOTIONPRODUCTMISA = false;
                //    obj.APPLYMISAPRODUCTID = "";
                //    obj.PROMOTIONMISADETAILID = "";
                //    obj.OUTQuantity = 0;
                //    obj.OUTQuantityOld = 0;
                //    obj.ISPROMOTIONPRODUCTMISA = false;
                //    lstProduct.Add(obj);
                //    //Cộng tiền chưa VAT
                //    objVATInvoice.AmountBF += Convert.ToDecimal(dr["SalePrice"].ToString()) * Convert.ToDecimal(dr["Quantity"].ToString());
                //    //Cộng tiền thuế VAT
                //    objVATInvoice.VATAmount += Convert.ToDecimal(dr["Quantity"].ToString()) * (Convert.ToDecimal(dr["SalePrice"].ToString()) * (Convert.ToDecimal(dr["VAT"].ToString()) / 100));

                //    //Cộng tiền có  VAT
                //    objVATInvoice.TotalAmount += (Convert.ToDecimal(dr["Quantity"].ToString()) * Convert.ToDecimal(dr["SalePrice"].ToString())) + (Convert.ToDecimal(dr["Quantity"].ToString()) * (Convert.ToDecimal(dr["SalePrice"].ToString()) * (Convert.ToDecimal(dr["VAT"].ToString()) / 100)));//item.TotalCost;

                //    //Cộng tiền hóa đơn trước thuế
                //    objVATInvoice.AmountInvoiceBF += Convert.ToDecimal(dr["SalePrice"].ToString()) * Convert.ToDecimal(dr["Quantity"].ToString());
                //    //Cộng tiền thuế VAT hóa đơn
                //    objVATInvoice.VATInvoice += Convert.ToDecimal(dr["Quantity"].ToString()) * (Convert.ToDecimal(dr["SalePrice"].ToString()) * (Convert.ToDecimal(dr["VAT"].ToString()) / 100));

                //    //Cộng tiền hóa đơn có VAT
                //    objVATInvoice.TotalAmountInvoice += (Convert.ToDecimal(dr["Quantity"].ToString()) * Convert.ToDecimal(dr["SalePrice"].ToString())) + (Convert.ToDecimal(dr["Quantity"].ToString()) * (Convert.ToDecimal(dr["SalePrice"].ToString()) * (Convert.ToDecimal(dr["VAT"].ToString()) / 100)));//item.TotalCost;

                //}
                ////Thuế suất hóa đơn GTGT
                //objVATInvoice.InvoiceVAT = dtbDataProduct.AsEnumerable().Max(x => x.Field<decimal>("INVOICEVAT"));

                objVATInvoice.OutputvoucherInfo = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                //objVATInvoice.VATInvoiceProduct = lstProduct.ToArray();
                //string strVATInvoiceID = "";
                //objVAT_PInvoice = CreateNewInputVATInvoice(this, objInputVoucher1);
                //var objResultMessage = new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().Insert_StoreChange(objVATInvoice, objVAT_PInvoice, strFormTypeBranchID, objOutputVoucher.OutputVoucherID, ref strVATInvoiceID);
                //if (objResultMessage.IsError)
                //{
                //    Library.AppCore.Forms.frmWaitDialog.Close();
                //    MessageBoxObject.ShowWarningMessage(hwdOwner, objResultMessage.MessageDetail);
                //    SystemErrorWS.Insert("Lỗi khi thêm mơi hóa đơn GTGT!", objResultMessage.MessageDetail, DUIInventory_Globals.ModuleName + "->InsertVATInvoice");
                //    return false;
                //}
                //new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().LoadInfo(ref objVATInvoiceMain, strVATInvoiceID);
                return objVATInvoice;
            }
            catch (Exception objExce)
            {
                Library.AppCore.Forms.frmWaitDialog.Close();
                SystemErrorWS.Insert("Lỗi khi thêm mơi hóa đơn GTGT!", objExce, DUIInventory_Globals.ModuleName + "->InsertVATInvoice");
                MessageBoxObject.ShowWarningMessage(hwdOwner, "Lỗi khi thêm mơi hóa đơn GTGT");
                return null;
            }
        }
        #endregion
        #endregion

        bool bolIsFinish = false;
        bool bolIsCloseForm = true;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (bolIsFinish)
            {
                bolIsFinish = false;
                bolIsCloseForm = true;
                Library.AppCore.Forms.frmWaitDialog.Close();
                // this.Close();
            }
        }

        private void frmInputVoucher_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!bolIsCloseForm)
            {
                e.Cancel = true;
            }
        }

        private void chkIsCheckRealInput_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsCheckRealInput.Checked)
                txtCheckRealInputNote.Focus();
        }
    }
}
