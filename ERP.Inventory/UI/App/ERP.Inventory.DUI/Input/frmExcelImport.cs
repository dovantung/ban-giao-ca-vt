﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using Library.AppCore;
using Library.AppCore.Forms;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Input
{
    public partial class frmExcelImport : Form
    {
        private DataTable dtbData;
        DataTable dtbProduct = null;
        List<Product> lstProduct = new List<Product>();
        int intCheckDuplicateImeiAllProductValue = Library.AppCore.AppConfig.GetIntConfigValue("ISCHECKDUPLICATEIMEIALLPRODUCT");
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = null;
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIList = null;
        private bool bolIsInputFromRetailInputPrice = false;
        private List<ProductImei> objProductImeiList = new List<ProductImei>();
        private bool bolIsInputFromOrder = false;
        //Nguyễn Văn Tài - chỉnh sửa bổ sung
        private Regex regex = new Regex(@"^\d$");
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> AllIMEIList
        {
            get { return objAllIMEIList; }
            set { objAllIMEIList = value; }
        }
        /// <summary>
        /// Nếu nhập từ định giá máy cũ
        /// </summary>
        public bool IsInputFromRetailInputPrice
        {
            get { return bolIsInputFromRetailInputPrice; }
            set { bolIsInputFromRetailInputPrice = value; }
        }
        /// <summary>
        /// Danh sách IMEI nhập
        /// </summary>
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> InputVoucherDetailIMEIList
        {
            get { return objInputVoucherDetailIMEIList; }
            set { objInputVoucherDetailIMEIList = value; }
        }
        private List<PLC.Input.WSInputVoucher.InputVoucherDetail> objInputVoucherDetailList = null;
        public List<PLC.Input.WSInputVoucher.InputVoucherDetail> InputVoucherDetailList
        {
            set { objInputVoucherDetailList = value; }
            get { return objInputVoucherDetailList; }
        }

        public frmExcelImport()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }

        public frmExcelImport(bool _bolIsInputFromOrder, List<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail> dtbData)
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            bolIsInputFromOrder = _bolIsInputFromOrder;
            this.dtbData = ConvertToDataTable(dtbData);
            this.dtbData.AsEnumerable().All(c => { c["Quantity"] = 0; return true; });
            if (!this.dtbData.Columns.Contains("IsHasWarranty"))
            {
                this.dtbData.Columns.Add("IsHasWarranty", typeof(Boolean));
            }
            this.dtbData.AsEnumerable().All(c => { c["IsHasWarranty"] = true; return true; });
            this.dtbData.Columns.Add("IsError", typeof(Boolean));
            grdData.DataSource = this.dtbData;
            dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
            if (!bolIsInputFromOrder)
            {
                colProductName.Width += colStatus.Width;
                colNote.Width += colOrderQuantity.Width;
                colOrderQuantity.Visible = false;
                colStatus.Visible = false;
            }
        }

        private void UpdateQuantity(DataTable dtbData)
        {
            for (int i = 0; i < dtbData.Rows.Count; i++)
            {
                dtbData.Rows[i]["QuantityToInput"] = int.Parse(dtbData.Rows[i]["QuantityToInput"].ToString()) + int.Parse(dtbData.Rows[i]["Quantity"].ToString());
                grdViewData.RefreshData();
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                //Hiếu thêm số lượng khi xuất Excel mẫu
                var lstData = (grdData.DataSource as DataTable).AsEnumerable();
                DataTable dtbProductId = ConvertColumnsAsRows(ConvertListToDatatable(lstData.Select(x => x["ProductID"].ToString()).ToList()));
                DataTable dtbProductName = ConvertColumnsAsRows(ConvertListToDatatable(lstData.Select(x => x["ProductName"].ToString()).ToList()));
                DataTable dtbProductQuantity = ConvertColumnsAsRowsQuantity(ConvertListToDatatable(lstData.Select(x => x["QuantityToInput"].ToString()).ToList()));
                dtbProductId.Merge(dtbProductName);
                dtbProductId.Merge(dtbProductQuantity);
                dtbProductId.Columns.RemoveAt(0);
                for (int i = 0; i < dtbProductId.Columns.Count; i++)
                {
                    dtbProductId.Columns[i].Caption = dtbProductId.Rows[0][i].ToString();
                }
                Library.AppCore.LoadControls.C1FlexGridObject.ExportDataTableToExcel(this, dtbProductId, C1.Win.C1FlexGrid.FileFlags.None, true);
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Không thể xuất file Excel mẫu!", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Không thể xuất file Excel mẫu!");
                return;
            }
        }

        public DataTable ConvertListToDatatable(List<string> lstData)
        {
            DataTable dtbData = new DataTable();
            DataColumn col = new DataColumn();
            dtbData.Columns.Add(col);
            foreach (var item in lstData)
            {
                dtbData.Rows.Add(item);
            }
            return dtbData;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
        private DataTable ConvertColumnsAsRows(DataTable dt)
        {
            DataTable dtnew = new DataTable();
            //Convert all the rows to columns 
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtnew.Columns.Add(Convert.ToString(i));
            }
            DataRow dr;
            // Convert All the Columns to Rows 
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dr = dtnew.NewRow();
                dr[0] = dt.Columns[j].ToString();
                for (int k = 1; k <= dt.Rows.Count; k++)
                    dr[k] = dt.Rows[k - 1][j];
                dtnew.Rows.Add(dr);
            }
            return dtnew;
        }

        //Nối chuỗi cho số lượng
        private DataTable ConvertColumnsAsRowsQuantity(DataTable dt)
        {
            DataTable dtnew = new DataTable();
            //Convert all the rows to columns 
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtnew.Columns.Add(Convert.ToString(i));
            }
            DataRow dr;
            // Convert All the Columns to Rows 
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dr = dtnew.NewRow();
                dr[0] = dt.Columns[j].ToString();
                for (int k = 1; k <= dt.Rows.Count; k++)
                    dr[k] = "(Số lượng cần nhập: " + dt.Rows[k - 1][j] + ")";
                dtnew.Rows.Add(dr);
            }
            return dtnew;
        }


        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang kiểm tra IMEI");
        }

        private bool ValidTemplatesExcel(DataTable dtbImeiImport, DataTable dtbGrid)
        {
            if (dtbImeiImport == null)
                return false;

            if (dtbImeiImport.Columns.Count == 0)
            {
                //  MessageBox.Show(this, "File excel không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (dtbImeiImport.Columns.Count != grdViewData.DataRowCount)
            {
                MessageBoxObject.ShowWarningMessage(this, "File Excel không đúng mẫu!");
                return false;
            }

            bool bolExistsProductID = false;
            bool bolInputDataColumn = false;
            int intExistColumn = 0;

            //Hiếu Xóa dòng số lượng
            dtbImeiImport.Rows.RemoveAt(2);

            for (int i = 0; i < dtbImeiImport.Columns.Count; i++)
            {
                string strProductID = dtbImeiImport.Rows[0][i].ToString();
                if (!string.IsNullOrEmpty(strProductID))
                {
                    //Kiểm tra template
                    if (dtbGrid.Select("[ProductID] = '" + dtbImeiImport.Rows[0][i].ToString().Trim() + "'").Length == 0)
                    {
                        intExistColumn += 1;
                        bolExistsProductID = true;
                        continue;
                    }

                    //End kiểm tra template
                    if (dtbImeiImport.AsEnumerable().Where(x => !string.IsNullOrEmpty(x[i].ToString())).Count() <= 2)
                    {
                        bolInputDataColumn = true;
                        continue;
                    }
                }
            }

            if (intExistColumn == dtbImeiImport.Columns.Count)
            {
                MessageBox.Show(this, "File import không có sản phẩm cần nhập!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (bolExistsProductID)
            {
                if (MessageBox.Show(this, "Trong file import tồn tại sản phẩm không có trong phiếu nhập.\nBạn có muốn tiếp tục?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
            }

            if (bolInputDataColumn)
            {
                MessageBox.Show(this, "Vui lòng nhập dữ liệu đầy đủ vào file import!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            //Hiếu thêm thông báo nếu có dữ liệu trên lưới

            DataTable dtbGrid = grdData.DataSource as DataTable;

            if (lstProduct != null && lstProduct.Count > 0)
            {
                if (MessageBox.Show("Toàn bộ dữ liệu trên lưới sẽ bị xóa. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                else
                {
                    lstProduct.Clear();
                    foreach (DataRow row in dtbGrid.Rows)
                    {
                        row["Quantity"] = 0;
                        row["Status"] = string.Empty;
                        row["IsError"] = false;
                        row["Note"] = string.Empty;
                    }
                }
            }

            DataTable dtbImeiImport = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
            
            //DataTable dtbGrid = grdData.DataSource as DataTable;
            if (!ValidTemplatesExcel(dtbImeiImport, dtbGrid))
                return;

            if (!dtbGrid.Columns.Contains("Status"))
            {
                DataColumn colStatus = new DataColumn();
                colStatus.ColumnName = "Status";
                dtbGrid.Columns.Add(colStatus);
            }
            if (!dtbGrid.Columns.Contains("Note"))
            {
                DataColumn colStatus = new DataColumn();
                colStatus.ColumnName = "Note";
                dtbGrid.Columns.Add(colStatus);
            }

            lstProduct = new List<Product>();
            objProductImeiList.Clear();
            foreach (DataRow rowGird in dtbGrid.Rows)
            {
                bool bolIsRequireImei = Convert.ToBoolean(rowGird["IsRequestIMEI"].ToString());
                string strProductID = rowGird["ProductID"].ToString().Trim();
                string strProductName = rowGird["ProductName"].ToString().Trim();
                rowGird["Note"] = string.Empty;
                rowGird["Status"] = string.Empty;

                for (int j = 0; j < dtbImeiImport.Columns.Count; j++)
                {
                    string strProductIDImport = dtbImeiImport.Rows[0][j].ToString();
                    if (!string.IsNullOrEmpty(strProductIDImport) && strProductIDImport == strProductID)
                    {
                        Product objProduct = null;
                        bool bolIsNew = true;
                        objProduct = lstProduct.FirstOrDefault(r => r.ProductID == strProductIDImport.Trim());
                        if (objProduct == null)
                        {
                            objProduct = new Product();
                            bolIsNew = true;
                        }
                        else
                        {
                            bolIsNew = false;
                        }

                        objProduct.ProductID = strProductID;
                        objProduct.ProductName = strProductName;

                        if (!bolIsRequireImei) // Không yêu cầu nhập Imei
                        {
                            int intQuantity = 0;
                            if (dtbImeiImport.Rows.Count > 2)
                                int.TryParse(dtbImeiImport.Rows[2][j].ToString(), out intQuantity);
                            objProduct.Quantity = intQuantity;
                        }
                        else // Sản phẩm yêu cầu nhập IMEI
                        {
                            //List<ProductImei> ImeiList = new List<ProductImei>();
                            objProduct.IsRequestImei = true;
                            objProduct.Quantity = 0;
                            for (int iRow = 2; iRow < dtbImeiImport.Rows.Count; iRow++)
                            {
                                string strImei = dtbImeiImport.Rows[iRow][j].ToString();
                                if (!string.IsNullOrEmpty(strImei))
                                {
                                    ProductImei objProductImei = new ProductImei();
                                    objProductImei.Imei = strImei.Trim().ToUpper();
                                    objProductImei.ProductID = objProduct.ProductID;
                                    objProductImei.ProductName = objProduct.ProductName;
                                    objProductImei.Error = "";
                                    if (!Library.AppCore.Other.CheckObject.CheckIMEI(strImei.Trim().ToUpper()))
                                    {
                                        objProductImei.Error = "Imei không đúng định dạng";
                                        objProductImei.IsError = true;
                                    }

                                    objProductImeiList.Add(objProductImei);
                                }
                            }
                        }
                        if (bolIsNew)
                            lstProduct.Add(objProduct);
                    }
                }

            }

            ValidateData();

            for (int i = 0; i < dtbGrid.Rows.Count; i++)
            {
                bool bolIsRequireImei = Convert.ToBoolean(dtbGrid.Rows[i]["IsRequestImei"].ToString());
                string strProductID = dtbGrid.Rows[i]["ProductID"].ToString().Trim();
                Product objProduct = lstProduct.FirstOrDefault(r => r.ProductID.Trim() == strProductID);
                string strStatus = string.Empty;
                string strError = string.Empty;
                if (bolIsRequireImei) // Yeu cau nhap IMEI
                {
                    int intAllImei = objProductImeiList.Where(r => r.ProductID == strProductID).Count();
                    int intQuantityInput = objProductImeiList.Where(r => r.ProductID == strProductID && !r.IsError).Count();
                    if (objProduct != null)
                        objProduct.Quantity = intQuantityInput;
                    dtbGrid.Rows[i]["Quantity"] = intQuantityInput;
                    if (intAllImei == 0)
                        dtbGrid.Rows[i]["Note"] = "Chưa nhập IMEI";
                    else if (intQuantityInput < intAllImei)
                        dtbGrid.Rows[i]["Note"] = "Số IMEI không hợp lệ: " + (intAllImei - intQuantityInput).ToString();
                }
                else
                {
                    if (objProduct != null)
                        dtbGrid.Rows[i]["Quantity"] = objProduct.Quantity;
                }

                if (bolIsInputFromOrder)
                {
                    decimal decUven = decimal.Parse(dtbGrid.Rows[i]["QuantityToInput"].ToString()) - decimal.Parse(dtbGrid.Rows[i]["Quantity"].ToString());

                    if (decUven == 0)
                    {
                        strStatus = "Đủ số lượng";
                    }
                    else if (decUven < 0)
                    {
                        strStatus = "Thừa số lượng";
                        if (objProduct != null)
                            objProduct.IsError = true;
                    }
                    else
                        strStatus = "Chưa đủ số lượng";
                }
                else
                {
                    if (objProduct != null)
                        objProduct.IsError = false;
                }

                dtbGrid.Rows[i]["Status"] = strStatus;
                if (objProduct != null)
                    dtbGrid.Rows[i]["IsError"] = objProduct.IsError;
            }
            grdData.RefreshDataSource();
            btnUpdate.Enabled = true;
            btnUpdate.Focus();
            grdViewData.FocusedRowHandle = -1;
        }

        private void ValidateData()
        {
            Thread objThread = new Thread(new ThreadStart(Excute));
            objThread.Start();

            var rlsImei = objProductImeiList.Where(r => !r.IsError);
            if (dtbProduct != null)
            {
                var lstDuplicate = from o in rlsImei
                                   join i in dtbProduct.AsEnumerable()
                                   on o.Imei.Trim() equals i["PRODUCTID"].ToString().Trim()
                                   select o;

                // Update trạng thái lỗi đối với Imei trùng mã sản phẩm
                lstDuplicate.All(c => { c.IsError = true; c.Error = "Số IMEI này trùng với mã sản phẩm"; return true; });
            }


            Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
            for (int i = 0; i < objProductImeiList.Count; i++)
            {
                if (objProductImeiList[i].IsError)
                    continue;

                if (dicCheckIMEI.ContainsKey(objProductImeiList[i].Imei))
                {
                    dicCheckIMEI[objProductImeiList[i].Imei].Add(i);
                }
                else
                {
                    List<int> lstIndex = new List<int>();
                    lstIndex.Add(i);
                    dicCheckIMEI.Add(objProductImeiList[i].Imei, lstIndex);
                }
            }

            foreach (string strIMEI in dicCheckIMEI.Keys)
            {
                if (dicCheckIMEI[strIMEI].Count() > 1)
                {
                    for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                    {
                        objProductImeiList[dicCheckIMEI[strIMEI][i]].IsError = true;
                        objProductImeiList[dicCheckIMEI[strIMEI][i]].Error = "Trùng IMEI trên file Excel;";
                    }
                }
            }

            // 2. Check trùng Imei trong listImei bên form dưới
            if (objAllIMEIList != null && objAllIMEIList.Count > 0)
            {
                var rlsIMei = objProductImeiList.Where(r => !r.IsError);
                var rlsExistImei = from r in rlsIMei
                                   join i in objAllIMEIList on r.Imei equals i.IMEI
                                   select r;
                if (rlsExistImei.Count() > 0)
                {
                    //var rlsImeiExistVoucher = rlsIMei.Where(r => rlsExistImei.Contains(r.Imei));
                    rlsExistImei.All(r => { r.IsError = true; r.Error = "IMEI đã tồn tại trong phiếu nhập"; return true; });
                }
            }

            #region Check trùng IMEI, PINCODE trong DB
            // Loại những Imei bị trùng trên lưới
            var listIMEI = from item in objProductImeiList.Where(r => !r.IsError)
                           select item.Imei;
            var noImeiDupes = listIMEI.Distinct().ToList();
            DataTable dtbImeiPara = new DataTable();
            dtbImeiPara.Columns.Add("IMEI");
            foreach (var item in noImeiDupes)
                dtbImeiPara.Rows.Add(item);
            string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbImeiPara, "IMEI");

            string xmlPinCode = string.Empty;
            DataSet dsResult = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("PM_CURRENTINSTOCKDT_IMEIBYPRO", new string[] { "v_Out1", "v_Out2" }, new object[] { "@IMEI", xmlIMEI, "@PINCODE", xmlPinCode });
            if (dsResult != null)
            {
                DataTable dtbResultImei = null;
                DataTable dtbResultPinCode = null;

                if (dsResult.Tables.Count > 0)
                    dtbResultImei = dsResult.Tables[0];
                if (dsResult.Tables.Count > 1)
                    dtbResultPinCode = dsResult.Tables[1];

                if (dtbResultImei != null && dtbResultImei.Rows.Count > 0)
                {
                    List<string> lstExitsImei = dtbResultImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList();
                    if (lstExitsImei.Count() > 0)
                    {
                        var lstImeiExists = from o in objProductImeiList
                                            join i in lstExitsImei
                                            on o.Imei.Trim() equals i.ToString().Trim()
                                            select o;
                        lstImeiExists.All(c => { c.IsError = true; c.Error = "Số IMEI này đã tồn tại"; return true; });
                    }
                }
            }

            #endregion

            frmWaitDialog.Close();
            Thread.Sleep(0);
            objThread.Abort();
        }


        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
                return;

            DataRow row = grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            if (row != null)
            {
                if (row["IsRequestImei"].ToString().ToLower() == "true")
                {
                    string strProductID = row["ProductID"].ToString().Trim();
                    string strProductName = row["ProductName"].ToString().Trim();
                    var rls = objProductImeiList.Where(r => r.ProductID == strProductID);
                    if (rls.Count() > 0)
                    {
                        frmExcelImportDetail frm = new frmExcelImportDetail();
                        frm.Text += strProductID + " - " + strProductName;
                        frm.ProductList = rls.ToList();
                        frm.ShowDialog();
                    }
                    else
                        MessageBoxObject.ShowWarningMessage(this, "Chưa nhập IMEI cho sản phẩm!");
                }
                else
                {
                    MessageBoxObject.ShowWarningMessage(this, "Sản phẩm này không có IMEI!");
                    return;
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!CheckValiadate())
            {
                return;
            }
            if (!InsertDataToInput()) return;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private bool InsertDataToInput()
        {
            try
            {
                foreach (var objProduct in lstProduct.Where(r => !r.IsError && r.Quantity > 0))
                {
                    if (objProduct.IsRequestImei)
                    {
                        PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = objInputVoucherDetailList.FirstOrDefault(r => r.ProductID == objProduct.ProductID);
                        foreach (var item in objProductImeiList.Where(x => x.ProductID == objProduct.ProductID && x.IsError == false))
                        {
                            PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailImei = new PLC.Input.WSInputVoucher.InputVoucherDetailIMEI();
                            DataTable dtbGrid = grdData.DataSource as DataTable;
                            objInputVoucherDetailImei.ProductID = item.ProductID;
                            objInputVoucherDetailImei.ProductName = item.ProductName;
                            objInputVoucherDetailImei.IMEI = item.Imei;
                            objInputVoucherDetailImei.IsValidate = true;
                            objInputVoucherDetailImei.Status = "";
                            objInputVoucherDetailImei.IsError = false;
                            DataRow[] dr = dtbGrid.Select("[ProductID] = '" + item.ProductID + "'");
                            bool bolIsHasWarranty = false;
                            if (dr.Length > 0)
                                bolIsHasWarranty = dr[0]["IsHasWarranty"].ToString().ToLower() == "true" ? true : false;
                            objInputVoucherDetailImei.IsHasWarranty = bolIsHasWarranty;
                            if (objInputVoucherDetail != null)
                                objInputVoucherDetailImei.IsRequirePinCode = objInputVoucherDetail.IsRequirePinCode;
                            objInputVoucherDetailIMEIList.Add(objInputVoucherDetailImei);
                        }
                    }
                    else
                    {
                        PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = objInputVoucherDetailList.Where(x => x.ProductID == objProduct.ProductID).SingleOrDefault();
                        objInputVoucherDetail.Quantity = objProduct.Quantity;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool CheckValiadate()
        {
            //Hiếu Kiểm tra số lượng nhập lớn hơn số lượng cần (Quantity > QuantityToInput)
            if (bolIsInputFromOrder)
            {
                DataTable dtbGrid = grdData.DataSource as DataTable;
                var listCheck = from o in dtbGrid.AsEnumerable()
                                where Convert.ToDecimal(o["QUANTITY"]) > Convert.ToDecimal(o["QUANTITYTOINPUT"])
                                select o;
                if (listCheck.Any())
                {
                    MessageBoxObject.ShowWarningMessage(this, "Có IMEI hoặc sản phẩm vượt quá số lượng cần nhập. Không cập nhật được danh sách!");
                    return false;
                }
            }

            if (lstProduct.Where(x => x.IsError == true).Count() > 0 || objProductImeiList.Where(x => x.IsError == true).Count() > 0)
            {
                if (MessageBox.Show(this, "Tồn tại IMEI hoặc sản phẩm không hợp lệ.\nBạn có muốn tiếp tục?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
            }
            return true;
        }


        private void grdViewData_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            DataRow row = grdViewData.GetDataRow(e.RowHandle);
            if (row != null && row["IsError"] != null && !string.IsNullOrEmpty(row["IsError"].ToString()))
            {
                bool bolError = Convert.ToBoolean(row["IsError"].ToString());
                if (bolError)
                    e.Appearance.BackColor = Color.Pink;
            }
        }

        private void grdViewData_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            //if (e.RowHandle < 0)
            //    return;

            //GridView view = sender as GridView;
            //bool bolError = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["IsError"]));
            //if (bolError)
            //    e.Appearance.BackColor = Color.LightCoral;
        }

        private void repoViewDetail_Click(object sender, EventArgs e)
        {
            grdData_DoubleClick(null, null);
        }

        private void frmExcelImport_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
        }

        private void frmExcelImport_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.F11)
            //    this.WindowState = FormWindowState.Maximized;
            //if (e.KeyCode == Keys.Escape)
            //    this.WindowState = FormWindowState.Normal;
        }

    }

    public class Product
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public bool IsRequestImei { get; set; }
        public bool IsError { get; set; }
        public string Error { get; set; }
        public List<ProductImei> lstImei { get; set; }
    }
    public class ProductImei
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string Imei { get; set; }
        public bool IsHasWarranty { get; set; }
        public bool IsError { get; set; }
        public string Error { get; set; }
    }
}
