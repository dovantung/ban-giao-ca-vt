﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.CustomControls.C1FlexGrid;
using ERP.Inventory.PLC.Output;
using ERP.Inventory.PLC.Input;


namespace ERP.Inventory.DUI.Input
{
    public partial class frmInputVoucherReturn : Form
    {
        private string strOutputVoucherID = "";
        private string strPermission_AdjustPrice = "PM_INPUTVOUCHERRETURN_ADJUSTPRICE";
        private bool bolIsReturnWithFee = false;
        private int intGetPriceType = 0;
        private int intVoucherTypeID = 0;
        private bool bolIsShowProduct = false;
        private bool bolIsLockByClosingDataMonth = false;
        private int intIsInputReturn = -1;
        private DataRow rOldEdit = null;
        private DataTable dtbStoreInStock = null;
        private ERP.Inventory.PLC.ProductChange.PLCProductChange objPLCProductChange = new PLC.ProductChange.PLCProductChange();
        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();

        private int intTotalQuantity = 0;
        private decimal decTotalAmountBF = 0;
        private decimal decTotalVAT = 0;
        private decimal decTotalAmount = 0;
        private decimal decPaymentCardSpend = 0;
        private int intGetCostPriceType = 0;
        public string OutputVoucherID
        {
            get { return strOutputVoucherID; }
            set { strOutputVoucherID = value; }
        }

        public bool IsReturnWithFee
        {
            get { return bolIsReturnWithFee; }
            set { bolIsReturnWithFee = value; }
        }

        public int IsInputReturn
        {
            get { return intIsInputReturn; }
            set { intIsInputReturn = value; }
        }

        private DataTable tblOutputVoucherDetail = null;
        private PLC.Output.WSOutputVoucher.OutputVoucher objOutputVoucher = null;
        public frmInputVoucherReturn()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        private void frmInputVoucherReturn_Load(object sender, EventArgs e)
        {
            intIsInputReturn = 0;
            cmdUpdate.Enabled = false;
            if (bolIsReturnWithFee)
            {
                chkIsOld.Checked = false;
                chkIsNew.Checked = true;
                bolIsShowProduct = false;
                mnuItemAdjustPrice.Enabled = true;
                this.Text = "Nhập trả hàng có thu phí";
            }
            if (!LoadCombobox())
                return;

            if (!LoadOutputVoucher())
                return;

            if (!LoadOutputVoucherDetail())
                return;
            if (!LoadInputType())
                return;

            if (bolIsLockByClosingDataMonth)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tháng " + dtOutputDate.Value.Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa");
                cmdUpdate.Enabled = false;
                return;
            }

            dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0, "@OutputVoucherID", strOutputVoucherID, "@IsRequestIMEI", true });
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                cmdUpdate.Enabled = false;
                return;
            }

            cmdUpdate.Enabled = true;
        }


        private bool LoadCombobox()
        {
            try
            {
                //Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboOutputStoreID, false, -1);
                cboOutputStoreID.InitControl(false, false);
                //Library.AppCore.LoadControls.SetDataSource.SetCustomer(this, cboCustomerID);
                Library.AppCore.LoadControls.SetDataSource.SetPayableType(this, cboPayableTypeID, false);
                Library.AppCore.LoadControls.SetDataSource.SetCurrencyUnit(this, cboCurrencyUnitID);
                Library.AppCore.LoadControls.SetDataSource.SetDiscountReason(this, cboDiscountReasonID, Library.AppCore.Constant.EnumType.DiscountType.INPUT);
                //Library.AppCore.LoadControls.SetDataSource.SetInputType(this, cboInputTypeID);
                //Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboInputStoreID, true, -1, -1, -1, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT);
                Library.AppCore.DataSource.FilterObject.StoreFilter obj =new Library.AppCore.DataSource.FilterObject.StoreFilter();
                obj.IsCheckPermission=true;
                obj.StorePermissionType=Library.AppCore.Constant.EnumType.StorePermissionType.INPUT;
                cboInputStoreID.InitControl(false, obj);
                Library.AppCore.LoadControls.SetDataSource.SetVoucherType(this, cboVoucherTypeID, false, Library.AppCore.Constant.EnumType.VoucherType.SPEND);
                Library.AppCore.LoadControls.SetDataSource.SetCurrencyUnit(this, cboCurrencyUnitID2);
                Library.AppCore.LoadControls.SetDataSource.SetCurrencyUnit(this, cboPaymentCurrencyUnitID);
                Library.AppCore.LoadControls.SetDataSource.SetPaymentCard(this, cboPaymentCardID);
                return true;
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nạp combobox");
                SystemErrorWS.Insert("Lỗi nạp combobox", objExec, DUIInventory_Globals.ModuleName);
                return false;
            }
        }

        private bool LoadOutputVoucher()
        {
            try
            {

                PLCOutputVoucher objPLCOutputVoucher = new PLCOutputVoucher();
                objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                ResultMessageApp objResultMessageApp = objPLCOutputVoucher.ResultMessageApp;
                if (objResultMessageApp.IsError)
                {
                    MessageBox.Show(this, objResultMessageApp.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                txtOutputVoucherID.Text = objOutputVoucher.OutputVoucherID;
                dtOutputDate.Value = objOutputVoucher.OutputDate.Value;
                try
                {
                    bolIsLockByClosingDataMonth = ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.OUTPUTVOUCHER,
                        objOutputVoucher.OutputDate.Value, objOutputVoucher.OutputStoreID);
                }
                catch { }
                dtInvoiceDate.Value = objOutputVoucher.InvoiceDate.Value;
                txtInVoiceID.Text = objOutputVoucher.InvoiceID;
                txtInvoiceSymbol.Text = objOutputVoucher.InvoiceSymbol;
                txtDenominator.Text = objOutputVoucher.Denominator;
                txtCustomerID.Text = Convert.ToString(objOutputVoucher.CustomerID);
                txtCustomerName.Text = objOutputVoucher.CustomerName;
                txtCustomerAddress.Text = objOutputVoucher.CustomerAddress;
                txtCustomerPhone.Text = objOutputVoucher.CustomerPhone;
                txtCustomerTaxID.Text = objOutputVoucher.CustomerTaxID;
                dtPayableTime.Value = objOutputVoucher.PayableDate.Value;
                txtOutputContent.Text = objOutputVoucher.OutputContent;
                txtDiscount.Value = 0;// objOutputVoucher.Discount;
                txtExchangeRate.Text = objOutputVoucher.CurrencyExchange.ToString("###,##0");
                txtOutputUser.Text = objOutputVoucher.CreatedUser + "-" + ERP.MasterData.SYS.PLC.PLCUser.GetUserFullName(objOutputVoucher.CreatedUser);
                cboOutputStoreID.SetValue(objOutputVoucher.OutputStoreID);
                txtCustomerID.Text = objOutputVoucher.CustomerID.ToString();
                Library.AppCore.LoadControls.ComboBoxObject.SetValue(cboPayableTypeID, objOutputVoucher.PayableTypeID);
                Library.AppCore.LoadControls.ComboBoxObject.SetValue(cboCurrencyUnitID, objOutputVoucher.CurrencyUnitID);
                Library.AppCore.LoadControls.ComboBoxObject.SetValue(cboCurrencyUnitID2, objOutputVoucher.CurrencyUnitID);
                //Library.AppCore.LoadControls.ComboBoxObject.SetValue(cboDiscountReasonID, objOutputVoucher.DiscountReasonID);

                ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders objPLCSaleOrders = new SalesAndServices.SaleOrders.PLC.PLCSaleOrders();
                ERP.SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrder objSaleOrder = objPLCSaleOrders.LoadInfo(objOutputVoucher.OrderID);
                if (objSaleOrder != null)
                {
                    txtTotalOrderPaid.Text = objSaleOrder.TotalPaid.ToString("###,##0");
                    PLC.Input.PLCInputVoucherReturn objPLCInputVoucherReturn = new PLCInputVoucherReturn();
                    lblVoucherAlert.Text = objPLCInputVoucherReturn.GetVoucherAlert(objSaleOrder.SaleOrderID);
                }
                else
                {
                    ERP.Production.PLC.ProductionOrder.PLCProductionOrder objPLCProductionOrder = new Production.PLC.ProductionOrder.PLCProductionOrder();
                    ERP.Production.PLC.WSProduction.ProductionOrder objProductionOrder = objPLCProductionOrder.LoadInfo(objOutputVoucher.OrderID);
                    if (objProductionOrder != null)
                        txtTotalOrderPaid.Text = objProductionOrder.TotalPaid.ToString("###,##0");
                }

                return true;
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy thông tin phiếu xuất", objExec.ToString());
                SystemErrorWS.Insert("Lỗi lấy thông tin phiếu xuất", objExec, DUIInventory_Globals.ModuleName);
                return false;
            }
        }

        private bool LoadOutputVoucherDetail()
        {
            PLCInputVoucherReturn objPLCInputVoucherReturn = new PLCInputVoucherReturn();
            tblOutputVoucherDetail = objPLCInputVoucherReturn.LoadOutputVoucherDetailForReturn(strOutputVoucherID, bolIsReturnWithFee, false);
            ResultMessageApp objResultMessageApp = objPLCInputVoucherReturn.ResultMessageApp;
            if (objResultMessageApp.IsError)
            {
                MessageBox.Show(this, objResultMessageApp.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (!tblOutputVoucherDetail.Columns.Contains("Quantity2"))
            {
                tblOutputVoucherDetail.Columns.Add("Quantity2", typeof(decimal));
            }
            for (int i = 0; i < tblOutputVoucherDetail.Rows.Count; i++)
            {
                tblOutputVoucherDetail.Rows[i]["Quantity2"] = tblOutputVoucherDetail.Rows[i]["Quantity"];
            }
            flexOutputVoucherDetail.DataSource = tblOutputVoucherDetail;
            CustomFlex();
            return true;
        }

        private void CustomFlex()
        {
            flexOutputVoucherDetail.Cols["IsSelect"].DataType = typeof(bool);
            flexOutputVoucherDetail.Cols[0].Visible = false;
            CustomizeC1FlexGrid.SetColumnVisible(flexOutputVoucherDetail, true, true, "IsSelect,OUTPUTTYPENAME,ProductID,ProductName,IMEI,Endwarrantydate,SalePrice,VAT,Quantity,TotalFee,TotalCost");
            CustomizeC1FlexGrid.SetColumnCaption(flexOutputVoucherDetail, "IsSelect", "Chọn",
                                                                           "OUTPUTTYPENAME", "Hình thức xuất",
                                                                           "ProductID", "Mã sản phẩm",
                                                                           "ProductName", "Tên sản phẩm",
                                                                           "Endwarrantydate", "Ngày hết BH",
                                                                           "SalePrice", "Đơn giá",
                                                                           "TotalFee", "Phí trả",
                                                                           "Quantity", "Số lượng",
                                                                           "TotalCost", "Thành tiền");
            CustomizeC1FlexGrid.SetColumnAllowEditing(flexOutputVoucherDetail, true, true, "IsSelect,Quantity");
            CustomizeC1FlexGrid.SetColumnFormat(flexOutputVoucherDetail, "Endwarrantydate", "dd/MM/yyyy",
                                                                         "SalePrice", "###,##0",
                                                                         "TotalFee", "###,##0",
                                                                         "Quantity", "###,##0.##",
                                                                         "TotalCost", "###,##0");
            CustomizeC1FlexGrid.SetColumnWidth(flexOutputVoucherDetail, "IsSelect", 40,
                                                                        "OUTPUTTYPENAME", 150,
                                                                        "ProductID", 110,
                                                                        "ProductName", 140,
                                                                        "IMEI", 140,
                                                                        "Endwarrantydate", 80,
                                                                        "SalePrice", 80,
                                                                        "VAT", 40,
                                                                        "Quantity", 40);

            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnDigit(flexOutputVoucherDetail, 6, 2, false, "Quantity");
        }

        private bool LoadInputType()
        {
            try
            {
                bool bolIsSelected = DataTableClass.CheckIsExist(tblOutputVoucherDetail, "IsSelect = 1");
                String strFilter = "";
                if (bolIsSelected)
                {
                    strFilter = "IsSelect = 1";
                }
                DataTable tblInputType = DataTableClass.SelectDistinct(tblOutputVoucherDetail, "RETURNINPUTTYPEID", strFilter);
                
                Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(tblInputType.Copy(), cboInputTypeID, "RETURNINPUTTYPEID", "INPUTTYPENAME", "--Chọn hình thức nhập--");
                intGetPriceType = 0;
                intGetCostPriceType = 0;
                if (cboInputTypeID.Items.Count == 2)
                {
                    cboInputTypeID.SelectedIndex = 1;
                    intGetPriceType = Convert.ToInt32(tblInputType.Rows[0]["GETPRICETYPE"]);
                    intGetCostPriceType = Convert.ToInt32(tblInputType.Rows[0]["GETCOSTPRICETYPE"]);
                    intVoucherTypeID = Convert.ToInt32(tblInputType.Rows[0]["VOUCHERTYPEID"]);
                    Library.AppCore.LoadControls.ComboBoxObject.SetValue(cboVoucherTypeID, intVoucherTypeID);
                    grpVoucher.Enabled = (intVoucherTypeID > 0);
                    grpVoucherDetail.Enabled = (intVoucherTypeID > 0);
                    cboDiscountReasonID.Enabled = ((intVoucherTypeID > 0) && Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHERRETURN_INPUTDISCOUNT"));
                    txtDiscount.ReadOnly = (!cboDiscountReasonID.Enabled);
                }
                else
                {
                    cboDiscountReasonID.Enabled = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHERRETURN_INPUTDISCOUNT");
                    txtDiscount.ReadOnly = (!cboDiscountReasonID.Enabled);
                    cboVoucherTypeID.SelectedIndex = 0;
                }
                int intIsNewCount = DataTableClass.GetIntComputeValue(tblOutputVoucherDetail, "count(IsNew)", "IsNew = 1 and IsSelect = 1");
                int intIsOldCount = DataTableClass.GetIntComputeValue(tblOutputVoucherDetail, "count(IsNew)", "IsNew = 0 and IsSelect = 1");
                chkIsNew.Checked = (intIsNewCount > 0);
                chkIsOld.Checked = (intIsOldCount > 0);
                return true;
            }
            catch (Exception objEx)
            {
                return false;
            }

        }

        private void flexOutputVoucherDetail_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexOutputVoucherDetail.Rows.Count <= flexOutputVoucherDetail.Rows.Fixed)
                return;
            if (e.Row < flexOutputVoucherDetail.Rows.Fixed)
                return;
            if (e.Col == flexOutputVoucherDetail.Cols["Quantity"].Index)
            {
                decimal decQuantity = Convert.ToDecimal(rOldEdit["Quantity2"]);
                //if (rOldEdit != null && rOldEdit["Quantity"] != DBNull.Value)
                //    decQuantity = Convert.ToDecimal(rOldEdit["Quantity"]);
                if (Convert.ToDecimal(flexOutputVoucherDetail[e.Row, "Quantity"]) > decQuantity || Convert.ToDecimal(flexOutputVoucherDetail[e.Row, "Quantity"]) < 1)
                    flexOutputVoucherDetail[e.Row, "Quantity"] = decQuantity;
            }
            else
            {
                if (flexOutputVoucherDetail.Cols[e.Col].Name.ToUpper() == "ISSELECT")
                {
                    tblOutputVoucherDetail.AcceptChanges();
                    if (Convert.ToBoolean(flexOutputVoucherDetail[e.Row, "ISSELECT"]))
                    {
                        string strProductID = Convert.ToString(flexOutputVoucherDetail[e.Row, "ProductID"]).Trim();
                        string strIMEI = Convert.ToString(flexOutputVoucherDetail[e.Row, "IMEI"]).Trim();
                        if (strIMEI != string.Empty)
                        {
                            DataRow[] drInStock = dtbStoreInStock.Select("TRIM(ProductID) = '" + strProductID + "' AND TRIM(IMEI) = '" + strIMEI + "'");
                            if (drInStock.Length > 0)
                            {
                                ERP.MasterData.PLC.MD.WSStore.Store objStoreInStock = null;
                                ERP.MasterData.PLC.MD.PLCStore objPLCStore = new MasterData.PLC.MD.PLCStore();
                                objPLCStore.LoadInfo(ref objStoreInStock, Convert.ToInt32(drInStock[0]["StoreID"]));
                                MessageBox.Show(this, "IMEI " + Convert.ToString(flexOutputVoucherDetail[e.Row, "IMEI"]).Trim() + " này đã tồn tại trên kho " + objStoreInStock.StoreName + ". Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                flexOutputVoucherDetail[e.Row, "ISSELECT"] = false;
                                return;
                            }
                        }
                    }
                    LoadInputType();
                }
            }
            UpdateTotalCost(e.Row);
        }

        private void UpdateTotalCost(int intRow)
        {
            flexOutputVoucherDetail[intRow, "TotalCost"] = Convert.ToDecimal(flexOutputVoucherDetail[intRow, "InputSalePrice"]) * Convert.ToDecimal(flexOutputVoucherDetail[intRow, "QUANTITY"]) * (1 + Convert.ToDecimal(flexOutputVoucherDetail[intRow, "VAT"]) * Convert.ToDecimal(flexOutputVoucherDetail[intRow, "VATPERCENT"]) / 10000);
            flexOutputVoucherDetail[intRow, "TotalCost2"] = Convert.ToDecimal(flexOutputVoucherDetail[intRow, "InputCOSTPRICE"]) * Convert.ToDecimal(flexOutputVoucherDetail[intRow, "QUANTITY"]) * (1 + Convert.ToDecimal(flexOutputVoucherDetail[intRow, "VAT"]) * Convert.ToDecimal(flexOutputVoucherDetail[intRow, "VATPERCENT"]) / 10000);
            if (bolIsReturnWithFee)
            {
                flexOutputVoucherDetail[intRow, "TotalFee"] = Convert.ToDecimal(flexOutputVoucherDetail[intRow, "Fee"]) * Convert.ToDecimal(flexOutputVoucherDetail[intRow, "QUANTITY"]);
            }
            ReCalcTotalMoney();
        }

        private void ReCalcTotalMoney()
        {
            GetTotalValue(ref intTotalQuantity, ref decTotalAmountBF, ref decTotalVAT, ref decTotalAmount);
            txtTotalQuantity.Text = intTotalQuantity.ToString("###,##0");

            txtTotalMoney.Text = decTotalAmount.ToString("###,##0");
            decimal dcTotalLiquidate = decTotalAmount - Convert.ToDecimal(txtDiscount.Value);
            txtTotalLiquidate.Text = dcTotalLiquidate.ToString("###,##0");
            txtTotalAmount.Text = dcTotalLiquidate.ToString("###,##0");
            //txtDebt.Text = decTotalAmount.ToString("###,##0");
            UpdateDebt();

        }

        private void GetTotalValue(ref int intTotalQuantity, ref decimal decTotalAmountBF, ref decimal decTotalVAT, ref decimal decTotalAmount)
        {
            intTotalQuantity = 0;
            decTotalAmountBF = 0;
            decTotalVAT = 0;
            decTotalAmount = 0;
            if (intGetPriceType == 3)
            {
                tblOutputVoucherDetail.AcceptChanges();
                intTotalQuantity = DataTableClass.GetIntComputeValue(tblOutputVoucherDetail, "sum(Quantity)", "ISSELECT = 1");
                return;
            }
            String strPriceCol = "InputSalePrice";
            if (intGetPriceType == 2)
                strPriceCol = "InputCostPrice";


            for (int i = 1; i < flexOutputVoucherDetail.Rows.Count; i++)
            {
                if (Convert.ToBoolean(flexOutputVoucherDetail[i, "ISSELECT"]))
                {
                    decimal decQuantity = Convert.ToDecimal(flexOutputVoucherDetail[i, "QUANTITY"]);
                    decimal decPrice = Convert.ToDecimal(flexOutputVoucherDetail[i, strPriceCol]);
                    decimal decVAT = Convert.ToDecimal(flexOutputVoucherDetail[i, "VAT"]);
                    decimal decVATPercent = Convert.ToDecimal(flexOutputVoucherDetail[i, "VATPERCENT"]);

                    if (chkLostVATInvoice.Checked)
                    {
                        decimal decTotalVAT1 = Convert.ToDecimal(flexOutputVoucherDetail[i, "SalePrice"]) * Convert.ToDecimal(decVAT * decVATPercent) / 10000;
                        decPrice = decPrice * (1 + decVAT * decVATPercent / 10000) - decTotalVAT1;
                    }
                    else
                    {
                        decTotalVAT += decPrice * decQuantity * decVAT * decVATPercent / 10000;
                    }
                    intTotalQuantity += Convert.ToInt32(decQuantity);
                    decTotalAmountBF += decPrice * decQuantity;
                }
            }

            decTotalAmount = decTotalAmountBF + decTotalVAT;


        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (bolIsLockByClosingDataMonth)
            {
                return;
            }
            tblOutputVoucherDetail.AcceptChanges();
            if (!CheckUpdate())
            {
                return;
            }
            AddInputVoucherReturn();
        }

        private bool CheckUpdate()
        {
            if (cboInputStoreID.StoreID < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn kho nhập trả.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInputStoreID.Focus();
                return false;
            }

            if (!Library.AppCore.CustomControls.C1FlexGrid.CustomizeC1FlexGrid.CheckIsSelected(flexOutputVoucherDetail, "IsSelect"))
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một sản phẩm nhập trả", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (cboInputTypeID.SelectedIndex == 0)
            {
                MessageBox.Show(this, "Bạn chỉ được phép chọn các sản phẩm cùng hình thức nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (Convert.ToDecimal(txtDiscount.Value) > 0 & cboDiscountReasonID.SelectedIndex == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn hình thức giảm giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboDiscountReasonID.Focus();
                return false;
            }

            if (Convert.ToDecimal(txtDiscount.Value) == 0 & cboDiscountReasonID.SelectedIndex > 0)
            {
                MessageBox.Show(this, "Vui nhập số tiền giảm giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtDiscount.Focus();
                return false;
            }

            decimal decTotalLiquidate = Convert.ToDecimal(txtTotalLiquidate.Text);
            decimal decCashVND = Convert.ToDecimal(txtCashVND.Text);
            decimal decCashUSD = Convert.ToDecimal(txtCashUSD.Text);
            decimal decCashUSDExchange = Convert.ToDecimal(txtCashUSDExchange.Text);
            decimal decPaymentCardAmount = Convert.ToDecimal(txtPaymentCard.Text);
            decimal decTotalCash_CardAmount = decCashVND + decCashUSDExchange + decPaymentCardAmount;

            decimal decTotalOrderPaid = Convert.ToDecimal(txtTotalOrderPaid.Text);
            decimal decTotalLiquidate1 = Convert.ToDecimal(txtTotalLiquidate.Text);

            if (decPaymentCardAmount > 0 && cboPaymentCardID.SelectedIndex == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn loại thẻ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboPaymentCardID.Focus();
                return false;
            }

            if (decPaymentCardAmount > 0 && txtPaymentCardVoucherID.Text.Trim() == "")
            {
                MessageBox.Show(this, "Vui lòng nhập số hóa đơn cà thẻ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPaymentCardVoucherID.Focus();
                return false;
            }

            if (decTotalOrderPaid >= decTotalLiquidate1)
            {
               /*if (Convert.ToDecimal(txtDebt.Text) > 0)
                {
                    MessageBox.Show(this, "Bạn phải chi đủ tiền cho phiếu chi này", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCashUSD.Focus();
                    return false;
                }*/

                if (decTotalCash_CardAmount > decTotalLiquidate1)
                {
                    MessageBox.Show(this, "Bạn không được phép chi hơn số tiền phải thanh toán", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCashUSD.Focus();
                    return false;
                }
            }
            else
            {
                if (decTotalCash_CardAmount > decTotalOrderPaid)
                {
                    MessageBox.Show(this, "Bạn không được phép chi hơn số tiền đã thu của khách", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCashUSD.Focus();
                    return false;
                }
            }

            int intIsNewCount = DataTableClass.GetIntComputeValue(tblOutputVoucherDetail, "count(IsNew)", "IsNew = 1 and IsSelect = 1");
            int intIsOldCount = DataTableClass.GetIntComputeValue(tblOutputVoucherDetail, "count(IsNew)", "IsNew = 0 and IsSelect = 1");

            if (intIsNewCount > 0 && intIsOldCount > 0)
            {
                MessageBox.Show(this, "Bạn chỉ được phép nhập trả lại các sản phẩm cùng trạng thái mới hoặc cũ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               return false;
            }

            DataRow[] drExWarrantyIMEI = tblOutputVoucherDetail.Select("IsSelect = 0 AND EXTENDWARRANTYIMEI IS NOT NULL");
            if (drExWarrantyIMEI.Length > 0)
            {
                for (int i = 0; i < drExWarrantyIMEI.Length; i++)
                {
                    string strExtendWarrantyIMEI = Convert.ToString(drExWarrantyIMEI[i]["EXTENDWARRANTYIMEI"]).Trim();
                    if (strExtendWarrantyIMEI != string.Empty)
                    {
                        if (tblOutputVoucherDetail.Select("IsSelect = 1 AND IMEI = '" + strExtendWarrantyIMEI + "'").Length > 0)
                        {
                            MessageBox.Show(this, "Vui lòng chọn nhập trả gói bảo hành mở rộng IMEI " + strExtendWarrantyIMEI, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexOutputVoucherDetail.Select(flexOutputVoucherDetail.Rows.Fixed + tblOutputVoucherDetail.Rows.IndexOf(drExWarrantyIMEI[i]), flexOutputVoucherDetail.Cols["IsSelect"].Index);
                            return false;
                        }
                    }
                }
            }

            int intCheckIsAcceptReturn = 0;

            PLC.Input.PLCInputVoucherReturn objPLCInputVoucherReturn = new PLCInputVoucherReturn();
            DataRow[] drIMEISelect = tblOutputVoucherDetail.Select("IsSelect = 1 AND IMEI IS NOT NULL");
            if (drIMEISelect.Length > 0)
            {
                for (int i = 0; i < drIMEISelect.Length; i++)
                {
                    string strIMEI = Convert.ToString(drIMEISelect[i]["IMEI"]).Trim();
                    if (strIMEI != string.Empty)
                    {
                        DataTable dtbIMEIWarranty = objPLCInputVoucherReturn.LoadOutputVoucherWarrantyExtend(strOutputVoucherID, strIMEI);
                        if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                        if (dtbIMEIWarranty.Rows.Count > 0)
                        {
                            if (Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim() != string.Empty && Convert.ToString(dtbIMEIWarranty.Rows[0]["IMEI"]).Trim() == Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim())
                            {
                                if (MessageBox.Show(this, "Phiếu xuất " + Convert.ToString(dtbIMEIWarranty.Rows[0]["OutputVoucherExtID"]).Trim() + " chưa nhập trả Care+. Nếu nhập trả IMEI sẽ không được áp dụng chính sách bảo hành mở rộng nữa. Bạn có chắc muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                                    return false;
                                else
                                    intCheckIsAcceptReturn = 1;
                            }
                        }
                    }
                }
            }

            //if (intCheckIsAcceptReturn == 0 && MessageBox.Show(this, "Bạn có chắc muốn nhập trả hàng phiếu xuất này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
            //{
            //    return false;
            //}

            return true;
        }

        private bool AddInputVoucherReturn()
        {
            
            try
            {
                cmdUpdate.Enabled = false;
                cmdClose.Enabled = false;

                String strPriceCol = "InputSalePrice";
                if (intGetPriceType == 3)
                    strPriceCol = "";
                else if (intGetPriceType == 2)
                    strPriceCol = "InputCostPrice";

                List<ERP.Inventory.PLC.Input.WSInputVoucherReturn.InputVoucherReturnDetail> objInputVoucherReturnDetailList = new List<PLC.Input.WSInputVoucherReturn.InputVoucherReturnDetail>();
                DataRow[] arrRow = tblOutputVoucherDetail.Select("IsSelect = 1");

                int intIsNewCount = DataTableClass.GetIntComputeValue(tblOutputVoucherDetail, "count(IsNew)", "IsNew = 1 and IsSelect = 1");

                foreach (DataRow objRow in arrRow)
                {
                    decimal decSalePrice = 0;
                    if (strPriceCol != "")
                        decSalePrice = Convert.ToDecimal(objRow[strPriceCol]);
                    ERP.Inventory.PLC.Input.WSInputVoucherReturn.InputVoucherReturnDetail objInputVoucherReturnDetail = new PLC.Input.WSInputVoucherReturn.InputVoucherReturnDetail();
                    objInputVoucherReturnDetail.ReturnStoreID = cboInputStoreID.StoreID;
                    objInputVoucherReturnDetail.ReturnDate = DateTime.Now;
                    objInputVoucherReturnDetail.OutputVoucherID = txtOutputVoucherID.Text.Trim();
                    objInputVoucherReturnDetail.OutputVoucherDetailID = Convert.ToString(objRow["OutputVoucherDetailID"]).Trim();
                    objInputVoucherReturnDetail.InputVoucherDetailID = Convert.ToString(objRow["InputVoucherDetailID"]).Trim();
                    objInputVoucherReturnDetail.ProductID = Convert.ToString(objRow["ProductID"]).Trim();
                    objInputVoucherReturnDetail.Quantity = Convert.ToInt32(objRow["Quantity"]);
                    objInputVoucherReturnDetail.VAT = Convert.ToInt32(objRow["VAT"]);
                    objInputVoucherReturnDetail.VATPercent = Convert.ToInt32(objRow["VATPercent"]);
                    objInputVoucherReturnDetail.IMEI = Convert.ToString(objRow["IMEI"]).Trim();
                    objInputVoucherReturnDetail.IsReturnWithFee = bolIsReturnWithFee;
                    objInputVoucherReturnDetail.SalePrice = decSalePrice;
                    objInputVoucherReturnDetail.ReturnFee = Convert.ToDecimal(objRow["TotalFee"]);
                    objInputVoucherReturnDetail.OriginalReturnPrice = decSalePrice;
                    objInputVoucherReturnDetail.TotalVATLost = 0;
                    objInputVoucherReturnDetail.AdjustPrice = 0;
                    objInputVoucherReturnDetail.ReturnPrice = decSalePrice;
                    objInputVoucherReturnDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    objInputVoucherReturnDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objInputVoucherReturnDetail.IsNew = (intIsNewCount > 0);
                    objInputVoucherReturnDetail.IsShowProduct = bolIsShowProduct;
                    objInputVoucherReturnDetail.AdjustPriceUser = "";
                    objInputVoucherReturnDetail.AdjustPriceContent = "";
                    if (intGetCostPriceType == 1)
                    {
                        objInputVoucherReturnDetail.CostPrice = Convert.ToDecimal(objRow["InputCostPrice"]);
                    }
                    else if (intGetCostPriceType == 2)
                        objInputVoucherReturnDetail.CostPrice = decSalePrice;
                    objInputVoucherReturnDetailList.Add(objInputVoucherReturnDetail);
                }

                GetTotalValue(ref intTotalQuantity, ref decTotalAmountBF, ref decTotalVAT, ref decTotalAmount);
                ERP.Inventory.PLC.Input.WSInputVoucherReturn.InputVoucherReturn objInputVoucherReturn = new PLC.Input.WSInputVoucherReturn.InputVoucherReturn();
                objInputVoucherReturn.OutputVoucherID = txtOutputVoucherID.Text.Trim();
                objInputVoucherReturn.OutVoucherID = txtVoucherID.Text.Trim();
                objInputVoucherReturn.ReturnStoreID = cboInputStoreID.StoreID;
                objInputVoucherReturn.ReturnDate = DateTime.Now;
                objInputVoucherReturn.IsReturnWithFee = bolIsReturnWithFee;
                objInputVoucherReturn.ReturnReason = txtReturnReason.Text.Trim();
                objInputVoucherReturn.ReturnNote = txtReturnNote.Text.Trim();
                objInputVoucherReturn.Discount = Convert.ToDecimal(txtDiscount.Value);
                objInputVoucherReturn.TotalAmountBFT = decTotalAmountBF;
                objInputVoucherReturn.TotalVAT = decTotalVAT;
                objInputVoucherReturn.TotalAmount = decTotalAmount;
                objInputVoucherReturn.TotalVATLost = 0;
                objInputVoucherReturn.TotalReturnFee = 0;
                objInputVoucherReturn.InVoiceID = txtInVoiceID.Text.Trim();
                objInputVoucherReturn.InVoiceSymbol = txtInvoiceSymbol.Text.Trim();
                objInputVoucherReturn.Denominator = txtDenominator.Text.Trim();
                objInputVoucherReturn.CustomerID = Convert.ToInt32(txtCustomerID.Text);//Convert.ToInt32(cboCustomerID.SelectedValue);
                objInputVoucherReturn.CustomerName = txtCustomerName.Text.Trim();
                objInputVoucherReturn.CustomerAddress = txtCustomerAddress.Text.Trim();
                objInputVoucherReturn.CustomerPhone = txtCustomerPhone.Text.Trim();
                objInputVoucherReturn.CustomerTaxID = txtCustomerTaxID.Text.Trim();
                objInputVoucherReturn.InputTypeID = Convert.ToInt32(cboInputTypeID.SelectedValue);
                objInputVoucherReturn.PayableTypeID = Convert.ToInt32(cboPayableTypeID.SelectedValue);
                objInputVoucherReturn.CurrencyUnitID = Convert.ToInt32(cboCurrencyUnitID.SelectedValue);
                objInputVoucherReturn.CurrencyExchange = 1;
                objInputVoucherReturn.DiscountReasonID = Convert.ToInt32(cboDiscountReasonID.SelectedValue);
                objInputVoucherReturn.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objInputVoucherReturn.CreatedUser = SystemConfig.objSessionUser.UserName;
                objInputVoucherReturn.InputVoucherReturnDetailList = objInputVoucherReturnDetailList.ToArray();
                objInputVoucherReturn.IsNew = (intIsNewCount > 0);

                ERP.Inventory.PLC.Input.WSInputVoucherReturn.VoucherDetail objVoucherDetail = new PLC.Input.WSInputVoucherReturn.VoucherDetail();
                objVoucherDetail.VNDCash = Convert.ToDecimal(txtCashVND.Text);
                objVoucherDetail.ForeignCash = Convert.ToDecimal(txtCashUSD.Text);
                objVoucherDetail.ForeignCashExchange = Convert.ToDecimal(txtCashUSDExchange.Text);
                objVoucherDetail.PaymentCardAmount = Convert.ToDecimal(txtPaymentCard.Text);
                objVoucherDetail.PaymentCardID = Convert.ToInt32(cboPaymentCardID.SelectedValue);
                objVoucherDetail.PaymentCardSpend = decPaymentCardSpend;
                objVoucherDetail.VoucherDate = dtSettlementDate.Value;
                objVoucherDetail.RefundAmount = 0;
                objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                objVoucherDetail.PaymentCardVoucherID = txtPaymentCardVoucherID.Text.Trim();

                objVoucherDetail.VoucherStoreID = cboInputStoreID.StoreID;
                objVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;

                ERP.Inventory.PLC.Input.WSInputVoucherReturn.Voucher objVoucher = new PLC.Input.WSInputVoucherReturn.Voucher();
                objVoucher.VoucherStoreID = cboInputStoreID.StoreID;
                objVoucher.InvoiceID = txtInVoiceID.Text.Trim();
                objVoucher.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                objVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                objVoucher.CustomerName = txtCustomerName.Text.Trim();
                objVoucher.CustomerAddress = txtCustomerAddress.Text.Trim();
                objVoucher.CustomerPhone = txtCustomerPhone.Text.Trim();
                objVoucher.CustomerTaxID = txtCustomerTaxID.Text.Trim();
                objVoucher.ProvinceID = 0;
                objVoucher.DistrictID = 0;
                objVoucher.Gender = false;
                objVoucher.AgeRangeID = 0;
                objVoucher.VoucherDate = DateTime.Now;
                objVoucher.InvoiceDate = DateTime.Now;
                objVoucher.OrderID = "";
                objVoucher.VoucherConcern = "";
                objVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objVoucher.Content = txtContent.Text.Trim();

                objVoucher.VoucherID = txtVoucherID.Text.Trim();
                objVoucher.VoucherTypeID = Convert.ToInt32(cboVoucherTypeID.SelectedValue);
                objVoucher.CurrencyUnitID = Convert.ToInt32(cboCurrencyUnitID2.SelectedValue);
                objVoucher.CurrencyExchange = Convert.ToDecimal(txtExchangeRate.Text);
                objVoucher.TotalMoney = decTotalAmount;
                objVoucher.Discount = Convert.ToDecimal(txtDiscount.Text);
                objVoucher.TotalLiquidate = decTotalAmount - objVoucher.Discount;

                objVoucher.Debt = Convert.ToDecimal(txtDebt.Text);
                objVoucher.VAT = 9;
                objVoucher.IsInVoucherOfSaleOrder = false;
                objVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objVoucher.OrderID = "";
                objVoucher.IsSupplementary = false;
                objVoucher.ShippingCost = 0;
                objVoucher.VoucherDetailList = new[] { objVoucherDetail };

                PLC.Input.PLCInputVoucherReturn objPLCInputVoucherReturn = new PLCInputVoucherReturn();
                objPLCInputVoucherReturn.InsertMasterAndDetailAndVoucher(objInputVoucherReturn, objVoucher);
                if (objPLCInputVoucherReturn.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objPLCInputVoucherReturn.ResultMessageApp.Message, objPLCInputVoucherReturn.ResultMessageApp.MessageDetail);
                    //cmdUpdate.Enabled = true;
                    cmdClose.Enabled = true;
                    return false;
                }
                intIsInputReturn = 1;
                MessageBox.Show(this, "Nhập trả hàng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmdClose.Enabled = true;
                this.Close();
                return true;
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nhập trả hàng");
                SystemErrorWS.Insert("Lỗi nhập trả hàng", objExec, DUIInventory_Globals.ModuleName);
                cmdUpdate.Enabled = true;
                cmdClose.Enabled = true;
                return false;
            }

        }

        private void txtCashVND_EditValueChanged(object sender, EventArgs e)
        {
            UpdateDebt();
            cboPaymentCardID_SelectionChangeCommitted(null, null);
        }

        private void UpdateDebt()
        {
            decimal decTotalLiquidate = Convert.ToDecimal(txtTotalLiquidate.Text);
            decimal decCashVND = Convert.ToDecimal(txtCashVND.Text);
            decimal decCashUSD = Convert.ToDecimal(txtCashUSD.Text);
            decimal decCashUSDExchange = Convert.ToDecimal(txtCashUSDExchange.Text);
            decimal decPaymentCardAmount = Convert.ToDecimal(txtPaymentCard.Text);
            decimal decTotalCash_CardAmount = decCashVND + decCashUSDExchange + decPaymentCardAmount;
            decimal decDebt = decTotalLiquidate;
            if (decTotalLiquidate > decTotalCash_CardAmount)
            {
                decDebt = decTotalLiquidate - decTotalCash_CardAmount;
            }
            else
            {
                decDebt = 0;
            }
            txtDebt.Text = decDebt.ToString("###,##0");
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtDiscount_ValueChanged(object sender, EventArgs e)
        {
            txtDiscount.UpdateValueWithCurrentText();
            txtVoucherDiscount.Value = txtDiscount.Value;
            ReCalcTotalMoney();
        }

        private void flexOutputVoucherDetail_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexOutputVoucherDetail.Rows.Count <= flexOutputVoucherDetail.Rows.Fixed)
                return;
            if (e.Row < flexOutputVoucherDetail.Rows.Fixed)
                return;
            if (e.Row >= flexOutputVoucherDetail.Rows.Fixed)
            {
                rOldEdit = tblOutputVoucherDetail.NewRow();
                DataRow rData = tblOutputVoucherDetail.Rows[e.Row - flexOutputVoucherDetail.Rows.Fixed];
                rOldEdit.ItemArray = rData.ItemArray;

                if (e.Col == flexOutputVoucherDetail.Cols["Quantity"].Index)
                {
                    if (rData["IMEI"] != DBNull.Value && Convert.ToString(rData["IMEI"]).Trim() != string.Empty)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void lblCustomer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ERP.MasterData.DUI.Search.frmCustomerSearch frm = new MasterData.DUI.Search.frmCustomerSearch(false);
            frm.ShowDialog();
            if (frm.Customer != null && frm.Customer.CustomerID > 0)
            {
                txtCustomerID.Text = frm.Customer.CustomerID.ToString();
                txtCustomerName.Text = frm.Customer.CustomerName;
                txtCustomerPhone.Text = frm.Customer.CustomerPhone;
                txtCustomerAddress.Text = frm.Customer.CustomerAddress;
                txtCustomerTaxID.Text = frm.Customer.CustomerTaxID;
            }
            else
            {
                if (txtCustomerID.Text != string.Empty)
                {
                    if (MessageBox.Show(this, "Bạn muốn bỏ chọn khách hàng hiện tại không?","Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                }
                txtCustomerID.Text = string.Empty;
                txtCustomerName.Text = string.Empty;
                txtCustomerPhone.Text = string.Empty;
                txtCustomerAddress.Text = string.Empty;
                txtCustomerTaxID.Text = string.Empty;
            }
        }

        private void cboPaymentCardID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboPaymentCardID.SelectedIndex < 1)
            {
                decPaymentCardSpend = 0;
                txtTotalPaymentCard.Text = "0";
                txtPaymentCardVoucherID.Text = string.Empty;
                txtPaymentCardVoucherID.ReadOnly = true;
            }
            else
            {
                decPaymentCardSpend = ERP.MasterData.PLC.MD.PLCPaymentCard.CalculatePaymentCardSpend(Convert.ToInt32(cboPaymentCardID.SelectedValue), Convert.ToDecimal(txtPaymentCard.Text));
                decimal decTotalPaymentCard = decPaymentCardSpend + Convert.ToDecimal(txtPaymentCard.Text);
                txtTotalPaymentCard.Text = decTotalPaymentCard.ToString();
                txtPaymentCardVoucherID.ReadOnly = false;
            }
        }

        private void mnuFlexPopup_Opening(object sender, CancelEventArgs e)
        {
            mnuItemAdjustPrice.Enabled = false;
            if (flexOutputVoucherDetail.RowSel < flexOutputVoucherDetail.Rows.Fixed)
            {
                return;
            }
            if (bolIsReturnWithFee)
            {
                if (Convert.ToBoolean(flexOutputVoucherDetail[flexOutputVoucherDetail.RowSel, "IsSelect"]))
                {
                    mnuItemAdjustPrice.Enabled = true;
                }
                else
                {
                    mnuItemAdjustPrice.Enabled = false;
                }
            }
        }

        private void mnuItemAdjustPrice_Click(object sender, EventArgs e)
        {
            if (flexOutputVoucherDetail.RowSel < flexOutputVoucherDetail.Rows.Fixed)
            {
                return;
            }
            int intRowEdit = flexOutputVoucherDetail.RowSel;
            if (!Convert.ToBoolean(flexOutputVoucherDetail[intRowEdit, "IsSelect"]) || Convert.ToDecimal(flexOutputVoucherDetail[intRowEdit, "SalePrice"]) == 0)
                return;

            ERP.SalesAndServices.SaleOrders.DUI.frmAdjustPrice frmAdjustPrice1 = new ERP.SalesAndServices.SaleOrders.DUI.frmAdjustPrice(strPermission_AdjustPrice);
            frmAdjustPrice1.AdjustProductName = Convert.ToString(flexOutputVoucherDetail[intRowEdit, "ProductName"]).Trim();
            frmAdjustPrice1.CurrentPrice = Convert.ToDecimal(flexOutputVoucherDetail[intRowEdit, "OriginalPrice"]);
            frmAdjustPrice1.VAT = Convert.ToInt32(flexOutputVoucherDetail[intRowEdit, "VAT"]);
            frmAdjustPrice1.VATPercent = Convert.ToInt32(flexOutputVoucherDetail[intRowEdit, "VATPercent"]);
            frmAdjustPrice1.ShowDialog();
            if (frmAdjustPrice1.IsUpdate)
            {
                flexOutputVoucherDetail[intRowEdit, "InputSalePrice"] = frmAdjustPrice1.Price;
                flexOutputVoucherDetail[intRowEdit, "AdjustPriceContent"] = frmAdjustPrice1.AdjustPriceContent;
                flexOutputVoucherDetail[intRowEdit, "AdjustPriceUser"] = frmAdjustPrice1.AdjustPriceUser;
                flexOutputVoucherDetail[intRowEdit, "AdjustPrice"] = frmAdjustPrice1.AdjustPrice;
                flexOutputVoucherDetail[intRowEdit, "TotalCost"] = Convert.ToDecimal(flexOutputVoucherDetail[intRowEdit, "InputSalePrice"]) * Convert.ToDecimal(flexOutputVoucherDetail[intRowEdit, "QUANTITY"]) * (Convert.ToDecimal(1) + Convert.ToDecimal(flexOutputVoucherDetail[intRowEdit, "VAT"]) * Convert.ToDecimal(flexOutputVoucherDetail[intRowEdit, "VATPERCENT"]) / Convert.ToDecimal(10000));

                UpdateTotalCost(intRowEdit);
            }
        }

    }
}
