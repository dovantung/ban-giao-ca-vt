﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Text.RegularExpressions;
using Library.AppCore.Forms;
using System.Threading;
using System.Diagnostics;
using System.Xml;

namespace ERP.Inventory.DUI.Input
{
    /// <summary>
    /// Created by: Nguyễn Linh Tuấn
    /// Desc: Nhập Imei
    /// </summary>
    public partial class frmInputVoucher_ValidBarcode : Form
    {
        public frmInputVoucher_ValidBarcode()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            intWidthForm = this.Width;
            intHeightForm = this.Height;
        }

        #region Variable
        private Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection();
        int intCheckDuplicateImeiAllProductValue = Library.AppCore.AppConfig.GetIntConfigValue("ISCHECKDUPLICATEIMEIALLPRODUCT");
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = null;
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objInputVoucherDetailIMEIListOld = null;
        private string strProductID = string.Empty;
        private string strProductNameText = string.Empty;
        private int intInputStoreID = 0;
        private bool bolIsPendingProcess = false;
        private bool bolIsImportExcel = false;
        private int intWidthForm = 0;
        private int intHeightForm = 0;
        private string strIMEIFormatEXP = string.Empty;
        private decimal decPrice = 0;
        private decimal decOrderQuantity = 0;
        private int intCountFinish = 0;
        private bool bolIsAutoCreateIMEI = false;
        private bool bolIsValidateAccept = false;
        private DataTable dtbIMEIImport = null;
        private bool bolIsRequirePincode = false;
        private bool bolIsInputFromRetailInputPrice = false;
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIList = null;
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objNewImeiList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
        private bool isCloseForm = false;
        private DataTable dtbProduct = null;

        //Nguyễn Văn Tài - chỉnh sửa bổ sung
        private decimal intWarranty = -1;
        private Regex regex = new Regex(@"^\d$");
        //End

        //Phạm Hải Đăng bổ sung không check tồn 
        private bool bolIsRequireIMEI = true;
        #endregion

        #region Property
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> AllIMEIList
        {
            get { return objAllIMEIList; }
            set { objAllIMEIList = value; }
        }
        /// <summary>
        /// Nếu nhập từ định giá máy cũ
        /// </summary>
        public bool IsInputFromRetailInputPrice
        {
            get { return bolIsInputFromRetailInputPrice; }
            set { bolIsInputFromRetailInputPrice = value; }
        }
        /// <summary>
        /// Danh sách IMEI nhập
        /// </summary>
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> InputVoucherDetailIMEIList
        {
            get { return objInputVoucherDetailIMEIList; }
            set { objInputVoucherDetailIMEIList = value; }
        }
        private List<PLC.Input.WSInputVoucher.InputVoucherDetail> objInputVoucherDetailList = null;
        public List<PLC.Input.WSInputVoucher.InputVoucherDetail> InputVoucherDetailList
        {
            set { objInputVoucherDetailList = value; }
        }
        /// <summary>
        /// Tên sản phẩm
        /// </summary>
        public string ProductNameText
        {
            set { strProductNameText = value; }
        }
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string ProductID
        {
            set { strProductID = value; }
            get { return strProductID; }
        }
        /// <summary>
        /// Kho nhập
        /// </summary>
        public int InputStoreID
        {
            set { intInputStoreID = value; }
        }
        /// <summary>
        /// Kiểm tra định dạng IMEI theo sản phẩm
        /// </summary>
        public string IMEIFormatEXP
        {
            set { strIMEIFormatEXP = value; }
        }
        /// <summary>
        /// Giá
        /// </summary>
        public decimal Price
        {
            set { decPrice = value; }
        }
        /// <summary>
        /// Số lượng đơn hàng
        /// </summary>
        public decimal OrderQuantity
        {
            set { decOrderQuantity = value; }
        }
        /// <summary>
        /// Tạo IMEI tự động
        /// </summary>
        public bool IsAutoCreateIMEI
        {
            set { bolIsAutoCreateIMEI = value; }
        }
        /// <summary>
        /// Đồng ý xác nhận
        /// </summary>
        public bool IsValidateAccept
        {
            get { return bolIsValidateAccept; }
        }

        /// <summary>
        /// Có yêu cầu nhập PinCode hay không
        /// </summary>
        public bool IsRequirePincode
        {
            get { return bolIsRequirePincode; }
            set { bolIsRequirePincode = value; }
        }

        /// <summary>
        /// Có yêu cầu nhập IMEI hay không
        /// </summary>
        public bool IsRequireIMEI
        {
            get { return bolIsRequireIMEI; }
            set { bolIsRequireIMEI = value; }
        }

        #endregion
        #region Function
        /// <summary>
        /// Kiểm tra tồn tại IMEI chưa xác nhận hoặc xác nhận lỗi
        /// </summary>
        /// <returns></returns>
        private bool CheckValidate()
        {
            var objCheck = from o in objInputVoucherDetailIMEIList
                           where o.IsValidate == true
                           select o;
            //if (objCheck.Count() == 0)
            //{
            //    MessageBox.Show("Không có IMEI hợp lệ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return false;
            //}
            if (decOrderQuantity > 0 && objCheck.Count() > decOrderQuantity)
            {
                MessageBox.Show("Số lượng IMEI nhập vào vượt quá số lượng yêu cầu nhập!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Xác nhận IMEI
        /// </summary>
        private void ValidateData()
        {

            List<string> lstPincode = new List<string>();
            var objCheck = from o in objInputVoucherDetailIMEIList
                           where !string.IsNullOrEmpty(o.IMEI)
                           & !o.Status.Contains("Số IMEI không đúng định dạng") & !o.Status.Contains("IMEI đã tồn tại trong phiếu nhập")
                           & !o.Status.Contains("Sản phẩm không yêu cầu nhập IMEI") & !o.Status.Contains("Trùng IMEI trên lưới")
                           & !o.Status.Contains("IMEI chưa nhập")
                           select o;

            if (objCheck.Count() > 0)
            {
                //Nguyễn Văn Tài - chỉnh sửa - tối ưu tốc độ
                Thread objThread = new Thread(new ThreadStart(Excute));
                objThread.Start();

                #region Check trùng PinCode trên lưới
                if (bolIsRequirePincode)
                {
                    // Pincode đã nhập chưa nhập
                    var rlsEnterPinCode = objCheck.Where(x => !string.IsNullOrEmpty(x.PINCode));
                    rlsEnterPinCode.All(c => { c.IsError = false; c.IsValidate = true; c.Status = c.Status.Replace("Vui lòng nhập Pincode cho IMEI này", "").Replace("PinCode chưa nhập;", "").Replace("PinCode đã tồn tại;", ""); return true; });

                    // PinCode chưa nhập
                    var rlsNonPinCode = objCheck.Where(x => string.IsNullOrEmpty(x.PINCode));
                    rlsNonPinCode.All(c => { c.IsError = true; c.IsValidate = false; c.Status = "Vui lòng nhập Pincode cho IMEI này"; return true; });

                    // Check trùng PINCODE trên lưới
                    //var rlsPincode = from r in objInputVoucherDetailIMEIList
                    //                 group r by r.PINCode into g
                    //                 where !string.IsNullOrEmpty(g.Key) && g.Count() > 1
                    //                 select g.Key;

                    //foreach (var itemi in rlsPincode)
                    //{
                    //    bool bolError = false;
                    //    var obj = objInputVoucherDetailIMEIList.Where(r => r.PINCode == itemi).FirstOrDefault();
                    //    if (obj != null)
                    //        bolError = obj.IsError;
                    //    objInputVoucherDetailIMEIList.Where(r => r.PINCode == itemi).All(r => { r.IsError = true; r.IsValidate = false; r.Status = r.Status.Replace("Trùng PinCode trên file Excel;", "") + "Trùng PinCode trên file Excel;"; return true; });
                    //    if (obj != null && !bolError)
                    //    {
                    //        obj.IsError = false;
                    //        obj.IsValidate = true;
                    //    }
                    //    if (obj != null)
                    //        obj.Status = obj.Status.Replace("Trùng PinCode trên file Excel;", "");
                    //}

                    Dictionary<string, List<int>> dicCheckPINCode = new Dictionary<string, List<int>>();
                    for (int i = 0; i < objInputVoucherDetailIMEIList.Count; i++)
                    {
                        if (dicCheckPINCode.ContainsKey(objInputVoucherDetailIMEIList[i].PINCode))
                        {
                            dicCheckPINCode[objInputVoucherDetailIMEIList[i].PINCode].Add(i);
                        }
                        else
                        {
                            List<int> lstIndex = new List<int>();
                            lstIndex.Add(i);
                            dicCheckPINCode.Add(objInputVoucherDetailIMEIList[i].PINCode, lstIndex);
                        }
                    }

                    foreach (string strPINCode in dicCheckPINCode.Keys)
                    {
                        if (dicCheckPINCode[strPINCode].Count() > 1)
                        {
                            for (int i = 1; i < dicCheckPINCode[strPINCode].Count(); i++)
                            {
                                objInputVoucherDetailIMEIList[dicCheckPINCode[strPINCode][i]].IsError = true;
                                objInputVoucherDetailIMEIList[dicCheckPINCode[strPINCode][i]].IsValidate = false;
                                objInputVoucherDetailIMEIList[dicCheckPINCode[strPINCode][i]].Status += objInputVoucherDetailIMEIList[dicCheckPINCode[strPINCode][i]].Status.Replace("Trùng PinCode trên file Excel;", "") + "Trùng PinCode trên file Excel;";
                            }
                        }
                    }
                }
                #endregion

                #region Check trùng IMEI trên lưới
                // 1. Check Trùng IMEI trên lưới
                //var rls = from item in objInputVoucherDetailIMEIList
                //          group item by item.IMEI into g
                //          where !string.IsNullOrEmpty(g.Key) && g.Count() > 1
                //          select g.Key;

                //foreach (var item in rls)
                //{
                //    bool bolError = false;
                //    var obj = objCheck.Where(r => r.IMEI == item).FirstOrDefault();
                //    if (obj != null)
                //        bolError = obj.IsError;
                //    var rlsImeiDup = objCheck.Where(r => r.IMEI == item);
                //    rlsImeiDup.All(r => { r.IsError = true; r.IsValidate = false; r.Status = r.Status.Replace("Trùng IMEI trên lưới;", "") + "Trùng IMEI trên lưới;"; return true; });
                //    if (obj != null && !bolError)
                //    {
                //        obj.IsError = false;
                //        obj.IsValidate = true;
                //    }
                //    if (obj != null)
                //        obj.Status = obj.Status.Replace("Trùng IMEI trên lưới;", "");
                //}

                Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
                for (int i = 0; i < objInputVoucherDetailIMEIList.Count; i++)
                {
                    if (dicCheckIMEI.ContainsKey(objInputVoucherDetailIMEIList[i].IMEI))
                    {
                        dicCheckIMEI[objInputVoucherDetailIMEIList[i].IMEI].Add(i);
                    }
                    else
                    {
                        List<int> lstIndex = new List<int>();
                        lstIndex.Add(i);
                        dicCheckIMEI.Add(objInputVoucherDetailIMEIList[i].IMEI, lstIndex);
                    }
                }

                foreach (string strIMEI in dicCheckIMEI.Keys)
                {
                    if (dicCheckIMEI[strIMEI].Count() > 1)
                    {
                        for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                        {
                            objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].IsError = true;
                            objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].IsValidate = false;
                            objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].Status = objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].Status.Replace("Trùng IMEI trên lưới;", "") + "Trùng IMEI trên lưới;";
                        }
                    }
                }

                #endregion

                #region Check trùng IMEI, PINCODE trong DB
                // Loại những Imei bị trùng trên lưới
                var listIMEI = objCheck.Where(r => !string.IsNullOrEmpty(r.IMEI) && r.Status != null && !r.Status.Contains("Trùng IMEI trên lưới"));
                var rlsImei = listIMEI.Select(x => x.IMEI);
                var noImeiDupes = rlsImei.Distinct().ToList();
                DataTable dtbImeiPara = new DataTable();
                dtbImeiPara.Columns.Add("IMEI");
                foreach (var item in noImeiDupes)
                    dtbImeiPara.Rows.Add(item);
                string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbImeiPara, "IMEI");

                string xmlPinCode = string.Empty;
                if (bolIsRequirePincode)
                {
                    var listPinCode = objCheck.Where(r => !string.IsNullOrEmpty(r.PINCode) && r.Status != null && !r.Status.Contains("Trùng IMEI trên lưới") && !r.Status.Contains("Trùng PinCode trên file Excel"));
                    var rlsPinCode = listPinCode.Select(x => x.PINCode);
                    var noPinCodeDupes = rlsPinCode.Distinct().ToList();
                    DataTable dtbPinCodePara = new DataTable();
                    dtbPinCodePara.Columns.Add("PINCODE");
                    foreach (var item in noPinCodeDupes)
                        dtbPinCodePara.Rows.Add(item);
                    xmlPinCode = DUIInventoryCommon.CreateXMLFromDataTable(dtbPinCodePara, "PINCODE");
                }
                if (bolIsRequireIMEI)
                {
                    DataSet dsResult = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("PM_CURRENTINSTOCKDT_IMEIBYPRO", new string[] { "v_Out1", "v_Out2" }, new object[] { "@IMEI", xmlIMEI, "@PINCODE", xmlPinCode });
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->ValidateData");
                    }
                    if (dsResult != null)
                    {
                        DataTable dtbResultImei = null;
                        DataTable dtbResultPinCode = null;

                        if (dsResult.Tables.Count > 0)
                            dtbResultImei = dsResult.Tables[0];
                        if (dsResult.Tables.Count > 1)
                            dtbResultPinCode = dsResult.Tables[1];

                        if (dtbResultPinCode != null && dtbResultPinCode.Rows.Count > 0)
                        {
                            List<string> lstExitsPinCode = dtbResultPinCode.AsEnumerable().Select(x => x["PINCODE"].ToString()).ToList();
                            var listPinCode = objCheck.Where(r => !string.IsNullOrEmpty(r.PINCode) && r.Status != null && !r.Status.Contains("Trùng IMEI trên lưới") && !r.Status.Contains("Trùng PinCode trên file Excel"));
                            var lstPinCodeExists = from o in listPinCode
                                                   join i in lstExitsPinCode
                                                   on o.PINCode.Trim() equals i.ToString().Trim()
                                                   select o;
                            if (lstPinCodeExists.Count() > 0)
                                lstPinCodeExists.All(c => { c.IsError = true; c.IsValidate = false; c.Status = "PINCODE đã tồn tại;"; return true; });
                        }

                        if (dtbResultImei != null && dtbResultImei.Rows.Count > 0)
                        {
                            List<string> lstExitsImei = dtbResultImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList();
                            var lstImeiExists = from o in listIMEI
                                                join i in lstExitsImei
                                                on o.IMEI.Trim() equals i.ToString().Trim()
                                                select o;
                            lstImeiExists.All(c => { c.IsError = true; c.IsValidate = false; c.Status = "IMEI đã tồn tại;"; return true; });
                        }
                    }
                }
                #endregion

                if (dtbProduct != null)
                {
                    var lstDuplicate = from o in objCheck
                                       join i in dtbProduct.AsEnumerable()
                                       on o.IMEI.Trim() equals i["PRODUCTID"].ToString().Trim()
                                       select o;
                    lstDuplicate.All(c => { c.IsError = true; c.IsValidate = false; c.Status = c.Status.Replace("IMEI trùng với mã sản phẩm;", "") + "IMEI trùng với mã sản phẩm;"; return true; });
                }

                frmWaitDialog.Close();
                Thread.Sleep(0);
                objThread.Abort();
            }

            GetTotal();
            grdIMEI.RefreshDataSource();
            if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) > 0)
            {
                if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) == Convert.ToInt32(InputVoucherDetailIMEIList.Count))
                {
                    MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + InputVoucherDetailIMEIList.Count.ToString("#,##0"),
                   "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + InputVoucherDetailIMEIList.Count.ToString("#,##0") + "\n Bạn có muốn tiếp tục không?",
                   "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        btnClose_Click(null, null);
                    }
                }
            }
            else
            {
                btnClose_Click(null, null);
            }
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang kiểm tra IMEI");
        }

        /// <summary>
        /// Xóa IMEI không hợp lệ hoặc chưa xác nhận
        /// </summary>
        private void RemoveInValidData()
        {
            for (int i = InputVoucherDetailIMEIList.Count - 1; i >= 0; i--)
            {
                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailIMEI = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)InputVoucherDetailIMEIList[i];
                if (!objInputVoucherDetailIMEI.IsValidate || objInputVoucherDetailIMEI.IsError)
                {
                    InputVoucherDetailIMEIList.RemoveAt(i);
                }
            }
            grdIMEI.RefreshDataSource();
        }

        #endregion

        private void btnValidate_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            frmWaitDialog.Close();
            Thread.Sleep(0);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CheckValidate())
            {
                RemoveInValidData();
                isCloseForm = true;
                this.DialogResult = DialogResult.OK;

                //if (MessageBox.Show(this, "Tồn tại IMEI chưa xác nhận hoặc không hợp lệ.\r\nKhi đóng màn hình các IMEI đó sẽ bị mất!\r\nBạn có chắc muốn đóng không?",
                //    "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                //{
                //    return;
                //}
                //else
                //{
                //    RemoveInValidData();
                //    isCloseForm = true;
                //    this.DialogResult = DialogResult.OK;
                //}
            }
        }

        private void frmInputVoucher_ValidBarcode_Load(object sender, EventArgs e)
        {
            objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, grvIMEI);
            objSelection.CheckMarkColumn.Width = 50;

            dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
            grpProduct.Text = "Sản phẩm [" + strProductNameText + "]" + " - Đơn giá [" + decPrice.ToString("#,##0") + "]";
            if (InputVoucherDetailIMEIList == null)
                InputVoucherDetailIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
            grdIMEI.DataSource = InputVoucherDetailIMEIList;
            btnValidate.Enabled = grvIMEI.DataRowCount > 0;
            txtBarcode.Focus();
            lblFirstIMEI.Text = "Số IMEI:";
            txtTotalIMEI.Text = InputVoucherDetailIMEIList.Count().ToString();
            txtTotalIMEIValid.Text = InputVoucherDetailIMEIList.Count().ToString();
            if (bolIsAutoCreateIMEI)
            {
                rbtnInsertIMEI.Checked = false;
                rbtnInsertIMEI.Enabled = false;
                rbtnSearchIMEI.Checked = true;
                //mnuItemImportExcel.Enabled = false;
                //btnValidate.Enabled = false;
            }
            if (IsInputFromRetailInputPrice)//không cho chỉnh sửa chỉ cho check bảo hành 
            {
                txtBarcode.ReadOnly = true;
                rbtnInsertIMEI.Enabled = false;
                rbtnSearchIMEI.Enabled = false;
                for (int i = 0; i < grvIMEI.Columns.Count - 1; i++)
                {
                    if (grvIMEI.Columns[i].FieldName.ToUpper().Trim() != "ISHASWARRANTY")
                        grvIMEI.Columns[i].OptionsColumn.AllowEdit = false;
                }
            }
            if (!bolIsRequirePincode)
            {
                grvIMEI.Columns["PINCode"].Visible = false;
                colStatus.Width = grdIMEI.Width - 70 - colIMEI.Width - colIsHasWarranty.Width;
            }
            else
                colStatus.Width = grdIMEI.Width - 70 - colIMEI.Width - colIsHasWarranty.Width - colPincode.Width;


            lblIsAutoCreateIMEI.Visible = bolIsAutoCreateIMEI;
            GetTotal();
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strIMEI = txtBarcode.Text;
            if (!string.IsNullOrEmpty(strIMEI) && e.KeyChar.Equals((char)Keys.Enter))
            {
                txtBarcode.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtBarcode.Text.Trim().Replace("'", string.Empty).ToUpper());
                if (rbtnInsertIMEI.Checked)
                {
                    InsertIMEI(strIMEI);
                    txtBarcode.Text = string.Empty;
                    txtBarcode.Focus();
                }
                else if (rbtnSearchIMEI.Checked)
                {
                    FindIMEI(strIMEI);
                }
                else if (rbtnEnterListIMEI.Checked)
                {
                    btnAutoGenImei_Click(null, null);
                }
            }
            btnValidate.Enabled = InputVoucherDetailIMEIList.Count > 0;
        }

        /// <summary>
        /// Tìm kiếm IMEI
        /// </summary>
        /// <param name="strIMEI"></param>
        private void FindIMEI(string strIMEI)
        {
            int intIndexFirstExist = -1;
            for (int i = 0; i < InputVoucherDetailIMEIList.Count; i++)
            {
                if (InputVoucherDetailIMEIList[i].IMEI.Trim().Contains(strIMEI))
                {
                    intIndexFirstExist = i;
                    InputVoucherDetailIMEIList[i].IsFind = true;
                    break;
                }
                else if (InputVoucherDetailIMEIList[i].IsFind)
                {
                    InputVoucherDetailIMEIList[i].IsFind = false;
                }
            }
            if (intIndexFirstExist >= 0)
            {
                grdIMEI.Select();
                grvIMEI.FocusedRowHandle = intIndexFirstExist;
                grvIMEI.FocusedColumn = grvIMEI.VisibleColumns[0];
                grvIMEI.ShowEditor();
            }
            else
                MessageBox.Show(this, "Không tìm thấy số IMEI [" + strIMEI + "] trên lưới", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            grdIMEI.RefreshDataSource();
        }

        /// <summary>
        /// Thêm IMEI vào lưới
        /// </summary>
        /// <param name="strIMEI"></param>
        private void InsertIMEI(string strIMEI)
        {
            if (decOrderQuantity > 0)
            {
                var ErrorQuantity = from o in InputVoucherDetailIMEIList
                                    where o.IsError == true
                                    select o;
                decimal decErrorQuantity = ErrorQuantity.Count();
                if (Convert.ToDecimal(InputVoucherDetailIMEIList.Count) + 1 - decErrorQuantity > decOrderQuantity)
                {
                    MessageBox.Show(this, "Số lượng IMEI không được vượt quá số lượng đặt là: " + decOrderQuantity.ToString("#,##0"), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (objInputVoucherDetailList != null && objInputVoucherDetailList.Count > 0)
                {
                    foreach (var item in objInputVoucherDetailList)
                    {
                        if (item.InputVoucherDetailIMEIList != null && item.InputVoucherDetailIMEIList.Length > 0)
                        {
                            var EMEIExist = from o in item.InputVoucherDetailIMEIList
                                            where o.IMEI.ToUpper() == strIMEI.ToUpper()
                                            select o;
                            if (EMEIExist.Count() > 0)
                            {
                                MessageBox.Show(this, "IMEI này đã tồn tại trong phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                }

            }
            if (objAllIMEIList != null && objAllIMEIList.Count > 0)
            {
                if (intCheckDuplicateImeiAllProductValue == 1)
                {
                    var EMEIExist = from o in objAllIMEIList
                                    where o.IMEI == strIMEI
                                    select o;
                    if (EMEIExist.Count() > 0)
                    {
                        MessageBox.Show(this, "IMEI này đã tồn tại trong phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
            string strResult = ValidateWhenInput(strIMEI);
            if (!string.IsNullOrEmpty(strResult))
            {
                MessageBox.Show(this, strResult, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (!string.IsNullOrEmpty(strResult))
            {
                MessageBox.Show(this, strResult, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            PLC.Input.WSInputVoucher.InputVoucherDetailIMEI obj = new PLC.Input.WSInputVoucher.InputVoucherDetailIMEI();
            obj.IsSelect = false;
            obj.ProductName = strProductNameText;
            obj.ProductID = strProductID;
            obj.IMEI = strIMEI;
            obj.IsHasWarranty = true;
            obj.Status = "";
            obj.IsValidate = true;
            obj.IsFind = false;
            obj.IsError = false;
            obj.IsRequirePinCode = bolIsRequirePincode;
            obj.IsHasWarranty = true;
            InputVoucherDetailIMEIList.Add(obj);
            grdIMEI.RefreshDataSource();
            txtTotalIMEI.Text = InputVoucherDetailIMEIList.Count().ToString();
            btnValidate.Enabled = grvIMEI.DataRowCount > 0;
        }



        /// <summary>
        /// Kiểm tra IMEI lúc bắn vào ngành PVI
        /// </summary>
        /// <param name="strIMEI"></param>
        /// <returns></returns>
        private bool CheckPVI(string strIMEI, string strProductID)
        {

            bool strMess = false;
            DataSet dsResult = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("GET_PVIPRODUCT", new string[] { "v_Out1" }, new object[] { "@ProductID", strProductID });
            if (dsResult.Tables[0].Rows.Count != 0)
            {
                strMess = true;
            }


            return strMess;
        }
        /// <summary>
        /// Kiểm tra IMEI lúc bắn vào
        /// </summary>
        /// <param name="strIMEI"></param>
        /// <returns></returns>
        private string ValidateWhenInput(string strIMEI)
        {
            if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI))
            {
                return "Số IMEI không đúng định dạng";
            }
            if (!string.IsNullOrEmpty(strIMEIFormatEXP))
            {
                if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI, strIMEIFormatEXP))
                {
                    return "Số IMEI không đúng định dạng";
                }
            }
            var EMEIExist = from o in InputVoucherDetailIMEIList
                            where o.IMEI == strIMEI
                            select o;
            if (EMEIExist.Count() > 0)
            {
                return "IMEI đã tồn tại trong phiếu nhập";
            }
            string strMess = Library.AppCore.Other.CheckObject.CheckIMEIStandard(strIMEI);

            if (CheckPVI(strIMEI, strProductID) == true)
            {
                if( strIMEI.Length != 11)
                { 
                strMess = "IMEI PVI phải có độ dài 11 ký tự";
                return strMess;
                }
            }
           



            return string.Empty;
        }

        /// <summary>
        /// Nhập trên lưới
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grvIMEI_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (grvIMEI.FocusedRowHandle >= 0)
            {
                if (e.Column.FieldName.ToUpper().Trim() != "PINCODE")
                    return;

                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)grvIMEI.GetRow(grvIMEI.FocusedRowHandle);
                objInputVoucherDetail.IsEdit = true;
                //btnSave.Enabled = false;

                if (objInputVoucherDetail.PINCode == null || objInputVoucherDetail.PINCode.Trim().Length == 0)
                    return;
                objInputVoucherDetail.PINCode = objInputVoucherDetail.PINCode.ToUpper();
                var PincodeExist = from o in InputVoucherDetailIMEIList
                                   where o.PINCode == objInputVoucherDetail.PINCode
                                   select o;
                if (PincodeExist.Count() > 1)
                {
                    MessageBox.Show(this, "Pincode này đã tồn tại. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objInputVoucherDetail.PINCode = string.Empty;
                    return;
                }
                if (!Library.AppCore.Other.CheckObject.CheckIMEI(objInputVoucherDetail.PINCode))
                {
                    MessageBox.Show(this, "Pincode này không đúng định dạng. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objInputVoucherDetail.PINCode = string.Empty;
                    return;
                }
                grdIMEI.RefreshDataSource();
            }
        }

        private void frmInputVoucher_ValidBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.B)
            {
                if (!rbtnInsertIMEI.Enabled)
                    return;
                rbtnInsertIMEI.Checked = true;
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.F)
            {
                if (!rbtnSearchIMEI.Enabled)
                    return;
                rbtnSearchIMEI.Checked = true;
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.S)
            {
                btnValidate_Click(null, null);
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.Delete)
            {
                mnuItemDeleteIMEI_Click(null, null);
            }
            else if (e.KeyCode == Keys.F4)
            {
                this.Close();
            }
            else if (e.KeyCode == Keys.F11)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.WindowState = FormWindowState.Normal;
                this.Width = this.intWidthForm;
                this.Height = this.intHeightForm;
            }
        }

        /// <summary>
        /// Xóa IMEI
        /// </summary>
        private void DeleteIMEI()
        {
            if (objSelection.SelectedCount == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn IMEI cần xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (MessageBox.Show(this, "Bạn có chắc muốn xóa các IMEI được chọn không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            for (int i = 0; i < objSelection.SelectedCount; i++)
            {
                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailIMEI = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)objSelection.GetSelectedRow(i);
                InputVoucherDetailIMEIList.Remove(objInputVoucherDetailIMEI);
            }
            grdIMEI.RefreshDataSource();
            GetTotal();

            //var v_InputVoucherDetail = from o in InputVoucherDetailIMEIList
            //                           where o.IsSelect == true
            //                           select o;
            //if (v_InputVoucherDetail.Count() == 0)
            //{
            //    MessageBox.Show(this, "Vui lòng chọn IMEI cần xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            //if (MessageBox.Show(this, "Bạn có chắc muốn xóa các IMEI được chọn không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            //    return;
            //for (int i = InputVoucherDetailIMEIList.Count - 1; i >= 0; i--)
            //{
            //    PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailIMEI = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)InputVoucherDetailIMEIList[i];
            //    if (objInputVoucherDetailIMEI.IsSelect == true)
            //    {
            //        InputVoucherDetailIMEIList.Remove(objInputVoucherDetailIMEI);
            //    }
            //}
            //grdIMEI.RefreshDataSource();
            //for (int i = 0; i < grvIMEI.RowCount; i++)
            //{
            //    grvIMEI.SetRowCellValue(i, "IsSelect", false);
            //}
            //GetTotal();
            //btnValidate.Enabled = InputVoucherDetailIMEIList.Count > 0;
        }

        private void GetTotal()
        {
            txtTotalIMEI.Text = InputVoucherDetailIMEIList.Count().ToString();
            var vTotalEMEIVaild = from o in InputVoucherDetailIMEIList
                                  where o.IsError == false && o.IsValidate == true
                                  select o;
            txtTotalIMEIValid.Text = vTotalEMEIVaild.Count().ToString();

            var vTotalEMEIInVaild = from o in InputVoucherDetailIMEIList
                                    where o.IsError == true
                                    select o;
            txtTotalIMEIInValid.Text = vTotalEMEIInVaild.Count().ToString();

            intCountFinish = vTotalEMEIVaild.Count();
            objSelection.ClearSelection();
        }
        private void mnuItemDeleteIMEI_Click(object sender, EventArgs e)
        {
            if (!mnuItemDeleteIMEI.Enabled)
            {
                return;
            }
            DeleteIMEI();
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName != string.Empty)
            {
                DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                bool bolIsSelect = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["IsError"]));
                if (bolIsSelect)
                {
                    e.Appearance.BackColor = Color.Pink;
                }
            }
        }

        private void frmInputVoucher_ValidBarcode_FormClosing(object sender, FormClosingEventArgs e)
        {
            var objCheck = from o in objInputVoucherDetailIMEIList
                           select o;
            if (!isCloseForm && objCheck.Count() > 0)
            {
                if (MessageBox.Show("Bạn muốn đóng form nhập IMEI?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    e.Cancel = true;
            }
        }


        private void ImportExcelFile()
        {
            try
            {
                if (InputVoucherDetailIMEIList.Count > 0)
                {
                    if (MessageBox.Show("Toàn bộ IMEI sẽ bị xóa. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                }
                objInputVoucherDetailIMEIListOld = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>(InputVoucherDetailIMEIList);
                InputVoucherDetailIMEIList.Clear();
                grdIMEI.RefreshDataSource();
                GetTotal();
                DataTable dtbExcelData = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(bolIsRequirePincode == true ? 3 : 2);
                if (dtbExcelData == null || dtbExcelData.Rows.Count < 1)
                    return;
                DataRow rowCheck = dtbExcelData.Rows[0];
                if (!bolIsRequirePincode)
                {
                    if (rowCheck[0].ToString().Trim().ToLower() != "imei"
                        || rowCheck[1].ToString().Trim().ToLower().Replace(" ", "") != "bảohành"
                        )
                    {
                        MessageBox.Show("File nhập không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    if (rowCheck[0].ToString().Trim().ToLower() != "imei" || rowCheck[1].ToString().Trim().ToLower().Replace(" ", "") != "pincode"
                        || rowCheck[2].ToString().Trim().ToLower().Replace(" ", "") != "bảohành"
                        )
                    {
                        MessageBox.Show("File nhập không đúng định dạng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                for (int i = 0; i < dtbExcelData.Columns.Count; i++)
                {
                    if (string.IsNullOrEmpty(dtbExcelData.Rows[0][i].ToString()))
                    {
                        MessageBox.Show("File nhập không đúng định dạng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    dtbExcelData.Columns[i].Caption = dtbExcelData.Rows[0][i].ToString();
                }
                if (dtbExcelData.Rows.Count > 0)
                {
                    dtbExcelData.Rows.RemoveAt(0);
                    ImportData(dtbExcelData);
                    grdIMEI.DataSource = InputVoucherDetailIMEIList;
                    grdIMEI.RefreshDataSource();
                    btnValidate.Enabled = grvIMEI.DataRowCount > 0;
                }
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi nhập dữ liệu từ Excel!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi nhập dữ liệu từ Excel", objExce.ToString(), "Excel -> ImportExcelFile()", DUIInventory_Globals.ModuleName);
            }
        }

        /// <summary>
        /// Định dạng bảng dữ liệu sau khi import
        /// </summary>
        /// <param name="dtbImport">DataTable</param>
        /// <returns>DataTable</returns>
        private void ImportData(DataTable dtbExcelData)
        {
            List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> lstInputVoucherDetailIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();

            foreach (DataRow row in dtbExcelData.Rows)
            {
                string strErrorImport = "";
                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI obj = new PLC.Input.WSInputVoucher.InputVoucherDetailIMEI();
                obj.IsSelect = false;
                string strIMEI = row[0].ToString().ToUpper();
                //bool bolIsOldIMEI = false;

                //var IMEIOld = from o in objInputVoucherDetailIMEIListOld
                //              where o.IMEI == strIMEI
                //              select o;
                //bolIsOldIMEI = IMEIOld.Count() > 0;

                //bool bolIsExistsIMEI = false;
                //if (objInputVoucherDetailList != null && objInputVoucherDetailList.Count > 0)
                //{
                //    foreach (var item in objInputVoucherDetailList)
                //    {
                //        if (item.InputVoucherDetailIMEIList != null && item.InputVoucherDetailIMEIList.Length > 0)
                //        {
                //            var IMEIExist = from o in item.InputVoucherDetailIMEIList
                //                            where o.IMEI == strIMEI
                //                            select o;
                //            bolIsExistsIMEI = IMEIExist.Count() > 0;
                //            if (bolIsExistsIMEI)
                //                break;
                //        }
                //    }
                //}
                if (string.IsNullOrEmpty(strIMEI))
                {
                    if (strErrorImport == "")
                        strErrorImport += "IMEI chưa nhập;";
                    obj.IsError = true;
                }
                else if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI))
                {
                    if (strErrorImport == "")
                        strErrorImport += "Số IMEI không đúng định dạng;";
                    obj.IsError = true;
                }
                else if (!string.IsNullOrEmpty(strIMEIFormatEXP))
                {
                    if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI, strIMEIFormatEXP))
                    {
                        if (strErrorImport != "")
                            strErrorImport += "Số IMEI không đúng định dạng;";
                        obj.IsError = true;
                    }
                }
                //else if (bolIsExistsIMEI)// && !bolIsOldIMEI)
                //{
                //    if (strErrorImport == string.Empty)
                //        strErrorImport += "IMEI đã tồn tại trong phiếu nhập;";
                //    obj.IsError = true;
                //}

                obj.IMEI = strIMEI;
                obj.ProductID = ProductID;
                obj.ProductName = strProductNameText;
                obj.IsRequirePinCode = bolIsRequirePincode;
                int intIndex = 1;
                if (bolIsRequirePincode)
                {
                    if (string.IsNullOrEmpty(row[1].ToString()))
                    {
                        if (strErrorImport == "")
                            strErrorImport += "PinCode chưa nhập;";
                        obj.IsError = true;
                    }
                    else
                    {
                        obj.PINCode = row[1].ToString().ToUpper().Trim();
                        if (!Library.AppCore.Other.CheckObject.CheckIMEI(obj.PINCode))
                        {
                            if (strErrorImport == "")
                                strErrorImport += "Số PinCode không đúng định dạng;";
                            obj.IsError = true;
                        }
                    }
                    intIndex = 2;
                }

                intWarranty = -1;
                if (string.IsNullOrEmpty(row[intIndex].ToString()))
                {
                    obj.IsHasWarranty = false;
                }
                else
                {
                    if (Regex.IsMatch(row[intIndex].ToString(), @"^\d$"))
                    {
                        decimal.TryParse(row[intIndex].ToString(), out intWarranty);
                        if (intWarranty < 0)
                        {
                            if (strErrorImport == "")
                                strErrorImport += "Bảo hành không đúng định dạng;";
                        }
                        else
                            if (intWarranty == 1)
                            obj.IsHasWarranty = true;
                        else
                            obj.IsHasWarranty = false;
                    }
                    else
                        strErrorImport += "Bảo hành không đúng định dạng;";
                }

                obj.IsValidate = (obj.IsError ? false : true);
                obj.Status = strErrorImport;
                InputVoucherDetailIMEIList.Add(obj);
            }

            // LE VAN DONG - 14/09/2017
            if (bolIsRequirePincode)
            {
                // Check trùng PINCODE trên lưới
                //var rlsPincode = from item in InputVoucherDetailIMEIList
                //                 group item by item.PINCode into g
                //                 where !string.IsNullOrEmpty(g.Key) && g.Count() > 1
                //                 select g.Key;

                //foreach (var item in rlsPincode)
                //{
                //    bool bolError = false;
                //    var obj = InputVoucherDetailIMEIList.Where(r => r.PINCode == item).FirstOrDefault();
                //    if (obj != null)
                //        bolError = obj.IsError;
                //    InputVoucherDetailIMEIList.Where(r => r.PINCode == item).All(r => { r.IsError = true; r.Status += "Trùng PinCode trên file Excel;"; return true; });
                //    if (obj != null && !bolError)
                //        obj.IsError = false;
                //    if (obj != null)
                //        obj.Status = obj.Status.Replace("Trùng PinCode trên file Excel;", "");
                //}

                Dictionary<string, List<int>> dicCheckPINCode = new Dictionary<string, List<int>>();
                for (int i = 0; i < InputVoucherDetailIMEIList.Count; i++)
                {
                    if (dicCheckPINCode.ContainsKey(InputVoucherDetailIMEIList[i].PINCode))
                    {
                        dicCheckPINCode[InputVoucherDetailIMEIList[i].PINCode].Add(i);
                    }
                    else
                    {
                        List<int> lstIndex = new List<int>();
                        lstIndex.Add(i);
                        dicCheckPINCode.Add(InputVoucherDetailIMEIList[i].PINCode, lstIndex);
                    }
                }

                foreach (string strPINCode in dicCheckPINCode.Keys)
                {
                    if (dicCheckPINCode[strPINCode].Count() > 1)
                    {
                        for (int i = 1; i < dicCheckPINCode[strPINCode].Count(); i++)
                        {
                            InputVoucherDetailIMEIList[dicCheckPINCode[strPINCode][i]].IsError = true;
                            InputVoucherDetailIMEIList[dicCheckPINCode[strPINCode][i]].IsValidate = false;
                            InputVoucherDetailIMEIList[dicCheckPINCode[strPINCode][i]].Status += "Trùng PinCode trên file Excel;";
                        }
                    }
                }
            }


            #region Check trùng IMEI trên lưới
            // 1. Check Trùng IMEI trên lưới
            //var rls = from item in objInputVoucherDetailIMEIList
            //          group item by item.IMEI into g
            //          where !string.IsNullOrEmpty(g.Key) && g.Count() > 1
            //          select g.Key;

            Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
            for (int i = 0; i < objInputVoucherDetailIMEIList.Count; i++)
            {
                if (dicCheckIMEI.ContainsKey(objInputVoucherDetailIMEIList[i].IMEI))
                {
                    dicCheckIMEI[objInputVoucherDetailIMEIList[i].IMEI].Add(i);
                }
                else
                {
                    List<int> lstIndex = new List<int>();
                    lstIndex.Add(i);
                    dicCheckIMEI.Add(objInputVoucherDetailIMEIList[i].IMEI, lstIndex);
                }
            }

            foreach (string strIMEI in dicCheckIMEI.Keys)
            {
                if (dicCheckIMEI[strIMEI].Count() > 1)
                {
                    for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                    {
                        objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].IsError = true;
                        objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].IsValidate = false;
                        objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].Status = objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].Status.Replace("Trùng IMEI trên lưới;", "") + "Trùng IMEI trên lưới;";
                    }
                }
            }

            //foreach (var item in rls)
            //{
            //    bool bolError = false;
            //    var obj = objInputVoucherDetailIMEIList.Where(r => r.IMEI == item).FirstOrDefault();
            //    //if (obj != null)
            //        bolError = obj.IsError;
            //    var rlsImeiDup = objInputVoucherDetailIMEIList.Where(r => r.IMEI == item);
            //    rlsImeiDup.All(r => { r.IsError = true; r.IsValidate = false; r.Status = r.Status.Replace("Trùng IMEI trên lưới;", "") + "Trùng IMEI trên lưới;"; return true; });
            //    if (!bolError)
            //    {
            //        obj.IsError = false;
            //        obj.IsValidate = true;
            //    }
            //    //if (obj != null)
            //        obj.Status = obj.Status.Replace("Trùng IMEI trên lưới;", "");
            //}

            var rlsImei = from r in objInputVoucherDetailIMEIList
                          join x in objAllIMEIList
                          on r.IMEI.Trim() equals x.IMEI.Trim()
                          select r;
            rlsImei.All(r => { r.IsError = true; r.IsValidate = false; r.Status = r.Status.Replace("IMEI đã tồn tại trong phiếu nhập;", "") + "IMEI đã tồn tại trong phiếu nhập;"; return true; });
            #endregion
            // >LVD
            GetTotal();
        }

        private void chksChon_CheckedChanged(object sender, EventArgs e)
        {
            if (grvIMEI.FocusedRowHandle < 0)
                return;
            DevExpress.XtraEditors.CheckEdit chkIsSelect = (DevExpress.XtraEditors.CheckEdit)sender;
            PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)grvIMEI.GetRow(grvIMEI.FocusedRowHandle);
            objInputVoucherDetail.IsSelect = chkIsSelect.Checked;
            grdIMEI.RefreshDataSource();
        }

        private void rbtnEnterListIMEI_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnEnterListIMEI.Checked)
            {
                lblFirstIMEI.Text = "Số IMEI đầu:";
                lblLastIMEI.Visible = true;
                txtLastIMEI.Visible = true;
                btnAutoGenImei.Visible = true;
            }
            else
            {
                lblFirstIMEI.Text = "Số IMEI:";
                lblLastIMEI.Visible = false;
                txtLastIMEI.Visible = false;
                btnAutoGenImei.Visible = false;
            }
        }


        private void Excute2()
        {
            frmWaitDialog.Show(string.Empty, "Đang tạo dãy IMEI tự động");
        }

        /// <summary>
        /// Tạo dãy Imei 
        /// </summary>
        /// <param name="intFisrtIMEI"></param>
        /// <param name="intLastIMEI"></param>
        private void insertListIMEI(decimal intFisrtIMEI, decimal intLastIMEI)
        {
            //btnSave.Enabled = false;
            string strIMEI_i = string.Empty;
            int intCountCreated = 0;
            DataTable dtbOld = ToDataTable(InputVoucherDetailIMEIList);

            for (decimal i = intFisrtIMEI; i <= intLastIMEI; i++)
            {
                string strErrorResult = "";
                strIMEI_i = i.ToString().PadLeft(txtBarcode.Text.Trim().Length, '0');
                if (decOrderQuantity > 0 && intCountFinish >= decOrderQuantity)
                {
                    MessageBox.Show(this, "Số lượng IMEI không được vượt quá số lượng đặt là: " + decOrderQuantity.ToString("#,##0"), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                }
                //if (intCheckDuplicateImeiAllProductValue == 1 && objAllIMEIList != null && objAllIMEIList.Count > 0)
                //{
                //    var EMEIExist = from o in objAllIMEIList
                //                    where o.IMEI.ToUpper() == strIMEI_i.ToUpper()
                //                    select o;
                //    if (EMEIExist.Count() > 0)
                //    {
                //        strErrorResult = "IMEI đã tồn tại trong phiếu nhập;";
                //        continue;
                //    }
                //}

                //if (dtbOld != null && dtbOld.Rows.Count > 0)
                //{
                //    DataRow[] row = dtbOld.Select("IMEI = '" + strIMEI_i + "'");
                //    if (row.Length > 0)
                //        continue;
                //}

                if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI_i))
                {
                    strErrorResult += "Số IMEI không đúng định dạng;";
                }
                if (!string.IsNullOrEmpty(strIMEIFormatEXP))
                {
                    if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI_i, strIMEIFormatEXP))
                    {
                        strErrorResult += "Số IMEI không đúng định dạng;";
                    }
                }

                if (bolIsRequirePincode)
                    strErrorResult += "PinCode chưa nhập;";

                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI obj = new PLC.Input.WSInputVoucher.InputVoucherDetailIMEI();
                obj.IsSelect = false;
                obj.ProductName = strProductNameText;
                obj.ProductID = strProductID;
                obj.IMEI = strIMEI_i;
                obj.IsHasWarranty = false;
                obj.Status = strErrorResult;
                obj.IsFind = false;
                obj.IsError = !string.IsNullOrEmpty(strErrorResult);
                obj.IsValidate = !obj.IsError;
                obj.IsRequirePinCode = bolIsRequirePincode;
                obj.IsHasWarranty = true;
                InputVoucherDetailIMEIList.Add(obj);
                intCountCreated += 1;
                if (!obj.IsError)
                    intCountFinish += 1;
            }

            #region Check trùng IMEI trên lưới
            // 1. Check Trùng IMEI trên lưới
            //var rls = from item in objInputVoucherDetailIMEIList
            //          group item by item.IMEI into g
            //          where !string.IsNullOrEmpty(g.Key) && g.Count() > 1
            //          select g.Key;

            //foreach (var item in rls)
            //{
            //    bool bolError = false;
            //    var obj = objInputVoucherDetailIMEIList.Where(r => r.IMEI == item).FirstOrDefault();
            //    if (obj != null)
            //        bolError = obj.IsError;
            //    var rlsImeiDup = objInputVoucherDetailIMEIList.Where(r => r.IMEI == item);
            //    rlsImeiDup.All(r => { r.IsError = true; r.IsValidate = false; r.Status = r.Status.Replace("Trùng IMEI trên lưới;", "") + "Trùng IMEI trên lưới;"; return true; });
            //    if (obj != null && !bolError)
            //    {
            //        obj.IsError = false;
            //        obj.IsValidate = true;
            //    }
            //    if (obj != null)
            //        obj.Status = obj.Status.Replace("Trùng IMEI trên lưới;", "");
            //}

            Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
            for (int i = 0; i < objInputVoucherDetailIMEIList.Count; i++)
            {
                if (dicCheckIMEI.ContainsKey(objInputVoucherDetailIMEIList[i].IMEI))
                {
                    dicCheckIMEI[objInputVoucherDetailIMEIList[i].IMEI].Add(i);
                }
                else
                {
                    List<int> lstIndex = new List<int>();
                    lstIndex.Add(i);
                    dicCheckIMEI.Add(objInputVoucherDetailIMEIList[i].IMEI, lstIndex);
                }
            }

            foreach (string strIMEI in dicCheckIMEI.Keys)
            {
                if (dicCheckIMEI[strIMEI].Count() > 1)
                {
                    for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                    {
                        objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].IsError = true;
                        objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].IsValidate = false;
                        objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].Status = objInputVoucherDetailIMEIList[dicCheckIMEI[strIMEI][i]].Status.Replace("Trùng IMEI trên lưới;", "") + "Trùng IMEI trên lưới;";
                    }
                }
            }

            var rlsImei = from r in objInputVoucherDetailIMEIList
                          join x in objAllIMEIList
                          on r.IMEI.Trim() equals x.IMEI.Trim()
                          select r;
            rlsImei.All(r => { r.IsError = true; r.IsValidate = false; r.Status = r.Status.Replace("IMEI đã tồn tại trong phiếu nhập;", "") + "IMEI đã tồn tại trong phiếu nhập;"; return true; });
            #endregion

            grdIMEI.RefreshDataSource();
            btnValidate.Enabled = grvIMEI.DataRowCount > 0;
            GetTotal();

            if (intCountCreated > 0)
                MessageBox.Show(this, "Tạo thành công " + intCountCreated.ToString() + "/" + (intLastIMEI - intFisrtIMEI + 1).ToString() + " IMEI!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void mnuPopUp_Opening(object sender, CancelEventArgs e)
        {
            mnuItemDeleteIMEI.Enabled = grvIMEI.DataRowCount > 0;
        }

        private void btnAutoGenImei_Click(object sender, EventArgs e)
        {
            string strFirstIMEI = txtBarcode.Text.Trim();
            string strlastIMEI = txtLastIMEI.Text.Trim();
            if (string.IsNullOrEmpty(strFirstIMEI))
            {
                MessageBox.Show(this, "Vui lòng nhập số IMEI đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBarcode.Focus();
            }
            else if (string.IsNullOrEmpty(strlastIMEI))
            {
                MessageBox.Show(this, "Vui lòng nhập số IMEI cuối!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtLastIMEI.Focus();
            }
            else
            {
                decimal decFisrtIMEI = 0;
                decimal decLastIMEI = 0;
                if (decimal.TryParse(strFirstIMEI, out decFisrtIMEI) && decimal.TryParse(strlastIMEI, out decLastIMEI))
                {
                    if (txtBarcode.Text.Trim() == txtLastIMEI.Text.Trim())
                    {
                        InsertIMEI(txtBarcode.Text.Trim());
                        return;
                    }
                    else if (decLastIMEI - decFisrtIMEI + 1 > 100000) // Giới hạn 100.000 dòng /lần
                    {
                        MessageBox.Show(this, "Chỉ được phép tạo tự động 100.000 IMEI/lần!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else if (decLastIMEI - decFisrtIMEI < 0)
                    {
                        MessageBox.Show(this, "Số IMEI cuối phải lớn hơn số IMEI đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    insertListIMEI(decFisrtIMEI, decLastIMEI);
                }
                else
                {
                    MessageBox.Show(this, "Số IMEI chỉ bao gồm kí tự số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            btnValidate.Enabled = InputVoucherDetailIMEIList.Count > 0;
            btnValidate.Focus();
        }

        private void txtLastIMEI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (rbtnEnterListIMEI.Checked && e.KeyChar.Equals((char)Keys.Enter))
            {
                btnAutoGenImei_Click(null, null);
            }
        }
        private DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        private void btnExportExcelTemplate_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.GridControl grdTemp = new DevExpress.XtraGrid.GridControl();
            DevExpress.XtraGrid.Views.Grid.GridView grvTemp = new DevExpress.XtraGrid.Views.Grid.GridView();
            grdTemp.MainView = grvTemp;
            if (!bolIsRequirePincode)
            {
                string[] fieldNames = new string[] { "IMEI", "Bảo hành" };
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdTemp, fieldNames);
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdTemp);
            }
            else
            {
                string[] fieldNames = new string[] { "IMEI", "PinCode", "Bảo hành" };
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdTemp, fieldNames);
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdTemp);
            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            ImportExcelFile();
            btnValidate.Focus();
        }


        private void frmInputVoucher_ValidBarcode_Resize(object sender, EventArgs e)
        {
            if (!bolIsRequirePincode)
            {
                colStatus.Width = grdIMEI.Width - 70 - colIMEI.Width - colIsHasWarranty.Width;
            }
            else
                colStatus.Width = grdIMEI.Width - 70 - colIMEI.Width - colIsHasWarranty.Width - colPincode.Width;
        }

        private void xuatExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdIMEI);
        }
    }

}
