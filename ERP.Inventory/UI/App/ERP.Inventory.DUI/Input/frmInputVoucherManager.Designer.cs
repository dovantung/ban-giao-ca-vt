﻿namespace ERP.Inventory.DUI.Input
{
    partial class frmInputVoucherManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInputVoucherManager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cboDataReport = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label4 = new System.Windows.Forms.Label();
            this.cboBranch = new Library.AppControl.ComboBoxControl.ComboBoxBranch();
            this.label2 = new System.Windows.Forms.Label();
            this.chkIsDetail = new System.Windows.Forms.CheckBox();
            this.cboMainGroup = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.cboInputType = new Library.AppControl.ComboBoxControl.ComboBoxInputType();
            this.cboCustomer = new Library.AppControl.ComboBoxControl.ComboBoxCustomer();
            this.chkIsDeleted = new System.Windows.Forms.CheckBox();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.label14 = new System.Windows.Forms.Label();
            this.cboSearchType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dtInputEnd = new DevExpress.XtraEditors.DateEdit();
            this.dtInputStart = new DevExpress.XtraEditors.DateEdit();
            this.txtTop = new DevExpress.XtraEditors.TextEdit();
            this.txtSearch = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.mnuMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuViewInputVoucher = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPrintInputVoucher = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemViewVoucher = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemReturnOutput = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemOutputChange = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuInputVoicherInKim = new System.Windows.Forms.ToolStripMenuItem();
            this.grvData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMERADDRESS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRODUCTID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRODUCTNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOTALAMOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDISCOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROTECTPRICEDISCOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOTALVND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCURRENCYEXCHANGE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDISCOUNTREASONNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemchkIsCheckRealInput = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemCheckRealInputNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColDeletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColDeletedUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColContentDeleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputStart.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.mnuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemchkIsCheckRealInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemCheckRealInputNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.Caption = "Đã Hủy";
            this.gridColumn23.FieldName = "ISDELETED";
            this.gridColumn23.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn23.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseImage = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.Controls.Add(this.cboDataReport);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.cboBranch);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.chkIsDetail);
            this.groupControl1.Controls.Add(this.cboMainGroup);
            this.groupControl1.Controls.Add(this.cboStore);
            this.groupControl1.Controls.Add(this.cboInputType);
            this.groupControl1.Controls.Add(this.cboCustomer);
            this.groupControl1.Controls.Add(this.chkIsDeleted);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.label14);
            this.groupControl1.Controls.Add(this.cboSearchType);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.label12);
            this.groupControl1.Controls.Add(this.dtInputEnd);
            this.groupControl1.Controls.Add(this.dtInputStart);
            this.groupControl1.Controls.Add(this.txtTop);
            this.groupControl1.Controls.Add(this.txtSearch);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.label11);
            this.groupControl1.Controls.Add(this.label13);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1007, 137);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            this.groupControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl1_Paint);
            // 
            // cboDataReport
            // 
            this.cboDataReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDataReport.Location = new System.Drawing.Point(101, 81);
            this.cboDataReport.Margin = new System.Windows.Forms.Padding(0);
            this.cboDataReport.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboDataReport.Name = "cboDataReport";
            this.cboDataReport.Size = new System.Drawing.Size(233, 24);
            this.cboDataReport.TabIndex = 17;
            this.cboDataReport.SelectionChangeCommitted += new System.EventHandler(this.cboDataReport_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 85);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "Nhóm dữ liệu:";
            // 
            // cboBranch
            // 
            this.cboBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBranch.Location = new System.Drawing.Point(101, 29);
            this.cboBranch.Margin = new System.Windows.Forms.Padding(0);
            this.cboBranch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(233, 24);
            this.cboBranch.TabIndex = 1;
            this.cboBranch.SelectionChangeCommitted += new System.EventHandler(this.cboBranch_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Chi nhánh:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkIsDetail
            // 
            this.chkIsDetail.AutoSize = true;
            this.chkIsDetail.Location = new System.Drawing.Point(812, 108);
            this.chkIsDetail.Name = "chkIsDetail";
            this.chkIsDetail.Size = new System.Drawing.Size(94, 20);
            this.chkIsDetail.TabIndex = 23;
            this.chkIsDetail.Text = "Xem chi tiết";
            this.chkIsDetail.UseVisualStyleBackColor = true;
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroup.Location = new System.Drawing.Point(731, 81);
            this.cboMainGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(261, 24);
            this.cboMainGroup.TabIndex = 21;
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(416, 29);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(225, 24);
            this.cboStore.TabIndex = 3;
            // 
            // cboInputType
            // 
            this.cboInputType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputType.Location = new System.Drawing.Point(416, 81);
            this.cboInputType.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputType.Name = "cboInputType";
            this.cboInputType.Size = new System.Drawing.Size(225, 24);
            this.cboInputType.TabIndex = 19;
            // 
            // cboCustomer
            // 
            this.cboCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomer.IsActivatedType = Library.AppCore.Constant.EnumType.IsActivatedType.ACTIVATED;
            this.cboCustomer.IsRequireActive = false;
            this.cboCustomer.Location = new System.Drawing.Point(731, 29);
            this.cboCustomer.Margin = new System.Windows.Forms.Padding(0);
            this.cboCustomer.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(261, 24);
            this.cboCustomer.TabIndex = 5;
            // 
            // chkIsDeleted
            // 
            this.chkIsDeleted.AutoSize = true;
            this.chkIsDeleted.Location = new System.Drawing.Point(648, 108);
            this.chkIsDeleted.Name = "chkIsDeleted";
            this.chkIsDeleted.Size = new System.Drawing.Size(161, 20);
            this.chkIsDeleted.TabIndex = 22;
            this.chkIsDeleted.Text = "Bao gồm phiếu đã hủy";
            this.chkIsDeleted.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Appearance.Options.UseFont = true;
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(909, 105);
            this.btnSearch.LookAndFeel.SkinName = "Blue";
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(83, 25);
            this.btnSearch.TabIndex = 24;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 16);
            this.label14.TabIndex = 15;
            this.label14.Text = "Top:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Visible = false;
            // 
            // cboSearchType
            // 
            this.cboSearchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchType.DropDownWidth = 250;
            this.cboSearchType.Items.AddRange(new object[] {
            "Mã phiếu",
            "Số hóa đơn",
            "Mã đơn hàng",
            "Mã nhân viên",
            "Tên nhân viên",
            "Mã sản phẩm",
            "Số IMEI"});
            this.cboSearchType.Location = new System.Drawing.Point(731, 55);
            this.cboSearchType.Name = "cboSearchType";
            this.cboSearchType.Size = new System.Drawing.Size(261, 24);
            this.cboSearchType.TabIndex = 15;
            this.cboSearchType.SelectedIndexChanged += new System.EventHandler(this.cboSearchType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(644, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "Tìm theo:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(644, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 16);
            this.label12.TabIndex = 20;
            this.label12.Text = "Ngành hàng:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtInputEnd
            // 
            this.dtInputEnd.EditValue = null;
            this.dtInputEnd.Location = new System.Drawing.Point(218, 57);
            this.dtInputEnd.Name = "dtInputEnd";
            this.dtInputEnd.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInputEnd.Properties.Appearance.Options.UseFont = true;
            this.dtInputEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtInputEnd.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtInputEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtInputEnd.Properties.LookAndFeel.SkinName = "Blue";
            this.dtInputEnd.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dtInputEnd.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtInputEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtInputEnd.Size = new System.Drawing.Size(116, 22);
            this.dtInputEnd.TabIndex = 10;
            // 
            // dtInputStart
            // 
            this.dtInputStart.EditValue = null;
            this.dtInputStart.Location = new System.Drawing.Point(101, 57);
            this.dtInputStart.Name = "dtInputStart";
            this.dtInputStart.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInputStart.Properties.Appearance.Options.UseFont = true;
            this.dtInputStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtInputStart.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtInputStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtInputStart.Properties.LookAndFeel.SkinName = "Blue";
            this.dtInputStart.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dtInputStart.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtInputStart.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtInputStart.Size = new System.Drawing.Size(116, 22);
            this.dtInputStart.TabIndex = 9;
            // 
            // txtTop
            // 
            this.txtTop.EditValue = "50";
            this.txtTop.Location = new System.Drawing.Point(87, 109);
            this.txtTop.Name = "txtTop";
            this.txtTop.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtTop.Properties.Appearance.Options.UseFont = true;
            this.txtTop.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTop.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTop.Properties.LookAndFeel.SkinName = "Blue";
            this.txtTop.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtTop.Properties.Mask.EditMask = "#####";
            this.txtTop.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTop.Properties.MaxLength = 4;
            this.txtTop.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTop.Size = new System.Drawing.Size(70, 22);
            this.txtTop.TabIndex = 16;
            this.txtTop.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(416, 57);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtSearch.Properties.Appearance.Options.UseFont = true;
            this.txtSearch.Properties.LookAndFeel.SkinName = "Blue";
            this.txtSearch.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtSearch.Properties.MaxLength = 20;
            this.txtSearch.Size = new System.Drawing.Size(225, 22);
            this.txtSearch.TabIndex = 12;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(340, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 11;
            this.label9.Text = "Chuỗi tìm:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Ngày nhập:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(340, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 16);
            this.label10.TabIndex = 18;
            this.label10.Text = "Loại phiếu:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(340, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "Kho nhập:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(644, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 16);
            this.label13.TabIndex = 4;
            this.label13.Text = "Khách hàng:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grdData
            // 
            this.grdData.ContextMenuStrip = this.mnuMain;
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(0, 137);
            this.grdData.MainView = this.grvData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.ItemchkIsCheckRealInput,
            this.ItemCheckRealInputNote});
            this.grdData.Size = new System.Drawing.Size(1007, 355);
            this.grdData.TabIndex = 1;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvData});
            this.grdData.DoubleClick += new System.EventHandler(this.grdData_DoubleClick);
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuViewInputVoucher,
            this.mnuPrintInputVoucher,
            this.mnuItemViewVoucher,
            this.mnuItemReturnOutput,
            this.mnuItemOutputChange,
            this.mnuInputVoicherInKim});
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(237, 136);
            this.mnuMain.Opening += new System.ComponentModel.CancelEventHandler(this.mnuMain_Opening);
            // 
            // mnuViewInputVoucher
            // 
            this.mnuViewInputVoucher.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuViewInputVoucher.Name = "mnuViewInputVoucher";
            this.mnuViewInputVoucher.Size = new System.Drawing.Size(236, 22);
            this.mnuViewInputVoucher.Text = "Xem phiếu nhập";
            this.mnuViewInputVoucher.Click += new System.EventHandler(this.mnuViewInputVoucher_Click);
            // 
            // mnuPrintInputVoucher
            // 
            this.mnuPrintInputVoucher.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.mnuPrintInputVoucher.Name = "mnuPrintInputVoucher";
            this.mnuPrintInputVoucher.Size = new System.Drawing.Size(236, 22);
            this.mnuPrintInputVoucher.Text = "In phiếu nhập";
            this.mnuPrintInputVoucher.Click += new System.EventHandler(this.mnuPrintInputVoucher_Click);
            // 
            // mnuItemViewVoucher
            // 
            this.mnuItemViewVoucher.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuItemViewVoucher.Name = "mnuItemViewVoucher";
            this.mnuItemViewVoucher.Size = new System.Drawing.Size(236, 22);
            this.mnuItemViewVoucher.Text = "Xem phiếu chi";
            this.mnuItemViewVoucher.Click += new System.EventHandler(this.mnuItemViewVoucher_Click);
            // 
            // mnuItemReturnOutput
            // 
            this.mnuItemReturnOutput.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuItemReturnOutput.Name = "mnuItemReturnOutput";
            this.mnuItemReturnOutput.Size = new System.Drawing.Size(236, 22);
            this.mnuItemReturnOutput.Text = "Xem phiếu xuất đổi";
            this.mnuItemReturnOutput.Click += new System.EventHandler(this.mnuItemReturnOutput_Click);
            // 
            // mnuItemOutputChange
            // 
            this.mnuItemOutputChange.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuItemOutputChange.Name = "mnuItemOutputChange";
            this.mnuItemOutputChange.Size = new System.Drawing.Size(236, 22);
            this.mnuItemOutputChange.Text = "Xem phiếu xuất đổi tại siêu thị ";
            this.mnuItemOutputChange.Click += new System.EventHandler(this.mnuItemOutputChange_Click);
            // 
            // mnuInputVoicherInKim
            // 
            this.mnuInputVoicherInKim.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.mnuInputVoicherInKim.Name = "mnuInputVoicherInKim";
            this.mnuInputVoicherInKim.Size = new System.Drawing.Size(236, 22);
            this.mnuInputVoicherInKim.Text = "In phiếu nhập kho 3 liên";
            this.mnuInputVoicherInKim.Click += new System.EventHandler(this.mnuInputVoucherInKim_Click);
            // 
            // grvData
            // 
            this.grvData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.FocusedRow.Options.UseFont = true;
            this.grvData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.FooterPanel.Options.UseFont = true;
            this.grvData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grvData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.Preview.Options.UseFont = true;
            this.grvData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.Row.Options.UseFont = true;
            this.grvData.ColumnPanelRowHeight = 30;
            this.grvData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn12,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn30,
            this.colCUSTOMERADDRESS,
            this.colPRODUCTID,
            this.colPRODUCTNAME,
            this.colIMEI,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.colQuantity,
            this.colTOTALAMOUNT,
            this.colDISCOUNT,
            this.colPROTECTPRICEDISCOUNT,
            this.colTOTALVND,
            this.gridColumn16,
            this.colCURRENCYEXCHANGE,
            this.colDISCOUNTREASONNAME,
            this.gridColumn19,
            this.gridColumn20,
            this.colNote,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColDeletedDate,
            this.gridColDeletedUser,
            this.gridColContentDeleted});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn23;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[ISDELETED]>0";
            styleFormatCondition1.Value1 = true;
            this.grvData.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.grvData.GridControl = this.grdData;
            this.grvData.Name = "grvData";
            this.grvData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvData.OptionsView.AllowCellMerge = true;
            this.grvData.OptionsView.ColumnAutoWidth = false;
            this.grvData.OptionsView.ShowAutoFilterRow = true;
            this.grvData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvData.OptionsView.ShowFooter = true;
            this.grvData.OptionsView.ShowGroupPanel = false;
            this.grvData.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvData_ShowingEditor);
            this.grvData.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvData_CellValueChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Mã phiếu nhập";
            this.gridColumn1.FieldName = "INPUTVOUCHERID";
            this.gridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "INPUTVOUCHERID", "Tổng cộng:")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 125;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "Mã đơn hàng";
            this.gridColumn2.FieldName = "ORDERID";
            this.gridColumn2.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ORDERID", "{0:#,##0}", "0")});
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 125;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn12.Caption = "Số hiệu đơn hàng";
            this.gridColumn12.FieldName = "CUSTOMERORDERID";
            this.gridColumn12.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 123;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Số hóa đơn";
            this.gridColumn3.FieldName = "INVOICEID";
            this.gridColumn3.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 90;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Ký hiệu HĐ";
            this.gridColumn4.FieldName = "INVOICESYMBOL";
            this.gridColumn4.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            this.gridColumn4.Width = 90;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Ngày HĐ";
            this.gridColumn5.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn5.FieldName = "INVOICEDATE";
            this.gridColumn5.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            this.gridColumn5.Width = 90;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "Kho nhập";
            this.gridColumn6.FieldName = "STORENAME";
            this.gridColumn6.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            this.gridColumn6.Width = 200;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "Khách hàng";
            this.gridColumn7.FieldName = "CUSTOMERNAME";
            this.gridColumn7.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            this.gridColumn7.Width = 158;
            // 
            // gridColumn30
            // 
            this.gridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn30.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn30.Caption = "Số ĐT khách hàng";
            this.gridColumn30.FieldName = "CUSTOMERPHONE";
            this.gridColumn30.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn30.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 8;
            this.gridColumn30.Width = 120;
            // 
            // colCUSTOMERADDRESS
            // 
            this.colCUSTOMERADDRESS.AppearanceHeader.Options.UseTextOptions = true;
            this.colCUSTOMERADDRESS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCUSTOMERADDRESS.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCUSTOMERADDRESS.Caption = "Địa chỉ khách hàng";
            this.colCUSTOMERADDRESS.FieldName = "CUSTOMERADDRESS";
            this.colCUSTOMERADDRESS.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCUSTOMERADDRESS.Name = "colCUSTOMERADDRESS";
            this.colCUSTOMERADDRESS.OptionsColumn.AllowEdit = false;
            this.colCUSTOMERADDRESS.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMERADDRESS.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCUSTOMERADDRESS.Visible = true;
            this.colCUSTOMERADDRESS.VisibleIndex = 9;
            this.colCUSTOMERADDRESS.Width = 220;
            // 
            // colPRODUCTID
            // 
            this.colPRODUCTID.AppearanceHeader.Options.UseTextOptions = true;
            this.colPRODUCTID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPRODUCTID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPRODUCTID.Caption = "Mã sản phẩm";
            this.colPRODUCTID.FieldName = "PRODUCTID";
            this.colPRODUCTID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPRODUCTID.Name = "colPRODUCTID";
            this.colPRODUCTID.OptionsColumn.AllowEdit = false;
            this.colPRODUCTID.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colPRODUCTID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTID.Width = 100;
            // 
            // colPRODUCTNAME
            // 
            this.colPRODUCTNAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colPRODUCTNAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPRODUCTNAME.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPRODUCTNAME.Caption = "Tên sản phẩm";
            this.colPRODUCTNAME.FieldName = "PRODUCTNAME";
            this.colPRODUCTNAME.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPRODUCTNAME.Name = "colPRODUCTNAME";
            this.colPRODUCTNAME.OptionsColumn.AllowEdit = false;
            this.colPRODUCTNAME.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colPRODUCTNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTNAME.Width = 180;
            // 
            // colIMEI
            // 
            this.colIMEI.AppearanceHeader.Options.UseTextOptions = true;
            this.colIMEI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIMEI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colIMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIMEI.Width = 120;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "Hình thức nhập";
            this.gridColumn8.FieldName = "INPUTTYPENAME";
            this.gridColumn8.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 10;
            this.gridColumn8.Width = 134;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "Người nhập";
            this.gridColumn9.FieldName = "FULLNAME";
            this.gridColumn9.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 11;
            this.gridColumn9.Width = 180;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "Ngày nhập";
            this.gridColumn10.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn10.FieldName = "INPUTDATE";
            this.gridColumn10.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 12;
            this.gridColumn10.Width = 110;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Nội dung nhập";
            this.gridColumn11.FieldName = "CONTENT";
            this.gridColumn11.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 13;
            this.gridColumn11.Width = 122;
            // 
            // colQuantity
            // 
            this.colQuantity.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantity.Caption = "Số lượng";
            this.colQuantity.DisplayFormat.FormatString = "#,##0.####;";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantity.FieldName = "QUANTITY";
            this.colQuantity.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowEdit = false;
            this.colQuantity.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colQuantity.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 14;
            this.colQuantity.Width = 80;
            // 
            // colTOTALAMOUNT
            // 
            this.colTOTALAMOUNT.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOTALAMOUNT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOTALAMOUNT.Caption = "Tổng tiền";
            this.colTOTALAMOUNT.DisplayFormat.FormatString = "n0";
            this.colTOTALAMOUNT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTOTALAMOUNT.FieldName = "TOTALAMOUNT";
            this.colTOTALAMOUNT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTOTALAMOUNT.Name = "colTOTALAMOUNT";
            this.colTOTALAMOUNT.OptionsColumn.AllowEdit = false;
            this.colTOTALAMOUNT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colTOTALAMOUNT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTOTALAMOUNT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNT", "{0:#,##0}", "0")});
            this.colTOTALAMOUNT.Visible = true;
            this.colTOTALAMOUNT.VisibleIndex = 15;
            this.colTOTALAMOUNT.Width = 115;
            // 
            // colDISCOUNT
            // 
            this.colDISCOUNT.AppearanceHeader.Options.UseTextOptions = true;
            this.colDISCOUNT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDISCOUNT.Caption = "Giảm giá";
            this.colDISCOUNT.DisplayFormat.FormatString = "n0";
            this.colDISCOUNT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDISCOUNT.FieldName = "DISCOUNT";
            this.colDISCOUNT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colDISCOUNT.Name = "colDISCOUNT";
            this.colDISCOUNT.OptionsColumn.AllowEdit = false;
            this.colDISCOUNT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colDISCOUNT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDISCOUNT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DISCOUNT", "{0:#,##0}", "0")});
            this.colDISCOUNT.Visible = true;
            this.colDISCOUNT.VisibleIndex = 16;
            this.colDISCOUNT.Width = 103;
            // 
            // colPROTECTPRICEDISCOUNT
            // 
            this.colPROTECTPRICEDISCOUNT.AppearanceHeader.Options.UseTextOptions = true;
            this.colPROTECTPRICEDISCOUNT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPROTECTPRICEDISCOUNT.Caption = "Bảo vệ giá";
            this.colPROTECTPRICEDISCOUNT.DisplayFormat.FormatString = "n0";
            this.colPROTECTPRICEDISCOUNT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPROTECTPRICEDISCOUNT.FieldName = "PROTECTPRICEDISCOUNT";
            this.colPROTECTPRICEDISCOUNT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPROTECTPRICEDISCOUNT.Name = "colPROTECTPRICEDISCOUNT";
            this.colPROTECTPRICEDISCOUNT.OptionsColumn.AllowEdit = false;
            this.colPROTECTPRICEDISCOUNT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colPROTECTPRICEDISCOUNT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPROTECTPRICEDISCOUNT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PROTECTPRICEDISCOUNT", "{0:#,##0}", "0")});
            this.colPROTECTPRICEDISCOUNT.Visible = true;
            this.colPROTECTPRICEDISCOUNT.VisibleIndex = 17;
            this.colPROTECTPRICEDISCOUNT.Width = 88;
            // 
            // colTOTALVND
            // 
            this.colTOTALVND.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOTALVND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOTALVND.Caption = "Tổng tiền VNĐ";
            this.colTOTALVND.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTOTALVND.FieldName = "TOTALVND";
            this.colTOTALVND.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTOTALVND.Name = "colTOTALVND";
            this.colTOTALVND.OptionsColumn.AllowEdit = false;
            this.colTOTALVND.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colTOTALVND.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTOTALVND.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALVND", "{0:#,##0}", "0")});
            this.colTOTALVND.Width = 111;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "Loại tiền";
            this.gridColumn16.FieldName = "CURRENCYUNITNAME";
            this.gridColumn16.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn16.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 18;
            this.gridColumn16.Width = 70;
            // 
            // colCURRENCYEXCHANGE
            // 
            this.colCURRENCYEXCHANGE.AppearanceHeader.Options.UseTextOptions = true;
            this.colCURRENCYEXCHANGE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCURRENCYEXCHANGE.Caption = "Tỷ giá";
            this.colCURRENCYEXCHANGE.DisplayFormat.FormatString = "n2";
            this.colCURRENCYEXCHANGE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCURRENCYEXCHANGE.FieldName = "CURRENCYEXCHANGE";
            this.colCURRENCYEXCHANGE.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCURRENCYEXCHANGE.Name = "colCURRENCYEXCHANGE";
            this.colCURRENCYEXCHANGE.OptionsColumn.AllowEdit = false;
            this.colCURRENCYEXCHANGE.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colCURRENCYEXCHANGE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCURRENCYEXCHANGE.Visible = true;
            this.colCURRENCYEXCHANGE.VisibleIndex = 19;
            this.colCURRENCYEXCHANGE.Width = 80;
            // 
            // colDISCOUNTREASONNAME
            // 
            this.colDISCOUNTREASONNAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colDISCOUNTREASONNAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDISCOUNTREASONNAME.Caption = "Hình thức giảm giá";
            this.colDISCOUNTREASONNAME.FieldName = "DISCOUNTREASONNAME";
            this.colDISCOUNTREASONNAME.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colDISCOUNTREASONNAME.Name = "colDISCOUNTREASONNAME";
            this.colDISCOUNTREASONNAME.OptionsColumn.AllowEdit = false;
            this.colDISCOUNTREASONNAME.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colDISCOUNTREASONNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDISCOUNTREASONNAME.Visible = true;
            this.colDISCOUNTREASONNAME.VisibleIndex = 20;
            this.colDISCOUNTREASONNAME.Width = 135;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "KT thực nhập";
            this.gridColumn19.ColumnEdit = this.ItemchkIsCheckRealInput;
            this.gridColumn19.FieldName = "ISCHECKREALINPUT";
            this.gridColumn19.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn19.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 21;
            this.gridColumn19.Width = 103;
            // 
            // ItemchkIsCheckRealInput
            // 
            this.ItemchkIsCheckRealInput.AutoHeight = false;
            this.ItemchkIsCheckRealInput.Name = "ItemchkIsCheckRealInput";
            this.ItemchkIsCheckRealInput.ValueChecked = ((short)(1));
            this.ItemchkIsCheckRealInput.ValueUnchecked = ((short)(0));
            this.ItemchkIsCheckRealInput.CheckedChanged += new System.EventHandler(this.ItemchkIsCheckRealInput_CheckedChanged);
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "Ghi chú kiểm tra";
            this.gridColumn20.ColumnEdit = this.ItemCheckRealInputNote;
            this.gridColumn20.FieldName = "CHECKREALINPUTNOTE";
            this.gridColumn20.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn20.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 22;
            this.gridColumn20.Width = 150;
            // 
            // ItemCheckRealInputNote
            // 
            this.ItemCheckRealInputNote.AutoHeight = false;
            this.ItemCheckRealInputNote.Name = "ItemCheckRealInputNote";
            // 
            // colNote
            // 
            this.colNote.AppearanceHeader.Options.UseTextOptions = true;
            this.colNote.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNote.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "NOTE";
            this.colNote.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colNote.Name = "colNote";
            this.colNote.OptionsColumn.AllowEdit = false;
            this.colNote.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colNote.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 23;
            this.colNote.Width = 150;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn21.Caption = "Ngày kiểm tra";
            this.gridColumn21.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn21.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn21.FieldName = "CHECKREALINPUTTIME";
            this.gridColumn21.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn21.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 24;
            this.gridColumn21.Width = 110;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn22.Caption = "Người kiểm tra";
            this.gridColumn22.FieldName = "CHECKREALFULLNAME";
            this.gridColumn22.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn22.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 25;
            this.gridColumn22.Width = 128;
            // 
            // gridColDeletedDate
            // 
            this.gridColDeletedDate.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColDeletedDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColDeletedDate.Caption = "Ngày hủy";
            this.gridColDeletedDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColDeletedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColDeletedDate.FieldName = "DELETEDDATE";
            this.gridColDeletedDate.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColDeletedDate.Name = "gridColDeletedDate";
            this.gridColDeletedDate.OptionsColumn.AllowEdit = false;
            this.gridColDeletedDate.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColDeletedDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColDeletedDate.Width = 120;
            // 
            // gridColDeletedUser
            // 
            this.gridColDeletedUser.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColDeletedUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColDeletedUser.Caption = "Người hủy";
            this.gridColDeletedUser.FieldName = "DELETEDUSER";
            this.gridColDeletedUser.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColDeletedUser.Name = "gridColDeletedUser";
            this.gridColDeletedUser.OptionsColumn.AllowEdit = false;
            this.gridColDeletedUser.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColDeletedUser.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColDeletedUser.Width = 220;
            // 
            // gridColContentDeleted
            // 
            this.gridColContentDeleted.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColContentDeleted.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColContentDeleted.Caption = "Lý do hủy";
            this.gridColContentDeleted.FieldName = "CONTENTDELETED";
            this.gridColContentDeleted.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColContentDeleted.Name = "gridColContentDeleted";
            this.gridColContentDeleted.OptionsColumn.AllowEdit = false;
            this.gridColContentDeleted.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColContentDeleted.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColContentDeleted.Width = 220;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.EditFormat.FormatString = "N4";
            this.repositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.Mask.EditMask = "##,###,###,###.##";
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl1.Controls.Add(this.btnUpdate);
            this.panelControl1.Controls.Add(this.btnAddNew);
            this.panelControl1.Controls.Add(this.btnExport);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 492);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1007, 30);
            this.panelControl1.TabIndex = 2;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(690, 3);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 25);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "   Cập &nhật";
            this.btnUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.btnAddNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddNew.Location = new System.Drawing.Point(796, 3);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(100, 25);
            this.btnAddNew.TabIndex = 1;
            this.btnAddNew.Text = "  Thêm &mới";
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(902, 3);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(100, 25);
            this.btnExport.TabIndex = 2;
            this.btnExport.Text = "    &Xuất excel";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // frmInputVoucherManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 522);
            this.Controls.Add(this.grdData);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panelControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(901, 549);
            this.Name = "frmInputVoucherManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý phiếu nhập";
            this.Load += new System.EventHandler(this.frmInputVoucherManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputStart.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.mnuMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemchkIsCheckRealInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemCheckRealInputNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cboSearchType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.DateEdit dtInputEnd;
        private DevExpress.XtraEditors.DateEdit dtInputStart;
        private DevExpress.XtraEditors.TextEdit txtTop;
        private DevExpress.XtraEditors.TextEdit txtSearch;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grvData;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn colTOTALAMOUNT;
        private DevExpress.XtraGrid.Columns.GridColumn colDISCOUNT;
        private DevExpress.XtraGrid.Columns.GridColumn colPROTECTPRICEDISCOUNT;
        private DevExpress.XtraGrid.Columns.GridColumn colTOTALVND;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCYEXCHANGE;
        private DevExpress.XtraGrid.Columns.GridColumn colDISCOUNTREASONNAME;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.Button btnExport;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ItemchkIsCheckRealInput;
        private System.Windows.Forms.ContextMenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem mnuViewInputVoucher;
        private System.Windows.Forms.ToolStripMenuItem mnuPrintInputVoucher;
        private System.Windows.Forms.ToolStripMenuItem mnuItemViewVoucher;
        private System.Windows.Forms.ToolStripMenuItem mnuItemReturnOutput;
        private System.Windows.Forms.ToolStripMenuItem mnuItemOutputChange;
        private System.Windows.Forms.CheckBox chkIsDeleted;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroup;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStore;
        private Library.AppControl.ComboBoxControl.ComboBoxInputType cboInputType;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomer cboCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColDeletedDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColDeletedUser;
        private DevExpress.XtraGrid.Columns.GridColumn gridColContentDeleted;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit ItemCheckRealInputNote;
        private System.Windows.Forms.CheckBox chkIsDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMERADDRESS;
        private DevExpress.XtraGrid.Columns.GridColumn colPRODUCTID;
        private DevExpress.XtraGrid.Columns.GridColumn colPRODUCTNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private System.Windows.Forms.ToolStripMenuItem mnuInputVoicherInKim;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private Library.AppControl.ComboBoxControl.ComboBoxBranch cboBranch;
        private System.Windows.Forms.Label label2;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboDataReport;
        private System.Windows.Forms.Label label4;
    }
}