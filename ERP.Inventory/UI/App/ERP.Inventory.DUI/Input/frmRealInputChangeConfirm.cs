﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Library.AppCore;
using Library.AppCore.Forms;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Input
{
    public partial class frmRealInputChangeConfirm : Form
    {
        private DataTable dtbData;
        List<ProductRealInput> lstProduct = new List<ProductRealInput>();
        int intCheckDuplicateImeiAllProductValue = Library.AppCore.AppConfig.GetIntConfigValue("ISCHECKDUPLICATEIMEIALLPRODUCT");
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objInputVoucherDetailIMEIListOld = null;
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = null;
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIList = null;
        private bool bolIsInputFromRetailInputPrice = false;
        //Nguyễn Văn Tài - chỉnh sửa bổ sung
        private DataTable dtbError = null;
        private Regex regex = new Regex(@"^\d$");
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> AllIMEIList
        {
            get { return objAllIMEIList; }
            set { objAllIMEIList = value; }
        }

        public string Note { get; set; }
        /// <summary>
        /// Nếu nhập từ định giá máy cũ
        /// </summary>
        public bool IsInputFromRetailInputPrice
        {
            get { return bolIsInputFromRetailInputPrice; }
            set { bolIsInputFromRetailInputPrice = value; }
        }
        /// <summary>
        /// Danh sách IMEI nhập
        /// </summary>
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> InputVoucherDetailIMEIList
        {
            get { return objInputVoucherDetailIMEIList; }
            set { objInputVoucherDetailIMEIList = value; }
        }
        private List<PLC.Input.WSInputVoucher.InputVoucherDetail> objInputVoucherDetailList = null;
        public List<PLC.Input.WSInputVoucher.InputVoucherDetail> InputVoucherDetailList
        {
            set { objInputVoucherDetailList = value; }
            get { return objInputVoucherDetailList; }
        }

        public decimal OrderQuantity { get; set; }

        public frmRealInputChangeConfirm()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }

        public frmRealInputChangeConfirm(List<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail> dtbData)
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            this.dtbData = ConvertToDataTable(dtbData);
            this.dtbData.Columns.Add("IsError", typeof(Boolean));
            this.dtbData.Columns.Add("RealQuantity", typeof(Decimal));
            this.dtbData.AsEnumerable().All(c => { c["RealQuantity"] = 0; return true; });
            grdData.DataSource = this.dtbData;
        }

        private void UpdateQuantity(DataTable dtbData)
        {
            for (int i = 0; i < dtbData.Rows.Count; i++)
            {
                dtbData.Rows[i]["QuantityToInput"] = int.Parse(dtbData.Rows[i]["QuantityToInput"].ToString()) + int.Parse(dtbData.Rows[i]["Quantity"].ToString());
                grdViewData.RefreshData();
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(gridControl1, DevExpress.XtraPrinting.TextExportMode.Text, false);
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Không thể xuất file excel mẫu!", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Không thể xuất file excel mẫu!");
                return;
            }
        }

        public DataTable ConvertListToDatatable(List<string> lstData)
        {
            DataTable dtbData = new DataTable();
            DataColumn col = new DataColumn();
            dtbData.Columns.Add(col);
            foreach (var item in lstData)
            {
                dtbData.Rows.Add(item);
            }
            return dtbData;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
        private DataTable ConvertColumnsAsRows(DataTable dt)
        {
            DataTable dtnew = new DataTable();
            //Convert all the rows to columns 
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtnew.Columns.Add(Convert.ToString(i));
            }
            DataRow dr;
            // Convert All the Columns to Rows 
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dr = dtnew.NewRow();
                dr[0] = dt.Columns[j].ToString();
                for (int k = 1; k <= dt.Rows.Count; k++)
                    dr[k] = dt.Rows[k - 1][j];
                dtnew.Rows.Add(dr);
            }
            return dtnew;
        }


        //Nối chuỗi cho số lượng
        private DataTable ConvertColumnsAsRowsQuantity(DataTable dt)
        {
            DataTable dtnew = new DataTable();
            //Convert all the rows to columns 
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtnew.Columns.Add(Convert.ToString(i));
            }
            DataRow dr;
            // Convert All the Columns to Rows 
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dr = dtnew.NewRow();
                dr[0] = dt.Columns[j].ToString();
                for (int k = 1; k <= dt.Rows.Count; k++)
                    dr[k] = "(Số lượng cần nhập: " + dt.Rows[k - 1][j] + ")";
                dtnew.Rows.Add(dr);
            }
            return dtnew;
        }
        private void ValidateData(List<ImeiRealInput> lstDataImei, List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> lstInputVoucherDetailImei)
        {
            Thread objThread = new Thread(new ThreadStart(Excute));
            objThread.Start();


            frmWaitDialog.Close();
            Thread.Sleep(0);
            objThread.Abort();
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang kiểm tra IMEI");
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            DataTable dtbGrid = grdData.DataSource as DataTable;
            dtbError = new DataTable();
            if (lstProduct != null && lstProduct.Count > 0)
            {
                if (MessageBox.Show("Toàn bộ IMEI sẽ bị xóa. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                else
                {
                    lstProduct.Clear();
                    foreach (DataRow row in dtbGrid.Rows)
                    {
                        row["RealQuantity"] = 0;
                        row["Status"] = string.Empty;
                        row["IsError"] = false;
                    }
                }
            }

            DataTable dtbImeiImport = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
            if (dtbImeiImport == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "File Excel không đúng mẫu!");
                return;
            }
            btnExportError.Enabled = false;
            if (dtbImeiImport.Columns.Count == 0 || dtbImeiImport.Rows.Count < 1) return;
            if (dtbImeiImport.Rows[0][0].ToString() != "IMEI")
            {
                MessageBoxObject.ShowWarningMessage(this, "File Excel không đúng mẫu!");
                return;
            }
            if (!dtbGrid.Columns.Contains("Status"))
            {
                DataColumn colStatus = new DataColumn();
                colStatus.ColumnName = "Status";
                dtbGrid.Columns.Add(colStatus);
            }
            if (!dtbGrid.Columns.Contains("Note"))
            {
                DataColumn colStatus = new DataColumn();
                colStatus.ColumnName = "Note";
                dtbGrid.Columns.Add(colStatus);
            }

            if (!string.IsNullOrEmpty(dtbImeiImport.Rows[0][0].ToString()))
                dtbImeiImport.Columns[0].ColumnName = dtbImeiImport.Rows[0][0].ToString();

            if (lstProduct == null)
                lstProduct = new List<ProductRealInput>();

            string strErrorId = string.Empty;
            dtbImeiImport.Rows.RemoveAt(0);
            List<string> lstImeiImportExcel = new List<string>();
            if (dtbImeiImport.Rows.Count > 0)
            {
                lstImeiImportExcel = dtbImeiImport.AsEnumerable().Select(x => x[0].ToString()).ToList();
                List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> lstImeiVoucher = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
                foreach (DataRow rowGird in dtbGrid.Rows)
                {
                    //Xử lý dữ liệu
                    bool bolIsRequireImei = Convert.ToBoolean(rowGird["IsRequestIMEI"].ToString());
                    string strProductID = rowGird["ProductID"].ToString().Trim();
                    string strProductName = rowGird["ProductName"].ToString().Trim();

                    for (int j = 0; j < 1; j++)
                    {
                        ProductRealInput objProduct = new ProductRealInput();
                        objProduct.ProductID = strProductID;
                        objProduct.ProductName = strProductName;
                        objProduct.IsRequestImei = bolIsRequireImei;
                        if (bolIsRequireImei) // Sản phẩm yêu cầu nhập IMEI
                        {
                            List<ImeiRealInput> ImeiList = new List<ImeiRealInput>();
                            objProduct.IsRequestImei = true;
                            objProduct.Quantity = 0;
                            lstImeiVoucher = InputVoucherDetailList.Where(x => x.ProductID.TrimEnd() == strProductID).ToList().Select(x => x.InputVoucherDetailIMEIList).First().ToList();
                            List<string> lst = (from i in lstImeiVoucher
                                                join o in lstImeiImportExcel
                                                on i.IMEI.TrimEnd() equals o.TrimEnd()
                                                select i.IMEI.TrimEnd()).ToList();
                            for (int iRow = 0; iRow < lst.Count; iRow++)
                            {
                                string strImei = lst[iRow];
                                if (!string.IsNullOrEmpty(strImei) && !ImeiList.Select(x => x.RealImei).Contains(strImei))
                                {
                                    ImeiRealInput objProductImei = new ImeiRealInput();
                                    objProductImei.RealImei = strImei.Trim().ToUpper();
                                    objProductImei.ProductID = objProduct.ProductID;
                                    objProductImei.ProductName = objProduct.ProductName;
                                    ImeiList.Add(objProductImei);
                                }
                            }
                            objProduct.lstImei = ImeiList;
                        }
                        lstProduct.Add(objProduct);
                    }
                }
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập dữ liệu cho file Excel!");
                return;
            }

            if (!string.IsNullOrEmpty(strErrorId))
            {
                MessageBoxObject.ShowWarningMessage(this, "Danh sách sản phẩm không hợp lệ:", strErrorId);
                return;
            }

            List<ImeiRealInput> lstImei = lstProduct.SelectMany(x => x.lstImei).Cast<ImeiRealInput>().ToList();
            List<String> lstImeiExept = new List<String>();
            dtbError.Columns.Add("IMEI không thuộc danh sách");
            lstImeiExept = lstImeiImportExcel.Except(lstImei.Select(x => x.RealImei)).ToList();
            lstImeiExept = lstImeiExept.Concat(lstImeiImportExcel.GroupBy(s => s).SelectMany(grp => grp.Skip(1)).ToList()).ToList();
            lstImeiExept.Where(x => !string.IsNullOrEmpty(x)).ToList().ForEach((item) => dtbError.Rows.Add(item));

            foreach (ProductRealInput item in lstProduct)
            {
                if (item.IsRequestImei)
                {
                    List<string> lstImeiList = InputVoucherDetailList.Where(x => x.ProductID.Trim() == item.ProductID.Trim()).First().InputVoucherDetailIMEIList.Select(x => x.IMEI.Trim()).ToList();
                    List<string> lstImeiImport = item.lstImei.Select(x => x.RealImei).ToList();
                    var intDuplicateItems = lstImeiImport.GroupBy(x => x).Where(x => x.Count() > 1).Select(x => x.Key);
                    List<string> lstExist = lstImeiImport.Join(
                                                                lstImeiList,
                                                                o => o,
                                                                id => id,
                                                                (o, id) => o).ToList();
                    List<string> lstNonExistImei = lstImeiList.Except(lstImeiImport).ToList();
                    var lstUpdate = item.lstImei.ToList().Join(lstExist, o => o.RealImei, i => i, (o, i) => o).ToList();
                    lstUpdate.All(c => { c.InvoiceImei = c.RealImei; return true; });
                    List<ImeiRealInput> lstImeiRealInput = new List<ImeiRealInput>();
                    lstImeiRealInput = lstNonExistImei.Select(x => new ImeiRealInput()
                    {
                        ProductID = item.ProductID,
                        ProductName = item.ProductName,
                        InvoiceImei = x,
                        IsError = false
                    }).ToList();
                    item.lstImei.AddRange(lstImeiRealInput);
                    item.lstImei = item.lstImei.OrderBy(x => x.IsError).ToList().OrderByDescending(x => x.InvoiceImei).ToList();

                    List<string> lstNonExistImeiVoucher = new List<string>();
                    lstNonExistImeiVoucher = lstImeiList.Except(lstImeiImport).ToList();
                    if (lstNonExistImeiVoucher.Count > 0)
                    {
                        dtbError.Columns.Add(item.ProductName + " (IMEI thiếu)");
                        DataTable dtbNewColumn = new DataTable();
                        dtbNewColumn.Columns.Add(item.ProductName + " (IMEI thiếu)");
                        lstNonExistImeiVoucher.ForEach((item2) => dtbNewColumn.Rows.Add(item2));
                        dtbError.Merge(dtbNewColumn);
                    }

                    var SameList = item.lstImei.Where(x => x.InvoiceImei == x.RealImei).ToList();
                    SameList.All(c => { c.IsError = false; c.Error = "Giống nhau"; return true; });
                    var DiffList = item.lstImei.Where(x => x.InvoiceImei != x.RealImei).ToList();
                    DiffList.All(c => { c.IsError = true; c.Error = "Khác nhau"; return true; });
                }
            }

            foreach (var item in lstProduct)
            {
                item.IsError = item.lstImei.Where(x => x.IsError == true).Count() > 0;
                if (item.IsError)
                {
                    item.Error = "Chưa khớp IMEI";
                }
            }
            for (int i = 0; i < dtbGrid.Rows.Count; i++)
            {
                string str = dtbGrid.Rows[i]["IsRequestImei"].ToString().ToLower();
                string strProductID = dtbGrid.Rows[i]["ProductID"].ToString().Trim();
                string strStatus = string.Empty;
                dtbGrid.Rows[i]["RealQuantity"] = lstProduct.Where(x => x.ProductID.TrimEnd() == strProductID).First().lstImei.Where(x => !string.IsNullOrEmpty(x.RealImei)).Count();// (dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x[0].ToString())).Count());
                int realInput = 0;
                int Quantity = 0;
                int.TryParse(dtbGrid.Rows[i]["Quantity"].ToString(), out Quantity);
                int.TryParse(dtbGrid.Rows[i]["RealQuantity"].ToString(), out realInput);
                if (realInput - Quantity == 0 && lstProduct[i].IsError == false)
                {
                    lstProduct[i].Error = "Khớp";
                }
                else if (realInput - Quantity < 0)
                {
                    lstProduct[i].Error = "Thực nhập thiếu " + Math.Abs(realInput - Quantity) + " cái";
                    lstProduct[i].IsError = true;
                }
                else if (realInput - Quantity > 0)
                {
                    lstProduct[i].Error = "Thực nhập thừa " + Math.Abs(realInput - Quantity) + " cái";
                    lstProduct[i].IsError = true;
                }
                dtbGrid.Rows[i]["Status"] = lstProduct[i].Error;
                dtbGrid.Rows[i]["IsError"] = lstProduct[i].IsError;
            }
            grdViewData.RefreshData();
            ClearSpace(dtbError);
            if (dtbError != null && dtbError.Rows.Count > 0)
            {
                btnExportError.Enabled = true;
                btnExportError_Click(null, null);
            }
        }

        private void btnExportError_Click(object sender, EventArgs e)
        {
            if (dtbError == null)
                return;

            frmRealInputChangeConfirmError frm = new frmRealInputChangeConfirmError();
            frm.DtbData = dtbError;
            frm.ShowDialog();
        }
        private void CheckDuplicateGrid(List<Product> lstProduct)
        {
            List<ProductImei> lstImei = lstProduct.SelectMany(x => x.lstImei).Cast<ProductImei>().ToList();
            var lstKey = lstImei.GroupBy(x => x.Imei)
                .Where(g => g.Count() > 1)
                .Select(y => y.Key).ToList();
            lstProduct.SelectMany(x => x.lstImei).Cast<ProductImei>().ToList().Where(x => lstKey.Contains(x.Imei)).Skip(1).All(c => { c.IsError = true; c.Error = "Lỗi duplicate file import"; return true; });

            foreach (var item in lstProduct)
            {
                item.IsError = item.lstImei.Where(x => x.IsError == true).Count() > 0;
                if (item.IsError)
                {
                    item.Error = "Số IMEI không hợp lệ là: " + item.lstImei.Where(x => x.IsError == true).Count();
                }
            }
        }

        /// <summary>
        /// Định dạng bảng dữ liệu sau khi import
        /// </summary>
        /// <param name="dtbImport">DataTable</param>
        /// <returns>DataTable</returns>
        private void ImportData(ProductRealInput objProduct, List<ImeiRealInput> lstData)
        {
            if (objProduct.IsRequestImei == true && objProduct.lstImei.Count == 0)
            {
                objProduct.IsError = true;
                objProduct.Error = "IMEI chưa nhập";
                return;
            }
            foreach (ImeiRealInput p in lstData)
            {
                if (!Library.AppCore.Other.CheckObject.CheckIMEI(p.RealImei))
                {
                    p.Error = "IMEI không đúng định dạng";
                    p.IsError = true;
                    continue;
                }
            }
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0) return;
            DataTable dtbGrid = grdData.DataSource as DataTable;
            DataRow row = grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            if (row != null)
                if (dtbGrid.Columns.Contains("Status"))
                {
                    if (row["IsRequestImei"].ToString().ToLower() == "true" && int.Parse(row["RealQuantity"].ToString()) != 0)
                    {
                        frmRealInputConfirmDetail frm = new frmRealInputConfirmDetail();
                        if (lstProduct != null && lstProduct.Count > grdViewData.FocusedRowHandle)
                            frm.ProductList = lstProduct[grdViewData.FocusedRowHandle].lstImei;
                        frm.ShowDialog();
                    }
                    else if (row["IsRequestImei"].ToString().ToLower() == "false")
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Sản phẩm này không có IMEI!");
                        return;
                    }
                }
                else
                {
                    MessageBoxObject.ShowWarningMessage(this, "Chưa nhập IMEI cho sản phẩm!");
                    return;
                }
        }

        private void InsertDataToInput()
        {
            foreach (var item in lstProduct.Where(x => x.IsRequestImei == false))
            {
                PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = new PLC.Input.WSInputVoucher.InputVoucherDetail();
                objInputVoucherDetail.ProductID = item.ProductID;
                objInputVoucherDetail.ProductName = item.ProductName;
                objInputVoucherDetail.Quantity = item.Quantity;
            }
            foreach (var item in lstProduct.Where(x => x.IsRequestImei == true).SelectMany(x => x.lstImei).Cast<ProductImei>().ToList())
            {
                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailImei = new PLC.Input.WSInputVoucher.InputVoucherDetailIMEI();
                objInputVoucherDetailImei.ProductID = item.ProductID;
                objInputVoucherDetailImei.ProductName = item.ProductName;
                objInputVoucherDetailImei.IMEI = item.Imei;
                objInputVoucherDetailIMEIList.Add(objInputVoucherDetailImei);
            }
        }

        private bool CheckValiadate()
        {
            if (lstProduct.SelectMany(x => x.lstImei).Cast<ProductImei>().ToList().Where(x => x.IsError == true).Count() > 0 ||
                lstProduct.Where(x => x.IsError == true).Count() > 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Tồn tại IMEI hoặc sản phẩm không hợp lệ.\r\nVui lòng kiểm tra lại!");
                return false;
            }
            return true;
        }

        private void grdViewData_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            DataRow row = grdViewData.GetDataRow(e.RowHandle);
            if (row != null)
            {
                if (row["IsError"].ToString().ToLower() == "true")
                    e.Appearance.BackColor = Color.Pink;
            }
        }

        private void grdViewData_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView view = sender as GridView;
            if (!view.IsFilterRow(e.RowHandle))
            {
                if (e.RowHandle == view.FocusedRowHandle)
                {
                    DataRow row = view.GetDataRow(e.RowHandle);
                    if (row != null)
                    {
                        if (row["IsError"].ToString().ToLower() == "true")
                            e.Appearance.BackColor = Color.Pink;
                    }
                }
            }
        }

        private void repoViewDetail_Click(object sender, EventArgs e)
        {
            grdData_DoubleClick(null, null);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtContent.Text.Trim()))
            {
                MessageBox.Show("Vui lòng nhập nội dung ghi chú!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtContent.Focus();
                return;
            }
            if (lstProduct.Count() == 0)
            {
                MessageBox.Show("Vui lòng nhập excel để kiểm tra thực nhập!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (lstProduct.Where(x => x.IsError == true).Count() == 0)
            {
                this.Note = txtContent.Text.Trim();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Có sản phẩm chưa khớp với thực nhập vui lòng kiểm tra lại!");
                return;
            }
        }

        private void frmRealInputConfirm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult != DialogResult.OK)
            {
                DialogResult r = MessageBox.Show("Bạn chưa xác nhận thực nhập.\r\nBạn có chắc chắn muốn đóng màn hình?", "Xác nhận thực nhập", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (r == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void frmRealInputConfirm_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            txtContent.Focus();
        }

        private void frmRealInputConfirm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F11)
                this.WindowState = FormWindowState.Maximized;
            if (e.KeyCode == Keys.Escape)
                this.WindowState = FormWindowState.Normal;
        }

        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

        private void ClearSpace(DataTable datable)
        {
            List<List<object>> listTemp = new List<List<object>>();

            for (int i = 0; i < datable.Columns.Count; i++)
            {
                List<object> stack = new List<object>();
                for (int j = 0; j < datable.Rows.Count; j++)
                {
                    object obj = datable.Rows[j][i];
                    if (obj.ToString() != "")
                    {
                        stack.Add(obj);
                    }
                }
                listTemp.Add(stack);
            }

            datable.Clear();

            while (hasData(listTemp) == true)
            {
                for (int i = 0; i < datable.Columns.Count; i++)
                {
                    DataRow dr = datable.NewRow();

                    for (int j = 0; j < listTemp.Count; j++)
                    {
                        if (listTemp[j].Count != 0)
                        {
                            dr[j] = listTemp[j][0];

                            listTemp[j].RemoveAt(0);
                        }
                    }
                    datable.Rows.Add(dr);
                }
            }
        }

        private bool hasData(List<List<object>> listTemp)
        {
            foreach (var item in listTemp)
            {
                if (item.Count != 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
