﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Text.RegularExpressions;
using Library.AppCore.Forms;
using System.Threading;
using System.Xml;

namespace ERP.Inventory.DUI.Input
{
    /// <summary>
    /// Created by: LE VĂN ĐÔNG
    /// Desc: Nhập Imei nhiều sản phẩm cùng lúc
    /// Date: 06/09/2017
    /// </summary>
    public partial class frmInputVoucher_ValidBarcodeByProduct : Form
    {
        public frmInputVoucher_ValidBarcodeByProduct()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            intWidthForm = this.Width;
            intHeightForm = this.Height;
        }

        #region Variable
        int intCheckDuplicateImeiAllProductValue = Library.AppCore.AppConfig.GetIntConfigValue("ISCHECKDUPLICATEIMEIALLPRODUCT");
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = null;
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIList = null;
        private bool isCloseForm = false;
        private DataTable dtbProduct = null;
        /// <summary>
        /// Danh sách IMEI nhập
        /// </summary>
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> InputVoucherDetailIMEIList
        {
            get { return objInputVoucherDetailIMEIList; }
            set { objInputVoucherDetailIMEIList = value; }
        }

        /// <summary>
        /// Danh sách IMEI từ form phiếu nhập
        /// </summary>
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> AllIMEIList
        {
            get { return objAllIMEIList; }
            set { objAllIMEIList = value; }
        }

        private int intInputStoreID = 0;
        private int intWidthForm = 0;
        private int intHeightForm = 0;
        private string strIMEIFormatEXP = string.Empty;
        private decimal decOrderQuantity = 0;
        private bool bolIsAutoCreateIMEI = false;
        private bool bolIsValidateAccept = false;
        private bool bolIsInputFromRetailInputPrice = false;

        //Nguyễn Văn Tài - chỉnh sửa bổ sung
        private decimal intWarranty = -1;
        private Regex regex = new Regex(@"^\d$");
        //End
        #endregion

        #region Property

        public DataTable dtbSub { get; set; }
        /// <summary>
        /// Nếu nhập từ định giá máy cũ
        /// </summary>
        public bool IsInputFromRetailInputPrice
        {
            get { return bolIsInputFromRetailInputPrice; }
            set { bolIsInputFromRetailInputPrice = value; }
        }


        private List<PLC.Input.WSInputVoucher.InputVoucherDetail> objInputVoucherDetailList = null;
        public List<PLC.Input.WSInputVoucher.InputVoucherDetail> InputVoucherDetailList
        {
            set { objInputVoucherDetailList = value; }
        }

        /// <summary>
        /// Kho nhập
        /// </summary>
        public int InputStoreID
        {
            set { intInputStoreID = value; }
        }

        /// <summary>
        /// Số lượng đơn hàng
        /// </summary>
        public decimal OrderQuantity
        {
            set { decOrderQuantity = value; }
        }
        /// <summary>
        /// Tạo IMEI tự động
        /// </summary>
        public bool IsAutoCreateIMEI
        {
            set { bolIsAutoCreateIMEI = value; }
        }
        /// <summary>
        /// Đồng ý xác nhận
        /// </summary>
        public bool IsValidateAccept
        {
            get { return bolIsValidateAccept; }
        }

        #endregion

        #region Function
        /// <summary>
        /// Kiểm tra tồn tại IMEI chưa xác nhận hoặc xác nhận lỗi
        /// </summary>
        /// <returns></returns>
        private bool CheckValidate()
        {
            var objCheck = from o in objInputVoucherDetailIMEIList
                           where o.IsValidate == true
                           select o;
            if (objCheck.Count() == 0)
            {
                MessageBox.Show("Không có IMEI hợp lệ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Xác nhận IMEI
        /// </summary>
        private void ValidateData()
        {
            var listProductID = objInputVoucherDetailList.Where(r => r.IsRequestIMEI == true).Select(r => r.ProductID);
            List<string> lstPincode = new List<string>();
            var objCheck = from o in objInputVoucherDetailIMEIList
                           where !string.IsNullOrEmpty(o.IMEI) & listProductID.Contains(o.ProductID)
                           & !o.Status.Contains("Số IMEI không đúng định dạng") & !o.Status.Contains("IMEI đã tồn tại trong phiếu nhập")
                           & !o.Status.Contains("Sản phẩm không yêu cầu nhập IMEI") & !o.Status.Contains("Trùng IMEI trên file Excel")
                           select o;
            if (objCheck.Count() > 0)
            {
                //Nguyễn Văn Tài - chỉnh sửa - tối ưu tốc độ
                Thread objThread = new Thread(new ThreadStart(Excute));
                objThread.Start();

                #region check trùng Pincode trên lưới
                foreach (var itm in objInputVoucherDetailList)
                {
                    if (itm.IsRequirePinCode)
                    {
                        // Pincode đã nhập chưa nhập
                        var rlsEnterPinCode = objCheck.Where(x => !string.IsNullOrEmpty(x.PINCode));
                        rlsEnterPinCode.All(c => { c.IsError = false; c.IsValidate = true; c.Status = c.Status.Replace("Vui lòng nhập Pincode cho IMEI này", "").Replace("PinCode chưa nhập;", "").Replace("PinCode đã tồn tại;", ""); return true; });

                        // PinCode chưa nhập
                        var rlsNonPinCode = objCheck.Where(x => x.ProductID == itm.ProductID && string.IsNullOrEmpty(x.PINCode));
                        rlsNonPinCode.All(c => { c.IsError = true; c.IsValidate = false; c.Status = "Vui lòng nhập Pincode cho IMEI này"; return true; });

                        // Check trùng PINCODE trên lưới
                        var rlsPincode = from r in objInputVoucherDetailIMEIList
                                         group r by r.PINCode into g
                                         where !string.IsNullOrEmpty(g.Key) && g.Count() > 1
                                         select g.Key;

                        foreach (var itemi in rlsPincode)
                        {
                            bool bolError = false;
                            var obj = objInputVoucherDetailIMEIList.Where(r => r.PINCode == itemi).FirstOrDefault();
                            if (obj != null)
                                bolError = obj.IsError;
                            objInputVoucherDetailIMEIList.Where(r => r.PINCode == itemi).All(r => { r.IsError = true; r.IsValidate = false; r.Status = r.Status.Replace("Trùng PinCode trên file Excel;", "") + "Trùng PinCode trên file Excel;"; return true; });
                            if (obj != null && !bolError)
                            {
                                obj.IsError = false;
                                obj.IsValidate = true;
                            }
                            if (obj != null)
                                obj.Status = obj.Status.Replace("Trùng PinCode trên file Excel;", "");
                        }
                    }
                }
                #endregion

                #region Check trùng IMEI trên lưới
                // Check trùng Imei trên lưới
                var rls = from item in objInputVoucherDetailIMEIList
                          group item by item.IMEI into g
                          where !string.IsNullOrEmpty(g.Key) && g.Count() > 1
                          select g.Key;

                foreach (var item in rls)
                {
                    bool bolError = false;
                    var obj = objCheck.Where(r => r.IMEI == item).FirstOrDefault();
                    if (obj != null)
                        bolError = obj.IsError;
                    objCheck.Where(r => r.IMEI == item).All(r => { r.IsError = true; r.IsValidate = false; r.Status += "Trùng IMEI trên file Excel;"; return true; });
                    if (obj != null && !bolError)
                    {
                        obj.IsError = false;
                        obj.IsValidate = true;
                    }
                    if (obj != null)
                        obj.Status = obj.Status.Replace("Trùng IMEI trên file Excel;", "");
                }
                #endregion

                #region Check trùng IMEI, PINCODE trong DB
                // Loại những Imei bị trùng trên lưới
                var listIMEI = objCheck.Where(r => !string.IsNullOrEmpty(r.IMEI) && r.Status != null && !r.Status.Contains("Trùng IMEI trên file Excel"));
                var rlsImei = listIMEI.Select(x => x.IMEI);
                var noImeiDupes = rlsImei.Distinct().ToList();
                DataTable dtbImeiPara = new DataTable();
                dtbImeiPara.Columns.Add("IMEI");
                foreach (var item in noImeiDupes)
                    dtbImeiPara.Rows.Add(item);
                string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbImeiPara, "IMEI");

                var listPinCode = objCheck.Where(r => !string.IsNullOrEmpty(r.PINCode) && r.Status != null && !r.Status.Contains("Trùng IMEI trên file Excel") && !r.Status.Contains("Trùng PinCode trên file Excel"));
                var rlsPinCode = listPinCode.Select(x => x.PINCode);
                var noPinCodeDupes = rlsPinCode.Distinct().ToList();
                DataTable dtbPinCodePara = new DataTable();
                dtbPinCodePara.Columns.Add("PINCODE");
                foreach (var item in noPinCodeDupes)
                    dtbPinCodePara.Rows.Add(item);
                string xmlPinCode = DUIInventoryCommon.CreateXMLFromDataTable(dtbPinCodePara, "PINCODE");

                DataSet dsResult = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("PM_CURRENTINSTOCKDT_IMEIBYPRO", new string[] { "v_Out1", "v_Out2" }, new object[] { "@IMEI", xmlIMEI, "@PINCODE", xmlPinCode });
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->ValidateData");
                }
                if (dsResult != null)
                {
                    DataTable dtbResultImei = null;
                    DataTable dtbResultPinCode = null;

                    if (dsResult.Tables.Count > 0)
                        dtbResultImei = dsResult.Tables[0];
                    if (dsResult.Tables.Count > 1)
                        dtbResultPinCode = dsResult.Tables[1];

                    if (dtbResultPinCode != null && dtbResultPinCode.Rows.Count > 0)
                    {
                        List<string> lstExitsPinCode = new List<string>();
                        lstExitsPinCode = dtbResultPinCode.AsEnumerable().Select(x => x["PINCODE"].ToString()).ToList();
                        var lstPinCodeExists = from o in listPinCode
                                               join i in lstExitsPinCode
                                               on o.PINCode.Trim() equals i.ToString().Trim()
                                               select o;
                        if (lstPinCodeExists.Count() > 0)
                            lstPinCodeExists.All(c => { c.IsError = true; c.IsValidate = false; c.Status = "PINCODE đã tồn tại;"; return true; });
                    }

                    if (dtbResultImei != null && dtbResultImei.Rows.Count > 0)
                    {
                        List<string> lstExitsImei = dtbResultImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList();
                        var lstImeiExists = from o in listIMEI
                                            join i in lstExitsImei
                                            on o.IMEI.Trim() equals i.ToString().Trim()
                                            select o;
                        lstImeiExists.All(c => { c.IsError = true; c.IsValidate = false; c.Status = "IMEI đã tồn tại;"; return true; });
                    }
                }

                #endregion

                if (dtbProduct != null)
                {
                    var lstDuplicate = from o in objCheck
                                       join i in dtbProduct.AsEnumerable()
                                       on o.IMEI.Trim() equals i["PRODUCTID"].ToString().Trim()
                                       select o;
                    lstDuplicate.All(c => { c.IsError = true; c.IsValidate = false; c.Status = c.Status.Replace("IMEI trùng với mã sản phẩm;", "") + "IMEI trùng với mã sản phẩm;"; return true; });
                }

                frmWaitDialog.Close();
                Thread.Sleep(0);
                objThread.Abort();
            }


            GetTotal();
            grdIMEI.RefreshDataSource();
            if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) > 0)
            {
                if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) == Convert.ToInt32(InputVoucherDetailIMEIList.Count))
                {
                    MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + InputVoucherDetailIMEIList.Count.ToString("#,##0"),
                   "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + InputVoucherDetailIMEIList.Count.ToString("#,##0") + "\n Bạn có muốn tiếp tục không?",
                   "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        btnClose_Click(null, null);
                    }
                }
            }
            else
            {
                btnClose_Click(null, null);
            }
            btnClose.Enabled = true;
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang kiểm tra IMEI");
        }
        /// <summary>
        /// Xóa IMEI không hợp lệ hoặc chưa xác nhận
        /// </summary>
        private void RemoveInValidData()
        {
            if (grvIMEI.FocusedRowHandle < 0)
            {
                MessageBox.Show(this, "Bạn chưa chọn dòng để xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            for (int i = InputVoucherDetailIMEIList.Count - 1; i >= 0; i--)
            {
                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailIMEI = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)InputVoucherDetailIMEIList[i];
                if (!objInputVoucherDetailIMEI.IsValidate || objInputVoucherDetailIMEI.IsError)
                {
                    InputVoucherDetailIMEIList.RemoveAt(i);
                }
            }
            grdIMEI.RefreshDataSource();
        }

        #endregion

        private void btnValidate_Click(object sender, EventArgs e)
        {
            if (grvIMEI.DataRowCount == 0)
            {
                MessageBox.Show(this, "Vui lòng nhập IMEI cho sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                ValidateData();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CheckValidate())
            {
                RemoveInValidData();
                isCloseForm = true;
                this.DialogResult = DialogResult.OK;

                //if (MessageBox.Show(this, "Tồn tại IMEI chưa xác nhận hoặc không hợp lệ.\r\nKhi đóng màn hình các IMEI đó sẽ bị mất!\r\nBạn có chắc muốn đóng không?",
                //    "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                //{
                //    return;
                //}
                //else
                //{
                //    RemoveInValidData();
                //    isCloseForm = true;
                //    this.DialogResult = DialogResult.OK;
                //}
            }
        }

        private void frmInputVoucher_ValidBarcodeByProduct_Load(object sender, EventArgs e)
        {
            dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
            grvIMEI.BestFitColumns();
            if (InputVoucherDetailIMEIList == null)
                InputVoucherDetailIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
            grdIMEI.DataSource = InputVoucherDetailIMEIList;
            btnValidate.Enabled = grvIMEI.DataRowCount > 0;
            txtTotalIMEI.Text = InputVoucherDetailIMEIList.Count().ToString();
            txtTotalIMEIValid.Text = InputVoucherDetailIMEIList.Count().ToString();
            if (IsInputFromRetailInputPrice)//không cho chỉnh sửa chỉ cho check bảo hành 
            {
                for (int i = 0; i < grvIMEI.Columns.Count - 1; i++)
                {
                    if (grvIMEI.Columns[i].FieldName.ToUpper().Trim() != "ISHASWARRANTY")
                        grvIMEI.Columns[i].OptionsColumn.AllowEdit = false;
                }
            }
            grvIMEI.Columns["PINCode"].Visible = true;
            GetTotal();
        }

        /// <summary>
        /// Tìm kiếm IMEI
        /// </summary>
        /// <param name="strIMEI"></param>
        private void FindIMEI(string strIMEI)
        {
            int intIndexFirstExist = -1;
            for (int i = 0; i < InputVoucherDetailIMEIList.Count; i++)
            {
                if (InputVoucherDetailIMEIList[i].IMEI.Trim().Contains(strIMEI))
                {
                    intIndexFirstExist = i;
                    InputVoucherDetailIMEIList[i].IsFind = true;
                    break;
                }
                else if (InputVoucherDetailIMEIList[i].IsFind)
                {
                    InputVoucherDetailIMEIList[i].IsFind = false;
                }
            }
            if (intIndexFirstExist >= 0)
            {
                grdIMEI.Select();
                grvIMEI.FocusedRowHandle = intIndexFirstExist;
                grvIMEI.FocusedColumn = grvIMEI.VisibleColumns[0];
                grvIMEI.ShowEditor();
            }
            else
                MessageBox.Show(this, "Không tìm thấy số IMEI [" + strIMEI + "] trên lưới", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            grdIMEI.RefreshDataSource();
        }



        /// <summary>
        /// Nhập trên lưới
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grvIMEI_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (grvIMEI.FocusedRowHandle >= 0)
            {
                if (e.Column.FieldName.ToUpper().Trim() != "PINCODE")
                    return;

                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)grvIMEI.GetRow(grvIMEI.FocusedRowHandle);

                objInputVoucherDetail.IsEdit = true;
                btnClose.Enabled = false;

                if (objInputVoucherDetail.PINCode == null || objInputVoucherDetail.PINCode.Trim().Length == 0)
                    return;
                objInputVoucherDetail.PINCode = objInputVoucherDetail.PINCode.ToUpper();
                var PincodeExist = from o in InputVoucherDetailIMEIList
                                   where o.PINCode == objInputVoucherDetail.PINCode
                                   select o;

                if (PincodeExist.Count() > 1)
                {
                    MessageBox.Show(this, "Pincode này đã tồn tại. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objInputVoucherDetail.PINCode = string.Empty;
                    return;
                }
                if (!Library.AppCore.Other.CheckObject.CheckIMEI(objInputVoucherDetail.PINCode))
                {
                    MessageBox.Show(this, "Pincode này không đúng định dạng. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objInputVoucherDetail.PINCode = string.Empty;
                    return;
                }
                else
                {
                    objInputVoucherDetail.Status.Replace("Số PinCode không đúng định dạng;", "");
                }
                grdIMEI.RefreshDataSource();
            }
        }

        private void frmInputVoucher_ValidBarcodeByProduct_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.S)
            {
                btnValidate_Click(null, null);
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.Delete)
            {
                mnuItemDeleteIMEI_Click(null, null);
            }
            else if (e.KeyCode == Keys.F4)
            {
                this.Close();
            }
            else if (e.KeyCode == Keys.F11)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.WindowState = FormWindowState.Normal;
                this.Width = this.intWidthForm;
                this.Height = this.intHeightForm;
            }
        }

        /// <summary>
        /// Xóa IMEI
        /// </summary>
        private void DeleteIMEI()
        {
            //InputVoucherDetailIMEIList = (List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>)grdIMEI.DataSource;
            var v_InputVoucherDetail = from o in InputVoucherDetailIMEIList
                                       where o.IsSelect == true
                                       select o;
            if (v_InputVoucherDetail.Count() == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn IMEI cần xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (MessageBox.Show(this, "Bạn có chắc muốn xóa các IMEI được chọn không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            for (int i = InputVoucherDetailIMEIList.Count - 1; i >= 0; i--)
            {
                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailIMEI = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)InputVoucherDetailIMEIList[i];
                if (objInputVoucherDetailIMEI.IsSelect == true)
                {
                    InputVoucherDetailIMEIList.Remove(objInputVoucherDetailIMEI);
                }
            }
            grdIMEI.RefreshDataSource();
            for (int i = 0; i < grvIMEI.RowCount; i++)
            {
                grvIMEI.SetRowCellValue(i, "IsSelect", false);
            }
            btnValidate.Enabled = grvIMEI.DataRowCount > 0;
            GetTotal();
        }

        private void GetTotal()
        {
            txtTotalIMEI.Text = InputVoucherDetailIMEIList.Count().ToString();
            var vTotalEMEIVaild = from o in InputVoucherDetailIMEIList
                                  where o.IsError == false && o.IsValidate == true
                                  select o;
            txtTotalIMEIValid.Text = vTotalEMEIVaild.Count().ToString();

            var vTotalEMEIInVaild = from o in InputVoucherDetailIMEIList
                                    where o.IsError == true
                                    select o;
            txtTotalIMEIInValid.Text = vTotalEMEIInVaild.Count().ToString();

        }
        private void mnuItemDeleteIMEI_Click(object sender, EventArgs e)
        {
            if (!mnuItemDeleteIMEI.Enabled)
            {
                return;
            }
            DeleteIMEI();
            //btnValidate.Enabled = InputVoucherDetailIMEIList.Count > 0;
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName != string.Empty)
            {
                DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                bool bolIsSelect = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["IsError"]));
                if (bolIsSelect)
                {
                    e.Appearance.BackColor = Color.Pink;
                }
            }
        }



        private void frmInputVoucher_ValidBarcodeByProduct_FormClosing(object sender, FormClosingEventArgs e)
        {
            var objCheck = from o in objInputVoucherDetailIMEIList
                           select o;
            if (!isCloseForm && objCheck.Count() > 0)
            {
                if (MessageBox.Show("Bạn muốn đóng form nhập IMEI?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void ImportExcelFile()
        {
            try
            {
                if (InputVoucherDetailIMEIList.Count > 0)
                {
                    if (MessageBox.Show("Toàn bộ IMEI sẽ bị xóa. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                }
                InputVoucherDetailIMEIList.Clear();
                grdIMEI.RefreshDataSource();
                DataTable dtbExcelData = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(4);
                if (dtbExcelData == null || dtbExcelData.Rows.Count < 1)
                    return;
                DataRow rowCheck = dtbExcelData.Rows[0];
                if (rowCheck[0].ToString().Trim().ToLower().Replace(" ", "") != "mãsảnphẩm"
                        || rowCheck[1].ToString().Trim().ToLower() != "imei"
                        || rowCheck[2].ToString().Trim().ToLower().Replace(" ", "") != "pincode"
                        || rowCheck[3].ToString().Trim().ToLower().Replace(" ", "") != "bảohành"
                        )
                {
                    MessageBox.Show("File nhập không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                for (int i = 0; i < dtbExcelData.Columns.Count; i++)
                {
                    if (string.IsNullOrEmpty(dtbExcelData.Rows[0][i].ToString()))
                    {
                        MessageBox.Show("File nhập không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    dtbExcelData.Columns[i].Caption = dtbExcelData.Rows[0][i].ToString();
                }
                if (dtbExcelData.Rows.Count > 0)
                {
                    dtbExcelData.Rows.RemoveAt(0);
                    ImportData(dtbExcelData);
                    grdIMEI.DataSource = InputVoucherDetailIMEIList;
                    grdIMEI.RefreshDataSource();
                    btnValidate.Enabled = grvIMEI.DataRowCount > 0;
                    GetTotal();
                }

            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi nhập dữ liệu từ Excel", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi nhập dữ liệu từ Excel", objExce.ToString(), "Excel -> ImportExcelFile()", DUIInventory_Globals.ModuleName);
            }
        }

        /// <summary>
        /// Định dạng bảng dữ liệu sau khi import
        /// </summary>
        /// <param name="dtbImport">DataTable</param>
        /// <returns>DataTable</returns>
        private void ImportData(DataTable dtbExcelData)
        {
            foreach (DataRow row in dtbExcelData.Rows)
            {
                string strErrorImport = string.Empty;
                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI obj = new PLC.Input.WSInputVoucher.InputVoucherDetailIMEI();
                obj.IsSelect = false;

                string strProductID = row[0].ToString();
                string strProductName = string.Empty;
                string strIMEI = row[1].ToString().ToUpper();

                bool bolIsOldIMEI = false;
                bool bolExistProductID = false;
                bool bolIsExistsIMEI = false;
                bool bolIsRequirePincode = false;
                bool bolIsRequireImei = false;

                if (!string.IsNullOrEmpty(strProductID))
                {
                    var ProductExist = (from r in objInputVoucherDetailList
                                        where r.ProductID == strProductID.Trim()
                                        select r).FirstOrDefault();

                    bolExistProductID = ProductExist == null;
                    if (!bolExistProductID)
                    {
                        strProductName = ProductExist.ProductName;
                        bolIsRequirePincode = ProductExist.IsRequirePinCode;
                        strIMEIFormatEXP = ProductExist.IMEIFormatEXP;
                        bolIsRequireImei = ProductExist.IsRequestIMEI;
                    }
                }

                if (string.IsNullOrEmpty(strProductID))
                {
                    if (strErrorImport == string.Empty)
                        strErrorImport += "Mã sản phẩm chưa nhập;";
                    obj.IsError = true;
                }
                else if (bolExistProductID)
                {
                    if (strErrorImport == string.Empty)
                        strErrorImport += "Mã sản phẩm không tồn tại trong phiếu nhập;";
                    obj.IsError = true;
                }
                else if (!bolIsRequireImei)
                {
                    if (strErrorImport == string.Empty)
                        strErrorImport += "Sản phẩm không yêu cầu nhập IMEI;";
                    obj.IsError = true;
                }
                else if (string.IsNullOrEmpty(strIMEI))
                {
                    if (strErrorImport == string.Empty)
                        strErrorImport += "IMEI chưa nhập;";
                    obj.IsError = true;
                }
                else if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI))
                {
                    if (strErrorImport == string.Empty)
                        strErrorImport += "Số IMEI không đúng định dạng;";
                    obj.IsError = true;
                }
                else if (!string.IsNullOrEmpty(strIMEIFormatEXP))
                {
                    if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI, strIMEIFormatEXP))
                    {
                        if (strErrorImport != string.Empty)
                            strErrorImport += "Số IMEI không đúng định dạng;";
                        obj.IsError = true;
                    }
                }
                else if (bolIsExistsIMEI && !bolIsOldIMEI)
                {
                    if (strErrorImport == string.Empty)
                        strErrorImport += "IMEI đã tồn tại trong phiếu nhập;";
                    obj.IsError = true;
                }


                obj.IMEI = strIMEI.Trim();
                obj.ProductID = strProductID.Trim();
                obj.ProductName = strProductName;
                obj.IsRequirePinCode = bolIsRequirePincode;

                if (bolIsRequirePincode)
                {
                    if (string.IsNullOrEmpty(row[2].ToString()))
                    {
                        if (strErrorImport == string.Empty)
                            strErrorImport += "PinCode chưa nhập;";
                        obj.IsError = true;
                    }
                    else
                    {
                        obj.PINCode = row[2].ToString().ToUpper().Trim();
                        if (!Library.AppCore.Other.CheckObject.CheckIMEI(obj.PINCode))
                        {
                            if (strErrorImport == string.Empty)
                                strErrorImport += "Số PinCode không đúng định dạng;";
                            obj.IsError = true;
                        }
                    }
                }


                intWarranty = -1;
                if (string.IsNullOrEmpty(row[3].ToString()))
                {
                    obj.IsHasWarranty = false;
                }
                else
                {
                    if (Regex.IsMatch(row[3].ToString(), @"^\d$"))
                    {
                        decimal.TryParse(row[3].ToString(), out intWarranty);
                        if (intWarranty < 0)
                        {
                            if (strErrorImport == string.Empty)
                                strErrorImport += "Bảo hành không đúng định dạng;";
                        }
                        else
                            if (intWarranty == 1)
                            obj.IsHasWarranty = true;
                        else
                            obj.IsHasWarranty = false;
                    }
                    else
                        strErrorImport += "Bảo hành không đúng định dạng;";
                }

                obj.IsValidate = (obj.IsError ? false : true);
                obj.Status = strErrorImport;
                InputVoucherDetailIMEIList.Add(obj);
            }

            // Check trùng PINCODE trên lưới
            var rlsPincode = from item in InputVoucherDetailIMEIList
                             group item by item.PINCode into g
                             where !string.IsNullOrEmpty(g.Key) && g.Count() > 1
                             select g.Key;

            foreach (var item in rlsPincode)
            {
                bool bolError = false;
                var obj = InputVoucherDetailIMEIList.Where(r => r.PINCode == item).FirstOrDefault();
                if (obj != null)
                    bolError = obj.IsError;
                InputVoucherDetailIMEIList.Where(r => r.PINCode == item).All(r => { r.IsError = true; r.Status += "Trùng PinCode trên file Excel;"; return true; });
                if (obj != null && !bolError)
                    obj.IsError = false;
                if (obj != null)
                    obj.Status = obj.Status.Replace("Trùng PinCode trên file Excel;", "");
            }


            // Check trùng Imei trên lưới
            var rls = from item in InputVoucherDetailIMEIList
                      group item by item.IMEI into g
                      where !string.IsNullOrEmpty(g.Key) && g.Count() > 1
                      select g.Key;

            foreach (var item in rls)
            {
                bool bolError = false;
                var obj = InputVoucherDetailIMEIList.Where(r => r.IMEI == item).FirstOrDefault();
                if (obj != null)
                    bolError = obj.IsError;
                InputVoucherDetailIMEIList.Where(r => r.IMEI == item).All(r => { r.IsError = true; r.Status = "Trùng IMEI trên file Excel;" + r.Status; return true; });
                if (obj != null && !bolError)
                    obj.IsError = false;
                if (obj != null)
                    obj.Status = obj.Status.Replace("Trùng IMEI trên file Excel;", "");
            }

            GetTotal();
        }

        private void chksChon_CheckedChanged(object sender, EventArgs e)
        {
            if (grvIMEI.FocusedRowHandle < 0)
                return;
            DevExpress.XtraEditors.CheckEdit chkIsSelect = (DevExpress.XtraEditors.CheckEdit)sender;
            PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)grvIMEI.GetRow(grvIMEI.FocusedRowHandle);
            objInputVoucherDetail.IsSelect = chkIsSelect.Checked;
            grdIMEI.RefreshDataSource();
        }

        private void mnuPopUp_Opening(object sender, CancelEventArgs e)
        {
            mnuItemDeleteIMEI.Enabled = grvIMEI.DataRowCount > 0;
        }

        private void grvIMEI_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (grvIMEI.FocusedRowHandle >= 0)
            {
                DevExpress.XtraGrid.Views.Base.ColumnView colview = (DevExpress.XtraGrid.Views.Base.ColumnView)sender;
                if (colview.FocusedColumn.FieldName.ToUpper().Trim() == "PINCODE")
                {
                    PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetail = (PLC.Input.WSInputVoucher.InputVoucherDetailIMEI)grvIMEI.GetRow(grvIMEI.FocusedRowHandle);
                    if (objInputVoucherDetail != null && !string.IsNullOrEmpty(objInputVoucherDetail.ProductName) && !objInputVoucherDetail.IsRequirePinCode)
                    {
                        MessageBox.Show("Sản phẩm không yêu cầu nhập PinCode!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                    }
                }
            }
        }

        private void btnExportExcelTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraGrid.GridControl grdTemp = new DevExpress.XtraGrid.GridControl();
                DevExpress.XtraGrid.Views.Grid.GridView grvTemp = new DevExpress.XtraGrid.Views.Grid.GridView();
                grdTemp.MainView = grvTemp;
                string[] fieldNames = new string[] { "Mã sản phẩm", "IMEI", "PinCode", "Bảo hành" };
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdTemp, fieldNames);
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdTemp);
            }
            catch (Exception objExc)
            {
            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            ImportExcelFile();
        }
    }
}
