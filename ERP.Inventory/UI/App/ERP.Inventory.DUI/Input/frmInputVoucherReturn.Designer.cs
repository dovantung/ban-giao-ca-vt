﻿namespace ERP.Inventory.DUI.Input
{
    partial class frmInputVoucherReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInputVoucherReturn));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.flexOutputVoucherDetail = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuFlexPopup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemAdjustPrice = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboInputStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.ctrlStoreManagerUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.chkLostVATInvoice = new System.Windows.Forms.CheckBox();
            this.chkIsOld = new System.Windows.Forms.CheckBox();
            this.chkIsNew = new System.Windows.Forms.CheckBox();
            this.txtReturnNote = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtReturnReason = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboInputTypeID = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboOutputStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.lblCustomer = new System.Windows.Forms.LinkLabel();
            this.txtOutputContent = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDiscount = new C1.Win.C1Input.C1NumericEdit();
            this.cboDiscountReasonID = new System.Windows.Forms.ComboBox();
            this.lblDeCount = new System.Windows.Forms.Label();
            this.cboCurrencyUnitID = new System.Windows.Forms.ComboBox();
            this.lblCurrencyUnit = new System.Windows.Forms.Label();
            this.dtPayableTime = new System.Windows.Forms.DateTimePicker();
            this.cboPayableTypeID = new System.Windows.Forms.ComboBox();
            this.lblPayableTime = new System.Windows.Forms.Label();
            this.lblPayableType = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCustomerTaxID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCustomerPhone = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCustomerAddress = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtOutputUser = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDenominator = new System.Windows.Forms.TextBox();
            this.dtInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.lblInvoiceDate = new System.Windows.Forms.Label();
            this.lblDenominator = new System.Windows.Forms.Label();
            this.lblStore = new System.Windows.Forms.Label();
            this.dtOutputDate = new System.Windows.Forms.DateTimePicker();
            this.lblOutputDate = new System.Windows.Forms.Label();
            this.lblInVoiceID = new System.Windows.Forms.Label();
            this.txtOutputVoucherID = new System.Windows.Forms.TextBox();
            this.lblOutputVoucherID = new System.Windows.Forms.Label();
            this.txtInVoiceID = new System.Windows.Forms.TextBox();
            this.lblOrderID = new System.Windows.Forms.Label();
            this.txtInvoiceSymbol = new System.Windows.Forms.TextBox();
            this.lblInvoiceSymbol = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblVoucherAlert = new System.Windows.Forms.Label();
            this.grpVoucherDetail = new System.Windows.Forms.GroupBox();
            this.txtPaymentCardVoucherID = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cmdExchangeRate = new System.Windows.Forms.Button();
            this.txtTotalPaymentCard = new DevExpress.XtraEditors.TextEdit();
            this.txtPaymentCard = new DevExpress.XtraEditors.TextEdit();
            this.txtCashUSDExchange = new DevExpress.XtraEditors.TextEdit();
            this.txtDebt = new DevExpress.XtraEditors.TextEdit();
            this.txtCashUSD = new DevExpress.XtraEditors.TextEdit();
            this.txtCashVND = new DevExpress.XtraEditors.TextEdit();
            this.cboPaymentCardID = new System.Windows.Forms.ComboBox();
            this.dtSettlementDate = new System.Windows.Forms.DateTimePicker();
            this.cboPaymentCurrencyUnitID = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.grpVoucher = new System.Windows.Forms.GroupBox();
            this.txtVoucherDiscount = new C1.Win.C1Input.C1NumericEdit();
            this.txtTotalOrderPaid = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.txtExchangeRate = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalLiquidate = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalMoney = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.cboCurrencyUnitID2 = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cboVoucherTypeID = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtVoucherID = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cmdUpdate = new System.Windows.Forms.Button();
            this.cmdClose = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.txtTotalQuantity = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexOutputVoucherDetail)).BeginInit();
            this.mnuFlexPopup.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.grpVoucherDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPaymentCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashUSDExchange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashUSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashVND.Properties)).BeginInit();
            this.grpVoucher.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoucherDiscount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalOrderPaid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLiquidate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalMoney.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(1, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(971, 546);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.flexOutputVoucherDetail);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(963, 517);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin phiếu";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // flexOutputVoucherDetail
            // 
            this.flexOutputVoucherDetail.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.Both;
            this.flexOutputVoucherDetail.AllowFreezing = C1.Win.C1FlexGrid.AllowFreezingEnum.Both;
            this.flexOutputVoucherDetail.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.FixedOnly;
            this.flexOutputVoucherDetail.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.Both;
            this.flexOutputVoucherDetail.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexOutputVoucherDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flexOutputVoucherDetail.AutoClipboard = true;
            this.flexOutputVoucherDetail.ClipboardCopyMode = C1.Win.C1FlexGrid.ClipboardCopyModeEnum.DataAndAllHeaders;
            this.flexOutputVoucherDetail.ColumnInfo = "1,1,0,0,0,105,Columns:";
            this.flexOutputVoucherDetail.ContextMenuStrip = this.mnuFlexPopup;
            this.flexOutputVoucherDetail.Location = new System.Drawing.Point(6, 321);
            this.flexOutputVoucherDetail.Name = "flexOutputVoucherDetail";
            this.flexOutputVoucherDetail.Rows.Count = 1;
            this.flexOutputVoucherDetail.Rows.DefaultSize = 21;
            this.flexOutputVoucherDetail.Size = new System.Drawing.Size(951, 190);
            this.flexOutputVoucherDetail.StyleInfo = resources.GetString("flexOutputVoucherDetail.StyleInfo");
            this.flexOutputVoucherDetail.TabIndex = 2;
            this.flexOutputVoucherDetail.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexOutputVoucherDetail.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexOutputVoucherDetail_BeforeEdit);
            this.flexOutputVoucherDetail.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexOutputVoucherDetail_AfterEdit);
            // 
            // mnuFlexPopup
            // 
            this.mnuFlexPopup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAdjustPrice});
            this.mnuFlexPopup.Name = "mnuFlexPopup";
            this.mnuFlexPopup.Size = new System.Drawing.Size(151, 26);
            this.mnuFlexPopup.Opening += new System.ComponentModel.CancelEventHandler(this.mnuFlexPopup_Opening);
            // 
            // mnuItemAdjustPrice
            // 
            this.mnuItemAdjustPrice.Enabled = false;
            this.mnuItemAdjustPrice.Name = "mnuItemAdjustPrice";
            this.mnuItemAdjustPrice.Size = new System.Drawing.Size(150, 22);
            this.mnuItemAdjustPrice.Text = "Điều chỉnh giá";
            this.mnuItemAdjustPrice.Click += new System.EventHandler(this.mnuItemAdjustPrice_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cboInputStoreID);
            this.groupBox2.Controls.Add(this.ctrlStoreManagerUser);
            this.groupBox2.Controls.Add(this.chkLostVATInvoice);
            this.groupBox2.Controls.Add(this.chkIsOld);
            this.groupBox2.Controls.Add(this.chkIsNew);
            this.groupBox2.Controls.Add(this.txtReturnNote);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtReturnReason);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cboInputTypeID);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(5, 231);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(952, 86);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin nhập trả";
            // 
            // cboInputStoreID
            // 
            this.cboInputStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputStoreID.Location = new System.Drawing.Point(101, 22);
            this.cboInputStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputStoreID.Name = "cboInputStoreID";
            this.cboInputStoreID.Size = new System.Drawing.Size(206, 24);
            this.cboInputStoreID.TabIndex = 1;
            // 
            // ctrlStoreManagerUser
            // 
            this.ctrlStoreManagerUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrlStoreManagerUser.IsValidate = true;
            this.ctrlStoreManagerUser.Location = new System.Drawing.Point(736, 24);
            this.ctrlStoreManagerUser.Margin = new System.Windows.Forms.Padding(4);
            this.ctrlStoreManagerUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ctrlStoreManagerUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ctrlStoreManagerUser.Name = "ctrlStoreManagerUser";
            this.ctrlStoreManagerUser.Size = new System.Drawing.Size(210, 22);
            this.ctrlStoreManagerUser.TabIndex = 5;
            this.ctrlStoreManagerUser.UserName = "";
            this.ctrlStoreManagerUser.WorkingPositionID = 0;
            // 
            // chkLostVATInvoice
            // 
            this.chkLostVATInvoice.AutoSize = true;
            this.chkLostVATInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLostVATInvoice.ForeColor = System.Drawing.Color.Red;
            this.chkLostVATInvoice.Location = new System.Drawing.Point(835, 55);
            this.chkLostVATInvoice.Name = "chkLostVATInvoice";
            this.chkLostVATInvoice.Size = new System.Drawing.Size(111, 20);
            this.chkLostVATInvoice.TabIndex = 12;
            this.chkLostVATInvoice.Text = "Mất HĐ VAT";
            // 
            // chkIsOld
            // 
            this.chkIsOld.AutoCheck = false;
            this.chkIsOld.AutoSize = true;
            this.chkIsOld.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsOld.ForeColor = System.Drawing.Color.Red;
            this.chkIsOld.Location = new System.Drawing.Point(784, 55);
            this.chkIsOld.Name = "chkIsOld";
            this.chkIsOld.Size = new System.Drawing.Size(45, 20);
            this.chkIsOld.TabIndex = 11;
            this.chkIsOld.Text = "Cũ";
            // 
            // chkIsNew
            // 
            this.chkIsNew.AutoCheck = false;
            this.chkIsNew.AutoSize = true;
            this.chkIsNew.Checked = true;
            this.chkIsNew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsNew.ForeColor = System.Drawing.Color.Red;
            this.chkIsNew.Location = new System.Drawing.Point(726, 55);
            this.chkIsNew.Name = "chkIsNew";
            this.chkIsNew.Size = new System.Drawing.Size(52, 20);
            this.chkIsNew.TabIndex = 10;
            this.chkIsNew.Text = "Mới";
            // 
            // txtReturnNote
            // 
            this.txtReturnNote.Location = new System.Drawing.Point(426, 54);
            this.txtReturnNote.MaxLength = 200;
            this.txtReturnNote.Name = "txtReturnNote";
            this.txtReturnNote.Size = new System.Drawing.Size(210, 22);
            this.txtReturnNote.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(316, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "Ghi chú:";
            // 
            // txtReturnReason
            // 
            this.txtReturnReason.Location = new System.Drawing.Point(101, 54);
            this.txtReturnReason.MaxLength = 200;
            this.txtReturnReason.Name = "txtReturnReason";
            this.txtReturnReason.Size = new System.Drawing.Size(206, 22);
            this.txtReturnReason.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 16);
            this.label9.TabIndex = 6;
            this.label9.Text = "Lý do trả:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(643, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 16);
            this.label6.TabIndex = 4;
            this.label6.Text = "NV Quản lý:";
            // 
            // cboInputTypeID
            // 
            this.cboInputTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInputTypeID.DropDownWidth = 250;
            this.cboInputTypeID.Enabled = false;
            this.cboInputTypeID.Location = new System.Drawing.Point(427, 23);
            this.cboInputTypeID.Name = "cboInputTypeID";
            this.cboInputTypeID.Size = new System.Drawing.Size(209, 24);
            this.cboInputTypeID.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(316, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Loại phiếu nhập:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Kho nhập:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboOutputStoreID);
            this.groupBox1.Controls.Add(this.txtCustomerID);
            this.groupBox1.Controls.Add(this.lblCustomer);
            this.groupBox1.Controls.Add(this.txtOutputContent);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtDiscount);
            this.groupBox1.Controls.Add(this.cboDiscountReasonID);
            this.groupBox1.Controls.Add(this.lblDeCount);
            this.groupBox1.Controls.Add(this.cboCurrencyUnitID);
            this.groupBox1.Controls.Add(this.lblCurrencyUnit);
            this.groupBox1.Controls.Add(this.dtPayableTime);
            this.groupBox1.Controls.Add(this.cboPayableTypeID);
            this.groupBox1.Controls.Add(this.lblPayableTime);
            this.groupBox1.Controls.Add(this.lblPayableType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtCustomerTaxID);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtCustomerPhone);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtCustomerAddress);
            this.groupBox1.Controls.Add(this.txtCustomerName);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.txtOutputUser);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtDenominator);
            this.groupBox1.Controls.Add(this.dtInvoiceDate);
            this.groupBox1.Controls.Add(this.lblInvoiceDate);
            this.groupBox1.Controls.Add(this.lblDenominator);
            this.groupBox1.Controls.Add(this.lblStore);
            this.groupBox1.Controls.Add(this.dtOutputDate);
            this.groupBox1.Controls.Add(this.lblOutputDate);
            this.groupBox1.Controls.Add(this.lblInVoiceID);
            this.groupBox1.Controls.Add(this.txtOutputVoucherID);
            this.groupBox1.Controls.Add(this.lblOutputVoucherID);
            this.groupBox1.Controls.Add(this.txtInVoiceID);
            this.groupBox1.Controls.Add(this.lblOrderID);
            this.groupBox1.Controls.Add(this.txtInvoiceSymbol);
            this.groupBox1.Controls.Add(this.lblInvoiceSymbol);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(951, 220);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin phiếu xuất";
            // 
            // cboOutputStoreID
            // 
            this.cboOutputStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboOutputStoreID.Location = new System.Drawing.Point(426, 74);
            this.cboOutputStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboOutputStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboOutputStoreID.Name = "cboOutputStoreID";
            this.cboOutputStoreID.Size = new System.Drawing.Size(210, 24);
            this.cboOutputStoreID.TabIndex = 15;
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCustomerID.Location = new System.Drawing.Point(100, 102);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.ReadOnly = true;
            this.txtCustomerID.Size = new System.Drawing.Size(210, 24);
            this.txtCustomerID.TabIndex = 19;
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Enabled = false;
            this.lblCustomer.Location = new System.Drawing.Point(4, 106);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(81, 16);
            this.lblCustomer.TabIndex = 18;
            this.lblCustomer.TabStop = true;
            this.lblCustomer.Text = "Khách hàng:";
            this.lblCustomer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblCustomer_LinkClicked);
            // 
            // txtOutputContent
            // 
            this.txtOutputContent.Location = new System.Drawing.Point(100, 188);
            this.txtOutputContent.Name = "txtOutputContent";
            this.txtOutputContent.Size = new System.Drawing.Size(535, 22);
            this.txtOutputContent.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 16);
            this.label2.TabIndex = 34;
            this.label2.Text = "Nội dung:";
            // 
            // txtDiscount
            // 
            this.txtDiscount.CustomFormat = "###,###,##0";
            this.txtDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtDiscount.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtDiscount.Location = new System.Drawing.Point(807, 188);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(138, 24);
            this.txtDiscount.TabIndex = 38;
            this.txtDiscount.Tag = null;
            this.txtDiscount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDiscount.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            this.txtDiscount.ValueChanged += new System.EventHandler(this.txtDiscount_ValueChanged);
            // 
            // cboDiscountReasonID
            // 
            this.cboDiscountReasonID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscountReasonID.DropDownWidth = 250;
            this.cboDiscountReasonID.Location = new System.Drawing.Point(735, 188);
            this.cboDiscountReasonID.Name = "cboDiscountReasonID";
            this.cboDiscountReasonID.Size = new System.Drawing.Size(72, 24);
            this.cboDiscountReasonID.TabIndex = 37;
            // 
            // lblDeCount
            // 
            this.lblDeCount.AutoSize = true;
            this.lblDeCount.Location = new System.Drawing.Point(643, 191);
            this.lblDeCount.Name = "lblDeCount";
            this.lblDeCount.Size = new System.Drawing.Size(65, 16);
            this.lblDeCount.TabIndex = 36;
            this.lblDeCount.Text = "Giảm giá:";
            // 
            // cboCurrencyUnitID
            // 
            this.cboCurrencyUnitID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyUnitID.DropDownWidth = 250;
            this.cboCurrencyUnitID.Location = new System.Drawing.Point(735, 158);
            this.cboCurrencyUnitID.Name = "cboCurrencyUnitID";
            this.cboCurrencyUnitID.Size = new System.Drawing.Size(210, 24);
            this.cboCurrencyUnitID.TabIndex = 33;
            // 
            // lblCurrencyUnit
            // 
            this.lblCurrencyUnit.AutoSize = true;
            this.lblCurrencyUnit.Location = new System.Drawing.Point(643, 162);
            this.lblCurrencyUnit.Name = "lblCurrencyUnit";
            this.lblCurrencyUnit.Size = new System.Drawing.Size(61, 16);
            this.lblCurrencyUnit.TabIndex = 32;
            this.lblCurrencyUnit.Text = "Loại tiền:";
            // 
            // dtPayableTime
            // 
            this.dtPayableTime.CustomFormat = "dd/MM/yyyy";
            this.dtPayableTime.Enabled = false;
            this.dtPayableTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPayableTime.Location = new System.Drawing.Point(426, 160);
            this.dtPayableTime.Name = "dtPayableTime";
            this.dtPayableTime.Size = new System.Drawing.Size(209, 22);
            this.dtPayableTime.TabIndex = 31;
            // 
            // cboPayableTypeID
            // 
            this.cboPayableTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPayableTypeID.DropDownWidth = 250;
            this.cboPayableTypeID.Enabled = false;
            this.cboPayableTypeID.Location = new System.Drawing.Point(100, 158);
            this.cboPayableTypeID.Name = "cboPayableTypeID";
            this.cboPayableTypeID.Size = new System.Drawing.Size(210, 24);
            this.cboPayableTypeID.TabIndex = 29;
            // 
            // lblPayableTime
            // 
            this.lblPayableTime.AutoSize = true;
            this.lblPayableTime.Location = new System.Drawing.Point(316, 162);
            this.lblPayableTime.Name = "lblPayableTime";
            this.lblPayableTime.Size = new System.Drawing.Size(108, 16);
            this.lblPayableTime.TabIndex = 30;
            this.lblPayableTime.Text = "Ngày thanh toán:";
            // 
            // lblPayableType
            // 
            this.lblPayableType.AutoSize = true;
            this.lblPayableType.Location = new System.Drawing.Point(4, 162);
            this.lblPayableType.Name = "lblPayableType";
            this.lblPayableType.Size = new System.Drawing.Size(81, 16);
            this.lblPayableType.TabIndex = 28;
            this.lblPayableType.Text = "Thanh toán: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 24;
            this.label1.Text = "Địa chỉ:";
            // 
            // txtCustomerTaxID
            // 
            this.txtCustomerTaxID.Location = new System.Drawing.Point(735, 131);
            this.txtCustomerTaxID.Name = "txtCustomerTaxID";
            this.txtCustomerTaxID.Size = new System.Drawing.Size(210, 22);
            this.txtCustomerTaxID.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(643, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 16);
            this.label11.TabIndex = 26;
            this.label11.Text = "Mã số thuế:";
            // 
            // txtCustomerPhone
            // 
            this.txtCustomerPhone.Location = new System.Drawing.Point(735, 104);
            this.txtCustomerPhone.Name = "txtCustomerPhone";
            this.txtCustomerPhone.Size = new System.Drawing.Size(210, 22);
            this.txtCustomerPhone.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(643, 107);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 16);
            this.label10.TabIndex = 22;
            this.label10.Text = "Điện thoại:";
            // 
            // txtCustomerAddress
            // 
            this.txtCustomerAddress.Location = new System.Drawing.Point(100, 131);
            this.txtCustomerAddress.Name = "txtCustomerAddress";
            this.txtCustomerAddress.Size = new System.Drawing.Size(535, 22);
            this.txtCustomerAddress.TabIndex = 25;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new System.Drawing.Point(426, 104);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(210, 22);
            this.txtCustomerName.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(316, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 16);
            this.label8.TabIndex = 20;
            this.label8.Text = "Họ tên:";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1.Location = new System.Drawing.Point(735, 74);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(210, 22);
            this.textBox1.TabIndex = 17;
            // 
            // txtOutputUser
            // 
            this.txtOutputUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtOutputUser.Location = new System.Drawing.Point(100, 73);
            this.txtOutputUser.Name = "txtOutputUser";
            this.txtOutputUser.ReadOnly = true;
            this.txtOutputUser.Size = new System.Drawing.Size(210, 24);
            this.txtOutputUser.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "Nhân viên xuất:";
            // 
            // txtDenominator
            // 
            this.txtDenominator.BackColor = System.Drawing.SystemColors.Info;
            this.txtDenominator.Location = new System.Drawing.Point(735, 46);
            this.txtDenominator.Name = "txtDenominator";
            this.txtDenominator.ReadOnly = true;
            this.txtDenominator.Size = new System.Drawing.Size(210, 22);
            this.txtDenominator.TabIndex = 11;
            // 
            // dtInvoiceDate
            // 
            this.dtInvoiceDate.CustomFormat = "dd/MM/yyyy";
            this.dtInvoiceDate.Enabled = false;
            this.dtInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtInvoiceDate.Location = new System.Drawing.Point(735, 18);
            this.dtInvoiceDate.Name = "dtInvoiceDate";
            this.dtInvoiceDate.Size = new System.Drawing.Size(210, 22);
            this.dtInvoiceDate.TabIndex = 5;
            // 
            // lblInvoiceDate
            // 
            this.lblInvoiceDate.AutoSize = true;
            this.lblInvoiceDate.Location = new System.Drawing.Point(643, 21);
            this.lblInvoiceDate.Name = "lblInvoiceDate";
            this.lblInvoiceDate.Size = new System.Drawing.Size(96, 16);
            this.lblInvoiceDate.TabIndex = 4;
            this.lblInvoiceDate.Text = "Ngày hóa đơn:";
            // 
            // lblDenominator
            // 
            this.lblDenominator.AutoSize = true;
            this.lblDenominator.Location = new System.Drawing.Point(643, 49);
            this.lblDenominator.Name = "lblDenominator";
            this.lblDenominator.Size = new System.Drawing.Size(58, 16);
            this.lblDenominator.TabIndex = 10;
            this.lblDenominator.Text = "Mẫu số: ";
            // 
            // lblStore
            // 
            this.lblStore.AutoSize = true;
            this.lblStore.Location = new System.Drawing.Point(316, 77);
            this.lblStore.Name = "lblStore";
            this.lblStore.Size = new System.Drawing.Size(61, 16);
            this.lblStore.TabIndex = 14;
            this.lblStore.Text = "Kho xuất:";
            // 
            // dtOutputDate
            // 
            this.dtOutputDate.CustomFormat = "dd/MM/yyyy";
            this.dtOutputDate.Enabled = false;
            this.dtOutputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtOutputDate.Location = new System.Drawing.Point(426, 18);
            this.dtOutputDate.Name = "dtOutputDate";
            this.dtOutputDate.Size = new System.Drawing.Size(210, 22);
            this.dtOutputDate.TabIndex = 3;
            // 
            // lblOutputDate
            // 
            this.lblOutputDate.AutoSize = true;
            this.lblOutputDate.Location = new System.Drawing.Point(316, 21);
            this.lblOutputDate.Name = "lblOutputDate";
            this.lblOutputDate.Size = new System.Drawing.Size(71, 16);
            this.lblOutputDate.TabIndex = 2;
            this.lblOutputDate.Text = "Ngày xuất:";
            // 
            // lblInVoiceID
            // 
            this.lblInVoiceID.AutoSize = true;
            this.lblInVoiceID.Location = new System.Drawing.Point(4, 49);
            this.lblInVoiceID.Name = "lblInVoiceID";
            this.lblInVoiceID.Size = new System.Drawing.Size(84, 16);
            this.lblInVoiceID.TabIndex = 6;
            this.lblInVoiceID.Text = "Hóa đơn số: ";
            // 
            // txtOutputVoucherID
            // 
            this.txtOutputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputVoucherID.Location = new System.Drawing.Point(100, 18);
            this.txtOutputVoucherID.Name = "txtOutputVoucherID";
            this.txtOutputVoucherID.ReadOnly = true;
            this.txtOutputVoucherID.Size = new System.Drawing.Size(210, 22);
            this.txtOutputVoucherID.TabIndex = 1;
            // 
            // lblOutputVoucherID
            // 
            this.lblOutputVoucherID.AutoSize = true;
            this.lblOutputVoucherID.Location = new System.Drawing.Point(4, 21);
            this.lblOutputVoucherID.Name = "lblOutputVoucherID";
            this.lblOutputVoucherID.Size = new System.Drawing.Size(93, 16);
            this.lblOutputVoucherID.TabIndex = 0;
            this.lblOutputVoucherID.Text = "Mã phiếu xuất:";
            // 
            // txtInVoiceID
            // 
            this.txtInVoiceID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInVoiceID.Location = new System.Drawing.Point(100, 46);
            this.txtInVoiceID.Name = "txtInVoiceID";
            this.txtInVoiceID.ReadOnly = true;
            this.txtInVoiceID.Size = new System.Drawing.Size(210, 22);
            this.txtInVoiceID.TabIndex = 7;
            // 
            // lblOrderID
            // 
            this.lblOrderID.AutoSize = true;
            this.lblOrderID.Location = new System.Drawing.Point(643, 77);
            this.lblOrderID.Name = "lblOrderID";
            this.lblOrderID.Size = new System.Drawing.Size(87, 16);
            this.lblOrderID.TabIndex = 16;
            this.lblOrderID.Text = "Yêu cầu xuất:";
            // 
            // txtInvoiceSymbol
            // 
            this.txtInvoiceSymbol.BackColor = System.Drawing.SystemColors.Info;
            this.txtInvoiceSymbol.Location = new System.Drawing.Point(426, 46);
            this.txtInvoiceSymbol.Name = "txtInvoiceSymbol";
            this.txtInvoiceSymbol.ReadOnly = true;
            this.txtInvoiceSymbol.Size = new System.Drawing.Size(210, 22);
            this.txtInvoiceSymbol.TabIndex = 9;
            // 
            // lblInvoiceSymbol
            // 
            this.lblInvoiceSymbol.AutoSize = true;
            this.lblInvoiceSymbol.Location = new System.Drawing.Point(316, 49);
            this.lblInvoiceSymbol.Name = "lblInvoiceSymbol";
            this.lblInvoiceSymbol.Size = new System.Drawing.Size(106, 16);
            this.lblInvoiceSymbol.TabIndex = 8;
            this.lblInvoiceSymbol.Text = "Ký hiệu hóa đơn:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblVoucherAlert);
            this.tabPage2.Controls.Add(this.grpVoucherDetail);
            this.tabPage2.Controls.Add(this.grpVoucher);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(963, 517);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Thông tin phiếu chi";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblVoucherAlert
            // 
            this.lblVoucherAlert.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblVoucherAlert.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVoucherAlert.ForeColor = System.Drawing.Color.Blue;
            this.lblVoucherAlert.Location = new System.Drawing.Point(7, 296);
            this.lblVoucherAlert.Name = "lblVoucherAlert";
            this.lblVoucherAlert.Size = new System.Drawing.Size(948, 199);
            this.lblVoucherAlert.TabIndex = 22;
            this.lblVoucherAlert.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpVoucherDetail
            // 
            this.grpVoucherDetail.Controls.Add(this.txtPaymentCardVoucherID);
            this.grpVoucherDetail.Controls.Add(this.label26);
            this.grpVoucherDetail.Controls.Add(this.cmdExchangeRate);
            this.grpVoucherDetail.Controls.Add(this.txtTotalPaymentCard);
            this.grpVoucherDetail.Controls.Add(this.txtPaymentCard);
            this.grpVoucherDetail.Controls.Add(this.txtCashUSDExchange);
            this.grpVoucherDetail.Controls.Add(this.txtDebt);
            this.grpVoucherDetail.Controls.Add(this.txtCashUSD);
            this.grpVoucherDetail.Controls.Add(this.txtCashVND);
            this.grpVoucherDetail.Controls.Add(this.cboPaymentCardID);
            this.grpVoucherDetail.Controls.Add(this.dtSettlementDate);
            this.grpVoucherDetail.Controls.Add(this.cboPaymentCurrencyUnitID);
            this.grpVoucherDetail.Controls.Add(this.label35);
            this.grpVoucherDetail.Controls.Add(this.label36);
            this.grpVoucherDetail.Controls.Add(this.label16);
            this.grpVoucherDetail.Controls.Add(this.label37);
            this.grpVoucherDetail.Controls.Add(this.label38);
            this.grpVoucherDetail.Controls.Add(this.label39);
            this.grpVoucherDetail.Controls.Add(this.label40);
            this.grpVoucherDetail.Controls.Add(this.label41);
            this.grpVoucherDetail.Controls.Add(this.label43);
            this.grpVoucherDetail.Location = new System.Drawing.Point(9, 143);
            this.grpVoucherDetail.Name = "grpVoucherDetail";
            this.grpVoucherDetail.Size = new System.Drawing.Size(951, 137);
            this.grpVoucherDetail.TabIndex = 2;
            this.grpVoucherDetail.TabStop = false;
            this.grpVoucherDetail.Text = "Thanh toán";
            // 
            // txtPaymentCardVoucherID
            // 
            this.txtPaymentCardVoucherID.BackColor = System.Drawing.SystemColors.Window;
            this.txtPaymentCardVoucherID.Location = new System.Drawing.Point(93, 107);
            this.txtPaymentCardVoucherID.MaxLength = 30;
            this.txtPaymentCardVoucherID.Name = "txtPaymentCardVoucherID";
            this.txtPaymentCardVoucherID.ReadOnly = true;
            this.txtPaymentCardVoucherID.Size = new System.Drawing.Size(187, 22);
            this.txtPaymentCardVoucherID.TabIndex = 41;
            this.txtPaymentCardVoucherID.TabStop = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(2, 110);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 16);
            this.label26.TabIndex = 39;
            this.label26.Text = "Số HĐ cà thẻ:";
            // 
            // cmdExchangeRate
            // 
            this.cmdExchangeRate.Location = new System.Drawing.Point(284, 51);
            this.cmdExchangeRate.Name = "cmdExchangeRate";
            this.cmdExchangeRate.Size = new System.Drawing.Size(24, 22);
            this.cmdExchangeRate.TabIndex = 38;
            this.cmdExchangeRate.Text = "?";
            // 
            // txtTotalPaymentCard
            // 
            this.txtTotalPaymentCard.EditValue = "0";
            this.txtTotalPaymentCard.Location = new System.Drawing.Point(741, 79);
            this.txtTotalPaymentCard.Name = "txtTotalPaymentCard";
            this.txtTotalPaymentCard.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtTotalPaymentCard.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaymentCard.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.txtTotalPaymentCard.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalPaymentCard.Properties.Appearance.Options.UseFont = true;
            this.txtTotalPaymentCard.Properties.Appearance.Options.UseForeColor = true;
            this.txtTotalPaymentCard.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalPaymentCard.Properties.EditFormat.FormatString = "n2";
            this.txtTotalPaymentCard.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalPaymentCard.Properties.Mask.EditMask = "N0";
            this.txtTotalPaymentCard.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalPaymentCard.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalPaymentCard.Properties.ReadOnly = true;
            this.txtTotalPaymentCard.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTotalPaymentCard.Size = new System.Drawing.Size(192, 22);
            this.txtTotalPaymentCard.TabIndex = 40;
            this.txtTotalPaymentCard.EditValueChanged += new System.EventHandler(this.txtCashVND_EditValueChanged);
            // 
            // txtPaymentCard
            // 
            this.txtPaymentCard.EditValue = "0";
            this.txtPaymentCard.Location = new System.Drawing.Point(93, 79);
            this.txtPaymentCard.Name = "txtPaymentCard";
            this.txtPaymentCard.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentCard.Properties.Appearance.Options.UseFont = true;
            this.txtPaymentCard.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPaymentCard.Properties.EditFormat.FormatString = "n2";
            this.txtPaymentCard.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPaymentCard.Properties.Mask.EditMask = "N0";
            this.txtPaymentCard.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPaymentCard.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPaymentCard.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPaymentCard.Size = new System.Drawing.Size(187, 22);
            this.txtPaymentCard.TabIndex = 33;
            this.txtPaymentCard.EditValueChanged += new System.EventHandler(this.txtCashVND_EditValueChanged);
            // 
            // txtCashUSDExchange
            // 
            this.txtCashUSDExchange.EditValue = "0";
            this.txtCashUSDExchange.Location = new System.Drawing.Point(741, 51);
            this.txtCashUSDExchange.Name = "txtCashUSDExchange";
            this.txtCashUSDExchange.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtCashUSDExchange.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashUSDExchange.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.txtCashUSDExchange.Properties.Appearance.Options.UseBackColor = true;
            this.txtCashUSDExchange.Properties.Appearance.Options.UseFont = true;
            this.txtCashUSDExchange.Properties.Appearance.Options.UseForeColor = true;
            this.txtCashUSDExchange.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCashUSDExchange.Properties.EditFormat.FormatString = "n2";
            this.txtCashUSDExchange.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCashUSDExchange.Properties.Mask.EditMask = "N0";
            this.txtCashUSDExchange.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCashUSDExchange.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCashUSDExchange.Properties.ReadOnly = true;
            this.txtCashUSDExchange.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCashUSDExchange.Size = new System.Drawing.Size(192, 22);
            this.txtCashUSDExchange.TabIndex = 31;
            // 
            // txtDebt
            // 
            this.txtDebt.EditValue = "0";
            this.txtDebt.Location = new System.Drawing.Point(434, 19);
            this.txtDebt.Name = "txtDebt";
            this.txtDebt.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtDebt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDebt.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.txtDebt.Properties.Appearance.Options.UseBackColor = true;
            this.txtDebt.Properties.Appearance.Options.UseFont = true;
            this.txtDebt.Properties.Appearance.Options.UseForeColor = true;
            this.txtDebt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDebt.Properties.Mask.EditMask = "N0";
            this.txtDebt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDebt.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDebt.Properties.NullText = "0";
            this.txtDebt.Properties.ReadOnly = true;
            this.txtDebt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDebt.Size = new System.Drawing.Size(194, 22);
            this.txtDebt.TabIndex = 22;
            // 
            // txtCashUSD
            // 
            this.txtCashUSD.EditValue = "0";
            this.txtCashUSD.Location = new System.Drawing.Point(93, 51);
            this.txtCashUSD.Name = "txtCashUSD";
            this.txtCashUSD.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtCashUSD.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashUSD.Properties.Appearance.Options.UseBackColor = true;
            this.txtCashUSD.Properties.Appearance.Options.UseFont = true;
            this.txtCashUSD.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCashUSD.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCashUSD.Properties.Mask.BeepOnError = true;
            this.txtCashUSD.Properties.Mask.EditMask = "N0";
            this.txtCashUSD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCashUSD.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCashUSD.Properties.ReadOnly = true;
            this.txtCashUSD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCashUSD.Size = new System.Drawing.Size(187, 22);
            this.txtCashUSD.TabIndex = 26;
            this.txtCashUSD.EditValueChanged += new System.EventHandler(this.txtCashVND_EditValueChanged);
            // 
            // txtCashVND
            // 
            this.txtCashVND.Location = new System.Drawing.Point(93, 23);
            this.txtCashVND.Name = "txtCashVND";
            this.txtCashVND.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtCashVND.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashVND.Properties.Appearance.Options.UseBackColor = true;
            this.txtCashVND.Properties.Appearance.Options.UseFont = true;
            this.txtCashVND.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCashVND.Properties.Mask.EditMask = "N0";
            this.txtCashVND.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCashVND.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCashVND.Properties.NullText = "0";
            this.txtCashVND.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCashVND.Size = new System.Drawing.Size(187, 22);
            this.txtCashVND.TabIndex = 20;
            this.txtCashVND.EditValueChanged += new System.EventHandler(this.txtCashVND_EditValueChanged);
            // 
            // cboPaymentCardID
            // 
            this.cboPaymentCardID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPaymentCardID.DropDownWidth = 250;
            this.cboPaymentCardID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPaymentCardID.Location = new System.Drawing.Point(434, 76);
            this.cboPaymentCardID.Name = "cboPaymentCardID";
            this.cboPaymentCardID.Size = new System.Drawing.Size(192, 24);
            this.cboPaymentCardID.TabIndex = 35;
            this.cboPaymentCardID.SelectionChangeCommitted += new System.EventHandler(this.cboPaymentCardID_SelectionChangeCommitted);
            // 
            // dtSettlementDate
            // 
            this.dtSettlementDate.CustomFormat = "dd/MM/yyyy";
            this.dtSettlementDate.Enabled = false;
            this.dtSettlementDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtSettlementDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtSettlementDate.Location = new System.Drawing.Point(741, 21);
            this.dtSettlementDate.Name = "dtSettlementDate";
            this.dtSettlementDate.Size = new System.Drawing.Size(192, 22);
            this.dtSettlementDate.TabIndex = 24;
            // 
            // cboPaymentCurrencyUnitID
            // 
            this.cboPaymentCurrencyUnitID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPaymentCurrencyUnitID.DropDownWidth = 250;
            this.cboPaymentCurrencyUnitID.Enabled = false;
            this.cboPaymentCurrencyUnitID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPaymentCurrencyUnitID.Location = new System.Drawing.Point(432, 48);
            this.cboPaymentCurrencyUnitID.Name = "cboPaymentCurrencyUnitID";
            this.cboPaymentCurrencyUnitID.Size = new System.Drawing.Size(194, 24);
            this.cboPaymentCurrencyUnitID.TabIndex = 29;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(634, 21);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(78, 16);
            this.label35.TabIndex = 23;
            this.label35.Text = "Thanh toán:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(314, 22);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(81, 16);
            this.label36.TabIndex = 21;
            this.label36.Text = "Còn nợ lại:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(634, 82);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 16);
            this.label16.TabIndex = 32;
            this.label16.Text = "Tiền cà thẻ:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(2, 25);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(94, 16);
            this.label37.TabIndex = 19;
            this.label37.Text = "Tiền mặt VNĐ:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(2, 82);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(85, 16);
            this.label38.TabIndex = 32;
            this.label38.Text = "Tiền qua thẻ:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(314, 80);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 16);
            this.label39.TabIndex = 34;
            this.label39.Text = "Loại thẻ:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(314, 53);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(93, 16);
            this.label40.TabIndex = 28;
            this.label40.Text = "Loại tiền khác:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(2, 53);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(70, 16);
            this.label41.TabIndex = 25;
            this.label41.Text = "Tiền khác:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(634, 53);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(88, 16);
            this.label43.TabIndex = 30;
            this.label43.Text = "Quy đổi VNĐ:";
            // 
            // grpVoucher
            // 
            this.grpVoucher.Controls.Add(this.txtVoucherDiscount);
            this.grpVoucher.Controls.Add(this.txtTotalOrderPaid);
            this.grpVoucher.Controls.Add(this.label14);
            this.grpVoucher.Controls.Add(this.txtExchangeRate);
            this.grpVoucher.Controls.Add(this.txtTotalLiquidate);
            this.grpVoucher.Controls.Add(this.txtTotalMoney);
            this.grpVoucher.Controls.Add(this.label27);
            this.grpVoucher.Controls.Add(this.cboCurrencyUnitID2);
            this.grpVoucher.Controls.Add(this.label28);
            this.grpVoucher.Controls.Add(this.label29);
            this.grpVoucher.Controls.Add(this.label30);
            this.grpVoucher.Controls.Add(this.txtContent);
            this.grpVoucher.Controls.Add(this.label31);
            this.grpVoucher.Controls.Add(this.cboVoucherTypeID);
            this.grpVoucher.Controls.Add(this.label33);
            this.grpVoucher.Controls.Add(this.txtVoucherID);
            this.grpVoucher.Controls.Add(this.label23);
            this.grpVoucher.Location = new System.Drawing.Point(6, 6);
            this.grpVoucher.Name = "grpVoucher";
            this.grpVoucher.Size = new System.Drawing.Size(951, 131);
            this.grpVoucher.TabIndex = 1;
            this.grpVoucher.TabStop = false;
            // 
            // txtVoucherDiscount
            // 
            this.txtVoucherDiscount.BackColor = System.Drawing.SystemColors.Info;
            this.txtVoucherDiscount.CustomFormat = "###,###,##0";
            this.txtVoucherDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtVoucherDiscount.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtVoucherDiscount.Location = new System.Drawing.Point(432, 65);
            this.txtVoucherDiscount.Name = "txtVoucherDiscount";
            this.txtVoucherDiscount.Size = new System.Drawing.Size(196, 24);
            this.txtVoucherDiscount.TabIndex = 11;
            this.txtVoucherDiscount.Tag = null;
            this.txtVoucherDiscount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtVoucherDiscount.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // txtTotalOrderPaid
            // 
            this.txtTotalOrderPaid.Location = new System.Drawing.Point(156, 95);
            this.txtTotalOrderPaid.Name = "txtTotalOrderPaid";
            this.txtTotalOrderPaid.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalOrderPaid.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalOrderPaid.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalOrderPaid.Properties.Appearance.Options.UseFont = true;
            this.txtTotalOrderPaid.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalOrderPaid.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTotalOrderPaid.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalOrderPaid.Properties.Mask.EditMask = "N0";
            this.txtTotalOrderPaid.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalOrderPaid.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalOrderPaid.Properties.NullText = "0";
            this.txtTotalOrderPaid.Properties.ReadOnly = true;
            this.txtTotalOrderPaid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTotalOrderPaid.Size = new System.Drawing.Size(183, 22);
            this.txtTotalOrderPaid.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 96);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(138, 16);
            this.label14.TabIndex = 15;
            this.label14.Text = "Đã thu từ yêu cầu xuất:";
            // 
            // txtExchangeRate
            // 
            this.txtExchangeRate.Location = new System.Drawing.Point(829, 14);
            this.txtExchangeRate.Name = "txtExchangeRate";
            this.txtExchangeRate.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtExchangeRate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeRate.Properties.Appearance.Options.UseBackColor = true;
            this.txtExchangeRate.Properties.Appearance.Options.UseFont = true;
            this.txtExchangeRate.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.Info;
            this.txtExchangeRate.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtExchangeRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExchangeRate.Properties.Mask.EditMask = "N2";
            this.txtExchangeRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtExchangeRate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtExchangeRate.Properties.ReadOnly = true;
            this.txtExchangeRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtExchangeRate.Size = new System.Drawing.Size(104, 22);
            this.txtExchangeRate.TabIndex = 6;
            // 
            // txtTotalLiquidate
            // 
            this.txtTotalLiquidate.EditValue = "0";
            this.txtTotalLiquidate.Location = new System.Drawing.Point(741, 64);
            this.txtTotalLiquidate.Name = "txtTotalLiquidate";
            this.txtTotalLiquidate.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtTotalLiquidate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalLiquidate.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.txtTotalLiquidate.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalLiquidate.Properties.Appearance.Options.UseFont = true;
            this.txtTotalLiquidate.Properties.Appearance.Options.UseForeColor = true;
            this.txtTotalLiquidate.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.Red;
            this.txtTotalLiquidate.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtTotalLiquidate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalLiquidate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalLiquidate.Properties.Mask.EditMask = "N0";
            this.txtTotalLiquidate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalLiquidate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalLiquidate.Properties.NullText = "0";
            this.txtTotalLiquidate.Properties.ReadOnly = true;
            this.txtTotalLiquidate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTotalLiquidate.Size = new System.Drawing.Size(192, 22);
            this.txtTotalLiquidate.TabIndex = 14;
            // 
            // txtTotalMoney
            // 
            this.txtTotalMoney.Location = new System.Drawing.Point(97, 67);
            this.txtTotalMoney.Name = "txtTotalMoney";
            this.txtTotalMoney.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalMoney.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMoney.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalMoney.Properties.Appearance.Options.UseFont = true;
            this.txtTotalMoney.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMoney.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTotalMoney.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalMoney.Properties.Mask.EditMask = "N0";
            this.txtTotalMoney.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalMoney.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalMoney.Properties.NullText = "0";
            this.txtTotalMoney.Properties.ReadOnly = true;
            this.txtTotalMoney.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTotalMoney.Size = new System.Drawing.Size(183, 22);
            this.txtTotalMoney.TabIndex = 10;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(634, 66);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 16);
            this.label27.TabIndex = 13;
            this.label27.Text = "Phải T. Toán:";
            // 
            // cboCurrencyUnitID2
            // 
            this.cboCurrencyUnitID2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyUnitID2.Enabled = false;
            this.cboCurrencyUnitID2.Location = new System.Drawing.Point(741, 13);
            this.cboCurrencyUnitID2.Name = "cboCurrencyUnitID2";
            this.cboCurrencyUnitID2.Size = new System.Drawing.Size(82, 24);
            this.cboCurrencyUnitID2.TabIndex = 5;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(634, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(61, 16);
            this.label28.TabIndex = 4;
            this.label28.Text = "Loại tiền:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(314, 67);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 16);
            this.label29.TabIndex = 11;
            this.label29.Text = "Giảm giá:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 70);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(73, 16);
            this.label30.TabIndex = 9;
            this.label30.Text = "Thành tiền:";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(97, 41);
            this.txtContent.MaxLength = 1000;
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(836, 22);
            this.txtContent.TabIndex = 8;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 43);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(65, 16);
            this.label31.TabIndex = 7;
            this.label31.Text = "Nội dung:";
            // 
            // cboVoucherTypeID
            // 
            this.cboVoucherTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVoucherTypeID.DropDownWidth = 250;
            this.cboVoucherTypeID.Enabled = false;
            this.cboVoucherTypeID.Location = new System.Drawing.Point(432, 13);
            this.cboVoucherTypeID.Name = "cboVoucherTypeID";
            this.cboVoucherTypeID.Size = new System.Drawing.Size(194, 24);
            this.cboVoucherTypeID.TabIndex = 3;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(314, 17);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(65, 16);
            this.label33.TabIndex = 2;
            this.label33.Text = "Hình thức:";
            // 
            // txtVoucherID
            // 
            this.txtVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtVoucherID.Location = new System.Drawing.Point(97, 14);
            this.txtVoucherID.Name = "txtVoucherID";
            this.txtVoucherID.ReadOnly = true;
            this.txtVoucherID.Size = new System.Drawing.Size(183, 22);
            this.txtVoucherID.TabIndex = 1;
            this.txtVoucherID.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 17);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(66, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "Mã phiếu:";
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cmdUpdate.Enabled = false;
            this.cmdUpdate.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdate.Image")));
            this.cmdUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdUpdate.Location = new System.Drawing.Point(784, 550);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Size = new System.Drawing.Size(88, 25);
            this.cmdUpdate.TabIndex = 1;
            this.cmdUpdate.Text = "   Cập nhật";
            this.cmdUpdate.UseVisualStyleBackColor = true;
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // cmdClose
            // 
            this.cmdClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cmdClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdClose.Image")));
            this.cmdClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClose.Location = new System.Drawing.Point(874, 550);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(88, 25);
            this.cmdClose.TabIndex = 2;
            this.cmdClose.Text = "   Đóng lại";
            this.cmdClose.UseVisualStyleBackColor = true;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(2, 553);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 16);
            this.label12.TabIndex = 26;
            this.label12.Text = "Tổng SL trả:";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(234, 554);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 16);
            this.label13.TabIndex = 28;
            this.label13.Text = "Tổng tiền trả:";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtTotalAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalAmount.Location = new System.Drawing.Point(330, 551);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(158, 22);
            this.txtTotalAmount.TabIndex = 29;
            // 
            // txtTotalQuantity
            // 
            this.txtTotalQuantity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtTotalQuantity.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalQuantity.Location = new System.Drawing.Point(96, 550);
            this.txtTotalQuantity.Name = "txtTotalQuantity";
            this.txtTotalQuantity.ReadOnly = true;
            this.txtTotalQuantity.Size = new System.Drawing.Size(123, 22);
            this.txtTotalQuantity.TabIndex = 27;
            // 
            // frmInputVoucherReturn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 578);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtTotalAmount);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtTotalQuantity);
            this.Controls.Add(this.cmdClose);
            this.Controls.Add(this.cmdUpdate);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmInputVoucherReturn";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập trả hàng";
            this.Load += new System.EventHandler(this.frmInputVoucherReturn_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexOutputVoucherDetail)).EndInit();
            this.mnuFlexPopup.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.grpVoucherDetail.ResumeLayout(false);
            this.grpVoucherDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPaymentCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashUSDExchange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashUSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashVND.Properties)).EndInit();
            this.grpVoucher.ResumeLayout(false);
            this.grpVoucher.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoucherDiscount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalOrderPaid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLiquidate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalMoney.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtOutputUser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDenominator;
        private System.Windows.Forms.DateTimePicker dtInvoiceDate;
        private System.Windows.Forms.Label lblInvoiceDate;
        private System.Windows.Forms.Label lblDenominator;
        private System.Windows.Forms.Label lblStore;
        private System.Windows.Forms.DateTimePicker dtOutputDate;
        private System.Windows.Forms.Label lblOutputDate;
        private System.Windows.Forms.Label lblInVoiceID;
        private System.Windows.Forms.TextBox txtOutputVoucherID;
        private System.Windows.Forms.Label lblOutputVoucherID;
        private System.Windows.Forms.TextBox txtInVoiceID;
        private System.Windows.Forms.Label lblOrderID;
        private System.Windows.Forms.TextBox txtInvoiceSymbol;
        private System.Windows.Forms.Label lblInvoiceSymbol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCustomerTaxID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCustomerPhone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCustomerAddress;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtPayableTime;
        private System.Windows.Forms.ComboBox cboPayableTypeID;
        private System.Windows.Forms.Label lblPayableTime;
        private System.Windows.Forms.Label lblPayableType;
        private C1.Win.C1Input.C1NumericEdit txtDiscount;
        private System.Windows.Forms.ComboBox cboDiscountReasonID;
        private System.Windows.Forms.Label lblDeCount;
        private System.Windows.Forms.ComboBox cboCurrencyUnitID;
        private System.Windows.Forms.Label lblCurrencyUnit;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkLostVATInvoice;
        private System.Windows.Forms.CheckBox chkIsOld;
        private System.Windows.Forms.CheckBox chkIsNew;
        private System.Windows.Forms.TextBox txtReturnNote;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtReturnReason;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboInputTypeID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOutputContent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpVoucher;
        private DevExpress.XtraEditors.TextEdit txtExchangeRate;
        private DevExpress.XtraEditors.TextEdit txtTotalLiquidate;
        private DevExpress.XtraEditors.TextEdit txtTotalMoney;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cboCurrencyUnitID2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cboVoucherTypeID;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtVoucherID;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox grpVoucherDetail;
        private DevExpress.XtraEditors.TextEdit txtPaymentCard;
        private DevExpress.XtraEditors.TextEdit txtCashUSDExchange;
        private DevExpress.XtraEditors.TextEdit txtDebt;
        private DevExpress.XtraEditors.TextEdit txtCashUSD;
        private DevExpress.XtraEditors.TextEdit txtCashVND;
        private System.Windows.Forms.ComboBox cboPaymentCardID;
        private System.Windows.Forms.DateTimePicker dtSettlementDate;
        private System.Windows.Forms.ComboBox cboPaymentCurrencyUnitID;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label43;
        private C1.Win.C1FlexGrid.C1FlexGrid flexOutputVoucherDetail;
        private System.Windows.Forms.Button cmdUpdate;
        private System.Windows.Forms.Button cmdClose;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ctrlStoreManagerUser;
        private System.Windows.Forms.Button cmdExchangeRate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTotalAmount;
        private System.Windows.Forms.TextBox txtTotalQuantity;
        private System.Windows.Forms.TextBox txtPaymentCardVoucherID;
        private System.Windows.Forms.Label label26;
        private DevExpress.XtraEditors.TextEdit txtTotalOrderPaid;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.LinkLabel lblCustomer;
        private System.Windows.Forms.TextBox txtCustomerID;
        private DevExpress.XtraEditors.TextEdit txtTotalPaymentCard;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ContextMenuStrip mnuFlexPopup;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAdjustPrice;
        private C1.Win.C1Input.C1NumericEdit txtVoucherDiscount;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboInputStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboOutputStoreID;
        private System.Windows.Forms.Label lblVoucherAlert;
    }
}