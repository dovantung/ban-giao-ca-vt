﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using Library.AppCore;
using Library.AppCore.Forms;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Input
{
    public partial class frmRealInputConfirm : Form
    {
        private DataTable dtbData;
        List<ProductRealInput> lstProduct = new List<ProductRealInput>();
        int intCheckDuplicateImeiAllProductValue = Library.AppCore.AppConfig.GetIntConfigValue("ISCHECKDUPLICATEIMEIALLPRODUCT");
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objInputVoucherDetailIMEIListOld = null;
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = null;
        private List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> objAllIMEIList = null;
        private bool bolIsInputFromRetailInputPrice = false;
        //Nguyễn Văn Tài - chỉnh sửa bổ sung
        private Regex regex = new Regex(@"^\d$");
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> AllIMEIList
        {
            get { return objAllIMEIList; }
            set { objAllIMEIList = value; }
        }

        public string Note { get; set; }
        /// <summary>
        /// Nếu nhập từ định giá máy cũ
        /// </summary>
        public bool IsInputFromRetailInputPrice
        {
            get { return bolIsInputFromRetailInputPrice; }
            set { bolIsInputFromRetailInputPrice = value; }
        }
        /// <summary>
        /// Danh sách IMEI nhập
        /// </summary>
        public List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> InputVoucherDetailIMEIList
        {
            get { return objInputVoucherDetailIMEIList; }
            set { objInputVoucherDetailIMEIList = value; }
        }
        private List<PLC.Input.WSInputVoucher.InputVoucherDetail> objInputVoucherDetailList = null;
        public List<PLC.Input.WSInputVoucher.InputVoucherDetail> InputVoucherDetailList
        {
            set { objInputVoucherDetailList = value; }
            get { return objInputVoucherDetailList; }
        }

        public decimal OrderQuantity { get; set; }

        public frmRealInputConfirm()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }

        public frmRealInputConfirm(List<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail> dtbData)
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            this.dtbData = ConvertToDataTable(dtbData);
            this.dtbData.Columns.Add("IsError", typeof(Boolean));
            this.dtbData.Columns.Add("RealQuantity", typeof(Decimal));
            this.dtbData.AsEnumerable().All(c => { c["RealQuantity"] = 0; return true; });
            grdData.DataSource = this.dtbData;
        }

        private void UpdateQuantity(DataTable dtbData)
        {
            for (int i = 0; i < dtbData.Rows.Count; i++)
            {
                dtbData.Rows[i]["QuantityToInput"] = int.Parse(dtbData.Rows[i]["QuantityToInput"].ToString()) + int.Parse(dtbData.Rows[i]["Quantity"].ToString());
                grdViewData.RefreshData();
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                var lstData = (grdData.DataSource as DataTable).AsEnumerable();
                DataTable dtbProductId = ConvertColumnsAsRows(ConvertListToDatatable(lstData.Select(x => x["ProductID"].ToString()).ToList()));
                DataTable dtbProductName = ConvertColumnsAsRows(ConvertListToDatatable(lstData.Select(x => x["ProductName"].ToString()).ToList()));
                DataTable dtbProductQuantity = ConvertColumnsAsRowsQuantity(ConvertListToDatatable(lstData.Select(x => x["Quantity"].ToString()).ToList()));
                dtbProductId.Merge(dtbProductName);
                dtbProductId.Merge(dtbProductQuantity);
                dtbProductId.Columns.RemoveAt(0);
                for (int i = 0; i < dtbProductId.Columns.Count; i++)
                {
                    dtbProductId.Columns[i].Caption = dtbProductId.Rows[0][i].ToString();
                }
                Library.AppCore.LoadControls.C1FlexGridObject.ExportDataTableToExcel(this, dtbProductId, C1.Win.C1FlexGrid.FileFlags.None, true);
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Không thể xuất file excel mẫu!", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Không thể xuất file excel mẫu!");
                return;
            }
        }

        public DataTable ConvertListToDatatable(List<string> lstData)
        {
            DataTable dtbData = new DataTable();
            DataColumn col = new DataColumn();
            dtbData.Columns.Add(col);
            foreach (var item in lstData)
            {
                dtbData.Rows.Add(item);
            }
            return dtbData;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
        private DataTable ConvertColumnsAsRows(DataTable dt)
        {
            DataTable dtnew = new DataTable();
            //Convert all the rows to columns 
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtnew.Columns.Add(Convert.ToString(i));
            }
            DataRow dr;
            // Convert All the Columns to Rows 
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dr = dtnew.NewRow();
                dr[0] = dt.Columns[j].ToString();
                for (int k = 1; k <= dt.Rows.Count; k++)
                    dr[k] = dt.Rows[k - 1][j];
                dtnew.Rows.Add(dr);
            }
            return dtnew;
        }


        //Nối chuỗi cho số lượng
        private DataTable ConvertColumnsAsRowsQuantity(DataTable dt)
        {
            DataTable dtnew = new DataTable();
            //Convert all the rows to columns 
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtnew.Columns.Add(Convert.ToString(i));
            }
            DataRow dr;
            // Convert All the Columns to Rows 
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dr = dtnew.NewRow();
                dr[0] = dt.Columns[j].ToString();
                for (int k = 1; k <= dt.Rows.Count; k++)
                    dr[k] = "(Số lượng cần nhập: " + dt.Rows[k - 1][j] + ")";
                dtnew.Rows.Add(dr);
            }
            return dtnew;
        }
        private void ValidateData(List<ImeiRealInput> lstDataImei, List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI> lstInputVoucherDetailImei)
        {
            Thread objThread = new Thread(new ThreadStart(Excute));
            objThread.Start();


            frmWaitDialog.Close();
            Thread.Sleep(0);
            objThread.Abort();
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang kiểm tra IMEI");
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            DataTable dtbGrid = grdData.DataSource as DataTable;

            if (lstProduct != null && lstProduct.Count > 0)
            {
                if (MessageBox.Show("Toàn bộ IMEI sẽ bị xóa. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                else
                {
                    lstProduct.Clear();
                    foreach (DataRow row in dtbGrid.Rows)
                    {
                        row["RealQuantity"] = 0;
                        row["Status"] = string.Empty;
                        row["IsError"] = false;
                    }
                }
            }

            DataTable dtbImeiImport = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(grdViewData.DataRowCount);
            if (dtbImeiImport == null) return;
            if (dtbImeiImport.Columns.Count == 0 || dtbImeiImport.Rows.Count < 1) return;
           
            if (dtbImeiImport.Columns.Count != grdViewData.DataRowCount)
            {
                MessageBoxObject.ShowWarningMessage(this, "File Excel không đúng mẫu!");
                return;
            }

            if (!dtbGrid.Columns.Contains("Status"))
            {
                DataColumn colStatus = new DataColumn();
                colStatus.ColumnName = "Status";
                dtbGrid.Columns.Add(colStatus);
            }
            if (!dtbGrid.Columns.Contains("Note"))
            {
                DataColumn colStatus = new DataColumn();
                colStatus.ColumnName = "Note";
                dtbGrid.Columns.Add(colStatus);
            }

            for (int i = 0; i < dtbImeiImport.Columns.Count; i++)
            {
                DataColumn col = dtbImeiImport.Columns[i];
                //Kiểm tra template
                if (dtbGrid.Select("[ProductID] = '" + dtbImeiImport.Rows[0][i].ToString().Trim() + "'").Length == 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "File Excel không đúng mẫu!");
                    return;
                }
            }
            foreach (DataColumn r in dtbImeiImport.Columns)
            {
                if (!string.IsNullOrEmpty(dtbImeiImport.Rows[0][r].ToString()))
                    r.ColumnName = dtbImeiImport.Rows[0][r].ToString();
            }

            if (lstProduct == null)
                lstProduct = new List<ProductRealInput>();

            string strErrorId = string.Empty;
            dtbImeiImport.Rows.RemoveAt(0);
            dtbImeiImport.Rows.RemoveAt(0);
            //Hiếu xóa dòng số lượng
            dtbImeiImport.Rows.RemoveAt(0);
            if (dtbImeiImport.Rows.Count > 0)
            {
                foreach (DataRow rowGird in dtbGrid.Rows)
                {
                    //Xử lý dữ liệu
                    bool bolIsRequireImei = Convert.ToBoolean(rowGird["IsRequestIMEI"].ToString());
                    string strProductID = rowGird["ProductID"].ToString().Trim();
                    string strProductName = rowGird["ProductName"].ToString().Trim();

                    for (int j = 0; j < dtbImeiImport.Columns.Count; j++)
                    {
                        if (dtbImeiImport.AsEnumerable().Where(x => !string.IsNullOrEmpty(x[j].ToString())).Count() == 0)
                        {
                            //MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập dữ liệu vào file import");
                            strErrorId += dtbImeiImport.Rows[0][j].ToString() + "\r\n";
                            continue;
                        }
                        string strProductIDImport = dtbImeiImport.Columns[j].ColumnName.ToString();
                        if (!string.IsNullOrEmpty(strProductIDImport) && strProductIDImport == strProductID)
                        {
                            ProductRealInput objProduct = new ProductRealInput();
                            objProduct.ProductID = strProductID;
                            objProduct.ProductName = strProductName;
                            objProduct.IsRequestImei = bolIsRequireImei;
                            if (!bolIsRequireImei) // Không yêu cầu nhập Imei
                            {
                                int intQuantity = 0;
                                if (dtbImeiImport.Rows.Count > 0)
                                    int.TryParse(dtbImeiImport.Rows[0][j].ToString(), out intQuantity);
                                objProduct.Quantity = intQuantity;
                                objProduct.lstImei = new List<ImeiRealInput>();
                            }
                            else // Sản phẩm yêu cầu nhập IMEI
                            {
                                List<ImeiRealInput> ImeiList = new List<ImeiRealInput>();
                                objProduct.IsRequestImei = true;
                                objProduct.Quantity = 0;
                                for (int iRow = 0; iRow < dtbImeiImport.Rows.Count; iRow++)
                                {
                                    string strImei = dtbImeiImport.Rows[iRow][j].ToString();
                                    if (!string.IsNullOrEmpty(strImei))
                                    {
                                        ImeiRealInput objProductImei = new ImeiRealInput();
                                        objProductImei.RealImei = strImei.Trim().ToUpper();
                                        objProductImei.ProductID = objProduct.ProductID;
                                        objProductImei.ProductName = objProduct.ProductName;
                                        ImeiList.Add(objProductImei);
                                    }
                                }
                                objProduct.lstImei = ImeiList;
                            }
                            lstProduct.Add(objProduct);
                        }
                    }
                }
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập dữ liệu cho file Excel!");
                return;
            }
            //for (int i = 0; i < dtbImeiImport.Columns.Count; i++)
            //{
            //    //DataColumn col = dtbImeiImport.Columns[i];
            //    ////Kiểm tra template
            //    //if (dtbGrid.Select("[ProductID] = '" + dtbImeiImport.Rows[0][i].ToString().Trim() + "'").Length == 0)
            //    //{
            //    //    MessageBoxObject.ShowWarningMessage(this, "File excel không đúng mẫu!");
            //    //    return;
            //    //}
            //    //End kiểm tra template

            //    if (dtbImeiImport.AsEnumerable().Where(x => !string.IsNullOrEmpty(x[i].ToString())).Count() <= 2)
            //    {
            //        //MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập dữ liệu vào file import");
            //        strErrorId += dtbImeiImport.Rows[0][i].ToString() + "\r\n";
            //        continue;
            //    }

            //    //Xử lý dữ liệu
            //    ProductRealInput objProduct = new ProductRealInput();
            //    objProduct.ProductID = dtbImeiImport.Rows[0][dtbGrid.Rows[i]["ProductID"].ToString().Trim()].ToString();
            //    objProduct.ProductName = dtbImeiImport.Rows[1][dtbGrid.Rows[i]["ProductID"].ToString().Trim()].ToString();
            //    objProduct.IsRequestImei = false;
            //    if (dtbGrid.Rows[i]["IsRequestIMEI"].ToString().ToLower() == "false")
            //    {
            //        int intQuantity = 0;
            //        if (dtbImeiImport.Rows.Count > 2)
            //            int.TryParse(dtbImeiImport.Rows[2][dtbGrid.Rows[i]["ProductID"].ToString().Trim()].ToString(), out intQuantity);
            //        objProduct.Quantity = intQuantity;
            //    }
            //    List<ImeiRealInput> ImeiList = new List<ImeiRealInput>();
            //    if (dtbGrid.Rows[i]["IsRequestIMEI"].ToString().ToLower() == "true")
            //    {
            //        objProduct.IsRequestImei = true;
            //        objProduct.Quantity = 0;
            //        for (int j = 2; j < dtbImeiImport.Rows.Count; j++)
            //        {
            //            if (!string.IsNullOrEmpty(dtbImeiImport.Rows[j][dtbGrid.Rows[i]["ProductID"].ToString().Trim()].ToString()))
            //            {
            //                ImeiRealInput objProductImei = new ImeiRealInput();
            //                objProductImei.RealImei = dtbImeiImport.Rows[j][dtbGrid.Rows[i]["ProductID"].ToString().Trim()].ToString();
            //                objProductImei.ProductID = objProduct.ProductID;
            //                objProductImei.ProductName = objProduct.ProductName;
            //                ImeiList.Add(objProductImei);
            //            }
            //        }
            //    }
            //    objProduct.lstImei = ImeiList;
            //    if (ImeiList.Count > 0)
            //    {
            //        ImportData(objProduct, objProduct.lstImei);
            //    }
            //    lstProduct.Add(objProduct);
            //    //End
            //}

            if (!string.IsNullOrEmpty(strErrorId))
            {
                MessageBoxObject.ShowWarningMessage(this, "Danh sách sản phẩm không hợp lệ:", strErrorId);
                return;
            }

            foreach (ProductRealInput item in lstProduct)
            {
                if (item.IsRequestImei)
                {
                    List<string> lstImeiList = InputVoucherDetailList.Where(x => x.ProductID.Trim() == item.ProductID.Trim()).First().InputVoucherDetailIMEIList.Select(x => x.IMEI.Trim()).ToList();
                    List<string> lstImeiImport = item.lstImei.Select(x => x.RealImei).ToList();
                    var intDuplicateItems = lstImeiImport.GroupBy(x => x).Where(x => x.Count() > 1).Select(x => x.Key);
                    if (intDuplicateItems.Count() > 0)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Tồn tại IMEI trùng trên sản phẩm " + item.ProductName);
                        grdData.DataSource = this.dtbData;
                        lstProduct.Clear();
                        return;
                    }
                    List<string> lstExist = lstImeiImport.Join(
                                                                lstImeiList,
                                                                o => o,
                                                                id => id,
                                                                (o, id) => o).ToList();
                    List<string> lstNonExistImei = lstImeiList.Except(lstImeiImport).ToList();
                    var lstUpdate = item.lstImei.ToList().Join(lstExist, o => o.RealImei, i => i, (o, i) => o).ToList();
                    lstUpdate.All(c => { c.InvoiceImei = c.RealImei; return true; });
                    List<ImeiRealInput> lstImeiRealInput = new List<ImeiRealInput>();
                    lstImeiRealInput = lstNonExistImei.Select(x => new ImeiRealInput()
                    {
                        ProductID = item.ProductID,
                        ProductName = item.ProductName,
                        InvoiceImei = x,
                        IsError = false
                    }).ToList();
                    item.lstImei.AddRange(lstImeiRealInput);
                    item.lstImei = item.lstImei.OrderBy(x => x.IsError).ToList().OrderByDescending(x => x.InvoiceImei).ToList();
                }
            }

            List<ImeiRealInput> lstImei = lstProduct.SelectMany(x => x.lstImei).Cast<ImeiRealInput>().ToList();
            var SameList = lstImei.Where(x => x.InvoiceImei == x.RealImei).ToList(); //lstImei.Where(x => !string.IsNullOrEmpty(x.InvoiceImei) && !string.IsNullOrEmpty(x.RealImei)).ToList();
            SameList.All(c => { c.IsError = false; c.Error = "Giống nhau"; return true; });
            var DiffList = lstImei.Where(x => x.InvoiceImei != x.RealImei).ToList();
            DiffList.All(c => { c.IsError = true; c.Error = "Khác nhau"; return true; });
            foreach (var item in lstProduct)
            {
                item.IsError = item.lstImei.Where(x => x.IsError == true).Count() > 0;
                if (item.IsError)
                {
                    item.Error = "Chưa khớp IMEI";
                }
            }
            for (int i = 0; i < dtbGrid.Rows.Count; i++)
            {
                string str = dtbGrid.Rows[i]["IsRequestImei"].ToString().ToLower();
                string strProductID = dtbGrid.Rows[i]["ProductID"].ToString().Trim();
                string strStatus = string.Empty;
                if (str == "true")
                {
                    DataTable dt = dtbImeiImport.DefaultView.ToTable(false, dtbImeiImport.Columns[strProductID].ColumnName);
                    dtbGrid.Rows[i]["RealQuantity"] = (dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x[0].ToString())).Count());
                }
                if (str == "false")
                {
                    DataTable dt = dtbImeiImport.DefaultView.ToTable(false, dtbImeiImport.Columns[strProductID].ColumnName);
                    int intQuantity = 0;
                    if (dt.Rows.Count > 0)
                    {
                        int.TryParse(dt.Rows[0][0].ToString(), out intQuantity);
                        dtbGrid.Rows[i]["RealQuantity"] = (intQuantity).ToString();
                    }
                }
                int realInput = 0;
                int Quantity = 0;
                int.TryParse(dtbGrid.Rows[i]["Quantity"].ToString(), out Quantity);
                int.TryParse(dtbGrid.Rows[i]["RealQuantity"].ToString(), out realInput);
                if (realInput - Quantity == 0 && lstProduct[i].IsError == false)
                {
                    lstProduct[i].Error = "Khớp";
                }
                else if (realInput - Quantity < 0)
                {
                    lstProduct[i].Error = "Thực nhập thiếu " + Math.Abs(realInput - Quantity) + " cái";
                    lstProduct[i].IsError = true;
                }
                else if (realInput - Quantity > 0)
                {
                    lstProduct[i].Error = "Thực nhập thừa " + Math.Abs(realInput - Quantity) + " cái";
                    lstProduct[i].IsError = true;
                }
                dtbGrid.Rows[i]["Status"] = lstProduct[i].Error;
                dtbGrid.Rows[i]["IsError"] = lstProduct[i].IsError;
            }
            grdViewData.RefreshData();
        }

        private void CheckDuplicateGrid(List<Product> lstProduct)
        {
            List<ProductImei> lstImei = lstProduct.SelectMany(x => x.lstImei).Cast<ProductImei>().ToList();
            var lstKey = lstImei.GroupBy(x => x.Imei)
                .Where(g => g.Count() > 1)
                .Select(y => y.Key).ToList();
            lstProduct.SelectMany(x => x.lstImei).Cast<ProductImei>().ToList().Where(x => lstKey.Contains(x.Imei)).Skip(1).All(c => { c.IsError = true; c.Error = "Lỗi duplicate file import"; return true; });

            foreach (var item in lstProduct)
            {
                item.IsError = item.lstImei.Where(x => x.IsError == true).Count() > 0;
                if (item.IsError)
                {
                    item.Error = "Số IMEI không hợp lệ là: " + item.lstImei.Where(x => x.IsError == true).Count();
                }
            }
        }

        /// <summary>
        /// Định dạng bảng dữ liệu sau khi import
        /// </summary>
        /// <param name="dtbImport">DataTable</param>
        /// <returns>DataTable</returns>
        private void ImportData(ProductRealInput objProduct, List<ImeiRealInput> lstData)
        {
            if (objProduct.IsRequestImei == true && objProduct.lstImei.Count == 0)
            {
                objProduct.IsError = true;
                objProduct.Error = "IMEI chưa nhập";
                return;
            }
            foreach (ImeiRealInput p in lstData)
            {
                if (!Library.AppCore.Other.CheckObject.CheckIMEI(p.RealImei))
                {
                    p.Error = "IMEI không đúng định dạng";
                    p.IsError = true;
                    continue;
                }
            }
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0) return;
            DataTable dtbGrid = grdData.DataSource as DataTable;
            DataRow row = grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            if (row != null)
                if (dtbGrid.Columns.Contains("Status"))
                {
                    if (row["IsRequestImei"].ToString().ToLower() == "true" && int.Parse(row["RealQuantity"].ToString()) != 0)
                    {
                        frmRealInputConfirmDetail frm = new frmRealInputConfirmDetail();
                        if (lstProduct != null && lstProduct.Count > grdViewData.FocusedRowHandle)
                            frm.ProductList = lstProduct[grdViewData.FocusedRowHandle].lstImei;
                        frm.ShowDialog();
                    }
                    else if (row["IsRequestImei"].ToString().ToLower() == "false")
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Sản phẩm này không có IMEI!");
                        return;
                    }
                }
                else
                {
                    MessageBoxObject.ShowWarningMessage(this, "Chưa nhập IMEI cho sản phẩm!");
                    return;
                }
        }

        private void InsertDataToInput()
        {
            foreach (var item in lstProduct.Where(x => x.IsRequestImei == false))
            {
                PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = new PLC.Input.WSInputVoucher.InputVoucherDetail();
                objInputVoucherDetail.ProductID = item.ProductID;
                objInputVoucherDetail.ProductName = item.ProductName;
                objInputVoucherDetail.Quantity = item.Quantity;
            }
            foreach (var item in lstProduct.Where(x => x.IsRequestImei == true).SelectMany(x => x.lstImei).Cast<ProductImei>().ToList())
            {
                PLC.Input.WSInputVoucher.InputVoucherDetailIMEI objInputVoucherDetailImei = new PLC.Input.WSInputVoucher.InputVoucherDetailIMEI();
                objInputVoucherDetailImei.ProductID = item.ProductID;
                objInputVoucherDetailImei.ProductName = item.ProductName;
                objInputVoucherDetailImei.IMEI = item.Imei;
                objInputVoucherDetailIMEIList.Add(objInputVoucherDetailImei);
            }
        }

        private bool CheckValiadate()
        {
            if (lstProduct.SelectMany(x => x.lstImei).Cast<ProductImei>().ToList().Where(x => x.IsError == true).Count() > 0 ||
                lstProduct.Where(x => x.IsError == true).Count() > 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Tồn tại IMEI hoặc sản phẩm không hợp lệ.\r\nVui lòng kiểm tra lại!");
                return false;
            }
            return true;
        }

        private void grdViewData_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            DataRow row = grdViewData.GetDataRow(e.RowHandle);
            if (row != null)
            {
                if (row["IsError"].ToString().ToLower() == "true")
                    e.Appearance.BackColor = Color.Pink;
            }
        }

        private void grdViewData_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView view = sender as GridView;
            if (!view.IsFilterRow(e.RowHandle))
            {
                if (e.RowHandle == view.FocusedRowHandle)
                {
                    DataRow row = view.GetDataRow(e.RowHandle);
                    if (row != null)
                    {
                        if (row["IsError"].ToString().ToLower() == "true")
                            e.Appearance.BackColor = Color.Pink;
                    }
                }
            }
        }

        private void repoViewDetail_Click(object sender, EventArgs e)
        {
            grdData_DoubleClick(null, null);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtContent.Text.Trim()))
            {
                MessageBox.Show("Vui lòng nhập nội dung ghi chú!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtContent.Focus();
                return;
            }
            if (lstProduct.Count() == 0)
            {
                MessageBox.Show("Vui lòng nhập excel để kiểm tra thực nhập!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (lstProduct.Where(x => x.IsError == true).Count() == 0)
            {
                this.Note = txtContent.Text.Trim();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Có sản phẩm chưa khớp với thực nhập vui lòng kiểm tra lại!");
                return;
            }
        }

        private void frmRealInputConfirm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult != DialogResult.OK)
            {
                DialogResult r = MessageBox.Show("Bạn chưa xác nhận thực nhập.\r\nBạn có chắc chắn muốn đóng màn hình?", "Xác nhận thực nhập", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (r == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void frmRealInputConfirm_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            txtContent.Focus();
        }

        private void frmRealInputConfirm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F11)
                this.WindowState = FormWindowState.Maximized;
            if (e.KeyCode == Keys.Escape)
                this.WindowState = FormWindowState.Normal;
        }

        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }
    }

    public class ProductRealInput
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public bool IsRequestImei { get; set; }
        public bool IsError { get; set; }
        public string Error { get; set; }
        public List<ImeiRealInput> lstImei { get; set; }
    }
    public class ImeiRealInput
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string InvoiceImei { get; set; }
        public string RealImei { get; set; }
        public bool IsError { get; set; }
        public string Error { get; set; }

        public ImeiRealInput(string ProductID, string ProductName, string InvoiceImei, string RealImei, bool IsError, string Error)
        {
            this.ProductID = ProductID;
            this.ProductName = ProductName;
            this.InvoiceImei = InvoiceImei;
            this.RealImei = RealImei;
            this.IsError = IsError;
            this.Error = Error;
        }
        public ImeiRealInput() { }
    }
}
