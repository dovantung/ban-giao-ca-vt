﻿using ERP.MasterData.DUI.Properties;

namespace ERP.Inventory.DUI.Input
{
    partial class frmInputVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtmDeliveryNoteDate = new DevExpress.XtraEditors.DateEdit();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkCreateAutoInvoice = new System.Windows.Forms.CheckBox();
            this.txtInVoiceID = new Library.AppControl.TextBoxControl.ASCIIText();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtInvoiceSymbol = new Library.AppControl.TextBoxControl.ASCIIText();
            this.txtDenominator = new Library.AppControl.TextBoxControl.ASCIIText();
            this.label14 = new System.Windows.Forms.Label();
            this.dtInvoiceDate = new DevExpress.XtraEditors.DateEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtProtectPriceDiscount = new DevExpress.XtraEditors.TextEdit();
            this.txtDiscountInput = new DevExpress.XtraEditors.TextEdit();
            this.txtCheckRealInputNote = new DevExpress.XtraEditors.TextEdit();
            this.chkIsCheckRealInput = new System.Windows.Forms.CheckBox();
            this.cboInputTypeSearch = new Library.AppControl.ComboBoxControl.ComboBoxInputType();
            this.cboStoreSearch = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label13 = new System.Windows.Forms.Label();
            this.cboTransportCustomer = new Library.AppControl.ComboBoxControl.ComboBoxCustomer();
            this.radOld = new System.Windows.Forms.RadioButton();
            this.radIsNew = new System.Windows.Forms.RadioButton();
            this.txtExchangeRate = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.cboDiscountReason = new System.Windows.Forms.ComboBox();
            this.cboCurrencyUnit = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.btnSearchProduct = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.txtInputContent = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtInputDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtInputVoucherID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrderID = new System.Windows.Forms.TextBox();
            this.dtPayableTime = new System.Windows.Forms.DateTimePicker();
            this.cboPayableType = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabCustomer = new System.Windows.Forms.TabPage();
            this.grpCustomer = new System.Windows.Forms.GroupBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.lblCustomerID = new System.Windows.Forms.LinkLabel();
            this.txtTaxID = new DevExpress.XtraEditors.TextEdit();
            this.txtPhone = new DevExpress.XtraEditors.TextEdit();
            this.txtCustomerIDCard = new DevExpress.XtraEditors.TextEdit();
            this.txtIDCardIssuePlace = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dtIDCardIssueDate = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblTaxID = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.tabOutVoucher = new System.Windows.Forms.TabPage();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.txtCashUSD = new DevExpress.XtraEditors.TextEdit();
            this.txtPaymentCard = new DevExpress.XtraEditors.TextEdit();
            this.txtCardSpend = new DevExpress.XtraEditors.TextEdit();
            this.txtCashUSDExchange = new DevExpress.XtraEditors.TextEdit();
            this.txtDebt = new DevExpress.XtraEditors.TextEdit();
            this.txtCashVND = new DevExpress.XtraEditors.TextEdit();
            this.cboPaymentCard = new System.Windows.Forms.ComboBox();
            this.dtSettlementDate = new System.Windows.Forms.DateTimePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtTotalLiquidate = new DevExpress.XtraEditors.TextEdit();
            this.txtDiscount = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalMoney = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cboVoucherType = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtVoucherID = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tabAttachFiles = new System.Windows.Forms.TabPage();
            this.grdAttachment = new DevExpress.XtraGrid.GridControl();
            this.mnuAttachment = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuAddAttachment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemScanImage = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemCaptureImage = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDelAttachment = new System.Windows.Forms.ToolStripMenuItem();
            this.grvAttachment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colView = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnView = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colDownload = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDownload = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.grpAttachment = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnRealInputConfirm = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.mnuFlex = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemIsAutoCreateIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemInputIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemInputIMEIByProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemInputbyExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemIsAutoSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuImportFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemDeleteProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.mnuGridEdit = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportData = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.grdInputVoucherDetail = new DevExpress.XtraGrid.GridControl();
            this.grvInputVoucherDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNKTBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityRemain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtItemQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colQuantityToInput = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtInputPrice = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaxedFee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtTaxedFee = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPurchaseFee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductStatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtmDeliveryNoteDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmDeliveryNoteDate.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtInvoiceDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInvoiceDate.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProtectPriceDiscount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountInput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckRealInputNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate.Properties)).BeginInit();
            this.tabCustomer.SuspendLayout();
            this.grpCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerIDCard.Properties)).BeginInit();
            this.tabOutVoucher.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashUSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardSpend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashUSDExchange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashVND.Properties)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLiquidate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalMoney.Properties)).BeginInit();
            this.tabAttachFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).BeginInit();
            this.mnuAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).BeginInit();
            this.panel1.SuspendLayout();
            this.mnuFlex.SuspendLayout();
            this.mnuGridEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdInputVoucherDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvInputVoucherDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxedFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabCustomer);
            this.tabControl1.Controls.Add(this.tabOutVoucher);
            this.tabControl1.Controls.Add(this.tabAttachFiles);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1072, 563);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.txtCashVND_EditValueChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.dtmDeliveryNoteDate);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1064, 534);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin phiếu nhập";
            // 
            // dtmDeliveryNoteDate
            // 
            this.dtmDeliveryNoteDate.EditValue = null;
            this.dtmDeliveryNoteDate.Location = new System.Drawing.Point(900, 160);
            this.dtmDeliveryNoteDate.Name = "dtmDeliveryNoteDate";
            this.dtmDeliveryNoteDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtmDeliveryNoteDate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtmDeliveryNoteDate.Properties.Appearance.Options.UseFont = true;
            this.dtmDeliveryNoteDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtmDeliveryNoteDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtmDeliveryNoteDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtmDeliveryNoteDate.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtmDeliveryNoteDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtmDeliveryNoteDate.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtmDeliveryNoteDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtmDeliveryNoteDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtmDeliveryNoteDate.Size = new System.Drawing.Size(137, 22);
            this.dtmDeliveryNoteDate.TabIndex = 35;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.chkCreateAutoInvoice);
            this.groupBox2.Controls.Add(this.txtInVoiceID);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtInvoiceSymbol);
            this.groupBox2.Controls.Add(this.txtDenominator);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.dtInvoiceDate);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Location = new System.Drawing.Point(777, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(273, 151);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin hóa đơn";
            // 
            // chkCreateAutoInvoice
            // 
            this.chkCreateAutoInvoice.AutoSize = true;
            this.chkCreateAutoInvoice.Checked = true;
            this.chkCreateAutoInvoice.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCreateAutoInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCreateAutoInvoice.Location = new System.Drawing.Point(19, 18);
            this.chkCreateAutoInvoice.Name = "chkCreateAutoInvoice";
            this.chkCreateAutoInvoice.Size = new System.Drawing.Size(154, 20);
            this.chkCreateAutoInvoice.TabIndex = 0;
            this.chkCreateAutoInvoice.Text = "Tạo hóa đơn về cùng";
            this.chkCreateAutoInvoice.UseVisualStyleBackColor = true;
            this.chkCreateAutoInvoice.CheckedChanged += new System.EventHandler(this.chkCreateAutoInvoice_CheckedChanged);
            // 
            // txtInVoiceID
            // 
            this.txtInVoiceID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInVoiceID.Location = new System.Drawing.Point(123, 41);
            this.txtInVoiceID.MaxLength = 10;
            this.txtInVoiceID.Name = "txtInVoiceID";
            this.txtInVoiceID.Size = new System.Drawing.Size(137, 23);
            this.txtInVoiceID.TabIndex = 2;
            this.txtInVoiceID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInVoiceID_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số hóa đơn:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ký hiệu hóa đơn:";
            // 
            // txtInvoiceSymbol
            // 
            this.txtInvoiceSymbol.Location = new System.Drawing.Point(123, 69);
            this.txtInvoiceSymbol.MaxLength = 20;
            this.txtInvoiceSymbol.Name = "txtInvoiceSymbol";
            this.txtInvoiceSymbol.Size = new System.Drawing.Size(137, 22);
            this.txtInvoiceSymbol.TabIndex = 4;
            // 
            // txtDenominator
            // 
            this.txtDenominator.Location = new System.Drawing.Point(123, 94);
            this.txtDenominator.MaxLength = 20;
            this.txtDenominator.Name = "txtDenominator";
            this.txtDenominator.Size = new System.Drawing.Size(137, 22);
            this.txtDenominator.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 97);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 16);
            this.label14.TabIndex = 5;
            this.label14.Text = "Mẫu hóa đơn:";
            // 
            // dtInvoiceDate
            // 
            this.dtInvoiceDate.EditValue = null;
            this.dtInvoiceDate.Location = new System.Drawing.Point(123, 119);
            this.dtInvoiceDate.Name = "dtInvoiceDate";
            this.dtInvoiceDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtInvoiceDate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInvoiceDate.Properties.Appearance.Options.UseFont = true;
            this.dtInvoiceDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtInvoiceDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtInvoiceDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtInvoiceDate.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtInvoiceDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtInvoiceDate.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtInvoiceDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtInvoiceDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtInvoiceDate.Size = new System.Drawing.Size(137, 22);
            this.dtInvoiceDate.TabIndex = 8;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 122);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(96, 16);
            this.label18.TabIndex = 7;
            this.label18.Text = "Ngày hóa đơn:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(793, 163);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(99, 16);
            this.label24.TabIndex = 9;
            this.label24.Text = "Ngày biên bản:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNote);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txtProtectPriceDiscount);
            this.groupBox1.Controls.Add(this.txtDiscountInput);
            this.groupBox1.Controls.Add(this.txtCheckRealInputNote);
            this.groupBox1.Controls.Add(this.chkIsCheckRealInput);
            this.groupBox1.Controls.Add(this.cboInputTypeSearch);
            this.groupBox1.Controls.Add(this.cboStoreSearch);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cboTransportCustomer);
            this.groupBox1.Controls.Add(this.radOld);
            this.groupBox1.Controls.Add(this.radIsNew);
            this.groupBox1.Controls.Add(this.txtExchangeRate);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cboDiscountReason);
            this.groupBox1.Controls.Add(this.cboCurrencyUnit);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.btnSearchProduct);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtBarcode);
            this.groupBox1.Controls.Add(this.txtInputContent);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dtInputDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtInputVoucherID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtOrderID);
            this.groupBox1.Controls.Add(this.dtPayableTime);
            this.groupBox1.Controls.Add(this.cboPayableType);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(768, 255);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin phiếu nhập";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(541, 145);
            this.txtNote.MaxLength = 200;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNote.Size = new System.Drawing.Size(222, 51);
            this.txtNote.TabIndex = 25;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(423, 148);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 16);
            this.label20.TabIndex = 24;
            this.label20.Text = "Ghi chú:";
            // 
            // txtProtectPriceDiscount
            // 
            this.txtProtectPriceDiscount.EditValue = "0";
            this.txtProtectPriceDiscount.Location = new System.Drawing.Point(541, 119);
            this.txtProtectPriceDiscount.Name = "txtProtectPriceDiscount";
            this.txtProtectPriceDiscount.Properties.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.txtProtectPriceDiscount.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProtectPriceDiscount.Properties.Appearance.Options.UseBackColor = true;
            this.txtProtectPriceDiscount.Properties.Appearance.Options.UseFont = true;
            this.txtProtectPriceDiscount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtProtectPriceDiscount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtProtectPriceDiscount.Properties.DisplayFormat.FormatString = "#,##0.##";
            this.txtProtectPriceDiscount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtProtectPriceDiscount.Properties.Mask.EditMask = "###,###,###,##0.##";
            this.txtProtectPriceDiscount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtProtectPriceDiscount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtProtectPriceDiscount.Properties.NullText = "0";
            this.txtProtectPriceDiscount.Size = new System.Drawing.Size(221, 22);
            this.txtProtectPriceDiscount.TabIndex = 21;
            // 
            // txtDiscountInput
            // 
            this.txtDiscountInput.EditValue = "0";
            this.txtDiscountInput.Location = new System.Drawing.Point(340, 119);
            this.txtDiscountInput.Name = "txtDiscountInput";
            this.txtDiscountInput.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtDiscountInput.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscountInput.Properties.Appearance.Options.UseBackColor = true;
            this.txtDiscountInput.Properties.Appearance.Options.UseFont = true;
            this.txtDiscountInput.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDiscountInput.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDiscountInput.Properties.DisplayFormat.FormatString = "#,##0.##";
            this.txtDiscountInput.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtDiscountInput.Properties.Mask.EditMask = "###,###,###,##0.##";
            this.txtDiscountInput.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDiscountInput.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDiscountInput.Properties.NullText = "0";
            this.txtDiscountInput.Size = new System.Drawing.Size(62, 22);
            this.txtDiscountInput.TabIndex = 19;
            // 
            // txtCheckRealInputNote
            // 
            this.txtCheckRealInputNote.Location = new System.Drawing.Point(542, 227);
            this.txtCheckRealInputNote.Name = "txtCheckRealInputNote";
            this.txtCheckRealInputNote.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCheckRealInputNote.Properties.Appearance.Options.UseFont = true;
            this.txtCheckRealInputNote.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtCheckRealInputNote.Properties.NullText = "Nhập ghi chú kiểm tra";
            this.txtCheckRealInputNote.Size = new System.Drawing.Size(221, 22);
            this.txtCheckRealInputNote.TabIndex = 34;
            // 
            // chkIsCheckRealInput
            // 
            this.chkIsCheckRealInput.AutoSize = true;
            this.chkIsCheckRealInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsCheckRealInput.ForeColor = System.Drawing.Color.Blue;
            this.chkIsCheckRealInput.Location = new System.Drawing.Point(423, 228);
            this.chkIsCheckRealInput.Name = "chkIsCheckRealInput";
            this.chkIsCheckRealInput.Size = new System.Drawing.Size(116, 20);
            this.chkIsCheckRealInput.TabIndex = 33;
            this.chkIsCheckRealInput.Text = "KT thực nhập";
            this.chkIsCheckRealInput.UseVisualStyleBackColor = true;
            this.chkIsCheckRealInput.CheckedChanged += new System.EventHandler(this.chkIsCheckRealInput_CheckedChanged);
            // 
            // cboInputTypeSearch
            // 
            this.cboInputTypeSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputTypeSearch.Location = new System.Drawing.Point(131, 67);
            this.cboInputTypeSearch.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputTypeSearch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputTypeSearch.Name = "cboInputTypeSearch";
            this.cboInputTypeSearch.Size = new System.Drawing.Size(270, 24);
            this.cboInputTypeSearch.TabIndex = 9;
            this.cboInputTypeSearch.SelectionChangeCommitted += new System.EventHandler(this.cboInputTypeSearch_SelectionChangeCommitted);
            // 
            // cboStoreSearch
            // 
            this.cboStoreSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreSearch.Location = new System.Drawing.Point(541, 40);
            this.cboStoreSearch.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreSearch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreSearch.Name = "cboStoreSearch";
            this.cboStoreSearch.Size = new System.Drawing.Size(221, 24);
            this.cboStoreSearch.TabIndex = 7;
            this.cboStoreSearch.SelectionChangeCommitted += new System.EventHandler(this.cboStoreSearch_SelectionChangeCommitted);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 148);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 16);
            this.label13.TabIndex = 22;
            this.label13.Text = "Đối tác vận chuyển:";
            // 
            // cboTransportCustomer
            // 
            this.cboTransportCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransportCustomer.IsActivatedType = Library.AppCore.Constant.EnumType.IsActivatedType.ACTIVATED;
            this.cboTransportCustomer.IsRequireActive = false;
            this.cboTransportCustomer.Location = new System.Drawing.Point(131, 144);
            this.cboTransportCustomer.Margin = new System.Windows.Forms.Padding(0);
            this.cboTransportCustomer.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboTransportCustomer.Name = "cboTransportCustomer";
            this.cboTransportCustomer.Size = new System.Drawing.Size(271, 24);
            this.cboTransportCustomer.TabIndex = 23;
            // 
            // radOld
            // 
            this.radOld.AutoSize = true;
            this.radOld.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radOld.Location = new System.Drawing.Point(598, 200);
            this.radOld.Name = "radOld";
            this.radOld.Size = new System.Drawing.Size(44, 20);
            this.radOld.TabIndex = 29;
            this.radOld.Text = "Cũ";
            this.radOld.UseVisualStyleBackColor = true;
            this.radOld.CheckedChanged += new System.EventHandler(this.radOld_CheckedChanged);
            // 
            // radIsNew
            // 
            this.radIsNew.AutoSize = true;
            this.radIsNew.Checked = true;
            this.radIsNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radIsNew.Location = new System.Drawing.Point(541, 200);
            this.radIsNew.Name = "radIsNew";
            this.radIsNew.Size = new System.Drawing.Size(51, 20);
            this.radIsNew.TabIndex = 28;
            this.radIsNew.TabStop = true;
            this.radIsNew.Text = "Mới";
            this.radIsNew.UseVisualStyleBackColor = true;
            // 
            // txtExchangeRate
            // 
            this.txtExchangeRate.Location = new System.Drawing.Point(628, 94);
            this.txtExchangeRate.Name = "txtExchangeRate";
            this.txtExchangeRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeRate.Properties.Appearance.Options.UseFont = true;
            this.txtExchangeRate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtExchangeRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtExchangeRate.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.Info;
            this.txtExchangeRate.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtExchangeRate.Properties.DisplayFormat.FormatString = "N2";
            this.txtExchangeRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExchangeRate.Properties.Mask.EditMask = "n";
            this.txtExchangeRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtExchangeRate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtExchangeRate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtExchangeRate.Size = new System.Drawing.Size(134, 22);
            this.txtExchangeRate.TabIndex = 16;
            this.txtExchangeRate.Leave += new System.EventHandler(this.txtExchangeRate_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(421, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 16);
            this.label8.TabIndex = 20;
            this.label8.Text = "Bảo vệ giá:";
            // 
            // cboDiscountReason
            // 
            this.cboDiscountReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscountReason.DropDownWidth = 250;
            this.cboDiscountReason.Location = new System.Drawing.Point(131, 118);
            this.cboDiscountReason.Name = "cboDiscountReason";
            this.cboDiscountReason.Size = new System.Drawing.Size(203, 24);
            this.cboDiscountReason.TabIndex = 18;
            this.cboDiscountReason.SelectionChangeCommitted += new System.EventHandler(this.cboDiscountReason_SelectionChangeCommitted);
            // 
            // cboCurrencyUnit
            // 
            this.cboCurrencyUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyUnit.DropDownWidth = 250;
            this.cboCurrencyUnit.Location = new System.Drawing.Point(541, 93);
            this.cboCurrencyUnit.Name = "cboCurrencyUnit";
            this.cboCurrencyUnit.Size = new System.Drawing.Size(80, 24);
            this.cboCurrencyUnit.TabIndex = 15;
            this.cboCurrencyUnit.SelectionChangeCommitted += new System.EventHandler(this.cboCurrencyUnit_SelectionChangeCommitted);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(421, 97);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 16);
            this.label17.TabIndex = 14;
            this.label17.Text = "Loại tiền:";
            // 
            // btnSearchProduct
            // 
            this.btnSearchProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearchProduct.Location = new System.Drawing.Point(376, 225);
            this.btnSearchProduct.Name = "btnSearchProduct";
            this.btnSearchProduct.Size = new System.Drawing.Size(26, 26);
            this.btnSearchProduct.TabIndex = 32;
            this.btnSearchProduct.Click += new System.EventHandler(this.btnSearchProduct_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 230);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 16);
            this.label15.TabIndex = 30;
            this.label15.Text = "Barcode:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Location = new System.Drawing.Point(131, 227);
            this.txtBarcode.MaxLength = 50;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(244, 22);
            this.txtBarcode.TabIndex = 31;
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // txtInputContent
            // 
            this.txtInputContent.Location = new System.Drawing.Point(131, 171);
            this.txtInputContent.MaxLength = 990;
            this.txtInputContent.Multiline = true;
            this.txtInputContent.Name = "txtInputContent";
            this.txtInputContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtInputContent.Size = new System.Drawing.Size(271, 52);
            this.txtInputContent.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 175);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 16);
            this.label10.TabIndex = 26;
            this.label10.Text = "Nội dung nhập:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 17;
            this.label9.Text = "Giảm giá:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(421, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Kho nhập:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "Hình thức nhập:";
            // 
            // dtInputDate
            // 
            this.dtInputDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtInputDate.Enabled = false;
            this.dtInputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtInputDate.Location = new System.Drawing.Point(541, 17);
            this.dtInputDate.Name = "dtInputDate";
            this.dtInputDate.Size = new System.Drawing.Size(221, 22);
            this.dtInputDate.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(421, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Ngày nhập:";
            // 
            // txtInputVoucherID
            // 
            this.txtInputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputVoucherID.Location = new System.Drawing.Point(131, 17);
            this.txtInputVoucherID.Name = "txtInputVoucherID";
            this.txtInputVoucherID.ReadOnly = true;
            this.txtInputVoucherID.Size = new System.Drawing.Size(271, 22);
            this.txtInputVoucherID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã phiếu nhập:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Mã đơn hàng:";
            // 
            // txtOrderID
            // 
            this.txtOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOrderID.Location = new System.Drawing.Point(131, 42);
            this.txtOrderID.MaxLength = 20;
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.Size = new System.Drawing.Size(271, 22);
            this.txtOrderID.TabIndex = 5;
            // 
            // dtPayableTime
            // 
            this.dtPayableTime.CustomFormat = "dd/MM/yyyy";
            this.dtPayableTime.Enabled = false;
            this.dtPayableTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPayableTime.Location = new System.Drawing.Point(131, 94);
            this.dtPayableTime.Name = "dtPayableTime";
            this.dtPayableTime.Size = new System.Drawing.Size(271, 22);
            this.dtPayableTime.TabIndex = 13;
            // 
            // cboPayableType
            // 
            this.cboPayableType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPayableType.DropDownWidth = 250;
            this.cboPayableType.Location = new System.Drawing.Point(541, 67);
            this.cboPayableType.Name = "cboPayableType";
            this.cboPayableType.Size = new System.Drawing.Size(221, 24);
            this.cboPayableType.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 97);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 16);
            this.label12.TabIndex = 12;
            this.label12.Text = "Ngày thanh toán:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(421, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 16);
            this.label11.TabIndex = 10;
            this.label11.Text = "HT thanh toán:";
            // 
            // tabCustomer
            // 
            this.tabCustomer.BackColor = System.Drawing.SystemColors.Control;
            this.tabCustomer.Controls.Add(this.grpCustomer);
            this.tabCustomer.Location = new System.Drawing.Point(4, 25);
            this.tabCustomer.Name = "tabCustomer";
            this.tabCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tabCustomer.Size = new System.Drawing.Size(1064, 534);
            this.tabCustomer.TabIndex = 3;
            this.tabCustomer.Text = "Thông tin khách hàng";
            // 
            // grpCustomer
            // 
            this.grpCustomer.Controls.Add(this.txtCustomerID);
            this.grpCustomer.Controls.Add(this.lblCustomerID);
            this.grpCustomer.Controls.Add(this.txtTaxID);
            this.grpCustomer.Controls.Add(this.txtPhone);
            this.grpCustomer.Controls.Add(this.txtCustomerIDCard);
            this.grpCustomer.Controls.Add(this.txtIDCardIssuePlace);
            this.grpCustomer.Controls.Add(this.label21);
            this.grpCustomer.Controls.Add(this.dtIDCardIssueDate);
            this.grpCustomer.Controls.Add(this.label16);
            this.grpCustomer.Controls.Add(this.label19);
            this.grpCustomer.Controls.Add(this.label22);
            this.grpCustomer.Controls.Add(this.txtCustomerName);
            this.grpCustomer.Controls.Add(this.txtAddress);
            this.grpCustomer.Controls.Add(this.lblPhone);
            this.grpCustomer.Controls.Add(this.lblTaxID);
            this.grpCustomer.Controls.Add(this.lblAddress);
            this.grpCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCustomer.Location = new System.Drawing.Point(3, 3);
            this.grpCustomer.Name = "grpCustomer";
            this.grpCustomer.Size = new System.Drawing.Size(1058, 528);
            this.grpCustomer.TabIndex = 0;
            this.grpCustomer.TabStop = false;
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCustomerID.Location = new System.Drawing.Point(91, 15);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.ReadOnly = true;
            this.txtCustomerID.Size = new System.Drawing.Size(210, 24);
            this.txtCustomerID.TabIndex = 1;
            // 
            // lblCustomerID
            // 
            this.lblCustomerID.AutoSize = true;
            this.lblCustomerID.Location = new System.Drawing.Point(11, 20);
            this.lblCustomerID.Name = "lblCustomerID";
            this.lblCustomerID.Size = new System.Drawing.Size(81, 16);
            this.lblCustomerID.TabIndex = 0;
            this.lblCustomerID.TabStop = true;
            this.lblCustomerID.Text = "Khách hàng:";
            this.lblCustomerID.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblCustomerID_LinkClicked);
            // 
            // txtTaxID
            // 
            this.txtTaxID.Location = new System.Drawing.Point(91, 146);
            this.txtTaxID.Name = "txtTaxID";
            this.txtTaxID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxID.Properties.Appearance.Options.UseFont = true;
            this.txtTaxID.Properties.DisplayFormat.FormatString = "############";
            this.txtTaxID.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTaxID.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTaxID.Properties.Mask.EditMask = "[0-9,.,-]{0,13}";
            this.txtTaxID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTaxID.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTaxID.Properties.MaxLength = 13;
            this.txtTaxID.Size = new System.Drawing.Size(191, 22);
            this.txtTaxID.TabIndex = 13;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(366, 146);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhone.Properties.Appearance.Options.UseFont = true;
            this.txtPhone.Properties.DisplayFormat.FormatString = "############";
            this.txtPhone.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtPhone.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtPhone.Properties.Mask.EditMask = "\\d{0,11}";
            this.txtPhone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPhone.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPhone.Properties.MaxLength = 11;
            this.txtPhone.Size = new System.Drawing.Size(191, 22);
            this.txtPhone.TabIndex = 15;
            // 
            // txtCustomerIDCard
            // 
            this.txtCustomerIDCard.Location = new System.Drawing.Point(91, 118);
            this.txtCustomerIDCard.Name = "txtCustomerIDCard";
            this.txtCustomerIDCard.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerIDCard.Properties.Appearance.Options.UseFont = true;
            this.txtCustomerIDCard.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCustomerIDCard.Properties.MaxLength = 12;
            this.txtCustomerIDCard.Size = new System.Drawing.Size(191, 22);
            this.txtCustomerIDCard.TabIndex = 7;
            // 
            // txtIDCardIssuePlace
            // 
            this.txtIDCardIssuePlace.Location = new System.Drawing.Point(366, 118);
            this.txtIDCardIssuePlace.MaxLength = 50;
            this.txtIDCardIssuePlace.Name = "txtIDCardIssuePlace";
            this.txtIDCardIssuePlace.Size = new System.Drawing.Size(191, 22);
            this.txtIDCardIssuePlace.TabIndex = 9;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(296, 121);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 16);
            this.label21.TabIndex = 8;
            this.label21.Text = "Nơi cấp:";
            // 
            // dtIDCardIssueDate
            // 
            this.dtIDCardIssueDate.CustomFormat = "dd/MM/yyyy";
            this.dtIDCardIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtIDCardIssueDate.Location = new System.Drawing.Point(641, 118);
            this.dtIDCardIssueDate.Name = "dtIDCardIssueDate";
            this.dtIDCardIssueDate.Size = new System.Drawing.Size(111, 22);
            this.dtIDCardIssueDate.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(572, 121);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 16);
            this.label16.TabIndex = 10;
            this.label16.Text = "Ngày cấp:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 121);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 16);
            this.label19.TabIndex = 6;
            this.label19.Text = "CMND:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 47);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 16);
            this.label22.TabIndex = 2;
            this.label22.Text = "Họ tên KH:";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new System.Drawing.Point(91, 44);
            this.txtCustomerName.MaxLength = 200;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(661, 22);
            this.txtCustomerName.TabIndex = 3;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(91, 71);
            this.txtAddress.MaxLength = 400;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(661, 42);
            this.txtAddress.TabIndex = 5;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(296, 149);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(70, 16);
            this.lblPhone.TabIndex = 14;
            this.lblPhone.Text = "Điện thoại:";
            // 
            // lblTaxID
            // 
            this.lblTaxID.AutoSize = true;
            this.lblTaxID.Location = new System.Drawing.Point(11, 149);
            this.lblTaxID.Name = "lblTaxID";
            this.lblTaxID.Size = new System.Drawing.Size(76, 16);
            this.lblTaxID.TabIndex = 12;
            this.lblTaxID.Text = "Mã số thuế:";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(11, 71);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 16);
            this.lblAddress.TabIndex = 4;
            this.lblAddress.Text = "Địa chỉ:";
            // 
            // tabOutVoucher
            // 
            this.tabOutVoucher.BackColor = System.Drawing.SystemColors.Control;
            this.tabOutVoucher.Controls.Add(this.xtraTabControl1);
            this.tabOutVoucher.Controls.Add(this.groupBox4);
            this.tabOutVoucher.Location = new System.Drawing.Point(4, 25);
            this.tabOutVoucher.Name = "tabOutVoucher";
            this.tabOutVoucher.Padding = new System.Windows.Forms.Padding(3);
            this.tabOutVoucher.Size = new System.Drawing.Size(1064, 534);
            this.tabOutVoucher.TabIndex = 2;
            this.tabOutVoucher.Text = "Thông tin phiếu chi";
            this.tabOutVoucher.Enter += new System.EventHandler(this.tabOutVoucher_Enter);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.xtraTabControl1.Appearance.Options.UseBackColor = true;
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Right;
            this.xtraTabControl1.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Vertical;
            this.xtraTabControl1.Location = new System.Drawing.Point(5, 101);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(938, 99);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage1.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage1.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.xtraTabPage1.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage1.Controls.Add(this.txtCashUSD);
            this.xtraTabPage1.Controls.Add(this.txtPaymentCard);
            this.xtraTabPage1.Controls.Add(this.txtCardSpend);
            this.xtraTabPage1.Controls.Add(this.txtCashUSDExchange);
            this.xtraTabPage1.Controls.Add(this.txtDebt);
            this.xtraTabPage1.Controls.Add(this.txtCashVND);
            this.xtraTabPage1.Controls.Add(this.cboPaymentCard);
            this.xtraTabPage1.Controls.Add(this.dtSettlementDate);
            this.xtraTabPage1.Controls.Add(this.label35);
            this.xtraTabPage1.Controls.Add(this.label36);
            this.xtraTabPage1.Controls.Add(this.label37);
            this.xtraTabPage1.Controls.Add(this.label38);
            this.xtraTabPage1.Controls.Add(this.label39);
            this.xtraTabPage1.Controls.Add(this.label41);
            this.xtraTabPage1.Controls.Add(this.label42);
            this.xtraTabPage1.Controls.Add(this.label43);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(912, 93);
            this.xtraTabPage1.Text = "Chi tiền";
            // 
            // txtCashUSD
            // 
            this.txtCashUSD.EditValue = "0";
            this.txtCashUSD.Location = new System.Drawing.Point(96, 63);
            this.txtCashUSD.Name = "txtCashUSD";
            this.txtCashUSD.Properties.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.txtCashUSD.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashUSD.Properties.Appearance.Options.UseBackColor = true;
            this.txtCashUSD.Properties.Appearance.Options.UseFont = true;
            this.txtCashUSD.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCashUSD.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCashUSD.Properties.DisplayFormat.FormatString = "#,##0.####";
            this.txtCashUSD.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtCashUSD.Properties.Mask.EditMask = "###,###,###,##0.####";
            this.txtCashUSD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCashUSD.Properties.NullText = "0";
            this.txtCashUSD.Size = new System.Drawing.Size(165, 22);
            this.txtCashUSD.TabIndex = 121;
            this.txtCashUSD.EditValueChanged += new System.EventHandler(this.txtCashUSD_EditValueChanged);
            // 
            // txtPaymentCard
            // 
            this.txtPaymentCard.EditValue = "0";
            this.txtPaymentCard.Location = new System.Drawing.Point(96, 35);
            this.txtPaymentCard.Name = "txtPaymentCard";
            this.txtPaymentCard.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentCard.Properties.Appearance.Options.UseFont = true;
            this.txtPaymentCard.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPaymentCard.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtPaymentCard.Properties.DisplayFormat.FormatString = "#,##0.####";
            this.txtPaymentCard.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtPaymentCard.Properties.Mask.EditMask = "###,###,###,##0.####";
            this.txtPaymentCard.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPaymentCard.Properties.NullText = "0";
            this.txtPaymentCard.Size = new System.Drawing.Size(165, 22);
            this.txtPaymentCard.TabIndex = 7;
            this.txtPaymentCard.EditValueChanged += new System.EventHandler(this.txtPaymentCard_EditValueChanged);
            // 
            // txtCardSpend
            // 
            this.txtCardSpend.EditValue = "0";
            this.txtCardSpend.Location = new System.Drawing.Point(698, 35);
            this.txtCardSpend.Name = "txtCardSpend";
            this.txtCardSpend.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtCardSpend.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCardSpend.Properties.Appearance.Options.UseBackColor = true;
            this.txtCardSpend.Properties.Appearance.Options.UseFont = true;
            this.txtCardSpend.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCardSpend.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCardSpend.Properties.DisplayFormat.FormatString = "#,##0.####";
            this.txtCardSpend.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtCardSpend.Properties.Mask.EditMask = "###,###,###,##0.####";
            this.txtCardSpend.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCardSpend.Properties.NullText = "0";
            this.txtCardSpend.Properties.ReadOnly = true;
            this.txtCardSpend.Size = new System.Drawing.Size(165, 22);
            this.txtCardSpend.TabIndex = 11;
            // 
            // txtCashUSDExchange
            // 
            this.txtCashUSDExchange.EditValue = "0";
            this.txtCashUSDExchange.Location = new System.Drawing.Point(415, 63);
            this.txtCashUSDExchange.Name = "txtCashUSDExchange";
            this.txtCashUSDExchange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtCashUSDExchange.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashUSDExchange.Properties.Appearance.Options.UseBackColor = true;
            this.txtCashUSDExchange.Properties.Appearance.Options.UseFont = true;
            this.txtCashUSDExchange.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCashUSDExchange.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCashUSDExchange.Properties.DisplayFormat.FormatString = "#,##0.##";
            this.txtCashUSDExchange.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtCashUSDExchange.Properties.Mask.EditMask = "###,###,###,##0.####";
            this.txtCashUSDExchange.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCashUSDExchange.Properties.NullText = "0";
            this.txtCashUSDExchange.Properties.ReadOnly = true;
            this.txtCashUSDExchange.Size = new System.Drawing.Size(171, 22);
            this.txtCashUSDExchange.TabIndex = 13;
            // 
            // txtDebt
            // 
            this.txtDebt.EditValue = "0";
            this.txtDebt.Location = new System.Drawing.Point(415, 7);
            this.txtDebt.Name = "txtDebt";
            this.txtDebt.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtDebt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDebt.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.txtDebt.Properties.Appearance.Options.UseBackColor = true;
            this.txtDebt.Properties.Appearance.Options.UseFont = true;
            this.txtDebt.Properties.Appearance.Options.UseForeColor = true;
            this.txtDebt.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDebt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDebt.Properties.DisplayFormat.FormatString = "#,##0.####";
            this.txtDebt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtDebt.Properties.EditFormat.FormatString = "#,##0.####";
            this.txtDebt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtDebt.Properties.NullText = "0";
            this.txtDebt.Properties.ReadOnly = true;
            this.txtDebt.Size = new System.Drawing.Size(171, 22);
            this.txtDebt.TabIndex = 3;
            // 
            // txtCashVND
            // 
            this.txtCashVND.EditValue = "0";
            this.txtCashVND.Location = new System.Drawing.Point(96, 7);
            this.txtCashVND.Name = "txtCashVND";
            this.txtCashVND.Properties.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.txtCashVND.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashVND.Properties.Appearance.Options.UseBackColor = true;
            this.txtCashVND.Properties.Appearance.Options.UseFont = true;
            this.txtCashVND.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCashVND.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCashVND.Properties.DisplayFormat.FormatString = "#,##0.####";
            this.txtCashVND.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtCashVND.Properties.Mask.EditMask = "###,###,###,##0.####";
            this.txtCashVND.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCashVND.Properties.NullText = "0";
            this.txtCashVND.Size = new System.Drawing.Size(165, 22);
            this.txtCashVND.TabIndex = 1;
            this.txtCashVND.EditValueChanged += new System.EventHandler(this.txtCashVND_EditValueChanged);
            // 
            // cboPaymentCard
            // 
            this.cboPaymentCard.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPaymentCard.DropDownWidth = 250;
            this.cboPaymentCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPaymentCard.Location = new System.Drawing.Point(415, 34);
            this.cboPaymentCard.Name = "cboPaymentCard";
            this.cboPaymentCard.Size = new System.Drawing.Size(171, 24);
            this.cboPaymentCard.TabIndex = 9;
            this.cboPaymentCard.SelectionChangeCommitted += new System.EventHandler(this.cboPaymentCard_SelectionChangeCommitted);
            // 
            // dtSettlementDate
            // 
            this.dtSettlementDate.CustomFormat = "dd/MM/yyyy";
            this.dtSettlementDate.Enabled = false;
            this.dtSettlementDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtSettlementDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtSettlementDate.Location = new System.Drawing.Point(698, 7);
            this.dtSettlementDate.Name = "dtSettlementDate";
            this.dtSettlementDate.Size = new System.Drawing.Size(165, 22);
            this.dtSettlementDate.TabIndex = 5;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(615, 10);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(78, 16);
            this.label35.TabIndex = 4;
            this.label35.Text = "Thanh toán:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(326, 10);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(81, 16);
            this.label36.TabIndex = 2;
            this.label36.Text = "Còn nợ lại:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(5, 10);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(94, 16);
            this.label37.TabIndex = 0;
            this.label37.Text = "Tiền mặt VNĐ:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(5, 38);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(85, 16);
            this.label38.TabIndex = 6;
            this.label38.Text = "Tiền qua thẻ:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(326, 38);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 16);
            this.label39.TabIndex = 8;
            this.label39.Text = "Loại thẻ:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(5, 66);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(70, 16);
            this.label41.TabIndex = 12;
            this.label41.Text = "Tiền khác:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(615, 38);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(51, 16);
            this.label42.TabIndex = 10;
            this.label42.Text = "Phí thẻ:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(326, 66);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(88, 16);
            this.label43.TabIndex = 14;
            this.label43.Text = "Quy đổi VNĐ:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtTotalLiquidate);
            this.groupBox4.Controls.Add(this.txtDiscount);
            this.groupBox4.Controls.Add(this.txtTotalMoney);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.txtContent);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.cboVoucherType);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.txtVoucherID);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Location = new System.Drawing.Point(5, 1);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(939, 94);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            // 
            // txtTotalLiquidate
            // 
            this.txtTotalLiquidate.EditValue = "0";
            this.txtTotalLiquidate.Location = new System.Drawing.Point(738, 67);
            this.txtTotalLiquidate.Name = "txtTotalLiquidate";
            this.txtTotalLiquidate.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtTotalLiquidate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalLiquidate.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.txtTotalLiquidate.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalLiquidate.Properties.Appearance.Options.UseFont = true;
            this.txtTotalLiquidate.Properties.Appearance.Options.UseForeColor = true;
            this.txtTotalLiquidate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalLiquidate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalLiquidate.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.Red;
            this.txtTotalLiquidate.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtTotalLiquidate.Properties.DisplayFormat.FormatString = "#,##0.####";
            this.txtTotalLiquidate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTotalLiquidate.Properties.EditFormat.FormatString = "#,##0.####";
            this.txtTotalLiquidate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTotalLiquidate.Properties.NullText = "0";
            this.txtTotalLiquidate.Properties.ReadOnly = true;
            this.txtTotalLiquidate.Size = new System.Drawing.Size(175, 22);
            this.txtTotalLiquidate.TabIndex = 11;
            // 
            // txtDiscount
            // 
            this.txtDiscount.EditValue = "0";
            this.txtDiscount.Location = new System.Drawing.Point(738, 41);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtDiscount.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.Properties.Appearance.Options.UseBackColor = true;
            this.txtDiscount.Properties.Appearance.Options.UseFont = true;
            this.txtDiscount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDiscount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDiscount.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtDiscount.Properties.DisplayFormat.FormatString = "#,##0.####";
            this.txtDiscount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtDiscount.Properties.EditFormat.FormatString = "#,##0.####";
            this.txtDiscount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtDiscount.Properties.NullText = "0";
            this.txtDiscount.Properties.ReadOnly = true;
            this.txtDiscount.Size = new System.Drawing.Size(173, 22);
            this.txtDiscount.TabIndex = 9;
            // 
            // txtTotalMoney
            // 
            this.txtTotalMoney.EditValue = "0";
            this.txtTotalMoney.Location = new System.Drawing.Point(738, 14);
            this.txtTotalMoney.Name = "txtTotalMoney";
            this.txtTotalMoney.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalMoney.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMoney.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalMoney.Properties.Appearance.Options.UseFont = true;
            this.txtTotalMoney.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalMoney.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalMoney.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMoney.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTotalMoney.Properties.DisplayFormat.FormatString = "#,##0.####";
            this.txtTotalMoney.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTotalMoney.Properties.EditFormat.FormatString = "#,##0.####";
            this.txtTotalMoney.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTotalMoney.Properties.NullText = "0";
            this.txtTotalMoney.Properties.ReadOnly = true;
            this.txtTotalMoney.Size = new System.Drawing.Size(173, 22);
            this.txtTotalMoney.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(615, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(118, 16);
            this.label27.TabIndex = 10;
            this.label27.Text = "Phải thanh toán:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(615, 41);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 16);
            this.label29.TabIndex = 8;
            this.label29.Text = "Giảm giá:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(615, 17);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(73, 16);
            this.label30.TabIndex = 4;
            this.label30.Text = "Thành tiền:";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(97, 41);
            this.txtContent.MaxLength = 1000;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(492, 46);
            this.txtContent.TabIndex = 7;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 43);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(65, 16);
            this.label31.TabIndex = 6;
            this.label31.Text = "Nội dung:";
            // 
            // cboVoucherType
            // 
            this.cboVoucherType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVoucherType.DropDownWidth = 250;
            this.cboVoucherType.Location = new System.Drawing.Point(345, 13);
            this.cboVoucherType.Name = "cboVoucherType";
            this.cboVoucherType.Size = new System.Drawing.Size(244, 24);
            this.cboVoucherType.TabIndex = 3;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(274, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(65, 16);
            this.label33.TabIndex = 2;
            this.label33.Text = "Hình thức:";
            // 
            // txtVoucherID
            // 
            this.txtVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtVoucherID.Location = new System.Drawing.Point(97, 14);
            this.txtVoucherID.Name = "txtVoucherID";
            this.txtVoucherID.ReadOnly = true;
            this.txtVoucherID.Size = new System.Drawing.Size(165, 22);
            this.txtVoucherID.TabIndex = 1;
            this.txtVoucherID.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 17);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(66, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "Mã phiếu:";
            // 
            // tabAttachFiles
            // 
            this.tabAttachFiles.BackColor = System.Drawing.SystemColors.Control;
            this.tabAttachFiles.Controls.Add(this.grdAttachment);
            this.tabAttachFiles.Location = new System.Drawing.Point(4, 25);
            this.tabAttachFiles.Name = "tabAttachFiles";
            this.tabAttachFiles.Padding = new System.Windows.Forms.Padding(3);
            this.tabAttachFiles.Size = new System.Drawing.Size(1064, 534);
            this.tabAttachFiles.TabIndex = 1;
            this.tabAttachFiles.Text = "Thông tin đính kèm";
            // 
            // grdAttachment
            // 
            this.grdAttachment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdAttachment.ContextMenuStrip = this.mnuAttachment;
            this.grdAttachment.Location = new System.Drawing.Point(3, 3);
            this.grdAttachment.MainView = this.grvAttachment;
            this.grdAttachment.Name = "grdAttachment";
            this.grdAttachment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDownload,
            this.btnView});
            this.grdAttachment.Size = new System.Drawing.Size(1058, 255);
            this.grdAttachment.TabIndex = 1;
            this.grdAttachment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvAttachment});
            // 
            // mnuAttachment
            // 
            this.mnuAttachment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAddAttachment,
            this.mnuItemScanImage,
            this.mnuItemCaptureImage,
            this.toolStripSeparator2,
            this.mnuDelAttachment});
            this.mnuAttachment.Name = "mnuFlex";
            this.mnuAttachment.Size = new System.Drawing.Size(143, 98);
            this.mnuAttachment.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAttachment_Opening);
            // 
            // mnuAddAttachment
            // 
            this.mnuAddAttachment.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.mnuAddAttachment.Name = "mnuAddAttachment";
            this.mnuAddAttachment.Size = new System.Drawing.Size(142, 22);
            this.mnuAddAttachment.Text = "Thêm tập tin";
            this.mnuAddAttachment.Click += new System.EventHandler(this.mnuAddAttachment_Click);
            // 
            // mnuItemScanImage
            // 
            this.mnuItemScanImage.Image = global::ERP.Inventory.DUI.Properties.Resources.Scan_image;
            this.mnuItemScanImage.Name = "mnuItemScanImage";
            this.mnuItemScanImage.Size = new System.Drawing.Size(142, 22);
            this.mnuItemScanImage.Text = "Scan ảnh";
            this.mnuItemScanImage.Click += new System.EventHandler(this.mnuItemScanImage_Click);
            // 
            // mnuItemCaptureImage
            // 
            this.mnuItemCaptureImage.Image = global::ERP.Inventory.DUI.Properties.Resources.Pictures_icon;
            this.mnuItemCaptureImage.Name = "mnuItemCaptureImage";
            this.mnuItemCaptureImage.Size = new System.Drawing.Size(142, 22);
            this.mnuItemCaptureImage.Text = "Chụp ảnh";
            this.mnuItemCaptureImage.Click += new System.EventHandler(this.mnuItemCaptureImage_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(139, 6);
            // 
            // mnuDelAttachment
            // 
            this.mnuDelAttachment.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.mnuDelAttachment.Name = "mnuDelAttachment";
            this.mnuDelAttachment.Size = new System.Drawing.Size(142, 22);
            this.mnuDelAttachment.Text = "Xóa tập tin";
            this.mnuDelAttachment.Click += new System.EventHandler(this.mnuDelAttachment_Click);
            // 
            // grvAttachment
            // 
            this.grvAttachment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvAttachment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvAttachment.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvAttachment.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvAttachment.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvAttachment.Appearance.Row.Options.UseFont = true;
            this.grvAttachment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn14,
            this.gridColumn15,
            this.colView,
            this.colDownload});
            this.grvAttachment.GridControl = this.grdAttachment;
            this.grvAttachment.Name = "grvAttachment";
            this.grvAttachment.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Tên tập tin";
            this.gridColumn8.FieldName = "FileName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 228;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Thời gian tạo";
            this.gridColumn14.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn14.FieldName = "CreatedDate";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            this.gridColumn14.Width = 140;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Nhân viên tạo";
            this.gridColumn15.FieldName = "CreatedUser";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 2;
            this.gridColumn15.Width = 240;
            // 
            // colView
            // 
            this.colView.Caption = "Xem";
            this.colView.ColumnEdit = this.btnView;
            this.colView.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colView.Name = "colView";
            this.colView.OptionsColumn.AllowSize = false;
            this.colView.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colView.OptionsColumn.FixedWidth = true;
            this.colView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colView.Visible = true;
            this.colView.VisibleIndex = 3;
            this.colView.Width = 40;
            // 
            // btnView
            // 
            this.btnView.AutoHeight = false;
            this.btnView.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::ERP.Inventory.DUI.Properties.Resources.view, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Xem tập tin", null, null, false)});
            this.btnView.Name = "btnView";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // colDownload
            // 
            this.colDownload.Caption = "Tải";
            this.colDownload.ColumnEdit = this.btnDownload;
            this.colDownload.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colDownload.Name = "colDownload";
            this.colDownload.OptionsColumn.AllowMove = false;
            this.colDownload.OptionsColumn.AllowSize = false;
            this.colDownload.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDownload.OptionsColumn.FixedWidth = true;
            this.colDownload.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colDownload.Visible = true;
            this.colDownload.VisibleIndex = 4;
            this.colDownload.Width = 40;
            // 
            // btnDownload
            // 
            this.btnDownload.AutoHeight = false;
            this.btnDownload.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::ERP.Inventory.DUI.Properties.Resources.Folder_Downloads_icon1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Tải tập tin", null, null, false)});
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // grpAttachment
            // 
            this.grpAttachment.Location = new System.Drawing.Point(0, 0);
            this.grpAttachment.Name = "grpAttachment";
            this.grpAttachment.Size = new System.Drawing.Size(200, 100);
            this.grpAttachment.TabIndex = 0;
            this.grpAttachment.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblMessage);
            this.panel1.Controls.Add(this.btnRealInputConfirm);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 532);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1072, 31);
            this.panel1.TabIndex = 24;
            // 
            // lblMessage
            // 
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.Color.Blue;
            this.lblMessage.Location = new System.Drawing.Point(7, 6);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(700, 16);
            this.lblMessage.TabIndex = 29;
            this.lblMessage.Text = "lblMessage";
            // 
            // btnRealInputConfirm
            // 
            this.btnRealInputConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRealInputConfirm.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnRealInputConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRealInputConfirm.Location = new System.Drawing.Point(707, 2);
            this.btnRealInputConfirm.Name = "btnRealInputConfirm";
            this.btnRealInputConfirm.Size = new System.Drawing.Size(152, 25);
            this.btnRealInputConfirm.TabIndex = 8;
            this.btnRealInputConfirm.Text = "    Xác nhận thực nhập";
            this.btnRealInputConfirm.Click += new System.EventHandler(this.btnRealInputConfirm_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(863, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 25);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "   &Hủy";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(967, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 25);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Text = "   Cập &nhật";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // mnuFlex
            // 
            this.mnuFlex.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemIsAutoCreateIMEI,
            this.mnuItemInputIMEI,
            this.mnuItemInputIMEIByProduct,
            this.mnuItemInputbyExcel,
            this.mnuItemIsAutoSeparator,
            this.mnuExportExcel,
            this.mnuImportFile,
            this.toolStripSeparator1,
            this.mnuItemDeleteProduct});
            this.mnuFlex.Name = "mnuFlex";
            this.mnuFlex.Size = new System.Drawing.Size(212, 170);
            this.mnuFlex.Opening += new System.ComponentModel.CancelEventHandler(this.mnuFlex_Opening);
            // 
            // mnuItemIsAutoCreateIMEI
            // 
            this.mnuItemIsAutoCreateIMEI.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuItemIsAutoCreateIMEI.Name = "mnuItemIsAutoCreateIMEI";
            this.mnuItemIsAutoCreateIMEI.Size = new System.Drawing.Size(211, 22);
            this.mnuItemIsAutoCreateIMEI.Text = "Phát sinh IMEI tự động";
            this.mnuItemIsAutoCreateIMEI.Click += new System.EventHandler(this.mnuItemIsAutoCreateIMEI_Click);
            // 
            // mnuItemInputIMEI
            // 
            this.mnuItemInputIMEI.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuItemInputIMEI.Name = "mnuItemInputIMEI";
            this.mnuItemInputIMEI.Size = new System.Drawing.Size(211, 22);
            this.mnuItemInputIMEI.Text = "Nhập IMEI";
            this.mnuItemInputIMEI.Click += new System.EventHandler(this.mnuItemInputIMEI_Click);
            // 
            // mnuItemInputIMEIByProduct
            // 
            this.mnuItemInputIMEIByProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuItemInputIMEIByProduct.Name = "mnuItemInputIMEIByProduct";
            this.mnuItemInputIMEIByProduct.Size = new System.Drawing.Size(211, 22);
            this.mnuItemInputIMEIByProduct.Text = "Nhập IMEI theo sản phẩm";
            this.mnuItemInputIMEIByProduct.Visible = false;
            this.mnuItemInputIMEIByProduct.Click += new System.EventHandler(this.mnuItemInputIMEIByProduct_Click);
            // 
            // mnuItemInputbyExcel
            // 
            this.mnuItemInputbyExcel.Enabled = false;
            this.mnuItemInputbyExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemInputbyExcel.Name = "mnuItemInputbyExcel";
            this.mnuItemInputbyExcel.Size = new System.Drawing.Size(211, 22);
            this.mnuItemInputbyExcel.Text = "Nhập IMEI từ excel";
            this.mnuItemInputbyExcel.Click += new System.EventHandler(this.mnuItemInputbyExcel_Click);
            // 
            // mnuItemIsAutoSeparator
            // 
            this.mnuItemIsAutoSeparator.Name = "mnuItemIsAutoSeparator";
            this.mnuItemIsAutoSeparator.Size = new System.Drawing.Size(208, 6);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.mnu_export_16;
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(211, 22);
            this.mnuExportExcel.Text = "Xuất ra Excel";
            this.mnuExportExcel.Visible = false;
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // mnuImportFile
            // 
            this.mnuImportFile.Image = global::ERP.Inventory.DUI.Properties.Resources.mnu_import_16;
            this.mnuImportFile.Name = "mnuImportFile";
            this.mnuImportFile.Size = new System.Drawing.Size(211, 22);
            this.mnuImportFile.Text = "Nhập từ tập tin";
            this.mnuImportFile.Visible = false;
            this.mnuImportFile.Click += new System.EventHandler(this.mnuImportFile_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(208, 6);
            // 
            // mnuItemDeleteProduct
            // 
            this.mnuItemDeleteProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.mnuItemDeleteProduct.Name = "mnuItemDeleteProduct";
            this.mnuItemDeleteProduct.Size = new System.Drawing.Size(211, 22);
            this.mnuItemDeleteProduct.Text = "Xóa sản phẩm";
            this.mnuItemDeleteProduct.Click += new System.EventHandler(this.mnuItemDeleteProduct_Click);
            // 
            // mnuGridEdit
            // 
            this.mnuGridEdit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportData});
            this.mnuGridEdit.Name = "contextMenuStrip1";
            this.mnuGridEdit.Size = new System.Drawing.Size(165, 26);
            // 
            // mnuItemExportData
            // 
            this.mnuItemExportData.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportData.Name = "mnuItemExportData";
            this.mnuItemExportData.Size = new System.Drawing.Size(164, 22);
            this.mnuItemExportData.Text = "Xuất dữ liệu mẫu";
            this.mnuItemExportData.Click += new System.EventHandler(this.mnuItemExportData_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // grdInputVoucherDetail
            // 
            this.grdInputVoucherDetail.ContextMenuStrip = this.mnuFlex;
            this.grdInputVoucherDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdInputVoucherDetail.Location = new System.Drawing.Point(2, 23);
            this.grdInputVoucherDetail.MainView = this.grvInputVoucherDetail;
            this.grdInputVoucherDetail.Name = "grdInputVoucherDetail";
            this.grdInputVoucherDetail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txtItemQuantity,
            this.txtTaxedFee,
            this.txtInputPrice});
            this.grdInputVoucherDetail.Size = new System.Drawing.Size(1060, 214);
            this.grdInputVoucherDetail.TabIndex = 0;
            this.grdInputVoucherDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvInputVoucherDetail,
            this.gridView1});
            // 
            // grvInputVoucherDetail
            // 
            this.grvInputVoucherDetail.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvInputVoucherDetail.Appearance.FocusedRow.Options.UseFont = true;
            this.grvInputVoucherDetail.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvInputVoucherDetail.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.grvInputVoucherDetail.Appearance.FooterPanel.Options.UseFont = true;
            this.grvInputVoucherDetail.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grvInputVoucherDetail.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvInputVoucherDetail.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvInputVoucherDetail.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvInputVoucherDetail.Appearance.Row.Options.UseFont = true;
            this.grvInputVoucherDetail.Appearance.ViewCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grvInputVoucherDetail.Appearance.ViewCaption.Options.UseFont = true;
            this.grvInputVoucherDetail.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grvInputVoucherDetail.ColumnPanelRowHeight = 20;
            this.grvInputVoucherDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCheck,
            this.colProductID,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.colNKTBH,
            this.colQuantityRemain,
            this.colQuantity,
            this.colQuantityToInput,
            this.colInputPrice,
            this.gridColumn1,
            this.colAmount,
            this.colTaxedFee,
            this.colPurchaseFee,
            this.colProductStatusName});
            this.grvInputVoucherDetail.GridControl = this.grdInputVoucherDetail;
            this.grvInputVoucherDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Amount", this.colAmount, "###,###,###,###.####")});
            this.grvInputVoucherDetail.Name = "grvInputVoucherDetail";
            this.grvInputVoucherDetail.OptionsView.ColumnAutoWidth = false;
            this.grvInputVoucherDetail.OptionsView.ShowFooter = true;
            this.grvInputVoucherDetail.OptionsView.ShowGroupPanel = false;
            this.grvInputVoucherDetail.OptionsView.ShowIndicator = false;
            this.grvInputVoucherDetail.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvInputVoucherDetail_ShowingEditor);
            this.grvInputVoucherDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvInputVoucherDetail_CellValueChanged);
            this.grvInputVoucherDetail.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.grvInputVoucherDetail_InvalidRowException);
            // 
            // colCheck
            // 
            this.colCheck.AppearanceHeader.Options.UseTextOptions = true;
            this.colCheck.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCheck.Caption = "Chọn";
            this.colCheck.FieldName = "IsCheckQuantity";
            this.colCheck.Name = "colCheck";
            this.colCheck.OptionsColumn.AllowEdit = false;
            this.colCheck.Width = 40;
            // 
            // colProductID
            // 
            this.colProductID.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductID.Caption = "Mã hàng";
            this.colProductID.FieldName = "ProductID";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 0;
            this.colProductID.Width = 112;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "Tên hàng";
            this.gridColumn2.FieldName = "ProductName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 206;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "ĐVT";
            this.gridColumn3.FieldName = "QuantityUnitName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 54;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "NSX";
            this.gridColumn4.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn4.FieldName = "ProductIONDate";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 83;
            // 
            // colNKTBH
            // 
            this.colNKTBH.AppearanceHeader.Options.UseTextOptions = true;
            this.colNKTBH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNKTBH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNKTBH.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNKTBH.Caption = "NKTBH";
            this.colNKTBH.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNKTBH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNKTBH.FieldName = "ENDWarrantyDate";
            this.colNKTBH.Name = "colNKTBH";
            this.colNKTBH.OptionsColumn.AllowEdit = false;
            this.colNKTBH.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colNKTBH.Visible = true;
            this.colNKTBH.VisibleIndex = 4;
            this.colNKTBH.Width = 80;
            // 
            // colQuantityRemain
            // 
            this.colQuantityRemain.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityRemain.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityRemain.Caption = "SL cần nhập";
            this.colQuantityRemain.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityRemain.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityRemain.FieldName = "QuantityRemain";
            this.colQuantityRemain.Name = "colQuantityRemain";
            this.colQuantityRemain.OptionsColumn.AllowEdit = false;
            this.colQuantityRemain.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colQuantityRemain.Width = 85;
            // 
            // colQuantity
            // 
            this.colQuantity.AppearanceCell.Options.UseTextOptions = true;
            this.colQuantity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colQuantity.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantity.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colQuantity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantity.Caption = "SL nhập";
            this.colQuantity.ColumnEdit = this.txtItemQuantity;
            this.colQuantity.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colQuantity.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Quantity", "{0:#,##0.####}", new decimal(new int[] {
                            0,
                            0,
                            0,
                            0}))});
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 5;
            this.colQuantity.Width = 70;
            // 
            // txtItemQuantity
            // 
            this.txtItemQuantity.AutoHeight = false;
            this.txtItemQuantity.DisplayFormat.FormatString = "#,##0.####";
            this.txtItemQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtItemQuantity.Mask.EditMask = "###,###,###,##0.####";
            this.txtItemQuantity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtItemQuantity.Name = "txtItemQuantity";
            this.txtItemQuantity.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryItemTextEdit2_EditValueChanging);
            // 
            // colQuantityToInput
            // 
            this.colQuantityToInput.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityToInput.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityToInput.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colQuantityToInput.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityToInput.Caption = "SL còn lại";
            this.colQuantityToInput.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityToInput.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityToInput.FieldName = "QuantityToInput";
            this.colQuantityToInput.Name = "colQuantityToInput";
            this.colQuantityToInput.OptionsColumn.AllowEdit = false;
            this.colQuantityToInput.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colQuantityToInput.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityToInput", "{0:#,##0.####}", "0")});
            this.colQuantityToInput.Width = 70;
            // 
            // colInputPrice
            // 
            this.colInputPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colInputPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colInputPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colInputPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInputPrice.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInputPrice.Caption = "Đơn giá";
            this.colInputPrice.ColumnEdit = this.txtInputPrice;
            this.colInputPrice.DisplayFormat.FormatString = "#,##0.####";
            this.colInputPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colInputPrice.FieldName = "InputPrice";
            this.colInputPrice.Name = "colInputPrice";
            this.colInputPrice.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colInputPrice.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.colInputPrice.Visible = true;
            this.colInputPrice.VisibleIndex = 6;
            this.colInputPrice.Width = 100;
            // 
            // txtInputPrice
            // 
            this.txtInputPrice.AutoHeight = false;
            this.txtInputPrice.DisplayFormat.FormatString = "#,##0.####";
            this.txtInputPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtInputPrice.Mask.EditMask = "###,###,###,##0.####";
            this.txtInputPrice.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInputPrice.Name = "txtInputPrice";
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "VAT";
            this.gridColumn1.DisplayFormat.FormatString = "##0.####";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn1.FieldName = "VAT";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            this.gridColumn1.Width = 50;
            // 
            // colAmount
            // 
            this.colAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAmount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAmount.Caption = "Thành tiền";
            this.colAmount.DisplayFormat.FormatString = "#,##0.####";
            this.colAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colAmount.FieldName = "Amount";
            this.colAmount.Name = "colAmount";
            this.colAmount.OptionsColumn.AllowEdit = false;
            this.colAmount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Amount", "{0:#,##0.####}", 0D)});
            this.colAmount.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.colAmount.Visible = true;
            this.colAmount.VisibleIndex = 8;
            this.colAmount.Width = 130;
            // 
            // colTaxedFee
            // 
            this.colTaxedFee.AppearanceHeader.Options.UseTextOptions = true;
            this.colTaxedFee.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTaxedFee.Caption = "Chi phí tính thuế";
            this.colTaxedFee.ColumnEdit = this.txtTaxedFee;
            this.colTaxedFee.DisplayFormat.FormatString = "#,##0.####";
            this.colTaxedFee.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTaxedFee.FieldName = "TaxedFee";
            this.colTaxedFee.Name = "colTaxedFee";
            this.colTaxedFee.Visible = true;
            this.colTaxedFee.VisibleIndex = 9;
            this.colTaxedFee.Width = 135;
            // 
            // txtTaxedFee
            // 
            this.txtTaxedFee.AutoHeight = false;
            this.txtTaxedFee.DisplayFormat.FormatString = "#,##0.##";
            this.txtTaxedFee.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTaxedFee.Mask.EditMask = "###,###,###,##0.####";
            this.txtTaxedFee.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTaxedFee.Name = "txtTaxedFee";
            // 
            // colPurchaseFee
            // 
            this.colPurchaseFee.AppearanceHeader.Options.UseTextOptions = true;
            this.colPurchaseFee.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPurchaseFee.Caption = "Chi phí mua hàng";
            this.colPurchaseFee.ColumnEdit = this.txtTaxedFee;
            this.colPurchaseFee.FieldName = "PurchaseFee";
            this.colPurchaseFee.Name = "colPurchaseFee";
            this.colPurchaseFee.Visible = true;
            this.colPurchaseFee.VisibleIndex = 10;
            this.colPurchaseFee.Width = 120;
            // 
            // colProductStatusName
            // 
            this.colProductStatusName.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductStatusName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductStatusName.Caption = "Trạng thái sản phẩm";
            this.colProductStatusName.FieldName = "ProductStatusName";
            this.colProductStatusName.Name = "colProductStatusName";
            this.colProductStatusName.Width = 180;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.grdInputVoucherDetail;
            this.gridView1.Name = "gridView1";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.grdInputVoucherDetail);
            this.groupControl1.Location = new System.Drawing.Point(5, 289);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1064, 239);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Chi tiết phiếu nhập";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Mô tả";
            this.gridColumn9.FieldName = "Description";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 322;
            // 
            // frmInputVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 563);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(970, 601);
            this.Name = "frmInputVoucher";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin phiếu nhập";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmInputVoucher_FormClosing);
            this.Load += new System.EventHandler(this.frmInputVoucher_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtmDeliveryNoteDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmDeliveryNoteDate.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtInvoiceDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInvoiceDate.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProtectPriceDiscount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountInput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckRealInputNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchangeRate.Properties)).EndInit();
            this.tabCustomer.ResumeLayout(false);
            this.grpCustomer.ResumeLayout(false);
            this.grpCustomer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerIDCard.Properties)).EndInit();
            this.tabOutVoucher.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashUSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardSpend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashUSDExchange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDebt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashVND.Properties)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLiquidate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalMoney.Properties)).EndInit();
            this.tabAttachFiles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).EndInit();
            this.mnuAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).EndInit();
            this.panel1.ResumeLayout(false);
            this.mnuFlex.ResumeLayout(false);
            this.mnuGridEdit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdInputVoucherDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvInputVoucherDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxedFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private Library.AppControl.TextBoxControl.ASCIIText txtDenominator;
        private System.Windows.Forms.ComboBox cboDiscountReason;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cboCurrencyUnit;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnSearchProduct;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtInputContent;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtInputDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtInputVoucherID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOrderID;
        private Library.AppControl.TextBoxControl.ASCIIText txtInvoiceSymbol;
        private System.Windows.Forms.DateTimePicker dtPayableTime;
        private System.Windows.Forms.ComboBox cboPayableType;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabCustomer;
        private System.Windows.Forms.GroupBox grpCustomer;
        private System.Windows.Forms.TextBox txtIDCardIssuePlace;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dtIDCardIssueDate;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblTaxID;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TabPage tabOutVoucher;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private System.Windows.Forms.ComboBox cboPaymentCard;
        private System.Windows.Forms.DateTimePicker dtSettlementDate;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cboVoucherType;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtVoucherID;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage tabAttachFiles;
        private System.Windows.Forms.GroupBox grpAttachment;
        private System.Windows.Forms.RadioButton radOld;
        private System.Windows.Forms.RadioButton radIsNew;
        private DevExpress.XtraEditors.TextEdit txtPaymentCard;
        private DevExpress.XtraEditors.TextEdit txtCardSpend;
        private DevExpress.XtraEditors.TextEdit txtDebt;
        private DevExpress.XtraEditors.TextEdit txtCashVND;
        private DevExpress.XtraEditors.TextEdit txtTotalLiquidate;
        private DevExpress.XtraEditors.TextEdit txtDiscount;
        private DevExpress.XtraEditors.TextEdit txtTotalMoney;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.TextEdit txtExchangeRate;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ContextMenuStrip mnuFlex;
        private System.Windows.Forms.ToolStripMenuItem mnuItemIsAutoCreateIMEI;
        private System.Windows.Forms.ToolStripMenuItem mnuItemInputIMEI;
        private System.Windows.Forms.ToolStripSeparator mnuItemIsAutoSeparator;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuImportFile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDeleteProduct;
        private DevExpress.XtraEditors.DateEdit dtInvoiceDate;
        private DevExpress.XtraGrid.GridControl grdAttachment;
        private DevExpress.XtraGrid.Views.Grid.GridView grvAttachment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colDownload;
        private System.Windows.Forms.ContextMenuStrip mnuAttachment;
        private System.Windows.Forms.ToolStripMenuItem mnuAddAttachment;
        private System.Windows.Forms.ToolStripMenuItem mnuDelAttachment;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDownload;
        private DevExpress.XtraEditors.TextEdit txtCustomerIDCard;
        private DevExpress.XtraEditors.TextEdit txtTaxID;
        private DevExpress.XtraEditors.TextEdit txtPhone;
        private System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.LinkLabel lblCustomerID;
        private System.Windows.Forms.Button btnDelete;
        private Library.AppControl.TextBoxControl.ASCIIText txtInVoiceID;
        private System.Windows.Forms.ToolTip toolTip1;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomer cboTransportCustomer;
        private System.Windows.Forms.Label label13;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreSearch;
        private Library.AppControl.ComboBoxControl.ComboBoxInputType cboInputTypeSearch;
        private System.Windows.Forms.CheckBox chkIsCheckRealInput;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.ContextMenuStrip mnuGridEdit;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportData;
        private DevExpress.XtraEditors.TextEdit txtCashUSDExchange;
        private DevExpress.XtraEditors.TextEdit txtCashUSD;
        private DevExpress.XtraEditors.TextEdit txtProtectPriceDiscount;
        private DevExpress.XtraEditors.TextEdit txtDiscountInput;
        private System.Windows.Forms.ToolStripMenuItem mnuItemScanImage;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCaptureImage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private DevExpress.XtraGrid.Columns.GridColumn colView;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnView;
        private System.Windows.Forms.ToolStripMenuItem mnuItemInputIMEIByProduct;
        private System.Windows.Forms.ToolStripMenuItem mnuItemInputbyExcel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkCreateAutoInvoice;
        private System.Windows.Forms.Button btnRealInputConfirm;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.TextEdit txtCheckRealInputNote;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraGrid.GridControl grdInputVoucherDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView grvInputVoucherDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colCheck;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colNKTBH;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityRemain;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtItemQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityToInput;
        private DevExpress.XtraGrid.Columns.GridColumn colInputPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtInputPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxedFee;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtTaxedFee;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseFee;
        private DevExpress.XtraGrid.Columns.GridColumn colProductStatusName;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.DateEdit dtmDeliveryNoteDate;
        private System.Windows.Forms.Label label24;
    }
}