﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Collections;
namespace ERP.Inventory.DUI.Input
{
    /// <summary>
    /// Created by: Nguyễn Linh Tuấn
    /// Desc: Quản lý phiếu nhập
    /// 
    /// LÊ VĂN ĐÔNG: 11/12/2017 -> Ẩn/Hiện menuitem in phiếu nhập và phiếu nhập 3 liên theo trạng thái thực nhập
    /// </summary>
    public partial class frmInputVoucherManager : Form
    {
        public frmInputVoucherManager()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvData);
        }

        #region Variable

        Hashtable htInputVoucherSelect = new Hashtable();
        private const string strPerrmissionRealInput = "PM_INPUTVOUCHER_INPUTVALIDATION";
        private const string strPerrmissionManualCreate = "PM_INPUTVOUCHER_MANUAL_CREATE";
        private bool bolPermissionRealInput = false;
        private DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
        private DataTable dtbInputTypeAll = Library.AppCore.DataSource.GetDataSource.GetInputType().Copy();
        #endregion

        #region phương thức
        /// <summary>
        /// Cập nhật thông tin thực nhập
        /// </summary>
        /// <returns></returns>
        private bool UpdateCheckRealInput()
        {
            try
            {
                List<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher> objInputVoucherBOList = new List<PLC.Input.WSInputVoucher.InputVoucher>();
                DataTable dtbData = grdData.DataSource as DataTable;
                foreach (DataRow item in dtbData.Rows)
                {
                    if (Convert.ToBoolean(item["IsEdit"]) && Convert.ToBoolean(item["IsCheckRealInput"]))
                    {
                        ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objInputVoucherBO = new PLC.Input.WSInputVoucher.InputVoucher();
                        objInputVoucherBO.InputVoucherID = Convert.ToString(item["InputVoucherID"]).Trim();
                        objInputVoucherBO.IsCheckRealInput = Convert.ToBoolean(item["IsCheckRealInput"]);
                        objInputVoucherBO.CheckRealInputUser = SystemConfig.objSessionUser.UserName;
                        objInputVoucherBO.CheckRealInputNote = Convert.ToString(item["CheckRealInputNote"]);
                        objInputVoucherBOList.Add(objInputVoucherBO);
                        item["IsEdit"] = false;
                    }
                }
                if (objInputVoucherBOList.Count <= 0)
                    return false;
                PLC.Input.PLCInputVoucher objInputVoucher = new PLC.Input.PLCInputVoucher();
                objInputVoucher.UpdateCheckRealInput(objInputVoucherBOList);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi cập nhật thông tin kiểm tra thực nhập", objExce, DUIInventory_Globals.ModuleName);
                return false;
            }
            grdData.RefreshDataSource();
            return true;
        }

        /// <summary>
        /// Nạp dữ liệu vào các combobox
        /// </summary>
        private void LoadComboBox()
        {
            try
            {
                cboInputType.InitControl(true);
                cboCustomer.InitControl(false, Library.AppCore.Constant.EnumType.CustomerType.ISPROVIDER);
                cboBranch.InitControl(true);
                //Hiếu sửa chọn nhiều kho
                //cboStore.InitControl(false, objStoreFilter);
                cboStore.InitControl(true, dtbStore,"STOREID","STORENAME","-- Tất cả --");
                cboStore.IsReturnAllWhenNotChoose = true;
                cboMainGroup.InitControl(true, true, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.ALL, Library.AppCore.Constant.EnumType.MainGroupPermissionType.INPUT);
                dtInputStart.DateTime = DateTime.Now;
                dtInputEnd.DateTime = DateTime.Now;
                txtTop.Text = "500";
                cboSearchType.SelectedIndex = 0;
                btnUpdate.Enabled = false;
                
                object[] objKeywords = new object[] { "@Keywords", "",
                                        "@DOCUMENTTYPEID", 2,// 1: Hóa đơn, 2: Nhập, 3: Xuất, 4: Thu/Chi
                                        "@IsDelete", 0};
                DataTable dtbData = null;
                dtbData = new MasterData.VAT.PLC.VAT.PLCDataReportType().SearchData(objKeywords);
                if (dtbData != null)
                {
                    cboDataReport.InitControl(true, dtbData, "DATAREPORTTYPEID", "DATAREPORTTYPENAME", "Tất cả");
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nạp dữ liệu vào các combobox", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi khởi tạo dữ liệu tìm kiếm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        /// <summary>
        /// Kiểm tra dữ liệu trước khi tìm kiếm
        /// </summary>
        private bool ValidatingSearch()
        {
            string strResult = Library.AppCore.Other.CheckObject.CheckMonthView(Library.AppCore.Constant.EnumType.Month_BussinessType.INVENTORY_INPUT,
                dtInputStart.DateTime, dtInputEnd.DateTime);
            if (!string.IsNullOrEmpty(strResult))
            {
                MessageBox.Show(strResult, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtInputEnd.Focus();
                return false;
            }
            if (cboSearchType.SelectedIndex == 6 && txtSearch.Text.Length < 5)
            {
                MessageBox.Show(this, "Vui lòng nhập ít nhất 5 ký tự khi tìm kiếm theo IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtSearch.Focus();
                return false;
            }
            return true;
        }

        #endregion

        #region Events

        private void frmInputVoucherManager_Load(object sender, EventArgs e)
        {
            bolPermissionRealInput = SystemConfig.objSessionUser.IsPermission(strPerrmissionRealInput);
            btnAddNew.Enabled = SystemConfig.objSessionUser.IsPermission(strPerrmissionManualCreate);
            LoadComboBox();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!ValidatingSearch())
                return;
            //Tạo trạng thái chờ trong khi tạo báo cáo
            this.Cursor = Cursors.WaitCursor;
            btnSearch.Enabled = false;
            this.Refresh();
            bool bolResult = false;
            try
            {

                //Hiếu sửa lấy nhiều kho
                string strMainGroupIDList = cboMainGroup.MainGroupIDList;
                //string strStoreIDList = Library.AppCore.Other.ConvertObject.ConvertIDListBracesToParentheses(cboStore.StoreIDList).Replace("(", "").Replace(")", "");
                //= cboStore.StoreIDList.Trim('<','>').Replace('><',',');
                string strInputType = cboInputType.InputTypeIDList;
                string strKeyword = txtSearch.Text;
                if (cboSearchType.SelectedIndex == 0 || cboSearchType.SelectedIndex == 1 || cboSearchType.SelectedIndex == 2 || cboSearchType.SelectedIndex == 3 || cboSearchType.SelectedIndex == 5 || cboSearchType.SelectedIndex == 6)
                    strKeyword = Library.AppCore.Globals.FilterVietkey(txtSearch.Text);
                //PLC.Input.PLCInputVoucher objPLCInputVoucher = new PLC.Input.PLCInputVoucher();
                //DataTable dtbTmp = objPLCInputVoucher.SearchData(new object[]
                //                            {
                //                                "@Keyword", strKeyword,
                //                                "@username", SystemConfig.objSessionUser.UserName,
                //                                "@StoreID", strStoreIDList,
                //                                "@CustomerID", cboCustomer.CustomerID,
                //                                "@InputTypeIDList", strInputType.Trim(),
                //                                "@FromDate", dtInputStart.DateTime,
                //                                "@ToDate", dtInputEnd.DateTime,
                //                                "@SearchType", cboSearchType.SelectedIndex,
                //                                "@Top", Convert.ToInt32(txtTop.Text.Trim()),
                //                                "@MainGroupIDList", strMainGroupIDList,
                //                                "@IsDeleted",chkIsDeleted.Checked,
                //                                "@ISVIEWDETAIL",chkIsDetail.Checked
                //                            });

                // DataTable dtbTmp = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_INPUTVOUCHER_SRH3", new object[]

                DataTable dtbTmp = new ERP.Inventory.PLC.Input.PLCInputVoucher().SearchData2(new object[]
                                            {
                                                "@Keyword", strKeyword,
                                                "@username", SystemConfig.objSessionUser.UserName,
                                                "@StoreID", cboStore.ColumnIDList,
                                                "@CustomerID", cboCustomer.CustomerID,
                                                "@InputTypeIDList", strInputType.Trim(),
                                                "@FromDate", dtInputStart.DateTime,
                                                "@ToDate", dtInputEnd.DateTime,
                                                "@SearchType", cboSearchType.SelectedIndex,
                                                "@Top", Convert.ToInt32(txtTop.Text.Trim()),
                                                "@MainGroupIDList", strMainGroupIDList,
                                                "@IsDeleted",chkIsDeleted.Checked,
                                                "@ISVIEWDETAIL",chkIsDetail.Checked
                                            });
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSearch.Enabled = true;
                    return;
                }
                DataColumn colIsEdit = new DataColumn("IsEdit", typeof(bool));
                colIsEdit.DefaultValue = false;
                if (!dtbTmp.Columns.Contains(colIsEdit.ColumnName))
                    dtbTmp.Columns.Add(colIsEdit);
                if (chkIsDetail.Checked)
                {
                    colTOTALAMOUNT.Caption = "Thành tiền";
                    colPRODUCTID.Visible = true;
                    colPRODUCTID.VisibleIndex = colCUSTOMERADDRESS.VisibleIndex + 1;
                    colPRODUCTNAME.Visible = true;
                    colPRODUCTNAME.VisibleIndex = colPRODUCTID.VisibleIndex + 1;
                    colIMEI.Visible = true;
                    colIMEI.VisibleIndex = colPRODUCTNAME.VisibleIndex + 1;
                    colQuantity.Visible = true;
                    colQuantity.VisibleIndex = gridColumn11.VisibleIndex + 1;
                    colDISCOUNT.Visible = false;
                    colDISCOUNTREASONNAME.Visible = false;
                    colPROTECTPRICEDISCOUNT.Visible = false;
                    gridColumn19.Visible = false;
                    colNote.VisibleIndex = gridColumn20.VisibleIndex + 1;
                }
                else
                {
                    colTOTALAMOUNT.Caption = "Tổng tiền";
                    colDISCOUNT.Visible = true;
                    colDISCOUNT.VisibleIndex = colTOTALAMOUNT.VisibleIndex + 1;
                    colPROTECTPRICEDISCOUNT.Visible = true;
                    colPROTECTPRICEDISCOUNT.VisibleIndex = colDISCOUNT.VisibleIndex + 1;
                    colDISCOUNTREASONNAME.Visible = true;
                    colDISCOUNTREASONNAME.VisibleIndex = colCURRENCYEXCHANGE.VisibleIndex + 1;
                    gridColumn19.Visible = false;
                    gridColumn19.VisibleIndex = colDISCOUNTREASONNAME.VisibleIndex + 1;
                    colPRODUCTID.Visible = false;
                    colPRODUCTNAME.Visible = false;
                    colQuantity.Visible = false;
                    colIMEI.Visible = false;
                    colNote.VisibleIndex = gridColumn20.VisibleIndex + 1;
                }
                grdData.DataSource = dtbTmp;
                bolResult = true;
            }
            catch (Exception objExe)
            {
                SystemErrorWS.Insert("Tham số tìm kiếm không chính xác!\nBạn vui lòng kiểm tra lại!", objExe, DUIInventory_Globals.ModuleName);
                bolResult = false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            if (!bolResult)
                MessageBox.Show(this, "Lỗi tìm kiếm thông tin phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            VisibleDeletedInfo(chkIsDeleted.Checked);
            btnSearch.Enabled = true;
            btnUpdate.Enabled = false;
            this.Refresh();
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            mnuViewInputVoucher_Click(null, null);
        }
        private void VisibleDeletedInfo(bool bolValue)
        {
            gridColContentDeleted.Visible = bolValue;
            gridColDeletedUser.Visible = bolValue;
            gridColDeletedDate.Visible = bolValue;
        }
        /// <summary>
        /// Xuất excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void mnuViewInputVoucher_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            if (drFocus != null)
            {
                frmInputVoucher frm = new frmInputVoucher();
                frm.InputVoucherID = drFocus["InputVoucherID"].ToString();
                frm.IsEdit = true;
                frm.bolIsReloadManager = false;
                frm.LoadControl();
                frm.ShowDialog();
                if (frm.bolIsReloadManager)
                    btnSearch_Click(null, null);
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            frmInputVoucher frm = new frmInputVoucher();
            frm.ShowDialog();
        }

        private void mnuPrintInputVoucher_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            if (drFocus != null)
            {
                Reports.Input.frmReportView frm = new Reports.Input.frmReportView();
                frm.InputVoucherID = drFocus["InputVoucherID"].ToString();

                frm.bolIsPriceHide = false;
                frm.ShowDialog();
            }
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (UpdateCheckRealInput())
            {
                MessageBox.Show(this, "Đã cập nhật dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void ItemchkIsCheckRealInput_CheckedChanged(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle >= 0)
            {

                DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);

                if (!bolPermissionRealInput)
                {
                    MessageBox.Show("Bạn không có quyền xác nhận thực nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    if (Convert.ToBoolean(row["ISCHECKREALINPUT"]))
                        grvData.SetRowCellValue(grvData.FocusedRowHandle, "ISCHECKREALINPUT", 0);
                    return;
                }
                if (!Convert.ToBoolean(row["ISCHECKREALINPUT"]))
                {
                    row["IsEdit"] = true;
                    //btnUpdate.Enabled = true;
                    grvData.SetRowCellValue(grvData.FocusedRowHandle, "IsEdit", 1);
                    grvData.SetRowCellValue(grvData.FocusedRowHandle, "ISCHECKREALINPUT", 1);
                    if (!htInputVoucherSelect.Contains(row["InputVoucherID"].ToString().Trim()))
                        htInputVoucherSelect.Add(row["InputVoucherID"].ToString().Trim(), row["InputVoucherID"].ToString().Trim());
                }
                else
                {
                    row["IsEdit"] = false;
                    //btnUpdate.Enabled = false;
                    grvData.SetRowCellValue(grvData.FocusedRowHandle, "IsEdit", 0);
                    grvData.SetRowCellValue(grvData.FocusedRowHandle, "ISCHECKREALINPUT", 0);
                    htInputVoucherSelect.Remove(row["InputVoucherID"].ToString().Trim());
                    grvData.SetRowCellValue(grvData.FocusedRowHandle, "CHECKREALINPUTNOTE", string.Empty);
                }
                grdData.RefreshDataSource();
            }
            if (htInputVoucherSelect.Count == 0)
                btnUpdate.Enabled = false;
            else
                btnUpdate.Enabled = true;
        }
        private void grvData_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                DataRow row = (DataRow)grvData.GetDataRow(e.RowHandle);
                if (e.Column.FieldName == "CHECKREALINPUTNOTE")
                {
                    if (!string.IsNullOrEmpty(row["CHECKREALINPUTNOTE"].ToString()))
                    {
                        row["IsEdit"] = true;
                        //btnUpdate.Enabled = true;
                        if (!htInputVoucherSelect.Contains(row["InputVoucherID"].ToString().Trim()))
                            htInputVoucherSelect.Add(row["InputVoucherID"].ToString().Trim(), row["InputVoucherID"].ToString().Trim());
                    }
                    else
                    {
                        row["IsEdit"] = false;
                        //btnUpdate.Enabled = false;
                        if (!Convert.ToBoolean(row["ISCHECKREALINPUT"]))
                            htInputVoucherSelect.Remove(row["InputVoucherID"].ToString().Trim());

                    }
                }
            }
            if (htInputVoucherSelect.Count == 0)
                btnUpdate.Enabled = false;
            else
                btnUpdate.Enabled = true;
        }

        private void grvData_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (grvData.FocusedRowHandle >= 0)
            {
                DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
                if (Convert.ToBoolean(row["IsCheckRealInput"]) && !Convert.ToBoolean(row["IsEdit"]))
                {
                    e.Cancel = true;
                }
                // Nhập chuyển kho thì không cho thay đổi trạng thái xác nhận nhập kho
                if (grvData.FocusedColumn.FieldName == "ISCHECKREALINPUT")
                {
                    if (row["IsStoreChange"] != null)
                    {
                        string t = row["IsStoreChange"].ToString();
                        if (Convert.ToBoolean(row["IsStoreChange"]))
                        {
                            MessageBox.Show("Chỉ xác nhận thực nhập ở Quản lý chuyển kho.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            e.Cancel = true;
                        }
                    }
                    if (!bolPermissionRealInput)
                    {
                        MessageBox.Show("Bạn không có quyền xác nhận thực nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                    }
                }
                else if (grvData.FocusedColumn.FieldName == "CHECKREALINPUTNOTE")
                {
                    if (!Convert.ToBoolean(row["ISCHECKREALINPUT"]))
                    {
                        e.Cancel = true;

                    }
                }
            }

        }


        #endregion

        private void mnuItemViewVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvData.FocusedRowHandle >= 0)
                {
                    DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
                    if (Convert.IsDBNull(row["VoucherID"]) || row["VoucherID"].ToString().Trim() == string.Empty)
                        return;
                    ERP.SalesAndServices.Payment.DUI.Payment.frmVoucher frm = new SalesAndServices.Payment.DUI.Payment.frmVoucher();
                    frm.Tag = "IsSpend=true";
                    frm.VoucherID = row["VoucherID"].ToString();
                    frm.ShowDialog();
                    btnSearch_Click(null, null);
                }
            }
            catch (Exception objExe)
            {
                SystemErrorWS.Insert("Lỗi xem thông tin phiếu chi!", objExe, DUIInventory_Globals.ModuleName);
            }
        }

        private void mnuMain_Opening(object sender, CancelEventArgs e)
        {
            if (grvData.FocusedRowHandle >= 0)
            {
                DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
                if (Convert.IsDBNull(row["VoucherID"]) || row["VoucherID"].ToString().Trim() == string.Empty)
                    mnuItemViewVoucher.Enabled = false;
                else
                    mnuItemViewVoucher.Enabled = true;
                int intInputTypeID = Convert.ToInt32(row["InputTypeID"]);
                DataTable dtbData = Library.AppCore.DataSource.GetDataSource.GetProductChangeParam().Copy();
                DataRow[] drData = dtbData.Select("InputTypeID=" + intInputTypeID + " and IsActive=1");
                if (drData != null && drData.Length > 0)
                {
                    mnuItemReturnOutput.Enabled = true;
                }
                else
                    mnuItemReturnOutput.Enabled = false;
                int intInputTypeIDTemp = Library.AppCore.AppConfig.GetIntConfigValue("InputChange_InputTypeID");
                if (intInputTypeIDTemp == intInputTypeID)
                    mnuItemOutputChange.Enabled = true;
                else
                    mnuItemOutputChange.Enabled = false;

                //Disable menu In phiếu nhập nếu 
                int intInputVoucherPrintRealInput = Library.AppCore.AppConfig.GetIntConfigValue("PM_INPUTVOUCHER_PRINTREALINPUT");

                //if (intInputVoucherPrintRealInput == 1 && (Convert.IsDBNull(row["IsCheckRealInput"]) || !Convert.ToBoolean(row["IsCheckRealInput"]))) //Lớn hơn 0 (có hoặc =1)
                //    mnuPrintInputVoucher.Enabled = mnuInputVoicherInKim.Enabled = false;
                //else
                //    mnuPrintInputVoucher.Enabled = mnuInputVoicherInKim.Enabled = true;

                // LÊ VĂN ĐÔNG: 11/12/2017 -> Ẩn/Hiện menuitem in phiếu nhập và phiếu nhập 3 liên theo trạng thái thực nhập
                if (intInputVoucherPrintRealInput == 1 && (Convert.IsDBNull(row["ISCHECKREALINPUTOLD"]) || !Convert.ToBoolean(row["ISCHECKREALINPUTOLD"]))) //Lớn hơn 0 (có hoặc =1)
                    mnuPrintInputVoucher.Enabled = mnuInputVoicherInKim.Enabled = false;
                else
                    mnuPrintInputVoucher.Enabled = mnuInputVoicherInKim.Enabled = true;

            }
        }

        private void mnuItemReturnOutput_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle >= 0)
            {
                DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
                PLC.ProductChange.WSProductChange.ProductChange objProductChange = new PLC.ProductChange.PLCProductChange().LoadInfo(Convert.ToString(row["InputVoucherID"]));
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSearch.Enabled = true;
                    return;
                }
                Output.frmOutputVoucher frmOutputVoucher1 = new Output.frmOutputVoucher();
                frmOutputVoucher1.strOutputVoucherID = objProductChange.OutputVoucherID;
                frmOutputVoucher1.ShowDialog();
            }
        }

        private void mnuItemOutputChange_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle >= 0)
            {
                DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
                PLC.InputChange.WSInputChange.InputChange objInputChange = new PLC.InputChange.PLCInputChange().LoadInfo(Convert.ToString(row["InputVoucherID"]));
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSearch.Enabled = true;
                    return;
                }
                Output.frmOutputVoucher frmOutputVoucher1 = new Output.frmOutputVoucher();
                frmOutputVoucher1.strOutputVoucherID = objInputChange.NewOutputVoucherID;
                frmOutputVoucher1.ShowDialog();
            }
        }

        private void cboSearchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strOldKeyWords = string.Empty;
            strOldKeyWords = txtSearch.Text.Trim();
            if (cboSearchType.SelectedIndex == 0 || cboSearchType.SelectedIndex == 1 || cboSearchType.SelectedIndex == 2 || cboSearchType.SelectedIndex == 5) //Nếu tìm theo mã phiếu, số HĐ, mã đơn hàng, mã sản phẩm
            {
                if (txtSearch.Text.Trim().Length > 20)
                    txtSearch.Text = strOldKeyWords.Substring(0, 20);
                txtSearch.Properties.MaxLength = 20;
            }
            else if (cboSearchType.SelectedIndex == 3) //Nếu tìm theo mã nhân viên
            {
                if (txtSearch.Text.Trim().Length > 20)
                    txtSearch.Text = strOldKeyWords.Substring(0, 20);
                txtSearch.Properties.MaxLength = 20;
            }
            else if (cboSearchType.SelectedIndex == 4) //Nếu tìm theo tên nhân viên
            {
                if (txtSearch.Text.Trim().Length > 200)
                    txtSearch.Text = strOldKeyWords.Substring(0, 200);
                txtSearch.Properties.MaxLength = 200;
            }
            else
            {
                if (txtSearch.Text.Trim().Length > 50)
                    txtSearch.Text = strOldKeyWords.Substring(0, 50);
                txtSearch.Properties.MaxLength = 50; //Nếu tìm theo IMEI
            }

        }

        private void mnuInputVoucherInKim_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            if (drFocus != null)
            {
                Reports.Input.frmReportView frm = new Reports.Input.frmReportView();
                frm.InputVoucherID = drFocus["InputVoucherID"].ToString();
                frm.bolIsPriceHide = false;
                frm.intIsInputVoucherInKim = 1;
                DataTable dtData = new PLC.Input.PLCInputVoucher().Report_GetInputVoucherData(frm.InputVoucherID);
                int countRow = 0;

                for (int i = 0; i < dtData.Rows.Count; i++)
                {

                    string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();

                    Font font = new Font("Arial", 9.0f);
                    int rowProductName = frm.GetNumOfLines(strProductName, 160, new Font(font, FontStyle.Bold));
                    countRow += rowProductName;
                }
                if (countRow > 15)
                {
                    MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                frm.ShowDialog();

            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(null, null);
        }

        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cboBranch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cboBranch.BranchIDList))
            {
                if (dtbStore != null)
                {
                    cboStore.InitControl(true, dtbStore, "STOREID", "STORENAME", "--Tất cả--");
                }
            }
            else
            {
                var lst = dtbStore.Select("'" + cboBranch.BranchIDList + "' LIKE '%<' + BRANCHID + '>%'");
                if (lst.Any())
                {
                    DataTable dtb = lst.CopyToDataTable();
                    cboStore.InitControl(true, dtb, "STOREID", "STORENAME", "--Tất cả--");
                }
            }
        }

        private void cboDataReport_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cboDataReport.ColumnIDList))
            {
                cboInputType.InitControl(true, dtbInputTypeAll);
                cboInputType.IsReturnAllWhenNotChoose = true;
            }
            else
            {
                object[] objKeywords = new object[] { "@DATAREPORTTYPEIDLIST", cboDataReport.ColumnIDList };
                DataTable dtbInPutType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_DATAREPORTTYPE_INTYPE_GET", objKeywords);
                if (dtbInPutType != null)
                {
                    cboInputType.InitControl(true, dtbInPutType);
                    cboInputType.IsReturnAllWhenNotChoose = true;
                }
            }
        }
    }
}
