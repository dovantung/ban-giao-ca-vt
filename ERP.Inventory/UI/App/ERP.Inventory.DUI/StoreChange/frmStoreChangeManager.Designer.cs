﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmStoreChangeManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStoreChangeManager));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cboStatusList = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.llblDate = new System.Windows.Forms.LinkLabel();
            this.cboSearchBy = new System.Windows.Forms.ComboBox();
            this.cboMainGroup = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cboToStoreSearch = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboFromStoreSearch = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.cboStoreChangeType = new System.Windows.Forms.ComboBox();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.chkIsViewDetail = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtKeyword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.lbTotalQuantity = new C1.Win.C1Input.C1NumericEdit();
            this.lbTotalWeight = new C1.Win.C1Input.C1NumericEdit();
            this.lbTotalQuantitySelected = new C1.Win.C1Input.C1NumericEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbTotalSizeSelected = new C1.Win.C1Input.C1NumericEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.lbTotalSize = new C1.Win.C1Input.C1NumericEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.lbTotalWeightSelected = new C1.Win.C1Input.C1NumericEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.flexStoreChange = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuPopUp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemPrintStoreAddressA4 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintStoreAddressA5 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintStoreAddressA6 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintInternalOutput_Transfer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintStoreChangeCommand = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintInternalOuput = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintStoreChangeHandOvers = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintReport = new System.Windows.Forms.ToolStripMenuItem();
            this.itemOutputVoucherTransfer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsViewDetail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeyword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalQuantitySelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalSizeSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalWeightSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexStoreChange)).BeginInit();
            this.mnuPopUp.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.cboStatusList);
            this.groupControl1.Controls.Add(this.llblDate);
            this.groupControl1.Controls.Add(this.cboSearchBy);
            this.groupControl1.Controls.Add(this.cboMainGroup);
            this.groupControl1.Controls.Add(this.cboToStoreSearch);
            this.groupControl1.Controls.Add(this.cboFromStoreSearch);
            this.groupControl1.Controls.Add(this.dtpToDate);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl12);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.cboStoreChangeType);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.chkIsViewDetail);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtKeyword);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.dtpFromDate);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(1011, 92);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(232, 37);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(5, 16);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "-";
            // 
            // cboStatusList
            // 
            this.cboStatusList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStatusList.Location = new System.Drawing.Point(431, 33);
            this.cboStatusList.Margin = new System.Windows.Forms.Padding(0);
            this.cboStatusList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStatusList.Name = "cboStatusList";
            this.cboStatusList.Size = new System.Drawing.Size(204, 24);
            this.cboStatusList.TabIndex = 5;
            // 
            // llblDate
            // 
            this.llblDate.AutoSize = true;
            this.llblDate.Location = new System.Drawing.Point(7, 37);
            this.llblDate.Name = "llblDate";
            this.llblDate.Size = new System.Drawing.Size(90, 16);
            this.llblDate.TabIndex = 17;
            this.llblDate.TabStop = true;
            this.llblDate.Text = "Ngày chuyển:";
            this.llblDate.Click += new System.EventHandler(this.llblDate_Click);
            // 
            // cboSearchBy
            // 
            this.cboSearchBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cboSearchBy.FormattingEnabled = true;
            this.cboSearchBy.Items.AddRange(new object[] {
            "Mã phiếu chuyển",
            "Mã thùng hàng",
            "Số hóa đơn",
            "Mã yêu cầu",
            "Mã phiếu nhập",
            "Mã phiếu xuất",
            "Mã sản phẩm",
            "Tên sản phẩm",
            "IMEI",
            "Nhân viên xuất",
            "Nhân viên giao",
            "Nhân viên vận chuyển",
            "Nhân viên ký nhận",
            "Nhân viên thực nhập"});
            this.cboSearchBy.Location = new System.Drawing.Point(431, 60);
            this.cboSearchBy.Name = "cboSearchBy";
            this.cboSearchBy.Size = new System.Drawing.Size(204, 24);
            this.cboSearchBy.TabIndex = 8;
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroup.Location = new System.Drawing.Point(715, 33);
            this.cboMainGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(200, 24);
            this.cboMainGroup.TabIndex = 6;
            // 
            // cboToStoreSearch
            // 
            this.cboToStoreSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreSearch.Location = new System.Drawing.Point(715, 4);
            this.cboToStoreSearch.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreSearch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreSearch.Name = "cboToStoreSearch";
            this.cboToStoreSearch.Size = new System.Drawing.Size(200, 24);
            this.cboToStoreSearch.TabIndex = 2;
            // 
            // cboFromStoreSearch
            // 
            this.cboFromStoreSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreSearch.Location = new System.Drawing.Point(431, 4);
            this.cboFromStoreSearch.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreSearch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreSearch.Name = "cboFromStoreSearch";
            this.cboFromStoreSearch.Size = new System.Drawing.Size(204, 24);
            this.cboFromStoreSearch.TabIndex = 1;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CalendarFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(244, 34);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(106, 22);
            this.dtpToDate.TabIndex = 4;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(10, 64);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(59, 16);
            this.labelControl9.TabIndex = 14;
            this.labelControl9.Text = "Chuỗi tìm:";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(364, 64);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(56, 16);
            this.labelControl12.TabIndex = 14;
            this.labelControl12.Text = "Tìm theo:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(364, 37);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(64, 16);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "Trạng thái:";
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Appearance.Options.UseFont = true;
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.Location = new System.Drawing.Point(822, 60);
            this.btnSearch.LookAndFeel.SkinName = "Blue";
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 25);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cboStoreChangeType
            // 
            this.cboStoreChangeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStoreChangeType.DropDownWidth = 270;
            this.cboStoreChangeType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreChangeType.FormattingEnabled = true;
            this.cboStoreChangeType.Location = new System.Drawing.Point(120, 4);
            this.cboStoreChangeType.Name = "cboStoreChangeType";
            this.cboStoreChangeType.Size = new System.Drawing.Size(230, 24);
            this.cboStoreChangeType.TabIndex = 0;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(642, 37);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(73, 16);
            this.labelControl10.TabIndex = 12;
            this.labelControl10.Text = "Ngành hàng:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(642, 8);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(58, 16);
            this.labelControl8.TabIndex = 12;
            this.labelControl8.Text = "Kho nhập:";
            // 
            // chkIsViewDetail
            // 
            this.chkIsViewDetail.Location = new System.Drawing.Point(713, 62);
            this.chkIsViewDetail.Name = "chkIsViewDetail";
            this.chkIsViewDetail.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsViewDetail.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkIsViewDetail.Properties.Appearance.Options.UseFont = true;
            this.chkIsViewDetail.Properties.Appearance.Options.UseForeColor = true;
            this.chkIsViewDetail.Properties.Caption = "Xem chi tiết";
            this.chkIsViewDetail.Properties.LookAndFeel.SkinName = "Blue";
            this.chkIsViewDetail.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.chkIsViewDetail.Size = new System.Drawing.Size(103, 21);
            this.chkIsViewDetail.TabIndex = 9;
            this.chkIsViewDetail.CheckedChanged += new System.EventHandler(this.chkIsViewDetail_CheckedChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(364, 8);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(54, 16);
            this.labelControl7.TabIndex = 5;
            this.labelControl7.Text = "Kho xuất:";
            // 
            // txtKeyword
            // 
            this.txtKeyword.Location = new System.Drawing.Point(120, 61);
            this.txtKeyword.Name = "txtKeyword";
            this.txtKeyword.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtKeyword.Properties.Appearance.Options.UseFont = true;
            this.txtKeyword.Properties.LookAndFeel.SkinName = "Blue";
            this.txtKeyword.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtKeyword.Size = new System.Drawing.Size(230, 22);
            this.txtKeyword.TabIndex = 7;
            this.txtKeyword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyword_KeyPress);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(10, 8);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(96, 16);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Loại chuyển kho:";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CalendarFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(120, 34);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(106, 22);
            this.dtpFromDate.TabIndex = 3;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.btnUpdate);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 549);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1011, 49);
            this.panelControl1.TabIndex = 32;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.lbTotalQuantity);
            this.panelControl2.Controls.Add(this.lbTotalWeight);
            this.panelControl2.Controls.Add(this.lbTotalQuantitySelected);
            this.panelControl2.Controls.Add(this.label16);
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Controls.Add(this.label2);
            this.panelControl2.Controls.Add(this.lbTotalSizeSelected);
            this.panelControl2.Controls.Add(this.label4);
            this.panelControl2.Controls.Add(this.lbTotalSize);
            this.panelControl2.Controls.Add(this.label5);
            this.panelControl2.Controls.Add(this.lbTotalWeightSelected);
            this.panelControl2.Controls.Add(this.label3);
            this.panelControl2.Location = new System.Drawing.Point(5, 4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(817, 40);
            this.panelControl2.TabIndex = 84;
            // 
            // lbTotalQuantity
            // 
            this.lbTotalQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbTotalQuantity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.lbTotalQuantity.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbTotalQuantity.CustomFormat = "###,##0";
            this.lbTotalQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalQuantity.ForeColor = System.Drawing.Color.Red;
            this.lbTotalQuantity.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.lbTotalQuantity.Location = new System.Drawing.Point(161, 3);
            this.lbTotalQuantity.Name = "lbTotalQuantity";
            this.lbTotalQuantity.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.lbTotalQuantity.ReadOnly = true;
            this.lbTotalQuantity.Size = new System.Drawing.Size(46, 14);
            this.lbTotalQuantity.TabIndex = 72;
            this.lbTotalQuantity.TabStop = false;
            this.lbTotalQuantity.Tag = null;
            this.lbTotalQuantity.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lbTotalQuantity.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // lbTotalWeight
            // 
            this.lbTotalWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbTotalWeight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.lbTotalWeight.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbTotalWeight.CustomFormat = "###,##0";
            this.lbTotalWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalWeight.ForeColor = System.Drawing.Color.Red;
            this.lbTotalWeight.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.lbTotalWeight.Location = new System.Drawing.Point(446, 2);
            this.lbTotalWeight.Name = "lbTotalWeight";
            this.lbTotalWeight.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.lbTotalWeight.ReadOnly = true;
            this.lbTotalWeight.Size = new System.Drawing.Size(80, 14);
            this.lbTotalWeight.TabIndex = 73;
            this.lbTotalWeight.TabStop = false;
            this.lbTotalWeight.Tag = null;
            this.lbTotalWeight.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lbTotalWeight.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // lbTotalQuantitySelected
            // 
            this.lbTotalQuantitySelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbTotalQuantitySelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.lbTotalQuantitySelected.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbTotalQuantitySelected.CustomFormat = "###,##0";
            this.lbTotalQuantitySelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalQuantitySelected.ForeColor = System.Drawing.Color.Red;
            this.lbTotalQuantitySelected.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.lbTotalQuantitySelected.Location = new System.Drawing.Point(161, 22);
            this.lbTotalQuantitySelected.Name = "lbTotalQuantitySelected";
            this.lbTotalQuantitySelected.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.lbTotalQuantitySelected.ReadOnly = true;
            this.lbTotalQuantitySelected.Size = new System.Drawing.Size(46, 14);
            this.lbTotalQuantitySelected.TabIndex = 74;
            this.lbTotalQuantitySelected.TabStop = false;
            this.lbTotalQuantitySelected.Tag = null;
            this.lbTotalQuantitySelected.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lbTotalQuantitySelected.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(4, 2);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 15);
            this.label16.TabIndex = 76;
            this.label16.Text = "Tổng số lượng:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(4, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 15);
            this.label1.TabIndex = 78;
            this.label1.Text = "Tổng SL được chọn chuyển:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(218, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 15);
            this.label2.TabIndex = 77;
            this.label2.Text = "Tổng trọng lượng (kg):";
            // 
            // lbTotalSizeSelected
            // 
            this.lbTotalSizeSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbTotalSizeSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.lbTotalSizeSelected.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbTotalSizeSelected.CustomFormat = "###,##0";
            this.lbTotalSizeSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalSizeSelected.ForeColor = System.Drawing.Color.Red;
            this.lbTotalSizeSelected.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.lbTotalSizeSelected.Location = new System.Drawing.Point(727, 22);
            this.lbTotalSizeSelected.Name = "lbTotalSizeSelected";
            this.lbTotalSizeSelected.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.lbTotalSizeSelected.ReadOnly = true;
            this.lbTotalSizeSelected.Size = new System.Drawing.Size(73, 14);
            this.lbTotalSizeSelected.TabIndex = 69;
            this.lbTotalSizeSelected.TabStop = false;
            this.lbTotalSizeSelected.Tag = null;
            this.lbTotalSizeSelected.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lbTotalSizeSelected.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(545, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 15);
            this.label4.TabIndex = 75;
            this.label4.Text = "Tổng thể tích:";
            // 
            // lbTotalSize
            // 
            this.lbTotalSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbTotalSize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.lbTotalSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbTotalSize.CustomFormat = "###,##0";
            this.lbTotalSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalSize.ForeColor = System.Drawing.Color.Red;
            this.lbTotalSize.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.lbTotalSize.Location = new System.Drawing.Point(727, 2);
            this.lbTotalSize.Name = "lbTotalSize";
            this.lbTotalSize.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.lbTotalSize.ReadOnly = true;
            this.lbTotalSize.Size = new System.Drawing.Size(73, 14);
            this.lbTotalSize.TabIndex = 70;
            this.lbTotalSize.TabStop = false;
            this.lbTotalSize.Tag = null;
            this.lbTotalSize.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lbTotalSize.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(545, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(182, 15);
            this.label5.TabIndex = 79;
            this.label5.Text = "Tổng thể tích được chọn chuyển:";
            // 
            // lbTotalWeightSelected
            // 
            this.lbTotalWeightSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbTotalWeightSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.lbTotalWeightSelected.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbTotalWeightSelected.CustomFormat = "###,##0";
            this.lbTotalWeightSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalWeightSelected.ForeColor = System.Drawing.Color.Red;
            this.lbTotalWeightSelected.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.lbTotalWeightSelected.Location = new System.Drawing.Point(446, 22);
            this.lbTotalWeightSelected.Name = "lbTotalWeightSelected";
            this.lbTotalWeightSelected.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.lbTotalWeightSelected.ReadOnly = true;
            this.lbTotalWeightSelected.Size = new System.Drawing.Size(80, 14);
            this.lbTotalWeightSelected.TabIndex = 71;
            this.lbTotalWeightSelected.TabStop = false;
            this.lbTotalWeightSelected.Tag = null;
            this.lbTotalWeightSelected.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lbTotalWeightSelected.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(218, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(229, 15);
            this.label3.TabIndex = 80;
            this.label3.Text = "Tổng trọng lượng được chọn chuyển (kg):";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Appearance.Options.UseFont = true;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(922, 11);
            this.btnUpdate.LookAndFeel.SkinName = "Blue";
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(84, 27);
            this.btnUpdate.TabIndex = 31;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // flexStoreChange
            // 
            this.flexStoreChange.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexStoreChange.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.FixedOnly;
            this.flexStoreChange.AutoClipboard = true;
            this.flexStoreChange.ColumnInfo = "0,0,0,0,0,105,Columns:";
            this.flexStoreChange.ContextMenuStrip = this.mnuPopUp;
            this.flexStoreChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexStoreChange.Location = new System.Drawing.Point(0, 92);
            this.flexStoreChange.Name = "flexStoreChange";
            this.flexStoreChange.Rows.DefaultSize = 21;
            this.flexStoreChange.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.CellRange;
            this.flexStoreChange.Size = new System.Drawing.Size(1011, 457);
            this.flexStoreChange.StyleInfo = resources.GetString("flexStoreChange.StyleInfo");
            this.flexStoreChange.TabIndex = 33;
            this.flexStoreChange.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.System;
            this.flexStoreChange.AfterSort += new C1.Win.C1FlexGrid.SortColEventHandler(this.flexStoreChange_AfterSort);
            this.flexStoreChange.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexStoreChange_BeforeEdit);
            this.flexStoreChange.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexStoreChange_AfterEdit);
            this.flexStoreChange.CellChanged += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexStoreChange_CellChanged);
            this.flexStoreChange.DoubleClick += new System.EventHandler(this.flexStoreChange_DoubleClick);
            // 
            // mnuPopUp
            // 
            this.mnuPopUp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemPrintStoreAddressA4,
            this.mnuItemPrintStoreAddressA5,
            this.mnuItemPrintStoreAddressA6,
            this.mnuItemPrintInternalOutput_Transfer,
            this.mnuItemPrintStoreChangeCommand,
            this.mnuItemPrintInternalOuput,
            this.mnuItemPrintStoreChangeHandOvers,
            this.mnuItemPrintReport,
            this.itemOutputVoucherTransfer,
            this.mnuItemSeparator,
            this.mnuItemExportExcel});
            this.mnuPopUp.Name = "mnuPopUp";
            this.mnuPopUp.Size = new System.Drawing.Size(296, 252);
            // 
            // mnuItemPrintStoreAddressA4
            // 
            this.mnuItemPrintStoreAddressA4.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemPrintStoreAddressA4.Image")));
            this.mnuItemPrintStoreAddressA4.Name = "mnuItemPrintStoreAddressA4";
            this.mnuItemPrintStoreAddressA4.Size = new System.Drawing.Size(295, 22);
            this.mnuItemPrintStoreAddressA4.Text = "In địa chỉ chuyển hàng A4";
            this.mnuItemPrintStoreAddressA4.Click += new System.EventHandler(this.mnuItemPrintStoreAddressA4_Click);
            // 
            // mnuItemPrintStoreAddressA5
            // 
            this.mnuItemPrintStoreAddressA5.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemPrintStoreAddressA5.Image")));
            this.mnuItemPrintStoreAddressA5.Name = "mnuItemPrintStoreAddressA5";
            this.mnuItemPrintStoreAddressA5.Size = new System.Drawing.Size(295, 22);
            this.mnuItemPrintStoreAddressA5.Text = "In địa chỉ chuyển hàng A5";
            this.mnuItemPrintStoreAddressA5.Click += new System.EventHandler(this.mnuItemPrintStoreAddressA5_Click);
            // 
            // mnuItemPrintStoreAddressA6
            // 
            this.mnuItemPrintStoreAddressA6.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemPrintStoreAddressA6.Image")));
            this.mnuItemPrintStoreAddressA6.Name = "mnuItemPrintStoreAddressA6";
            this.mnuItemPrintStoreAddressA6.Size = new System.Drawing.Size(295, 22);
            this.mnuItemPrintStoreAddressA6.Text = "In địa chỉ chuyển hàng A5 1/2";
            this.mnuItemPrintStoreAddressA6.Click += new System.EventHandler(this.mnuItemPrintStoreAddressA6_Click);
            // 
            // mnuItemPrintInternalOutput_Transfer
            // 
            this.mnuItemPrintInternalOutput_Transfer.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.mnuItemPrintInternalOutput_Transfer.Name = "mnuItemPrintInternalOutput_Transfer";
            this.mnuItemPrintInternalOutput_Transfer.Size = new System.Drawing.Size(295, 22);
            this.mnuItemPrintInternalOutput_Transfer.Text = "In phiếu xuất kho kiêm vận chuyển";
            this.mnuItemPrintInternalOutput_Transfer.Click += new System.EventHandler(this.mnuItemPrintInternalOutput_Transfer_Click);
            // 
            // mnuItemPrintStoreChangeCommand
            // 
            this.mnuItemPrintStoreChangeCommand.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.mnuItemPrintStoreChangeCommand.Name = "mnuItemPrintStoreChangeCommand";
            this.mnuItemPrintStoreChangeCommand.Size = new System.Drawing.Size(295, 22);
            this.mnuItemPrintStoreChangeCommand.Text = "In lệnh điều động hàng hóa";
            this.mnuItemPrintStoreChangeCommand.Click += new System.EventHandler(this.mnuItemPrintStoreChangeCommand_Click);
            // 
            // mnuItemPrintInternalOuput
            // 
            this.mnuItemPrintInternalOuput.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.mnuItemPrintInternalOuput.Name = "mnuItemPrintInternalOuput";
            this.mnuItemPrintInternalOuput.Size = new System.Drawing.Size(295, 22);
            this.mnuItemPrintInternalOuput.Text = "In phiếu xuất kho";
            this.mnuItemPrintInternalOuput.Click += new System.EventHandler(this.mnuItemPrintInternalOuput_Click);
            // 
            // mnuItemPrintStoreChangeHandOvers
            // 
            this.mnuItemPrintStoreChangeHandOvers.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.mnuItemPrintStoreChangeHandOvers.Name = "mnuItemPrintStoreChangeHandOvers";
            this.mnuItemPrintStoreChangeHandOvers.Size = new System.Drawing.Size(295, 22);
            this.mnuItemPrintStoreChangeHandOvers.Text = "In biên bản bàn giao";
            this.mnuItemPrintStoreChangeHandOvers.Click += new System.EventHandler(this.mnuItemPrintStoreChangeHandOvers_Click);
            // 
            // mnuItemPrintReport
            // 
            this.mnuItemPrintReport.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.mnuItemPrintReport.Name = "mnuItemPrintReport";
            this.mnuItemPrintReport.Size = new System.Drawing.Size(295, 22);
            this.mnuItemPrintReport.Text = "In lệnh điều động nội bộ";
            this.mnuItemPrintReport.Click += new System.EventHandler(this.mnuItemPrintReport_Click);
            // 
            // itemOutputVoucherTransfer
            // 
            this.itemOutputVoucherTransfer.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.itemOutputVoucherTransfer.Name = "itemOutputVoucherTransfer";
            this.itemOutputVoucherTransfer.Size = new System.Drawing.Size(295, 22);
            this.itemOutputVoucherTransfer.Text = "In phiếu xuất kho kiêm vận chuyển nội bộ";
            this.itemOutputVoucherTransfer.Click += new System.EventHandler(this.itemOutputVoucherTransfer_Click);
            // 
            // mnuItemSeparator
            // 
            this.mnuItemSeparator.Name = "mnuItemSeparator";
            this.mnuItemSeparator.Size = new System.Drawing.Size(292, 6);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(295, 22);
            this.mnuItemExportExcel.Text = "Xuất Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // frmStoreChangeManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 598);
            this.Controls.Add(this.flexStoreChange);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panelControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmStoreChangeManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý xuất chuyển kho";
            this.Load += new System.EventHandler(this.frmStoreChangeManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsViewDetail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeyword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalQuantitySelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalSizeSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalWeightSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexStoreChange)).EndInit();
            this.mnuPopUp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.SimpleButton btnUpdate;
        private System.Windows.Forms.ComboBox cboStoreChangeType;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CheckEdit chkIsViewDetail;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtKeyword;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private C1.Win.C1Input.C1NumericEdit lbTotalQuantitySelected;
        private System.Windows.Forms.Label label1;
        private C1.Win.C1Input.C1NumericEdit lbTotalSizeSelected;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private C1.Win.C1Input.C1NumericEdit lbTotalWeightSelected;
        private C1.Win.C1Input.C1NumericEdit lbTotalSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private C1.Win.C1Input.C1NumericEdit lbTotalQuantity;
        private System.Windows.Forms.Label label16;
        private C1.Win.C1Input.C1NumericEdit lbTotalWeight;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreSearch;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreSearch;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroup;
        private System.Windows.Forms.ComboBox cboSearchBy;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private System.Windows.Forms.LinkLabel llblDate;
        private C1.Win.C1FlexGrid.C1FlexGrid flexStoreChange;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStatusList;
        private System.Windows.Forms.ContextMenuStrip mnuPopUp;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintStoreAddressA4;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintStoreAddressA5;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintStoreAddressA6;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintInternalOutput_Transfer;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintStoreChangeCommand;
        private System.Windows.Forms.ToolStripSeparator mnuItemSeparator;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintReport;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintInternalOuput;
        private System.Windows.Forms.ToolStripMenuItem itemOutputVoucherTransfer;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintStoreChangeHandOvers;
    }
}