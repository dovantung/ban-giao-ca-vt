﻿using ERP.Inventory.DUI.FIFO;
using Library.AppCore.Forms;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmPopUpIMEI : Form
    {
        private DataTable dtbFromOut = new DataTable();
        private DataTable dtbCurrentInStock = null;
        private DataTable dtbGrid = new DataTable();
        private DataTable dtbAdd = new DataTable();
        public DataTable dtbResult = null;
        private int intWidth = 0;
        private int intHeight = 0;
        private int intStoreID = 0;
        private int intInStockStatusID = 0;
        private PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        DataTable dtbProductConsignmentTypeBySCOTID = null;
        public DataTable ProductConsignmentTypeBySCOTID
        {
            get { return dtbProductConsignmentTypeBySCOTID; }
            set { dtbProductConsignmentTypeBySCOTID = value; }
        }
        public int StoreID
        {
            get { return intStoreID; }
            set { intStoreID = value; }
        }

        public int InStockStatusID
        {
            get { return intInStockStatusID; }
            set { intInStockStatusID = value; }
        }

        public DataTable DataTableFromOut
        {
            get { return dtbFromOut; }
            set { dtbFromOut = value; }
        }

        public frmPopUpIMEI()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
        }

        private void frmPopUpIMEI_Load(object sender, EventArgs e)
        {
            this.intWidth = this.Width;
            this.intHeight = this.Height;
            FormatDataGrid();
            btnSave.Enabled = false;
        }
        bool IsCanChangeProduct(int intPRODUCTCONSIGNMENTTYPEID)
        {
            //PRODUCTCONSIGNMENTTYPEID
            if (dtbProductConsignmentTypeBySCOTID != null && dtbProductConsignmentTypeBySCOTID.Rows.Count > 0)
            {
                DataRow[] drs = dtbProductConsignmentTypeBySCOTID.Select("PRODUCTCONSIGNMENTTYPEID=" + intPRODUCTCONSIGNMENTTYPEID.ToString());
                if (drs != null && drs.Length > 0)
                    return true;
                return false;
            }
            return true;

        }
        private void FormatDataGrid()
        {
            dtbGrid.Columns.Add("ISSELECT", typeof(bool));
            dtbGrid.Columns.Add("PRODUCTID", typeof(string));
            dtbGrid.Columns.Add("PRODUCTNAME", typeof(string));
            dtbGrid.Columns.Add("IMEI", typeof(string));
            dtbGrid.Columns.Add("ISWARRANTY", typeof(bool));
            dtbGrid.Columns.Add("STATUS", typeof(string));
            dtbGrid.Columns.Add("ISERROR", typeof(bool));

            dtbAdd.Columns.Add("ISSELECT", typeof(bool));
            dtbAdd.Columns.Add("PRODUCTID", typeof(string));
            dtbAdd.Columns.Add("PRODUCTNAME", typeof(string));
            dtbAdd.Columns.Add("IMEI", typeof(string));
            dtbAdd.Columns.Add("ISWARRANTY", typeof(bool));
            dtbAdd.Columns.Add("STATUS", typeof(string));
            dtbAdd.Columns.Add("ISERROR", typeof(bool));
        }

        private void btnSearchProduct_Click(object sender, EventArgs e)
        {
            string strProductID = txtProductID.Text.Trim();
            MasterData.DUI.Search.frmProductSearch frm = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
            frm.ShowDialog();
            if (frm.Product != null)
            {
                if (frm.Product.IsRequestIMEI == false)
                {
                    MessageBox.Show(this, "Vui lòng chọn sản phẩm thuộc loại có IMEI!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    if (frm.Product.ProductID.ToString().Trim() != strProductID)
                    {
                        txtProductID.Text = frm.Product.ProductID.ToString();
                        txtProductName.Text = frm.Product.ProductName.ToString();
                        grpProduct.Text = string.Empty;
                        grpProduct.Text += frm.Product.ProductID.Trim() + " - " + frm.Product.ProductName.ToString();
                    }
                }
                btnValidate.Enabled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ClearProduct();
        }

        private void ClearProduct()
        {
            // btnSave.Enabled = false;
            grpProduct.Text = string.Empty;
            grpProduct.Text = "Sản phẩm";
            txtProductName.Clear();
            txtProductID.Clear();
        }

        private bool CheckIMEIBox()
        {
            try
            {
                if (string.IsNullOrEmpty(txtProductID.Text.Trim()))
                {
                    MessageBox.Show(this, "Vui lòng chọn sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnSearchProduct.Focus();
                    return false;
                }
                if (!SearchProduct(txtProductID.Text.Trim()))
                {
                    txtProductID.Focus();
                    return false;
                }
                if (string.IsNullOrEmpty(txtFirstIMEI.Text.Trim()))
                {
                    MessageBox.Show(this, "Vui lòng nhập IMEI đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtFirstIMEI.Focus();
                    return false;
                }
                if (string.IsNullOrEmpty(txtLastIMEI.Text.Trim()))
                {
                    MessageBox.Show(this, "Vui lòng nhập IMEI cuối!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtLastIMEI.Focus();
                    return false;
                }
                if (Convert.ToDouble(txtLastIMEI.Text.Trim()) < Convert.ToDouble(txtFirstIMEI.Text.Trim()))
                {
                    MessageBox.Show(this, "Vui lòng nhập IMEI cuối lớn hơn IMEI đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtFirstIMEI.Focus();
                    return false;
                }
                //if (Convert.ToDouble(txtLastIMEI.Text.Trim()) - Convert.ToDouble(txtFirstIMEI.Text.Trim()) >= 9999)
                //{
                //    MessageBox.Show(this, "Chỉ được phép nhập 1 lần 9999 IMEI!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    txtFirstIMEI.Focus();
                //    return false;
                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private void txtFirstIMEI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtLastIMEI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            dtbAdd.Clear();
            if (!CheckIMEIBox()) return;
            // if (!GetCurrentInstock()) return;
            if (!GenList()) return;


            btnSave.Enabled = true;
            frmWaitDialog.Close();
            Thread.Sleep(0);
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang gen IMEI");
        }
        private bool GenList()
        {
            try
            {
                Thread objThread = new Thread(new ThreadStart(Excute));
                objThread.Start();

                if (!GetCurrentInstock())
                    return false;
                //Lấy dữ liệu cần
                int length = txtFirstIMEI.Text.Trim().Length;
                string productID = txtProductID.Text.Trim();
                string productName = txtProductName.Text.Trim();
                decimal decFirst = Convert.ToDecimal(txtFirstIMEI.Text.Trim());
                decimal decLast = Convert.ToDecimal(txtLastIMEI.Text.Trim());

                DateTime midnight = DateTime.Now.Date;


                for (decimal doubleNum = decFirst; doubleNum <= decLast; doubleNum++)
                {
                    string newIMEI = Convert.ToString(doubleNum).PadLeft(length, '0');
                    //Kiểm tra có bỏ vào chưa

                    DataRow[] drr = dtbGrid.Select("IMEI='" + newIMEI + "' and PRODUCTID='" + productID + "'");
                    if (drr == null || drr.Count() < 1)
                    {
                        DataRow[] drrData = dtbCurrentInStock.Select("IMEI='" + newIMEI + "'");
                        if (drrData != null && drrData.Count() > 0)
                        {
                            if ((Convert.ToInt32(drrData[0]["ISORDER"]) == 1
                                || Convert.ToInt32(drrData[0]["ISORDERING"]) == 1
                                || Convert.ToInt32(drrData[0]["ISDELIVERY"]) == 1))
                                dtbAdd.Rows.Add(false, productID, productName,
                                          newIMEI, false, "IMEI này đã được đặt", true);
                            else
                            {
                                int intPRODUCTCONSIGNMENTTYPEID = Convert.ToInt32(drrData[0]["CONSIGNMENTTYPE"]);
                                if (!IsCanChangeProduct(intPRODUCTCONSIGNMENTTYPEID))
                                {
                                    dtbAdd.Rows.Add(false, productID, productName,
                                                      newIMEI, false, "Sản phẩm không thuộc loại hàng hóa trong yêu cầu xuất chuyển kho!", true);
                                }
                                else
                                {
                                    bool isSelect = false;
                                    if (Convert.ToInt32(drrData[0]["STATUSFIFOID"]) == 1)
                                    {
                                        isSelect = true;

                                    }
                                    //Kiểm tra cùng loại mới cũ
                                    if (Convert.ToInt32(drrData[0]["INSTOCKSTATUSID"]) != InStockStatusID)
                                        dtbAdd.Rows.Add(false, productID, productName,
                                                      newIMEI, false, "Không cùng loại trạng thái yêu cầu", true);
                                    else
                                    {
                                        //Kiểm tra bảo hành
                                        bool IsWarranty = false;
                                        if (Convert.ToDateTime(drrData[0]["ENDWARRANTYDATE"]) >= midnight)
                                            IsWarranty = true;
                                        else
                                            IsWarranty = false;

                                        //Kiểm tra có ngoài lưới
                                        if (DataTableFromOut != null && DataTableFromOut.Rows.Count > 0)
                                        {
                                            DataRow[] drrFromOut = DataTableFromOut.Select("IMEI='" + newIMEI + "' and PRODUCTID='" + productID + "'");
                                            if (drrFromOut != null && drrFromOut.Count() > 0)
                                                dtbAdd.Rows.Add(false, productID, productName,
                                                             newIMEI, IsWarranty, "IMEI này đã được sử dụng", true);
                                            else
                                                dtbAdd.Rows.Add(isSelect, productID, productName,
                                                       newIMEI, IsWarranty, "", false);
                                        }
                                        else
                                            dtbAdd.Rows.Add(isSelect, productID, productName,
                                                       newIMEI, IsWarranty, "", false);
                                    }
                                }

                            }

                        }
                        else
                        {
                            dtbAdd.Rows.Add(false, productID, productName,
                                        newIMEI, false, "Số IMEI không tồn tại trong kho", true);
                        }
                    }

                }
                frmWaitDialog.Close();
                Thread.Sleep(0);
                objThread.Abort();

                var varReulst = from o in dtbAdd.AsEnumerable()
                                where o.Field<bool>("ISERROR") == false && o.Field<bool>("ISSELECT") == true
                                select o;
                if (varReulst.Any())
                {
                    DataTable dtbImeiPara = new DataTable();
                    dtbImeiPara.Columns.Add("IMEI");
                    //varReulst.All(c => { c["ISSELECT"] = false; return true; });
                    var varIMEIOK = from o in dtbAdd.AsEnumerable()
                                    where o.Field<bool>("ISERROR") == false && o.Field<bool>("ISSELECT") == false
                                    select o;
                    if (varIMEIOK.Any())
                    {
                        foreach (DataRow item in varIMEIOK)
                        {
                            string strImei = (item["IMEI"] ?? "").ToString().Trim();
                            if (!string.IsNullOrEmpty(strImei))
                                dtbImeiPara.Rows.Add(strImei);
                        }
                    }
                    //string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbImeiPara, "IMEI");
                    // new PLC.PLCProductInStock().Update_IsOrdering(xmlIMEI, IsOrdering, cboFromStoreSearch.StoreID);
                    foreach (DataRow dr in varReulst)
                    {

                        dr["STATUS"] = "IMEI vi phạm xuất fifo";
                    }
                    DialogResult drResult =
                        ClsFIFO.ShowMessengerFIFOList_ByIMEI(dtbImeiPara, "Có tồn tại dữ liệu vi phạm xuất fifo bạn có muốn tiếp tục?"
                            , txtProductID.Text.Trim(), StoreID, -1);
                    if (drResult != System.Windows.Forms.DialogResult.OK)
                    {
                        foreach (DataRow dr in varReulst)
                            dr["ISERROR"] = true;
                    }
                }
                foreach (DataRow dr in dtbAdd.Rows)
                    dr["ISSELECT"] = false;
                //ERP.MasterData.DUI.MasterData_Globals.frmUserSearch.ShowDialog();
                Thread objThread2 = new Thread(new ThreadStart(Excute));
                objThread2.Start();
                //for (decimal doubleNum = decFirst; doubleNum <= decLast; doubleNum++)
                //{
                //    string newIMEI = Convert.ToString(doubleNum).PadLeft(length, '0');
                //    //Kiểm tra có bỏ vào chưa
                //    DataRow[] drr = dtbGrid.Select("IMEI='" + newIMEI + "' and PRODUCTID='" + productID + "'");
                //    if (drr == null || drr.Count() < 1)
                //        dtbAdd.Rows.Add(false, productID, productName,
                //                        newIMEI, false, "Số IMEI không tồn tại trong kho", true);
                //}

                //var lstCurrentInStock = from o in dtbCurrentInStock.AsEnumerable()
                //                        join i in dtbAdd.AsEnumerable()
                //                        on o["IMEI"].ToString().Trim() equals i["IMEI"].ToString().Trim()
                //                        select o;

                //foreach (var x in lstCurrentInStock)
                //{
                //    // Check IMEI Exist
                //    var itemToChange = dtbAdd.AsEnumerable()
                //                            .FirstOrDefault(d => Convert.ToString(d["IMEI"]) == Convert.ToString(x["IMEI"]));
                //    if (itemToChange != null)
                //    {
                //        itemToChange["STATUS"] = string.Empty;
                //        itemToChange["ISERROR"] = false;
                //        itemToChange["ISSELECT"] = true;

                //        //Kiểm tra IsNew 
                //        if (Convert.ToBoolean(itemToChange["ISERROR"]) == false
                //            && Convert.ToBoolean(x["ISNEW"]) != IsNew)
                //        {
                //            itemToChange["STATUS"] = "Không cùng loại mới/cũ với yêu cầu";
                //            itemToChange["ISERROR"] = true;
                //            itemToChange["ISSELECT"] = false;
                //        }
                //        else
                //        {
                //            if ((Convert.ToInt32(x["ISORDER"]) == 1
                //                || Convert.ToInt32(x["ISORDERING"]) == 1
                //                || Convert.ToInt32(x["ISDELIVERY"]) == 1)
                //                && Convert.ToBoolean(itemToChange["ISERROR"]) == false)
                //            {
                //                itemToChange["STATUS"] = "IMEI này đã được đặt";
                //                itemToChange["ISERROR"] = true;
                //                itemToChange["ISSELECT"] = false;
                //            }
                //            else
                //            {
                //                if (Convert.ToBoolean(itemToChange["ISERROR"]) == false
                //                    && Convert.ToDateTime(x["ENDWARRANTYDATE"]) >= DateTime.Now)
                //                    itemToChange["ISWARRANTY"] = true;
                //            }
                //        }
                //    }
                //}
                //TH1 >= một nửa
                #region OldUse
                //var itemToCheckIsNew = dtbAdd.AsEnumerable().Where(d => Convert.ToBoolean(d["ISERROR"]) == false)
                //                                                .FirstOrDefault(d => Convert.ToDecimal(d["IMEI"].ToString().Trim().TrimStart('0')) == Convert.ToDecimal(x["IMEI"].ToString().Trim().TrimStart('0'))
                //                                                                    && Convert.ToBoolean(x["ISNEW"]) != IsNew);
                ////Check IsNew
                //if (itemToCheckIsNew != null)
                //{
                //    itemToCheckIsNew["STATUS"] = "Không cùng loại mới/cũ với yêu cầu";
                //    itemToCheckIsNew["ISERROR"] = true;
                //    itemToCheckIsNew["ISSELECT"] = false;
                //}
                //var itemToCheckIsOrder = dtbAdd.AsEnumerable().Where(d => Convert.ToBoolean(d["ISERROR"]) == false)
                //                                                .FirstOrDefault(d => Convert.ToDecimal(d["IMEI"].ToString().Trim().TrimStart('0')) == Convert.ToDecimal(x["IMEI"].ToString().Trim().TrimStart('0'))
                //                                                                    && (Convert.ToInt32(x["ISORDER"]) == 1 ||
                //                                                                    Convert.ToInt32(x["ISORDERING"]) == 1 ||
                //                                                                    Convert.ToInt32(x["ISDELIVERY"]) == 1));
                //if (itemToCheckIsOrder != null)
                //{
                //    itemToCheckIsOrder["STATUS"] = "IMEI này đã được đặt";
                //    itemToCheckIsOrder["ISERROR"] = true;
                //    itemToCheckIsOrder["ISSELECT"] = false;
                //}
                //// Update Warranty
                //var itemToChangeWaranty = dtbAdd.AsEnumerable().Where(d => Convert.ToBoolean(d["ISERROR"]) == false)
                //                                                .FirstOrDefault(d => Convert.ToDecimal(d["IMEI"].ToString().Trim().TrimStart('0')) == Convert.ToDecimal(x["IMEI"].ToString().Trim().TrimStart('0'))
                //                                                                    && Convert.ToDateTime(x["ENDWARRANTYDATE"]) >= DateTime.Now);
                //if (itemToChangeWaranty != null)
                //    itemToChangeWaranty["ISWARRANTY"] = true;
                #endregion


                //if (DataTableFromOut.Rows.Count > 0 && DataTableFromOut != null)
                //{
                //    var lstFromOut = from l in DataTableFromOut.AsEnumerable()
                //                     from o in dtbAdd.AsEnumerable()
                //                     where (Convert.ToString(l["PRODUCTID"].ToString().Trim()) == Convert.ToString(o["PRODUCTID"].ToString().Trim())
                //                                    && Convert.ToBoolean(o["ISERROR"]) == false
                //                                    && Convert.ToString(l["IMEI"]) == Convert.ToString(o["IMEI"]))
                //                     select l;

                //    foreach (var x in lstFromOut)
                //    {
                //        var itemToChange = dtbAdd.AsEnumerable()
                //                                .FirstOrDefault(d => Convert.ToString(d["IMEI"]) == Convert.ToString(x["IMEI"]));
                //        if (itemToChange != null)
                //        {
                //            itemToChange["STATUS"] = "IMEI này đã được đặt";
                //            itemToChange["ISERROR"] = true;
                //            itemToChange["ISSELECT"] = false;
                //        }
                //    }
                //}

                frmWaitDialog.Close();
                Thread.Sleep(0);
                objThread2.Abort();
                dtbGrid.Merge(dtbAdd);


                grdIMEI.DataSource = dtbGrid;
                CountIMEIToText();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private bool GetCurrentInstock()
        {
            try
            {
                dtbCurrentInStock = objPLCProductInStock.GetIMEIByQuantity(txtProductID.Text.Trim(), StoreID, 0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //var lstToAdd = dtbGrid.AsEnumerable().Where(d => Convert.ToBoolean(d["ISERROR"]) == false && Convert.ToBoolean(d["ISSELECT"]) == true);
            var lstToAdd = dtbGrid.AsEnumerable().Where(d => Convert.ToBoolean(d["ISERROR"]) == false);
            if (lstToAdd.Count() > 0)
                dtbResult = lstToAdd.CopyToDataTable();
            this.Close();
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            try
            {
                if (e.Column.FieldName != string.Empty)
                {
                    DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                    bool bolIsSelect = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["ISERROR"]));
                    if (bolIsSelect)
                    {
                        e.Appearance.BackColor = Color.Pink;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void grvIMEI_ShowingEditor(object sender, CancelEventArgs e)
        {
            //try
            //{
            //    if (grvIMEI.FocusedRowHandle < 0)
            //        return;
            //    if (grvIMEI.FocusedColumn.FieldName.ToUpper() == "ISSELECT")
            //    {
            //        DataRow dr = (DataRow)grvIMEI.GetDataRow(grvIMEI.FocusedRowHandle);
            //        if (Convert.ToBoolean(dr["ISERROR"]))
            //        {
            //            e.Cancel = true;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        private void mnuItemSelectAll_Click(object sender, EventArgs e)
        {
            //foreach (var item in dtbGrid.AsEnumerable().Where(d => Convert.ToBoolean(d["ISERROR"]) == false))
            foreach (var item in dtbGrid.AsEnumerable())
                item["ISSELECT"] = true;
            grdIMEI.DataSource = dtbGrid;
        }

        private void mnuItemSelectNone_Click(object sender, EventArgs e)
        {
            //foreach (var item in dtbGrid.AsEnumerable().Where(d => Convert.ToBoolean(d["ISERROR"]) == false))
            foreach (var item in dtbGrid.AsEnumerable())//{
                item["ISSELECT"] = false;
            grdIMEI.DataSource = dtbGrid;
        }

        private void frmPopUpIMEI_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.X)
            {
                btnValidate_Click(null, null);
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.Delete)
            {
                txtProductID.Focus();
                mnuItemDeleteRow_Click(null, null);
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.F4)
            {
                this.Close();
            }
            else if (e.KeyCode == Keys.F11)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.WindowState = FormWindowState.Normal;
                this.Width = this.intWidth;
                this.Height = this.intHeight;
            }
        }

        private void frmPopUpIMEI_FormClosing(object sender, FormClosingEventArgs e)
        {
            StoreID = -1;
            intInStockStatusID = 0;
        }

        private void txtFirstIMEI_TextChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
        }

        private void txtLastIMEI_TextChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
        }

        private void txtLastIMEI_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnValidate_Click(null, null);
            }
        }

        private bool SearchProduct(string strProductID)
        {
            try
            {
                MasterData.PLC.MD.WSProduct.Product objProduct = MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);

                if (objProduct != null)
                {
                    if (objProduct.IsRequestIMEI == false)
                    {
                        ClearProduct();
                        MessageBox.Show(this, "Vui lòng chọn sản phẩm thuộc loại có IMEI!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }

                    txtProductID.Text = objProduct.ProductID.ToString();
                    txtProductName.Text = objProduct.ProductName.ToString();
                    grpProduct.Text = string.Empty;
                    grpProduct.Text += objProduct.ProductID.Trim() + " - " + objProduct.ProductName.ToString();

                    btnValidate.Enabled = true;
                }
                else
                {
                    MessageBox.Show(this, "Không tìm thấy sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtProductID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!SearchProduct(txtProductID.Text.Trim()))
                {
                    ClearProduct();
                    txtProductID.Focus();
                    return;
                }
                txtFirstIMEI.Focus();
            }
        }

        private void menuContext_Opening(object sender, CancelEventArgs e)
        {
            DataTable dtbInGrid = (DataTable)grdIMEI.DataSource;
            if (dtbInGrid != null && dtbInGrid.Rows.Count > 0)
            {
                mnuItemSelectAll.Enabled = mnuItemSelectNone.Enabled = mnuItemDeleteRow.Enabled = true;
            }
            else
                mnuItemSelectAll.Enabled = mnuItemSelectNone.Enabled = mnuItemDeleteRow.Enabled = false;
        }

        private void mnuItemDeleteRow_Click(object sender, EventArgs e)
        {
            if (grvIMEI.FocusedRowHandle < 0)
                return;
            btnSave.Focus();
            DataTable dtbCheck = (DataTable)grdIMEI.DataSource;
            DataRow[] drrCheck = dtbCheck.Select("ISSELECT = 'true'");
            if (drrCheck != null && drrCheck.Count() > 0)
            {
                foreach (var rowDel in drrCheck)
                    dtbGrid.Rows.Remove(rowDel);
            }
            else
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một dòng để xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            grdIMEI.RefreshDataSource();
            CountIMEIToText();
        }

        private void CountIMEIToText()
        {
            var listCount = dtbGrid.AsEnumerable().ToList();
            txtCountIMEI.Text = listCount.Count().ToString("#,##0");
            txtCountInvalidIMEI.Text = listCount.Where(p => Convert.ToBoolean(p["ISERROR"]) == false).Count().ToString("#,##0");
            txtCountUninvalidIMEI.Text = listCount.Where(p => Convert.ToBoolean(p["ISERROR"]) == true).Count().ToString("#,##0");
        }

        private void txtFirstIMEI_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnValidate_Click(null, null);
            }
        }

        //private void grvIMEI_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        //{
        //    if (grvIMEI.FocusedRowHandle < 0)
        //        return;
        //    DataRow row = (DataRow)grvIMEI.GetDataRow(grvIMEI.FocusedRowHandle);
        //    string strProductID = row["PRODUCTID"].ToString().Trim();
        //    string strIMEI = row["IMEI"].ToString().Trim();
        //    var itemToCheck = dtbGrid.AsEnumerable().FirstOrDefault(d => d["IMEI"].ToString() == strIMEI && d["PRODUCTID"].ToString() == strProductID);
        //    if (itemToCheck != null)
        //        itemToCheck["ISSELECT"] = !Convert.ToBoolean(itemToCheck["ISSELECT"]);
        //}
    }
}
