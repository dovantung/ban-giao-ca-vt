﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmStoreChangeOrderConfigVOffice : Form
    {
        private const string strFMSAplication = "FMSAplication_ProERP_InputChangeOrderAttachment";        
        private string strMatotrinh = string.Empty;
        private string strFileName = string.Empty;
        frmStoreChangeOrder frmFormOut;

        public frmStoreChangeOrderConfigVOffice(frmStoreChangeOrder frmIn)
        {
            InitializeComponent();
            frmFormOut = frmIn;
        }

        public string FileName {
            get { return strFileName; }
            set { strFileName = value; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //Upload file
            //string strFilePath = string.Empty;
            //string strFileID = string.Empty;
            //string strResult = ERP.FMS.DUI.DUIFileManager.UploadFile(this, strFMSAplication, this.fileNameUpload, ref strFileID, ref strFilePath);
            //Insert into PM_STORECHANGEORDER_ATTACHMENT 
            strMatotrinh = this.txtMatotrinh.Text;
            frmFormOut.Matotrinh = this.txtMatotrinh.Text.ToString().Trim();
            DialogResult dlgResult = this.openFileDialog.ShowDialog();
            if (dlgResult == DialogResult.OK) {
                this.FileName = this.openFileDialog.FileName.Trim();
                frmFormOut.FileName = this.FileName;
                frmFormOut.SafeFileName = this.openFileDialog.SafeFileName.Trim();
            }
            this.Close();            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }        
    }
}
