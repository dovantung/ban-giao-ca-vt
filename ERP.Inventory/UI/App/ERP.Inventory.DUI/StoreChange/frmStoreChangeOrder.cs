﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC.PM;
using ERP.Inventory.PLC.StoreChange;
using ERP.Inventory.PLC.PM.WSStoreChangeOrder;
using Library.AppCore.LoadControls;
using System.Collections;
using DevExpress.XtraGrid.Views.Grid;
using ERP.Inventory.PLC.WSProductInStock;
using ERP.Inventory.DUI.FIFO;
using ERP.MasterData.PLC.MD;
using System.IO;
using System.Diagnostics;
using ERP.Report.DUI;

namespace ERP.Inventory.DUI.StoreChange
{
    /// <summary>
    /// Created by  :   Hoang Nhu Phong
    /// Description :   Quản lý yêu cầu chuyển kho
    /// 
    /// LÊ VĂN ĐÔNG: 07/12/2017 BỔ SUNG -> NẾU TỰ ĐỘNG DUYỆT THÌ BỎ QUA
    /// </summary>
    public partial class frmStoreChangeOrder : Form
    {
        #region Khai báo biến
        private PLCStore objPLCStore = new PLCStore();
        private DataTable dtbStoreCache = null;
        private string strPermission_Add = "MD_StoreChangeOrder_Add";
        private string strPermission_Delete = "MD_StoreChangeOrder_Delete";
        private string strPermission_Edit = "MD_StoreChangeOrder_Edit";
        private string strPermission_ExportExcel = "MD_StoreChangeOrder_ExportExcel";
        private string strEdit_AFReview = "PM_SCORDER_EDIT_AFTER_REVIEW";
        private PLCStoreChangeOrder objPLCStoreChangeOrder = new PLCStoreChangeOrder();
        private string strStoreChangeOrderID = string.Empty;
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;
        private PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();

        private StoreChangeOrder objStoreChangeOrder = null;
        private StoreChangeOrderAttachment objStoreChangeOrderAttachment = null;
        private string strFMSAplication = "FMSAplication_ProERP_SCOAttachment";
        private bool bolChanged = false;
        // lay gia tri cu cua kho xuat, kho nhap
        private int intFromStore = -1;
        private int intToStore = -1;
        private frmStoreChangeOrderManager frmManager = null;
        private DataTable dtbToStore = null;
        private DataTable dtbNotification_User = null;
        int intCheckCompanyType = -1;
        int intCheckProvinceType = -1;
        int intCheckBranchType = -1;
        int intStoreChangeTypeShowProductID = -1;
        int intInStockStatusID = 1;
        private bool bolIsOnlyCertifyFinger = false;
        public string strTRANSPORTCOMPANYID = string.Empty;
        public string strTRASPORTSERVICEID = string.Empty;
        private string strReviewUser = string.Empty;
        private bool bolIsCertifyByFinger = false;
        private bool bolIsHasAction = false;
        private bool bolInputIMEI = true;
        private bool bolIsImport = false;
        private bool bolIsLoadForm = false;
        DataTable dtbProductConsignmentTypeBySCOTID = null;
        private DataTable dtbTransportCompany = null;
        private DataTable dtbTransportServices = null;
        private DataTable dtbTransportTYPE = null;

        // Loai chuyen kho
        private ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType = null;
        private string strMatotrinh = string.Empty;
        private string strFileName = string.Empty;
        private string strSafeFileName = string.Empty;
        private bool bolPermission_PM_InputVoucher_Attachment_View;
        private bool bolIsTaoXuatChuyenKhoCCDC = false;

        DataTable dtAtm = null;
        #endregion

        #region Property
        public bool IsCopy { get; set; }
        public bool IsTaoXuatChuyenKhoCCDC
        {
            get { return bolIsTaoXuatChuyenKhoCCDC; }
            set { bolIsTaoXuatChuyenKhoCCDC = value; }
        }
        public string Matotrinh
        {
            get { return strMatotrinh; }
            set { strMatotrinh = value; }
        }
        public string FileName
        {
            get { return strFileName; }
            set { strFileName = value; }
        }
        public string SafeFileName
        {
            get { return strSafeFileName; }
            set { strSafeFileName = value; }
        }
        public string StoreChangeOrderID
        {
            get { return strStoreChangeOrderID; }
            set
            {
                strStoreChangeOrderID = value;
                if (string.IsNullOrWhiteSpace(strStoreChangeOrderID))
                {
                    FormState = FormStateType.ADD;
                }
                else
                    FormState = FormStateType.EDIT;
            }
        }
        public frmStoreChangeOrderManager FormManager
        {
            get { return frmManager; }
            set { frmManager = value; }
        }
        public bool IsOnlyCertifyFinger
        {
            set { bolIsOnlyCertifyFinger = value; }
        }
        /// <summary>
        /// Có thực hiện thao tác
        /// </summary>
        public bool IsHasAction
        {
            get { return bolIsHasAction; }
        }
        private bool bolIsReviewed = false;

        public bool IsReviewed
        {
            get { return bolIsReviewed; }
            set { bolIsReviewed = value; }
        }


        #endregion  Property

        #region Constructor
        public frmStoreChangeOrder()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        public frmStoreChangeOrder(bool isReviewed = false)
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            if (isReviewed)
            {
                //this.btnTrinhky.Visible = true;
                this.btnVOffice.Visible = true;
            }
            else
            {
                this.btnTrinhky.Visible = false;
                this.btnVOffice.Visible = false;
            }
        }
        #endregion

        #region Các hàm sự kiện
        private void frmStoreChangeOrder_Load(object sender, EventArgs e)
        {
            bolIsLoadForm = false;
            InitControl();
            this.btnVOffice.Visible = false;
            this.btnTrinhky.Visible = false;
            if (!GetStoreChangeOrderType())
            {
                EnableButton(false);
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Visible = false;
                return;
            }
            //LoadComboBox();
            CreateReviewItem();
            LoadStoreChangeStatus();
            LoadComboBox();
            if (!EditData())
            {
                EnableButton(false);
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Visible = false;
                return;
            }
            if (intFormState == FormStateType.ADD)
            {

                dropdownStoreChange.Visible = false;
                btnStoreChangeAll.Visible = false;
                btnReview.Visible = false;
                btnDelete.Visible = false;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Visible = false;
                if (!IsTaoXuatChuyenKhoCCDC) cboFromStoreID.SetValue(SystemConfig.intDefaultStoreID);
            }
            DataTable dtbSource = (grdProduct.DataSource as DataTable).Copy();
            var item = dtbSource.AsEnumerable().Where(x => !string.IsNullOrEmpty(x["IMEI"].ToString().Trim()));
            if (item.Count() <= 0 && grdViewProduct.RowCount > 0)
            {
                grdViewProduct.Columns["IMEI"].Visible = false;
                rbtnQuantity.Checked = true;
                rbtnIMEI.Checked = false;
            }
            bolPermission_PM_InputVoucher_Attachment_View = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHER_ATTACHMENT_VIEW");//Phiếu nhập - Xem tập tin đính kèm

            if (IsTaoXuatChuyenKhoCCDC)
            {
                (grdProduct.DataSource as DataTable).Rows[0]["IMEI"] = string.Empty;
                rbtnIMEI.Checked = true;
                rbtnQuantity.Checked = false;
                this.btnDelete.Visible = false;
                //this.cboTransportTypeID.Enabled = true;
                //this.cboTransportCompany.Enabled = true;
                this.txtSearchProduct.Enabled = true;
                this.txtSearchProduct.ReadOnly = false;
                //this.cboTransportServices.Enabled = true;
            }
            bolIsLoadForm = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (!btnDelete.Enabled)
                return;
            if (!DeleteData())
                return;
            this.bolIsHasAction = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!btnUpdate.Enabled)
                return;
            if (!ValidateData())
                return;
            if (!UpdateData())
                return;
            dtAtm = null;
            if (IsTaoXuatChuyenKhoCCDC)
            {
                this.btnUpdate.Enabled = false;
            }

            this.bolIsHasAction = true;
            if (this.IsCopy) this.Close();
        }
        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            if (grdViewProduct.FocusedRowHandle < 0) return;
            grdViewProduct.DeleteRow(grdViewProduct.FocusedRowHandle);
            grdViewProduct.RefreshData();

        }
        private void mnuItemExport_Click(object sender, EventArgs e)
        {
            ExportExcel(grdProduct);
        }
        private void cmdExcelReview_Click(object sender, EventArgs e)
        {
            ExportExcel(grdReviewLevel);
        }
        private void ExportExcel(DevExpress.XtraGrid.GridControl grid)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grid);
        }

        private void frmStoreChangeOrder_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void ChangeFocus(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                this.SelectNextControl(this.ActiveControl, true, true, true, true);
        }
        private void SelectAll(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == System.Windows.Forms.Keys.A))
            {
                (sender as TextBox).SelectAll();
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }
        private void LoadInputStore(int intFromStore)
        {
            if (dtbToStore == null || intFromStore == -1)
                return;
            try
            {
                if (objStoreChangeOrderType.StoreChangeTypeID == intStoreChangeTypeShowProductID
                    || objStoreChangeOrderType.ToStoreID > 0)
                {
                    // cboToStoreID.InitControl(false, dtbToStore);
                    if (objStoreChangeOrderType.ToStoreID > 0)
                        cboToStoreID.SetValue(objStoreChangeOrderType.ToStoreID);
                    else cboToStoreID.SetValue(intFromStore);
                    //return;
                }
            }
            catch (Exception ex)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi load danh sách kho nhập dựa trên loại chuyển kho");
                return;
            }


            int intCompanyID = -1;
            int intProvinceID = -1;
            int intBranchID = -1;

            bool bolIsRealStore_FromStore = false;//là kho chính
            int intHostStoreID_FromStore = 0;//mã kho chính
            DataTable dtbFromStore = cboFromStoreID.DataSource as DataTable;
            DataRow[] rowsFromStore = dtbFromStore.Select("STOREID = " + intFromStore);
            if (rowsFromStore.Length < 1)
            {
                // MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin kho xuất");
                cboFromStoreID.SetValue(-1);
                return;
            }
            Int32.TryParse(rowsFromStore[0]["COMPANYID"].ToString(), out intCompanyID);
            Int32.TryParse(rowsFromStore[0]["PROVINCEID"].ToString(), out intProvinceID);
            Int32.TryParse(rowsFromStore[0]["BRANCHID"].ToString(), out intBranchID);

            bolIsRealStore_FromStore = Convert.ToBoolean(rowsFromStore[0]["ISREALSTORE"]);
            Int32.TryParse(rowsFromStore[0]["HOSTSTOREID"].ToString(), out intHostStoreID_FromStore);
            bool bolIsInputStore = Convert.ToBoolean(rowsFromStore[0]["ISINPUTSTORE"]);
            StringBuilder sbFilter = new StringBuilder();
            if (objStoreChangeOrderType.StoreChangeTypeID == intStoreChangeTypeShowProductID)
                sbFilter.Append("STOREID = " + intFromStore + " AND COMPANYID = " + intCompanyID);
            else
            {
                sbFilter.Append("STOREID <> " + intFromStore + " ");
                switch (intCheckCompanyType)
                {
                    case 1:
                        sbFilter.Append(" AND COMPANYID = " + intCompanyID);
                        break;
                    case 2:
                        sbFilter.Append(" AND COMPANYID <> " + intCompanyID);
                        break;
                    default:
                        break;
                }
                switch (intCheckProvinceType)
                {
                    case 1:
                        sbFilter.Append(" AND PROVINCEID = " + intProvinceID);
                        break;
                    case 2:
                        sbFilter.Append(" AND PROVINCEID <> " + intProvinceID);
                        break;
                    default:
                        break;
                }
                switch (intCheckBranchType)
                {
                    case 1:
                        sbFilter.Append(" AND BRANCHID = " + intBranchID);
                        break;
                    case 2:
                        sbFilter.Append(" AND BRANCHID <> " + intBranchID);
                        break;
                    default:
                        break;
                }
            }
            //chon kho xuat kho chính -> kho nhan la kho chinh hoặc kho phụ của kho xuất
            if (bolIsRealStore_FromStore)
            {
                sbFilter.Append(" AND (ISREALSTORE = 1 OR (ISREALSTORE = 0 AND HOSTSTOREID=" + intFromStore + ")) ");
            }
            else//Chon kho xuat la kho phu -> kho nhan la kho chinh cua kho xuat
            {
                sbFilter.Append(" AND ISREALSTORE = 1 AND STOREID=" + intHostStoreID_FromStore);
            }
            try
            {

                dtbToStore.DefaultView.RowFilter = sbFilter.ToString();
                DataTable dtbTmp = dtbToStore.DefaultView.ToTable();
                cboToStoreID.InitControl(false, dtbTmp);
                cboToStoreID.SetValue(objStoreChangeOrderType.ToStoreID);
            }
            catch (Exception ex)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi load danh sách kho nhập dựa trên loại chuyển kho");
            }
        }
        private void cboFromStoreID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (grdProduct.DataSource != null && grdViewProduct.RowCount > 0)
            {
                if (MessageBox.Show(this, "Thay đổi kho xuất sẽ làm thay đổi dữ liệu trên lưới, bạn có muốn tiếp tục không.", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    DataTable dtb = (grdProduct.DataSource as DataTable).Clone();
                    grdProduct.DataSource = dtb;
                    grdViewProduct.RefreshData();
                }
                else
                {
                    cboFromStoreID.SetValue(intFromStore);
                }
            }
            intFromStore = cboFromStoreID.StoreID;
            LoadInputStore(intFromStore);
        }

        private void cboToStoreID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (grdProduct.DataSource != null && grdViewProduct.RowCount > 0)
            {
                if (MessageBox.Show(this, "Thay đổi kho nhập sẽ làm thay đổi dữ liệu trên lưới, bạn có muốn tiếp tục không.", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    DataTable dtb = (grdProduct.DataSource as DataTable).Clone();
                    grdProduct.DataSource = dtb;
                    grdViewProduct.RefreshData();
                    txtSearchProduct.ReadOnly = false;
                    btnSearchProduct.Enabled = true;
                }
                else
                {
                    cboToStoreID.SetValue(intToStore);
                }
            }
            intToStore = cboToStoreID.StoreID;
        }
        private void grdViewProduct_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            bolChanged = true;
            GridView view = sender as GridView;

            //nieu so luong la null hoac empty thi tra ve 0
            if (view.FocusedColumn == view.Columns["STORECHANGEQUANTITY"] && e.Value == null)
            {
                view.SetFocusedRowCellValue("STORECHANGEQUANTITY", 0);
            }
            //nieu so luong la null hoac empty thi tra ve 0
            if (view.FocusedColumn == view.Columns["STORECHANGEQUANTITY"] && e.Value == null)
            {
                view.SetFocusedRowCellValue("STORECHANGEQUANTITY", 0);
            }
        }

        private void grdViewProduct_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedRowHandle < 0) return;
            if (view.FocusedColumn == view.Columns["IMEI"])
            {
                if (!string.IsNullOrWhiteSpace(e.Value.ToString()))
                {
                    if (!Library.AppCore.Other.CheckObject.CheckIMEI(e.Value.ToString().Trim()))
                    {
                        e.Valid = false;
                        e.ErrorText = "IMEI không đúng định dạng.";
                        return;
                    }
                    string strProductID = view.GetFocusedRowCellValue("PRODUCTID").ToString().Trim();
                    string strProductName = view.GetFocusedRowCellValue("PRODUCTNAME").ToString().Trim();
                    string strError = ValidateImei(e.Value.ToString(), strProductID, strProductName);
                    if (!string.IsNullOrEmpty(strError))
                    {
                        e.Valid = false;
                        e.ErrorText = strError;
                        return;
                    }
                }

            }

            if (view.FocusedColumn == view.Columns["QUANTITY"])
            {
                if (e.Value != null)
                {
                    decimal decQuantity = 0;
                    Decimal.TryParse(e.Value.ToString().Trim(), out decQuantity);
                    decimal decQuantityInStock = 0;
                    string strProductId = view.GetFocusedRowCellValue("PRODUCTID").ToString().Trim();
                    string strIMEI = view.GetFocusedRowCellValue("IMEI").ToString().Trim();

                    ProductInStock objProductInStock = null;
                    if (!string.IsNullOrEmpty(strIMEI) && rbtnIMEI.Checked)
                    {
                        objProductInStock = new PLC.PLCProductInStock().LoadInfo(strIMEI, cboFromStoreID.StoreID, Convert.ToInt32(cboInStockStatusID.SelectedValue));
                    }
                    else
                        objProductInStock = new PLC.PLCProductInStock().LoadInfo(strProductId, cboFromStoreID.StoreID, Convert.ToInt32(cboInStockStatusID.SelectedValue));
                    decQuantityInStock = objProductInStock.Quantity;
                    if (decQuantity > decQuantityInStock)
                    {
                        e.Valid = false;
                        e.ErrorText = "Số lượng chuyển lớn hơn số lượng tồn kho.";
                        return;
                    }
                }




            }

            if (view.FocusedColumn == view.Columns["STORECHANGEQUANTITY"])
            {
                if (e.Value != null && !string.IsNullOrEmpty(e.Value.ToString().Trim()))
                {
                    decimal decQuantity = 0;
                    Decimal.TryParse(view.GetFocusedRowCellValue("QUANTITY").ToString().Trim(), out decQuantity);
                    decimal decStoreQuantity = 0;
                    Decimal.TryParse(e.Value.ToString().Trim(), out decStoreQuantity);
                    if (decStoreQuantity > decQuantity)
                    {
                        e.Valid = false;
                        e.ErrorText = "Số lượng chuyển thực tế không thể lớn hơn số lượng yêu cầu.";
                        return;
                    }
                }
            }
        }

        private void grdViewProduct_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Column.FieldName == "IMEI")
            {
                string strIsRequestImei = view.GetRowCellValue(e.RowHandle, view.Columns["ISREQUESTIMEI"]).ToString().Trim();
                //if (strIsRequestImei == "0" || string.IsNullOrEmpty(strIsRequestImei))
                //{
                //    e.Appearance.BackColor = System.Drawing.SystemColors.Info;
                //}
                // string strIMEI = view.GetRowCellValue(e.RowHandle, view.Columns["IMEI"]).ToString().Trim();
                bool bIsRequestImei = false;
                try
                {
                    bIsRequestImei = Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, "ISREQUESTIMEI"));
                }
                catch
                {

                }

                string strQuantity = view.GetRowCellValue(e.RowHandle, view.Columns["QUANTITY"]).ToString().Trim();
                decimal decQuantity = 0;
                decimal.TryParse(strQuantity, out decQuantity);
                if (decQuantity != 1 && !(strIsRequestImei == "0" || string.IsNullOrEmpty(strIsRequestImei)))
                {
                    e.Appearance.BackColor = System.Drawing.SystemColors.Info;
                }
                else if ((strIsRequestImei == "0" || string.IsNullOrEmpty(strIsRequestImei)))
                {
                    e.Appearance.BackColor = System.Drawing.SystemColors.Info;
                }
                else
                {
                    e.Appearance.BackColor = System.Drawing.SystemColors.Window;
                }
            }
            //so luong = 0 thi tô đỏ
            //if (e.Column.FieldName == "QUANTITY" )
            //{
            //    string quantity = view.GetRowCellDisplayText(e.RowHandle, view.Columns["QUANTITY"]);
            //    if (quantity == "0" || string.IsNullOrEmpty(quantity))
            //    {
            //        e.Appearance.BackColor = Color.Red;
            //        e.Appearance.BackColor2 = Color.LightCyan;
            //    }
            //}

        }

        private void grdViewProduct_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = sender as GridView;
            if (objStoreChangeOrder.IsReviewed)
            {
                e.Cancel = true;
            }
            if (FormState == FormStateType.ADD || (FormState == FormStateType.EDIT && !objStoreChangeOrder.IsReviewed))
            {

                bool bIsRequestImei = Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, "ISREQUESTIMEI"));
                int intQuantity = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QUANTITY"));
                string strIMEI = view.GetRowCellValue(view.FocusedRowHandle, "IMEI").ToString().Trim();
                string strProducId = view.GetRowCellValue(view.FocusedRowHandle, "PRODUCTID").ToString().Trim();
                if (FormState == FormStateType.EDIT && !objStoreChangeOrder.IsReviewed)
                {
                    if (rbtnIMEI.Checked)
                    {
                        if (bIsRequestImei)
                        {
                            if (view.FocusedColumn == view.Columns["QUANTITY"] || view.FocusedColumn == view.Columns["STORECHANGEQUANTITY"])
                                e.Cancel = true;
                        }
                        else
                        {
                            if (view.FocusedColumn == view.Columns["IMEI"])
                                e.Cancel = true;
                        }
                        if (intQuantity != 1 && bIsRequestImei)
                        {
                            if (view.FocusedColumn == view.Columns["IMEI"])
                                e.Cancel = true;
                        }
                        else if (intQuantity == 1 && bIsRequestImei)
                        {
                            if (view.FocusedColumn == view.Columns["IMEI"])
                                e.Cancel = false;
                        }
                        else
                        {
                            if (view.FocusedColumn == view.Columns["IMEI"])
                                e.Cancel = true;
                        }
                        if (view.FocusedColumn == view.Columns["QUANTITY"])
                        {

                            if (bIsRequestImei)
                            {
                                e.Cancel = true;
                            }
                            else
                                e.Cancel = false;
                        }
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                }
                else
                {
                    if (bolInputIMEI)
                    {
                        if (bIsRequestImei)
                        {
                            if (view.FocusedColumn == view.Columns["QUANTITY"] || view.FocusedColumn == view.Columns["STORECHANGEQUANTITY"])
                                e.Cancel = true;
                        }
                        else
                        {
                            if (view.FocusedColumn == view.Columns["IMEI"])
                                e.Cancel = true;
                        }
                        if (intQuantity != 1 && bIsRequestImei)
                        {
                            if (view.FocusedColumn == view.Columns["IMEI"])
                                e.Cancel = true;
                        }
                        else if (intQuantity == 1 && bIsRequestImei)
                        {
                            if (view.FocusedColumn == view.Columns["IMEI"])
                                e.Cancel = false;
                        }
                        else
                        {
                            if (view.FocusedColumn == view.Columns["IMEI"])
                                e.Cancel = true;
                        }
                        if (view.FocusedColumn == view.Columns["QUANTITY"])
                        {

                            if (bIsRequestImei)
                            {
                                e.Cancel = true;
                            }
                            else
                                e.Cancel = false;
                        }
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                }
            }
            else
            {
                if (view.FocusedColumn == view.Columns["IMEI"])
                    e.Cancel = true;
            }

        }
        private void btnSearchProduct_Click(object sender, EventArgs e)
        {
            if (!ValidateStore()) return;
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch = MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch.IsMultiSelect = true;
            frmProductSearch.ShowDialog();
            if (frmProductSearch.ProductList != null && frmProductSearch.ProductList.Count > 0)
            {
                //add san pham vao chi tiet
                //txtSearchProduct.Text = frmProductSearch.ProductList[0].ProductID;
                foreach (ERP.MasterData.PLC.MD.WSProduct.Product objProduct in frmProductSearch.ProductList)
                {
                    AddProduct(objProduct.ProductID, 1);
                }
                txtSearchProduct.Focus();
            }
        }
        private void txtSearchProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 13) return;
            if (!ValidateStore()) return;
            txtSearchProduct.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtSearchProduct.Text.Trim());
            if(!AddProduct(txtSearchProduct.Text.Trim(), 1)) return;
            txtSearchProduct.Text = string.Empty;
            txtSearchProduct.Focus();
        }

        private void tabReviewLevel_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.ADD)
                tabControl.SelectedTab = tabGeneral;
        }

        private void tabAttachment_Enter(object sender, EventArgs e)
        {
            //if (FormState == FormStateType.ADD)
            //    tabControl.SelectedTab = tabGeneral;
        }

        private void btnOutputStoreChange_Click(object sender, EventArgs e)
        {
            frmStoreChange frmStoreChange1 = new frmStoreChange();
            frmStoreChange1.StoreChangeOrderID = strStoreChangeOrderID.Trim();
            frmStoreChange1.ShowDialog();
        }
        #endregion

        #region Các hàm hỗ trợ
        private void EnableButton(bool bolEnable)
        {
            btnDelete.Enabled = bolEnable;
            dropdownStoreChange.Enabled = bolEnable;
            btnStoreChangeAll.Enabled = bolEnable;
            btnReview.Enabled = bolEnable;
            btnSearchProduct.Enabled = bolEnable;
            btnUpdate.Enabled = bolEnable;
            cmdReview.Enabled = bolEnable;
            btnVOffice.Enabled = bolEnable;
            btnTrinhky.Enabled = bolEnable;

            if (!bolEnable)
            {
                dropdownStoreChange.Visible = false;
                btnStoreChangeAll.Visible = false;
                btnVOffice.Visible = false;
                btnTrinhky.Visible = false;
                btnReview.Visible = false;
                btnDelete.Visible = false;
            }

            ERP.MasterData.DUI.Common.CommonFunction.SetReadOnly(txtSearchProduct, true);
        }
        /// <summary>
        /// Khởi tạo cho các control
        /// Xét các thuộc tính
        /// </summary>
        private void InitControl()
        {
            lblWarning.Visible = false;
            this.MinimumSize = new System.Drawing.Size(984, 576);
            txtContent.MaxLength = 2000;
            txtContentDeleted.MaxLength = 2000;
            if (string.IsNullOrEmpty(this.strStoreChangeOrderID))
            {
                this.mnuItemExportTemplate.Visible = true;
                this.mnuSeparator2.Visible = true;
                this.mnuImportExcel.Visible = true;
            }
            else
            {
                this.mnuItemExportTemplate.Visible = false;
                this.mnuSeparator2.Visible = false;
                this.mnuImportExcel.Visible = false;
            }
            ////Event Enter
            dteExpiryDate.KeyPress += new KeyPressEventHandler(ChangeFocus);
            cboFromStoreID.KeyPress += new KeyPressEventHandler(ChangeFocus);
            cboToStoreID.KeyPress += new KeyPressEventHandler(ChangeFocus);
            cboTransportTypeID.KeyPress += new KeyPressEventHandler(ChangeFocus);
            cboStoreChangeStatus.KeyPress += new KeyPressEventHandler(ChangeFocus);
            cboInStockStatusID.KeyPress += new KeyPressEventHandler(ChangeFocus);
            //radioIsNew.KeyPress += new KeyPressEventHandler(ChangeFocus);
            //radioIsOld.KeyPress += new KeyPressEventHandler(ChangeFocus);
            ckbIsDeleted.KeyPress += new KeyPressEventHandler(ChangeFocus);
            ckbIsReview.KeyPress += new KeyPressEventHandler(ChangeFocus);
            ckbIsUrgent.KeyPress += new KeyPressEventHandler(ChangeFocus);
            txtContent.KeyUp += new KeyEventHandler(SelectAll);
            txtContentDeleted.KeyUp += new KeyEventHandler(SelectAll);
            rbtnIMEI.Checked = true;
            CreateColumnGridReview(false);
            FormatGridReview();
            if (grdViewProduct.RowCount > 0)
            {
                mnuItemExport.Enabled = true;
            }
            else
            {
                mnuItemExport.Enabled = false;
            }
            mnuItemImportIMEI.Enabled = !txtSearchProduct.ReadOnly && bolInputIMEI;

        }
        private bool GetStoreChangeOrderType()
        {
            int intStoreChangeOrderType = 0;
            Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
            try
            {
                intStoreChangeOrderType = Convert.ToInt32(hstbParam["StoreChangeOrderType"]);
                ERP.MasterData.PLC.MD.WSStoreChangeOrderType.ResultMessage objResultMessage = new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfo(ref objStoreChangeOrderType, intStoreChangeOrderType);
                if (objResultMessage.IsError || objStoreChangeOrderType == null)
                {
                    throw new Exception(objResultMessage.Message);
                }
                this.Text = objStoreChangeOrderType.StoreChangeOrderTypeName.Replace("Loại", "");
                dtbProductConsignmentTypeBySCOTID = null;
                objResultMessage = new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().GetProductConsignmentTypeBySCOTID(ref dtbProductConsignmentTypeBySCOTID, objStoreChangeOrderType.StoreChangeOrderTypeID);
                if (objResultMessage.IsError)
                {
                    throw new Exception(objResultMessage.Message);
                }
                objStoreChangeType = new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo((int)objStoreChangeOrderType.StoreChangeTypeID);

            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi lấy tham số loại yêu cầu chuyển kho", objExce, DUIInventory_Globals.ModuleName + "->GetStoreChangeOrderType");
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi lấy tham số loại yêu cầu chuyển kho.");
                return false;
            }
            return true;
        }


        bool IsCanChangeProduct(int intPRODUCTCONSIGNMENTTYPEID)
        {
            //PRODUCTCONSIGNMENTTYPEID
            if (dtbProductConsignmentTypeBySCOTID != null && dtbProductConsignmentTypeBySCOTID.Rows.Count > 0)
            {
                DataRow[] drs = dtbProductConsignmentTypeBySCOTID.Select("PRODUCTCONSIGNMENTTYPEID=" + intPRODUCTCONSIGNMENTTYPEID.ToString());
                if (drs != null && drs.Length > 0)
                    return true;
                return false;
            }
            return true;

        }
        private void LoadComboBox()
        {
            object[] objKeyWords = new object[] { "@ISDELETED", 0 };
            ERP.MasterData.PLC.MD.WSStore.ResultMessage objResultMessage = objPLCStore.SearchData(ref dtbStoreCache, objKeyWords);
            if (objResultMessage.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return;
            }
            //Hiếu sửa không xét quyền trên kho khi trong mode Edit / Khóa comboBox trong mode Edit
            Library.AppCore.DataSource.FilterObject.StoreFilter objFromFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objFromFilter.IsCheckPermission = true;
            objFromFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;


            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objStoreFilter.IsCheckPermission = true;
            objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;


            switch (objStoreChangeOrderType.CreateStorePermissionType)
            {
                case 1:
                    if (FormState == FormStateType.EDIT)
                    {
                        cboToStoreID.InitControl(false, dtbStoreCache);
                        cboFromStoreID.InitControl(false, dtbStoreCache);
                    }
                    else
                    {
                        cboFromStoreID.InitControl(false, objFromFilter);
                        objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGETO;
                        cboToStoreID.InitControl(false, objStoreFilter);
                    }
                    break;
                case 2:
                    if (FormState == FormStateType.EDIT)
                    {
                        cboToStoreID.InitControl(false, dtbStoreCache);
                        cboFromStoreID.InitControl(false, dtbStoreCache);
                    }
                    else
                    {
                        cboFromStoreID.InitControl(false);
                        cboToStoreID.InitControl(false, objStoreFilter);
                    }
                    break;
                //cboFromStoreID.InitControl(false, false);
                //objStoreFilter.IsCheckPermission = true;
                //objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;
                //cboToStoreID.InitControl(false, objStoreFilter);
                //break;
                case 3:
                    //objFromFilter.IsCheckPermission = true;
                    //objFromFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;
                    //if (FormState == FormStateType.EDIT)
                    //    cboFromStoreID.InitControl(false, false);
                    //else
                    //    cboFromStoreID.InitControl(false, objFromFilter);
                    //objStoreFilter.IsCheckPermission = true;
                    //objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;
                    //cboToStoreID.InitControl(false, objStoreFilter);
                    //break;
                    if (FormState == FormStateType.EDIT)
                    {
                        cboToStoreID.InitControl(false, dtbStoreCache);
                        cboFromStoreID.InitControl(false, dtbStoreCache);
                    }
                    else
                    {
                        cboFromStoreID.InitControl(false, objFromFilter);
                        cboToStoreID.InitControl(false, objStoreFilter);
                    }
                    break;
                default:
                    cboFromStoreID.InitControl(false, dtbStoreCache);
                    cboToStoreID.InitControl(false, dtbStoreCache);
                    break;
            }

            dtbToStore = (cboToStoreID.DataSource as DataTable).Copy();

            DataTable dtbInstockStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            dtbInstockStatus.Columns.Add("INSTOCKSTATUSID", typeof(int));
            foreach (DataRow dr in dtbInstockStatus.Rows)
                dr["INSTOCKSTATUSID"] = Convert.ToInt32(dr["PRODUCTSTATUSID"]);
            dtbInstockStatus.Columns["PRODUCTSTATUSNAME"].ColumnName = "INSTOCKSTATUSNAME";
            dtbInstockStatus.AcceptChanges();
            cboInStockStatusID.DataSource = dtbInstockStatus;
            cboInStockStatusID.ValueMember = "INSTOCKSTATUSID";
            cboInStockStatusID.DisplayMember = "INSTOCKSTATUSNAME";
            cboInStockStatusID.SelectedValue = intInStockStatusID;
            this.cboInStockStatusID.SelectionChangeCommitted += new System.EventHandler(cboInStockStatusID_SelectionChangeCommitted);

            // Dịch vụ
            dtbTransportServices = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("TP_TRANSPORTSERVICE_CACHE");
            if (dtbTransportServices != null)
            {
                cboTransportServices.InitControl(false, dtbTransportServices, "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--Chọn dịch vụ chuyển phát--");
            }
            object[] objKeywords = new object[] { "@TRANSPORTTYPEID", -1
            };
            dtbTransportCompany = objPLCStoreChangeCommand.SearchDataTranCompany(objKeywords);

            if (dtbTransportCompany != null)
            {
                cboTransportCompany.InitControl(false, dtbTransportCompany, "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đơn vị chuyển phát--");
            }

            dtbTransportTYPE = Library.AppCore.DataSource.GetDataSource.GetTransportType();
            if (dtbTransportTYPE != null)
            {

                //DataRow[] rowService = dtbTransportServices.Select("TRANSPORTCOMPANYID = " + cboTransportCompany.ColumnID);
                //if(rowService.Any())
                //{
                cboTransportTypeID.InitControl(false, dtbTransportTYPE, "TRANSPORTTYPEID", "TRANSPORTTYPENAME", "--Chọn đơn phương tiện--");
                //}

            }

            ////
             dtAtm = new DataTable();
            dtAtm.Columns.Add(new DataColumn("VOFFICEID", typeof(string)));
            dtAtm.Columns.Add(new DataColumn("ATTACHMENTNAME", typeof(string)));
            dtAtm.Columns.Add(new DataColumn("DESCRIPTION", typeof(string)));
            dtAtm.Columns.Add(new DataColumn("CREATEDDATE", typeof(string)));
            dtAtm.Columns.Add(new DataColumn("CREATEDUSER", typeof(string)));
            dtAtm.Columns.Add(new DataColumn("ATTACHMENTID", typeof(string)));
            dtAtm.Columns.Add(new DataColumn("ATTACHMENTPATH", typeof(string)));
            dtAtm.Columns.Add(new DataColumn("FILEID", typeof(string)));
        }

        private void LoadStoreChangeStatus()
        {
            LoadStoreChangeStatus(false);
        }
        private void LoadStoreChangeStatus(bool isExpired)
        {
            DataTable dtbStoreChangeStatus = new DataTable("StoreChangeStatus ");
            dtbStoreChangeStatus.Columns.Add("Value");
            dtbStoreChangeStatus.Columns.Add("Text");
            if (!isExpired)
                dtbStoreChangeStatus.Rows.Add(0, "Chưa chuyển kho");
            dtbStoreChangeStatus.Rows.Add(1, "Đã chuyển nhưng chưa xong");
            if (!isExpired)
                dtbStoreChangeStatus.Rows.Add(2, "Đã chuyển kho xong");
            dtbStoreChangeStatus.Rows.Add(3, "Kết thúc");
            cboStoreChangeStatus.DataSource = dtbStoreChangeStatus;
            cboStoreChangeStatus.DisplayMember = "Text";
            cboStoreChangeStatus.ValueMember = "Value";
        }
        /// <summary>
        /// Hàm lấy dữ liệu Chỉnh sửa
        /// </summary>
        private bool EditData(bool isEditFromAutoReviewCCDC = false)
        {
            try
            {
      
                ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType =
                    new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo((int)objStoreChangeOrderType.StoreChangeTypeID);
                if (objStoreChangeType == null)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Loại yêu cầu chuyển kho chưa khai báo Loại chuyển kho");
                    return false;
                }
                try
                {
                    intStoreChangeTypeShowProductID = Library.AppCore.AppConfig.GetIntConfigValue("PM_STORECHANGETYPE_SHOWPRODUCT");
                    intCheckCompanyType = objStoreChangeType.CheckCompanyType;
                    intCheckProvinceType = objStoreChangeType.CheckProvinceType;
                    intCheckBranchType = objStoreChangeType.CheckBranchType;
                    if (objStoreChangeOrderType.FromStoreID > 0)
                        cboFromStoreID.Enabled = false;
                }
                catch
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin loại yêu cầu chuyển kho");
                }
                objStoreChangeOrder = objPLCStoreChangeOrder.LoadInfo(strStoreChangeOrderID, objStoreChangeOrderType.StoreChangeOrderTypeID);
                if (objStoreChangeOrder == null)
                    throw new Exception();

                if (IsTaoXuatChuyenKhoCCDC && !isEditFromAutoReviewCCDC)
                {
                    int tempID = objStoreChangeOrder.FromStoreID;
                    objStoreChangeOrder.FromStoreID = objStoreChangeOrder.ToStoreID;
                    objStoreChangeOrder.ToStoreID = tempID;
                }


                dropdownStoreChange.Enabled = false;
                btnStoreChangeAll.Enabled = false;
                if (IsCopy)
                {
                    if(objStoreChangeOrderType.StoreChangeOrderTypeID != 39)
                    {
                        MessageBoxObject.ShowErrorMessage(this, "Chỉ copy được yêu cầu chuyển kho CCDC");
                        this.Close();
                        return false;
                    }
                    if(!objStoreChangeOrderType.ProductConsignmentType.Contains(6))
                    {
                        MessageBoxObject.ShowErrorMessage(this, "Chỉ copy được loại chuyển kho có áp dụng cho hàng hóa CCDC");
                        this.Close();
                        return false;
                    }
                    if (objStoreChangeOrder.StoreChangeStatus != 3)
                    {
                        MessageBoxObject.ShowErrorMessage(this, "Chỉ copy được yêu cầu chuyển kho trạng thái đã kết thúc");
                        this.Close();
                        return false;
                    }
                    objStoreChangeOrder.StoreChangeOrderID = strStoreChangeOrderID = "";

                }

                if (string.IsNullOrWhiteSpace(strStoreChangeOrderID))
                {
                    cboTransportTypeID.Enabled = true;
                    cboTransportCompany.Enabled = true;
                    cboTransportServices.Enabled = true;
                    FormState = FormStateType.ADD;
                    txtStoreChangeOrderID.Text = objPLCStoreChangeOrder.GetStoreChangeOrderNewID(SystemConfig.intDefaultStoreID);
                    
                    if (IsCopy)
                    {
                        if(objStoreChangeOrder.ToStoreID != SystemConfig.intDefaultStoreID)
                        {
                            MessageBoxObject.ShowErrorMessage(this, "Kho đăng nhập phải là kho " + objStoreChangeOrder.ToStoreID);
                            this.Close();
                            return false;

                        }
                        int tmp = objStoreChangeOrder.FromStoreID;
                        objStoreChangeOrder.FromStoreID = objStoreChangeOrder.ToStoreID;
                        objStoreChangeOrder.ToStoreID = tmp;
                        cboFromStoreID.SetValue(objStoreChangeOrder.FromStoreID);
                        cboToStoreID.SetValue(objStoreChangeOrder.ToStoreID);
                        ////
                        objStoreChangeOrder.StoreChangeStatus = 0;
                        objStoreChangeOrder.Content = "";
                        objStoreChangeOrder.ContentDeleted = "";
                        objStoreChangeOrder.IsReviewed = false;
                        objStoreChangeOrder.IsUrgent = false;
                        objStoreChangeOrder.IsDeleted  = false;
                        objStoreChangeOrder.ReviewedUser = "";
                        objStoreChangeOrder.ReviewedDate = null;
                        //objStoreChangeOrder.DtbStoreChangeOrderDetail ;
                        //objStoreChangeOrder.DtbReviewLevel = null;
                        foreach (DataRow r in objStoreChangeOrder.DtbReviewLevel.Rows)
                        {
                            r["IsReviewed"] = 0;
                            r["STORECHANGEORDERID"] = -2;
                            r["REVIEWSTATUS"] = 0;
                            r["REVIEWEDUSER"] = "";
                            r["REVIEWEDUSFULLNAME"] = "";
                            r["REVIEWEDDATE"] = DBNull.Value;
                            r["NOTE"] = "";
                        }
                        foreach (DataRow r in objStoreChangeOrder.DtbStoreChangeOrderDetail.Rows)
                        {
                            r["STORECHANGEQUANTITY"] = 0;

                        }
                        grdReviewLevel.DataSource = objStoreChangeOrder.DtbReviewLevel;
                        objStoreChangeOrder.DtbAttachment = null;
                        objStoreChangeOrder.IsExpired = false;
                        CreateColumnGridReview(false);
                        FormatGridReview();
                    }
                    else
                    {
                        objStoreChangeOrder.FromStoreID = Convert.ToInt32(objStoreChangeOrderType.FromStoreID);
                        if (objStoreChangeOrder.FromStoreID < 1)
                            objStoreChangeOrder.FromStoreID = SystemConfig.intDefaultStoreID;
                        if (objStoreChangeOrderType.StoreChangeTypeID == intStoreChangeTypeShowProductID)
                            objStoreChangeOrder.ToStoreID = objStoreChangeOrder.FromStoreID;
                        else
                            objStoreChangeOrder.ToStoreID = (int)objStoreChangeOrderType.ToStoreID;
                    }
                    btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.CreateFunctionID);
                    dteOrderDate.Value = Globals.GetServerDateTime();
                    dteExpiryDate.Value = DateTime.Now;
                    objStoreChangeOrder.IsNew = true;
                    objStoreChangeOrder.InStockStatusID = intInStockStatusID;
                    btnReview.Enabled = false;
                }
                else
                {

                    if (IsTaoXuatChuyenKhoCCDC)
                    {
                        txtStoreChangeOrderID.Text = objPLCStoreChangeOrder.GetStoreChangeOrderNewID(SystemConfig.intDefaultStoreID);
                    }
                    else
                    {
                        txtStoreChangeOrderID.Text = objStoreChangeOrder.StoreChangeOrderID.Trim();
                    }
                    cboTransportTypeID.Enabled = false;
                    cboTransportCompany.Enabled = false;
                    cboTransportServices.Enabled = false;
                    txtStoreChangeOrderID.Text = objStoreChangeOrder.StoreChangeOrderID.Trim();
                    btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.DeleteFunctionID);
                    btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.EditFunctionID);
                    cmdReview.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.ReviewFunctionID);
                    dteOrderDate.Value = objStoreChangeOrder.OrderDate.Value;
                    dteExpiryDate.Value = objStoreChangeOrder.ExpiryDate.Value;
                    if (objStoreChangeOrderType.IsReviewByTransType == true)
                    {
                        DataTable dtbTransportTypeCurrent = (DataTable)cboTransportTypeID.DataSource;
                        if (cboTransportTypeID.ColumnID != null)
                        {
                            int intTransportTypeIDcurent = -1;
                            int.TryParse((objStoreChangeOrder.TransportTypeID).ToString().Trim(), out intTransportTypeIDcurent);
                            string strReviewByTranSportID = "";
                            DataRow[] drs = dtbTransportTypeCurrent.Select("TRANSPORTTYPEID = " + intTransportTypeIDcurent.ToString());
                            if (drs != null && drs.Length > 0)
                                strReviewByTranSportID = (drs[0]["REVIEWFUNCTIONID"] ?? "").ToString().Trim();
                            if (string.IsNullOrEmpty(strReviewByTranSportID))
                            { 
                                btnReview.Enabled = false;
                            }
                            else
                            {
                                btnReview.Enabled = SystemConfig.objSessionUser.IsPermission(strReviewByTranSportID) && !objStoreChangeOrder.IsReviewed;
                            }
                        }
                    }
                    else
                    {
                        btnReview.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.ReviewFunctionID) && !objStoreChangeOrder.IsReviewed;
                    }

                }
                bool IsCCDC = IsCanChangeProduct(6);
                ckbIsUrgent.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.URGENTorderFunctionID);

                cboFromStoreID.SetValue(objStoreChangeOrder.FromStoreID);
                LoadInputStore(objStoreChangeOrder.FromStoreID);
                cboToStoreID.SetValue(objStoreChangeOrder.ToStoreID);

                cboTransportTypeID.SetValue(objStoreChangeOrder.TransportTypeID);
                cboTransportCompany.SetValue(objStoreChangeOrder.TransportCompanyID);
                cboTransportServices.SetValue(objStoreChangeOrder.TransportServicesID);
                cboStoreChangeStatus.SelectedValue = objStoreChangeOrder.StoreChangeStatus;
                txtContent.Text = objStoreChangeOrder.Content;
                txtContentDeleted.Text = objStoreChangeOrder.ContentDeleted;
                ckbIsReview.Checked = objStoreChangeOrder.IsReviewed;
                ckbIsUrgent.Checked = objStoreChangeOrder.IsUrgent;
                if (objStoreChangeOrder.InStockStatusID < 0)
                    objStoreChangeOrder.InStockStatusID = 0;
                cboInStockStatusID.SelectedValue = objStoreChangeOrder.InStockStatusID;
                intInStockStatusID = objStoreChangeOrder.InStockStatusID;
                //if (objStoreChangeOrder.IsNew)
                //    radioIsNew.Checked = true;
                //else
                //    radioIsOld.Checked = true;
                ckbIsDeleted.Checked = objStoreChangeOrder.IsDeleted;
                grdProduct.DataSource = objStoreChangeOrder.DtbStoreChangeOrderDetail;
                grdViewProduct.Columns["IMEI"].Visible = bolInputIMEI;
                grdReviewLevel.DataSource = objStoreChangeOrder.DtbReviewLevel;
                grdAttachment.DataSource = objStoreChangeOrder.DtbAttachment;
                intFromStore = objStoreChangeOrder.FromStoreID;
                intToStore = objStoreChangeOrder.ToStoreID;

                if (objStoreChangeOrder.DtbReviewLevel == null || objStoreChangeOrder.DtbReviewLevel.Rows.Count == 0)
                {
                    //cboStatus.Enabled = false;
                    //btnReview.Enabled = false;
                    cmdReview.Enabled = false;
                }
                //load combobox status cua muc duyet
                if (objStoreChangeOrder.IsReviewed)
                {
                    btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(strEdit_AFReview) && objStoreChangeOrder.StoreChangeStatus == 0;
                    btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.DeleteAfterReviewFunctionID);
                    //cboStatus.SelectedIndex = 3;
                    //cboStatus.Enabled = false;
                    //btnReview.Enabled = false;
                    cmdReview.Text = "Đồng ý";
                    cmdReview.ImageIndex = 3;
                    cmdReview.Enabled = false;
                    CreateMainGroupMenuItem();
                    dropdownStoreChange.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.StoreChangeFunctionID);
                    btnStoreChangeAll.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.StoreChangeFunctionID);
                    cboFromStoreID.Enabled = false;
                    cboToStoreID.Enabled = false;
                }

                if (objStoreChangeOrder.IsExpired)
                {
                    dropdownStoreChange.Enabled = false;
                    btnStoreChangeAll.Enabled = false;
                    if (objStoreChangeOrder.StoreChangeStatus == 1)
                    {
                        cboStoreChangeStatus.Enabled = true;
                        btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.EditFunctionID);
                        if (objStoreChangeOrder.IsReviewed)
                        {
                            btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(strEdit_AFReview) && objStoreChangeOrder.StoreChangeStatus == 0;
                        }
                        LoadStoreChangeStatus(true);
                    }
                }
                if (objStoreChangeOrder.IsDeleted)
                    EnableButton(false);
                if (objStoreChangeOrder.StoreChangeStatus > 0)
                {
                    btnDelete.Enabled = false;
                }
                if (objStoreChangeOrder.OrderDate != null && ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.STORECHANGE,
                    objStoreChangeOrder.OrderDate.Value, objStoreChangeOrder.CreatedStoreID))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Tháng " + objStoreChangeOrder.OrderDate.Value.Month.ToString() + " đã khóa sổ. Bạn không thể chỉnh sửa.");
                    EnableButton(false);
                }
                //if (objStoreChangeOrderType.IsCanAddNewProduct)
                //{
                btnTrinhky.Visible = IsCCDC && objStoreChangeOrder.IsReviewed && FormState != FormStateType.ADD; ;
                btnVOffice.Visible = IsCCDC && objStoreChangeOrder.IsReviewed && FormState != FormStateType.ADD && objStoreChangeOrder.StoreChangeStatus != 2 && objStoreChangeOrder.StoreChangeStatus != 3;
                dropdownStoreChange.Visible = !IsCCDC;
                btnStoreChangeAll.Visible = !IsCCDC;
                txtSearchProduct.Enabled = !objStoreChangeOrder.IsReviewed;
                btnSearchProduct.Enabled = !objStoreChangeOrder.IsReviewed;
                btnTrinhky.Visible = false;
                dteExpiryDate.Enabled = !objStoreChangeOrder.IsReviewed || (objStoreChangeOrder.IsReviewed && objStoreChangeOrder.StoreChangeStatus == 0);
                //  btnUpdate.Enabled = !objStoreChangeOrder.IsReviewed || (objStoreChangeOrder.IsReviewed && objStoreChangeOrder.StoreChangeStatus == 0);
                //  }
                if (IsTaoXuatChuyenKhoCCDC)
                {
                    btnReview.Visible = false;
                }


            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin yêu cầu chuyển kho");
                SystemErrorWS.Insert("Lỗi nạp thông tin yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Tuấn thêm hàm chức năng chuyển kho
        /// Tạo danh sách ngành hàng
        /// </summary>
        private void CreateMainGroupMenuItem()
        {
            try
            {
                ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutpputType = null;
                bool bolIsFullOutput = false;
                if (objStoreChangeType != null)
                {
                    new ERP.MasterData.PLC.MD.PLCOutputType().LoadInfo(ref objOutpputType, (int)objStoreChangeType.OutputTypeID);
                    if (objOutpputType != null)
                        bolIsFullOutput = objOutpputType.IsFullOutput;
                }

                List<string> lstTag = new List<string>();
                DataTable dtbData = new PLCStoreChangeOrder().CreateMainGroupMenuItem(objStoreChangeOrder.StoreChangeOrderID.Trim());
                mnuSelectMainGroupID.ItemLinks.Clear();

                if (dtbData != null)
                {
                    foreach (DataRow dtRow in dtbData.Rows)
                    {
                        lstTag.Add(Convert.ToString(dtRow["MAINGROUPID"]).Trim());
                        if (!bolIsFullOutput)
                        {
                            DevExpress.XtraBars.BarButtonItem mnuItem = new DevExpress.XtraBars.BarButtonItem();
                            mnuItem.Caption = Convert.ToString(dtRow["MAINGROUPNAME"]).Trim();
                            mnuItem.Tag = Convert.ToString(dtRow["MAINGROUPID"]).Trim();
                            mnuItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuItem_ItemClick);
                            mnuSelectMainGroupID.AddItem(mnuItem);
                        }

                    }
                }

                if (dtbData == null || dtbData.Rows.Count == 0)
                {
                    lblWarning.Visible = true;
                    dropdownStoreChange.Visible = false;
                    btnStoreChangeAll.Visible = false;
                }
                else
                {
                    btnStoreChangeAll.Visible = false;
                    DataTable dtbCheckAll = new ERP.Inventory.PLC.PLCDataSource().GetHeavyDataSource("MD_OUPUTTYPE_GETOUTPUTALL",
                        new object[] { "@STORECHANGEORDERID", strStoreChangeOrderID });
                    if (dtbCheckAll != null && dtbCheckAll.Rows.Count > 0)
                    {
                        if (dtbCheckAll.Rows[0]["ISOUTPUTALLMAINGROUP"].ToString() == "1")
                        {
                            DevExpress.XtraBars.BarButtonItem mnuItem = new DevExpress.XtraBars.BarButtonItem();
                            mnuItem.Caption = Convert.ToString("Tất cả").Trim();
                            mnuItem.Tag = string.Join(",", lstTag);
                            mnuItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuItem_ItemClick);
                            mnuSelectMainGroupID.AddItem(mnuItem);

                            btnStoreChangeAll.Visible = dtbCheckAll.Rows[0]["ISFULLOUTPUT"].ToString() == "1";
                            btnStoreChangeAll.Tag = string.Join(",", lstTag);
                            dropdownStoreChange.Visible = dtbCheckAll.Rows[0]["ISFULLOUTPUT"].ToString() != "1";
                        }
                    }
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tạo danh sách ngành hàng", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tạo danh sách ngành hàng", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void StoreChangeAction(string strMainGroupList)
        {
            if (Globals.DateDiff(dteExpiryDate.Value, Globals.GetServerDateTime(), Globals.DateDiffType.DATE) < 0)
            {
                MessageBox.Show(this, "Yêu cầu này đã hết hạn chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            Library.AppCore.DataSource.FilterObject.StoreFilter objFromStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objFromStoreFilter.IsCheckPermission = true;
            objFromStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGE;
            DataTable dtbStore = Library.AppCore.DataSource.GetDataFilter.GetStore(objFromStoreFilter);
            var drStorePermission = dtbStore.Select("STOREID=" + cboFromStoreID.StoreID);
            if (drStorePermission == null || drStorePermission.Count() == 0)
            {
                MessageBox.Show(this, "Bạn không có quyền chuyển kho trên kho xuất " + cboFromStoreID.StoreName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //Lấy danh sách người nhận thông báo
            CreateTableUserNotify();
            GetListNotification_UserList(5, objStoreChangeOrder.FromStoreID, 1);
            GetListNotification_UserList(5, objStoreChangeOrder.ToStoreID, 2);
            GetListNotification_UserList(5, objStoreChangeOrder.CreatedStoreID, 3);


            frmStoreChange frmStoreChange = new StoreChange.frmStoreChange();
            frmStoreChange.StoreChangeOrderID = txtStoreChangeOrderID.Text.Trim();
            frmStoreChange.StoreChangeOrderType = objStoreChangeOrderType;
            if (!strMainGroupList.Contains(","))
            {
                frmStoreChange.MainGroupID = Convert.ToInt32(strMainGroupList);
            }
            else
                frmStoreChange.ListMainGroupId = strMainGroupList;
            frmStoreChange.IsRequestIMEI = objStoreChangeOrderType.IsRequestDetailIMEI;
            frmStoreChange.dtbNotification_User = dtbNotification_User;
            frmStoreChange.ShowDialog();
            if (frmStoreChange.IsUpdate)
            {
                this.Close();
                this.bolIsHasAction = true;
            }
        }

        private void mnuItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StoreChangeAction(e.Item.Tag.ToString().Trim());
        }

        private decimal GetQuantity(ref decimal decTotalQuantity, ref decimal decTotalStoreQuantity)
        {

            for (int i = 0; i < grdViewProduct.RowCount; i++)
            {
                decimal decQuantity = 0;
                decimal decStoreQuantity = 0;
                Decimal.TryParse(grdViewProduct.GetRowCellValue(i, "QUANTITY").ToString(), out decQuantity);
                Decimal.TryParse(grdViewProduct.GetRowCellValue(i, "STORECHANGEQUANTITY").ToString(), out decStoreQuantity);
                decTotalQuantity += decQuantity;
                decStoreQuantity += decStoreQuantity;
            }
            return 0;
        }
        /// <summary>
        /// Hàm bind dữ liệu vào object
        /// </summary>
        /// <returns></returns>
        private bool BindObject()
        {
            try
            {
                if (objStoreChangeOrder == null)
                    objStoreChangeOrder = new StoreChangeOrder();
                objStoreChangeOrder.StoreChangeOrderID = txtStoreChangeOrderID.Text.Trim();
                objStoreChangeOrder.StoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                objStoreChangeOrder.StoreChangeTypeID = Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID);
                objStoreChangeOrder.ExpiryDate = dteExpiryDate.Value;
                objStoreChangeOrder.OrderDate = dteOrderDate.Value;
                objStoreChangeOrder.ContentDeleted = txtContentDeleted.Text;
                objStoreChangeOrder.FromStoreName = cboFromStoreID.StoreName;
                objStoreChangeOrder.ToStoreName = cboToStoreID.StoreName;
                objStoreChangeOrder.Content = txtContent.Text;
                objStoreChangeOrder.FromStoreID = cboFromStoreID.StoreID;
                objStoreChangeOrder.ToStoreID = cboToStoreID.StoreID;
                if (cboTransportTypeID.ColumnID != null)
                    objStoreChangeOrder.TransportTypeID = Convert.ToInt32(cboTransportTypeID.ColumnID);
                if (cboStoreChangeStatus.SelectedValue != null)
                    objStoreChangeOrder.StoreChangeStatus = Convert.ToInt32(cboStoreChangeStatus.SelectedValue);
                else
                    objStoreChangeOrder.StoreChangeStatus = 0;

                DataRowView drStockStatus = (DataRowView)cboInStockStatusID.SelectedItem;

                objStoreChangeOrder.IsNew = Convert.ToBoolean(drStockStatus["ISNEW"]);// radioIsNew.Checked;
                objStoreChangeOrder.InStockStatusID = Convert.ToInt32(drStockStatus["INSTOCKSTATUSID"]);


                objStoreChangeOrder.IsUrgent = ckbIsUrgent.Checked;
                objStoreChangeOrder.CreatedStoreID = SystemConfig.intDefaultStoreID;
                decimal decToltal = 0;
                decimal decTotalStoreQuantity = 0;
                GetQuantity(ref decToltal, ref decTotalStoreQuantity);
                objStoreChangeOrder.TotalQuantity = decToltal;
                objStoreChangeOrder.TotalStoreChangeQuantity = decTotalStoreQuantity;
                objStoreChangeOrder.DtbStoreChangeOrderDetail = grdProduct.DataSource as DataTable;
                objStoreChangeOrder.DtbStoreChangeOrderDetail.AcceptChanges();
                objStoreChangeOrder.DtbReviewLevel = grdReviewLevel.DataSource as DataTable;
                if (objStoreChangeOrder.IsReviewed)
                    objStoreChangeOrder.DtbReviewLevel = null;
                else
                    objStoreChangeOrder.DtbReviewLevel.AcceptChanges();
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy dữ liệu để xử lý.", ex.ToString());
                return false;
            }
            return true;
        }

        private void GetListNotification_RVL_UserList(int intReviewLevelID, int intStoreID, int intStoreType)
        {
            try
            {
                DataTable dtbUserNotify = Library.AppCore.DataSource.GetDataSource.GetSCOrderType_RVL_Notify();
                if (dtbNotification_User == null)
                    CreateTableUserNotify();
                string strFilter = @"ReviewLevelID={0} and (STOREID={1} and (STORETYPE={2} or STORETYPE=0))";
                strFilter = string.Format(strFilter, intReviewLevelID, intStoreID, intStoreType);
                if (dtbUserNotify != null && dtbUserNotify.Rows.Count > 0)
                {
                    DataRow[] drowList = dtbUserNotify.Select(strFilter);
                    if (drowList != null && drowList.Length > 0)
                    {
                        foreach (var item in drowList)
                        {
                            if (dtbNotification_User.Select(string.Format("UserName='{0}'", item["UserName"])).Length > 0)
                                continue;
                            DataRow row = dtbNotification_User.NewRow();
                            row["USERNAME"] = item["USERNAME"];
                            row["ISSHIFTCONSIDER"] = item["ISSHIFTCONSIDER"];
                            dtbNotification_User.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception objExec)
            {
                SystemErrorWS.Insert("Lỗi lấy danh sách người nhận thông báo", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        private void CreateTableUserNotify()
        {
            dtbNotification_User = new DataTable();
            dtbNotification_User.TableName = "dtbNotification_User";
            dtbNotification_User.Columns.Add("USERNAME", typeof(string));
            dtbNotification_User.Columns.Add("ISSHIFTCONSIDER", typeof(Int16));
        }

        private void GetListNotification_UserList(int intStoreChangeOrderEventID, int intStoreID, int intStoreType)
        {
            try
            {
                DataTable dtbUserNotify = Library.AppCore.DataSource.GetDataSource.GetSCOrderType_Notify();
                if (dtbNotification_User == null)
                    CreateTableUserNotify();
                string strFilter = @"STORECHANGEORDERTYPEID={0} and STORECHANGEORDEREVENTID={1} and (STOREID={2} and (STORETYPE={3} or STORETYPE=0))";
                strFilter = string.Format(strFilter, objStoreChangeOrderType.StoreChangeOrderTypeID, intStoreChangeOrderEventID, intStoreID, intStoreType);
                if (dtbUserNotify != null && dtbUserNotify.Rows.Count > 0)
                {
                    DataRow[] drowList = dtbUserNotify.Select(strFilter);
                    if (drowList != null && drowList.Length > 0)
                    {
                        foreach (var item in drowList)
                        {
                            if (dtbNotification_User.Select(string.Format("UserName='{0}'", item["UserName"])).Length > 0)
                                continue;
                            DataRow row = dtbNotification_User.NewRow();
                            row["USERNAME"] = item["USERNAME"];
                            row["ISSHIFTCONSIDER"] = item["ISSHIFTCONSIDER"];
                            dtbNotification_User.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception objExec)
            {
                SystemErrorWS.Insert("Lỗi lấy danh sách người nhận thông báo", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        /// <summary>
        /// Hàm cập nhật dữ liệu
        /// Thêm, chỉnh sửa
        /// </summary>
        /// <returns>true or false</returns>
        private bool UpdateData()
        {
            if (!ValidateProduct()) return false;
            string strMessage = string.Empty;
            try
            {
                //Library.AppCore.Forms.frmWaitDialog.Show("", "Đang cập nhật dữ liệu");
                btnUpdate.Enabled = false;
                bool bolIsReview = false;
                if (objStoreChangeOrderType.IsAutoReview && FormState == FormStateType.ADD || (objStoreChangeOrderType.IsAutoReview && IsTaoXuatChuyenKhoCCDC))
                {
                    //tu dong duyet
                    string strError = string.Empty;
                    bolIsReview = SetReview(3, true, ref strError, false);
                    if (!string.IsNullOrEmpty(strError))
                    {
                        //Library.AppCore.Forms.frmWaitDialog.Close();
                        MessageBoxObject.ShowWarningMessage(this, strError);
                        //return false;
                    }
                    if (IsTaoXuatChuyenKhoCCDC)
                    {
                        //Xuat chuyen kho tu dong
                        String strMainGroupList = btnStoreChangeAll.Tag.ToString().Trim();
                        frmStoreChange frmStoreChange = new StoreChange.frmStoreChange();
                        frmStoreChange.StoreChangeOrderID = txtStoreChangeOrderID.Text.Trim();
                        frmStoreChange.StoreChangeOrderType = objStoreChangeOrderType;
                        if (!strMainGroupList.Contains(","))
                        {
                            frmStoreChange.MainGroupID = Convert.ToInt32(strMainGroupList);
                        }
                        else
                            frmStoreChange.ListMainGroupId = strMainGroupList;
                        frmStoreChange.IsRequestIMEI = objStoreChangeOrderType.IsRequestDetailIMEI;
                        frmStoreChange.dtbNotification_User = dtbNotification_User;
                        frmStoreChange.prepaidDataForCCDC();
                        string message = frmStoreChange.InsertStoreChangeCCDC();
                        if (message.Equals("Chuyển kho thành công"))
                        {
                            MessageBoxObject.ShowInfoMessage(this, message);
                        }
                        else
                        {
                            MessageBoxObject.ShowErrorMessage(this, message);
                        }
                        EditData(true);
                    }
                }
                if (!BindObject())
                {
                    //Library.AppCore.Forms.frmWaitDialog.Close();
                    return false;
                }
                if (FormState == FormStateType.ADD)
                {
                    GetListNotification_UserList(1, objStoreChangeOrder.FromStoreID, 1);
                    GetListNotification_UserList(1, objStoreChangeOrder.ToStoreID, 2);
                    GetListNotification_UserList(1, objStoreChangeOrder.CreatedStoreID, 3);
                    objStoreChangeOrder.UserNotifyData = dtbNotification_User;
                    objStoreChangeOrder.TransportCompanyID = cboTransportCompany.ColumnID;
                    objStoreChangeOrder.TransportServicesID = cboTransportServices.ColumnID;
                    if (objStoreChangeOrderType.IsAutoReview && !bolIsReview && grdViewReviewLevel.RowCount < 1)
                        bolIsReview = true;
                    objStoreChangeOrder.IsReviewed = bolIsReview;
                    if (objStoreChangeOrder.IsReviewed)
                    {

                        objStoreChangeOrder.ReviewedDate = Globals.GetServerDateTime();
                        objStoreChangeOrder.ReviewedUser = SystemConfig.objSessionUser.UserName;
                    }
                    txtStoreChangeOrderID.Text = objPLCStoreChangeOrder.Insert(objStoreChangeOrder);

                    strMessage = "Thêm";
                    strStoreChangeOrderID = string.Empty;
                }
                else if (FormState == FormStateType.EDIT)
                {
                    if (IsTaoXuatChuyenKhoCCDC)
                    {
                        MessageBox.Show("Insert storeChangeOrder");
                        GetListNotification_UserList(2, objStoreChangeOrder.FromStoreID, 1);
                        GetListNotification_UserList(2, objStoreChangeOrder.ToStoreID, 2);
                        GetListNotification_UserList(2, objStoreChangeOrder.CreatedStoreID, 3);
                        objStoreChangeOrder.UserNotifyData = dtbNotification_User;
                        objStoreChangeOrder.TransportCompanyID = cboTransportCompany.ColumnID;
                        objStoreChangeOrder.TransportServicesID = cboTransportServices.ColumnID;
                        objPLCStoreChangeOrder.Insert(objStoreChangeOrder);
                        strMessage = "Thêm";
                    }
                    else
                    {
                        GetListNotification_UserList(2, objStoreChangeOrder.FromStoreID, 1);
                        GetListNotification_UserList(2, objStoreChangeOrder.ToStoreID, 2);
                        GetListNotification_UserList(2, objStoreChangeOrder.CreatedStoreID, 3);
                        objStoreChangeOrder.TransportCompanyID = cboTransportCompany.ColumnID;
                        objStoreChangeOrder.TransportServicesID = cboTransportServices.ColumnID;
                        objStoreChangeOrder.UserNotifyData = dtbNotification_User;
                        objPLCStoreChangeOrder.Update(objStoreChangeOrder);
                        strMessage = "Cập nhật";
                    }
                }
            }
            catch (Exception objExce)
            {
                //Library.AppCore.Forms.frmWaitDialog.Close();
                MessageBoxObject.ShowWarningMessage(this, "Lỗi " + strMessage.ToLower() + " yêu cầu chuyển kho");
                SystemErrorWS.Insert("Lỗi " + strMessage.ToLower() + " yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> UpdateData", DUIInventory_Globals.ModuleName);
                return false;
            }
            finally
            {
                btnUpdate.Enabled = true;
                Library.AppCore.Forms.frmWaitDialog.Close();
            }
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            else
            {
                //đóng form hoặc load lại
                MessageBox.Show(strMessage + " yêu cầu chuyển kho thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (IsCopy)
                    this.Close() ;
                else { 
                EditData();
                txtSearchProduct.Text = string.Empty;
                }
            }

            return true;
        }
        /// <summary>
        /// Hàm kiểm tra dữ liệu
        /// </summary>
        /// <returns></returns>
        private bool ValidateData()
        {

            if (string.IsNullOrEmpty(txtStoreChangeOrderID.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Mã yêu cầu chuyển kho không thể trống!Vui lòng tắt form mở lại.");
                tabControl.SelectedTab = tabGeneral;
                txtStoreChangeOrderID.Focus();
                return false;
            }
            if (string.IsNullOrWhiteSpace(strStoreChangeOrderID))
                dteOrderDate.Value = Globals.GetServerDateTime();
            else
                dteOrderDate.Value = objStoreChangeOrder.OrderDate.Value;
            if (!ERP.MasterData.DUI.Common.CommonFunction.EndDateValidation(dteOrderDate.Value, dteExpiryDate.Value, false))
            {
                MessageBoxObject.ShowWarningMessage(this, "Ngày hết hạn không thể nhỏ hơn ngày yêu cầu!");
                tabControl.SelectedTab = tabGeneral;
                dteExpiryDate.Focus();
                return false;
            }

            if (FormState == FormStateType.ADD && !ValidateStore()) return false;
            if (cboTransportTypeID.ColumnID <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn phương tiện.");
                tabControl.SelectedTab = tabGeneral;
                cboTransportTypeID.Focus();
                return false;
            }
            if (cboTransportCompany.ColumnID <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn Đối tác.");
                tabControl.SelectedTab = tabGeneral;
                cboTransportCompany.Focus();
                return false;
            }
            if (cboTransportServices.ColumnID <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn Dịch vụ.");
                tabControl.SelectedTab = tabGeneral;
                cboTransportServices.Focus();
                return false;
            }
            grdProduct.RefreshDataSource();
            (grdProduct.DataSource as DataTable).AcceptChanges();
            if (grdProduct.DataSource == null || (grdProduct.DataSource as DataTable).Rows.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không có sản phẩm nào để chuyển.");
                tabControl.SelectedTab = tabGeneral;
                grdProduct.Focus();
                return false;
            }
            if (IsTaoXuatChuyenKhoCCDC)
            {
                if (string.IsNullOrEmpty((grdProduct.DataSource as DataTable).Rows[0]["IMEI"].ToString()))
                {
                    MessageBoxObject.ShowErrorMessage(this, "IMEI không được để trống, hãy nhập IMEI.");
                    grdProduct.Focus();
                    return false;
                }
            }




            return true;
        }
        private bool ValidateProduct()
        {
            DataTable dtb = (grdProduct.DataSource as DataTable).Copy();
            for (int i = 0; i < dtb.Rows.Count; i++)
            {
                decimal Quantity = 0;
                decimal.TryParse(dtb.Rows[i]["QUANTITY"].ToString(), out Quantity);
                if (Quantity <= 0)
                {
                    grdViewProduct.SelectRow(i);
                    MessageBoxObject.ShowWarningMessage(this, "Số lượng sản phẩm yêu cầu chuyển phải lớn hơn 0.");
                    return false;
                }
            }
            return true;
        }
        private string ValidateImei(string strIMEI, string strProductID, string strProductName)
        {
            DataTable dtb = (grdProduct.DataSource as DataTable).Copy();
            //lay nhung imei null
            string strImeiNull = string.Empty;
            //lay nhung imei dat hang 
            string strImeiOder = string.Empty;
            //lay nhung imei da ton tai
            string strImeiExist = string.Empty;
            //lay nhung imei dang la hang trung bay
            //string strImeiShowing = string.Empty;
            int intFromStoreID = cboFromStoreID.StoreID;
            //if (cboFromStoreID.SelectedValue != null)
            //    Int32.TryParse(cboFromStoreID.SelectedValue.ToString(), out intFromStoreID);
            if (!string.IsNullOrWhiteSpace(strIMEI.Trim()))
            {
                //lay nhung imei khong thuoc san pham da chon
                string strImeiNotSameProductID = string.Empty;
                strIMEI = strIMEI.Replace("\r\n", ",").Replace("\n", ",").Replace(";", ",");
                foreach (string imei in strIMEI.Split(','))
                {
                    if (!string.IsNullOrEmpty(imei.Trim()))
                    {
                        //imei khong trung voi ma san pham
                        if (string.Equals(imei.Trim(), strProductID.Trim()))
                        {
                            return "Imei: " + imei.Trim() + " đã trùng với mã sản phẩm. Vui lòng nhập imei khác.";
                        }
                        if (grdViewProduct.FocusedRowHandle >= 0)
                            dtb.Rows.RemoveAt(grdViewProduct.FocusedRowHandle);
                        DataRow[] rowImei = dtb.Select("IMEI = '" + imei.Trim() + "'");
                        if (rowImei.Count() > 0)
                        {
                            strImeiExist = "," + imei.Trim();
                        }
                        else
                        {
                            PLC.WSProductInStock.ProductInStock objProductInStock = new PLC.PLCProductInStock().LoadInfo(imei, intFromStoreID, Convert.ToInt32(cboInStockStatusID.SelectedValue), true);
                            if (objProductInStock == null)
                            {
                                strImeiNull += "," + imei.Trim();
                            }
                            else if (objProductInStock.IsOrder)
                            {
                                strImeiOder += "," + imei.Trim() + " đã đặt trong đơn hàng(" + objProductInStock.OrderID + ") rồi.";
                            }
                            else if (objProductInStock.ProductID.Trim() != strProductID)
                            {
                                strImeiNotSameProductID += "," + imei.Trim();
                            }
                        }
                    }
                }
                if (!string.IsNullOrWhiteSpace(strImeiExist.Trim()))
                {
                    if (strImeiExist[0] == ',') strImeiExist = strImeiExist.Remove(0, 1);
                    //MessageBoxObject.ShowWarningMessage(this, "Imei: " + strImeiExist + " này đã tồn tại rồi. Vui lòng nhập imei khác.");
                    return "Imei: " + strImeiExist + " này đã tồn tại trong danh sách yêu cầu rồi. Vui lòng nhập imei khác.";
                }
                if (!string.IsNullOrWhiteSpace(strImeiNotSameProductID))
                {
                    if (strImeiNotSameProductID[0] == ',') strImeiNotSameProductID = strImeiNotSameProductID.Remove(0, 1);
                    //MessageBoxObject.ShowWarningMessage(this, "Imei: " + strImeiNotSameProductID + " không thuộc sản phẩm " + strProductName);
                    return "Imei: " + strImeiNotSameProductID + " không thuộc sản phẩm " + strProductName;
                }

            }
            if (!string.IsNullOrWhiteSpace(strImeiNull.Trim()))
            {
                if (strImeiNull[0] == ',') strImeiNull = strImeiNull.Remove(0, 1);
                //MessageBoxObject.ShowWarningMessage(this, "Imei: " + strImeiNull + " không tồn tại trong hệ thống.");
                return "Imei: " + strImeiNull + " không tồn tại trong hệ thống.";
            }
            if (!string.IsNullOrWhiteSpace(strImeiOder.Trim()))
            {
                if (strImeiOder[0] == ',') strImeiOder = strImeiOder.Remove(0, 1);
                //MessageBoxObject.ShowWarningMessage(this, "Imei: " + strImeiOder + " đã đặt hàng rồi.");
                return "Imei: " + strImeiOder;
            }
            return string.Empty;
        }

        private bool ValidateStore()
        {
            if (cboFromStoreID.StoreID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho xuất.");
                tabControl.SelectedTab = tabGeneral;
                cboFromStoreID.Focus();
                return false;
            }
            if (cboToStoreID.StoreID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho nhập.");
                tabControl.SelectedTab = tabGeneral;
                cboToStoreID.Focus();
                return false;
            }
            if (cboToStoreID.Enabled)
            {
                if (cboToStoreID.StoreID == cboFromStoreID.StoreID)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Kho nhập phải khác kho xuất.");
                    tabControl.SelectedTab = tabGeneral;
                    cboToStoreID.Focus();
                    return false;
                }
            }
            if (Convert.ToInt32(cboInStockStatusID.SelectedValue) < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn trạng thái sản phẩm.");
                tabControl.SelectedTab = tabGeneral;
                cboInStockStatusID.Focus();
                return false;
            }
            return true;
        }

        private string ShowReason(string strTitle, int MaxLengthString)
        {
            string strReasion = "-1";
            Library.AppControl.FormCommon.frmInputString frm = new Library.AppControl.FormCommon.frmInputString();
            frm.MaxLengthString = MaxLengthString;
            frm.IsAllowEmpty = false;
            frm.Text = strTitle;
            frm.ShowDialog();
            if (frm.IsAccept)
            {
                strReasion = frm.ContentString;
            }

            return strReasion;
        }


        /// <summary>
        /// Hàm xóa dữ liệu
        /// </summary>
        /// <returns>true or false</returns>
        private bool DeleteData()
        {
            try
            {

                Library.AppCore.DataSource.FilterObject.StoreFilter objFromStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objFromStoreFilter.IsCheckPermission = true;
                objFromStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGE;
                DataTable dtbStore = Library.AppCore.DataSource.GetDataFilter.GetStore(objFromStoreFilter);
                var drStorePermission = dtbStore.Select("STOREID=" + cboFromStoreID.StoreID);
                if (drStorePermission == null || drStorePermission.Count() == 0)
                {
                    MessageBox.Show(this, "Bạn không có quyền chuyển kho trên kho xuất " + cboFromStoreID.StoreName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                if (objStoreChangeOrder == null)
                {
                    MessageBox.Show(this, "Lỗi lấy thông tin yêu cầu chuyển kho để hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                if (objStoreChangeOrder.StoreChangeStatus > 0)
                {
                    MessageBox.Show(this, "Yêu cầu này đã xuất chuyển kho rồi. Không thể hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                //if (string.IsNullOrWhiteSpace(txtContentDeleted.Text))
                //{
                //    MessageBox.Show(this, "Vui lòng nhập lý do hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    tabControl.SelectedTab = tabGeneral;
                //    txtContentDeleted.Focus();
                //    return false;
                //}
                //if (MessageBox.Show(this, "Bạn có chắc muốn hủy yêu cầu chuyển kho đang chọn không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                //    return false;

                //Hiển thị form Nhập lý do hủy

                string Reason = ShowReason("Nội dung hủy", 2000);
                if (Reason == "-1" || string.IsNullOrWhiteSpace(Reason))
                    return false;
                objStoreChangeOrder.ContentDeleted = Reason;
                CreateTableUserNotify();
                GetListNotification_UserList(3, objStoreChangeOrder.FromStoreID, 1);
                GetListNotification_UserList(3, objStoreChangeOrder.ToStoreID, 2);
                GetListNotification_UserList(3, objStoreChangeOrder.CreatedStoreID, 3);
                objStoreChangeOrder.UserNotifyData = dtbNotification_User;
                objPLCStoreChangeOrder.Delete(objStoreChangeOrder);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                MessageBox.Show(this, "Hủy yêu cầu thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.FormManager.SearchData();
                this.Close();
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi hủy thông tin yêu cầu chuyển kho");
                SystemErrorWS.Insert("Lỗi hủy thông tin yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> DeleteData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private bool CheckProduct(ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock)
        {

            if (!objProductInStock.IsInstock || objProductInStock.Quantity <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " không tồn kho trong kho " + cboFromStoreID.StoreName);
                return false;
            }

            if (objProductInStock.IsService && !objProductInStock.IsCheckStockQuantity)
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn không được tạo yêu cầu trên ngành hàng này!");
                return false;
            }


            //if (!objProductInStock.IsNew && radioIsNew.Checked)
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " là hàng cũ. Vui lòng chọn lại.");
            //    return false;
            //}
            if (objStoreChangeOrderType.IsRequestDetailIMEI && !objProductInStock.IsRequestIMEI)
            {
                MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " không có IMEI. Vui lòng nhập sản phẩm khác có IMEI.");
                return false;
            }

            ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
            decimal decOutPutQuantity = 0;// số lượng cho mượn sản phẩm//
            objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOutPutQuantity, cboFromStoreID.StoreID, objProductInStock.ProductID, objProductInStock.IMEI);
            if (string.IsNullOrWhiteSpace(objProductInStock.IMEI))
            {
                if (decOutPutQuantity > 0)
                {
                    objProductInStock.Quantity = objProductInStock.Quantity - decOutPutQuantity;

                }
                if (objProductInStock.Quantity <= 0)
                {
                    MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductID + " - " + objProductInStock.ProductName + " không tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            return true;
        }
        private bool CheckImei(ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock)
        {

            if (!Library.AppCore.Other.CheckObject.CheckIMEI(objProductInStock.IMEI))
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI không đúng định dạng.");
                return false;
            }
            if (objProductInStock.IsOrder)
            {
                MessageBoxObject.ShowWarningMessage(this, "IMEI này đã được đặt trong đơn hàng ( " + objProductInStock.OrderID + " ) rồi. Vui lòng nhập IMEI khác.");
                return false;
            }
            if (objProductInStock.IsShowProduct && MessageBox.Show("IMEI này đang là hàng trưng bày. Bạn có muốn chuyển IMEI này không.", "Thông báo", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
            {
                return false;
            }
            if (objProductInStock.IsDelivery)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI này đã giao cho khách");
                return false;
            }
            else if (!objProductInStock.IsCheckRealInput && objProductInStock.IMEI.Length > 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI này chưa được xác nhận nhập kho");
                return false;
            }
            else if (objProductInStock.IsOrder) // kiểm tra IMEI có được đặt hàng hay chưa
            {
                string strError = "Số IMEI " + objProductInStock.IMEI + " đã được đặt hàng trong đơn hàng: \"" + objProductInStock.OrderID + "\"";
                MessageBoxObject.ShowWarningMessage(this, strError);
                return false;
            }
            if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI))
            {
                ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                decimal decOutPutQuantity = 0;// số lượng cho mượn sản phẩm//
                objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOutPutQuantity, cboFromStoreID.StoreID, objProductInStock.ProductID, objProductInStock.IMEI);
                if (decOutPutQuantity > 0)
                {
                    MessageBox.Show(this, "IMEI [" + objProductInStock.IMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            if (objProductInStock.StatusFIFOID == 1)
            {
                DialogResult drResult =
                    ClsFIFO.ShowMessengerFIFOList(objProductInStock.StatusFIFO, objProductInStock.ProductID, objProductInStock.IMEI, cboFromStoreID.StoreID, -1);
                // MessageBox.Show(this, objProductInStock.StatusFIFO, "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (drResult != System.Windows.Forms.DialogResult.OK)
                {
                    new PLC.PLCProductInStock().Update_IsOrderingWithIMEI(objProductInStock.IMEI.ToString().Trim(), 0, cboFromStoreID.StoreID);
                    return false;
                }

            }
            if (objProductInStock.StatusFIFOID == 2
                && objProductInStock.StatusFIFO.IndexOf("<2>", 0) < 0
                )
            {
                DialogResult drResult =
                    ClsFIFO.ShowMessengerFIFOList("IMEI " + objProductInStock.IMEI.ToString().Trim() + " vi phạm xuất fifo bạn có muốn tiếp tục?",
                    objProductInStock.ProductID, objProductInStock.IMEI, cboFromStoreID.StoreID, -1);
                //MessageBox.Show(this, , "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (drResult != System.Windows.Forms.DialogResult.OK)
                {
                    new PLC.PLCProductInStock().Update_IsOrderingWithIMEI(objProductInStock.IMEI.ToString().Trim(), 0, cboFromStoreID.StoreID);
                    return false;
                }
            }
            return true;
        }
        private string GetIMEI(string strProduct)
        {
            string strImeiList = string.Empty;
            for (int i = 0; i < grdViewProduct.RowCount; i++)
            {
                string productID = grdViewProduct.GetRowCellValue(i, "PRODUCTID").ToString().Trim();
                if (productID == strProduct)
                {
                    string imei = grdViewProduct.GetRowCellValue(i, "IMEI").ToString().Trim();
                    if (!string.IsNullOrEmpty(imei))
                        strImeiList += "<" + imei + ">";
                }
            }
            return strImeiList;
        }
        private bool AddProduct(string strProduct, int intQuantity)
        {
            if (string.IsNullOrWhiteSpace(strProduct))
                return false;
            int intFromStoreID = cboFromStoreID.StoreID;
            string strImeiList = string.Empty;
            ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = null;
            if (bolInputIMEI)
            {
                strImeiList = GetIMEI(strProduct);
                objProductInStock = new ERP.Inventory.PLC.PLCProductInStock().LoadInfo(strProduct, intFromStoreID, (int)(objStoreChangeType == null ? 0 : objStoreChangeType.OutputTypeID), strImeiList, true, Convert.ToInt32(cboInStockStatusID.SelectedValue), true);
            }
            else
                objProductInStock = new PLC.PLCProductInStock().LoadInfo(strProduct, cboFromStoreID.StoreID, 0, string.Empty, false, Convert.ToInt32(cboInStockStatusID.SelectedValue), true);
            if (objProductInStock == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Sản phẩm không tồn tại trong hệ thống hoặc số imei không có trong kho.");
                return false;
            }
            if (objProductInStock.IsOrder)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI đang được giữ cho nghiệp vụ có mã \"" + objProductInStock.OrderID + "\"!");
                return false;
            }
            if (objProductInStock != null && strProduct.Trim().Equals(objProductInStock.IMEI.Trim()) && !bolInputIMEI)
            {
                MessageBoxObject.ShowWarningMessage(this, "Chỉ cho phép nhập mã sản phẩm ở chế độ này");
                return false;
            }

            if(!objStoreChangeOrderType.ProductStatus.Contains(objProductInStock.InStockStatusID))
            {
                MessageBoxObject.ShowWarningMessage(this, "Trạng thái sản phẩm không nằm trong loại yêu cầu chuyển kho");
                return false;
            }

            // Neu san pham ban vao = ma san pham
            if (strProduct.Trim().Equals(objProductInStock.ProductID.Trim()))
            {
                if (Convert.ToInt32(cboInStockStatusID.SelectedValue) != 1
                    //&& !string.IsNullOrEmpty(objProductInStock.IMEI.Trim())
                    //&& objProductInStock.InStockStatusID != Convert.ToInt32(cboInStockStatusID.SelectedValue)
                    )
                {
                    objProductInStock = new PLC.PLCProductInStock().LoadInfo(strProduct, cboFromStoreID.StoreID, 0, string.Empty, false, Convert.ToInt32(cboInStockStatusID.SelectedValue), true);
                    if (objProductInStock == null)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Sản phẩm không tồn tại trong hệ thống hoặc số imei không có trong kho.");
                        return false;
                    }
                    if (objProductInStock != null && strProduct.Trim().Equals(objProductInStock.IMEI.Trim()) && !bolInputIMEI)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Chỉ cho phép nhập mã sản phẩm ở chế độ này");
                        return false;
                    }
                    DataRowView drStockStatus = (DataRowView)cboInStockStatusID.SelectedItem;
                    objProductInStock.IsNew = Convert.ToBoolean(drStockStatus["ISNEW"]);
                    objProductInStock.IsShowProduct = Convert.ToBoolean(drStockStatus["ISSHOWPRODUCT"]);
                    objProductInStock.InStockStatusID = Convert.ToInt32(drStockStatus["INSTOCKSTATUSID"]);
                    objProductInStock.IMEI = string.Empty;
                }
            }

            //if (strProduct.Trim().Equals(objProductInStock.ProductID.Trim()) 
            //    && Convert.ToInt32(cboInStockStatusID.SelectedValue) != 1 
            //    && objProductInStock.IsRequestIMEI 
            //    && bolInputIMEI)
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Chỉ cho phép lấy sản phẩm với trạng thái \"Mới\"");
            //    return false;
            //}
            if (objProductInStock.IsOrder)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI đang được giữ cho nghiệp vụ có mã \"" + objProductInStock.OrderID + "\"!");
                return false;
            }
            if (strProduct.Trim().Equals(objProductInStock.IMEI.Trim())
                && objProductInStock.InStockStatusID != Convert.ToInt32(cboInStockStatusID.SelectedValue))
            {
                string strMess = "Sản phẩm " + objProductInStock.ProductName + " không thuộc trạng thái " + cboInStockStatusID.SelectedText + ". Vui lòng chọn lại.";
                if (!string.IsNullOrEmpty(objProductInStock.IMEI.Trim()))
                    strMess = "Sản phẩm " + objProductInStock.ProductName + " có IMEI " + objProductInStock.IMEI.Trim() + " không thuộc trạng thái " + cboInStockStatusID.SelectedText + ". Vui lòng chọn lại.";
                MessageBoxObject.ShowWarningMessage(this, strMess);
                return false;
            }
            if (!IsCanChangeProduct(objProductInStock.ConsignmentType))
            {
                MessageBoxObject.ShowWarningMessage(this, "Sản phẩm không thuộc loại hàng hóa trong yêu cầu xuất chuyển kho!");
                return false;
            }
            //row["QUANTITYINSTOCK"] = objProductInStockTemp.Quantity;
            if (!CheckProduct(objProductInStock)) return false;
            if (!CheckImei(objProductInStock)) return false;

            DataTable dtbProduct = grdProduct.DataSource as DataTable;
            var varSum = dtbProduct.AsEnumerable()
                        .Where(row => (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == objProductInStock.ProductID.Trim()).Sum(row => Convert.ToDecimal(row.Field<object>("QUANTITY")));// ("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "' AND IMEI = ''")
            if ((varSum + intQuantity) > objProductInStock.RealQuantityProduct && objProductInStock.IsCheckStockQuantity)
            {
                MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " không tồn đủ trong kho " + cboFromStoreID.StoreName);
                return false;
            }
            DataRow[] rowProduct = null;
            if (dtbProduct != null)
            {

                rowProduct = dtbProduct.Select("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "'");
                if (rowProduct.Count() > 0)
                {
                    //Trường hợp không cần nhập IMEI, thông báo ra và nhập số lượng
                    if (!objStoreChangeOrderType.IsRequestDetailIMEI && !objProductInStock.IsRequestIMEI)
                    {
                        if ((Convert.ToInt32(rowProduct[0]["QUANTITY"]) + intQuantity) > objProductInStock.Quantity && objProductInStock.IsCheckStockQuantity)
                            rowProduct[0]["QUANTITY"] = objProductInStock.Quantity;
                        else
                            rowProduct[0]["QUANTITY"] = Convert.ToInt32(rowProduct[0]["QUANTITY"]) + intQuantity;
                        //MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " đã tồn tại rồi. Vui lòng nhập sản phẩm khác.");
                        grdProduct.RefreshDataSource();
                        return true;
                    }

                }
                if (string.IsNullOrEmpty(objProductInStock.IMEI.Trim()))
                {
                    // Check ton 

                    rowProduct = dtbProduct.Select("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "' AND IMEI = ''");
                    if (rowProduct.Count() > 0)
                    {
                        if ((Convert.ToInt32(rowProduct[0]["QUANTITY"]) + intQuantity) > objProductInStock.RealQuantityProduct && objProductInStock.IsCheckStockQuantity)
                            rowProduct[0]["QUANTITY"] = objProductInStock.RealQuantityProduct;
                        else
                            rowProduct[0]["QUANTITY"] = Convert.ToInt32(rowProduct[0]["QUANTITY"]) + intQuantity;
                        //MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " đã tồn tại rồi. Vui lòng nhập sản phẩm khác.");
                        grdProduct.RefreshDataSource();
                        return true;
                    }
                }
                rowProduct = null;
                //lay lai row product , khi do ham kiem tra nhieu imei ko con tac dung
                if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI) && bolInputIMEI)
                {
                    rowProduct = dtbProduct.Select("IMEI = '" + objProductInStock.IMEI.Trim() + "'");
                    if (rowProduct.Count() > 0)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "IMEI " + objProductInStock.IMEI + " đã tồn tại rồi. Vui lòng nhập IMEI khác.");
                        return false;
                    }
                }
                if (strProduct.Trim() == objProductInStock.IMEI && objProductInStock.IsRequestIMEI && bolInputIMEI)
                {
                    DataRow[] rowlist = dtbProduct.Select("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "' and IMEI = ''");
                    if (rowlist.Length > 0)
                    {

                        rowlist[0]["IMEI"] = objProductInStock.IMEI;
                        rowlist[0]["QUANTITY"] = 1;
                        return true;
                    }
                }
            }

            DataTable dtbTemp = CreateTableProduct(dtbProduct, rowProduct, objProductInStock.IsRequestIMEI, objProductInStock.MainGroupID, objProductInStock.ProductID, objProductInStock.ProductName, objProductInStock.IMEI, intQuantity, 0, string.Empty, objProductInStock.Quantity, objProductInStock.IsAllowDecimal, objProductInStock.InputVoucherDate);
            if (dtbTemp.Rows.Count > 0) mnuItemExport.Enabled = true;
            grdProduct.DataSource = dtbTemp;
            return true;
        }
        private DataTable CreateTableProduct(DataTable dtbProduct, DataRow[] rowProduct, bool IsReQuestIMEI, int intMaingroupID, string strProductID, string strProductName, string strImei, int intQuantity, int intStoreChangeQuantity, string strNote, decimal QuantityInStock, bool bolIsAllowDecimal, DateTime? dtInputDate )
        {
            DataTable dtb = dtbProduct;
            if (dtb == null)
            {
                dtb = new DataTable("TableProduct");
                DataColumn[] col = new DataColumn[]{new DataColumn("STORECHANGEORDERDETAILID", typeof(String))
                                                ,new DataColumn("STORECHANGEORDERID", typeof(String))
                                                ,new DataColumn("PRODUCTID", typeof(String))
                                                ,new DataColumn("PRODUCTNAME", typeof(String))
                                                ,new DataColumn("IMEI", typeof(String))
                                                ,new DataColumn("QUANTITY", typeof(Decimal))
                                                ,new DataColumn("STORECHANGEQUANTITY", typeof(Decimal))
                                                ,new DataColumn("INPUTDATE", typeof(String))
                                                ,new DataColumn("NOTE", typeof(String))
                                                ,new DataColumn("ISREQUESTIMEI", typeof(Boolean))
                                                ,new DataColumn("QUANTITYINSTOCK", typeof(Decimal))
                                                ,new DataColumn("MAINGROUPID", typeof(Int32))
                                                ,new DataColumn("IsAllowDecimal", typeof(Boolean))
                                                ,new DataColumn("PRODUCTSTATUS", typeof(Int32))
                                                };
                dtb.Columns.AddRange(col);
                intInStockStatusID = Convert.ToInt32(cboInStockStatusID.SelectedValue);
            }
            if (dtb.Rows.Count == 0)
                intInStockStatusID = Convert.ToInt32(cboInStockStatusID.SelectedValue);
            if (rowProduct == null || rowProduct.Count() == 0)
            {
                if (IsReQuestIMEI)
                {
                    if (!(bolIsImport && rbtnQuantity.Checked))
                    {
                        intQuantity = 1;// ERP.MasterData.DUI.Common.CommonFunction.GetQuantityFromIMEI(strImei);
                        intStoreChangeQuantity = 0;
                    }

                }

                DataRow row = dtb.NewRow();
                row["STORECHANGEORDERID"] = strStoreChangeOrderID;
                row["PRODUCTID"] = strProductID;
                row["PRODUCTNAME"] = strProductName;
                row["IMEI"] = strImei;
                row["QUANTITY"] = bolIsAllowDecimal ? (QuantityInStock < 1 && QuantityInStock > 0) ? QuantityInStock : intQuantity : intQuantity;
                row["STORECHANGEQUANTITY"] = intStoreChangeQuantity;
                row["NOTE"] = strNote;
                if(dtInputDate !=null) row["INPUTDATE"] = dtInputDate;
                row["ISREQUESTIMEI"] = IsReQuestIMEI;
                row["QUANTITYINSTOCK"] = QuantityInStock;
                row["MAINGROUPID"] = intMaingroupID;
                row["ISALLOWDECIMAL"] = bolIsAllowDecimal;
                row["PRODUCTSTATUS"] = cboInStockStatusID.SelectedValue;
                if (dtbProduct != null)
                {
                    var item = dtbProduct.AsEnumerable().FirstOrDefault(x => x.Field<object>("PRODUCTID").ToString().Trim().Equals(strProductID.Trim()));

                    if (rbtnQuantity.Checked && item != null)
                    {
                        if (bolIsImport)
                        {
                            item["QUANTITY"] = Convert.ToDecimal(item["QUANTITY"]) + intQuantity;
                        }
                        else item["QUANTITY"] = Convert.ToDecimal(item["QUANTITY"]) + 1;
                    }
                    else dtb.Rows.Add(row);
                }

                else dtb.Rows.Add(row);
            }
            //nhap nhieu imei tren 1 dong
            else if (IsReQuestIMEI)
            {
                try
                {
                    int index = dtb.Rows.IndexOf(rowProduct[0]);
                    string strOldImei = Convert.ToString(grdViewProduct.GetRowCellValue(index, "IMEI")).Trim();
                    if (!string.IsNullOrWhiteSpace(strImei) && strOldImei.Contains(strImei))
                    {
                        //Hien thi thong bao imei da ton tai
                        MessageBoxObject.ShowWarningMessage(this, "IMEI " + strImei + " đã tồn tại.Vui lòng nhập IMEI khác.");
                        return dtb;
                    }
                    if (string.IsNullOrEmpty(strOldImei))
                        strImei = strOldImei;
                    else
                        strImei = strOldImei + "\r\n" + strImei;
                    int iQuantity = ERP.MasterData.DUI.Common.CommonFunction.GetQuantityFromIMEI(strImei);
                    grdViewProduct.SetRowCellValue(index, "IMEI", strImei);
                    grdViewProduct.SetRowCellValue(index, "QUANTITY", iQuantity);
                    grdViewProduct.SetRowCellValue(index, "STORECHANGEQUANTITY", iQuantity);

                }
                catch (Exception ex)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy thông tin IMEI.", ex.ToString());
                }
            }
            return dtb;
        }

        private string CheckMainGroupPermission()
        {
            if (strReviewUser == "administrator")
                return string.Empty;
            DataTable dtbMainGroupPermission = objPLCStoreChangeOrder.LoadMainGroupPermission(strReviewUser);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                return SystemConfig.objSessionUser.ResultMessageApp.Message;
            }
            string strMainGroupID = string.Empty;
            for (int i = 0; i < grdViewProduct.RowCount; i++)
            {
                int intMainGroupID = 0;
                Int32.TryParse(grdViewProduct.GetRowCellValue(i, "MAINGROUPID").ToString().Trim(), out intMainGroupID);
                DataRow[] rows = dtbMainGroupPermission.Select("MainGroupID = " + intMainGroupID + "and ISCANSTORECHANGEORDER = 1 and IsNew = 1");
                if (rows.Length < 1)
                {
                    if ((grdViewProduct.GetRowCellValue(i, "MAINGROUPNAME")) == null)
                    {
                        return string.Empty;
                    }
                    else grdViewProduct.GetRowCellValue(i, "MAINGROUPNAME").ToString().Trim();
                }

                //if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(intMainGroupID, Library.AppCore.Constant.EnumType.MainGroupPermissionType.STORECHANGEORDER, Library.AppCore.Constant.EnumType.IsNewPermissionType.ISNEW))
                //    return grdViewProduct.GetRowCellValue(i, "MAINGROUPNAME").ToString().Trim();
            }
            return string.Empty;
        }

        int intCurrentReviewLevelID = 0;
        private bool SetReview(int intReviewStatus, bool bolIsReviewAll, ref string strError, bool bolIsCertifyByFinger)
        {
            bool bolIsReview = false;
            string strNote = bolIsCertifyByFinger ? "Duyệt bằng vân tay" : "Duyệt bằng đăng nhập tài khoản";
            //Quyen duyet tren kho
            bool bolFromStorePermission = true;
            bool bolToStorePermission = true;
            if (strReviewUser != "administrator")
            {
                bool bolResult = objPLCStoreChangeOrder.CheckStorePermission(objStoreChangeOrder.FromStoreID, objStoreChangeOrder.ToStoreID, strReviewUser, ref bolFromStorePermission, ref bolToStorePermission);
                if (!bolResult)
                {
                    strError = SystemConfig.objSessionUser.ResultMessageApp.Message;
                    return false;
                }
            }
            //bool bolFromStorePermission = ERP.MasterData.PLC.MD.PLCStore.CheckPermission(objStoreChangeOrder.FromStoreID, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER);
            //bool bolToStorePermission = ERP.MasterData.PLC.MD.PLCStore.CheckPermission(objStoreChangeOrder.ToStoreID, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER);
            //quyet duyet tren nganh hang
            string strMainGroupPermission = CheckMainGroupPermission();
            if (grdViewReviewLevel.RowCount == 0)
            {
                // LÊ VĂN ĐÔNG: 07/12/2017 BỔ SUNG -> NẾU TỰ ĐỘNG DUYỆT THÌ BỎ QUA
                if (objStoreChangeOrderType.IsAutoReview && FormState == FormStateType.ADD)
                    return true;

                if (bolFromStorePermission)
                {
                    return true;
                }
                else
                {
                    strError = "Bạn không có quyền duyệt phiếu";
                    return false;
                }
            }
            for (int i = 0; i < grdViewReviewLevel.RowCount; i++)
            {
                int intCurenReview = 0;
                Int32.TryParse(grdViewReviewLevel.GetRowCellValue(i, "REVIEWSTATUS").ToString(), out intCurenReview);
                if (intCurenReview != 3)
                {
                    int intStorePermission = 0;
                    Int32.TryParse(grdViewReviewLevel.GetRowCellValue(i, grdViewReviewLevel.Columns["STOREPERMISSIONTYPE"]).ToString(), out intStorePermission);
                    int intMainGroupPermission = 0;
                    Int32.TryParse(grdViewReviewLevel.GetRowCellValue(i, grdViewReviewLevel.Columns["MAINGROUPPERMISSIONTYPE"]).ToString(), out intMainGroupPermission);
                    Int32.TryParse(grdViewReviewLevel.GetRowCellValue(i, grdViewReviewLevel.Columns["REVIEWLEVELID"]).ToString(), out intCurrentReviewLevelID);
                    bool bolReview = false;
                    string strReviewName = grdViewReviewLevel.GetRowCellValue(i, "REVIEWLEVELNAME").ToString().Trim();
                    switch (intStorePermission)
                    {
                        case 1://kho xuat
                            if (bolFromStorePermission)
                            {
                                bolReview = true;
                            }
                            else
                            {
                                bolReview = false;
                                if (bolIsReviewAll)
                                    strError = "Bạn không có quyền duyệt(" + strReviewName + ") trên kho xuất  ";
                                else
                                    strError = "Bạn không có quyền duyệt trên kho xuất  ";
                                return false;
                            }
                            break;
                        case 2: //kho nhap
                            if (bolToStorePermission)
                            {
                                bolReview = true;
                            }
                            else
                            {
                                bolReview = false;
                                if (bolIsReviewAll)
                                    strError = "Bạn không có quyền duyệt(" + strReviewName + ") trên kho nhập.";
                                else

                                    strError = "Bạn không có quyền duyệt trên kho nhập.";
                                return false;
                            }
                            break;
                        case 3: //ca 2
                            if (bolToStorePermission && bolFromStorePermission)
                            {
                                bolReview = true;
                            }
                            else
                            {
                                bolReview = false;
                                string strStoreType = string.Empty;
                                if (!bolToStorePermission)
                                    strStoreType = "nhập";
                                else
                                    strStoreType = "xuất";
                                if (bolIsReviewAll)
                                    strError = "Bạn không có quyền duyệt(" + strReviewName + ") trên kho " + strStoreType + ".";
                                else
                                    strError = "Bạn không có quyền duyệt trên kho " + strStoreType + ".";
                                return false;
                            }
                            break;
                        default://khong xet
                            bolReview = true;
                            break;
                    }

                    switch (intMainGroupPermission)
                    {
                        case 1:
                            if (string.IsNullOrEmpty(strMainGroupPermission))
                                bolReview = true;
                            else
                            {
                                bolReview = false;
                                if (bolIsReviewAll)
                                    strError = "Bạn không có quyền duyệt(" + strReviewName + ") trên ngành hàng " + strMainGroupPermission;
                                else
                                    strError = "Bạn không có quyền duyệt trên ngành hàng ";
                                return false;
                            }
                            break;
                        default://khong xet
                            bolReview = true;
                            break;
                    }
                    if (bolReview)
                    {
                        grdViewReviewLevel.SetRowCellValue(i, "REVIEWSTATUS", intReviewStatus);
                        grdViewReviewLevel.SetRowCellValue(i, "REVIEWEDUSER", strReviewUser);
                        grdViewReviewLevel.SetRowCellValue(i, "REVIEWEDDATE", Globals.GetServerDateTime());
                        if (intReviewStatus == 3)
                        {
                            grdViewReviewLevel.SetRowCellValue(i, "ISREVIEWED", 1);
                            if (!bolIsReviewAll)
                                grdViewReviewLevel.SetRowCellValue(i, "NOTE", strNote);
                        }
                        if (i == grdViewReviewLevel.RowCount - 1 && intReviewStatus == 3)
                        {
                            bolIsReview = true;
                        }
                    }
                    if (!bolIsReviewAll)
                        return bolIsReview;
                }
            }
            return bolIsReview;
        }
        private bool Review(int intReviewStatus, bool bolIsCertifyByFinger)
        {
            if (objStoreChangeOrder != null && objStoreChangeOrder.IsExpired)
            {
                MessageBoxObject.ShowWarningMessage(this, "Yêu cầu này đã hết hạn. Vui lòng kiểm tra lại ngày hết hạn.");
                return false;
            }
            if (!ValidateData()) return false;

            //kiem tra quyen tren nganh hang va kho xuat
            string strMessage = string.Empty;
            try
            {
                //int intReviewStatus = cboStatus.SelectedIndex;
                //nieu duyet het thi cap nhat lai IsReview = true
                bool bolIsReview = false;
                string strError = string.Empty;
                bolIsReview = SetReview(intReviewStatus, false, ref strError, bolIsCertifyByFinger);
                if (!string.IsNullOrEmpty(strError))
                {

                    MessageBoxObject.ShowWarningMessage(this, strError);
                    return false;
                }
                if (!BindObject())
                {

                    return false;
                }
                objStoreChangeOrder.IsReviewed = bolIsReview;

                if (objStoreChangeOrder.IsReviewed)
                {

                    objStoreChangeOrder.ReviewedDate = Globals.GetServerDateTime();
                    objStoreChangeOrder.ReviewedUser = strReviewUser;
                }
                GetListNotification_RVL_UserList(intCurrentReviewLevelID, objStoreChangeOrder.FromStoreID, 1);
                GetListNotification_RVL_UserList(intCurrentReviewLevelID, objStoreChangeOrder.ToStoreID, 2);
                GetListNotification_RVL_UserList(intCurrentReviewLevelID, objStoreChangeOrder.CreatedStoreID, 3);
                objStoreChangeOrder.TransportCompanyID = cboTransportCompany.ColumnID;
                objStoreChangeOrder.TransportServicesID = cboTransportServices.ColumnID;
                objStoreChangeOrder.UserNotifyData = dtbNotification_User;
                objPLCStoreChangeOrder.Update(objStoreChangeOrder);
                Library.AppCore.Forms.frmWaitDialog.Close();
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi duyệt yêu cầu chuyển kho");
                SystemErrorWS.Insert("Lỗi duyệt yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> ReviewData", DUIInventory_Globals.ModuleName);
                return false;
            }

            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            else
            {
                //đóng form hoặc load lại
                MessageBox.Show("Duyệt yêu cầu chuyển kho thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                EditData();
                tabControl.SelectedTab = tabReviewLevel;
                this.Focus();
            }
            return true;
        }
        #endregion

        #region Create Grid
        private void CreateColumnGridProduct(bool bolAllowEdit)
        {
            string[] fieldNames = new string[] { "STORECHANGEORDERDETAILID", "STORECHANGEORDERID", "MAINGROUPID", "MAINGROUPNAME", "PRODUCTID", "PRODUCTNAME", "IMEI", "QUANTITY", "STORECHANGEQUANTITY", "INPUTDATE", "NOTE", "ISREQUESTIMEI", "PRODUCTSTATUS" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdProduct, true, bolAllowEdit, true, true, fieldNames);
        }
        private bool FormatGridProduct()
        {
            try
            {
                grdViewProduct.Columns["STORECHANGEQUANTITY"].OptionsColumn.AllowEdit = false;
                grdViewProduct.Columns["PRODUCTID"].OptionsColumn.AllowEdit = false;
                grdViewProduct.Columns["PRODUCTNAME"].OptionsColumn.AllowEdit = false;
                grdViewProduct.Columns["NOTE"].OptionsColumn.AllowEdit = true;
                grdViewProduct.Columns["IMEI"].OptionsColumn.AllowEdit = true;

                grdViewProduct.Columns["MAINGROUPID"].Visible = false;
                grdViewProduct.Columns["MAINGROUPNAME"].Visible = false;
                grdViewProduct.Columns["ISREQUESTIMEI"].Visible = false;
                grdViewProduct.Columns["STORECHANGEORDERDETAILID"].Visible = false;
                grdViewProduct.Columns["STORECHANGEORDERID"].Visible = false;
                grdViewProduct.Columns["PRODUCTID"].Visible = true;
                grdViewProduct.Columns["PRODUCTNAME"].Visible = true;
                grdViewProduct.Columns["IMEI"].Visible = true;
                grdViewProduct.Columns["QUANTITY"].Visible = true;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Visible = true;
                grdViewProduct.Columns["INPUTDATE"].Visible = true;
                grdViewProduct.Columns["NOTE"].Visible = true;
                grdViewProduct.Columns["PRODUCTSTATUS"].Visible = false;

                grdViewProduct.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grdViewProduct.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grdViewProduct.Columns["IMEI"].Caption = "IMEI";
                grdViewProduct.Columns["QUANTITY"].Caption = "Số lượng yêu cầu";
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Caption = "Số lượng chuyển kho";
                grdViewProduct.Columns["INPUTDATE"].Caption = "Ngày nhập";
                grdViewProduct.Columns["NOTE"].Caption = "Ghi chú";
                grdViewProduct.OptionsView.ColumnAutoWidth = false;
                //Sumary
                grdViewProduct.Columns["QUANTITY"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:#,##0.##}");
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "STORECHANGEQUANTITY", "{0:#,##0.##}");

                grdViewProduct.Columns["STORECHANGEORDERDETAILID"].Width = 50;
                grdViewProduct.Columns["STORECHANGEORDERID"].Width = 50;
                grdViewProduct.Columns["PRODUCTID"].Width = 120;
                grdViewProduct.Columns["PRODUCTNAME"].Width = 200;
                grdViewProduct.Columns["IMEI"].Width = 150;
                grdViewProduct.Columns["QUANTITY"].Width = 90;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Width = 100;
                grdViewProduct.Columns["INPUTDATE"].Width = 100;
                grdViewProduct.Columns["NOTE"].Width = 250;
                grdViewProduct.Columns["QUANTITY"].DisplayFormat.FormatString = "#,##0.##";
                grdViewProduct.Columns["QUANTITY"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].DisplayFormat.FormatString = "#,##0.##";
                grdViewProduct.Columns["STORECHANGEQUANTITY"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;

                DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repSpinQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
                repSpinQuantity.MaxLength = 10;
                repSpinQuantity.IsFloatValue = true;
                repSpinQuantity.Mask.EditMask = "#,###,###,###,##0.##";
                repSpinQuantity.MaxLength = 50;
                repSpinQuantity.NullText = "0";
                //repSpinQuantity.MaxValue = new decimal(new int[] { -727379969, 232, 0, 0 });
                repSpinQuantity.Buttons.Clear();
                grdViewProduct.Columns["QUANTITY"].ColumnEdit = repSpinQuantity;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].ColumnEdit = repSpinQuantity;

                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtNote.MaxLength = 2000;
                grdViewProduct.Columns["NOTE"].ColumnEdit = repTxtNote;

                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtImei.CharacterCasing = CharacterCasing.Upper;
                grdViewProduct.Columns["IMEI"].ColumnEdit = repTxtImei;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Visible = !(FormState == FormStateType.ADD);
                //DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repTxtImei = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
                //grdViewProduct.Columns["IMEI"].ColumnEdit = repTxtImei;

                grdViewProduct.Appearance.FooterPanel.ForeColor = Color.Red;
                grdViewProduct.ColumnPanelRowHeight = 40;
                grdViewProduct.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewProduct.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewProduct.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewProduct.OptionsView.ShowAutoFilterRow = false;
                grdViewProduct.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewProduct.OptionsView.ShowFooter = true;
                grdViewProduct.OptionsView.ShowGroupPanel = false;
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewProduct);
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách chi tiết yêu cầu chuyển kho");
                SystemErrorWS.Insert("Định dạng lưới danh sách chi tiết yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> FormatGridProduct", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void CreateColumnGridReview(bool bolAllowEdit)
        {
            string[] fieldNames = new string[] { "REVIEWLEVELID", "STORECHANGEORDERTYPEID", "REVIEWORDER", "REVIEWLEVELNAME", "REVIEWEDUSER", "REVIEWEDUSFULLNAME",
                "REVIEWSTATUS","REVIEWSTATUSNAME" ,"ISREVIEWED", "REVIEWEDDATE", "NOTE", "STOREPERMISSIONTYPE", "MAINGROUPPERMISSIONTYPE" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdReviewLevel, true, bolAllowEdit, true, true, fieldNames);
        }
        private bool FormatGridReview()
        {
            try
            {
                grdViewReviewLevel.Columns["STOREPERMISSIONTYPE"].Visible = false;
                grdViewReviewLevel.Columns["MAINGROUPPERMISSIONTYPE"].Visible = false;
                grdViewReviewLevel.Columns["REVIEWLEVELID"].Visible = false;
                grdViewReviewLevel.Columns["STORECHANGEORDERTYPEID"].Visible = false;
                grdViewReviewLevel.Columns["REVIEWORDER"].Visible = true;
                grdViewReviewLevel.Columns["REVIEWLEVELNAME"].Visible = true;
                grdViewReviewLevel.Columns["REVIEWEDUSER"].Visible = false;
                grdViewReviewLevel.Columns["REVIEWSTATUS"].Visible = false;
                grdViewReviewLevel.Columns["REVIEWSTATUSNAME"].Visible = true;
                grdViewReviewLevel.Columns["ISREVIEWED"].Visible = true;
                grdViewReviewLevel.Columns["REVIEWEDDATE"].Visible = true;
                grdViewReviewLevel.Columns["NOTE"].Visible = true;
                grdViewReviewLevel.Columns["STOREPERMISSIONTYPE"].Visible = false;
                grdViewReviewLevel.Columns["MAINGROUPPERMISSIONTYPE"].Visible = false;
                grdViewReviewLevel.Columns["REVIEWEDUSFULLNAME"].Visible = true;

                grdViewReviewLevel.Columns["REVIEWORDER"].Caption = "Thứ tự";
                grdViewReviewLevel.Columns["REVIEWLEVELNAME"].Caption = "Tên mức duyệt";
                grdViewReviewLevel.Columns["REVIEWEDUSFULLNAME"].Caption = "Người duyệt";
                grdViewReviewLevel.Columns["REVIEWSTATUSNAME"].Caption = "Trạng thái duyệt";
                grdViewReviewLevel.Columns["ISREVIEWED"].Caption = "Đã duyệt";
                grdViewReviewLevel.Columns["REVIEWEDDATE"].Caption = "Ngày duyệt";
                grdViewReviewLevel.Columns["NOTE"].Caption = "Ghi chú";

                grdViewReviewLevel.OptionsView.ColumnAutoWidth = false;

                grdViewReviewLevel.Columns["REVIEWLEVELID"].Width = 50;
                grdViewReviewLevel.Columns["STORECHANGEORDERTYPEID"].Width = 50;
                grdViewReviewLevel.Columns["REVIEWORDER"].Width = 50;
                grdViewReviewLevel.Columns["REVIEWLEVELNAME"].Width = 120;
                grdViewReviewLevel.Columns["REVIEWEDUSFULLNAME"].Width = 180;
                grdViewReviewLevel.Columns["REVIEWSTATUSNAME"].Width = 120;
                grdViewReviewLevel.Columns["ISREVIEWED"].Width = 80;
                grdViewReviewLevel.Columns["REVIEWEDDATE"].Width = 120;
                grdViewReviewLevel.Columns["NOTE"].Width = 220;
                grdViewReviewLevel.Columns["STOREPERMISSIONTYPE"].Width = 50;
                grdViewReviewLevel.Columns["MAINGROUPPERMISSIONTYPE"].Width = 50;
                for (int i = 0; i < grdViewReviewLevel.Columns.Count; i++)
                {
                    grdViewReviewLevel.Columns[i].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                    grdViewReviewLevel.Columns[i].OptionsColumn.AllowMove = false;
                }

                //grdViewReviewLevel.Columns["REVIEWEDDATE"].DisplayFormat = ;

                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtReviewNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtReviewNote.MaxLength = 2000;
                repTxtReviewNote.ReadOnly = true;
                grdViewReviewLevel.Columns["NOTE"].ColumnEdit = repTxtReviewNote;
                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repCkbIsReviewed = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                repCkbIsReviewed.ValueChecked = ((short)1);
                repCkbIsReviewed.ValueUnchecked = ((short)0);
                grdViewReviewLevel.Columns["ISREVIEWED"].ColumnEdit = repCkbIsReviewed;

                grdViewReviewLevel.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewReviewLevel.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewReviewLevel.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewReviewLevel.OptionsView.ShowAutoFilterRow = false;
                grdViewReviewLevel.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewReviewLevel.OptionsView.ShowFooter = false;
                grdViewReviewLevel.OptionsView.ShowGroupPanel = false;
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewReviewLevel);
                grdViewReviewLevel.Columns["REVIEWEDDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                grdViewReviewLevel.Columns["REVIEWEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách chi tiết yêu cầu chuyển kho");
                SystemErrorWS.Insert("Định dạng lưới danh sách chi tiết yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> FormatGridProduct", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        #endregion Create Grid

        #region Form Status
        /// <summary>
        /// Các trạng thái của Form
        /// </summary>
        enum FormStateType
        {
            LIST,
            ADD,
            EDIT
        }

        FormStateType intFormState = FormStateType.LIST;
        FormStateType FormState
        {
            set
            {

                //Kiểm tra quyền:
                intFormState = value;
                if (intFormState == FormStateType.ADD)
                {
                    CreateColumnGridProduct(true);
                    FormatGridProduct();

                }
                else if (intFormState == FormStateType.EDIT)
                {

                    CreateColumnGridProduct(!IsReviewed);
                    FormatGridProduct();
                    cboFromStoreID.Enabled = false;
                    cboToStoreID.Enabled = false;
                    //cboTransportTypeID.Enabled = false;
                    cboStoreChangeStatus.Enabled = false;
                    cboInStockStatusID.Enabled = false;
                    //radioIsNew.Enabled = false;
                    //radioIsOld.Enabled = false;
                    lblContentDeleted.Enabled = true;
                    lblStoreChangeStatus.Enabled = true;
                    txtContentDeleted.Enabled = true;
                    txtContentDeleted.ReadOnly = true;
                    //cboStatus.SelectedIndex = 0;
                    //ckbIsDeleted.Enabled = true;
                    //ckbIsReview.Enabled = true;
                    txtSearchProduct.ReadOnly = true;
                    txtStoreChangeOrderID.BackColor = System.Drawing.SystemColors.Info;
                    btnSearchProduct.Enabled = false;
                    rbtnIMEI.Enabled = false;
                    rbtnQuantity.Enabled = false;

                }
            }
            get
            {
                return intFormState;
            }
        }

        private void CreateReviewItem()
        {
            DevExpress.XtraBars.BarButtonItem barItemReview0 = new DevExpress.XtraBars.BarButtonItem();
            DevExpress.XtraBars.BarButtonItem barItemReview1 = new DevExpress.XtraBars.BarButtonItem();
            DevExpress.XtraBars.BarButtonItem barItemReview2 = new DevExpress.XtraBars.BarButtonItem();
            DevExpress.XtraBars.BarButtonItem barItemReview3 = new DevExpress.XtraBars.BarButtonItem();

            barItemReview0.Caption = "Chưa duyệt";
            barItemReview0.Name = "0";
            barItemReview0.Tag = "0";
            barItemReview0.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
            pMnuReview.AddItem(barItemReview0);

            barItemReview1.Caption = "Đang xử lý";
            barItemReview1.Name = "1";
            barItemReview1.Tag = "1";
            barItemReview1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
            pMnuReview.AddItem(barItemReview1);

            barItemReview2.Caption = "Từ chối";
            barItemReview2.Name = "2";
            barItemReview2.Tag = "2";
            barItemReview2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
            pMnuReview.AddItem(barItemReview2);

            barItemReview3.Caption = "Đồng ý";
            barItemReview3.Name = "3";
            barItemReview3.Tag = "3";
            barItemReview3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
            pMnuReview.AddItem(barItemReview3);

        }

        private void barItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!ValidateStore())
                return;
            int intReviewStatus = Convert.ToInt32(e.Item.Tag);
            Review(intReviewStatus, false);
        }
        #endregion
        private void cboInStockStatusID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (intInStockStatusID == Convert.ToInt32(cboInStockStatusID.SelectedValue)) return;
            if (grdViewProduct.RowCount > 0)
            {
                if (MessageBox.Show(this, "Thay đổi trạng thái thì danh sách sản phẩm sẽ bị mất! Bạn chắc chắn muốn thay đổi không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    cboInStockStatusID.SelectedValue = intInStockStatusID;
                    return;
                }
                else
                {
                    (grdProduct.DataSource as DataTable).Clear();
                    grdProduct.RefreshDataSource();
                }
            }
            intInStockStatusID = Convert.ToInt32(cboInStockStatusID.SelectedValue);
        }

        #region Duyet SCO
        public bool IsSusscessReview()
        {
            if (objStoreChangeOrderType.IsReviewByTransType)
            {

                DataTable dtbTransportTypeCurrent = (DataTable)cboTransportTypeID.DataSource;
                if (cboTransportTypeID.ColumnID != null)
                {
                    int intTransportTypeIDcurent = -1;
                    int.TryParse((cboTransportTypeID.ColumnID).ToString().Trim(), out intTransportTypeIDcurent);
                    string strReviewByTranSportID = "";
                    DataRow[] drs = dtbTransportTypeCurrent.Select("TRANSPORTTYPEID = " + intTransportTypeIDcurent.ToString());
                    if (drs != null && drs.Length > 0)
                        strReviewByTranSportID = (drs[0]["REVIEWFUNCTIONID"] ?? "").ToString().Trim();
                    if (string.IsNullOrEmpty(strReviewByTranSportID))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Chưa có quyền duyệt!");
                        return false;
                    }
                }
             
            }
            //else
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Chưa có quyền duyệt!");
            //    return false;
            //}
            // HUE check Duyệt khi  sp trong yêu cầu xuất khác
            if (objPLCStoreChangeOrder.ReturnInstock(objStoreChangeOrder) == true)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số lượng tồn không đủ");
                return false;
            }
            if (!btnReview.Enabled)
                return false;
            //Check Yes/No trước khi tiếp tục
            DialogResult dialogResult = MessageBox.Show(this, "Bạn có muốn duyệt yêu cầu này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult != DialogResult.Yes)
                return false;
            if (!ValidateStore())
                return false;
            if (objStoreChangeOrder != null && objStoreChangeOrder.IsExpired)
            {
                MessageBoxObject.ShowWarningMessage(this, "Yêu cầu này đã hết hạn. Vui lòng kiểm tra lại ngày hết hạn.");
                return false;
            }
            if (!ValidateData())
                return false;
            List<string> lstPermission = new List<string>();
            lstPermission.Add(objStoreChangeOrderType.ReviewFunctionID);
            strReviewUser = SystemConfig.objSessionUser.UserName.ToString().Trim();
            return Review(3, false);
        }
        #region Cong Cu Dung Cu
        private bool IsUploadAttachmentSuccess(string strAttachmentId = null)
        {
            if (!String.IsNullOrEmpty(this.FileName) && !String.IsNullOrEmpty(this.SafeFileName) && !String.IsNullOrEmpty(this.Matotrinh))
            {
                //Upload file
                string strFilePath = string.Empty;
                string strFileID = string.Empty;
                string strResult = ERP.FMS.DUI.DUIFileManager.UploadFile(this, strFMSAplication, this.FileName, ref strFileID, ref strFilePath);

                if (!String.IsNullOrEmpty(strResult)) //if file uploaded is not successfully
                {
                    MessageBoxObject.ShowWarningMessage(this, "Đính kèm file Voffice thất bại! Vui lòng kiểm tra lại!");
                    return false;
                }
                else
                {
                    objStoreChangeOrderAttachment = new StoreChangeOrderAttachment();
                    objStoreChangeOrderAttachment.StoreChangeOrderID = this.StoreChangeOrderID;
                    objStoreChangeOrderAttachment.FileID = strFileID;
                    objStoreChangeOrderAttachment.AttachMENTName = this.SafeFileName;
                    objStoreChangeOrderAttachment.AttachMENTPath = strFilePath;
                    objStoreChangeOrderAttachment.CreatedUser = SystemConfig.objSessionUser.UserName.ToString().Trim();
                    objStoreChangeOrderAttachment.VOFFICEID = this.Matotrinh;
                    objStoreChangeOrderAttachment.Description = "Description";
                    bool result = false;
                    string action = string.Empty;
                    if (string.IsNullOrEmpty(strAttachmentId)) // insert
                    {
                        result = objPLCStoreChangeOrder.InsertStoreChangeOrderAttachment(objStoreChangeOrderAttachment);
                        if (result) MessageBoxObject.ShowInfoMessage(this, "Insert attachment successful");
                        else
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Insert file đính kèm Voffice thất bại! Vui lòng kiểm tra lại!");
                        }
                    }
                    else
                    { // update
                        objStoreChangeOrderAttachment.AttachMENTID = strAttachmentId;
                        result = objPLCStoreChangeOrder.UpdateStoreChangeOrderAttachment(objStoreChangeOrderAttachment);
                        if (result) MessageBoxObject.ShowInfoMessage(this, "Update attachment successful");
                        else
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Update file đính kèm Voffice thất bại! Vui lòng kiểm tra lại!");
                        }
                    }
                    return true;
                }
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn chưa nhập đủ thông tin cần thiết, hãy nhập lại");
                return false;
            }
        }

        private bool UpLoadFile(string strAttachmentId = null)
        {
            DialogResult dlgResult = this.openFileDialog.ShowDialog();
            if (dlgResult == DialogResult.OK)
            {
                this.FileName = this.openFileDialog.FileName.Trim();
                this.FileName = this.FileName;
                this.SafeFileName = this.openFileDialog.SafeFileName.Trim();
            }
   
            if (!String.IsNullOrEmpty(this.FileName) && !String.IsNullOrEmpty(this.SafeFileName))// && !String.IsNullOrEmpty(this.Matotrinh))
            {
                //Upload file
                string strFilePath = string.Empty;
                string strFileID = string.Empty;
                string strResult = ERP.FMS.DUI.DUIFileManager.UploadFile(this, strFMSAplication, this.FileName, ref strFileID, ref strFilePath);

                if (!String.IsNullOrEmpty(strResult)) //if file uploaded is not successfully
                {
                    MessageBoxObject.ShowWarningMessage(this, "Đính kèm file Voffice thất bại! Vui lòng kiểm tra lại!");
                    return false;
                }
                else
                {
                    
                    objStoreChangeOrderAttachment = new StoreChangeOrderAttachment();
                    objStoreChangeOrderAttachment.StoreChangeOrderID = this.txtStoreChangeOrderID.Text.Trim();
                    objStoreChangeOrderAttachment.FileID = strFileID;
                    objStoreChangeOrderAttachment.AttachMENTName = this.SafeFileName;
                    objStoreChangeOrderAttachment.AttachMENTPath = strFilePath;
                    objStoreChangeOrderAttachment.CreatedUser = SystemConfig.objSessionUser.UserName.ToString().Trim();
                    objStoreChangeOrderAttachment.VOFFICEID = this.Matotrinh;
                    objStoreChangeOrderAttachment.Description = "Description";
                    bool result = false;
                    string action = string.Empty;
                    if (string.IsNullOrEmpty(strAttachmentId)) // insert
                    {
                        result = objPLCStoreChangeOrder.InsertStoreChangeOrderAttachment(objStoreChangeOrderAttachment);
                        if (result)
                        {
                            //dtAtm.Columns.Add(new DataColumn("VOFFICEID", typeof(string)));
                            //dtAtm.Columns.Add(new DataColumn("ATTACHMENTNAME", typeof(string)));
                            //dtAtm.Columns.Add(new DataColumn("DESCRIPTION", typeof(string)));
                            //dtAtm.Columns.Add(new DataColumn("CREATEDDATE", typeof(string)));
                            //dtAtm.Columns.Add(new DataColumn("CREATEDUSER", typeof(string)));
                            //dtAtm.Columns.Add(new DataColumn("ATTACHMENTID", typeof(string)));
                            //dtAtm.Columns.Add(new DataColumn("ATTACHMENTPATH", typeof(string)));
                            MessageBoxObject.ShowInfoMessage(this, "Insert attachment successful");
                            dtAtm.Rows.Add("", objStoreChangeOrderAttachment.AttachMENTName, "Description", DateTime.Now.ToShortDateString(), SystemConfig.objSessionUser.UserName,
                               strFileID, objStoreChangeOrderAttachment.AttachMENTPath, strFileID);
                            grdAttachment.DataSource = dtAtm;

                        }
                        else
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Insert file đính kèm Voffice thất bại! Vui lòng kiểm tra lại!");
                        }
                    }
                    else
                    { // update
                        objStoreChangeOrderAttachment.AttachMENTID = strAttachmentId;
                        result = objPLCStoreChangeOrder.UpdateStoreChangeOrderAttachment(objStoreChangeOrderAttachment);
                        if (result)
                        { MessageBoxObject.ShowInfoMessage(this, "Update attachment successful");
                            dtAtm.Rows.Add("", objStoreChangeOrderAttachment.AttachMENTName, "Description", DateTime.Now.ToShortDateString(), SystemConfig.objSessionUser.UserName,
                              strFileID, objStoreChangeOrderAttachment.AttachMENTPath, strFileID);
                            grdAttachment.DataSource = dtAtm;
                        }
                        else
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Update file đính kèm Voffice thất bại! Vui lòng kiểm tra lại!");
                        }
                    }
                    return true;
                }
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn chưa nhập đủ thông tin cần thiết, hãy nhập lại");
                return false;
            }
        }

        public bool IsSuccessReviewCCDC()
        {
            frmStoreChangeOrderConfigVOffice frmStoreChageOrderCfgVOffice = new frmStoreChangeOrderConfigVOffice(this);
            if (objStoreChangeOrder.DtbAttachment.Rows.Count > 0)
            {
                DataRow row = objStoreChangeOrder.DtbAttachment.Rows[0];
                if (!string.IsNullOrEmpty(row["ATTACHMENTNAME"].ToString()))
                {
                    if (MessageBox.Show("Đã có file đính kèm\n" + row["ATTACHMENTNAME"].ToString() + "\nBạn có muốn thay thế file khác không?", "Message", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        string strAttachmentId = row["ATTACHMENTID"].ToString();
                        frmStoreChageOrderCfgVOffice.ShowDialog();
                        if (IsUploadAttachmentSuccess(strAttachmentId))
                        {
                            EditData();
                            tabControl.SelectedTab = tabAttachment;
                            return true;
                        }
                    }
                    return true;
                }
            }
            else
            {
                frmStoreChageOrderCfgVOffice.ShowDialog();
                if (IsUploadAttachmentSuccess())
                {
                    EditData();
                    tabControl.SelectedTab = tabAttachment;
                    return true;
                }
            }
            return false;
        }

        #endregion

        private void btnReview_Click(object sender, EventArgs e)
        {
            bool IsCCDC = this.IsCanChangeProduct(6);
            if (IsSusscessReview())
            {
                this.bolIsHasAction = true;
                if (IsCCDC)
                {
                //    System.Diagnostics.Process.Start("http://voffice.viettel.vn/");
                btnVOffice.Visible = true;
                btnVOffice.Enabled = true;
                btnStoreChangeAll.Visible = false;
                    this.IsReviewed = true;
                }
            }
        }


        #endregion
        private void mnuItemExportTemplate_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.GridControl grdTemp = new DevExpress.XtraGrid.GridControl();
            DevExpress.XtraGrid.Views.Grid.GridView grvTemp = new DevExpress.XtraGrid.Views.Grid.GridView();
            grdTemp.MainView = grvTemp;
            string[] fieldNames = new string[] { "Mã sản phẩm", "IMEI", "Số lượng" };
            if (rbtnQuantity.Checked)
            {
                fieldNames = new string[] { "Mã sản phẩm", "Số lượng" };
            }
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdTemp, fieldNames);
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdTemp);
        }

        private void mnuImportExcel_Click(object sender, EventArgs e)
        {
            if (FormState != FormStateType.ADD)
            {
                return;
            }
            try
            {
                if (!ValidateStore()) return;
                DataTable dtbExcelData = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(3);
                if (dtbExcelData == null || dtbExcelData.Columns.Count == 0)
                {
                    return;
                }
                if (dtbExcelData.Rows.Count < 2)
                {
                    MessageBox.Show("Tập tin excel không có dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (rbtnQuantity.Checked)
                {
                    if (dtbExcelData.Columns.Count < 2)
                    {
                        MessageBox.Show("Tập tin excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else if (dtbExcelData.Columns.Count < 3 || dtbExcelData.Rows[0][1].ToString().ToUpper().Trim() != "IMEI")
                {
                    MessageBox.Show("Tập tin excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (rbtnQuantity.Checked)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        if (string.IsNullOrEmpty(dtbExcelData.Rows[0][i].ToString()))
                        {
                            MessageBox.Show("Tập tin excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        dtbExcelData.Columns[i].Caption = dtbExcelData.Rows[0][i].ToString();
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (string.IsNullOrEmpty(dtbExcelData.Rows[0][i].ToString()))
                        {
                            MessageBox.Show("Tập tin excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        dtbExcelData.Columns[i].Caption = dtbExcelData.Rows[0][i].ToString();
                    }
                }

                dtbExcelData.Rows.RemoveAt(0);
                dtbExcelData.AcceptChanges();
                // Check Validate


                foreach (DataRow rBarcode in dtbExcelData.Rows)
                {
                    KeyPressEventArgs eventBarcode = new KeyPressEventArgs((char)Keys.Enter);
                    bolIsImport = true;
                    if (rbtnQuantity.Checked)
                    {
                        if (rBarcode[0] != DBNull.Value && !string.IsNullOrEmpty(rBarcode[0].ToString()))
                        {
                            int intQuantity = 0;
                            try
                            {
                                intQuantity = Convert.ToInt32(rBarcode[1].ToString());
                            }
                            catch
                            {
                                intQuantity = 0;
                            }
                            if (intQuantity > 0)
                            {
                                //txtSearchProduct.Text = rBarcode[1].ToString().Trim();
                                txtSearchProduct.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(rBarcode[0].ToString().Trim());
                                AddProduct(txtSearchProduct.Text.Trim(), intQuantity);
                                txtSearchProduct.Text = string.Empty;
                                txtSearchProduct.Focus();
                                bolIsImport = false;
                            }
                        }
                    }
                    else
                    {
                        if (rBarcode[1] != DBNull.Value && !string.IsNullOrEmpty(rBarcode[1].ToString()))
                        {
                            txtSearchProduct.Text = rBarcode[1].ToString().Trim();
                            txtSearchProduct.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(rBarcode[1].ToString().Trim());
                            AddProduct(txtSearchProduct.Text.Trim(), 1);
                            txtSearchProduct.Text = string.Empty;
                            txtSearchProduct.Focus();
                        }
                        else
                        {
                            if (rBarcode[0] != DBNull.Value && !string.IsNullOrEmpty(rBarcode[0].ToString()))
                            {
                                int intQuantity = 0;
                                try
                                {
                                    intQuantity = Convert.ToInt32(rBarcode[2].ToString());
                                }
                                catch
                                {
                                    intQuantity = 0;
                                }
                                if (intQuantity > 0)
                                {
                                    txtSearchProduct.Text = rBarcode[1].ToString().Trim();
                                    txtSearchProduct.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(rBarcode[0].ToString().Trim());
                                    AddProduct(txtSearchProduct.Text.Trim(), intQuantity);
                                    txtSearchProduct.Text = string.Empty;
                                    txtSearchProduct.Focus();
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            finally
            {
                txtSearchProduct.Text = string.Empty;
            }
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            mnuItemDelete.Enabled = grdViewProduct.FocusedRowHandle >= 0 && (!ckbIsReview.Checked || intFormState == FormStateType.ADD);
            if (cboFromStoreID.StoreID > 0 && cboToStoreID.StoreID > 0 && !ckbIsReview.Checked)
                mnuItemImportIMEI.Enabled = bolInputIMEI && !txtSearchProduct.ReadOnly;
            else
                mnuItemImportIMEI.Enabled = false;
            mnuItemExport.Enabled = grdViewProduct.RowCount > 0;
        }

        private void grdViewProduct_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            string strColName = grdViewProduct.FocusedColumn.FieldName.ToUpper();
            if (grdViewProduct.FocusedRowHandle < 0)
                return;
            if (strColName == "QUANTITY")
            {
                DataRow row = (DataRow)grdViewProduct.GetDataRow(grdViewProduct.FocusedRowHandle);
                string strProducId = row["PRODUCTID"].ToString().Trim();
                if (row["QUANTITY"] == DBNull.Value || Convert.ToDecimal(row["QUANTITY"]) < 0)
                    row["QUANTITY"] = 0;
                if (!Convert.ToBoolean(row["IsAllowDecimal"]))
                {
                    row["Quantity"] = Math.Round(Convert.ToDecimal(row["QUANTITY"]), 0, MidpointRounding.AwayFromZero);
                }
                else
                {
                    row["Quantity"] = Math.Round(Convert.ToDecimal(row["QUANTITY"]), 0, MidpointRounding.AwayFromZero);
                }

            }
            if (strColName == "IMEI")
            {
                DataRow row = (DataRow)grdViewProduct.GetDataRow(grdViewProduct.FocusedRowHandle);
                bool bIsRequestImei = Convert.ToBoolean(row["ISREQUESTIMEI"]);
                int intQuantity = Convert.ToInt32(row["QUANTITY"]);
                string strIMEI = row["IMEI"].ToString().Trim();
                string strProducId = row["PRODUCTID"].ToString().Trim();

                if (string.IsNullOrEmpty(strIMEI))
                {
                    PLC.WSProductInStock.ProductInStock objProductInStockTemp = new PLC.PLCProductInStock().LoadInfo(strProducId, cboFromStoreID.StoreID, 0, string.Empty, false, Convert.ToInt32(cboInStockStatusID.SelectedValue));
                    row["QUANTITYINSTOCK"] = objProductInStockTemp.Quantity;
                }
                else
                {
                    row["QUANTITYINSTOCK"] = 1;
                }
            }
        }

        private void mnuItemImportIMEI_Click(object sender, EventArgs e)
        {
            DataTable dtbImeiPara = new DataTable();
            dtbImeiPara.Columns.Add("IMEI");
            frmPopUpIMEI frm = new frmPopUpIMEI();
            DataTable dtbTemp = (DataTable)grdProduct.DataSource;
            if (dtbTemp.Rows.Count > 0 && dtbTemp != null)
                frm.DataTableFromOut = dtbTemp;
            frm.StoreID = cboFromStoreID.StoreID;
            frm.InStockStatusID = Convert.ToInt32(cboInStockStatusID.SelectedValue);
            frm.ProductConsignmentTypeBySCOTID = dtbProductConsignmentTypeBySCOTID;
            frm.ShowDialog();
            try
            {
                if (frm.dtbResult != null && frm.dtbResult.Rows.Count > 0)
                {
                    DataTable dtb = frm.dtbResult.AsEnumerable().CopyToDataTable();
                    DataTable dtbGrid = (DataTable)grdProduct.DataSource;

                    DataTable dtbProduct = dtb.DefaultView.ToTable(true, "PRODUCTID");
                    if (dtbProduct != null && dtbProduct.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbProduct.Rows.Count; i++)
                        {
                            MasterData.PLC.MD.WSProduct.Product objProduct = MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(dtbProduct.Rows[0]["PRODUCTID"].ToString().Trim());
                            if (objProduct != null)
                            {
                                string strProductID = objProduct.ProductID.Trim();
                                string strProductName = objProduct.ProductName.Trim();
                                int intMainGroupID = objProduct.MainGroupID;
                                bool bolIsAllowDecimal = objProduct.IsAllowDecimal;

                                foreach (var itemToAdd in dtb.AsEnumerable())
                                {
                                    if (itemToAdd != null)
                                    {
                                        grdProduct.DataSource = CreateTableProduct(dtbGrid, null, true, intMainGroupID, strProductID
                                            , strProductName, itemToAdd["IMEI"].ToString().Trim(), 1, 0,
                                            "", 1, bolIsAllowDecimal, null);
                                        if (!string.IsNullOrEmpty(itemToAdd["IMEI"].ToString().Trim()))
                                            dtbImeiPara.Rows.Add(itemToAdd["IMEI"].ToString().Trim());
                                    }
                                }
                            }
                        }
                        string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbImeiPara, "IMEI");
                        new PLC.PLCProductInStock().Update_IsOrdering(xmlIMEI, 1, cboFromStoreID.StoreID);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void rbtnIMEI_CheckedChanged(object sender, EventArgs e)
        {
            if (!bolIsLoadForm)
                return;

            if (bolInputIMEI == rbtnIMEI.Checked) return;
            if (grdViewProduct.RowCount > 0)
            {
                if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
                {
                    rbtnIMEI.Checked = bolInputIMEI;
                    rbtnQuantity.Checked = !bolInputIMEI;
                    return;
                }
                else
                {
                    mnuItemDelete.Enabled = false;
                    mnuItemExport.Enabled = false;
                    (grdProduct.DataSource as DataTable).Clear();
                    grdProduct.RefreshDataSource();
                }
            }
            bolInputIMEI = rbtnIMEI.Checked;
            mnuItemImportIMEI.Enabled = bolInputIMEI;

            grdViewProduct.Columns["IMEI"].Visible = bolInputIMEI;
            grdViewProduct.Columns["IMEI"].OptionsColumn.AllowEdit = bolInputIMEI;
        }

        private void btnStoreChangeAll_Click(object sender, EventArgs e)
        {
            StoreChangeAction(btnStoreChangeAll.Tag.ToString().Trim());
        }

        #region Check Import san pham
        private bool ValidateImportExcel(string strProduct, int intQuantity, out string strError)
        {
            strError = string.Empty;
            if (string.IsNullOrWhiteSpace(strProduct))
                return false;
            int intFromStoreID = cboFromStoreID.StoreID;
            string strImeiList = string.Empty;
            ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = null;
            if (bolInputIMEI)
            {
                strImeiList = GetIMEI(strProduct);
                objProductInStock = new ERP.Inventory.PLC.PLCProductInStock().LoadInfo(strProduct, intFromStoreID, (int)(objStoreChangeType == null ? 0 : objStoreChangeType.OutputTypeID), strImeiList, true, Convert.ToInt32(cboInStockStatusID.SelectedValue), true);
            }
            else
                objProductInStock = new PLC.PLCProductInStock().LoadInfo(strProduct, cboFromStoreID.StoreID, 0, string.Empty, false, Convert.ToInt32(cboInStockStatusID.SelectedValue), true);
            if (objProductInStock == null)
            {
                strError = "Sản phẩm không tồn tại trong hệ thống hoặc số IMEI không có trong kho.";
                return false;
            }
            if (objProductInStock != null && strProduct.Trim().Equals(objProductInStock.IMEI.Trim()) && !bolInputIMEI)
            {
                strError = "Không được phép import IMEI ở chế độ \"Nhập sản phẩm\".";
                return false;
            }

            // Neu san pham ban vao = ma san pham
            if (strProduct.Trim().Equals(objProductInStock.ProductID.Trim()))
            {
                if (Convert.ToInt32(cboInStockStatusID.SelectedValue) != 1
                    //&& !string.IsNullOrEmpty(objProductInStock.IMEI.Trim())
                    //&& objProductInStock.InStockStatusID != Convert.ToInt32(cboInStockStatusID.SelectedValue)
                    )
                {
                    objProductInStock = new PLC.PLCProductInStock().LoadInfo(strProduct, cboFromStoreID.StoreID, 0, string.Empty, false, Convert.ToInt32(cboInStockStatusID.SelectedValue), true);
                    if (objProductInStock == null)
                    {
                        strError = "Sản phẩm không tồn tại trong hệ thống hoặc số IMEI không có trong kho.";
                        return false;
                    }
                    if (objProductInStock != null && strProduct.Trim().Equals(objProductInStock.IMEI.Trim()) && !bolInputIMEI)
                    {
                        strError = "Không được phép import IMEI ở chế độ \"Nhập sản phẩm\".";
                        return false;
                    }
                    DataRowView drStockStatus = (DataRowView)cboInStockStatusID.SelectedItem;
                    objProductInStock.IsNew = Convert.ToBoolean(drStockStatus["ISNEW"]);
                    objProductInStock.IsShowProduct = Convert.ToBoolean(drStockStatus["ISSHOWPRODUCT"]);
                    objProductInStock.InStockStatusID = Convert.ToInt32(drStockStatus["INSTOCKSTATUSID"]);
                    objProductInStock.IMEI = string.Empty;
                }
            }
            if (strProduct.Trim().Equals(objProductInStock.IMEI.Trim())
                && objProductInStock.InStockStatusID != Convert.ToInt32(cboInStockStatusID.SelectedValue))
            {
                string strMess = "Sản phẩm " + objProductInStock.ProductName + " không thuộc trạng thái " + cboInStockStatusID.SelectedText + ". Vui lòng chọn lại.";
                if (!string.IsNullOrEmpty(objProductInStock.IMEI.Trim()))
                    strMess = "Sản phẩm " + objProductInStock.ProductName + " có IMEI " + objProductInStock.IMEI.Trim() + " không thuộc trạng thái " + cboInStockStatusID.SelectedText + ". Vui lòng chọn lại.";
                strError = strMess;
                return false;
            }
            if (!IsCanChangeProduct(objProductInStock.ConsignmentType))
            {
                strError = "Sản phẩm không thuộc loại hàng hóa trong yêu cầu xuất chuyển kho!";
                return false;
            }
            //row["QUANTITYINSTOCK"] = objProductInStockTemp.Quantity;
            if (!CheckProduct(objProductInStock))
                return false;
            if (!CheckImei(objProductInStock))
                return false;

            DataTable dtbProduct = grdProduct.DataSource as DataTable;
            var varSum = dtbProduct.AsEnumerable()
                        .Where(row => (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == objProductInStock.ProductID.Trim()).Sum(row => Convert.ToDecimal(row.Field<object>("QUANTITY")));// ("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "' AND IMEI = ''")
            if ((varSum + intQuantity) > objProductInStock.RealQuantityProduct && objProductInStock.IsCheckStockQuantity)
            {
                strError = "Sản phẩm " + objProductInStock.ProductName + " không tồn đủ trong kho " + cboFromStoreID.StoreName;
                return false;
            }
            DataRow[] rowProduct = null;
            if (dtbProduct != null)
            {

                rowProduct = dtbProduct.Select("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "'");
                if (rowProduct.Count() > 0)
                {
                    //Trường hợp không cần nhập IMEI, thông báo ra và nhập số lượng
                    if (!objStoreChangeOrderType.IsRequestDetailIMEI && !objProductInStock.IsRequestIMEI)
                    {
                        if ((Convert.ToInt32(rowProduct[0]["QUANTITY"]) + intQuantity) > objProductInStock.Quantity && objProductInStock.IsCheckStockQuantity)
                            rowProduct[0]["QUANTITY"] = objProductInStock.Quantity;
                        else
                            rowProduct[0]["QUANTITY"] = Convert.ToInt32(rowProduct[0]["QUANTITY"]) + intQuantity;
                        //MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " đã tồn tại rồi. Vui lòng nhập sản phẩm khác.");
                        grdProduct.RefreshDataSource();
                        return true;
                    }

                }
                if (string.IsNullOrEmpty(objProductInStock.IMEI.Trim()))
                {
                    // Check ton 

                    rowProduct = dtbProduct.Select("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "' AND IMEI = ''");
                    if (rowProduct.Count() > 0)
                    {
                        if ((Convert.ToInt32(rowProduct[0]["QUANTITY"]) + intQuantity) > objProductInStock.RealQuantityProduct && objProductInStock.IsCheckStockQuantity)
                            rowProduct[0]["QUANTITY"] = objProductInStock.RealQuantityProduct;
                        else
                            rowProduct[0]["QUANTITY"] = Convert.ToInt32(rowProduct[0]["QUANTITY"]) + intQuantity;
                        //MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " đã tồn tại rồi. Vui lòng nhập sản phẩm khác.");
                        grdProduct.RefreshDataSource();
                        return true;
                    }
                }
                rowProduct = null;
                //lay lai row product , khi do ham kiem tra nhieu imei ko con tac dung
                if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI) && bolInputIMEI)
                {
                    rowProduct = dtbProduct.Select("IMEI = '" + objProductInStock.IMEI.Trim() + "'");
                    if (rowProduct.Count() > 0)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "IMEI " + objProductInStock.IMEI + " đã tồn tại rồi. Vui lòng nhập IMEI khác.");
                        return false;
                    }
                }
                if (strProduct.Trim() == objProductInStock.IMEI && objProductInStock.IsRequestIMEI && bolInputIMEI)
                {
                    DataRow[] rowlist = dtbProduct.Select("PRODUCTID = '" + objProductInStock.ProductID.Trim() + "' and IMEI = ''");
                    if (rowlist.Length > 0)
                    {

                        rowlist[0]["IMEI"] = objProductInStock.IMEI;
                        rowlist[0]["QUANTITY"] = 1;
                        return true;
                    }
                }
            }

            DataTable dtbTemp = CreateTableProduct(dtbProduct, rowProduct, objProductInStock.IsRequestIMEI, objProductInStock.MainGroupID, objProductInStock.ProductID, objProductInStock.ProductName, objProductInStock.IMEI, intQuantity, 0, string.Empty, objProductInStock.Quantity, objProductInStock.IsAllowDecimal, objProductInStock.InputVoucherDate);
            if (dtbTemp.Rows.Count > 0) mnuItemExport.Enabled = true;
            grdProduct.DataSource = dtbTemp;
            return true;
        }
        #endregion



        private void cboTransportCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {

            DataRow[] rowService = dtbTransportServices.Select("TRANSPORTCOMPANYID = " + cboTransportCompany.ColumnID);
            if (!rowService.Any())
            {
                DataTable dtb = new DataTable();
                dtb.Columns.Add("TRANSPORTSERVICEID", typeof(int));
                dtb.Columns.Add("TRANSPORTSERVICENAME", typeof(string));
                cboTransportServices.InitControl(false, dtb, "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--- Dịch vụ vận chuyển ---");
                return;
            }
            cboTransportServices.InitControl(false, rowService.CopyToDataTable(), "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--- Dịch vụ vận chuyển ---");
        }

        private void chkCreateAutoInvoice_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtInVoiceID_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void chkIsCheckRealInput_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cboInputTypeSearch_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void cboStoreSearch_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void radOld_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cboDiscountReason_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void cboCurrencyUnit_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void lblCustomerID_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void cboPaymentCard_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }
        
        private void btnVOffice_Click(object sender, EventArgs e)
        {
            bool IsCCDC = this.IsCanChangeProduct(6);
            //if (IsCCDC && IsSuccessReviewCCDC() && this.IsReviewed) // Duyet cong cu dung cu 
            if (IsCCDC &&  this.IsReviewed) // Duyet cong cu dung cu 
            {

                // OK => Xuat tu dong 
                String strMainGroupList = btnStoreChangeAll.Tag == null ? "" : btnStoreChangeAll.Tag.ToString().Trim();
                frmStoreChange frmStoreChange = new StoreChange.frmStoreChange();
                frmStoreChange.StoreChangeOrderID = txtStoreChangeOrderID.Text.Trim();
                frmStoreChange.StoreChangeOrderType = objStoreChangeOrderType;
                if (!string.IsNullOrEmpty(strMainGroupList) && !strMainGroupList.Contains(","))
                {
                    frmStoreChange.MainGroupID = Convert.ToInt32(strMainGroupList);
                }
                else
                    frmStoreChange.ListMainGroupId = strMainGroupList;
                frmStoreChange.IsRequestIMEI = objStoreChangeOrderType.IsRequestDetailIMEI;
                frmStoreChange.dtbNotification_User = dtbNotification_User;
                frmStoreChange.IsGetFifoCCDC = rbtnQuantity.Checked;
                frmStoreChange.prepaidDataForCCDC();
                string message = frmStoreChange.InsertStoreChangeCCDC();
                if (message.Equals("Chuyển kho thành công"))
                {
                    MessageBoxObject.ShowInfoMessage(this, message);
                    btnVOffice.Enabled = false;
                }
                else
                {
                    MessageBoxObject.ShowErrorMessage(this, message);
                }
                EditData();
                tabControl.SelectedTab = tabGeneral;
                this.bolIsHasAction = true;
            }
        }

        private void btnTrinhky_Click(object sender, EventArgs e)
        {
            //Old reprot
            #region
            //string strValueConfig = Library.AppCore.AppConfig.GetStringConfigValue("RPT_SCO_VOFFICE_CCDC");
            //if (!string.IsNullOrEmpty(strValueConfig))
            //{
            //    string[] codes = strValueConfig.Split(',');
            //    if (codes.Length == 2)
            //    {
            //        try
            //        {
            //            int mau01 = Int32.Parse(codes[0]);
            //            int mau02 = Int32.Parse(codes[1]);

            //            ERP.MasterData.PLC.MD.WSReport.Report objReportMau01 = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(mau01);

            //            ERP.MasterData.PLC.MD.WSReport.Report objReportMau02 = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(mau02);
            //            Hashtable htbParameterValue = new Hashtable();
            //            htbParameterValue.Add("STORECHANGEORDERID", this.objStoreChangeOrder.StoreChangeOrderID);
            //            if (objReportMau01 == null)
            //            {
            //                throw new NullReferenceException("Can not load Report infomation from cache");
            //            }
            //            else
            //            {
            //                DUIReportDataSource.ShowReport(this, objReportMau01, htbParameterValue);
            //            }
            //            if (objReportMau02 == null)
            //            {
            //                throw new NullReferenceException("Can not load Report infomation from cache");
            //            }
            //            else
            //            {
            //                DUIReportDataSource.ShowReport(this, objReportMau02, htbParameterValue);
            //            }
            //        }
            //        catch (NullReferenceException ex)
            //        {
            //            MessageBoxObject.ShowErrorMessage(this, ex.Message);
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBoxObject.ShowErrorMessage(this, ex.Message);
            //        }
            //    }
            //}
            //else
            //{
            //    MessageBoxObject.ShowErrorMessage(this, "Can not load Report configuration");
            //}
            #endregion
            //New report
            #region
            ScoExportTemplateTrinhKy objExportTemplateTrinhKy = new ScoExportTemplateTrinhKy();
            ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new ERP.Report.PLC.PLCReportDataSource();
            object[] objKeywords = new object[]
                {
                "@STORECHANGEORDERID", this.objStoreChangeOrder.StoreChangeOrderID
                };
            DataTable dtbResourceMau01 = objPLCReportDataSource.GetHeavyDataSource("RPT_SCO_PRINTER01", objKeywords);
            DataTable dtbResourceMau02 = objPLCReportDataSource.GetHeavyDataSource("RPT_SCO_PRINTER02", objKeywords);
            DataTable dtbTemp01 = (DataTable)dtbResourceMau01;
            DataTable dtbTemp02 = (DataTable)dtbResourceMau02;
            objExportTemplateTrinhKy.ExportData(dtbTemp01, dtbTemp02);
            #endregion
        }

        private void btnDownload_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (!bolPermission_PM_InputVoucher_Attachment_View)
            {
                MessageBox.Show(this, "Bạn không có quyền xem file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (this.grvAttachment.FocusedRowHandle < 0) //grvAttachment.FocusedRowHandle < 0
                return;
            DataRow obj = ((DataRowView)this.grvAttachment.GetRow(this.grvAttachment.FocusedRowHandle)).Row;
            //if ((obj["IsDeleted"] ?? "").ToString().Equals("1"))
            //{
            //    MessageBox.Show(this, "File đã bị xoá trên server!");
            //    return;
            //}
            DownloadFile(obj["FILEID"].ToString(), obj["ATTACHMENTNAME"].ToString());
        }

        /// <summary>
        /// Tải file đính kèm
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="strLocation"></param>
        /// <returns></returns>
        public bool DownloadFile(string strFileID, string strFileName)
        {
            try
            {
                SaveFileDialog dlgSaveFile = new SaveFileDialog();
                dlgSaveFile.Title = "Lưu file tải về";
                dlgSaveFile.FileName = strFileName;
                string strExtension = Path.GetExtension(strFileName);
                switch (strExtension)
                {
                    case ".xls":
                    case ".xlsx":
                        dlgSaveFile.Filter = "Excel Worksheets(*.xls, *.xlsx)|*.xls; *.xlsx";
                        break;
                    case ".doc":
                    case ".docx":
                        dlgSaveFile.Filter = "Word Documents(*.doc, *.docx)|*.doc; *.docx";
                        break;
                    case ".pdf":
                        dlgSaveFile.Filter = "PDF Files(*.pdf)|*.pdf";
                        break;
                    case ".zip":
                    case ".rar":
                        dlgSaveFile.Filter = "Compressed Files(*.zip, *.rar)|*.zip;*.rar";
                        break;
                    case ".jpg":
                    case ".jpe":
                    case ".jpeg":
                    case ".gif":
                    case ".png":
                    case ".bmp":
                    case ".tif":
                    case ".tiff":
                        dlgSaveFile.Filter = "Image Files(*.jpg, *.jpe, *.jpeg, *.gif, *.png, *.bmp, *.tif, *.tiff)|*.jpg; *.jpe; *.jpeg; *.gif; *.png; *.bmp; *.tif; *.tiff";
                        break;
                    default:
                        dlgSaveFile.Filter = "All Files (*)|*";
                        break;
                }
                if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                {
                    if (dlgSaveFile.FileName != string.Empty)
                    {
                        ERP.FMS.DUI.DUIFileManager.DownloadFile(this, strFMSAplication, strFileID, string.Empty, dlgSaveFile.FileName);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception objExce)
            {
                MessageBox.Show("Lỗi tải file đính kèm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi tải file đính kèm", objExce.ToString(), " frmInputVoucher-> DownloadFile", DUIInventory_Globals.ModuleName);
                return false;
            }
        }

        private void btnView_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (!bolPermission_PM_InputVoucher_Attachment_View)
            {
                MessageBox.Show(this, "Bạn không có quyền xem file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            //PLC.Input.WSInputVoucher.InputVoucher_Attachment obj = (PLC.Input.WSInputVoucher.InputVoucher_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
            DataRow obj = ((DataRowView)this.grvAttachment.GetRow(this.grvAttachment.FocusedRowHandle)).Row;
            string strFileName = string.Empty;
            strFileName = LoadProcess(obj["FILEID"].ToString(), obj["ATTACHMENTNAME"].ToString());
            if (!string.IsNullOrWhiteSpace(strFileName))
            {
                Process.Start(strFileName);
            }
        }

        // <summary>
        /// Load url Process tu Server len pictureBox
        /// </summary>
        /// <param name="strFileID"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        private string LoadProcess(string strFileID, string strFileName)
        {
            string strURL = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(strFileID))
                {
                    strURL = Path.GetTempPath() + @"NC_Attachment";
                    if (!Directory.Exists(strURL))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(strURL);
                    }
                    strURL = Path.GetTempPath() + @"NC_Attachment\" + strFileName;
                    ERP.FMS.DUI.DUIFileManager.DownloadFile(this, strFMSAplication, strFileID, string.Empty, strURL);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    }
                }
            }
            catch
            {
                MessageBox.Show(this, "Không thể tải hình lên được", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return strURL;
        }

        private void cboTransportTypeID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            btnReview.Enabled = false;
            int intTransportTypeIDcurent = -1;
            int.TryParse((cboTransportTypeID.ColumnID).ToString().Trim(), out intTransportTypeIDcurent);
            if (intTransportTypeIDcurent < 1)
            {
                DataTable dtb = new DataTable();
                dtb.Columns.Add("TRANSPORTCOMPANYID", typeof(int));
                dtb.Columns.Add("TRANSPORTCOMPANYNAME", typeof(string));
                cboTransportCompany.InitControl(false, dtb, "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đơn vị chuyển phát--");
            }
            else
            {
                DataRow[] dr = dtbTransportCompany.Select("TRANSPORTTYPEID=" + intTransportTypeIDcurent.ToString());
                if (dr.Any())
                    cboTransportCompany.InitControl(false, dr.CopyToDataTable(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đối tác--");
                else
                {
                    DataTable dtb = new DataTable();
                    dtb.Columns.Add("TRANSPORTCOMPANYID", typeof(int));
                    dtb.Columns.Add("TRANSPORTCOMPANYNAME", typeof(string));
                    cboTransportCompany.InitControl(false, dtb, "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đơn vị chuyển phát--");
                }
                // cboTransportCompany.InitControl(false, dtbTransportCompany.Clone(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đối tác--");
            }
            if (objStoreChangeOrderType.IsReviewByTransType)
            {
                DataTable dtbTransportTypeCurrent = (DataTable)cboTransportTypeID.DataSource;
                if (cboTransportTypeID.ColumnID != null)
                {
                    //int intTransportTypeIDcurent = -1;
                    //int.TryParse((cboTransportTypeID.SelectedValue ?? "").ToString().Trim(), out intTransportTypeIDcurent);
                    string strReviewByTranSportID = "";
                    DataRow[] drs = dtbTransportTypeCurrent.Select("TRANSPORTTYPEID = " + intTransportTypeIDcurent.ToString());
                    if (drs != null && drs.Length > 0)
                        strReviewByTranSportID = (drs[0]["REVIEWFUNCTIONID"] ?? "").ToString().Trim();
                    if (!string.IsNullOrEmpty(strReviewByTranSportID))
                        btnReview.Enabled = SystemConfig.objSessionUser.IsPermission(strReviewByTranSportID) && !objStoreChangeOrder.IsReviewed;
                }
            }

            //object[] objKeywords = new object[] { "@TRANSPORTTYPEID", intTransportTypeIDcurent
            //};
            //PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
            ////dtbTransportCompany = objPLCStoreChangeCommand.SearchDataTranCompany(objKeywords);
            //if (dtbTransportCompany != null)
            //{
            //    DataRow[] rowService = dtbTransportServices.Select("TRANSPORTTYPEID = " + cboTransportTypeID.SelectedValue);
            //    cboTransportCompany.InitControl(false, rowService.CopyToDataTable(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đơn vị chuyển phát--");

            //strTRANSPORTCOMPANYID = (cboTransportTypeID.SelectedValue).ToString();
            //if (!String.IsNullOrEmpty(strTRANSPORTCOMPANYID))
            //{
            //    DataRow[] dr = dtbTransportCompany.Select("TransportTypeID=" + cboTransportTypeID.SelectedValue);
            //    if (dr.Any())
            //        cboTransportCompany.InitControl(false, dr.CopyToDataTable(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đối tác--");
            //    else
            //        cboTransportCompany.InitControl(false, dtbTransportCompany.Clone(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đối tác--");
            //}
            //else
            //    cboTransportCompany.InitControl(false, dtbTransportCompany.Clone(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đối tác--");



            btnUpdate.Enabled = true;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            UpLoadFile();
        }
    }
}
