﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System.Threading;
using System.Collections;
using System.Text.RegularExpressions;
using Library.AppCore.Forms;
using System.Diagnostics;
using Library.AppCore.Constant;
using DevExpress.XtraGrid.Views.Grid;
using ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange;
using C1.C1Excel;
using System.Globalization;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmEvictionIMEI : Form
    {
        #region Constructor
        public frmEvictionIMEI()
        {
            InitializeComponent();
        }
        #endregion

        #region Variables 
        private int intStoreChangeCommandTypeID = -1;
        private PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
        private DataTable dtbAutoStoreChange = null;
        public DataTable dtbStore = null;
        DataTable dtbStoreMainGroup = null;
        DataTable dtbMainGroupPermission = null;
        public int intMainGroupID = 0;
        private PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private DataTable dtbStoreInStock = null; // Bảng tồn kho các sản phẩm nhập từ excel
        private DataTable dtbStoreInStockNoIMEI = null;
        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private bool bolIsReceive = false;

        private int intStoreID = -1;
        private string strStoreIDList = null;
        private DataTable dtbProductCache = null;

        // LE VAN DONG - 01/10/2017
        private DataTable dtbStoreCache = null;
        private ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType = null;


        private DataTable dtbToStore = null;
        private DataTable dtbFromStore = null;
        private DataTable dtbWarrantyCenter = null;
        private DataTable dtbWarrantyCenterFilter = null;
        private DataTable dtbInputChangeOrderType = null;
        private DataTable dtbMachineErrorGroup = null;
        private DataTable dtbMachineError = null;
        private DataTable dtbViewProductStatus = null;
        private List<string> listInStockStatusID = null;
        private DataTable dtbSearch = null;
        private PLC.InputChangeOrder.PLCInputChangeOrder objPLCInputChangeOrder = new PLC.InputChangeOrder.PLCInputChangeOrder();
        private bool bolIsPermission = false;
        private int intToStoreID = -1;
        private string strToStoreName = string.Empty;
        #endregion

        #region Property
        #endregion

        #region Support Functions

        private void EnableControl(bool bolIsEnable)
        {
            panel1.Enabled = bolIsEnable;
            mnuGrid.Enabled = bolIsEnable;
        }

        private void InitAutoStoreChangeTable()
        {
            dtbAutoStoreChange = new DataTable();
            dtbAutoStoreChange.Columns.Add("Select", typeof(bool));
            dtbAutoStoreChange.Columns.Add("ProductID", typeof(string));
            dtbAutoStoreChange.Columns.Add("ProductName", typeof(String));
            dtbAutoStoreChange.Columns.Add("FromStoreID", typeof(int));
            dtbAutoStoreChange.Columns.Add("FromStoreName", typeof(String));
            dtbAutoStoreChange.Columns.Add("FromStoreInStock", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("ToStoreID", typeof(int));
            dtbAutoStoreChange.Columns.Add("ToStoreName", typeof(String));
            dtbAutoStoreChange.Columns.Add("ToStoreInStock", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("Quantity", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("QuantityInStock", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("IsAllowDecimal", typeof(bool));
            dtbAutoStoreChange.Columns.Add("IMEI", typeof(string)).DefaultValue = string.Empty;
            dtbAutoStoreChange.Columns.Add("IsUrgent", typeof(bool));
            dtbAutoStoreChange.Columns.Add("Note", typeof(string)).DefaultValue = string.Empty;

        }

        private decimal GetQuantity(string strProductID, int intStoreID, int productStatusID = -1)
        {
            Decimal decQuantity = 0;
            if (dtbStoreInStock == null || dtbStoreInStock.Rows.Count < 1)
                return decQuantity;
            if (productStatusID == -1)
            {
                DataRow[] row = dtbStoreInStock.Select("ProductID ='" + strProductID + "'" + " and StoreID = " + intStoreID);
                if (row != null && row.Length > 0)
                {
                    foreach (DataRow rowItem in row)
                        decQuantity += Convert.ToDecimal(row[0]["QUANTITY"]);
                }
            }
            else
            {
                DataRow[] row = dtbStoreInStock.Select("ProductID ='" + strProductID + "'" + " and StoreID = " + intStoreID + " and INSTOCKSTATUSID = " + productStatusID);
                if (row != null && row.Length > 0)
                {
                    foreach (DataRow rowItem in row)
                        decQuantity += Convert.ToDecimal(row[0]["QUANTITY"]);
                }

            }
            return decQuantity;
        }

        public bool IsNumber(string pText)
        {
            if (string.IsNullOrEmpty(pText))
                return false;
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(pText);
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang tạo dữ liệu...");
        }

        private void LoadComboBox()
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(bagrvReport);

            Report.PLC.PLCReportDataSource objPLCReport = new Report.PLC.PLCReportDataSource();
            dtbInputChangeOrderType = objPLCReport.GetDataSource("MD_INPUTCHANGEORDERTYPE_GET", new object[] { });

            dtbFromStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            dtbToStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            dtbWarrantyCenter = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_WARRANTYCENTER_CACHE").Copy();
            dtbMachineErrorGroup = Library.AppCore.DataSource.GetDataSource.GetMachineErrorGroup().Copy();
            dtbMachineError = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_MACHINEERROR_SRH4").Copy();
            dtbViewProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            dtbWarrantyCenterFilter = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_WARRANTYCENTER_SGROUP_SRH2").Copy();

            //Kho thu hồi ----------------s
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilterToStore = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objStoreFilterToStore.OtherCondition = "ISWARRANTYSTORE = 1";
            objStoreFilterToStore.OtherCondition = "ISCENTERSTORE = 1";
            //objStoreFilterToStore.StorePermissionType = EnumType.StorePermissionType.;
            cboToStore.InitControl(false, objStoreFilterToStore);
            //Kho thu hồi ----------------

            //Siêu thị --------------------
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilterFromStore = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objStoreFilterFromStore.AreaIDList = cboArea.AreaIDList;
            // objStoreFilterFromStore.StorePermissionType = EnumType.StorePermissionType.STORECHANGEORDER;
            cboFromStore.InitControl(true, objStoreFilterFromStore);
            cboFromStore.IsReturnAllWhenNotChoose = true;
            //Siêu thị --------------------

            //Trạng thái SP----------------

            if (listInStockStatusID != null && listInStockStatusID.Any() && !string.IsNullOrWhiteSpace(listInStockStatusID[0]))
            {
                DataTable dtbViewProductStatusTemp = dtbViewProductStatus.Clone();
                var lstViewProductStatusTemp = from a in dtbViewProductStatus.AsEnumerable()
                                               join b in listInStockStatusID
                                               on a["PRODUCTSTATUSID"].ToString().Trim() equals b.ToString().Trim()
                                               select a;
                if (lstViewProductStatusTemp.Any())
                    dtbViewProductStatusTemp.Merge(lstViewProductStatusTemp.CopyToDataTable());
                cboProductStatus.InitControl(true, dtbViewProductStatusTemp, "PRODUCTSTATUSID", "PRODUCTSTATUSNAME", "-- Tất cả --");
            }
            else
                cboProductStatus.InitControl(true, dtbViewProductStatus, "PRODUCTSTATUSID", "PRODUCTSTATUSNAME", "-- Tất cả --");

            cboProductStatus.IsReturnAllWhenNotChoose = true;
            //Trạng thái SP----------------

            cboMainGroup.InitControl(true);
            cboBrand.InitControl(true, cboMainGroup.MainGroupIDList);
            cboSubGroup.InitControl(true, cboMainGroup.MainGroupIDList);
            cboArea.InitControl(true);
            cboWarrantyCenter.InitControl(true, dtbWarrantyCenter, "WARRANTYCENTERID", "WARRANTYCENTERNAME", "-- Tất cả --");
            cboInputChangeOrderType.InitControl(true, dtbInputChangeOrderType, "INPUTCHANGEORDERTYPEID", "INPUTCHANGEORDERTYPENAME", "-- Tất cả --");
            //cboInputChangeOrderType.IsReturnAllWhenNotChoose = true;
            cboMachineErrorGroup.InitControl(true, dtbMachineErrorGroup, "MACHINEERRORGROUPID", "MACHINEERRORGROUPNAME", "-- Tất cả --");
            cboMachineError.InitControl(true, dtbMachineError, "MACHINEERRORID", "MACHINEERRORNAME", "-- Tất cả --");
            //  cboMachineError.IsReturnAllWhenNotChoose = true;
            cboProductList.InitControl(0, string.Empty, string.Empty);
            dtToOutputDate.Value = DateTime.Now;
            dtToOutputDate.MaxDate = DateTime.Now;
            dtFromOutputDate.Value = dtToOutputDate.Value;
            dtFromOutputDate.MaxDate = DateTime.Now;
        }

        private DataTable CreateImportResultTable()
        {
            DataTable tblResult = new DataTable();
            tblResult.Columns.Add("ProductID", typeof(String));
            tblResult.Columns.Add("ProductName", typeof(String));
            tblResult.Columns.Add("FromStoreName", typeof(String));
            tblResult.Columns.Add("FromInStockQuantity", typeof(decimal));
            tblResult.Columns.Add("ToStoreName", typeof(String));
            tblResult.Columns.Add("ToInStockQuantity", typeof(decimal));
            tblResult.Columns.Add("Quantity", typeof(decimal));
            tblResult.Columns.Add("Status", typeof(String));
            tblResult.Columns.Add("StatusID", typeof(Int32));
            tblResult.Columns.Add("IsUrgent", typeof(Boolean));
            tblResult.Columns.Add("ErrorContent", typeof(String));
            tblResult.Columns.Add("IMEI", typeof(string)).DefaultValue = string.Empty;
            return tblResult;
        }

        private bool CheckInputCreateOrder(ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType, int intFromStoreID, int intToStoreID)
        {
            DataRow[] rowFromStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
            DataRow[] rowToStore = dtbStoreCache.Select("STOREID = " + intToStoreID);
            // 1 Kiem tra cong ty
            if (objStoreChangeType.CheckCompanyType == 1)
            {
                if (Convert.ToInt32(rowFromStore[0]["COMPANYID"]) != Convert.ToInt32(rowToStore[0]["COMPANYID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckCompanyType == 2)
            {
                if (Convert.ToInt32(rowFromStore[0]["COMPANYID"]) == Convert.ToInt32(rowToStore[0]["COMPANYID"]))
                    return false;
            }
            // 2 kiem tra tỉnh
            if (objStoreChangeType.CheckProvinceType == 1)
            {
                if (Convert.ToInt32(rowFromStore[0]["PROVINCEID"]) != Convert.ToInt32(rowToStore[0]["PROVINCEID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckProvinceType == 2)
            {
                if (Convert.ToInt32(rowFromStore[0]["PROVINCEID"]) == Convert.ToInt32(rowToStore[0]["PROVINCEID"]))
                    return false;
            }

            // 3 Kiem tra chi nhánh
            if (objStoreChangeType.CheckBranchType == 1)
            {
                if (Convert.ToInt32(rowFromStore[0]["BRANCHID"]) != Convert.ToInt32(rowToStore[0]["BRANCHID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckBranchType == 2)
            {
                if (Convert.ToInt32(rowFromStore[0]["BRANCHID"]) == Convert.ToInt32(rowToStore[0]["BRANCHID"]))
                    return false;
            }
            return true;
        }

        private bool CheckImportExcel(DataTable tProduct)
        {
            try
            {
                if (string.IsNullOrEmpty(tProduct.Rows[0][0].ToString().Trim().ToLower())
                    || tProduct.Rows[0][0].ToString().Trim().ToLower() != "imei")
                    return false;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            return true;
        }
        private List<int> GetConsignmentType(int storeChangeOrderTypeID)
        {
            List<int> listProdConsignmentType = new List<int>();
            DataTable dtb = null;
            new MasterData.PLC.MD.PLCStoreChangeOrderType().GetProductConsignmentTypeBySCOTID(ref dtb, storeChangeOrderTypeID);
            if (dtb != null)
            {
                listProdConsignmentType = dtb.AsEnumerable().ToList().Select(d => Convert.ToInt32(d["PRODUCTCONSIGNMENTTYPEID"])).ToList();
            }
            return listProdConsignmentType;
        }

        private void GetDataSourceImport()
        {
            bool bolIsInputChangeOrderType = false;
            dtbSearch = new DataTable();
            string strMainGroupIDList = string.Empty;
            string strSubGroupIDList = string.Empty;
            string strBrandIDList = string.Empty;
            string strProductIDList = string.Empty;
            string strInputChangeOrderTypeIDList = string.Empty;
            string strMachineErrorIDList = string.Empty;
            string strInStockStatusID = string.Empty;
            string strFromStoreIDList = string.Empty;
            string strWarratyCenterIDList = string.Empty;

            if (string.IsNullOrEmpty(cboInputChangeOrderType.ColumnIDList))
            {
                DataTable dtbType = cboInputChangeOrderType.DataSource.Copy();
                if (dtbType != null && dtbType.Rows.Count > 0)
                {
                    foreach (DataRow rowType in dtbType.Rows)
                        strInputChangeOrderTypeIDList += rowType["INPUTCHANGEORDERTYPEID"].ToString() + ",";
                    strInputChangeOrderTypeIDList = strInputChangeOrderTypeIDList.Trim().TrimEnd(',');
                }
            }
            else
            {
                strInputChangeOrderTypeIDList = cboInputChangeOrderType.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                bolIsInputChangeOrderType = true;
            }

            if (string.IsNullOrWhiteSpace(cboMachineErrorGroup.ColumnIDList))
            {
                if (!string.IsNullOrWhiteSpace(cboMachineError.ColumnIDList))
                    strMachineErrorIDList = cboMachineError.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(cboMachineError.ColumnIDList))
                {
                    DataTable dtbType = cboMachineError.DataSource.Copy();
                    if (dtbType != null && dtbType.Rows.Count > 0)
                    {
                        foreach (DataRow rowType in dtbType.Rows)
                            strMachineErrorIDList += rowType["MACHINEERRORID"].ToString() + ",";
                        strMachineErrorIDList = strMachineErrorIDList.Trim().TrimEnd(',');
                    }
                }
                else
                    strMachineErrorIDList = cboMachineError.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            }

            strInStockStatusID = cboProductStatus.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            strFromStoreIDList = cboFromStore.StoreIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            strWarratyCenterIDList = cboWarrantyCenter.ColumnIDList.Trim();
            strMainGroupIDList = cboMainGroup.MainGroupIDList.Trim();
            strSubGroupIDList = cboSubGroup.SubGroupIDList.Trim();
            strBrandIDList = cboBrand.BrandIDList.Trim();

            //Chọn WarrantyCenter và không chọn bất cứ 3 combo nào
            if (!string.IsNullOrWhiteSpace(strWarratyCenterIDList)
                && string.IsNullOrWhiteSpace(strMainGroupIDList)
                && string.IsNullOrWhiteSpace(strSubGroupIDList)
                && string.IsNullOrWhiteSpace(strBrandIDList))
            {
                DataTable dtbWarrantyCenter = dtbWarrantyCenterFilter.Copy();
                List<string> lstWarrantyChoose = new List<string>(cboWarrantyCenter.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "").Split(','));
                if (lstWarrantyChoose.Any())
                {
                    var listFirst = from a in dtbWarrantyCenter.AsEnumerable()
                                    join b in lstWarrantyChoose
                                    on a["WARRANTYCENTERID"].ToString().Trim() equals b.ToString().Trim()
                                    select a;

                    if (listFirst.Any())
                    {
                        //SubGroupID
                        foreach (DataRow row in listFirst.CopyToDataTable().AsDataView().ToTable(true, "SUBGROUPID").Rows)
                            strSubGroupIDList += "<" + (Convert.ToString(row["SUBGROUPID"]).Trim()) + ">";

                        //BrandID
                        foreach (DataRow row in listFirst.CopyToDataTable().AsDataView().ToTable(true, "BRANDID").Rows)
                            strBrandIDList += "<" + (Convert.ToString(row["BRANDID"]).Trim()) + ">";

                        foreach (DataRow row in listFirst.CopyToDataTable().AsDataView().ToTable(true, "MAINGROUPID").Rows)
                            strMainGroupIDList += "<" + (Convert.ToString(row["MAINGROUPID"]).Trim()) + ">";

                        if (string.IsNullOrWhiteSpace(strSubGroupIDList) || string.IsNullOrWhiteSpace(strBrandIDList) || string.IsNullOrWhiteSpace(strMainGroupIDList))
                        {
                            MessageBox.Show(this, "Trung tâm bảo hành được chọn chưa được thiết lập nhóm hàng, ngành hàng và NSX!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            cboWarrantyCenter.Focus();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "Trung tâm bảo hành được chọn chưa được thiết lập nhóm hàng,ngành hàng  hoặc NSX!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        cboWarrantyCenter.Focus();
                        return;
                    }
                }
            }
            DataTable dtbSearchTemp = new DataTable();
            int intType = 0;
            if (bolIsInputChangeOrderType)
                intType = 1;
            object[] objKeywords = new object[]{"@MAINGROUPIDLIST", strMainGroupIDList,
                                                "@SUBGROUPIDLIST", strSubGroupIDList,
                                                "@BRANDIDLIST", strBrandIDList,
                                                "@PRODUCTIDLIST", cboProductList.ProductIDList.Trim(),
                                                "@INSTOCKSTATUSIDLIST", strInStockStatusID,
                                                "@FROMDATE", dtFromOutputDate.Value,
                                                "@TODATE", dtToOutputDate.Value,
                                                "@INPUTCHANGEORDERTYPEIDLIST", strInputChangeOrderTypeIDList,
                                                "@MACHINEERRORIDLIST", strMachineErrorIDList,
                                                "@AREAIDLIST", cboArea.AreaIDList.Trim(),
                                                "@STOREIDLIST", strFromStoreIDList,
                                                "@USERNAME", SystemConfig.objSessionUser.UserName,
                                                "@ISINPUTCHANGEORDERTYPE", intType,
                                                "@TOSTOREID", cboToStore.StoreID
                                                };
            dtbSearchTemp = objPLCInputChangeOrder.GetEvictionIMEI(objKeywords);
            if (dtbSearchTemp != null)
            {
                dtbSearch = dtbSearchTemp.Copy();
                //if (dtbSearchTemp.Rows.Count > 0)
                //{
                //    var q = from n in dtbSearchTemp.AsEnumerable().Where(p => !string.IsNullOrWhiteSpace(p["IMEI"].ToString()))
                //            group n by n["IMEI"] into g
                //            select g.OrderByDescending(t =>
                //             DateTime.ParseExact(t["CHANGEDATE"].ToString().Trim(), "dd'/'MM'/'yyyy HH':'mm", CultureInfo.InvariantCulture)
                //                                                 ).FirstOrDefault();
                //    dtbSearch = q.CopyToDataTable();
                //}
                //else
                //    dtbSearch = dtbSearchTemp.Copy();
            }
            else
            {
                MessageBox.Show(this, "Lỗi lấy dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            intToStoreID = cboToStore.StoreID;
            strToStoreName = cboToStore.StoreName.Trim();
        }

        private void ImportExcelIMEI()
        {
            #region Lấy dữ liệu và kiểm tra IMEI
            DataTable tblProductIMEI = new DataTable();
            string strDefaultExt = ".xls,.xlsx";
            string strFilter = "All Excel files (*.xls, *.xlsx)|*.xls;*.xlsx|Excel 2003 (*.xls)|*.xls|Excel 2007 (*.xlsx)|*.xlsx";
            OpenFileDialog frmOpenDialog = new OpenFileDialog();
            frmOpenDialog.DefaultExt = strDefaultExt;
            frmOpenDialog.Filter = strFilter;
            String strFileName = string.Empty;
            if (frmOpenDialog.ShowDialog() == DialogResult.OK)
            {
                strFileName = frmOpenDialog.FileName;
            }
            else
                return;
            if (strFileName.Trim().Length > 0)
                tblProductIMEI = OpenExcel2DataTable(strFileName, 1);

            if (tblProductIMEI == null)
            {
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (tblProductIMEI.Rows.Count == 0)
            {
                MessageBox.Show(this, "File excel không có dữ liệu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (tblProductIMEI.Rows.Count >= 1)
            {
                if (string.IsNullOrWhiteSpace(tblProductIMEI.Rows[0][0].ToString()))
                {
                    MessageBox.Show(this, "File excel không có dữ liệu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (!CheckImportExcel(tblProductIMEI))
                {
                    MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    if (tblProductIMEI.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x["Column0"].ToString().Trim())).Count() < 2)
                    {
                        MessageBox.Show(this, "File excel không có dữ liệu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }

            else { return; }


            #endregion

            Thread objThread = new Thread(new ThreadStart(Excute));
            objThread.Start();

            #region Lấy dữ liệu từ search
            GetDataSourceImport();
            DataTable dtbFullTable = dtbSearch.Copy();
            #endregion

            #region Đẩy dữ liệu vào list để kiểm tra
            DataTable dtbIMEI = new DataTable();
            dtbIMEI.Columns.Add("IMEI", typeof(String));
            dtbIMEI.Clear();

            DataTable dtbCheck = new DataTable();
            DataTable tblResultIMEI = CreateImportResultTable();
            tblResultIMEI.Columns.Add("FromStoreID", typeof(string));
            tblResultIMEI.Columns.Add("ToStoreID", typeof(string));

            tblProductIMEI.Rows.RemoveAt(0);
            foreach (DataRow row in tblProductIMEI.Rows)
            {
                if (!string.IsNullOrWhiteSpace(row[0].ToString().Trim()))
                    dtbIMEI.Rows.Add(row[0].ToString().Trim());
            }
            //tblProductIMEI.Rows.RemoveAt(0);
            #endregion

            string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbIMEI.AsDataView().ToTable(true, "IMEI"), "IMEI");
            DataTable dtbTotalImei = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("CHECKIMEI_INSTOCK_2", new object[] { "@IMEI", xmlIMEI });

            //Lấy danh sách Sản phẩm, Kho, Tồn kho
            DataTable dtbProduct = dtbTotalImei.AsDataView().ToTable(true, "PRODUCTID");
            DataTable dtbStore = dtbTotalImei.AsDataView().ToTable(true, "STOREID");
            GetStoreInStockWithIMEI(dtbStore, dtbProduct);

            #region Đẩy list

            string toStoreID = cboToStore.StoreID.ToString().Trim();
            string toStoreName = cboToStore.StoreName.Trim();

            foreach (DataRow row in tblProductIMEI.Rows)
            {
                if (!string.IsNullOrWhiteSpace(row[0].ToString().Trim()))
                {
                    string strIMEI = row[0].ToString().Trim();
                    string strProductID = string.Empty;
                    string productName = string.Empty;
                    decimal decFromInStockQuantity = 0;
                    decimal decToInStockQuantity = 0;
                    string fromStoreID = string.Empty;
                    string fromStoreName = string.Empty;

                    tblResultIMEI.Rows.Add(strProductID, productName, fromStoreName,
                            decFromInStockQuantity, toStoreName, decToInStockQuantity, 1,
                            "Lỗi", 1, false, "Không tìm thấy thông tin IMEI", strIMEI, fromStoreID, toStoreID);
                }
            }
            #endregion

            #region KT IMEI
            //Thông tin IMEI

            var listIMEI = from a in dtbTotalImei.AsEnumerable()
                           join b in tblResultIMEI.AsEnumerable()
                           on a["IMEI"].ToString().Trim() equals b["IMEI"].ToString().Trim()
                           select new { a, b };
            if (listIMEI.Any())
            {
                listIMEI.All(c =>
                {
                    c.b["ProductID"] = c.a["PRODUCTID"].ToString().Trim();
                    c.b["ProductName"] = c.a["PRODUCTNAME"].ToString().Trim();
                    c.b["FromStoreID"] = c.a["STOREID"].ToString().Trim();
                    c.b["FromStoreName"] = c.a["STORENAME"].ToString().Trim();
                    c.b["StatusID"] = 1;
                    c.b["Status"] = "Lỗi";
                    c.b["FromInStockQuantity"] = GetQuantity(c.a["PRODUCTID"].ToString().Trim(), Convert.ToInt32(c.a["STOREID"].ToString()));
                    c.b["ToInStockQuantity"] = GetQuantity(c.a["PRODUCTID"].ToString().Trim(), Convert.ToInt32(toStoreID));
                    c.b["ErrorContent"] = "IMEI không thuộc dữ liệu đang xét";
                    return true;
                });
            }



            //Search Filter
            var listFilter = from a in dtbSearch.AsEnumerable()
                             join b in tblResultIMEI.AsEnumerable()
                             on a["IMEI"].ToString() equals b["IMEI"].ToString().Trim()
                             select b;
            if (listFilter.Any())
                listFilter.All(c =>
                {
                    c["StatusID"] = 0;
                    c["Status"] = "Thành công";
                    c["ErrorContent"] = string.Empty;
                    return true;
                });

            //Check Trùng
            var listDuplicates = from a in tblResultIMEI.AsEnumerable().GroupBy(x => x["IMEI"].ToString().Trim())
                                            .Where(g => g.Count() > 1).SelectMany(g => g)
                                 select a;
            if (listDuplicates.Any())
            {
                DataTable dtbDuplicates = listDuplicates.CopyToDataTable().AsDataView().ToTable(true, "IMEI");
                foreach (DataRow row in dtbDuplicates.Rows)
                {
                    var listCheck = from a in tblResultIMEI.AsEnumerable().Where(p => p["IMEI"].ToString().Trim()
                                                        == row["IMEI"].ToString().Trim()).Skip(1)
                                    select a;
                    if (listCheck.Any())
                    {
                        listCheck.All(c =>
                        {
                            c["StatusID"] = 1;
                            c["Status"] = "Lỗi";
                            c["ErrorContent"] = "IMEI bị trùng";
                            return true;
                        });
                    }
                }
            }

            var listInsert = from a in dtbSearch.AsEnumerable()
                             join b in tblResultIMEI.AsEnumerable().Where(p => p["StatusID"].ToString() == "0")
                             on a["IMEI"].ToString().Trim() equals b["IMEI"].ToString().Trim()
                             select a;

            if (listInsert.Any())
            {
                DataTable dtbSearchImport = listInsert.CopyToDataTable().Copy();
                dtbSearch.Clear();
                dtbSearch = dtbSearchImport.Copy();
                CreateBandView_Eviction(dtbSearch);
                grdReport.DataSource = dtbSearch;
            }

            #endregion

            frmWaitDialog.Close();
            Thread.Sleep(0);
            objThread.Abort();

            //--------------------Kiểm tra và chạy Kết quả Excel

            if (tblResultIMEI != null && tblResultIMEI.Rows.Count > 0
                && tblResultIMEI.AsEnumerable().Where(p => p["StatusID"].ToString().Trim() != "0").FirstOrDefault() != null)
            {
                frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                frmShowImportResult1.ImportResult = tblResultIMEI;
                frmShowImportResult1.HaveIMEI = true;
                frmShowImportResult1.ShowDialog();
            }
        }

        private void GetStoreInStockWithIMEI(DataTable dtbStore, DataTable dtbProduct)
        {

            if (dtbStore == null || dtbProduct.Rows.Count < 1 || dtbStore.Rows.Count < 1 || dtbProduct == null)
                return;
            string strProductIDList = string.Empty;
            string strStoreIDList = "<" + cboToStore.StoreID.ToString() + ">";
            string strInStockStatusID = string.Empty;

            foreach (DataRow row in dtbStore.Rows)
                strStoreIDList = strStoreIDList + "<" + row["StoreID"].ToString().Trim() + ">";

            foreach (DataRow row in dtbProduct.Rows)
                strProductIDList = strProductIDList + "<" + row["ProductID"].ToString().Trim() + ">";

            if (string.IsNullOrWhiteSpace(cboProductStatus.ColumnIDList))
            {
                DataTable dtbType = cboProductStatus.DataSource.Copy();
                if (dtbType != null && dtbType.Rows.Count > 0)
                {
                    foreach (DataRow rowType in dtbType.Rows)
                        strInStockStatusID += rowType["PRODUCTSTATUSID"].ToString() + ",";
                    strInStockStatusID = strInStockStatusID.Trim().TrimEnd(',');
                }
            }
            else
                strInStockStatusID = cboProductStatus.ColumnIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");



            strProductIDList = strProductIDList.Replace("<", string.Empty).Replace(">", ",");
            strStoreIDList = strStoreIDList.Replace("<", string.Empty).Replace(">", ",");
            object[] objKeywords = new object[]{"@PRODUCTIDLIST", strProductIDList,
                                                "@STOREIDLIST", strStoreIDList,
                                                "@ISCHECKREALINPUT", 1,
                                                "@InStockStatusIDList", strInStockStatusID ,
                                                };
            dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_STOCKREPORT_AUTOCHANGE", objKeywords);






            //object[] objKeywords = new object[] { "@PRODUCTIDLIST", strProductIDList.ToString(),
            //                                    "@STOREIDLIST", strStoreIDList,
            //                                    "@ISCHECKREALINPUT", 1,
            //                                    "@InStockStatusIDList", strInStockStatusID,
            //                                    "@IsExistStock", 1 };

            //dtbStoreInStock = objPLCReportDataSource.GetDataSource("RPT_PM_STOCKREPORT_BYSTORE", objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                MessageBoxObject.ShowWarningMessage(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);

        }

        private void GetDataSource()
        {
            DataTable dtbSearchTemp = new DataTable();
            bool bolIsInputChangeOrderType = false;
            dtbSearch = new DataTable();
            string strMainGroupIDList = string.Empty;
            string strSubGroupIDList = string.Empty;
            string strBrandIDList = string.Empty;
            string strProductIDList = string.Empty;
            string strInputChangeOrderTypeIDList = string.Empty;
            string strMachineErrorIDList = string.Empty;
            string strInStockStatusID = string.Empty;
            string strFromStoreIDList = string.Empty;
            string strWarratyCenterIDList = string.Empty;

            strFromStoreIDList = cboFromStore.StoreIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            if (string.IsNullOrEmpty(cboInputChangeOrderType.ColumnIDList))
            {
                DataTable dtbType = cboInputChangeOrderType.DataSource.Copy();
                if (dtbType != null && dtbType.Rows.Count > 0)
                {
                    foreach (DataRow rowType in dtbType.Rows)
                        strInputChangeOrderTypeIDList += rowType["INPUTCHANGEORDERTYPEID"].ToString() + ",";
                    strInputChangeOrderTypeIDList = strInputChangeOrderTypeIDList.Trim().TrimEnd(',');
                }
            }
            else
            {
                strInputChangeOrderTypeIDList = cboInputChangeOrderType.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                bolIsInputChangeOrderType = true;
            }
            strInStockStatusID = cboProductStatus.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");

            if (string.IsNullOrWhiteSpace(cboMachineErrorGroup.ColumnIDList))
            {
                if (!string.IsNullOrWhiteSpace(cboMachineError.ColumnIDList))
                    strMachineErrorIDList = cboMachineError.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(cboMachineError.ColumnIDList))
                {
                    DataTable dtbType = cboMachineError.DataSource.Copy();
                    if (dtbType != null && dtbType.Rows.Count > 0)
                    {
                        foreach (DataRow rowType in dtbType.Rows)
                            strMachineErrorIDList += rowType["MACHINEERRORID"].ToString() + ",";
                        strMachineErrorIDList = strMachineErrorIDList.Trim().TrimEnd(',');
                    }
                }
                else
                    strMachineErrorIDList = cboMachineError.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            }

            strMainGroupIDList = cboMainGroup.MainGroupIDList.Trim();
            strSubGroupIDList = cboSubGroup.SubGroupIDList.Trim();
            strBrandIDList = cboBrand.BrandIDList.Trim();
            strWarratyCenterIDList = cboWarrantyCenter.ColumnIDList.Trim();

            //Chọn WarrantyCenter và không chọn bất cứ 3 combo nào
            if (!string.IsNullOrWhiteSpace(strWarratyCenterIDList)
                && string.IsNullOrWhiteSpace(strMainGroupIDList)
                && string.IsNullOrWhiteSpace(strSubGroupIDList)
                && string.IsNullOrWhiteSpace(strBrandIDList))
            {
                DataTable dtbWarrantyCenter = dtbWarrantyCenterFilter.Copy();
                List<string> lstWarrantyChoose = new List<string>(cboWarrantyCenter.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "").Split(','));
                if (lstWarrantyChoose.Any())
                {
                    var listFirst = from a in dtbWarrantyCenter.AsEnumerable()
                                    join b in lstWarrantyChoose
                                    on a["WARRANTYCENTERID"].ToString().Trim() equals b.ToString().Trim()
                                    select a;

                    if (listFirst.Any())
                    {
                        //SubGroupID
                        foreach (DataRow row in listFirst.CopyToDataTable().AsDataView().ToTable(true, "SUBGROUPID").Rows)
                            strSubGroupIDList += "<" + (Convert.ToString(row["SUBGROUPID"]).Trim()) + ">";

                        //BrandID
                        foreach (DataRow row in listFirst.CopyToDataTable().AsDataView().ToTable(true, "BRANDID").Rows)
                            strBrandIDList += "<" + (Convert.ToString(row["BRANDID"]).Trim()) + ">";

                        foreach (DataRow row in listFirst.CopyToDataTable().AsDataView().ToTable(true, "MAINGROUPID").Rows)
                            strMainGroupIDList += "<" + (Convert.ToString(row["MAINGROUPID"]).Trim()) + ">";

                        if (string.IsNullOrWhiteSpace(strSubGroupIDList) || string.IsNullOrWhiteSpace(strBrandIDList) || string.IsNullOrWhiteSpace(strMainGroupIDList))
                        {
                            MessageBox.Show(this, "Trung tâm bảo hành được chọn chưa được thiết lập nhóm hàng, ngành hàng và NSX!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            cboWarrantyCenter.Focus();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "Trung tâm bảo hành được chọn chưa được thiết lập nhóm hàng, ngành hàng và NSX!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        cboWarrantyCenter.Focus();
                        return;
                    }
                }
            }
            int intType = 0;
            if (bolIsInputChangeOrderType)
                intType = 1;
            object[] objKeywords = new object[]{"@MAINGROUPIDLIST", strMainGroupIDList,
                                                "@SUBGROUPIDLIST", strSubGroupIDList,
                                                "@BRANDIDLIST", strBrandIDList,
                                                "@PRODUCTIDLIST", cboProductList.ProductIDList.Trim(),
                                                "@INSTOCKSTATUSIDLIST", strInStockStatusID,
                                                "@FROMDATE", dtFromOutputDate.Value,
                                                "@TODATE", dtToOutputDate.Value,
                                                "@INPUTCHANGEORDERTYPEIDLIST", strInputChangeOrderTypeIDList,
                                                "@MACHINEERRORIDLIST", strMachineErrorIDList,
                                                "@AREAIDLIST", cboArea.AreaIDList.Trim(),
                                                "@STOREIDLIST", strFromStoreIDList,
                                                "@USERNAME", SystemConfig.objSessionUser.UserName,
                                                "@ISINPUTCHANGEORDERTYPE", intType,
                                                "@TOSTOREID", cboToStore.StoreID
                                                };
            dtbSearchTemp = objPLCInputChangeOrder.GetEvictionIMEI(objKeywords);
            if (dtbSearchTemp != null)
            {
                dtbSearch = dtbSearchTemp.Copy();
            }
            //if (dtbSearchTemp.Rows.Count > 0)
            //{
            //    var q = from n in dtbSearchTemp.AsEnumerable().Where(p => !string.IsNullOrWhiteSpace(p["CHANGEDATE"].ToString()))
            //            group n by n["IMEI"] into g
            //            select g.OrderByDescending(t =>
            //            DateTime.ParseExact(t["CHANGEDATE"].ToString().Trim(), "dd'/'MM'/'yyyy HH':'mm", CultureInfo.InvariantCulture)
            //                                                ).FirstOrDefault();
            //    if (q.Any())
            //        dtbSearch.Merge(q.CopyToDataTable());

            //    var w = from n in dtbSearchTemp.AsEnumerable().Where(p => string.IsNullOrWhiteSpace(p["IMEI"].ToString()))
            //            select n;

            //    if (w.Any())
            //        dtbSearch.Merge(w.CopyToDataTable());
            //    if (dtbSearch != null && dtbSearch.Rows.Count ==0)
            //    {
            //        dtbSearch = dtbSearchTemp.Copy();
            //    }
            //}
            //else
            //    dtbSearch = dtbSearchTemp.Copy();

            else
            {
                MessageBox.Show(this, "Lỗi lấy dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //Lấy tồn kho theo kho

            //Lấy hàng phụ kiện - Tồn kho theo kho 
            //if (!GetStoreInStockNoIMEI(strMainGroupIDList, strSubGroupIDList, strBrandIDList))
            //{
            //    MessageBox.Show(this, "Lỗi lấy hàng phụ kiện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            //Insert vào lưới 
            //List<string> lstFromStore = new List<string>(cboFromStore.StoreIDList.Replace("><", ", ").Replace("<", "").Replace(">", "").Split(','));
            //var listInsert = from a in dtbStoreInStockNoIMEI.AsEnumerable().Where(p => p["ISREQUESTIMEI"].ToString() == "0")
            //                 join b in lstFromStore
            //                 on a["STOREID"].ToString().Trim() equals b.ToString().Trim()
            //                 select a;

            //if (listInsert.Any())
            //{
            //    foreach (var row in listInsert)
            //    {
            //        string strStoreName = string.Empty;
            //        DataRow[] drr = dtbStoreCache.Select("STOREID = '" + row["STOREID"].ToString().Trim() + "'");
            //        if (drr != null && drr.Length > 0)
            //            strStoreName = drr[0]["STOREID"].ToString().Trim() + " - " + drr[0]["STORENAME"].ToString().Trim();

            //        dtbSearch.Rows.Add(1, Convert.ToInt32(row["STOREID"].ToString().Trim()),
            //            strStoreName, Convert.ToString(row["PRODUCTID"].ToString().Trim()), Convert.ToString(row["PRODUCTNAME"].ToString().Trim()),
            //            "", "", Convert.ToDecimal(row["QUANTITY"].ToString()),
            //            "", "", "", "", Convert.ToString(row["INSTOCKSTATUSNAME"].ToString().Trim()));
            //    }
            //}

            CreateBandView_Eviction(dtbSearch);
            DataTable dtbStore = dtbSearch.AsDataView().ToTable(true, "StoreID");
            DataTable dtbProduct = dtbSearch.AsDataView().ToTable(true, "ProductID");
            GetStoreInStockWithIMEI(dtbStore, dtbProduct);
            intToStoreID = cboToStore.StoreID;
            strToStoreName = cboToStore.StoreName.Trim();
        }

        private bool GetStoreInStockNoIMEI(string strMainGroupIDList, string strSubGroupIDList, string strBrandIDList)
        {
            string strProductIDList = string.Empty;
            string strStoreIDList = "<" + cboToStore.StoreID.ToString() + ">";
            string strInStockStatusID = string.Empty;

            strStoreIDList += cboFromStore.StoreIDList;
            strProductIDList = cboProductList.ProductIDList;
            strInStockStatusID = cboProductStatus.ColumnIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");

            object[] objKeywords = new object[] { "@PRODUCTIDLIST", strProductIDList.ToString(),
                                                "@MAINGROUPIDLIST", strMainGroupIDList,
                                                "@SUBGROUPIDLIST", strSubGroupIDList,
                                                "@BRANDIDLIST", strBrandIDList,
                                                "@AREAIDLIST", cboArea.AreaIDList,
                                                "@STOREIDLIST", strStoreIDList,
                                                "@ISCHECKREALINPUT", 1,
                                                "@InStockStatusIDList", strInStockStatusID,
                                                "@IsExistStock", 1 };

            dtbStoreInStockNoIMEI = objPLCReportDataSource.GetDataSource("RPT_PM_STOCKREPORT_BYSTORE", objKeywords);
            return !Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError;
        }

        private bool ValidateData()
        {
            if (bagrvReport.RowCount >= 1)
            {
                if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    return false;
            }
            if (cboToStore.StoreID < 0)
            {
                MessageBox.Show(this, "Vui lòng kho thu hồi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStore.Focus();
                SendKeys.Send("F4");
                return false;
            }
            if (Globals.CompareDate(dtFromOutputDate.Value, dtToOutputDate.Value) > 1)
            {
                MessageBox.Show(this, "Từ ngày phải nhỏ đến đến ngày!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtFromOutputDate.Focus();
                return false;
            }
            if (Globals.DateDiff(dtToOutputDate.Value, dtFromOutputDate.Value.AddMonths(6), Globals.DateDiffType.DATE) > 0)
            {
                MessageBox.Show("Vui lòng tìm kiếm trong 6 tháng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtFromOutputDate.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Chuyển dữ liệu từ Excel sang DataTable
        /// </summary>
        /// <param name="strFileName">Tên file</param>
        /// <param name="intMaxColCount">Số lượng cột cần lấy</param>
        /// <returns></returns>
        public static DataTable OpenExcel2DataTable(String strFileName, int intMaxColCount)
        {
            C1XLBook objBook = new C1XLBook();
            try
            {
                if (strFileName.Substring(strFileName.Length - 5, 5).Equals(".xlsx"))
                {
                    objBook.Load(strFileName, FileFormat.OpenXml);
                }
                else
                {
                    objBook.Load(strFileName);
                }
                XLSheet objSheet = objBook.Sheets[0];
                DataTable tblResult = new DataTable();
                if (intMaxColCount == 0)
                    intMaxColCount = objSheet.Columns.Count;

                for (int i = 0; i < intMaxColCount; i++)
                {
                    tblResult.Columns.Add("Column" + i.ToString());
                }


                for (int i = 0; i < objSheet.Rows.Count; i++)
                {
                    bool bolIsRowRull = true;
                    DataRow objRow = tblResult.NewRow();
                    for (int j = 0; j < intMaxColCount; j++)
                    {
                        objRow[j] = objSheet[i, j].Value;
                        bolIsRowRull = bolIsRowRull && Convert.IsDBNull(objRow[j]);
                    }
                    //if (bolIsRowRull && i > 1)
                    //    break;
                    //else
                    tblResult.Rows.Add(objRow);
                }
                return tblResult;
            }
            catch (Exception objExce)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi đọc tập tin Excel: " + strFileName, objExce, Globals.ModuleName);
            }

            return null;

        }

        #endregion

        #region Events
        private void cmdAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateData())
                    return;
                EnableControl(false);
                Thread objThread = new Thread(new ThreadStart(Excute));
                objThread.Start();
                GetDataSource();
                frmWaitDialog.Close();
                Thread.Sleep(0);
                objThread.Abort();
                EnableControl(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnExportTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!btnExportTemplate.Enabled)
                    return;

                DataTable dtbExportTemplateIMEI = new DataTable();
                dtbExportTemplateIMEI.Columns.Add("IMEI", typeof(string));
                flexExportTemplate.DataSource = dtbExportTemplateIMEI;
                flexExportTemplate.Cols[0].Width = 150;
                Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexExportTemplate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cmdImportExcel_Click(object sender, EventArgs e)
        {
            if (bagrvReport.RowCount >= 1)
            {
                if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    return;
                dtbSearch.Clear();
            }
            if (!ValidateData())
                return;
            EnableControl(false);
            ImportExcelIMEI();
            EnableControl(true);
        }

        private void cmdCreateStoreChangeCommand_Click(object sender, EventArgs e)
        {
            Boolean bolIsSelected = false;
            for (int i = 0; i < bagrvReport.RowCount; i++)
            {
                if (Convert.ToBoolean(bagrvReport.GetDataRow(i)["IsSelect"]))
                {
                    bolIsSelected = true;
                    break;
                }
            }
            if (!bolIsSelected)
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một mặt hàng cần chuyển!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (cboToStore.StoreID < 0)
            {
                MessageBox.Show(this, "Vui lòng kho thu hồi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStore.Focus();
                return;
            }



            //int intToStoreID = cboToStore.StoreID;
            //string strToStoreName = cboToStore.StoreName;
            //Lấy danh sách ToStore
            InitAutoStoreChangeTable();

            #region bolIsAutoCreateOrder
            frmSelectInfo frm = new frmSelectInfo();
            frm.STORECHANGECOMMANDTYPEID = intStoreChangeCommandTypeID;
            frm.ShowDialog();
            if (frm.IsAccept)
            {
                ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = frm.StoreChangeOrderType;
                objStoreChangeType = new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo((int)objStoreChangeOrderType.StoreChangeTypeID);
                List<int> listConsignMentType = GetConsignmentType(frm.StoreChangeOrderType.StoreChangeOrderTypeID);
                DataTable dtbDataSource = dtbSearch.Clone();
                //Lấy listKhôngChọn 
                var listExcept = dtbSearch.Copy().AsEnumerable()
                    .Where(p => Convert.ToInt32(p["ISSELECT"].ToString()) == 0).ToList();

                //Kiểm tra 
                var list = dtbSearch.Copy().AsEnumerable().Where(p => Convert.ToInt32(p["ISSELECT"].ToString()) == 1).ToList();

                if (list != null && list.Count() > 0)
                {
                    foreach (var item in list)
                    {
                        if (CheckInputCreateOrder(objStoreChangeType, Convert.ToInt32(item["STOREID"].ToString().Trim()), intToStoreID))
                            dtbDataSource.ImportRow(item);
                        else
                            listExcept.Add(item);
                    }
                }
                DataTable dtbExcept = new DataTable();
                if (listExcept.Any())
                    dtbExcept = listExcept.CopyToDataTable();
                else
                    dtbExcept = dtbSearch.Clone();
                DataTable TblListIMEI = new DataTable();
                TblListIMEI.Columns.Add("FROMSTOREID", typeof(string));
                TblListIMEI.Columns.Add("TOSTOREID", typeof(string));
                TblListIMEI.Columns.Add("PRODUCTID", typeof(string));
                TblListIMEI.Columns.Add("IMEI", typeof(string));
                TblListIMEI.Columns.Add("NOTEERROR", typeof(string));


                DataTable dtbDataSourceTemp = dtbAutoStoreChange.Clone();

                var rlsProductFull = dtbSearch.Copy().AsEnumerable().Where(r => Convert.ToInt32(r["ISSELECT"].ToString()) == 1).ToList();
                var dtbFull = dtbSearch.Copy().AsEnumerable().Where(r => Convert.ToInt32(r["ISSELECT"].ToString()) == 1).CopyToDataTable();
                List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> listObjStoreChangeContext = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext>();
                if (dtbFull != null && dtbFull.Rows.Count > 0)
                {
                    listObjStoreChangeContext = ParseToList(dtbFull);
                    string storeChangeCommandID = objPLCStoreChangeCommand.GetStoreChangeCommandNewID(SystemConfig.intDefaultStoreID);
                    listObjStoreChangeContext.ForEach(item =>
                    {
                        item.StoreChangeCommandID = storeChangeCommandID;
                        item.StoreChangeCommandType = intStoreChangeCommandTypeID;
                        item.StoreChangeTypeID = objStoreChangeType.StoreChangeTypeID;
                        item.StoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                        item.IsValidInput = CheckInputCreateOrder(objStoreChangeType, item.FromStore, item.ToStore);

                        item.Detail.ToList().ForEach(f =>
                        {
                            f.IsValidConsignmentType = listConsignMentType.Any(g => dtbProductCache.Rows.Find(f.ProductID) != null
                             && g == Convert.ToInt32(dtbProductCache.Rows.Find(f.ProductID)["CONSIGNMENTTYPE"]));
                        });
                    });

                    DataTable dtbTemp = rlsProductFull.CopyToDataTable();
                    dtbTemp.Columns.Remove("IMEI");
                    var rlsProductID = (from x in dtbTemp.AsEnumerable()
                                        select new
                                        {
                                            SELECT = x["ISSELECT"],
                                            PRODUCTID = x["PRODUCTID"],
                                            PRODUCTNAME = x["PRODUCTNAME"],
                                            FROMSTOREID = x["STOREID"],
                                            FROMSTORENAME = x["STORENAME"]
                                        }).Distinct();
                    foreach (var item in rlsProductID)
                    {
                        var lstTemp = rlsProductFull.Where(p => p["STOREID"].ToString().Trim() == item.FROMSTOREID.ToString().Trim()
                                                               && p["PRODUCTID"].ToString().Trim() == item.PRODUCTID.ToString().Trim()).ToList();
                        //Kiểm tra CheckInputCreateOrder 
                        if (!(CheckInputCreateOrder(objStoreChangeType, Convert.ToInt32(item.FROMSTOREID.ToString().Trim()), intToStoreID))
                           || !listConsignMentType.Any(g => dtbProductCache.Rows.Find(item.PRODUCTID) != null && g == Convert.ToInt32(dtbProductCache.Rows.Find(item.PRODUCTID)["CONSIGNMENTTYPE"])))
                        {
                            if (lstTemp != null && lstTemp.Count() > 0)
                            {
                                foreach (var temp in lstTemp)
                                    listExcept.Add(temp);
                            }
                            continue;
                        }
                        //else
                        //{
                        //    if (lstTemp != null && lstTemp.Count() > 0)
                        //    {
                        //        foreach (var temp in lstTemp.Where(p => !string.IsNullOrWhiteSpace(p["IMEI"].ToString().Trim())))
                        //            TblListIMEI.Rows.Add(
                        //                temp["StoreID"].ToString().Trim(), intToStoreID.ToString(),
                        //                temp["ProductID"].ToString().Trim(), temp["IMEI"].ToString().Trim()
                        //                , temp["MACHINEERRORCONTENT"].ToString().Trim())
                        //                 ;
                        //    }
                        //}

                        //DataRow dr = dtbDataSourceTemp.NewRow();
                        //dr["SELECT"] = Convert.ToBoolean(item.SELECT);
                        //dr["PRODUCTID"] = item.PRODUCTID.ToString().Trim();
                        //dr["PRODUCTNAME"] = item.PRODUCTNAME;
                        //dr["FROMSTOREID"] = item.FROMSTOREID;
                        //dr["FROMSTORENAME"] = item.FROMSTORENAME;
                        //dr["FROMSTOREINSTOCK"] = GetQuantity(item.PRODUCTID.ToString(), Convert.ToInt32(item.FROMSTOREID));
                        //dr["TOSTOREID"] = intToStoreID;
                        //dr["TOSTORENAME"] = strToStoreName;
                        //dr["TOSTOREINSTOCK"] = GetQuantity(item.PRODUCTID.ToString(), intToStoreID);
                        ////IMEI đếm dòng, còn quantity lấy sum quantity
                        //dr["QUANTITY"] = rlsProductFull.Where(p => p["PRODUCTID"].ToString().Trim() == item.PRODUCTID.ToString().Trim()
                        //                                        && p["STOREID"].ToString().Trim() == item.FROMSTOREID.ToString().Trim()
                        //                                        && Convert.ToInt32(p["ISSELECT"].ToString()) == 1).ToList().Sum(x => Convert.ToDecimal(x["QUANTITY"]));
                        //dr["QUANTITYINSTOCK"] = dr["FROMSTOREINSTOCK"];
                        //dr["ISALLOWDECIMAL"] = false;
                        //dr["IMEI"] = string.Empty;
                        //dr["ISURGENT"] = false;
                        //dtbDataSourceTemp.Rows.Add(dr);
                    }
                }

                StoreChangeCommand.AutoStoreChange.frmCreateStoreChangeOrder frmCreateStoreChangeOrder = new StoreChangeCommand.AutoStoreChange.frmCreateStoreChangeOrder(listObjStoreChangeContext);
                frmCreateStoreChangeOrder.IsAllowEditNote = false;

                //frmCreateStoreChangeOrder.DataSource = dtbDataSourceTemp;
                //frmCreateStoreChangeOrder.StoreChangeOrderType = objStoreChangeOrderType;
                //frmCreateStoreChangeOrder.StoreChangeCommandTypeID = intStoreChangeCommandTypeID;
                //frmCreateStoreChangeOrder.FromIMEI = true;
                //frmCreateStoreChangeOrder.FromEviction = true;
                //frmCreateStoreChangeOrder.TblListIMEI = TblListIMEI;
                frmCreateStoreChangeOrder.ShowDialog();
                if (frmCreateStoreChangeOrder.IsAccept)
                {
                    dtbSearch.Clear();
                    cmdCreateStoreChangeCommand.Enabled = false;
                    bagrvReport.OptionsBehavior.Editable = false;
                    cmdAdd.Enabled = false;
                    cmdImportExcel.Enabled = false;
                    if (dtbExcept == null || dtbExcept.Rows.Count < 1)
                    {
                        dtbAutoStoreChange = null;
                        bagrvReport.OptionsBehavior.Editable = true;
                        grdReport.DataSource = dtbSearch;
                        cmdCreateStoreChangeCommand.Enabled = false;
                        cmdAdd.Enabled = true;
                        cmdImportExcel.Enabled = true;
                        //cboFromStore.SetValue(-1);
                        //cboToStore.SetValue(-1);
                        cboProductList.ClearProductComboSource();
                    }
                    else
                    {
                        dtbSearch = dtbExcept;
                        dtbSearch.AcceptChanges();
                        bagrvReport.OptionsBehavior.Editable = true;
                        grdReport.DataSource = dtbSearch;
                        cmdCreateStoreChangeCommand.Enabled = bagrvReport.RowCount > 0;
                        cmdAdd.Enabled = true;
                        cmdImportExcel.Enabled = true;
                        // cboToStore.SetValue(-1);
                    }
                }
                else
                {
                    dtbAutoStoreChange.AcceptChanges();
                }
            }
            #endregion


        }

        private List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> ParseToList(DataTable dtb)
        {
            List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> result = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext>();
            if (dtb == null || dtb.Rows.Count == 0)
                return result;
            int toStoreID = cboToStore.StoreID;
            string toStoreName = string.Empty;
            if (dtbStoreCache != null && dtbStoreCache.Rows.Count > 0)
            {
                DataRow toStoreInfo = dtbStoreCache.Rows.Find(toStoreID);
                if (toStoreInfo != null)
                {
                    toStoreName = toStoreID + " - " + toStoreInfo["STORENAME"].GetString();
                }
            }

            List<int> listFromStoreID = dtb.AsEnumerable().Select(d => Convert.ToInt32(d["STOREID"])).Distinct().ToList();
            listFromStoreID.ForEach(fromStoreID =>
            {
                DataRow fromStoreInfo = null;
                if (dtbStoreCache != null && dtbStoreCache.Rows.Count > 0)
                    fromStoreInfo = dtbStoreCache.Rows.Find(fromStoreID);
                string fromStoreName = string.Empty;
                if (fromStoreInfo != null)
                    fromStoreName = fromStoreID + " - " + fromStoreInfo["STORENAME"].GetString();
                List<int> listProductState = dtb.AsEnumerable().Where(d => Convert.ToInt32(d["STOREID"]) == fromStoreID)
                .Select(d => Convert.ToInt32(d["PRODUCTSTATUSID"])).Distinct().ToList();

                listProductState.ForEach(productStatusID =>
                {
                    DataRow productStatusInfo = null;
                    if (dtbViewProductStatus != null && dtbViewProductStatus.Rows.Count > 0)
                        productStatusInfo = dtbViewProductStatus.Rows.Find(productStatusID);
                    DataTable dtb_From_Status = dtb.AsEnumerable().Where(d => Convert.ToInt32(d["STOREID"]) == fromStoreID
                    && Convert.ToInt32(d["PRODUCTSTATUSID"]) == productStatusID).CopyToDataTable();
                    if (dtb_From_Status != null && dtb_From_Status.Rows.Count != 0)
                    {
                        PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext objStoreChangeOrder = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext()
                        {
                            FromStore = fromStoreID,
                            FromStoreName = fromStoreName,
                            ToStore = toStoreID,
                            ToStoreName = toStoreName,
                            ProductState = productStatusID,
                            ImportProductStateID = productStatusID.ToString(),
                            ImportProductStateName = productStatusInfo != null ? productStatusInfo["PRODUCTSTATUSNAME"].GetString() : string.Empty,
                            IsUrgent = false
                        };

                        List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail> details = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail>();
                        dtb_From_Status.AsEnumerable().ToList().ForEach(productInfo =>
                        {
                            PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail detail = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail()
                            {
                                ProductID = productInfo["PRODUCTID"].GetString(),
                                ProductName = productInfo["PRODUCTNAME"].GetString(),
                                FromInStockQuantity = GetQuantity(productInfo["PRODUCTID"].GetString(), fromStoreID, objStoreChangeOrder.ProductState),
                                ToInStockQuantity = GetQuantity(productInfo["PRODUCTID"].GetString(), toStoreID, objStoreChangeOrder.ProductState),
                                IsManualAdd = false,
                                IMEI = productInfo["IMEI"].GetString(),
                                IsSelected = true,
                                IsUrgent = false,
                                AutoQuantity = Convert.ToDecimal(productInfo["QUANTITY"]),
                                Quantity = Convert.ToDecimal(productInfo["QUANTITY"]),
                                ImportQuantity = productInfo["QUANTITY"].GetString(),
                                Note = productInfo["MACHINEERRORCONTENT"].GetString()
                            };
                            details.Add(detail);
                        });
                        objStoreChangeOrder.Detail = details.ToArray();
                        if (objStoreChangeOrder.Detail != null && objStoreChangeOrder.Detail.Any())
                            result.Add(objStoreChangeOrder);
                    }
                });
            });

            return result;
        }


        private void frmAutoStoreChangeDetail_Load(object sender, EventArgs e)
        {
            bolIsPermission = SystemConfig.objSessionUser.IsPermission("PM_STORECHANGECOMMAND_CREATE");
            dtbStoreCache = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            dtbProductCache = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
            dtbStoreMainGroup = Library.AppCore.DataSource.GetDataSource.GetStoreMainGroup().Copy();
            intStoreChangeCommandTypeID = AppConfig.GetIntConfigValue("AUTOSTORECHANGE_STORECHANGECOMMANDTYPEID");
            cmdCreateStoreChangeCommand.Enabled = bolIsPermission;
            if (intStoreChangeCommandTypeID < 0)
            {
                MessageBox.Show(this, "Bạn chưa cấu hình loại lệnh chuyển kho mặc định", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }

            #region Tham số Menu

            if (this.Tag != null && !string.IsNullOrWhiteSpace(this.Tag.ToString().Trim()))
            {
                Hashtable _hashTable = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                string strListInStockStatusID = Convert.ToString(_hashTable["InStockStatusID"]);
                if (!string.IsNullOrWhiteSpace(strListInStockStatusID))
                {
                    string[] strInStockStatusID = strListInStockStatusID.Split(',');
                    listInStockStatusID = new List<string>(strInStockStatusID);
                    if (!listInStockStatusID.Any())
                    {
                        MessageBox.Show(this, "Cấu hình trạng thái sản phẩm ở tham số menu chưa đúng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.Close();
                    }
                    else
                    {
                        bool bolIsWrong = false;
                        foreach (var item in listInStockStatusID)
                        {
                            if (!IsNumber(item))
                            {
                                bolIsWrong = true;
                                break;
                            }
                        }
                        if (bolIsWrong)
                        {
                            MessageBox.Show(this, "Cấu hình trạng thái sản phẩm ở tham số menu chưa đúng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            this.Close();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "Vui lòng cấu hình trạng thái sản phẩm ở tham số menu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }

            #endregion

            if (dtbStore != null && dtbStore.Columns.Count > 0)
            {
                if (dtbStore.Columns[0].ColumnName.ToUpper() == "ISSELECT")
                {
                    dtbStore.Columns.RemoveAt(0);
                }
            }
            cmdCreateStoreChangeCommand.Enabled = false;
            LoadComboBox();
            if (intMainGroupID > 0)
            {
                cboMainGroup.SetValue(intMainGroupID);
                cboMainGroup.Enabled = false;
            }
            //BindData();
            //cmdAdd.Enabled = SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_ADDPRODUCT");
            //    btnHintStock.Click += new EventHandler(btnHintStock_Click);

        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            if (!mnuExportExcel.Enabled)
                return;
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdReport);
        }

        private void mnuDeleteRow_Click(object sender, EventArgs e)
        {
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            cmdCreateStoreChangeCommand.Focus();
            bagrvReport.DeleteSelectedRows();
            dtbSearch.AcceptChanges();
            grdReport.RefreshDataSource();
            bagrvReport.Focus();
            cmdCreateStoreChangeCommand.Enabled = bagrvReport.RowCount > 0 && bolIsPermission ? true : false;
        }

        private void mnuGrid_Opening(object sender, CancelEventArgs e)
        {
            mnuDeleteRow.Enabled = mnuExportExcel.Enabled = mnuGrid.Enabled = mnuItemSelectAll.Enabled
                    = mnuItemUnSelectAll.Enabled = bagrvReport.RowCount < 1 ? false : true;
        }

        private void mnuItemSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in dtbSearch.Rows)
                row["ISSELECT"] = true;
            grdReport.RefreshDataSource();
        }

        private void mnuItemUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in dtbSearch.Rows)
                row["ISSELECT"] = false;
            grdReport.RefreshDataSource();
        }

        private void bagrvReport_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void bagrvReport_ShowingEditor(object sender, CancelEventArgs e)
        {
            try
            {
                GridView view = sender as GridView;
                string strIMEI = view.GetRowCellValue(view.FocusedRowHandle, "IMEI").ToString().Trim();
                if (!string.IsNullOrEmpty(strIMEI))
                {
                    if (view.FocusedColumn.FieldName.ToString().ToLower() == "quantity")
                        e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ComboBoxEvent
        private void cboMainGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboProductList.InitControl(cboMainGroup.MainGroupIDList, string.Empty, string.Empty);
            cboBrand.InitControl(true, cboMainGroup.MainGroupIDList);
            cboSubGroup.InitControl(true, cboMainGroup.MainGroupIDList);
            LoadWarrantyCenterFilter();
        }

        private void cboSubGroup_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(cboMainGroup.MainGroupIDList))
            cboProductList.InitControl(cboMainGroup.MainGroupIDList, cboSubGroup.SubGroupIDList, cboBrand.BrandIDList);
            //TT bảo hành ------------------
            LoadWarrantyCenterFilter();
            //TT bảo hành ------------------
        }

        private void cboBrand_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(cboBrand.BrandIDList))
            cboProductList.InitControl(cboMainGroup.MainGroupIDList, cboSubGroup.SubGroupIDList, cboBrand.BrandIDList);
            //TT bảo hành ------------------
            LoadWarrantyCenterFilter();
            //TT bảo hành ------------------
        }

        private void cboArea_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cboArea.AreaIDList))
                return;
            //Siêu thị --------------------
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilterFromStore = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objStoreFilterFromStore.AreaIDList = cboArea.AreaIDList;
            cboFromStore.InitControl(true, objStoreFilterFromStore);
            //Siêu thị --------------------
        }

        private void cboMachineErrorGroup_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string strMachineErrorGroupIDList = cboMachineErrorGroup.ColumnIDList;
            if (string.IsNullOrEmpty(strMachineErrorGroupIDList))
                cboMachineError.InitControl(true, dtbMachineError, "MACHINEERRORID", "MACHINEERRORNAME", "-- Tất cả --");
            else
            {
                string[] strArrMachineErrorGroupIDList = strMachineErrorGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "").Split(',');
                List<string> listMachineErrorGroup = new List<string>(strArrMachineErrorGroupIDList);
                if (listMachineErrorGroup.Any())
                {
                    DataTable dtbMachineErrorTemp = dtbMachineError.Clone();
                    var lstMachineErrorTemp = (from a in dtbMachineError.AsEnumerable()
                                               join b in listMachineErrorGroup
                                               on a["MACHINEERRORGROUPID"].ToString().Trim() equals b.ToString().Trim()
                                               select a);
                    if (lstMachineErrorTemp.Any())
                        dtbMachineErrorTemp.Merge(lstMachineErrorTemp.CopyToDataTable());
                    cboMachineError.InitControl(true, dtbMachineErrorTemp, "MACHINEERRORID", "MACHINEERRORNAME", "-- Tất cả --");
                }
            }
        }

        private void LoadWarrantyCenterFilter()
        {
            try
            {
                string strMainGroup = cboMainGroup.MainGroupIDList.Trim();
                string strSubGroup = cboSubGroup.SubGroupIDList.Trim();
                string strBrand = cboBrand.BrandIDList.Trim();
                if (string.IsNullOrWhiteSpace(strMainGroup) && string.IsNullOrWhiteSpace(strSubGroup) && string.IsNullOrWhiteSpace(strBrand))
                {
                    DataRow[] drr = dtbWarrantyCenter.Select("IsSelect = 'True'");
                    if (drr != null && drr.Count() > 0)
                    {
                        foreach (DataRow row in drr)
                            row["IsSelect"] = false;
                    }

                    cboWarrantyCenter.InitControl(true, dtbWarrantyCenter, "WARRANTYCENTERID", "WARRANTYCENTERNAME", "-- Tất cả --");
                    //cboWarrantyCenter.InitControl(true, dtbWarrantyCenterFilter.AsDataView()
                    // .ToTable(true, "WARRANTYCENTERID", "WARRANTYCENTERNAME"), "WARRANTYCENTERID", "WARRANTYCENTERNAME", "-- Tất cả --");
                }
                else
                {
                    List<string> listMainGroupID = new List<string>(strMainGroup.Replace("><", ", ").Replace("<", "").Replace(">", "").Split(','));
                    List<string> listSubGroupID = new List<string>(strSubGroup.Replace("><", ", ").Replace("<", "").Replace(">", "").Split(','));
                    List<string> listBrandID = new List<string>(strBrand.Replace("><", ", ").Replace("<", "").Replace(">", "").Split(','));

                    DataTable dtbWarrantyCenterFilterTemp = dtbWarrantyCenterFilter.Copy();
                    if (listMainGroupID.Any() && !string.IsNullOrWhiteSpace(listMainGroupID[0]))
                    {
                        var lstWarrantyFilter = from a in dtbWarrantyCenterFilter.AsEnumerable()
                                                join b in listMainGroupID
                                                on a["MAINGROUPID"].ToString().Trim() equals b.ToString().Trim()
                                                select a;
                        if (lstWarrantyFilter.Any())
                            dtbWarrantyCenterFilterTemp = lstWarrantyFilter.CopyToDataTable();
                        else
                            dtbWarrantyCenterFilterTemp.Clear();
                    }
                    if (listSubGroupID.Any() && !string.IsNullOrWhiteSpace(listSubGroupID[0]))
                    {
                        var lstWarrantyFilter2 = from a in dtbWarrantyCenterFilterTemp.AsEnumerable()
                                                 join b in listSubGroupID
                                                 on a["SUBGROUPID"].ToString().Trim() equals b.ToString().Trim()
                                                 select a;
                        //dtbWarrantyCenterFilterTemp.Clear();
                        if (lstWarrantyFilter2.Any())
                            dtbWarrantyCenterFilterTemp = lstWarrantyFilter2.CopyToDataTable();
                        else
                            dtbWarrantyCenterFilterTemp.Clear();
                    }
                    if (listBrandID.Any() && !string.IsNullOrWhiteSpace(listBrandID[0]))
                    {
                        var lstWarrantyFilter3 = from a in dtbWarrantyCenterFilterTemp.AsEnumerable()
                                                 join b in listBrandID
                                                 on a["BRANDID"].ToString().Trim() equals b.ToString().Trim()
                                                 select a;
                        //dtbWarrantyCenterFilterTemp.Clear();
                        if (lstWarrantyFilter3.Any())
                            // dtbWarrantyCenterFilterTemp.Clear();
                            dtbWarrantyCenterFilterTemp = lstWarrantyFilter3.CopyToDataTable();
                        else
                            dtbWarrantyCenterFilterTemp.Clear();
                    }
                    //Distinc

                    cboWarrantyCenter.InitControl(true, dtbWarrantyCenterFilterTemp.AsDataView()
                        .ToTable(true, "WARRANTYCENTERID", "WARRANTYCENTERNAME"), "WARRANTYCENTERID", "WARRANTYCENTERNAME", "-- Tất cả --");
                    //cboWarrantyCenter.SetValue(-1);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cboToStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (dtbSearch != null && bagrvReport.RowCount > 0)
            {
                if (cboToStore.StoreID != intToStoreID)
                {
                    if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nThay đổi kho thu hồi sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới"
                        , "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilterFromStore = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                        objStoreFilterFromStore.AreaIDList = cboArea.AreaIDList;
                        objStoreFilterFromStore.OtherCondition = "STOREID <> " + cboToStore.StoreID.ToString().Trim();
                        cboFromStore.InitControl(true, objStoreFilterFromStore);
                        cboFromStore.IsReturnAllWhenNotChoose = true;

                        dtbSearch.Clear();
                        bagrvReport.RefreshData();
                    }
                    else
                        cboToStore.SetValue(intToStoreID);
                }
            }
            else
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilterFromStore = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilterFromStore.AreaIDList = cboArea.AreaIDList;
                objStoreFilterFromStore.OtherCondition = "STOREID <> " + cboToStore.StoreID.ToString().Trim();
                cboFromStore.InitControl(true, objStoreFilterFromStore);
                cboFromStore.IsReturnAllWhenNotChoose = true;
            }
        }

        #endregion

        #region Định dạng lưới + Đổ dữ liệu 
        private void CreateBandView_Eviction(DataTable dtbData)
        {
            ClearGrid();
            //bagrvReport.OptionsFilter.AllowFilterEditor;
            bagrvReport.OptionsView.ShowAutoFilterRow = true;
            //bagrvReport.OptionsView.ShowFooter = true;
            bagrvReport.MinBandPanelRowCount = 2;
            CreateGridBand(dtbData, "ISSELECT", typeof(decimal), "Chọn", 40, true);
            bagrvReport.Columns["ISSELECT"].ColumnEdit = repIsOutput;
            CreateGridBand(dtbData, "STORENAME", typeof(string), "Siêu thị", 320, false);
            CreateGridBand(dtbData, "PRODUCTID", typeof(string), "Mã sản phẩm", 120, false);
            bagrvReport.Columns["PRODUCTID"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "PRODUCTID", "Tổng cộng:");
            CreateGridBand(dtbData, "PRODUCTNAME", typeof(string), "Tên sản phẩm", 220, false);
            bagrvReport.Columns["PRODUCTNAME"].Summary.Add(DevExpress.Data.SummaryItemType.Count, "PRODUCTNAME", "{0:N0}");
            CreateGridBand(dtbData, "IMEI", typeof(string), "IMEI", 150, false);
            CreateGridBand(dtbData, "QUANTITY", typeof(decimal), "Số lượng", 50, false);
            bagrvReport.Columns["QUANTITY"].ColumnEdit = reposNumber;
            CreateGridBand(dtbData, "PRODUCTSTATUSNAME", typeof(string), "Trạng thái sản phẩm", 150, false);
            CreateGridBand(dtbData, "INPUTDATE", typeof(string), "Ngày nhập", 140, false);

            //BandGrid

            DevExpress.XtraGrid.Views.BandedGrid.GridBand grdBandInputChangeInfo = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            grdBandInputChangeInfo.Caption = "Thông tin đổi trả";
            grdBandInputChangeInfo.Name = "grdBand_InputChangeInfo";
            grdBandInputChangeInfo.OptionsBand.AllowMove = false;

            CreateGridBand(dtbData, "INPUTCHANGEORDERTYPENAME", typeof(string), "Loại yêu cầu đổi trả", 220, false);
            CreateGridBand(dtbData, "CHANGEDATE", typeof(string), "Ngày nhập đổi trả", 140, false);
            CreateGridBand(dtbData, "MACHINEERRORGROUPNAME", typeof(string), "Nhóm lỗi", 150, false);
            CreateGridBand(dtbData, "MACHINEERRORCONTENT", typeof(string), "Lỗi", 220, false);

            grdBandInputChangeInfo.Children.Add(bagrvReport.Bands["grdBand_INPUTCHANGEORDERTYPENAME"]);
            grdBandInputChangeInfo.Children.Add(bagrvReport.Bands["grdBand_CHANGEDATE"]);
            grdBandInputChangeInfo.Children.Add(bagrvReport.Bands["grdBand_MACHINEERRORGROUPNAME"]);
            grdBandInputChangeInfo.Children.Add(bagrvReport.Bands["grdBand_MACHINEERRORCONTENT"]);

            grdBandInputChangeInfo.Width = 600;
            bagrvReport.Bands.Add(grdBandInputChangeInfo);

            grdReport.DataSource = dtbData;

            cmdCreateStoreChangeCommand.Enabled = bagrvReport.RowCount > 0 && bolIsPermission ? true : false;
            //EnableCommandChange();
        }

        private void CreateGridBand(DataTable dtb, string strColumnName, Type objType, string strCation, int intWidth, bool bolAllowEdit)
        {
            if (!dtb.Columns.Contains(strColumnName))
                dtb.Columns.Add(strColumnName, objType);
            if (!bagrvReport.Columns.Contains(bagrvReport.Columns[strColumnName]))
                CreateColumn(strColumnName, intWidth, objType, bolAllowEdit);
            if (dtb.Columns.Contains(strColumnName))
            {
                DevExpress.XtraGrid.Views.BandedGrid.GridBand grdBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
                grdBand.Caption = strCation;
                grdBand.Name = "grdBand_" + strColumnName;
                grdBand.Width = intWidth;
                grdBand.Columns.Add(bagrvReport.Columns[strColumnName]);
                bagrvReport.Bands.Add(grdBand);
            }
        }

        private void CreateColumn(string ColumnName, int intWidth, Type objType, bool bolAllowEdit)
        {
            DevExpress.XtraGrid.Columns.GridColumn column;
            column = bagrvReport.Columns.AddField(ColumnName);
            column.Caption = ColumnName;
            column.Width = intWidth;
            column.Visible = true;

            column.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, FontStyle.Bold);
            column.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            column.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            column.AppearanceHeader.Options.UseTextOptions = true;
            column.AppearanceHeader.Options.UseFont = true;

            column.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            column.AppearanceCell.Options.UseFont = true;

            column.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            column.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;

            column.OptionsColumn.AllowEdit = bolAllowEdit;
            column.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.Default;

            if (objType == typeof(DateTime))
            {
                column.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                column.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            }
        }

        private void ClearGrid()
        {
            grdReport.DataSource = null;
            bagrvReport.Columns.Clear();
            bagrvReport.Bands.Clear();
        }

        #endregion
    }
}
