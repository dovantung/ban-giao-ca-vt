﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmEvictionIMEI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEvictionIMEI));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.mnuGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemUnSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDeleteRow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.pMnuImport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.pMnuExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.cachedrptInputVoucherReport1 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            this.cachedrptInputVoucherReport2 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboToStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label14 = new System.Windows.Forms.Label();
            this.cboFromStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label13 = new System.Windows.Forms.Label();
            this.cboArea = new Library.AppControl.ComboBoxControl.ComboBoxArea();
            this.label12 = new System.Windows.Forms.Label();
            this.cboMachineError = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label11 = new System.Windows.Forms.Label();
            this.cboInputChangeOrderType = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtFromOutputDate = new System.Windows.Forms.DateTimePicker();
            this.dtToOutputDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.cboMachineErrorGroup = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label7 = new System.Windows.Forms.Label();
            this.cboProductStatus = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label4 = new System.Windows.Forms.Label();
            this.cboWarrantyCenter = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label6 = new System.Windows.Forms.Label();
            this.cboBrand = new Library.AppControl.ComboBoxControl.ComboBoxBrand();
            this.label1 = new System.Windows.Forms.Label();
            this.cboSubGroup = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.label2 = new System.Windows.Forms.Label();
            this.cboProductList = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.cmdImportExcel = new System.Windows.Forms.Button();
            this.btnExportTemplate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cboMainGroup = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cmdCreateStoreChangeCommand = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmdAdd = new System.Windows.Forms.Button();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grdReport = new DevExpress.XtraGrid.GridControl();
            this.bagrvReport = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.reposNumber = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repFormatDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repCheckBox = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repIsOutput = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.flexExportTemplate = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuImport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuExport)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bagrvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFormatDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexExportTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuGrid
            // 
            this.mnuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemSelectAll,
            this.mnuItemUnSelectAll,
            this.mnuDeleteRow,
            this.toolStripSeparator1,
            this.mnuExportExcel});
            this.mnuGrid.Name = "mnuGrid";
            this.mnuGrid.Size = new System.Drawing.Size(151, 98);
            this.mnuGrid.Opening += new System.ComponentModel.CancelEventHandler(this.mnuGrid_Opening);
            // 
            // mnuItemSelectAll
            // 
            this.mnuItemSelectAll.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.mnuItemSelectAll.Name = "mnuItemSelectAll";
            this.mnuItemSelectAll.Size = new System.Drawing.Size(150, 22);
            this.mnuItemSelectAll.Text = "Chọn tất cả";
            this.mnuItemSelectAll.Click += new System.EventHandler(this.mnuItemSelectAll_Click);
            // 
            // mnuItemUnSelectAll
            // 
            this.mnuItemUnSelectAll.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.mnuItemUnSelectAll.Name = "mnuItemUnSelectAll";
            this.mnuItemUnSelectAll.Size = new System.Drawing.Size(150, 22);
            this.mnuItemUnSelectAll.Text = "Bỏ chọn tất cả";
            this.mnuItemUnSelectAll.Click += new System.EventHandler(this.mnuItemUnSelectAll_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Size = new System.Drawing.Size(150, 22);
            this.mnuDeleteRow.Text = "Xóa dòng này";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(147, 6);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(150, 22);
            this.mnuExportExcel.Text = "Xuất Excel";
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // pMnuImport
            // 
            this.pMnuImport.Manager = this.barManager1;
            this.pMnuImport.Name = "pMnuImport";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1011, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 517);
            this.barDockControlBottom.Size = new System.Drawing.Size(1011, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1011, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 517);
            // 
            // pMnuExport
            // 
            this.pMnuExport.Manager = this.barManager1;
            this.pMnuExport.Name = "pMnuExport";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.groupControl2);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1010, 516);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cboToStore);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.cboFromStore);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.cboArea);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.cboMachineError);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.cboInputChangeOrderType);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.dtFromOutputDate);
            this.panel2.Controls.Add(this.dtToOutputDate);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cboMachineErrorGroup);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cboProductStatus);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.cboWarrantyCenter);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.cboBrand);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cboSubGroup);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.cboProductList);
            this.panel2.Controls.Add(this.cmdImportExcel);
            this.panel2.Controls.Add(this.btnExportTemplate);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.cboMainGroup);
            this.panel2.Controls.Add(this.cmdCreateStoreChangeCommand);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.cmdAdd);
            this.panel2.Location = new System.Drawing.Point(4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1001, 144);
            this.panel2.TabIndex = 0;
            // 
            // cboToStore
            // 
            this.cboToStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStore.Location = new System.Drawing.Point(88, 111);
            this.cboToStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStore.Name = "cboToStore";
            this.cboToStore.Size = new System.Drawing.Size(220, 24);
            this.cboToStore.TabIndex = 25;
            this.cboToStore.SelectionChangeCommitted += new System.EventHandler(this.cboToStore_SelectionChangeCommitted);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 16);
            this.label14.TabIndex = 24;
            this.label14.Text = "Kho thu hồi:";
            // 
            // cboFromStore
            // 
            this.cboFromStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStore.Location = new System.Drawing.Point(413, 111);
            this.cboFromStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStore.Name = "cboFromStore";
            this.cboFromStore.Size = new System.Drawing.Size(220, 24);
            this.cboFromStore.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(326, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 16);
            this.label13.TabIndex = 26;
            this.label13.Text = "Siêu thị:";
            // 
            // cboArea
            // 
            this.cboArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboArea.Location = new System.Drawing.Point(88, 83);
            this.cboArea.Margin = new System.Windows.Forms.Padding(0);
            this.cboArea.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboArea.Name = "cboArea";
            this.cboArea.Size = new System.Drawing.Size(220, 24);
            this.cboArea.TabIndex = 19;
            this.cboArea.SelectionChangeCommitted += new System.EventHandler(this.cboArea_SelectionChangeCommitted);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 88);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 16);
            this.label12.TabIndex = 18;
            this.label12.Text = "Khu vực:";
            // 
            // cboMachineError
            // 
            this.cboMachineError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMachineError.Location = new System.Drawing.Point(413, 57);
            this.cboMachineError.Margin = new System.Windows.Forms.Padding(0);
            this.cboMachineError.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMachineError.Name = "cboMachineError";
            this.cboMachineError.Size = new System.Drawing.Size(220, 24);
            this.cboMachineError.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(327, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 16);
            this.label11.TabIndex = 14;
            this.label11.Text = "Lỗi:";
            // 
            // cboInputChangeOrderType
            // 
            this.cboInputChangeOrderType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputChangeOrderType.Location = new System.Drawing.Point(740, 54);
            this.cboInputChangeOrderType.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputChangeOrderType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputChangeOrderType.Name = "cboInputChangeOrderType";
            this.cboInputChangeOrderType.Size = new System.Drawing.Size(220, 24);
            this.cboInputChangeOrderType.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(642, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 16);
            this.label10.TabIndex = 16;
            this.label10.Text = "Loại YC đổi trả:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(326, 87);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 16);
            this.label9.TabIndex = 20;
            this.label9.Text = "Ngày:";
            // 
            // dtFromOutputDate
            // 
            this.dtFromOutputDate.CustomFormat = "dd/MM/yyyy";
            this.dtFromOutputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFromOutputDate.Location = new System.Drawing.Point(413, 85);
            this.dtFromOutputDate.Name = "dtFromOutputDate";
            this.dtFromOutputDate.Size = new System.Drawing.Size(102, 22);
            this.dtFromOutputDate.TabIndex = 21;
            this.dtFromOutputDate.Value = new System.DateTime(2017, 12, 31, 0, 0, 0, 0);
            // 
            // dtToOutputDate
            // 
            this.dtToOutputDate.CustomFormat = "dd/MM/yyyy";
            this.dtToOutputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtToOutputDate.Location = new System.Drawing.Point(524, 85);
            this.dtToOutputDate.Name = "dtToOutputDate";
            this.dtToOutputDate.Size = new System.Drawing.Size(109, 22);
            this.dtToOutputDate.TabIndex = 23;
            this.dtToOutputDate.Value = new System.DateTime(2017, 12, 31, 0, 0, 0, 0);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(514, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 16);
            this.label8.TabIndex = 22;
            this.label8.Text = "-";
            // 
            // cboMachineErrorGroup
            // 
            this.cboMachineErrorGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMachineErrorGroup.Location = new System.Drawing.Point(88, 57);
            this.cboMachineErrorGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMachineErrorGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMachineErrorGroup.Name = "cboMachineErrorGroup";
            this.cboMachineErrorGroup.Size = new System.Drawing.Size(220, 24);
            this.cboMachineErrorGroup.TabIndex = 13;
            this.cboMachineErrorGroup.SelectionChangeCommitted += new System.EventHandler(this.cboMachineErrorGroup_SelectionChangeCommitted);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Nhóm lỗi:";
            // 
            // cboProductStatus
            // 
            this.cboProductStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductStatus.Location = new System.Drawing.Point(413, 30);
            this.cboProductStatus.Margin = new System.Windows.Forms.Padding(0);
            this.cboProductStatus.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboProductStatus.Name = "cboProductStatus";
            this.cboProductStatus.Size = new System.Drawing.Size(220, 24);
            this.cboProductStatus.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(323, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Trạng thái SP:";
            // 
            // cboWarrantyCenter
            // 
            this.cboWarrantyCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboWarrantyCenter.Location = new System.Drawing.Point(740, 28);
            this.cboWarrantyCenter.Margin = new System.Windows.Forms.Padding(0);
            this.cboWarrantyCenter.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboWarrantyCenter.Name = "cboWarrantyCenter";
            this.cboWarrantyCenter.Size = new System.Drawing.Size(220, 24);
            this.cboWarrantyCenter.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(642, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "TT bảo hành:";
            // 
            // cboBrand
            // 
            this.cboBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBrand.Location = new System.Drawing.Point(740, 2);
            this.cboBrand.Margin = new System.Windows.Forms.Padding(0);
            this.cboBrand.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBrand.Name = "cboBrand";
            this.cboBrand.Size = new System.Drawing.Size(220, 24);
            this.cboBrand.TabIndex = 5;
            this.cboBrand.SelectionChangeCommitted += new System.EventHandler(this.cboBrand_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(642, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nhà sản xuất:";
            // 
            // cboSubGroup
            // 
            this.cboSubGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroup.Location = new System.Drawing.Point(413, 4);
            this.cboSubGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroup.Name = "cboSubGroup";
            this.cboSubGroup.Size = new System.Drawing.Size(220, 24);
            this.cboSubGroup.TabIndex = 3;
            this.cboSubGroup.SelectionChangeCommitted += new System.EventHandler(this.cboSubGroup_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(326, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nhóm hàng:";
            // 
            // cboProductList
            // 
            this.cboProductList.BackColor = System.Drawing.Color.Transparent;
            this.cboProductList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductList.Location = new System.Drawing.Point(88, 31);
            this.cboProductList.Margin = new System.Windows.Forms.Padding(4);
            this.cboProductList.Name = "cboProductList";
            this.cboProductList.ProductIDList = "";
            this.cboProductList.Size = new System.Drawing.Size(220, 23);
            this.cboProductList.TabIndex = 7;
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdImportExcel.Image")));
            this.cmdImportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdImportExcel.Location = new System.Drawing.Point(738, 110);
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Size = new System.Drawing.Size(106, 25);
            this.cmdImportExcel.TabIndex = 30;
            this.cmdImportExcel.Text = "Nhập Excel";
            this.cmdImportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdImportExcel.UseVisualStyleBackColor = true;
            this.cmdImportExcel.Click += new System.EventHandler(this.cmdImportExcel_Click);
            // 
            // btnExportTemplate
            // 
            this.btnExportTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnExportTemplate.Image")));
            this.btnExportTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportTemplate.Location = new System.Drawing.Point(739, 82);
            this.btnExportTemplate.Name = "btnExportTemplate";
            this.btnExportTemplate.Size = new System.Drawing.Size(158, 25);
            this.btnExportTemplate.TabIndex = 29;
            this.btnExportTemplate.Text = "    Xuất TT mẫu";
            this.btnExportTemplate.UseVisualStyleBackColor = true;
            this.btnExportTemplate.Click += new System.EventHandler(this.btnExportTemplate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ngành hàng:";
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroup.Location = new System.Drawing.Point(88, 4);
            this.cboMainGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(220, 24);
            this.cboMainGroup.TabIndex = 1;
            this.cboMainGroup.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroupID_SelectionChangeCommitted);
            // 
            // cmdCreateStoreChangeCommand
            // 
            this.cmdCreateStoreChangeCommand.Enabled = false;
            this.cmdCreateStoreChangeCommand.Image = ((System.Drawing.Image)(resources.GetObject("cmdCreateStoreChangeCommand.Image")));
            this.cmdCreateStoreChangeCommand.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdCreateStoreChangeCommand.Location = new System.Drawing.Point(846, 110);
            this.cmdCreateStoreChangeCommand.Name = "cmdCreateStoreChangeCommand";
            this.cmdCreateStoreChangeCommand.Size = new System.Drawing.Size(149, 25);
            this.cmdCreateStoreChangeCommand.TabIndex = 31;
            this.cmdCreateStoreChangeCommand.Text = "Tạo YC chuyển kho";
            this.cmdCreateStoreChangeCommand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdCreateStoreChangeCommand.UseVisualStyleBackColor = true;
            this.cmdCreateStoreChangeCommand.Click += new System.EventHandler(this.cmdCreateStoreChangeCommand_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Sản phẩm:";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAdd.Location = new System.Drawing.Point(644, 110);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(88, 25);
            this.cmdAdd.TabIndex = 28;
            this.cmdAdd.Text = "Tìm kiếm";
            this.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdAdd.UseVisualStyleBackColor = true;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl2.Controls.Add(this.grdReport);
            this.groupControl2.Controls.Add(this.flexExportTemplate);
            this.groupControl2.Location = new System.Drawing.Point(2, 153);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1005, 360);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Danh sách:";
            // 
            // grdReport
            // 
            this.grdReport.ContextMenuStrip = this.mnuGrid;
            this.grdReport.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.grdReport.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grdReport.Location = new System.Drawing.Point(2, 22);
            this.grdReport.MainView = this.bagrvReport;
            this.grdReport.Name = "grdReport";
            this.grdReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.reposNumber,
            this.repFormatDate,
            this.repCheckBox,
            this.repIsOutput});
            this.grdReport.Size = new System.Drawing.Size(1001, 336);
            this.grdReport.TabIndex = 36;
            this.grdReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bagrvReport});
            // 
            // bagrvReport
            // 
            this.bagrvReport.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bagrvReport.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.bagrvReport.Appearance.BandPanel.Options.UseFont = true;
            this.bagrvReport.Appearance.BandPanel.Options.UseForeColor = true;
            this.bagrvReport.Appearance.BandPanel.Options.UseTextOptions = true;
            this.bagrvReport.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bagrvReport.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bagrvReport.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bagrvReport.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bagrvReport.Appearance.GroupFooter.Options.UseFont = true;
            this.bagrvReport.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bagrvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bagrvReport.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bagrvReport.Appearance.GroupRow.Options.UseFont = true;
            this.bagrvReport.Appearance.GroupRow.Options.UseForeColor = true;
            this.bagrvReport.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bagrvReport.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bagrvReport.Appearance.HeaderPanel.Options.UseFont = true;
            this.bagrvReport.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bagrvReport.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.bagrvReport.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bagrvReport.Appearance.Row.Options.UseTextOptions = true;
            this.bagrvReport.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bagrvReport.GridControl = this.grdReport;
            this.bagrvReport.GroupFormat = "[#image]{1} {2}";
            this.bagrvReport.Name = "bagrvReport";
            this.bagrvReport.OptionsBehavior.AutoExpandAllGroups = true;
            this.bagrvReport.OptionsNavigation.UseTabKey = false;
            this.bagrvReport.OptionsPrint.AutoWidth = false;
            this.bagrvReport.OptionsPrint.PrintHeader = false;
            this.bagrvReport.OptionsPrint.UsePrintStyles = true;
            this.bagrvReport.OptionsView.ColumnAutoWidth = false;
            this.bagrvReport.OptionsView.ShowColumnHeaders = false;
            this.bagrvReport.OptionsView.ShowFooter = true;
            this.bagrvReport.OptionsView.ShowGroupPanel = false;
            this.bagrvReport.OptionsView.ShowIndicator = false;
            // 
            // reposNumber
            // 
            this.reposNumber.AutoHeight = false;
            this.reposNumber.Mask.EditMask = "N0";
            this.reposNumber.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.reposNumber.Mask.UseMaskAsDisplayFormat = true;
            this.reposNumber.Name = "reposNumber";
            this.reposNumber.NullText = "0";
            // 
            // repFormatDate
            // 
            this.repFormatDate.AutoHeight = false;
            this.repFormatDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.repFormatDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repFormatDate.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repFormatDate.Name = "repFormatDate";
            // 
            // repCheckBox
            // 
            this.repCheckBox.AutoHeight = false;
            this.repCheckBox.Name = "repCheckBox";
            this.repCheckBox.ValueChecked = ((long)(1));
            this.repCheckBox.ValueUnchecked = ((long)(0));
            // 
            // repIsOutput
            // 
            this.repIsOutput.AutoHeight = false;
            this.repIsOutput.Name = "repIsOutput";
            this.repIsOutput.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repIsOutput.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // flexExportTemplate
            // 
            this.flexExportTemplate.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.FixedOnly;
            this.flexExportTemplate.AutoClipboard = true;
            this.flexExportTemplate.ColumnInfo = "9,0,0,0,0,105,Columns:";
            this.flexExportTemplate.Location = new System.Drawing.Point(2, 25);
            this.flexExportTemplate.Name = "flexExportTemplate";
            this.flexExportTemplate.Rows.Count = 1;
            this.flexExportTemplate.Rows.DefaultSize = 21;
            this.flexExportTemplate.Size = new System.Drawing.Size(109, 67);
            this.flexExportTemplate.StyleInfo = resources.GetString("flexExportTemplate.StyleInfo");
            this.flexExportTemplate.TabIndex = 1;
            this.flexExportTemplate.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            // 
            // frmEvictionIMEI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1011, 517);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(16, 556);
            this.Name = "frmEvictionIMEI";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thu hồi sản phẩm lỗi, đổi trả";
            this.Load += new System.EventHandler(this.frmAutoStoreChangeDetail_Load);
            this.mnuGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pMnuImport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuExport)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bagrvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFormatDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexExportTemplate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport1;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport2;
        private System.Windows.Forms.ContextMenuStrip mnuGrid;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuDeleteRow;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSelectAll;
        private System.Windows.Forms.ToolStripMenuItem mnuItemUnSelectAll;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.PopupMenu pMnuExport;
        private DevExpress.XtraBars.PopupMenu pMnuImport;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private C1.Win.C1FlexGrid.C1FlexGrid flexExportTemplate;
        private System.Windows.Forms.Panel panel2;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStore;
        private System.Windows.Forms.Label label14;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStore;
        private System.Windows.Forms.Label label13;
        private Library.AppControl.ComboBoxControl.ComboBoxArea cboArea;
        private System.Windows.Forms.Label label12;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboMachineError;
        private System.Windows.Forms.Label label11;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboInputChangeOrderType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtFromOutputDate;
        private System.Windows.Forms.DateTimePicker dtToOutputDate;
        private System.Windows.Forms.Label label8;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboMachineErrorGroup;
        private System.Windows.Forms.Label label7;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboProductStatus;
        private System.Windows.Forms.Label label4;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboWarrantyCenter;
        private System.Windows.Forms.Label label6;
        private Library.AppControl.ComboBoxControl.ComboBoxBrand cboBrand;
        private System.Windows.Forms.Label label1;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroup;
        private System.Windows.Forms.Label label2;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProductList;
        private System.Windows.Forms.Button cmdImportExcel;
        private System.Windows.Forms.Button btnExportTemplate;
        private System.Windows.Forms.Label label5;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroup;
        private System.Windows.Forms.Button cmdCreateStoreChangeCommand;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button cmdAdd;
        private DevExpress.XtraGrid.GridControl grdReport;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bagrvReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit reposNumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repFormatDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repCheckBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repIsOutput;
    }
}