﻿using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmStoreChangeImportExcelDetailImei : Form
    {
        public List<frmStoreChangeImportExcelImei.ProductImei> ProductList { get; set; }
        public frmStoreChangeImportExcelDetailImei()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }

        private void btnViewImeiValid_Click(object sender, EventArgs e)
        {
            chkViewAll.Checked = false;
            if (ProductList != null)
                grdData.DataSource = ProductList.Where(x => x.IsError == false).ToArray();
        }

        private void btnViewIncorrect_Click(object sender, EventArgs e)
        {
            chkViewAll.Checked = false;
            if (ProductList != null)
                grdData.DataSource = ProductList.Where(x => x.IsError == true).ToArray();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmStoreChangeImportExcelDetailImei_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            if (ProductList != null)
            {
                btnViewImeiValid_Click(null, null);
                txtTotal.Text = ProductList.Count.ToString("#,##0");
                txtCorrect.Text = ProductList.Where(x => x.IsError == false).Count().ToString("#,##0");
                txtIncorrect.Text = ProductList.Where(x => x.IsError == true).Count().ToString("#,##0");
            }
        }

        private void grdViewData_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView view = sender as GridView;
            if (!view.IsFilterRow(e.RowHandle))
            {
                if (e.RowHandle == view.FocusedRowHandle)
                {
                    frmStoreChangeImportExcelImei.ProductImei item = view.GetRow(e.RowHandle) as frmStoreChangeImportExcelImei.ProductImei;
                    if (item != null)
                        if (item.IsError)
                            e.Appearance.BackColor = Color.Pink;
                }
            }
        }

        private void grdViewData_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            frmStoreChangeImportExcelImei.ProductImei item = grdViewData.GetRow(e.RowHandle) as frmStoreChangeImportExcelImei.ProductImei;
            if (item != null)
                if (item.IsError)
                    e.Appearance.BackColor = Color.Pink;
        }

        private void frmStoreChangeImportExcelDetailImei_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void chkViewAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkViewAll.Checked)
            {
                if (ProductList != null)
                    grdData.DataSource = ProductList.ToArray();
            }
            else
                btnViewImeiValid_Click(null, null);
            
        }
    }
}
