﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmStoreChangeManager : Form
    {

        private string strPermission_MultiPrint = "PromotionProgram_Copy";
        private string strPermission_IsTransferedUpdate = "PM_StoreChange_IsTransferedUpdate";//Quản lý phiếu chuyển kho – Cập nhật trạng thái chuyển kho
        private string strPermission_IsReceivedTransfer = "PM_STORECHANGE_RECEIVEDTRANSFER";//Quản lý phiếu chuyển kho – Cập nhận hàng để vận chuyển
        private string strPermission_IsReceive = "PM_StoreChange_CheckIsReceive"; //Quản lý phiếu chuyển kho - Quyền nhập kho
        private int intStoreChange3_A4 = -1;
        private int intStoreChange3_A5 = -1;
        private int intStoreChange3_A6 = -1;
        private int intStoreChangeReportID = -1;
        private int intStoreChangeReportID2 = -1;
        private int intInternalOutput_TransferID = -1;
        private int intStoreChangeCommandID = -1;
        private bool bolIsStoreChangeDate = true;
        private int intCheckIsReceive = Library.AppCore.AppConfig.GetIntConfigValue("PM_STORECHANGE_CHECKISRECEIVE");
        private int intPrintStoreChangeHandovers = -1;

        private Hashtable htb = null;
        private int intOnlyCertifyFinger = 0;

        public frmStoreChangeManager()
        {
            InitializeComponent();
        }

        private void frmStoreChangeManager_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            //Lấy tham số form
            htb = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString());

            //if (!Convert.IsDBNull(htb["PrintStoreChangeHandovers"]))
            //    PrintStoreChangeHandovers = Convert.ToInt32(htb["PrintStoreChangeHandovers"]);
            try
            {
                var configPrintStoreChangeHandovers = Library.AppCore.AppConfig.GetConfigValue("RPT_STORECHANGE_HANDOVER");
                if (configPrintStoreChangeHandovers != null)
                {
                    string strPrintStoreChangeHandovers = configPrintStoreChangeHandovers.ToString().Trim();
                    if (!string.IsNullOrEmpty(strPrintStoreChangeHandovers))
                    {
                        intPrintStoreChangeHandovers = Convert.ToInt32(strPrintStoreChangeHandovers);
                        mnuItemPrintStoreChangeHandOvers.Visible = intPrintStoreChangeHandovers > 0;
                    }
                }
                else
                {
                    mnuItemPrintStoreChangeHandOvers.Visible = false;
                }
            }
            catch
            {
                MessageBox.Show(this, "Lỗi khai báo cấu hình ứng dụng biên bản bàn giao", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            try
            {

                string strStoreChangeReportID = AppConfig.GetStringConfigValue("PM_STORECHANGE_REPORTID");
                string strStoreChangeReportID2 = AppConfig.GetStringConfigValue("PM_STORECHANGE_REPORTID2");
                if (strStoreChangeReportID.Length > 0)
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(strStoreChangeReportID);
                    Hashtable hstbParam2 = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(strStoreChangeReportID2);
                    if (!Convert.IsDBNull(hstbParam["StoreChange3_A4"]))
                    {
                        intStoreChange3_A4 = Convert.ToInt32(hstbParam["StoreChange3_A4"]);
                        mnuItemPrintStoreAddressA4.Visible = intStoreChange3_A4 > 0;
                    }
                    if (!Convert.IsDBNull(hstbParam["StoreChange3_A5"]))
                    {
                        intStoreChange3_A5 = Convert.ToInt32(hstbParam["StoreChange3_A5"]);
                        mnuItemPrintStoreAddressA5.Visible = intStoreChange3_A5 > 0;
                    }
                    if (!Convert.IsDBNull(hstbParam["StoreChange3_A6"]))
                    {
                        intStoreChange3_A6 = Convert.ToInt32(hstbParam["StoreChange3_A6"]);
                        mnuItemPrintStoreAddressA6.Visible = intStoreChange3_A6 > 0;
                    }
                    if (!Convert.IsDBNull(hstbParam["StoreChangeReportID"]))
                    {
                        intStoreChangeReportID = Convert.ToInt32(hstbParam["StoreChangeReportID"]);
                        mnuItemPrintReport.Visible = intStoreChangeReportID > 0;
                    }
                    if (!Convert.IsDBNull(hstbParam2["StoreChangeReportID"]))
                    {
                        intStoreChangeReportID2 = Convert.ToInt32(hstbParam2["StoreChangeReportID"]);
                        mnuItemPrintReport.Visible = intStoreChangeReportID2 > 0;
                    }
                    if (!Convert.IsDBNull(hstbParam["storeChangeCommandID"]))
                    {
                        intStoreChangeCommandID = Convert.ToInt32(hstbParam["storeChangeCommandID"]);
                        mnuItemPrintStoreChangeCommand.Visible = intStoreChangeCommandID > 0;
                    }
                    if (!Convert.IsDBNull(hstbParam["internalOutput_TransferID"]))
                    {
                        intInternalOutput_TransferID = Convert.ToInt32(hstbParam["internalOutput_TransferID"]);
                        mnuItemPrintInternalOutput_Transfer.Visible = intInternalOutput_TransferID > 0;
                    }
                    if (!Convert.IsDBNull(htb["ISONLYCERTIFYFINGER"]))
                        intOnlyCertifyFinger = Convert.ToInt32(htb["ISONLYCERTIFYFINGER"]);
                }
            }
            catch
            {
                MessageBox.Show(this, "Lỗi đọc tham số loại báo cáo chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Nạp giá trị cho combobox
        /// </summary>
        private void LoadComboBox()
        {

            try
            {
                //Trạng thái
                DataTable dtbStatus = new DataTable();
                dtbStatus.Columns.Add("StatusID", typeof(int));
                dtbStatus.Columns.Add("StatusName", typeof(string));
                DataRow row1 = dtbStatus.NewRow();
                row1["StatusID"] = 0;
                row1["StatusName"] = "Chưa giao hàng";
                dtbStatus.Rows.Add(row1);
                DataRow row2 = dtbStatus.NewRow();
                row2["StatusID"] = 1;
                row2["StatusName"] = "Đã giao hàng - Chưa vận chuyển";
                dtbStatus.Rows.Add(row2);
                DataRow row3 = dtbStatus.NewRow();
                row3["StatusID"] = 2;
                row3["StatusName"] = "Đã vận chuyển - Chưa ký nhận nhập";
                dtbStatus.Rows.Add(row3);
                DataRow row4 = dtbStatus.NewRow();
                row4["StatusID"] = 3;
                row4["StatusName"] = "Đã ký nhận nhập - Chưa xác nhận thực nhập";
                dtbStatus.Rows.Add(row4);
                DataRow row5 = dtbStatus.NewRow();
                row5["StatusID"] = 4;
                row5["StatusName"] = "Đã xác nhận thực nhập";
                dtbStatus.Rows.Add(row5);

                cboStatusList.InitControl(true, dtbStatus, "StatusID", "StatusName", "--Chọn trạng thái--");

                // Kho nhập - Kho xuất
                DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore();
                cboFromStoreSearch.InitControl(true);
                cboToStoreSearch.InitControl(true);
                //// Phương tiện vận chuyển
                //DataTable dtbTransportType = Library.AppCore.DataSource.GetDataSource.GetTransportType();
                //Library.AppCore.LoadControls.CheckedComboBoxEditObject.LoadDataFromDataTable(dtbTransportType.Copy(), cboTransportType);

                // Hình thức chuyển
                Library.AppCore.LoadControls.SetDataSource.SetStoreChangeType(this, cboStoreChangeType);

                cboMainGroup.InitControl(true, true);

                //Load giá trị mặc định cho combobox 
                cboStoreChangeType.SelectedIndex = 0;
                cboSearchBy.SelectedIndex = 0;
                // dtpFromStoreChangeDate.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day);
                dtpFromDate.Value = DateTime.Today;
                dtpToDate.Value = DateTime.Today;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Lỗi nạp combobox", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }
        /// <summary>
        /// Định dạng lưới
        /// </summary>
        private void FormatFlex()
        {
            if (flexStoreChange.DataSource == null) return;
            flexStoreChange.Rows.Fixed = 2;

            #region Caption
            for (int i = 0; i < flexStoreChange.Cols.Count; i++)
            {
                flexStoreChange.Cols[i].AllowMerging = true;
                flexStoreChange.Cols[i].Visible = false;
            }

            flexStoreChange[0, "StoreChangeID"] = "Mã phiếu chuyển";
            flexStoreChange[1, "StoreChangeID"] = "Mã phiếu chuyển";
            flexStoreChange.Cols["StoreChangeID"].Visible = true;

            flexStoreChange[0, "DateCreateStoreChange"] = "Ngày chuyển";
            flexStoreChange[1, "DateCreateStoreChange"] = "Ngày chuyển";
            flexStoreChange.Cols["DateCreateStoreChange"].Visible = true;

            flexStoreChange[0, "CaskCode"] = "Mã số thùng";
            flexStoreChange[1, "CaskCode"] = "Mã số thùng";
            flexStoreChange.Cols["CaskCode"].Visible = true;

            flexStoreChange[0, "FromStoreName"] = "Kho xuất";
            flexStoreChange[1, "FromStoreName"] = "Kho xuất";
            flexStoreChange.Cols["FromStoreName"].Visible = true;


            flexStoreChange[0, "ToStoreName"] = "Kho nhập";
            flexStoreChange[1, "ToStoreName"] = "Kho nhập";
            flexStoreChange.Cols["ToStoreName"].Visible = true;

            flexStoreChange[0, "IsStoreChanged"] = "Thông tin xuất kho";
            flexStoreChange[1, "IsStoreChanged"] = "Đã xuất";
            flexStoreChange.Cols["IsStoreChanged"].Visible = true;

            flexStoreChange[0, "StorechangeDate"] = "Thông tin xuất kho";
            flexStoreChange[1, "StorechangeDate"] = "Thời gian xuất";
            flexStoreChange.Cols["StorechangeDate"].Visible = true;

            flexStoreChange[0, "TransferedDate"] = "Thông tin giao hàng";
            flexStoreChange[1, "TransferedDate"] = "Thời gian giao";
            flexStoreChange.Cols["TransferedDate"].Visible = true;
            //flexStoreChange[0, "StoreChangeDate"] = "Ngày chuyển"; //Trước đó Ngày chuyển StoreChangeDate
            //flexStoreChange[1, "StoreChangeDate"] = "Ngày chuyển";//Trước đó Ngày chuyển StoreChangeDate
            flexStoreChange[0, "DateReceive"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "DateReceive"] = "Thời gian nhập";
            flexStoreChange.Cols["DateReceive"].Visible = true;

            flexStoreChange[0, "Content"] = "Nội dung";
            flexStoreChange[1, "Content"] = "Nội dung";
            flexStoreChange.Cols["Content"].Visible = true;

            //flexStoreChange[0, "DaysOfStoreChange"] = "TG đi đường";//TG đi đường trước đó
            //flexStoreChange[1, "DaysOfStoreChange"] = "TG đi đường";//TG đi đường trước đó
            flexStoreChange[0, "TransferTime"] = "TG đi đường";
            flexStoreChange[1, "TransferTime"] = "TG đi đường";
            flexStoreChange.Cols["TransferTime"].Visible = true;

            flexStoreChange[0, "InvoiceID"] = "Số hóa đơn";
            flexStoreChange[1, "InvoiceID"] = "Số hóa đơn";
            flexStoreChange.Cols["InvoiceID"].Visible = true;

            flexStoreChange[0, "TotalPacking"] = "Số thùng";
            flexStoreChange[1, "TotalPacking"] = "Số thùng";
            flexStoreChange.Cols["TotalPacking"].Visible = true;

            flexStoreChange[0, "StoreChangeTypeName"] = "Loại chuyển kho";
            flexStoreChange[1, "StoreChangeTypeName"] = "Loại chuyển kho";
            flexStoreChange.Cols["StoreChangeTypeName"].Visible = true;

            flexStoreChange[0, "TransportTypeName"] = "Phương tiện vận chuyển";
            flexStoreChange[1, "TransportTypeName"] = "Phương tiện vận chuyển";
            flexStoreChange.Cols["TransportTypeName"].Visible = true;

            flexStoreChange[0, "IsNew"] = "Mới";
            flexStoreChange[1, "IsNew"] = "Mới";
            flexStoreChange.Cols["IsNew"].Visible = true;

            flexStoreChange[0, "IsTransfered"] = "Thông tin giao hàng";
            flexStoreChange[1, "IsTransfered"] = "Đã giao";
            flexStoreChange.Cols["IsTransfered"].Visible = true;

            flexStoreChange[0, "IsRequireVoucher"] = "Yêu cầu chứng từ";
            flexStoreChange[1, "IsRequireVoucher"] = "Yêu cầu chứng từ";
            flexStoreChange.Cols["IsRequireVoucher"].Visible = true;

            flexStoreChange[0, "TransportVoucherID"] = "Chứng từ vận chuyển";
            flexStoreChange[1, "TransportVoucherID"] = "Chứng từ vận chuyển";
            flexStoreChange.Cols["TransportVoucherID"].Visible = true;

            flexStoreChange[0, "IsReceive"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "IsReceive"] = "Đã nhập";
            flexStoreChange.Cols["IsReceive"].Visible = true;

            flexStoreChange[0, "ReceiveNote"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "ReceiveNote"] = "Ghi chú nhập";
            flexStoreChange.Cols["ReceiveNote"].Visible = true;

            flexStoreChange[0, "TotalWeight"] = "Tổng trọng lượng (kg)";
            flexStoreChange[1, "TotalWeight"] = "Tổng trọng lượng (kg)";
            flexStoreChange.Cols["TotalWeight"].Visible = true;

            flexStoreChange[0, "TotalSize"] = "Tổng thể tích (m³)";
            flexStoreChange[1, "TotalSize"] = "Tổng thể tích (m³)";
            flexStoreChange.Cols["TotalSize"].Visible = true;

            flexStoreChange[0, "TotalShippingCost"] = "Chi phí vận chuyển tạm tính";
            flexStoreChange[1, "TotalShippingCost"] = "Chi phí vận chuyển tạm tính";
            flexStoreChange.Cols["TotalShippingCost"].Visible = true;

            flexStoreChange[0, "StoreChangeOrderID"] = "Mã yêu cầu";
            flexStoreChange[1, "StoreChangeOrderID"] = "Mã yêu cầu";
            flexStoreChange.Cols["StoreChangeOrderID"].Visible = true;

            flexStoreChange[0, "InputVoucherID"] = "Mã phiếu nhập";
            flexStoreChange[1, "InputVoucherID"] = "Mã phiếu nhập";
            flexStoreChange.Cols["InputVoucherID"].Visible = true;

            flexStoreChange[0, "OutputVoucherID"] = "Mã phiếu xuất";
            flexStoreChange[1, "OutputVoucherID"] = "Mã phiếu xuất";
            flexStoreChange.Cols["OutputVoucherID"].Visible = true;

            flexStoreChange[0, "StoreChangeUser"] = "Thông tin xuất kho";
            flexStoreChange[1, "StoreChangeUser"] = "Nhân viên xuất";
            flexStoreChange.Cols["StoreChangeUser"].Visible = true;

            flexStoreChange[0, "StoreChangeUserFullName"] = "Thông tin xuất kho";
            flexStoreChange[1, "StoreChangeUserFullName"] = "Nhân viên xuất";
            flexStoreChange.Cols["StoreChangeUserFullName"].Visible = true;

            flexStoreChange[0, "ReceiveUser"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "ReceiveUser"] = "Nhân viên nhập";
            flexStoreChange.Cols["ReceiveUser"].Visible = true;

            flexStoreChange[0, "ReceiveUserFullName"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "ReceiveUserFullName"] = "Nhân viên nhập";
            flexStoreChange.Cols["ReceiveUserFullName"].Visible = true;

            flexStoreChange[0, "ReceivedTransferDate"] = "Thông tin nhận hàng để vận chuyển";
            flexStoreChange[1, "ReceivedTransferDate"] = "Thời gian nhận";
            flexStoreChange.Cols["ReceivedTransferDate"].Visible = true;

            flexStoreChange[0, "TransferedUser"] = "Thông tin giao hàng";
            flexStoreChange[1, "TransferedUser"] = "Nhân viên giao";
            flexStoreChange.Cols["TransferedUser"].Visible = true;

            flexStoreChange[0, "TransferedUserFullName"] = "Thông tin giao hàng";
            flexStoreChange[1, "TransferedUserFullName"] = "Nhân viên giao";
            flexStoreChange.Cols["TransferedUserFullName"].Visible = true;

            flexStoreChange[0, "IsSelect"] = " ";
            flexStoreChange[1, "IsSelect"] = " ";

            flexStoreChange[0, "IsReceivedTransfer"] = "Thông tin nhận hàng để vận chuyển";
            flexStoreChange[1, "IsReceivedTransfer"] = "Đã nhận";
            flexStoreChange.Cols["IsReceivedTransfer"].Visible = true;

            flexStoreChange[0, "ReceivedTransferUser"] = "Thông tin nhận hàng để vận chuyển";
            flexStoreChange[1, "ReceivedTransferUser"] = "Nhân viên nhận";
            flexStoreChange.Cols["ReceivedTransferUser"].Visible = true;

            flexStoreChange[0, "ReceivedTransferUserFullName"] = "Thông tin nhận hàng để vận chuyển";
            flexStoreChange[1, "ReceivedTransferUserFullName"] = "Nhân viên nhận";
            flexStoreChange.Cols["ReceivedTransferUserFullName"].Visible = true;

            flexStoreChange[0, "IsSignReceive"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "IsSignReceive"] = "Đã ký nhận";
            flexStoreChange.Cols["IsSignReceive"].Visible = true;

            flexStoreChange[0, "SignReceiveUser"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "SignReceiveUser"] = "Nhân viên ký nhận";
            flexStoreChange.Cols["SignReceiveUser"].Visible = true;

            flexStoreChange[0, "SignReceiveUserFullName"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "SignReceiveUserFullName"] = "Nhân viên ký nhận";
            flexStoreChange.Cols["SignReceiveUserFullName"].Visible = true;

            flexStoreChange[0, "SignReceiveDate"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "SignReceiveDate"] = "Thời gian ký nhận";
            flexStoreChange.Cols["SignReceiveDate"].Visible = true;

            flexStoreChange[0, "SignReceiveNote"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "SignReceiveNote"] = "Ghi chú ký nhận";
            flexStoreChange.Cols["SignReceiveNote"].Visible = true;

            flexStoreChange[0, "REVIEWUSER"] = "Người duyệt";
            flexStoreChange[1, "REVIEWUSER"] = "Người duyệt";
            flexStoreChange.Cols["REVIEWUSER"].Visible = true;

            #endregion

            for (int i = 1; i < flexStoreChange.Cols.Count; i++)
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowEditing(flexStoreChange, flexStoreChange.Cols[i].Name, false);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowEditing(flexStoreChange, "IsSelect,TransportVoucherID, IsTransfered, IsReceive, IsReceivedTransfer, IsSignReceive, ReceiveNote, SignReceiveNote", true);
            flexStoreChange.Cols["IsTransfered"].AllowSorting = false;
            flexStoreChange.Cols["IsReceive"].AllowSorting = false;
            flexStoreChange.Cols["IsReceivedTransfer"].AllowSorting = false;
            flexStoreChange.Cols["IsStoreChanged"].AllowSorting = false;
            flexStoreChange.Cols["IsSignReceive"].AllowSorting = false;

            flexStoreChange.Cols["TransferedUser"].AllowMerging = true;
            flexStoreChange.Cols["TransferedUserFullName"].AllowMerging = true;
            flexStoreChange.Cols["ReceivedTransferUser"].AllowMerging = true;
            flexStoreChange.Cols["ReceivedTransferUserFullName"].AllowMerging = true;
            flexStoreChange.Cols["SignReceiveUser"].AllowMerging = true;
            flexStoreChange.Cols["SignReceiveUserFullName"].AllowMerging = true;
            flexStoreChange.Cols["StoreChangeUser"].AllowMerging = true;
            flexStoreChange.Cols["StoreChangeUserFullName"].AllowMerging = true;
            flexStoreChange.Cols["ReceiveUserFullName"].AllowMerging = true;

            #region Format
            flexStoreChange.Cols["TotalShippingCost"].Format = "#,##0";
            flexStoreChange.Cols["DateCreateStoreChange"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["StoreChangeDate"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["TransferedDate"].Format = "dd/MM/yyyy HH:mm";//Ngày chuyển
            flexStoreChange.Cols["DateReceive"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["SignReceiveDate"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["ReceivedTransferDate"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["IsNew"].DataType = typeof(bool);
            flexStoreChange.Cols["IsReceive"].DataType = typeof(bool);
            flexStoreChange.Cols["IsSelect"].DataType = typeof(bool);
            flexStoreChange.Cols["IsTransfered"].DataType = typeof(bool);
            flexStoreChange.Cols["IsRequireVoucher"].DataType = typeof(bool);
            flexStoreChange.Cols["IsStoreChanged"].DataType = typeof(bool);
            flexStoreChange.Cols["IsReceivedTransfer"].DataType = typeof(bool);
            flexStoreChange.Cols["IsSignReceive"].DataType = typeof(bool);
            flexStoreChange.Cols["FromStoreID"].Visible = false;
            Library.AppCore.LoadControls.C1FlexGridObject.AddColumnHeaderCheckBox(flexStoreChange, "IsSelect");
            #endregion

            #region Width
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexStoreChange,
                "StoreChangeID", 130,
                "DateCreateStoreChange", 110,
                "CaskCode", 200,
                "FromStoreName", 220,
                "ToStoreID", 80,
                "ToStoreName", 220,
                "StoreChangeDate", 110,
                "TransferedDate", 110,
                "DateReceive", 110,
                "Content", 220,
                "DaysOfStoreChange", 100,
                "TransferTime", 100,
                "InvoiceID", 80,
                "TotalPacking", 70,
                "StoreChangeTypeName", 150,
                "StoreChangeUser", 42,
                "ReceiveUser", 42,
                "TransferedUser", 42,
                "StoreChangeUserFullName", 140,
                "ReceiveUserFullName", 140,
                "ReceivedTransferDate", 110,
                "TransferedUserFullName", 140,

                "TransportTypeName", 150,
                "IsNew", 40,
                "IsReceive", 80,
                "IsTransfered", 80,
                "IsRequireVoucher", 80,
                "ReceiveNote", 150,
                "StoreChangeOrderID", 120,
                "InputVoucherID", 120,
                "OutputVoucherID", 120,
                "IsReceiveOld", 80,
                "IsEdit", 40,
                "IsSelect", 40,
                "IsStoreChanged", 60,
                "TransportVoucherID", 120,
                "TotalWeight", 80,
                "TotalSize", 80,
                "IsReceivedTransfer", 80,
                "ReceivedTransferUser", 42,
                "ReceivedTransferUserFullName", 140,
                "TotalShippingCost", 110,
                "IsSignReceive", 80,
                "SignReceiveUser", 42,
                "SignReceiveUserFullName", 140,
                "SignReceiveDate", 110,
                "SignReceiveNote", 150);
            #endregion

            #region TextAlign
            flexStoreChange.Cols["IsNew"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsReceive"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsStoreChanged"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["TotalPacking"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["DaysOfStoreChange"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsSelect"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsReceivedTransfer"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsTransfered"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsSignReceive"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            #endregion

            #region Xét visible cho các cột
            if (!chkIsViewDetail.Checked)
            {
                // Xét thuộc tính ẩn cho các cột
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexStoreChange,
                    false,
                    "CaskCode",
                    "DateReceive",
                    "StoreChangeUser",
                    "StoreChangeUserFullName",
                    "StoreChangeDate",
                    "ReceiveUser",
                    "ReceiveUserFullName",
                    "TransportTypeName",
                    "IsNew",
                    "StoreChangeOrderID",
                    "InputVoucherID",
                    "OutputVoucherID",
                    "TransferedUser",
                    "TransferedUserFullName",
                    "TransferedDate",
                    "ReceivedTransferDate",
                    "ReceivedTransferUser",
                    "ReceivedTransferUserFullName",
                    "ReceiveNote",
                    "SignReceiveUser",
                    "SignReceiveUserFullName",
                    "SignReceiveDate",
                    "SignReceiveNote",
                    "IsRequireVoucher",
                    "TransportVoucherID",
                    "TotalShippingCost");

                flexStoreChange[0, "IsStoreChanged"] = "Đã xuất";
                flexStoreChange[1, "IsStoreChanged"] = "Đã xuất";
                flexStoreChange.Cols["IsStoreChanged"].AllowEditing = false;
                flexStoreChange.Cols["IsStoreChanged"].Visible = true;

                flexStoreChange[0, "IsTransfered"] = "Giao vận chuyển";
                flexStoreChange[1, "IsTransfered"] = "Giao vận chuyển";
                flexStoreChange.Cols["IsTransfered"].AllowEditing = false;
                flexStoreChange.Cols["IsTransfered"].Visible = true;

                flexStoreChange[0, "IsReceive"] = "Xác nhận thực nhập";
                flexStoreChange[1, "IsReceive"] = "Xác nhận thực nhập";
                flexStoreChange.Cols["IsReceive"].AllowEditing = false;
                flexStoreChange.Cols["IsReceive"].Visible = true;

                flexStoreChange[0, "IsReceivedTransfer"] = "Nhận vận chuyển";
                flexStoreChange[1, "IsReceivedTransfer"] = "Nhận vận chuyển";
                flexStoreChange.Cols["IsReceivedTransfer"].AllowEditing = false;
                flexStoreChange.Cols["IsReceivedTransfer"].Visible = true;

                flexStoreChange[0, "IsSignReceive"] = "Ký nhận nhập";
                flexStoreChange[1, "IsSignReceive"] = "Ký nhận nhập";
                flexStoreChange.Cols["IsSignReceive"].AllowEditing = false;
                flexStoreChange.Cols["IsSignReceive"].Visible = true;
                flexStoreChange.Rows[0].Height = 20;
                flexStoreChange.Rows[1].Height = 20;

            }
            else
            {
                // Xét thuộc tính hiển thị cho các cột
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexStoreChange,
                    true,
                    "CaskCode",
                    "DateReceive",
                    "Content",
                    "StoreChangeUser",
                    "StoreChangeUserFullName",
                    "StoreChangeDate",
                    "ReceiveUser",
                    "ReceiveUserFullName",
                    "TransportTypeName",
                    "IsNew",
                    "StoreChangeOrderID",
                    "InputVoucherID",
                    "OutputVoucherID",
                    "TransferedUser",
                    "TransferedUserFullName",
                    "TransferedDate",
                    "ReceivedTransferDate",
                    "ReceivedTransferUser",
                    "ReceivedTransferUserFullName",
                    "ReceiveNote",
                    "SignReceiveUser",
                    "SignReceiveUserFullName",
                    "SignReceiveDate",
                    "SignReceiveNote",
                    "IsRequireVoucher",
                    "TransportVoucherID",
                    "TotalShippingCost");

                flexStoreChange[0, "IsStoreChanged"] = "Thông tin xuất kho";
                flexStoreChange[1, "IsStoreChanged"] = "Đã xuất";
                flexStoreChange.Cols["IsStoreChanged"].AllowEditing = false;
                flexStoreChange.Cols["IsStoreChanged"].Visible = true;

                flexStoreChange[0, "IsTransfered"] = "Thông tin giao hàng";
                flexStoreChange[1, "IsTransfered"] = "Đã giao";
                flexStoreChange.Cols["IsTransfered"].AllowEditing = false;
                flexStoreChange.Cols["IsTransfered"].Visible = true;

                flexStoreChange[0, "IsReceive"] = "Thông tin xác nhận thực nhập";
                flexStoreChange[1, "IsReceive"] = "Đã nhập";
                flexStoreChange.Cols["IsReceive"].AllowEditing = false;
                flexStoreChange.Cols["IsReceive"].Visible = true;

                flexStoreChange[0, "IsReceivedTransfer"] = "Thông tin nhận hàng để vận chuyển";
                flexStoreChange[1, "IsReceivedTransfer"] = "Đã nhận";
                flexStoreChange.Cols["IsReceivedTransfer"].AllowEditing = false;
                flexStoreChange.Cols["IsReceivedTransfer"].Visible = true;

                flexStoreChange[0, "IsSignReceive"] = "Thông tin ký nhận hàng";
                flexStoreChange[1, "IsSignReceive"] = "Đã ký nhận";
                flexStoreChange.Cols["IsSignReceive"].AllowEditing = false;
                flexStoreChange.Cols["IsSignReceive"].Visible = true;
                flexStoreChange.Rows[0].Height = 20;
                flexStoreChange.Rows[1].Height = 20;
            }
            #endregion

            #region Định dạng Style cho lưới
            Library.AppCore.LoadControls.C1FlexGridObject.SetBackColor(flexStoreChange, SystemColors.Info);
            C1.Win.C1FlexGrid.CellStyle style = flexStoreChange.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexStoreChange.GetCellRange(0, 0, 0, flexStoreChange.Cols.Count - 1);
            range.Style = style;
            flexStoreChange.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowMerging(flexStoreChange, "TransferedUser,TransferedUserFullName,ReceivedTransferUser,ReceivedTransferUserFullName,SignReceiveUser,SignReceiveUserFullName,StoreChangeUser,StoreChangeUserFullName, ReceiveUserFullName, ReceiveUser", true);
            flexStoreChange.Rows[0].AllowMerging = true;
            flexStoreChange.Rows[1].AllowMerging = true;
            #endregion
        }

        /// <summary>
        /// Kiểm tra dữ liệu nhập vào
        /// </summary>
        /// <returns></returns>
        private bool CheckInput()
        {

            if (!dtpFromDate.Checked)
            {
                MessageBox.Show(this, "Vui lòng chọn ngày chuyển hoặc ngày nhận", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpFromDate.Focus();
                return false;
            }

            if (dtpFromDate.Checked)
            {
                if (dtpToDate.Value.Subtract(dtpFromDate.Value).Days > 31)
                {
                    MessageBox.Show(this, "Vui lòng xem trong vòng 1 tháng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dtpFromDate.Focus();
                    return false;
                }
            }
            return true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckInput()) return;
            btnSearch.Enabled = false;

            try
            {
                #region Object KeyWord
                string strDaysOfStoreChange = "1";
                string strFromStoreList = Library.AppCore.Other.ConvertObject.ConvertIDListBracesToParentheses(cboFromStoreSearch.StoreIDList).Replace("(", "").Replace(")", "");
                string strToStoreList = Library.AppCore.Other.ConvertObject.ConvertIDListBracesToParentheses(cboToStoreSearch.StoreIDList).Replace("(", "").Replace(")", "");
                //string strTransportTypeIDList = Library.AppCore.LoadControls.CheckedComboBoxEditObject.GetCheckedItemToString(cboTransportType,Library.AppCore.Constant.EnumType.IDStringType.PARENTHESES);
                string strMainGroupIDList = cboMainGroup.MainGroupIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                string strStatusIDList = cboStatusList.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");

                DateTime? dtFromStoreChangeDate = dtpFromDate.Value.Date;
                DateTime? dtToStoreChangeDate = dtpToDate.Value.Date;
                DateTime? dtFromReceiveDate = dtpFromDate.Value.Date;
                DateTime? dtToReceiveDate = dtpToDate.Value.Date;

                List<object> objList = new List<object>();
                objList.AddRange(new object[]
                                    {
                                        "@StoreChangeTypeID", cboStoreChangeType.SelectedValue,
                                        "@StatusIDList", strStatusIDList,
                                        "@FromStoreList",strFromStoreList,
                                        "@ToStoreList", strToStoreList,
                                        "@MainGroupIDList", strMainGroupIDList,
                                        "@DaysOfStoreChange", strDaysOfStoreChange,
                                        "@UserName", Library.AppCore.SystemConfig.objSessionUser.UserName,
                                        "@Keyword", txtKeyword.Text.Trim(),
                                        "@SearchBy", cboSearchBy.SelectedIndex
                                    });

                if (bolIsStoreChangeDate) objList.AddRange(new object[] { "@FromStoreChangeDate", dtpFromDate.Value.Date });
                if (bolIsStoreChangeDate) objList.AddRange(new object[] { "@ToStoreChangeDate", dtpToDate.Value.Date });
                if (!bolIsStoreChangeDate) objList.AddRange(new object[] { "@FromReceiveDate", dtpFromDate.Value.Date });
                if (!bolIsStoreChangeDate) objList.AddRange(new object[] { "@ToReceiveDate", dtpToDate.Value.Date });

                #endregion

                ERP.Inventory.PLC.StoreChange.PLCStoreChange objPLCStoreChange = new PLC.StoreChange.PLCStoreChange();
                DataTable dtbData = objPLCStoreChange.SearchData(objList.ToArray());
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSearch.Enabled = true;
                    return;
                }
                dtbData.Columns.Add("IsTransferedBefore", typeof(Boolean));
                dtbData.Columns.Add("IsReceivedTransferBefore", typeof(Boolean));
                dtbData.Columns.Add("IsSignReceiveBefore", typeof(Boolean));
                for (int i = 0; i < dtbData.Rows.Count; i++)
                {
                    dtbData.Rows[i]["IsTransferedBefore"] = Convert.ToBoolean(dtbData.Rows[i]["IsTransfered"]);
                    dtbData.Rows[i]["IsReceivedTransferBefore"] = Convert.ToBoolean(dtbData.Rows[i]["IsReceivedTransfer"]);
                    dtbData.Rows[i]["IsSignReceiveBefore"] = Convert.ToBoolean(dtbData.Rows[i]["IsSignReceive"]);
                }
                flexStoreChange.DataSource = dtbData;
                FormatFlex();
                btnUpdate.Enabled = true;

                lbTotalQuantity.Value = dtbData.Rows.Count; //Đếm tổng số lượng phiếu chuyển kho tìm được
                lbTotalWeight.Value = Convert.ToDecimal(Globals.IsNull(dtbData.Compute("Sum(TotalWeight)", string.Empty), 0));//Tổng trọng lượng
                lbTotalSize.Value = Convert.ToDecimal(Globals.IsNull(dtbData.Compute("Sum(TotalSize)", string.Empty), 0));//Tổng thể tích
                lbTotalQuantitySelected.Value = Convert.ToInt32(Globals.IsNull(dtbData.Compute("Sum(IsSelect)", "(IsTransfered = 1 Or IsReceivedTransfer = 1) And IsEdit = 1"), 0));
                lbTotalWeightSelected.Value = Convert.ToDecimal(Globals.IsNull(dtbData.Compute("Sum(TotalWeight)", "IsTransfered = 1 And IsEdit = 1"), 0));
                lbTotalSizeSelected.Value = Convert.ToDecimal(Globals.IsNull(dtbData.Compute("Sum(TotalSize)", "IsTransfered = 1 And IsEdit = 1"), 0));
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tìm kiếm phiếu chuyển kho", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tìm kiếm phiếu chuyển kho", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btnSearch.Enabled = true;
            }
        }

        private void flexStoreChange_DoubleClick(object sender, EventArgs e)
        {
            if (flexStoreChange.RowSel <= 1) return;
            if (flexStoreChange.ColSel == flexStoreChange.Cols["ReceiveNote"].Index) return;
            if (flexStoreChange.ColSel == flexStoreChange.Cols["IsTransfered"].Index) return;
            if (flexStoreChange.ColSel == flexStoreChange.Cols["IsReceive"].Index) return;
            if (flexStoreChange.ColSel == flexStoreChange.Cols["TransportVoucherID"].Index) return;

            frmStoreChange frmStoreChange1 = new frmStoreChange();
            frmStoreChange1.StoreChangeID = Convert.ToString(flexStoreChange[flexStoreChange.RowSel, "StoreChangeID"]).Trim();
            frmStoreChange1.IsOnlyCertifyFinger = Convert.ToBoolean(intOnlyCertifyFinger);
            frmStoreChange1.IsEdit = true;
            frmStoreChange1.ShowDialog();
            if (frmStoreChange1.IsUpdate)
                btnSearch_Click(null, null);
        }

        private void chkIsViewDetail_CheckedChanged(object sender, EventArgs e)
        {
            if (flexStoreChange.DataSource != null)
            {
                FormatFlex();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            #region Lấy danh sách các dòng có chỉnh sửa
            //Lấy danh sách các dòng có chỉnh sửa IsEdit = 1
            DataTable tblStoreChangeList = flexStoreChange.DataSource as DataTable;
            DataRow[] arrRow = tblStoreChangeList.Select("IsEdit = 1");
            if (arrRow.Length == 0)
                return;
            for (int i = 0; i < arrRow.Length; i++)
            {
                if (Convert.ToBoolean(arrRow[i]["IsRequireVoucher"]) && Convert.ToBoolean(arrRow[i]["IsReceive"])
                    && Convert.ToString(arrRow[i]["TransportVoucherID"]).Trim().Length < 1)
                {
                    MessageBox.Show(this, "Vui lòng nhập chứng từ vận chuyển đối với phiếu bắt buộc nhập.", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

            }

            bool bolIsInputSignReciveNote = false;
            for (int i = 0; i < arrRow.Length; i++)
            {
                if (Convert.ToBoolean(arrRow[i]["IsSignReceive"]) && !Convert.ToBoolean(arrRow[i]["ISSIGNRECEIVEOLD"])
                    && Convert.ToString(arrRow[i]["SignReceiveNote"]).Trim().Length < 1)
                {
                    bolIsInputSignReciveNote = true;
                    break;
                }
            }
            if (bolIsInputSignReciveNote)
            {
                Library.AppControl.FormCommon.frmInputString frmInputString1 = new Library.AppControl.FormCommon.frmInputString();
                frmInputString1.MaxLengthString = 200;
                frmInputString1.Text = "Nhập ghi chú ký nhận";
                frmInputString1.ShowDialog();
                if (frmInputString1.IsAccept)
                {
                    if (!string.IsNullOrEmpty(frmInputString1.ContentString))
                    {
                        for (int k = 0; k < tblStoreChangeList.Rows.Count; k++)
                        {
                            if (Convert.ToInt32(tblStoreChangeList.Rows[k]["IsEdit"]) == 1)
                                tblStoreChangeList.Rows[k]["SignReceiveNote"] = frmInputString1.ContentString;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Vui lòng nhập nội dung ghi chú", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnUpdate_Click(sender, e);
                        return;
                    }

                }
                else
                    return;
            }

            bool bolIsInputReciveNote = false;
            for (int i = 0; i < arrRow.Length; i++)
            {
                if (Convert.ToBoolean(arrRow[i]["IsReceive"]) && !Convert.ToBoolean(arrRow[i]["ISRECEIVEOLD"])
                    && Convert.ToString(arrRow[i]["ReceiveNote"]).Trim().Length < 1)
                {
                    bolIsInputReciveNote = true;
                    break;
                }
            }

            if (bolIsInputReciveNote)
            {
                Library.AppControl.FormCommon.frmInputString frmInputString1 = new Library.AppControl.FormCommon.frmInputString();
                frmInputString1.MaxLengthString = 200;
                frmInputString1.Text = "Nhập ghi chú xác nhận thực nhập";
                frmInputString1.ShowDialog();
                if (frmInputString1.IsAccept)
                {
                    if (!string.IsNullOrEmpty(frmInputString1.ContentString))
                    {
                        for (int k = 0; k < tblStoreChangeList.Rows.Count; k++)
                        {
                            if (Convert.ToInt32(tblStoreChangeList.Rows[k]["IsEdit"]) == 1)
                                tblStoreChangeList.Rows[k]["ReceiveNote"] = frmInputString1.ContentString;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng nhập nội dung ghi chú", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnUpdate_Click(sender, e);
                        return;
                    }
                }
                else
                    return;
            }
            #endregion

            try
            {
                string strCertifyTransferedUser = SystemConfig.objSessionUser.UserName;
                bool bolIsTransferedCheckedByFinger = false;
                string strCertifyReceivedTransferUser = SystemConfig.objSessionUser.UserName; ;
                bool bolIsReceivedTransferCheckinByFinger = false;
                #region Lấy thông tin kích thước sản phẩm
                //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
                ERP.Inventory.PLC.StoreChange.PLCStoreChange objPLCStoreChange = new PLC.StoreChange.PLCStoreChange();
                //Nếu có check "Đã chuyển" thì hiển thị Form 
                //Lấy thông tin hàng hóa có kích thước tối đa theo Mã phiếu xuất chuyển kho
                if (Convert.ToInt32(lbTotalQuantitySelected.Value) > 0)
                {
                    bool bolIsAccept = false;
                    string strStoreChangeIDlst = string.Empty;
                    DataRow[] lstTransfered = tblStoreChangeList.Select("IsTransfered = 1 And IsTransferedBefore = 0 And IsEdit = 1");
                    DataRow[] lstReceivedTransfer = tblStoreChangeList.Select("IsReceivedTransfer = 1 And IsReceivedTransferBefore = 0 And IsEdit = 1");
                    foreach (DataRow row in lstTransfered)
                    {
                        strStoreChangeIDlst = strStoreChangeIDlst + Convert.ToString(row["StoreChangeID"]).Trim() + ",";
                    }
                    strStoreChangeIDlst = "," + strStoreChangeIDlst;
                    DataTable dtbProductInfoMaxSize = objPLCStoreChange.GetProductInfoMaxSize(strStoreChangeIDlst);
                    if (dtbProductInfoMaxSize != null && dtbProductInfoMaxSize.Rows.Count > 0)
                    {
                        frmProductInfoMaxSize frmProductInfoMaxSize1 = new frmProductInfoMaxSize();
                        frmProductInfoMaxSize1.ProductInfoMaxSize = dtbProductInfoMaxSize;
                        frmProductInfoMaxSize1.ShowDialog();
                        //Nếu chấp nhận thì thực hiện Update
                        bolIsAccept = frmProductInfoMaxSize1.IsAccept;
                        if (!bolIsAccept)
                            return;
                    }
                    // Kiểm tra nếu có phiếu xuất chuyển kho có nhân viên đăng nhập không phải là nhân viên chuyển kho thì gọi form xác nhận nhân viên giao hàng
                    if (lstTransfered.Length > 0)
                    {
                        bool bolIsDifferentUser = false;
                        DataRow[] lstDifferentUser = tblStoreChangeList.Select("IsTransfered = 1 And IsEdit = 1 And StoreChangeUser <> '" + SystemConfig.objSessionUser.UserName + "'");
                        if (lstDifferentUser.Length > 0)
                            bolIsDifferentUser = true;
                        if (bolIsDifferentUser)
                        {
                            bool bolIsEnableLogin = Convert.ToBoolean(intOnlyCertifyFinger);
                            Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên giao hàng", "Giao hàng", null, null, !bolIsEnableLogin, true);
                            frmCertifyLogin1.ShowDialog(this);
                            //Nếu xác nhận thành công thì lấy thông tin xác nhận
                            if (!frmCertifyLogin1.IsCorrect)
                            {
                                return;
                            }
                            strCertifyTransferedUser = frmCertifyLogin1.UserName;
                            bolIsTransferedCheckedByFinger = frmCertifyLogin1.IsCertifyByFinger;
                        }
                    }

                    if (lstReceivedTransfer.Length > 0)
                    {
                        bool bolIsEnableLogin = Convert.ToBoolean(intOnlyCertifyFinger);
                        List<string> lstFunctionID = new List<string>();
                        lstFunctionID.Add(strPermission_IsReceivedTransfer);
                        Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên nhận hàng", "Nhận hàng", null, lstFunctionID, !bolIsEnableLogin, true);
                        frmCertifyLogin1.ShowDialog(this);
                        //Nếu xác nhận thành công thì lấy thông tin xác nhận
                        if (!frmCertifyLogin1.IsCorrect)
                        {
                            return;
                        }
                        strCertifyReceivedTransferUser = frmCertifyLogin1.UserName;
                        bolIsReceivedTransferCheckinByFinger = frmCertifyLogin1.IsCertifyByFinger;

                    }

                }
                #endregion

                #region Gán giá trị cho các đối tượng WS
                List<ERP.Inventory.PLC.StoreChange.WSStoreChange.StoreChange> StoreChangeList = new List<ERP.Inventory.PLC.StoreChange.WSStoreChange.StoreChange>();
                foreach (DataRow objRow in arrRow)
                {
                    ERP.Inventory.PLC.StoreChange.WSStoreChange.StoreChange objStoreChangeBO = new PLC.StoreChange.WSStoreChange.StoreChange();
                    objStoreChangeBO.StoreChangeID = Convert.ToString(objRow["StoreChangeID"]).Trim();
                    objStoreChangeBO.StoreChangeUser = Convert.ToString(objRow["StoreChangeUser"]).Trim();
                    objStoreChangeBO.IsReceive = Convert.ToBoolean(objRow["IsReceive"]);
                    objStoreChangeBO.ReceiveNote = Convert.ToString(objRow["ReceiveNote"]).Trim();
                    objStoreChangeBO.UserReceive = SystemConfig.objSessionUser.UserName;
                    objStoreChangeBO.InputVoucherID = Convert.ToString(objRow["InputVoucherID"]).Trim();
                    objStoreChangeBO.IsTransfered = Convert.ToBoolean(objRow["IsTransfered"]);
                    objStoreChangeBO.TransportVoucherID = Convert.ToString(objRow["TransportVoucherID"]).Trim();
                    objStoreChangeBO.InvoiceID = Convert.ToString(objRow["InvoiceID"]).Trim();
                    objStoreChangeBO.InvoiceSymbol = Convert.ToString(objRow["InvoiceSymbol"]).Trim();
                    ////Trường hợp nhân viên đăng nhập không phải là nhân viên tạo phiếu thì nhân viên giao hàng là nhân viên đã xác nhận, ngược lại là nhân viên đăng nhập
                    //if (objStoreChangeBO.StoreChangeUser != SystemConfig.objSessionUser.UserName)
                    //    objStoreChangeBO.TransferedUser = strCertifyTransferedUser;
                    //else
                    //    objStoreChangeBO.TransferedUser = SystemConfig.objSessionUser.UserName;

                    objStoreChangeBO.TransferedUser = strCertifyTransferedUser;

                    objStoreChangeBO.IsReceivedTransfer = Convert.ToBoolean(objRow["IsReceivedTransfer"]);
                    //Trường hợp nhân viên đăng nhập không phải là nhân viên tạo phiếu thì nhân viên vận chuyển nhận hàng là nhân viên đã xác nhận, ngược lại là nhân viên đăng nhập
                    //if (objStoreChangeBO.StoreChangeUser != SystemConfig.objSessionUser.UserName)
                    //    objStoreChangeBO.ReceivedTransferUser = strCertifyReceivedTransferUser;
                    //else
                    //    objStoreChangeBO.ReceivedTransferUser = SystemConfig.objSessionUser.UserName;

                    objStoreChangeBO.ReceivedTransferUser = strCertifyReceivedTransferUser;

                    //Nếu xác nhận bằng vân tay
                    objStoreChangeBO.IsTransferedCheckinByFinger = bolIsTransferedCheckedByFinger;
                    objStoreChangeBO.IsReceivedCheckinByFinger = bolIsReceivedTransferCheckinByFinger;

                    //Thông tin ký nhận hàng
                    objStoreChangeBO.IsSignReceive = Convert.ToBoolean(objRow["IsSignReceive"]);
                    objStoreChangeBO.SignReceiveUser = SystemConfig.objSessionUser.UserName;
                    objStoreChangeBO.SignReceiveNote = Convert.ToString(objRow["SignReceiveNote"]).Trim();
                    StoreChangeList.Add(objStoreChangeBO);
                }
                #endregion

                #region Thực hiện Update dữ liệu
                objPLCStoreChange.UpdateStoreChangeList(StoreChangeList);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message);
                    SystemErrorWS.Insert(SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                MessageBox.Show(this, "Cập nhật phiếu chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnSearch_Click(null, null);
                #endregion
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi cập nhật phiếu chuyển kho", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi cập nhật phiếu chuyển kho", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void flexStoreChange_AfterSort(object sender, C1.Win.C1FlexGrid.SortColEventArgs e)
        {

            for (int i = flexStoreChange.Rows.Fixed; i < flexStoreChange.Rows.Count; i++)
            {
                if (Convert.ToBoolean(flexStoreChange.Rows[i]["IsTransfered"]) && !Convert.ToBoolean(flexStoreChange.Rows[i]["IsTransferedBefore"]))
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowForeColor(flexStoreChange, i, Color.MediumBlue);
                else
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowForeColor(flexStoreChange, i, Color.Black);
            }
        }

        private void flexStoreChange_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row <= 1)
                return;
            int intFromStore = Convert.ToInt32(flexStoreChange.Rows[e.Row]["FromStoreID"]);
            int intToStore = Convert.ToInt32(flexStoreChange.Rows[e.Row]["ToStoreID"]);
            //Không phải là cột chọn và đã nhập kho thì không cho phép chỉnh sửa
            if (e.Col != flexStoreChange.Cols["IsSelect"].Index && Convert.ToBoolean(flexStoreChange[e.Row, "ISRECEIVEOLD"]))
            {
                e.Cancel = true;
                return;
            }

            // Được nhập ghi chú xác nhận nhập kho nếu
            // Có quyền "PM_StoreChange_IsTransferedUpdate"
            // Có quyền "StoreChange_CheckIsReceive"
            // Có quyền trên kho nhập
            // Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            if (e.Col == flexStoreChange.Cols["SignReceiveNote"].Index)
            {
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
                    SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
                    (intCheckIsReceive > 0 ? (SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) : true))
                {
                    //Nếu nhân viên vận chuyển chưa nhận hàng thì không được nhập ghi chú
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]) &&
                        !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["ISSIGNRECEIVEOLD"]))
                    {
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }
            // Được nhập ghi chú ký nhận nếu
            // Có quyền "PM_StoreChange_IsTransferedUpdate"
            // Có quyền "StoreChange_CheckIsReceive"
            // Có quyền trên kho nhập
            // Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            if (e.Col == flexStoreChange.Cols["ReceiveNote"].Index)
            {
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
    SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
    (intCheckIsReceive > 0 ? (SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) : true))
                {
                    //Chưa ký nhận thì không được nhập ghi chú 
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["ISRECEIVEOLD"]))
                    {
                        e.Cancel = false;
                    }

                    else
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }

            //#region Đã chuyển - IsTransfered
            ////Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            //if (e.Col == flexStoreChange.Cols["IsTransfered"].Index)
            //{
            //    //Được check chọn "Đã chuyển" nếu:
            //    //Có quyền "PM_StoreChange_IsTransferedUpdate"
            //    //Có quyền trên Kho Xuất
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intFromStore, Library.AppCore.Constant.EnumType.StorePermissionType.OUTPUT))
            //    {
            //        //Nếu chưa nhận hàng để vận chuyển thì được check Đã chuyển
            //        if (!Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransfer"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"])) { e.Cancel = false; }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //}

            ////Được check chọn "Đã nhận" nếu:
            ////Có quyền "PM_StoreChange_IsTransferedUpdate"
            //if (e.Col == flexStoreChange.Cols["IsReceivedTransfer"].Index)
            //{
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate))
            //    {
            //        //Nếu đã check Đã chuyển, chưa ký Nhận hàng từ nhân viên vận chuyển và chưa check Đã nhận thì được check Đã nhận
            //        if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceive"])
            //            && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]))
            //        {
            //            e.Cancel = false;
            //        }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }

            //}

            //// Được check chọn "Đã ký nhận" nếu
            //// Có quyền "PM_StoreChange_IsTransferedUpdate"
            //// Có quyền "StoreChange_CheckIsReceive"
            //// Có quyền trên kho nhập
            //// Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            //if (e.Col == flexStoreChange.Cols["IsSignReceive"].Index)
            //{
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
            //        SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
            //        (intCheckIsReceive > 0 ? (SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) : true))
            //    {
            //        //Nếu nhân viên vận chuyển chưa nhận hàng thì không được check
            //        if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]))
            //        {
            //            e.Cancel = false;
            //        }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //}
            //#endregion

            //#region Đã nhận - IsReceive
            ////Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            //if (e.Col == flexStoreChange.Cols["IsReceive"].Index)
            //{
            //    //Được check chọn "Đã nhập" nếu:
            //    //Có quyền "strPermission_IsTransferedUpdate"
            //    //Có quyền "StoreChange_CheckIsReceive"
            //    //Có quyền trên Kho Nhập
            //    //Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
            //        SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
            //        (intCheckIsReceive>0 ? (SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) : true))
            //    {
            //        //Nếu check Đã ký nhận thì ko đc check Đã nhập liền, phải cập nhật trạng thái đã ký nhận
            //        if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]))
            //        {
            //            e.Cancel = false;
            //        }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //}
            //#endregion

            //#region Chứng từ - TransportVoucherID
            ////Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            //if (e.Col == flexStoreChange.Cols["TransportVoucherID"].Index)
            //{
            //    //Được nhập "Chứng từ vận chuyển" nếu:
            //    //Có quyền "PM_StoreChange_IsTransferedUpdate"
            //    //Có quyền trên Kho Xuất
            //    //Chứng từ chưa ký nhận
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate))
            //    {
            //        if (ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intFromStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER))
            //        {
            //            if (!Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceive"])) { e.Cancel = false;}
            //        }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }

            //    //Có quyền "PM_StoreChange_EditTransportVoucher"
            //    //Có quyền trên Kho Xuất & Kho Nhập
            //    if (SystemConfig.objSessionUser.IsPermission("PM_StoreChange_EditTransportVoucher"))
            //    {
            //        if (ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intFromStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER)
            //            || ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER))
            //        { e.Cancel = false; }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    e.Cancel = true;
            //    return;
            //}
            //#endregion
        }

        private void flexStoreChange_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            //if (e.Row <= 1) return;
            flexStoreChange[e.Row, "IsEdit"] = true;

            #region Đã chuyển - IsTransfered
            if (e.Col == flexStoreChange.Cols["IsTransfered"].Index)
            {
                //Nếu check "Đã chuyển" thì tô chữ màu xanh
                if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransfered"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"]))
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowForeColor(flexStoreChange, e.Row, Color.MediumBlue);
                else
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowForeColor(flexStoreChange, e.Row, Color.Black);
            }

            #endregion

            #region Đã nhận - IsReceive
            //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            if (e.Col == flexStoreChange.Cols["IsSignReceive"].Index)
            {
                if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceive"]))
                {
                    //Nếu phiếu yêu cầu nhập chứng từ vận chuyển
                    //thì bắt buộc nhập chứng từ trước khi check Đã ký nhận
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsRequireVoucher"]) &&
                    Convert.ToString(flexStoreChange.Rows[e.Row]["TransportVoucherID"]).Trim().Length == 0)
                    {
                        MessageBox.Show(this, "Vui lòng nhập chứng từ trước.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexStoreChange.Rows[e.Row]["IsSignReceive"] = false;
                        return;
                    }
                }
            }
            #endregion

            #region Tính tổng
            //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            DataTable dtbFromFlex = (DataTable)flexStoreChange.DataSource;
            dtbFromFlex.AcceptChanges();
            lbTotalQuantitySelected.Value = Convert.ToInt32(Globals.IsNull(dtbFromFlex.Compute("Count(IsSelect)", "(IsTransfered = 1 And IsEdit = 1 And IsTransferedBefore = 0) Or (IsReceivedTransfer = 1 And IsEdit = 1 And IsReceivedTransferBefore = 0)"), 0));
            lbTotalWeightSelected.Value = Convert.ToDecimal(Globals.IsNull(dtbFromFlex.Compute("Sum(TotalWeight)", "IsTransfered = 1 And IsEdit = 1 And IsTransferedBefore = 0"), 0));
            lbTotalSizeSelected.Value = Convert.ToDecimal(Globals.IsNull(dtbFromFlex.Compute("Sum(TotalSize)", "IsTransfered = 1 And IsEdit = 1 And IsTransferedBefore = 0"), 0));
            #endregion
        }

        //In lệnh điều động nội bộ
        private void mnuItemPrintStoreChangeReport_Click(object sender, EventArgs e)
        {
            String strStoreChangeID = Convert.ToString(flexStoreChange[flexStoreChange.RowSel, "StoreChangeID"]).Trim();
            String strUserCreate = Convert.ToString(flexStoreChange[flexStoreChange.RowSel, "CreatedUser"]).Trim();
            if (strUserCreate != SystemConfig.objSessionUser.UserName && SystemConfig.objSessionUser.UserName != "administrator")
            {
                MessageBox.Show(this, "Bạn không có quyền in lệnh điều động nội bộ này", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            // PrintStoreChangeReport(strStoreChangeID);
        }

        //In địa chỉ chuyển hàng A4
        private void mnuItemPrintStoreAddressA4_Click(object sender, EventArgs e)
        {
            if (flexStoreChange.Row < 1) return;
            string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            PrintStoreAddress(strStoreChangeID, intStoreChange3_A4);
        }

        /// <summary>
        /// In địa chỉ chuyển hàng
        /// </summary>
        private void PrintStoreAddress(string strStoreChangeID, int intReportID)
        {
            ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
            objReport.PrinterTypeID = "CommonPrinter";
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("StoreChangeID", strStoreChangeID);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        /// <summary>
        /// In lệnh điều động nội bộ.
        /// </summary>
        /// <param name="strStoreChangeID">Mã phiếu chuyển kho.</param>
        /// <returns></returns>
        public bool PrintStoreChangeReport(String strStorelChangeID)
        {
            bool result = false;
            ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intStoreChangeReportID);
            if (objReport != null)
            {
                objReport.PrinterTypeID = "CommonPrinter";
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("StoreChangeID", strStorelChangeID);
             //   htbParameterValue.Add("PrintUser", SystemConfig.objSessionUser.UserName);
             //   htbParameterValue.Add("UserPrintFullName", SystemConfig.objSessionUser.FullName);
                result = ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);//ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
                //return UpdatePrintTimes(strStoreChangeID);
            }
            else
            {
                MessageBox.Show(this, "Không tìm thấy thông tin cấu hình báo cáo " + intStoreChangeReportID + ", vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return result;
        }

      


        private void mnuItemPrintStoreAddressA5_Click(object sender, EventArgs e)
        {
            if (flexStoreChange.Row < 1) return;
            string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            PrintStoreAddress(strStoreChangeID, intStoreChange3_A5);
        }

        private void mnuItemPrintStoreAddressA6_Click(object sender, EventArgs e)
        {
            if (flexStoreChange.Row < 1) return;
            string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            PrintStoreAddress(strStoreChangeID, intStoreChange3_A6);
        }

        private void mnuItemPrintReport_Click(object sender, EventArgs e)
        {
            if (flexStoreChange.Row < 1) return;
            string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            PrintStoreChangeReport(strStoreChangeID);
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexStoreChange);
        }

        private void llblDate_Click(object sender, EventArgs e)
        {
            bolIsStoreChangeDate = !bolIsStoreChangeDate;
            if (bolIsStoreChangeDate)
                llblDate.Text = "Ngày chuyển:";
            else
                llblDate.Text = "Ngày nhận:";
        }

        private void txtKeyword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
                btnSearch_Click(null, null);
        }

        private void flexStoreChange_CellChanged(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row <= 1)
                return;
            int intFromStore = Convert.ToInt32(flexStoreChange.Rows[e.Row]["FromStoreID"]);
            int intToStore = Convert.ToInt32(flexStoreChange.Rows[e.Row]["ToStoreID"]);

            //int storeChangeTypeID = Convert.ToInt32(cboStoreChangeType.SelectedValue);
            DataTable dtbTemp = flexStoreChange.DataSource as DataTable;
            var item = dtbTemp.AsEnumerable().Where(x => x["StoreChangeID"].ToString().Trim().Equals(flexStoreChange.Rows[e.Row]["StoreChangeID"].ToString().Trim())).ToArray();
            int storeChangeTypeID = Convert.ToInt32(item[0]["storeChangeTypeID"]);
            bool bolIsnotCheckReceiver = false;
            if (storeChangeTypeID > 0)
            {
                MasterData.PLC.MD.PLCStoreChangeType objPLCStoreChangeType = new MasterData.PLC.MD.PLCStoreChangeType();
                MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType = objPLCStoreChangeType.LoadInfo(storeChangeTypeID);
                bolIsnotCheckReceiver = objStoreChangeType.IsNotCheckReceiver;
            }


            #region Đã chuyển - IsTransfered
            //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            if (e.Col == flexStoreChange.Cols["IsTransfered"].Index)
            {
                //Được check chọn "Đã chuyển" nếu:
                //Có quyền "PM_StoreChange_IsTransferedUpdate"
                //Có quyền trên Kho Xuất
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intFromStore, Library.AppCore.Constant.EnumType.StorePermissionType.OUTPUT))
                {
                    //Nếu chưa nhận hàng để vận chuyển thì được check Đã chuyển
                    if (!Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransfer"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"]))
                    {
                        flexStoreChange[e.Row, "IsTransfered"] = flexStoreChange[e.Row, "IsTransfered"];
                    }
                    else
                    {
                        flexStoreChange[e.Row, "IsTransfered"] = flexStoreChange[e.Row, "IsTransferedBefore"];
                        return;
                    }
                }
                else
                {
                    flexStoreChange[e.Row, "IsTransfered"] = flexStoreChange[e.Row, "IsTransferedBefore"];
                    return;
                }
            }

            //Được check chọn "Đã nhận" nếu:
            //Có quyền "PM_StoreChange_IsTransferedUpdate"
            if (e.Col == flexStoreChange.Cols["IsReceivedTransfer"].Index)
            {
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate))
                {
                    //Nếu đã check Đã chuyển, chưa ký Nhận hàng từ nhân viên vận chuyển và chưa check Đã nhận thì được check Đã nhận
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceive"])
                        && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]))
                    {
                        flexStoreChange[e.Row, "IsReceivedTransfer"] = flexStoreChange[e.Row, "IsReceivedTransfer"];
                    }
                    else
                    {
                        flexStoreChange[e.Row, "IsReceivedTransfer"] = flexStoreChange[e.Row, "IsReceivedTransferBefore"];
                        return;
                    }
                }
                else
                {
                    flexStoreChange[e.Row, "IsReceivedTransfer"] = flexStoreChange[e.Row, "IsReceivedTransferBefore"];
                    return;
                }

            }

            // Được check chọn "Đã ký nhận" nếu
            // Có quyền "PM_StoreChange_IsTransferedUpdate"
            // Có quyền "StoreChange_CheckIsReceive"
            // Có quyền trên kho nhập
            // Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            if (e.Col == flexStoreChange.Cols["IsSignReceive"].Index)
            {

                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
                    SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
                    (intCheckIsReceive > 0 ? (((SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) && !bolIsnotCheckReceiver) || (bolIsnotCheckReceiver && SystemConfig.objSessionUser.IsPermission("MD_STORECHANGETYPE_SIGNRECEIPT"))) : true))
                {
                    //((SystemConfig.objSessionUser.IsPermission("MD_STORECHANGETYPE_INPUTVALIDATION") && objStoreChangeType.IsNotCheckReceiver) || !objStoreChangeType.IsNotCheckReceiver)
                    //Nếu nhân viên vận chuyển chưa nhận hàng thì không được check
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceiveOld"]))
                    {
                        flexStoreChange[e.Row, "IsSignReceive"] = flexStoreChange[e.Row, "IsSignReceive"];
                    }
                    else
                    {
                        flexStoreChange[e.Row, "IsSignReceive"] = flexStoreChange[e.Row, "IsSignReceiveBefore"];
                        return;
                    }
                }
                else
                {
                    flexStoreChange[e.Row, "IsSignReceive"] = flexStoreChange[e.Row, "IsSignReceiveBefore"];
                    return;
                }
            }
            #endregion

            #region Đã nhận - IsReceive
            //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            if (e.Col == flexStoreChange.Cols["IsReceive"].Index)
            {
                //Được check chọn "Đã nhập" nếu:
                //Có quyền "strPermission_IsTransferedUpdate"
                //Có quyền "StoreChange_CheckIsReceive"
                //Có quyền trên Kho Nhập
                //Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
                    SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
                    (intCheckIsReceive > 0 ? (((SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) && !bolIsnotCheckReceiver) || (bolIsnotCheckReceiver && SystemConfig.objSessionUser.IsPermission("MD_STORECHANGETYPE_INPUTVALIDATION"))) : true))
                {
                    //Nếu check Đã ký nhận thì ko đc check Đã nhập liền, phải cập nhật trạng thái đã ký nhận
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceiveOld"]))
                    {
                        flexStoreChange[e.Row, "IsReceive"] = flexStoreChange[e.Row, "IsReceive"];
                    }
                    else
                    {
                        flexStoreChange[e.Row, "IsReceive"] = flexStoreChange[e.Row, "IsReceiveOld"];
                        return;
                    }
                }
                else
                {
                    flexStoreChange[e.Row, "IsReceive"] = flexStoreChange[e.Row, "IsReceiveOld"];
                    return;
                }
            }
            #endregion

        }

        private void mnuItemPrintInternalOutput_Transfer_Click(object sender, EventArgs e)
        {
            if (flexStoreChange.Row < 1) return;
            string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intInternalOutput_TransferID);
            objReport.PrinterTypeID = "CommonPrinter";
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("STORECHANGEID", strStoreChangeID);
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
            htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
            htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
            htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        private void mnuItemPrintStoreChangeCommand_Click(object sender, EventArgs e)
        {
            if (flexStoreChange.Row < 1) return;
            string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intStoreChangeCommandID);
            objReport.PrinterTypeID = "CommonPrinter";
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("STORECHANGEID", strStoreChangeID);
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
            htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
            htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
            htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        //Kiểm lấy số dòng
        private int GetNumOfLines(string multiPageString, int wrapWidth, Font fnt)
        {
            StringFormat sfFmt = new StringFormat(StringFormatFlags.LineLimit);
            using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
            {
                SizeF size = g.MeasureString(multiPageString, fnt,0, sfFmt);
                return (int)Math.Ceiling(size.Width / wrapWidth);
                
                //int iHeight = (int)(Math.Ceiling(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height));
                //int iOneLineHeight = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Height;
                //int oneChar = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Width;
                //int oneLine = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Width;
                //int oneLineHeight = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height;
                //return (int)(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height / g.MeasureString("W", fnt, wrapWidth, sfFmt).Height);
            }
        }

        //In phiếu xuất (In kim)
        private void mnuItemPrintInternalOuput_Click(object sender, EventArgs e)
        {
            if (flexStoreChange.Row < 1) return;
            string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            //Kiểm tra số dòng 
            DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(strStoreChangeID);
            int countRow = 0;
            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                Font font = new Font("Arial", 9.0f);
                int rowProductName = GetNumOfLines(strProductName, 140, new Font(font, FontStyle.Bold));
                countRow += rowProductName;
                if (countRow > 16)
                {
                    MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            if (countRow < 16)
            {
                int padding = 16 - countRow;
                for (int i = 0; i < padding; i++)
                {
                    DataRow rowPadding = dtData.NewRow();
                    dtData.Rows.Add(rowPadding);
                }
            }

            if (!string.IsNullOrEmpty(strStoreChangeID))
            {
                Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                frm.StoreChangeID = strStoreChangeID;
                frm.intIsStoreChangePrintInKim = 1;
                frm.DataTableSouce = dtData;
                frm.ShowDialog();
            }
        }


        private void itemOutputVoucherTransfer_Click(object sender, EventArgs e)
        {
            if (flexStoreChange.Row < 1) return;
            string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            //Kiểm tra số dòng 
            DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(strStoreChangeID);
            int countRow = 0;
            //dtData.AsEnumerable().OrderBy(p=>p["PRODUCTNAME"]);
            DataTable dtbRes = dtData.Clone();
            dtbRes.Columns.Add("PAGE", typeof(int));
            int numPage = 1;
            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                Font font = new Font("Times New Roman", 10.0f);
                //double a;
                int rowProductName = GetNumOfLines(strProductName, 248, new Font(font, FontStyle.Bold));
               
                countRow += rowProductName;
                if (countRow > 12)
                {
                    if (countRow - rowProductName < 12)
                    {
                        int padding = 12 - (countRow - rowProductName);
                        for (int j = 0; j < padding; j++)
                        {
                            //DataRow rowPadding = dtData.NewRow();
                            // dtData.Rows.Add(rowPadding);
                            DataRow rowPadding = dtbRes.NewRow();
                            rowPadding["PAGE"] = numPage;
                            dtbRes.Rows.Add(rowPadding);
                        }
                    }
                    numPage++;
                    //MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //return;
                    countRow = rowProductName;  
                                      
                }
                DataRow row = dtbRes.NewRow();
                row["PRODUCTNAME"] = strProductName;
                row["QUANTITYUNITNAME"] = dtData.Rows[i]["QUANTITYUNITNAME"].ToString().Trim();
                row["STT"] = Convert.ToInt32(dtData.Rows[i]["STT"]);
                row["QUANTITY"] = Convert.ToInt32(dtData.Rows[i]["QUANTITY"]);
                row["PAGE"] = numPage;
                dtbRes.Rows.Add(row);
            }
            if (countRow < 12)
            {
                int padding = 12 - countRow;
                for (int i = 0; i < padding; i++)
                {
                    //DataRow rowPadding = dtData.NewRow();
                    // dtData.Rows.Add(rowPadding);
                    DataRow rowPadding = dtbRes.NewRow();
                    rowPadding["PAGE"] = numPage;
                    dtbRes.Rows.Add(rowPadding);
                }
            }

            if (!string.IsNullOrEmpty(strStoreChangeID))
            {
                Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                frm.StoreChangeID = strStoreChangeID;
                frm.intIsOutputVoucherTransferInKim = 1;
                //frm.DataTableSouce = dtData;
                frm.DataTableSouce = dtbRes;
                frm.ShowDialog();
            }
        }

        private void mnuItemPrintStoreChangeHandOvers_Click(object sender, EventArgs e)
        {
            try
            {
                if (intPrintStoreChangeHandovers < 0) return;
                if (flexStoreChange.Row < 1) return;

                //lấy cấu hình xét Thẻ cào vật lý
                string strSubGroupID = string.Empty;
                DataTable dtbAppConfig = Library.AppCore.DataSource.GetDataSource.GetAppConfig();
                var itemAppConfig = dtbAppConfig.AsEnumerable().Where(p => p["APPCONFIGID"].ToString().Trim()
                                                    == "MD_SUBGROUP_PHISYCALCARD").FirstOrDefault();
                if (itemAppConfig != null)
                    strSubGroupID = itemAppConfig["CONFIGVALUE"].ToString();

                //Cắt chuỗi
                List<int> lstSubGroub = new List<int>();
                string[] strArray = strSubGroupID.Split(';');
                if (strArray != null && strArray.Count() > 0)
                {
                    foreach (string item in strArray)
                    {
                        int intSubGroup = 0;
                        int.TryParse(item.Trim(), out intSubGroup);
                        if (intSubGroup != 0) lstSubGroub.Add(intSubGroup);
                    }

                }
                //Chạy hàm 
                string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString().Trim();
                object[] objKeywords = new object[] { "@STORECHANGEID", strStoreChangeID };
                DataTable dtbFull = new ERP.Report.PLC.PLCReportDataSource().GetHeavyDataSource("RPT_STORECHANGE_HANDOVERS", objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuItemPrintStoreChangeHandOvers_Click");
                }
                var lstJoinData = from o in dtbFull.AsEnumerable()
                                  join i in lstSubGroub
                                  on Convert.ToInt32(o.Field<object>("SUBGROUPID")) equals i
                                  select o;
                if (dtbFull.Rows.Count > 0 && dtbFull != null)
                {
                    DataTable dtbReport = dtbFull.Copy();
                    if (lstJoinData != null && lstJoinData.Count() > 0)
                    {
                        #region Xóa phần phải kiểm tra
                        foreach (int j in lstSubGroub)
                        {
                            DataRow[] drr = dtbReport.Select("SUBGROUPID= '" + j + "'");
                            if (drr != null && drr.Count() > 0)
                            {
                                foreach (var item in drr)
                                    dtbReport.Rows.Remove(item);
                            }
                        }
                        #endregion
                        DataTable dtbTemp = lstJoinData.CopyToDataTable();
                        int i = 1;
                        foreach (DataRow dr in dtbTemp.Rows)
                        {
                            #region Đầu hàng
                            if (i == 1)
                            {
                                dtbReport.ImportRow(dr);
                                dtbReport.AcceptChanges();
                                dtbFull.AcceptChanges();
                            }
                            #endregion
                            #region Cuối hàng
                            if (i == lstJoinData.Count() && i > 1)
                            {
                                var temp = dtbReport.AsEnumerable().OrderByDescending(p => p.Field<object>("STT")).FirstOrDefault();
                                int intSTT = Convert.ToInt32(temp.Field<object>("STT"));

                                string strFullName = dr["FULLNAME"].ToString().Trim();
                                decimal decIMEI = 0;
                                if (!Convert.IsDBNull(dr["IMEI"]))
                                    decIMEI = Convert.ToDecimal(dr["IMEI"].ToString().Trim());
                                //bool bolImport = true;
                                DataRow drCopyBefore = dtbTemp.Rows[i - 2];
                                string strFullNameCopyBefore = drCopyBefore["FULLNAME"].ToString().Trim();
                                decimal decIMEICopyBefore = 0;
                                if (!Convert.IsDBNull(drCopyBefore["IMEI"]))
                                    decIMEICopyBefore = Convert.ToDecimal(drCopyBefore["IMEI"].ToString().Trim());

                                if (strFullName == strFullNameCopyBefore)
                                {
                                    if (decIMEI == decIMEICopyBefore + 1)
                                    {
                                        dr["REQUESTQUANTITY"] = -1;
                                        dr["REALQUANTITY"] = -1;

                                        DataRow[] drr = dtbReport.Select("STT = '" + intSTT + "'");
                                        if (drr != null && drr.Count() > 0)
                                        {
                                            drr[0]["REQUESTQUANTITY"] = Convert.ToInt32(drr[0]["REQUESTQUANTITY"]) + 1;
                                            drr[0]["REALQUANTITY"] = Convert.ToInt32(drr[0]["REALQUANTITY"]) + 1;
                                        }
                                        //bolImport = false;
                                        //temp["IMEI"] = temp.Field<object>("IMEI").ToString().Trim() + " - " + dr["IMEI"].ToString().Trim();
                                    }
                                }
                                dtbReport.ImportRow(dr);

                            }
                            #endregion
                            #region Giữa hàng
                            if (i > 1 && i < lstJoinData.Count())
                            {
                                var temp = dtbReport.AsEnumerable().OrderByDescending(p => p.Field<object>("STT")).FirstOrDefault();
                                int intSTT = Convert.ToInt32(temp.Field<object>("STT"));

                                DataRow drCopyBefore = dtbTemp.Rows[i - 2];
                                string strFullNameCopyBefore = drCopyBefore["FULLNAME"].ToString().Trim();
                                decimal decIMEICopyBefore = 0;
                                if (!Convert.IsDBNull(drCopyBefore["IMEI"]))
                                {
                                    decIMEICopyBefore = Convert.ToDecimal(drCopyBefore.Field<object>("IMEI"));
                                }

                                string strFullName = dr["FULLNAME"].ToString().Trim();
                                decimal decIMEI = Convert.ToDecimal(dr["IMEI"].ToString().Trim());

                                DataRow drCopyAfter = dtbTemp.Rows[i];
                                string strFullNameCopyAfter = drCopyAfter["FULLNAME"].ToString().Trim();
                                decimal decIMEICopyAfter = 0;
                                if (!Convert.IsDBNull(drCopyAfter["IMEI"]))
                                {
                                    decIMEICopyAfter = Convert.ToDecimal(drCopyAfter.Field<object>("IMEI"));
                                }
                                //Xét cùng sản phẩm
                                if (strFullName == strFullNameCopyBefore)
                                {
                                    if (decIMEI != decIMEICopyBefore + 1)
                                    {
                                        dtbReport.ImportRow(dr);
                                    }
                                    //Số kế tiếp bằng với số phía trước + 1 
                                    if (decIMEI == decIMEICopyBefore + 1)
                                    {
                                        DataRow[] drr = dtbReport.Select("STT = '" + intSTT + "'");
                                        if (drr != null && drr.Count() > 0)
                                        {
                                            drr[0]["REQUESTQUANTITY"] = Convert.ToInt32(drr[0]["REQUESTQUANTITY"]) + 1;
                                            drr[0]["REALQUANTITY"] = Convert.ToInt32(drr[0]["REALQUANTITY"]) + 1;
                                        }
                                        ////Ngắt quãng
                                        if (decIMEI != decIMEICopyAfter - 1)
                                        {
                                            dr["REQUESTQUANTITY"] = -1;
                                            dr["REALQUANTITY"] = -1;
                                            //Ân gộp dòng imei
                                            //temp["IMEI"] = temp.Field<object>("IMEI").ToString().Trim() + " - " + dr["IMEI"].ToString().Trim();
                                            dtbReport.ImportRow(dr);
                                        }
                                        //Tiếp tục quãng
                                    }
                                    // dtbReport.ImportRow(drCopyBefore);
                                    //}
                                }
                                else
                                    dtbReport.ImportRow(dr);
                            }
                            #endregion
                            i++;
                        }

                    }
                    DataTable dtbReportFinal = dtbReport.Copy();
                    DataTable dtbSubGroup = dtbReport.Copy();
                    dtbSubGroup.Clear();

                    // Xóa phần sub group
                    foreach (int j in lstSubGroub)
                    {
                        DataRow[] drr = dtbReportFinal.Select("SUBGROUPID= '" + j + "'");
                        if (drr != null && drr.Count() > 0)
                        {
                            foreach (var item in drr)
                            {
                                dtbSubGroup.ImportRow(item);
                                if (item["REQUESTQUANTITY"].ToString() == "-1")
                                    dtbReportFinal.Rows.Remove(item);

                            }
                        }
                    }

                    //DataTable dtbAfterSubGroup = dtbReport.AsEnumerable().Where(p => p["SUBGROUPID"].ToString() != "-1").CopyToDataTable().Copy();


                    //DataTable dtbClone = 

                    //Lấy phần cần subgroup
                    int k = 0;
                    foreach (DataRow dr in dtbSubGroup.Rows)
                    {
                        if (Convert.ToString(dr["REQUESTQUANTITY"]) == "-1")
                        {
                            DataRow drCopyBefore = dtbSubGroup.Rows[k - 1];
                            string strIMEI = drCopyBefore["IMEI"].ToString().Trim();

                            DataRow[] drrTemp = dtbReportFinal.Select("IMEI = '" + strIMEI + "'");
                            // drrTemp[0]["IMEI"] = strIMEI + " - " + dr["IMEI"].ToString().Trim();
                            drrTemp[0]["IMEI"] = "Từ: " + strIMEI + "\nĐến: " + dr["IMEI"].ToString().Trim();
                        }
                        k++;
                    }

                    dtbReport.Clear();
                    dtbReport = dtbReportFinal;
                    dtbReport.DefaultView.Sort = "STT asc";
                    int stt_Final = 0;
                    string strName = string.Empty;
                    for (int i = 0; i < dtbReport.Rows.Count; i++)
                    {
                        if (!dtbReport.Rows[i]["FULLNAME"].ToString().Trim().Equals(strName.Trim()))
                        {
                            strName = dtbReport.Rows[i]["FULLNAME"].ToString().Trim();
                            stt_Final += 1;
                            dtbReport.Rows[i]["STT"] = stt_Final;
                        }
                    }
                    DataTable dtbClone = dtbReport;//.DefaultView.ToTable(true, "STT", "FULLNAME", "QUANTITYUNITNAME", "REQUESTQUANTITY", "REALQUANTITY", "IMEI", "SUBGROUPID", "PRODUCTID");
                    // Quăng vào form Report
                    if (!string.IsNullOrEmpty(strStoreChangeID))
                    {
                        ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intPrintStoreChangeHandovers);
                        if (objReport == null)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin in biên bản bàn giao");
                            return;
                        }

                        object[] objKeywords2 = new object[] { "@STORECHANGEID", strStoreChangeID };
                        Hashtable htbParameterValue = new Hashtable();
                        DataTable dtbInfo = new ERP.Report.PLC.PLCReportDataSource().GetHeavyDataSource("RPT_STORECHANGE_HANDOVERS_INFO", objKeywords2);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuItemPrintStoreChangeHandOvers_Click");
                        }
                        if (dtbInfo != null && dtbInfo.Rows.Count > 0)
                        {
                            htbParameterValue.Add("CompanyName", dtbInfo.Rows[0]["FromstoreName"].ToString().Trim());
                            htbParameterValue.Add("CompanyAddress", dtbInfo.Rows[0]["FromstoreAddress"].ToString().Trim());
                            htbParameterValue.Add("OrderReason", dtbInfo.Rows[0]["CONTENT"].ToString().Trim());
                            htbParameterValue.Add("StoreName", dtbInfo.Rows[0]["STORENAME"].ToString().Trim());
                            htbParameterValue.Add("StoreAddress", dtbInfo.Rows[0]["STOREADDRESS"].ToString().Trim());
                            htbParameterValue.Add("PrintDate", Convert.ToDateTime(dtbInfo.Rows[0]["STORECHANGEDATE"]));
                            htbParameterValue.Add("ToUser", dtbInfo.Rows[0]["FULLNAME"].ToString().Trim());
                            htbParameterValue.Add("CodeNumber", dtbInfo.Rows[0]["CODE"].ToString().Trim());
                        }
                        else
                        {
                            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
                            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
                            htbParameterValue.Add("OrderReason", string.Empty);
                            htbParameterValue.Add("StoreName", string.Empty);
                            htbParameterValue.Add("StoreAddress", string.Empty);
                            htbParameterValue.Add("PrintDate", DateTime.Now);
                            htbParameterValue.Add("ToUser", string.Empty);
                            htbParameterValue.Add("CodeNumber", string.Empty);
                        }
                        htbParameterValue.Add("STORECHANGEORDERID", strStoreChangeID);
                        dtbReport.Columns.Remove("SUBGROUPID");
                        dtbReport.Columns.Remove("PRODUCTID");
                        objReport.DataTableSource = dtbReport;
                        ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //else
            //    MessageBox.Show(this, "Lỗi in biên bản bàn giao!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        
    }
}