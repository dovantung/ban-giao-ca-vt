﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmStoreChangeOrderProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStoreChangeOrderProduct));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.TabManager = new System.Windows.Forms.TabPage();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.mnuReview = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmdExcelReview = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.OtherTransferID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Note = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FromStoreID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ToStoreID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TransferDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.OtherProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Createduser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UpdateUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UpdateDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.cboStoreChangeStatusManager = new System.Windows.Forms.ComboBox();
            this.comboToStore2 = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cbxFromManager = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.btnExportToExcel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chkDelete = new System.Windows.Forms.CheckBox();
            this.txtKeyWords = new System.Windows.Forms.TextBox();
            this.ucCreateUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.cboTransferOtherPro = new System.Windows.Forms.ComboBox();
            this.dteToDate = new System.Windows.Forms.DateTimePicker();
            this.dteFromDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cboTransportTypeID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label31 = new System.Windows.Forms.Label();
            this.cboTransportServicesID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label10 = new System.Windows.Forms.Label();
            this.cboTransportCompanyID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label13 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.cboStoreChangeStatusDT = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cboTransferOtherProDt = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboFromStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboToStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.dteExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.dteOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtStoreChangeOrderID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnTransfer = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.TabManager.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.mnuReview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.tabControl);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 461);
            this.panel1.TabIndex = 1;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.TabManager);
            this.tabControl.Controls.Add(this.tabGeneral);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(984, 424);
            this.tabControl.TabIndex = 0;
            // 
            // TabManager
            // 
            this.TabManager.Controls.Add(this.grdData);
            this.TabManager.Controls.Add(this.groupControl4);
            this.TabManager.Location = new System.Drawing.Point(4, 25);
            this.TabManager.Name = "TabManager";
            this.TabManager.Size = new System.Drawing.Size(976, 395);
            this.TabManager.TabIndex = 2;
            this.TabManager.Text = "Quản lý yêu cầu điều chuyển hàng hóa khác";
            this.TabManager.UseVisualStyleBackColor = true;
            this.TabManager.Enter += new System.EventHandler(this.TabManager_Enter);
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(0, 111);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3});
            this.grdData.Size = new System.Drawing.Size(976, 284);
            this.grdData.TabIndex = 0;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            this.grdData.DoubleClick += new System.EventHandler(this.grdData_DoubleClick);
            // 
            // mnuReview
            // 
            this.mnuReview.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdExcelReview,
            this.toolStripMenuItem1});
            this.mnuReview.Name = "contextMenuStrip1";
            this.mnuReview.Size = new System.Drawing.Size(130, 48);
            // 
            // cmdExcelReview
            // 
            this.cmdExcelReview.Enabled = false;
            this.cmdExcelReview.Image = ((System.Drawing.Image)(resources.GetObject("cmdExcelReview.Image")));
            this.cmdExcelReview.Name = "cmdExcelReview";
            this.cmdExcelReview.Size = new System.Drawing.Size(129, 22);
            this.cmdExcelReview.Text = "Xuất excel";
            this.cmdExcelReview.Click += new System.EventHandler(this.cmdExcelReview_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(129, 22);
            this.toolStripMenuItem1.Text = "Thêm mới";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.OtherTransferID,
            this.Note,
            this.FromStoreID,
            this.ToStoreID,
            this.StatusID,
            this.TransferDate,
            this.OtherProduct,
            this.Createduser,
            this.gridColumn1,
            this.UpdateUser,
            this.UpdateDate});
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsBehavior.Editable = false;
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewData.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            // 
            // OtherTransferID
            // 
            this.OtherTransferID.AppearanceHeader.Options.UseTextOptions = true;
            this.OtherTransferID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.OtherTransferID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.OtherTransferID.Caption = "Mã phiếu chuyển";
            this.OtherTransferID.FieldName = "OTHERTRANSFERID";
            this.OtherTransferID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.OtherTransferID.Name = "OtherTransferID";
            this.OtherTransferID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.OtherTransferID.Visible = true;
            this.OtherTransferID.VisibleIndex = 0;
            this.OtherTransferID.Width = 120;
            // 
            // Note
            // 
            this.Note.AppearanceHeader.Options.UseTextOptions = true;
            this.Note.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Note.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Note.Caption = "Nội Dung";
            this.Note.FieldName = "CONTENT";
            this.Note.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.Note.Name = "Note";
            this.Note.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.Note.Visible = true;
            this.Note.VisibleIndex = 1;
            this.Note.Width = 200;
            // 
            // FromStoreID
            // 
            this.FromStoreID.AppearanceHeader.Options.UseTextOptions = true;
            this.FromStoreID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FromStoreID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FromStoreID.Caption = "Kho gửi";
            this.FromStoreID.FieldName = "FROMSTOREID";
            this.FromStoreID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.FromStoreID.Name = "FromStoreID";
            this.FromStoreID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.FromStoreID.Visible = true;
            this.FromStoreID.VisibleIndex = 2;
            this.FromStoreID.Width = 200;
            // 
            // ToStoreID
            // 
            this.ToStoreID.AppearanceCell.Options.UseTextOptions = true;
            this.ToStoreID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ToStoreID.AppearanceHeader.Options.UseTextOptions = true;
            this.ToStoreID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ToStoreID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ToStoreID.Caption = "Kho nhận";
            this.ToStoreID.FieldName = "TOSTOREID";
            this.ToStoreID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.ToStoreID.Name = "ToStoreID";
            this.ToStoreID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.ToStoreID.Visible = true;
            this.ToStoreID.VisibleIndex = 3;
            this.ToStoreID.Width = 200;
            // 
            // StatusID
            // 
            this.StatusID.AppearanceCell.Options.UseTextOptions = true;
            this.StatusID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.StatusID.AppearanceHeader.Options.UseTextOptions = true;
            this.StatusID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.StatusID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.StatusID.Caption = "Trạng thái hàng";
            this.StatusID.FieldName = "STATUSID";
            this.StatusID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.StatusID.Name = "StatusID";
            this.StatusID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.StatusID.Visible = true;
            this.StatusID.VisibleIndex = 4;
            this.StatusID.Width = 120;
            // 
            // TransferDate
            // 
            this.TransferDate.AppearanceCell.Options.UseTextOptions = true;
            this.TransferDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TransferDate.AppearanceHeader.Options.UseTextOptions = true;
            this.TransferDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TransferDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TransferDate.Caption = "Ngày gửi";
            this.TransferDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.TransferDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TransferDate.FieldName = "TRANSFERDATE";
            this.TransferDate.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.TransferDate.Name = "TransferDate";
            this.TransferDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TransferDate.Visible = true;
            this.TransferDate.VisibleIndex = 5;
            this.TransferDate.Width = 120;
            // 
            // OtherProduct
            // 
            this.OtherProduct.AppearanceHeader.Options.UseTextOptions = true;
            this.OtherProduct.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.OtherProduct.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.OtherProduct.Caption = "Loại hàng hóa";
            this.OtherProduct.FieldName = "OTHERPRODUCT";
            this.OtherProduct.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.OtherProduct.Name = "OtherProduct";
            this.OtherProduct.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.OtherProduct.Visible = true;
            this.OtherProduct.VisibleIndex = 6;
            this.OtherProduct.Width = 119;
            // 
            // Createduser
            // 
            this.Createduser.AppearanceHeader.Options.UseTextOptions = true;
            this.Createduser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Createduser.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Createduser.Caption = "Người tạo";
            this.Createduser.FieldName = "CREATEDUSER";
            this.Createduser.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.Createduser.Name = "Createduser";
            this.Createduser.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.Createduser.Visible = true;
            this.Createduser.VisibleIndex = 7;
            this.Createduser.Width = 220;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Ngày tạo";
            this.gridColumn1.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn1.FieldName = "CREATEDATE";
            this.gridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 8;
            this.gridColumn1.Width = 120;
            // 
            // UpdateUser
            // 
            this.UpdateUser.AppearanceHeader.Options.UseTextOptions = true;
            this.UpdateUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UpdateUser.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.UpdateUser.Caption = "Người cập nhật";
            this.UpdateUser.FieldName = "UPDATEUSER";
            this.UpdateUser.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.UpdateUser.Name = "UpdateUser";
            this.UpdateUser.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.UpdateUser.Visible = true;
            this.UpdateUser.VisibleIndex = 9;
            this.UpdateUser.Width = 220;
            // 
            // UpdateDate
            // 
            this.UpdateDate.AppearanceCell.Options.UseTextOptions = true;
            this.UpdateDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UpdateDate.AppearanceHeader.Options.UseTextOptions = true;
            this.UpdateDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UpdateDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.UpdateDate.Caption = "Ngày cập nhật";
            this.UpdateDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.UpdateDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.UpdateDate.FieldName = "UPDATEDATE";
            this.UpdateDate.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.UpdateDate.Name = "UpdateDate";
            this.UpdateDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.UpdateDate.Visible = true;
            this.UpdateDate.VisibleIndex = 10;
            this.UpdateDate.Width = 120;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit2.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit3.ValueUnchecked = ((short)(0));
            // 
            // groupControl4
            // 
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.cboStoreChangeStatusManager);
            this.groupControl4.Controls.Add(this.comboToStore2);
            this.groupControl4.Controls.Add(this.cbxFromManager);
            this.groupControl4.Controls.Add(this.btnExportToExcel);
            this.groupControl4.Controls.Add(this.btnSearch);
            this.groupControl4.Controls.Add(this.chkDelete);
            this.groupControl4.Controls.Add(this.txtKeyWords);
            this.groupControl4.Controls.Add(this.ucCreateUser);
            this.groupControl4.Controls.Add(this.cboTransferOtherPro);
            this.groupControl4.Controls.Add(this.dteToDate);
            this.groupControl4.Controls.Add(this.dteFromDate);
            this.groupControl4.Controls.Add(this.label8);
            this.groupControl4.Controls.Add(this.label12);
            this.groupControl4.Controls.Add(this.label14);
            this.groupControl4.Controls.Add(this.label15);
            this.groupControl4.Controls.Add(this.label16);
            this.groupControl4.Controls.Add(this.label18);
            this.groupControl4.Controls.Add(this.label20);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(0, 0);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.ShowCaption = false;
            this.groupControl4.Size = new System.Drawing.Size(976, 111);
            this.groupControl4.TabIndex = 1;
            this.groupControl4.Text = "Thông tin tìm kiếm";
            // 
            // cboStoreChangeStatusManager
            // 
            this.cboStoreChangeStatusManager.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStoreChangeStatusManager.FormattingEnabled = true;
            this.cboStoreChangeStatusManager.Location = new System.Drawing.Point(86, 67);
            this.cboStoreChangeStatusManager.Name = "cboStoreChangeStatusManager";
            this.cboStoreChangeStatusManager.Size = new System.Drawing.Size(185, 24);
            this.cboStoreChangeStatusManager.TabIndex = 12;
            // 
            // comboToStore2
            // 
            this.comboToStore2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboToStore2.Location = new System.Drawing.Point(393, 35);
            this.comboToStore2.Margin = new System.Windows.Forms.Padding(0);
            this.comboToStore2.MinimumSize = new System.Drawing.Size(24, 24);
            this.comboToStore2.Name = "comboToStore2";
            this.comboToStore2.Size = new System.Drawing.Size(193, 24);
            this.comboToStore2.TabIndex = 10;
            // 
            // cbxFromManager
            // 
            this.cbxFromManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxFromManager.Location = new System.Drawing.Point(86, 35);
            this.cbxFromManager.Margin = new System.Windows.Forms.Padding(0);
            this.cbxFromManager.MinimumSize = new System.Drawing.Size(24, 24);
            this.cbxFromManager.Name = "cbxFromManager";
            this.cbxFromManager.Size = new System.Drawing.Size(184, 24);
            this.cbxFromManager.TabIndex = 8;
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportToExcel.Appearance.Options.UseFont = true;
            this.btnExportToExcel.Enabled = false;
            this.btnExportToExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportToExcel.Location = new System.Drawing.Point(794, 63);
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Size = new System.Drawing.Size(105, 26);
            this.btnExportToExcel.TabIndex = 17;
            this.btnExportToExcel.Text = "Xuất Excel";
            this.btnExportToExcel.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(682, 65);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(106, 24);
            this.btnSearch.TabIndex = 16;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // chkDelete
            // 
            this.chkDelete.AutoSize = true;
            this.chkDelete.Location = new System.Drawing.Point(603, 69);
            this.chkDelete.Name = "chkDelete";
            this.chkDelete.Size = new System.Drawing.Size(68, 20);
            this.chkDelete.TabIndex = 15;
            this.chkDelete.Text = "Đã hủy";
            this.chkDelete.UseVisualStyleBackColor = true;
            // 
            // txtKeyWords
            // 
            this.txtKeyWords.Location = new System.Drawing.Point(393, 67);
            this.txtKeyWords.Name = "txtKeyWords";
            this.txtKeyWords.Size = new System.Drawing.Size(193, 22);
            this.txtKeyWords.TabIndex = 14;
            // 
            // ucCreateUser
            // 
            this.ucCreateUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucCreateUser.IsOnlyShowRealStaff = false;
            this.ucCreateUser.IsValidate = true;
            this.ucCreateUser.Location = new System.Drawing.Point(682, 10);
            this.ucCreateUser.Margin = new System.Windows.Forms.Padding(4);
            this.ucCreateUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucCreateUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucCreateUser.Name = "ucCreateUser";
            this.ucCreateUser.Size = new System.Drawing.Size(217, 22);
            this.ucCreateUser.TabIndex = 6;
            this.ucCreateUser.UserName = "";
            this.ucCreateUser.WorkingPositionID = 0;
            // 
            // cboTransferOtherPro
            // 
            this.cboTransferOtherPro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransferOtherPro.FormattingEnabled = true;
            this.cboTransferOtherPro.Location = new System.Drawing.Point(393, 8);
            this.cboTransferOtherPro.Name = "cboTransferOtherPro";
            this.cboTransferOtherPro.Size = new System.Drawing.Size(193, 24);
            this.cboTransferOtherPro.TabIndex = 4;
            // 
            // dteToDate
            // 
            this.dteToDate.CustomFormat = "dd/MM/yyyy";
            this.dteToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteToDate.Location = new System.Drawing.Point(185, 10);
            this.dteToDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(86, 22);
            this.dteToDate.TabIndex = 2;
            // 
            // dteFromDate
            // 
            this.dteFromDate.CustomFormat = "dd/MM/yyyy";
            this.dteFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteFromDate.Location = new System.Drawing.Point(87, 10);
            this.dteFromDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(92, 22);
            this.dteFromDate.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(282, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 16);
            this.label8.TabIndex = 9;
            this.label8.Text = "Kho nhập:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 16);
            this.label12.TabIndex = 7;
            this.label12.Text = "Kho xuất:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(282, 70);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 16);
            this.label14.TabIndex = 13;
            this.label14.Text = "Mã yêu cầu:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 16);
            this.label15.TabIndex = 11;
            this.label15.Text = "Trạng thái:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(602, 13);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 16);
            this.label16.TabIndex = 5;
            this.label16.Text = "Người tạo:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(66, 16);
            this.label18.TabIndex = 0;
            this.label18.Text = "Ngày tạo:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(282, 12);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 16);
            this.label20.TabIndex = 3;
            this.label20.Text = "Loại hàng gửi:";
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.groupControl1);
            this.tabGeneral.Location = new System.Drawing.Point(4, 25);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneral.Size = new System.Drawing.Size(976, 395);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "Chi tiết";
            this.tabGeneral.UseVisualStyleBackColor = true;
            this.tabGeneral.Enter += new System.EventHandler(this.tabGeneral_Enter);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.cboTransportTypeID);
            this.groupControl1.Controls.Add(this.label31);
            this.groupControl1.Controls.Add(this.cboTransportServicesID);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.cboTransportCompanyID);
            this.groupControl1.Controls.Add(this.label13);
            this.groupControl1.Controls.Add(this.label30);
            this.groupControl1.Controls.Add(this.cboStoreChangeStatusDT);
            this.groupControl1.Controls.Add(this.label11);
            this.groupControl1.Controls.Add(this.cboTransferOtherProDt);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.cboFromStoreID);
            this.groupControl1.Controls.Add(this.cboToStoreID);
            this.groupControl1.Controls.Add(this.txtContent);
            this.groupControl1.Controls.Add(this.dteExpiryDate);
            this.groupControl1.Controls.Add(this.dteOrderDate);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.txtStoreChangeOrderID);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(970, 389);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin yêu cầu";
            // 
            // cboTransportTypeID
            // 
            this.cboTransportTypeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransportTypeID.Location = new System.Drawing.Point(158, 97);
            this.cboTransportTypeID.Margin = new System.Windows.Forms.Padding(0);
            this.cboTransportTypeID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboTransportTypeID.Name = "cboTransportTypeID";
            this.cboTransportTypeID.Size = new System.Drawing.Size(308, 24);
            this.cboTransportTypeID.TabIndex = 11;
            this.cboTransportTypeID.SelectionChangeCommitted += new System.EventHandler(this.cboTransportTypeID_SelectionChangeCommitted);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(76, 160);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(14, 16);
            this.label31.TabIndex = 18;
            this.label31.Text = "*";
            // 
            // cboTransportServicesID
            // 
            this.cboTransportServicesID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransportServicesID.Location = new System.Drawing.Point(158, 157);
            this.cboTransportServicesID.Margin = new System.Windows.Forms.Padding(0);
            this.cboTransportServicesID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboTransportServicesID.Name = "cboTransportServicesID";
            this.cboTransportServicesID.Size = new System.Drawing.Size(308, 24);
            this.cboTransportServicesID.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 16);
            this.label10.TabIndex = 96;
            this.label10.Text = "Dịch vụ:";
            // 
            // cboTransportCompanyID
            // 
            this.cboTransportCompanyID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransportCompanyID.Location = new System.Drawing.Point(158, 129);
            this.cboTransportCompanyID.Margin = new System.Windows.Forms.Padding(0);
            this.cboTransportCompanyID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboTransportCompanyID.Name = "cboTransportCompanyID";
            this.cboTransportCompanyID.Size = new System.Drawing.Size(308, 24);
            this.cboTransportCompanyID.TabIndex = 15;
            this.cboTransportCompanyID.SelectionChangeCommitted += new System.EventHandler(this.cboTransportCompany_SelectionChangeCommitted);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 133);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 16);
            this.label13.TabIndex = 14;
            this.label13.Text = "Đối tác vận chuyển:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(137, 133);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(14, 16);
            this.label30.TabIndex = 94;
            this.label30.Text = "*";
            // 
            // cboStoreChangeStatusDT
            // 
            this.cboStoreChangeStatusDT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStoreChangeStatusDT.Enabled = false;
            this.cboStoreChangeStatusDT.FormattingEnabled = true;
            this.cboStoreChangeStatusDT.Location = new System.Drawing.Point(589, 125);
            this.cboStoreChangeStatusDT.Name = "cboStoreChangeStatusDT";
            this.cboStoreChangeStatusDT.Size = new System.Drawing.Size(349, 24);
            this.cboStoreChangeStatusDT.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(485, 128);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 16);
            this.label11.TabIndex = 16;
            this.label11.Text = "Trạng thái:";
            // 
            // cboTransferOtherProDt
            // 
            this.cboTransferOtherProDt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransferOtherProDt.FormattingEnabled = true;
            this.cboTransferOtherProDt.Location = new System.Drawing.Point(588, 92);
            this.cboTransferOtherProDt.Name = "cboTransferOtherProDt";
            this.cboTransferOtherProDt.Size = new System.Drawing.Size(350, 24);
            this.cboTransferOtherProDt.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(485, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Loại hàng gửi:";
            // 
            // cboFromStoreID
            // 
            this.cboFromStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreID.Location = new System.Drawing.Point(158, 67);
            this.cboFromStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreID.Name = "cboFromStoreID";
            this.cboFromStoreID.Size = new System.Drawing.Size(308, 24);
            this.cboFromStoreID.TabIndex = 7;
            this.cboFromStoreID.SelectionChangeCommitted += new System.EventHandler(this.cboFromStoreID_SelectionChangeCommitted);
            // 
            // cboToStoreID
            // 
            this.cboToStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreID.Location = new System.Drawing.Point(588, 59);
            this.cboToStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreID.Name = "cboToStoreID";
            this.cboToStoreID.Size = new System.Drawing.Size(350, 24);
            this.cboToStoreID.TabIndex = 9;
            this.cboToStoreID.SelectionChangeCommitted += new System.EventHandler(this.cboToStoreID_SelectionChangeCommitted);
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(158, 189);
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(780, 135);
            this.txtContent.TabIndex = 22;
            // 
            // dteExpiryDate
            // 
            this.dteExpiryDate.CustomFormat = "dd/MM/yyyy ";
            this.dteExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteExpiryDate.Location = new System.Drawing.Point(829, 31);
            this.dteExpiryDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dteExpiryDate.Name = "dteExpiryDate";
            this.dteExpiryDate.Size = new System.Drawing.Size(109, 22);
            this.dteExpiryDate.TabIndex = 5;
            // 
            // dteOrderDate
            // 
            this.dteOrderDate.CustomFormat = "dd/MM/yyyy ";
            this.dteOrderDate.Enabled = false;
            this.dteOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteOrderDate.Location = new System.Drawing.Point(587, 31);
            this.dteOrderDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dteOrderDate.Name = "dteOrderDate";
            this.dteOrderDate.Size = new System.Drawing.Size(137, 22);
            this.dteOrderDate.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(485, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "Kho nhập:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(737, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngày hết hạn:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Kho xuất:";
            // 
            // txtStoreChangeOrderID
            // 
            this.txtStoreChangeOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtStoreChangeOrderID.Location = new System.Drawing.Point(158, 34);
            this.txtStoreChangeOrderID.Name = "txtStoreChangeOrderID";
            this.txtStoreChangeOrderID.ReadOnly = true;
            this.txtStoreChangeOrderID.Size = new System.Drawing.Size(308, 22);
            this.txtStoreChangeOrderID.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 192);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "Nội dung:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Phương tiện:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(485, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ngày yêu cầu:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã yêu cầu:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Info;
            this.panel2.Controls.Add(this.btnTransfer);
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Controls.Add(this.btnUpdate);
            this.panel2.Controls.Add(this.btncancel);
            this.panel2.Controls.Add(this.btnDelete);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 424);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(984, 37);
            this.panel2.TabIndex = 1;
            // 
            // btnTransfer
            // 
            this.btnTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTransfer.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransfer.Appearance.Options.UseFont = true;
            this.btnTransfer.Image = global::ERP.Inventory.DUI.Properties.Resources._out;
            this.btnTransfer.Location = new System.Drawing.Point(747, 8);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(117, 23);
            this.btnTransfer.TabIndex = 3;
            this.btnTransfer.Text = "Chuyển vận đơn";
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Appearance.Options.UseFont = true;
            this.btnAdd.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.btnAdd.Location = new System.Drawing.Point(445, 8);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(86, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Thêm mới";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(646, 8);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btncancel
            // 
            this.btncancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btncancel.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.btncancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btncancel.Location = new System.Drawing.Point(870, 8);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(106, 24);
            this.btncancel.TabIndex = 4;
            this.btncancel.Text = "Bỏ qua";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(537, 8);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(103, 25);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Hủy Phiếu";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // frmStoreChangeOrderProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1000, 400);
            this.Name = "frmStoreChangeOrderProduct";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý yêu cầu chuyển kho";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmStoreChangeOrder_FormClosing);
            this.Load += new System.EventHandler(this.frmStoreChangeOrder_Load);
            this.panel1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.TabManager.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.mnuReview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            this.tabGeneral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnUpdate;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.DateTimePicker dteExpiryDate;
        private System.Windows.Forms.DateTimePicker dteOrderDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStoreChangeOrderID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ContextMenuStrip mnuReview;
        private System.Windows.Forms.ToolStripMenuItem cmdExcelReview;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreID;
        private System.Windows.Forms.TabPage TabManager;
        private System.Windows.Forms.Button btncancel;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboTransferOtherProDt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboStoreChangeStatusDT;
        private DevExpress.XtraEditors.SimpleButton btnTransfer;
        private System.Windows.Forms.Button btnDelete;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraGrid.Columns.GridColumn OtherTransferID;
        private DevExpress.XtraGrid.Columns.GridColumn Note;
        private DevExpress.XtraGrid.Columns.GridColumn FromStoreID;
        private DevExpress.XtraGrid.Columns.GridColumn ToStoreID;
        private DevExpress.XtraGrid.Columns.GridColumn StatusID;
        private DevExpress.XtraGrid.Columns.GridColumn TransferDate;
        private DevExpress.XtraGrid.Columns.GridColumn OtherProduct;
        private DevExpress.XtraGrid.Columns.GridColumn Createduser;
        private DevExpress.XtraGrid.Columns.GridColumn UpdateDate;
        private DevExpress.XtraGrid.Columns.GridColumn UpdateUser;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private System.Windows.Forms.ComboBox cboStoreChangeStatusManager;
        private Library.AppControl.ComboBoxControl.ComboBoxStore comboToStore2;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cbxFromManager;
        private DevExpress.XtraEditors.SimpleButton btnExportToExcel;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox chkDelete;
        private System.Windows.Forms.TextBox txtKeyWords;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucCreateUser;
        private System.Windows.Forms.ComboBox cboTransferOtherPro;
        private System.Windows.Forms.DateTimePicker dteToDate;
        private System.Windows.Forms.DateTimePicker dteFromDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboTransportTypeID;
        private System.Windows.Forms.Label label31;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboTransportServicesID;
        private System.Windows.Forms.Label label10;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboTransportCompanyID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}