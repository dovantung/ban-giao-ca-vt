﻿using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmStoreChangeOrderDetalError : Form
    {
        private DataTable dtbData = null;
        public frmStoreChangeOrderDetalError(DataTable dtbDataError)
        {
            InitializeComponent();
            this.dtbData = dtbDataError;
        }
        
        private void frmStoreChangeOrderDetalError_Load(object sender, EventArgs e)
        {
            grdProduct.DataSource = dtbData;
            grdProduct.RefreshDataSource();
            //FormatGridProduct();
        }

        private bool FormatGridProduct()
        {
            try
            {
                for (int i = 0; i < grdViewProduct.Columns.Count; i++)
                {
                    grdViewProduct.Columns[i].Visible = false;
                }
                grdViewProduct.Columns["PRODUCTID"].OptionsColumn.AllowEdit = false;
                grdViewProduct.Columns["PRODUCTNAME"].OptionsColumn.AllowEdit = false;
                grdViewProduct.Columns["NOTE"].OptionsColumn.AllowEdit = false;
                grdViewProduct.Columns["IMEI"].OptionsColumn.AllowEdit = false;

                grdViewProduct.Columns["MAINGROUPID"].Visible = false;                
                grdViewProduct.Columns["ISREQUESTIMEI"].Visible = false;
                
                
                grdViewProduct.Columns["PRODUCTID"].Visible = true;
                grdViewProduct.Columns["PRODUCTNAME"].Visible = true;
                grdViewProduct.Columns["IMEI"].Visible = true;
                grdViewProduct.Columns["QUANTITY"].Visible = true;
                grdViewProduct.Columns["NOTE"].Visible = true;
                grdViewProduct.Columns["ERRORLOG"].Visible = true;

                grdViewProduct.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grdViewProduct.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grdViewProduct.Columns["IMEI"].Caption = "IMEI";
                grdViewProduct.Columns["QUANTITY"].Caption = "Số lượng yêu cầu";
                grdViewProduct.Columns["NOTE"].Caption = "Ghi chú";
                grdViewProduct.Columns["ERRORLOG"].Caption = "Lý do không đủ điều kiện";
                grdViewProduct.OptionsView.ColumnAutoWidth = false;
                //Sumary
                grdViewProduct.Columns["QUANTITY"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:#,##0.##}");
                
                grdViewProduct.Columns["PRODUCTID"].Width = 120;
                grdViewProduct.Columns["PRODUCTNAME"].Width = 200;
                grdViewProduct.Columns["IMEI"].Width = 150;
                grdViewProduct.Columns["QUANTITY"].Width = 90;
                grdViewProduct.Columns["ERRORLOG"].Width = 300;
                grdViewProduct.Columns["NOTE"].Width = 250;
                grdViewProduct.Columns["QUANTITY"].DisplayFormat.FormatString = "#,##0.##";
                grdViewProduct.Columns["QUANTITY"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
              
                DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repSpinQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
                repSpinQuantity.MaxLength = 10;
                repSpinQuantity.IsFloatValue = true;
                repSpinQuantity.Mask.EditMask = "#,###,###,###,##0.##";
                repSpinQuantity.MaxLength = 50;
                repSpinQuantity.NullText = "0";
                //repSpinQuantity.MaxValue = new decimal(new int[] { -727379969, 232, 0, 0 });
                repSpinQuantity.Buttons.Clear();
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtNote.MaxLength = 2000;
                grdViewProduct.Columns["NOTE"].ColumnEdit = repTxtNote;

                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtImei.CharacterCasing = CharacterCasing.Upper;
                grdViewProduct.Columns["IMEI"].ColumnEdit = repTxtImei;
                //DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repTxtImei = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
                //grdViewProduct.Columns["IMEI"].ColumnEdit = repTxtImei;

                grdViewProduct.Appearance.FooterPanel.ForeColor = Color.Red;
                grdViewProduct.ColumnPanelRowHeight = 40;
                grdViewProduct.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewProduct.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewProduct.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewProduct.OptionsView.ShowAutoFilterRow = false;
                grdViewProduct.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewProduct.OptionsView.ShowFooter = true;
                grdViewProduct.OptionsView.ShowGroupPanel = false;
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewProduct);
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi Định dạng lưới danh sách chi tiết yêu cầu chuyển kho không đủ điều kiên");
                SystemErrorWS.Insert("Lỗi Định dạng lưới danh sách chi tiết yêu cầu chuyển kho không đủ điều kiên", objExce.ToString(), this.Name + " -> FormatGridProduct", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdProduct,DevExpress.XtraPrinting.TextExportMode.Text,false);
        }
    }
}
