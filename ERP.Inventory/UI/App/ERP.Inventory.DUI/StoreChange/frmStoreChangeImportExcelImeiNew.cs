﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using Library.AppCore;
using Library.AppCore.Forms;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmStoreChangeImportExcelImeiNew : Form
    {
        private DataTable dtbData;
        public DataTable dtbSub { get; set; }

        public List<frmStoreChange.Product> lstProduct = new List<frmStoreChange.Product>();
        public List<frmStoreChange.ProductImei> objProductImeiList = new List<frmStoreChange.ProductImei>();
        public List<frmStoreChange.ProductImei> objProductImeiListResult = new List<frmStoreChange.ProductImei>();
        private DataTable dtbResultData = null;
        private int intOutputStoreID;
        private bool bolIsFromStoreChange;
        private bool bolIsChkFiFo = false;
        private int intOutputTypeID;
        private int intInputTypeID;
        private int intInStockStatusID = 1;
        private int intCustomerID = -1;

        public int InStockStatusID
        {
            set { intInStockStatusID = value; }
        }

        public string OrderID { get; set; }

        public int InputTypeID
        {
            set { intInputTypeID = value; }
        }
        public int OutputTypeID
        {
            set { intOutputTypeID = value; }
        }
        public int OutputStoreID
        {
            set { intOutputStoreID = value; }
        }

        public bool IsFromStoreChange
        {
            get { return bolIsFromStoreChange; }
            set { bolIsFromStoreChange = value; }
        }

        public bool IsChkFiFo
        {
            get { return bolIsChkFiFo; }
            set { bolIsChkFiFo = value; }
        }

        public int CustomerID
        {
            get { return intCustomerID; }
            set { intCustomerID = value; }
        }
        public string InputStoreName { get; set; }

        public frmStoreChangeImportExcelImeiNew()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }

        public frmStoreChangeImportExcelImeiNew(List<ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucherDetail> dtbData)
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;

            this.dtbData = ConvertToDataTable(dtbData);
            this.dtbData.AsEnumerable().All(c => { c["Quantity"] = 0; return true; });
            this.dtbData.Columns.Add("IsError", typeof(Boolean));
            grdData.DataSource = this.dtbData;
        }

        //private void UpdateQuantity(DataTable dtbData)
        //{
        //    for (int i = 0; i < dtbData.Rows.Count; i++)
        //    {
        //        dtbData.Rows[i]["OrderQuantity"] = int.Parse(dtbData.Rows[i]["OrderQuantity"].ToString()) + int.Parse(dtbData.Rows[i]["Quantity"].ToString());
        //        grdViewData.RefreshData();
        //    }
        //}

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(gridControl1);
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Không thể xuất file Excel mẫu!", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Không thể xuất file Excel mẫu!");
                return;
            }
        }



        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang kiểm tra IMEI");
        }

        private bool ValidTemplatesExcel(DataTable dtbImeiImport, DataTable dtbGrid)
        {
            if (dtbImeiImport == null || dtbImeiImport.Columns.Count == 0 || dtbImeiImport.Rows.Count == 0)
                return false;

            if (dtbImeiImport.Rows[0][0].ToString().Trim().ToLower() != "imei")
            {
                MessageBox.Show("File nhập không đúng định dạng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            bool bolInputDataColumn = false;

            for (int i = 0; i < dtbImeiImport.Columns.Count; i++)
            {
                string strProductID = dtbImeiImport.Rows[0][i].ToString();
                if (!string.IsNullOrEmpty(strProductID))
                {
                    if (dtbImeiImport.AsEnumerable().Where(x => !string.IsNullOrEmpty(x[i].ToString())).Count() <= 1)
                    {
                        bolInputDataColumn = true;
                        continue;
                    }
                }
            }
            if (bolInputDataColumn)
            {
                MessageBox.Show(this, "Vui lòng nhập dữ liệu đầy đủ vào file import!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            DataTable dtbImeiImport = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(1);
            DataTable dtbGrid = grdData.DataSource as DataTable;
            if (!ValidTemplatesExcel(dtbImeiImport, dtbGrid))
                return;
            linkLabel1.Visible = false;
            dtbResultData = null;
            if (!dtbGrid.Columns.Contains("Status"))
            {
                DataColumn colStatus = new DataColumn();
                colStatus.ColumnName = "Status";
                dtbGrid.Columns.Add(colStatus);
            }
            if (!dtbGrid.Columns.Contains("Note"))
            {
                DataColumn colStatus = new DataColumn();
                colStatus.ColumnName = "Note";
                dtbGrid.Columns.Add(colStatus);
            }

            lstProduct = new List<frmStoreChange.Product>();
            objProductImeiList.Clear();
            foreach (DataRow rowGird in dtbGrid.Rows)
            {
                rowGird["Note"] = string.Empty;
                rowGird["Status"] = string.Empty;
            }

            foreach (DataRow rowGird in dtbGrid.Rows)
            {
                bool bolIsRequireImei = Convert.ToBoolean(rowGird["IsRequestIMEI"].ToString());
                string strProductID = rowGird["ProductID"].ToString().Trim();
                string strProductName = rowGird["ProductName"].ToString().Trim();

                frmStoreChange.Product objProduct = null;
                objProduct = new frmStoreChange.Product();
                objProduct.ProductID = strProductID;
                objProduct.ProductName = strProductName;
                if (!bolIsRequireImei) // Không yêu cầu nhập Imei
                {
                    int intQuantity = 0;
                }
                else // Sản phẩm yêu cầu nhập IMEI
                {
                    objProduct.IsRequestImei = true;
                    objProduct.Quantity = 0;
                }
                lstProduct.Add(objProduct);
            }

            foreach (DataRow rowGird in dtbGrid.Rows)
            {
                bool bolIsRequireImei = Convert.ToBoolean(rowGird["IsRequestIMEI"].ToString());
                string strProductID = rowGird["ProductID"].ToString().Trim();
                string strProductName = rowGird["ProductName"].ToString().Trim();

                for (int j = 0; j < dtbImeiImport.Columns.Count; j++)
                {
                    string strProductIDImport = dtbImeiImport.Rows[0][j].ToString();
                    frmStoreChange.Product objProduct = null;
                    objProduct = new frmStoreChange.Product();

                    objProduct.ProductID = strProductID;
                    objProduct.ProductName = strProductName;

                    if (!bolIsRequireImei) // Không yêu cầu nhập Imei
                    {
                        int intQuantity = 0;
                        if (dtbImeiImport.Rows.Count > 2)
                            int.TryParse(dtbImeiImport.Rows[2][j].ToString(), out intQuantity);
                        objProduct.Quantity = intQuantity;
                    }
                    else // Sản phẩm yêu cầu nhập IMEI
                    {
                        objProduct.IsRequestImei = true;
                        objProduct.Quantity = 0;
                        for (int iRow = 1; iRow < dtbImeiImport.Rows.Count; iRow++)
                        {
                            string strImei = dtbImeiImport.Rows[iRow][j].ToString();
                            if (!string.IsNullOrEmpty(strImei))
                            {
                                frmStoreChange.ProductImei objProductImei = new frmStoreChange.ProductImei();
                                objProductImei.Imei = strImei.Trim().ToUpper();
                                objProductImei.ProductID = objProduct.ProductID;
                                objProductImei.ProductName = objProduct.ProductName;
                                objProductImei.Error = "";
                                if (!Library.AppCore.Other.CheckObject.CheckIMEI(strImei.Trim().ToUpper()))
                                {
                                    objProductImei.Error = "Imei không đúng định dạng";
                                    objProductImei.IsError = true;
                                }

                                objProductImeiList.Add(objProductImei);
                            }
                        }
                    }
                }
                break;
            }

            ValidateData(dtbGrid);

            for (int i = 0; i < dtbGrid.Rows.Count; i++)
            {
                bool bolIsRequireImei = Convert.ToBoolean(dtbGrid.Rows[i]["IsRequestImei"].ToString());
                string strProductID = dtbGrid.Rows[i]["ProductID"].ToString().Trim();
                frmStoreChange.Product objProduct = lstProduct.FirstOrDefault(r => r.ProductID.Trim() == strProductID);
                string strStatus = string.Empty;
                string strError = string.Empty;
                if (bolIsRequireImei) // Yeu cau nhap IMEI
                {
                    int intAllImei = objProductImeiList.Where(r => r.ProductID == strProductID).Count();
                    int intQuantityInput = objProductImeiList.Where(r => r.ProductID == strProductID && !r.IsError).Count();
                    dtbGrid.Rows[i]["Quantity"] = intQuantityInput;
                    if (objProduct != null)
                        objProduct.Quantity = intQuantityInput;

                    if (intAllImei == 0)
                        dtbGrid.Rows[i]["Note"] = "Chưa nhập IMEI";
                    else if (intAllImei > intQuantityInput)
                        dtbGrid.Rows[i]["Note"] = "Số IMEI không hợp lệ: " + (intAllImei - intQuantityInput).ToString();
                }
                else
                {
                    if (objProduct != null)
                        dtbGrid.Rows[i]["Quantity"] = objProduct.Quantity;
                }

                decimal decOrderQuantity = decimal.Parse(dtbGrid.Rows[i]["OrderQuantity"].ToString());
                if (decOrderQuantity > 0)
                {
                    decimal decUven = decimal.Parse(dtbGrid.Rows[i]["OrderQuantity"].ToString()) - decimal.Parse(dtbGrid.Rows[i]["Quantity"].ToString());

                    if (decUven == 0)
                    {
                        strStatus = "Đủ số lượng";
                    }
                    else if (decUven < 0)
                    {
                        strStatus = "Thừa số lượng";
                        if (objProduct != null)
                            objProduct.IsError = true;
                    }
                    else
                        strStatus = "Chưa đủ số lượng";
                }


                dtbGrid.Rows[i]["Status"] = strStatus;
                if (objProduct != null)
                    dtbGrid.Rows[i]["IsError"] = objProduct.IsError;
            }
            grdData.RefreshDataSource();
            btnUpdate.Enabled = true;

            //Nguyen Van Tai bo sung
            var lstResult = dtbGrid.AsEnumerable().Where(x => Math.Abs(decimal.Parse(x["Quantity"].ToString()) - decimal.Parse(x["OrderQuantity"].ToString())) > 0);
            if (lstResult.Count() > 0)
                dtbResultData = lstResult.CopyToDataTable();
            if (dtbResultData != null && dtbResultData.Rows.Count > 0)
            {
                linkLabel1.Visible = true;
                frmStoreChangeImportExcelImeiError frm = new frmStoreChangeImportExcelImeiError();
                frm.dtbData = dtbResultData;
                frm.InputStoreName = InputStoreName;
                frm.ShowDialog();
            }
            //End
        }

        private void ValidateData(DataTable dtbGrid)
        {
            Thread objThread = new Thread(new ThreadStart(Excute));
            objThread.Start();

            var rlsImei = objProductImeiList.Where(r => !r.IsError);

            Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
            for (int i = 0; i < objProductImeiList.Count(); i++)
            {
                if (dicCheckIMEI.ContainsKey(objProductImeiList[i].Imei))
                {
                    dicCheckIMEI[objProductImeiList[i].Imei].Add(i);
                }
                else
                {
                    List<int> lstIndex = new List<int>();
                    lstIndex.Add(i);
                    dicCheckIMEI.Add(objProductImeiList[i].Imei, lstIndex);
                }
            }

            foreach (string strIMEI in dicCheckIMEI.Keys)
            {
                if (dicCheckIMEI[strIMEI].Count() > 1)
                {
                    for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                    {
                        objProductImeiList[dicCheckIMEI[strIMEI][i]].IsError = true;
                        objProductImeiList[dicCheckIMEI[strIMEI][i]].Error = objProductImeiList[dicCheckIMEI[strIMEI][i]].Error.Replace("Trùng IMEI trên lưới;", "") + "Trùng IMEI trên lưới;";
                    }
                }
            }

            #region Check trùng IMEI trong DB
            // Loại những Imei bị trùng trên lưới
            var listIMEINonError = objProductImeiList.Where(r => r.Error != null && !r.Error.Contains("Trùng IMEI trên file Excel"));


            var listIMEI = from item in listIMEINonError
                           select item.Imei;
            var noImeiDupes = listIMEI.Distinct().ToList();
            DataTable dtbImeiPara = new DataTable();
            dtbImeiPara.Columns.Add("IMEI");
            foreach (var item in noImeiDupes)
                dtbImeiPara.Rows.Add(item);
            string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbImeiPara, "IMEI");

            string xmlPinCode = string.Empty;
            DataTable dtbTotalImei = null;
            DataTable dtbTotalImeiNotInStock = null;
            DataSet dsData = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("CHECKIMEI_INSTOCK_NOTINSTOCK",
                new string[] { "v_Out1", "v_Out2" }, new object[] { "@StoreID", intOutputStoreID,
                    "@OUTPUTTYPEID", intOutputTypeID, "@INPUTTYPEID", intInputTypeID, "@IMEI", xmlIMEI });

            if (dsData != null)
            {
                if (dsData.Tables.Count > 0)
                    dtbTotalImei = dsData.Tables[0].Copy();
                if (dsData.Tables.Count > 1)
                    dtbTotalImeiNotInStock = dsData.Tables[1].Copy();
                List<string> lstIMEI = new List<string>();
                lstIMEI = lstIMEI.Concat(dtbTotalImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList()).ToList();
                int IsCanInputFifo = 0;
                foreach (var item in listIMEINonError)
                {
                    DataRow[] lstdr = dtbTotalImei.Select("IMEI = '" + item.Imei + "'");
                    if (lstdr.Length > 0)
                    {
                        var product = dtbGrid.Select("PRODUCTID = '" + lstdr[0]["PRODUCTID"].ToString() + "'");
                        if (product.Length > 0)
                        {
                            item.ProductID = lstdr[0]["PRODUCTID"].ToString();
                            item.EndWarrantyDate = Convert.IsDBNull(lstdr[0]["ENDWARRANTYDATE"]) ? null : (DateTime?)Convert.ToDateTime(lstdr[0]["ENDWARRANTYDATE"]);
                            item.IsHasWarranty = Convert.ToBoolean(lstdr[0]["IsHasWarranty"]);
                            item.EndWarrantyDate = Convert.IsDBNull(lstdr[0]["ENDWARRANTYDATE"]) ? null : (DateTime?)Convert.ToDateTime(lstdr[0]["ENDWARRANTYDATE"]);
                            item.IsHasWarranty = Convert.ToBoolean(lstdr[0]["IsHasWarranty"]);
                            item.IsNew = Convert.ToBoolean(lstdr[0]["IsNew"]);
                            item.IsShowProduct = Convert.ToBoolean(lstdr[0]["IsShowProduct"]);
                            item.InVAT = Convert.ToDecimal(lstdr[0]["InVAT"]);
                            item.OutVAT = Convert.ToDecimal(lstdr[0]["OutVAT"]);
                            item.InputPrice = Convert.ToDecimal(lstdr[0]["InputPrice"]);
                            item.OutputPrice = Convert.ToDecimal(lstdr[0]["OutputPrice"]);
                            item.CostPrice = Convert.ToDecimal(lstdr[0]["CostPrice"]);
                            item.IsErrorFiFo = Convert.ToInt32(lstdr[0]["STATUSFIFOID"]);
                            //if (item.IsErrorFiFo == 1)
                            //{
                            //    if (IsCanInputFifo == 0)
                            //    {
                            //        IsCanInputFifo = 1;
                            //        DialogResult drResult = MessageBox.Show(this, "Có tồn tại dữ liệu vi phạm xuất fifo bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                            //        if (drResult != System.Windows.Forms.DialogResult.OK)
                            //        {
                            //            item.IsError = true;
                            //            item.Error = "IMEI " + item.Imei.ToString().Trim() + " vi phạm xuất fifo";
                            //            IsCanInputFifo = -1;
                            //        }
                            //    }
                            //    if (IsCanInputFifo == -1)
                            //    {
                            //        item.IsError = true;
                            //        item.Error = "IMEI " + item.Imei.ToString().Trim() + " vi phạm xuất fifo";
                            //    }
                            //}
                            if (item.IsErrorFiFo == 1 && IsChkFiFo)
                                item.Error = "IMEI " + item.Imei.ToString().Trim() + " vi phạm xuất fifo";
                            if (lstdr[0]["INSTOCKSTATUSID"].ToString().Trim() != intInStockStatusID.ToString())
                            {
                                item.IsError = true;
                                item.Error = "IMEI này không thuộc trạng thái sản phẩm đã chọn!";
                            }
                            if (intCustomerID > 0)
                            {
                                if (lstdr[0]["FIRSTCUSTOMERID"].ToString().Trim() != intCustomerID.ToString() && Convert.ToBoolean(lstdr[0]["ISCHECKREALINPUT"]))
                                {
                                    item.IsError = true;
                                    item.Error = "IMEI này không thuộc nhà cung cấp đã chọn!";
                                }
                            }
                        }
                        else
                        {
                            item.IsError = true;
                            item.Error = "IMEI này không có trong sản phẩm trên lưới";
                        }
                    }
                    else
                    {
                        item.IsError = true;
                        item.Error = "IMEI này không tồn tại trong kho";
                    }
                }

                var lstExists = from o in listIMEINonError
                                join i in lstIMEI
                                on o.Imei equals i.ToString()
                                select o;

                var LstIMEIInProductOrder = from o in listIMEINonError
                                            join i in dtbTotalImei.Select("ISORDER = 1 and ORDERID <> '" + OrderID + "'")
                                            on o.ProductID equals i["PRODUCTID"].ToString()
                                            select i;
                if (LstIMEIInProductOrder.Any())
                {
                    var LstIMEIInProduct = from o in listIMEINonError
                                           join i in LstIMEIInProductOrder
                                           on o.Imei equals i["IMEI"].ToString()
                                           select o;
                    if (LstIMEIInProduct.Any())
                    {
                        LstIMEIInProduct.All(c => { c.IsError = true; c.Error = "IMEI đã được đặt hàng"; return true; });
                    }
                }

                var LstIMEIInProductStore = from o in listIMEINonError
                                            join i in dtbTotalImei.Select("STOREID <> " + intOutputStoreID)
                                            on o.Imei equals i["IMEI"].ToString()
                                            select o;
                if (LstIMEIInProductStore.Any())
                {
                    LstIMEIInProductStore.All(c => { c.IsError = true; c.Error = "IMEI không tồn kho"; return true; });
                }

                var LstIMEIInProductRealinstock = from o in listIMEINonError
                                                  join i in dtbTotalImei.Select("ISCHECKREALINPUT <> 1")
                                                  on o.Imei.Trim() equals i["IMEI"].ToString().Trim()
                                                  select o;
                if (LstIMEIInProductRealinstock.Any())
                {
                    LstIMEIInProductRealinstock.All(c => { c.IsError = true; c.Error = "IMEI chưa thực nhập"; return true; });
                }
                var LstIMEINotInStock = from o in listIMEINonError
                                        join i in dtbTotalImeiNotInStock.AsEnumerable()
                                        on o.Imei equals i["IMEI"].ToString()
                                        select o;
                if (LstIMEINotInStock.Any())
                {
                    LstIMEINotInStock.All(c => { c.IsError = true; c.Error = "IMEI đang cho mượn"; return true; });
                }

                //kiểm tra imei đã tồn tại trong form trước
                if (dtbSub != null && dtbSub.Rows.Count > 0)
                {
                    var lstExistsSub = from r in listIMEINonError
                                       join x in dtbSub.AsEnumerable()
                                           on r.Imei equals x["IMEI"].ToString()
                                       select r;
                    lstExistsSub.All(c => { c.IsError = true; c.Error = "Số IMEI này đã tồn tại"; return true; });
                }
            }
            #endregion

            frmWaitDialog.Close();
            Thread.Sleep(0);
            objThread.Abort();
            // Kiem tra fifo
            if (IsChkFiFo)
            {
                var varReulst = from o in objProductImeiList
                                where o.IsError == false && o.IsErrorFiFo == 1
                                select o;
                if (varReulst.Any())
                {
                    //varReulst.All(c => { c.Error = "IMEI " + c.Imei.ToString().Trim() + " vi phạm xuất fifo"; return true; });
                    DialogResult drResult = MessageBox.Show(this, "Có tồn tại dữ liệu vi phạm xuất fifo bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (drResult != System.Windows.Forms.DialogResult.OK)
                    {
                        foreach (var item in varReulst)
                            item.IsError = true;
                        //varReulst.All(c => { c.IsError = true; c.Error = "IMEI " + c.Imei.ToString().Trim() + " vi phạm xuất fifo"; return true; });
                    }
                }
            }
        }


        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
                return;

            DataRow row = grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            if (row != null)
            {
                if (row["IsRequestImei"].ToString().ToLower() == "true")
                {
                    string strProductID = row["ProductID"].ToString().Trim();
                    string strProductName = row["ProductName"].ToString().Trim();
                    var rls = objProductImeiList.Where(r => r.ProductID == strProductID);
                    if (rls.Count() > 0)
                    {
                        frmStoreChangeImportExcelDetailImeiNew frm = new frmStoreChangeImportExcelDetailImeiNew();
                        frm.Text += strProductID + " - " + strProductName;
                        frm.ProductList = rls.ToList();
                        frm.ShowDialog();
                    }
                    else
                        MessageBoxObject.ShowWarningMessage(this, "Chưa nhập IMEI cho sản phẩm!");
                }
                else
                {
                    MessageBoxObject.ShowWarningMessage(this, "Sản phẩm này không yêu cầu nhập IMEI!");
                    return;
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!CheckValiadate())
            {
                return;
            }
            if (!InsertDataToInput()) return;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private bool InsertDataToInput()
        {
            try
            {
                for (int i = lstProduct.Count - 1; i >= 0; i--)
                {
                    if (lstProduct[i].IsError)
                        lstProduct.RemoveAt(i);
                    else
                    {
                        var rls = objProductImeiList.Where(r => r.ProductID == lstProduct[i].ProductID && !r.IsError);
                        objProductImeiListResult.AddRange(rls.ToList());
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool CheckValiadate()
        {
            if (lstProduct.Where(x => x.IsError == true).Count() > 0 || objProductImeiList.Where(x => x.IsError == true).Count() > 0)
            {
                if (MessageBox.Show(this, "Tồn tại Imei hoặc sản phẩm không hợp lệ.\nBạn có muốn tiếp tục?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
            }
            return true;
        }


        private void grdViewData_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            DataRow row = grdViewData.GetDataRow(e.RowHandle);
            if (row != null && row["IsError"] != null && !string.IsNullOrEmpty(row["IsError"].ToString()))
            {
                bool bolError = Convert.ToBoolean(row["IsError"].ToString());
                if (bolError)
                    e.Appearance.BackColor = Color.Pink;
            }
        }


        private void repoViewDetail_Click(object sender, EventArgs e)
        {
            grdData_DoubleClick(null, null);
        }

        private void frmStoreChangeImportExcelImei_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            if (!bolIsFromStoreChange)
            {
                colProductName.Width += colStatus.Width;
                colNote.Width += colOrderQuantity.Width;
                colOrderQuantity.Visible = false;
                colStatus.Visible = false;
            }
        }

        private void frmStoreChangeImportExcelImei_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
        public DataTable ConvertListToDatatable(List<string> lstData)
        {
            DataTable dtbData = new DataTable();
            DataColumn col = new DataColumn();
            dtbData.Columns.Add(col);
            foreach (var item in lstData)
            {
                dtbData.Rows.Add(item);
            }
            return dtbData;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
        private DataTable ConvertColumnsAsRows(DataTable dt)
        {
            DataTable dtnew = new DataTable();
            //Convert all the rows to columns 
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtnew.Columns.Add(Convert.ToString(i));
            }
            DataRow dr;
            // Convert All the Columns to Rows 
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dr = dtnew.NewRow();
                dr[0] = dt.Columns[j].ToString();
                for (int k = 1; k <= dt.Rows.Count; k++)
                    dr[k] = dt.Rows[k - 1][j];
                dtnew.Rows.Add(dr);
            }
            return dtnew;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmStoreChangeImportExcelImeiError frm = new frmStoreChangeImportExcelImeiError();
            frm.dtbData = dtbResultData;
            frm.InputStoreName = InputStoreName;
            frm.ShowDialog();
        }

    }


}
