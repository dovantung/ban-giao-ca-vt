﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmPopupUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ucUserName = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.btnChoose = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Người nhặt hàng:";
            // 
            // ucUserName
            // 
            this.ucUserName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ucUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucUserName.IsOnlyShowRealStaff = false;
            this.ucUserName.IsValidate = true;
            this.ucUserName.Location = new System.Drawing.Point(120, 8);
            this.ucUserName.Margin = new System.Windows.Forms.Padding(4);
            this.ucUserName.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucUserName.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucUserName.Name = "ucUserName";
            this.ucUserName.Size = new System.Drawing.Size(257, 22);
            this.ucUserName.TabIndex = 0;
            this.ucUserName.UserName = "";
            this.ucUserName.WorkingPositionID = 0;
            // 
            // btnChoose
            // 
            this.btnChoose.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnChoose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnChoose.Location = new System.Drawing.Point(146, 37);
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Size = new System.Drawing.Size(95, 25);
            this.btnChoose.TabIndex = 1;
            this.btnChoose.Text = "    Chọn";
            this.btnChoose.UseVisualStyleBackColor = true;
            this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(252, 37);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(95, 25);
            this.btnUndo.TabIndex = 2;
            this.btnUndo.Text = "    Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // frmPopupUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 68);
            this.Controls.Add(this.btnUndo);
            this.Controls.Add(this.btnChoose);
            this.Controls.Add(this.ucUserName);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPopupUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Người nhặt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucUserName;
        private System.Windows.Forms.Button btnChoose;
        private System.Windows.Forms.Button btnUndo;
    }
}