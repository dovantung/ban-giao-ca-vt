﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmStoreChange_ValidBarcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.rdFindBarcode = new System.Windows.Forms.RadioButton();
            this.rdInsetBarcode = new System.Windows.Forms.RadioButton();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.txtBarcode = new DevExpress.XtraEditors.TextEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grdIMEI = new DevExpress.XtraGrid.GridControl();
            this.grvIMEI = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chksChon = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPincode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsHasWarranty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsError = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnIMEIValid = new System.Windows.Forms.Button();
            this.btnTotalIMEI = new System.Windows.Forms.Button();
            this.btnIMEIInValid = new System.Windows.Forms.Button();
            this.txtTotalIMEIInValid = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalIMEIValid = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalIMEI = new DevExpress.XtraEditors.TextEdit();
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdIMEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvIMEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksChon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEIInValid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEIValid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEI.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.groupControl1.Controls.Add(this.grdIMEI);
            this.groupControl1.Controls.Add(this.rdFindBarcode);
            this.groupControl1.Controls.Add(this.rdInsetBarcode);
            this.groupControl1.Controls.Add(this.lblBarcode);
            this.groupControl1.Controls.Add(this.txtBarcode);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(874, 448);
            this.groupControl1.TabIndex = 52;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            // 
            // rdFindBarcode
            // 
            this.rdFindBarcode.AutoSize = true;
            this.rdFindBarcode.Location = new System.Drawing.Point(317, 34);
            this.rdFindBarcode.Name = "rdFindBarcode";
            this.rdFindBarcode.Size = new System.Drawing.Size(110, 20);
            this.rdFindBarcode.TabIndex = 79;
            this.rdFindBarcode.Text = "Tìm kiếm IMEI";
            this.rdFindBarcode.UseVisualStyleBackColor = true;
            // 
            // rdInsetBarcode
            // 
            this.rdInsetBarcode.AutoSize = true;
            this.rdInsetBarcode.Checked = true;
            this.rdInsetBarcode.Location = new System.Drawing.Point(434, 34);
            this.rdInsetBarcode.Name = "rdInsetBarcode";
            this.rdInsetBarcode.Size = new System.Drawing.Size(105, 20);
            this.rdInsetBarcode.TabIndex = 80;
            this.rdInsetBarcode.TabStop = true;
            this.rdInsetBarcode.Text = "Bắn Barcode";
            this.rdInsetBarcode.UseVisualStyleBackColor = true;
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(12, 36);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(63, 16);
            this.lblBarcode.TabIndex = 78;
            this.lblBarcode.Text = "Barcode:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(76, 33);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtBarcode.Properties.Appearance.Options.UseFont = true;
            this.txtBarcode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarcode.Properties.LookAndFeel.SkinName = "Blue";
            this.txtBarcode.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtBarcode.Size = new System.Drawing.Size(234, 22);
            this.txtBarcode.TabIndex = 77;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.txtTotalIMEIInValid);
            this.panel1.Controls.Add(this.txtTotalIMEIValid);
            this.panel1.Controls.Add(this.txtTotalIMEI);
            this.panel1.Controls.Add(this.btnIMEIValid);
            this.panel1.Controls.Add(this.btnTotalIMEI);
            this.panel1.Controls.Add(this.btnIMEIInValid);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 448);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(874, 38);
            this.panel1.TabIndex = 54;
            // 
            // grdIMEI
            // 
            this.grdIMEI.Location = new System.Drawing.Point(5, 65);
            this.grdIMEI.MainView = this.grvIMEI;
            this.grdIMEI.Name = "grdIMEI";
            this.grdIMEI.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chksChon});
            this.grdIMEI.Size = new System.Drawing.Size(864, 378);
            this.grdIMEI.TabIndex = 81;
            this.grdIMEI.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvIMEI});
            // 
            // grvIMEI
            // 
            this.grvIMEI.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvIMEI.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvIMEI.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grvIMEI.ColumnPanelRowHeight = 20;
            this.grvIMEI.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.colIMEI,
            this.colPincode,
            this.colIsHasWarranty,
            this.colStatus,
            this.colIsError});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colIsError;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[Status]=true";
            styleFormatCondition1.Value1 = true;
            this.grvIMEI.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.grvIMEI.GridControl = this.grdIMEI;
            this.grvIMEI.Name = "grvIMEI";
            this.grvIMEI.OptionsView.ShowGroupPanel = false;
            this.grvIMEI.OptionsView.ShowIndicator = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Chọn";
            this.gridColumn1.ColumnEdit = this.chksChon;
            this.gridColumn1.FieldName = "IsSelect";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 57;
            // 
            // chksChon
            // 
            this.chksChon.AutoHeight = false;
            this.chksChon.Name = "chksChon";
            // 
            // colIMEI
            // 
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 1;
            this.colIMEI.Width = 200;
            // 
            // colPincode
            // 
            this.colPincode.Caption = "Pincode";
            this.colPincode.FieldName = "PINCode";
            this.colPincode.Name = "colPincode";
            this.colPincode.Visible = true;
            this.colPincode.VisibleIndex = 2;
            this.colPincode.Width = 218;
            // 
            // colIsHasWarranty
            // 
            this.colIsHasWarranty.Caption = "Bảo hành";
            this.colIsHasWarranty.FieldName = "IsHasWarranty";
            this.colIsHasWarranty.Name = "colIsHasWarranty";
            this.colIsHasWarranty.Visible = true;
            this.colIsHasWarranty.VisibleIndex = 3;
            this.colIsHasWarranty.Width = 70;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Trạng thái";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 4;
            this.colStatus.Width = 241;
            // 
            // colIsError
            // 
            this.colIsError.Caption = "IsError";
            this.colIsError.FieldName = "IsError";
            this.colIsError.Name = "colIsError";
            // 
            // btnIMEIValid
            // 
            this.btnIMEIValid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnIMEIValid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIMEIValid.ForeColor = System.Drawing.Color.Blue;
            this.btnIMEIValid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIMEIValid.Location = new System.Drawing.Point(451, 6);
            this.btnIMEIValid.Margin = new System.Windows.Forms.Padding(1);
            this.btnIMEIValid.Name = "btnIMEIValid";
            this.btnIMEIValid.Size = new System.Drawing.Size(108, 24);
            this.btnIMEIValid.TabIndex = 82;
            this.btnIMEIValid.Text = "Số IMEI hợp lệ:";
            this.btnIMEIValid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIMEIValid.UseVisualStyleBackColor = true;
            // 
            // btnTotalIMEI
            // 
            this.btnTotalIMEI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnTotalIMEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotalIMEI.ForeColor = System.Drawing.Color.Blue;
            this.btnTotalIMEI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTotalIMEI.Location = new System.Drawing.Point(9, 6);
            this.btnTotalIMEI.Margin = new System.Windows.Forms.Padding(1);
            this.btnTotalIMEI.Name = "btnTotalIMEI";
            this.btnTotalIMEI.Size = new System.Drawing.Size(102, 24);
            this.btnTotalIMEI.TabIndex = 80;
            this.btnTotalIMEI.Text = "Tổng số IMEI:";
            this.btnTotalIMEI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTotalIMEI.UseVisualStyleBackColor = true;
            // 
            // btnIMEIInValid
            // 
            this.btnIMEIInValid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnIMEIInValid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIMEIInValid.ForeColor = System.Drawing.Color.Blue;
            this.btnIMEIInValid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIMEIInValid.Location = new System.Drawing.Point(251, 6);
            this.btnIMEIInValid.Margin = new System.Windows.Forms.Padding(1);
            this.btnIMEIInValid.Name = "btnIMEIInValid";
            this.btnIMEIInValid.Size = new System.Drawing.Size(82, 24);
            this.btnIMEIInValid.TabIndex = 81;
            this.btnIMEIInValid.Text = "Số IMEI lỗi:";
            this.btnIMEIInValid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIMEIInValid.UseVisualStyleBackColor = true;
            // 
            // txtTotalIMEIInValid
            // 
            this.txtTotalIMEIInValid.Location = new System.Drawing.Point(563, 7);
            this.txtTotalIMEIInValid.Name = "txtTotalIMEIInValid";
            this.txtTotalIMEIInValid.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalIMEIInValid.Properties.Appearance.Options.UseFont = true;
            this.txtTotalIMEIInValid.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalIMEIInValid.Properties.Mask.EditMask = "n";
            this.txtTotalIMEIInValid.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalIMEIInValid.Properties.NullText = "0";
            this.txtTotalIMEIInValid.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtTotalIMEIInValid.Properties.ReadOnly = true;
            this.txtTotalIMEIInValid.Size = new System.Drawing.Size(100, 22);
            this.txtTotalIMEIInValid.TabIndex = 85;
            // 
            // txtTotalIMEIValid
            // 
            this.txtTotalIMEIValid.Location = new System.Drawing.Point(337, 7);
            this.txtTotalIMEIValid.Name = "txtTotalIMEIValid";
            this.txtTotalIMEIValid.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalIMEIValid.Properties.Appearance.Options.UseFont = true;
            this.txtTotalIMEIValid.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalIMEIValid.Properties.Mask.EditMask = "n";
            this.txtTotalIMEIValid.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalIMEIValid.Properties.NullText = "0";
            this.txtTotalIMEIValid.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtTotalIMEIValid.Properties.ReadOnly = true;
            this.txtTotalIMEIValid.Size = new System.Drawing.Size(100, 22);
            this.txtTotalIMEIValid.TabIndex = 84;
            // 
            // txtTotalIMEI
            // 
            this.txtTotalIMEI.Location = new System.Drawing.Point(113, 7);
            this.txtTotalIMEI.Name = "txtTotalIMEI";
            this.txtTotalIMEI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalIMEI.Properties.Appearance.Options.UseFont = true;
            this.txtTotalIMEI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalIMEI.Properties.Mask.EditMask = "n";
            this.txtTotalIMEI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalIMEI.Properties.NullText = "0";
            this.txtTotalIMEI.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtTotalIMEI.Properties.ReadOnly = true;
            this.txtTotalIMEI.Size = new System.Drawing.Size(100, 22);
            this.txtTotalIMEI.TabIndex = 83;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Appearance.Options.UseFont = true;
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.Location = new System.Drawing.Point(742, 6);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 23);
            this.btnUpdate.TabIndex = 86;
            this.btnUpdate.Text = "Cập nhật IMEI";
            // 
            // frmStoreChange_ValidBarcode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 486);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmStoreChange_ValidBarcode";
            this.ShowIcon = false;
            this.Text = "Danh sách IMEI xuất chuyển kho";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdIMEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvIMEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksChon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEIInValid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEIValid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIMEI.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.RadioButton rdFindBarcode;
        private System.Windows.Forms.RadioButton rdInsetBarcode;
        private System.Windows.Forms.Label lblBarcode;
        private DevExpress.XtraEditors.TextEdit txtBarcode;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraGrid.GridControl grdIMEI;
        private DevExpress.XtraGrid.Views.Grid.GridView grvIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chksChon;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colPincode;
        private DevExpress.XtraGrid.Columns.GridColumn colIsHasWarranty;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIsError;
        private System.Windows.Forms.Button btnIMEIValid;
        private System.Windows.Forms.Button btnTotalIMEI;
        private System.Windows.Forms.Button btnIMEIInValid;
        private DevExpress.XtraEditors.TextEdit txtTotalIMEIInValid;
        private DevExpress.XtraEditors.TextEdit txtTotalIMEIValid;
        private DevExpress.XtraEditors.TextEdit txtTotalIMEI;
        private DevExpress.XtraEditors.SimpleButton btnUpdate;
    }
}