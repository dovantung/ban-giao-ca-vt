﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmStoreChangeOrdersDetail : Form
    {
        private string _strStoreChangeOrderIDList;
        private string _excelTemplatePath = Application.StartupPath + "\\" + "Templates\\Reports\\RPT_PM_STORECHANGEORDER_DETAIL_TEMPLATE.xlsx";
        private DataTable dtbStoreChangeOrdersDetail { get; set; }
        ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource { get; set; }
        private const string RPT_PM_STORECHANGEORDER_DETAIL = "RPT_PM_STORECHANGEORDER_DETAIL";
        private bool IsPermissionView { get { return true; } }
        private bool IsPermissionExport { get { return true; } }

        public frmStoreChangeOrdersDetail(string strStoreChangeOrderIDList)
        {
            InitializeComponent();
            _strStoreChangeOrderIDList = strStoreChangeOrderIDList;
            objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
            this.Shown += FrmStoreChangeOrdersDetail_Shown;
        }

        private void FrmStoreChangeOrdersDetail_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            if (!IsPermissionView)
            {
                MessageBox.Show(this, "Bạn không có quyền xem [Báo cáo chi tiết yêu cầu chuyển kho bảo hành]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            Control.CheckForIllegalCrossThreadCalls = false;
            new Thread(new ThreadStart(() =>
            {
                LoadData();
                FillData();
            })).Start();
        }

        private void frmStoreChangeOrdersDetail_Load(object sender, EventArgs e)
        {

        }

        private void LoadData()
        {
            dtbStoreChangeOrdersDetail = null;
            try
            {
                if (IsValidInput(_strStoreChangeOrderIDList))
                {
                    var parameters = BuildParameters();
                    var stringLenght = parameters[1].ToString().Length;
                    dtbStoreChangeOrdersDetail = objPLCReportDataSource.GetDataSource(RPT_PM_STORECHANGEORDER_DETAIL, parameters);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Có lỗi xảy ra trong quá trình tải dữ liệu báo cáo", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void FillData()
        {
            grdData.DataSource = dtbStoreChangeOrdersDetail;
            btnExportToExcel.Enabled = (dtbStoreChangeOrdersDetail != null && dtbStoreChangeOrdersDetail.Rows.Count > 0)
                && IsPermissionExport;
            pnSearchStatus.Visible = false;
        }
        private object[] BuildParameters()
        {
            object[] result = null;
            try
            {
                result = new object[] {
                    "@ListStoreChangeOrderID",
                    _strStoreChangeOrderIDList
                };
            }
            catch { }

            return result;
        }

        private bool IsValidInput(object inputData)
        {
            bool result = false;
            StringBuilder errors = new StringBuilder();
            try
            {
                if (string.IsNullOrWhiteSpace(inputData.ToString()))
                    errors.AppendLine("Vui lòng chọn phiếu yêu cầu cần xem báo cáo!");
                //.... validate more
            }
            catch (Exception ex)
            {
                errors.AppendLine("Có lổi xảy ra trong quá trình kiểm tra thông tin đầu vào");
            }
            result = errors.Length == 0;
            if (!result)
                MessageBox.Show(errors.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            return result;
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportExcel();
        }

        private void ExportExcel()
        {
            if (dtbStoreChangeOrdersDetail != null && dtbStoreChangeOrdersDetail.Rows.Count > 0)
            {
                int cursor_Index = 6;
                if (!File.Exists(_excelTemplatePath))
                {
                    MessageBox.Show(this, "Không tìm thấy file template!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                ExcelFile xlsFile = new ExcelFile(_excelTemplatePath);
                ExcelWorksheet xlsSheet = xlsFile.Worksheets[0];
                WriteExcelContent(ref xlsSheet, ref cursor_Index);
                WriteExcelFooter(ref xlsSheet, ref cursor_Index);
                SaveFile(xlsFile);
            }
        }
        private void WriteExcelContent(ref ExcelWorksheet xlsSheet, ref int cursor_Index)
        {
            CellStyle cellStyle = new CellStyle();
            cellStyle.Borders.SetBorders(MultipleBorders.Outside, Color.Black, LineStyle.Thin);
            cellStyle.Font.Size = 220;
            cellStyle.Font.Name = "Calibri";
            cellStyle.VerticalAlignment = VerticalAlignmentStyle.Center;
            cellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Left;
            cellStyle.WrapText = true;

            var xlsColumnMappers = GetColumnMapper();
            foreach (DataRow row in dtbStoreChangeOrdersDetail.AsEnumerable())
            {
                foreach (ExcelColumnMapper colMapper in xlsColumnMappers)
                {
                    if (row[colMapper.ColumnName] != DBNull.Value)
                    {
                        object val = null;
                        if (colMapper.DataType == typeof(DateTime))
                            val = row.Field<System.DateTime>(colMapper.ColumnName).ToString("dd/MM/yyyy");
                        if (colMapper.DataType == typeof(decimal))
                            val = row.Field<decimal>(colMapper.ColumnName).ToString("N0");

                        if (val == null)
                            val = row[colMapper.ColumnName].ToString();

                        InsertValueCell(ref xlsSheet, colMapper.MapToColumn, cursor_Index, val);
                    }
                    xlsSheet.Cells[colMapper.MapToColumn + cursor_Index].Style = cellStyle;
                }
                cursor_Index++;
            }
        }
        private void WriteExcelFooter(ref ExcelWorksheet xlsSheet, ref int cursor_Index)
        {

            CellStyle footerCellStyle = new CellStyle();
            footerCellStyle.Font.Weight = 700;
            footerCellStyle.Font.Size = 220;
            footerCellStyle.Font.Name = "Calibri";
            footerCellStyle.VerticalAlignment = VerticalAlignmentStyle.Center;
            footerCellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center;


            cursor_Index++;
            string footerDate = "Ngày....tháng.....năm.....";
            InsertValueCell(ref xlsSheet, "O", cursor_Index, footerDate);
            xlsSheet.get_Range("O" + cursor_Index, "P" + cursor_Index).Merged = true;
            xlsSheet.Cells["O" + cursor_Index].Style = footerCellStyle;

        }
        private void SaveFile(ExcelFile xls)
        {
            if (xls != null)
            {
                SaveFileDialog fileDialog = new SaveFileDialog();
                fileDialog.Filter = "Excel Workbook|*.xlsx";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        xls.SaveExcel(fileDialog.FileName);
                        if (MessageBox.Show("Bạn có muốn mở file vừa lưu?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            System.Diagnostics.Process.Start(fileDialog.FileName);
                        }
                    }
                    catch
                    {
                        MessageBox.Show("File excel đang được mở, vui lòng đóng file và thực hiện xuất excel lại!", "Thông báo",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }
        private ExcelColumnMapper[] GetColumnMapper()
        {
            List<ExcelColumnMapper> result = new List<ExcelColumnMapper>();
            result.AddRange(new ExcelColumnMapper[] {
                new ExcelColumnMapper { ColumnName = "ORDERINDEX", MapToColumn = "A", DataType = typeof(Int64) },
                new ExcelColumnMapper { ColumnName = "STORECHANGEORDERTYPENAME", MapToColumn = "B", DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "STORECHANGEORDERID", MapToColumn = "C", DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "STOREOUT", MapToColumn = "D", DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "STOREIN", MapToColumn = "E", DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "ORDERDATE", MapToColumn = "F", DataType = typeof(DateTime) },
                new ExcelColumnMapper { ColumnName = "CREATEDUSER", MapToColumn = "G", DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "TEXT_STORECHANGESTATUS", MapToColumn = "H", DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "TEXT_APPROVESTATUS", MapToColumn = "I", DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "TEXT_TRANSFERED", MapToColumn = "J", DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "TEXT_RECEIVEDTRANSFER", MapToColumn = "K", DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "CONTENT", MapToColumn = "L", DataType = typeof(string)  },
                new ExcelColumnMapper { ColumnName = "REVIEWEDUSER", MapToColumn = "M", DataType = typeof(string)  },
                new ExcelColumnMapper { ColumnName = "REVIEWEDDATE", MapToColumn = "N" , DataType = typeof(DateTime) },
                new ExcelColumnMapper { ColumnName = "PRODUCTID", MapToColumn = "O" , DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "PRODUCTNAME", MapToColumn = "P" , DataType = typeof(string) },
                new ExcelColumnMapper { ColumnName = "IMEI", MapToColumn = "Q", DataType = typeof(string)  },
                new ExcelColumnMapper { ColumnName = "QUANTITY", MapToColumn = "R", DataType = typeof(System.Int64)  },
                new ExcelColumnMapper { ColumnName = "PRODUCTSTATUSNAME", MapToColumn = "S", DataType = typeof(string)  },
                new ExcelColumnMapper { ColumnName = "NOTE", MapToColumn = "T", DataType = typeof(string)  },

            });
            return result.ToArray();
        }
        private void InsertValueCell(ref ExcelWorksheet worksheet, String strColumnName, int iRow, Object value, Boolean isAdd = false, Boolean isBold = false)
        {
            try
            {
                ExcelCell cell = worksheet.Cells[strColumnName + iRow];
                SetValueRange(ref cell, value, isAdd, isBold);
            }
            catch { }
        }
        private void SetValueRange(ref ExcelCell cell, Object value, Boolean isAdd, Boolean isBold)
        {
            try
            {
                if (isBold)
                    cell.Style.Font.Weight = ExcelFont.BoldWeight;
            }
            catch { }

            if (cell != null)
            {
                try
                {
                    if (isAdd)
                        cell.Value = cell.Value.ToString() + " " + value;
                    else
                        cell.Value = value;

                    if (value.ToString().StartsWith("="))
                        cell.Formula = value.ToString();
                }
                catch { }
            }
        }

        private class ExcelColumnMapper
        {
            //Data table column
            public string ColumnName { get; set; }

            //ExcelColumn
            public string MapToColumn { get; set; }

            //Data Type
            public Type DataType { get; set; }
        }

        private void grdViewData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                Clipboard.SetText(grdViewData.GetFocusedDisplayText());
                e.Handled = true;
            }
        }
    }

}
