﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmStoreChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStoreChange));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.mnuProduct = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuDelProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelAllImei = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemCheckRealInput = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportExcelTemp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemSubIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuGhiChuIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSelectToUser1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemSelectToUser1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnAction = new DevExpress.XtraEditors.DropDownButton();
            this.pMnu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.lblProductCount = new DevExpress.XtraEditors.TextEdit();
            this.lblTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.lblTotalSize = new DevExpress.XtraEditors.TextEdit();
            this.lbTotalWeight = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cboInStockStatusID = new System.Windows.Forms.ComboBox();
            this.txtTransportVoucherID = new Library.AppControl.TextBoxControl.ASCIIText();
            this.txtInVoiceSymbol = new Library.AppControl.TextBoxControl.ASCIIText();
            this.txtInVoiceID = new Library.AppControl.TextBoxControl.ASCIIText();
            this.label19 = new System.Windows.Forms.Label();
            this.txtContent = new DevExpress.XtraEditors.MemoEdit();
            this.txtToUser2 = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.label20 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtInputVoucherID = new DevExpress.XtraEditors.TextEdit();
            this.txtOutputVoucherID = new DevExpress.XtraEditors.TextEdit();
            this.cboToStoreSearch = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboFromStoreSearch = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.txtToUser1 = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.grpPrintStoreAddress = new System.Windows.Forms.GroupBox();
            this.rdPrintStoreAddressA5 = new System.Windows.Forms.RadioButton();
            this.rdPrintStoreAddressA4 = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.rdPrintStoreAddressA6 = new System.Windows.Forms.RadioButton();
            this.txtPackingNumber = new DevExpress.XtraEditors.SpinEdit();
            this.lblPackingNumber = new System.Windows.Forms.Label();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.cmdSearch = new DevExpress.XtraEditors.SimpleButton();
            this.txtBarcode = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboStoreChangeTypeID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label18 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cboTransportTypeID = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dtChangeDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCaskCode = new DevExpress.XtraEditors.TextEdit();
            this.txtStoreChangeOrderID = new DevExpress.XtraEditors.TextEdit();
            this.txtStoreChangeID = new DevExpress.XtraEditors.TextEdit();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtReciveNote = new System.Windows.Forms.TextBox();
            this.lblInputStatus = new System.Windows.Forms.Label();
            this.chkIsRecived = new System.Windows.Forms.CheckBox();
            this.txtSignReceiveNote = new System.Windows.Forms.TextBox();
            this.lblSignReceiveStatus = new System.Windows.Forms.Label();
            this.chkIsSignReceive = new System.Windows.Forms.CheckBox();
            this.lblReciveStatus = new System.Windows.Forms.Label();
            this.chkIsRecivedTransfer = new System.Windows.Forms.CheckBox();
            this.lblTransferStatus = new System.Windows.Forms.Label();
            this.chkIsTransfered = new System.Windows.Forms.CheckBox();
            this.lblOutputStatus = new System.Windows.Forms.Label();
            this.chkIsOutputed = new System.Windows.Forms.CheckBox();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.grdInputVoucherDetail = new DevExpress.XtraGrid.GridControl();
            this.grvInputVoucherDetail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.MicrosoftSansSerif = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.chkSelect = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colProductID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandORDERQUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colORDERQUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colAmount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Tahoma = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colNOTE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colBarcode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.btn_Barcode = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colImportExcel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.btn_ImportExcel = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colGenIMEI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.btn_genIMEI = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colGetFIFO = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.btn_getFIFO = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.grdTemp = new DevExpress.XtraGrid.GridControl();
            this.grvTemp = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colENDWARRANTYDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colISHASWARRANTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mnuProduct.SuspendLayout();
            this.mnuSelectToUser1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pMnu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProductCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputVoucherID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOutputVoucherID.Properties)).BeginInit();
            this.grpPrintStoreAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPackingNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaskCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreChangeOrderID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreChangeID.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdInputVoucherDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvInputVoucherDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_ImportExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_genIMEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_getFIFO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuProduct
            // 
            this.mnuProduct.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDelProduct,
            this.mnuDelAllImei,
            this.mnuItemCheckRealInput,
            this.mnuItemExportExcelTemp,
            this.mnuItemImportExcel,
            this.toolStripSeparator1,
            this.mnuItemExportExcel,
            this.mnuItemSubIMEI,
            this.mnuGhiChuIMEI});
            this.mnuProduct.Name = "mnuStore";
            this.mnuProduct.Size = new System.Drawing.Size(194, 186);
            this.mnuProduct.Opening += new System.ComponentModel.CancelEventHandler(this.mnuProduct_Opening);
            // 
            // mnuDelProduct
            // 
            this.mnuDelProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuDelProduct.Name = "mnuDelProduct";
            this.mnuDelProduct.Size = new System.Drawing.Size(193, 22);
            this.mnuDelProduct.Text = "Xóa sản phẩm/IMEI";
            this.mnuDelProduct.Click += new System.EventHandler(this.mnuDelProduct_Click);
            // 
            // mnuDelAllImei
            // 
            this.mnuDelAllImei.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuDelAllImei.Name = "mnuDelAllImei";
            this.mnuDelAllImei.Size = new System.Drawing.Size(193, 22);
            this.mnuDelAllImei.Text = "Xóa danh sách IMEI";
            this.mnuDelAllImei.Click += new System.EventHandler(this.mnuDelAllImei_Click);
            // 
            // mnuItemCheckRealInput
            // 
            this.mnuItemCheckRealInput.Enabled = false;
            this.mnuItemCheckRealInput.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.mnuItemCheckRealInput.Name = "mnuItemCheckRealInput";
            this.mnuItemCheckRealInput.Size = new System.Drawing.Size(193, 22);
            this.mnuItemCheckRealInput.Text = "Xác nhận nhập hàng";
            this.mnuItemCheckRealInput.Click += new System.EventHandler(this.mnuItemCheckRealInput_Click);
            // 
            // mnuItemExportExcelTemp
            // 
            this.mnuItemExportExcelTemp.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcelTemp.Name = "mnuItemExportExcelTemp";
            this.mnuItemExportExcelTemp.Size = new System.Drawing.Size(193, 22);
            this.mnuItemExportExcelTemp.Text = "Xuất excel mẫu";
            this.mnuItemExportExcelTemp.Visible = false;
            this.mnuItemExportExcelTemp.Click += new System.EventHandler(this.mnuItemExportExcelTemp_Click);
            // 
            // mnuItemImportExcel
            // 
            this.mnuItemImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemImportExcel.Name = "mnuItemImportExcel";
            this.mnuItemImportExcel.Size = new System.Drawing.Size(193, 22);
            this.mnuItemImportExcel.Text = "Nhập IMEI từ excel";
            this.mnuItemImportExcel.Click += new System.EventHandler(this.mnuItemImportExcel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(190, 6);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(193, 22);
            this.mnuItemExportExcel.Text = "Xuất excel";
            this.mnuItemExportExcel.Visible = false;
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // mnuItemSubIMEI
            // 
            this.mnuItemSubIMEI.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemSubIMEI.Name = "mnuItemSubIMEI";
            this.mnuItemSubIMEI.Size = new System.Drawing.Size(193, 22);
            this.mnuItemSubIMEI.Text = "Xuất excel chi tiết IMEI";
            this.mnuItemSubIMEI.Visible = false;
            this.mnuItemSubIMEI.Click += new System.EventHandler(this.mnuItemSubIMEI_Click);
            // 
            // mnuGhiChuIMEI
            // 
            this.mnuGhiChuIMEI.Name = "mnuGhiChuIMEI";
            this.mnuGhiChuIMEI.Size = new System.Drawing.Size(193, 22);
            this.mnuGhiChuIMEI.Text = "Ghi chú chi tiết IMEI";
            this.mnuGhiChuIMEI.Click += new System.EventHandler(this.mnuGhiChu_Click);
            // 
            // mnuSelectToUser1
            // 
            this.mnuSelectToUser1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemSelectToUser1});
            this.mnuSelectToUser1.Name = "mnuStore";
            this.mnuSelectToUser1.Size = new System.Drawing.Size(189, 26);
            // 
            // mnuItemSelectToUser1
            // 
            this.mnuItemSelectToUser1.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemSelectToUser1.Image")));
            this.mnuItemSelectToUser1.Name = "mnuItemSelectToUser1";
            this.mnuItemSelectToUser1.Size = new System.Drawing.Size(188, 22);
            this.mnuItemSelectToUser1.Text = "Chọn nhân viên nhận";
            this.mnuItemSelectToUser1.Click += new System.EventHandler(this.mnuItemSelectToUser1_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnAction);
            this.panelControl1.Controls.Add(this.lblProductCount);
            this.panelControl1.Controls.Add(this.lblTotalQuantity);
            this.panelControl1.Controls.Add(this.lblTotalSize);
            this.panelControl1.Controls.Add(this.lbTotalWeight);
            this.panelControl1.Controls.Add(this.label10);
            this.panelControl1.Controls.Add(this.label6);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.label7);
            this.panelControl1.Controls.Add(this.btnUpdate);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 457);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1094, 34);
            this.panelControl1.TabIndex = 1;
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAction.Appearance.Options.UseFont = true;
            this.btnAction.DropDownControl = this.pMnu;
            this.btnAction.Location = new System.Drawing.Point(864, 5);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(125, 25);
            this.btnAction.TabIndex = 7;
            this.btnAction.Text = "Giao nhận hàng";
            this.btnAction.Visible = false;
            // 
            // pMnu
            // 
            this.pMnu.Manager = this.barManager1;
            this.pMnu.Name = "pMnu";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1094, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 491);
            this.barDockControlBottom.Size = new System.Drawing.Size(1094, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 491);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1094, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 491);
            // 
            // lblProductCount
            // 
            this.lblProductCount.Location = new System.Drawing.Point(68, 7);
            this.lblProductCount.Name = "lblProductCount";
            this.lblProductCount.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.lblProductCount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductCount.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblProductCount.Properties.Appearance.Options.UseBackColor = true;
            this.lblProductCount.Properties.Appearance.Options.UseFont = true;
            this.lblProductCount.Properties.Appearance.Options.UseForeColor = true;
            this.lblProductCount.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblProductCount.Properties.DisplayFormat.FormatString = "n0";
            this.lblProductCount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.lblProductCount.Properties.Mask.EditMask = "N0";
            this.lblProductCount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.lblProductCount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.lblProductCount.Properties.NullText = "0";
            this.lblProductCount.Properties.ReadOnly = true;
            this.lblProductCount.Size = new System.Drawing.Size(76, 20);
            this.lblProductCount.TabIndex = 78;
            // 
            // lblTotalQuantity
            // 
            this.lblTotalQuantity.EditValue = "0";
            this.lblTotalQuantity.Location = new System.Drawing.Point(216, 7);
            this.lblTotalQuantity.Name = "lblTotalQuantity";
            this.lblTotalQuantity.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.lblTotalQuantity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalQuantity.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblTotalQuantity.Properties.Appearance.Options.UseBackColor = true;
            this.lblTotalQuantity.Properties.Appearance.Options.UseFont = true;
            this.lblTotalQuantity.Properties.Appearance.Options.UseForeColor = true;
            this.lblTotalQuantity.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblTotalQuantity.Properties.DisplayFormat.FormatString = "n0";
            this.lblTotalQuantity.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.lblTotalQuantity.Properties.Mask.EditMask = "N0";
            this.lblTotalQuantity.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.lblTotalQuantity.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.lblTotalQuantity.Properties.NullText = "0";
            this.lblTotalQuantity.Properties.ReadOnly = true;
            this.lblTotalQuantity.Size = new System.Drawing.Size(67, 20);
            this.lblTotalQuantity.TabIndex = 77;
            // 
            // lblTotalSize
            // 
            this.lblTotalSize.Location = new System.Drawing.Point(676, 7);
            this.lblTotalSize.Name = "lblTotalSize";
            this.lblTotalSize.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.lblTotalSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalSize.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblTotalSize.Properties.Appearance.Options.UseBackColor = true;
            this.lblTotalSize.Properties.Appearance.Options.UseFont = true;
            this.lblTotalSize.Properties.Appearance.Options.UseForeColor = true;
            this.lblTotalSize.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblTotalSize.Properties.DisplayFormat.FormatString = "n0";
            this.lblTotalSize.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.lblTotalSize.Properties.Mask.EditMask = "N0";
            this.lblTotalSize.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.lblTotalSize.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.lblTotalSize.Properties.NullText = "0";
            this.lblTotalSize.Properties.ReadOnly = true;
            this.lblTotalSize.Size = new System.Drawing.Size(100, 20);
            this.lblTotalSize.TabIndex = 76;
            this.lblTotalSize.Visible = false;
            // 
            // lbTotalWeight
            // 
            this.lbTotalWeight.Location = new System.Drawing.Point(443, 7);
            this.lbTotalWeight.Name = "lbTotalWeight";
            this.lbTotalWeight.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.lbTotalWeight.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalWeight.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTotalWeight.Properties.Appearance.Options.UseBackColor = true;
            this.lbTotalWeight.Properties.Appearance.Options.UseFont = true;
            this.lbTotalWeight.Properties.Appearance.Options.UseForeColor = true;
            this.lbTotalWeight.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbTotalWeight.Properties.DisplayFormat.FormatString = "n0";
            this.lbTotalWeight.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.lbTotalWeight.Properties.Mask.EditMask = "N0";
            this.lbTotalWeight.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.lbTotalWeight.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.lbTotalWeight.Properties.NullText = "0";
            this.lbTotalWeight.Properties.ReadOnly = true;
            this.lbTotalWeight.Size = new System.Drawing.Size(100, 20);
            this.lbTotalWeight.TabIndex = 75;
            this.lbTotalWeight.Visible = false;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(4, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 16);
            this.label10.TabIndex = 69;
            this.label10.Text = "Đã chọn:";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(150, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 20);
            this.label6.TabIndex = 68;
            this.label6.Text = "Tổng SL:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(282, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 16);
            this.label5.TabIndex = 71;
            this.label5.Text = "Tổng trọng lượng (kg):";
            this.label5.Visible = false;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(542, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 16);
            this.label7.TabIndex = 73;
            this.label7.Text = "Tổng thể tích (m³):";
            this.label7.Visible = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Appearance.Options.UseFont = true;
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.Location = new System.Drawing.Point(995, 5);
            this.btnUpdate.LookAndFeel.SkinName = "Blue";
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(94, 25);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.MaxLength = 200;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "n";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1094, 246);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cboInStockStatusID);
            this.tabPage1.Controls.Add(this.txtTransportVoucherID);
            this.tabPage1.Controls.Add(this.txtInVoiceSymbol);
            this.tabPage1.Controls.Add(this.txtInVoiceID);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.txtContent);
            this.tabPage1.Controls.Add(this.txtToUser2);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.txtInputVoucherID);
            this.tabPage1.Controls.Add(this.txtOutputVoucherID);
            this.tabPage1.Controls.Add(this.cboToStoreSearch);
            this.tabPage1.Controls.Add(this.cboFromStoreSearch);
            this.tabPage1.Controls.Add(this.txtToUser1);
            this.tabPage1.Controls.Add(this.grpPrintStoreAddress);
            this.tabPage1.Controls.Add(this.txtPackingNumber);
            this.tabPage1.Controls.Add(this.lblPackingNumber);
            this.tabPage1.Controls.Add(this.lblBarcode);
            this.tabPage1.Controls.Add(this.cmdSearch);
            this.tabPage1.Controls.Add(this.txtBarcode);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.cboStoreChangeTypeID);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.cboTransportTypeID);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.dtChangeDate);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.txtCaskCode);
            this.tabPage1.Controls.Add(this.txtStoreChangeOrderID);
            this.tabPage1.Controls.Add(this.txtStoreChangeID);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1086, 217);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin phiếu chuyển";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cboInStockStatusID
            // 
            this.cboInStockStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInStockStatusID.FormattingEnabled = true;
            this.cboInStockStatusID.Location = new System.Drawing.Point(396, 185);
            this.cboInStockStatusID.Name = "cboInStockStatusID";
            this.cboInStockStatusID.Size = new System.Drawing.Size(180, 24);
            this.cboInStockStatusID.TabIndex = 87;
            // 
            // txtTransportVoucherID
            // 
            this.txtTransportVoucherID.BackColor = System.Drawing.Color.White;
            this.txtTransportVoucherID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtTransportVoucherID.Location = new System.Drawing.Point(119, 65);
            this.txtTransportVoucherID.MaxLength = 19;
            this.txtTransportVoucherID.Name = "txtTransportVoucherID";
            this.txtTransportVoucherID.Size = new System.Drawing.Size(271, 22);
            this.txtTransportVoucherID.TabIndex = 55;
            this.txtTransportVoucherID.Text = "0000000";
            // 
            // txtInVoiceSymbol
            // 
            this.txtInVoiceSymbol.BackColor = System.Drawing.Color.White;
            this.txtInVoiceSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtInVoiceSymbol.Location = new System.Drawing.Point(119, 93);
            this.txtInVoiceSymbol.MaxLength = 19;
            this.txtInVoiceSymbol.Name = "txtInVoiceSymbol";
            this.txtInVoiceSymbol.Size = new System.Drawing.Size(125, 22);
            this.txtInVoiceSymbol.TabIndex = 58;
            this.txtInVoiceSymbol.Text = "0000000";
            // 
            // txtInVoiceID
            // 
            this.txtInVoiceID.BackColor = System.Drawing.Color.White;
            this.txtInVoiceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtInVoiceID.Location = new System.Drawing.Point(266, 93);
            this.txtInVoiceID.MaxLength = 19;
            this.txtInVoiceID.Name = "txtInVoiceID";
            this.txtInVoiceID.Size = new System.Drawing.Size(124, 22);
            this.txtInVoiceID.TabIndex = 59;
            this.txtInVoiceID.Text = "0000000";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(250, 96);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(10, 16);
            this.label19.TabIndex = 85;
            this.label19.Text = "/";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(495, 121);
            this.txtContent.Name = "txtContent";
            this.txtContent.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContent.Properties.Appearance.Options.UseFont = true;
            this.txtContent.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.Info;
            this.txtContent.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtContent.Properties.LookAndFeel.SkinName = "Blue";
            this.txtContent.Properties.MaxLength = 2000;
            this.txtContent.Size = new System.Drawing.Size(582, 51);
            this.txtContent.TabIndex = 66;
            // 
            // txtToUser2
            // 
            this.txtToUser2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToUser2.IsOnlyShowRealStaff = false;
            this.txtToUser2.IsValidate = true;
            this.txtToUser2.Location = new System.Drawing.Point(119, 148);
            this.txtToUser2.Margin = new System.Windows.Forms.Padding(0);
            this.txtToUser2.MaximumSize = new System.Drawing.Size(400, 22);
            this.txtToUser2.MinimumSize = new System.Drawing.Size(0, 22);
            this.txtToUser2.Name = "txtToUser2";
            this.txtToUser2.Size = new System.Drawing.Size(271, 22);
            this.txtToUser2.TabIndex = 65;
            this.txtToUser2.UserName = "";
            this.txtToUser2.WorkingPositionID = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(744, 68);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(99, 16);
            this.label20.TabIndex = 82;
            this.label20.Text = "Mã phiếu nhập:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(396, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 16);
            this.label12.TabIndex = 83;
            this.label12.Text = "Mã phiếu xuất:";
            // 
            // txtInputVoucherID
            // 
            this.txtInputVoucherID.Location = new System.Drawing.Point(849, 65);
            this.txtInputVoucherID.Name = "txtInputVoucherID";
            this.txtInputVoucherID.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputVoucherID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtInputVoucherID.Properties.Appearance.Options.UseBackColor = true;
            this.txtInputVoucherID.Properties.Appearance.Options.UseFont = true;
            this.txtInputVoucherID.Properties.LookAndFeel.SkinName = "Blue";
            this.txtInputVoucherID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtInputVoucherID.Properties.ReadOnly = true;
            this.txtInputVoucherID.Size = new System.Drawing.Size(228, 22);
            this.txtInputVoucherID.TabIndex = 57;
            // 
            // txtOutputVoucherID
            // 
            this.txtOutputVoucherID.Location = new System.Drawing.Point(495, 65);
            this.txtOutputVoucherID.Name = "txtOutputVoucherID";
            this.txtOutputVoucherID.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputVoucherID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtOutputVoucherID.Properties.Appearance.Options.UseBackColor = true;
            this.txtOutputVoucherID.Properties.Appearance.Options.UseFont = true;
            this.txtOutputVoucherID.Properties.LookAndFeel.SkinName = "Blue";
            this.txtOutputVoucherID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtOutputVoucherID.Properties.ReadOnly = true;
            this.txtOutputVoucherID.Size = new System.Drawing.Size(236, 22);
            this.txtOutputVoucherID.TabIndex = 56;
            // 
            // cboToStoreSearch
            // 
            this.cboToStoreSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreSearch.Location = new System.Drawing.Point(849, 39);
            this.cboToStoreSearch.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreSearch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreSearch.Name = "cboToStoreSearch";
            this.cboToStoreSearch.Size = new System.Drawing.Size(228, 24);
            this.cboToStoreSearch.TabIndex = 54;
            // 
            // cboFromStoreSearch
            // 
            this.cboFromStoreSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreSearch.Location = new System.Drawing.Point(494, 39);
            this.cboFromStoreSearch.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreSearch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreSearch.Name = "cboFromStoreSearch";
            this.cboFromStoreSearch.Size = new System.Drawing.Size(237, 24);
            this.cboFromStoreSearch.TabIndex = 52;
            this.cboFromStoreSearch.SelectionChangeCommitted += new System.EventHandler(this.cboFromStoreSearch_SelectionChangeCommitted);
            // 
            // txtToUser1
            // 
            this.txtToUser1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToUser1.IsOnlyShowRealStaff = false;
            this.txtToUser1.IsValidate = true;
            this.txtToUser1.Location = new System.Drawing.Point(119, 121);
            this.txtToUser1.Margin = new System.Windows.Forms.Padding(0);
            this.txtToUser1.MaximumSize = new System.Drawing.Size(400, 22);
            this.txtToUser1.MinimumSize = new System.Drawing.Size(0, 22);
            this.txtToUser1.Name = "txtToUser1";
            this.txtToUser1.Size = new System.Drawing.Size(271, 22);
            this.txtToUser1.TabIndex = 63;
            this.txtToUser1.UserName = "";
            this.txtToUser1.WorkingPositionID = 0;
            // 
            // grpPrintStoreAddress
            // 
            this.grpPrintStoreAddress.Controls.Add(this.rdPrintStoreAddressA5);
            this.grpPrintStoreAddress.Controls.Add(this.rdPrintStoreAddressA4);
            this.grpPrintStoreAddress.Controls.Add(this.label15);
            this.grpPrintStoreAddress.Controls.Add(this.rdPrintStoreAddressA6);
            this.grpPrintStoreAddress.Location = new System.Drawing.Point(750, 177);
            this.grpPrintStoreAddress.Name = "grpPrintStoreAddress";
            this.grpPrintStoreAddress.Size = new System.Drawing.Size(327, 32);
            this.grpPrintStoreAddress.TabIndex = 75;
            this.grpPrintStoreAddress.TabStop = false;
            // 
            // rdPrintStoreAddressA5
            // 
            this.rdPrintStoreAddressA5.Checked = true;
            this.rdPrintStoreAddressA5.Location = new System.Drawing.Point(191, 9);
            this.rdPrintStoreAddressA5.Name = "rdPrintStoreAddressA5";
            this.rdPrintStoreAddressA5.Size = new System.Drawing.Size(43, 17);
            this.rdPrintStoreAddressA5.TabIndex = 2;
            this.rdPrintStoreAddressA5.TabStop = true;
            this.rdPrintStoreAddressA5.Text = "A5";
            this.rdPrintStoreAddressA5.UseVisualStyleBackColor = true;
            // 
            // rdPrintStoreAddressA4
            // 
            this.rdPrintStoreAddressA4.Location = new System.Drawing.Point(124, 9);
            this.rdPrintStoreAddressA4.Name = "rdPrintStoreAddressA4";
            this.rdPrintStoreAddressA4.Size = new System.Drawing.Size(47, 20);
            this.rdPrintStoreAddressA4.TabIndex = 1;
            this.rdPrintStoreAddressA4.Text = "A4";
            this.rdPrintStoreAddressA4.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 16);
            this.label15.TabIndex = 0;
            this.label15.Text = "In địa chỉ chuyển:";
            // 
            // rdPrintStoreAddressA6
            // 
            this.rdPrintStoreAddressA6.Location = new System.Drawing.Point(253, 9);
            this.rdPrintStoreAddressA6.Name = "rdPrintStoreAddressA6";
            this.rdPrintStoreAddressA6.Size = new System.Drawing.Size(66, 17);
            this.rdPrintStoreAddressA6.TabIndex = 3;
            this.rdPrintStoreAddressA6.Text = "A5 1/2";
            this.rdPrintStoreAddressA6.UseVisualStyleBackColor = true;
            // 
            // txtPackingNumber
            // 
            this.txtPackingNumber.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtPackingNumber.Location = new System.Drawing.Point(694, 185);
            this.txtPackingNumber.Name = "txtPackingNumber";
            this.txtPackingNumber.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPackingNumber.Properties.Appearance.Options.UseFont = true;
            this.txtPackingNumber.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.Info;
            this.txtPackingNumber.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtPackingNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtPackingNumber.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtPackingNumber.Size = new System.Drawing.Size(53, 22);
            this.txtPackingNumber.TabIndex = 73;
            this.txtPackingNumber.EditValueChanged += new System.EventHandler(this.txtPackingNumber_EditValueChanged);
            this.txtPackingNumber.TextChanged += new System.EventHandler(this.txtPackingNumber_TextChanged);
            // 
            // lblPackingNumber
            // 
            this.lblPackingNumber.AutoSize = true;
            this.lblPackingNumber.Location = new System.Drawing.Point(621, 188);
            this.lblPackingNumber.Name = "lblPackingNumber";
            this.lblPackingNumber.Size = new System.Drawing.Size(67, 16);
            this.lblPackingNumber.TabIndex = 81;
            this.lblPackingNumber.Text = "Thùng số:";
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(5, 189);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(63, 16);
            this.lblBarcode.TabIndex = 80;
            this.lblBarcode.Text = "Barcode:";
            // 
            // cmdSearch
            // 
            this.cmdSearch.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSearch.Appearance.Options.UseFont = true;
            this.cmdSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.cmdSearch.Location = new System.Drawing.Point(366, 185);
            this.cmdSearch.LookAndFeel.SkinName = "Blue";
            this.cmdSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(24, 22);
            this.cmdSearch.TabIndex = 69;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(119, 184);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtBarcode.Properties.Appearance.Options.UseFont = true;
            this.txtBarcode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarcode.Properties.LookAndFeel.SkinName = "Blue";
            this.txtBarcode.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtBarcode.Size = new System.Drawing.Size(246, 26);
            this.txtBarcode.TabIndex = 67;
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(744, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 16);
            this.label14.TabIndex = 74;
            this.label14.Text = "Mã số thùng:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(744, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 16);
            this.label2.TabIndex = 61;
            this.label2.Text = "Kho nhập:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(396, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 16);
            this.label1.TabIndex = 53;
            this.label1.Text = "Kho xuất:";
            // 
            // cboStoreChangeTypeID
            // 
            this.cboStoreChangeTypeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreChangeTypeID.Location = new System.Drawing.Point(119, 39);
            this.cboStoreChangeTypeID.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreChangeTypeID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreChangeTypeID.Name = "cboStoreChangeTypeID";
            this.cboStoreChangeTypeID.Size = new System.Drawing.Size(271, 24);
            this.cboStoreChangeTypeID.TabIndex = 51;
            this.cboStoreChangeTypeID.SelectionChangeCommitted += new System.EventHandler(this.cboStoreChangeTypeID_SelectionChangeCommitted);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 43);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(108, 16);
            this.label18.TabIndex = 50;
            this.label18.Text = "Loại chuyển kho:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 16);
            this.label3.TabIndex = 76;
            this.label3.Text = "Nhân viên nhập 2:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 121);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(114, 16);
            this.label16.TabIndex = 68;
            this.label16.Text = "Nhân viên nhập 1:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 14);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 16);
            this.label17.TabIndex = 46;
            this.label17.Text = "Mã phiếu:";
            // 
            // cboTransportTypeID
            // 
            this.cboTransportTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransportTypeID.FormattingEnabled = true;
            this.cboTransportTypeID.Location = new System.Drawing.Point(848, 92);
            this.cboTransportTypeID.Name = "cboTransportTypeID";
            this.cboTransportTypeID.Size = new System.Drawing.Size(229, 24);
            this.cboTransportTypeID.TabIndex = 62;
            this.cboTransportTypeID.SelectedIndexChanged += new System.EventHandler(this.txtPackingNumber_EditValueChanged);
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(396, 124);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 21);
            this.label21.TabIndex = 78;
            this.label21.Text = "Nội dung:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(744, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 16);
            this.label8.TabIndex = 77;
            this.label8.Text = "Phương tiện:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(396, 14);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 16);
            this.label13.TabIndex = 55;
            this.label13.Text = "Mã yêu cầu:";
            // 
            // dtChangeDate
            // 
            this.dtChangeDate.CustomFormat = "dd/MM/yyyy";
            this.dtChangeDate.Enabled = false;
            this.dtChangeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtChangeDate.Location = new System.Drawing.Point(494, 93);
            this.dtChangeDate.Name = "dtChangeDate";
            this.dtChangeDate.Size = new System.Drawing.Size(237, 22);
            this.dtChangeDate.TabIndex = 60;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(396, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 16);
            this.label4.TabIndex = 71;
            this.label4.Text = "Ngày chuyển:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 16);
            this.label11.TabIndex = 79;
            this.label11.Text = "Mẫu số:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 16);
            this.label9.TabIndex = 64;
            this.label9.Text = "Ký hiệu/Số:";
            // 
            // txtCaskCode
            // 
            this.txtCaskCode.Location = new System.Drawing.Point(848, 11);
            this.txtCaskCode.Name = "txtCaskCode";
            this.txtCaskCode.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtCaskCode.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtCaskCode.Properties.Appearance.Options.UseBackColor = true;
            this.txtCaskCode.Properties.Appearance.Options.UseFont = true;
            this.txtCaskCode.Properties.LookAndFeel.SkinName = "Blue";
            this.txtCaskCode.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtCaskCode.Properties.ReadOnly = true;
            this.txtCaskCode.Size = new System.Drawing.Size(229, 22);
            this.txtCaskCode.TabIndex = 49;
            // 
            // txtStoreChangeOrderID
            // 
            this.txtStoreChangeOrderID.Location = new System.Drawing.Point(495, 11);
            this.txtStoreChangeOrderID.Name = "txtStoreChangeOrderID";
            this.txtStoreChangeOrderID.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtStoreChangeOrderID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtStoreChangeOrderID.Properties.Appearance.Options.UseBackColor = true;
            this.txtStoreChangeOrderID.Properties.Appearance.Options.UseFont = true;
            this.txtStoreChangeOrderID.Properties.LookAndFeel.SkinName = "Blue";
            this.txtStoreChangeOrderID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtStoreChangeOrderID.Properties.ReadOnly = true;
            this.txtStoreChangeOrderID.Size = new System.Drawing.Size(237, 22);
            this.txtStoreChangeOrderID.TabIndex = 48;
            // 
            // txtStoreChangeID
            // 
            this.txtStoreChangeID.Location = new System.Drawing.Point(119, 11);
            this.txtStoreChangeID.Name = "txtStoreChangeID";
            this.txtStoreChangeID.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtStoreChangeID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtStoreChangeID.Properties.Appearance.Options.UseBackColor = true;
            this.txtStoreChangeID.Properties.Appearance.Options.UseFont = true;
            this.txtStoreChangeID.Properties.LookAndFeel.SkinName = "Blue";
            this.txtStoreChangeID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtStoreChangeID.Properties.ReadOnly = true;
            this.txtStoreChangeID.Size = new System.Drawing.Size(271, 22);
            this.txtStoreChangeID.TabIndex = 47;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtReciveNote);
            this.tabPage2.Controls.Add(this.lblInputStatus);
            this.tabPage2.Controls.Add(this.chkIsRecived);
            this.tabPage2.Controls.Add(this.txtSignReceiveNote);
            this.tabPage2.Controls.Add(this.lblSignReceiveStatus);
            this.tabPage2.Controls.Add(this.chkIsSignReceive);
            this.tabPage2.Controls.Add(this.lblReciveStatus);
            this.tabPage2.Controls.Add(this.chkIsRecivedTransfer);
            this.tabPage2.Controls.Add(this.lblTransferStatus);
            this.tabPage2.Controls.Add(this.chkIsTransfered);
            this.tabPage2.Controls.Add(this.lblOutputStatus);
            this.tabPage2.Controls.Add(this.chkIsOutputed);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1086, 217);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Thông tin xuất nhập kho";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Enter += new System.EventHandler(this.tabPage2_Enter);
            // 
            // txtReciveNote
            // 
            this.txtReciveNote.BackColor = System.Drawing.SystemColors.Info;
            this.txtReciveNote.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtReciveNote.Location = new System.Drawing.Point(32, 177);
            this.txtReciveNote.Multiline = true;
            this.txtReciveNote.Name = "txtReciveNote";
            this.txtReciveNote.ReadOnly = true;
            this.txtReciveNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReciveNote.Size = new System.Drawing.Size(461, 35);
            this.txtReciveNote.TabIndex = 19;
            // 
            // lblInputStatus
            // 
            this.lblInputStatus.AutoSize = true;
            this.lblInputStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblInputStatus.Location = new System.Drawing.Point(236, 158);
            this.lblInputStatus.Name = "lblInputStatus";
            this.lblInputStatus.Size = new System.Drawing.Size(0, 16);
            this.lblInputStatus.TabIndex = 17;
            // 
            // chkIsRecived
            // 
            this.chkIsRecived.Enabled = false;
            this.chkIsRecived.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkIsRecived.Location = new System.Drawing.Point(12, 156);
            this.chkIsRecived.Name = "chkIsRecived";
            this.chkIsRecived.Size = new System.Drawing.Size(213, 20);
            this.chkIsRecived.TabIndex = 16;
            this.chkIsRecived.Text = "Đã xác nhận thực nhập kho";
            this.chkIsRecived.UseVisualStyleBackColor = true;
            // 
            // txtSignReceiveNote
            // 
            this.txtSignReceiveNote.BackColor = System.Drawing.SystemColors.Info;
            this.txtSignReceiveNote.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSignReceiveNote.Location = new System.Drawing.Point(32, 112);
            this.txtSignReceiveNote.Multiline = true;
            this.txtSignReceiveNote.Name = "txtSignReceiveNote";
            this.txtSignReceiveNote.ReadOnly = true;
            this.txtSignReceiveNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSignReceiveNote.Size = new System.Drawing.Size(461, 35);
            this.txtSignReceiveNote.TabIndex = 15;
            // 
            // lblSignReceiveStatus
            // 
            this.lblSignReceiveStatus.AutoSize = true;
            this.lblSignReceiveStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSignReceiveStatus.Location = new System.Drawing.Point(236, 91);
            this.lblSignReceiveStatus.Name = "lblSignReceiveStatus";
            this.lblSignReceiveStatus.Size = new System.Drawing.Size(0, 16);
            this.lblSignReceiveStatus.TabIndex = 13;
            // 
            // chkIsSignReceive
            // 
            this.chkIsSignReceive.Enabled = false;
            this.chkIsSignReceive.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkIsSignReceive.Location = new System.Drawing.Point(12, 89);
            this.chkIsSignReceive.Name = "chkIsSignReceive";
            this.chkIsSignReceive.Size = new System.Drawing.Size(213, 20);
            this.chkIsSignReceive.TabIndex = 12;
            this.chkIsSignReceive.Text = "Đã ký nhận tại kho nhập";
            this.chkIsSignReceive.UseVisualStyleBackColor = true;
            // 
            // lblReciveStatus
            // 
            this.lblReciveStatus.AutoSize = true;
            this.lblReciveStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblReciveStatus.Location = new System.Drawing.Point(236, 65);
            this.lblReciveStatus.Name = "lblReciveStatus";
            this.lblReciveStatus.Size = new System.Drawing.Size(0, 16);
            this.lblReciveStatus.TabIndex = 11;
            // 
            // chkIsRecivedTransfer
            // 
            this.chkIsRecivedTransfer.Enabled = false;
            this.chkIsRecivedTransfer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkIsRecivedTransfer.Location = new System.Drawing.Point(12, 63);
            this.chkIsRecivedTransfer.Name = "chkIsRecivedTransfer";
            this.chkIsRecivedTransfer.Size = new System.Drawing.Size(213, 20);
            this.chkIsRecivedTransfer.TabIndex = 10;
            this.chkIsRecivedTransfer.Text = "Đã nhận hàng để vận chuyển";
            this.chkIsRecivedTransfer.UseVisualStyleBackColor = true;
            // 
            // lblTransferStatus
            // 
            this.lblTransferStatus.AutoSize = true;
            this.lblTransferStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblTransferStatus.Location = new System.Drawing.Point(236, 39);
            this.lblTransferStatus.Name = "lblTransferStatus";
            this.lblTransferStatus.Size = new System.Drawing.Size(0, 16);
            this.lblTransferStatus.TabIndex = 9;
            // 
            // chkIsTransfered
            // 
            this.chkIsTransfered.Enabled = false;
            this.chkIsTransfered.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkIsTransfered.Location = new System.Drawing.Point(12, 37);
            this.chkIsTransfered.Name = "chkIsTransfered";
            this.chkIsTransfered.Size = new System.Drawing.Size(213, 20);
            this.chkIsTransfered.TabIndex = 8;
            this.chkIsTransfered.Text = "Đã giao hàng cho vận chuyển";
            this.chkIsTransfered.UseVisualStyleBackColor = true;
            // 
            // lblOutputStatus
            // 
            this.lblOutputStatus.AutoSize = true;
            this.lblOutputStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblOutputStatus.Location = new System.Drawing.Point(236, 13);
            this.lblOutputStatus.Name = "lblOutputStatus";
            this.lblOutputStatus.Size = new System.Drawing.Size(0, 16);
            this.lblOutputStatus.TabIndex = 7;
            // 
            // chkIsOutputed
            // 
            this.chkIsOutputed.Enabled = false;
            this.chkIsOutputed.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkIsOutputed.Location = new System.Drawing.Point(12, 11);
            this.chkIsOutputed.Name = "chkIsOutputed";
            this.chkIsOutputed.Size = new System.Drawing.Size(213, 20);
            this.chkIsOutputed.TabIndex = 6;
            this.chkIsOutputed.Text = "Đã xuất kho";
            this.chkIsOutputed.UseVisualStyleBackColor = true;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.grdInputVoucherDetail);
            this.groupControl1.Controls.Add(this.grdTemp);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 246);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1094, 211);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Chi tiết phiếu chuyển";
            // 
            // grdInputVoucherDetail
            // 
            this.grdInputVoucherDetail.ContextMenuStrip = this.mnuProduct;
            this.grdInputVoucherDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdInputVoucherDetail.Location = new System.Drawing.Point(2, 24);
            this.grdInputVoucherDetail.MainView = this.grvInputVoucherDetail;
            this.grdInputVoucherDetail.Name = "grdInputVoucherDetail";
            this.grdInputVoucherDetail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit3,
            this.btn_Barcode,
            this.btn_ImportExcel,
            this.btn_genIMEI,
            this.btn_getFIFO});
            this.grdInputVoucherDetail.Size = new System.Drawing.Size(1090, 185);
            this.grdInputVoucherDetail.TabIndex = 0;
            this.grdInputVoucherDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvInputVoucherDetail});
            // 
            // grvInputVoucherDetail
            // 
            this.grvInputVoucherDetail.Appearance.FocusedRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grvInputVoucherDetail.Appearance.FocusedRow.Options.UseFont = true;
            this.grvInputVoucherDetail.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvInputVoucherDetail.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.grvInputVoucherDetail.Appearance.FooterPanel.Options.UseFont = true;
            this.grvInputVoucherDetail.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grvInputVoucherDetail.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvInputVoucherDetail.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvInputVoucherDetail.Appearance.Row.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grvInputVoucherDetail.Appearance.Row.Options.UseFont = true;
            this.grvInputVoucherDetail.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.MicrosoftSansSerif,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.bandORDERQUANTITY,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8,
            this.gridBand9,
            this.Tahoma,
            this.gridBand15,
            this.gridBand2});
            this.grvInputVoucherDetail.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.chkSelect,
            this.colProductID,
            this.gridColumn3,
            this.gridColumn4,
            this.colORDERQUANTITY,
            this.gridColumn5,
            this.gridColumn8,
            this.gridColumn9,
            this.colAmount,
            this.colBarcode,
            this.colImportExcel,
            this.colGenIMEI,
            this.colGetFIFO,
            this.colNOTE});
            this.grvInputVoucherDetail.DetailHeight = 450;
            this.grvInputVoucherDetail.GridControl = this.grdInputVoucherDetail;
            this.grvInputVoucherDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", this.gridColumn5, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ORDERQUANTITY", this.colORDERQUANTITY, "")});
            this.grvInputVoucherDetail.Name = "grvInputVoucherDetail";
            this.grvInputVoucherDetail.OptionsDetail.ShowDetailTabs = false;
            this.grvInputVoucherDetail.OptionsView.ColumnAutoWidth = false;
            this.grvInputVoucherDetail.OptionsView.ShowColumnHeaders = false;
            this.grvInputVoucherDetail.OptionsView.ShowFooter = true;
            this.grvInputVoucherDetail.OptionsView.ShowGroupPanel = false;
            this.grvInputVoucherDetail.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvInputVoucherDetail_ShowingEditor);
            this.grvInputVoucherDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvInputVoucherDetail_CellValueChanged);
            // 
            // MicrosoftSansSerif
            // 
            this.MicrosoftSansSerif.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.MicrosoftSansSerif.AppearanceHeader.Options.UseFont = true;
            this.MicrosoftSansSerif.AppearanceHeader.Options.UseTextOptions = true;
            this.MicrosoftSansSerif.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MicrosoftSansSerif.Caption = "Chọn";
            this.MicrosoftSansSerif.Columns.Add(this.chkSelect);
            this.MicrosoftSansSerif.MinWidth = 20;
            this.MicrosoftSansSerif.Name = "MicrosoftSansSerif";
            this.MicrosoftSansSerif.RowCount = 2;
            this.MicrosoftSansSerif.Width = 52;
            // 
            // chkSelect
            // 
            this.chkSelect.Caption = "Chọn";
            this.chkSelect.ColumnEdit = this.repositoryItemCheckEdit1;
            this.chkSelect.FieldName = "ISSELECT";
            this.chkSelect.Name = "chkSelect";
            this.chkSelect.Visible = true;
            this.chkSelect.Width = 52;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemCheckEdit1.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.repositoryItemCheckEdit1.CheckedChanged += new System.EventHandler(this.chkIsSelect_CheckedChanged);
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Mã sản phẩm";
            this.gridBand3.Columns.Add(this.colProductID);
            this.gridBand3.MinWidth = 20;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 112;
            // 
            // colProductID
            // 
            this.colProductID.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductID.Caption = "Mã sản phẩm";
            this.colProductID.FieldName = "PRODUCTID";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colProductID.Visible = true;
            this.colProductID.Width = 112;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Tên sản phẩm";
            this.gridBand4.Columns.Add(this.gridColumn3);
            this.gridBand4.MinWidth = 20;
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 230;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "Tên sản phẩm";
            this.gridColumn3.FieldName = "PRODUCTNAME";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.Visible = true;
            this.gridColumn3.Width = 230;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "ĐVT";
            this.gridBand5.Columns.Add(this.gridColumn4);
            this.gridBand5.MinWidth = 20;
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Width = 63;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.Caption = "ĐVT";
            this.gridColumn4.FieldName = "QUANTITYUNITNAME";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn4.Visible = true;
            this.gridColumn4.Width = 63;
            // 
            // bandORDERQUANTITY
            // 
            this.bandORDERQUANTITY.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.bandORDERQUANTITY.AppearanceHeader.Options.UseFont = true;
            this.bandORDERQUANTITY.AppearanceHeader.Options.UseTextOptions = true;
            this.bandORDERQUANTITY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandORDERQUANTITY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandORDERQUANTITY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandORDERQUANTITY.Caption = "SL yêu cầu";
            this.bandORDERQUANTITY.Columns.Add(this.colORDERQUANTITY);
            this.bandORDERQUANTITY.MinWidth = 20;
            this.bandORDERQUANTITY.Name = "bandORDERQUANTITY";
            this.bandORDERQUANTITY.RowCount = 2;
            this.bandORDERQUANTITY.Width = 63;
            // 
            // colORDERQUANTITY
            // 
            this.colORDERQUANTITY.AppearanceHeader.Options.UseTextOptions = true;
            this.colORDERQUANTITY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colORDERQUANTITY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colORDERQUANTITY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colORDERQUANTITY.Caption = "SL yêu cầu";
            this.colORDERQUANTITY.DisplayFormat.FormatString = "#,##0.####";
            this.colORDERQUANTITY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colORDERQUANTITY.FieldName = "ORDERQUANTITY";
            this.colORDERQUANTITY.Name = "colORDERQUANTITY";
            this.colORDERQUANTITY.OptionsColumn.AllowEdit = false;
            this.colORDERQUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ORDERQUANTITY", "{0:N0}")});
            this.colORDERQUANTITY.Visible = true;
            this.colORDERQUANTITY.Width = 63;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand6.Caption = "SL Xuất";
            this.gridBand6.Columns.Add(this.gridColumn5);
            this.gridBand6.MinWidth = 20;
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Width = 62;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "SL xuất";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn5.DisplayFormat.FormatString = "#,##0.##";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn5.FieldName = "QUANTITY";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:N0}")});
            this.gridColumn5.Visible = true;
            this.gridColumn5.Width = 62;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "#,###,###,###,##0.##";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            this.repositoryItemTextEdit3.NullText = "0";
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Thùng số";
            this.gridBand7.Columns.Add(this.gridColumn8);
            this.gridBand7.MinWidth = 20;
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.Visible = false;
            this.gridBand7.Width = 102;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn8.Caption = "Thùng số";
            this.gridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn8.FieldName = "PACKINGNUMBER";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn8.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumn8.Width = 102;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand8.Caption = "Tổng trọng lượng (Kg)";
            this.gridBand8.Columns.Add(this.gridColumn9);
            this.gridBand8.MinWidth = 20;
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.RowCount = 2;
            this.gridBand8.Visible = false;
            this.gridBand8.Width = 96;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Tổng trọng lượng (Kg)";
            this.gridColumn9.FieldName = "TOTALWEIGHT";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn9.Width = 96;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand9.Caption = "Tổng thể tích";
            this.gridBand9.Columns.Add(this.colAmount);
            this.gridBand9.MinWidth = 20;
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.RowCount = 2;
            this.gridBand9.Visible = false;
            this.gridBand9.Width = 68;
            // 
            // colAmount
            // 
            this.colAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAmount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAmount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAmount.Caption = "Tổng thể tích";
            this.colAmount.FieldName = "TOTALSIZE";
            this.colAmount.Name = "colAmount";
            this.colAmount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colAmount.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.colAmount.Width = 68;
            // 
            // Tahoma
            // 
            this.Tahoma.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tahoma.AppearanceHeader.Options.UseFont = true;
            this.Tahoma.AppearanceHeader.Options.UseTextOptions = true;
            this.Tahoma.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tahoma.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Tahoma.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Tahoma.Caption = "Ghi chú";
            this.Tahoma.Columns.Add(this.colNOTE);
            this.Tahoma.MinWidth = 20;
            this.Tahoma.Name = "Tahoma";
            this.Tahoma.Width = 150;
            // 
            // colNOTE
            // 
            this.colNOTE.Caption = "Ghi chú";
            this.colNOTE.FieldName = "NOTE";
            this.colNOTE.Name = "colNOTE";
            this.colNOTE.Visible = true;
            this.colNOTE.Width = 150;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand15.AppearanceHeader.Options.UseFont = true;
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand15.Caption = "SL yêu cầu";
            this.gridBand15.MinWidth = 20;
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.RowCount = 2;
            this.gridBand15.Visible = false;
            this.gridBand15.Width = 63;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Chức năng";
            this.gridBand2.Columns.Add(this.colBarcode);
            this.gridBand2.Columns.Add(this.colImportExcel);
            this.gridBand2.Columns.Add(this.colGenIMEI);
            this.gridBand2.Columns.Add(this.colGetFIFO);
            this.gridBand2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 327;
            // 
            // colBarcode
            // 
            this.colBarcode.Caption = "Bắn IMEI";
            this.colBarcode.ColumnEdit = this.btn_Barcode;
            this.colBarcode.Name = "colBarcode";
            this.colBarcode.Visible = true;
            this.colBarcode.Width = 77;
            // 
            // btn_Barcode
            // 
            this.btn_Barcode.AutoHeight = false;
            serializableAppearanceObject1.Options.UseTextOptions = true;
            this.btn_Barcode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Bắn IMEI", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleLeft, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btn_Barcode.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btn_Barcode.LookAndFeel.SkinName = "Caramel";
            this.btn_Barcode.Name = "btn_Barcode";
            this.btn_Barcode.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn_Barcode.Click += new System.EventHandler(this.btn_Barcode_Click);
            // 
            // colImportExcel
            // 
            this.colImportExcel.Caption = "Import Excel";
            this.colImportExcel.ColumnEdit = this.btn_ImportExcel;
            this.colImportExcel.Name = "colImportExcel";
            this.colImportExcel.Visible = true;
            this.colImportExcel.Width = 94;
            // 
            // btn_ImportExcel
            // 
            this.btn_ImportExcel.AutoHeight = false;
            this.btn_ImportExcel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Import Excel", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleLeft, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btn_ImportExcel.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btn_ImportExcel.LookAndFeel.SkinName = "Blue";
            this.btn_ImportExcel.Name = "btn_ImportExcel";
            this.btn_ImportExcel.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn_ImportExcel.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.btn_ImportExcel_CustomDisplayText);
            this.btn_ImportExcel.Click += new System.EventHandler(this.btn_ImportExcel_Click);
            // 
            // colGenIMEI
            // 
            this.colGenIMEI.Caption = "Sinh IMEI";
            this.colGenIMEI.ColumnEdit = this.btn_genIMEI;
            this.colGenIMEI.Name = "colGenIMEI";
            this.colGenIMEI.Visible = true;
            this.colGenIMEI.Width = 77;
            // 
            // btn_genIMEI
            // 
            this.btn_genIMEI.AutoHeight = false;
            this.btn_genIMEI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sinh IMEI", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleLeft, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.btn_genIMEI.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btn_genIMEI.LookAndFeel.SkinName = "Black";
            this.btn_genIMEI.Name = "btn_genIMEI";
            this.btn_genIMEI.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn_genIMEI.Click += new System.EventHandler(this.btn_genIMEI_Click);
            // 
            // colGetFIFO
            // 
            this.colGetFIFO.Caption = "Lấy FIFO";
            this.colGetFIFO.ColumnEdit = this.btn_getFIFO;
            this.colGetFIFO.Name = "colGetFIFO";
            this.colGetFIFO.Visible = true;
            this.colGetFIFO.Width = 79;
            // 
            // btn_getFIFO
            // 
            this.btn_getFIFO.AutoHeight = false;
            this.btn_getFIFO.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Lấy FIFO", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleLeft, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.btn_getFIFO.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btn_getFIFO.LookAndFeel.SkinName = "Lilian";
            this.btn_getFIFO.Name = "btn_getFIFO";
            this.btn_getFIFO.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn_getFIFO.Click += new System.EventHandler(this.btn_getFIFO_Click);
            // 
            // grdTemp
            // 
            this.grdTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdTemp.Location = new System.Drawing.Point(2, 24);
            this.grdTemp.MainView = this.grvTemp;
            this.grdTemp.Name = "grdTemp";
            this.grdTemp.Size = new System.Drawing.Size(1090, 185);
            this.grdTemp.TabIndex = 16;
            this.grdTemp.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvTemp,
            this.gridView4,
            this.gridView5});
            this.grdTemp.Visible = false;
            // 
            // grvTemp
            // 
            this.grvTemp.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvTemp.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvTemp.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvTemp.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvTemp.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvTemp.Appearance.Row.Options.UseFont = true;
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[Status]=true";
            styleFormatCondition1.Value1 = true;
            this.grvTemp.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.grvTemp.GridControl = this.grdTemp;
            this.grvTemp.Name = "grvTemp";
            this.grvTemp.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
            this.grvTemp.OptionsView.ColumnAutoWidth = false;
            this.grvTemp.OptionsView.ShowAutoFilterRow = true;
            this.grvTemp.OptionsView.ShowFooter = true;
            this.grvTemp.OptionsView.ShowGroupPanel = false;
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.grdTemp;
            this.gridView4.Name = "gridView4";
            // 
            // gridView5
            // 
            this.gridView5.GridControl = this.grdTemp;
            this.gridView5.Name = "gridView5";
            // 
            // colIMEI
            // 
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 3;
            this.colIMEI.Width = 140;
            // 
            // colENDWARRANTYDATE
            // 
            this.colENDWARRANTYDATE.AppearanceHeader.Options.UseTextOptions = true;
            this.colENDWARRANTYDATE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colENDWARRANTYDATE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colENDWARRANTYDATE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colENDWARRANTYDATE.Caption = "Ngày hết BH";
            this.colENDWARRANTYDATE.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colENDWARRANTYDATE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colENDWARRANTYDATE.FieldName = "ENDWARRANTYDATE";
            this.colENDWARRANTYDATE.Name = "colENDWARRANTYDATE";
            this.colENDWARRANTYDATE.OptionsColumn.AllowEdit = false;
            this.colENDWARRANTYDATE.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colENDWARRANTYDATE.Visible = true;
            this.colENDWARRANTYDATE.VisibleIndex = 6;
            this.colENDWARRANTYDATE.Width = 81;
            // 
            // colISHASWARRANTY
            // 
            this.colISHASWARRANTY.AppearanceHeader.Options.UseTextOptions = true;
            this.colISHASWARRANTY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colISHASWARRANTY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colISHASWARRANTY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colISHASWARRANTY.Caption = "Có bảo hành";
            this.colISHASWARRANTY.FieldName = "ISHASWARRANTY";
            this.colISHASWARRANTY.Name = "colISHASWARRANTY";
            this.colISHASWARRANTY.OptionsColumn.AllowEdit = false;
            this.colISHASWARRANTY.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colISHASWARRANTY.Visible = true;
            this.colISHASWARRANTY.VisibleIndex = 7;
            this.colISHASWARRANTY.Width = 88;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmStoreChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 491);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1000, 525);
            this.Name = "frmStoreChange";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xuất chuyển kho";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmStoreChange_FormClosing);
            this.Load += new System.EventHandler(this.frmStoreChange_Load);
            this.mnuProduct.ResumeLayout(false);
            this.mnuSelectToUser1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pMnu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProductCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputVoucherID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOutputVoucherID.Properties)).EndInit();
            this.grpPrintStoreAddress.ResumeLayout(false);
            this.grpPrintStoreAddress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPackingNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaskCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreChangeOrderID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreChangeID.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdInputVoucherDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvInputVoucherDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_ImportExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_genIMEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_getFIFO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnUpdate;
        private System.Windows.Forms.ContextMenuStrip mnuSelectToUser1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSelectToUser1;
        private System.Windows.Forms.ContextMenuStrip mnuProduct;
        private System.Windows.Forms.ToolStripMenuItem mnuDelProduct;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit lblTotalSize;
        private DevExpress.XtraEditors.TextEdit lbTotalWeight;
        private DevExpress.XtraEditors.TextEdit lblTotalQuantity;
        private DevExpress.XtraEditors.TextEdit lblProductCount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcelTemp;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportExcel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit txtInputVoucherID;
        private DevExpress.XtraEditors.TextEdit txtOutputVoucherID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreSearch;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreSearch;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch txtToUser2;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch txtToUser1;
        private System.Windows.Forms.GroupBox grpPrintStoreAddress;
        private System.Windows.Forms.RadioButton rdPrintStoreAddressA5;
        private System.Windows.Forms.RadioButton rdPrintStoreAddressA4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton rdPrintStoreAddressA6;
        private DevExpress.XtraEditors.SpinEdit txtPackingNumber;
        private System.Windows.Forms.Label lblPackingNumber;
        private System.Windows.Forms.Label lblBarcode;
        private DevExpress.XtraEditors.SimpleButton cmdSearch;
        private DevExpress.XtraEditors.TextEdit txtBarcode;
        private DevExpress.XtraEditors.MemoEdit txtContent;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStoreChangeTypeID;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cboTransportTypeID;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dtChangeDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.TextEdit txtCaskCode;
        private Library.AppControl.TextBoxControl.ASCIIText txtInVoiceID;
        private DevExpress.XtraEditors.TextEdit txtStoreChangeOrderID;
        private DevExpress.XtraEditors.TextEdit txtStoreChangeID;
        private System.Windows.Forms.TabPage tabPage2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.DropDownButton btnAction;
        private DevExpress.XtraBars.PopupMenu pMnu;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private System.Windows.Forms.TextBox txtReciveNote;
        private System.Windows.Forms.Label lblInputStatus;
        private System.Windows.Forms.CheckBox chkIsRecived;
        private System.Windows.Forms.TextBox txtSignReceiveNote;
        private System.Windows.Forms.Label lblSignReceiveStatus;
        private System.Windows.Forms.CheckBox chkIsSignReceive;
        private System.Windows.Forms.Label lblReciveStatus;
        private System.Windows.Forms.CheckBox chkIsRecivedTransfer;
        private System.Windows.Forms.Label lblTransferStatus;
        private System.Windows.Forms.CheckBox chkIsTransfered;
        private System.Windows.Forms.Label lblOutputStatus;
        private System.Windows.Forms.CheckBox chkIsOutputed;
        private System.Windows.Forms.Label label19;
        private Library.AppControl.TextBoxControl.ASCIIText txtInVoiceSymbol;
        private Library.AppControl.TextBoxControl.ASCIIText txtTransportVoucherID;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colENDWARRANTYDATE;
        private DevExpress.XtraGrid.Columns.GridColumn colISHASWARRANTY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNOTE;
        private DevExpress.XtraGrid.GridControl grdInputVoucherDetail;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView grvInputVoucherDetail;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn chkSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colProductID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAmount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBarcode;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_Barcode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colImportExcel;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_ImportExcel;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGenIMEI;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_genIMEI;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGetFIFO;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_getFIFO;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colORDERQUANTITY;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCheckRealInput;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand MicrosoftSansSerif;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandORDERQUANTITY;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand Tahoma;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private System.Windows.Forms.ToolStripMenuItem mnuDelAllImei;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSubIMEI;
        private DevExpress.XtraGrid.GridControl grdTemp;
        private DevExpress.XtraGrid.Views.Grid.GridView grvTemp;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private System.Windows.Forms.ComboBox cboInStockStatusID;
        private System.Windows.Forms.ToolStripMenuItem mnuGhiChuIMEI;
    }
}