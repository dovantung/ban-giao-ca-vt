﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmRePrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRePrint));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtReason = new System.Windows.Forms.RichTextBox();
            this.lblReason = new System.Windows.Forms.Label();
            this.txtOldNumber = new System.Windows.Forms.TextBox();
            this.lblOldNumber = new System.Windows.Forms.Label();
            this.radioPrintNew = new System.Windows.Forms.RadioButton();
            this.radioPrintOld = new System.Windows.Forms.RadioButton();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.txtSymbol = new System.Windows.Forms.TextBox();
            this.txtNewNumber = new System.Windows.Forms.TextBox();
            this.txtMauSo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNewNumber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.grdHistoryPrint = new DevExpress.XtraGrid.GridControl();
            this.grvHistoryPrint = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsHasWarranty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkIsWarranty = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.cboMaThietLap = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHistoryPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHistoryPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsWarranty)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(752, 290);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.cboMaThietLap);
            this.tabPage1.Controls.Add(this.txtReason);
            this.tabPage1.Controls.Add(this.lblReason);
            this.tabPage1.Controls.Add(this.txtOldNumber);
            this.tabPage1.Controls.Add(this.lblOldNumber);
            this.tabPage1.Controls.Add(this.radioPrintNew);
            this.tabPage1.Controls.Add(this.radioPrintOld);
            this.tabPage1.Controls.Add(this.btnPrint);
            this.tabPage1.Controls.Add(this.btnUndo);
            this.tabPage1.Controls.Add(this.txtSymbol);
            this.tabPage1.Controls.Add(this.txtNewNumber);
            this.tabPage1.Controls.Add(this.txtMauSo);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.lblNewNumber);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(744, 261);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "In PXK kiêm VCNB";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtReason
            // 
            this.txtReason.Location = new System.Drawing.Point(99, 117);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(202, 113);
            this.txtReason.TabIndex = 27;
            this.txtReason.Text = "";
            // 
            // lblReason
            // 
            this.lblReason.AutoSize = true;
            this.lblReason.Location = new System.Drawing.Point(6, 132);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(74, 16);
            this.lblReason.TabIndex = 26;
            this.lblReason.Text = "Lý do in lại:";
            // 
            // txtOldNumber
            // 
            this.txtOldNumber.Location = new System.Drawing.Point(99, 80);
            this.txtOldNumber.Name = "txtOldNumber";
            this.txtOldNumber.Size = new System.Drawing.Size(202, 22);
            this.txtOldNumber.TabIndex = 25;
            // 
            // lblOldNumber
            // 
            this.lblOldNumber.AutoSize = true;
            this.lblOldNumber.Location = new System.Drawing.Point(3, 83);
            this.lblOldNumber.Name = "lblOldNumber";
            this.lblOldNumber.Size = new System.Drawing.Size(81, 16);
            this.lblOldNumber.TabIndex = 24;
            this.lblOldNumber.Text = "Số phiếu cũ:";
            // 
            // radioPrintNew
            // 
            this.radioPrintNew.AutoSize = true;
            this.radioPrintNew.Location = new System.Drawing.Point(167, 6);
            this.radioPrintNew.Name = "radioPrintNew";
            this.radioPrintNew.Size = new System.Drawing.Size(79, 20);
            this.radioPrintNew.TabIndex = 23;
            this.radioPrintNew.Text = "In số mới";
            this.radioPrintNew.UseVisualStyleBackColor = true;
            this.radioPrintNew.CheckedChanged += new System.EventHandler(this.radioPrintNew_CheckedChanged);
            // 
            // radioPrintOld
            // 
            this.radioPrintOld.AutoSize = true;
            this.radioPrintOld.Checked = true;
            this.radioPrintOld.Location = new System.Drawing.Point(19, 6);
            this.radioPrintOld.Name = "radioPrintOld";
            this.radioPrintOld.Size = new System.Drawing.Size(106, 20);
            this.radioPrintOld.TabIndex = 22;
            this.radioPrintOld.TabStop = true;
            this.radioPrintOld.Text = "In lại phiếu cũ";
            this.radioPrintOld.UseVisualStyleBackColor = true;
            this.radioPrintOld.CheckedChanged += new System.EventHandler(this.radioPrintOld_CheckedChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(435, 156);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(95, 25);
            this.btnPrint.TabIndex = 21;
            this.btnPrint.Text = "   In";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.Image")));
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(536, 156);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(95, 25);
            this.btnUndo.TabIndex = 20;
            this.btnUndo.Text = "   Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // txtSymbol
            // 
            this.txtSymbol.Location = new System.Drawing.Point(432, 78);
            this.txtSymbol.Name = "txtSymbol";
            this.txtSymbol.Size = new System.Drawing.Size(199, 22);
            this.txtSymbol.TabIndex = 18;
            // 
            // txtNewNumber
            // 
            this.txtNewNumber.Location = new System.Drawing.Point(432, 120);
            this.txtNewNumber.Name = "txtNewNumber";
            this.txtNewNumber.Size = new System.Drawing.Size(199, 22);
            this.txtNewNumber.TabIndex = 17;
            this.txtNewNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNewNumber_KeyPress);
            // 
            // txtMauSo
            // 
            this.txtMauSo.Location = new System.Drawing.Point(432, 38);
            this.txtMauSo.Name = "txtMauSo";
            this.txtMauSo.Size = new System.Drawing.Size(198, 22);
            this.txtMauSo.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(337, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 16);
            this.label3.TabIndex = 15;
            this.label3.Text = "Ký hiệu:";
            // 
            // lblNewNumber
            // 
            this.lblNewNumber.AutoSize = true;
            this.lblNewNumber.Location = new System.Drawing.Point(337, 126);
            this.lblNewNumber.Name = "lblNewNumber";
            this.lblNewNumber.Size = new System.Drawing.Size(89, 16);
            this.lblNewNumber.TabIndex = 14;
            this.lblNewNumber.Text = "Số phiếu mới:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(337, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "Mẫu số:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.grdHistoryPrint);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(744, 261);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Lịch sử in";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // grdHistoryPrint
            // 
            this.grdHistoryPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHistoryPrint.Location = new System.Drawing.Point(3, 3);
            this.grdHistoryPrint.MainView = this.grvHistoryPrint;
            this.grdHistoryPrint.Name = "grdHistoryPrint";
            this.grdHistoryPrint.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.chkIsWarranty});
            this.grdHistoryPrint.Size = new System.Drawing.Size(738, 255);
            this.grdHistoryPrint.TabIndex = 14;
            this.grdHistoryPrint.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvHistoryPrint});
            // 
            // grvHistoryPrint
            // 
            this.grvHistoryPrint.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvHistoryPrint.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvHistoryPrint.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvHistoryPrint.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvHistoryPrint.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvHistoryPrint.Appearance.Row.Options.UseFont = true;
            this.grvHistoryPrint.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grvHistoryPrint.ColumnPanelRowHeight = 50;
            this.grvHistoryPrint.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colProductID,
            this.colProductName,
            this.colIMEI,
            this.colIsHasWarranty,
            this.colStatus,
            this.gridColumn1,
            this.gridColumn2});
            styleFormatCondition2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition2.Appearance.Options.UseBackColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition2.Expression = "[Status]=true";
            styleFormatCondition2.Value1 = true;
            this.grvHistoryPrint.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.grvHistoryPrint.GridControl = this.grdHistoryPrint;
            this.grvHistoryPrint.Name = "grvHistoryPrint";
            this.grvHistoryPrint.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
            this.grvHistoryPrint.OptionsView.ShowAutoFilterRow = true;
            this.grvHistoryPrint.OptionsView.ShowGroupPanel = false;
            this.grvHistoryPrint.OptionsView.ShowIndicator = false;
            // 
            // colProductID
            // 
            this.colProductID.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID.Caption = "Ngày in";
            this.colProductID.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colProductID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colProductID.FieldName = "PRINTEDDATE";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.ReadOnly = true;
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 0;
            this.colProductID.Width = 110;
            // 
            // colProductName
            // 
            this.colProductName.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductName.Caption = "Người in";
            this.colProductName.FieldName = "PRINTEDBY";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 1;
            this.colProductName.Width = 92;
            // 
            // colIMEI
            // 
            this.colIMEI.AppearanceHeader.Options.UseTextOptions = true;
            this.colIMEI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIMEI.Caption = "Số phiếu";
            this.colIMEI.FieldName = "INVOICEID";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 2;
            this.colIMEI.Width = 82;
            // 
            // colIsHasWarranty
            // 
            this.colIsHasWarranty.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsHasWarranty.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsHasWarranty.Caption = "Mẫu số";
            this.colIsHasWarranty.FieldName = "DENOMINATOR";
            this.colIsHasWarranty.Name = "colIsHasWarranty";
            this.colIsHasWarranty.OptionsColumn.AllowEdit = false;
            this.colIsHasWarranty.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIsHasWarranty.Visible = true;
            this.colIsHasWarranty.VisibleIndex = 4;
            this.colIsHasWarranty.Width = 85;
            // 
            // colStatus
            // 
            this.colStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatus.Caption = "Ký hiệu";
            this.colStatus.FieldName = "INVOICESYMBOL";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 5;
            this.colStatus.Width = 64;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Lý do in lại";
            this.gridColumn1.FieldName = "PRINTEDREASON";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 6;
            this.gridColumn1.Width = 126;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "Số phiếu cũ";
            this.gridColumn2.FieldName = "INVOICEIDOLD";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            this.gridColumn2.Width = 78;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // chkIsWarranty
            // 
            this.chkIsWarranty.AutoHeight = false;
            this.chkIsWarranty.Name = "chkIsWarranty";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 16);
            this.label4.TabIndex = 29;
            this.label4.Text = "Mã thiết lập:";
            // 
            // cboMaThietLap
            // 
            this.cboMaThietLap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMaThietLap.FormattingEnabled = true;
            this.cboMaThietLap.Location = new System.Drawing.Point(99, 33);
            this.cboMaThietLap.Name = "cboMaThietLap";
            this.cboMaThietLap.Size = new System.Drawing.Size(202, 24);
            this.cboMaThietLap.TabIndex = 28;
            this.cboMaThietLap.SelectedIndexChanged += new System.EventHandler(this.cboMaThietLap_SelectedIndexChanged);
            // 
            // frmRePrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 290);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmRePrint";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "In lại PXK kiêm VCNB";
            this.Load += new System.EventHandler(this.frmRePrint_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdHistoryPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHistoryPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsWarranty)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.TextBox txtSymbol;
        private System.Windows.Forms.TextBox txtNewNumber;
        private System.Windows.Forms.TextBox txtMauSo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblNewNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtReason;
        private System.Windows.Forms.Label lblReason;
        private System.Windows.Forms.TextBox txtOldNumber;
        private System.Windows.Forms.Label lblOldNumber;
        private System.Windows.Forms.RadioButton radioPrintNew;
        private System.Windows.Forms.RadioButton radioPrintOld;
        private DevExpress.XtraGrid.GridControl grdHistoryPrint;
        private DevExpress.XtraGrid.Views.Grid.GridView grvHistoryPrint;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colIsHasWarranty;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsWarranty;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboMaThietLap;
    }
}