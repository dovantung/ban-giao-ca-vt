﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERP.Inventory.PLC.StoreChange;
using Library.AppCore.LoadControls;
using Library.AppCore;
using ERP.Inventory.PLC.StoreChange.WSStoreChange;
using System.Text.RegularExpressions;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmConfigPrintMaster : Form
    {
        private object[] objKeywords = null;
        private PLCStoreChange objPLCStoreChange = new PLCStoreChange();
        private ResultMessage objResultMessage = new ResultMessage();
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;

        public frmConfigPrintMaster()
        {
            InitializeComponent();
        }

        private void frmConfigPrintMaster_Load(object sender, EventArgs e)
        {

            if (!LoadData())
                return;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvData);
            FormState = FormStateType.LIST;
        }

        private bool LoadData()
        {
            objKeywords = new object[] { "@StoreID", SystemConfig.ConfigStoreID,
                                         "@UserName",SystemConfig.objSessionUser.UserName,
                                         "@STORECHANGECURVOUCHERID",string.Empty};

            DataTable dtbData = objPLCStoreChange.SearchDataConfigPrint(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            grdData.DataSource = dtbData;
            return true;
        }
        private void ClearValueControl()
        {
            txtMaThietLap.Text = string.Empty;
            txtInvoiceId.Text = string.Empty;
            txtInvoiceStart.Text = string.Empty;
            txtMauSo.Text = string.Empty;
            txtSymbol.Text = string.Empty;
            txtInvoiceEnd.Text = string.Empty;
            dtFromDate.EditValue = DateTime.Now;
            dtToDate.EditValue = null;

            txtMaThietLap.Enabled = true;
            txtMaThietLap.ReadOnly = false;

            txtInvoiceId.Enabled = true;
            txtInvoiceId.ReadOnly = false;
            txtInvoiceId.Visible = true;

            txtInvoiceStart.Enabled = true;
            txtInvoiceStart.ReadOnly = false;
            txtInvoiceStart.Visible = true;

            txtMauSo.Enabled = true;
            txtMauSo.ReadOnly = false;
            txtMauSo.Visible = true;

            txtSymbol.Enabled = true;
            txtSymbol.ReadOnly = false;
            txtSymbol.Visible = true;

            txtInvoiceEnd.Enabled = true;
            txtInvoiceEnd.ReadOnly = false;
            txtInvoiceEnd.Visible = true;

        }

        #region Form Status
        /// <summary>
        /// Các trạng thái của Form
        /// </summary>
        enum FormStateType
        {
            LIST,
            ADD,
            EDIT
        }

        FormStateType intFormState = FormStateType.LIST;
        FormStateType FormState
        {
            set
            {
                intFormState = value;
                if (intFormState == FormStateType.LIST)
                {
                    ////Kiểm tra quyền:
                    //Common.CommonFunction.EnableButtons(grdViewData, strPermission_Add, strPermission_Edit, strPermission_Delete, strPermission_ExportExcel, mnuItemAdd, mnuItemEdit, mnuItemDelete, mnuItemExport, btnAdd, btnEdit, btnDelete, btnExportExcel);
                    ////mnuItemAdd.Enabled= btnAdd.Enabled =  SystemConfig.objSessionUser.IsPermission(strPermission_Add);
                    ////mnuItemDelete.Enabled= btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
                    ////mnuItemEdit.Enabled= btnEdit.Enabled =  SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
                    ////mnuItemExport.Enabled= btnExportExcel.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel);
                    btnAdd.Enabled = btnEdit.Enabled = true;
                    btnUndo.Enabled = false;
                    btnUpdate.Enabled = false;
                    tabControl1.SelectedTab = tabPage1;
                    grvData.ClearColumnsFilter();
                }
                else if (intFormState == FormStateType.ADD || intFormState == FormStateType.EDIT)
                {
                    btnAdd.Enabled = false;
                    btnEdit.Enabled = false;
                    btnUndo.Enabled = true;
                    btnUpdate.Enabled = true;
                    tabControl1.SelectedTab = tabPage2;
                    if (intFormState == FormStateType.ADD)
                    {
                        txtMaThietLap.ReadOnly = false;
                        dtFromDate.EditValue = DateTime.Now;
                        txtInvoiceId.Visible = false;
                        txtInvoiceId.Enabled = false;
                        label8.Visible = false;
                        txtUserName.Text = SystemConfig.objSessionUser.UserName;
                        txtStored.Text = SystemConfig.ConfigStoreID.ToString() + " - " + GetStoreName(SystemConfig.ConfigStoreID);
                    }
                    else
                    {
                        txtMaThietLap.ReadOnly = true;
                        //   txtInvoiceId.ReadOnly = true;
                        txtInvoiceId.Visible = true;
                        txtInvoiceId.Enabled = true;
                        label8.Visible = true;
                    }
                }
            }
            get
            {
                return intFormState;
            }
        }

        #endregion

        private void tabPage1_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.ADD || FormState == FormStateType.EDIT)
            {
                tabControl1.SelectedTab = tabPage2;
            }
        }

        private void tabPage2_Enter(object sender, EventArgs e)
        {


            if (FormState == FormStateType.LIST)
            {
                tabControl1.SelectedTab = tabPage1;
            }
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            FormState = FormStateType.LIST;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled)
                return;
            ClearValueControl();
            FormState = FormStateType.ADD;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (!btnEdit.Enabled)
                return;
            if (grvData.FocusedRowHandle < 0)
                return;
            FormState = FormStateType.EDIT;
            if (!EditData())
                return;
        }
        int intStoreId = 0;
        private bool EditData()
        {
            try
            {
                DataRow row = grvData.GetDataRow(grvData.FocusedRowHandle);
                txtMaThietLap.Text = row["STORECHANGECURVOUCHERID"].ToString().Trim();
                txtInvoiceEnd.Text = row["INVOICEEND"].ToString().Trim();
                txtInvoiceId.Text = row["INVOICEID"].ToString().Trim();
                txtInvoiceStart.Text = row["INVOICEIDSTART"].ToString().Trim();
                txtMauSo.Text = row["DENOMINATOR"].ToString().Trim();
                txtSymbol.Text = row["INVOICESYMBOL"].ToString().Trim();
                if (!Convert.IsDBNull(row["FROMDATE"]))
                    dtFromDate.EditValue = Convert.ToDateTime(row["FROMDATE"]);
                else
                    dtFromDate.EditValue = null;
                if (!Convert.IsDBNull(row["TODATE"]))
                    dtToDate.EditValue = Convert.ToDateTime(row["TODATE"]);
                else
                    dtToDate.EditValue = null;
                if (Convert.ToDecimal(txtInvoiceId.Text) == Convert.ToDecimal(txtInvoiceStart.Text))
                {
                    //  txtMauSo.Enabled = false;
                    //    txtSymbol.Enabled = false;
                    //    txtInvoiceStart.Enabled = false;
                    txtInvoiceId.ReadOnly = true;
                }
                else if (Convert.ToDecimal(txtInvoiceId.Text) > Convert.ToDecimal(txtInvoiceStart.Text))
                {
                    txtMauSo.ReadOnly = true;
                    txtMaThietLap.ReadOnly = true;
                    txtSymbol.ReadOnly = true;
                    txtInvoiceStart.ReadOnly = true;
                    txtInvoiceId.ReadOnly = false;
                }
                else
                {

                }
                txtUserName.Text = row["USERNAME"].ToString().Trim();
                txtStored.Text = row["STOREID"].ToString().Trim() + " - " + GetStoreName(Convert.ToInt32(row["STOREID"]));
                intStoreId = Convert.ToInt32(row["STOREID"]);
                //if (!string.IsNullOrEmpty(txtMaThietLap.Text))
                //    txtMaThietLap.ReadOnly = true;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin cấu hình in kim");
                SystemErrorWS.Insert("Lỗi nạp thông tin chi nhánh", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            btnEdit_Click(null, null);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!btnUpdate.Enabled)
                return;
            if (!ValidateData())
                return;
            if (!UpdateData())
                return;
            LoadData();
            FormState = FormStateType.LIST;
        }
        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(txtMaThietLap.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập mã thiết lập!");
                txtMaThietLap.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtInvoiceId.Text) && FormState == FormStateType.EDIT)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập số hiện tại!");
                txtInvoiceId.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtInvoiceStart.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập số bắt đầu!");
                txtInvoiceStart.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtMauSo.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập mẫu số!");
                txtMauSo.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtSymbol.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập ký hiệu!");
                txtSymbol.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtInvoiceEnd.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập số kết thúc!");
                txtInvoiceEnd.Focus();
                return false;
            }
            if (dtFromDate.EditValue == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập ngày bắt đầu!");
                dtFromDate.Focus();
                return false;
            }
            if (FormState == FormStateType.EDIT && Convert.ToDecimal(txtInvoiceEnd.Text) < Convert.ToDecimal(txtInvoiceId.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Số kết thúc phải lớn hơn số hiện tại!");
                txtInvoiceEnd.Focus();
                return false;
            }
            if (Convert.ToDecimal(txtInvoiceEnd.Text) <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số kết thúc phải lớn hơn 0!");
                txtInvoiceEnd.Focus();
                return false;
            }

            if (Convert.ToDecimal(txtInvoiceStart.Text) <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số bắt đầu phải lớn hơn 0!");
                txtInvoiceStart.Focus();
                return false;
            }
            if (FormState == FormStateType.EDIT && Convert.ToDecimal(txtInvoiceId.Text) <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số hiện tại phải lớn hơn 0!");
                txtInvoiceId.Focus();
                return false;
            }
            if (dtToDate.EditValue != null)
            {
                if (Convert.ToDateTime(dtToDate.EditValue) < Convert.ToDateTime(dtFromDate.EditValue))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Từ ngày phải nhỏ hơn đến ngày!");
                    dtFromDate.Focus();
                    return false;
                }
            }
            if (Convert.ToDecimal(txtInvoiceStart.Text) > Convert.ToDecimal(txtInvoiceEnd.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Số bắt đầu phải nhỏ hơn số kết thúc!");
                txtInvoiceStart.Focus();
                return false;
            }
            return true;
        }

        private bool UpdateData()
        {
            string strMessage = string.Empty;
            try
            {
                StoredChange_CurrentVoucher objStoredChange_CurrentVoucher = new StoredChange_CurrentVoucher();

                objStoredChange_CurrentVoucher.Denominator = txtMauSo.Text.Trim();
                objStoredChange_CurrentVoucher.FROMDATE = Convert.ToDateTime(dtFromDate.EditValue);
                if (dtToDate.EditValue == null)
                    objStoredChange_CurrentVoucher.TODATE = null;
                else
                    objStoredChange_CurrentVoucher.TODATE = Convert.ToDateTime(dtToDate.EditValue);
                objStoredChange_CurrentVoucher.INVOICEEND = Convert.ToDecimal(txtInvoiceEnd.Text.Trim());

                objStoredChange_CurrentVoucher.InvoiceIdStart = Convert.ToDecimal(txtInvoiceStart.Text.Trim());
                objStoredChange_CurrentVoucher.InvoiceSymbol = txtSymbol.Text.Trim();
                objStoredChange_CurrentVoucher.STORECHANGECURVOUCHERID = txtMaThietLap.Text.Trim();

                objStoredChange_CurrentVoucher.UserName = SystemConfig.objSessionUser.UserName;
                if (FormState == FormStateType.ADD)
                {
                    objStoredChange_CurrentVoucher.InvoiceID = objStoredChange_CurrentVoucher.InvoiceIdStart;
                    objStoredChange_CurrentVoucher.StoreID = SystemConfig.ConfigStoreID;
                    objPLCStoreChange.InsertConfigPrint(objStoredChange_CurrentVoucher);
                    strMessage = "Thêm";
                }
                else if (FormState == FormStateType.EDIT)
                {
                    objStoredChange_CurrentVoucher.InvoiceID = Convert.ToDecimal(txtInvoiceId.Text.Trim());
                    objStoredChange_CurrentVoucher.StoreID = intStoreId;
                    objPLCStoreChange.UpdateConfigPrint(objStoredChange_CurrentVoucher);
                    //   
                    strMessage = "Cập nhật";
                }
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                MessageBox.Show(strMessage + " cấu hình in kim thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi " + strMessage.ToLower() + " cấu hình in kim");
                SystemErrorWS.Insert("Lỗi " + strMessage.ToLower() + " cấu hình in kim", objExce.ToString(), this.Name + " -> UpdateData", DUIInventory_Globals.ModuleName);
                return false;
            }

            return true;
        }

        private void txtInvoiceStart_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (/*txtCurrentNumber.Text.Length > 6 &&*/ !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
            if (txtInvoiceStart.Text.Length > 6 && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void txtInvoiceEnd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (/*txtCurrentNumber.Text.Length > 6 &&*/ !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
            if (txtInvoiceEnd.Text.Length > 6 && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void txtInvoiceId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (/*txtCurrentNumber.Text.Length > 6 &&*/ !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
            if (txtInvoiceId.Text.Length > 6 && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void txtMaThietLap_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == Convert.ToChar(Keys.Space)))
            {
                e.Handled = true;
            }
            if (txtMaThietLap.Text.Length >= 20 && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
            if (e.KeyChar >= 97 && e.KeyChar <= 122)
            {
                e.KeyChar = Convert.ToChar(e.KeyChar - 32);
            }
            if (e.KeyChar >= 128)
            {
                e.Handled = true;
            }
        }

        private void txtMaThietLap_KeyDown(object sender, KeyEventArgs e)
        {
            //Keys key = e.KeyCode;
            //if (key == Keys.Space)
            //{
            //    e.Handled = true;
            //}

            //base.OnKeyDown(e);
        }
        public string GetStoreName(int intStoreId)
        {
            DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            if (dtbStore != null)
            {
                DataRow row = dtbStore.AsEnumerable().Where(x => Convert.ToInt32(x.Field<object>("STOREID")) == intStoreId).FirstOrDefault();
                if (row != null)
                {
                    return row.Field<object>("STORENAME").ToString().Trim();
                }
            }
            return string.Empty;
        }
    }
}
