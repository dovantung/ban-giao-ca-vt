﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Text.RegularExpressions;
using Library.AppCore.Forms;
using System.Threading;
using System.Xml;
using ERP.Inventory.DUI.FIFO;

namespace ERP.Inventory.DUI.StoreChange
{
    /// <summary>
    /// Created by: Phạm Thiên Ân
    /// Desc: Nhập Imei
    /// </summary>
    public partial class frmStoreChange_GenImei : Form
    {
        public frmStoreChange_GenImei()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        #region Variable
        private List<frmStoreChange.ProductImei> objProductImeiList = null;
        private Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection();

        private int intOutputStoreID = 0;
        private decimal decOrderQuantity = 0;
        private bool isCloseForm = false;
        private string strProductID = string.Empty;
        private string strProductNameText = string.Empty;

        private int intOutputTypeID;
        private int intInputTypeID;
        private int intInStockStatusID = 0;
        public DataTable dtbSub { get; set; }
        //End
        #endregion

        #region Property
        public string OrderID { get; set; }
        public int InputTypeID
        {
            set { intInputTypeID = value; }
        }
        public int OutputTypeID
        {
            set { intOutputTypeID = value; }
        }

        /// <summary>
        /// Danh sách IMEI nhập
        /// </summary>
        public List<frmStoreChange.ProductImei> ProductImeiList
        {
            get { return objProductImeiList; }
            set { objProductImeiList = value; }
        }

        /// <summary>
        /// Tên sản phẩm
        /// </summary>
        public string ProductNameText
        {
            set { strProductNameText = value; }
        }
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string ProductID
        {
            set { strProductID = value; }
            get { return strProductID; }
        }
        /// <summary>
        /// Kho nhập
        /// </summary>
        public int OutputStoreID
        {
            set { intOutputStoreID = value; }
        }

        /// <summary>
        /// Số lượng đơn hàng
        /// </summary>
        public decimal OrderQuantity
        {
            set { decOrderQuantity = value; }
        }


        public int InStockStatusID
        {
            set { intInStockStatusID = value; }
        }
        #endregion


        #region Function

        /// <summary>
        /// Kiểm tra tồn tại IMEI chưa xác nhận hoặc xác nhận lỗi
        /// </summary>
        /// <returns></returns>
        private bool CheckValidate()
        {
            var imeilist = from o in objProductImeiList
                           where o.IsError == false
                           select o;

            if (imeilist.Count() == 0)
            {
                MessageBox.Show("Không có IMEI hợp lệ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            //else
            //if (decOrderQuantity > 0 && objProductImeiList.Count() > decOrderQuantity)
            //{
            //    MessageBox.Show("Số lượng IMEI nhập lớn hơn số lượng yêu cầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return false;
            //}
            return true;
        }
        /// <summary>
        /// Xác nhận IMEI
        /// </summary>
        private void ValidateData()
        {
            if (objProductImeiList.Count() > 0)
            {
                Thread objThread = new Thread(new ThreadStart(Excute));
                objThread.Start();
                foreach (var item in objProductImeiList)
                {
                    item.IsError = false;
                    item.IsErrorFiFo = 0;
                }

                #region Check IMEI
                // Check trùng Imei trên lưới
                Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
                for (int i = 0; i < objProductImeiList.Count(); i++)
                {
                    if (dicCheckIMEI.ContainsKey(objProductImeiList[i].Imei))
                    {
                        dicCheckIMEI[objProductImeiList[i].Imei].Add(i);
                    }
                    else
                    {
                        List<int> lstIndex = new List<int>();
                        lstIndex.Add(i);
                        dicCheckIMEI.Add(objProductImeiList[i].Imei, lstIndex);
                    }
                }

                foreach (string strIMEI in dicCheckIMEI.Keys)
                {
                    if (dicCheckIMEI[strIMEI].Count() > 1)
                    {
                        for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                        {
                            objProductImeiList[dicCheckIMEI[strIMEI][i]].IsError = true;
                            objProductImeiList[dicCheckIMEI[strIMEI][i]].Error = objProductImeiList[dicCheckIMEI[strIMEI][i]].Error.Replace("Trùng IMEI trên file Excel;", "") + "Trùng IMEI trên file Excel;";
                        }
                    }
                }

                // Loại những Imei bị trùng trên lưới
                objProductImeiList = objProductImeiList.Where(r => r.Error != null && !r.Error.Contains("Trùng IMEI trên file Excel")).ToList();
                var listIMEI = objProductImeiList.Select(x => x.Imei); // Danh sach String IMEI đã nhập

                // Check Imei trong DB
                DataTable dtData = new DataTable();
                dtData.Columns.Add("IMEI");
                foreach (var item in listIMEI)
                {
                    dtData.Rows.Add(item);
                }
                string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtData, "IMEI");

                DataTable dtbTotalImei = null;
                DataTable dtbTotalImeiNotInStock = null;
                DataSet dsData = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("CHECKIMEI_INSTOCK_NOTINSTOCK",
                new string[] { "v_Out1", "v_Out2" }, new object[] { "@StoreID", intOutputStoreID,
                    "@OUTPUTTYPEID", intOutputTypeID, "@INPUTTYPEID", intInputTypeID, "@IMEI", xmlIMEI });
                if (dsData != null && dsData.Tables.Count > 0)
                {
                    dtbTotalImei = dsData.Tables[0].Copy();
                }
                if (dsData != null && dsData.Tables.Count > 0)
                {
                    dtbTotalImeiNotInStock = dsData.Tables[1].Copy();
                }
                List<string> lstIMEI = new List<string>();
                lstIMEI = lstIMEI.Concat(dtbTotalImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList()).ToList();
                int IsCanInputFifo = 0;
                foreach (var item in objProductImeiList)
                {

                    DataRow[] lstdr = dtbTotalImei.Select("IMEI = '" + item.Imei.Trim() + "'");
                    if (lstdr.Any())
                    {
                        item.EndWarrantyDate = Convert.IsDBNull(lstdr[0]["ENDWARRANTYDATE"]) ? null : (DateTime?)Convert.ToDateTime(lstdr[0]["ENDWARRANTYDATE"]);
                        item.IsHasWarranty = Convert.ToBoolean(lstdr[0]["IsHasWarranty"]);
                        item.IsNew = Convert.ToBoolean(lstdr[0]["IsNew"]);
                        item.IsShowProduct = Convert.ToBoolean(lstdr[0]["IsShowProduct"]);
                        item.InVAT = Convert.ToDecimal(lstdr[0]["InVAT"]);
                        item.OutVAT = Convert.ToDecimal(lstdr[0]["OutVAT"]);
                        item.InputPrice = Convert.ToDecimal(lstdr[0]["InputPrice"]);
                        item.OutputPrice = Convert.ToDecimal(lstdr[0]["OutputPrice"]);
                        item.CostPrice = Convert.ToDecimal(lstdr[0]["CostPrice"]);
                        item.IsErrorFiFo = Convert.ToInt32(lstdr[0]["STATUSFIFOID"]);
                        if (item.IsErrorFiFo == 1)
                            item.Error = "IMEI " + item.Imei.ToString().Trim() + " vi phạm xuất fifo";

                        if (lstdr[0]["INSTOCKSTATUSID"].ToString().Trim() != intInStockStatusID.ToString())
                        {
                            item.IsError = true;
                            item.Error = "IMEI này không thuộc trạng thái sản phẩm đã chọn!";
                        }
                        else if (item.ProductID != lstdr[0]["PRODUCTID"].ToString())
                        {
                            item.IsError = true;
                            item.Error = "IMEI này không có trong sản phẩm!";
                        }
                    }
                    else
                    {
                        item.IsError = true;
                        item.Error = "IMEI này không tồn tại trong kho";
                    }
                }

                var LstIMEIInProductOrder = from o in objProductImeiList
                                            join i in dtbTotalImei.Select("ISORDER = 1 and ORDERID <> '" + OrderID + "'")
                                            on o.ProductID.Trim() equals i["PRODUCTID"].ToString().Trim()
                                            select i;
                if (LstIMEIInProductOrder.Any())
                {
                    var LstIMEIInProduct = from o in objProductImeiList
                                           join i in LstIMEIInProductOrder
                                           on o.Imei.Trim() equals i["IMEI"].ToString().Trim()
                                           select o;
                    if (LstIMEIInProduct.Any())
                    {
                        LstIMEIInProduct.All(c => { c.IsError = true; c.Error = "IMEI đã được đặt hàng"; return true; });
                    }
                }
                var LstIMEIInProductStore = from o in objProductImeiList
                                            join i in dtbTotalImei.Select("STOREID <> " + intOutputStoreID)
                                            on o.Imei.Trim() equals i["IMEI"].ToString().Trim()
                                            select o;
                if (LstIMEIInProductStore.Any())
                {
                    LstIMEIInProductStore.All(c => { c.IsError = true; c.Error = "IMEI không tồn kho"; return true; });
                }
                var LstIMEIInProductRealinstock = from o in objProductImeiList
                                                  join i in dtbTotalImei.Select("ISCHECKREALINPUT <> 1")
                                                  on o.Imei.Trim() equals i["IMEI"].ToString().Trim()
                                                  select o;
                if (LstIMEIInProductRealinstock.Any())
                {
                    LstIMEIInProductRealinstock.All(c => { c.IsError = true; c.Error = "IMEI chưa thực nhập"; return true; });
                }
                var LstIMEINotInStock = from o in objProductImeiList
                                        join i in dtbTotalImeiNotInStock.AsEnumerable()
                                        on o.Imei.Trim() equals i["IMEI"].ToString().Trim()
                                        select o;
                if (LstIMEINotInStock.Any())
                {
                    LstIMEINotInStock.All(c => { c.IsError = true; c.Error = "IMEI đang cho mượn"; return true; });
                }



                //kiểm tra imei đã tồn tại trong form trước
                if (dtbSub != null && dtbSub.Rows.Count > 0)
                {
                    var lstExistsSub = from r in objProductImeiList
                                       join x in dtbSub.AsEnumerable()
                                           on r.Imei.Trim() equals x["IMEI"].ToString().Trim()
                                       select r;
                    lstExistsSub.All(c => { c.IsError = true; c.Error = "Số IMEI này đã tồn tại trên phiếu"; return true; });
                }
                List<frmStoreChange.ProductImei> LstIMEINotError = objProductImeiList.Where(x => x.IsError == false).ToList();
                if (decOrderQuantity > 0)
                {
                    if (decOrderQuantity < LstIMEINotError.Count())
                    {
                        for (int i = int.Parse(decOrderQuantity.ToString()); i < LstIMEINotError.Count(); i++)
                        {
                            LstIMEINotError[i].IsError = true;
                            LstIMEINotError[i].Error = "Số IMEI này vượt quá số lượng đặt!";
                        }
                    }
                }
                #endregion
                frmWaitDialog.Close();
                Thread.Sleep(0);
                objThread.Abort();


            }
            GetTotal();
            var varReulst = from o in objProductImeiList
                            where o.IsError == false && o.IsErrorFiFo == 1
                            select o;
            if (varReulst.Any())
            {
                DataTable dtbImeiPara = new DataTable();
                dtbImeiPara.Columns.Add("IMEI");
                //varReulst.All(c => { c["ISSELECT"] = false; return true; });
                var varIMEIOK = from o in objProductImeiList
                                where o.IsError == false && o.IsErrorFiFo == 0
                                select o;
                if (varIMEIOK.Any())
                {
                    foreach (var item in varIMEIOK)
                    {
                        string strImei = (item.Imei).ToString().Trim();
                        if (!string.IsNullOrEmpty(strImei))
                            dtbImeiPara.Rows.Add(strImei);
                    }
                }
                string strMessenger = "Có tồn tại dữ liệu vi phạm xuất fifo bạn có muốn tiếp tục?";
                if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) > 0)
                {
                    if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) == Convert.ToInt32(objProductImeiList.Count))
                    {
                        MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + objProductImeiList.Count.ToString("#,##0"),
                                 "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                        strMessenger = "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + objProductImeiList.Count.ToString("#,##0") + "\nCó tồn tại dữ liệu vi phạm xuất fifo bạn có muốn tiếp tục?";
                }
                DialogResult drResult = ClsFIFO.ShowMessengerFIFOList_ByIMEI(dtbImeiPara, strMessenger
                        , ProductID, intOutputStoreID, -1);
                if (drResult != System.Windows.Forms.DialogResult.OK)
                {
                    foreach (var item in varReulst)
                        item.IsError = true;
                }
                else
                    btnClose_Click(null, null);
                grdIMEI.RefreshDataSource();
                return;
            }

            if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) > 0)
            {
                if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) == Convert.ToInt32(objProductImeiList.Count))
                {
                    MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + objProductImeiList.Count.ToString("#,##0"),
                   "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + objProductImeiList.Count.ToString("#,##0") + "\n Bạn có muốn tiếp tục không?",
                   "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        btnClose_Click(null, null);
                    }
                }
            }
            else
            {
                btnClose_Click(null, null);
            }
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang kiểm tra IMEI");
        }
        /// <summary>
        /// Xóa IMEI không hợp lệ hoặc chưa xác nhận
        /// </summary>
        private void RemoveInValidData()
        {
            for (int i = objProductImeiList.Count - 1; i >= 0; i--)
            {
                frmStoreChange.ProductImei objInputVoucherDetailIMEI = objProductImeiList[i];
                if (objInputVoucherDetailIMEI.IsError)
                    ProductImeiList.RemoveAt(i);
            }
            grdIMEI.RefreshDataSource();
        }

        #endregion
        private void btnValidate_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateData();
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi nhập dữ liệu khi genimei", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi nhập dữ liệu imei", objExce.ToString(), "ValidateData()", DUIInventory_Globals.ModuleName);
            }
            frmWaitDialog.Close();
            Thread.Sleep(0);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CheckValidate())
            {
                RemoveInValidData();
                isCloseForm = true;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void frmStoreChange_GenImei_Load(object sender, EventArgs e)
        {
            objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, grvIMEI);
            objSelection.CheckMarkColumn.Width = 50;
            grpProduct.Text = strProductID + " - " + strProductNameText;
            if (ProductImeiList == null)
                ProductImeiList = new List<frmStoreChange.ProductImei>();
            grdIMEI.DataSource = ProductImeiList;
            txtBarcode.Focus();
            btnValidate.Enabled = false;
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strIMEI = txtBarcode.Text;
            if (!string.IsNullOrEmpty(strIMEI) && e.KeyChar.Equals((char)Keys.Enter))
            {
                txtBarcode.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtBarcode.Text.Trim().Replace("'", string.Empty).ToUpper());
                btnAutoGenImei_Click(null, null);
            }
        }


        /// <summary>
        /// Thêm IMEI vào lưới
        /// </summary>
        /// <param name="strIMEI"></param>
        private void InsertIMEI(string strIMEI)
        {
            if (decOrderQuantity > 0)
            {
                var ErrorQuantity = from o in objProductImeiList
                                    where o.IsError == true
                                    select o;
                decimal decErrorQuantity = ErrorQuantity.Count();
                if (Convert.ToDecimal(objProductImeiList.Count) + 1 - decErrorQuantity > decOrderQuantity)
                {
                    MessageBox.Show(this, "Số lượng IMEI không được vượt quá số lượng đặt là: " + decOrderQuantity.ToString("#,##0"), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (dtbSub != null && dtbSub.Rows.Count > 0)
                {
                    DataRow[] row = dtbSub.Select("IMEI = '" + strIMEI.Trim() + "'");
                    if (row.Length > 0)
                    {
                        MessageBox.Show(this, "IMEI này đã tồn tại trong phiếu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }

            if (dtbSub != null && dtbSub.Rows.Count > 0)
            {
                DataRow[] row = dtbSub.Select("IMEI = '" + strIMEI.Trim() + "'");
                if (row.Length > 0)
                {
                    MessageBox.Show(this, "IMEI này đã tồn tại trong phiếu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            if (objProductImeiList != null && objProductImeiList.Count > 0)
            {
                var EMEIExist = from o in objProductImeiList
                                where o.Imei == strIMEI
                                select o;
                if (EMEIExist.Count() > 0)
                {
                    MessageBox.Show(this, "IMEI đã tồn tại trên lưới", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            frmStoreChange.ProductImei obj = new frmStoreChange.ProductImei();
            obj.ProductName = strProductNameText;
            obj.ProductID = strProductID;
            obj.Imei = strIMEI;
            obj.IsHasWarranty = true;
            obj.Error = "";
            obj.IsError = false;
            objProductImeiList.Add(obj);
            GetTotal();
        }


        private void frmStoreChange_GenImeiKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Xóa IMEI
        /// </summary>
        private void DeleteIMEI()
        {
            if (objSelection.SelectedCount == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn IMEI cần xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            for (int i = 0; i < objSelection.SelectedCount; i++)
            {
                frmStoreChange.ProductImei objInputVoucherDetailIMEI = (frmStoreChange.ProductImei)objSelection.GetSelectedRow(i);
                ProductImeiList.Remove(objInputVoucherDetailIMEI);
            }

            GetTotal();
            btnValidate.Enabled = grvIMEI.DataRowCount > 0;
        }

        private void GetTotal()
        {
            grdIMEI.DataSource = ProductImeiList;
            grdIMEI.RefreshDataSource();
            txtTotalIMEI.Text = objProductImeiList.Count().ToString();
            var vTotalEMEIVaild = from o in objProductImeiList
                                  where o.IsError == false
                                  select o;
            txtTotalIMEIValid.Text = vTotalEMEIVaild.Count().ToString();

            var vTotalEMEIInVaild = from o in objProductImeiList
                                    where o.IsError == true
                                    select o;
            txtTotalIMEIInValid.Text = vTotalEMEIInVaild.Count().ToString();
            objSelection.ClearSelection();
            btnValidate.Enabled = grvIMEI.DataRowCount > 0;
        }
        private void mnuItemDeleteIMEI_Click(object sender, EventArgs e)
        {
            if (!mnuItemDeleteIMEI.Enabled)
                return;
            DeleteIMEI();
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName != string.Empty)
            {
                DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                bool bolIsSelect = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["IsError"]));
                if (bolIsSelect)
                {
                    e.Appearance.BackColor = Color.Pink;
                }
            }
        }

        private void frmStoreChange_GenImeiFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isCloseForm && objProductImeiList.Count() > 0)
            {
                if (MessageBox.Show("Bạn muốn đóng form nhập IMEI?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    e.Cancel = true;
            }
        }

        /// <summary>
        /// Tạo dãy Imei 
        /// </summary>
        /// <param name="intFisrtIMEI"></param>
        /// <param name="intLastIMEI"></param>
        private void insertListIMEI(decimal intFisrtIMEI, decimal intLastIMEI, string IMEIcode, int lenIMEInumber)
        {
            string strIMEI_i = string.Empty;
            int intCountFinish = 0;

            for (decimal i = intFisrtIMEI; i <= intLastIMEI; i++)
            {
                strIMEI_i = i.ToString().PadLeft(lenIMEInumber, '0');
                if (objProductImeiList.Count() > 0)
                {
                    var objProductImei = objProductImeiList.Find(x => x.Imei == strIMEI_i);
                    if (objProductImei == null)
                    {
                        frmStoreChange.ProductImei obj = new frmStoreChange.ProductImei();
                        obj.ProductName = strProductNameText;
                        obj.ProductID = strProductID;
                        obj.Imei = IMEIcode + strIMEI_i;
                        obj.IsHasWarranty = false;
                        obj.Error = "";
                        obj.IsError = false;
                        objProductImeiList.Add(obj);
                    }
                }
                else
                {
                    frmStoreChange.ProductImei obj = new frmStoreChange.ProductImei();
                    obj.ProductName = strProductNameText;
                    obj.ProductID = strProductID;
                    obj.Imei = IMEIcode + strIMEI_i;
                    obj.IsHasWarranty = false;
                    obj.Error = "";
                    obj.IsError = false;
                    objProductImeiList.Add(obj);
                }
                intCountFinish += 1;
            }

            if (dtbSub != null && dtbSub.Rows.Count > 0)
            {
                var lstExistsSub = from r in objProductImeiList
                                   join x in dtbSub.AsEnumerable()
                                       on r.Imei equals x["IMEI"].ToString()
                                   select r;
                lstExistsSub.All(c => { c.IsError = true; c.Error = "Số IMEI này đã tồn tại trong phiếu"; return true; });
            }

            GetTotal();
        }

        private void mnuPopUp_Opening(object sender, CancelEventArgs e)
        {
            mnuItemDeleteIMEI.Enabled = grvIMEI.DataRowCount > 0;
        }

        private void btnAutoGenImei_Click(object sender, EventArgs e)
        {
            string strFirstIMEI = txtBarcode.Text.Trim();
            string strlastIMEI = txtLastIMEI.Text.Trim();
            string strIMEICode = string.Empty;
            if (string.IsNullOrEmpty(strFirstIMEI))
            {
                MessageBox.Show(this, "Vui lòng nhập số IMEI đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBarcode.Focus();
            }
            else if (string.IsNullOrEmpty(strlastIMEI))
            {
                MessageBox.Show(this, "Vui lòng nhập số IMEI cuối!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtLastIMEI.Focus();
            }
            else
            {
                decimal decFisrtIMEI = 0;
                decimal decLastIMEI = 0;


                if (decimal.TryParse(strFirstIMEI, out decFisrtIMEI) && decimal.TryParse(strlastIMEI, out decLastIMEI))
                {
                    //if (decOrderQuantity > 0 && ((decLastIMEI - decFisrtIMEI) + Convert.ToDecimal(objProductImeiList.Count) + 1) > (decOrderQuantity))
                    //{
                    //    MessageBox.Show(this, "Số lượng IMEI không được vượt quá số lượng đặt là: " + decOrderQuantity.ToString("#,##0"), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //    return;
                    //}
                    if (txtBarcode.Text.Trim() == txtLastIMEI.Text.Trim())
                    {
                        InsertIMEI(txtBarcode.Text.Trim());
                        return;
                    }
                    else if (decLastIMEI - decFisrtIMEI + 1 > 10000) // Giới hạn 10.000 dòng /lần
                    {
                        MessageBox.Show(this, "Chỉ được phép tạo tự động 10.000 IMEI/lần!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else if (decLastIMEI - decFisrtIMEI < 0)
                    {
                        MessageBox.Show(this, "Số IMEI cuối phải lớn hơn số IMEI đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    insertListIMEI(decFisrtIMEI, decLastIMEI, strIMEICode, strFirstIMEI.Length);
                }
                else
                {
                    MessageBox.Show(this, "Số IMEI chỉ bao gồm kí tự số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            btnValidate.Enabled = objProductImeiList.Count > 0;
        }

        private void txtLastIMEI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals((char)Keys.Enter))
            {
                btnAutoGenImei_Click(null, null);
            }
        }

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}



