﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmPopupWarning : Form
    {
        private string strStoreChangeOrderID = string.Empty;
        public string StoreChangeOrderID
        {
            get { return strStoreChangeOrderID; }
            set { strStoreChangeOrderID = value; }
        }
        public DataTable dtbDetail { get; set; }
        private bool bolIsOk = false;

        public bool IsOk
        {
            get { return bolIsOk; }
            set { bolIsOk = value; }
        }

        public frmPopupWarning()
        {
            InitializeComponent();
        }

        private void frmPopupWarning_Load(object sender, EventArgs e)
        {

        }

        private void lblViewDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmPopupWarningDetail objfrmPopupWarningDetail = new frmPopupWarningDetail();
            objfrmPopupWarningDetail.strStoreChangeOrderID = StoreChangeOrderID;
            objfrmPopupWarningDetail.dtbDetail = dtbDetail;
            objfrmPopupWarningDetail.ShowDialog();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            IsOk = false;
            this.Close();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            IsOk = true;
            this.Close();
        }
    }
}
