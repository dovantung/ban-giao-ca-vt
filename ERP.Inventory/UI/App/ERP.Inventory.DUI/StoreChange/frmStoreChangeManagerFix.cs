﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.Inventory.PLC.StoreChange.WSStoreChange;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmStoreChangeManagerFix : Form
    {

        private string strPermission_MultiPrint = "PromotionProgram_Copy";
        private string strPermission_IsTransferedUpdate = "PM_StoreChange_IsTransferedUpdate";//Quản lý phiếu chuyển kho – Cập nhật trạng thái chuyển kho
        private string strPermission_IsReceivedTransfer = "PM_STORECHANGE_RECEIVEDTRANSFER";//Quản lý phiếu chuyển kho – Cập nhận hàng để vận chuyển
        private string strPermission_IsReceive = "PM_StoreChange_CheckIsReceive"; //Quản lý phiếu chuyển kho - Quyền nhập kho
        private int intStoreChange3_A4 = -1;
        private int intStoreChange3_A5 = -1;
        private int intStoreChange3_A6 = -1;
        private int intStoreChangeReportID = -1;
        private int intInternalOutput_TransferID = -1;
        private int intStoreChangeCommandID = -1;
        private bool bolIsStoreChangeDate = true;
        private int intCheckIsReceive = Library.AppCore.AppConfig.GetIntConfigValue("PM_STORECHANGE_CHECKISRECEIVE");
        private int intPrintStoreChangeHandovers = -1;
        private int intPrintStoreChangeInputVoucher = -1;
        private const string STORECHANGEDETAILREPORT_VIEW = "STORECHANGEDETAILREPORT_VIEW";
        private Library.AppCore.CustomControls.GridControl.BandGridCheckMarksSelection selection;

        private Hashtable htb = null;
        private int intOnlyCertifyFinger = 0;
        private ResultMessage objResultMessage = new ResultMessage();
        private PLC.StoreChange.PLCStoreChange objPLCStoreChange = new PLC.StoreChange.PLCStoreChange();
        private ERP.Inventory.PLC.StoreChange.WSStoreChange.StoreChange objStoreChange = new PLC.StoreChange.WSStoreChange.StoreChange();
        private ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType = null;
        private ERP.MasterData.PLC.MD.WSStore.Store objFromStore = null;
        private ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
        // private StoreChangeParam objStoreChangeParam = new StoreChangeParam();
        public frmStoreChangeManagerFix()
        {
            InitializeComponent();
            InitControlGrid();

        }

        private void InitControlGrid()
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(bagrvReport);

        }

        private void frmStoreChangeManagerFix_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            //Lấy tham số form
            htb = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString());

            //if (!Convert.IsDBNull(htb["PrintStoreChangeHandovers"]))
            //    PrintStoreChangeHandovers = Convert.ToInt32(htb["PrintStoreChangeHandovers"]);
            try
            {
                var configPrintStoreChangeHandovers = Library.AppCore.AppConfig.GetConfigValue("RPT_STORECHANGE_HANDOVER");
                if (configPrintStoreChangeHandovers != null)
                {
                    string strPrintStoreChangeHandovers = configPrintStoreChangeHandovers.ToString().Trim();
                    if (!string.IsNullOrEmpty(strPrintStoreChangeHandovers))
                    {
                        intPrintStoreChangeHandovers = Convert.ToInt32(strPrintStoreChangeHandovers);
                        //mnuItemPrintStoreChangeHandOvers.Visible = intPrintStoreChangeHandovers > 0;
                    }
                }
                else
                {
                    mnuItemPrintStoreChangeHandOvers.Visible = false;
                }
            }
            catch
            {
                MessageBox.Show(this, "Lỗi khai báo cấu hình ứng dụng biên bản bàn giao", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }


            try
            {
                var configPrintStoreChangeInputVoucher = Library.AppCore.AppConfig.GetConfigValue("PM_RPT_STORECHANGE_INPUTVOUCHER");
                if (configPrintStoreChangeInputVoucher != null)
                {
                    string strPrintStoreChangeInputVoucher = configPrintStoreChangeInputVoucher.ToString().Trim();
                    if (!string.IsNullOrEmpty(strPrintStoreChangeInputVoucher))
                    {
                        intPrintStoreChangeInputVoucher = Convert.ToInt32(strPrintStoreChangeInputVoucher);
                    }
                }
            }
            catch
            {
                MessageBox.Show(this, "Lỗi khai báo cấu hình ứng dụng phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            //try
            //{

            //    string strStoreChangeReportID = AppConfig.GetStringConfigValue("PM_STORECHANGE_REPORTID");
            //    if (strStoreChangeReportID.Length > 0)
            //    {
            //        Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(strStoreChangeReportID);
            //        if (!Convert.IsDBNull(hstbParam["StoreChange3_A4"]))
            //        {
            //            intStoreChange3_A4 = Convert.ToInt32(hstbParam["StoreChange3_A4"]);
            //            mnuItemPrintStoreAddressA4.Visible = intStoreChange3_A4 > 0;
            //        }
            //        if (!Convert.IsDBNull(hstbParam["StoreChange3_A5"]))
            //        {
            //            intStoreChange3_A5 = Convert.ToInt32(hstbParam["StoreChange3_A5"]);
            //            mnuItemPrintStoreAddressA5.Visible = intStoreChange3_A5 > 0;
            //        }
            //        if (!Convert.IsDBNull(hstbParam["StoreChange3_A6"]))
            //        {
            //            intStoreChange3_A6 = Convert.ToInt32(hstbParam["StoreChange3_A6"]);
            //            mnuItemPrintStoreAddressA6.Visible = intStoreChange3_A6 > 0;
            //        }
            //        if (!Convert.IsDBNull(hstbParam["StoreChangeReportID"]))
            //        {
            //            intStoreChangeReportID = Convert.ToInt32(hstbParam["StoreChangeReportID"]);
            //            mnuItemPrintReport.Visible = intStoreChangeReportID > 0;
            //        }
            //        if (!Convert.IsDBNull(hstbParam["storeChangeCommandID"]))
            //        {
            //            intStoreChangeCommandID = Convert.ToInt32(hstbParam["storeChangeCommandID"]);
            //            mnuItemPrintStoreChangeCommand.Visible = intStoreChangeCommandID > 0;
            //        }
            //        if (!Convert.IsDBNull(hstbParam["internalOutput_TransferID"]))
            //        {
            //            intInternalOutput_TransferID = Convert.ToInt32(hstbParam["internalOutput_TransferID"]);
            //            mnuItemPrintInternalOutput_Transfer.Visible = intInternalOutput_TransferID > 0;
            //        }
            //        if (!Convert.IsDBNull(htb["ISONLYCERTIFYFINGER"]))
            //            intOnlyCertifyFinger = Convert.ToInt32(htb["ISONLYCERTIFYFINGER"]);
            //    }
            //}

            //catch
            //{
            //    MessageBox.Show(this, "Lỗi đọc tham số loại báo cáo chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
            // CreateColumnGrid();
            // FormatGrid();
        }

        /// <summary>
        /// Nạp giá trị cho combobox
        /// </summary>
        private void LoadComboBox()
        {

            try
            {
                //Trạng thái
                DataTable dtbStatus = new DataTable();
                dtbStatus.Columns.Add("StatusID", typeof(int));
                dtbStatus.Columns.Add("StatusName", typeof(string));
                DataRow row1 = dtbStatus.NewRow();
                row1["StatusID"] = 0;
                row1["StatusName"] = "Chưa giao hàng";
                dtbStatus.Rows.Add(row1);
                DataRow row2 = dtbStatus.NewRow();
                row2["StatusID"] = 1;
                row2["StatusName"] = "Đã giao hàng - Chưa vận chuyển";
                dtbStatus.Rows.Add(row2);
                DataRow row3 = dtbStatus.NewRow();
                row3["StatusID"] = 2;
                row3["StatusName"] = "Đã vận chuyển - Chưa ký nhận nhập";
                dtbStatus.Rows.Add(row3);
                DataRow row4 = dtbStatus.NewRow();
                row4["StatusID"] = 3;
                row4["StatusName"] = "Đã ký nhận nhập - Chưa xác nhận thực nhập";
                dtbStatus.Rows.Add(row4);
                DataRow row5 = dtbStatus.NewRow();
                row5["StatusID"] = 4;
                row5["StatusName"] = "Đã xác nhận thực nhập";
                dtbStatus.Rows.Add(row5);

                cboStatusList.InitControl(true, dtbStatus, "StatusID", "StatusName", "--Chọn trạng thái--");

                // Kho nhập - Kho xuất
                DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore();
                cboFromStoreSearch.InitControl(true);
                cboToStoreSearch.InitControl(true);
                //// Phương tiện vận chuyển
                //DataTable dtbTransportType = Library.AppCore.DataSource.GetDataSource.GetTransportType();
                //Library.AppCore.LoadControls.CheckedComboBoxEditObject.LoadDataFromDataTable(dtbTransportType.Copy(), cboTransportType);

                // Hình thức chuyển
                Library.AppCore.LoadControls.SetDataSource.SetStoreChangeType(this, cboStoreChangeType);

                cboMainGroup.InitControl(true, true);

                //Load giá trị mặc định cho combobox 
                cboStoreChangeType.SelectedIndex = 0;
                cboSearchBy.SelectedIndex = 0;
                // dtpFromStoreChangeDate.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day);
                dtpFromDate.Value = DateTime.Today;
                dtpToDate.Value = DateTime.Today;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Lỗi nạp combobox", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }
        /// <summary>
        /// Định dạng lưới
        /// </summary>

        private void FormatFlex()
        {
            if (flexStoreChange.DataSource == null) return;
            flexStoreChange.Rows.Fixed = 2;

            #region Caption
            for (int i = 0; i < flexStoreChange.Cols.Count; i++)
            {
                flexStoreChange.Cols[i].AllowMerging = true;
                flexStoreChange.Cols[i].Visible = false;
            }

            flexStoreChange[0, "StoreChangeID"] = "Mã phiếu chuyển";
            flexStoreChange[1, "StoreChangeID"] = "Mã phiếu chuyển";
            flexStoreChange.Cols["StoreChangeID"].Visible = true;

            flexStoreChange[0, "DateCreateStoreChange"] = "Ngày chuyển";
            flexStoreChange[1, "DateCreateStoreChange"] = "Ngày chuyển";
            flexStoreChange.Cols["DateCreateStoreChange"].Visible = true;

            flexStoreChange[0, "CaskCode"] = "Mã số thùng";
            flexStoreChange[1, "CaskCode"] = "Mã số thùng";
            flexStoreChange.Cols["CaskCode"].Visible = true;

            flexStoreChange[0, "FromStoreName"] = "Kho xuất";
            flexStoreChange[1, "FromStoreName"] = "Kho xuất";
            flexStoreChange.Cols["FromStoreName"].Visible = true;


            flexStoreChange[0, "ToStoreName"] = "Kho nhập";
            flexStoreChange[1, "ToStoreName"] = "Kho nhập";
            flexStoreChange.Cols["ToStoreName"].Visible = true;

            flexStoreChange[0, "IsStoreChanged"] = "Thông tin xuất kho";
            flexStoreChange[1, "IsStoreChanged"] = "Đã xuất";
            flexStoreChange.Cols["IsStoreChanged"].Visible = true;

            flexStoreChange[0, "StorechangeDate"] = "Thông tin xuất kho";
            flexStoreChange[1, "StorechangeDate"] = "Thời gian xuất";
            flexStoreChange.Cols["StorechangeDate"].Visible = true;

            flexStoreChange[0, "TransferedDate"] = "Thông tin giao hàng";
            flexStoreChange[1, "TransferedDate"] = "Thời gian giao";
            flexStoreChange.Cols["TransferedDate"].Visible = true;
            //flexStoreChange[0, "StoreChangeDate"] = "Ngày chuyển"; //Trước đó Ngày chuyển StoreChangeDate
            //flexStoreChange[1, "StoreChangeDate"] = "Ngày chuyển";//Trước đó Ngày chuyển StoreChangeDate
            flexStoreChange[0, "DateReceive"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "DateReceive"] = "Thời gian nhập";
            flexStoreChange.Cols["DateReceive"].Visible = true;

            flexStoreChange[0, "Content"] = "Nội dung";
            flexStoreChange[1, "Content"] = "Nội dung";
            flexStoreChange.Cols["Content"].Visible = true;

            //flexStoreChange[0, "DaysOfStoreChange"] = "TG đi đường";//TG đi đường trước đó
            //flexStoreChange[1, "DaysOfStoreChange"] = "TG đi đường";//TG đi đường trước đó
            flexStoreChange[0, "TransferTime"] = "TG đi đường";
            flexStoreChange[1, "TransferTime"] = "TG đi đường";
            flexStoreChange.Cols["TransferTime"].Visible = true;

            flexStoreChange[0, "InvoiceID"] = "Số hóa đơn";
            flexStoreChange[1, "InvoiceID"] = "Số hóa đơn";
            flexStoreChange.Cols["InvoiceID"].Visible = true;

            flexStoreChange[0, "TotalPacking"] = "Số thùng";
            flexStoreChange[1, "TotalPacking"] = "Số thùng";
            flexStoreChange.Cols["TotalPacking"].Visible = true;

            flexStoreChange[0, "StoreChangeTypeName"] = "Loại chuyển kho";
            flexStoreChange[1, "StoreChangeTypeName"] = "Loại chuyển kho";
            flexStoreChange.Cols["StoreChangeTypeName"].Visible = true;

            flexStoreChange[0, "TransportTypeName"] = "Phương tiện vận chuyển";
            flexStoreChange[1, "TransportTypeName"] = "Phương tiện vận chuyển";
            flexStoreChange.Cols["TransportTypeName"].Visible = true;

            flexStoreChange[0, "IsNew"] = "Mới";
            flexStoreChange[1, "IsNew"] = "Mới";
            flexStoreChange.Cols["IsNew"].Visible = true;

            flexStoreChange[0, "IsTransfered"] = "Thông tin giao hàng";
            flexStoreChange[1, "IsTransfered"] = "Đã giao";
            flexStoreChange.Cols["IsTransfered"].Visible = true;

            flexStoreChange[0, "IsRequireVoucher"] = "Yêu cầu chứng từ";
            flexStoreChange[1, "IsRequireVoucher"] = "Yêu cầu chứng từ";
            flexStoreChange.Cols["IsRequireVoucher"].Visible = true;

            flexStoreChange[0, "TransportVoucherID"] = "Chứng từ vận chuyển";
            flexStoreChange[1, "TransportVoucherID"] = "Chứng từ vận chuyển";
            flexStoreChange.Cols["TransportVoucherID"].Visible = true;

            flexStoreChange[0, "IsReceive"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "IsReceive"] = "Đã nhập";
            flexStoreChange.Cols["IsReceive"].Visible = true;

            flexStoreChange[0, "ReceiveNote"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "ReceiveNote"] = "Ghi chú nhập";
            flexStoreChange.Cols["ReceiveNote"].Visible = true;

            flexStoreChange[0, "TotalWeight"] = "Tổng trọng lượng (kg)";
            flexStoreChange[1, "TotalWeight"] = "Tổng trọng lượng (kg)";
            flexStoreChange.Cols["TotalWeight"].Visible = true;

            flexStoreChange[0, "TotalSize"] = "Tổng thể tích (m³)";
            flexStoreChange[1, "TotalSize"] = "Tổng thể tích (m³)";
            flexStoreChange.Cols["TotalSize"].Visible = true;

            flexStoreChange[0, "TotalShippingCost"] = "Chi phí vận chuyển tạm tính";
            flexStoreChange[1, "TotalShippingCost"] = "Chi phí vận chuyển tạm tính";
            flexStoreChange.Cols["TotalShippingCost"].Visible = true;

            flexStoreChange[0, "StoreChangeOrderID"] = "Mã yêu cầu";
            flexStoreChange[1, "StoreChangeOrderID"] = "Mã yêu cầu";
            flexStoreChange.Cols["StoreChangeOrderID"].Visible = true;

            flexStoreChange[0, "InputVoucherID"] = "Mã phiếu nhập";
            flexStoreChange[1, "InputVoucherID"] = "Mã phiếu nhập";
            flexStoreChange.Cols["InputVoucherID"].Visible = true;

            flexStoreChange[0, "OutputVoucherID"] = "Mã phiếu xuất";
            flexStoreChange[1, "OutputVoucherID"] = "Mã phiếu xuất";
            flexStoreChange.Cols["OutputVoucherID"].Visible = true;

            flexStoreChange[0, "StoreChangeUser"] = "Thông tin xuất kho";
            flexStoreChange[1, "StoreChangeUser"] = "Nhân viên xuất";
            flexStoreChange.Cols["StoreChangeUser"].Visible = true;

            flexStoreChange[0, "StoreChangeUserFullName"] = "Thông tin xuất kho";
            flexStoreChange[1, "StoreChangeUserFullName"] = "Nhân viên xuất";
            flexStoreChange.Cols["StoreChangeUserFullName"].Visible = true;

            flexStoreChange[0, "ReceiveUser"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "ReceiveUser"] = "Nhân viên nhập";
            flexStoreChange.Cols["ReceiveUser"].Visible = true;

            flexStoreChange[0, "ReceiveUserFullName"] = "Thông tin xác nhận thực nhập";
            flexStoreChange[1, "ReceiveUserFullName"] = "Nhân viên nhập";
            flexStoreChange.Cols["ReceiveUserFullName"].Visible = true;

            flexStoreChange[0, "ReceivedTransferDate"] = "Thông tin nhận hàng để vận chuyển";
            flexStoreChange[1, "ReceivedTransferDate"] = "Thời gian nhận";
            flexStoreChange.Cols["ReceivedTransferDate"].Visible = true;

            flexStoreChange[0, "TransferedUser"] = "Thông tin giao hàng";
            flexStoreChange[1, "TransferedUser"] = "Nhân viên giao";
            flexStoreChange.Cols["TransferedUser"].Visible = true;

            flexStoreChange[0, "TransferedUserFullName"] = "Thông tin giao hàng";
            flexStoreChange[1, "TransferedUserFullName"] = "Nhân viên giao";
            flexStoreChange.Cols["TransferedUserFullName"].Visible = true;

            flexStoreChange[0, "IsSelect"] = " ";
            flexStoreChange[1, "IsSelect"] = " ";

            flexStoreChange[0, "IsReceivedTransfer"] = "Thông tin nhận hàng để vận chuyển";
            flexStoreChange[1, "IsReceivedTransfer"] = "Đã nhận";
            flexStoreChange.Cols["IsReceivedTransfer"].Visible = true;

            flexStoreChange[0, "ReceivedTransferUser"] = "Thông tin nhận hàng để vận chuyển";
            flexStoreChange[1, "ReceivedTransferUser"] = "Nhân viên nhận";
            flexStoreChange.Cols["ReceivedTransferUser"].Visible = true;

            flexStoreChange[0, "ReceivedTransferUserFullName"] = "Thông tin nhận hàng để vận chuyển";
            flexStoreChange[1, "ReceivedTransferUserFullName"] = "Nhân viên nhận";
            flexStoreChange.Cols["ReceivedTransferUserFullName"].Visible = true;

            flexStoreChange[0, "IsSignReceive"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "IsSignReceive"] = "Đã ký nhận";
            flexStoreChange.Cols["IsSignReceive"].Visible = true;

            flexStoreChange[0, "SignReceiveUser"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "SignReceiveUser"] = "Nhân viên ký nhận";
            flexStoreChange.Cols["SignReceiveUser"].Visible = true;

            flexStoreChange[0, "SignReceiveUserFullName"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "SignReceiveUserFullName"] = "Nhân viên ký nhận";
            flexStoreChange.Cols["SignReceiveUserFullName"].Visible = true;

            flexStoreChange[0, "SignReceiveDate"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "SignReceiveDate"] = "Thời gian ký nhận";
            flexStoreChange.Cols["SignReceiveDate"].Visible = true;

            flexStoreChange[0, "SignReceiveNote"] = "Thông tin ký nhận hàng";
            flexStoreChange[1, "SignReceiveNote"] = "Ghi chú ký nhận";
            flexStoreChange.Cols["SignReceiveNote"].Visible = true;

            flexStoreChange[0, "REVIEWUSER"] = "Người duyệt";
            flexStoreChange[1, "REVIEWUSER"] = "Người duyệt";
            flexStoreChange.Cols["REVIEWUSER"].Visible = true;

            #endregion

            for (int i = 1; i < flexStoreChange.Cols.Count; i++)
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowEditing(flexStoreChange, flexStoreChange.Cols[i].Name, false);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowEditing(flexStoreChange, "IsSelect,TransportVoucherID, IsTransfered, IsReceive, IsReceivedTransfer, IsSignReceive, ReceiveNote, SignReceiveNote", true);
            flexStoreChange.Cols["IsTransfered"].AllowSorting = false;
            flexStoreChange.Cols["IsReceive"].AllowSorting = false;
            flexStoreChange.Cols["IsReceivedTransfer"].AllowSorting = false;
            flexStoreChange.Cols["IsStoreChanged"].AllowSorting = false;
            flexStoreChange.Cols["IsSignReceive"].AllowSorting = false;

            flexStoreChange.Cols["TransferedUser"].AllowMerging = true;
            flexStoreChange.Cols["TransferedUserFullName"].AllowMerging = true;
            flexStoreChange.Cols["ReceivedTransferUser"].AllowMerging = true;
            flexStoreChange.Cols["ReceivedTransferUserFullName"].AllowMerging = true;
            flexStoreChange.Cols["SignReceiveUser"].AllowMerging = true;
            flexStoreChange.Cols["SignReceiveUserFullName"].AllowMerging = true;
            flexStoreChange.Cols["StoreChangeUser"].AllowMerging = true;
            flexStoreChange.Cols["StoreChangeUserFullName"].AllowMerging = true;
            flexStoreChange.Cols["ReceiveUserFullName"].AllowMerging = true;

            #region Format
            flexStoreChange.Cols["TotalShippingCost"].Format = "#,##0";
            flexStoreChange.Cols["DateCreateStoreChange"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["StoreChangeDate"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["TransferedDate"].Format = "dd/MM/yyyy HH:mm";//Ngày chuyển
            flexStoreChange.Cols["DateReceive"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["SignReceiveDate"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["ReceivedTransferDate"].Format = "dd/MM/yyyy HH:mm";
            flexStoreChange.Cols["IsNew"].DataType = typeof(bool);
            flexStoreChange.Cols["IsReceive"].DataType = typeof(bool);
            flexStoreChange.Cols["IsSelect"].DataType = typeof(bool);
            flexStoreChange.Cols["IsTransfered"].DataType = typeof(bool);
            flexStoreChange.Cols["IsRequireVoucher"].DataType = typeof(bool);
            flexStoreChange.Cols["IsStoreChanged"].DataType = typeof(bool);
            flexStoreChange.Cols["IsReceivedTransfer"].DataType = typeof(bool);
            flexStoreChange.Cols["IsSignReceive"].DataType = typeof(bool);
            flexStoreChange.Cols["FromStoreID"].Visible = false;
            Library.AppCore.LoadControls.C1FlexGridObject.AddColumnHeaderCheckBox(flexStoreChange, "IsSelect");
            #endregion

            #region Width
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexStoreChange,
                "StoreChangeID", 130,
                "DateCreateStoreChange", 110,
                "CaskCode", 200,
                "FromStoreName", 220,
                "ToStoreID", 80,
                "ToStoreName", 220,
                "StoreChangeDate", 110,
                "TransferedDate", 110,
                "DateReceive", 110,
                "Content", 220,
                "DaysOfStoreChange", 100,
                "TransferTime", 100,
                "InvoiceID", 80,
                "TotalPacking", 70,
                "StoreChangeTypeName", 150,
                "StoreChangeUser", 42,
                "ReceiveUser", 42,
                "TransferedUser", 42,
                "StoreChangeUserFullName", 140,
                "ReceiveUserFullName", 140,
                "ReceivedTransferDate", 110,
                "TransferedUserFullName", 140,

                "TransportTypeName", 150,
                "IsNew", 40,
                "IsReceive", 80,
                "IsTransfered", 80,
                "IsRequireVoucher", 80,
                "ReceiveNote", 150,
                "StoreChangeOrderID", 120,
                "InputVoucherID", 120,
                "OutputVoucherID", 120,
                "IsReceiveOld", 80,
                "IsEdit", 40,
                "IsSelect", 40,
                "IsStoreChanged", 60,
                "TransportVoucherID", 120,
                "TotalWeight", 80,
                "TotalSize", 80,
                "IsReceivedTransfer", 80,
                "ReceivedTransferUser", 42,
                "ReceivedTransferUserFullName", 140,
                "TotalShippingCost", 110,
                "IsSignReceive", 80,
                "SignReceiveUser", 42,
                "SignReceiveUserFullName", 140,
                "SignReceiveDate", 110,
                "SignReceiveNote", 150);
            #endregion

            #region TextAlign
            flexStoreChange.Cols["IsNew"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsReceive"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsStoreChanged"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["TotalPacking"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["DaysOfStoreChange"].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsSelect"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsReceivedTransfer"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsTransfered"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            flexStoreChange.Cols["IsSignReceive"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            #endregion

            #region Xét visible cho các cột
            if (!chkIsViewDetail.Checked)
            {
                // Xét thuộc tính ẩn cho các cột
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexStoreChange,
                    false,
                    "CaskCode",
                    "DateReceive",
                    "StoreChangeUser",
                    "StoreChangeUserFullName",
                    "StoreChangeDate",
                    "ReceiveUser",
                    "ReceiveUserFullName",
                    "TransportTypeName",
                    "IsNew",
                    "StoreChangeOrderID",
                    "InputVoucherID",
                    "OutputVoucherID",
                    "TransferedUser",
                    "TransferedUserFullName",
                    "TransferedDate",
                    "ReceivedTransferDate",
                    "ReceivedTransferUser",
                    "ReceivedTransferUserFullName",
                    "ReceiveNote",
                    "SignReceiveUser",
                    "SignReceiveUserFullName",
                    "SignReceiveDate",
                    "SignReceiveNote",
                    "IsRequireVoucher",
                    "TransportVoucherID",
                    "TotalShippingCost");

                flexStoreChange[0, "IsStoreChanged"] = "Đã xuất";
                flexStoreChange[1, "IsStoreChanged"] = "Đã xuất";
                flexStoreChange.Cols["IsStoreChanged"].AllowEditing = false;
                flexStoreChange.Cols["IsStoreChanged"].Visible = true;

                flexStoreChange[0, "IsTransfered"] = "Giao vận chuyển";
                flexStoreChange[1, "IsTransfered"] = "Giao vận chuyển";
                flexStoreChange.Cols["IsTransfered"].AllowEditing = false;
                flexStoreChange.Cols["IsTransfered"].Visible = true;

                flexStoreChange[0, "IsReceive"] = "Xác nhận thực nhập";
                flexStoreChange[1, "IsReceive"] = "Xác nhận thực nhập";
                flexStoreChange.Cols["IsReceive"].AllowEditing = false;
                flexStoreChange.Cols["IsReceive"].Visible = true;

                flexStoreChange[0, "IsReceivedTransfer"] = "Nhận vận chuyển";
                flexStoreChange[1, "IsReceivedTransfer"] = "Nhận vận chuyển";
                flexStoreChange.Cols["IsReceivedTransfer"].AllowEditing = false;
                flexStoreChange.Cols["IsReceivedTransfer"].Visible = true;

                flexStoreChange[0, "IsSignReceive"] = "Ký nhận nhập";
                flexStoreChange[1, "IsSignReceive"] = "Ký nhận nhập";
                flexStoreChange.Cols["IsSignReceive"].AllowEditing = false;
                flexStoreChange.Cols["IsSignReceive"].Visible = true;
                flexStoreChange.Rows[0].Height = 20;
                flexStoreChange.Rows[1].Height = 20;

            }
            else
            {
                // Xét thuộc tính hiển thị cho các cột
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexStoreChange,
                    true,
                    "CaskCode",
                    "DateReceive",
                    "Content",
                    "StoreChangeUser",
                    "StoreChangeUserFullName",
                    "StoreChangeDate",
                    "ReceiveUser",
                    "ReceiveUserFullName",
                    "TransportTypeName",
                    "IsNew",
                    "StoreChangeOrderID",
                    "InputVoucherID",
                    "OutputVoucherID",
                    "TransferedUser",
                    "TransferedUserFullName",
                    "TransferedDate",
                    "ReceivedTransferDate",
                    "ReceivedTransferUser",
                    "ReceivedTransferUserFullName",
                    "ReceiveNote",
                    "SignReceiveUser",
                    "SignReceiveUserFullName",
                    "SignReceiveDate",
                    "SignReceiveNote",
                    "IsRequireVoucher",
                    "TransportVoucherID",
                    "TotalShippingCost");

                flexStoreChange[0, "IsStoreChanged"] = "Thông tin xuất kho";
                flexStoreChange[1, "IsStoreChanged"] = "Đã xuất";
                flexStoreChange.Cols["IsStoreChanged"].AllowEditing = false;
                flexStoreChange.Cols["IsStoreChanged"].Visible = true;

                flexStoreChange[0, "IsTransfered"] = "Thông tin giao hàng";
                flexStoreChange[1, "IsTransfered"] = "Đã giao";
                flexStoreChange.Cols["IsTransfered"].AllowEditing = false;
                flexStoreChange.Cols["IsTransfered"].Visible = true;

                flexStoreChange[0, "IsReceive"] = "Thông tin xác nhận thực nhập";
                flexStoreChange[1, "IsReceive"] = "Đã nhập";
                flexStoreChange.Cols["IsReceive"].AllowEditing = false;
                flexStoreChange.Cols["IsReceive"].Visible = true;

                flexStoreChange[0, "IsReceivedTransfer"] = "Thông tin nhận hàng để vận chuyển";
                flexStoreChange[1, "IsReceivedTransfer"] = "Đã nhận";
                flexStoreChange.Cols["IsReceivedTransfer"].AllowEditing = false;
                flexStoreChange.Cols["IsReceivedTransfer"].Visible = true;

                flexStoreChange[0, "IsSignReceive"] = "Thông tin ký nhận hàng";
                flexStoreChange[1, "IsSignReceive"] = "Đã ký nhận";
                flexStoreChange.Cols["IsSignReceive"].AllowEditing = false;
                flexStoreChange.Cols["IsSignReceive"].Visible = true;
                flexStoreChange.Rows[0].Height = 20;
                flexStoreChange.Rows[1].Height = 20;
            }
            #endregion

            #region Định dạng Style cho lưới
            Library.AppCore.LoadControls.C1FlexGridObject.SetBackColor(flexStoreChange, SystemColors.Info);
            C1.Win.C1FlexGrid.CellStyle style = flexStoreChange.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexStoreChange.GetCellRange(0, 0, 0, flexStoreChange.Cols.Count - 1);
            range.Style = style;
            flexStoreChange.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowMerging(flexStoreChange, "TransferedUser,TransferedUserFullName,ReceivedTransferUser,ReceivedTransferUserFullName,SignReceiveUser,SignReceiveUserFullName,StoreChangeUser,StoreChangeUserFullName, ReceiveUserFullName, ReceiveUser", true);
            flexStoreChange.Rows[0].AllowMerging = true;
            flexStoreChange.Rows[1].AllowMerging = true;
            #endregion
        }

        //Format Grid

        //private void CreateColumnGrid()
        //{
        //    string[] fieldNames = new string[]
        //    {
        //        "STORECHANGEID", "DATECREATESTORECHANGE", "CASKCODE",
        //        "FROMSTORENAME", "TOSTORENAME", "ISSTORECHANGED", "TRANSFEREDDATE", "DATERECEIVE",
        //        "CONTENT", "TRANSFERTIME", "INVOICEID", "TOTALPACKING", "STORECHANGETYPENAME",
        //        "TRANSPORTTYPENAME", "ISNEW", "ISTRANSFERED","ISREQUIREVOUCHER", "TRANSPORTVOUCHERID",
        //        "ISRECEIVE", "RECEIVENOTE", "TOTALWEIGHT","TOTALSIZE", "TOTALSHIPPINGCOST",
        //        "STORECHANGEORDERID", "INPUTVOUCHERID", "OUTPUTVOUCHERID",
        //        "STORECHANGEUSER", "RECEIVEUSER", "RECEIVEUSERFULLNAME", "RECEIVEDTRANSFERDATE", "TRANSFEREDUSER",
        //        "TRANSFEREDUSERFULLNAME", "ISSELECT", "ISRECEIVEDTRANSFER", "RECEIVEDTRANSFERUSER", "RECEIVEDTRANSFERUSERFULLNAME",
        //        "ISSIGNRECEIVE", "SIGNRECEIVEUSER", "SIGNRECEIVEUSERFULLNAME", "SIGNRECEIVEDATE", "SIGNRECEIVENOTE",
        //        "REVIEWUSER"
        //    };
        //    Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
        //    //FormatGrid();
        //}
        //private bool FormatGrid()
        //{
        //    try
        //    {
        //        bool bolIsDetail = chkIsViewDetail.Checked;

        //        grdViewData.Columns["STORECHANGEID"].Visible = true;
        //        grdViewData.Columns["DATECREATESTORECHANGE"].Visible = true;
        //        grdViewData.Columns["CASKCODE"].Visible = true;
        //        grdViewData.Columns["FROMSTORENAME"].Visible = true;
        //        grdViewData.Columns["TOSTORENAME"].Visible = true;
        //        grdViewData.Columns["ISSTORECHANGED"].Visible = true;
        //        grdViewData.Columns["TRANSFEREDDATE"].Visible = true;
        //        grdViewData.Columns["DATERECEIVE"].Visible = true;
        //        grdViewData.Columns["CONTENT"].Visible = true;
        //        grdViewData.Columns["TRANSFERTIME"].Visible = true;
        //        grdViewData.Columns["INVOICEID"].Visible = true;
        //        grdViewData.Columns["TOTALPACKING"].Visible = true;
        //        grdViewData.Columns["STORECHANGETYPENAME"].Visible = true;
        //        grdViewData.Columns["TRANSPORTTYPENAME"].Visible = true;
        //        grdViewData.Columns["ISNEW"].Visible = true;
        //        grdViewData.Columns["ISTRANSFERED"].Visible = true;
        //        grdViewData.Columns["ISREQUIREVOUCHER"].Visible = true;
        //        grdViewData.Columns["TRANSPORTVOUCHERID"].Visible = true;
        //        grdViewData.Columns["ISRECEIVE"].Visible = true;
        //        grdViewData.Columns["RECEIVENOTE"].Visible = true;
        //        grdViewData.Columns["TOTALWEIGHT"].Visible = true;
        //        grdViewData.Columns["TOTALSIZE"].Visible = true;
        //        grdViewData.Columns["TOTALSHIPPINGCOST"].Visible = true;
        //        grdViewData.Columns["STORECHANGEORDERID"].Visible = true;
        //        grdViewData.Columns["INPUTVOUCHERID"].Visible = true;
        //        grdViewData.Columns["OUTPUTVOUCHERID"].Visible = true;
        //        grdViewData.Columns["STORECHANGEUSER"].Visible = true;
        //        grdViewData.Columns["RECEIVEUSER"].Visible = true;
        //        grdViewData.Columns["RECEIVEUSERFULLNAME"].Visible = true;
        //        grdViewData.Columns["RECEIVEDTRANSFERDATE"].Visible = true;
        //        grdViewData.Columns["TRANSFEREDUSER"].Visible = true;
        //        grdViewData.Columns["TRANSFEREDUSERFULLNAME"].Visible = true;
        //        grdViewData.Columns["ISSELECT"].Visible = true;
        //        grdViewData.Columns["ISRECEIVEDTRANSFER"].Visible = true;
        //        grdViewData.Columns["RECEIVEDTRANSFERUSER"].Visible = true;
        //        grdViewData.Columns["RECEIVEDTRANSFERUSERFULLNAME"].Visible = true;
        //        grdViewData.Columns["ISSIGNRECEIVE"].Visible = true;
        //        grdViewData.Columns["SIGNRECEIVEUSER"].Visible = true;
        //        grdViewData.Columns["SIGNRECEIVEUSERFULLNAME"].Visible = true;
        //        grdViewData.Columns["SIGNRECEIVEDATE"].Visible = true;
        //        grdViewData.Columns["SIGNRECEIVENOTE"].Visible = true;
        //        grdViewData.Columns["REVIEWUSER"].Visible = true;

        //        grdViewData.Columns["STORECHANGEID"].Caption = "Mã phiếu chuyển";
        //        grdViewData.Columns["DATECREATESTORECHANGE"].Caption = "Ngày chuyển";
        //        grdViewData.Columns["CASKCODE"].Caption = "Mã số thùng";
        //        grdViewData.Columns["FROMSTORENAME"].Caption = "Kho xuất";
        //        grdViewData.Columns["TOSTORENAME"].Caption = "Kho nhập";
        //        grdViewData.Columns["ISSTORECHANGED"].Caption = "Đã xuất";
        //        grdViewData.Columns["TRANSFEREDDATE"].Caption = "Thời gian giao";
        //        grdViewData.Columns["DATERECEIVE"].Caption = "";
        //        grdViewData.Columns["CONTENT"].Caption = "";
        //        grdViewData.Columns["TRANSFERTIME"].Caption = "";
        //        grdViewData.Columns["INVOICEID"].Caption = "";
        //        grdViewData.Columns["TOTALPACKING"].Caption = "";
        //        grdViewData.Columns["STORECHANGETYPENAME"].Caption = "";
        //        grdViewData.Columns["TRANSPORTTYPENAME"].Caption = "";
        //        grdViewData.Columns["ISNEW"].Caption = "";
        //        grdViewData.Columns["ISTRANSFERED"].Caption = "";
        //        grdViewData.Columns["ISREQUIREVOUCHER"].Caption = "";
        //        grdViewData.Columns["TRANSPORTVOUCHERID"].Caption = "";
        //        grdViewData.Columns["ISRECEIVE"].Caption = "";
        //        grdViewData.Columns["RECEIVENOTE"].Caption = "";
        //        grdViewData.Columns["TOTALWEIGHT"].Caption = "";
        //        grdViewData.Columns["TOTALSIZE"].Caption = "";
        //        grdViewData.Columns["TOTALSHIPPINGCOST"].Caption = "";
        //        grdViewData.Columns["STORECHANGEORDERID"].Caption = "";
        //        grdViewData.Columns["INPUTVOUCHERID"].Caption = "";
        //        grdViewData.Columns["OUTPUTVOUCHERID"].Caption = "";
        //        grdViewData.Columns["STORECHANGEUSER"].Caption = "";
        //        grdViewData.Columns["RECEIVEUSER"].Caption = "";
        //        grdViewData.Columns["RECEIVEUSERFULLNAME"].Caption = "";
        //        grdViewData.Columns["RECEIVEDTRANSFERDATE"].Caption = "";
        //        grdViewData.Columns["TRANSFEREDUSER"].Caption = "";
        //        grdViewData.Columns["TRANSFEREDUSERFULLNAME"].Caption = "";
        //        grdViewData.Columns["ISSELECT"].Caption = "";
        //        grdViewData.Columns["ISRECEIVEDTRANSFER"].Caption = "";
        //        grdViewData.Columns["RECEIVEDTRANSFERUSER"].Caption = "";
        //        grdViewData.Columns["RECEIVEDTRANSFERUSERFULLNAME"].Caption = "";
        //        grdViewData.Columns["ISSIGNRECEIVE"].Caption = "";
        //        grdViewData.Columns["SIGNRECEIVEUSER"].Caption = "";
        //        grdViewData.Columns["SIGNRECEIVEUSERFULLNAME"].Caption = "";
        //        grdViewData.Columns["SIGNRECEIVEDATE"].Caption = "";
        //        grdViewData.Columns["SIGNRECEIVENOTE"].Caption = "";
        //        grdViewData.Columns["REVIEWUSER"].Caption = "";

        //        grdViewData.ColumnPanelRowHeight = 50;
        //        grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        //        grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
        //        grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
        //        grdViewData.OptionsView.ShowAutoFilterRow = true;
        //        grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
        //        grdViewData.OptionsView.ShowFooter = false;
        //        grdViewData.OptionsView.ShowGroupPanel = false;
        //        Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
        //    }
        //    catch (Exception objExce)
        //    {
        //        MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách yêu cầu chuyển kho");
        //        SystemErrorWS.Insert("Định dạng lưới danh sách  yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> FormatGridProduct", DUIInventory_Globals.ModuleName);
        //        return false;
        //    }
        //    return true;
        //}
        //CreateGrid

        /// <summary>
        /// Kiểm tra dữ liệu nhập vào
        /// </summary>
        /// <returns></returns>
        private bool CheckInput()
        {

            if (!dtpFromDate.Checked)
            {
                MessageBox.Show(this, "Vui lòng chọn ngày chuyển hoặc ngày nhận", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpFromDate.Focus();
                return false;
            }

            if (dtpFromDate.Checked)
            {
                if (dtpToDate.Value.Subtract(dtpFromDate.Value).Days > 31)
                {
                    MessageBox.Show(this, "Vui lòng xem trong vòng 1 tháng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dtpFromDate.Focus();
                    return false;
                }
            }
            return true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckInput()) return;
            btnSearch.Enabled = false;

            try
            {
                #region Object KeyWord
                string strDaysOfStoreChange = "1";
                string strFromStoreList = Library.AppCore.Other.ConvertObject.ConvertIDListBracesToParentheses(cboFromStoreSearch.StoreIDList).Replace("(", "").Replace(")", "");
                string strToStoreList = Library.AppCore.Other.ConvertObject.ConvertIDListBracesToParentheses(cboToStoreSearch.StoreIDList).Replace("(", "").Replace(")", "");
                //string strTransportTypeIDList = Library.AppCore.LoadControls.CheckedComboBoxEditObject.GetCheckedItemToString(cboTransportType,Library.AppCore.Constant.EnumType.IDStringType.PARENTHESES);
                string strMainGroupIDList = cboMainGroup.MainGroupIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                string strStatusIDList = cboStatusList.ColumnIDList.Replace("><", ",").Replace("<", "").Replace(">", "");

                DateTime? dtFromStoreChangeDate = dtpFromDate.Value.Date;
                DateTime? dtToStoreChangeDate = dtpToDate.Value.Date;
                DateTime? dtFromReceiveDate = dtpFromDate.Value.Date;
                DateTime? dtToReceiveDate = dtpToDate.Value.Date;

                List<object> objList = new List<object>();
                objList.AddRange(new object[]
                                    {
                                        "@StoreChangeTypeID", cboStoreChangeType.SelectedValue,
                                        "@StatusIDList", strStatusIDList,
                                        "@FromStoreList",strFromStoreList,
                                        "@ToStoreList", strToStoreList,
                                        "@MainGroupIDList", strMainGroupIDList,
                                        "@DaysOfStoreChange", strDaysOfStoreChange,
                                        "@UserName", Library.AppCore.SystemConfig.objSessionUser.UserName,
                                        "@Keyword", txtKeyword.Text.Trim(),
                                        "@SearchBy", cboSearchBy.SelectedIndex
                                    });

                if (bolIsStoreChangeDate) objList.AddRange(new object[] { "@FromStoreChangeDate", dtpFromDate.Value.Date });
                if (bolIsStoreChangeDate) objList.AddRange(new object[] { "@ToStoreChangeDate", dtpToDate.Value.Date });
                if (!bolIsStoreChangeDate) objList.AddRange(new object[] { "@FromReceiveDate", dtpFromDate.Value.Date });
                if (!bolIsStoreChangeDate) objList.AddRange(new object[] { "@ToReceiveDate", dtpToDate.Value.Date });

                #endregion

                ERP.Inventory.PLC.StoreChange.PLCStoreChange objPLCStoreChange = new PLC.StoreChange.PLCStoreChange();
                DataTable dtbData = objPLCStoreChange.SearchData(objList.ToArray());
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSearch.Enabled = true;
                    return;
                }
                dtbData.Columns.Add("IsTransferedBefore", typeof(Boolean));
                dtbData.Columns.Add("IsReceivedTransferBefore", typeof(Boolean));
                dtbData.Columns.Add("IsSignReceiveBefore", typeof(Boolean));
                for (int i = 0; i < dtbData.Rows.Count; i++)
                {
                    dtbData.Rows[i]["IsTransferedBefore"] = Convert.ToBoolean(dtbData.Rows[i]["IsTransfered"]);
                    dtbData.Rows[i]["IsReceivedTransferBefore"] = Convert.ToBoolean(dtbData.Rows[i]["IsReceivedTransfer"]);
                    dtbData.Rows[i]["IsSignReceiveBefore"] = Convert.ToBoolean(dtbData.Rows[i]["IsSignReceive"]);
                }
                //flexStoreChange.DataSource = dtbData;
                //FormatFlex();
                if (chkIsViewDetail.Checked)
                    CreateBandView_StoreChangeDetail(dtbData);
                else
                    CreateBandView_StoreChange(dtbData);
                //grdData.DataSource = dtbData;
                bagrvReport.OptionsView.ShowAutoFilterRow = true;
                bagrvReport.OptionsView.ShowFooter = true;

                btnUpdate.Enabled = true;
                selection = new Library.AppCore.CustomControls.GridControl.BandGridCheckMarksSelection(true, true, bagrvReport);

                //  lbTotalQuantity.Value = dtbData.Rows.Count; //Đếm tổng số lượng phiếu chuyển kho tìm được
                //  lbTotalWeight.Value = Convert.ToDecimal(Globals.IsNull(dtbData.Compute("Sum(TotalWeight)", string.Empty), 0));//Tổng trọng lượng
                //  lbTotalSize.Value = Convert.ToDecimal(Globals.IsNull(dtbData.Compute("Sum(TotalSize)", string.Empty), 0));//Tổng thể tích
                // lbTotalQuantitySelected.Value = Convert.ToInt32(Globals.IsNull(dtbData.Compute("Sum(IsSelect)", "(IsTransfered = 1 Or IsReceivedTransfer = 1) And IsEdit = 1"), 0));
                //  lbTotalWeightSelected.Value = Convert.ToDecimal(Globals.IsNull(dtbData.Compute("Sum(TotalWeight)", "IsTransfered = 1 And IsEdit = 1"), 0));
                //  lbTotalSizeSelected.Value = Convert.ToDecimal(Globals.IsNull(dtbData.Compute("Sum(TotalSize)", "IsTransfered = 1 And IsEdit = 1"), 0));
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tìm kiếm phiếu chuyển kho", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tìm kiếm phiếu chuyển kho", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btnSearch.Enabled = true;
            }
        }

        private void flexStoreChange_DoubleClick(object sender, EventArgs e)
        {
            if (flexStoreChange.RowSel <= 1) return;
            if (flexStoreChange.ColSel == flexStoreChange.Cols["ReceiveNote"].Index) return;
            if (flexStoreChange.ColSel == flexStoreChange.Cols["IsTransfered"].Index) return;
            if (flexStoreChange.ColSel == flexStoreChange.Cols["IsReceive"].Index) return;
            if (flexStoreChange.ColSel == flexStoreChange.Cols["TransportVoucherID"].Index) return;

            frmStoreChange frmStoreChange1 = new frmStoreChange();
            frmStoreChange1.StoreChangeID = Convert.ToString(flexStoreChange[flexStoreChange.RowSel, "StoreChangeID"]).Trim();
            frmStoreChange1.IsOnlyCertifyFinger = Convert.ToBoolean(intOnlyCertifyFinger);
            frmStoreChange1.IsEdit = true;
            frmStoreChange1.ShowDialog();
            if (frmStoreChange1.IsUpdate)
                btnSearch_Click(null, null);
        }

        private void chkIsViewDetail_CheckedChanged(object sender, EventArgs e)
        {
            DataTable dtb = (DataTable)grdReport.DataSource;
            if (dtb != null && dtb.Rows.Count > 0)
            {
                if (chkIsViewDetail.Checked)
                    CreateBandView_StoreChangeDetail(dtb);
                else
                    CreateBandView_StoreChange(dtb);
                selection = new Library.AppCore.CustomControls.GridControl.BandGridCheckMarksSelection(true, true, bagrvReport);
            }
            //if (grdData.DataSource != null)
            //{
            //    FormatGrid();
            //}
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            #region Lấy danh sách các dòng có chỉnh sửa
            //Lấy danh sách các dòng có chỉnh sửa IsEdit = 1
            DataTable tblStoreChangeList = flexStoreChange.DataSource as DataTable;
            DataRow[] arrRow = tblStoreChangeList.Select("IsEdit = 1");
            if (arrRow.Length == 0)
                return;
            for (int i = 0; i < arrRow.Length; i++)
            {
                if (Convert.ToBoolean(arrRow[i]["IsRequireVoucher"]) && Convert.ToBoolean(arrRow[i]["IsReceive"])
                    && Convert.ToString(arrRow[i]["TransportVoucherID"]).Trim().Length < 1)
                {
                    MessageBox.Show(this, "Vui lòng nhập chứng từ vận chuyển đối với phiếu bắt buộc nhập.", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

            }

            bool bolIsInputSignReciveNote = false;
            for (int i = 0; i < arrRow.Length; i++)
            {
                if (Convert.ToBoolean(arrRow[i]["IsSignReceive"]) && !Convert.ToBoolean(arrRow[i]["ISSIGNRECEIVEOLD"])
                    && Convert.ToString(arrRow[i]["SignReceiveNote"]).Trim().Length < 1)
                {
                    bolIsInputSignReciveNote = true;
                    break;
                }
            }
            if (bolIsInputSignReciveNote)
            {
                Library.AppControl.FormCommon.frmInputString frmInputString1 = new Library.AppControl.FormCommon.frmInputString();
                frmInputString1.MaxLengthString = 200;
                frmInputString1.Text = "Nhập ghi chú ký nhận";
                frmInputString1.ShowDialog();
                if (frmInputString1.IsAccept)
                {
                    if (!string.IsNullOrEmpty(frmInputString1.ContentString))
                    {
                        for (int k = 0; k < tblStoreChangeList.Rows.Count; k++)
                        {
                            if (Convert.ToInt32(tblStoreChangeList.Rows[k]["IsEdit"]) == 1)
                                tblStoreChangeList.Rows[k]["SignReceiveNote"] = frmInputString1.ContentString;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Vui lòng nhập nội dung ghi chú", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnUpdate_Click(sender, e);
                        return;
                    }

                }
                else
                    return;
            }

            bool bolIsInputReciveNote = false;
            for (int i = 0; i < arrRow.Length; i++)
            {
                if (Convert.ToBoolean(arrRow[i]["IsReceive"]) && !Convert.ToBoolean(arrRow[i]["ISRECEIVEOLD"])
                    && Convert.ToString(arrRow[i]["ReceiveNote"]).Trim().Length < 1)
                {
                    bolIsInputReciveNote = true;
                    break;
                }
            }

            if (bolIsInputReciveNote)
            {
                Library.AppControl.FormCommon.frmInputString frmInputString1 = new Library.AppControl.FormCommon.frmInputString();
                frmInputString1.MaxLengthString = 200;
                frmInputString1.Text = "Nhập ghi chú xác nhận thực nhập";
                frmInputString1.ShowDialog();
                if (frmInputString1.IsAccept)
                {
                    if (!string.IsNullOrEmpty(frmInputString1.ContentString))
                    {
                        for (int k = 0; k < tblStoreChangeList.Rows.Count; k++)
                        {
                            if (Convert.ToInt32(tblStoreChangeList.Rows[k]["IsEdit"]) == 1)
                                tblStoreChangeList.Rows[k]["ReceiveNote"] = frmInputString1.ContentString;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng nhập nội dung ghi chú", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnUpdate_Click(sender, e);
                        return;
                    }
                }
                else
                    return;
            }
            #endregion

            try
            {
                string strCertifyTransferedUser = SystemConfig.objSessionUser.UserName;
                bool bolIsTransferedCheckedByFinger = false;
                string strCertifyReceivedTransferUser = SystemConfig.objSessionUser.UserName; ;
                bool bolIsReceivedTransferCheckinByFinger = false;
                #region Lấy thông tin kích thước sản phẩm
                //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
                ERP.Inventory.PLC.StoreChange.PLCStoreChange objPLCStoreChange = new PLC.StoreChange.PLCStoreChange();
                //Nếu có check "Đã chuyển" thì hiển thị Form 
                //Lấy thông tin hàng hóa có kích thước tối đa theo Mã phiếu xuất chuyển kho
                if (Convert.ToInt32(lbTotalQuantitySelected.Value) > 0)
                {
                    bool bolIsAccept = false;
                    string strStoreChangeIDlst = string.Empty;
                    DataRow[] lstTransfered = tblStoreChangeList.Select("IsTransfered = 1 And IsTransferedBefore = 0 And IsEdit = 1");
                    DataRow[] lstReceivedTransfer = tblStoreChangeList.Select("IsReceivedTransfer = 1 And IsReceivedTransferBefore = 0 And IsEdit = 1");
                    foreach (DataRow row in lstTransfered)
                    {
                        strStoreChangeIDlst = strStoreChangeIDlst + Convert.ToString(row["StoreChangeID"]).Trim() + ",";
                    }
                    strStoreChangeIDlst = "," + strStoreChangeIDlst;
                    DataTable dtbProductInfoMaxSize = objPLCStoreChange.GetProductInfoMaxSize(strStoreChangeIDlst);
                    if (dtbProductInfoMaxSize != null && dtbProductInfoMaxSize.Rows.Count > 0)
                    {
                        frmProductInfoMaxSize frmProductInfoMaxSize1 = new frmProductInfoMaxSize();
                        frmProductInfoMaxSize1.ProductInfoMaxSize = dtbProductInfoMaxSize;
                        frmProductInfoMaxSize1.ShowDialog();
                        //Nếu chấp nhận thì thực hiện Update
                        bolIsAccept = frmProductInfoMaxSize1.IsAccept;
                        if (!bolIsAccept)
                            return;
                    }
                    // Kiểm tra nếu có phiếu xuất chuyển kho có nhân viên đăng nhập không phải là nhân viên chuyển kho thì gọi form xác nhận nhân viên giao hàng
                    if (lstTransfered.Length > 0)
                    {
                        bool bolIsDifferentUser = false;
                        DataRow[] lstDifferentUser = tblStoreChangeList.Select("IsTransfered = 1 And IsEdit = 1 And StoreChangeUser <> '" + SystemConfig.objSessionUser.UserName + "'");
                        if (lstDifferentUser.Length > 0)
                            bolIsDifferentUser = true;
                        if (bolIsDifferentUser)
                        {
                            bool bolIsEnableLogin = Convert.ToBoolean(intOnlyCertifyFinger);
                            Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên giao hàng", "Giao hàng", null, null, !bolIsEnableLogin, true);
                            frmCertifyLogin1.ShowDialog(this);
                            //Nếu xác nhận thành công thì lấy thông tin xác nhận
                            if (!frmCertifyLogin1.IsCorrect)
                            {
                                return;
                            }
                            strCertifyTransferedUser = frmCertifyLogin1.UserName;
                            bolIsTransferedCheckedByFinger = frmCertifyLogin1.IsCertifyByFinger;
                        }
                    }

                    if (lstReceivedTransfer.Length > 0)
                    {
                        bool bolIsEnableLogin = Convert.ToBoolean(intOnlyCertifyFinger);
                        List<string> lstFunctionID = new List<string>();
                        lstFunctionID.Add(strPermission_IsReceivedTransfer);
                        Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận nhân viên nhận hàng", "Nhận hàng", null, lstFunctionID, !bolIsEnableLogin, true);
                        frmCertifyLogin1.ShowDialog(this);
                        //Nếu xác nhận thành công thì lấy thông tin xác nhận
                        if (!frmCertifyLogin1.IsCorrect)
                        {
                            return;
                        }
                        strCertifyReceivedTransferUser = frmCertifyLogin1.UserName;
                        bolIsReceivedTransferCheckinByFinger = frmCertifyLogin1.IsCertifyByFinger;

                    }

                }
                #endregion

                #region Gán giá trị cho các đối tượng WS
                List<ERP.Inventory.PLC.StoreChange.WSStoreChange.StoreChange> StoreChangeList = new List<ERP.Inventory.PLC.StoreChange.WSStoreChange.StoreChange>();
                foreach (DataRow objRow in arrRow)
                {
                    ERP.Inventory.PLC.StoreChange.WSStoreChange.StoreChange objStoreChangeBO = new PLC.StoreChange.WSStoreChange.StoreChange();
                    objStoreChangeBO.StoreChangeID = Convert.ToString(objRow["StoreChangeID"]).Trim();
                    objStoreChangeBO.StoreChangeUser = Convert.ToString(objRow["StoreChangeUser"]).Trim();
                    objStoreChangeBO.IsReceive = Convert.ToBoolean(objRow["IsReceive"]);
                    objStoreChangeBO.ReceiveNote = Convert.ToString(objRow["ReceiveNote"]).Trim();
                    objStoreChangeBO.UserReceive = SystemConfig.objSessionUser.UserName;
                    objStoreChangeBO.InputVoucherID = Convert.ToString(objRow["InputVoucherID"]).Trim();
                    objStoreChangeBO.IsTransfered = Convert.ToBoolean(objRow["IsTransfered"]);
                    objStoreChangeBO.TransportVoucherID = Convert.ToString(objRow["TransportVoucherID"]).Trim();
                    objStoreChangeBO.InvoiceID = Convert.ToString(objRow["InvoiceID"]).Trim();
                    objStoreChangeBO.InvoiceSymbol = Convert.ToString(objRow["InvoiceSymbol"]).Trim();
                    ////Trường hợp nhân viên đăng nhập không phải là nhân viên tạo phiếu thì nhân viên giao hàng là nhân viên đã xác nhận, ngược lại là nhân viên đăng nhập
                    //if (objStoreChangeBO.StoreChangeUser != SystemConfig.objSessionUser.UserName)
                    //    objStoreChangeBO.TransferedUser = strCertifyTransferedUser;
                    //else
                    //    objStoreChangeBO.TransferedUser = SystemConfig.objSessionUser.UserName;

                    objStoreChangeBO.TransferedUser = strCertifyTransferedUser;

                    objStoreChangeBO.IsReceivedTransfer = Convert.ToBoolean(objRow["IsReceivedTransfer"]);
                    //Trường hợp nhân viên đăng nhập không phải là nhân viên tạo phiếu thì nhân viên vận chuyển nhận hàng là nhân viên đã xác nhận, ngược lại là nhân viên đăng nhập
                    //if (objStoreChangeBO.StoreChangeUser != SystemConfig.objSessionUser.UserName)
                    //    objStoreChangeBO.ReceivedTransferUser = strCertifyReceivedTransferUser;
                    //else
                    //    objStoreChangeBO.ReceivedTransferUser = SystemConfig.objSessionUser.UserName;

                    objStoreChangeBO.ReceivedTransferUser = strCertifyReceivedTransferUser;

                    //Nếu xác nhận bằng vân tay
                    objStoreChangeBO.IsTransferedCheckinByFinger = bolIsTransferedCheckedByFinger;
                    objStoreChangeBO.IsReceivedCheckinByFinger = bolIsReceivedTransferCheckinByFinger;

                    //Thông tin ký nhận hàng
                    objStoreChangeBO.IsSignReceive = Convert.ToBoolean(objRow["IsSignReceive"]);
                    objStoreChangeBO.SignReceiveUser = SystemConfig.objSessionUser.UserName;
                    objStoreChangeBO.SignReceiveNote = Convert.ToString(objRow["SignReceiveNote"]).Trim();
                    StoreChangeList.Add(objStoreChangeBO);
                }
                #endregion

                #region Thực hiện Update dữ liệu
                objPLCStoreChange.UpdateStoreChangeList(StoreChangeList);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message);
                    SystemErrorWS.Insert(SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                MessageBox.Show(this, "Cập nhật phiếu chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnSearch_Click(null, null);
                #endregion
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi cập nhật phiếu chuyển kho", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi cập nhật phiếu chuyển kho", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void flexStoreChange_AfterSort(object sender, C1.Win.C1FlexGrid.SortColEventArgs e)
        {

            for (int i = flexStoreChange.Rows.Fixed; i < flexStoreChange.Rows.Count; i++)
            {
                if (Convert.ToBoolean(flexStoreChange.Rows[i]["IsTransfered"]) && !Convert.ToBoolean(flexStoreChange.Rows[i]["IsTransferedBefore"]))
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowForeColor(flexStoreChange, i, Color.MediumBlue);
                else
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowForeColor(flexStoreChange, i, Color.Black);
            }
        }

        private void flexStoreChange_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row <= 1)
                return;
            int intFromStore = Convert.ToInt32(flexStoreChange.Rows[e.Row]["FromStoreID"]);
            int intToStore = Convert.ToInt32(flexStoreChange.Rows[e.Row]["ToStoreID"]);
            //Không phải là cột chọn và đã nhập kho thì không cho phép chỉnh sửa
            if (e.Col != flexStoreChange.Cols["IsSelect"].Index && Convert.ToBoolean(flexStoreChange[e.Row, "ISRECEIVEOLD"]))
            {
                e.Cancel = true;
                return;
            }

            // Được nhập ghi chú xác nhận nhập kho nếu
            // Có quyền "PM_StoreChange_IsTransferedUpdate"
            // Có quyền "StoreChange_CheckIsReceive"
            // Có quyền trên kho nhập
            // Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            if (e.Col == flexStoreChange.Cols["SignReceiveNote"].Index)
            {
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
                    SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
                    (intCheckIsReceive > 0 ? (SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) : true))
                {
                    //Nếu nhân viên vận chuyển chưa nhận hàng thì không được nhập ghi chú
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]) &&
                        !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["ISSIGNRECEIVEOLD"]))
                    {
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }
            // Được nhập ghi chú ký nhận nếu
            // Có quyền "PM_StoreChange_IsTransferedUpdate"
            // Có quyền "StoreChange_CheckIsReceive"
            // Có quyền trên kho nhập
            // Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            if (e.Col == flexStoreChange.Cols["ReceiveNote"].Index)
            {
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
    SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
    (intCheckIsReceive > 0 ? (SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) : true))
                {
                    //Chưa ký nhận thì không được nhập ghi chú 
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["ISRECEIVEOLD"]))
                    {
                        e.Cancel = false;
                    }

                    else
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }

            //#region Đã chuyển - IsTransfered
            ////Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            //if (e.Col == flexStoreChange.Cols["IsTransfered"].Index)
            //{
            //    //Được check chọn "Đã chuyển" nếu:
            //    //Có quyền "PM_StoreChange_IsTransferedUpdate"
            //    //Có quyền trên Kho Xuất
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intFromStore, Library.AppCore.Constant.EnumType.StorePermissionType.OUTPUT))
            //    {
            //        //Nếu chưa nhận hàng để vận chuyển thì được check Đã chuyển
            //        if (!Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransfer"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"])) { e.Cancel = false; }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //}

            ////Được check chọn "Đã nhận" nếu:
            ////Có quyền "PM_StoreChange_IsTransferedUpdate"
            //if (e.Col == flexStoreChange.Cols["IsReceivedTransfer"].Index)
            //{
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate))
            //    {
            //        //Nếu đã check Đã chuyển, chưa ký Nhận hàng từ nhân viên vận chuyển và chưa check Đã nhận thì được check Đã nhận
            //        if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceive"])
            //            && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]))
            //        {
            //            e.Cancel = false;
            //        }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }

            //}

            //// Được check chọn "Đã ký nhận" nếu
            //// Có quyền "PM_StoreChange_IsTransferedUpdate"
            //// Có quyền "StoreChange_CheckIsReceive"
            //// Có quyền trên kho nhập
            //// Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            //if (e.Col == flexStoreChange.Cols["IsSignReceive"].Index)
            //{
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
            //        SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
            //        (intCheckIsReceive > 0 ? (SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) : true))
            //    {
            //        //Nếu nhân viên vận chuyển chưa nhận hàng thì không được check
            //        if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]))
            //        {
            //            e.Cancel = false;
            //        }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //}
            //#endregion

            //#region Đã nhận - IsReceive
            ////Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            //if (e.Col == flexStoreChange.Cols["IsReceive"].Index)
            //{
            //    //Được check chọn "Đã nhập" nếu:
            //    //Có quyền "strPermission_IsTransferedUpdate"
            //    //Có quyền "StoreChange_CheckIsReceive"
            //    //Có quyền trên Kho Nhập
            //    //Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
            //        SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
            //        ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
            //        (intCheckIsReceive>0 ? (SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) : true))
            //    {
            //        //Nếu check Đã ký nhận thì ko đc check Đã nhập liền, phải cập nhật trạng thái đã ký nhận
            //        if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]))
            //        {
            //            e.Cancel = false;
            //        }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //}
            //#endregion

            //#region Chứng từ - TransportVoucherID
            ////Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            //if (e.Col == flexStoreChange.Cols["TransportVoucherID"].Index)
            //{
            //    //Được nhập "Chứng từ vận chuyển" nếu:
            //    //Có quyền "PM_StoreChange_IsTransferedUpdate"
            //    //Có quyền trên Kho Xuất
            //    //Chứng từ chưa ký nhận
            //    if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate))
            //    {
            //        if (ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intFromStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER))
            //        {
            //            if (!Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceive"])) { e.Cancel = false;}
            //        }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }

            //    //Có quyền "PM_StoreChange_EditTransportVoucher"
            //    //Có quyền trên Kho Xuất & Kho Nhập
            //    if (SystemConfig.objSessionUser.IsPermission("PM_StoreChange_EditTransportVoucher"))
            //    {
            //        if (ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intFromStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER)
            //            || ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER))
            //        { e.Cancel = false; }
            //        else
            //        {
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    e.Cancel = true;
            //    return;
            //}
            //#endregion
        }

        private void flexStoreChange_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            //if (e.Row <= 1) return;
            flexStoreChange[e.Row, "IsEdit"] = true;

            #region Đã chuyển - IsTransfered
            if (e.Col == flexStoreChange.Cols["IsTransfered"].Index)
            {
                //Nếu check "Đã chuyển" thì tô chữ màu xanh
                if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransfered"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"]))
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowForeColor(flexStoreChange, e.Row, Color.MediumBlue);
                else
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowForeColor(flexStoreChange, e.Row, Color.Black);
            }

            #endregion

            #region Đã nhận - IsReceive
            //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            if (e.Col == flexStoreChange.Cols["IsSignReceive"].Index)
            {
                if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceive"]))
                {
                    //Nếu phiếu yêu cầu nhập chứng từ vận chuyển
                    //thì bắt buộc nhập chứng từ trước khi check Đã ký nhận
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsRequireVoucher"]) &&
                    Convert.ToString(flexStoreChange.Rows[e.Row]["TransportVoucherID"]).Trim().Length == 0)
                    {
                        MessageBox.Show(this, "Vui lòng nhập chứng từ trước.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexStoreChange.Rows[e.Row]["IsSignReceive"] = false;
                        return;
                    }
                }
            }
            #endregion

            #region Tính tổng
            //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            DataTable dtbFromFlex = (DataTable)flexStoreChange.DataSource;
            dtbFromFlex.AcceptChanges();
            lbTotalQuantitySelected.Value = Convert.ToInt32(Globals.IsNull(dtbFromFlex.Compute("Count(IsSelect)", "(IsTransfered = 1 And IsEdit = 1 And IsTransferedBefore = 0) Or (IsReceivedTransfer = 1 And IsEdit = 1 And IsReceivedTransferBefore = 0)"), 0));
            lbTotalWeightSelected.Value = Convert.ToDecimal(Globals.IsNull(dtbFromFlex.Compute("Sum(TotalWeight)", "IsTransfered = 1 And IsEdit = 1 And IsTransferedBefore = 0"), 0));
            lbTotalSizeSelected.Value = Convert.ToDecimal(Globals.IsNull(dtbFromFlex.Compute("Sum(TotalSize)", "IsTransfered = 1 And IsEdit = 1 And IsTransferedBefore = 0"), 0));
            #endregion
        }

        //In lệnh điều động nội bộ
        private void mnuItemPrintStoreChangeReport_Click(object sender, EventArgs e)
        {
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                string strStoreChangeID = drFocus["StoreChangeID"].ToString().Trim();
                PrintStoreAddress(strStoreChangeID, intStoreChange3_A4);
                string strUserCreate = drFocus["CreatedUser"].ToString().Trim();
                if (strUserCreate != SystemConfig.objSessionUser.UserName && SystemConfig.objSessionUser.UserName != "administrator")
                {
                    MessageBox.Show(this, "Bạn không có quyền in lệnh điều động nội bộ này", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            // PrintStoreChangeReport(strStoreChangeID);
        }

        //In địa chỉ chuyển hàng A4
        private void mnuItemPrintStoreAddressA4_Click(object sender, EventArgs e)
        {
            //if (flexStoreChange.Row < 1) return;
            //string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();

            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                string strStoreChangeID = drFocus["StoreChangeID"].ToString();
                PrintStoreAddress(strStoreChangeID, intStoreChange3_A4);
            }

        }

        /// <summary>
        /// In địa chỉ chuyển hàng
        /// </summary>
        private void PrintStoreAddress(string strStoreChangeID, int intReportID)
        {
            ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
            objReport.PrinterTypeID = "CommonPrinter";
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("StoreChangeID", strStoreChangeID);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        /// <summary>
        /// In lệnh điều động nội bộ.
        /// </summary>
        /// <param name="strStoreChangeID">Mã phiếu chuyển kho.</param>
        /// <returns></returns>
        public bool PrintStoreChangeReport(String strStorelChangeID)
        {
            bool result = false;
            ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intStoreChangeReportID);
            if (objReport != null)
            {
                objReport.PrinterTypeID = "CommonPrinter";
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("StoreChangeID", strStorelChangeID);
                //   htbParameterValue.Add("PrintUser", SystemConfig.objSessionUser.UserName);
                //   htbParameterValue.Add("UserPrintFullName", SystemConfig.objSessionUser.FullName);
                result = ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);//ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
                //return UpdatePrintTimes(strStoreChangeID);
            }
            else
            {
                MessageBox.Show(this, "Không tìm thấy thông tin cấu hình báo cáo " + intStoreChangeReportID + ", vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return result;
        }

        private void mnuItemPrintStoreAddressA5_Click(object sender, EventArgs e)
        {
            //if (flexStoreChange.Row < 1) return;
            //string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                string strStoreChangeID = drFocus["StoreChangeID"].ToString();
                PrintStoreAddress(strStoreChangeID, intStoreChange3_A5);
            }
        }

        private void mnuItemPrintStoreAddressA6_Click(object sender, EventArgs e)
        {
            //if (flexStoreChange.Row < 1) return;
            //string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                string strStoreChangeID = drFocus["StoreChangeID"].ToString();
                PrintStoreAddress(strStoreChangeID, intStoreChange3_A6);
            }
        }

        private void mnuItemPrintReport_Click(object sender, EventArgs e)
        {
            //if (flexStoreChange.Row < 1) return;
            //string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                string strStoreChangeID = drFocus["StoreChangeID"].ToString();
                PrintStoreChangeReport(strStoreChangeID);
            }
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            //Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexStoreChange);
            if (!mnuItemExportExcel.Enabled)
                return;
            if (bagrvReport.DataRowCount == 0)
                return;
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdReport);
        }

        private void llblDate_Click(object sender, EventArgs e)
        {
            bolIsStoreChangeDate = !bolIsStoreChangeDate;
            if (bolIsStoreChangeDate)
                llblDate.Text = "Ngày chuyển:";
            else
                llblDate.Text = "Ngày nhận:";
        }

        private void txtKeyword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
                btnSearch_Click(null, null);
        }

        private void flexStoreChange_CellChanged(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row <= 1)
                return;
            int intFromStore = Convert.ToInt32(flexStoreChange.Rows[e.Row]["FromStoreID"]);
            int intToStore = Convert.ToInt32(flexStoreChange.Rows[e.Row]["ToStoreID"]);

            //int storeChangeTypeID = Convert.ToInt32(cboStoreChangeType.SelectedValue);
            DataTable dtbTemp = flexStoreChange.DataSource as DataTable;
            var item = dtbTemp.AsEnumerable().Where(x => x["StoreChangeID"].ToString().Trim().Equals(flexStoreChange.Rows[e.Row]["StoreChangeID"].ToString().Trim())).ToArray();
            int storeChangeTypeID = Convert.ToInt32(item[0]["storeChangeTypeID"]);
            bool bolIsnotCheckReceiver = false;
            if (storeChangeTypeID > 0)
            {
                MasterData.PLC.MD.PLCStoreChangeType objPLCStoreChangeType = new MasterData.PLC.MD.PLCStoreChangeType();
                MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType = objPLCStoreChangeType.LoadInfo(storeChangeTypeID);
                bolIsnotCheckReceiver = objStoreChangeType.IsNotCheckReceiver;
            }


            #region Đã chuyển - IsTransfered
            //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            if (e.Col == flexStoreChange.Cols["IsTransfered"].Index)
            {
                //Được check chọn "Đã chuyển" nếu:
                //Có quyền "PM_StoreChange_IsTransferedUpdate"
                //Có quyền trên Kho Xuất
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intFromStore, Library.AppCore.Constant.EnumType.StorePermissionType.OUTPUT))
                {
                    //Nếu chưa nhận hàng để vận chuyển thì được check Đã chuyển
                    if (!Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransfer"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"]))
                    {
                        flexStoreChange[e.Row, "IsTransfered"] = flexStoreChange[e.Row, "IsTransfered"];
                    }
                    else
                    {
                        flexStoreChange[e.Row, "IsTransfered"] = flexStoreChange[e.Row, "IsTransferedBefore"];
                        return;
                    }
                }
                else
                {
                    flexStoreChange[e.Row, "IsTransfered"] = flexStoreChange[e.Row, "IsTransferedBefore"];
                    return;
                }
            }

            //Được check chọn "Đã nhận" nếu:
            //Có quyền "PM_StoreChange_IsTransferedUpdate"
            if (e.Col == flexStoreChange.Cols["IsReceivedTransfer"].Index)
            {
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate))
                {
                    //Nếu đã check Đã chuyển, chưa ký Nhận hàng từ nhân viên vận chuyển và chưa check Đã nhận thì được check Đã nhận
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsTransferedBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceive"])
                        && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]))
                    {
                        flexStoreChange[e.Row, "IsReceivedTransfer"] = flexStoreChange[e.Row, "IsReceivedTransfer"];
                    }
                    else
                    {
                        flexStoreChange[e.Row, "IsReceivedTransfer"] = flexStoreChange[e.Row, "IsReceivedTransferBefore"];
                        return;
                    }
                }
                else
                {
                    flexStoreChange[e.Row, "IsReceivedTransfer"] = flexStoreChange[e.Row, "IsReceivedTransferBefore"];
                    return;
                }

            }

            // Được check chọn "Đã ký nhận" nếu
            // Có quyền "PM_StoreChange_IsTransferedUpdate"
            // Có quyền "StoreChange_CheckIsReceive"
            // Có quyền trên kho nhập
            // Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
            if (e.Col == flexStoreChange.Cols["IsSignReceive"].Index)
            {

                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
                    SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
                    (intCheckIsReceive > 0 ? (((SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) && !bolIsnotCheckReceiver) || (bolIsnotCheckReceiver && SystemConfig.objSessionUser.IsPermission("MD_STORECHANGETYPE_SIGNRECEIPT"))) : true))
                {
                    //((SystemConfig.objSessionUser.IsPermission("MD_STORECHANGETYPE_INPUTVALIDATION") && objStoreChangeType.IsNotCheckReceiver) || !objStoreChangeType.IsNotCheckReceiver)
                    //Nếu nhân viên vận chuyển chưa nhận hàng thì không được check
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceivedTransferBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceiveOld"]))
                    {
                        flexStoreChange[e.Row, "IsSignReceive"] = flexStoreChange[e.Row, "IsSignReceive"];
                    }
                    else
                    {
                        flexStoreChange[e.Row, "IsSignReceive"] = flexStoreChange[e.Row, "IsSignReceiveBefore"];
                        return;
                    }
                }
                else
                {
                    flexStoreChange[e.Row, "IsSignReceive"] = flexStoreChange[e.Row, "IsSignReceiveBefore"];
                    return;
                }
            }
            #endregion

            #region Đã nhận - IsReceive
            //Chỉnh sửa quyền theo yêu cầu ngày 01/08/2012
            if (e.Col == flexStoreChange.Cols["IsReceive"].Index)
            {
                //Được check chọn "Đã nhập" nếu:
                //Có quyền "strPermission_IsTransferedUpdate"
                //Có quyền "StoreChange_CheckIsReceive"
                //Có quyền trên Kho Nhập
                //Trường hợp kiểm tra nhân viên nhập thì phải là một trong hai nhân viên nhập (Nhân viên nhập 1 hoặc nhân viên nhập 2)
                if (SystemConfig.objSessionUser.IsPermission(strPermission_IsTransferedUpdate) &&
                    SystemConfig.objSessionUser.IsPermission(strPermission_IsReceive) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT) &&
                    ERP.MasterData.PLC.MD.PLCStore.CheckPermission(intToStore, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER) &&
                    (intCheckIsReceive > 0 ? (((SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser1"].ToString() || SystemConfig.objSessionUser.UserName == flexStoreChange.Rows[e.Row]["ToUser2"].ToString()) && !bolIsnotCheckReceiver) || (bolIsnotCheckReceiver && SystemConfig.objSessionUser.IsPermission("MD_STORECHANGETYPE_INPUTVALIDATION"))) : true))
                {
                    //Nếu check Đã ký nhận thì ko đc check Đã nhập liền, phải cập nhật trạng thái đã ký nhận
                    if (Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsSignReceiveBefore"]) && !Convert.ToBoolean(flexStoreChange.Rows[e.Row]["IsReceiveOld"]))
                    {
                        flexStoreChange[e.Row, "IsReceive"] = flexStoreChange[e.Row, "IsReceive"];
                    }
                    else
                    {
                        flexStoreChange[e.Row, "IsReceive"] = flexStoreChange[e.Row, "IsReceiveOld"];
                        return;
                    }
                }
                else
                {
                    flexStoreChange[e.Row, "IsReceive"] = flexStoreChange[e.Row, "IsReceiveOld"];
                    return;
                }
            }
            #endregion

        }

        private void mnuItemPrintInternalOutput_Transfer_Click(object sender, EventArgs e)
        {
            //if (flexStoreChange.Row < 1) return;
            //string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                string strStoreChangeID = drFocus["StoreChangeID"].ToString();
                ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intInternalOutput_TransferID);
                objReport.PrinterTypeID = "CommonPrinter";
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("STORECHANGEID", strStoreChangeID);
                htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
                htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
                htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
                htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
                htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
                htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
        }

        private void mnuItemPrintStoreChangeCommand_Click(object sender, EventArgs e)
        {
            //if (flexStoreChange.Row < 1) return;
            //string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                string strStoreChangeID = drFocus["StoreChangeID"].ToString();
                ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intStoreChangeCommandID);
                objReport.PrinterTypeID = "CommonPrinter";
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("STORECHANGEID", strStoreChangeID);
                htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
                htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
                htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
                htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
                htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
                htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
        }
        private int GetNumOfLines2(string multiPageString, float wrapWidth, Font fnt, ref List<string> lstRes)
        {
            //wrapWidth += 5;
            StringFormat sfFmt = StringFormat.GenericDefault;
            int intNumLines = 0;
            int charactersFitted = 0;
            int linesFilled = 0;
            int linesFilledWord = 0;
            string[] words = multiPageString.Split(' ');
            string text = string.Empty;

            for (int i = 0; i < words.Length; i++)
            {
                using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
                {
                    SizeF size = g.MeasureString(words[i], fnt, new SizeF(wrapWidth - 6.5f, 150), null, out charactersFitted, out linesFilledWord);
                }
                if (linesFilledWord > 1)
                {
                    for (int j = 1; j < words[i].Length; j++)
                    {
                        string strChar = words[i].Substring(0, words[i].Length - j);
                        int linesFilledChar = 0;
                        using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
                        {
                            SizeF size = g.MeasureString(words[i], fnt, new SizeF(wrapWidth, 150), null, out charactersFitted, out linesFilledChar);
                        }
                        if (linesFilledChar <= 1)
                        {
                            if (!string.IsNullOrEmpty(text))
                            {
                                lstRes.Add(text);
                                intNumLines++;
                            }
                            lstRes.Add(strChar);
                            text = words[i].Substring(words[i].Length - j, j);
                            //lstRes.Add(words[i].Substring(words[i].Length - i, i));
                            intNumLines++;
                            break;
                        }
                    }
                    if (i == words.Length - 1)
                    {
                        lstRes.Add(text);
                        intNumLines++;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(text)) text += words[i];
                    else
                    {
                        using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
                        {
                            SizeF size = g.MeasureString(text + " " + words[i], fnt, new SizeF(wrapWidth, 150), null, out charactersFitted, out linesFilled);
                        }
                        if (linesFilled > 1)
                        {
                            lstRes.Add(text);
                            text = words[i];
                            intNumLines++;

                        }
                        else
                        {
                            text = text + " " + words[i];
                        }
                        //if (i == words.Length - 1)
                        //{
                        //    lstRes.Add(text);
                        //    intNumLines++;
                        //}

                    }
                    if (i == words.Length - 1)
                    {
                        lstRes.Add(text);
                        intNumLines++;
                    }
                }


            }
            intNumLines = lstRes.Count;
            return intNumLines;
            //using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
            //{
            //    SizeF size = g.MeasureString(multiPageString, fnt, new SizeF(wrapWidth,150),null,out charactersFitted,out linesFilled);
            //    return linesFilled;
            //    //int iHeight = (int)(Math.Ceiling(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height));
            //    //int iOneLineHeight = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Height;
            //    //int oneChar = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Width;
            //    //int oneLine = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Width;
            //    //int oneLineHeight = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height;
            //    //return (int)(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height / g.MeasureString("W", fnt, wrapWidth, sfFmt).Height);
            //}
        }
        //Kiểm lấy số dòng
        private int GetNumOfLines(string multiPageString, float wrapWidth, Font fnt, ref List<string> lstRes)
        {
            //wrapWidth += 5;
            //StringFormat sfFmt = StringFormat.GenericDefault;
            int intNumLines = 1;
            int charactersFitted = 0;
            int linesFilled = 0;
            int linesFilledWord = 0;
            string[] words = multiPageString.Split(' ');
            string text = string.Empty;

            for (int i = 0; i < words.Length; i++)
            {
                if (string.IsNullOrEmpty(text)) text += words[i];
                else
                {
                    using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
                    {
                        SizeF size = g.MeasureString(text + " " + words[i], fnt, new SizeF(wrapWidth, 150), null, out charactersFitted, out linesFilled);
                    }
                    if (linesFilled > 1)
                    {
                        lstRes.Add(text);
                        text = words[i];
                        intNumLines++;

                    }
                    else
                    {
                        text = text + " " + words[i];
                    }
                    if (i == words.Length - 1)
                    {
                        lstRes.Add(text);
                    }
                }
            }

            return intNumLines;
            //using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
            //{
            //    SizeF size = g.MeasureString(multiPageString, fnt, new SizeF(wrapWidth,150),null,out charactersFitted,out linesFilled);
            //    return linesFilled;
            //    //int iHeight = (int)(Math.Ceiling(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height));
            //    //int iOneLineHeight = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Height;
            //    //int oneChar = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Width;
            //    //int oneLine = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Width;
            //    //int oneLineHeight = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height;
            //    //return (int)(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height / g.MeasureString("W", fnt, wrapWidth, sfFmt).Height);
            //}
        }
        //Kiểm lấy số dòng
        private int GetNumOfLines1(string multiPageString, float wrapWidth, Font fnt)
        {
            //wrapWidth += 5;
            //StringFormat sfFmt = StringFormat.GenericDefault;

            int charactersFitted = 0;
            int linesFilled = 0;



            using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
            {
                SizeF size = g.MeasureString(multiPageString, fnt, new SizeF(wrapWidth, 150), null, out charactersFitted, out linesFilled);
                return linesFilled;
                //int iHeight = (int)(Math.Ceiling(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height));
                //int iOneLineHeight = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Height;
                //int oneChar = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Width;
                //int oneLine = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Width;
                //int oneLineHeight = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height;
                //return (int)(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height / g.MeasureString("W", fnt, wrapWidth, sfFmt).Height);
            }
        }
        //In phiếu xuất (In kim)
        private void mnuItemPrintInternalOuput_Click(object sender, EventArgs e)
        {
            //if (flexStoreChange.Row < 1) return;
            //string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                string strStoreChangeID = drFocus["StoreChangeID"].ToString();
                //Kiểm tra số dòng 
                DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(strStoreChangeID);
                int countRow = 0;
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                    Font font = new Font("Arial", 9.0f);

                    int rowProductName = GetNumOfLines1(strProductName, 268, new Font(font, FontStyle.Bold));
                    countRow += rowProductName;
                    if (countRow > 16)
                    {
                        MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                if (countRow < 16)
                {
                    int padding = 16 - countRow;
                    for (int i = 0; i < padding; i++)
                    {
                        DataRow rowPadding = dtData.NewRow();
                        dtData.Rows.Add(rowPadding);
                    }
                }

                if (!string.IsNullOrEmpty(strStoreChangeID))
                {
                    Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                    frm.StoreChangeID = strStoreChangeID;
                    frm.intIsStoreChangePrintInKim = 1;
                    frm.DataTableSouce = dtData;
                    frm.ShowDialog();
                }
            }
        }


        private void itemOutputVoucherTransfer_Click(object sender, EventArgs e)
        {
            //if (flexStoreChange.Row < 1) return;
            //string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString();
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                string strStoreChangeID = drFocus["StoreChangeID"].ToString();
                //Kiểm tra số dòng 
                DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(strStoreChangeID);
                int countRow = 0;
                //dtData.AsEnumerable().OrderBy(p=>p["PRODUCTNAME"]);
                DataTable dtbRes = dtData.Clone();
                dtbRes.Columns.Add("PAGE", typeof(int));
                int numPage = 1;
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                    Font font = new Font("Times New Roman", 10.0f);
                    List<string> productNameList = new List<string>();
                    int rowProductName = GetNumOfLines2(strProductName, 248.2f, new Font(font, FontStyle.Bold), ref productNameList);
                    countRow += rowProductName;
                    if (countRow > 12)
                    {
                        //if (countRow - rowProductName < 12)
                        //{
                        //    int padding = 12 - (countRow - rowProductName);
                        //    for (int j = 0; j < padding; j++)
                        //    {
                        //        //DataRow rowPadding = dtData.NewRow();
                        //        // dtData.Rows.Add(rowPadding);
                        //        DataRow rowPadding = dtbRes.NewRow();
                        //        rowPadding["PAGE"] = numPage;
                        //        dtbRes.Rows.Add(rowPadding);
                        //    }
                        //}
                        numPage++;
                        //MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //return;
                        countRow = rowProductName;

                    }
                    for (int j = 0; j < productNameList.Count; j++)
                    {
                        DataRow row = dtbRes.NewRow();
                        if (j == 0)
                        {
                            row["PRODUCTNAME"] = productNameList[j];
                            row["QUANTITYUNITNAME"] = dtData.Rows[i]["QUANTITYUNITNAME"].ToString().Trim();
                            row["STT"] = Convert.ToInt32(dtData.Rows[i]["STT"]);

                            row["QUANTITY"] = Convert.ToInt32(dtData.Rows[i]["QUANTITY"]);
                            row["PAGE"] = numPage;

                        }
                        else
                        {
                            row["PRODUCTNAME"] = productNameList[j];
                            //row["STT"] = 0;
                            row["PAGE"] = numPage;
                        }
                        dtbRes.Rows.Add(row);
                    }
                }
                //if (countRow < 12)
                //{
                //    int padding = 12 - countRow;
                //    for (int i = 0; i < padding; i++)
                //    {
                //        //DataRow rowPadding = dtData.NewRow();
                //        // dtData.Rows.Add(rowPadding);
                //        DataRow rowPadding = dtbRes.NewRow();
                //        rowPadding["PAGE"] = numPage;
                //        dtbRes.Rows.Add(rowPadding);
                //    }
                //}

                if (!string.IsNullOrEmpty(strStoreChangeID))
                {
                    Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                    frm.StoreChangeID = strStoreChangeID;
                    frm.intIsOutputVoucherTransferInKim = 1;
                    //frm.DataTableSouce = dtData;
                    frm.DataTableSouce = dtbRes;
                    frm.ShowDialog();
                }
            }
        }

        private void mnuItemPrintStoreChangeHandOvers_Click(object sender, EventArgs e)
        {
            try
            {
                if (intPrintStoreChangeHandovers < 0) return;
                if (bagrvReport.FocusedRowHandle < 0)
                    return;
                //DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
                //if (drFocus != null)
                //{
                //    string strStoreChangeID = drFocus["StoreChangeID"].ToString();

                //lấy cấu hình xét Thẻ cào vật lý
                string strSubGroupID = string.Empty;
                DataTable dtbAppConfig = Library.AppCore.DataSource.GetDataSource.GetAppConfig();
                var itemAppConfig = dtbAppConfig.AsEnumerable().Where(p => p["APPCONFIGID"].ToString().Trim()
                                                    == "MD_SUBGROUP_PHISYCALCARD").FirstOrDefault();
                if (itemAppConfig != null)
                    strSubGroupID = itemAppConfig["CONFIGVALUE"].ToString();

                //Cắt chuỗi
                List<int> lstSubGroub = new List<int>();
                string[] strArray = strSubGroupID.Split(';');
                if (strArray != null && strArray.Count() > 0)
                {
                    foreach (string item in strArray)
                    {
                        int intSubGroup = 0;
                        int.TryParse(item.Trim(), out intSubGroup);
                        if (intSubGroup != 0) lstSubGroub.Add(intSubGroup);
                    }

                }
                //Chạy hàm 

                DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
                if (drFocus != null)
                {
                    string strStoreChangeID = drFocus["StoreChangeID"].ToString();
                    //   string strStoreChangeID = flexStoreChange[flexStoreChange.Row, "StoreChangeID"].ToString().Trim();
                    object[] objKeywords = new object[] { "@STORECHANGEID", strStoreChangeID };
                    DataTable dtbFull = new ERP.Report.PLC.PLCReportDataSource().GetHeavyDataSource("RPT_STORECHANGE_HANDOVERS", objKeywords);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuItemPrintStoreChangeHandOvers_Click");
                    }
                    var lstJoinData = from o in dtbFull.AsEnumerable()
                                      join i in lstSubGroub
                                      on Convert.ToInt32(o.Field<object>("SUBGROUPID")) equals i
                                      select o;
                    if (dtbFull.Rows.Count > 0 && dtbFull != null)
                    {
                        DataTable dtbReport = dtbFull.Copy();
                        if (lstJoinData != null && lstJoinData.Count() > 0)
                        {
                            #region Xóa phần phải kiểm tra
                            foreach (int j in lstSubGroub)
                            {
                                DataRow[] drr = dtbReport.Select("SUBGROUPID= '" + j + "'");
                                if (drr != null && drr.Count() > 0)
                                {
                                    foreach (var item in drr)
                                        dtbReport.Rows.Remove(item);
                                }
                            }
                            #endregion
                            DataTable dtbTemp = lstJoinData.CopyToDataTable();
                            int i = 1;
                            foreach (DataRow dr in dtbTemp.Rows)
                            {
                                #region Đầu hàng
                                if (i == 1)
                                {
                                    dtbReport.ImportRow(dr);
                                    dtbReport.AcceptChanges();
                                    dtbFull.AcceptChanges();
                                }
                                #endregion
                                #region Cuối hàng
                                if (i == lstJoinData.Count() && i > 1)
                                {
                                    var temp = dtbReport.Rows[dtbReport.Rows.Count - 1];
                                    int intSTT = Convert.ToInt32(temp.Field<object>("STT"));

                                    string strFullName = dr["FULLNAME"].ToString().Trim();
                                    decimal decIMEI = 0;
                                    if (!Convert.IsDBNull(dr["IMEI"]))
                                        decIMEI = Convert.ToDecimal(dr["IMEI"].ToString().Trim());
                                    //bool bolImport = true;
                                    DataRow drCopyBefore = dtbTemp.Rows[i - 2];
                                    string strFullNameCopyBefore = drCopyBefore["FULLNAME"].ToString().Trim();
                                    decimal decIMEICopyBefore = 0;
                                    if (!Convert.IsDBNull(drCopyBefore["IMEI"]))
                                        decIMEICopyBefore = Convert.ToDecimal(drCopyBefore["IMEI"].ToString().Trim());

                                    if (strFullName == strFullNameCopyBefore)
                                    {
                                        if (decIMEI == decIMEICopyBefore + 1)
                                        {
                                            dr["REQUESTQUANTITY"] = -1;
                                            dr["REALQUANTITY"] = -1;

                                            DataRow[] drr = dtbReport.Select("STT = '" + intSTT + "'");
                                            if (drr != null && drr.Count() > 0)
                                            {
                                                drr[0]["REQUESTQUANTITY"] = Convert.ToInt32(drr[0]["REQUESTQUANTITY"]) + 1;
                                                drr[0]["REALQUANTITY"] = Convert.ToInt32(drr[0]["REALQUANTITY"]) + 1;
                                            }
                                            //bolImport = false;
                                            //temp["IMEI"] = temp.Field<object>("IMEI").ToString().Trim() + " - " + dr["IMEI"].ToString().Trim();
                                        }
                                    }
                                    dtbReport.ImportRow(dr);

                                }
                                #endregion
                                #region Giữa hàng
                                if (i > 1 && i < lstJoinData.Count())
                                {
                                    var temp = dtbReport.Rows[dtbReport.Rows.Count-1];
                                    int intSTT = Convert.ToInt32(temp.Field<object>("STT"));

                                    DataRow drCopyBefore = dtbTemp.Rows[i - 2];
                                    string strFullNameCopyBefore = drCopyBefore["FULLNAME"].ToString().Trim();
                                    decimal decIMEICopyBefore = 0;
                                    if (!Convert.IsDBNull(drCopyBefore["IMEI"]))
                                    {
                                        decIMEICopyBefore = Convert.ToDecimal(drCopyBefore.Field<object>("IMEI"));
                                    }

                                    string strFullName = dr["FULLNAME"].ToString().Trim();
                                    decimal decIMEI = Convert.ToDecimal(dr["IMEI"].ToString().Trim());

                                    DataRow drCopyAfter = dtbTemp.Rows[i];
                                    string strFullNameCopyAfter = drCopyAfter["FULLNAME"].ToString().Trim();
                                    decimal decIMEICopyAfter = 0;
                                    if (!Convert.IsDBNull(drCopyAfter["IMEI"]))
                                    {
                                        decIMEICopyAfter = Convert.ToDecimal(drCopyAfter.Field<object>("IMEI"));
                                    }
                                    //Xét cùng sản phẩm
                                    if (strFullName == strFullNameCopyBefore)
                                    {
                                        if (decIMEI != decIMEICopyBefore + 1)
                                        {
                                            dtbReport.ImportRow(dr);
                                        }
                                        //Số kế tiếp bằng với số phía trước + 1 
                                        if (decIMEI == decIMEICopyBefore + 1)
                                        {
                                            DataRow[] drr = dtbReport.Select("STT = '" + intSTT + "'");
                                            if (drr != null && drr.Count() > 0)
                                            {
                                                drr[0]["REQUESTQUANTITY"] = Convert.ToInt32(drr[0]["REQUESTQUANTITY"]) + 1;
                                                drr[0]["REALQUANTITY"] = Convert.ToInt32(drr[0]["REALQUANTITY"]) + 1;
                                            }
                                            ////Ngắt quãng
                                            if (decIMEI != decIMEICopyAfter - 1)
                                            {
                                                dr["REQUESTQUANTITY"] = -1;
                                                dr["REALQUANTITY"] = -1;
                                                //Ân gộp dòng imei
                                                //temp["IMEI"] = temp.Field<object>("IMEI").ToString().Trim() + " - " + dr["IMEI"].ToString().Trim();
                                                dtbReport.ImportRow(dr);
                                            }
                                            //Tiếp tục quãng
                                        }
                                        // dtbReport.ImportRow(drCopyBefore);
                                        //}
                                    }
                                    else
                                        dtbReport.ImportRow(dr);
                                }
                                #endregion
                                i++;
                            }

                        }
                        DataTable dtbReportFinal = dtbReport.Copy();
                        DataTable dtbSubGroup = dtbReport.Copy();
                        dtbSubGroup.Clear();

                        // Xóa phần sub group
                        foreach (int j in lstSubGroub)
                        {
                            DataRow[] drr = dtbReportFinal.Select("SUBGROUPID= '" + j + "'");
                            if (drr != null && drr.Count() > 0)
                            {
                                foreach (var item in drr)
                                {
                                    dtbSubGroup.ImportRow(item);
                                    if (item["REQUESTQUANTITY"].ToString() == "-1")
                                        dtbReportFinal.Rows.Remove(item);

                                }
                            }
                        }

                        //DataTable dtbAfterSubGroup = dtbReport.AsEnumerable().Where(p => p["SUBGROUPID"].ToString() != "-1").CopyToDataTable().Copy();


                        //DataTable dtbClone = 

                        //Lấy phần cần subgroup
                        int k = 0;
                        foreach (DataRow dr in dtbSubGroup.Rows)
                        {
                            if (Convert.ToString(dr["REQUESTQUANTITY"]) == "-1")
                            {
                                DataRow drCopyBefore = dtbSubGroup.Rows[k - 1];
                                string strIMEI = drCopyBefore["IMEI"].ToString().Trim();

                                DataRow[] drrTemp = dtbReportFinal.Select("IMEI = '" + strIMEI + "'");
                                // drrTemp[0]["IMEI"] = strIMEI + " - " + dr["IMEI"].ToString().Trim();
                                drrTemp[0]["IMEI"] = "Từ: " + strIMEI + "\nĐến: " + dr["IMEI"].ToString().Trim();
                            }
                            k++;
                        }

                        dtbReport.Clear();
                        dtbReport = dtbReportFinal;
                        dtbReport.DefaultView.Sort = "STT asc";
                        int stt_Final = 0;
                        string strName = string.Empty;
                        for (int i = 0; i < dtbReport.Rows.Count; i++)
                        {
                            if (!dtbReport.Rows[i]["FULLNAME"].ToString().Trim().Equals(strName.Trim()))
                            {
                                strName = dtbReport.Rows[i]["FULLNAME"].ToString().Trim();
                                stt_Final += 1;
                                dtbReport.Rows[i]["STT"] = stt_Final;
                            }
                        }
                        DataTable dtbClone = dtbReport;//.DefaultView.ToTable(true, "STT", "FULLNAME", "QUANTITYUNITNAME", "REQUESTQUANTITY", "REALQUANTITY", "IMEI", "SUBGROUPID", "PRODUCTID");
                                                       // Quăng vào form Report
                        if (!string.IsNullOrEmpty(strStoreChangeID))
                        {
                            ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intPrintStoreChangeHandovers);
                            if (objReport == null)
                            {
                                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin in biên bản bàn giao");
                                return;
                            }

                            object[] objKeywords2 = new object[] { "@STORECHANGEID", strStoreChangeID };
                            Hashtable htbParameterValue = new Hashtable();
                            DataTable dtbInfo = new ERP.Report.PLC.PLCReportDataSource().GetHeavyDataSource("RPT_STORECHANGE_HANDOVERS_INFO", objKeywords2);
                            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                            {
                                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                                SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuItemPrintStoreChangeHandOvers_Click");
                            }
                            if (dtbInfo != null && dtbInfo.Rows.Count > 0)
                            {
                                htbParameterValue.Add("CompanyName", dtbInfo.Rows[0]["FromstoreName"].ToString().Trim());
                                htbParameterValue.Add("CompanyAddress", dtbInfo.Rows[0]["FromstoreAddress"].ToString().Trim());
                                htbParameterValue.Add("OrderReason", dtbInfo.Rows[0]["CONTENT"].ToString().Trim());
                                htbParameterValue.Add("StoreName", dtbInfo.Rows[0]["STORENAME"].ToString().Trim());
                                htbParameterValue.Add("StoreAddress", dtbInfo.Rows[0]["STOREADDRESS"].ToString().Trim());
                                htbParameterValue.Add("PrintDate", Convert.ToDateTime(dtbInfo.Rows[0]["STORECHANGEDATE"]));
                                htbParameterValue.Add("ToUser", dtbInfo.Rows[0]["FULLNAME"].ToString().Trim());
                                htbParameterValue.Add("CodeNumber", dtbInfo.Rows[0]["CODE"].ToString().Trim());
                            }
                            else
                            {
                                htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
                                htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
                                htbParameterValue.Add("OrderReason", string.Empty);
                                htbParameterValue.Add("StoreName", string.Empty);
                                htbParameterValue.Add("StoreAddress", string.Empty);
                                htbParameterValue.Add("PrintDate", DateTime.Now);
                                htbParameterValue.Add("ToUser", string.Empty);
                                htbParameterValue.Add("CodeNumber", string.Empty);
                            }
                            htbParameterValue.Add("STORECHANGEORDERID", strStoreChangeID);
                            dtbReport.Columns.Remove("SUBGROUPID");
                            dtbReport.Columns.Remove("PRODUCTID");
                            objReport.DataTableSource = dtbReport;
                            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //else
            //    MessageBox.Show(this, "Lỗi in biên bản bàn giao!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }


        #region Tao luoi
        /// <summary>
        /// Tạo Grid theo phieu
        /// </summary>
        /// <param name="dtbData"></param>
        private void CreateBandView_StoreChange(DataTable dtbData)
        {
            ClearGrid();
            bagrvReport.MinBandPanelRowCount = 2;

            CreateGridBand(dtbData, "STORECHANGEID", typeof(string), "Mã phiếu chuyển", 140);
            bagrvReport.Columns["STORECHANGEID"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "STORECHANGEID", "Tổng cộng:");
            bagrvReport.Columns["STORECHANGEID"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            CreateGridBand(dtbData, "DATECREATESTORECHANGE", typeof(DateTime), "Ngày chuyển", 120);
            bagrvReport.Columns["DATECREATESTORECHANGE"].Summary.Add(DevExpress.Data.SummaryItemType.Count, "DATECREATESTORECHANGE", "{0:N0}");
            CreateGridBand(dtbData, "REVIEWUSER", typeof(string), "Người duyệt yêu cầu", 150);
            CreateGridBand(dtbData, "STORECHANGEUSERFULLUNAME", typeof(string), "Nhân viên xuất", 150);
            CreateGridBand(dtbData, "STORECHANGETYPENAME", typeof(string), "Loại chuyển kho", 150);
            CreateGridBand(dtbData, "FROMSTORENAME", typeof(string), "Kho xuất", 150);
            CreateGridBand(dtbData, "TOSTORENAME", typeof(string), "Kho nhập", 150);
            CreateGridBand(dtbData, "INVOICEID", typeof(string), "Số hóa đơn", 80);
            CreateGridBand(dtbData, "CONTENT", typeof(string), "Nội dung", 220);
            CreateGridBand(dtbData, "ISSTORECHANGED", typeof(decimal), "Đã xuất", 40);
            bagrvReport.Columns["ISSTORECHANGED"].ColumnEdit = repIsOutput;
            CreateGridBand(dtbData, "ISTRANSFERED", typeof(Int32), "Giao vận chuyển", 80);
            bagrvReport.Columns["ISTRANSFERED"].ColumnEdit = repCheckBox;
            CreateGridBand(dtbData, "ISRECEIVEDTRANSFER", typeof(Int32), "Nhận vận chuyển", 80);
            bagrvReport.Columns["ISRECEIVEDTRANSFER"].ColumnEdit = repCheckBox;
            CreateGridBand(dtbData, "ISSIGNRECEIVE", typeof(Int32), "Ký nhận nhập", 80);
            bagrvReport.Columns["ISSIGNRECEIVE"].ColumnEdit = repCheckBox;
            CreateGridBand(dtbData, "ISRECEIVE", typeof(Int32), "Xác nhận thực nhập", 80);
            bagrvReport.Columns["ISRECEIVE"].ColumnEdit = repCheckBox;
            CreateGridBand(dtbData, "DAYSOFSTORECHANGE", typeof(decimal), "TG đi đường", 100);
            bagrvReport.Columns["DAYSOFSTORECHANGE"].ColumnEdit = reposNumber;
            CreateGridBand(dtbData, "TOTALPACKING", typeof(decimal), "Số thùng", 70);
            bagrvReport.Columns["TOTALPACKING"].ColumnEdit = reposNumber;
            CreateGridBand(dtbData, "TOTALWEIGHT", typeof(decimal), "Tổng trọng lượng (Kg)", 80);
            bagrvReport.Columns["TOTALWEIGHT"].ColumnEdit = reposNumber;
            bagrvReport.Columns["TOTALWEIGHT"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "TOTALWEIGHT", "{0:N0}");
            CreateGridBand(dtbData, "TOTALSIZE", typeof(decimal), "Tổng thể tích (m³)", 80);
            bagrvReport.Columns["TOTALSIZE"].ColumnEdit = reposNumber;
            bagrvReport.Columns["TOTALSIZE"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "TOTALSIZE", "{0:N0}");

            grdReport.DataSource = dtbData;
        }

        /// <summary>
        /// Tạo Grid theo chi tiet phieu
        /// </summary>
        /// <param name="dtbData"></param>
        private void CreateBandView_StoreChangeDetail(DataTable dtbData)
        {
            ClearGrid();
            bagrvReport.MinBandPanelRowCount = 3;
            CreateGridBand(dtbData, "STORECHANGEID", typeof(string), "Mã phiếu chuyển", 140);
            // bagrvReport.Columns["STORECHANGEID"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            bagrvReport.Columns["STORECHANGEID"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "STORECHANGEID", "Tổng cộng:");
            CreateGridBand(dtbData, "DATECREATESTORECHANGE", typeof(DateTime), "Ngày chuyển", 120);
            bagrvReport.Columns["DATECREATESTORECHANGE"].Summary.Add(DevExpress.Data.SummaryItemType.Count, "DATECREATESTORECHANGE", "{0:N0}");
            CreateGridBand(dtbData, "REVIEWUSER", typeof(string), "Người duyệt yêu cầu", 220);

            //BandGrid

            DevExpress.XtraGrid.Views.BandedGrid.GridBand grdBandStoreChanged = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            grdBandStoreChanged.Caption = "Thông tin xuất hàng";
            grdBandStoreChanged.Name = "grdBand_StoreChanged";
            grdBandStoreChanged.OptionsBand.AllowMove = false;

            CreateGridBand(dtbData, "ISSTORECHANGED", typeof(decimal), "Đã xuất", 40);
            bagrvReport.Columns["ISSTORECHANGED"].ColumnEdit = repIsOutput;
            CreateGridBand(dtbData, "STORECHANGEUSERFULLUNAME", typeof(string), "Nhân viên xuất", 220);
            CreateGridBand(dtbData, "STORECHANGEDATE", typeof(DateTime), "Thời gian xuất", 120);

            grdBandStoreChanged.Children.Add(bagrvReport.Bands["grdBand_ISSTORECHANGED"]);
            grdBandStoreChanged.Children.Add(bagrvReport.Bands["grdBand_STORECHANGEUSERFULLUNAME"]);
            grdBandStoreChanged.Children.Add(bagrvReport.Bands["grdBand_STORECHANGEDATE"]);

            grdBandStoreChanged.Width = 600;
            bagrvReport.Bands.Add(grdBandStoreChanged);

            CreateGridBand(dtbData, "STORECHANGETYPENAME", typeof(string), "Loại chuyển kho", 150);
            CreateGridBand(dtbData, "FROMSTORENAME", typeof(string), "Kho xuất", 220);
            CreateGridBand(dtbData, "TOSTORENAME", typeof(string), "Kho nhập", 220);
            CreateGridBand(dtbData, "INVOICEID", typeof(string), "Số hóa đơn", 80);
            CreateGridBand(dtbData, "CONTENT", typeof(string), "Nội dung", 220);



            //-----------------------------

            DevExpress.XtraGrid.Views.BandedGrid.GridBand grdBandTransfered = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            grdBandTransfered.Caption = "Thông tin giao hàng";
            grdBandTransfered.Name = "grdBand_Transfered";
            grdBandTransfered.OptionsBand.AllowMove = false;

            CreateGridBand(dtbData, "ISTRANSFERED", typeof(Int32), "Đã giao", 40);
            bagrvReport.Columns["ISTRANSFERED"].ColumnEdit = repCheckBox;
            CreateGridBand(dtbData, "TRANSFEREDUSERFULLUNAME", typeof(string), "Nhân viên giao", 220);
            CreateGridBand(dtbData, "TRANSFEREDDATE", typeof(DateTime), "Thời gian giao", 120);

            grdBandTransfered.Children.Add(bagrvReport.Bands["grdBand_ISTRANSFERED"]);
            grdBandTransfered.Children.Add(bagrvReport.Bands["grdBand_TRANSFEREDUSERFULLUNAME"]);
            grdBandTransfered.Children.Add(bagrvReport.Bands["grdBand_TRANSFEREDDATE"]);
            //-----------------------------

            DevExpress.XtraGrid.Views.BandedGrid.GridBand grdBandReceivedTransfer = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            grdBandReceivedTransfer.Caption = "Thông tin nhận hàng để vận chuyển";
            grdBandReceivedTransfer.Name = "grdBand_ReceivedTransfer";
            grdBandReceivedTransfer.OptionsBand.AllowMove = false;

            CreateGridBand(dtbData, "ISRECEIVEDTRANSFER", typeof(Int32), "Đã nhận", 40);
            bagrvReport.Columns["ISRECEIVEDTRANSFER"].ColumnEdit = repCheckBox;
            CreateGridBand(dtbData, "RECEIVEDTRANSFERUSERFULLUNAME", typeof(string), "Nhân viên nhận", 220);
            CreateGridBand(dtbData, "RECEIVEDTRANSFERDATE", typeof(DateTime), "Thời gian nhận", 120);

            grdBandReceivedTransfer.Children.Add(bagrvReport.Bands["grdBand_ISRECEIVEDTRANSFER"]);
            grdBandReceivedTransfer.Children.Add(bagrvReport.Bands["grdBand_RECEIVEDTRANSFERUSERFULLUNAME"]);
            grdBandReceivedTransfer.Children.Add(bagrvReport.Bands["grdBand_RECEIVEDTRANSFERDATE"]);

            //-----------------------------


            DevExpress.XtraGrid.Views.BandedGrid.GridBand grdBandSignReceived = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            grdBandSignReceived.Caption = "Thông tin ký nhận hàng";
            grdBandSignReceived.Name = "grdBand_SignReceived";
            grdBandSignReceived.OptionsBand.AllowMove = false;

            CreateGridBand(dtbData, "ISSIGNRECEIVE", typeof(Int32), "Đã ký nhận", 60);
            bagrvReport.Columns["ISSIGNRECEIVE"].ColumnEdit = repCheckBox;
            CreateGridBand(dtbData, "SIGNRECEIVEUSERFULLUNAME", typeof(string), "Nhân viên ký nhận", 220);
            CreateGridBand(dtbData, "SIGNRECEIVEDATE", typeof(DateTime), "Thời gian ký nhận", 120);
            CreateGridBand(dtbData, "SIGNRECEIVENOTE", typeof(string), "Ghi chú ký nhận", 150);

            grdBandSignReceived.Children.Add(bagrvReport.Bands["grdBand_ISSIGNRECEIVE"]);
            grdBandSignReceived.Children.Add(bagrvReport.Bands["grdBand_SIGNRECEIVEUSERFULLUNAME"]);
            grdBandSignReceived.Children.Add(bagrvReport.Bands["grdBand_SIGNRECEIVEDATE"]);
            grdBandSignReceived.Children.Add(bagrvReport.Bands["grdBand_SIGNRECEIVENOTE"]);
            //-----------------------------

            DevExpress.XtraGrid.Views.BandedGrid.GridBand grdBandIsReceive = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            grdBandIsReceive.Caption = "Thông tin xác nhận thực nhập";
            grdBandIsReceive.Name = "grdBand_IsReceive";
            grdBandIsReceive.OptionsBand.AllowMove = false;

            CreateGridBand(dtbData, "ISRECEIVE", typeof(Int32), "Đã nhập", 40);
            bagrvReport.Columns["ISRECEIVE"].ColumnEdit = repCheckBox;
            CreateGridBand(dtbData, "RECEIVEUSERFULLUNAME", typeof(string), "Nhân viên nhập", 220);
            CreateGridBand(dtbData, "DATERECEIVE", typeof(DateTime), "Thời gian nhập", 120);
            bagrvReport.Columns["DATERECEIVE"].ColumnEdit = repFormatDate;
            CreateGridBand(dtbData, "RECEIVENOTE", typeof(string), "Ghi chú nhập", 150);

            grdBandIsReceive.Children.Add(bagrvReport.Bands["grdBand_ISRECEIVE"]);
            grdBandIsReceive.Children.Add(bagrvReport.Bands["grdBand_RECEIVEUSERFULLUNAME"]);
            grdBandIsReceive.Children.Add(bagrvReport.Bands["grdBand_DATERECEIVE"]);
            grdBandIsReceive.Children.Add(bagrvReport.Bands["grdBand_RECEIVENOTE"]);



            grdBandTransfered.Width = 600;
            grdBandSignReceived.Width = 600;
            grdBandReceivedTransfer.Width = 600;
            grdBandIsReceive.Width = 600;

            bagrvReport.Bands.Add(grdBandTransfered);
            bagrvReport.Bands.Add(grdBandReceivedTransfer);
            bagrvReport.Bands.Add(grdBandSignReceived);
            bagrvReport.Bands.Add(grdBandIsReceive);


            //Columns thường

            CreateGridBand(dtbData, "ISNEW", typeof(Int32), "Mới", 40);
            bagrvReport.Columns["ISNEW"].ColumnEdit = repCheckBox;
            CreateGridBand(dtbData, "STORECHANGEORDERID", typeof(string), "Mã yêu cầu", 140);
            CreateGridBand(dtbData, "INPUTVOUCHERID", typeof(string), "Mã phiếu nhập", 140);
            CreateGridBand(dtbData, "OUTPUTVOUCHERID", typeof(string), "Mã phiếu xuất", 140);
            CreateGridBand(dtbData, "DAYSOFSTORECHANGE", typeof(decimal), "TG đi đường", 100);
            bagrvReport.Columns["DAYSOFSTORECHANGE"].ColumnEdit = reposNumber;
            CreateGridBand(dtbData, "TOTALPACKING", typeof(decimal), "Số thùng", 70);
            bagrvReport.Columns["TOTALPACKING"].ColumnEdit = reposNumber;
            CreateGridBand(dtbData, "TOTALWEIGHT", typeof(decimal), "Tổng trọng lượng (Kg)", 80);
            bagrvReport.Columns["TOTALWEIGHT"].ColumnEdit = reposNumber;
            bagrvReport.Columns["TOTALWEIGHT"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "TOTALWEIGHT", "{0:N0}");
            CreateGridBand(dtbData, "TOTALSIZE", typeof(decimal), "Tổng thể tích (m³)", 80);
            bagrvReport.Columns["TOTALSIZE"].ColumnEdit = reposNumber;
            bagrvReport.Columns["TOTALSIZE"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "TOTALSIZE", "{0:N0}");
            CreateGridBand(dtbData, "ISREQUIREVOUCHER", typeof(decimal), "Yêu cầu chứng từ", 80);
            bagrvReport.Columns["ISREQUIREVOUCHER"].ColumnEdit = repIsOutput;
            CreateGridBand(dtbData, "TRANSPORTVOUCHERID", typeof(string), "Chứng từ vận chuyển", 100);

            CreateGridBand(dtbData, "TOTALSHIPPINGCOST", typeof(decimal), "Chi phí vận chuyển tạm tính", 100);
            bagrvReport.Columns["TOTALSHIPPINGCOST"].ColumnEdit = reposNumber;
            CreateGridBand(dtbData, "TRANSPORTTYPENAME", typeof(decimal), "Phương tiện vận chuyển", 120);
            CreateGridBand(dtbData, "CASKCODE", typeof(string), "Mã số thùng", 200);

            grdReport.DataSource = dtbData;
        }

        private void CreateGridBand(DataTable dtb, string strColumnName, Type objType, string strCation, int intWidth)
        {
            if (!dtb.Columns.Contains(strColumnName))
                dtb.Columns.Add(strColumnName, objType);
            if (!bagrvReport.Columns.Contains(bagrvReport.Columns[strColumnName]))
                CreateColumn(strColumnName, intWidth, objType);
            if (dtb.Columns.Contains(strColumnName))
            {
                DevExpress.XtraGrid.Views.BandedGrid.GridBand grdBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
                grdBand.Caption = strCation;
                grdBand.Name = "grdBand_" + strColumnName;
                grdBand.Width = intWidth;
                grdBand.Columns.Add(bagrvReport.Columns[strColumnName]);
                bagrvReport.Bands.Add(grdBand);
            }
        }

        private void CreateColumn(string ColumnName, int intWidth, Type objType)
        {
            DevExpress.XtraGrid.Columns.GridColumn column;
            column = bagrvReport.Columns.AddField(ColumnName);
            column.Caption = ColumnName;
            column.Width = intWidth;
            column.Visible = true;

            column.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, FontStyle.Bold);
            column.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            column.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            column.AppearanceHeader.Options.UseTextOptions = true;
            column.AppearanceHeader.Options.UseFont = true;

            column.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            column.AppearanceCell.Options.UseFont = true;

            column.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            column.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;

            column.OptionsColumn.AllowEdit = false;
            column.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.Default;
            //column.OptionsColumn.AllowMove = false;

            if (objType == typeof(DateTime))
            {
                column.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                column.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            }
        }
        private void ClearGrid()
        {
            grdReport.DataSource = null;
            bagrvReport.Columns.Clear();
            bagrvReport.Bands.Clear();
        }
        #endregion

        private void grdReport_DoubleClick(object sender, EventArgs e)
        {
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            if (drFocus != null)
            {
                frmStoreChange frmStoreChange1 = new frmStoreChange();
                frmStoreChange1.StoreChangeID = drFocus["StoreChangeID"].ToString().Trim();
                frmStoreChange1.IsOnlyCertifyFinger = Convert.ToBoolean(intOnlyCertifyFinger);
                frmStoreChange1.IsEdit = true;
                frmStoreChange1.ShowDialog();
                if (frmStoreChange1.IsUpdate)
                    btnSearch_Click(null, null);
            }
            //    if (flexStoreChange.RowSel <= 1) return;
            //if (flexStoreChange.ColSel == flexStoreChange.Cols["ReceiveNote"].Index) return;
            //if (flexStoreChange.ColSel == flexStoreChange.Cols["IsTransfered"].Index) return;
            //if (flexStoreChange.ColSel == flexStoreChange.Cols["IsReceive"].Index) return;
            //if (flexStoreChange.ColSel == flexStoreChange.Cols["TransportVoucherID"].Index) return;


        }

        private void bagrvReport_MouseDown(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            DevExpress.XtraGrid.Views.BandedGrid.ViewInfo.BandedGridHitInfo hit = bagrvReport.CalcHitInfo(pt);

            if (hit != null && hit.InRow == false)
            {
                if (hit.Band != null && hit.Band.Columns.Count > 0)
                {
                    if (hit.Band.Columns[0].SortOrder == DevExpress.Data.ColumnSortOrder.Ascending)
                    {
                        bagrvReport.ClearSorting();
                        hit.Band.Columns[0].SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                    }
                    else
                    {
                        bagrvReport.ClearSorting();
                        hit.Band.Columns[0].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                    }
                }
            }
        }

        private void mnuPopUp_Opening(object sender, CancelEventArgs e)
        {
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            if (mnuItemViewOtherReport.HasDropDownItems)
            {
                mnuItemViewOtherReport.DropDownItems.Clear();
            }
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            int intStoreChangeTypeID = Convert.ToInt32(drFocus["STORECHANGETYPEID"]);
            string strStoreChangeId = drFocus["StoreChangeID"].ToString().Trim();
            objStoreChangeType = new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo(intStoreChangeTypeID);
            String strExp = "AUTOPRINTSTEPID = 0";
            DataTable tblReportList = objStoreChangeType.StoreChangeType_ReportDTB;
            DataTable tblReport = Globals.SelectDistinct(tblReportList, "ReportID", strExp);
            foreach (DataRow objRow in tblReport.Rows)
            {
                bool bolIsPermissionView = SystemConfig.objSessionUser.IsPermission(Convert.ToString(objRow["ViewAllFunctionID"]).Trim());
                if (!bolIsPermissionView)
                {
                    bolIsPermissionView = SystemConfig.objSessionUser.IsPermission(Convert.ToString(objRow["ViewFunctionID"]).Trim())
                        && SystemConfig.objSessionUser.UserName.Equals(drFocus["CreatedUser"].ToString().Trim());
                }
                int intReportID = Convert.ToInt32(objRow["ReportID"]);
                String strReportName = string.Empty;
                if (objRow["ISPRINTVAT"].ToString() == "1")
                    strReportName = "Phiếu xuất kho kiêm vận chuyển nội bộ(mẫu in kim)";
                else
                    strReportName = Convert.ToString(objRow["ReportName"]).Trim();

                ToolStripMenuItem mnuItem = new ToolStripMenuItem(strReportName);
                mnuItem.Enabled = bolIsPermissionView;
                mnuItem.Tag = objRow;
                mnuItem.Click += new EventHandler(mnuItem_Click);
                mnuItemViewOtherReport.DropDownItems.Add(mnuItem);
            }
            bool IsTransfered = Convert.ToBoolean(drFocus["IsTransfered"]);
            bool IsReceivedTransfer = Convert.ToBoolean(drFocus["IsReceivedTransfer"]);
            bool IsSignReceive = Convert.ToBoolean(drFocus["IsSignReceive"]);
            bool IsReceive = Convert.ToBoolean(drFocus["IsReceive"]);
            bool bolIsVisibleConfigPrint = CheckStoreChange(IsTransfered, IsReceivedTransfer, IsSignReceive, IsReceive);
            int intFromStoredId = Convert.ToInt32(drFocus["FromStoreID"]);
            int intToStoredId = Convert.ToInt32(drFocus["ToStoreID"]);
            new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objFromStore, intFromStoredId);
            if (objFromStore == null)
            {
                MessageBox.Show(this, "Không tìm thấy kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, intToStoredId);
            if (objToStore == null)
            {
                MessageBox.Show(this, "Không tìm thấy kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            ToolStripMenuItem mnuItemTemp = new ToolStripMenuItem("In lại PXK kiêm VCNB");

            mnuItemTemp.Enabled = bolIsVisibleConfigPrint && objFromStore.BranchID == objToStore.BranchID;
            mnuItemTemp.Tag = 1;
            mnuItemTemp.Click += new EventHandler(mnuItem_Click);
            mnuItemViewOtherReport.DropDownItems.Add(mnuItemTemp);

            //begin locpt 22/10/2018 thêm theo y/c của Bình BA
            ToolStripMenuItem mnuItemTemp2 = new ToolStripMenuItem("In PXK kiêm VCNB tại chi nhánh 1");

            mnuItemTemp2.Enabled = bolIsVisibleConfigPrint && objFromStore.BranchID == 1;
            mnuItemTemp2.Tag = 2;
            mnuItemTemp2.Click += new EventHandler(mnuItem_Click);
            mnuItemViewOtherReport.DropDownItems.Add(mnuItemTemp2);

            mnuItemConfigPrint.Enabled = objFromStore.BranchID == objToStore.BranchID;
            if (objFromStore.BranchID != objToStore.BranchID)
            {
                ERP.Inventory.PLC.StoreChange.WSStoreChange.StoreChange objStoreChangeFocused = objPLCStoreChange.LoadInfo(strStoreChangeId);
                PLC.PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder = new PLC.StoreChange.PLCStoreChangeOrder().LoadInfo(objStoreChangeFocused.StoreChangeOrderID, -1);
                if (objStoreChangeOrder != null)
                {
                    MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfoFromCache(objStoreChangeOrder.StoreChangeOrderTypeID);
                    if (objStoreChangeOrderType != null)
                    {
                        mnuItemConfigPrint.Enabled = objStoreChangeOrderType.IsPrintOutputVoucher;
                        mnuItemTemp.Enabled = bolIsVisibleConfigPrint && objStoreChangeOrderType.IsPrintOutputVoucher;
                    }
                }
            }

            mnuItemViewOtherReport.Visible = mnuItemViewOtherReport.HasDropDownItems;
            mnuViewStoreChangeDetailReport.Visible = SystemConfig.objSessionUser.IsPermission(STORECHANGEDETAILREPORT_VIEW)
                && selection != null && selection.SelectedCount > 0;
        }

        void mnuItem_Click(object sender, EventArgs e)
        {
            if (bagrvReport.FocusedRowHandle < 0)
                return;
            DataRow drFocus = (DataRow)bagrvReport.GetDataRow(bagrvReport.FocusedRowHandle);
            ToolStripMenuItem mnuItem = sender as ToolStripMenuItem;
            int intTag = 0;
            int.TryParse(mnuItem.Tag.ToString(), out intTag);
            if (intTag == 1 || intTag == 2)
            {
                StoreChangeParam objStoreChangeParam = new StoreChangeParam();
                objStoreChangeParam.InvoiceId = Convert.ToString(drFocus["InvoiceID"]).Trim();
                objStoreChangeParam.StoreChangeId = Convert.ToString(drFocus["StoreChangeID"]).Trim();
                objStoreChangeParam.OutputVoucherId = Convert.ToString(drFocus["OutputVoucherID"]).Trim();
                objStoreChangeParam.FromStoreId = Convert.ToInt32(drFocus["FromStoreID"]);
                objStoreChangeParam.ToStoreId = Convert.ToInt32(drFocus["ToStoreID"]);
                objStoreChangeParam.StoreChangeOrderId = Convert.ToString(drFocus["StoreChangeOrderID"]).Trim();
                objStoreChangeParam.Mauso = Convert.ToString(drFocus["TransportVoucherID"]).Trim();
                objStoreChangeParam.InvoiceSymbol = Convert.ToString(drFocus["INVOICESYMBOL"]).Trim();

                //StoredChange_CurrentVoucher objStoredChange_CurrentVoucher = null;
                //objStoredChange_CurrentVoucher = objPLCStoreChange.LoadInfoConfigPrint(SystemConfig.ConfigStoreID);
                //if (objStoredChange_CurrentVoucher == null)
                //{
                //    MessageBoxObject.ShowWarningMessage(this, "Vui lòng kiểm tra cấu hình in kim!");
                //    return;
                //}
                //objStoreChangeParam.CurrentNumber = Convert.ToInt32(objStoredChange_CurrentVoucher.InvoiceID);
                //objStoreChangeParam.InvoiceIDStart = Convert.ToInt32(objStoredChange_CurrentVoucher.InvoiceIdStart);
                object[] objKeywords = new object[] { "@StoreID", SystemConfig.ConfigStoreID,
                                         "@UserName",SystemConfig.objSessionUser.UserName,
                                         "@STORECHANGECURVOUCHERID",string.Empty,
                                         "@IsGetAll",false};
                DataTable dtbData = objPLCStoreChange.SearchDataConfigPrint(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                if (dtbData == null || dtbData.Rows.Count < 1)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Chưa cấu hình in kim hoặc cấu hình in kim không hợp lệ. Vui lòng kiểm tra lại!");
                    return;
                }
                DataTable dtbHistory = null;
                dtbHistory = objPLCStoreChange.GetHistoryPrint(objStoreChangeParam.StoreChangeId);

                if (dtbHistory == null || dtbHistory.Rows.Count < 1)
                {
                    frmConfigPrint frmConfigPrint = new frmConfigPrint();
                  //  frmConfigPrint.IsPrint = true;
                  //  frmConfigPrint.StoredChange_CurrentVoucher = objStoredChange_CurrentVoucher;
                    frmConfigPrint.StoreChangeID = objStoreChangeParam.StoreChangeId;
                    frmConfigPrint.OutputVoucherID = objStoreChangeParam.OutputVoucherId;
                    frmConfigPrint.IsRePrint = true;
                    frmConfigPrint.Tag = intTag;
                    frmConfigPrint.StoreChangeParam = objStoreChangeParam;
                    frmConfigPrint.DtbData = dtbData;
                    frmConfigPrint.ShowDialog();
                    if (frmConfigPrint.IsUpdate)
                    {
                        btnSearch_Click(null, null);
                    }
                }
                else
                {
                    var row = dtbHistory.AsEnumerable().Where(x => Convert.ToInt32(x.Field<object>("INVOICEID")) == Convert.ToInt32(objStoreChangeParam.InvoiceId)
                                                            && Convert.ToInt32(x.Field<object>("PRINTEDSTOREID")) == SystemConfig.ConfigStoreID &&
                                                              x.Field<object>("DENOMINATOR").ToString().Trim() == objStoreChangeParam.Mauso &&
                                                              x.Field<object>("INVOICESYMBOL").ToString().Trim() == objStoreChangeParam.InvoiceSymbol
                                                              && x.Field<object>("PRINTEDBY").ToString().Trim() == SystemConfig.objSessionUser.UserName)
                                                              .OrderByDescending(x => Convert.ToDateTime(x.Field<object>("PRINTEDDATE"))).FirstOrDefault();
                    if (row != null)
                    {
                        objStoreChangeParam.STORECHANGECURVOUCHERID = row["STORECHANGECURVOUCHERID"].ToString().Trim();
                    }
                    else
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Không lấy được mã thiết lập của cấu hình từ lịch sử in!");
                        return;
                    }

                    frmRePrint frmRePrint = new frmRePrint();
                    frmRePrint.StoreChangeParam = objStoreChangeParam;
                    frmRePrint.DtbData = dtbData;
                    frmRePrint.Tag = intTag;
                    frmRePrint.ShowDialog();
                    if (frmRePrint.IsUpdate)
                        btnSearch_Click(null, null);
                }
            }
            else
            {
                DataRow objReportInfo = mnuItem.Tag as DataRow;
                int intReportID = Convert.ToInt32(objReportInfo["REPORTID"]);
                string strStoreChangeID = Convert.ToString(drFocus["StoreChangeID"]).Trim();
                String strPrinterTypeID = Convert.ToString(objReportInfo["PRINTERTYPEID"]).Trim();
                if (objReportInfo["ISPRINTVAT"].ToString() == "1")
                {
                    PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVAT_InvoiceMain = null;
                    PLC.PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder = new PLC.StoreChange.PLCStoreChangeOrder().LoadInfo(objStoreChange.StoreChangeOrderID, -1);
                    DataTable dtbInvoiceId = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_INVOICE_SELBYOUTPUTID", new object[] { "@OUTPUTVOUCHERID", objStoreChange.OutputVoucherID });
                    string strVATINVOICEID = string.Empty;
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        SystemErrorWS.Insert(SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->barItem_ItemClick");
                        return;
                    }
                    if (dtbInvoiceId != null && dtbInvoiceId.Rows.Count > 0)
                    {
                        strVATINVOICEID = dtbInvoiceId.Rows[0][0].ToString();
                        new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().LoadInfo(ref objVAT_InvoiceMain, strVATINVOICEID);
                    }

                    //new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objFromStore, objStoreChange.FromStoreID);
                    //if (objFromStore == null)
                    //{
                    //    MessageBox.Show(this, "Không tìm thấy kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //    return;
                    //}

                    //new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, objStoreChange.ToStoreID);
                    //if (objToStore == null)
                    //{
                    //    MessageBox.Show(this, "Không tìm thấy kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //    return;
                    //}
                    MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfoFromCache(objStoreChangeOrder.StoreChangeOrderTypeID);
                    if (string.IsNullOrWhiteSpace(objStoreChange.StoreChangeOrderID))
                    {
                        if (objFromStore.BranchID == objToStore.BranchID)
                        {
                            DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(strStoreChangeID);
                            int countRow = 0;
                            //dtData.AsEnumerable().OrderBy(p=>p["PRODUCTNAME"]);
                            DataTable dtbRes = dtData.Clone();
                            dtbRes.Columns.Add("PAGE", typeof(int));
                            int numPage = 1;
                            for (int i = 0; i < dtData.Rows.Count; i++)
                            {
                                string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                                Font font = new Font("Times New Roman", 10.0f);
                                List<string> productNameList = new List<string>();
                                int rowProductName = GetNumOfLines2(strProductName, 248.8f, new Font(font, FontStyle.Bold), ref productNameList);
                                countRow += rowProductName;
                                if (countRow > 12)
                                {
                                    //if (countRow - rowProductName < 12)
                                    //{
                                    //    int padding = 12 - (countRow - rowProductName);
                                    //    for (int j = 0; j < padding; j++)
                                    //    {
                                    //        //DataRow rowPadding = dtData.NewRow();
                                    //        // dtData.Rows.Add(rowPadding);
                                    //        DataRow rowPadding = dtbRes.NewRow();
                                    //        rowPadding["PAGE"] = numPage;
                                    //        dtbRes.Rows.Add(rowPadding);
                                    //    }
                                    //}
                                    numPage++;
                                    //MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    //return;
                                    countRow = rowProductName;

                                }
                                for (int j = 0; j < productNameList.Count; j++)
                                {
                                    DataRow row = dtbRes.NewRow();
                                    if (j == 0)
                                    {
                                        row["PRODUCTNAME"] = productNameList[j];
                                        row["QUANTITYUNITNAME"] = dtData.Rows[i]["QUANTITYUNITNAME"].ToString().Trim();
                                        row["STT"] = Convert.ToInt32(dtData.Rows[i]["STT"]);

                                        row["QUANTITY"] = Convert.ToInt32(dtData.Rows[i]["QUANTITY"]);
                                        row["PAGE"] = numPage;

                                    }
                                    else
                                    {
                                        row["PRODUCTNAME"] = productNameList[j];
                                        //row["STT"] = 0;
                                        row["PAGE"] = numPage;
                                    }
                                    dtbRes.Rows.Add(row);
                                }
                            }
                            //if (countRow < 12)
                            //{
                            //    int padding = 12 - countRow;
                            //    for (int i = 0; i < padding; i++)
                            //    {
                            //        //DataRow rowPadding = dtData.NewRow();
                            //        // dtData.Rows.Add(rowPadding);
                            //        DataRow rowPadding = dtbRes.NewRow();
                            //        rowPadding["PAGE"] = numPage;
                            //        dtbRes.Rows.Add(rowPadding);
                            //    }
                            //}

                            if (!string.IsNullOrEmpty(strStoreChangeID))
                            {
                                Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                                frm.StoreChangeID = strStoreChangeID;
                                frm.intIsOutputVoucherTransferInKim = 1;
                                //frm.DataTableSouce = dtData;
                                frm.DataTableSouce = dtbRes;
                                frm.ShowDialog(this);
                            }
                        }
                        else
                        {
                            MessageBox.Show(this, "Kho xuất và kho nhập khác chi nhánh.\r\n Bạn không thể in mẫu phiếu xuất kho kiêm vận chuyển nội bộ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    else
                    {
                        if (objFromStore.BranchID != objToStore.BranchID)
                        {
                            MessageBox.Show(this, "Kho xuất và kho nhập khác chi nhánh.\r\n Bạn không thể in mẫu phiếu xuất kho kiêm vận chuyển nội bộ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else
                        {

                            DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(strStoreChangeID);
                            int countRow = 0;
                            //dtData.AsEnumerable().OrderBy(p=>p["PRODUCTNAME"]);
                            DataTable dtbRes = dtData.Clone();
                            dtbRes.Columns.Add("PAGE", typeof(int));
                            int numPage = 1;
                            for (int i = 0; i < dtData.Rows.Count; i++)
                            {
                                string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                                Font font = new Font("Times New Roman", 10.0f);
                                List<string> productNameList = new List<string>();
                                int rowProductName = GetNumOfLines2(strProductName, 245.8f, new Font(font, FontStyle.Bold), ref productNameList);
                                countRow += rowProductName;
                                if (countRow > 12)
                                {
                                    //if (countRow - rowProductName < 12)
                                    //{
                                    //    int padding = 12 - (countRow - rowProductName);
                                    //    for (int j = 0; j < padding; j++)
                                    //    {
                                    //        //DataRow rowPadding = dtData.NewRow();
                                    //        // dtData.Rows.Add(rowPadding);
                                    //        DataRow rowPadding = dtbRes.NewRow();
                                    //        rowPadding["PAGE"] = numPage;
                                    //        dtbRes.Rows.Add(rowPadding);
                                    //    }
                                    //}
                                    numPage++;
                                    //MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    //return;
                                    countRow = rowProductName;

                                }
                                for (int j = 0; j < productNameList.Count; j++)
                                {
                                    DataRow row = dtbRes.NewRow();
                                    if (j == 0)
                                    {
                                        row["PRODUCTNAME"] = productNameList[j];
                                        row["QUANTITYUNITNAME"] = dtData.Rows[i]["QUANTITYUNITNAME"].ToString().Trim();
                                        row["STT"] = Convert.ToInt32(dtData.Rows[i]["STT"]);

                                        row["QUANTITY"] = Convert.ToInt32(dtData.Rows[i]["QUANTITY"]);
                                        row["PAGE"] = numPage;

                                    }
                                    else
                                    {
                                        row["PRODUCTNAME"] = productNameList[j];
                                        //row["STT"] = 0;
                                        row["PAGE"] = numPage;
                                    }
                                    dtbRes.Rows.Add(row);
                                }

                            }
                            //if (countRow < 12)
                            //{
                            //    int padding = 12 - countRow;
                            //    for (int i = 0; i < padding; i++)
                            //    {
                            //        //DataRow rowPadding = dtData.NewRow();
                            //        // dtData.Rows.Add(rowPadding);
                            //        DataRow rowPadding = dtbRes.NewRow();
                            //        rowPadding["PAGE"] = numPage;
                            //        dtbRes.Rows.Add(rowPadding);
                            //    }
                            //}

                            if (!string.IsNullOrEmpty(strStoreChangeID))
                            {
                                Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                                frm.StoreChangeID = strStoreChangeID;
                                frm.intIsOutputVoucherTransferInKim = 1;
                                //frm.DataTableSouce = dtData;
                                frm.DataTableSouce = dtbRes;
                                frm.ShowDialog(this);
                            }
                        }
                    }
                }
                else
                {
                    if (intPrintStoreChangeInputVoucher > 0 && !string.IsNullOrWhiteSpace(drFocus["InputVoucherID"].ToString().Trim()))
                    {
                        if (intPrintStoreChangeInputVoucher == intReportID)
                        {
                            Reports.Input.frmReportView frm = new Reports.Input.frmReportView();
                            frm.InputVoucherID = drFocus["InputVoucherID"].ToString().Trim();
                            frm.bolIsPriceHide = false;
                            frm.ShowDialog();
                        }
                        else
                        {
                            if (intPrintStoreChangeHandovers > 0)
                            {
                                if (intPrintStoreChangeHandovers == intReportID)
                                {
                                    mnuItemPrintStoreChangeHandOvers_Click(sender, e);
                                }
                                else
                                {
                                    ActionReport(strStoreChangeID, intReportID, strPrinterTypeID, 0, PLC.StoreChange.PLCStoreChange.ReportActionType.VIEW);
                                }
                            }
                            else
                            {
                                ActionReport(strStoreChangeID, intReportID, strPrinterTypeID, 0, PLC.StoreChange.PLCStoreChange.ReportActionType.VIEW);
                            }
                        }
                    }
                    else if (intPrintStoreChangeHandovers > 0)
                    {
                        if (intPrintStoreChangeHandovers == intReportID)
                        {
                            mnuItemPrintStoreChangeHandOvers_Click(sender, e);
                        }
                        else
                        {
                            ActionReport(strStoreChangeID, intReportID, strPrinterTypeID, 0, PLC.StoreChange.PLCStoreChange.ReportActionType.VIEW);
                        }
                    }
                    else
                    {
                        ActionReport(strStoreChangeID, intReportID, strPrinterTypeID, 0, PLC.StoreChange.PLCStoreChange.ReportActionType.VIEW);
                    }
                }
            }

        }

        private bool ActionReport(string strStoreChangeID, int intReportID, String strPrinterTypeID, int intNumberOfCopy,
            PLC.StoreChange.PLCStoreChange.ReportActionType enuReportActionType, bool bolIsShowOnly = false)
        {
            ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
            if (objReport == null)
            {
                return false;
            }
            objReport.PrinterTypeID = "CommonPrinter";
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("STORECHANGEID", strStoreChangeID);
            htbParameterValue.Add("StoreChangeID", strStoreChangeID);
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
            htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
            htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
            htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
            bool bolPrintResult = false;
            objReport.NumberOfCopy = intNumberOfCopy;
            if (!string.IsNullOrWhiteSpace(strPrinterTypeID))
                objReport.PrinterTypeID = strPrinterTypeID;
            objReport.IsShowOnly = bolIsShowOnly;
            if (enuReportActionType == PLC.StoreChange.PLCStoreChange.ReportActionType.VIEW)
                bolPrintResult = ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            else
                bolPrintResult = ERP.Report.DUI.DUIReportDataSource.PrintReport(this, objReport, htbParameterValue);
            return bolPrintResult;
        }


        private void mnuItemConfigPrint_Click(object sender, EventArgs e)
        {
            //   frmConfigPrint frmConfigPrint = new frmConfigPrint();
            frmConfigPrintMaster frmConfigPrintMaster = new frmConfigPrintMaster();
            StoredChange_CurrentVoucher objStoredChange_CurrentVoucher = null;
            objStoredChange_CurrentVoucher = objPLCStoreChange.LoadInfoConfigPrint(SystemConfig.ConfigStoreID);

            //  frmConfigPrint.IsPrint = false;
            //if (objStoredChange_CurrentVoucher != null)
            //{
            //    // frmConfigPrint.IsEdit = true;
            //    if (MessageBox.Show("Kho đã tồn tại, bạn có muốn cập nhật lại!", "Cấu hình in kim PXK kiêm VCNB", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            //    {
            //        return;
            //    }
            //}
            // frmConfigPrint.StoredChange_CurrentVoucher = objStoredChange_CurrentVoucher;
            frmConfigPrintMaster.ShowDialog();
        }

        private bool CheckStoreChange(bool IsTransfered, bool IsReceivedTransfer, bool IsSignReceive, bool IsReceive)
        {
            if (objStoreChange == null)
                return false;
            DataTable dtbReport = objStoreChangeType.StoreChangeType_ReportDTB;
            DataRow dtRow = dtbReport.Select("ISPRINTVAT = 1").FirstOrDefault();
            if (dtRow != null)
            {
                int intAUTOPRINTSTEPID = Convert.ToInt32(dtRow["AUTOPRINTSTEPID"]);
                if (intAUTOPRINTSTEPID < 1)
                    return false;
                if (intAUTOPRINTSTEPID == 1)
                    return true;
                if (intAUTOPRINTSTEPID == 2 && IsTransfered)
                    return true;
                if (intAUTOPRINTSTEPID == 3 && IsTransfered && IsReceivedTransfer)
                    return true;
                if (intAUTOPRINTSTEPID == 4 && IsTransfered && IsReceivedTransfer && IsSignReceive)
                    return true;
                if (intAUTOPRINTSTEPID == 5 && IsTransfered && IsReceivedTransfer && IsSignReceive && IsReceive)
                    return true;
            }
            return false;
        }

        private void mnuViewStoreChangeDetailReport_Click(object sender, EventArgs e)
        {
            if (selection == null || selection.SelectedCount == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn phiếu yêu cầu cần xem báo cáo!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string strStoreChangeIDList = string.Empty;
            for (int i = 0; i < selection.SelectedCount; i++)
            {
                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                strStoreChangeIDList += Convert.ToString(objRow["STORECHANGEID"]).Trim() + ",";
            }

            View_RPT_PM_STORECHANGE_DETAIL(strStoreChangeIDList);
        }

        private void View_RPT_PM_STORECHANGE_DETAIL(string strStoreChangeIDList)
        {
            if (!string.IsNullOrEmpty(strStoreChangeIDList))
            {
                frmStoreChangesDetail frm = new frmStoreChangesDetail(strStoreChangeIDList);
                frm.ShowDialog(this);
            }
        }

    }
    public class StoreChangeParam
    {
        private string strInvoiceId;

        public string InvoiceId
        {
            get { return strInvoiceId; }
            set { strInvoiceId = value; }
        }

        private string strStoreChangeId;

        public string StoreChangeId
        {
            get { return strStoreChangeId; }
            set { strStoreChangeId = value; }
        }
        private string strOutputVoucherId;

        public string OutputVoucherId
        {
            get { return strOutputVoucherId; }
            set { strOutputVoucherId = value; }
        }

        private int intFromStoreId;

        public int FromStoreId
        {
            get { return intFromStoreId; }
            set { intFromStoreId = value; }
        }

        private int intToStoreId;

        public int ToStoreId
        {
            get { return intToStoreId; }
            set { intToStoreId = value; }
        }
        private string strStoreChangeOrderId;

        public string StoreChangeOrderId
        {
            get { return strStoreChangeOrderId; }
            set { strStoreChangeOrderId = value; }
        }

        private string strInvoiceSymbol;

        public string InvoiceSymbol
        {
            get { return strInvoiceSymbol; }
            set { strInvoiceSymbol = value; }
        }
        private string strMauso;

        public string Mauso
        {
            get { return strMauso; }
            set { strMauso = value; }
        }
        private int intCurrentNumber;

        public int CurrentNumber
        {
            get { return intCurrentNumber; }
            set { intCurrentNumber = value; }
        }
        private int intInvoiceIDStart;

        public int InvoiceIDStart
        {
            get { return intInvoiceIDStart; }
            set { intInvoiceIDStart = value; }
        }

        private string strSTORECHANGECURVOUCHERID;

        public string STORECHANGECURVOUCHERID
        {
            get { return strSTORECHANGECURVOUCHERID; }
            set { strSTORECHANGECURVOUCHERID = value; }
        }

    }
}

