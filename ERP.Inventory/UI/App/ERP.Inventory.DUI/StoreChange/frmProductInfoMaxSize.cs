﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmProductInfoMaxSize : Form
    {
        public frmProductInfoMaxSize()
        {
            InitializeComponent();
        }
        #region Khai báo
        private DataTable dtbProductInfoMaxSize;
        private Boolean bolIsAccept;
        private Boolean bolIsReview = false;

        public Boolean IsReview
        {
            get { return bolIsReview; }
            set { bolIsReview = value; }
        }

        public Boolean IsAccept
        {
            get { return bolIsAccept; }
            set { bolIsAccept = value; }
        }

        public DataTable ProductInfoMaxSize
        {
            get { return dtbProductInfoMaxSize; }
            set { dtbProductInfoMaxSize = value; }
        }

        #endregion

        #region Phương thức
        private void FormatFlex()
        {
            flexData.Cols["ProductID"].Caption = "Mã sản phẩm";
            flexData.Cols["ProductName"].Caption = "Tên sản phẩm";
            flexData.Cols["Quantity"].Caption = "Số lượng";
            flexData.Cols["ProductLength"].Caption = "Chiều dài (m)";
            flexData.Cols["ProductWidth"].Caption = "Chiều rộng (m)";
            flexData.Cols["ProductHeight"].Caption = "Chiều cao (m)";

            flexData.Cols[0].Visible = false;
            for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
            {
                flexData.Cols[i].AllowEditing = false;
            }

            flexData.Cols["ProductID"].Width = 120;
            flexData.Cols["ProductName"].Width = 160;
            flexData.Cols["Quantity"].Width = 80;
            flexData.Cols["ProductLength"].Width = 100;
            flexData.Cols["ProductWidth"].Width = 110;
            flexData.Cols["ProductHeight"].Width = 100;

            #region Định dạng Style cho lưới
            Library.AppCore.LoadControls.C1FlexGridObject.SetBackColor(flexData, SystemColors.Info);
            C1.Win.C1FlexGrid.CellStyle style = flexData.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexData.GetCellRange(0, 0, 0, flexData.Cols.Count - 1);
            range.Style = style;
            flexData.Rows[0].Height = 35;
            #endregion
        }
        #endregion
        private void frmProductInfoMaxSize_Load(object sender, EventArgs e)
        {
            DataTable dtbData = new DataTable();
            dtbData = dtbProductInfoMaxSize.Clone();
            dtbData.PrimaryKey = new DataColumn[] { dtbData.Columns["ProductID"] };
            int intMaxLength = Convert.ToInt32(dtbProductInfoMaxSize.Compute("Max(ProductLength)", string.Empty));
            int intMaxWidth = Convert.ToInt32(dtbProductInfoMaxSize.Compute("Max(ProductWidth)", string.Empty));
            int intMaxHeight = Convert.ToInt32(dtbProductInfoMaxSize.Compute("Max(ProductHeight)", string.Empty));
            lbMaxLength.Value = intMaxLength;
            lbMaxWidth.Value = intMaxWidth;
            lbMaxHeight.Value = intMaxHeight;
            foreach (DataRow row in dtbProductInfoMaxSize.Rows)
            {
                if (!Convert.IsDBNull(row["ProductLength"]) && Convert.ToInt32(row["ProductLength"]) == intMaxLength)
                {
                    if (!dtbData.Rows.Contains(row["ProductID"]))
                    {
                        dtbData.ImportRow(row);
                        intMaxLength = -1;
                    }
                }
                if (!Convert.IsDBNull(row["ProductWidth"]) && Convert.ToInt32(row["ProductWidth"]) == intMaxWidth)
                {
                    if (!dtbData.Rows.Contains(row["ProductID"]))
                    {
                        dtbData.ImportRow(row);
                        intMaxWidth = -1;
                    }
                }
                if (!Convert.IsDBNull(row["ProductHeight"]) && Convert.ToInt32(row["ProductHeight"]) == intMaxHeight)
                {
                    if (!dtbData.Rows.Contains(row["ProductID"]))
                    {
                        dtbData.ImportRow(row);
                        intMaxHeight = -1;
                    }
                }
                if (dtbData.Rows.Count == 3)
                    break;
            }


            for (int i = 0; i < dtbData.Rows.Count; i++)
            {
                int intRowMaxLength = Convert.ToInt32(dtbData.Rows[i]["ProductLength"]);
                int intRowMaxWidth = Convert.ToInt32(dtbData.Rows[i]["ProductWidth"]);
                int intRowMaxHeight = Convert.ToInt32(dtbData.Rows[i]["ProductHeight"]);
                if (Convert.ToInt32(lbMaxLength.Value) == intRowMaxLength && Convert.ToInt32(lbMaxWidth.Value) == intRowMaxWidth
                    && Convert.ToInt32(lbMaxHeight.Value) == intRowMaxHeight)
                {
                    DataRow rowMax = dtbData.NewRow();
                    rowMax.ItemArray = dtbData.Rows[i].ItemArray;
                    object[] arrItemArray = dtbData.Rows[i].ItemArray;
                    dtbData.Clear();
                    dtbData.Rows.Add(arrItemArray);
                    break;
                }
            }

            flexData.DataSource = dtbData;
            FormatFlex();

            if (bolIsReview)
                btnAccept.Visible = false;
            else
                btnAccept.Visible = true;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.IsAccept = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.IsAccept = false;
            this.Close();
        }
    }
}
