﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmIMEINote : Form
    {
        public frmIMEINote()
        {
            InitializeComponent();
        }

        private void btnChapNhan_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        public string IMEI_NOTE
        {
            get { return txtNote.Text; }
        }
    }
}
