﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERP.Inventory.PLC.StoreChange;
using Library.AppCore.LoadControls;
using Library.AppCore;
using ERP.Inventory.PLC.StoreChange.WSStoreChange;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmConfigPrint : Form
    {
        private PLCStoreChange objPLCStoreChange = new PLCStoreChange();
        private ResultMessage objResultMessage = new ResultMessage();
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;
        // private DataTable dtbData = null;
        decimal decInvoiceStart = 0;
        decimal decInvoiceEnd = 0;
        private object[] objKeywords = null;

        private bool bolIsEdit = false;
        public bool IsEdit
        {
            get { return bolIsEdit; }
            set { bolIsEdit = value; }
        }
        private bool bolIsOk = false;

        public bool IsOk
        {
            get { return bolIsOk; }
            set { bolIsOk = value; }
        }
        private decimal intInvoiced = 0;

        public decimal Invoiced
        {
            get { return intInvoiced; }
            set { intInvoiced = value; }
        }


        private int intTag;

        public int Tag
        {
            get { return intTag; }
            set { intTag = value; }
        }

        private string strOutputVoucherID = string.Empty;

        public string OutputVoucherID
        {
            get { return strOutputVoucherID; }
            set { strOutputVoucherID = value; }
        }
        private string strStoreChangeID = string.Empty;

        public string StoreChangeID
        {
            get { return strStoreChangeID; }
            set { strStoreChangeID = value; }
        }

        private StoreChangeParam objStoreChangeParam = null;

        public StoreChangeParam StoreChangeParam
        {
            get { return objStoreChangeParam; }
            set { objStoreChangeParam = value; }
        }
        private int intNumPage;

        public int NumPage
        {
            get { return intNumPage; }
            set { intNumPage = value; }
        }




        //private StoredChange_CurrentVoucher objStoredChange_CurrentVoucher = null;

        //public StoredChange_CurrentVoucher StoredChange_CurrentVoucher
        //{
        //    get { return objStoredChange_CurrentVoucher; }
        //    set { objStoredChange_CurrentVoucher = value; }
        //}

        private bool bolIsPrint = false;

        public bool IsPrint
        {
            get { return bolIsPrint; }
            set { bolIsPrint = value; }
        }

        private int intFromStoreId;



        private bool bolIsRePrint = false;

        public bool IsRePrint
        {
            get { return bolIsRePrint; }
            set { bolIsRePrint = value; }
        }
        private bool bolIsUpdate;

        public bool IsUpdate
        {
            get { return bolIsUpdate; }
            set { bolIsUpdate = value; }
        }
        private DataTable dtbData = null;

        public DataTable DtbData
        {
            get { return dtbData; }
            set { dtbData = value; }
        }



        public frmConfigPrint()
        {
            InitializeComponent();
        }
        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(txtMauSo.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Chưa nhập mẫu số");
                txtMauSo.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtCurrentNumber.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Chưa nhập số hiện tại");
                txtCurrentNumber.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtSymbol.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Chưa nhập ký hiệu");
                txtSymbol.Focus();
                return false;
            }
            if (bolIsPrint)
            {
                DataRowView drStockStatus = (DataRowView)cboMaThietLap.SelectedItem;
                if (Convert.ToInt32(drStockStatus["INVOICEIDSTART"]) > Convert.ToInt32(txtCurrentNumber.Text))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Số hóa đơn mới phải lớn hơn " + ERP.SalesAndServices.Payment.DUI.DUIPayment_Common.FormatInvoiceID(Convert.ToInt32(drStockStatus["INVOICEIDSTART"])));
                    txtCurrentNumber.Focus();
                    return false;
                }
            }
            if (bolIsRePrint)
            {
                if (objStoreChangeParam.InvoiceIDStart > Convert.ToInt32(txtCurrentNumber.Text))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Số hóa đơn mới phải lớn hơn " + ERP.SalesAndServices.Payment.DUI.DUIPayment_Common.FormatInvoiceID(objStoreChangeParam.InvoiceIDStart));
                    txtCurrentNumber.Focus();
                    return false;
                }
            }
            if (decInvoiceEnd < Convert.ToInt32(txtCurrentNumber.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Số phiếu phải nhỏ hơn " + ERP.SalesAndServices.Payment.DUI.DUIPayment_Common.FormatInvoiceID(Convert.ToInt32(decInvoiceEnd)));
                txtCurrentNumber.Focus();
                return false;
            }
            if (bolIsEdit)
            {

            }
            return true;
        }
        public bool UpdateData()
        {
            string strMessage = bolIsEdit ? "Cập nhật" : "Thêm";
            try
            {
                if (!ValidateData())
                    return false;
                StoredChange_CurrentVoucher objStoredChange_CurrentVoucher = new StoredChange_CurrentVoucher();
                objStoredChange_CurrentVoucher.StoreID = SystemConfig.ConfigStoreID;
                objStoredChange_CurrentVoucher.Denominator = txtMauSo.Text;
                objStoredChange_CurrentVoucher.InvoiceSymbol = txtSymbol.Text;
                objStoredChange_CurrentVoucher.InvoiceID = Convert.ToDecimal(txtCurrentNumber.Text);
                if (!bolIsEdit)
                    objResultMessage = objPLCStoreChange.InsertConfigPrint(objStoredChange_CurrentVoucher);
                else
                    objResultMessage = objPLCStoreChange.UpdateConfigPrint(objStoredChange_CurrentVoucher);
                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    return false;
                }

                MessageBoxObject.ShowInfoMessage(this, strMessage + " cấu hình in kim PXK kiêm VCNB thành công");

            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi " + strMessage.ToLower() + " cấu hình in kim PXK kiêm VCNB", objExce.ToString(), this.Name + " -> UpdateData", Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi " + strMessage.ToLower() + " cấu hình in kim PXK kiêm VCNB");
                return false;
            }
            return true;
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!UpdateData())
                return;
            Close();
        }
        private void InitControl()
        {
            //  cboMaThietLap.read
            cboMaThietLap.DataSource = dtbData;
            cboMaThietLap.ValueMember = "STORECHANGECURVOUCHERID";
            cboMaThietLap.DisplayMember = "STORECHANGECURVOUCHERID";
            cboMaThietLap.SelectedIndex = 0;
            cboMaThietLap_SelectedIndexChanged(null, null);
        }
        private void frmConfigPrint_Load(object sender, EventArgs e)
        {
            InitControl();
        }
        private bool LoadData()
        {
            objKeywords = new object[] { "@StoreID", SystemConfig.ConfigStoreID,
                                         "@UserName",SystemConfig.objSessionUser.UserName,
                                         "@STORECHANGECURVOUCHERID",string.Empty,
                                         "@IsGetAll",false};
            dtbData = objPLCStoreChange.SearchDataConfigPrint(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            if (dtbData == null || dtbData.Rows.Count < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Chưa cấu hình in kim. Vui lòng kiểm tra lại!");
                return false;
            }
            return true;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;

            if (!bolIsRePrint && UpdateDataVoucher())
            {
                bolIsOk = true;
            }
            else if (bolIsRePrint)
            {
                DataTable dtbSource = null;
                int intNumPage = 0;
                CreateDataSource(ref dtbSource, ref intNumPage);
                this.intNumPage = intNumPage;
                if (UpdateDataVoucher())
                {
                    if (!GetOutputVouchertranferData(1, dtbSource, intNumPage))
                        return;
                    bolIsUpdate = true;
                }
            }
            this.Close();

        }

        private void txtCurrentNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (/*txtCurrentNumber.Text.Length > 6 &&*/ !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
            if (txtCurrentNumber.Text.Length > 6 && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }


        }

        private bool UpdateDataVoucher()
        {
            try
            {

                PLC.StoreChange.PLCStoreChange objPLCStoreChange = new PLC.StoreChange.PLCStoreChange();
                ResultMessage objResultMessage = new ResultMessage();
                StoreChange_CurrentVoucherUpdate objStoreChange_CurrentVoucherUpdate = new StoreChange_CurrentVoucherUpdate();
                objStoreChange_CurrentVoucherUpdate.StoreChangeId = strStoreChangeID;
                objStoreChange_CurrentVoucherUpdate.OutputVoucherID = strOutputVoucherID;
                objStoreChange_CurrentVoucherUpdate.Denominator = txtMauSo.Text;
                objStoreChange_CurrentVoucherUpdate.InvoiceSymbol = txtSymbol.Text;
                objStoreChange_CurrentVoucherUpdate.StoreId = SystemConfig.ConfigStoreID;
                objStoreChange_CurrentVoucherUpdate.PrintedReason = string.Empty;
                objStoreChange_CurrentVoucherUpdate.InvoiceId = ERP.SalesAndServices.Payment.DUI.DUIPayment_Common.FormatInvoiceID(Convert.ToInt32(txtCurrentNumber.Text) + intNumPage - 1);
                objStoreChange_CurrentVoucherUpdate.IsReNewPrint = false;
                objStoreChange_CurrentVoucherUpdate.IsReOldPrint = false;
                objStoreChange_CurrentVoucherUpdate.InvoiceIdNew = ERP.SalesAndServices.Payment.DUI.DUIPayment_Common.FormatInvoiceID(Convert.ToInt32(txtCurrentNumber.Text) + intNumPage - 1);
                objStoreChange_CurrentVoucherUpdate.STORECHANGECURVOUCHERID = cboMaThietLap.SelectedValue.ToString().Trim();
                bool bolIsDuplicate = false;
                objResultMessage = objPLCStoreChange.UpdateStoreChange_Voucher(objStoreChange_CurrentVoucherUpdate, ref bolIsDuplicate);

                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    return false;
                }
                if (bolIsDuplicate)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Số phiếu đã tồn tại, vui lòng kiểm tra lại!");
                    return false;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi cập nhật phiếu xuất từ chuyển kho", objExce.ToString(), this.Name + " -> UpdateDataVoucher", Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi cập nhật phiếu xuất từ chuyển kho");
                return false;
            }
            return true;
        }

        private void btnUndoPrint_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void CreateDataSource(ref DataTable dtbRes, ref int numPage)
        {
            PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVAT_InvoiceMain = null;
            if (objStoreChangeParam == null)
            {
                return;
            }
            PLC.PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder = new PLC.StoreChange.PLCStoreChangeOrder().LoadInfo(objStoreChangeParam.StoreChangeOrderId, -1);
            DataTable dtbInvoiceId = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_INVOICE_SELBYOUTPUTID", new object[] { "@OUTPUTVOUCHERID", objStoreChangeParam.OutputVoucherId });
            string strVATINVOICEID = string.Empty;
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                SystemErrorWS.Insert(SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->barItem_ItemClick");
                return;
            }
            if (dtbInvoiceId != null && dtbInvoiceId.Rows.Count > 0)
            {
                strVATINVOICEID = dtbInvoiceId.Rows[0][0].ToString();
                new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().LoadInfo(ref objVAT_InvoiceMain, strVATINVOICEID);
            }
            ERP.MasterData.PLC.MD.WSStore.Store objFromStore = null;
            ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
            new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objFromStore, objStoreChangeParam.FromStoreId);
            if (objFromStore == null)
            {
                MessageBox.Show(this, "Không tìm thấy kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, objStoreChangeParam.ToStoreId);
            if (objToStore == null)
            {
                MessageBox.Show(this, "Không tìm thấy kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            objStoreChangeOrderType = new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfoFromCache(objStoreChangeOrder.StoreChangeOrderTypeID);
            if (string.IsNullOrWhiteSpace(objStoreChangeParam.StoreChangeOrderId))
            {
                #region Không được xóa
                if (objFromStore.BranchID == objToStore.BranchID)
                {
                    DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(objStoreChangeParam.StoreChangeId);
                    int countRow = 0;
                    //dtData.AsEnumerable().OrderBy(p=>p["PRODUCTNAME"]);
                    dtbRes = dtData.Clone();
                    dtbRes.Columns.Add("PAGE", typeof(int));
                    numPage = 1;
                    for (int i = 0; i < dtData.Rows.Count; i++)
                    {
                        string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                        Font font = new Font("Times New Roman", 10.0f);
                        List<string> productNameList = new List<string>();
                        int rowProductName = GetNumOfLines2(strProductName, 248.8f, new Font(font, FontStyle.Bold), ref productNameList);
                        countRow += rowProductName;
                        if (countRow > 12)
                        {
                            //if (countRow - rowProductName < 12)
                            //{
                            //    int padding = 12 - (countRow - rowProductName);
                            //    for (int j = 0; j < padding; j++)
                            //    {
                            //        //DataRow rowPadding = dtData.NewRow();
                            //        // dtData.Rows.Add(rowPadding);
                            //        DataRow rowPadding = dtbRes.NewRow();
                            //        rowPadding["PAGE"] = numPage;
                            //        dtbRes.Rows.Add(rowPadding);
                            //    }
                            //}
                            numPage++;
                            //MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //return;
                            countRow = rowProductName;

                        }
                        for (int j = 0; j < productNameList.Count; j++)
                        {
                            DataRow row = dtbRes.NewRow();
                            if (j == 0)
                            {
                                row["PRODUCTNAME"] = productNameList[j];
                                row["QUANTITYUNITNAME"] = dtData.Rows[i]["QUANTITYUNITNAME"].ToString().Trim();
                                row["STT"] = Convert.ToInt32(dtData.Rows[i]["STT"]);

                                row["QUANTITY"] = Convert.ToInt32(dtData.Rows[i]["QUANTITY"]);
                                row["PAGE"] = numPage;

                            }
                            else
                            {
                                row["PRODUCTNAME"] = productNameList[j];
                                //row["STT"] = 0;
                                row["PAGE"] = numPage;
                            }
                            dtbRes.Rows.Add(row);
                        }
                    }
                    //if (countRow < 12)
                    //{
                    //    int padding = 12 - countRow;
                    //    for (int i = 0; i < padding; i++)
                    //    {
                    //        //DataRow rowPadding = dtData.NewRow();
                    //        // dtData.Rows.Add(rowPadding);
                    //        DataRow rowPadding = dtbRes.NewRow();
                    //        rowPadding["PAGE"] = numPage;
                    //        dtbRes.Rows.Add(rowPadding);
                    //    }
                    //}
                    #endregion
                    if (!string.IsNullOrEmpty(objStoreChangeParam.StoreChangeId))
                    {

                        // Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                        //  frm.StoreChangeID = strStoreChangeID;
                        //  frm.intIsOutputVoucherTransferInKim = 1;
                        //frm.DataTableSouce = dtData;
                        //  frm.DataTableSouce = dtbRes;
                        //   GetOutputVouchertranferData(1, dtbRes);
                        //  frm.ShowDialog(this);
                        // 
                    }
                }
            }
            else
            {
                
                if (intTag == 2)
                {
                    DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(strStoreChangeID);
                    int countRow = 0;
                    dtbRes = dtData.Clone();
                    dtbRes.Columns.Add("PAGE", typeof(int));
                    numPage = 1;
                    for (int i = 0; i < dtData.Rows.Count; i++)
                    {
                        string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                        Font font = new Font("Times New Roman", 10.0f);
                        List<string> productNameList = new List<string>();
                        int rowProductName = GetNumOfLines2(strProductName, 248.8f, new Font(font, FontStyle.Bold), ref productNameList);
                        countRow += rowProductName;
                        if (countRow > 12)
                        {
                            numPage++;
                            countRow = rowProductName;
                        }
                        for (int j = 0; j < productNameList.Count; j++)
                        {
                            DataRow row = dtbRes.NewRow();
                            if (j == 0)
                            {
                                row["PRODUCTNAME"] = productNameList[j];
                                row["QUANTITYUNITNAME"] = dtData.Rows[i]["QUANTITYUNITNAME"].ToString().Trim();
                                row["STT"] = Convert.ToInt32(dtData.Rows[i]["STT"]);
                                row["QUANTITY"] = Convert.ToInt32(dtData.Rows[i]["QUANTITY"]);
                                row["PAGE"] = numPage;
                            }
                            else
                            {
                                row["PRODUCTNAME"] = productNameList[j];
                                row["PAGE"] = numPage;
                            }
                            dtbRes.Rows.Add(row);
                        }
                    }
                }
                else if (objFromStore.BranchID != objToStore.BranchID)
                {
                    if (objVAT_InvoiceMain != null)
                    {
                        if (objVAT_InvoiceMain.VATInvoicePrint != null && objVAT_InvoiceMain.VATInvoicePrint.Count() > 0)
                        {
                            PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                            objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, objVAT_InvoiceMain, true, false, "", objStoreChangeOrderType.VATPagesPrint, false, true);
                        }
                        else
                        {
                            PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                            objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, objVAT_InvoiceMain, false, false, "", objStoreChangeOrderType.VATPagesPrint, true, true);
                        }
                    }
                    #region In PXK kiêm VNCB nếu có check IsPrintOutputVoucher ở StoreChangeOrderType -- Tấn Tài -- 2018.01.10

                    if (objStoreChangeOrderType.IsPrintOutputVoucher)
                    {
                        DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(strStoreChangeID);
                        int countRow = 0;
                        dtbRes = dtData.Clone();
                        dtbRes.Columns.Add("PAGE", typeof(int));
                         numPage = 1;
                        for (int i = 0; i < dtData.Rows.Count; i++)
                        {
                            string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                            Font font = new Font("Times New Roman", 10.0f);
                            List<string> productNameList = new List<string>();
                            int rowProductName = GetNumOfLines2(strProductName, 248.8f, new Font(font, FontStyle.Bold), ref productNameList);
                            countRow += rowProductName;
                            if (countRow > 12)
                            {
                                numPage++;
                                countRow = rowProductName;
                            }
                            for (int j = 0; j < productNameList.Count; j++)
                            {
                                DataRow row = dtbRes.NewRow();
                                if (j == 0)
                                {
                                    row["PRODUCTNAME"] = productNameList[j];
                                    row["QUANTITYUNITNAME"] = dtData.Rows[i]["QUANTITYUNITNAME"].ToString().Trim();
                                    row["STT"] = Convert.ToInt32(dtData.Rows[i]["STT"]);
                                    row["QUANTITY"] = Convert.ToInt32(dtData.Rows[i]["QUANTITY"]);
                                    row["PAGE"] = numPage;
                                }
                                else
                                {
                                    row["PRODUCTNAME"] = productNameList[j];
                                    row["PAGE"] = numPage;
                                }
                                dtbRes.Rows.Add(row);
                            }
                        }

                        //if (!string.IsNullOrEmpty(strStoreChangeID))
                        //{
                        //    if (!ShowConfigPrint(numPage))
                        //        return false;
                        //    Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                        //    frm.StoreChangeID = strStoreChangeID;
                        //    frm.intIsOutputVoucherTransferInKim = 1;
                        //    frm.DataTableSouce = dtbRes;
                        //    GetOutputVouchertranferData(1, dtbRes, numPage);
                        //}
                    }

                    #endregion
                }
                else
                {
                    if (objVAT_InvoiceMain != null)
                    {
                        if (MessageBox.Show(this, "Loại yêu cầu này có thiết lập tạo hóa đơn. Bạn có muốn xuất hóa đơn không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            if (objVAT_InvoiceMain.VATInvoicePrint != null && objVAT_InvoiceMain.VATInvoicePrint.Count() > 0)
                            {
                                PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                                objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, objVAT_InvoiceMain, true, false, "", objStoreChangeOrderType.VATPagesPrint, false, true);
                            }
                            else
                            {
                                PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                                objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, objVAT_InvoiceMain, false, false, "", objStoreChangeOrderType.VATPagesPrint, true, true);
                            }
                        }
                        else
                        {
                            DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(objStoreChangeParam.StoreChangeId);
                            int countRow = 0;
                            //dtData.AsEnumerable().OrderBy(p=>p["PRODUCTNAME"]);
                            dtbRes = dtData.Clone();
                            dtbRes.Columns.Add("PAGE", typeof(int));
                            numPage = 1;
                            for (int i = 0; i < dtData.Rows.Count; i++)
                            {
                                string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                                Font font = new Font("Times New Roman", 10.0f);
                                List<string> productNameList = new List<string>();
                                int rowProductName = GetNumOfLines2(strProductName, 248.8f, new Font(font, FontStyle.Bold), ref productNameList);
                                countRow += rowProductName;
                                if (countRow > 12)
                                {
                                    //if (countRow - rowProductName < 12)
                                    //{
                                    //    int padding = 12 - (countRow - rowProductName);
                                    //    for (int j = 0; j < padding; j++)
                                    //    {
                                    //        //DataRow rowPadding = dtData.NewRow();
                                    //        // dtData.Rows.Add(rowPadding);
                                    //        DataRow rowPadding = dtbRes.NewRow();
                                    //        rowPadding["PAGE"] = numPage;
                                    //        dtbRes.Rows.Add(rowPadding);
                                    //    }
                                    //}
                                    numPage++;
                                    //MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    //return;
                                    countRow = rowProductName;

                                }
                                for (int j = 0; j < productNameList.Count; j++)
                                {
                                    DataRow row = dtbRes.NewRow();
                                    if (j == 0)
                                    {
                                        row["PRODUCTNAME"] = productNameList[j];
                                        row["QUANTITYUNITNAME"] = dtData.Rows[i]["QUANTITYUNITNAME"].ToString().Trim();
                                        row["STT"] = Convert.ToInt32(dtData.Rows[i]["STT"]);

                                        row["QUANTITY"] = Convert.ToInt32(dtData.Rows[i]["QUANTITY"]);
                                        row["PAGE"] = numPage;

                                    }
                                    else
                                    {
                                        row["PRODUCTNAME"] = productNameList[j];
                                        //row["STT"] = 0;
                                        row["PAGE"] = numPage;
                                    }
                                    dtbRes.Rows.Add(row);
                                }
                            }
                            //if (countRow < 12)
                            //{
                            //    int padding = 12 - countRow;
                            //    for (int i = 0; i < padding; i++)
                            //    {
                            //        //DataRow rowPadding = dtData.NewRow();
                            //        // dtData.Rows.Add(rowPadding);
                            //        DataRow rowPadding = dtbRes.NewRow();
                            //        rowPadding["PAGE"] = numPage;
                            //        dtbRes.Rows.Add(rowPadding);
                            //    }
                            //}

                            if (!string.IsNullOrEmpty(objStoreChangeParam.StoreChangeId))
                            {
                                //Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                                //frm.StoreChangeID = strStoreChangeID;
                                //frm.intIsOutputVoucherTransferInKim = 1;
                                ////frm.DataTableSouce = dtData;
                                //frm.DataTableSouce = dtbRes;
                                //GetOutputVouchertranferData(1, dtbRes);
                                ////  frm.ShowDialog(this);
                            }
                        }
                    }
                    else
                    {
                        DataTable dtData = new PLC.StoreChange.PLCStoreChange().GetStoreChangeRPT_GetData(objStoreChangeParam.StoreChangeId);
                        int countRow = 0;
                        //dtData.AsEnumerable().OrderBy(p=>p["PRODUCTNAME"]);
                        dtbRes = dtData.Clone();
                        dtbRes.Columns.Add("PAGE", typeof(int));
                        numPage = 1;
                        for (int i = 0; i < dtData.Rows.Count; i++)
                        {
                            string strProductName = dtData.Rows[i]["PRODUCTNAME"].ToString().Trim();
                            Font font = new Font("Times New Roman", 10.0f);
                            List<string> productNameList = new List<string>();
                            int rowProductName = GetNumOfLines2(strProductName, 248.8f, new Font(font, FontStyle.Bold), ref productNameList);
                            countRow += rowProductName;
                            if (countRow > 12)
                            {
                                //if (countRow - rowProductName < 12)
                                //{
                                //    int padding = 12 - (countRow - rowProductName);
                                //    for (int j = 0; j < padding; j++)
                                //    {
                                //        //DataRow rowPadding = dtData.NewRow();
                                //        // dtData.Rows.Add(rowPadding);
                                //        DataRow rowPadding = dtbRes.NewRow();
                                //        rowPadding["PAGE"] = numPage;
                                //        dtbRes.Rows.Add(rowPadding);
                                //    }
                                //}
                                numPage++;
                                //MessageBox.Show(this, "Số sản phẩm vượt quá số dòng cho phép trên mẫu in!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                //return;
                                countRow = rowProductName;

                            }
                            for (int j = 0; j < productNameList.Count; j++)
                            {
                                DataRow row = dtbRes.NewRow();
                                if (j == 0)
                                {
                                    row["PRODUCTNAME"] = productNameList[j];
                                    row["QUANTITYUNITNAME"] = dtData.Rows[i]["QUANTITYUNITNAME"].ToString().Trim();
                                    row["STT"] = Convert.ToInt32(dtData.Rows[i]["STT"]);

                                    row["QUANTITY"] = Convert.ToInt32(dtData.Rows[i]["QUANTITY"]);
                                    row["PAGE"] = numPage;

                                }
                                else
                                {
                                    row["PRODUCTNAME"] = productNameList[j];
                                    //row["STT"] = 0;
                                    row["PAGE"] = numPage;
                                }
                                dtbRes.Rows.Add(row);
                            }
                        }
                        //if (countRow < 12)
                        //{
                        //    int padding = 12 - countRow;
                        //    for (int i = 0; i < padding; i++)
                        //    {
                        //        //DataRow rowPadding = dtData.NewRow();
                        //        // dtData.Rows.Add(rowPadding);
                        //        DataRow rowPadding = dtbRes.NewRow();
                        //        rowPadding["PAGE"] = numPage;
                        //        dtbRes.Rows.Add(rowPadding);
                        //    }
                        //}

                        if (!string.IsNullOrEmpty(strStoreChangeID))
                        {
                            //Reports.StoreChange.frmReportView frm = new Reports.StoreChange.frmReportView();
                            //frm.StoreChangeID = strStoreChangeID;
                            //frm.intIsOutputVoucherTransferInKim = 1;
                            ////frm.DataTableSouce = dtData;
                            //GetOutputVouchertranferData(1, dtbRes);
                            //frm.DataTableSouce = dtbRes;
                            ////   frm.ShowDialog(this);
                        }
                    }
                }
            }
        }
        private int GetNumOfLines2(string multiPageString, float wrapWidth, Font fnt, ref List<string> lstRes)
        {
            //wrapWidth += 5;
            StringFormat sfFmt = StringFormat.GenericDefault;
            int intNumLines = 0;
            int charactersFitted = 0;
            int linesFilled = 0;
            int linesFilledWord = 0;
            string[] words = multiPageString.Split(' ');
            string text = string.Empty;

            for (int i = 0; i < words.Length; i++)
            {
                using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
                {
                    SizeF size = g.MeasureString(words[i], fnt, new SizeF(wrapWidth - 6.5f, 150), null, out charactersFitted, out linesFilledWord);
                }
                if (linesFilledWord > 1)
                {
                    for (int j = 1; j < words[i].Length; j++)
                    {
                        string strChar = words[i].Substring(0, words[i].Length - j);
                        int linesFilledChar = 0;
                        using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
                        {
                            SizeF size = g.MeasureString(words[i], fnt, new SizeF(wrapWidth, 150), null, out charactersFitted, out linesFilledChar);
                        }
                        if (linesFilledChar <= 1)
                        {
                            if (!string.IsNullOrEmpty(text))
                            {
                                lstRes.Add(text);
                                intNumLines++;
                            }
                            lstRes.Add(strChar);
                            text = words[i].Substring(words[i].Length - j, j);
                            //lstRes.Add(words[i].Substring(words[i].Length - i, i));
                            intNumLines++;
                            break;
                        }
                    }
                    if (i == words.Length - 1)
                    {
                        lstRes.Add(text);
                        intNumLines++;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(text)) text += words[i];
                    else
                    {
                        using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
                        {
                            SizeF size = g.MeasureString(text + " " + words[i], fnt, new SizeF(wrapWidth, 150), null, out charactersFitted, out linesFilled);
                        }
                        if (linesFilled > 1)
                        {
                            lstRes.Add(text);
                            text = words[i];
                            intNumLines++;

                        }
                        else
                        {
                            text = text + " " + words[i];
                        }
                        //if (i == words.Length - 1)
                        //{
                        //    lstRes.Add(text);
                        //    intNumLines++;
                        //}

                    }
                    if (i == words.Length - 1)
                    {
                        lstRes.Add(text);
                        intNumLines++;
                    }
                }


            }
            intNumLines = lstRes.Count;
            return intNumLines;
            //using (Graphics g = Graphics.FromImage(new Bitmap(1, 1)))
            //{
            //    SizeF size = g.MeasureString(multiPageString, fnt, new SizeF(wrapWidth,150),null,out charactersFitted,out linesFilled);
            //    return linesFilled;
            //    //int iHeight = (int)(Math.Ceiling(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height));
            //    //int iOneLineHeight = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Height;
            //    //int oneChar = (int)g.MeasureString("W", fnt, wrapWidth, sfFmt).Width;
            //    //int oneLine = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Width;
            //    //int oneLineHeight = (int)g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height;
            //    //return (int)(g.MeasureString(multiPageString, fnt, wrapWidth, sfFmt).Height / g.MeasureString("W", fnt, wrapWidth, sfFmt).Height);
            //}
        }

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
        private bool GetOutputVouchertranferData(int Lien, DataTable DataTableSouce, int intNumPage)
        {
            try
            {
                CrystalDecisions.CrystalReports.Engine.ReportDocument crDoc = null;
                //  this.Text = "In phiếu xuất kho " + objStoreChangeParam.StoreChangeId;
                //    crystalReportViewer1.ReportSource = null;
                // crystalReportViewer1.Refresh();
                String strPathReport = "\\StoreChange\\rptOutputVoucherTranfer.rpt";
                if (crDoc != null)
                {
                    crDoc.Close();
                    crDoc.Dispose();
                }
                crDoc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                crDoc.Load(DUIInventory_Globals.ReportPath + strPathReport);

                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }



                crDoc.SetDataSource(DataTableSouce);
                DataTable dtInfo = new PLC.StoreChange.PLCStoreChange().Report_GetOutputVoucherTransferData(objStoreChangeParam.StoreChangeId);
                DateTime _datetime = Convert.ToDateTime(dtInfo.Rows[0]["STORECHANGEDATE"].ToString());
                DateTime _OutputDate = Convert.ToDateTime(dtInfo.Rows[0]["OUTPUTDATE"].ToString());
                if (dtInfo != null && dtInfo.Rows.Count > 0)
                {
                    crDoc.SetParameterValue("DD", _datetime.ToString("dd"));
                    crDoc.SetParameterValue("MM", _datetime.ToString("MM"));
                    crDoc.SetParameterValue("YY", _datetime.ToString("yyyy").Substring(2, 2).ToString());

                    crDoc.SetParameterValue("COMPANYNAME", SystemConfig.DefaultCompany.CompanyName);
                    crDoc.SetParameterValue("COMPANYADDRESS", SystemConfig.DefaultCompany.Address);
                    crDoc.SetParameterValue("COMPANYTAX", SystemConfig.DefaultCompany.TaxID);
                    crDoc.SetParameterValue("TOSTORE", dtInfo.Rows[0]["TOSTORE"].ToString());
                    crDoc.SetParameterValue("FROMSTORE", dtInfo.Rows[0]["FROMSTORE"].ToString());
                    crDoc.SetParameterValue("TransferedUserFullName", dtInfo.Rows[0]["TransferedUserFullName"].ToString());
                    crDoc.SetParameterValue("TransportTypeName", dtInfo.Rows[0]["TransportTypeName"].ToString());
                    crDoc.SetParameterValue("INVOICEID", dtInfo.Rows[0]["INVOICEID"].ToString());
                    crDoc.SetParameterValue("STORECHANGETYPENAME", dtInfo.Rows[0]["STORECHANGETYPENAME"].ToString());
                    crDoc.SetParameterValue("STORECHANGEDATETEXT", dtInfo.Rows[0]["STORECHANGEDATETEXT"].ToString());
                    crDoc.SetParameterValue("TransferedDate", dtInfo.Rows[0]["TransferedDate"].ToString());
                    crDoc.SetParameterValue("StoreChangeUserFullName", dtInfo.Rows[0]["StoreChangeUserFullName"].ToString());
                    crDoc.SetParameterValue("BRANCHNAME", dtInfo.Rows[0]["BRANCHNAME"].ToString());
                    crDoc.SetParameterValue("BRANCHADDRESS", dtInfo.Rows[0]["BRANCHADDRESS"].ToString());
                    crDoc.SetParameterValue("BRANCHTAX", dtInfo.Rows[0]["BRANCHTAX"].ToString());
                    crDoc.SetParameterValue("ORDERID", dtInfo.Rows[0]["ORDERID"].ToString());

                    crDoc.SetParameterValue("OutDD", _OutputDate.ToString("dd"));
                    crDoc.SetParameterValue("OutMM", _OutputDate.ToString("MM"));
                    crDoc.SetParameterValue("OutYY", _OutputDate.ToString("yyyy"));

                }
                else
                {
                    crDoc.SetParameterValue("DD", _datetime.ToString("dd"));
                    crDoc.SetParameterValue("MM", _datetime.ToString("MM"));
                    crDoc.SetParameterValue("YY", _datetime.ToString("yyyy").Substring(2, 2).ToString());
                    crDoc.SetParameterValue("COMPANYNAME", string.Empty);
                    crDoc.SetParameterValue("COMPANYADDRESS", string.Empty);
                    crDoc.SetParameterValue("COMPANYTAX", string.Empty);
                    crDoc.SetParameterValue("TOSTORE", string.Empty);
                    crDoc.SetParameterValue("FROMSTORE", string.Empty);
                    crDoc.SetParameterValue("TransferedUserFullName", string.Empty);
                    crDoc.SetParameterValue("TransportTypeName", string.Empty);
                    crDoc.SetParameterValue("INVOICEID", string.Empty);
                    crDoc.SetParameterValue("STORECHANGETYPENAME", string.Empty);
                    crDoc.SetParameterValue("STORECHANGEDATETEXT", string.Empty);
                    crDoc.SetParameterValue("TransferedDate", string.Empty);
                    crDoc.SetParameterValue("StoreChangeUserFullName", string.Empty);
                    crDoc.SetParameterValue("BRANCHNAME", string.Empty);
                    crDoc.SetParameterValue("BRANCHADDRESS", string.Empty);
                    crDoc.SetParameterValue("BRANCHTAX", string.Empty);
                    crDoc.SetParameterValue("ORDERID", string.Empty);
                    crDoc.SetParameterValue("OutDD", string.Empty);
                    crDoc.SetParameterValue("OutMM", string.Empty);
                    crDoc.SetParameterValue("OutYY", string.Empty);
                }
                switch (Lien)
                {
                    case 1:
                        crDoc.SetParameterValue("Lien", "1: Lưu Kho");
                        break;
                    case 2:
                        crDoc.SetParameterValue("Lien", "2: Dùng để vận chuyển hàng");
                        break;
                    case 3:
                        crDoc.SetParameterValue("Lien", "3: Lưu kế toán");
                        break;
                    case 4:
                        crDoc.SetParameterValue("Lien", "4: Nội bộ");
                        break;
                    default:
                        break;
                }
                //if (Lien == 1) crDoc.SetParameterValue("Lien", "1: Lưu Kho");
                //if (Lien == 2) crDoc.SetParameterValue("Lien", "2: Dùng để vận chuyển hàng");
                //if (Lien == 3) crDoc.SetParameterValue("Lien", "3: Lưu kế toán");
                //if (Lien == 4) crDoc.SetParameterValue("Lien", "4: Nội bộ");

                //crDoc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.;
                crDoc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
                string strPrinterName = SystemConfig.GetParameter("DefaultPrinter_VATInvoicePrinter");
                if (!string.IsNullOrEmpty(strPrinterName))
                    crDoc.PrintOptions.PrinterName = strPrinterName;
                System.Drawing.Printing.PaperSize oPaperSize = new System.Drawing.Printing.PaperSize();
                crDoc.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;
                oPaperSize.RawKind = (int)System.Drawing.Printing.PaperKind.Custom;
                oPaperSize.Height = 210;
                oPaperSize.Width = 277;
                crDoc.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)oPaperSize.Kind;
                // crDoc.PrintToPrinter(1, true, 0, intNumPage - 1);
                crDoc.PrintToPrinter(1, false, 0, 0);
                //  crystalReportViewer1.ReportSource = crDoc;
                //   crystalReportViewer1.PrintReport();
                // crystalReportViewer1.ReportSource = crDoc;
                //   btnShowInputVoucher.Enabled = false;
                //  btnShowInputVoucherDetail.Enabled = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void cboMaThietLap_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMaThietLap = cboMaThietLap.SelectedValue.ToString().Trim();
            DataRow row = dtbData.AsEnumerable().Where(x => x.Field<object>("STORECHANGECURVOUCHERID").ToString().Trim() == strMaThietLap && x.Field<object>("USERNAME").ToString().Trim() == SystemConfig.objSessionUser.UserName && Convert.ToInt32(x.Field<object>("STOREID")) == SystemConfig.ConfigStoreID).FirstOrDefault();
            if (row != null)
            {
                txtCurrentNumber.Text = row["INVOICEID"].ToString().Trim();
                txtMauSo.Text = row["DENOMINATOR"].ToString().Trim();
                txtSymbol.Text = row["INVOICESYMBOL"].ToString().Trim();
                decInvoiceStart = Convert.ToDecimal(row["INVOICEIDSTART"]);
                decInvoiceEnd = Convert.ToDecimal(row["INVOICEEND"]);
            }
        }
    }
}
