﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
using Library.AppCore;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmStoreChangeImportExcelImeiError : Form
    {
        public DataTable dtbData { get; set; }
        public string InputStoreName { get; set; }
        private List<ProductError> lstProductError = new List<ProductError>();
        public frmStoreChangeImportExcelImeiError()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData, DevExpress.XtraPrinting.TextExportMode.Text, false, InputStoreName==null?string.Empty:InputStoreName);
        }

        private void frmStoreChangeImportExcelImeiError_Load(object sender, EventArgs e)
        {
            try
            {
                lstProductError = dtbData.AsEnumerable().Select(x =>
                    new ProductError()
                    {
                        ProductId = x["PRODUCTID"].ToString().TrimEnd(),
                        ProductName = x["PRODUCTNAME"].ToString().TrimEnd(),
                        Quantity = getQuantity(decimal.Parse(x["Quantity"].ToString()), decimal.Parse(x["OrderQuantity"].ToString()))
                    }
                    ).ToList();
                grdData.DataSource = lstProductError;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi nạp dữ liệu!", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nạp dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string getQuantity(decimal quantity, decimal orderquantity)
        {
            string strQuantity = string.Empty;
            if (quantity - orderquantity > 0)
                strQuantity = "+" + (quantity - orderquantity).ToString();
            else
                strQuantity = (quantity - orderquantity).ToString();
            return strQuantity;
        }
    }
    public class ProductError
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string Quantity { get; set; }
    }
}
