﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmProductInfoMaxSize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductInfoMaxSize));
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.flexData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.lbMaxHeight = new C1.Win.C1Input.C1NumericEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.lbMaxWidth = new C1.Win.C1Input.C1NumericEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.lbMaxLength = new C1.Win.C1Input.C1NumericEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnAccept = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMaxHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMaxWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMaxLength)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.flexData);
            this.groupControl2.Location = new System.Drawing.Point(9, 7);
            this.groupControl2.LookAndFeel.SkinName = "Blue";
            this.groupControl2.Margin = new System.Windows.Forms.Padding(4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(776, 457);
            this.groupControl2.TabIndex = 28;
            this.groupControl2.Text = "Thông tin chi tiết";
            // 
            // flexData
            // 
            this.flexData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexData.AllowEditing = false;
            this.flexData.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.FixedOnly;
            this.flexData.AutoClipboard = true;
            this.flexData.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.flexData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flexData.Location = new System.Drawing.Point(2, 24);
            this.flexData.Margin = new System.Windows.Forms.Padding(4);
            this.flexData.Name = "flexData";
            this.flexData.Rows.Count = 1;
            this.flexData.Rows.DefaultSize = 19;
            this.flexData.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.CellRange;
            this.flexData.Size = new System.Drawing.Size(772, 431);
            this.flexData.StyleInfo = resources.GetString("flexData.StyleInfo");
            this.flexData.TabIndex = 2;
            this.flexData.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            // 
            // lbMaxHeight
            // 
            this.lbMaxHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbMaxHeight.BackColor = System.Drawing.SystemColors.Control;
            this.lbMaxHeight.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbMaxHeight.CustomFormat = "###,###,###,##0";
            this.lbMaxHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaxHeight.ForeColor = System.Drawing.Color.Red;
            this.lbMaxHeight.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.lbMaxHeight.Location = new System.Drawing.Point(485, 472);
            this.lbMaxHeight.Margin = new System.Windows.Forms.Padding(4);
            this.lbMaxHeight.Name = "lbMaxHeight";
            this.lbMaxHeight.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.lbMaxHeight.ReadOnly = true;
            this.lbMaxHeight.Size = new System.Drawing.Size(121, 14);
            this.lbMaxHeight.TabIndex = 68;
            this.lbMaxHeight.TabStop = false;
            this.lbMaxHeight.Tag = null;
            this.lbMaxHeight.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lbMaxHeight.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(388, 471);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 15);
            this.label2.TabIndex = 70;
            this.label2.Text = "Chiều cao tối đa:";
            // 
            // lbMaxWidth
            // 
            this.lbMaxWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbMaxWidth.BackColor = System.Drawing.SystemColors.Control;
            this.lbMaxWidth.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbMaxWidth.CustomFormat = "###,###,###,##0";
            this.lbMaxWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaxWidth.ForeColor = System.Drawing.Color.Red;
            this.lbMaxWidth.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.lbMaxWidth.Location = new System.Drawing.Point(295, 471);
            this.lbMaxWidth.Margin = new System.Windows.Forms.Padding(4);
            this.lbMaxWidth.Name = "lbMaxWidth";
            this.lbMaxWidth.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.lbMaxWidth.ReadOnly = true;
            this.lbMaxWidth.Size = new System.Drawing.Size(105, 14);
            this.lbMaxWidth.TabIndex = 66;
            this.lbMaxWidth.TabStop = false;
            this.lbMaxWidth.Tag = null;
            this.lbMaxWidth.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lbMaxWidth.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(190, 471);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 15);
            this.label1.TabIndex = 71;
            this.label1.Text = "Chiều rộng tối đa:";
            // 
            // lbMaxLength
            // 
            this.lbMaxLength.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbMaxLength.BackColor = System.Drawing.SystemColors.Control;
            this.lbMaxLength.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbMaxLength.CustomFormat = "###,###,###,##0";
            this.lbMaxLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaxLength.ForeColor = System.Drawing.Color.Red;
            this.lbMaxLength.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.lbMaxLength.Location = new System.Drawing.Point(104, 471);
            this.lbMaxLength.Margin = new System.Windows.Forms.Padding(4);
            this.lbMaxLength.Name = "lbMaxLength";
            this.lbMaxLength.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.lbMaxLength.ReadOnly = true;
            this.lbMaxLength.Size = new System.Drawing.Size(73, 14);
            this.lbMaxLength.TabIndex = 67;
            this.lbMaxLength.TabStop = false;
            this.lbMaxLength.Tag = null;
            this.lbMaxLength.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.lbMaxLength.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(8, 471);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(95, 15);
            this.label16.TabIndex = 69;
            this.label16.Text = "Chiều dài tối đa:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.btnCancel.Location = new System.Drawing.Point(696, 466);
            this.btnCancel.LookAndFeel.SkinName = "Blue";
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(89, 26);
            this.btnCancel.TabIndex = 87;
            this.btnCancel.Text = "Bỏ qua";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccept.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccept.Appearance.Options.UseFont = true;
            this.btnAccept.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnAccept.Location = new System.Drawing.Point(599, 466);
            this.btnAccept.LookAndFeel.SkinName = "Blue";
            this.btnAccept.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(89, 26);
            this.btnAccept.TabIndex = 86;
            this.btnAccept.Text = "Đồng ý";
            this.btnAccept.Visible = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // frmProductInfoMaxSize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 495);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.lbMaxHeight);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbMaxWidth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbMaxLength);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.groupControl2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmProductInfoMaxSize";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin hàng hóa có kích thước tối đa";
            this.Load += new System.EventHandler(this.frmProductInfoMaxSize_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMaxHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMaxWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMaxLength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private C1.Win.C1FlexGrid.C1FlexGrid flexData;
        private C1.Win.C1Input.C1NumericEdit lbMaxHeight;
        private System.Windows.Forms.Label label2;
        private C1.Win.C1Input.C1NumericEdit lbMaxWidth;
        private System.Windows.Forms.Label label1;
        private C1.Win.C1Input.C1NumericEdit lbMaxLength;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnAccept;
    }
}