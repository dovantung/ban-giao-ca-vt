﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmPopUpIMEI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colIsError = new DevExpress.XtraGrid.Columns.GridColumn();
            this.menuContext = new System.Windows.Forms.ContextMenuStrip();
            this.mnuItemSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemSelectNone = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDeleteRow = new System.Windows.Forms.ToolStripMenuItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem7 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem10 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem11 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem12 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem13 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem14 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.grpProduct = new DevExpress.XtraEditors.GroupControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnValidate = new DevExpress.XtraEditors.SimpleButton();
            this.button3 = new System.Windows.Forms.Button();
            this.txtLastIMEI = new System.Windows.Forms.TextBox();
            this.txtFirstIMEI = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSearchProduct = new System.Windows.Forms.Button();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCountUninvalidIMEI = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCountInvalidIMEI = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCountIMEI = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grdIMEI = new DevExpress.XtraGrid.GridControl();
            this.grvIMEI = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsHasWarranty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkIsWarranty = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.menuContext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpProduct)).BeginInit();
            this.grpProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdIMEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvIMEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsWarranty)).BeginInit();
            this.SuspendLayout();
            // 
            // colIsError
            // 
            this.colIsError.Caption = "IsError";
            this.colIsError.FieldName = "ISERROR";
            this.colIsError.Name = "colIsError";
            // 
            // menuContext
            // 
            this.menuContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemSelectAll,
            this.mnuItemSelectNone,
            this.mnuItemDeleteRow});
            this.menuContext.Name = "menuContext";
            this.menuContext.Size = new System.Drawing.Size(151, 70);
            this.menuContext.Opening += new System.ComponentModel.CancelEventHandler(this.menuContext_Opening);
            // 
            // mnuItemSelectAll
            // 
            this.mnuItemSelectAll.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.mnuItemSelectAll.Name = "mnuItemSelectAll";
            this.mnuItemSelectAll.Size = new System.Drawing.Size(150, 22);
            this.mnuItemSelectAll.Text = "Chọn tất cả";
            this.mnuItemSelectAll.Click += new System.EventHandler(this.mnuItemSelectAll_Click);
            // 
            // mnuItemSelectNone
            // 
            this.mnuItemSelectNone.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.mnuItemSelectNone.Name = "mnuItemSelectNone";
            this.mnuItemSelectNone.Size = new System.Drawing.Size(150, 22);
            this.mnuItemSelectNone.Text = "Bỏ chọn tất cả";
            this.mnuItemSelectNone.Click += new System.EventHandler(this.mnuItemSelectNone_Click);
            // 
            // mnuItemDeleteRow
            // 
            this.mnuItemDeleteRow.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuItemDeleteRow.Name = "mnuItemDeleteRow";
            this.mnuItemDeleteRow.Size = new System.Drawing.Size(150, 22);
            this.mnuItemDeleteRow.Text = "Xóa";
            this.mnuItemDeleteRow.Click += new System.EventHandler(this.mnuItemDeleteRow_Click);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Bắn IMEI: Ctrl+B";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Tìm IMEI: Ctrl+F";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "Xác nhận: Ctrl+X";
            this.barStaticItem3.Id = 2;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "Xóa IMEI: Ctrl+Del";
            this.barStaticItem4.Id = 3;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "Phóng to: F11";
            this.barStaticItem5.Id = 4;
            this.barStaticItem5.Name = "barStaticItem5";
            this.barStaticItem5.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "Thu nhỏ: ESC";
            this.barStaticItem6.Id = 5;
            this.barStaticItem6.Name = "barStaticItem6";
            this.barStaticItem6.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem7
            // 
            this.barStaticItem7.Caption = "Đóng lại: Ctrl+F4";
            this.barStaticItem7.Id = 6;
            this.barStaticItem7.Name = "barStaticItem7";
            this.barStaticItem7.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(853, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 483);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem10,
            this.barStaticItem11,
            this.barStaticItem12,
            this.barStaticItem13,
            this.barStaticItem14});
            this.barManager1.MaxItemId = 7;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar3.Appearance.Options.UseFont = true;
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem11),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem12),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem13),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem14)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem10
            // 
            this.barStaticItem10.Caption = "Gen IMEI: Ctrl+X";
            this.barStaticItem10.Id = 2;
            this.barStaticItem10.Name = "barStaticItem10";
            this.barStaticItem10.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem11
            // 
            this.barStaticItem11.Caption = "Xóa IMEI: Ctrl+Del";
            this.barStaticItem11.Id = 3;
            this.barStaticItem11.Name = "barStaticItem11";
            this.barStaticItem11.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem12
            // 
            this.barStaticItem12.Caption = "Phóng to: F11";
            this.barStaticItem12.Id = 4;
            this.barStaticItem12.Name = "barStaticItem12";
            this.barStaticItem12.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem13
            // 
            this.barStaticItem13.Caption = "Thu nhỏ: ESC";
            this.barStaticItem13.Id = 5;
            this.barStaticItem13.Name = "barStaticItem13";
            this.barStaticItem13.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem14
            // 
            this.barStaticItem14.Caption = "Đóng lại: Ctrl+F4";
            this.barStaticItem14.Id = 6;
            this.barStaticItem14.Name = "barStaticItem14";
            this.barStaticItem14.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(853, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 483);
            this.barDockControlBottom.Size = new System.Drawing.Size(853, 26);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 483);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl1.Location = new System.Drawing.Point(853, 0);
            this.barDockControl1.Size = new System.Drawing.Size(0, 483);
            // 
            // grpProduct
            // 
            this.grpProduct.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpProduct.AppearanceCaption.Options.UseFont = true;
            this.grpProduct.AppearanceCaption.Options.UseTextOptions = true;
            this.grpProduct.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grpProduct.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.grpProduct.Controls.Add(this.panelControl1);
            this.grpProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpProduct.Location = new System.Drawing.Point(0, 0);
            this.grpProduct.Margin = new System.Windows.Forms.Padding(6);
            this.grpProduct.Name = "grpProduct";
            this.grpProduct.Size = new System.Drawing.Size(853, 483);
            this.grpProduct.TabIndex = 0;
            this.grpProduct.Text = "Sản phẩm";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl1.Controls.Add(this.panel2);
            this.panelControl1.Location = new System.Drawing.Point(5, 34);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(843, 443);
            this.panelControl1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.btnValidate);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.txtLastIMEI);
            this.panel2.Controls.Add(this.txtFirstIMEI);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnSearchProduct);
            this.panel2.Controls.Add(this.txtProductName);
            this.panel2.Controls.Add(this.txtProductID);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtCountUninvalidIMEI);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtCountInvalidIMEI);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtCountIMEI);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.grdIMEI);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(839, 439);
            this.panel2.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label8.Location = new System.Drawing.Point(720, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 16);
            this.label8.TabIndex = 12;
            this.label8.Text = "IMEI không hợp lệ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label9.Location = new System.Drawing.Point(599, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 16);
            this.label9.TabIndex = 11;
            this.label9.Text = "IMEI hợp lệ";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Pink;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(679, 34);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(35, 20);
            this.pictureBox2.TabIndex = 73;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(558, 34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 20);
            this.pictureBox1.TabIndex = 74;
            this.pictureBox1.TabStop = false;
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Enabled = false;
            this.btnSave.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnSave.Location = new System.Drawing.Point(748, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Đồng ý";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnValidate
            // 
            this.btnValidate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidate.Appearance.Options.UseFont = true;
            this.btnValidate.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.btnValidate.Location = new System.Drawing.Point(656, 6);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(86, 23);
            this.btnValidate.TabIndex = 9;
            this.btnValidate.Text = "Gen IMEI";
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // button3
            // 
            this.button3.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.button3.Location = new System.Drawing.Point(286, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(24, 24);
            this.button3.TabIndex = 4;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtLastIMEI
            // 
            this.txtLastIMEI.Location = new System.Drawing.Point(395, 33);
            this.txtLastIMEI.MaxLength = 20;
            this.txtLastIMEI.Name = "txtLastIMEI";
            this.txtLastIMEI.Size = new System.Drawing.Size(157, 22);
            this.txtLastIMEI.TabIndex = 8;
            this.txtLastIMEI.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLastIMEI_KeyDown);
            this.txtLastIMEI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLastIMEI_KeyPress);
            // 
            // txtFirstIMEI
            // 
            this.txtFirstIMEI.Location = new System.Drawing.Point(395, 6);
            this.txtFirstIMEI.MaxLength = 20;
            this.txtFirstIMEI.Name = "txtFirstIMEI";
            this.txtFirstIMEI.Size = new System.Drawing.Size(157, 22);
            this.txtFirstIMEI.TabIndex = 6;
            this.txtFirstIMEI.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFirstIMEI_KeyDown);
            this.txtFirstIMEI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFirstIMEI_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(312, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Số IMEI cuối:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(312, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Số IMEI đầu:";
            // 
            // btnSearchProduct
            // 
            this.btnSearchProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearchProduct.Location = new System.Drawing.Point(263, 5);
            this.btnSearchProduct.Name = "btnSearchProduct";
            this.btnSearchProduct.Size = new System.Drawing.Size(24, 24);
            this.btnSearchProduct.TabIndex = 3;
            this.btnSearchProduct.Click += new System.EventHandler(this.btnSearchProduct_Click);
            // 
            // txtProductName
            // 
            this.txtProductName.BackColor = System.Drawing.Color.Bisque;
            this.txtProductName.Location = new System.Drawing.Point(139, 6);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.ReadOnly = true;
            this.txtProductName.Size = new System.Drawing.Size(124, 22);
            this.txtProductName.TabIndex = 2;
            // 
            // txtProductID
            // 
            this.txtProductID.Location = new System.Drawing.Point(75, 6);
            this.txtProductID.MaxLength = 20;
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.Size = new System.Drawing.Size(63, 22);
            this.txtProductID.TabIndex = 1;
            this.txtProductID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProductID_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sản phẩm";
            // 
            // txtCountUninvalidIMEI
            // 
            this.txtCountUninvalidIMEI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtCountUninvalidIMEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountUninvalidIMEI.Location = new System.Drawing.Point(722, 412);
            this.txtCountUninvalidIMEI.Name = "txtCountUninvalidIMEI";
            this.txtCountUninvalidIMEI.ReadOnly = true;
            this.txtCountUninvalidIMEI.Size = new System.Drawing.Size(80, 22);
            this.txtCountUninvalidIMEI.TabIndex = 19;
            this.txtCountUninvalidIMEI.Text = "0";
            this.txtCountUninvalidIMEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(563, 415);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 16);
            this.label6.TabIndex = 18;
            this.label6.Text = "Số IMEI không hợp lệ";
            // 
            // txtCountInvalidIMEI
            // 
            this.txtCountInvalidIMEI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtCountInvalidIMEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountInvalidIMEI.Location = new System.Drawing.Point(395, 412);
            this.txtCountInvalidIMEI.Name = "txtCountInvalidIMEI";
            this.txtCountInvalidIMEI.ReadOnly = true;
            this.txtCountInvalidIMEI.Size = new System.Drawing.Size(80, 22);
            this.txtCountInvalidIMEI.TabIndex = 17;
            this.txtCountInvalidIMEI.Text = "0";
            this.txtCountInvalidIMEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Green;
            this.label5.Location = new System.Drawing.Point(280, 415);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 16);
            this.label5.TabIndex = 16;
            this.label5.Text = "Số IMEI hợp lệ";
            // 
            // txtCountIMEI
            // 
            this.txtCountIMEI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtCountIMEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountIMEI.Location = new System.Drawing.Point(122, 412);
            this.txtCountIMEI.Name = "txtCountIMEI";
            this.txtCountIMEI.ReadOnly = true;
            this.txtCountIMEI.Size = new System.Drawing.Size(80, 22);
            this.txtCountIMEI.TabIndex = 15;
            this.txtCountIMEI.Text = "0";
            this.txtCountIMEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(17, 415);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "Tổng số IMEI";
            // 
            // grdIMEI
            // 
            this.grdIMEI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdIMEI.ContextMenuStrip = this.menuContext;
            this.grdIMEI.Location = new System.Drawing.Point(0, 61);
            this.grdIMEI.MainView = this.grvIMEI;
            this.grdIMEI.Name = "grdIMEI";
            this.grdIMEI.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.chkIsWarranty});
            this.grdIMEI.Size = new System.Drawing.Size(839, 345);
            this.grdIMEI.TabIndex = 13;
            this.grdIMEI.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvIMEI});
            // 
            // grvIMEI
            // 
            this.grvIMEI.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvIMEI.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvIMEI.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvIMEI.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvIMEI.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvIMEI.Appearance.Row.Options.UseFont = true;
            this.grvIMEI.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grvIMEI.ColumnPanelRowHeight = 50;
            this.grvIMEI.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.colProductID,
            this.colProductName,
            this.colIMEI,
            this.colIsHasWarranty,
            this.colStatus,
            this.colIsError});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colIsError;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[Status]=true";
            styleFormatCondition1.Value1 = true;
            this.grvIMEI.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.grvIMEI.GridControl = this.grdIMEI;
            this.grvIMEI.Name = "grvIMEI";
            this.grvIMEI.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
            this.grvIMEI.OptionsView.ShowAutoFilterRow = true;
            this.grvIMEI.OptionsView.ShowGroupPanel = false;
            this.grvIMEI.OptionsView.ShowIndicator = false;
            this.grvIMEI.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.grvIMEI_RowCellStyle);
            this.grvIMEI.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvIMEI_ShowingEditor);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Chọn";
            this.gridColumn1.ColumnEdit = this.chkIsSelect;
            this.gridColumn1.FieldName = "ISSELECT";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 64;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // colProductID
            // 
            this.colProductID.Caption = "Mã sản phẩm";
            this.colProductID.FieldName = "PRODUCTID";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.ReadOnly = true;
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 1;
            this.colProductID.Width = 136;
            // 
            // colProductName
            // 
            this.colProductName.Caption = "Tên sản phẩm";
            this.colProductName.FieldName = "PRODUCTNAME";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 2;
            this.colProductName.Width = 331;
            // 
            // colIMEI
            // 
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 3;
            this.colIMEI.Width = 183;
            // 
            // colIsHasWarranty
            // 
            this.colIsHasWarranty.Caption = "Bảo hành";
            this.colIsHasWarranty.ColumnEdit = this.chkIsWarranty;
            this.colIsHasWarranty.FieldName = "ISWARRANTY";
            this.colIsHasWarranty.Name = "colIsHasWarranty";
            this.colIsHasWarranty.OptionsColumn.AllowEdit = false;
            this.colIsHasWarranty.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIsHasWarranty.Visible = true;
            this.colIsHasWarranty.VisibleIndex = 4;
            this.colIsHasWarranty.Width = 128;
            // 
            // chkIsWarranty
            // 
            this.chkIsWarranty.AutoHeight = false;
            this.chkIsWarranty.Name = "chkIsWarranty";
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Trạng thái";
            this.colStatus.FieldName = "STATUS";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 5;
            this.colStatus.Width = 303;
            // 
            // frmPopUpIMEI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 509);
            this.Controls.Add(this.grpProduct);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimizeBox = false;
            this.Name = "frmPopUpIMEI";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Danh sách IMEI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPopUpIMEI_FormClosing);
            this.Load += new System.EventHandler(this.frmPopUpIMEI_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPopUpIMEI_KeyDown);
            this.menuContext.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpProduct)).EndInit();
            this.grpProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdIMEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvIMEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsWarranty)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip menuContext;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSelectAll;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSelectNone;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraBars.BarStaticItem barStaticItem7;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem10;
        private DevExpress.XtraBars.BarStaticItem barStaticItem11;
        private DevExpress.XtraBars.BarStaticItem barStaticItem12;
        private DevExpress.XtraBars.BarStaticItem barStaticItem13;
        private DevExpress.XtraBars.BarStaticItem barStaticItem14;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraEditors.GroupControl grpProduct;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnValidate;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtLastIMEI;
        private System.Windows.Forms.TextBox txtFirstIMEI;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSearchProduct;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.TextBox txtProductID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCountUninvalidIMEI;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCountInvalidIMEI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCountIMEI;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraGrid.GridControl grdIMEI;
        private DevExpress.XtraGrid.Views.Grid.GridView grvIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colIsHasWarranty;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsWarranty;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIsError;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDeleteRow;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
    }
}