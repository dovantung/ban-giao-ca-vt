﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmPopupUser : Form
    {
        private string strUserName = string.Empty;
        public string UserName
        {
            get { return strUserName; }
            set { strUserName = value; }
        }
        public frmPopupUser()
        {
            InitializeComponent();
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ucUserName.UserName))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn người nhận hàng!");
                return;
            }
            if (string.IsNullOrEmpty(ucUserName.FullName))
            {
                MessageBoxObject.ShowWarningMessage(this, "Người nhận hàng không đúng. \r\nVui lòng kiểm tra lại!");
                return;
            }
            strUserName = ucUserName.FullName;
            this.DialogResult = DialogResult.OK;
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
