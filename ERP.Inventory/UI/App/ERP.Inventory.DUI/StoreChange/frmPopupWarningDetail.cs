﻿using Library.AppCore.Forms;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmPopupWarningDetail : Form
    {
        public DataTable dtbDetail { get; set; }
        public string strStoreChangeOrderID { get; set; }
        public frmPopupWarningDetail()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvStoreChangeOrderDetail);
        }

        private void frmPopupWarningDetail_Load(object sender, EventArgs e)
        {
            grpProduct.Text = grpProduct.Text + " " + strStoreChangeOrderID;
            if (dtbDetail != null && dtbDetail.Rows.Count > 0)
            {
                grdStoreChangeOrderDetail.DataSource = dtbDetail;
            }
            else
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Không tìm thấy thông tin chênh lệch!");
                this.Close();
            }
            this.Top = 0;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        private void xuấtExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdStoreChangeOrderDetail);
        }
    }
}
