﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmConfigPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfigPrint));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMauSo = new System.Windows.Forms.TextBox();
            this.txtCurrentNumber = new System.Windows.Forms.TextBox();
            this.txtSymbol = new System.Windows.Forms.TextBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnUndoPrint = new System.Windows.Forms.Button();
            this.cboMaThietLap = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(314, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mẫu số:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số hiện tại:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ký hiệu:";
            // 
            // txtMauSo
            // 
            this.txtMauSo.Location = new System.Drawing.Point(393, 9);
            this.txtMauSo.Name = "txtMauSo";
            this.txtMauSo.ReadOnly = true;
            this.txtMauSo.Size = new System.Drawing.Size(204, 22);
            this.txtMauSo.TabIndex = 0;
            // 
            // txtCurrentNumber
            // 
            this.txtCurrentNumber.Location = new System.Drawing.Point(393, 37);
            this.txtCurrentNumber.Name = "txtCurrentNumber";
            this.txtCurrentNumber.Size = new System.Drawing.Size(204, 22);
            this.txtCurrentNumber.TabIndex = 3;
            this.txtCurrentNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurrentNumber_KeyPress);
            // 
            // txtSymbol
            // 
            this.txtSymbol.Location = new System.Drawing.Point(87, 37);
            this.txtSymbol.Name = "txtSymbol";
            this.txtSymbol.ReadOnly = true;
            this.txtSymbol.Size = new System.Drawing.Size(202, 22);
            this.txtSymbol.TabIndex = 1;
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(393, 73);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(95, 25);
            this.btnPrint.TabIndex = 12;
            this.btnPrint.Text = "   In";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnUndoPrint
            // 
            this.btnUndoPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnUndoPrint.Image")));
            this.btnUndoPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndoPrint.Location = new System.Drawing.Point(502, 73);
            this.btnUndoPrint.Name = "btnUndoPrint";
            this.btnUndoPrint.Size = new System.Drawing.Size(95, 25);
            this.btnUndoPrint.TabIndex = 13;
            this.btnUndoPrint.Text = "   Bỏ qua";
            this.btnUndoPrint.UseVisualStyleBackColor = true;
            this.btnUndoPrint.Click += new System.EventHandler(this.btnUndoPrint_Click);
            // 
            // cboMaThietLap
            // 
            this.cboMaThietLap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMaThietLap.FormattingEnabled = true;
            this.cboMaThietLap.Location = new System.Drawing.Point(87, 9);
            this.cboMaThietLap.Name = "cboMaThietLap";
            this.cboMaThietLap.Size = new System.Drawing.Size(202, 24);
            this.cboMaThietLap.TabIndex = 14;
            this.cboMaThietLap.SelectedIndexChanged += new System.EventHandler(this.cboMaThietLap_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "Mã thiết lập:";
            // 
            // frmConfigPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 110);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cboMaThietLap);
            this.Controls.Add(this.btnUndoPrint);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.txtSymbol);
            this.Controls.Add(this.txtCurrentNumber);
            this.Controls.Add(this.txtMauSo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmConfigPrint";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "In PXK kiêm VCNB";
            this.Load += new System.EventHandler(this.frmConfigPrint_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMauSo;
        private System.Windows.Forms.TextBox txtCurrentNumber;
        private System.Windows.Forms.TextBox txtSymbol;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnUndoPrint;
        private System.Windows.Forms.ComboBox cboMaThietLap;
        private System.Windows.Forms.Label label4;
    }
}