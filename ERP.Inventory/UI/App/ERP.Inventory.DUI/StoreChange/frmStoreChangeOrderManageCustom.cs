﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.StoreChange
{
    public partial class frmStoreChangeOrderManager
    {
        //Tao xuất chuyển tự động cho hàng công cụ dụng cụ
        private void mnuStoreChangeOrderExport_Click(object sender, EventArgs e)
        {
            DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            try
            {
                string strStoreChangeOrderTypeID = Convert.ToString(row["STORECHANGEORDERTYPEID"]);
                string strStoreChangeOrderID = Convert.ToString(row["STORECHANGEORDERID"]);
                string strTRANSPORTCOMPANYID = Convert.ToString(row["TRANSPORTCOMPANYID"]);
                string strTRASPORTSERVICEID = Convert.ToString(row["TRANSPORTSERVICESID"]);
                string TRANSPORTCOMPANYNAME = Convert.ToString(row["TRANSPORTCOMPANYNAME"]);
                string TRANSPORTSERVICENAME = Convert.ToString(row["TRANSPORTSERVICENAME"]);
                bool bolIsReviewed = Convert.ToBoolean(row["ISREVIEWED"]);
                frmStoreChangeOrder formStoreChangeOrder = new frmStoreChangeOrder(bolIsReviewed);
                formStoreChangeOrder.Tag = "StoreChangeOrderType = " + strStoreChangeOrderTypeID;
                formStoreChangeOrder.StoreChangeOrderID = strStoreChangeOrderID;
                formStoreChangeOrder.strTRANSPORTCOMPANYID = strTRANSPORTCOMPANYID;
                formStoreChangeOrder.strTRASPORTSERVICEID = strTRASPORTSERVICEID;
                formStoreChangeOrder.IsOnlyCertifyFinger = bolIsOnlyCertifyFinger;
                formStoreChangeOrder.FormManager = this;
                formStoreChangeOrder.IsReviewed = bolIsReviewed;
                formStoreChangeOrder.IsTaoXuatChuyenKhoCCDC = true;
                
                formStoreChangeOrder.ShowDialog();
                if (formStoreChangeOrder.IsHasAction)
                    btnSearch_Click(null, null);
            }
            catch (Exception)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi chọn yêu cầu để chỉnh sửa.");
                return;
            }
        }
    }
}
