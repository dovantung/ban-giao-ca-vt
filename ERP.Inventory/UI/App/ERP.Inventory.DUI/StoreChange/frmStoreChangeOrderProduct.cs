﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC.PM;
using ERP.Inventory.PLC.StoreChange;
using ERP.Inventory.PLC.PM.WSStoreChangeOrder;
using Library.AppCore.LoadControls;
using System.Collections;
using DevExpress.XtraGrid.Views.Grid;
using ERP.Inventory.PLC.WSProductInStock;
using ERP.Inventory.DUI.FIFO;
using ERP.MasterData.PLC.MD;

namespace ERP.Inventory.DUI.StoreChange
{
    /// <summary>
    /// Created by  :  Nguyễn Thị Huê
    /// Description :   Quản lý yêu cầu chuyển kho hàng hóa khác  16/11/2019 
    /// </summary>
    public partial class frmStoreChangeOrderProduct : Form
    {
        #region Khai báo biến
        private PLCStore objPLCStore = new PLCStore();
        private DataTable dtbStoreCache = null;
        private string strPermission_Add = "MD_StoreChangeOrder_Add";
        private string strPermission_Delete = "MD_StoreChangeOrder_Delete";
        private string strPermission_Edit = "MD_StoreChangeOrder_Edit";
        private string strPermission_ExportExcel = "MD_StoreChangeOrder_ExportExcel";
        private string strEdit_AFReview = "PM_SCORDER_EDIT_AFTER_REVIEW";
        private DataTable dtbTransportCompany = null;
        private DataTable dtbTransportServices = null;

        private DataTable dtbStoreChangeDetail = null;
        private int intMainGroupID = 0;
        private string strStoreChangeID = string.Empty;
        private PLCStoreChangeOrder objPLCStoreChangeOrder = new PLCStoreChangeOrder();
        private ERP.Inventory.PLC.StoreChange.WSStoreChange.StoreChange objStoreChange = new PLC.StoreChange.WSStoreChange.StoreChange();
        PLC.WSTransportOrder.TP_TransportOrder objTransportOrder = null;

        private string strStoreChangeOrderID = string.Empty;
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;

        private StoreChangeOrder objStoreChangeOrder = null;
        private bool bolChanged = false;
        // lay gia tri cu cua kho xuat, kho nhap
        private int intFromStore = -1;
        private int intToStore = -1;
        private frmStoreChangeOrderManager frmManager = null;
        private DataTable dtbToStore = null;
        private DataTable dtbNotification_User = null;
        int intCheckCompanyType = -1;
        int intCheckProvinceType = -1;
        int intCheckBranchType = -1;
        int intStoreChangeTypeShowProductID = -1;
        int intInStockStatusID = 1;
        private bool bolIsOnlyCertifyFinger = false;
        private string strReviewUser = string.Empty;
        private bool bolIsCertifyByFinger = false;
        private bool bolIsHasAction = false;
        private bool bolInputIMEI = true;
        private bool bolIsImport = false;
        private bool bolIsLoadForm = false;
        private bool ISADDNEW = false;
        DataTable dtbProductConsignmentTypeBySCOTID = null;
        DataTable dtb = null;
        private ERP.MasterData.PLC.MD.WSInputType.InputType objInputType = null;

        Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection selection;
        // Loai chuyen kho
        private ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType = null;
        #endregion


        #region Property
        private string MainGroupIdlst;
        public string ListMainGroupId
        {
            get { return MainGroupIdlst; }
            set { MainGroupIdlst = value; }
        }
        public int MainGroupID
        {
            get { return intMainGroupID; }
            set { intMainGroupID = value; }
        }
        public string StoreChangeOrderID
        {
            get { return strStoreChangeOrderID; }
            set
            {
                strStoreChangeOrderID = value;
                if (string.IsNullOrWhiteSpace(strStoreChangeOrderID))
                {
                    FormState = FormStateType.ADD;
                }
                else
                    FormState = FormStateType.EDIT;
            }
        }
        public frmStoreChangeOrderManager FormManager
        {
            get { return frmManager; }
            set { frmManager = value; }
        }
        public bool IsOnlyCertifyFinger
        {
            set { bolIsOnlyCertifyFinger = value; }
        }
        /// <summary>
        /// Có thực hiện thao tác
        /// </summary>
        public bool IsHasAction
        {
            get { return bolIsHasAction; }
        }
        private bool bolIsReviewed = false;

        public bool IsReviewed
        {
            get { return bolIsReviewed; }
            set { bolIsReviewed = value; }
        }


        #endregion  Property

        #region Constructor
        public frmStoreChangeOrderProduct()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        #endregion

        public void SearchData()
        {

            btnSearch.Enabled = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                btnSearch.Enabled = false;

                if (selection != null)
                    selection.ClearSelection();


                //int fromStore = cboFromStoreID.StoreID;
                //int toStore = cboToStoreID.StoreID;

                //-----Filter kho theo theo quyền ISCANVIEWSTORECHANGEORDER (Nếu user != admin)
                string strFromStoreIDList = string.Empty;
                string strToStoreIDList = string.Empty;

                //if (cboFromStoreID.DataSource.Rows.Count > 0)
                //strFromStoreIDList = cbxFromManager.StoreIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                ////if (cboToStoreID.DataSource.Rows.Count > 0)
                //strToStoreIDList = comboToStore2.StoreIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                var OtherProDt = "";
                if (cboTransferOtherPro.SelectedIndex != 0)
                {
                    OtherProDt = cboTransferOtherPro.SelectedValue.ToString().Split('-')[0];
                };


                object[] objKeyWords = new object[]
                {
                   //  "@STORECHANGEORDERTYPE", storeChangeOrderType,
                  //  "@STORECHANGETYPE", storeChangeType,
                    "@CREATEUSER", ucCreateUser.UserName.Trim(),
                    "@FROMSTOREIDLIST", cbxFromManager.StoreID,
                    "@TOSTOREIDLIST", comboToStore2.StoreID,
                    "@FROMDATE", ERP.MasterData.DUI.Common.DateTimeProcessor.FromDate(dteFromDate.Value),
                    "@TODATE", ERP.MasterData.DUI.Common.DateTimeProcessor.ToDate(dteToDate.Value),
                    "@ISREVIEW", -1,
                    "@STORECHANGESTATUSIDLIST", cboStoreChangeStatusManager.SelectedValue,
                  "@OTHERPRODUCTID",  OtherProDt.Trim().ToString(),
                  //  "@MAINGROUPID", intMainGroup,
                    "@KEYWORDS", txtKeyWords.Text.Trim(),
               //     "@SEARCHBY", cboSearchBy.SelectedIndex,
                    "@ISDELETE", chkDelete.Checked,
               //     "@ISEXPIRE", chkExpire.Checked,
              //      "@ISURGENT", chkUrgent.Checked,
                 //   "@SUBGROUPIDLIST",cboSubGroupIDList.SubGroupIDList
                };

                dtb = objPLCStoreChangeOrder.SearchDataOtherTransfer(objKeyWords);
                this.Cursor = Cursors.Default;

                btnSearch.Enabled = true;
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSearch.Enabled = true;
                    return;
                }
                grdData.DataSource = dtb;
                btnExportToExcel.Enabled = dtb != null && dtb.Rows.Count > 0;
                cmdExcelReview.Enabled = dtb != null && dtb.Rows.Count > 0;
                //Hiển thị Lý do hủy 

            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi tìm kiếm thông tin yêu cầu chuyển kho", ex.ToString());
                this.Cursor = Cursors.Arrow;

            }
            btnSearch.Enabled = true;
        }

        #region Các hàm sự kiện
        private void frmStoreChangeOrder_Load(object sender, EventArgs e)
        {
            InitControl();
            EnableButton(false);
            if (!GetStoreChangeOrderType())
            {
                return;
            }
            //CreateReviewItem();
            LoadStoreChangeStatus();
            LoadComboBox();
            //if (intFormState == FormStateType.ADD)
            //{
            //    dropdownStoreChange.Enabled = false;
            //    btnStoreChangeAll.Enabled = false;
            //    //ckbIsReview.Enabled = false;
            //    //ckbIsDeleted.Enabled = false;
            //    btnReview.Enabled = false;
            //    btnDelete.Enabled = false;
            //    grdViewProduct.Columns["STORECHANGEQUANTITY"].Enabled = false;
            //    cboFromStoreID.SetValue(SystemConfig.intDefaultStoreID);
            //}
            //DataTable dtbSource = (grdProduct.DataSource as DataTable).Copy();
            //var item = dtbSource.AsEnumerable().Where(x => !string.IsNullOrEmpty(x["IMEI"].ToString().Trim()));
            //if (item.Count() <= 0 && grdViewProduct.RowCount > 0)
            //{
            //    grdViewProduct.Columns["IMEI"].Enabled = false;
            //    rbtnQuantity.Checked = true;
            //    rbtnIMEI.Checked = false;
            //}
            //bolIsLoadForm = true;
        }
        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (!btnDelete.Enabled)
                return;
            if (!DeleteData())
                return;
            this.bolIsHasAction = true;
            ISADDNEW = false;
            btnDelete.Enabled = false;
            btnTransfer.Enabled = false;
            btnUpdate.Enabled = false;
            tabControl.SelectedTab = TabManager;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!btnUpdate.Enabled)
                return;
            if (!ValidateData())
                return;
            if (!UpdateData())
                return;
            this.bolIsHasAction = true;
            ISADDNEW = false;
            btncancel.Enabled = false;
            btnAdd.Enabled = true;
            toolStripMenuItem1.Enabled = true;
            SearchData();
            tabControl.SelectedTab = TabManager;
        }
        private void mnuItemDelete_Click(object sender, EventArgs e)
        {


        }
        private void mnuItemExport_Click(object sender, EventArgs e)
        {

        }
        private void cmdExcelReview_Click(object sender, EventArgs e)
        {
            btnExportToExcel_Click(null, null);
        }
        private void ExportExcel(DevExpress.XtraGrid.GridControl grid)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grid);
        }

        private void frmStoreChangeOrder_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void ChangeFocus(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                this.SelectNextControl(this.ActiveControl, true, true, true, true);
        }
        private void SelectAll(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == System.Windows.Forms.Keys.A))
            {
                (sender as TextBox).SelectAll();
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }
        private void LoadInputStore(int intFromStore)
        {
            if (dtbToStore == null || intFromStore == -1)
                return;
            try
            {
                if (objStoreChangeOrderType.StoreChangeTypeID == intStoreChangeTypeShowProductID
                    || objStoreChangeOrderType.ToStoreID > 0)
                {
                    // cboToStoreID.InitControl(false, dtbToStore);
                    if (objStoreChangeOrderType.ToStoreID > 0)
                        cboToStoreID.SetValue(objStoreChangeOrderType.ToStoreID);
                    else cboToStoreID.SetValue(intFromStore);
                    //return;
                }
            }
            catch (Exception ex)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi load danh sách kho nhập dựa trên loại chuyển kho");
                return;
            }


            int intCompanyID = -1;
            int intProvinceID = -1;
            int intBranchID = -1;

            bool bolIsRealStore_FromStore = false;//là kho chính
            int intHostStoreID_FromStore = 0;//mã kho chính
            DataTable dtbFromStore = cboFromStoreID.DataSource as DataTable;
            DataRow[] rowsFromStore = dtbFromStore.Select("STOREID = " + intFromStore);
            if (rowsFromStore.Length < 1)
            {
                // MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin kho xuất");
                cboFromStoreID.SetValue(-1);
                return;
            }
            Int32.TryParse(rowsFromStore[0]["COMPANYID"].ToString(), out intCompanyID);
            Int32.TryParse(rowsFromStore[0]["PROVINCEID"].ToString(), out intProvinceID);
            Int32.TryParse(rowsFromStore[0]["BRANCHID"].ToString(), out intBranchID);

            bolIsRealStore_FromStore = Convert.ToBoolean(rowsFromStore[0]["ISREALSTORE"]);
            Int32.TryParse(rowsFromStore[0]["HOSTSTOREID"].ToString(), out intHostStoreID_FromStore);
            bool bolIsInputStore = Convert.ToBoolean(rowsFromStore[0]["ISINPUTSTORE"]);
            StringBuilder sbFilter = new StringBuilder();
            if (objStoreChangeOrderType.StoreChangeTypeID == intStoreChangeTypeShowProductID)
                sbFilter.Append("STOREID = " + intFromStore + " AND COMPANYID = " + intCompanyID);
            else
            {
                sbFilter.Append("STOREID <> " + intFromStore + " ");
                switch (intCheckCompanyType)
                {
                    case 1:
                        sbFilter.Append(" AND COMPANYID = " + intCompanyID);
                        break;
                    case 2:
                        sbFilter.Append(" AND COMPANYID <> " + intCompanyID);
                        break;
                    default:
                        break;
                }
                switch (intCheckProvinceType)
                {
                    case 1:
                        sbFilter.Append(" AND PROVINCEID = " + intProvinceID);
                        break;
                    case 2:
                        sbFilter.Append(" AND PROVINCEID <> " + intProvinceID);
                        break;
                    default:
                        break;
                }
                switch (intCheckBranchType)
                {
                    case 1:
                        sbFilter.Append(" AND BRANCHID = " + intBranchID);
                        break;
                    case 2:
                        sbFilter.Append(" AND BRANCHID <> " + intBranchID);
                        break;
                    default:
                        break;
                }
            }
            //chon kho xuat kho chính -> kho nhan la kho chinh hoặc kho phụ của kho xuất
            if (bolIsRealStore_FromStore)
            {
                sbFilter.Append(" AND (ISREALSTORE = 1 OR (ISREALSTORE = 0 AND HOSTSTOREID=" + intFromStore + ")) ");
            }
            else//Chon kho xuat la kho phu -> kho nhan la kho chinh cua kho xuat
            {
                sbFilter.Append(" AND ISREALSTORE = 1 AND STOREID=" + intHostStoreID_FromStore);
            }
            try
            {

                dtbToStore.DefaultView.RowFilter = sbFilter.ToString();
                DataTable dtbTmp = dtbToStore.DefaultView.ToTable();
                //if(objStoreChangeOrderType.ToStoreID>0 && dtbTmp!=null 
                //    && dtbTmp.Select($"STOREID={objStoreChangeOrderType.ToStoreID}").Length==0)
                //{
                //    MessageBox.Show(this, "Kho xuất không không hợp lệ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    cboFromStoreID.SetValue(-1);
                //    return;
                //}
                cboToStoreID.InitControl(false, dtbTmp);
                cboToStoreID.SetValue(objStoreChangeOrderType.ToStoreID);
            }
            catch (Exception ex)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi load danh sách kho nhập dựa trên loại chuyển kho");
            }
        }
        private void cboFromStoreID_SelectionChangeCommitted(object sender, EventArgs e)
        {

            intFromStore = cboFromStoreID.StoreID;
            LoadInputStore(intFromStore);
        }

        private void cboToStoreID_SelectionChangeCommitted(object sender, EventArgs e)
        {

            intToStore = cboToStoreID.StoreID;
        }
        private void grdViewProduct_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            bolChanged = true;
            GridView view = sender as GridView;

            //nieu so luong la null hoac empty thi tra ve 0
            if (view.FocusedColumn == view.Columns["STORECHANGEQUANTITY"] && e.Value == null)
            {
                view.SetFocusedRowCellValue("STORECHANGEQUANTITY", 0);
            }
            //nieu so luong la null hoac empty thi tra ve 0
            if (view.FocusedColumn == view.Columns["STORECHANGEQUANTITY"] && e.Value == null)
            {
                view.SetFocusedRowCellValue("STORECHANGEQUANTITY", 0);
            }
            //Nieu la nhap nhieu Imei
            //if (view.FocusedColumn == view.Columns["IMEI"] && e.Value != null)
            //{
            //    //Kiem tra trung Imei
            //    string strRealImei = string.Empty;
            //    string strImei = e.Value.ToString().Trim();
            //    int intQuantity = ERP.MasterData.DUI.Common.CommonFunction.GetQuantityFromIMEI(e.Value.ToString().Trim(), true, ref strRealImei);
            //    //Thay doi Imei thi thay doi so luong
            //    view.SetFocusedRowCellValue("QUANTITY", intQuantity);
            //    view.SetFocusedRowCellValue("STORECHANGEQUANTITY", intQuantity);
            //}



        }

        private void grdViewProduct_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {

        }

        private void grdViewProduct_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Column.FieldName == "IMEI")
            {
                string strIsRequestImei = view.GetRowCellValue(e.RowHandle, view.Columns["ISREQUESTIMEI"]).ToString().Trim();
                //if (strIsRequestImei == "0" || string.IsNullOrEmpty(strIsRequestImei))
                //{
                //    e.Appearance.BackColor = System.Drawing.SystemColors.Info;
                //}
                // string strIMEI = view.GetRowCellValue(e.RowHandle, view.Columns["IMEI"]).ToString().Trim();
                bool bIsRequestImei = Convert.ToBoolean(view.GetRowCellValue(view.FocusedRowHandle, "ISREQUESTIMEI"));
                string strQuantity = view.GetRowCellValue(e.RowHandle, view.Columns["QUANTITY"]).ToString().Trim();
                decimal decQuantity = 0;
                decimal.TryParse(strQuantity, out decQuantity);
                if (decQuantity != 1 && !(strIsRequestImei == "0" || string.IsNullOrEmpty(strIsRequestImei)))
                {
                    e.Appearance.BackColor = System.Drawing.SystemColors.Info;
                }
                else if ((strIsRequestImei == "0" || string.IsNullOrEmpty(strIsRequestImei)))
                {
                    e.Appearance.BackColor = System.Drawing.SystemColors.Info;
                }
                else
                {
                    e.Appearance.BackColor = System.Drawing.SystemColors.Window;
                }
            }
            //so luong = 0 thi tô đỏ
            //if (e.Column.FieldName == "QUANTITY" )
            //{
            //    string quantity = view.GetRowCellDisplayText(e.RowHandle, view.Columns["QUANTITY"]);
            //    if (quantity == "0" || string.IsNullOrEmpty(quantity))
            //    {
            //        e.Appearance.BackColor = Color.Red;
            //        e.Appearance.BackColor2 = Color.LightCyan;
            //    }
            //}

        }

        private void grdViewProduct_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = sender as GridView;
            if (objStoreChangeOrder.IsReviewed)
            {
                e.Cancel = true;
            }
            if (FormState == FormStateType.ADD || (FormState == FormStateType.EDIT && !objStoreChangeOrder.IsReviewed))
            {



            }
            else
            {
                if (view.FocusedColumn == view.Columns["IMEI"])
                    e.Cancel = true;
            }

        }
        private void btnSearchProduct_Click(object sender, EventArgs e)
        {
            if (!ValidateStore()) return;
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch = MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch.IsMultiSelect = true;
            frmProductSearch.ShowDialog();
            if (frmProductSearch.ProductList != null && frmProductSearch.ProductList.Count > 0)
            {
                //add san pham vao chi tiet
                //txtSearchProduct.Text = frmProductSearch.ProductList[0].ProductID;
                foreach (ERP.MasterData.PLC.MD.WSProduct.Product objProduct in frmProductSearch.ProductList)
                {
                    AddProduct(objProduct.ProductID, 1);
                }

            }
        }
        private void txtSearchProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 13) return;
            if (!ValidateStore()) return;
        }

        private void tabReviewLevel_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.ADD)
                tabControl.SelectedTab = tabGeneral;
        }
        //private void btnReview_Click(object sender, EventArgs e)
        //{
        //    if (!ValidateStore())
        //        return;
        //    Review();
        //}

        private void btnOutputStoreChange_Click(object sender, EventArgs e)
        {
            frmStoreChange frmStoreChange1 = new frmStoreChange();
            frmStoreChange1.StoreChangeOrderID = strStoreChangeOrderID.Trim();
            frmStoreChange1.ShowDialog();
        }
        #endregion

        #region Các hàm hỗ trợ
        private void EnableButton(bool bolEnable)
        {

            btnDelete.Enabled = bolEnable;
            btnUpdate.Enabled = false;
            btnTransfer.Enabled = false;
            btncancel.Enabled = false;



        }
        /// <summary>
        /// Khởi tạo cho các control
        /// Xét các thuộc tính
        /// </summary>
        private void InitControl()
        {

            this.MinimumSize = new System.Drawing.Size(984, 576);
            txtContent.MaxLength = 2000;

            ////Event Enter
            dteExpiryDate.KeyPress += new KeyPressEventHandler(ChangeFocus);
            cboFromStoreID.KeyPress += new KeyPressEventHandler(ChangeFocus);
            cboToStoreID.KeyPress += new KeyPressEventHandler(ChangeFocus);
            cboTransportTypeID.KeyPress += new KeyPressEventHandler(ChangeFocus);
            //radioIsNew.KeyPress += new KeyPressEventHandler(ChangeFocus);
            //radioIsOld.KeyPress += new KeyPressEventHandler(ChangeFocus);
            txtContent.KeyUp += new KeyEventHandler(SelectAll);
            CreateColumnGridReview(false);
            FormatGridReview();


        }
        private bool GetStoreChangeOrderType()
        {
            int intStoreChangeOrderType = 0;
            Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
            try
            {
                intStoreChangeOrderType = Convert.ToInt32(hstbParam["StoreChangeOrderType"]);
                ERP.MasterData.PLC.MD.WSStoreChangeOrderType.ResultMessage objResultMessage = new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfo(ref objStoreChangeOrderType, intStoreChangeOrderType);
                if (objResultMessage.IsError || objStoreChangeOrderType == null)
                {
                    throw new Exception(objResultMessage.Message);
                }

                dtbProductConsignmentTypeBySCOTID = null;
                objResultMessage = new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().GetProductConsignmentTypeBySCOTID(ref dtbProductConsignmentTypeBySCOTID, objStoreChangeOrderType.StoreChangeOrderTypeID);
                if (objResultMessage.IsError)
                {
                    throw new Exception(objResultMessage.Message);
                }
                objStoreChangeType = new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo((int)objStoreChangeOrderType.StoreChangeTypeID);

            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi lấy tham số loại yêu cầu chuyển kho", objExce, DUIInventory_Globals.ModuleName + "->GetStoreChangeOrderType");
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi lấy tham số loại yêu cầu chuyển kho.");
                return false;
            }
            return true;
        }


        bool IsCanChangeProduct(int intPRODUCTCONSIGNMENTTYPEID)
        {
            //PRODUCTCONSIGNMENTTYPEID
            if (dtbProductConsignmentTypeBySCOTID != null && dtbProductConsignmentTypeBySCOTID.Rows.Count > 0)
            {
                DataRow[] drs = dtbProductConsignmentTypeBySCOTID.Select("PRODUCTCONSIGNMENTTYPEID=" + intPRODUCTCONSIGNMENTTYPEID.ToString());
                if (drs != null && drs.Length > 0)
                    return true;
                return false;
            }
            return true;

        }
        private void LoadComboBox()
        {
            ISADDNEW = false;
            btnAdd.Enabled = true;
            toolStripMenuItem1.Enabled = true;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
            btncancel.Enabled = false;
            btnTransfer.Enabled = false;
            cboTransferOtherPro_load();
            txtStoreChangeOrderID.Text = "";

            object[] objKeyWords = new object[] { "@ISDELETED", 0 };
            ERP.MasterData.PLC.MD.WSStore.ResultMessage objResultMessage = objPLCStore.SearchData(ref dtbStoreCache, objKeyWords);
            if (objResultMessage.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return;
            }
            Library.AppCore.DataSource.FilterObject.StoreFilter objFromFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objFromFilter.IsCheckPermission = true;
            objFromFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;

            cbxFromManager.InitControl(false, objFromFilter);
            comboToStore2.InitControl(false, dtbStoreCache);

            //if (FormState == FormStateType.EDIT)
            //{
            //    cboToStoreID.InitControl(false, dtbStoreCache);
            //    cboFromStoreID.InitControl(false, dtbStoreCache);
            //}
            //else
            //{
            //    cboFromStoreID.InitControl(false, objFromFilter);
            //    //cboToStoreID.InitControl(false, dtbStoreCache);
            //    cboToStoreID.InitControl(false);
            //}


            if (FormState == FormStateType.EDIT)
            {
                cboToStoreID.InitControl(false, dtbStoreCache);
                cboFromStoreID.InitControl(false, dtbStoreCache);
            }
            else
            {
                cboFromStoreID.InitControl(false, objFromFilter);


                cboToStoreID.InitControl(false, dtbStoreCache);

                //Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                //objStoreFilter.IsCheckPermission = true;
                //objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGETO;
                //cboToStoreID.InitControl(false, objStoreFilter);

            }


            DataTable dtbInstockStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            dtbInstockStatus.Columns.Add("INSTOCKSTATUSID", typeof(int));
            foreach (DataRow dr in dtbInstockStatus.Rows)
                dr["INSTOCKSTATUSID"] = Convert.ToInt32(dr["PRODUCTSTATUSID"]);
            dtbInstockStatus.Columns["PRODUCTSTATUSNAME"].ColumnName = "INSTOCKSTATUSNAME";
            dtbInstockStatus.AcceptChanges();

            dtbTransportServices = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("TP_TRANSPORTSERVICE_CACHE");
            //if (dtbTransportServices != null)
            //{
            //    cboTransportServicesID.InitControl(false, dtbTransportServices, "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--Chọn dịch vụ chuyển phát--");
            //}
            // Đơn vị chuyển phát
            dtbTransportCompany = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_TRANSPORTCOMPANY_CACHE");
            //if (dtbTransportCompany != null)
            //{
            //    cboTransportCompanyID.InitControl(false, dtbTransportCompany, "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đơn vị chuyển phát--");
            //}

            cboFromStoreID.Enabled = true;
            cboToStoreID.Enabled = true;
            DataTable dtbTransportType = Library.AppCore.DataSource.GetDataSource.GetTransportType().Copy();
            cboTransportTypeID.InitControl(false, dtbTransportType, "TRANSPORTTYPEID", "TRANSPORTTYPENAME", "--Chọn Phương tiện vận chuyển--");

        }

        private void LoadStoreChangeStatus()
        {
            LoadStoreChangeStatus(false);
        }
        private void LoadStoreChangeStatus(bool isExpired)
        {
            cboStoreChangeStatusManager.DataSource = null;
            cboStoreChangeStatusDT.DataSource = null;

            DataTable dtbStoreChangeStatus = new DataTable("StoreChangeStatus ");
            dtbStoreChangeStatus.Columns.Add("Value");
            dtbStoreChangeStatus.Columns.Add("Text");

            dtbStoreChangeStatus.Rows.Add(-1, "Trạng thái chuyển kho");
            dtbStoreChangeStatus.Rows.Add(0, "Chưa chuyển kho");
            dtbStoreChangeStatus.Rows.Add(3, "Đã chuyển kho");

            cboStoreChangeStatusManager.DataSource = dtbStoreChangeStatus.Copy();
            cboStoreChangeStatusManager.DisplayMember = "Text";
            cboStoreChangeStatusManager.ValueMember = "Value";
            cboStoreChangeStatusManager.SelectedIndex = 0;




            DataTable dtbStoreChangeStatusDT = new DataTable("StoreChangeStatus ");
            dtbStoreChangeStatusDT.Columns.Add("Value");
            dtbStoreChangeStatusDT.Columns.Add("Text");

            dtbStoreChangeStatusDT.Rows.Add(-1, "Trạng thái chuyển kho");
            dtbStoreChangeStatusDT.Rows.Add(0, "Chưa chuyển kho");
            dtbStoreChangeStatusDT.Rows.Add(3, "Đã chuyển kho");

            cboStoreChangeStatusDT.DataSource = dtbStoreChangeStatusDT.Copy();
            cboStoreChangeStatusDT.DisplayMember = "Text";
            cboStoreChangeStatusDT.ValueMember = "Value";
            cboStoreChangeStatusDT.SelectedIndex = 0;


        }
        /// <summary>
        /// Hàm lấy add
        /// </summary>
        private bool ADDNEW()
        {

            btnAdd.Enabled = false;
            toolStripMenuItem1.Enabled = false;
            btnTransfer.Enabled = false;
            try
            {
                ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType =
                    new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo((int)objStoreChangeOrderType.StoreChangeTypeID);
                if (objStoreChangeType == null)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Loại yêu cầu chuyển kho chưa khai báo Loại chuyển kho");
                    return false;
                }
                try
                {
                    intStoreChangeTypeShowProductID = Library.AppCore.AppConfig.GetIntConfigValue("PM_STORECHANGETYPE_SHOWPRODUCT");
                    intCheckCompanyType = objStoreChangeType.CheckCompanyType;
                    intCheckProvinceType = objStoreChangeType.CheckProvinceType;
                    intCheckBranchType = objStoreChangeType.CheckBranchType;
                    //if (objStoreChangeOrderType.StoreChangeTypeID == intStoreChangeTypeShowProductID || objStoreChangeOrderType.ToStoreID > 0)
                    //    cboToStoreID.Enabled = false;
                    //else
                    //    cboToStoreID.Enabled = true;
                    // cboToStoreID.Enabled = false;
                    if (objStoreChangeOrderType.FromStoreID > 0)
                        cboFromStoreID.Enabled = false;
                }
                catch
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin loại yêu cầu chuyển kho");
                }
                objStoreChangeOrder = new StoreChangeOrder();
                objStoreChangeOrder = objPLCStoreChangeOrder.LoadInfo(strStoreChangeOrderID, objStoreChangeOrderType.StoreChangeOrderTypeID);
                if (objStoreChangeOrder == null)
                    throw new Exception();
                txtStoreChangeOrderID.Text = objPLCStoreChangeOrder.GetStoreChangeOrderNewID(SystemConfig.intDefaultStoreID);

                FormState = FormStateType.ADD;

                objStoreChangeOrder.FromStoreID = -1;
                if (objStoreChangeOrder.FromStoreID < 1)
                    objStoreChangeOrder.FromStoreID = SystemConfig.intDefaultStoreID;

                objStoreChangeOrder.ToStoreID = -1;

                dteOrderDate.Value = Globals.GetServerDateTime();
                dteExpiryDate.Value = DateTime.Now.AddDays(1);
                objStoreChangeOrder.IsNew = true;
                objStoreChangeOrder.ISORTHERPRODUCT = true;
                objStoreChangeOrder.InStockStatusID = intInStockStatusID;
                btncancel.Enabled = true;
                Library.AppCore.DataSource.FilterObject.StoreFilter objFromFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objFromFilter.IsCheckPermission = true;
                objFromFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilter.IsCheckPermission = true;
                objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGETO;
                cboFromStoreID.InitControl(false, objFromFilter);
                cboToStoreID.InitControl(false, objStoreFilter);
                cboFromStoreID.SetValue(objStoreChangeOrder.FromStoreID);
                cboToStoreID.SetValue(-1);

                LoadInputStore(-1);

                cboTransportTypeID.ResetDefaultValue();
                cboStoreChangeStatusDT.SelectedValue = 0;

                if (objStoreChangeOrder.InStockStatusID < 0)
                    objStoreChangeOrder.InStockStatusID = 0;
                intInStockStatusID = objStoreChangeOrder.InStockStatusID;

                intFromStore = objStoreChangeOrder.FromStoreID;
                intToStore = -1;




                CreateMainGroupMenuItem();



                if (objStoreChangeOrder.IsExpired)
                {
                    if (objStoreChangeOrder.StoreChangeStatus == 1)
                    {
                        cboStoreChangeStatusDT.Enabled = true;
                        btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.EditFunctionID);
                        if (objStoreChangeOrder.IsReviewed)
                        {
                            btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(strEdit_AFReview) && objStoreChangeOrder.StoreChangeStatus == 0;
                        }

                    }
                }
                if (objStoreChangeOrder.IsDeleted)
                    EnableButton(false);
                if (objStoreChangeOrder.StoreChangeStatus > 0)
                {
                    btnDelete.Enabled = false;
                }
                if (objStoreChangeOrder.OrderDate != null && ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.STORECHANGE,
                    objStoreChangeOrder.OrderDate.Value, objStoreChangeOrder.CreatedStoreID))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Tháng " + objStoreChangeOrder.OrderDate.Value.Month.ToString() + " đã khóa sổ. Bạn không thể chỉnh sửa.");
                    EnableButton(false);
                }

                dteExpiryDate.Enabled = !objStoreChangeOrder.IsReviewed || (objStoreChangeOrder.IsReviewed && objStoreChangeOrder.StoreChangeStatus == 0);
                btnUpdate.Enabled = true;

                tabControl.SelectedTab = tabGeneral;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin yêu cầu chuyển kho");
                SystemErrorWS.Insert("Lỗi nạp thông tin yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private void SetEnable(bool bolStatus)
        {
            btnTransfer.Enabled = bolStatus;
            btnUpdate.Enabled = bolStatus;
            cboFromStoreID.Enabled = bolStatus;
            cboToStoreID.Enabled = bolStatus;
            cboTransferOtherProDt.Enabled = bolStatus;
            cboTransportTypeID.Enabled = bolStatus;
            cboTransportCompanyID.Enabled = bolStatus;
            cboTransportServicesID.Enabled = bolStatus;
            txtContent.Enabled = bolStatus;
        }

        private bool CheckStorePermission(DataTable cbxFromManager, string FromStoreID)
        {
            bool bolcheckstore = false;
            if (cbxFromManager.Select("STOREID=" + FromStoreID).Count() > 0)
            {
                bolcheckstore = true;
            }
            return bolcheckstore;
        }
        /// <summary>
        /// Hàm lấy dữ liệu Chỉnh sửa
        /// </summary>
        /// 


        private bool EditData()
        {
            bool bolCheckStorePermission = false;

            ISADDNEW = true;
            DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            if (row != null)
            {

                btnAdd.Enabled = false;
                toolStripMenuItem1.Enabled = false;
                btncancel.Enabled = true;

                //     strStoreChangeOrderTypeID = Convert.ToString(row["STORECHANGEORDERTYPEID"]);
                strStoreChangeOrderID = Convert.ToString(row["OTHERTRANSFERID"]);
                string STATUSID = Convert.ToString(row["STATUSID"]);


                try
                {
                    ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType =
                        new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo((int)objStoreChangeOrderType.StoreChangeTypeID);

                    if (objStoreChangeType == null)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Loại yêu cầu chuyển kho chưa khai báo Loại chuyển kho");
                        return false;
                    }
                    try
                    {
                        intStoreChangeTypeShowProductID = Library.AppCore.AppConfig.GetIntConfigValue("PM_STORECHANGETYPE_SHOWPRODUCT");
                        intCheckCompanyType = objStoreChangeType.CheckCompanyType;
                        intCheckProvinceType = objStoreChangeType.CheckProvinceType;
                        intCheckBranchType = objStoreChangeType.CheckBranchType;
                        //if (objStoreChangeOrderType.StoreChangeTypeID == intStoreChangeTypeShowProductID || objStoreChangeOrderType.ToStoreID > 0)
                        //    cboToStoreID.Enabled = false;
                        //else
                        //    cboToStoreID.Enabled = true;
                        // cboToStoreID.Enabled = false;


                        cboStoreChangeStatusDT.Enabled = false;
                    }
                    catch
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin loại yêu cầu chuyển kho");
                    }

                    objStoreChangeOrder = objPLCStoreChangeOrder.LoadInfo(strStoreChangeOrderID, objStoreChangeOrderType.StoreChangeOrderTypeID);

                    if (objStoreChangeOrder == null)
                        throw new Exception();
                    if (string.IsNullOrWhiteSpace(strStoreChangeOrderID))
                    {

                        FormState = FormStateType.ADD;
                        txtStoreChangeOrderID.Text = objPLCStoreChangeOrder.GetStoreChangeOrderNewID(SystemConfig.intDefaultStoreID);
                        objStoreChangeOrder.FromStoreID = Convert.ToInt32(objStoreChangeOrderType.FromStoreID);
                        if (objStoreChangeOrder.FromStoreID < 1)
                            objStoreChangeOrder.FromStoreID = SystemConfig.intDefaultStoreID;
                        if (objStoreChangeOrderType.StoreChangeTypeID == intStoreChangeTypeShowProductID)
                            objStoreChangeOrder.ToStoreID = objStoreChangeOrder.FromStoreID;
                        else
                            objStoreChangeOrder.ToStoreID = (int)objStoreChangeOrderType.ToStoreID;
                        btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.CreateFunctionID);
                        dteOrderDate.Value = Globals.GetServerDateTime();
                        dteExpiryDate.Value = DateTime.Now.AddDays(1);
                        objStoreChangeOrder.IsNew = true;
                        objStoreChangeOrder.InStockStatusID = intInStockStatusID;
                    }
                    else
                    {
                        bolCheckStorePermission = CheckStorePermission(cbxFromManager.DataSource, objStoreChangeOrder.FromStoreID.ToString());

                        txtStoreChangeOrderID.Text = objStoreChangeOrder.StoreChangeOrderID.Trim();
                        btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.DeleteFunctionID);
                        btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.EditFunctionID);
                        //if (objStoreChangeOrder.IsReviewed)
                        //{
                        //    btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(strEdit_AFReview);
                        //}
                        dteOrderDate.Value = objStoreChangeOrder.OrderDate.Value;
                        dteExpiryDate.Value = objStoreChangeOrder.ExpiryDate.Value;

                        if (STATUSID == "Chưa chuyển kho")
                        {
                            SetEnable(bolCheckStorePermission);
                        }
                        else
                        {
                            SetEnable(false);
                        }

                    }
                    string nameproducts = objStoreChangeOrder.DtbStoreChangeOrderDetail.Rows[0]["PRODUCTID"].ToString();
                    nameproducts += " - " + objStoreChangeOrder.DtbStoreChangeOrderDetail.Rows[0]["PRODUCTNAME"].ToString();

                    Library.AppCore.DataSource.FilterObject.StoreFilter objFromFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                    objFromFilter.IsCheckPermission = true;
                    objFromFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;

                    Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                    objStoreFilter.IsCheckPermission = true;
                    objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGETO;

                    LoadInputStore(objStoreChangeOrder.FromStoreID);
                    if (bolCheckStorePermission == true)
                    {
                        cboToStoreID.InitControl(false, objStoreFilter);
                        cboFromStoreID.InitControl(false, objFromFilter);
                        cboFromStoreID.SetValue(objStoreChangeOrder.FromStoreID);
                        cboToStoreID.SetValue(objStoreChangeOrder.ToStoreID);
                    }
                    else
                    {
                        cboFromStoreID.InitControl(false, dtbStoreCache);
                        cboToStoreID.InitControl(false, dtbStoreCache);
                        cboFromStoreID.SetValue(objStoreChangeOrder.FromStoreID);
                        cboToStoreID.SetValue(objStoreChangeOrder.ToStoreID);
                    }
                    cboTransferOtherPro_EDIT(nameproducts);
                    if (Convert.ToInt32(objStoreChangeOrder.StoreChangeStatus) == 0)
                    {
                        cboStoreChangeStatusDT.SelectedValue = objStoreChangeOrder.StoreChangeStatus;
                    }
                    else
                    {
                        cboStoreChangeStatusDT.SelectedValue = 3;
                    }
                    cboTransportTypeID.SetValue(objStoreChangeOrder.TransportTypeID);
                    cboTransportTypeID_SelectionChangeCommitted(null, null);
                    cboTransportCompanyID.SetValue(objStoreChangeOrder.TransportCompanyID);
                    cboTransportCompany_SelectionChangeCommitted(null, null);
                    cboTransportServicesID.SetValue(objStoreChangeOrder.TransportServicesID);
                    txtContent.Text = objStoreChangeOrder.Content;
                    if (objStoreChangeOrder.InStockStatusID < 0)
                        objStoreChangeOrder.InStockStatusID = 0;
                    intInStockStatusID = objStoreChangeOrder.InStockStatusID;
                    //if (objStoreChangeOrder.IsNew)
                    //    radioIsNew.Checked = true;
                    //else
                    //    radioIsOld.Checked = true;
                    intFromStore = objStoreChangeOrder.FromStoreID;
                    intToStore = objStoreChangeOrder.ToStoreID;
                    objTransportOrder = new PLC.WSTransportOrder.TP_TransportOrder();
                    objTransportOrder.TransportOrderID = txtStoreChangeOrderID.Text;
                    objTransportOrder.CreatedDate = dteOrderDate.Value;
                    objTransportOrder.OldStoreID = intFromStore.ToString();
                    objTransportOrder.SendFullName = cboFromStoreID.StoreName;
                    objTransportOrder.SendAddress = cboFromStoreID.StoreName;
                    objTransportOrder.SendEmail = "";
                    objTransportOrder.SendPhone = "";
                    objTransportOrder.MapSendProvince = "";
                    objTransportOrder.MapSendDistrict = "";


                    objTransportOrder.ReceivedFullName = cboToStoreID.StoreName;
                    objTransportOrder.ReceivedAddress = cboToStoreID.StoreName;
                    objTransportOrder.ReceivedEmail = "";
                    objTransportOrder.ReceivedPhone = "";
                    objTransportOrder.MapReceivedProvince = "TINH_KHNHAN";
                    objTransportOrder.MapReceivedDistrict = "HUYEN_KHNHAN";

                    objTransportOrder.Description = txtContent.Text;
                    objTransportOrder.CompanyServiceCode = cboTransportTypeID.ColumnName;

                    if (objStoreChangeOrder.DtbReviewLevel == null || objStoreChangeOrder.DtbReviewLevel.Rows.Count == 0)
                    {
                        //cboStatus.Enabled = false;
                        //btnReview.Enabled = false;
                    }
                    //load combobox status cua muc duyet


                    btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(strEdit_AFReview) && objStoreChangeOrder.StoreChangeStatus == 0;

                    btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.DeleteAfterReviewFunctionID);
                    //cboStatus.SelectedIndex = 3;
                    //cboStatus.Enabled = false;
                    //btnReview.Enabled = false;
                    CreateMainGroupMenuItem();



                    if (objStoreChangeOrder.IsExpired)
                    {
                        if (objStoreChangeOrder.StoreChangeStatus == 1)
                        {
                            cboStoreChangeStatusDT.Enabled = true;
                            btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.EditFunctionID);
                            if (objStoreChangeOrder.IsReviewed)
                            {
                                btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(strEdit_AFReview) && objStoreChangeOrder.StoreChangeStatus == 0;
                            }

                        }
                    }
                    if (objStoreChangeOrder.IsDeleted)
                        EnableButton(false);
                    if (objStoreChangeOrder.StoreChangeStatus > 0)
                    {
                        btnDelete.Enabled = false;
                    }
                    if (objStoreChangeOrder.OrderDate != null && ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.STORECHANGE,
                        objStoreChangeOrder.OrderDate.Value, objStoreChangeOrder.CreatedStoreID))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Tháng " + objStoreChangeOrder.OrderDate.Value.Month.ToString() + " đã khóa sổ. Bạn không thể chỉnh sửa.");
                        EnableButton(false);
                    }
                    //if (objStoreChangeOrderType.IsCanAddNewProduct)
                    //{
                    dteExpiryDate.Enabled = !objStoreChangeOrder.IsReviewed || (objStoreChangeOrder.IsReviewed && objStoreChangeOrder.StoreChangeStatus == 0);
                    //  btnUpdate.Enabled = !objStoreChangeOrder.IsReviewed || (objStoreChangeOrder.IsReviewed && objStoreChangeOrder.StoreChangeStatus == 0);
                    //  }

                    tabControl.SelectedTab = tabGeneral;

                }
                catch (Exception objExce)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin yêu cầu chuyển kho");
                    SystemErrorWS.Insert("Lỗi nạp thông tin yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                    return false;
                }
            }
            return false;

        }

        /// <summary>
        /// Tuấn thêm hàm chức năng chuyển kho
        /// Tạo danh sách ngành hàng
        /// </summary>
        private void CreateMainGroupMenuItem()
        {
            try
            {
                ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutpputType = null;
                bool bolIsFullOutput = false;
                if (objStoreChangeType != null)
                {
                    new ERP.MasterData.PLC.MD.PLCOutputType().LoadInfo(ref objOutpputType, (int)objStoreChangeType.OutputTypeID);
                    if (objOutpputType != null)
                        bolIsFullOutput = objOutpputType.IsFullOutput;
                }

                List<string> lstTag = new List<string>();
                DataTable dtbData = new PLCStoreChangeOrder().CreateMainGroupMenuItem(objStoreChangeOrder.StoreChangeOrderID.Trim());
                //mnuSelectMainGroupID.ItemLinks.Clear();

                if (dtbData != null)
                {
                    foreach (DataRow dtRow in dtbData.Rows)
                    {
                        lstTag.Add(Convert.ToString(dtRow["MAINGROUPID"]).Trim());
                        if (!bolIsFullOutput)
                        {
                            DevExpress.XtraBars.BarButtonItem mnuItem = new DevExpress.XtraBars.BarButtonItem();
                            mnuItem.Caption = Convert.ToString(dtRow["MAINGROUPNAME"]).Trim();
                            mnuItem.Tag = Convert.ToString(dtRow["MAINGROUPID"]).Trim();
                            mnuItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuItem_ItemClick);
                            //mnuSelectMainGroupID.AddItem(mnuItem);
                        }

                    }
                }

                if (dtbData == null || dtbData.Rows.Count == 0)
                {

                }
                else
                {

                    DataTable dtbCheckAll = new ERP.Inventory.PLC.PLCDataSource().GetHeavyDataSource("MD_OUPUTTYPE_GETOUTPUTALL",
                        new object[] { "@STORECHANGEORDERID", strStoreChangeOrderID });
                    if (dtbCheckAll != null && dtbCheckAll.Rows.Count > 0)
                    {
                        if (dtbCheckAll.Rows[0]["ISOUTPUTALLMAINGROUP"].ToString() == "1")
                        {
                            DevExpress.XtraBars.BarButtonItem mnuItem = new DevExpress.XtraBars.BarButtonItem();
                            mnuItem.Caption = Convert.ToString("Tất cả").Trim();
                            mnuItem.Tag = string.Join(",", lstTag);
                            mnuItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuItem_ItemClick);
                            //mnuSelectMainGroupID.AddItem(mnuItem);


                            btnTransfer.Tag = string.Join(",", lstTag);

                        }
                    }
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tạo danh sách ngành hàng", objExce.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tạo danh sách ngành hàng", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        #region Hàm tính tổng số lượng, tổng giá trị sản phẩm,VAT...

        private void GetTotalValues(ref decimal decTotalQuantity, ref decimal decTotalAmountBF_Out,
            ref decimal decTotalVAT_Out, ref decimal decTotalAmount_Out, ref decimal decTotalAmountBF_In,
            ref decimal decTotalVAT_In, ref decimal decTotalAmount_In)
        {
            decTotalQuantity = 0;
            decTotalAmountBF_Out = 0;
            decTotalVAT_Out = 0;
            decTotalAmount_Out = 0;

            decTotalAmountBF_In = 0;
            decTotalVAT_In = 0;
            decTotalAmount_In = 0;
            DataRow[] arrRow = dtbStoreChangeDetail.Select("ISSELECT= 1");
            foreach (DataRow objRow in arrRow)
            {
                decTotalQuantity += Convert.ToDecimal(objRow["Quantity"]);
                decimal decTotalCost_Out = Convert.ToDecimal(objRow["Quantity"]) * Convert.ToDecimal(objRow["OutputPrice"]);
                decimal decTotalCost_In = Convert.ToDecimal(objRow["Quantity"]) * Convert.ToDecimal(objRow["InputPrice"]);
                decimal decOutputVAT = Convert.ToDecimal(objRow["OutputVAT"]) * Convert.ToDecimal(objRow["VATPercent"]) / 10000;
                decimal decInputVAT = Convert.ToDecimal(objRow["InputVAT"]) * Convert.ToDecimal(objRow["VATPercent"]) / 10000;
                decTotalAmountBF_Out += decTotalCost_Out;
                decTotalVAT_Out += decTotalCost_Out * decOutputVAT;
                decTotalAmount_Out += decTotalCost_Out * (1 + decOutputVAT);

                decTotalAmountBF_In += decTotalCost_In;
                decTotalVAT_In += decTotalCost_In * decInputVAT;
                decTotalAmount_In += decTotalCost_In * (1 + decInputVAT);
            }
        }


        private Decimal GetTotalAmount()
        {
            int intStorChangeTypeID = cboTransportTypeID.ColumnID;
            if (intStorChangeTypeID != 2 && intStorChangeTypeID != 4) return GetTotalAmountBF();
            Decimal decResult = 0;
            DataRow[] arrRow = dtbStoreChangeDetail.Select("ISSELECT= 1");
            foreach (DataRow objRow in arrRow)
            {
                decResult += Convert.ToDecimal(objRow["TotalCost"]);
            }
            return decResult;
        }

        private decimal GetTotalAmountBF()
        {
            int intStorChangeTypeID = cboTransportTypeID.ColumnID;
            if (intStorChangeTypeID != 2 && intStorChangeTypeID != 4) return 0;
            decimal decResult = 0;
            DataRow[] arrRow = dtbStoreChangeDetail.Select("ISSELECT= 1");
            foreach (DataRow objRow in arrRow)
            {
                decResult += Convert.ToInt32(objRow["Quantity"]) * Convert.ToDecimal(objRow["Price"]);
            }
            return decResult;
        }

        private Decimal GetTotalVAT()
        {
            int intStorChangeTypeID = cboTransportTypeID.ColumnID;
            if (intStorChangeTypeID != 2 && intStorChangeTypeID != 4) return 0;
            Decimal decResult = 0;
            DataRow[] arrRow = dtbStoreChangeDetail.Select("ISSELECT= 1");
            foreach (DataRow objRow in arrRow)
            {
                decResult += Convert.ToInt32(objRow["Quantity"]) * Convert.ToDecimal(objRow["Price"]) * Convert.ToInt32(objRow["VATPercent"]) * Convert.ToInt32(objRow["VAT"]) * Convert.ToDecimal(0.0001);
            }
            return decResult;
        }



        #endregion


        /// <summary>
        /// Tạo bảng chi tiết phiếu xuất chuyển kho
        /// </summary>
        /// <returns></returns>
        public static DataTable InitStoreChangeDetail()
        {
            DataTable tblResult = new DataTable("VoucherDetail");
            DataColumn colIsSelect = new DataColumn("ISSELECT", typeof(Decimal));
            colIsSelect.DefaultValue = 1;
            tblResult.Columns.Add(colIsSelect);
            tblResult.Columns.Add(new DataColumn("PRODUCTID", typeof(String)));
            tblResult.Columns.Add(new DataColumn("PRODUCTNAME", typeof(String)));
            tblResult.Columns.Add(new DataColumn("IMEI", typeof(String)));
            tblResult.Columns.Add(new DataColumn("QUANTITYUNITNAME", typeof(String)));
            tblResult.Columns.Add(new DataColumn("ENDWARRANTYDATE", typeof(DateTime)));
            tblResult.Columns.Add(new DataColumn("QUANTITY", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("PRICE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("VAT", typeof(float)));
            tblResult.Columns.Add(new DataColumn("VATPERCENT", typeof(int)));
            tblResult.Columns.Add(new DataColumn("TOTALCOST", typeof(decimal), "[Quantity] * [Price] + ([Quantity] * [Price]) * [VATPercent]*[VAT]*0.0001"));
            tblResult.Columns.Add(new DataColumn("ISREQUESTIMEI", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("ISHASWARRANTY", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("PACKINGNUMBER", typeof(int)));
            tblResult.Columns.Add(new DataColumn("COSTPRICE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("INPUTVOUCHERDATE", typeof(DateTime)));
            tblResult.Columns.Add(new DataColumn("INPUTCUSTOMERID", typeof(int)));
            tblResult.Columns.Add(new DataColumn("INPUTUSERID", typeof(string)));
            tblResult.Columns.Add(new DataColumn("INPUTTYPEID", typeof(int)));
            tblResult.Columns.Add(new DataColumn("ISSHOWPRODUCT", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("NOTE", typeof(String)));
            tblResult.Columns.Add(new DataColumn("ISRETURNPRODUCT", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("STORECHANGEORDERDETAILID", typeof(String)));
            tblResult.Columns.Add(new DataColumn("ORDERQUANTITY", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("USERNOTE", typeof(String)));
            tblResult.Columns.Add(new DataColumn("OUTPUTPRICE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("INPUTPRICE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("OUTPUTVAT", typeof(int)));
            tblResult.Columns.Add(new DataColumn("INPUTVAT", typeof(int)));
            tblResult.Columns.Add(new DataColumn("MAINGROUPID", typeof(int)));
            tblResult.Columns.Add(new DataColumn("ISALLOWDECIMAL", typeof(bool)));
            tblResult.Columns.Add(new DataColumn("BRANDID", typeof(int)));
            tblResult.Columns.Add(new DataColumn("TOTALWEIGHT", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("TOTALSIZE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("PRODUCTWEIGHT", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("PRODUCTSIZE", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("SHIPPINGCOST", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("TOTALSHIPPINGCOST", typeof(decimal)));
            tblResult.Columns.Add(new DataColumn("IMEI_OLD", typeof(String)));
            tblResult.Columns.Add(new DataColumn("ISFIFOERROR", typeof(int)));
            tblResult.Columns["ISFIFOERROR"].DefaultValue = 0;
            return tblResult;
        }

        #region Tạo phiếu xuất chuyển kho và nhập chuyển kho
        /// <summary>
        /// Tạo phiếu Chuyển kho - Phiếu xuất - Phiếu nhập
        /// IsCreateInOutVoucher = true : Tạo Phiếu thu - Phiếu chi
        /// </summary>
        /// <returns></returns>
        public bool CreateOuputInputVoucher(decimal decTotalQuantity, decimal decTotalAmountBF_Out, decimal decTotalVAT_Out, decimal decTotalAmount_Out, decimal decTotalAmountBF_In, decimal decTotalVAT_In, decimal decTotalAmount_In, bool bolIsCheckRealInput)
        {
            ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
            new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, cboToStoreID.StoreID);
            ERP.MasterData.PLC.MD.WSStore.Store objFromStore = null;
            new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objFromStore, cboFromStoreID.StoreID);
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomerInput = null;
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomerOutput = null;
            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
            String strCustomerName = string.Empty;
            String strStorePhone = string.Empty;
            String strStoreTaxID = string.Empty;
            String strTaxAddress = string.Empty;
            String strCustomerAddress = string.Empty;
            if (objStoreChangeType.IsStoreCustomer)
            {
                if (objFromStore.CustomerID <= 0)
                {
                    MessageBox.Show("Chưa thiết lập khách hàng cho kho [" + objFromStore.StoreName + "] và kho [" + objToStore.StoreName + "]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                if (objToStore.CustomerID <= 0)
                {
                    MessageBox.Show("Chưa thiết lập khách hàng cho kho [" + objToStore.StoreID + " - " + objToStore.StoreName + "]", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                objCustomerInput = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objFromStore.CustomerID));
                objCustomerOutput = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objToStore.CustomerID));
            }
            else
            {
                objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objStoreChangeType.CustomerID));
                strCustomerName = objToStore.StoreName;
                strStorePhone = objToStore.StorePhoneNumber;
                strStoreTaxID = objToStore.TaxCode;
                strTaxAddress = objToStore.TaxAddress;
                strCustomerAddress = objToStore.StoreAddress;
                //Nếu khác công ty
                //if (objStoreChangeType.StoreChangeTypeID == 2 || objStoreChangeType.StoreChangeTypeID == 4)
                if (objStoreChangeType.CheckCompanyType == 2)
                    strCustomerAddress = strTaxAddress;
                //Nếu là Xuất chuyển hàng trưng bày thì IsCheckRealInput = true
                //if (objStoreChangeType.StoreChangeTypeID == 3)
                //{
                //    //bolIsCheckRealInput = objInputType.
                //    bolIsCheckRealInput = true;
                //}
            }
            try
            {
                // ****** Thông tin phiếu xuất ****** //
                PLC.StoreChange.WSStoreChange.OutputVoucher objOutputVoucher = new PLC.StoreChange.WSStoreChange.OutputVoucher();
                objOutputVoucher.OrderID = this.strStoreChangeOrderID;
                objOutputVoucher.CurrencyUnitID = Convert.ToInt32(objStoreChangeType.CurrencyUnitID);
                objOutputVoucher.CurrencyExchange = 1;// decCurrencyExchange;
                objOutputVoucher.InvoiceDate = objStoreChange.StoreChangeDate;
                objOutputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objOutputVoucher.OutputStoreID = cboFromStoreID.StoreID;
                objOutputVoucher.InvoiceSymbol = "";//txtInVoiceSymbol.Text.Trim();
                objOutputVoucher.InvoiceID = "";// txtInVoiceID.Text.Trim();
                objOutputVoucher.Denominator = "";// txtTransportVoucherID.Text.Trim();
                objOutputVoucher.OutputDate = objStoreChange.StoreChangeDate;
                objOutputVoucher.PayableTypeID = Convert.ToInt32(objStoreChangeType.PayableTypeID);
                objOutputVoucher.PayableDate = DateTime.Now;
                objOutputVoucher.Discount = 0;
                objOutputVoucher.DiscountReasonID = Convert.ToInt32(objStoreChangeType.DiscountReasonID);
                objOutputVoucher.TotalAmountBFT = decTotalAmountBF_Out;
                objOutputVoucher.TotalVAT = decTotalVAT_Out;
                objOutputVoucher.TotalAmount = decTotalAmount_Out;
                objOutputVoucher.Denominator = "";
                objOutputVoucher.StaffUser = Library.AppCore.SystemConfig.objSessionUser.UserName; //txtTransportUser.UserName;
                objOutputVoucher.IsError = false;
                objOutputVoucher.ErrorContent = "";
                objOutputVoucher.OutputNote = txtContent.Text;
                if (objCustomerOutput != null)
                {
                    objOutputVoucher.CustomerID = objCustomerOutput.CustomerID;
                    objOutputVoucher.CustomerAddress = objCustomerOutput.CustomerAddress;
                    objOutputVoucher.CustomerName = objCustomerOutput.CustomerName;
                    objOutputVoucher.CustomerPhone = objCustomerOutput.CustomerPhone;
                    objOutputVoucher.CustomerTaxID = objCustomerOutput.CustomerTaxID;
                    objOutputVoucher.TaxCustomerAddress = objCustomerOutput.TaxCustomerAddress;
                    if (objToStore != null)
                    {
                        var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + objToStore.BranchID);
                        if (objBranch != null)
                        {
                            objOutputVoucher.CustomerPhone = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerPhone) ? objBranch[0]["PhoneNumber"].ToString().Trim() : objOutputVoucher.CustomerPhone;
                            objOutputVoucher.TaxCustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.TaxCustomerAddress) ? objBranch[0]["Address"].ToString().Trim() : objOutputVoucher.TaxCustomerAddress;
                            objOutputVoucher.CustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerAddress) ? objBranch[0]["Address"].ToString().Trim() : objOutputVoucher.CustomerAddress;
                            objOutputVoucher.CustomerTaxID = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerTaxID) ? objBranch[0]["TaxCode"].ToString().Trim() : objOutputVoucher.CustomerTaxID;
                        }
                    }
                }
                else
                {
                    if (objCustomer != null)
                    {
                        if (objCustomer.IsDefault)
                        {
                            objOutputVoucher.CustomerID = objCustomer.CustomerID;
                            objOutputVoucher.CustomerAddress = strTaxAddress;
                            objOutputVoucher.CustomerName = strCustomerName;
                            objOutputVoucher.CustomerPhone = strStorePhone;
                            objOutputVoucher.CustomerTaxID = strStoreTaxID;
                            objOutputVoucher.TaxCustomerAddress = objCustomer.TaxCustomerAddress;
                            if (objToStore != null)
                            {
                                var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + objToStore.BranchID);
                                if (objBranch != null)
                                {
                                    objOutputVoucher.CustomerPhone = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerPhone) ? objBranch[0]["PhoneNumber"].ToString().Trim() : objOutputVoucher.CustomerPhone;
                                    objOutputVoucher.TaxCustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.TaxCustomerAddress) ? objBranch[0]["Address"].ToString().Trim() : objOutputVoucher.TaxCustomerAddress;
                                    objOutputVoucher.CustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerAddress) ? objBranch[0]["Address"].ToString().Trim() : objOutputVoucher.CustomerAddress;
                                    objOutputVoucher.CustomerTaxID = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerTaxID) ? objBranch[0]["TaxCode"].ToString().Trim() : objOutputVoucher.CustomerTaxID;
                                }
                            }
                        }
                        else
                        {
                            objOutputVoucher.CustomerID = objCustomer.CustomerID;
                            objOutputVoucher.CustomerAddress = objCustomer.CustomerAddress;
                            objOutputVoucher.CustomerName = objCustomer.CustomerName;
                            objOutputVoucher.CustomerPhone = objCustomer.CustomerPhone;
                            objOutputVoucher.CustomerTaxID = objCustomer.CustomerTaxID;
                            objOutputVoucher.TaxCustomerAddress = objCustomer.TaxCustomerAddress;
                            if (objToStore != null)
                            {
                                var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + objToStore.BranchID);
                                if (objBranch != null)
                                {
                                    objOutputVoucher.CustomerPhone = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerPhone) ? objBranch[0]["PhoneNumber"].ToString().Trim() : objOutputVoucher.CustomerPhone;
                                    objOutputVoucher.TaxCustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.TaxCustomerAddress) ? objBranch[0]["Address"].ToString().Trim() : objOutputVoucher.TaxCustomerAddress;
                                    objOutputVoucher.CustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerAddress) ? objBranch[0]["Address"].ToString().Trim() : objOutputVoucher.CustomerAddress;
                                    objOutputVoucher.CustomerTaxID = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerTaxID) ? objBranch[0]["TaxCode"].ToString().Trim() : objOutputVoucher.CustomerTaxID;
                                }
                            }
                        }
                    }
                }
                objOutputVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objOutputVoucher.IsStoreChange = true;
                objStoreChange.OutputVoucherBO = objOutputVoucher;

                // ****** Thông tin phiếu nhập ****** //
                PLC.StoreChange.WSStoreChange.InputVoucher objInputVoucher = new PLC.StoreChange.WSStoreChange.InputVoucher();
                objInputVoucher.InVoiceID = "";//txtInVoiceID.Text.Trim();
                objInputVoucher.Denominator = "";// txtTransportVoucherID.Text.Trim();
                objInputVoucher.OrderID = this.strStoreChangeOrderID.Trim();
                objInputVoucher.InVoiceSymbol = "";// txtInVoiceSymbol.Text.Trim();
                objInputVoucher.InVoiceDate = objStoreChange.StoreChangeDate;
                objInputVoucher.InputTypeID = Convert.ToInt32(objStoreChangeType.InputTypeID);
                objInputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objInputVoucher.InputDate = objStoreChange.StoreChangeDate;
                objInputVoucher.InputStoreID = cboToStoreID.StoreID;
                if (objCustomerInput != null)
                {
                    objInputVoucher.CustomerID = objCustomerInput.CustomerID;
                    objInputVoucher.CustomerAddress = objCustomerInput.CustomerAddress;
                    objInputVoucher.CustomerName = objCustomerInput.CustomerName;
                    objInputVoucher.CustomerPhone = objCustomerInput.CustomerPhone;
                    objInputVoucher.CustomerTaxID = objCustomerInput.CustomerTaxID;
                }
                else
                {
                    if (objCustomer != null)
                    {
                        objInputVoucher.CustomerID = objCustomer.CustomerID;
                        objInputVoucher.CustomerAddress = objCustomer.CustomerAddress;
                        objInputVoucher.CustomerName = objCustomer.CustomerName;
                        objInputVoucher.CustomerPhone = objCustomer.CustomerPhone;
                        objInputVoucher.CustomerTaxID = objCustomer.CustomerTaxID;
                    }
                }
                objInputVoucher.Discount = 0;
                objInputVoucher.DiscountReasonID = Convert.ToInt32(objStoreChangeType.DiscountReasonID);
                objInputVoucher.PayableTypeID = Convert.ToInt32(objStoreChangeType.PayableTypeID);
                objInputVoucher.PayableDate = DateTime.Now;
                objInputVoucher.TotalAmountBFT = decTotalAmountBF_In;
                objInputVoucher.Denominator = "";
                objInputVoucher.TotalVAT = decTotalVAT_In;
                objInputVoucher.TotalAmount = decTotalAmount_In;
                objInputVoucher.IsNew = objStoreChange.IsNew;
                objInputVoucher.CurrencyUnitID = Convert.ToInt32(objStoreChangeType.CurrencyUnitID);
                objInputVoucher.CurrencyExchange = 1;//decCurrencyExchange;
                objInputVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objInputVoucher.IsCheckRealInput = objInputType.IsAutoCheckRealInput;
                objInputVoucher.CheckRealInputNote = "";
                objInputVoucher.ISStoreChange = true;
                objInputVoucher.Note = txtContent.Text;
                //List<PLC.Input.WSInputVoucher.InputVoucherDetail>
                PLC.Input.WSInputVoucher.InputVoucherDetail objInputVoucherDetail = new PLC.Input.WSInputVoucher.InputVoucherDetail();

                //objInputVoucher.InputVoucherDetailList = 

                objStoreChange.InputVoucherBO = objInputVoucher;

            }
            catch (Exception objEx)
            {

                SystemErrorWS.Insert("Lỗi lấy thông tin phiếu xuất", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi lấy thông tin phiếu xuất \n Bạn vui lòng kiểm tra lại thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        #endregion
        private void LoadInfoFromStoreChangeOrder()
        {
            if (StoreChangeOrderID.Length == 0)
                return;
            PLC.StoreChange.PLCStoreChangeOrder objPLCStoreChangeOrder = new PLC.StoreChange.PLCStoreChangeOrder();
            PLC.PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder = objPLCStoreChangeOrder.LoadInfo(strStoreChangeOrderID, -1);
            if (objStoreChangeOrder == null)
                return;


            cboTransportTypeID.SetValue(objStoreChangeOrder.TransportTypeID);
            //Library.AppCore.LoadControls.ComboBoxObject.SetValue(cboTransportTypeID, objStoreChangeOrder.TransportTypeID);
            intInStockStatusID = objStoreChangeOrder.InStockStatusID;
            //chkIsNew.Checked = objStoreChangeOrder.IsNew;
            //chkIsOld.Checked = !objStoreChangeOrder.IsNew;
            txtStoreChangeOrderID.Text = objStoreChangeOrder.StoreChangeOrderID;
            txtContent.Text = objStoreChangeOrder.Content;
            DataTable tblStoreChangeOrderDetail = null;
            objStoreChangeType = new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo(objStoreChangeOrder.StoreChangeTypeID);
            objInputType = new MasterData.PLC.MD.PLCInputType().LoadInfo(Convert.ToInt32(objStoreChangeType.InputTypeID));
            //new MasterData.PLC.MD.PLCOutputType().LoadInfo(ref objOutputType, Convert.ToInt32(objStoreChangeType.OutputTypeID));
            if (intMainGroupID != 0)
                MainGroupIdlst = intMainGroupID.ToString();
            //if (intMainGroupID != 0)
            //    tblStoreChangeOrderDetail = objPLCStoreChangeOrder.LoadDetailForStoreChange(intMainGroupID, objStoreChangeOrder.StoreChangeOrderID);
            //else
            //{
            //tblStoreChangeOrderDetail = objPLCStoreChangeOrder.LoadDetailForStoreChange(MainGroupIdlst, objStoreChangeOrder.StoreChangeOrderID,0, 0);
            // }

            tblStoreChangeOrderDetail = objStoreChangeOrder.DtbStoreChangeOrderDetail;
            //if (drowListError != null && drowListError.Rows.Count > 0)
            //{
            //    frmStoreChangeOrderDetalError frm = new frmStoreChangeOrderDetalError(drowListError);
            //    frm.ShowDialog();
            //}
            dtbStoreChangeDetail = InitStoreChangeDetail();
            if (tblStoreChangeOrderDetail != null)
            {
                DataRow[] drowList = tblStoreChangeOrderDetail.Select("1=1");
                foreach (DataRow item in drowList)
                {
                    DataRow row = dtbStoreChangeDetail.NewRow();
                    row["ProductID"] = item["ProductID"].ToString().Trim();
                    row["ProductName"] = item["ProductName"];
                    row["IMEI"] = item["IMEI"];
                    if (row.Table.Columns.Contains("IMEI_OLD"))
                        row["IMEI_OLD"] = item["IMEI"];

                    row["QuantityUnitName"] = item["QuantityUnitName"];
                    row["EndWarrantyDate"] = item["ORDERDATE"];

                    row["Price"] = 0;
                    row["VAT"] = 0;
                    row["VATPercent"] = 0;
                    row["CostPrice"] = 0;
                    row["OutputPrice"] = 0;
                    row["InputPrice"] = 0;
                    row["OutputVAT"] = 0;
                    row["InputVAT"] = 0;

                    row["InputVoucherDate"] = item["ORDERDATE"];
                    row["InputCustomerID"] = -1;
                    row["InputUserID"] = 0;
                    row["InputTypeID"] = 0;
                    row["IsRequestIMEI"] = 0;
                    row["IsHasWarranty"] = 1;
                    row["PackingNumber"] = 1;

                    row["IsShowProduct"] = false;


                    row["Note"] = item["Note"];
                    row["IsReturnProduct"] = 0;
                    row["StoreChangeOrderDetailID"] = item["StoreChangeOrderDetailID"];
                    row["OrderQuantity"] = item["Quantity"];
                    row["MainGroupID"] = item["MainGroupID"];
                    row["IsAllowDecimal"] = item["IsAllowDecimal"];
                    row["BrandID"] = 0;
                    row["IsSelect"] = false;
                    if (!Convert.ToBoolean(row["ISREQUESTIMEI"]))
                        row["IsSelect"] = true;
                    if (!string.IsNullOrEmpty((row["IMEI"] ?? "").ToString().Trim()))
                        row["IsSelect"] = true;
                    row["Quantity"] = item["Quantity"];
                    row["TotalWeight"] = 0;
                    row["TotalSize"] = 0;
                    row["TotalShippingCost"] = 0;
                    dtbStoreChangeDetail.Rows.Add(row);
                }
                //String strWarningMsg = AddProductFromStoreChangeOrder(tblStoreChangeOrderDetail);
                //if (strWarningMsg.Length > 0)
                //{
                //    strWarningMsg = "Không thể xuất chuyển kho các mặt hàng sau:" + Environment.NewLine + strWarningMsg;
                //    MessageBox.Show(this, strWarningMsg, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                //    if (objOutputType != null && objOutputType.IsFullOutput)
                //    {
                //        bolIsCloseForm = true;
                //    }
                //}
                if (dtbStoreChangeDetail == null && dtbStoreChangeDetail.Rows.Count <= 0)
                    btnUpdate.Enabled = false;
            }

            //if (objStoreChangeType.MaxModelCount > 0)
            //{
            //    int intCountModel = 0;
            //    if (intCountModel > objStoreChangeType.MaxModelCount)
            //    {
            //        MessageBox.Show(this, "Bạn chỉ được phép xuất tối đa " + Convert.ToString(objStoreChangeType.MaxModelCount) + " mặt hàng\n trong " + objStoreChangeType.StoreChangeTypeName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    }
            //}

            //if (objStoreChangeType.MaxModelAndPriceCount > 0)
            //{
            //    int intCountModelAndPrice = SumDistinctQuantity2();
            //    if (intCountModelAndPrice > objStoreChangeType.MaxModelAndPriceCount)
            //    {
            //        MessageBox.Show(this, "Bạn chỉ được phép xuất tối đa " + Convert.ToString(objStoreChangeType.MaxModelAndPriceCount) + " mặt hàng\n trong " + objStoreChangeType.StoreChangeTypeName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    }
            //}
            //grdInputVoucherDetail.RefreshDataSource();
        }
        private void InsertStoreChange()
        {
            try
            {
                btnUpdate.Enabled = false;


                //Load hình thức xuất chuyển kho
                decimal decTotalQuantity = 0;
                decimal decTotalAmountBF_Out = 0;
                decimal decTotalVAT_Out = 0;
                decimal decTotalAmount_Out = 0;
                decimal decTotalAmountBF_In = 0;
                decimal decTotalVAT_In = 0;
                decimal decTotalAmount_In = 0;

                GetTotalValues(ref decTotalQuantity, ref decTotalAmountBF_Out, ref decTotalVAT_Out, ref decTotalAmount_Out, ref decTotalAmountBF_In, ref decTotalVAT_In, ref decTotalAmount_In);
                objStoreChange = new PLC.StoreChange.WSStoreChange.StoreChange();
                //foreach (var item in dtbStoreChangeDetail.AsEnumerable())
                //{
                //    if (Convert.ToBoolean(item["ISREQUESTIMEI"]))
                //    {
                //        var itemIMEI = dtbSub.AsEnumerable().Where(x => x["IMEI"].ToString().Trim().Equals(item["IMEI"].ToString().Trim())).ToArray();
                //        if (itemIMEI.Any())
                //        {
                //            item["NOTE"] = itemIMEI[0]["NOTE"].ToString().Trim();
                //        }

                //    }
                //    else
                //    {
                //        var itemIMEI = dtbParrent.AsEnumerable().Where(x => x["PRODUCTID"].ToString().Trim().Equals(item["PRODUCTID"].ToString().Trim())).ToArray();
                //        if (itemIMEI.Any())
                //        {
                //            item["NOTE"] = itemIMEI[0]["NOTE"].ToString().Trim();
                //            item["QUANTITY"] = Convert.ToDecimal(itemIMEI[0]["QUANTITY"]);
                //        }
                //    }
                //}
                //Lưu ghi chú và Thêm chi tiết xuất chuyển kho
                //for (int i = 0; i < dtbStoreChangeDetail.Rows.Count; i++)
                //{
                //    dtbStoreChangeDetail.Rows[i]["USERNOTE"] = dtbStoreChangeDetail.Rows[i]["NOTE"];
                //}
                objStoreChange.StoreChangeDetailData = dtbStoreChangeDetail;
                // objStoreChange.StoreChangeDetailData = dtbSdtbStoreChangeDetail_Temp;
                objStoreChange.IsInputFromOrder = true;// bolIsInputFromOrder;//True: Lưu ghi chú
                objStoreChange.IsCreateInOutVoucher = false; //objStoreChangeType.IsCreateInOutVoucher;//True: Tạo phiếu thu chi
                objStoreChange.StoreChangeTypeID = objStoreChangeType.StoreChangeTypeID;
                objStoreChange.StoreChangeOrderID = strStoreChangeOrderID;
                objStoreChange.ToStoreID = cboToStoreID.StoreID;
                objStoreChange.FromStoreID = cboFromStoreID.StoreID;
                objStoreChange.TransportTypeID = Convert.ToInt32(cboTransportTypeID.ColumnID);
                objStoreChange.StoreChangeDate = dteOrderDate.Value;
                objStoreChange.StoreChangeUser = Library.AppCore.SystemConfig.objSessionUser.UserName;//txtTransportUser.UserName.Trim();
                objStoreChange.CaskCode = "";// txtCaskCode.Text;
                //DataRowView drStockStatus = (DataRowView)cboInStockStatusID.SelectedItem;
                objStoreChange.IsNew = true;// Convert.ToBoolean(drStockStatus["ISNEW"]);// radioIsNew.Checked;
                objStoreChange.InStockStatusID = 1;// Convert.ToInt32(drStockStatus["INSTOCKSTATUSID"]);
                objStoreChange.InvoiceID = "000"; //txtInVoiceID.Text;
                objStoreChange.InvoiceSymbol = "000"; // txtInVoiceSymbol.Text;
                objStoreChange.OutputTypeID = Convert.ToInt32(objStoreChangeType.OutputTypeID);
                objStoreChange.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChange.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChange.Content = txtContent.Text;
                objStoreChange.TransportVoucherID = ""; // txtTransportVoucherID.Text;
                objStoreChange.DateReceive = DateTime.Now.AddDays(1);
                objStoreChange.TransferedDate = DateTime.Now.AddDays(1);
                objStoreChange.IsUrgent = false;// bolIsUrgent;
                objStoreChange.FromStoreName = cboFromStoreID.StoreName;
                objStoreChange.ToStoreName = cboToStoreID.StoreName;
                dtbStoreChangeDetail.AcceptChanges();
                //DataTable tblPacking = DataTableClass.SelectDistinct(dtbStoreChangeDetail, "PackingNumber", "1 =1");
                objStoreChange.TotalPacking = 1;
                //if (chkIsReceive.Checked == true)
                //{
                //    objStoreChange.ReceiveNote = txtReceiveNote.Text;
                //    objStoreChange.UserReceive = SystemConfig.objSessionUser.UserName;
                //}
                objStoreChange.ToUser1 = "";// txtToUser1.UserName.Trim();
                objStoreChange.ToUser2 = "";// txtToUser2.UserName.Trim();
                objStoreChange.TotalSize = 0;//  Convert.ToDecimal(lblTotalSize.Text);
                objStoreChange.TotalWeight = 0;//  Convert.ToDecimal(lbTotalWeight.Text);
                // Kiểm tra chuyển bước tự động

                ////Tạo phiếu chi và phiếu thu              
                //if (objStoreChangeType.IsCreateInOutVoucher)
                //{
                //    if (!CreateInOutVoucher(decTotalQuantity, decTotalAmountBF_Out, decTotalVAT_Out, decTotalAmount_Out, decTotalAmountBF_In, decTotalVAT_In, decTotalAmount_In))
                //    {
                //        btnUpdate.Enabled = true;

                //        MessageBox.Show(this, "Lỗi Tạo phiếu chi và phiếu thu\n Bạn vui lòng kiểm tra lại thông tin nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //        return;
                //    }
                //}
                var lstQuantity = dtbStoreChangeDetail.AsEnumerable().Where(x => Convert.ToInt32(x["QUANTITY"]) <= 0 && Convert.ToBoolean(x["ISSELECT"]));
                if (lstQuantity.Count() >= 1)
                {
                    btnUpdate.Enabled = true;

                    MessageBox.Show(this, "Số lượng sản phẩm không được bằng 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                //Tạo phiếu nhập -phiếu xuất
                if (!CreateOuputInputVoucher(decTotalQuantity, decTotalAmountBF_Out, decTotalVAT_Out, decTotalAmount_Out, decTotalAmountBF_In, decTotalVAT_In, decTotalAmount_In, objInputType.IsAutoCheckRealInput || objStoreChange.IsReceive))
                {
                    btnUpdate.Enabled = true;

                    //MessageBox.Show(this, "Lỗi Tạo phiếu nhập -phiếu xuất \n Bạn vui lòng kiểm tra lại thông tin nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                string strOutPutVoucherID = string.Empty;
                List<string> lstStoreChangeID = null;
                int intConfigMaxRowNumber = 0;
                try
                {
                    intConfigMaxRowNumber = Library.AppCore.AppConfig.GetIntConfigValue("VAT_MAXROWNUMBER");
                }
                catch
                {
                    intConfigMaxRowNumber = 15;
                }

                if (!dtbStoreChangeDetail.Columns.Contains("NUMBERLINE")) dtbStoreChangeDetail.Columns.Add("NUMBERLINE");
                List<string> lstVATInvoiceID = null;
                //Lưu database
                if (objStoreChangeOrderType != null)
                {
                    strStoreChangeID = new PLC.StoreChange.PLCStoreChange().InsertStoreChangeNew(dtbStoreChangeDetail, objStoreChange, ref strOutPutVoucherID, ref lstStoreChangeID, ref lstVATInvoiceID, objStoreChangeOrderType.StoreChangeOrderTypeID, objStoreChangeOrderType.CreateVATInvoice, intConfigMaxRowNumber);
                }
                else
                    strStoreChangeID = new PLC.StoreChange.PLCStoreChange().InsertStoreChangeNew(dtbStoreChangeDetail, objStoreChange, ref strOutPutVoucherID, ref lstStoreChangeID, ref lstVATInvoiceID, 0, 0, intConfigMaxRowNumber);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {

                    btnUpdate.Enabled = true;
                    MessageBox.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    if (lstVATInvoiceID != null && lstVATInvoiceID.Count > 0)
                    {
                        foreach (string strVATInvoiceID in lstVATInvoiceID)
                        {
                            if (!string.IsNullOrWhiteSpace(strVATInvoiceID))
                            {
                                ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
                                objPLCPrintVAT.CreateVATSInvoiceByVATInvoiceID(strVATInvoiceID);
                            }
                        }
                    }
                    MessageBox.Show(this, "Chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception objEx)
            {

                SystemErrorWS.Insert("Lỗi xuất chuyển kho", objEx.ToString(), Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xuất chuyển kho! \n Bạn vui lòng kiểm tra lại thông tin nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                ISADDNEW = false;
                btncancel.Enabled = false;
                btnAdd.Enabled = true;
                toolStripMenuItem1.Enabled = true;
            }

            btnUpdate.Enabled = true;
        }
        public void StoreChangeAction(string strMainGroupList)
        {
            if (Globals.DateDiff(dteExpiryDate.Value, Globals.GetServerDateTime(), Globals.DateDiffType.DATE) < 0)
            {
                MessageBox.Show(this, "Yêu cầu này đã hết hạn chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btncancel.Enabled = true;
                return;
            }
            Library.AppCore.DataSource.FilterObject.StoreFilter objFromStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objFromStoreFilter.IsCheckPermission = true;
            objFromStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGE;
            DataTable dtbStore = Library.AppCore.DataSource.GetDataFilter.GetStore(objFromStoreFilter);
            var drStorePermission = dtbStore.Select("STOREID=" + cboFromStoreID.StoreID);
            if (drStorePermission == null || drStorePermission.Count() == 0)
            {
                MessageBox.Show(this, "Bạn không có quyền chuyển kho trên kho xuất " + cboFromStoreID.StoreName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //Lấy danh sách người nhận thông báo
            CreateTableUserNotify();
            GetListNotification_UserList(5, objStoreChangeOrder.FromStoreID, 1);
            GetListNotification_UserList(5, objStoreChangeOrder.ToStoreID, 2);
            GetListNotification_UserList(5, objStoreChangeOrder.CreatedStoreID, 3);

            InsertStoreChange();
            //frmStoreChange frmStoreChange = new StoreChange.frmStoreChange();
            //frmStoreChange.StoreChangeOrderID = txtStoreChangeOrderID.Text.Trim();
            //frmStoreChange.StoreChangeOrderType = objStoreChangeOrderType;
            //if (!strMainGroupList.Contains(","))
            //{
            //    frmStoreChange.MainGroupID = Convert.ToInt32(strMainGroupList);
            //}
            //else
            //    frmStoreChange.ListMainGroupId = strMainGroupList;
            //frmStoreChange.IsRequestIMEI = objStoreChangeOrderType.IsRequestDetailIMEI;
            //frmStoreChange.dtbNotification_User = dtbNotification_User;
            //frmStoreChange.ShowDialog();
            //if (frmStoreChange.IsUpdate)
            //{
            //    this.Close();
            //    this.bolIsHasAction = true;
            //}
        }

        private void mnuItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StoreChangeAction(e.Item.Tag.ToString().Trim());
        }

        private void cboTransferOtherPro_EDIT(string Item)
        {
            cboTransferOtherProDt.SelectedValue = Item;
        }
        private void cboTransferOtherPro_load()
        {
            string TransferOtherProList = Library.AppCore.AppConfig.GetStringConfigValue("TRANSFEROTHERGOODS");
            var listitem1 = TransferOtherProList.Split(',').ToArray();
            if (cboTransferOtherPro.Items.Count == 0 && cboTransferOtherProDt.Items.Count == 0)
            {
                DataTable dtb = new DataTable();
                dtb.Columns.Add("VALUE");
                dtb.Rows.Add("Loại hàng hóa");
                foreach (var item in listitem1)
                {
                    dtb.Rows.Add(item.Trim());
                }

                cboTransferOtherPro.DataSource = dtb.Copy();
                cboTransferOtherPro.DisplayMember = "VALUE";
                cboTransferOtherPro.ValueMember = "VALUE";
                cboTransferOtherProDt.DataSource = dtb.Copy();
                cboTransferOtherProDt.DisplayMember = "VALUE";
                cboTransferOtherProDt.ValueMember = "VALUE";
            }
            cboTransferOtherPro.SelectedIndex = 0;
            cboTransferOtherProDt.SelectedIndex = 0;
            //cboTransferOtherPro.DisplayMember = "Text";
            //cboTransferOtherPro.ValueMember = "Value";
            //cboTransferOtherProDt.DataSource = dtbTransferOtherPro;
            //cboTransferOtherProDt.DisplayMember = "Text";
            //cboTransferOtherProDt.ValueMember = "Value";
        }
        private decimal GetQuantity(ref decimal decTotalQuantity, ref decimal decTotalStoreQuantity)
        {


            return 0;
        }
        /// <summary>
        /// Hàm bind dữ liệu vào object
        /// </summary>
        /// <returns></returns>
        private bool BindObject()
        {
            try
            {
                if (objStoreChangeOrder == null)
                    objStoreChangeOrder = new StoreChangeOrder();
                objStoreChangeOrder.StoreChangeOrderID = txtStoreChangeOrderID.Text.Trim();
                objStoreChangeOrder.StoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                objStoreChangeOrder.StoreChangeTypeID = Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID);
                objStoreChangeOrder.ExpiryDate = dteExpiryDate.Value;
                objStoreChangeOrder.OrderDate = dteOrderDate.Value;
                objStoreChangeOrder.FromStoreName = cboFromStoreID.StoreName;
                objStoreChangeOrder.ToStoreName = cboToStoreID.StoreName;
                objStoreChangeOrder.Content = txtContent.Text;
                objStoreChangeOrder.FromStoreID = cboFromStoreID.StoreID;
                objStoreChangeOrder.ToStoreID = cboToStoreID.StoreID;
                if (cboTransportTypeID.ColumnID > 0)
                    objStoreChangeOrder.TransportTypeID = Convert.ToInt32(cboTransportTypeID.ColumnID);
                //if (cboStoreChangeStatus.SelectedValue != null)
                //    objStoreChangeOrder.StoreChangeStatus = Convert.ToInt32(cboStoreChangeStatus.SelectedValue);
                //else
                //    objStoreChangeOrder.StoreChangeStatus = 0;

                //objStoreChangeOrder.IsUrgent = ckbIsUrgent.Checked;
                objStoreChangeOrder.CreatedStoreID = SystemConfig.intDefaultStoreID;
                decimal decToltal = 1;
                decimal decTotalStoreQuantity = 1;
                GetQuantity(ref decToltal, ref decTotalStoreQuantity);
                objStoreChangeOrder.TotalQuantity = decToltal;
                objStoreChangeOrder.TotalStoreChangeQuantity = decTotalStoreQuantity;
                DataTable DtbStoreChangeOrderDetail = new DataTable();

                DtbStoreChangeOrderDetail.TableName = "DtbStoreChangeOrderDetail";
                DtbStoreChangeOrderDetail.Columns.Add("PRODUCTID", typeof(string));
                DtbStoreChangeOrderDetail.Columns.Add("IMEI", typeof(string));
                DtbStoreChangeOrderDetail.Columns.Add("QUANTITY", typeof(string));
                DtbStoreChangeOrderDetail.Columns.Add("NOTE", typeof(string));
                DtbStoreChangeOrderDetail.Columns.Add("STORECHANGEQUANTITY", typeof(int));

                DataRow row = DtbStoreChangeOrderDetail.NewRow();
                row["PRODUCTID"] = cboTransferOtherProDt.SelectedValue.ToString().Split('-')[0].Trim();
                row["IMEI"] = "";
                row["QUANTITY"] = "1";
                row["NOTE"] = "";
                row["STORECHANGEQUANTITY"] = 0;
                DtbStoreChangeOrderDetail.Rows.Add(row);




                objStoreChangeOrder.DtbStoreChangeOrderDetail = DtbStoreChangeOrderDetail;

                objStoreChangeOrder.DtbStoreChangeOrderDetail.AcceptChanges();

                if (objStoreChangeOrder.IsReviewed)
                    objStoreChangeOrder.DtbReviewLevel = null;
                else
                    objStoreChangeOrder.DtbReviewLevel.AcceptChanges();
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy dữ liệu để xử lý.", ex.ToString());
                return false;
            }
            return true;
        }

        private void GetListNotification_RVL_UserList(int intReviewLevelID, int intStoreID, int intStoreType)
        {
            try
            {
                DataTable dtbUserNotify = Library.AppCore.DataSource.GetDataSource.GetSCOrderType_RVL_Notify();
                if (dtbNotification_User == null)
                    CreateTableUserNotify();
                string strFilter = @"ReviewLevelID={0} and (STOREID={1} and (STORETYPE={2} or STORETYPE=0))";
                strFilter = string.Format(strFilter, intReviewLevelID, intStoreID, intStoreType);
                if (dtbUserNotify != null && dtbUserNotify.Rows.Count > 0)
                {
                    DataRow[] drowList = dtbUserNotify.Select(strFilter);
                    if (drowList != null && drowList.Length > 0)
                    {
                        foreach (var item in drowList)
                        {
                            if (dtbNotification_User.Select(string.Format("UserName='{0}'", item["UserName"])).Length > 0)
                                continue;
                            DataRow row = dtbNotification_User.NewRow();
                            row["USERNAME"] = item["USERNAME"];
                            row["ISSHIFTCONSIDER"] = item["ISSHIFTCONSIDER"];
                            dtbNotification_User.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception objExec)
            {
                SystemErrorWS.Insert("Lỗi lấy danh sách người nhận thông báo", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        private void CreateTableUserNotify()
        {
            dtbNotification_User = new DataTable();
            dtbNotification_User.TableName = "dtbNotification_User";
            dtbNotification_User.Columns.Add("USERNAME", typeof(string));
            dtbNotification_User.Columns.Add("ISSHIFTCONSIDER", typeof(Int16));
        }

        private void GetListNotification_UserList(int intStoreChangeOrderEventID, int intStoreID, int intStoreType)
        {
            try
            {
                DataTable dtbUserNotify = Library.AppCore.DataSource.GetDataSource.GetSCOrderType_Notify();
                if (dtbNotification_User == null)
                    CreateTableUserNotify();
                string strFilter = @"STORECHANGEORDERTYPEID={0} and STORECHANGEORDEREVENTID={1} and (STOREID={2} and (STORETYPE={3} or STORETYPE=0))";
                strFilter = string.Format(strFilter, objStoreChangeOrderType.StoreChangeOrderTypeID, intStoreChangeOrderEventID, intStoreID, intStoreType);
                if (dtbUserNotify != null && dtbUserNotify.Rows.Count > 0)
                {
                    DataRow[] drowList = dtbUserNotify.Select(strFilter);
                    if (drowList != null && drowList.Length > 0)
                    {
                        foreach (var item in drowList)
                        {
                            if (dtbNotification_User.Select(string.Format("UserName='{0}'", item["UserName"])).Length > 0)
                                continue;
                            DataRow row = dtbNotification_User.NewRow();
                            row["USERNAME"] = item["USERNAME"];
                            row["ISSHIFTCONSIDER"] = item["ISSHIFTCONSIDER"];
                            dtbNotification_User.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception objExec)
            {
                SystemErrorWS.Insert("Lỗi lấy danh sách người nhận thông báo", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        /// <summary>
        /// Hàm cập nhật dữ liệu
        /// Thêm, chỉnh sửa
        /// </summary>
        /// <returns>true or false</returns>
        private bool UpdateData()
        {

            string strMessage = string.Empty;
            try
            {
                //Library.AppCore.Forms.frmWaitDialog.Show("", "Đang cập nhật dữ liệu");
                btnUpdate.Enabled = false;
                bool bolIsReview = false;

                //tu dong duyet
                string strError = string.Empty;
                bolIsReview = SetReview(3, true, ref strError, false);
                if (!string.IsNullOrEmpty(strError))
                {
                    //Library.AppCore.Forms.frmWaitDialog.Close();
                    MessageBoxObject.ShowWarningMessage(this, strError);
                    //return false;
                }
                intFromStore = objStoreChangeOrder.FromStoreID;
                intToStore = objStoreChangeOrder.ToStoreID;
                LoadInputStore(objStoreChangeOrder.FromStoreID);
                if (!BindObject())
                {
                    //Library.AppCore.Forms.frmWaitDialog.Close();
                    return false;
                }

                if (FormState == FormStateType.ADD)
                {
                    GetListNotification_UserList(1, objStoreChangeOrder.FromStoreID, 1);
                    GetListNotification_UserList(1, objStoreChangeOrder.ToStoreID, 2);
                    GetListNotification_UserList(1, objStoreChangeOrder.CreatedStoreID, 3);
                    objStoreChangeOrder.UserNotifyData = dtbNotification_User;
                    objStoreChangeOrder.TransportCompanyID = cboTransportCompanyID.ColumnID;
                    objStoreChangeOrder.TransportServicesID = cboTransportServicesID.ColumnID;
                    objStoreChangeOrder.TransportTypeID = cboTransportTypeID.ColumnID;
                    objStoreChangeOrder.StoreChangeStatus = Convert.ToInt32(cboStoreChangeStatusDT.SelectedValue);
                    objStoreChangeOrder.IsReviewed = bolIsReview;
                    if (objStoreChangeOrder.IsReviewed)
                    {

                        objStoreChangeOrder.ReviewedDate = Globals.GetServerDateTime();
                        objStoreChangeOrder.ReviewedUser = SystemConfig.objSessionUser.UserName;
                    }
                    txtStoreChangeOrderID.Text = objPLCStoreChangeOrder.Insert(objStoreChangeOrder);

                    strMessage = "Thêm";
                    strStoreChangeOrderID = string.Empty;
                }
                else if (FormState == FormStateType.EDIT)
                {
                    GetListNotification_UserList(2, objStoreChangeOrder.FromStoreID, 1);
                    GetListNotification_UserList(2, objStoreChangeOrder.ToStoreID, 2);
                    GetListNotification_UserList(2, objStoreChangeOrder.CreatedStoreID, 3);
                    objStoreChangeOrder.UserNotifyData = dtbNotification_User;
                    objStoreChangeOrder.TransportCompanyID = cboTransportCompanyID.ColumnID;
                    objStoreChangeOrder.TransportServicesID = cboTransportServicesID.ColumnID;
                    objStoreChangeOrder.TransportTypeID = cboTransportTypeID.ColumnID;
                    objPLCStoreChangeOrder.Update(objStoreChangeOrder);
                    strMessage = "Cập nhật";
                }
            }
            catch (Exception objExce)
            {
                //Library.AppCore.Forms.frmWaitDialog.Close();
                MessageBoxObject.ShowWarningMessage(this, "Lỗi " + strMessage.ToLower() + " yêu cầu chuyển kho");
                SystemErrorWS.Insert("Lỗi " + strMessage.ToLower() + " yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> UpdateData", DUIInventory_Globals.ModuleName);
                return false;
            }
            finally
            {
                btnUpdate.Enabled = true;
                Library.AppCore.Forms.frmWaitDialog.Close();
            }
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            else
            {
                //đóng form hoặc load lại
                MessageBox.Show(strMessage + " yêu cầu chuyển kho thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //LoadComboBox();


            }

            return true;
        }
        /// <summary>
        /// Hàm kiểm tra dữ liệu
        /// </summary>
        /// <returns></returns>
        private bool ValidateData()
        {

            if (string.IsNullOrEmpty(txtStoreChangeOrderID.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Mã yêu cầu chuyển kho không thể trống!Vui lòng tắt form mở lại.");
                tabControl.SelectedTab = tabGeneral;
                txtStoreChangeOrderID.Focus();
                return false;
            }
            if (string.IsNullOrWhiteSpace(strStoreChangeOrderID))
                dteOrderDate.Value = Globals.GetServerDateTime();
            else
                dteOrderDate.Value = objStoreChangeOrder.OrderDate.Value;
            if (!ERP.MasterData.DUI.Common.CommonFunction.EndDateValidation(dteOrderDate.Value, dteExpiryDate.Value, false))
            {
                MessageBoxObject.ShowWarningMessage(this, "Ngày hết hạn không thể nhỏ hơn ngày yêu cầu!");
                tabControl.SelectedTab = tabGeneral;
                dteExpiryDate.Focus();
                return false;
            }

            if (FormState == FormStateType.ADD && !ValidateStore()) return false;
            if (cboTransportTypeID.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn phương tiện.");
                tabControl.SelectedTab = tabGeneral;
                cboTransportTypeID.Focus();
                return false;
            }
            if (cboTransportCompanyID.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn đối tác.");
                tabControl.SelectedTab = tabGeneral;
                cboTransportCompanyID.Focus();
                return false;
            }
            if (cboTransportServicesID.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn dịch vụ.");
                tabControl.SelectedTab = tabGeneral;
                cboTransportServicesID.Focus();
                return false;
            }
            if (cboTransferOtherProDt.SelectedIndex == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn loại hàng hóa.");
                cboTransferOtherProDt.Focus();
                return false;
            }

            return true;
        }

        private string ValidateImei(string strIMEI, string strProductID, string strProductName)
        {

            //lay nhung imei null
            string strImeiNull = string.Empty;
            //lay nhung imei dat hang 
            string strImeiOder = string.Empty;
            //lay nhung imei da ton tai
            string strImeiExist = string.Empty;
            //lay nhung imei dang la hang trung bay
            //string strImeiShowing = string.Empty;
            int intFromStoreID = cboFromStoreID.StoreID;
            //if (cboFromStoreID.SelectedValue != null)
            //    Int32.TryParse(cboFromStoreID.SelectedValue.ToString(), out intFromStoreID);
            if (!string.IsNullOrWhiteSpace(strIMEI.Trim()))
            {
                //lay nhung imei khong thuoc san pham da chon
                string strImeiNotSameProductID = string.Empty;
                strIMEI = strIMEI.Replace("\r\n", ",").Replace("\n", ",").Replace(";", ",");
                foreach (string imei in strIMEI.Split(','))
                {
                    if (!string.IsNullOrEmpty(imei.Trim()))
                    {
                        //imei khong trung voi ma san pham
                        if (string.Equals(imei.Trim(), strProductID.Trim()))
                        {
                            return "Imei: " + imei.Trim() + " đã trùng với mã sản phẩm. Vui lòng nhập imei khác.";
                        }

                        DataRow[] rowImei = dtb.Select("IMEI = '" + imei.Trim() + "'");

                    }
                }
                if (!string.IsNullOrWhiteSpace(strImeiExist.Trim()))
                {
                    if (strImeiExist[0] == ',') strImeiExist = strImeiExist.Remove(0, 1);
                    //MessageBoxObject.ShowWarningMessage(this, "Imei: " + strImeiExist + " này đã tồn tại rồi. Vui lòng nhập imei khác.");
                    return "Imei: " + strImeiExist + " này đã tồn tại trong danh sách yêu cầu rồi. Vui lòng nhập imei khác.";
                }
                if (!string.IsNullOrWhiteSpace(strImeiNotSameProductID))
                {
                    if (strImeiNotSameProductID[0] == ',') strImeiNotSameProductID = strImeiNotSameProductID.Remove(0, 1);
                    //MessageBoxObject.ShowWarningMessage(this, "Imei: " + strImeiNotSameProductID + " không thuộc sản phẩm " + strProductName);
                    return "Imei: " + strImeiNotSameProductID + " không thuộc sản phẩm " + strProductName;
                }

            }
            if (!string.IsNullOrWhiteSpace(strImeiNull.Trim()))
            {
                if (strImeiNull[0] == ',') strImeiNull = strImeiNull.Remove(0, 1);
                //MessageBoxObject.ShowWarningMessage(this, "Imei: " + strImeiNull + " không tồn tại trong hệ thống.");
                return "Imei: " + strImeiNull + " không tồn tại trong hệ thống.";
            }
            if (!string.IsNullOrWhiteSpace(strImeiOder.Trim()))
            {
                if (strImeiOder[0] == ',') strImeiOder = strImeiOder.Remove(0, 1);
                //MessageBoxObject.ShowWarningMessage(this, "Imei: " + strImeiOder + " đã đặt hàng rồi.");
                return "Imei: " + strImeiOder;
            }
            return string.Empty;
        }

        private bool ValidateStore()
        {
            if (cboFromStoreID.StoreID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho xuất.");
                tabControl.SelectedTab = tabGeneral;
                cboFromStoreID.Focus();
                return false;
            }
            if (cboToStoreID.StoreID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho nhập.");
                tabControl.SelectedTab = tabGeneral;
                cboToStoreID.Focus();
                return false;
            }
            if (cboToStoreID.Enabled)
            {
                if (cboToStoreID.StoreID == cboFromStoreID.StoreID)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Kho nhập phải khác kho xuất.");
                    tabControl.SelectedTab = tabGeneral;
                    cboToStoreID.Focus();
                    return false;
                }
            }

            return true;
        }

        private string ShowReason(string strTitle, int MaxLengthString)
        {
            string strReasion = "-1";
            Library.AppControl.FormCommon.frmInputString frm = new Library.AppControl.FormCommon.frmInputString();
            frm.MaxLengthString = MaxLengthString;
            frm.IsAllowEmpty = false;
            frm.Text = strTitle;
            frm.ShowDialog();
            if (frm.IsAccept)
            {
                strReasion = frm.ContentString;
            }

            return strReasion;
        }


        /// <summary>
        /// Hàm xóa dữ liệu
        /// </summary>
        /// <returns>true or false</returns>
        private bool DeleteData()
        {
            try
            {

                Library.AppCore.DataSource.FilterObject.StoreFilter objFromStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objFromStoreFilter.IsCheckPermission = true;
                objFromStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGE;
                DataTable dtbStore = Library.AppCore.DataSource.GetDataFilter.GetStore(objFromStoreFilter);
                var drStorePermission = dtbStore.Select("STOREID=" + cboFromStoreID.StoreID);
                if (drStorePermission == null || drStorePermission.Count() == 0)
                {
                    MessageBox.Show(this, "Bạn không có quyền chuyển kho trên kho xuất " + cboFromStoreID.StoreName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                if (objStoreChangeOrder == null)
                {
                    MessageBox.Show(this, "Lỗi lấy thông tin yêu cầu chuyển kho để hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                if (objStoreChangeOrder.StoreChangeStatus > 0)
                {
                    MessageBox.Show(this, "Yêu cầu này đã xuất chuyển kho rồi. Không thể hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                //if (string.IsNullOrWhiteSpace(txtContentDeleted.Text))
                //{
                //    MessageBox.Show(this, "Vui lòng nhập lý do hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    tabControl.SelectedTab = tabGeneral;
                //    txtContentDeleted.Focus();
                //    return false;
                //}
                //if (MessageBox.Show(this, "Bạn có chắc muốn hủy yêu cầu chuyển kho đang chọn không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                //    return false;

                //Hiển thị form Nhập lý do hủy

                string Reason = ShowReason("Nội dung hủy", 2000);
                if (Reason == "-1" || string.IsNullOrWhiteSpace(Reason))
                    return false;
                objStoreChangeOrder.ContentDeleted = Reason;
                CreateTableUserNotify();
                GetListNotification_UserList(3, objStoreChangeOrder.FromStoreID, 1);
                GetListNotification_UserList(3, objStoreChangeOrder.ToStoreID, 2);
                GetListNotification_UserList(3, objStoreChangeOrder.CreatedStoreID, 3);
                objStoreChangeOrder.UserNotifyData = dtbNotification_User;
                objPLCStoreChangeOrder.Delete(objStoreChangeOrder);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                MessageBox.Show(this, "Hủy yêu cầu thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SearchData();

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi hủy thông tin yêu cầu chuyển kho");
                SystemErrorWS.Insert("Lỗi hủy thông tin yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> DeleteData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private bool CheckProduct(ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock)
        {

            if (!objProductInStock.IsInstock || objProductInStock.Quantity <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " không tồn kho trong kho " + cboFromStoreID.StoreName);
                return false;
            }

            if (objProductInStock.IsService && !objProductInStock.IsCheckStockQuantity)
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn không được tạo yêu cầu trên ngành hàng này!");
                return false;
            }


            //if (!objProductInStock.IsNew && radioIsNew.Checked)
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " là hàng cũ. Vui lòng chọn lại.");
            //    return false;
            //}
            if (objStoreChangeOrderType.IsRequestDetailIMEI && !objProductInStock.IsRequestIMEI)
            {
                MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + objProductInStock.ProductName + " không có IMEI. Vui lòng nhập sản phẩm khác có IMEI.");
                return false;
            }

            ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
            decimal decOutPutQuantity = 0;// số lượng cho mượn sản phẩm//
            objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOutPutQuantity, cboFromStoreID.StoreID, objProductInStock.ProductID, objProductInStock.IMEI);
            if (string.IsNullOrWhiteSpace(objProductInStock.IMEI))
            {
                if (decOutPutQuantity > 0)
                {
                    objProductInStock.Quantity = objProductInStock.Quantity - decOutPutQuantity;

                }
                if (objProductInStock.Quantity <= 0)
                {
                    MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductID + " - " + objProductInStock.ProductName + " không tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            return true;
        }
        private bool CheckImei(ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock)
        {

            if (!Library.AppCore.Other.CheckObject.CheckIMEI(objProductInStock.IMEI))
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI không đúng định dạng.");
                return false;
            }
            if (objProductInStock.IsOrder)
            {
                MessageBoxObject.ShowWarningMessage(this, "IMEI này đã được đặt trong đơn hàng ( " + objProductInStock.OrderID + " ) rồi. Vui lòng nhập IMEI khác.");
                return false;
            }
            if (objProductInStock.IsShowProduct && MessageBox.Show("IMEI này đang là hàng trưng bày. Bạn có muốn chuyển IMEI này không.", "Thông báo", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
            {
                return false;
            }
            if (objProductInStock.IsDelivery)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI này đã giao cho khách");
                return false;
            }
            else if (!objProductInStock.IsCheckRealInput && objProductInStock.IMEI.Length > 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số IMEI này chưa được xác nhận nhập kho");
                return false;
            }
            else if (objProductInStock.IsOrder) // kiểm tra IMEI có được đặt hàng hay chưa
            {
                string strError = "Số IMEI " + objProductInStock.IMEI + " đã được đặt hàng trong đơn hàng: \"" + objProductInStock.OrderID + "\"";
                MessageBoxObject.ShowWarningMessage(this, strError);
                return false;
            }
            if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI))
            {
                ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                decimal decOutPutQuantity = 0;// số lượng cho mượn sản phẩm//
                objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOutPutQuantity, cboFromStoreID.StoreID, objProductInStock.ProductID, objProductInStock.IMEI);
                if (decOutPutQuantity > 0)
                {
                    MessageBox.Show(this, "IMEI [" + objProductInStock.IMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            if (objProductInStock.StatusFIFOID == 1)
            {
                DialogResult drResult =
                    ClsFIFO.ShowMessengerFIFOList(objProductInStock.StatusFIFO, objProductInStock.ProductID, objProductInStock.IMEI, cboFromStoreID.StoreID, -1);
                // MessageBox.Show(this, objProductInStock.StatusFIFO, "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (drResult != System.Windows.Forms.DialogResult.OK)
                {
                    new PLC.PLCProductInStock().Update_IsOrderingWithIMEI(objProductInStock.IMEI.ToString().Trim(), 0, cboFromStoreID.StoreID);
                    return false;
                }

            }
            if (objProductInStock.StatusFIFOID == 2
                && objProductInStock.StatusFIFO.IndexOf("<2>", 0) < 0
                )
            {
                DialogResult drResult =
                    ClsFIFO.ShowMessengerFIFOList("IMEI " + objProductInStock.IMEI.ToString().Trim() + " vi phạm xuất fifo bạn có muốn tiếp tục?",
                    objProductInStock.ProductID, objProductInStock.IMEI, cboFromStoreID.StoreID, -1);
                //MessageBox.Show(this, , "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (drResult != System.Windows.Forms.DialogResult.OK)
                {
                    new PLC.PLCProductInStock().Update_IsOrderingWithIMEI(objProductInStock.IMEI.ToString().Trim(), 0, cboFromStoreID.StoreID);
                    return false;
                }
            }
            return true;
        }
        private string GetIMEI(string strProduct)
        {
            string strImeiList = string.Empty;

            return strImeiList;
        }
        private bool AddProduct(string strProduct, int intQuantity)
        {

            return true;
        }
        private DataTable CreateTableProduct(DataTable dtbProduct, DataRow[] rowProduct, bool IsReQuestIMEI, int intMaingroupID, string strProductID, string strProductName, string strImei, int intQuantity, int intStoreChangeQuantity, string strNote, decimal QuantityInStock, bool bolIsAllowDecimal)
        {
            DataTable dtb = dtbProduct;
            if (dtb == null)
            {
                dtb = new DataTable("TableProduct");
                DataColumn[] col = new DataColumn[]{new DataColumn("STORECHANGEORDERDETAILID", typeof(String))
                                                ,new DataColumn("STORECHANGEORDERID", typeof(String))
                                                ,new DataColumn("PRODUCTID", typeof(String))
                                                ,new DataColumn("PRODUCTNAME", typeof(String))
                                                ,new DataColumn("IMEI", typeof(String))
                                                ,new DataColumn("QUANTITY", typeof(Decimal))
                                                ,new DataColumn("STORECHANGEQUANTITY", typeof(Decimal))
                                                ,new DataColumn("NOTE", typeof(String))
                                                ,new DataColumn("ISREQUESTIMEI", typeof(Boolean))
                                                ,new DataColumn("QUANTITYINSTOCK", typeof(Decimal))
                                                ,new DataColumn("MAINGROUPID", typeof(Int32))
                                                ,new DataColumn("IsAllowDecimal", typeof(Boolean))

                                                };
                dtb.Columns.AddRange(col);

            }
            if (dtb.Rows.Count == 0)

                if (rowProduct == null || rowProduct.Count() == 0)
                {

                    DataRow row = dtb.NewRow();
                    row["STORECHANGEORDERID"] = strStoreChangeOrderID;
                    row["PRODUCTID"] = strProductID;
                    row["PRODUCTNAME"] = strProductName;
                    row["IMEI"] = strImei;
                    row["QUANTITY"] = bolIsAllowDecimal ? (QuantityInStock < 1 && QuantityInStock > 0) ? QuantityInStock : intQuantity : intQuantity;
                    row["STORECHANGEQUANTITY"] = intStoreChangeQuantity;
                    row["NOTE"] = strNote;
                    row["ISREQUESTIMEI"] = IsReQuestIMEI;
                    row["QUANTITYINSTOCK"] = QuantityInStock;
                    row["MAINGROUPID"] = intMaingroupID;
                    row["ISALLOWDECIMAL"] = bolIsAllowDecimal;
                    if (dtbProduct != null)
                    {
                        var item = dtbProduct.AsEnumerable().FirstOrDefault(x => x.Field<object>("PRODUCTID").ToString().Trim().Equals(strProductID.Trim()));


                    }

                    else dtb.Rows.Add(row);
                }
                //nhap nhieu imei tren 1 dong
                else if (IsReQuestIMEI)
                {
                    try
                    {
                        int index = dtb.Rows.IndexOf(rowProduct[0]);
                        int iQuantity = ERP.MasterData.DUI.Common.CommonFunction.GetQuantityFromIMEI(strImei);
                    }
                    catch (Exception ex)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy thông tin IMEI.", ex.ToString());
                    }
                }
            return dtb;
        }

        private string CheckMainGroupPermission()
        {
            if (strReviewUser == "administrator")
                return string.Empty;
            DataTable dtbMainGroupPermission = objPLCStoreChangeOrder.LoadMainGroupPermission(strReviewUser);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                return SystemConfig.objSessionUser.ResultMessageApp.Message;
            }
            string strMainGroupID = string.Empty;
            return string.Empty;
        }

        int intCurrentReviewLevelID = 0;
        private bool SetReview(int intReviewStatus, bool bolIsReviewAll, ref string strError, bool bolIsCertifyByFinger)
        {
            bool bolIsReview = false;
            string strNote = bolIsCertifyByFinger ? "Duyệt bằng vân tay" : "Duyệt bằng đăng nhập tài khoản";
            //Quyen duyet tren kho
            bool bolFromStorePermission = true;
            bool bolToStorePermission = true;
            if (strReviewUser != "administrator")
            {
                bool bolResult = objPLCStoreChangeOrder.CheckStorePermission(objStoreChangeOrder.FromStoreID, objStoreChangeOrder.ToStoreID, strReviewUser, ref bolFromStorePermission, ref bolToStorePermission);
                if (!bolResult)
                {
                    strError = SystemConfig.objSessionUser.ResultMessageApp.Message;
                    return false;
                }
            }
            //bool bolFromStorePermission = ERP.MasterData.PLC.MD.PLCStore.CheckPermission(objStoreChangeOrder.FromStoreID, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER);
            //bool bolToStorePermission = ERP.MasterData.PLC.MD.PLCStore.CheckPermission(objStoreChangeOrder.ToStoreID, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER);
            //quyet duyet tren nganh hang
            string strMainGroupPermission = CheckMainGroupPermission();


            return bolIsReview;
        }
        private bool Review(int intReviewStatus, bool bolIsCertifyByFinger)
        {
            if (objStoreChangeOrder != null && objStoreChangeOrder.IsExpired)
            {
                MessageBoxObject.ShowWarningMessage(this, "Yêu cầu này đã hết hạn. Vui lòng kiểm tra lại ngày hết hạn.");
                return false;
            }
            if (!ValidateData()) return false;

            //kiem tra quyen tren nganh hang va kho xuat
            string strMessage = string.Empty;
            try
            {
                //int intReviewStatus = cboStatus.SelectedIndex;
                //nieu duyet het thi cap nhat lai IsReview = true
                bool bolIsReview = false;
                string strError = string.Empty;
                bolIsReview = SetReview(intReviewStatus, false, ref strError, bolIsCertifyByFinger);
                if (!string.IsNullOrEmpty(strError))
                {

                    MessageBoxObject.ShowWarningMessage(this, strError);
                    return false;
                }
                if (!BindObject())
                {

                    return false;
                }
                objStoreChangeOrder.IsReviewed = bolIsReview;

                if (objStoreChangeOrder.IsReviewed)
                {

                    objStoreChangeOrder.ReviewedDate = Globals.GetServerDateTime();
                    objStoreChangeOrder.ReviewedUser = strReviewUser;
                }
                GetListNotification_RVL_UserList(intCurrentReviewLevelID, objStoreChangeOrder.FromStoreID, 1);
                GetListNotification_RVL_UserList(intCurrentReviewLevelID, objStoreChangeOrder.ToStoreID, 2);
                GetListNotification_RVL_UserList(intCurrentReviewLevelID, objStoreChangeOrder.CreatedStoreID, 3);
                objStoreChangeOrder.UserNotifyData = dtbNotification_User;
                objPLCStoreChangeOrder.Update(objStoreChangeOrder);
                Library.AppCore.Forms.frmWaitDialog.Close();
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi duyệt yêu cầu chuyển kho");
                SystemErrorWS.Insert("Lỗi duyệt yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> ReviewData", DUIInventory_Globals.ModuleName);
                return false;
            }

            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            else
            {
                //đóng form hoặc load lại
                MessageBox.Show("Duyệt yêu cầu chuyển kho thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Focus();
            }
            return true;
        }
        #endregion

        #region Create Grid
        private void CreateColumnGridProduct(bool bolAllowEdit)
        {
            string[] fieldNames = new string[] { "STORECHANGEORDERDETAILID", "STORECHANGEORDERID", "MAINGROUPID", "MAINGROUPNAME", "PRODUCTID", "PRODUCTNAME", "IMEI", "QUANTITY", "STORECHANGEQUANTITY", "NOTE", "ISREQUESTIMEI" };
        }
        private bool FormatGridProduct()
        {
            try
            {


                DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repSpinQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
                repSpinQuantity.MaxLength = 10;
                repSpinQuantity.IsFloatValue = true;
                repSpinQuantity.Mask.EditMask = "#,###,###,###,##0.##";
                repSpinQuantity.MaxLength = 50;
                repSpinQuantity.NullText = "0";
                //repSpinQuantity.MaxValue = new decimal(new int[] { -727379969, 232, 0, 0 });
                repSpinQuantity.Buttons.Clear();


                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtNote.MaxLength = 2000;


                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtImei.CharacterCasing = CharacterCasing.Upper;

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách chi tiết yêu cầu chuyển kho");
                SystemErrorWS.Insert("Định dạng lưới danh sách chi tiết yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> FormatGridProduct", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void CreateColumnGridReview(bool bolAllowEdit)
        {
            string[] fieldNames = new string[] { "REVIEWLEVELID", "STORECHANGEORDERTYPEID", "REVIEWORDER", "REVIEWLEVELNAME", "REVIEWEDUSER", "REVIEWEDUSFULLNAME",
                "REVIEWSTATUS","REVIEWSTATUSNAME" ,"ISREVIEWED", "REVIEWEDDATE", "NOTE", "STOREPERMISSIONTYPE", "MAINGROUPPERMISSIONTYPE" };

        }
        private bool FormatGridReview()
        {
            try
            {

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách chi tiết yêu cầu chuyển kho");
                SystemErrorWS.Insert("Định dạng lưới danh sách chi tiết yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> FormatGridProduct", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        #endregion Create Grid

        #region Form Status
        /// <summary>
        /// Các trạng thái của Form
        /// </summary>
        enum FormStateType
        {
            LIST,
            ADD,
            EDIT
        }

        FormStateType intFormState = FormStateType.LIST;
        FormStateType FormState
        {
            set
            {

                //Kiểm tra quyền:
                intFormState = value;
                if (intFormState == FormStateType.ADD)
                {
                    CreateColumnGridProduct(true);
                    FormatGridProduct();

                }
                else if (intFormState == FormStateType.EDIT)
                {

                    CreateColumnGridProduct(!IsReviewed);
                    FormatGridProduct();
                    cboFromStoreID.Enabled = false;
                    cboToStoreID.Enabled = false;
                    //cboTransportTypeID.Enabled = false;
                    //radioIsNew.Enabled = false;
                    //radioIsOld.Enabled = false;
                    //cboStatus.SelectedIndex = 0;
                    //ckbIsDeleted.Enabled = true;
                    //ckbIsReview.Enabled = true;
                    txtStoreChangeOrderID.BackColor = System.Drawing.SystemColors.Info;

                }
            }
            get
            {
                return intFormState;
            }
        }

        private void CreateReviewItem()
        {
            //DevExpress.XtraBars.BarButtonItem barItemReview0 = new DevExpress.XtraBars.BarButtonItem();
            //DevExpress.XtraBars.BarButtonItem barItemReview1 = new DevExpress.XtraBars.BarButtonItem();
            //DevExpress.XtraBars.BarButtonItem barItemReview2 = new DevExpress.XtraBars.BarButtonItem();
            //DevExpress.XtraBars.BarButtonItem barItemReview3 = new DevExpress.XtraBars.BarButtonItem();

            //barItemReview0.Caption = "Chưa duyệt";
            //barItemReview0.Name = "0";
            //barItemReview0.Tag = "0";
            //barItemReview0.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
            //pMnuReview.AddItem(barItemReview0);

            //barItemReview1.Caption = "Đang xử lý";
            //barItemReview1.Name = "1";
            //barItemReview1.Tag = "1";
            //barItemReview1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
            //pMnuReview.AddItem(barItemReview1);

            //barItemReview2.Caption = "Từ chối";
            //barItemReview2.Name = "2";
            //barItemReview2.Tag = "2";
            //barItemReview2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
            //pMnuReview.AddItem(barItemReview2);

            //barItemReview3.Caption = "Đồng ý";
            //barItemReview3.Name = "3";
            //barItemReview3.Tag = "3";
            //barItemReview3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
            //pMnuReview.AddItem(barItemReview3);

        }

        private void barItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!ValidateStore())
                return;
            int intReviewStatus = Convert.ToInt32(e.Item.Tag);
            Review(intReviewStatus, false);
        }
        #endregion
        private void cboInStockStatusID_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void btnReview_Click(object sender, EventArgs e)
        {
            //Library.AppCore.DataSource.FilterObject.StoreFilter objFromStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            //objFromStoreFilter.IsCheckPermission = true;
            //objFromStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGE;
            //DataTable dtbStore = Library.AppCore.DataSource.GetDataFilter.GetStore(objFromStoreFilter);
            //var drStorePermission = dtbStore.Select("STOREID=" + cboFromStoreID.StoreID);
            //if (drStorePermission == null || drStorePermission.Count() == 0)
            //{
            //    MessageBox.Show(this, "Bạn không có quyền chuyển kho trên kho xuất " + cboFromStoreID.StoreName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            if (objStoreChangeOrderType.IsReviewByTransType)
            {

                DataTable dtbTransportTypeCurrent = (DataTable)cboTransportTypeID.DataSource;
                if (cboTransportTypeID.ColumnID > 0)
                {
                    int intTransportTypeIDcurent = cboTransportTypeID.ColumnID;
                    //int.TryParse((cboTransportTypeID.ColumnID.ToString() ?? "0").ToString().Trim(), out intTransportTypeIDcurent);
                    string strReviewByTranSportID = "";
                    DataRow[] drs = dtbTransportTypeCurrent.Select("TRANSPORTTYPEID = " + intTransportTypeIDcurent.ToString());
                    if (drs != null && drs.Length > 0)
                        strReviewByTranSportID = (drs[0]["REVIEWFUNCTIONID"] ?? "").ToString().Trim();
                    if (string.IsNullOrEmpty(strReviewByTranSportID))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Chưa có quyền duyệt!");
                        return;
                    }


                }

            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Chưa có quyền duyệt!");
                return;
            }

            //Check Yes/No trước khi tiếp tục
            DialogResult dialogResult = MessageBox.Show(this, "Bạn có muốn duyệt yêu cầu này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult != DialogResult.Yes)
                return;
            if (!ValidateStore())
                return;
            if (objStoreChangeOrder != null && objStoreChangeOrder.IsExpired)
            {
                MessageBoxObject.ShowWarningMessage(this, "Yêu cầu này đã hết hạn. Vui lòng kiểm tra lại ngày hết hạn.");
                return;
            }
            if (!ValidateData())
                return;
            List<string> lstPermission = new List<string>();
            lstPermission.Add(objStoreChangeOrderType.ReviewFunctionID);

            //Hiếu: Chỉnh duyệt thẳng bằng user đang đăng nhập

            //Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Duyệt yêu cầu chuyển kho", "Duyệt", null, lstPermission, !bolIsOnlyCertifyFinger, true);
            //frmCertifyLogin1.ShowDialog(this);
            //if (!frmCertifyLogin1.IsCorrect)
            //{
            //    return;
            //}
            // strReviewUser = frmCertifyLogin1.UserName;
            // bolIsCertifyByFinger = frmCertifyLogin1.IsCertifyByFinger;
            strReviewUser = SystemConfig.objSessionUser.UserName.ToString().Trim();

            //Review(3, bolIsCertifyByFinger);
            Review(3, false);
            this.bolIsHasAction = true;
        }

        private void mnuItemExportTemplate_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.GridControl grdTemp = new DevExpress.XtraGrid.GridControl();
            DevExpress.XtraGrid.Views.Grid.GridView grvTemp = new DevExpress.XtraGrid.Views.Grid.GridView();
            grdTemp.MainView = grvTemp;
            string[] fieldNames = new string[] { "Mã sản phẩm", "IMEI", "Số lượng" };

            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdTemp, fieldNames);
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdTemp);
        }

        private void mnuImportExcel_Click(object sender, EventArgs e)
        {
            if (FormState != FormStateType.ADD)
            {
                return;
            }
            try
            {
                if (!ValidateStore()) return;
                DataTable dtbExcelData = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(3);
                if (dtbExcelData == null || dtbExcelData.Columns.Count == 0)
                {
                    return;
                }
                if (dtbExcelData.Rows.Count < 2)
                {
                    MessageBox.Show("Tập tin excel không có dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                else if (dtbExcelData.Columns.Count < 3 || dtbExcelData.Rows[0][1].ToString().ToUpper().Trim() != "IMEI")
                {
                    MessageBox.Show("Tập tin excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                dtbExcelData.Rows.RemoveAt(0);
                dtbExcelData.AcceptChanges();
                // Check Validate


                foreach (DataRow rBarcode in dtbExcelData.Rows)
                {
                    KeyPressEventArgs eventBarcode = new KeyPressEventArgs((char)Keys.Enter);
                    bolIsImport = true;


                }
            }
            catch { }
            finally
            {

            }
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            //mnuItemDelete.Enabled = grdViewProduct.FocusedRowHandle >= 0 && (!ckbIsReview.Checked || intFormState == FormStateType.ADD);
            //if (cboFromStoreID.StoreID > 0 && cboToStoreID.StoreID > 0 && !ckbIsReview.Checked)
            //    mnuItemImportIMEI.Enabled = bolInputIMEI && !txtSearchProduct.ReadOnly;
            //else
            //    mnuItemImportIMEI.Enabled = false;
            //mnuItemExport.Enabled = grdViewProduct.RowCount > 0;
        }

        private void grdViewProduct_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

        }

        private void mnuItemImportIMEI_Click(object sender, EventArgs e)
        {
            DataTable dtbImeiPara = new DataTable();
            dtbImeiPara.Columns.Add("IMEI");
            frmPopUpIMEI frm = new frmPopUpIMEI();

            frm.ProductConsignmentTypeBySCOTID = dtbProductConsignmentTypeBySCOTID;
            frm.ShowDialog();
            try
            {
                if (frm.dtbResult != null && frm.dtbResult.Rows.Count > 0)
                {
                    DataTable dtb = frm.dtbResult.AsEnumerable().CopyToDataTable();


                    DataTable dtbProduct = dtb.DefaultView.ToTable(true, "PRODUCTID");
                    if (dtbProduct != null && dtbProduct.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbProduct.Rows.Count; i++)
                        {
                            MasterData.PLC.MD.WSProduct.Product objProduct = MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(dtbProduct.Rows[0]["PRODUCTID"].ToString().Trim());
                            if (objProduct != null)
                            {
                                string strProductID = objProduct.ProductID.Trim();
                                string strProductName = objProduct.ProductName.Trim();
                                int intMainGroupID = objProduct.MainGroupID;
                                bool bolIsAllowDecimal = objProduct.IsAllowDecimal;
                            }
                        }
                        string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbImeiPara, "IMEI");
                        new PLC.PLCProductInStock().Update_IsOrdering(xmlIMEI, 1, cboFromStoreID.StoreID);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void rbtnIMEI_CheckedChanged(object sender, EventArgs e)
        {
            if (!bolIsLoadForm)
                return;




        }



        #region Check Import san pham
        private bool ValidateImportExcel(string strProduct, int intQuantity, out string strError)
        {
            strError = string.Empty;
            if (string.IsNullOrWhiteSpace(strProduct))
                return false;
            int intFromStoreID = cboFromStoreID.StoreID;
            string strImeiList = string.Empty;
            ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = null;
            return true;
        }
        #endregion

        private void cboTransportTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {

            //if (objStoreChangeOrderType.IsReviewByTransType)
            //{
            //    DataTable dtbTransportTypeCurrent = (DataTable)cboTransportTypeID.DataSource;
            //    if (cboTransportTypeID.SelectedValue != null)
            //    {
            //        int intTransportTypeIDcurent = -1;
            //        int.TryParse((cboTransportTypeID.SelectedValue ?? "").ToString().Trim(), out intTransportTypeIDcurent);
            //        string strReviewByTranSportID = "";
            //        DataRow[] drs = dtbTransportTypeCurrent.Select("TRANSPORTTYPEID = " + intTransportTypeIDcurent.ToString());
            //        if (drs != null && drs.Length > 0)
            //            strReviewByTranSportID = (drs[0]["REVIEWFUNCTIONID"] ?? "").ToString().Trim();


            //    }
            //}
            //btnUpdate.Enabled = true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchData();
        }
        private void clearControl()
        {
            cboFromStoreID.SetValue(-1);
            cboToStoreID.SetValue(-1);
            cboTransportTypeID.SetValue(-1);
            cboTransportCompanyID.SetValue(-1);
            cboTransportServicesID.SetValue(-1);
            cboStoreChangeStatusManager.SelectedIndex = 0;
            cboStoreChangeStatusDT.SelectedIndex = 0;
            cboTransferOtherPro.SelectedIndex = 0;
            txtContent.Text = "";
            cboTransferOtherProDt.SelectedIndex = 0;

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            clearControl();
            SetEnable(true);
            ISADDNEW = true;
            btnDelete.Enabled = false;
            ADDNEW();

            //cboTransferOtherPro_load();
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {

            FormState = FormStateType.EDIT;
            EditData();

        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            ISADDNEW = false;
            btnTransfer.Enabled = false;
            btncancel.Enabled = false;
            btnAdd.Enabled = true;
            toolStripMenuItem1.Enabled = true;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
            tabControl.SelectedTab = TabManager;

        }

        private void tabControl_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTab = TabManager;
        }

        private bool SendService(string strWorkFlowID)
        {
            DataRow[] rowCompany = dtbTransportCompany.Select("TRANSPORTCOMPANYID = " + 1);
            if (rowCompany.Length > 0 && Convert.ToBoolean(rowCompany[0]["TRANSPORTGATEWAY"]))
            {

                if (string.IsNullOrEmpty(strWorkFlowID))
                {
                    objTransportOrder.TransportGateWay = true;
                    List<PLC.WSTransportOrder.TP_TransportOrder_WorkFlow> lstWorkFlow = new List<PLC.WSTransportOrder.TP_TransportOrder_WorkFlow>();
                    PLC.WSTransportOrder.TP_TransportOrder_WorkFlow objWorkFlow = new PLC.WSTransportOrder.TP_TransportOrder_WorkFlow();
                    objWorkFlow.RealTransportID = objTransportOrder.RealTransportID;
                    objWorkFlow.TransportorderID = objTransportOrder.TransportOrderID;
                    objWorkFlow.TransportStatusID = 1;
                    objWorkFlow.TransportStatusName = "";
                    lstWorkFlow.Add(objWorkFlow);
                    objTransportOrder.TransportOrder_WorkFlowList = lstWorkFlow.ToArray();
                }


                var objResultMessage = objPLCStoreChangeOrder.SendRequest(objTransportOrder, strWorkFlowID);
                if (objResultMessage.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi gọi Service gửi vận đơn!", objResultMessage.MessageDetail, DUIInventory_Globals.ModuleName + "->SendService");
                    return false;
                }
                else
                {
                    MessageBox.Show("Gửi vận đơn thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //frmTransportOrderEdit(objTransportOrder.TransportOrderID);
                    return true;
                }
            }
            return true;


        }
        private void btnTransfer_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;
            if (!UpdateData())
                return;
            LoadInfoFromStoreChangeOrder();
            StoreChangeAction(btnTransfer.Tag.ToString().Trim());

            SearchData();
            tabControl.SelectedTab = TabManager;
            btnTransfer.Enabled = false;
            btnUpdate.Enabled = false;
            //btncancel.Enabled = false;
            btnDelete.Enabled = false;
            //LoadStoreChangeStatus(true);
        }

        private void tabGeneral_Enter(object sender, EventArgs e)
        {
            if (ISADDNEW == false)
            {
                tabControl.SelectedTab = TabManager;
            }
        }

        private void TabManager_Enter(object sender, EventArgs e)
        {
            if (ISADDNEW == true)
            {
                tabControl.SelectedTab = tabGeneral;
            }
        }

        private void cboTransportTypeID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (this.cboTransportTypeID.ColumnID > 0)
            {
                DataRow[] dr = dtbTransportCompany.Select("TransportTypeID=" + cboTransportTypeID.ColumnID);
                if (dr.Any())
                    cboTransportCompanyID.InitControl(false, dr.CopyToDataTable(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đối tác--");
                else
                    cboTransportCompanyID.InitControl(false, dtbTransportCompany.Clone(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đối tác--");
            }
            else
                cboTransportCompanyID.InitControl(false, dtbTransportCompany.Clone(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đối tác--");

        }

        private void cboTransportCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (this.cboTransportCompanyID.ColumnID > 0)
            {
                DataRow[] dr = dtbTransportServices.Select("TRANSPORTCOMPANYID=" + cboTransportCompanyID.ColumnID);
                if (dr.Any())
                    cboTransportServicesID.InitControl(false, dr.CopyToDataTable(), "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--Chọn dịch vụ--");
                else
                    cboTransportServicesID.InitControl(false, dtbTransportServices.Clone(), "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--Chọn dịch vụ--");
            }
            else
                cboTransportServicesID.InitControl(false, dtbTransportServices.Clone(), "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--Chọn dịch vụ--");
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            btnAdd_Click(null, null);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            btnDelete_Click(null, null);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            btnTransfer_Click(null, null);
        }
    }
}
