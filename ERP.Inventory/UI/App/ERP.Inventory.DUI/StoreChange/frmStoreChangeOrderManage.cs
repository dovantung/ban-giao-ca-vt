﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC.PM;
using ERP.Inventory.PLC.StoreChange;
using ERP.Inventory.PLC.PM.WSStoreChangeOrder;
using Library.AppCore.LoadControls;
using System.Collections;
using DevExpress.XtraGrid.Views.Grid;
using Library.AppCore.Other;
using ERP.MasterData.PLC.MD;
using Library.AppCore.DataSource.FilterObject;

namespace ERP.Inventory.DUI.StoreChange
{
    /// <summary>
    /// Created by  :   Hoang Nhu Phong
    /// Description :   Quản lý yêu cầu chuyển kho
    /// </summary>
    public partial class frmStoreChangeOrderManager : Form
    {
        private PLCStore objPLCStore = new PLCStore();
        private PLCStoreChangeOrder objPLCStoreChangeOrder = new PLCStoreChangeOrder();
        Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection selection;
        private int intBillGoodsReportID = 0;
        private int intBillGoodsReportID1 = 0;
        private int intBillReportID = 0;//In sản phẩm theo kho
        private int intBillStoreReportID = 0;//In phiếu nhặt hàng
        private int intBillStoreChangeRecordsID = 0; //In biên bản bàn giao hàng hóa
        private int intBillReportDetailID = 0;//In phiếu nhặt hàng riêng lẻ
        private int intStoreChangeCommandReportID = -1; //In lệnh điều động nội bộ
        private int intStoreChangeCommandReportID1 = -1; //In lệnh điều động nội bộ
        private bool bolIsOnlyCertifyFinger = false;
        private const string STORECHANGEORDERDETAILREPORT_VIEW = "STORECHANGEORDERDETAILREPORT_VIEW";
        private string strPermission_Delete = "PM_SCORDER_DELETE_AFTER_REVIEW";
        private DataTable dtbStoreCache = null;
        DataTable dtb = null;

        public frmStoreChangeOrderManager()
        {
            InitializeComponent();
        }
        #region Event

        private void frmStoreChangeOrderManage_Load(object sender, EventArgs e)
        {
            CreateColumnGrid();
            InitControl();
            LoadCombo();
            LoadInfoFromTag();

            txtKeyWords.Focus();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            SearchData();
        }
        private void mnuItemExport_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);

        }

        #endregion Event
        #region Method
        private void InitControl()
        {
            dteFromDate.Value = DateTime.Now;
            dteToDate.Value = DateTime.Now;
            txtKeyWords.KeyPress += new KeyPressEventHandler(delegate (object sender, KeyPressEventArgs e) { ERP.MasterData.DUI.Common.CommonFunction.EnterClickButton(btnSearch, sender, e); });
            cboIsReview.SelectedIndex = 0;
            btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
        }
        private void LoadCombo()
        {
          
            StoreFilter objStoreFilter = new StoreFilter();
            //objStoreFilter.IsCheckPermission = true;
            //objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.VIEWSTORECHANGEORDER;
            //Load Kho
            //Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            //objStoreFilter.IsCheckPermission = true;
            //objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.;
            //object[] objKeyWords = new object[] { "@ISDELETED", 0 };
            //MasterData.PLC.MD.WSStore.ResultMessage objResultMessage = objPLCStore.SearchData(ref dtbStoreCache, objKeyWords);
            //if (objResultMessage.IsError)
            //{
            //    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
            //    return;
            //}

            //object[] objKeyWordsViewSCO = new object[] { "@ISDELETED", 0, "@USERNAME", SystemConfig.objSessionUser.UserName };
            //MasterData.PLC.MD.WSStore.ResultMessage objResultMessageViewSCO = objPLCStore.SearchData(ref dtbStoreFilterPermissionViewSCO, objKeyWords);
            //if (objResultMessageViewSCO.IsError)
            //{
            //    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessageViewSCO.Message, objResultMessageViewSCO.MessageDetail);
            //    return;
            //}
            //    cboFromStoreID.InitControl(true,true,objStoreFilter)
            //-------------------------------------------------
            dtbStoreCache = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            ERP.MasterData.PLC.MD.PLCStore plc = new PLCStore();
            DataTable dtF = null;
            DataTable dtT = null;
            object[] objKeyWords = new object[]
                {
                    "@USERNAME", "administrator",
                    "@ISDELETED", 0
                };
            plc.SearchData(ref dtF, objKeyWords);
            if (dtF != null)
            {
                dtT = dtF.Copy();
            }
        
            Library.AppCore.LoadControls.SetDataSource.SetStoreChangeType(this, cboSCType);
            //cboFromStoreID.InitControl(true, dtbStoreCache.Select("ISACTIVE=1").CopyToDataTable());
            //cboToStoreID.InitControl(true, dtbStoreCache.Select("ISACTIVE=1").CopyToDataTable());
            cboFromStoreID.InitControl(true, dtF);
            cboToStoreID.InitControl(true, dtT);
            cboMainGroup.InitControl(false, true);
            cboSubGroupIDList.InitControl(true, -1);
            Library.AppCore.LoadControls.SetDataSource.SetStoreChangeOrderType(this, cboSCOrderType);
            cboFromStoreID.IsReturnAllWhenNotChoose = true;
            cboToStoreID.IsReturnAllWhenNotChoose = true;
            cboFromStoreID.SetValue(SystemConfig.intDefaultStoreID);
            //Trạng thái duyệt
            //DataTable dtbReviewStatus = new DataTable();
            //dtbReviewStatus.Columns.Add("ReviewStatusID", typeof(int));
            //dtbReviewStatus.Columns.Add("ReviewStatusName", typeof(string));
            //DataRow row0 = dtbReviewStatus.NewRow();
            //row0["ReviewStatusID"] = 0;
            //row0["ReviewStatusName"] = "Chưa duyệt";
            //DataRow row1 = dtbReviewStatus.NewRow();
            //row1["ReviewStatusID"] = 1;
            //row1["ReviewStatusName"] = "Đang xử lý";
            //DataRow row2 = dtbReviewStatus.NewRow();
            //row2["ReviewStatusID"] = 2;
            //row2["ReviewStatusName"] = "Từ chối";
            //DataRow row3 = dtbReviewStatus.NewRow();
            //row3["ReviewStatusID"] = 3;
            //row3["ReviewStatusName"] = "Đồng ý";
            //dtbReviewStatus.Rows.Add(row0);
            //dtbReviewStatus.Rows.Add(row1);
            //dtbReviewStatus.Rows.Add(row2);
            //dtbReviewStatus.Rows.Add(row3);
            //cboReviewStatusList.InitControl(true, dtbReviewStatus, "ReviewStatusID", "ReviewStatusName", "--Chọn trạng thái duyệt--");

            //Trạng thái chuyển
            DataTable dtbStoreChangeStatus = new DataTable();
            dtbStoreChangeStatus.Columns.Add("StoreChangeStatusID", typeof(int));
            dtbStoreChangeStatus.Columns.Add("StoreChangeStatusName", typeof(string));
            DataRow dr0 = dtbStoreChangeStatus.NewRow();
            dr0["StoreChangeStatusID"] = 0;
            dr0["StoreChangeStatusName"] = "Chưa chuyển kho";
            DataRow dr1 = dtbStoreChangeStatus.NewRow();
            dr1["StoreChangeStatusID"] = 1;
            dr1["StoreChangeStatusName"] = "Đã chuyển nhưng chưa xong";
            DataRow dr2 = dtbStoreChangeStatus.NewRow();
            dr2["StoreChangeStatusID"] = 2;
            dr2["StoreChangeStatusName"] = "Đã chuyển kho xong";
            DataRow dr3 = dtbStoreChangeStatus.NewRow();
            dr3["StoreChangeStatusID"] = 3;
            dr3["StoreChangeStatusName"] = "Kết thúc";
            dtbStoreChangeStatus.Rows.Add(dr0);
            dtbStoreChangeStatus.Rows.Add(dr1);
            dtbStoreChangeStatus.Rows.Add(dr2);
            dtbStoreChangeStatus.Rows.Add(dr3);
            cboStoreChangeStatusIDList.InitControl(true, dtbStoreChangeStatus, "StoreChangeStatusID", "StoreChangeStatusName", "--Chọn trạng thái chuyển kho--");

            ERP.MasterData.DUI.Common.CommonFunction.SetDropDownWidth(cboSCOrderType);
            ERP.MasterData.DUI.Common.CommonFunction.SetDropDownWidth(cboSCType);
            ERP.MasterData.DUI.Common.CommonFunction.SetDropDownWidth(cboSearchBy);
            cboSearchBy.SelectedIndex = 0;

        }
        private bool ValidateData()
        {
            if (dteToDate.Value.Subtract(dteFromDate.Value).Days > 31)
            {
                MessageBox.Show(this, "Vui lòng xem trong vòng 1 tháng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dteFromDate.Focus();
                return false;
            }
            if (txtKeyWords.Text.Length < 5 && cboSearchBy.SelectedIndex == 3)
            {
                MessageBox.Show(this, "Vui lòng nhập ít nhất 5 ký tự khi tìm kiếm theo IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtKeyWords.Focus();
                return false;
            }
            return true;
        }
        public void SearchData()
        {
            if (!ValidateData()) return;
            btnSearch.Enabled = false;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                btnSearch.Enabled = false;
           
                if (selection != null)
                    selection.ClearSelection();
                int storeChangeOrderType = -1;
                if (cboSCOrderType.SelectedValue != null)
                    storeChangeOrderType = Convert.ToInt32(cboSCOrderType.SelectedValue);
                int storeChangeType = -1;
                if (cboSCType.SelectedValue != null)
                    storeChangeType = Convert.ToInt32(cboSCType.SelectedValue);
                //int fromStore = cboFromStoreID.StoreID;
                //int toStore = cboToStoreID.StoreID;
                int intMainGroup = cboMainGroup.MainGroupID;

                //-----Filter kho theo theo quyền ISCANVIEWSTORECHANGEORDER (Nếu user != admin)
                string strFromStoreIDList = string.Empty;
                string strToStoreIDList = string.Empty;

                //if (cboFromStoreID.DataSource.Rows.Count > 0)
                strFromStoreIDList = cboFromStoreID.StoreIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                //if (cboToStoreID.DataSource.Rows.Count > 0)
                strToStoreIDList = cboToStoreID.StoreIDList.Replace("><", ",").Replace("<", "").Replace(">", "");


                object[] objKeyWords = new object[]
                {
                    "@STORECHANGEORDERTYPE", storeChangeOrderType,
                    "@STORECHANGETYPE", storeChangeType,
                    "@CREATEUSER", ucCreateUser.UserName.Trim(),
                    "@FROMSTOREIDLIST", strFromStoreIDList,
                    "@TOSTOREIDLIST", strToStoreIDList,
                    "@FROMDATE", ERP.MasterData.DUI.Common.DateTimeProcessor.FromDate(dteFromDate.Value),
                    "@TODATE", ERP.MasterData.DUI.Common.DateTimeProcessor.ToDate(dteToDate.Value),
                    "@ISREVIEW", cboIsReview.SelectedIndex - 1,
                    "@STORECHANGESTATUSIDLIST", cboStoreChangeStatusIDList.ColumnIDList.Replace("><",",").Replace("<","").Replace(">",""),
                    "@MAINGROUPID", intMainGroup,
                    "@KEYWORDS", txtKeyWords.Text.Trim(),
                    "@SEARCHBY", cboSearchBy.SelectedIndex,
                    "@ISDELETE", chkDelete.Checked,
                    "@ISEXPIRE", chkExpire.Checked,
                    "@ISURGENT", chkUrgent.Checked,
                    "@SUBGROUPIDLIST",cboSubGroupIDList.SubGroupIDList
                };

                dtb = objPLCStoreChangeOrder.SearchData(objKeyWords);
                this.Cursor = Cursors.Default;
                this.Refresh();
                btnSearch.Enabled = true;
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSearch.Enabled = true;
                    return;
                }
                grdData.DataSource = dtb;
                //Hiển thị Lý do hủy 
                if (chkDelete.Checked)
                {
                    grdViewData.Columns["DELETEDUSERNAME"].Visible = true;
                    grdViewData.Columns["DELETEDDATE"].Visible = true;
                    grdViewData.Columns["CONTENTDELETED"].Visible = true;
                    grdViewData.Columns["DELETEDUSERNAME"].VisibleIndex = 14;
                    grdViewData.Columns["DELETEDDATE"].VisibleIndex = 15;
                    grdViewData.Columns["CONTENTDELETED"].VisibleIndex = 16;
                }
                else
                {
                    grdViewData.Columns["DELETEDUSERNAME"].Visible = false;
                    grdViewData.Columns["DELETEDDATE"].Visible = false;
                    grdViewData.Columns["CONTENTDELETED"].Visible = false;
                }

                if (selection == null)
                {
                    selection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, grdViewData);
                    selection.CheckMarkColumn.VisibleIndex = 0;
                    selection.CheckMarkColumn.Width = 40;
                }

                if (!string.IsNullOrEmpty(txtKeyWords.Text.Trim())
                    && grdViewData.DataRowCount == 1)
                {
                    DataRow row = grdViewData.GetDataRow(0) as DataRow;
                    if (row["STORECHANGEORDERID"].ToString().Trim() == txtKeyWords.Text.Trim())
                    {
                        try
                        {
                            string strStoreChangeOrderTypeID = Convert.ToString(row["STORECHANGEORDERTYPEID"]);
                            string strStoreChangeOrderID = Convert.ToString(row["STORECHANGEORDERID"]);
                            frmStoreChangeOrder frmStoreChangeOrder1 = new frmStoreChangeOrder();
                            frmStoreChangeOrder1.Tag = "StoreChangeOrderType = " + strStoreChangeOrderTypeID;
                            frmStoreChangeOrder1.StoreChangeOrderID = strStoreChangeOrderID;
                            frmStoreChangeOrder1.IsOnlyCertifyFinger = bolIsOnlyCertifyFinger;
                            frmStoreChangeOrder1.FormManager = this;
                            frmStoreChangeOrder1.ShowDialog();
                            if (frmStoreChangeOrder1.IsHasAction)
                                btnSearch_Click(null, null);
                        }
                        catch
                        {
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi tìm kiếm thông tin yêu cầu chuyển kho", ex.ToString());

            }
            btnSearch.Enabled = true;
        }
        private void CreateColumnGrid()
        {
            string[] fieldNames = new string[] { "STORECHANGECOMMANDID", "STORECHANGEORDERTYPEID", "STORECHANGEORDERTYPENAME", "STORECHANGEORDERID", "FROMSTOREID", "TOSTOREID", "ORDERDATE", "EXPIRYDATE", "CREATEUSERNAME", "STORECHANGESTATUS",
                "REVIEWSTATUS", "PRODUCTSTATUSNAME","CONTENT", "REVIEWEDUSER",
                "REVIEWEDDATE","DELETEDUSERNAME","DELETEDDATE" , "CONTENTDELETED",
                "TRANSPORTCOMPANYID","TRANSPORTSERVICESID","TRANSPORTCOMPANYNAME","TRANSPORTSERVICENAME"};
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            FormatGrid();
        }
        private bool FormatGrid()
        {
            try
            {
                grdViewData.Columns["STORECHANGECOMMANDID"].Visible = true;
                grdViewData.Columns["STORECHANGEORDERTYPEID"].Visible = false;
                grdViewData.Columns["TRANSPORTCOMPANYID"].Visible = false;
                grdViewData.Columns["TRANSPORTSERVICESID"].Visible = false;
                grdViewData.Columns["TRANSPORTCOMPANYNAME"].Visible = false;
                grdViewData.Columns["TRANSPORTSERVICENAME"].Visible = false; 
                grdViewData.Columns["STORECHANGEORDERTYPEID"].OptionsColumn.ShowInCustomizationForm = false;
                grdViewData.Columns["STORECHANGEORDERTYPENAME"].Visible = true;
                grdViewData.Columns["STORECHANGEORDERID"].Visible = true;
                grdViewData.Columns["FROMSTOREID"].Visible = true;
                grdViewData.Columns["TOSTOREID"].Visible = true;
                grdViewData.Columns["ORDERDATE"].Visible = true;
                grdViewData.Columns["EXPIRYDATE"].Visible = true;
                grdViewData.Columns["STORECHANGESTATUS"].Visible = true;
                grdViewData.Columns["CREATEUSERNAME"].Visible = true;
                grdViewData.Columns["REVIEWSTATUS"].Visible = true;
                grdViewData.Columns["CONTENT"].Visible = true;
                grdViewData.Columns["REVIEWEDUSER"].Visible = true;
                grdViewData.Columns["REVIEWEDDATE"].Visible = true;
                grdViewData.Columns["DELETEDUSERNAME"].Visible = false;
                grdViewData.Columns["DELETEDDATE"].Visible = false;
                grdViewData.Columns["CONTENTDELETED"].Visible = false;
                grdViewData.Columns["PRODUCTSTATUSNAME"].Visible = true;


                grdViewData.Columns["STORECHANGECOMMANDID"].Caption = "Mã lệnh chuyển kho";
                grdViewData.Columns["STORECHANGEORDERTYPENAME"].Caption = "Loại yêu cầu";
                grdViewData.Columns["STORECHANGEORDERID"].Caption = "Mã yêu cầu";
                grdViewData.Columns["FROMSTOREID"].Caption = "Kho xuất";
                grdViewData.Columns["TOSTOREID"].Caption = "Kho nhập";
                grdViewData.Columns["ORDERDATE"].Caption = "Ngày yêu cầu";
                grdViewData.Columns["EXPIRYDATE"].Caption = "Ngày hết hạn";
                grdViewData.Columns["CREATEUSERNAME"].Caption = "Người tạo";
                grdViewData.Columns["STORECHANGESTATUS"].Caption = "Trạng thái chuyển";
                grdViewData.Columns["REVIEWSTATUS"].Caption = "Trạng thái duyệt";
                grdViewData.Columns["CONTENT"].Caption = "Nội dung";
                grdViewData.Columns["REVIEWEDUSER"].Caption = "Người duyệt";
                grdViewData.Columns["REVIEWEDDATE"].Caption = "Ngày duyệt";
                grdViewData.Columns["DELETEDUSERNAME"].Caption = "Người hủy";
                grdViewData.Columns["DELETEDDATE"].Caption = "Ngày hủy";
                grdViewData.Columns["CONTENTDELETED"].Caption = "Lý do hủy";
                grdViewData.Columns["PRODUCTSTATUSNAME"].Caption = "Trạng thái sản phẩm";

                grdViewData.OptionsView.ColumnAutoWidth = false;
                //Sumary
                grdViewData.Columns["STORECHANGECOMMANDID"].Width = 140;
                grdViewData.Columns["STORECHANGEORDERTYPENAME"].Width = 150;
                grdViewData.Columns["STORECHANGEORDERID"].Width = 140;
                grdViewData.Columns["FROMSTOREID"].Width = 200;
                grdViewData.Columns["TOSTOREID"].Width = 200;
                grdViewData.Columns["ORDERDATE"].Width = 80;
                grdViewData.Columns["ORDERDATE"].DisplayFormat.FormatString = ERP.MasterData.DUI.Common.CommonFunction.strFormmatDatetime;
                grdViewData.Columns["ORDERDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                grdViewData.Columns["EXPIRYDATE"].Width = 80;
                grdViewData.Columns["EXPIRYDATE"].DisplayFormat.FormatString = ERP.MasterData.DUI.Common.CommonFunction.strFormmatDatetime;
                grdViewData.Columns["EXPIRYDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                grdViewData.Columns["REVIEWEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
                grdViewData.Columns["REVIEWEDDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                grdViewData.Columns["STORECHANGESTATUS"].Width = 120;
                grdViewData.Columns["CREATEUSERNAME"].Width = 220;
                grdViewData.Columns["REVIEWSTATUS"].Width = 100;
                grdViewData.Columns["CONTENT"].Width = 220;
                grdViewData.Columns["REVIEWEDUSER"].Width = 220;
                grdViewData.Columns["REVIEWEDDATE"].Width = 120;
                grdViewData.Columns["DELETEDUSERNAME"].Width = 220;
                grdViewData.Columns["DELETEDDATE"].Width = 120;
                grdViewData.Columns["DELETEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
                grdViewData.Columns["DELETEDDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                grdViewData.Columns["CONTENTDELETED"].Width = 220;
                grdViewData.Columns["TRANSPORTCOMPANYID"].Width = 220;


                //Change filter Mode
                grdViewData.Columns["STORECHANGEORDERTYPENAME"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["STORECHANGEORDERID"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["FROMSTOREID"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["TOSTOREID"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["ORDERDATE"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["EXPIRYDATE"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["REVIEWEDDATE"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["STORECHANGESTATUS"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["CREATEUSERNAME"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["REVIEWSTATUS"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["CONTENT"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["PRODUCTSTATUSNAME"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["REVIEWEDUSER"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["REVIEWEDDATE"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["DELETEDUSERNAME"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["DELETEDDATE"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns["CONTENTDELETED"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                //Change Filter Contains
                grdViewData.Columns["STORECHANGEORDERTYPENAME"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["STORECHANGEORDERID"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["FROMSTOREID"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["TOSTOREID"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["ORDERDATE"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["EXPIRYDATE"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["REVIEWEDDATE"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["STORECHANGESTATUS"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["CREATEUSERNAME"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["REVIEWSTATUS"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["CONTENT"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["REVIEWEDUSER"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["REVIEWEDDATE"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["DELETEDUSERNAME"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["DELETEDDATE"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["CONTENTDELETED"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["PRODUCTSTATUSNAME"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["STORECHANGEORDERTYPENAME"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                    new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "STORECHANGEORDERTYPENAME", "Tổng cộng:")});
                grdViewData.Columns["STORECHANGEORDERID"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                    new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "STORECHANGEORDERID", "{0:N0}")});

                grdViewData.ColumnPanelRowHeight = 50;
                grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = true;
                grdViewData.OptionsView.ShowGroupPanel = false;
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách yêu cầu chuyển kho");
                SystemErrorWS.Insert("Định dạng lưới danh sách  yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> FormatGridProduct", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool LoadInfoFromTag()
        {
            if (this.Tag == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy tham số mã báo cáo in phiếu lấy hàng");
                return false;
            }
            Hashtable hstbParam = ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
           
            try
            {
                bolIsOnlyCertifyFinger = Convert.ToString(hstbParam["IsOnlyCertifyFinger"]).Trim().Equals("1") ? true : false;

                string lstBillGoddsReportID = Convert.ToString(hstbParam["BillGoodsReportID"]);
                if (!string.IsNullOrWhiteSpace(lstBillGoddsReportID))
                {
                    string[] arrayReportID = lstBillGoddsReportID.Split(',');
                    if (arrayReportID.Length > 0)
                        intBillGoodsReportID = Convert.ToInt32(arrayReportID[0]);
                    if (arrayReportID.Length > 1)
                        intBillGoodsReportID1 = Convert.ToInt32(arrayReportID[1]);
                }

                if (!Convert.IsDBNull(hstbParam["StoreChangeCommandReportID"]))
                {
                    intStoreChangeCommandReportID = Convert.ToInt32(hstbParam["StoreChangeCommandReportID"]);
                    mnuItemPrintReport.Visible = intStoreChangeCommandReportID > 0;
                }
                if (!Convert.IsDBNull(hstbParam["StoreChangeCommandReportID1"]))
                {
                    intStoreChangeCommandReportID1 = Convert.ToInt32(hstbParam["StoreChangeCommandReportID1"]);
                    mnuItemPrintReport.Visible = intStoreChangeCommandReportID > 0;
                }

                intBillReportID = Convert.ToInt32(hstbParam["BillReportID"]);
                intBillStoreReportID = Convert.ToInt32(hstbParam["BillStoreReportID"]);
                intBillStoreChangeRecordsID = Convert.ToInt32(hstbParam["BillStoreChangeRecordsID"]);
                int.TryParse(hstbParam["BillReportDetailID"].ToString(), out intBillReportDetailID);
            }
            catch
            {
                intBillGoodsReportID = 0;
                intBillGoodsReportID1 = 0;
                bolIsOnlyCertifyFinger = false;
            }
            itemPrintOrder.Enabled = intBillGoodsReportID > 0;
            itemPrintOrderByProduct.Enabled = intBillGoodsReportID1 > 0;
            return true;
        }
        #endregion Method

        private void grdViewData_DoubleClick(object sender, EventArgs e)
        {
            DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            try
            {
                string strStoreChangeOrderTypeID = Convert.ToString(row["STORECHANGEORDERTYPEID"]);
                string strStoreChangeOrderID = Convert.ToString(row["STORECHANGEORDERID"]);
                string strTRANSPORTCOMPANYID = Convert.ToString(row["TRANSPORTCOMPANYID"]);
                string strTRASPORTSERVICEID = Convert.ToString(row["TRANSPORTSERVICESID"]);
                string TRANSPORTCOMPANYNAME = Convert.ToString(row["TRANSPORTCOMPANYNAME"]);
                string TRANSPORTSERVICENAME = Convert.ToString(row["TRANSPORTSERVICENAME"]);
                bool bolIsReviewed = Convert.ToBoolean(row["ISREVIEWED"]);
                frmStoreChangeOrder frmStoreChangeOrder1 = new frmStoreChangeOrder(bolIsReviewed);
                frmStoreChangeOrder1.Tag = "StoreChangeOrderType = " + strStoreChangeOrderTypeID;
                frmStoreChangeOrder1.StoreChangeOrderID = strStoreChangeOrderID;
                frmStoreChangeOrder1.strTRANSPORTCOMPANYID = strTRANSPORTCOMPANYID;
                frmStoreChangeOrder1.strTRASPORTSERVICEID = strTRASPORTSERVICEID;
                frmStoreChangeOrder1.IsOnlyCertifyFinger = bolIsOnlyCertifyFinger;
                frmStoreChangeOrder1.FormManager = this;
                frmStoreChangeOrder1.IsReviewed = bolIsReviewed;                
                frmStoreChangeOrder1.ShowDialog();
                if (frmStoreChangeOrder1.IsHasAction)
                    btnSearch_Click(null, null);
            }
            catch (Exception)
            {
                //MessageBoxObject.ShowWarningMessage(this, "Lỗi khi chọn yêu cầu để chỉnh sửa.");
                return;
            }
        }

        private void itemPrintOrder_Click(object sender, EventArgs e)
        {
            if (selection == null || selection.SelectedCount <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn các yêu cầu chuyển kho cần in");
                return;
            }
            string strStoreChangeOrderIDList = ",";
            for (int i = 0; i < selection.SelectedCount; i++)
            {

                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                strStoreChangeOrderIDList += Convert.ToString(objRow["STORECHANGEORDERID"]) + ",";

            }
            ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intBillGoodsReportID);
            if (objReport == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in phiếu lấy hàng");
                return;
            }
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
            htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
            htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
            htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
            htbParameterValue.Add("STORECHANGEORDERIDLIST", strStoreChangeOrderIDList);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        private void itemPrintOrderByProduct_Click(object sender, EventArgs e)
        {
            if (selection == null || selection.SelectedCount <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn các yêu cầu chuyển kho cần in");
                return;
            }
            string strStoreChangeOrderIDList = ",";
            for (int i = 0; i < selection.SelectedCount; i++)
            {

                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                strStoreChangeOrderIDList += Convert.ToString(objRow["STORECHANGEORDERID"]) + ",";

            }
            ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intBillGoodsReportID1);
            if (objReport == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in phiếu lấy hàng");
                return;
            }
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
            htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
            htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
            htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
            htbParameterValue.Add("STORECHANGEORDERIDLIST", strStoreChangeOrderIDList);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        private void itemPrintOrderByStore_Click(object sender, EventArgs e)
        {
            if (selection == null || selection.SelectedCount <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn các yêu cầu chuyển kho cần in");
                return;
            }
            string strStoreChangeOrderIDList = ",";
            for (int i = 0; i < selection.SelectedCount; i++)
            {

                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                strStoreChangeOrderIDList += Convert.ToString(objRow["STORECHANGEORDERID"]) + ",";

            }
            ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intBillReportID);
            if (objReport == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in phiếu lấy hàng nhóm theo kho");
                return;
            }
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
            htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
            htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
            htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
            htbParameterValue.Add("STORECHANGEORDERIDLIST", strStoreChangeOrderIDList);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        private void itemPrintOrderRecords_Click(object sender, EventArgs e)
        {
            if (selection == null || selection.SelectedCount <= 0 || selection.SelectedCount > 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chỉ chọn một yêu cầu để in biên bản");
                return;
            }

            string strStoreChangeOrderID = string.Empty;
            string strStoreChangeOrderTypeID = string.Empty;
            for (int i = 0; i < selection.SelectedCount; i++)
            {
                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                strStoreChangeOrderID = Convert.ToString(objRow["STORECHANGEORDERID"]);
                strStoreChangeOrderTypeID = Convert.ToString(objRow["STORECHANGEORDERTYPEID"]);
            }
            ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intBillStoreChangeRecordsID);
            if (objReport == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin in biên bản bàn giao");
                return;
            }
            PLCStoreChangeOrder objPLCStoreChangeOrder = new PLCStoreChangeOrder();
            StoreChangeOrder objStoreChangeOrder = null;
            objStoreChangeOrder = objPLCStoreChangeOrder.LoadInfo(strStoreChangeOrderID, Convert.ToInt32(strStoreChangeOrderTypeID));
            if (objStoreChangeOrder == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin để in biên bản");
                return;
            }
            DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            StringBuilder strFilter = new StringBuilder();
            strFilter.Append("StoreID = " + objStoreChangeOrder.FromStoreID.ToString());
            DataView dv = new DataView();
            dv = dtbStore.DefaultView;
            dv.RowFilter = strFilter.ToString();

            PLCStoreChange objPLCStoreChange = new PLCStoreChange();
            DataTable dtbStoreChange = null;
            dtbStoreChange = objPLCStoreChange.GetStoreChange_BySCOID(strStoreChangeOrderID.ToString().Trim());

            string strToUser = dtbStoreChange.Rows[0]["FULLNAME"].ToString().Trim();
            string strPrintDate = dtbStoreChange.Rows[0]["STORECHANGEDATE"].ToString().Trim();
            string strCode = objPLCStoreChange.GetStoreChangeOrder_GetCode(strStoreChangeOrderID.ToString().Trim());
            string strStoreName = dv.ToTable().Rows[0]["STORENAME"].ToString();
            string strStoreAddress = dv.ToTable().Rows[0]["STOREADDRESS"].ToString();

            //Lấy người nhận: 

            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("PrintDate", Convert.ToDateTime(strPrintDate));
            htbParameterValue.Add("OrderReason", objStoreChangeOrder.Content.Trim());
            htbParameterValue.Add("StoreAddress", strStoreAddress.Trim());
            htbParameterValue.Add("StoreName", strStoreName.Trim());
            htbParameterValue.Add("ToUser", strToUser);
            htbParameterValue.Add("Code", strCode);
            htbParameterValue.Add("STORECHANGEORDERID", strStoreChangeOrderID);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            
            mnuItemExport.Enabled = grdViewData.DataRowCount > 0;

            itemPrintOrderRecords.Enabled = false;
            itemPrintOrderRecords.Visible = false;
            itemPrintInputOrder.Visible = false;
            //itemPrintInputOrderDetail.Visible = false;
            itemPrintOrder.Visible = false;
            itemPrintOrderByStore.Visible = false;
            itemPrintOrderByProduct.Visible = false;
            if (selection == null || selection.SelectedCount <= 0 || selection.SelectedCount > 1)
                itemPrintOrderRecords.Enabled = false;
            else
            {
                DataRowView objRow = selection.GetSelectedRow(0) as DataRowView;
                string strStoreChangeOrderID = Convert.ToString(objRow["STORECHANGEORDERID"]).Trim();
                string strStoreChangeOrderTypeID = Convert.ToString(objRow["STORECHANGEORDERTYPEID"]).Trim();

                PLCStoreChangeOrder objPLCStoreChangeOrder = new PLCStoreChangeOrder();
                StoreChangeOrder objStoreChangeOrder = null;
                objStoreChangeOrder = objPLCStoreChangeOrder.LoadInfo(strStoreChangeOrderID, Convert.ToInt32(strStoreChangeOrderTypeID));
                if (objStoreChangeOrder == null)
                {
                    itemPrintOrderRecords.Enabled = false;
                }
                else
                {
                    int status = Convert.ToInt32(objStoreChangeOrder.StoreChangeStatus.ToString().Trim());
                    if (status == 1 || status == 2)
                        itemPrintOrderRecords.Enabled = true;
                    else
                        itemPrintOrderRecords.Enabled = false;
                }
            }
            mnuItemPrintStoreChangeOrderDetail.Visible = SystemConfig.objSessionUser.IsPermission(STORECHANGEORDERDETAILREPORT_VIEW);
            mnuStoreChangeOrderExport.Visible = false;
        }

        private void itemPrintInputOrder_Click(object sender, EventArgs e)
        {
            if (selection == null || selection.SelectedCount <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn các yêu cầu chuyển kho cần in");
                return;
            }

            string strStoreChangeOrderIDList = ",";
            int intFromStoreId = 0;
            int intToStoreId = 0;
            string strFromStoreName = string.Empty;
            string strToStoreName = string.Empty;
            string strFromStoreAddress = string.Empty;
            string strToStoreAddress = string.Empty;
            int intStoreChangeStatus = 0;
            for (int i = 0; i < selection.SelectedCount; i++)
            {
                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                if (string.IsNullOrEmpty(strFromStoreName))
                    strFromStoreName = objRow["FROMSTOREID"].ToString();
                if (string.IsNullOrEmpty(strToStoreName))
                    strToStoreName = objRow["TOSTOREID"].ToString();
                if (dtb != null)
                {
                    if (dtb.Columns.Contains("STOREFROMID") &&
                        dtb.Columns.Contains("STORETOID"))
                    {
                        if (intFromStoreId == 0)
                            int.TryParse(objRow["STOREFROMID"].ToString(), out intFromStoreId);
                        else
                        {
                            if (intFromStoreId != int.Parse(objRow["STOREFROMID"].ToString()))
                            {
                                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn phiếu có cùng kho nhập và kho xuất!");
                                return;
                            }
                        }
                        if (intToStoreId == 0)
                            int.TryParse(objRow["STORETOID"].ToString(), out intToStoreId);
                        else
                        {
                            if (intToStoreId != int.Parse(objRow["STORETOID"].ToString()))
                            {
                                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn phiếu có cùng kho nhập và kho xuất!");
                                return;
                            }
                        }
                    }
                    if (dtb.Columns.Contains("FROMSTOREADDRESS"))
                        strFromStoreAddress = objRow["FROMSTOREADDRESS"].ToString();
                    if (dtb.Columns.Contains("TOSTOREADDRESS"))
                        strToStoreAddress = objRow["TOSTOREADDRESS"].ToString();
                    //if (dtb.Columns.Contains("STORECHANGESTATUSID"))
                    //{
                    //    int.TryParse(objRow["STORECHANGESTATUSID"].ToString(), out intStoreChangeStatus);
                    //    if (intStoreChangeStatus == 0 || intStoreChangeStatus == 1)
                    //    {
                    //        MessageBoxObject.ShowWarningMessage(this, "Tồn tại yêu cầu chưa được chuyển kho.\r\n Vui lòng kiểm tra lại!");
                    //        return;
                    //    }
                    //}
                }
                strStoreChangeOrderIDList += Convert.ToString(objRow["STORECHANGEORDERID"]) + ",";

            }
            ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intBillStoreReportID);
            if (objReport == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in phiếu nhặt hàng");
                return;
            }
            string strReceiveUser = string.Empty;
            frmPopupUser frm = new frmPopupUser();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                strReceiveUser = frm.UserName;
            }
            if (string.IsNullOrEmpty(strReceiveUser)) return;
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("Receiveuser", strReceiveUser);
            htbParameterValue.Add("Storename", strToStoreName);
            htbParameterValue.Add("Address", strToStoreAddress);
            htbParameterValue.Add("OrderId", string.Empty);
            htbParameterValue.Add("FROMSTOREID", intFromStoreId);
            htbParameterValue.Add("TOSTOREID", intToStoreId);
            htbParameterValue.Add("STORECHANGEORDERIDLIST", strStoreChangeOrderIDList);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        private void itemPrintInputOrderDetail_Click(object sender, EventArgs e)
        {
            if (selection == null || selection.SelectedCount <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn các yêu cầu chuyển kho cần in");
                return;
            }

            string strStoreChangeOrderIDList = ",";

            ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intBillReportDetailID);
            if (objReport == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in phiếu nhặt hàng riêng lẻ");
                return;
            }

            for (int i = 0; i < selection.SelectedCount; i++)
            {
                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                strStoreChangeOrderIDList += Convert.ToString(objRow["STORECHANGEORDERID"]) + ",";
            }

            string strReceiveUser = string.Empty;
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("Receiveuser", strReceiveUser);
            htbParameterValue.Add("OrderId", string.Empty);
            htbParameterValue.Add("STORECHANGEORDERIDLIST", strStoreChangeOrderIDList);

            DataTable dtbReportDataSource = new ERP.Report.PLC.PLCReportDataSource().GetDataSource(objReport.ReportStoreProcName, new object[] { "@STORECHANGEORDERIDLIST", strStoreChangeOrderIDList });
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->itemPrintInputOrderDetail_Click");
            }

            if (dtbReportDataSource != null && dtbReportDataSource.Rows.Count > 0)
            {
                DataTable dtbBarcode = new DataTable();
                dtbBarcode.TableName = "TMP_BARCODE_COLUMN";
                DataColumn colID = new DataColumn("CODEID");
                dtbBarcode.Columns.Add(colID);
                DataColumn colBarcode = new DataColumn("BARCODE");
                colBarcode.DataType = System.Type.GetType("System.Byte[]");
                colBarcode.DefaultValue = null;
                dtbBarcode.Columns.Add(colBarcode);

                for (int i = 0; i < selection.SelectedCount; i++)
                {
                    DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                    DataRow[] rowDetail = dtbReportDataSource.Select("STORECHANGEORDERID = '" + Convert.ToString(objRow["STORECHANGEORDERID"]).Trim() + "'");
                    DataRow rcode = dtbBarcode.NewRow();
                    rcode["CODEID"] = rowDetail[0]["STORECHANGEORDERDETAILID"].ToString().Trim();
                    byte[] byteArrayDefaultValue = GenBarCode(objRow["STORECHANGEORDERID"].ToString().Trim());
                    rcode["BARCODE"] = byteArrayDefaultValue;
                    dtbBarcode.Rows.Add(rcode);
                }

                DataSet dsTotal = new DataSet();
                if (dtbBarcode != null)
                {
                    dtbReportDataSource.TableName = objReport.ReportStoreProcName;
                    dsTotal.Tables.Add(dtbReportDataSource.Copy());
                    dsTotal.Tables.Add(dtbBarcode);

                    dsTotal.Relations.Add("STORECHANGEORDERID_PK",
                    dsTotal.Tables[objReport.ReportStoreProcName].Columns["STORECHANGEORDERDETAILID"],
                    dsTotal.Tables["TMP_BARCODE_COLUMN"].Columns["CODEID"]);
                }
                else
                {
                    dsTotal.Tables.Add(dtbReportDataSource);
                }
                objReport.BarCodeColumnName = string.Empty;
                objReport.DataSetSource = dsTotal;
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in phiếu nhặt hàng riêng lẻ");
            }
        }

        private static byte[] GenBarCode(string Code)
        {
            byte[] arrByte = null;
            Code = Code.Trim();
            if (Code == string.Empty)
                return arrByte = new byte[] { };
            System.Drawing.Image img = null;
            BarcodeLib.Barcode barC = new BarcodeLib.Barcode();
            barC.Alignment = BarcodeLib.AlignmentPositions.CENTER;
            BarcodeLib.TYPE type = BarcodeLib.TYPE.UNSPECIFIED;
            type = BarcodeLib.TYPE.CODE128;
            string strPre = String.Empty;
            for (int t = 0; t <= 7 - Code.Length; t++)
            {
                strPre = "0" + strPre;
            }
            Code = strPre + Code;
            if (type != BarcodeLib.TYPE.UNSPECIFIED)
            {
                barC.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
                img = barC.Encode(type, Code, System.Drawing.Color.Black, System.Drawing.Color.White, 220, 120);
            }
            if (img != null)
            {
                using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
                {
                    img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    stream.Close();
                    arrByte = stream.ToArray();
                }
            }
            return arrByte;
        }

        private void frmStoreChangeOrderManager_Activated(object sender, EventArgs e)
        {
            this.Focus();
            txtKeyWords.Select();
            txtKeyWords.Focus();
        }

        /// <summary>
        /// In lệnh điều động nội bộ. 
        /// LÊ VĂN ĐÔNG -> BỔ SUNG NGÀY 08/12/2017
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuItemPrintReport_Click(object sender, EventArgs e)
        {
            if (!mnuItemPrintReport.Enabled)
                return;
            if (selection == null || selection.SelectedCount == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn phiếu yêu cầu cần in lệnh!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string strStoreChangeOrderIDList = string.Empty;
            for (int i = 0; i < selection.SelectedCount; i++)
            {
                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                strStoreChangeOrderIDList += Convert.ToString(objRow["STORECHANGEORDERID"]) + ",";
            }


            bool result = false;
            ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intStoreChangeCommandReportID);
            if (objReport != null)
            {
                objReport.PrinterTypeID = "CommonPrinter";
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("STORECHANGEORDERIDLIST", strStoreChangeOrderIDList);
                result = ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
            else
            {
                MessageBox.Show(this, "Không tìm thấy thông tin cấu hình báo cáo lệnh điều động nội bộ. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void cboMainGroup_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboSubGroupIDList.InitControl(true, cboMainGroup.MainGroupID);
        }

        private void mnuItemPrintStoreChangeOrderDetail_Click(object sender, EventArgs e)
        {
            if (selection == null || selection.SelectedCount == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn phiếu yêu cầu cần xem báo cáo!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string strStoreChangeOrderIDList = string.Empty;
            for (int i = 0; i < selection.SelectedCount; i++)
            {
                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                strStoreChangeOrderIDList += Convert.ToString(objRow["STORECHANGEORDERID"]) + ",";
            }

            View_RPT_PM_STORECHANGEORDER_DETAIL(strStoreChangeOrderIDList);
        }
        private void View_RPT_PM_STORECHANGEORDER_DETAIL(string strStoreChangeOrderIDList)
        {
            if (!string.IsNullOrEmpty(strStoreChangeOrderIDList))
            {
                frmStoreChangeOrdersDetail frm = new frmStoreChangeOrdersDetail(strStoreChangeOrderIDList);
                frm.ShowDialog(this);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            if (!DeleteData())
                return;
            btnSearch_Click(null, null);

        }
        private bool DeleteData()
        {

            if (selection == null || selection.SelectedCount <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn các yêu cầu chuyển kho cần hủy!");
                return false;
            }

            List<StoreChangeOrder> lstStoreChangeOrder = new List<StoreChangeOrder>();
            for (int i = 0; i < selection.SelectedCount; i++)
            {
                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                int intStoreChangeOrderTypeId = Convert.ToInt32(objRow["STORECHANGEORDERTYPEID"]);

                ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;
                ERP.MasterData.PLC.MD.WSStoreChangeOrderType.ResultMessage objResultMessage = new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfo(ref objStoreChangeOrderType, intStoreChangeOrderTypeId);
                if (objStoreChangeOrderType == null)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi lấy thông tin loại yêu cầu chuyển kho!");
                    return false;
                }

                string strStoreChangeOrderID = Convert.ToString(objRow["STORECHANGEORDERID"]);

                StoreChangeOrder objStoreChangeOrder = objPLCStoreChangeOrder.LoadInfo(strStoreChangeOrderID, intStoreChangeOrderTypeId);

                if (objStoreChangeOrder == null)
                {
                    MessageBox.Show(this, "Lỗi lấy thông tin yêu cầu chuyển kho để hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                if (!objStoreChangeOrder.IsReviewed && !SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.DeleteFunctionID))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Không có quyền hủy yêu cầu chuyển kho!");
                    return false;
                }
                if (objStoreChangeOrder.IsReviewed && !SystemConfig.objSessionUser.IsPermission(objStoreChangeOrderType.DeleteAfterReviewFunctionID))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Không có quyền hủy yêu cầu chuyển kho sau duyệt!");
                    return false;
                }
                if (objStoreChangeOrder.StoreChangeStatus > 0)
                {
                    MessageBox.Show(this, "Chỉ được hủy những phiếu chưa chuyển kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }


                lstStoreChangeOrder.Add(objStoreChangeOrder);
            }
            string Reason = ShowReason("Nội dung hủy", 2000);
            if (Reason == "-1" || string.IsNullOrWhiteSpace(Reason))
                return false;
            if (lstStoreChangeOrder.Count > 0)
            {
                foreach (var item in lstStoreChangeOrder)
                {
                    item.ContentDeleted = Reason;
                }
                objPLCStoreChangeOrder.DeleteList(lstStoreChangeOrder);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                MessageBox.Show(this, "Hủy yêu cầu thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            MessageBox.Show(this, "Chưa chọn loại yêu cầu chuyển kho cần hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return false;

        }
        private string ShowReason(string strTitle, int MaxLengthString)
        {
            string strReasion = "-1";
            Library.AppControl.FormCommon.frmInputString frm = new Library.AppControl.FormCommon.frmInputString();
            frm.MaxLengthString = MaxLengthString;
            frm.IsAllowEmpty = false;
            frm.Text = strTitle;
            frm.ShowDialog();
            if (frm.IsAccept)
            {
                strReasion = frm.ContentString;
            }

            return strReasion;
        }

        bool _formated = false;
        private bool FormatGridProduct()
        {
            if (_formated) return false;
            _formated = true;
            try
            {
                grdViewProduct.Columns["STORECHANGEQUANTITY"].OptionsColumn.AllowEdit = false;
                grdViewProduct.Columns["PRODUCTID"].OptionsColumn.AllowEdit = false;
                grdViewProduct.Columns["PRODUCTNAME"].OptionsColumn.AllowEdit = false;
                grdViewProduct.Columns["NOTE"].OptionsColumn.AllowEdit = true;
                grdViewProduct.Columns["IMEI"].OptionsColumn.AllowEdit = true;

                grdViewProduct.Columns["MAINGROUPID"].Visible = false;
                grdViewProduct.Columns["MAINGROUPNAME"].Visible = false;
                grdViewProduct.Columns["ISREQUESTIMEI"].Visible = false;
                grdViewProduct.Columns["STORECHANGEORDERDETAILID"].Visible = false;
                grdViewProduct.Columns["STORECHANGEORDERID"].Visible = false;
                grdViewProduct.Columns["PRODUCTID"].Visible = true;
                grdViewProduct.Columns["PRODUCTNAME"].Visible = true;
                grdViewProduct.Columns["IMEI"].Visible = true;
                grdViewProduct.Columns["QUANTITY"].Visible = true;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Visible = true;
                grdViewProduct.Columns["NOTE"].Visible = true;
                grdViewProduct.Columns["PRODUCTSTATUSNAME"].Visible = true;
                grdViewProduct.Columns["INPUTDATE"].Visible = true;

                grdViewProduct.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grdViewProduct.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grdViewProduct.Columns["IMEI"].Caption = "IMEI";
                grdViewProduct.Columns["QUANTITY"].Caption = "Số lượng yêu cầu";
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Caption = "Số lượng chuyển kho";
                grdViewProduct.Columns["PRODUCTSTATUSNAME"].Caption = "Trạng thái";
                grdViewProduct.Columns["INPUTDATE"].Caption = "Ngày nhập";
                grdViewProduct.Columns["NOTE"].Caption = "Ghi chú";
                grdViewProduct.OptionsView.ColumnAutoWidth = false;
                //Sumary
                grdViewProduct.Columns["QUANTITY"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:#,##0.##}");
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "STORECHANGEQUANTITY", "{0:#,##0.##}");

                grdViewProduct.Columns["STORECHANGEORDERDETAILID"].Width = 50;
                grdViewProduct.Columns["STORECHANGEORDERID"].Width = 50;
                grdViewProduct.Columns["PRODUCTID"].Width = 120;
                grdViewProduct.Columns["PRODUCTNAME"].Width = 200;
                grdViewProduct.Columns["IMEI"].Width = 150;
                grdViewProduct.Columns["QUANTITY"].Width = 90;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Width = 100;
                grdViewProduct.Columns["PRODUCTSTATUSNAME"].Width = 100;
                grdViewProduct.Columns["INPUTDATE"].Width = 100;
                grdViewProduct.Columns["NOTE"].Width = 250;
                grdViewProduct.Columns["QUANTITY"].DisplayFormat.FormatString = "#,##0.##";
                grdViewProduct.Columns["QUANTITY"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].DisplayFormat.FormatString = "#,##0.##";
                grdViewProduct.Columns["STORECHANGEQUANTITY"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;

                DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repSpinQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
                repSpinQuantity.MaxLength = 10;
                repSpinQuantity.IsFloatValue = true;
                repSpinQuantity.Mask.EditMask = "#,###,###,###,##0.##";
                repSpinQuantity.MaxLength = 50;
                repSpinQuantity.NullText = "0";
                //repSpinQuantity.MaxValue = new decimal(new int[] { -727379969, 232, 0, 0 });
                repSpinQuantity.Buttons.Clear();
                grdViewProduct.Columns["QUANTITY"].ColumnEdit = repSpinQuantity;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].ColumnEdit = repSpinQuantity;

                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtNote.MaxLength = 2000;
                grdViewProduct.Columns["NOTE"].ColumnEdit = repTxtNote;

                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtImei.CharacterCasing = CharacterCasing.Upper;
                grdViewProduct.Columns["IMEI"].ColumnEdit = repTxtImei;
                grdViewProduct.Columns["STORECHANGEQUANTITY"].Visible = true;
                //DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repTxtImei = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
                //grdViewProduct.Columns["IMEI"].ColumnEdit = repTxtImei;

                grdViewProduct.Appearance.FooterPanel.ForeColor = Color.Red;
                grdViewProduct.ColumnPanelRowHeight = 40;
                grdViewProduct.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewProduct.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewProduct.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewProduct.OptionsView.ShowAutoFilterRow = false;
                grdViewProduct.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewProduct.OptionsView.ShowFooter = true;
                grdViewProduct.OptionsView.ShowGroupPanel = false;
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewProduct);
                List<string> listColName = new List<string> { "PRODUCTID", "PRODUCTNAME", "IMEI", "QUANTITY", "STORECHANGEQUANTITY", "PRODUCTSTATUSNAME", "INPUTDATE", "NOTE" };
                foreach (DevExpress.XtraGrid.Columns.GridColumn col in grdViewProduct.Columns)
                {
                    col.Visible = listColName.Contains(col.FieldName);
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách chi tiết yêu cầu chuyển kho");
                SystemErrorWS.Insert("Định dạng lưới danh sách chi tiết yêu cầu chuyển kho", objExce.ToString(), this.Name + " -> FormatGridProduct", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void grdViewData_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                System.Data.DataRowView row = grdViewData.GetRow(e.FocusedRowHandle) as System.Data.DataRowView;
                if (row != null)
                {
                    string strStoreChangeOrderTypeID = Convert.ToString(row["STORECHANGEORDERTYPEID"]);
                    string strStoreChangeOrderID = Convert.ToString(row["STORECHANGEORDERID"]);
                    StoreChangeOrder objStoreChangeOrder = null;
                    objStoreChangeOrder = objPLCStoreChangeOrder.LoadInfo(strStoreChangeOrderID, Convert.ToInt32(strStoreChangeOrderTypeID));
                    if (objStoreChangeOrder == null)
                        throw new Exception("Không tìm thấy chi tiết yêu cầu chuyển kho");
                    grdProduct.DataSource = objStoreChangeOrder.DtbStoreChangeOrderDetail;
                    grdViewProduct.Columns["IMEI"].Visible = true;
                    FormatGridProduct();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mnuItemPrintReport1_Click(object sender, EventArgs e)
        {
            if (!mnuItemPrintReport.Enabled)
                return;
            if (selection == null || selection.SelectedCount == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn phiếu yêu cầu cần in lệnh!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string strStoreChangeOrderIDList = string.Empty;
            for (int i = 0; i < selection.SelectedCount; i++)
            {
                DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                strStoreChangeOrderIDList += Convert.ToString(objRow["STORECHANGEORDERID"]) + ",";
            }


            bool result = false;
            ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intStoreChangeCommandReportID1);
            if (objReport != null)
            {
                objReport.PrinterTypeID = "CommonPrinter";
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("STORECHANGEORDERIDLIST", strStoreChangeOrderIDList);
                result = ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
            else
            {
                MessageBox.Show(this, "Không tìm thấy thông tin cấu hình báo cáo lệnh điều động nội bộ. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuCopy_Click(object sender, EventArgs e)
        {
            DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            try
            {
                string strStoreChangeOrderTypeID = Convert.ToString(row["STORECHANGEORDERTYPEID"]);
                string strStoreChangeOrderID = Convert.ToString(row["STORECHANGEORDERID"]);
                string strTRANSPORTCOMPANYID = Convert.ToString(row["TRANSPORTCOMPANYID"]);
                string strTRASPORTSERVICEID = Convert.ToString(row["TRANSPORTSERVICESID"]);
                string TRANSPORTCOMPANYNAME = Convert.ToString(row["TRANSPORTCOMPANYNAME"]);
                string TRANSPORTSERVICENAME = Convert.ToString(row["TRANSPORTSERVICENAME"]);
                bool bolIsReviewed = Convert.ToBoolean(row["ISREVIEWED"]);
                frmStoreChangeOrder frmStoreChangeOrder1 = new frmStoreChangeOrder(bolIsReviewed);
                frmStoreChangeOrder1.Tag = "StoreChangeOrderType = " + strStoreChangeOrderTypeID;
                frmStoreChangeOrder1.StoreChangeOrderID = strStoreChangeOrderID;
                frmStoreChangeOrder1.strTRANSPORTCOMPANYID = strTRANSPORTCOMPANYID;
                frmStoreChangeOrder1.strTRASPORTSERVICEID = strTRASPORTSERVICEID;
                frmStoreChangeOrder1.IsOnlyCertifyFinger = bolIsOnlyCertifyFinger;
                frmStoreChangeOrder1.FormManager = this;
                frmStoreChangeOrder1.IsReviewed = bolIsReviewed;
                frmStoreChangeOrder1.IsCopy = true;
                //frmStoreChangeOrder1.IsTaoXuatChuyenKhoCCDC = true;
                frmStoreChangeOrder1.ShowDialog();
                if (frmStoreChangeOrder1.IsHasAction)
                    btnSearch_Click(null, null);
            }
            catch (Exception)
            {
                //MessageBoxObject.ShowWarningMessage(this, "Lỗi khi chọn yêu cầu để chỉnh sửa.");
                return;
            }
        }
    }
}
