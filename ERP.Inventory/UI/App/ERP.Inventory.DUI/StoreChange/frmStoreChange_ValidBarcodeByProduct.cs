﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Text.RegularExpressions;
using Library.AppCore.Forms;
using System.Threading;
using ERP.Inventory.DUI.FIFO;

namespace ERP.Inventory.DUI.StoreChange
{
    /// <summary>
    /// Created by: LE VĂN ĐÔNG
    /// Desc: Nhập Imei nhiều sản phẩm cùng lúc
    /// Date: 06/09/2017
    /// </summary>
    public partial class frmStoreChange_ValidBarcodeByProduct : Form
    {
        public frmStoreChange_ValidBarcodeByProduct()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        #region Variable
        private List<frmStoreChange.ProductImei> objProductImeiList = null;
        private Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection();
        private bool isCloseForm = false;
        private bool bolIsFromStoreChange = false;
        public string productID = string.Empty;
        public string productName = string.Empty;
        private int intOutputTypeID;
        private int intInputTypeID;
        private int intOutputStoreID = 0;
        private decimal decOrderQuantity = 0;
        private int intInStockStatusID = 0;

        #endregion

        #region Property
        public int InStockStatusID
        {
            set { intInStockStatusID = value; }
        }

        /// <summary>
        /// Danh sách IMEI nhập
        /// </summary>
        public List<frmStoreChange.ProductImei> ProductImeiList
        {
            get { return objProductImeiList; }
            set { objProductImeiList = value; }
        }

        public string OrderID { get; set; }
        public DataTable dtbSub { get; set; }

        public int InputTypeID
        {
            set { intInputTypeID = value; }
        }
        public int OutputTypeID
        {
            set { intOutputTypeID = value; }
        }
        /// <summary>
        /// Kho xuất
        /// </summary>
        public int OutputStoreID
        {
            set { intOutputStoreID = value; }
        }

        /// <summary>
        /// Số lượng đơn hàng
        /// </summary>
        public decimal OrderQuantity
        {
            set { decOrderQuantity = value; }
        }

        public bool IsFromStoreChange
        {
            get { return bolIsFromStoreChange; }
            set { bolIsFromStoreChange = value; }
        }

        #endregion

        #region Function
        /// <summary>
        /// Kiểm tra tồn tại IMEI chưa xác nhận hoặc xác nhận lỗi
        /// </summary>
        /// <returns></returns>
        private bool CheckValidate()
        {
            var imeilist = from o in objProductImeiList
                           where o.IsError == false
                           select o;

            if (imeilist.Count() == 0)
            {
                MessageBox.Show("Không có IMEI hợp lệ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (decOrderQuantity > 0 && objProductImeiList.Count() > decOrderQuantity)
            {
                MessageBox.Show("Số lượng IMEI nhập lớn hơn số lượng yêu cầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Xác nhận IMEI
        /// </summary>
        private void ValidateData()
        {
            if (objProductImeiList.Count() > 0)
            {
                Thread objThread = new Thread(new ThreadStart(Excute));
                objThread.Start();
                foreach (var item in objProductImeiList)
                {
                    item.IsError = false;
                    item.IsErrorFiFo = 0;
                }
                #region Check IMEI
                // Check trùng Imei trên lưới
                Dictionary<string, List<int>> dicCheckIMEI = new Dictionary<string, List<int>>();
                for (int i = 0; i < objProductImeiList.Count(); i++)
                {
                    if (dicCheckIMEI.ContainsKey(objProductImeiList[i].Imei))
                    {
                        dicCheckIMEI[objProductImeiList[i].Imei].Add(i);
                    }
                    else
                    {
                        List<int> lstIndex = new List<int>();
                        lstIndex.Add(i);
                        dicCheckIMEI.Add(objProductImeiList[i].Imei, lstIndex);
                    }
                }

                foreach (string strIMEI in dicCheckIMEI.Keys)
                {
                    if (dicCheckIMEI[strIMEI].Count() > 1)
                    {
                        for (int i = 1; i < dicCheckIMEI[strIMEI].Count(); i++)
                        {
                            objProductImeiList[dicCheckIMEI[strIMEI][i]].IsError = true;
                            objProductImeiList[dicCheckIMEI[strIMEI][i]].Error = objProductImeiList[dicCheckIMEI[strIMEI][i]].Error.Replace("Trùng IMEI trên file Excel;", "") + "Trùng IMEI trên file Excel;";
                        }
                    }
                }

                // Loại những Imei bị trùng trên lưới
                objProductImeiList = objProductImeiList.Where(r => r.Error != null && !r.Error.Contains("Trùng IMEI trên file Excel")).ToList();
                var listIMEI = objProductImeiList.Select(x => x.Imei); // Danh sach String IMEI đã nhập

                // Check Imei trong DB
                DataTable dtData = new DataTable();
                dtData.Columns.Add("IMEI");
                foreach (var item in listIMEI)
                {
                    dtData.Rows.Add(item);
                }
                string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtData, "IMEI");

                DataTable dtbTotalImei = null;
                DataTable dtbTotalImeiNotInStock = null;
                DataSet dsData = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("CHECKIMEI_INSTOCK_NOTINSTOCK",
                new string[] { "v_Out1", "v_Out2" }, new object[] { "@StoreID", intOutputStoreID,
                    "@OUTPUTTYPEID", intOutputTypeID, "@INPUTTYPEID", intInputTypeID, "@IMEI", xmlIMEI });
                if (dsData != null && dsData.Tables.Count > 0)
                {
                    dtbTotalImei = dsData.Tables[0].Copy();
                }
                if (dsData != null && dsData.Tables.Count > 0)
                {
                    dtbTotalImeiNotInStock = dsData.Tables[1].Copy();
                }
                List<string> lstIMEI = new List<string>();
                lstIMEI = lstIMEI.Concat(dtbTotalImei.AsEnumerable().Select(x => x["IMEI"].ToString()).ToList()).ToList();
                int IsCanInputFifo = 0;
                foreach (var item in objProductImeiList)
                {
                    DataRow[] lstdr = dtbTotalImei.Select("IMEI = '" + item.Imei.Trim() + "'");
                    if (lstdr.Any())
                    {
                        item.EndWarrantyDate = Convert.IsDBNull(lstdr[0]["ENDWARRANTYDATE"]) ? null : (DateTime?)Convert.ToDateTime(lstdr[0]["ENDWARRANTYDATE"]);
                        item.IsHasWarranty = Convert.ToBoolean(lstdr[0]["IsHasWarranty"]);
                        item.IsNew = Convert.ToBoolean(lstdr[0]["IsNew"]);
                        item.IsShowProduct = Convert.ToBoolean(lstdr[0]["IsShowProduct"]);
                        item.InVAT = Convert.ToDecimal(lstdr[0]["InVAT"]);
                        item.OutVAT = Convert.ToDecimal(lstdr[0]["OutVAT"]);
                        item.InputPrice = Convert.ToDecimal(lstdr[0]["InputPrice"]);
                        item.OutputPrice = Convert.ToDecimal(lstdr[0]["OutputPrice"]);
                        item.CostPrice = Convert.ToDecimal(lstdr[0]["CostPrice"]);
                        item.IsErrorFiFo = Convert.ToInt32(lstdr[0]["STATUSFIFOID"]);
                        if (lstdr[0]["INSTOCKSTATUSID"].ToString().Trim() != intInStockStatusID.ToString())
                        {
                            item.IsError = true;
                            item.Error = "IMEI này không thuộc trạng thái sản phẩm đã chọn!";
                        }
                        else if (item.ProductID != lstdr[0]["PRODUCTID"].ToString())
                        {
                            item.IsError = true;
                            item.Error = "IMEI này không có trong sản phẩm";
                        }
                        if (item.IsErrorFiFo == 1 && !item.IsError)
                            item.Error = "IMEI " + item.Imei.ToString().Trim() + " vi phạm xuất fifo";
                    }
                    else
                    {
                        item.IsError = true;
                        item.Error = "IMEI này không tồn tại trong kho";
                    }
                }

                var LstIMEIInProductOrder = from o in objProductImeiList
                                            join i in dtbTotalImei.Select("ISORDER = 1 and ORDERID <> '" + OrderID + "'")
                                            on o.ProductID.Trim() equals i["PRODUCTID"].ToString().Trim()
                                            select i;
                if (LstIMEIInProductOrder.Any())
                {
                    var LstIMEIInProduct = from o in objProductImeiList
                                           join i in LstIMEIInProductOrder
                                           on o.Imei.Trim() equals i["IMEI"].ToString().Trim()
                                           select o;
                    if (LstIMEIInProduct.Any())
                    {
                        LstIMEIInProduct.All(c => { c.IsError = true; c.Error = "IMEI đã được đặt hàng"; return true; });
                    }
                }
                var LstIMEIInProductStore = from o in objProductImeiList
                                            join i in dtbTotalImei.Select("STOREID <> " + intOutputStoreID)
                                            on o.Imei.Trim() equals i["IMEI"].ToString().Trim()
                                            select o;
                if (LstIMEIInProductStore.Any())
                {
                    LstIMEIInProductStore.All(c => { c.IsError = true; c.Error = "IMEI không tồn kho"; return true; });
                }
                var LstIMEIInProductRealinstock = from o in objProductImeiList
                                                  join i in dtbTotalImei.Select("ISCHECKREALINPUT <> 1")
                                                  on o.Imei.Trim() equals i["IMEI"].ToString().Trim()
                                                  select o;
                if (LstIMEIInProductRealinstock.Any())
                {
                    LstIMEIInProductRealinstock.All(c => { c.IsError = true; c.Error = "IMEI chưa thực nhập"; return true; });
                }
                var LstIMEINotInStock = from o in objProductImeiList
                                        join i in dtbTotalImeiNotInStock.AsEnumerable()
                                        on o.Imei.Trim() equals i["IMEI"].ToString().Trim()
                                        select o;
                if (LstIMEINotInStock.Any())
                {
                    LstIMEINotInStock.All(c => { c.IsError = true; c.Error = "IMEI đang cho mượn"; return true; });
                }



                //kiểm tra imei đã tồn tại trong form trước
                if (dtbSub != null && dtbSub.Rows.Count > 0)
                {
                    var lstExistsSub = from r in objProductImeiList
                                       join x in dtbSub.AsEnumerable()
                                           on r.Imei.Trim() equals x["IMEI"].ToString().Trim()
                                       select r;
                    lstExistsSub.All(c => { c.IsError = true; c.Error = "Số IMEI này đã tồn tại trên phiếu"; return true; });
                }
                #endregion

                frmWaitDialog.Close();
                Thread.Sleep(0);
                objThread.Abort();

            }
            GetTotal();
            //Lay danh sach fifo
            var varReulst = from o in objProductImeiList
                            where o.IsError == false && o.IsErrorFiFo == 1
                            select o;
            if (varReulst.Any())
            {
                DataTable dtbImeiPara = new DataTable();
                dtbImeiPara.Columns.Add("IMEI");
                //varReulst.All(c => { c["ISSELECT"] = false; return true; });
                var varIMEIOK = from o in objProductImeiList
                                where o.IsError == false && o.IsErrorFiFo == 0
                                select o;
                if (varIMEIOK.Any())
                {
                    foreach (var item in varIMEIOK)
                    {
                        string strImei = (item.Imei).ToString().Trim();
                        if (!string.IsNullOrEmpty(strImei))
                            dtbImeiPara.Rows.Add(strImei);
                    }
                }
                string strMessenger = "Có tồn tại dữ liệu vi phạm xuất fifo bạn có muốn tiếp tục?";
                if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) > 0)
                {
                    if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) == Convert.ToInt32(objProductImeiList.Count))
                    {
                        MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + objProductImeiList.Count.ToString("#,##0"),
                                 "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnClose.Enabled = true;
                        return;
                    }
                    else
                        strMessenger = "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + objProductImeiList.Count.ToString("#,##0") + "\nCó tồn tại dữ liệu vi phạm xuất fifo bạn có muốn tiếp tục?";
                }
                DialogResult drResult = ClsFIFO.ShowMessengerFIFOList_ByIMEI(dtbImeiPara, strMessenger
                        , productID, intOutputStoreID, -1);
                if (drResult != System.Windows.Forms.DialogResult.OK)
                {
                    foreach (var item in varReulst)
                        item.IsError = true;
                    //varReulst.All(c => { c.IsError = true; c.Error = "IMEI " + c.Imei.ToString().Trim() + " vi phạm xuất fifo"; return true; });
                }
                else
                    btnClose_Click(null, null);
                btnClose.Enabled = true;
                grdIMEI.RefreshDataSource();
                return;
            }
            if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) > 0)
            {
                if (Convert.ToInt32(txtTotalIMEIInValid.EditValue) == Convert.ToInt32(objProductImeiList.Count))
                {
                    MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + objProductImeiList.Count.ToString("#,##0"),
                   "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (MessageBox.Show(this, "Số IMEI không hợp lệ là " + txtTotalIMEIInValid.Text + "/" + objProductImeiList.Count.ToString("#,##0") + "\n Bạn có muốn tiếp tục không?",
                   "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        btnClose_Click(null, null);
                    }
                }
            }
            else
            {
                btnClose_Click(null, null);
            }
            grdIMEI.RefreshDataSource();
            btnClose.Enabled = true;
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang kiểm tra IMEI");
        }
        /// <summary>
        /// Xóa IMEI không hợp lệ hoặc chưa xác nhận
        /// </summary>
        private void RemoveInValidData()
        {
            for (int i = objProductImeiList.Count - 1; i >= 0; i--)
            {
                frmStoreChange.ProductImei objProductImei = objProductImeiList[i];
                if (objProductImei.IsError)
                    objProductImeiList.RemoveAt(i);
            }
            grdIMEI.RefreshDataSource();
        }

        #endregion

        private void btnValidate_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateData();
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi nhập dữ liệu khi import imei", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi nhập dữ liệu từ Excel", objExce.ToString(), "ValidateData()", DUIInventory_Globals.ModuleName);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CheckValidate())
            {
                RemoveInValidData();
                isCloseForm = true;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void frmInputVoucher_ValidBarcodeByProduct_Load(object sender, EventArgs e)
        {
            objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, grvIMEI);
            objSelection.CheckMarkColumn.Width = 50;
            grpProduct.Text = productID + " - " + productName;
            if (ProductImeiList == null)
                ProductImeiList = new List<frmStoreChange.ProductImei>();
            grdIMEI.DataSource = objProductImeiList;
        }

        private void frmInputVoucher_ValidBarcodeByProduct_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Xóa IMEI
        /// </summary>
        private void DeleteIMEI()
        {
            if (objSelection.SelectedCount == 0)
            {
                MessageBox.Show(this, "Vui lòng chọn IMEI cần xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            for (int i = 0; i < objSelection.SelectedCount; i++)
            {
                frmStoreChange.ProductImei objInputVoucherDetailIMEI = (frmStoreChange.ProductImei)objSelection.GetSelectedRow(i);
                objProductImeiList.Remove(objInputVoucherDetailIMEI);
            }

            GetTotal();
            btnValidate.Enabled = grvIMEI.DataRowCount > 0;
        }

        private void GetTotal()
        {
            grdIMEI.DataSource = ProductImeiList;
            grdIMEI.RefreshDataSource();
            txtTotalIMEI.Text = objProductImeiList.Count().ToString();
            var vTotalEMEIVaild = from o in objProductImeiList
                                  where o.IsError == false && o.IsErrorFiFo == 0
                                  select o;
            txtTotalIMEIValid.Text = vTotalEMEIVaild.Count().ToString();

            var vTotalEMEIInVaild = from o in objProductImeiList
                                    where o.IsError == true && o.IsErrorFiFo == 0
                                    select o;
            txtTotalIMEIInValid.Text = vTotalEMEIInVaild.Count().ToString();
            objSelection.ClearSelection();
            btnValidate.Enabled = grvIMEI.DataRowCount > 0;
        }
        private void mnuItemDeleteIMEI_Click(object sender, EventArgs e)
        {
            if (!mnuItemDeleteIMEI.Enabled)
                return;
            DeleteIMEI();
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName != string.Empty)
            {
                DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                bool bolIsSelect = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["IsError"]));
                int bolIsErrorFiFo = Convert.ToInt32(View.GetRowCellValue(e.RowHandle, View.Columns["IsErrorFiFo"]));
                if (bolIsSelect)
                {
                    e.Appearance.BackColor = Color.Pink;
                }
                if (!bolIsSelect && bolIsErrorFiFo == 1)
                {
                    e.Appearance.BackColor = SystemColors.Info;
                }
            }
        }

        private void frmInputVoucher_ValidBarcodeByProduct_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isCloseForm && objProductImeiList.Count() > 0)
            {
                if (MessageBox.Show("Bạn muốn đóng form nhập IMEI?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(gridControl1);
            }
            catch (Exception objExc)
            {
            }
        }

        private void mnuItemImportExcel_Click(object sender, EventArgs e)
        {
            ImportExcelFile();
        }

        private void ImportExcelFile()
        {
            try
            {
                if (objProductImeiList != null && objProductImeiList.Count > 0)
                {
                    if (MessageBox.Show("Toàn bộ IMEI trên lưới sẽ bị xóa. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                }
                objProductImeiList.Clear();
                grdIMEI.RefreshDataSource();
                GetTotal();
                DataTable dtbExcelData = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(1);
                if (dtbExcelData == null || dtbExcelData.Rows.Count < 1)
                    return;
                DataRow rowCheck = dtbExcelData.Rows[0];
                if (rowCheck[0].ToString().Trim().ToLower() != "imei")
                {
                    MessageBox.Show("File nhập không đúng định dạng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                for (int i = 0; i < dtbExcelData.Columns.Count; i++)
                {
                    if (string.IsNullOrEmpty(dtbExcelData.Rows[0][i].ToString()))
                    {
                        MessageBox.Show("File nhập không đúng định dạng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    dtbExcelData.Columns[i].Caption = dtbExcelData.Rows[0][i].ToString();
                }
                if (dtbExcelData.Rows.Count > 0)
                {
                    dtbExcelData.Rows.RemoveAt(0);
                    ImportData_StoreChange(dtbExcelData);
                }

            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi nhập dữ liệu từ Excel", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi nhập dữ liệu từ Excel", objExce.ToString(), "frmStoreChange_ValidBarcodeByProduct -> ImportExcelFile()", DUIInventory_Globals.ModuleName);
            }
        }



        private void ImportData_StoreChange(DataTable dtbExcelData)
        {
            List<frmStoreChange.ProductImei> lstImei = new List<frmStoreChange.ProductImei>();
            foreach (DataRow row in dtbExcelData.Rows)
            {
                frmStoreChange.ProductImei obj = new frmStoreChange.ProductImei();
                string strIMEI = row[0].ToString();
                if (string.IsNullOrEmpty(strIMEI))
                    continue;

                obj.Imei = strIMEI.Trim();
                obj.ProductID = productID;
                obj.ProductName = productName;
                obj.IsHasWarranty = true;
                obj.Error = "";
                obj.IsError = false;
                obj.IsErrorFiFo = 0;
                lstImei.Add(obj);
            }
            if (decOrderQuantity > 0 && lstImei.Count > decOrderQuantity)
            {
                MessageBox.Show("File nhập có số lượng IMEI nhiều hơn số lượng yêu cầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lstImei.Clear();
            }

            if (dtbSub != null && dtbSub.Rows.Count > 0)
            {
                var lstExistsSub = from r in lstImei
                                   join x in dtbSub.AsEnumerable()
                                       on r.Imei equals x["IMEI"].ToString()
                                   select r;
                lstExistsSub.All(c => { c.IsError = true; c.Error = "Số IMEI này đã tồn tại trong phiếu"; return true; });
            }

            objProductImeiList.AddRange(lstImei);
            grdIMEI.DataSource = objProductImeiList;
            GetTotal();
        }


        private void mnuPopUp_Opening(object sender, CancelEventArgs e)
        {
            mnuItemDeleteIMEI.Enabled = grvIMEI.DataRowCount > 0;
        }

    }
}
