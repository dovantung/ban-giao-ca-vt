﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmStoreChangesDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStoreChangesDetail));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pnSearchStatus = new System.Windows.Forms.Panel();
            this.btnCancelThread = new System.Windows.Forms.Button();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.label12 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ORDERINDEX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STORECHANGEID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STORECHANGEDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FROMSTORENAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TOSTORENAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PRODUCTID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PRODUCTNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chksChon = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repTextPinCode150Char = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repoViewDetail = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnExportToExcel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.pnSearchStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksChon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextPinCode150Char)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pnSearchStatus);
            this.panelControl1.Controls.Add(this.grdData);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(762, 367);
            this.panelControl1.TabIndex = 0;
            // 
            // pnSearchStatus
            // 
            this.pnSearchStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnSearchStatus.Controls.Add(this.btnCancelThread);
            this.pnSearchStatus.Controls.Add(this.marqueeProgressBarControl1);
            this.pnSearchStatus.Controls.Add(this.label12);
            this.pnSearchStatus.Location = new System.Drawing.Point(85, 195);
            this.pnSearchStatus.Margin = new System.Windows.Forms.Padding(4);
            this.pnSearchStatus.Name = "pnSearchStatus";
            this.pnSearchStatus.Size = new System.Drawing.Size(609, 125);
            this.pnSearchStatus.TabIndex = 40;
            // 
            // btnCancelThread
            // 
            this.btnCancelThread.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelThread.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelThread.Location = new System.Drawing.Point(482, 90);
            this.btnCancelThread.Name = "btnCancelThread";
            this.btnCancelThread.Size = new System.Drawing.Size(92, 25);
            this.btnCancelThread.TabIndex = 20;
            this.btnCancelThread.TabStop = false;
            this.btnCancelThread.Text = "  Hủy bỏ";
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(33, 52);
            this.marqueeProgressBarControl1.Margin = new System.Windows.Forms.Padding(4);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Properties.LookAndFeel.SkinName = "The Asphalt World";
            this.marqueeProgressBarControl1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(541, 31);
            this.marqueeProgressBarControl1.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label12.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label12.Location = new System.Drawing.Point(30, 20);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(555, 28);
            this.label12.TabIndex = 0;
            this.label12.Text = "Đang tải dữ liệu, vui lòng đợi trong giây lát...";
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(2, 40);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Margin = new System.Windows.Forms.Padding(10, 10, 10, 3);
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chksChon,
            this.repTextPinCode150Char,
            this.repoViewDetail});
            this.grdData.Size = new System.Drawing.Size(758, 325);
            this.grdData.TabIndex = 6;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grdViewData.ColumnPanelRowHeight = 40;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ORDERINDEX,
            this.STORECHANGEID,
            this.STORECHANGEDATE,
            this.FROMSTORENAME,
            this.TOSTORENAME,
            this.PRODUCTID,
            this.PRODUCTNAME,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[Status]=true";
            styleFormatCondition1.Value1 = true;
            this.grdViewData.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsBehavior.Editable = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsPrint.AutoWidth = false;
            this.grdViewData.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.OptionsView.ShowIndicator = false;
            this.grdViewData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdViewData_KeyDown);
            // 
            // ORDERINDEX
            // 
            this.ORDERINDEX.Caption = "STT";
            this.ORDERINDEX.FieldName = "ORDERINDEX";
            this.ORDERINDEX.Name = "ORDERINDEX";
            this.ORDERINDEX.OptionsColumn.FixedWidth = true;
            this.ORDERINDEX.OptionsColumn.ReadOnly = true;
            this.ORDERINDEX.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.ORDERINDEX.Visible = true;
            this.ORDERINDEX.VisibleIndex = 0;
            this.ORDERINDEX.Width = 50;
            // 
            // STORECHANGEID
            // 
            this.STORECHANGEID.Caption = "Mã phiếu";
            this.STORECHANGEID.FieldName = "STORECHANGEID";
            this.STORECHANGEID.Name = "STORECHANGEID";
            this.STORECHANGEID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.STORECHANGEID.Visible = true;
            this.STORECHANGEID.VisibleIndex = 1;
            this.STORECHANGEID.Width = 140;
            // 
            // STORECHANGEDATE
            // 
            this.STORECHANGEDATE.Caption = "Ngày chuyển kho";
            this.STORECHANGEDATE.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.STORECHANGEDATE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.STORECHANGEDATE.FieldName = "STORECHANGEDATE";
            this.STORECHANGEDATE.Name = "STORECHANGEDATE";
            this.STORECHANGEDATE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.STORECHANGEDATE.Visible = true;
            this.STORECHANGEDATE.VisibleIndex = 2;
            this.STORECHANGEDATE.Width = 100;
            // 
            // FROMSTORENAME
            // 
            this.FROMSTORENAME.Caption = "Kho xuất";
            this.FROMSTORENAME.FieldName = "FROMSTORENAME";
            this.FROMSTORENAME.Name = "FROMSTORENAME";
            this.FROMSTORENAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.FROMSTORENAME.Visible = true;
            this.FROMSTORENAME.VisibleIndex = 3;
            this.FROMSTORENAME.Width = 250;
            // 
            // TOSTORENAME
            // 
            this.TOSTORENAME.Caption = "Kho nhập";
            this.TOSTORENAME.FieldName = "TOSTORENAME";
            this.TOSTORENAME.Name = "TOSTORENAME";
            this.TOSTORENAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TOSTORENAME.Visible = true;
            this.TOSTORENAME.VisibleIndex = 4;
            this.TOSTORENAME.Width = 250;
            // 
            // PRODUCTID
            // 
            this.PRODUCTID.Caption = "Mã sản phẩm";
            this.PRODUCTID.FieldName = "PRODUCTID";
            this.PRODUCTID.Name = "PRODUCTID";
            this.PRODUCTID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.PRODUCTID.Visible = true;
            this.PRODUCTID.VisibleIndex = 5;
            this.PRODUCTID.Width = 120;
            // 
            // PRODUCTNAME
            // 
            this.PRODUCTNAME.Caption = "Tên sản phẩm";
            this.PRODUCTNAME.FieldName = "PRODUCTNAME";
            this.PRODUCTNAME.Name = "PRODUCTNAME";
            this.PRODUCTNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.PRODUCTNAME.Visible = true;
            this.PRODUCTNAME.VisibleIndex = 6;
            this.PRODUCTNAME.Width = 250;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "IMEI";
            this.gridColumn16.FieldName = "IMEI";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 7;
            this.gridColumn16.Width = 150;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Số lượng";
            this.gridColumn17.FieldName = "QUANTITY";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 8;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Nội dung lỗi";
            this.gridColumn18.FieldName = "NOTE";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 9;
            this.gridColumn18.Width = 250;
            // 
            // chksChon
            // 
            this.chksChon.AutoHeight = false;
            this.chksChon.Name = "chksChon";
            // 
            // repTextPinCode150Char
            // 
            this.repTextPinCode150Char.AutoHeight = false;
            this.repTextPinCode150Char.MaxLength = 150;
            this.repTextPinCode150Char.Name = "repTextPinCode150Char";
            // 
            // repoViewDetail
            // 
            this.repoViewDetail.AutoHeight = false;
            serializableAppearanceObject1.Options.UseTextOptions = true;
            serializableAppearanceObject1.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            serializableAppearanceObject1.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repoViewDetail.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Xem chi tiết", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleLeft, ((System.Drawing.Image)(resources.GetObject("repoViewDetail.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repoViewDetail.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.repoViewDetail.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.repoViewDetail.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.repoViewDetail.Name = "repoViewDetail";
            this.repoViewDetail.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnExportToExcel);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(758, 38);
            this.panelControl2.TabIndex = 0;
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportToExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportToExcel.Appearance.Options.UseFont = true;
            this.btnExportToExcel.Enabled = false;
            this.btnExportToExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportToExcel.Location = new System.Drawing.Point(620, 5);
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Size = new System.Drawing.Size(128, 26);
            this.btnExportToExcel.TabIndex = 3;
            this.btnExportToExcel.Text = "Xuất Excel";
            this.btnExportToExcel.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // frmStoreChangesDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 367);
            this.Controls.Add(this.panelControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmStoreChangesDetail";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Báo cáo chi tiết chuyền kho bảo hành";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.pnSearchStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksChon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextPinCode150Char)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chksChon;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTextPinCode150Char;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repoViewDetail;
        private DevExpress.XtraEditors.SimpleButton btnExportToExcel;
        private DevExpress.XtraGrid.Columns.GridColumn ORDERINDEX;
        private DevExpress.XtraGrid.Columns.GridColumn STORECHANGEID;
        private DevExpress.XtraGrid.Columns.GridColumn STORECHANGEDATE;
        private DevExpress.XtraGrid.Columns.GridColumn FROMSTORENAME;
        private DevExpress.XtraGrid.Columns.GridColumn TOSTORENAME;
        private DevExpress.XtraGrid.Columns.GridColumn PRODUCTID;
        private DevExpress.XtraGrid.Columns.GridColumn PRODUCTNAME;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private System.Windows.Forms.Panel pnSearchStatus;
        private System.Windows.Forms.Button btnCancelThread;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private System.Windows.Forms.Label label12;
    }
}