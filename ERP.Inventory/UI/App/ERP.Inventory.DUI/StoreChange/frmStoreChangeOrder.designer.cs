﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmStoreChangeOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStoreChangeOrder));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemExport = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemImportIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grdProduct = new DevExpress.XtraGrid.GridControl();
            this.grdViewProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cboTransportTypeID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.cboTransportServices = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.cboTransportCompany = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboInStockStatusID = new System.Windows.Forms.ComboBox();
            this.btnSearchProduct = new System.Windows.Forms.Button();
            this.txtContentDeleted = new System.Windows.Forms.TextBox();
            this.cboFromStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboToStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ckbIsUrgent = new System.Windows.Forms.CheckBox();
            this.ckbIsDeleted = new System.Windows.Forms.CheckBox();
            this.ckbIsReview = new System.Windows.Forms.CheckBox();
            this.txtSearchProduct = new System.Windows.Forms.TextBox();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.cboStoreChangeStatus = new System.Windows.Forms.ComboBox();
            this.dteExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.dteOrderDate = new System.Windows.Forms.DateTimePicker();
            this.lblContentDeleted = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblStoreChangeStatus = new System.Windows.Forms.Label();
            this.txtStoreChangeOrderID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbtnQuantity = new System.Windows.Forms.RadioButton();
            this.rbtnIMEI = new System.Windows.Forms.RadioButton();
            this.tabReviewLevel = new System.Windows.Forms.TabPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.grdReviewLevel = new DevExpress.XtraGrid.GridControl();
            this.mnuReview = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmdExcelReview = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewReviewLevel = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkActiveItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkSystemItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.tabAttachment = new System.Windows.Forms.TabPage();
            this.grdAttachment = new DevExpress.XtraGrid.GridControl();
            this.mnuAttachment = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.grvAttachment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVOFFICEID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAttachmentName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAttachMENTID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAttachMENTPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colView = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnView = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colDownload = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDownload = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnTrinhky = new System.Windows.Forms.Button();
            this.btnVOffice = new System.Windows.Forms.Button();
            this.btnStoreChangeAll = new System.Windows.Forms.Button();
            this.dropdownStoreChange = new DevExpress.XtraEditors.DropDownButton();
            this.mnuSelectMainGroupID = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnReview = new System.Windows.Forms.Button();
            this.cmdReview = new DevExpress.XtraEditors.DropDownButton();
            this.pMnuReview = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblWarning = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.mnuAction.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabReviewLevel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdReviewLevel)).BeginInit();
            this.mnuReview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewReviewLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).BeginInit();
            this.tabAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).BeginInit();
            this.mnuAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mnuSelectMainGroupID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuReview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemDelete,
            this.toolStripMenuItem1,
            this.mnuItemExport,
            this.mnuItemExportTemplate,
            this.mnuSeparator2,
            this.mnuImportExcel,
            this.mnuItemImportIMEI});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(169, 126);
            this.mnuAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAction_Opening);
            // 
            // mnuItemDelete
            // 
            this.mnuItemDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelete.Image")));
            this.mnuItemDelete.Name = "mnuItemDelete";
            this.mnuItemDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuItemDelete.Size = new System.Drawing.Size(168, 22);
            this.mnuItemDelete.Text = "Xóa";
            this.mnuItemDelete.Click += new System.EventHandler(this.mnuItemDelete_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(165, 6);
            // 
            // mnuItemExport
            // 
            this.mnuItemExport.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExport.Image")));
            this.mnuItemExport.Name = "mnuItemExport";
            this.mnuItemExport.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnuItemExport.Size = new System.Drawing.Size(168, 22);
            this.mnuItemExport.Text = "Xuất Excel";
            this.mnuItemExport.Click += new System.EventHandler(this.mnuItemExport_Click);
            // 
            // mnuItemExportTemplate
            // 
            this.mnuItemExportTemplate.Image = global::ERP.Inventory.DUI.Properties.Resources.mnu_export_16;
            this.mnuItemExportTemplate.Name = "mnuItemExportTemplate";
            this.mnuItemExportTemplate.Size = new System.Drawing.Size(168, 22);
            this.mnuItemExportTemplate.Text = "Xuất Excel mẫu";
            this.mnuItemExportTemplate.Click += new System.EventHandler(this.mnuItemExportTemplate_Click);
            // 
            // mnuSeparator2
            // 
            this.mnuSeparator2.Name = "mnuSeparator2";
            this.mnuSeparator2.Size = new System.Drawing.Size(165, 6);
            // 
            // mnuImportExcel
            // 
            this.mnuImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.mnu_import_16;
            this.mnuImportExcel.Name = "mnuImportExcel";
            this.mnuImportExcel.Size = new System.Drawing.Size(168, 22);
            this.mnuImportExcel.Text = "Nhập từ Excel";
            this.mnuImportExcel.Click += new System.EventHandler(this.mnuImportExcel_Click);
            // 
            // mnuItemImportIMEI
            // 
            this.mnuItemImportIMEI.Image = global::ERP.Inventory.DUI.Properties.Resources.mnu_import_16;
            this.mnuItemImportIMEI.Name = "mnuItemImportIMEI";
            this.mnuItemImportIMEI.Size = new System.Drawing.Size(168, 22);
            this.mnuItemImportIMEI.Text = "Nhập IMEI";
            this.mnuItemImportIMEI.Click += new System.EventHandler(this.mnuItemImportIMEI_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.tabControl);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1108, 576);
            this.panel1.TabIndex = 1;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabGeneral);
            this.tabControl.Controls.Add(this.tabReviewLevel);
            this.tabControl.Controls.Add(this.tabAttachment);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1108, 538);
            this.tabControl.TabIndex = 0;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.groupControl2);
            this.tabGeneral.Controls.Add(this.groupControl1);
            this.tabGeneral.Location = new System.Drawing.Point(4, 25);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneral.Size = new System.Drawing.Size(1100, 509);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "Thông tin yêu cầu";
            this.tabGeneral.UseVisualStyleBackColor = true;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.grdProduct);
            this.groupControl2.Location = new System.Drawing.Point(3, 229);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1094, 272);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Thông tin sản phẩm";
            // 
            // grdProduct
            // 
            this.grdProduct.ContextMenuStrip = this.mnuAction;
            this.grdProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdProduct.Location = new System.Drawing.Point(2, 22);
            this.grdProduct.MainView = this.grdViewProduct;
            this.grdProduct.Name = "grdProduct";
            this.grdProduct.Size = new System.Drawing.Size(1090, 248);
            this.grdProduct.TabIndex = 0;
            this.grdProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProduct});
            // 
            // grdViewProduct
            // 
            this.grdViewProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProduct.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProduct.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProduct.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProduct.Appearance.Preview.Options.UseFont = true;
            this.grdViewProduct.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProduct.Appearance.Row.Options.UseFont = true;
            this.grdViewProduct.GridControl = this.grdProduct;
            this.grdViewProduct.Name = "grdViewProduct";
            this.grdViewProduct.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProduct.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewProduct.OptionsNavigation.UseTabKey = false;
            this.grdViewProduct.OptionsSelection.MultiSelect = true;
            this.grdViewProduct.OptionsView.ShowAutoFilterRow = true;
            this.grdViewProduct.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProduct.OptionsView.ShowFooter = true;
            this.grdViewProduct.OptionsView.ShowGroupPanel = false;
            this.grdViewProduct.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.grdViewProduct_RowCellStyle);
            this.grdViewProduct.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewProduct_ShowingEditor);
            this.grdViewProduct.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdViewProduct_CellValueChanged);
            this.grdViewProduct.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdViewProduct_CellValueChanging);
            this.grdViewProduct.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.grdViewProduct_ValidatingEditor);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.cboTransportTypeID);
            this.groupControl1.Controls.Add(this.cboTransportServices);
            this.groupControl1.Controls.Add(this.cboTransportCompany);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.cboInStockStatusID);
            this.groupControl1.Controls.Add(this.btnSearchProduct);
            this.groupControl1.Controls.Add(this.txtContentDeleted);
            this.groupControl1.Controls.Add(this.cboFromStoreID);
            this.groupControl1.Controls.Add(this.cboToStoreID);
            this.groupControl1.Controls.Add(this.groupBox1);
            this.groupControl1.Controls.Add(this.txtSearchProduct);
            this.groupControl1.Controls.Add(this.txtContent);
            this.groupControl1.Controls.Add(this.cboStoreChangeStatus);
            this.groupControl1.Controls.Add(this.dteExpiryDate);
            this.groupControl1.Controls.Add(this.dteOrderDate);
            this.groupControl1.Controls.Add(this.lblContentDeleted);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.lblStoreChangeStatus);
            this.groupControl1.Controls.Add(this.txtStoreChangeOrderID);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.groupBox2);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1094, 220);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin yêu cầu";
            // 
            // cboTransportTypeID
            // 
            this.cboTransportTypeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransportTypeID.Location = new System.Drawing.Point(91, 81);
            this.cboTransportTypeID.Margin = new System.Windows.Forms.Padding(0);
            this.cboTransportTypeID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboTransportTypeID.Name = "cboTransportTypeID";
            this.cboTransportTypeID.Size = new System.Drawing.Size(220, 24);
            this.cboTransportTypeID.TabIndex = 95;
            this.cboTransportTypeID.SelectionChangeCommitted += new System.EventHandler(this.cboTransportTypeID_SelectionChangeCommitted);
            // 
            // cboTransportServices
            // 
            this.cboTransportServices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransportServices.Location = new System.Drawing.Point(409, 111);
            this.cboTransportServices.Margin = new System.Windows.Forms.Padding(0);
            this.cboTransportServices.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboTransportServices.Name = "cboTransportServices";
            this.cboTransportServices.Size = new System.Drawing.Size(207, 24);
            this.cboTransportServices.TabIndex = 94;
            // 
            // cboTransportCompany
            // 
            this.cboTransportCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransportCompany.Location = new System.Drawing.Point(91, 111);
            this.cboTransportCompany.Margin = new System.Windows.Forms.Padding(0);
            this.cboTransportCompany.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboTransportCompany.Name = "cboTransportCompany";
            this.cboTransportCompany.Size = new System.Drawing.Size(220, 24);
            this.cboTransportCompany.TabIndex = 93;
            this.cboTransportCompany.SelectionChangeCommitted += new System.EventHandler(this.cboTransportCompany_SelectionChangeCommitted);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(334, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 16);
            this.label8.TabIndex = 92;
            this.label8.Text = "Dịch vụ:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 91;
            this.label7.Text = "Đối tác:";
            // 
            // cboInStockStatusID
            // 
            this.cboInStockStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInStockStatusID.Location = new System.Drawing.Point(317, 189);
            this.cboInStockStatusID.Name = "cboInStockStatusID";
            this.cboInStockStatusID.Size = new System.Drawing.Size(180, 24);
            this.cboInStockStatusID.TabIndex = 88;
            // 
            // btnSearchProduct
            // 
            this.btnSearchProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearchProduct.Location = new System.Drawing.Point(287, 188);
            this.btnSearchProduct.Name = "btnSearchProduct";
            this.btnSearchProduct.Size = new System.Drawing.Size(24, 24);
            this.btnSearchProduct.TabIndex = 20;
            this.btnSearchProduct.Click += new System.EventHandler(this.btnSearchProduct_Click);
            // 
            // txtContentDeleted
            // 
            this.txtContentDeleted.Location = new System.Drawing.Point(695, 81);
            this.txtContentDeleted.Multiline = true;
            this.txtContentDeleted.Name = "txtContentDeleted";
            this.txtContentDeleted.ReadOnly = true;
            this.txtContentDeleted.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContentDeleted.Size = new System.Drawing.Size(269, 94);
            this.txtContentDeleted.TabIndex = 15;
            // 
            // cboFromStoreID
            // 
            this.cboFromStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreID.Location = new System.Drawing.Point(91, 53);
            this.cboFromStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreID.Name = "cboFromStoreID";
            this.cboFromStoreID.Size = new System.Drawing.Size(427, 24);
            this.cboFromStoreID.TabIndex = 7;
            this.cboFromStoreID.SelectionChangeCommitted += new System.EventHandler(this.cboFromStoreID_SelectionChangeCommitted);
            // 
            // cboToStoreID
            // 
            this.cboToStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreID.Location = new System.Drawing.Point(618, 53);
            this.cboToStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreID.Name = "cboToStoreID";
            this.cboToStoreID.Size = new System.Drawing.Size(346, 24);
            this.cboToStoreID.TabIndex = 9;
            this.cboToStoreID.SelectionChangeCommitted += new System.EventHandler(this.cboToStoreID_SelectionChangeCommitted);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ckbIsUrgent);
            this.groupBox1.Controls.Add(this.ckbIsDeleted);
            this.groupBox1.Controls.Add(this.ckbIsReview);
            this.groupBox1.Location = new System.Drawing.Point(695, 181);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(269, 34);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            // 
            // ckbIsUrgent
            // 
            this.ckbIsUrgent.AutoSize = true;
            this.ckbIsUrgent.Location = new System.Drawing.Point(175, 11);
            this.ckbIsUrgent.Name = "ckbIsUrgent";
            this.ckbIsUrgent.Size = new System.Drawing.Size(99, 20);
            this.ckbIsUrgent.TabIndex = 2;
            this.ckbIsUrgent.Text = "Chuyển gấp";
            this.ckbIsUrgent.UseVisualStyleBackColor = true;
            // 
            // ckbIsDeleted
            // 
            this.ckbIsDeleted.AutoSize = true;
            this.ckbIsDeleted.Enabled = false;
            this.ckbIsDeleted.Location = new System.Drawing.Point(101, 11);
            this.ckbIsDeleted.Name = "ckbIsDeleted";
            this.ckbIsDeleted.Size = new System.Drawing.Size(68, 20);
            this.ckbIsDeleted.TabIndex = 1;
            this.ckbIsDeleted.Text = "Đã hủy";
            this.ckbIsDeleted.UseVisualStyleBackColor = true;
            // 
            // ckbIsReview
            // 
            this.ckbIsReview.AutoSize = true;
            this.ckbIsReview.Enabled = false;
            this.ckbIsReview.Location = new System.Drawing.Point(14, 11);
            this.ckbIsReview.Name = "ckbIsReview";
            this.ckbIsReview.Size = new System.Drawing.Size(80, 20);
            this.ckbIsReview.TabIndex = 0;
            this.ckbIsReview.Text = "Đã duyệt";
            this.ckbIsReview.UseVisualStyleBackColor = true;
            // 
            // txtSearchProduct
            // 
            this.txtSearchProduct.Location = new System.Drawing.Point(91, 189);
            this.txtSearchProduct.Name = "txtSearchProduct";
            this.txtSearchProduct.Size = new System.Drawing.Size(196, 22);
            this.txtSearchProduct.TabIndex = 19;
            this.txtSearchProduct.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchProduct_KeyPress);
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(91, 140);
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(525, 42);
            this.txtContent.TabIndex = 17;
            // 
            // cboStoreChangeStatus
            // 
            this.cboStoreChangeStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStoreChangeStatus.Enabled = false;
            this.cboStoreChangeStatus.FormattingEnabled = true;
            this.cboStoreChangeStatus.Location = new System.Drawing.Point(409, 81);
            this.cboStoreChangeStatus.Name = "cboStoreChangeStatus";
            this.cboStoreChangeStatus.Size = new System.Drawing.Size(207, 24);
            this.cboStoreChangeStatus.TabIndex = 13;
            // 
            // dteExpiryDate
            // 
            this.dteExpiryDate.CustomFormat = "dd/MM/yyyy ";
            this.dteExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteExpiryDate.Location = new System.Drawing.Point(618, 27);
            this.dteExpiryDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dteExpiryDate.Name = "dteExpiryDate";
            this.dteExpiryDate.Size = new System.Drawing.Size(109, 22);
            this.dteExpiryDate.TabIndex = 5;
            // 
            // dteOrderDate
            // 
            this.dteOrderDate.CustomFormat = "dd/MM/yyyy ";
            this.dteOrderDate.Enabled = false;
            this.dteOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteOrderDate.Location = new System.Drawing.Point(409, 27);
            this.dteOrderDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dteOrderDate.Name = "dteOrderDate";
            this.dteOrderDate.Size = new System.Drawing.Size(109, 22);
            this.dteOrderDate.TabIndex = 3;
            // 
            // lblContentDeleted
            // 
            this.lblContentDeleted.AutoSize = true;
            this.lblContentDeleted.Enabled = false;
            this.lblContentDeleted.Location = new System.Drawing.Point(628, 87);
            this.lblContentDeleted.Name = "lblContentDeleted";
            this.lblContentDeleted.Size = new System.Drawing.Size(68, 16);
            this.lblContentDeleted.TabIndex = 14;
            this.lblContentDeleted.Text = "Lý do hủy:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(526, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "Kho nhập:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(526, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngày hết hạn:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Kho xuất:";
            // 
            // lblStoreChangeStatus
            // 
            this.lblStoreChangeStatus.AutoSize = true;
            this.lblStoreChangeStatus.Enabled = false;
            this.lblStoreChangeStatus.Location = new System.Drawing.Point(318, 85);
            this.lblStoreChangeStatus.Name = "lblStoreChangeStatus";
            this.lblStoreChangeStatus.Size = new System.Drawing.Size(71, 16);
            this.lblStoreChangeStatus.TabIndex = 12;
            this.lblStoreChangeStatus.Text = "Trạng thái:";
            // 
            // txtStoreChangeOrderID
            // 
            this.txtStoreChangeOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtStoreChangeOrderID.Location = new System.Drawing.Point(91, 27);
            this.txtStoreChangeOrderID.Name = "txtStoreChangeOrderID";
            this.txtStoreChangeOrderID.ReadOnly = true;
            this.txtStoreChangeOrderID.Size = new System.Drawing.Size(220, 22);
            this.txtStoreChangeOrderID.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 191);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 16);
            this.label10.TabIndex = 18;
            this.label10.Text = "BarCode:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 16;
            this.label9.Text = "Nội dung:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Phương tiện:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(318, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ngày yêu cầu:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã yêu cầu:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Location = new System.Drawing.Point(494, 181);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 34);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rbtnQuantity);
            this.panel3.Controls.Add(this.rbtnIMEI);
            this.panel3.Location = new System.Drawing.Point(10, 8);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(184, 20);
            this.panel3.TabIndex = 24;
            // 
            // rbtnQuantity
            // 
            this.rbtnQuantity.AutoSize = true;
            this.rbtnQuantity.Location = new System.Drawing.Point(95, 2);
            this.rbtnQuantity.Name = "rbtnQuantity";
            this.rbtnQuantity.Size = new System.Drawing.Size(78, 20);
            this.rbtnQuantity.TabIndex = 1;
            this.rbtnQuantity.TabStop = true;
            this.rbtnQuantity.Text = "Nhập SL";
            this.rbtnQuantity.UseVisualStyleBackColor = true;
            // 
            // rbtnIMEI
            // 
            this.rbtnIMEI.AutoSize = true;
            this.rbtnIMEI.Location = new System.Drawing.Point(3, 2);
            this.rbtnIMEI.Name = "rbtnIMEI";
            this.rbtnIMEI.Size = new System.Drawing.Size(88, 20);
            this.rbtnIMEI.TabIndex = 0;
            this.rbtnIMEI.TabStop = true;
            this.rbtnIMEI.Text = "Nhập IMEI";
            this.rbtnIMEI.UseVisualStyleBackColor = true;
            this.rbtnIMEI.CheckedChanged += new System.EventHandler(this.rbtnIMEI_CheckedChanged);
            // 
            // tabReviewLevel
            // 
            this.tabReviewLevel.Controls.Add(this.groupControl3);
            this.tabReviewLevel.Location = new System.Drawing.Point(4, 25);
            this.tabReviewLevel.Name = "tabReviewLevel";
            this.tabReviewLevel.Padding = new System.Windows.Forms.Padding(3);
            this.tabReviewLevel.Size = new System.Drawing.Size(1100, 509);
            this.tabReviewLevel.TabIndex = 1;
            this.tabReviewLevel.Text = "Mức duyệt";
            this.tabReviewLevel.UseVisualStyleBackColor = true;
            this.tabReviewLevel.Enter += new System.EventHandler(this.tabReviewLevel_Enter);
            // 
            // groupControl3
            // 
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.grdReviewLevel);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(3, 3);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1094, 503);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "Danh sách mức duyệt";
            // 
            // grdReviewLevel
            // 
            this.grdReviewLevel.ContextMenuStrip = this.mnuReview;
            this.grdReviewLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdReviewLevel.Location = new System.Drawing.Point(2, 22);
            this.grdReviewLevel.MainView = this.grdViewReviewLevel;
            this.grdReviewLevel.Name = "grdReviewLevel";
            this.grdReviewLevel.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.chkActiveItem,
            this.chkSystemItem});
            this.grdReviewLevel.Size = new System.Drawing.Size(1090, 479);
            this.grdReviewLevel.TabIndex = 1;
            this.grdReviewLevel.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewReviewLevel});
            // 
            // mnuReview
            // 
            this.mnuReview.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdExcelReview});
            this.mnuReview.Name = "contextMenuStrip1";
            this.mnuReview.Size = new System.Drawing.Size(129, 26);
            // 
            // cmdExcelReview
            // 
            this.cmdExcelReview.Image = ((System.Drawing.Image)(resources.GetObject("cmdExcelReview.Image")));
            this.cmdExcelReview.Name = "cmdExcelReview";
            this.cmdExcelReview.Size = new System.Drawing.Size(128, 22);
            this.cmdExcelReview.Text = "Xuất excel";
            this.cmdExcelReview.Click += new System.EventHandler(this.cmdExcelReview_Click);
            // 
            // grdViewReviewLevel
            // 
            this.grdViewReviewLevel.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewReviewLevel.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewReviewLevel.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewReviewLevel.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewReviewLevel.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewReviewLevel.Appearance.Preview.Options.UseFont = true;
            this.grdViewReviewLevel.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewReviewLevel.Appearance.Row.Options.UseFont = true;
            this.grdViewReviewLevel.GridControl = this.grdReviewLevel;
            this.grdViewReviewLevel.Name = "grdViewReviewLevel";
            this.grdViewReviewLevel.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewReviewLevel.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewReviewLevel.OptionsNavigation.UseTabKey = false;
            this.grdViewReviewLevel.OptionsView.ShowAutoFilterRow = true;
            this.grdViewReviewLevel.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewReviewLevel.OptionsView.ShowFooter = true;
            this.grdViewReviewLevel.OptionsView.ShowGroupPanel = false;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // chkActiveItem
            // 
            this.chkActiveItem.AutoHeight = false;
            this.chkActiveItem.Name = "chkActiveItem";
            this.chkActiveItem.ValueChecked = ((short)(1));
            this.chkActiveItem.ValueUnchecked = ((short)(0));
            // 
            // chkSystemItem
            // 
            this.chkSystemItem.AutoHeight = false;
            this.chkSystemItem.Name = "chkSystemItem";
            this.chkSystemItem.ValueChecked = ((short)(1));
            this.chkSystemItem.ValueUnchecked = ((short)(0));
            // 
            // tabAttachment
            // 
            this.tabAttachment.Controls.Add(this.grdAttachment);
            this.tabAttachment.Location = new System.Drawing.Point(4, 25);
            this.tabAttachment.Name = "tabAttachment";
            this.tabAttachment.Padding = new System.Windows.Forms.Padding(3);
            this.tabAttachment.Size = new System.Drawing.Size(1100, 509);
            this.tabAttachment.TabIndex = 2;
            this.tabAttachment.Text = "Thông tin đính kèm";
            this.tabAttachment.UseVisualStyleBackColor = true;
            this.tabAttachment.Enter += new System.EventHandler(this.tabAttachment_Enter);
            // 
            // grdAttachment
            // 
            this.grdAttachment.ContextMenuStrip = this.mnuAttachment;
            this.grdAttachment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdAttachment.Location = new System.Drawing.Point(3, 3);
            this.grdAttachment.MainView = this.grvAttachment;
            this.grdAttachment.Name = "grdAttachment";
            this.grdAttachment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDownload,
            this.btnView});
            this.grdAttachment.Size = new System.Drawing.Size(1094, 503);
            this.grdAttachment.TabIndex = 2;
            this.grdAttachment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvAttachment});
            // 
            // mnuAttachment
            // 
            this.mnuAttachment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.mnuAttachment.Name = "contextMenuStrip1";
            this.mnuAttachment.Size = new System.Drawing.Size(184, 26);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.toolStripMenuItem2.Size = new System.Drawing.Size(183, 22);
            this.toolStripMenuItem2.Text = "Thêm tập tin";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // grvAttachment
            // 
            this.grvAttachment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvAttachment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvAttachment.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvAttachment.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvAttachment.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvAttachment.Appearance.Row.Options.UseFont = true;
            this.grvAttachment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVOFFICEID,
            this.colAttachmentName,
            this.colDescription,
            this.colCreatedDate,
            this.colCreatedUser,
            this.colAttachMENTID,
            this.colAttachMENTPath,
            this.colView,
            this.colDownload});
            this.grvAttachment.GridControl = this.grdAttachment;
            this.grvAttachment.Name = "grvAttachment";
            this.grvAttachment.OptionsView.ShowGroupPanel = false;
            // 
            // colVOFFICEID
            // 
            this.colVOFFICEID.Caption = "Mã tờ trình(VOFFICE)";
            this.colVOFFICEID.FieldName = "VOFFICEID";
            this.colVOFFICEID.Name = "colVOFFICEID";
            this.colVOFFICEID.OptionsColumn.AllowEdit = false;
            this.colVOFFICEID.Visible = true;
            this.colVOFFICEID.VisibleIndex = 0;
            // 
            // colAttachmentName
            // 
            this.colAttachmentName.Caption = "Tên tập tin";
            this.colAttachmentName.FieldName = "ATTACHMENTNAME";
            this.colAttachmentName.Name = "colAttachmentName";
            this.colAttachmentName.OptionsColumn.AllowEdit = false;
            this.colAttachmentName.Visible = true;
            this.colAttachmentName.VisibleIndex = 1;
            this.colAttachmentName.Width = 228;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Mô tả";
            this.colDescription.FieldName = "DESCRIPTION";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 2;
            // 
            // colCreatedDate
            // 
            this.colCreatedDate.Caption = "Thời gian tạo";
            this.colCreatedDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colCreatedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCreatedDate.FieldName = "CREATEDDATE";
            this.colCreatedDate.Name = "colCreatedDate";
            this.colCreatedDate.OptionsColumn.AllowEdit = false;
            this.colCreatedDate.Visible = true;
            this.colCreatedDate.VisibleIndex = 3;
            this.colCreatedDate.Width = 140;
            // 
            // colCreatedUser
            // 
            this.colCreatedUser.Caption = "Nhân viên tạo";
            this.colCreatedUser.FieldName = "CREATEDUSER";
            this.colCreatedUser.Name = "colCreatedUser";
            this.colCreatedUser.OptionsColumn.AllowEdit = false;
            this.colCreatedUser.Visible = true;
            this.colCreatedUser.VisibleIndex = 4;
            this.colCreatedUser.Width = 240;
            // 
            // colAttachMENTID
            // 
            this.colAttachMENTID.Caption = "FileID";
            this.colAttachMENTID.FieldName = "ATTACHMENTID";
            this.colAttachMENTID.Name = "colAttachMENTID";
            this.colAttachMENTID.OptionsColumn.AllowEdit = false;
            this.colAttachMENTID.Visible = true;
            this.colAttachMENTID.VisibleIndex = 5;
            // 
            // colAttachMENTPath
            // 
            this.colAttachMENTPath.Caption = "FilePath";
            this.colAttachMENTPath.FieldName = "ATTACHMENTPATH";
            this.colAttachMENTPath.Name = "colAttachMENTPath";
            this.colAttachMENTPath.OptionsColumn.AllowEdit = false;
            this.colAttachMENTPath.Visible = true;
            this.colAttachMENTPath.VisibleIndex = 6;
            // 
            // colView
            // 
            this.colView.Caption = "Xem";
            this.colView.ColumnEdit = this.btnView;
            this.colView.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colView.Name = "colView";
            this.colView.OptionsColumn.AllowSize = false;
            this.colView.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colView.OptionsColumn.FixedWidth = true;
            this.colView.OptionsColumn.ReadOnly = true;
            this.colView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colView.Visible = true;
            this.colView.VisibleIndex = 7;
            this.colView.Width = 40;
            // 
            // btnView
            // 
            this.btnView.AutoHeight = false;
            this.btnView.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::ERP.Inventory.DUI.Properties.Resources.view, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Xem tập tin", null, null, false)});
            this.btnView.Name = "btnView";
            this.btnView.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnView_ButtonClick);
            // 
            // colDownload
            // 
            this.colDownload.Caption = "Tải";
            this.colDownload.ColumnEdit = this.btnDownload;
            this.colDownload.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colDownload.Name = "colDownload";
            this.colDownload.OptionsColumn.AllowMove = false;
            this.colDownload.OptionsColumn.AllowSize = false;
            this.colDownload.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDownload.OptionsColumn.FixedWidth = true;
            this.colDownload.OptionsColumn.ReadOnly = true;
            this.colDownload.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colDownload.Visible = true;
            this.colDownload.VisibleIndex = 8;
            this.colDownload.Width = 40;
            // 
            // btnDownload
            // 
            this.btnDownload.AutoHeight = false;
            this.btnDownload.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::ERP.Inventory.DUI.Properties.Resources.Folder_Downloads_icon1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Tải tập tin", null, null, false)});
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnDownload_ButtonClick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Info;
            this.panel2.Controls.Add(this.btnTrinhky);
            this.panel2.Controls.Add(this.btnVOffice);
            this.panel2.Controls.Add(this.btnStoreChangeAll);
            this.panel2.Controls.Add(this.dropdownStoreChange);
            this.panel2.Controls.Add(this.btnReview);
            this.panel2.Controls.Add(this.cmdReview);
            this.panel2.Controls.Add(this.btnUpdate);
            this.panel2.Controls.Add(this.btnDelete);
            this.panel2.Controls.Add(this.lblWarning);
            this.panel2.Controls.Add(this.barDockControlLeft);
            this.panel2.Controls.Add(this.barDockControlRight);
            this.panel2.Controls.Add(this.barDockControlBottom);
            this.panel2.Controls.Add(this.barDockControlTop);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 538);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1108, 38);
            this.panel2.TabIndex = 1;
            // 
            // btnTrinhky
            // 
            this.btnTrinhky.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTrinhky.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.btnTrinhky.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTrinhky.Location = new System.Drawing.Point(559, 7);
            this.btnTrinhky.Name = "btnTrinhky";
            this.btnTrinhky.Size = new System.Drawing.Size(116, 25);
            this.btnTrinhky.TabIndex = 16;
            this.btnTrinhky.Text = "     File Trình ký";
            this.btnTrinhky.UseVisualStyleBackColor = true;
            this.btnTrinhky.Visible = false;
            this.btnTrinhky.Click += new System.EventHandler(this.btnTrinhky_Click);
            // 
            // btnVOffice
            // 
            this.btnVOffice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVOffice.Image = ((System.Drawing.Image)(resources.GetObject("btnVOffice.Image")));
            this.btnVOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVOffice.Location = new System.Drawing.Point(681, 7);
            this.btnVOffice.Name = "btnVOffice";
            this.btnVOffice.Size = new System.Drawing.Size(118, 25);
            this.btnVOffice.TabIndex = 15;
            this.btnVOffice.Text = "     Duyệt VOffice";
            this.btnVOffice.UseVisualStyleBackColor = true;
            this.btnVOffice.Visible = false;
            this.btnVOffice.Click += new System.EventHandler(this.btnVOffice_Click);
            // 
            // btnStoreChangeAll
            // 
            this.btnStoreChangeAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStoreChangeAll.Image = ((System.Drawing.Image)(resources.GetObject("btnStoreChangeAll.Image")));
            this.btnStoreChangeAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStoreChangeAll.Location = new System.Drawing.Point(627, 7);
            this.btnStoreChangeAll.Name = "btnStoreChangeAll";
            this.btnStoreChangeAll.Size = new System.Drawing.Size(172, 25);
            this.btnStoreChangeAll.TabIndex = 10;
            this.btnStoreChangeAll.Text = "  Xuất chuyển kho tất cả";
            this.btnStoreChangeAll.UseVisualStyleBackColor = true;
            this.btnStoreChangeAll.Click += new System.EventHandler(this.btnStoreChangeAll_Click);
            // 
            // dropdownStoreChange
            // 
            this.dropdownStoreChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropdownStoreChange.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropdownStoreChange.Appearance.Options.UseFont = true;
            this.dropdownStoreChange.DropDownControl = this.mnuSelectMainGroupID;
            this.dropdownStoreChange.Image = global::ERP.Inventory.DUI.Properties.Resources.undo2;
            this.dropdownStoreChange.Location = new System.Drawing.Point(665, 7);
            this.dropdownStoreChange.LookAndFeel.SkinName = "Blue";
            this.dropdownStoreChange.Name = "dropdownStoreChange";
            this.dropdownStoreChange.Size = new System.Drawing.Size(134, 25);
            this.dropdownStoreChange.TabIndex = 1;
            this.dropdownStoreChange.Text = "Xuất chuyển kho";
            // 
            // mnuSelectMainGroupID
            // 
            this.mnuSelectMainGroupID.Manager = this.barManager1;
            this.mnuSelectMainGroupID.Name = "mnuSelectMainGroupID";
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowMoveBarOnToolbar = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.AllowShowToolbarsPopup = false;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            this.barManager1.ShowScreenTipsInToolbars = false;
            this.barManager1.ShowShortcutInScreenTips = false;
            this.barManager1.UseAltKeyForMenu = false;
            this.barManager1.UseF10KeyForMenu = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1108, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 38);
            this.barDockControlBottom.Size = new System.Drawing.Size(1108, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 38);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1108, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 38);
            // 
            // btnReview
            // 
            this.btnReview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReview.Image = ((System.Drawing.Image)(resources.GetObject("btnReview.Image")));
            this.btnReview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReview.Location = new System.Drawing.Point(805, 7);
            this.btnReview.Name = "btnReview";
            this.btnReview.Size = new System.Drawing.Size(95, 25);
            this.btnReview.TabIndex = 3;
            this.btnReview.Text = "  Duyệt";
            this.btnReview.UseVisualStyleBackColor = true;
            this.btnReview.Click += new System.EventHandler(this.btnReview_Click);
            // 
            // cmdReview
            // 
            this.cmdReview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdReview.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdReview.Appearance.Options.UseFont = true;
            this.cmdReview.DropDownControl = this.pMnuReview;
            this.cmdReview.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.cmdReview.Location = new System.Drawing.Point(704, 7);
            this.cmdReview.LookAndFeel.SkinName = "Blue";
            this.cmdReview.Name = "cmdReview";
            this.cmdReview.Size = new System.Drawing.Size(95, 25);
            this.cmdReview.TabIndex = 2;
            this.cmdReview.Text = "Duyệt";
            this.cmdReview.Visible = false;
            // 
            // pMnuReview
            // 
            this.pMnuReview.Manager = this.barManager2;
            this.pMnuReview.Name = "pMnuReview";
            // 
            // barManager2
            // 
            this.barManager2.AllowCustomization = false;
            this.barManager2.AllowMoveBarOnToolbar = false;
            this.barManager2.AllowQuickCustomization = false;
            this.barManager2.AllowShowToolbarsPopup = false;
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1});
            this.barManager2.MaxItemId = 1;
            this.barManager2.ShowScreenTipsInToolbars = false;
            this.barManager2.ShowShortcutInScreenTips = false;
            this.barManager2.UseAltKeyForMenu = false;
            this.barManager2.UseF10KeyForMenu = false;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(1108, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 576);
            this.barDockControl2.Size = new System.Drawing.Size(1108, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 576);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1108, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 576);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Đồng ý";
            this.barSubItem1.Id = 0;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(1005, 7);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Enabled = false;
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(905, 7);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(95, 25);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Hủy";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblWarning
            // 
            this.lblWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarning.ForeColor = System.Drawing.Color.Blue;
            this.lblWarning.Location = new System.Drawing.Point(5, 11);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(291, 16);
            this.lblWarning.TabIndex = 0;
            this.lblWarning.Text = "Đơn hàng đã nhập hết hoặc bạn không có quyền nhập trên ngành hàng";
            this.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblWarning.Visible = false;
            // 
            // frmStoreChangeOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1108, 576);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "frmStoreChangeOrder";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý yêu cầu chuyển kho";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmStoreChangeOrder_FormClosing);
            this.Load += new System.EventHandler(this.frmStoreChangeOrder_Load);
            this.mnuAction.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabReviewLevel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdReviewLevel)).EndInit();
            this.mnuReview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewReviewLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).EndInit();
            this.tabAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).EndInit();
            this.mnuAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mnuSelectMainGroupID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuReview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.TabPage tabReviewLevel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnUpdate;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.DateTimePicker dteExpiryDate;
        private System.Windows.Forms.DateTimePicker dteOrderDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStoreChangeOrderID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraGrid.GridControl grdReviewLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewReviewLevel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkActiveItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkSystemItem;
        private System.Windows.Forms.TextBox txtSearchProduct;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.TextBox txtContentDeleted;
        private System.Windows.Forms.ComboBox cboStoreChangeStatus;
        private System.Windows.Forms.Label lblContentDeleted;
        private System.Windows.Forms.Label lblStoreChangeStatus;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraGrid.GridControl grdProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewProduct;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ckbIsUrgent;
        private System.Windows.Forms.CheckBox ckbIsDeleted;
        private System.Windows.Forms.CheckBox ckbIsReview;
        private System.Windows.Forms.Button btnSearchProduct;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ContextMenuStrip mnuReview;
        private System.Windows.Forms.ToolStripMenuItem cmdExcelReview;
        private DevExpress.XtraEditors.DropDownButton dropdownStoreChange;
        private DevExpress.XtraBars.PopupMenu mnuSelectMainGroupID;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreID;
        private DevExpress.XtraEditors.DropDownButton cmdReview;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.PopupMenu pMnuReview;
        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private System.Windows.Forms.Button btnReview;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportTemplate;
        private System.Windows.Forms.ToolStripSeparator mnuSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuImportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportIMEI;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rbtnIMEI;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbtnQuantity;
        private System.Windows.Forms.ComboBox cboInStockStatusID;
        private System.Windows.Forms.Button btnStoreChangeAll;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboTransportServices;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboTransportCompany;
        private System.Windows.Forms.TabPage tabAttachment;
        private System.Windows.Forms.Button btnTrinhky;
        private System.Windows.Forms.Button btnVOffice;
        private DevExpress.XtraGrid.GridControl grdAttachment;
        private DevExpress.XtraGrid.Views.Grid.GridView grvAttachment;
        private DevExpress.XtraGrid.Columns.GridColumn colAttachmentName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedUser;
        private DevExpress.XtraGrid.Columns.GridColumn colView;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnView;
        private DevExpress.XtraGrid.Columns.GridColumn colDownload;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDownload;
        private DevExpress.XtraGrid.Columns.GridColumn colVOFFICEID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAttachMENTID;
        private DevExpress.XtraGrid.Columns.GridColumn colAttachMENTPath;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboTransportTypeID;
        private System.Windows.Forms.ContextMenuStrip mnuAttachment;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}