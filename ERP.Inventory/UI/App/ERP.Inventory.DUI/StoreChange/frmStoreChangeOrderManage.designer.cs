﻿namespace ERP.Inventory.DUI.StoreChange
{
    partial class frmStoreChangeOrderManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStoreChangeOrderManager));
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExport = new System.Windows.Forms.ToolStripMenuItem();
            this.itemPrintOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.itemPrintOrderByProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.itemPrintOrderByStore = new System.Windows.Forms.ToolStripMenuItem();
            this.itemPrintInputOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.itemPrintOrderRecords = new System.Windows.Forms.ToolStripMenuItem();
            this.itemPrintInputOrderDetail = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintReport1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintReport = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintStoreChangeOrderDetail = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuStoreChangeOrderExport = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.grdProduct = new DevExpress.XtraGrid.GridControl();
            this.grdViewProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnDelete = new System.Windows.Forms.Button();
            this.cboSubGroupIDList = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.label11 = new System.Windows.Forms.Label();
            this.cboIsReview = new System.Windows.Forms.ComboBox();
            this.cboStoreChangeStatusIDList = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label10 = new System.Windows.Forms.Label();
            this.cboSearchBy = new System.Windows.Forms.ComboBox();
            this.cboMainGroup = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cboToStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboFromStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chkExpire = new System.Windows.Forms.CheckBox();
            this.chkDelete = new System.Windows.Forms.CheckBox();
            this.chkUrgent = new System.Windows.Forms.CheckBox();
            this.txtKeyWords = new System.Windows.Forms.TextBox();
            this.ucCreateUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.cboSCType = new System.Windows.Forms.ComboBox();
            this.cboSCOrderType = new System.Windows.Forms.ComboBox();
            this.dteToDate = new System.Windows.Forms.DateTimePicker();
            this.dteFromDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblStoreChangeStatus = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxMainGroup1 = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.mnuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAction.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExport,
            this.itemPrintOrder,
            this.itemPrintOrderByProduct,
            this.itemPrintOrderByStore,
            this.itemPrintInputOrder,
            this.itemPrintOrderRecords,
            this.itemPrintInputOrderDetail,
            this.mnuItemPrintReport1,
            this.mnuItemPrintReport,
            this.mnuItemPrintStoreChangeOrderDetail,
            this.mnuStoreChangeOrderExport,
            this.mnuCopy});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(345, 290);
            this.mnuAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAction_Opening);
            // 
            // mnuItemExport
            // 
            this.mnuItemExport.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExport.Image")));
            this.mnuItemExport.Name = "mnuItemExport";
            this.mnuItemExport.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnuItemExport.Size = new System.Drawing.Size(344, 22);
            this.mnuItemExport.Text = "Xuất excel";
            this.mnuItemExport.Click += new System.EventHandler(this.mnuItemExport_Click);
            // 
            // itemPrintOrder
            // 
            this.itemPrintOrder.Image = ((System.Drawing.Image)(resources.GetObject("itemPrintOrder.Image")));
            this.itemPrintOrder.Name = "itemPrintOrder";
            this.itemPrintOrder.Size = new System.Drawing.Size(344, 22);
            this.itemPrintOrder.Text = "In phiếu lấy hàng";
            this.itemPrintOrder.Click += new System.EventHandler(this.itemPrintOrder_Click);
            // 
            // itemPrintOrderByProduct
            // 
            this.itemPrintOrderByProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.itemPrintOrderByProduct.Name = "itemPrintOrderByProduct";
            this.itemPrintOrderByProduct.Size = new System.Drawing.Size(344, 22);
            this.itemPrintOrderByProduct.Text = "In phiếu lấy hàng theo sản phẩm";
            this.itemPrintOrderByProduct.Click += new System.EventHandler(this.itemPrintOrderByProduct_Click);
            // 
            // itemPrintOrderByStore
            // 
            this.itemPrintOrderByStore.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.itemPrintOrderByStore.Name = "itemPrintOrderByStore";
            this.itemPrintOrderByStore.Size = new System.Drawing.Size(344, 22);
            this.itemPrintOrderByStore.Text = "In phiếu lấy hàng nhóm theo kho";
            this.itemPrintOrderByStore.Click += new System.EventHandler(this.itemPrintOrderByStore_Click);
            // 
            // itemPrintInputOrder
            // 
            this.itemPrintInputOrder.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.itemPrintInputOrder.Name = "itemPrintInputOrder";
            this.itemPrintInputOrder.Size = new System.Drawing.Size(344, 22);
            this.itemPrintInputOrder.Text = "In phiếu nhặt hàng";
            this.itemPrintInputOrder.Click += new System.EventHandler(this.itemPrintInputOrder_Click);
            // 
            // itemPrintOrderRecords
            // 
            this.itemPrintOrderRecords.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.itemPrintOrderRecords.Name = "itemPrintOrderRecords";
            this.itemPrintOrderRecords.Size = new System.Drawing.Size(344, 22);
            this.itemPrintOrderRecords.Text = "In biên bản bàn giao hàng hóa";
            this.itemPrintOrderRecords.Visible = false;
            this.itemPrintOrderRecords.Click += new System.EventHandler(this.itemPrintOrderRecords_Click);
            // 
            // itemPrintInputOrderDetail
            // 
            this.itemPrintInputOrderDetail.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.itemPrintInputOrderDetail.Name = "itemPrintInputOrderDetail";
            this.itemPrintInputOrderDetail.Size = new System.Drawing.Size(344, 22);
            this.itemPrintInputOrderDetail.Text = "In phiếu nhặt hàng riêng lẻ";
            this.itemPrintInputOrderDetail.Click += new System.EventHandler(this.itemPrintInputOrderDetail_Click);
            // 
            // mnuItemPrintReport1
            // 
            this.mnuItemPrintReport1.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.mnuItemPrintReport1.Name = "mnuItemPrintReport1";
            this.mnuItemPrintReport1.Size = new System.Drawing.Size(344, 22);
            this.mnuItemPrintReport1.Text = "In lệnh điều động nội bộ hàng ký gửi";
            this.mnuItemPrintReport1.Click += new System.EventHandler(this.mnuItemPrintReport1_Click);
            // 
            // mnuItemPrintReport
            // 
            this.mnuItemPrintReport.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.mnuItemPrintReport.Name = "mnuItemPrintReport";
            this.mnuItemPrintReport.Size = new System.Drawing.Size(344, 22);
            this.mnuItemPrintReport.Text = "In lệnh điều động nội bộ";
            this.mnuItemPrintReport.Click += new System.EventHandler(this.mnuItemPrintReport_Click);
            // 
            // mnuItemPrintStoreChangeOrderDetail
            // 
            this.mnuItemPrintStoreChangeOrderDetail.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.mnuItemPrintStoreChangeOrderDetail.Name = "mnuItemPrintStoreChangeOrderDetail";
            this.mnuItemPrintStoreChangeOrderDetail.Size = new System.Drawing.Size(344, 22);
            this.mnuItemPrintStoreChangeOrderDetail.Text = "Xem báo cáo chi tiết yêu cầu chuyển kho bảo hành";
            this.mnuItemPrintStoreChangeOrderDetail.Click += new System.EventHandler(this.mnuItemPrintStoreChangeOrderDetail_Click);
            // 
            // mnuStoreChangeOrderExport
            // 
            this.mnuStoreChangeOrderExport.Image = global::ERP.Inventory.DUI.Properties.Resources._switch;
            this.mnuStoreChangeOrderExport.Name = "mnuStoreChangeOrderExport";
            this.mnuStoreChangeOrderExport.Size = new System.Drawing.Size(344, 22);
            this.mnuStoreChangeOrderExport.Text = "Tạo xuất chuyển kho";
            this.mnuStoreChangeOrderExport.Click += new System.EventHandler(this.mnuStoreChangeOrderExport_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.splitContainerControl1);
            this.panel1.Controls.Add(this.groupControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1102, 572);
            this.panel1.TabIndex = 1;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 143);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.grdData);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.grdProduct);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1102, 429);
            this.splitContainerControl1.SplitterPosition = 216;
            this.splitContainerControl1.TabIndex = 3;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // grdData
            // 
            this.grdData.ContextMenuStrip = this.mnuAction;
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(0, 0);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3});
            this.grdData.Size = new System.Drawing.Size(1102, 216);
            this.grdData.TabIndex = 1;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            this.grdData.DoubleClick += new System.EventHandler(this.grdViewData_DoubleClick);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewData.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grdViewData_FocusedRowChanged);
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit2.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit3.ValueUnchecked = ((short)(0));
            // 
            // grdProduct
            // 
            this.grdProduct.ContextMenuStrip = this.mnuAction;
            this.grdProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdProduct.Location = new System.Drawing.Point(0, 0);
            this.grdProduct.MainView = this.grdViewProduct;
            this.grdProduct.Name = "grdProduct";
            this.grdProduct.Size = new System.Drawing.Size(1102, 208);
            this.grdProduct.TabIndex = 1;
            this.grdProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProduct});
            // 
            // grdViewProduct
            // 
            this.grdViewProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProduct.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProduct.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProduct.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProduct.Appearance.Preview.Options.UseFont = true;
            this.grdViewProduct.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProduct.Appearance.Row.Options.UseFont = true;
            this.grdViewProduct.GridControl = this.grdProduct;
            this.grdViewProduct.Name = "grdViewProduct";
            this.grdViewProduct.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProduct.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewProduct.OptionsNavigation.UseTabKey = false;
            this.grdViewProduct.OptionsSelection.MultiSelect = true;
            this.grdViewProduct.OptionsView.ShowAutoFilterRow = true;
            this.grdViewProduct.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProduct.OptionsView.ShowFooter = true;
            this.grdViewProduct.OptionsView.ShowGroupPanel = false;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btnDelete);
            this.groupControl1.Controls.Add(this.cboSubGroupIDList);
            this.groupControl1.Controls.Add(this.label11);
            this.groupControl1.Controls.Add(this.cboIsReview);
            this.groupControl1.Controls.Add(this.cboStoreChangeStatusIDList);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.cboSearchBy);
            this.groupControl1.Controls.Add(this.cboMainGroup);
            this.groupControl1.Controls.Add(this.cboToStoreID);
            this.groupControl1.Controls.Add(this.cboFromStoreID);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.chkExpire);
            this.groupControl1.Controls.Add(this.chkDelete);
            this.groupControl1.Controls.Add(this.chkUrgent);
            this.groupControl1.Controls.Add(this.txtKeyWords);
            this.groupControl1.Controls.Add(this.ucCreateUser);
            this.groupControl1.Controls.Add(this.cboSCType);
            this.groupControl1.Controls.Add(this.cboSCOrderType);
            this.groupControl1.Controls.Add(this.dteToDate);
            this.groupControl1.Controls.Add(this.dteFromDate);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.lblStoreChangeStatus);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(1102, 143);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(857, 108);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(106, 24);
            this.btnDelete.TabIndex = 28;
            this.btnDelete.Text = "    Hủy phiếu";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // cboSubGroupIDList
            // 
            this.cboSubGroupIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroupIDList.Location = new System.Drawing.Point(427, 58);
            this.cboSubGroupIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroupIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroupIDList.Name = "cboSubGroupIDList";
            this.cboSubGroupIDList.Size = new System.Drawing.Size(193, 24);
            this.cboSubGroupIDList.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(308, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 16);
            this.label11.TabIndex = 27;
            this.label11.Text = "Nhóm hàng:";
            // 
            // cboIsReview
            // 
            this.cboIsReview.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsReview.FormattingEnabled = true;
            this.cboIsReview.Items.AddRange(new object[] {
            "-- Tất cả --",
            "Chưa duyệt",
            "Đã duyệt"});
            this.cboIsReview.Location = new System.Drawing.Point(117, 84);
            this.cboIsReview.Name = "cboIsReview";
            this.cboIsReview.Size = new System.Drawing.Size(184, 24);
            this.cboIsReview.TabIndex = 11;
            // 
            // cboStoreChangeStatusIDList
            // 
            this.cboStoreChangeStatusIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreChangeStatusIDList.Location = new System.Drawing.Point(427, 84);
            this.cboStoreChangeStatusIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreChangeStatusIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreChangeStatusIDList.Name = "cboStoreChangeStatusIDList";
            this.cboStoreChangeStatusIDList.Size = new System.Drawing.Size(193, 24);
            this.cboStoreChangeStatusIDList.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(308, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 16);
            this.label10.TabIndex = 24;
            this.label10.Text = "Tìm theo:";
            // 
            // cboSearchBy
            // 
            this.cboSearchBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchBy.FormattingEnabled = true;
            this.cboSearchBy.Items.AddRange(new object[] {
            "Mã yêu cầu",
            "Mã sản phẩm",
            "Tên sản phẩm",
            "IMEI"});
            this.cboSearchBy.Location = new System.Drawing.Point(427, 109);
            this.cboSearchBy.Name = "cboSearchBy";
            this.cboSearchBy.Size = new System.Drawing.Size(193, 24);
            this.cboSearchBy.TabIndex = 15;
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroup.Location = new System.Drawing.Point(117, 58);
            this.cboMainGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(184, 24);
            this.cboMainGroup.TabIndex = 7;
            this.cboMainGroup.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroup_SelectionChangeCommitted);
            // 
            // cboToStoreID
            // 
            this.cboToStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreID.Location = new System.Drawing.Point(427, 32);
            this.cboToStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreID.Name = "cboToStoreID";
            this.cboToStoreID.Size = new System.Drawing.Size(193, 24);
            this.cboToStoreID.TabIndex = 5;
            // 
            // cboFromStoreID
            // 
            this.cboFromStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreID.Location = new System.Drawing.Point(117, 32);
            this.cboFromStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreID.Name = "cboFromStoreID";
            this.cboFromStoreID.Size = new System.Drawing.Size(184, 24);
            this.cboFromStoreID.TabIndex = 4;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(746, 108);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(106, 24);
            this.btnSearch.TabIndex = 16;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // chkExpire
            // 
            this.chkExpire.AutoSize = true;
            this.chkExpire.Location = new System.Drawing.Point(878, 60);
            this.chkExpire.Name = "chkExpire";
            this.chkExpire.Size = new System.Drawing.Size(73, 20);
            this.chkExpire.TabIndex = 10;
            this.chkExpire.Text = "Hết hạn";
            this.chkExpire.UseVisualStyleBackColor = true;
            // 
            // chkDelete
            // 
            this.chkDelete.AutoSize = true;
            this.chkDelete.Location = new System.Drawing.Point(632, 86);
            this.chkDelete.Name = "chkDelete";
            this.chkDelete.Size = new System.Drawing.Size(68, 20);
            this.chkDelete.TabIndex = 13;
            this.chkDelete.Text = "Đã hủy";
            this.chkDelete.UseVisualStyleBackColor = true;
            // 
            // chkUrgent
            // 
            this.chkUrgent.AutoSize = true;
            this.chkUrgent.Location = new System.Drawing.Point(632, 60);
            this.chkUrgent.Name = "chkUrgent";
            this.chkUrgent.Size = new System.Drawing.Size(213, 20);
            this.chkUrgent.TabIndex = 9;
            this.chkUrgent.Text = "Chỉ hiển thị yêu cầu chuyển gấp";
            this.chkUrgent.UseVisualStyleBackColor = true;
            // 
            // txtKeyWords
            // 
            this.txtKeyWords.Location = new System.Drawing.Point(117, 110);
            this.txtKeyWords.Name = "txtKeyWords";
            this.txtKeyWords.Size = new System.Drawing.Size(184, 22);
            this.txtKeyWords.TabIndex = 14;
            // 
            // ucCreateUser
            // 
            this.ucCreateUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucCreateUser.IsOnlyShowRealStaff = false;
            this.ucCreateUser.IsValidate = true;
            this.ucCreateUser.Location = new System.Drawing.Point(746, 33);
            this.ucCreateUser.Margin = new System.Windows.Forms.Padding(4);
            this.ucCreateUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucCreateUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucCreateUser.Name = "ucCreateUser";
            this.ucCreateUser.Size = new System.Drawing.Size(217, 22);
            this.ucCreateUser.TabIndex = 6;
            this.ucCreateUser.UserName = "";
            this.ucCreateUser.WorkingPositionID = 0;
            // 
            // cboSCType
            // 
            this.cboSCType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSCType.FormattingEnabled = true;
            this.cboSCType.Location = new System.Drawing.Point(746, 5);
            this.cboSCType.Name = "cboSCType";
            this.cboSCType.Size = new System.Drawing.Size(217, 24);
            this.cboSCType.TabIndex = 3;
            // 
            // cboSCOrderType
            // 
            this.cboSCOrderType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSCOrderType.FormattingEnabled = true;
            this.cboSCOrderType.Location = new System.Drawing.Point(427, 5);
            this.cboSCOrderType.Name = "cboSCOrderType";
            this.cboSCOrderType.Size = new System.Drawing.Size(193, 24);
            this.cboSCOrderType.TabIndex = 2;
            // 
            // dteToDate
            // 
            this.dteToDate.CustomFormat = "dd/MM/yyyy";
            this.dteToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteToDate.Location = new System.Drawing.Point(215, 7);
            this.dteToDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(86, 22);
            this.dteToDate.TabIndex = 1;
            // 
            // dteFromDate
            // 
            this.dteFromDate.CustomFormat = "dd/MM/yyyy";
            this.dteFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteFromDate.Location = new System.Drawing.Point(117, 7);
            this.dteFromDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(92, 22);
            this.dteFromDate.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(308, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "Kho nhập:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Kho xuất:";
            // 
            // lblStoreChangeStatus
            // 
            this.lblStoreChangeStatus.AutoSize = true;
            this.lblStoreChangeStatus.Location = new System.Drawing.Point(308, 88);
            this.lblStoreChangeStatus.Name = "lblStoreChangeStatus";
            this.lblStoreChangeStatus.Size = new System.Drawing.Size(117, 16);
            this.lblStoreChangeStatus.TabIndex = 14;
            this.lblStoreChangeStatus.Text = "Trạng thái chuyển:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 113);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 16);
            this.label9.TabIndex = 22;
            this.label9.Text = "Từ khóa:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "Trạng thái duyệt:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(629, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 16);
            this.label7.TabIndex = 4;
            this.label7.Text = "Người tạo:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 16);
            this.label8.TabIndex = 10;
            this.label8.Text = "Ngành hàng:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 16);
            this.label3.TabIndex = 19;
            this.label3.Text = "Ngày tạo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(629, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Loại chuyển kho:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(308, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Loại yêu cầu:";
            // 
            // comboBoxMainGroup1
            // 
            this.comboBoxMainGroup1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxMainGroup1.Location = new System.Drawing.Point(660, 92);
            this.comboBoxMainGroup1.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxMainGroup1.MinimumSize = new System.Drawing.Size(24, 24);
            this.comboBoxMainGroup1.Name = "comboBoxMainGroup1";
            this.comboBoxMainGroup1.Size = new System.Drawing.Size(113, 24);
            this.comboBoxMainGroup1.TabIndex = 7;
            // 
            // mnuCopy
            // 
            this.mnuCopy.Image = global::ERP.Inventory.DUI.Properties.Resources._switch;
            this.mnuCopy.Name = "mnuCopy";
            this.mnuCopy.Size = new System.Drawing.Size(344, 22);
            this.mnuCopy.Text = "Tạo yêu cầu chuyển kho ngược lại";
            this.mnuCopy.Click += new System.EventHandler(this.mnuCopy_Click);
            // 
            // frmStoreChangeOrderManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 572);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "frmStoreChangeOrderManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý yêu cầu chuyển kho";
            this.Activated += new System.EventHandler(this.frmStoreChangeOrderManager_Activated);
            this.Load += new System.EventHandler(this.frmStoreChangeOrderManage_Load);
            this.mnuAction.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExport;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox chkExpire;
        private System.Windows.Forms.CheckBox chkDelete;
        private System.Windows.Forms.CheckBox chkUrgent;
        private System.Windows.Forms.TextBox txtKeyWords;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucCreateUser;
        private System.Windows.Forms.ComboBox cboSCType;
        private System.Windows.Forms.ComboBox cboSCOrderType;
        private System.Windows.Forms.DateTimePicker dteToDate;
        private System.Windows.Forms.DateTimePicker dteFromDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblStoreChangeStatus;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroup;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup comboBoxMainGroup1;
        private System.Windows.Forms.ToolStripMenuItem itemPrintOrder;
        private System.Windows.Forms.ComboBox cboSearchBy;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStoreChangeStatusIDList;
        private System.Windows.Forms.ComboBox cboIsReview;
        private System.Windows.Forms.ToolStripMenuItem itemPrintOrderByProduct;
        private System.Windows.Forms.ToolStripMenuItem itemPrintOrderByStore;
        private System.Windows.Forms.ToolStripMenuItem itemPrintInputOrder;
        private System.Windows.Forms.ToolStripMenuItem itemPrintOrderRecords;
        private System.Windows.Forms.ToolStripMenuItem itemPrintInputOrderDetail;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintReport;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroupIDList;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintStoreChangeOrderDetail;
        private System.Windows.Forms.Button btnDelete;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl grdProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewProduct;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintReport1;
        private System.Windows.Forms.ToolStripMenuItem mnuStoreChangeOrderExport;
        private System.Windows.Forms.ToolStripMenuItem mnuCopy;
    }
}