﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
namespace ERP.Inventory.DUI.StoreChange
{
    /// <summary>
    /// Created by  :   Nguyễn Văn Tài
    /// Date        :   06/12/2017
    /// Desc        :   Form nhập chuỗi thông tin
    /// </summary>
    public partial class frmRealInputCheck : Form
    {
        #region Variable
        private string strContentString = string.Empty;
        private bool bolIsAccept = false;
        private int intMaxLengthString = 100;
        private bool bolIsAllowEmpty = false;

        #endregion
        #region Property
        public string ContentString
        {
            get { return strContentString; }
            set { strContentString = value; }
        }
        public bool IsAccept
        {
            get { return bolIsAccept; }
        }
        public int MaxLengthString
        {
            set { intMaxLengthString = value; }
        }

        public bool IsAllowEmpty
        {
            set { bolIsAllowEmpty = value; }
        }

        public List<PLC.Input.WSInputVoucher.InputVoucherDetail> InputVoucherDetailList { get; set; }
        #endregion
        #region Constructor
        public frmRealInputCheck()
        {
            InitializeComponent();
        }
        #endregion
        #region Event
        private void frmInputString_Load(object sender, EventArgs e)
        {
            this.txtContent.MaxLength = intMaxLengthString;
            this.txtContent.Text = strContentString;
            this.txtContent.Focus();
            this.txtContent.SelectAll();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtContent.Text.Trim()) && !bolIsAllowEmpty)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập nội dung");
                txtContent.Focus();
                return;
            }
            this.strContentString = txtContent.Text.Trim();
            this.bolIsAccept = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.bolIsAccept = false;
            this.Close();
        }
        #endregion

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                ERP.Inventory.DUI.Input.frmRealInputConfirm frm = new ERP.Inventory.DUI.Input.frmRealInputConfirm(InputVoucherDetailList);
                frm.InputVoucherDetailList = InputVoucherDetailList;
                if (frm.ShowDialog() == DialogResult.OK)
                {
                }
            }
            catch { }
        }
    }
}