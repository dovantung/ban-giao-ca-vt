﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.SplitProduct
{
    public partial class frmSplitProduct : Form
    {
        private string strSplitProduct_StoreIDList = "";
        private string strMainGroupID = string.Empty;
        private int intCustomerID = -1;
        private ERP.MasterData.PLC.MD.WSStore.Store objStore = null;
        private ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_IMEI = null;
        private ERP.MasterData.PLC.MD.WSInputType.InputType objInputType_IMEI = null;
        private ERP.MasterData.PLC.MD.WSInputType.InputType objInputType = null;
        private PLC.SplitProduct.PLCSplitProduct objPLCSplitProduct = new PLC.SplitProduct.PLCSplitProduct();
        private PLC.SplitProduct.WSSplitProduct.SplitProduct objSplitProduct = null;
        private PLC.WSProductInStock.ProductInStock objProductInStock = null;
        public frmSplitProduct()
        {
            InitializeComponent();
            this.Deactivate += new EventHandler(frmSplitProduct_Deactivate);
            this.Activated += new EventHandler(frmSplitProduct_Activated);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvProductDetail);
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        void frmSplitProduct_Activated(object sender, EventArgs e)
        {
            txtIMEI.Focus();
        }

        void frmSplitProduct_Deactivate(object sender, EventArgs e)
        {
            txtIMEI_Leave(null, null);
        }

        private void frmSplitProduct_Load(object sender, EventArgs e)
        {
            if (!LoadTextBox())
            {
                txtIMEI.Enabled = false;
                btnDeleteIMEI.Enabled = false;
                btnSearch.Enabled = false;
                txtBarcode.Enabled = false;
                btnUpdate.Enabled = false;
                return;
            }
            grdProductDetail.DataSource = CreatedDataProductDetail();
            this.ActiveControl = txtIMEI;
        }

        private bool ValidateData()
        {
            if (txtIMEI.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this,"Vui lòng nhập IMEI cần tách linh kiện!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                txtIMEI.Focus();
                return false;
            }
            if (txtProductNameIMEI.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập IMEI cần tách linh kiện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtIMEI.Focus();
                return false;
            }

            if (txtContent.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập nội dung tách linh kiện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtContent.Focus();
                return false;
            }

            if (grvProductDetail.RowCount <= 0 || grdProductDetail.DataSource == null)
            {
                MessageBox.Show(this, "Vui lòng thêm linh kiện vào danh sách!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBarcode.Focus();
                return false;
            }

            DataTable dtb = grdProductDetail.DataSource as DataTable;
            dtb.AcceptChanges();
            decimal decSalePrice = Convert.ToDecimal(txtSalePrice.Text);
            decimal decTotalAmount = Convert.ToDecimal(dtb.Compute("Sum(AMOUNT)", string.Empty));
            for (int i = 0; i < grvProductDetail.RowCount; i++)
            {
                int intQuantity = Convert.ToInt32(grvProductDetail.GetRowCellValue(i, "QUANTITY"));
                if (intQuantity <= 0)
                {
                    MessageBox.Show(this, "Số lượng tách phải lớn hơn 0!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    grvProductDetail.FocusedRowHandle = i;
                    grvProductDetail.FocusedColumn = grvProductDetail.Columns["QUANTITY"];
                    return false;
                }
            }
            if (decTotalAmount > decSalePrice)
            {
                MessageBox.Show(this, "Tổng tiền linh kiện không được vượt quá giá IMEI tách!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                grdProductDetail.Focus();
                return false;
            }
            return true;
        }

        private bool UpdateData()
        {
            try
            {
                objSplitProduct = new PLC.SplitProduct.WSSplitProduct.SplitProduct();
                objSplitProduct.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objSplitProduct.IMEI = txtIMEI.Text.Trim();
                objSplitProduct.Price = Convert.ToDecimal(txtSalePrice.Text);
                objSplitProduct.PriceRemain = Convert.ToDecimal(txtInputPriceIMEI.Text);
                objSplitProduct.ProductID = txtProductNameIMEI.Text.Split('-')[0].Trim();
                objSplitProduct.SplitContent = txtContent.Text;
                objSplitProduct.InputVoucher = new PLC.SplitProduct.WSSplitProduct.InputVoucher();
                if (!CreateOuputInputVoucherIMEI())
                    return false;
                if (!CreateInputVoucher())
                    return false;
                objPLCSplitProduct.InsertData(objSplitProduct);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                MessageBox.Show(this, "Tách linh kiện thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi cập nhật tách linh kiện", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi cập nhật tách linh kiện \n Bạn vui lòng kiểm tra lại thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        #region Tạo phiếu xuất chuyển kho và nhập chuyển kho
        /// <summary>
        /// Tạo phiếu Chuyển kho - Phiếu xuất - Phiếu nhập
        /// IsCreateInOutVoucher = true : Tạo Phiếu thu - Phiếu chi
        /// </summary>
        /// <returns></returns>
        public bool CreateOuputInputVoucherIMEI()
        {
           
            String strStorePhone = objStore.StorePhoneNumber;
            String strStoreTaxID = objStore.TaxCode;
            String strCustomerName = objStore.CompanyTITLE;
            String strTaxAddress = objStore.TaxAddress;
            String strCustomerAddress = objStore.StoreAddress;
            try
            {
                #region ****** Thông tin phiếu xuất IMEI ****** //
                PLC.SplitProduct.WSSplitProduct.OutputVoucher objOutputVoucher = new PLC.SplitProduct.WSSplitProduct.OutputVoucher();
                objOutputVoucher.OrderID = string.Empty;
                objOutputVoucher.CurrencyUnitID = MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyUnitDefaultID();
                objOutputVoucher.CurrencyExchange = MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyExchange(objOutputVoucher.CurrencyUnitID);
                objOutputVoucher.InvoiceID = string.Empty;
                objOutputVoucher.InvoiceSymbol = string.Empty;
                objOutputVoucher.InvoiceDate = DateTime.Now;
                objOutputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objOutputVoucher.OutputStoreID = objStore.StoreID;
                objOutputVoucher.OutputDate = DateTime.Now;
                //objOutputVoucher.PayableTypeID = 
                objOutputVoucher.PayableDate = DateTime.Now;
                //objOutputVoucher.Discount = 0;
                //objOutputVoucher.DiscountReasonID = 0;
                objOutputVoucher.TotalAmountBFT = Convert.ToDecimal(txtSalePrice.Text); 
                //objOutputVoucher.TotalVAT = decTotalVAT_Out;
                objOutputVoucher.TotalAmount = Convert.ToDecimal(txtSalePrice.Text);
                objOutputVoucher.Denominator = "";
                objOutputVoucher.StaffUser = SystemConfig.objSessionUser.UserName;
                objOutputVoucher.CustomerID = intCustomerID;
                objOutputVoucher.CustomerName = strCustomerName.Trim();
                objOutputVoucher.CustomerAddress = strCustomerAddress.Trim();
                objOutputVoucher.CustomerPhone = strStorePhone.Trim();
                objOutputVoucher.CustomerTaxID = strStoreTaxID.Trim();
                objOutputVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                //Chi tiết phiếu xuất
                List<PLC.SplitProduct.WSSplitProduct.OutputVoucherDetail> objOutputVoucherDetailList = new List<PLC.SplitProduct.WSSplitProduct.OutputVoucherDetail>();
                PLC.SplitProduct.WSSplitProduct.OutputVoucherDetail objOutputVoucherDetail = new PLC.SplitProduct.WSSplitProduct.OutputVoucherDetail();
                objOutputVoucherDetail.OutputStoreID = objStore.StoreID;
                objOutputVoucherDetail.OutputTypeID = objOutputType_IMEI.OutputTypeID;
                objOutputVoucherDetail.ProductID = objProductInStock.ProductID;
                objOutputVoucherDetail.Quantity = 1;
                objOutputVoucherDetail.FirstCustomerID = objProductInStock.FirstCustomerID;
                objOutputVoucherDetail.FirstInputVoucherID = objProductInStock.FirstInputVoucherID;
                objOutputVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objOutputVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                objOutputVoucherDetail.CostPrice = objProductInStock.CostPrice;
                objOutputVoucherDetail.SalePrice = Convert.ToDecimal(txtSalePrice.Text);
                objOutputVoucherDetail.BFAdjustSalePrice = Convert.ToDecimal(txtSalePrice.Text);
                objOutputVoucherDetail.OriginalSalePrice = Convert.ToDecimal(txtSalePrice.Text);
                objOutputVoucherDetail.RetailPrice = Convert.ToDecimal(txtSalePrice.Text);
                objOutputVoucherDetail.IMEI = txtIMEI.Text;
                objOutputVoucherDetail.IsNew = objProductInStock.IsNew;
                objOutputVoucherDetailList.Add(objOutputVoucherDetail);
                objOutputVoucher.OutputVoucherDetailList = objOutputVoucherDetailList.ToArray(); 
                #endregion

                #region ****** Thông tin phiếu nhập IMEI ****** 
                PLC.SplitProduct.WSSplitProduct.InputVoucher objInputVoucher = new PLC.SplitProduct.WSSplitProduct.InputVoucher();
                objInputVoucher.InVoiceID = string.Empty;
                objInputVoucher.OrderID = string.Empty;
                objInputVoucher.InVoiceSymbol = "";
                objInputVoucher.InVoiceDate = DateTime.Now;
                objInputVoucher.InputDate = DateTime.Now;
                objInputVoucher.InputTypeID = objInputType_IMEI.InputTypeID;
                objInputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objInputVoucher.InputDate = DateTime.Now;
                objInputVoucher.InputStoreID = objStore.StoreID;
                objInputVoucher.CustomerID = intCustomerID;
                objInputVoucher.CustomerName = strCustomerName.Trim();
                objInputVoucher.CustomerAddress = strCustomerAddress.Trim();
                objInputVoucher.CustomerPhone = strStorePhone.Trim();
                objInputVoucher.CustomerTaxID = strStoreTaxID.Trim();
                objInputVoucher.Discount = 0;
                //objInputVoucher.DiscountReasonID = Convert.ToInt32(objStoreChangeType.DiscountReasonID);
                //objInputVoucher.PayableTypeID = Convert.ToInt32(objStoreChangeType.PayableTypeID);
                objInputVoucher.PayableDate = DateTime.Now;
                objInputVoucher.TotalAmountBFT = Convert.ToDecimal(txtInputPriceIMEI.Text); 
                //objInputVoucher.Denominator = "";
                //objInputVoucher.TotalVAT = decTotalVAT_In;
                objInputVoucher.TotalAmount = Convert.ToDecimal(txtInputPriceIMEI.Text);
                objInputVoucher.StaffUser = SystemConfig.objSessionUser.UserName;
                objInputVoucher.IsNew = objProductInStock.IsNew;
                objInputVoucher.CurrencyUnitID = MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyUnitDefaultID();
                objInputVoucher.CurrencyExchange = MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyExchange(objInputVoucher.CurrencyUnitID);
                objInputVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objInputVoucher.IsCheckRealInput = objInputType_IMEI.IsAutoCheckRealInput;
                objInputVoucher.CheckRealInputNote = string.Empty;
                //Chi tiết phiếu nhập
                List<PLC.SplitProduct.WSSplitProduct.InputVoucherDetail> objInputVoucherDetailList = new List<PLC.SplitProduct.WSSplitProduct.InputVoucherDetail>();
                PLC.SplitProduct.WSSplitProduct.InputVoucherDetail objInputVoucherDetail = new PLC.SplitProduct.WSSplitProduct.InputVoucherDetail();
                objInputVoucherDetail.InputStoreID = objStore.StoreID;
                objInputVoucherDetail.InputPrice = Convert.ToDecimal(txtInputPriceIMEI.Text);
                objInputVoucherDetail.CostPrice = objInputVoucherDetail.InputPrice;
                objInputVoucherDetail.ORIGINALInputPrice = Convert.ToDecimal(txtInputPriceIMEI.Text);
                objInputVoucherDetail.FirstPrice = objProductInStock.FirstPrice;
                objInputVoucherDetail.FirstInputTypeID = objProductInStock.FirstInputTypeID;
                objInputVoucherDetail.ProductID = objProductInStock.ProductID;
                objInputVoucherDetail.IsCheckRealInput = objInputType_IMEI.IsAutoCheckRealInput;
                objInputVoucherDetail.Quantity = 1;
                objInputVoucherDetail.ENDWarrantyDate = objProductInStock.EndWarrantyDate;
                objInputVoucherDetail.ProductIONDate = objProductInStock.InputVoucherDate;
                objInputVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objInputVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                objInputVoucherDetail.IsNew = objProductInStock.IsNew;
                objInputVoucherDetail.FirstCustomerID = objProductInStock.FirstCustomerID;
                objInputVoucherDetail.FirstInputVoucherID = objProductInStock.FirstInputVoucherID;
                //Chi tiet IMEI
                List<PLC.SplitProduct.WSSplitProduct.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = new List<PLC.SplitProduct.WSSplitProduct.InputVoucherDetailIMEI>();
                PLC.SplitProduct.WSSplitProduct.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.SplitProduct.WSSplitProduct.InputVoucherDetailIMEI();
                objInputVoucherDetailIMEI.CostPrice = objInputVoucherDetail.CostPrice;
                objInputVoucherDetailIMEI.InputDate = objInputVoucher.InputDate;
                objInputVoucherDetailIMEI.FirstPrice = objProductInStock.FirstPrice;
                objInputVoucherDetailIMEI.InputUser= SystemConfig.objSessionUser.UserName;
                objInputVoucherDetailIMEI.IMEI = objSplitProduct.IMEI;
                objInputVoucherDetailIMEI.InputStoreID = objStore.StoreID;
                objInputVoucherDetailIMEI.CreatedStoreID = objInputVoucherDetail.CreatedStoreID;
                objInputVoucherDetailIMEI.CreatedUser = objInputVoucherDetail.CreatedUser;
                objInputVoucherDetailIMEI.InputUser = objSplitProduct.CreatedUser;
                objInputVoucherDetailIMEI.InputDate = DateTime.Now;
                objInputVoucherDetailIMEI.IsHasWarranty = objProductInStock.IsHasWarranty;
                objInputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                objInputVoucherDetail.InputVoucherDetailIMEIList = objInputVoucherDetailIMEIList.ToArray();
                objInputVoucherDetailList.Add(objInputVoucherDetail);
                objInputVoucher.InputVoucherDetailList = objInputVoucherDetailList.ToArray(); 
                #endregion
                
                objSplitProduct.OutputVoucherIMEI = objOutputVoucher;
                objSplitProduct.InputVoucherIMEI = objInputVoucher;
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi lấy thông tin phiếu xuất - nhập IMEI", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi lấy thông tin phiếu xuất - nhập IMEI \n Bạn vui lòng kiểm tra lại thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private bool CreateInputVoucher()
        {
            try
            {
                String strStorePhone = objStore.StorePhoneNumber;
                String strStoreTaxID = objStore.TaxCode;
                String strCustomerName = objStore.CompanyTITLE;
                String strTaxAddress = objStore.TaxAddress;
                String strCustomerAddress = objStore.StoreAddress;

                PLC.SplitProduct.WSSplitProduct.InputVoucher objInputVoucher = new PLC.SplitProduct.WSSplitProduct.InputVoucher();
                objInputVoucher.InVoiceID = string.Empty;
                objInputVoucher.OrderID = string.Empty;
                objInputVoucher.InVoiceSymbol = "";
                objInputVoucher.InVoiceDate = DateTime.Now;
                objInputVoucher.InputTypeID = objInputType.InputTypeID;
                objInputVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objInputVoucher.StaffUser = SystemConfig.objSessionUser.UserName;
                objInputVoucher.InputDate = DateTime.Now;
                objInputVoucher.InputStoreID = objStore.StoreID;
                objInputVoucher.CustomerID = intCustomerID;
                objInputVoucher.CustomerName = strCustomerName.Trim();
                objInputVoucher.CustomerAddress = strCustomerAddress.Trim();
                objInputVoucher.CustomerPhone = strStorePhone.Trim();
                objInputVoucher.CustomerTaxID = strStoreTaxID.Trim();
                objInputVoucher.Discount = 0;
                //objInputVoucher.DiscountReasonID = Convert.ToInt32(objStoreChangeType.DiscountReasonID);
                //objInputVoucher.PayableTypeID = Convert.ToInt32(objStoreChangeType.PayableTypeID);
                objInputVoucher.PayableDate = DateTime.Now;
                objInputVoucher.TotalAmountBFT = Convert.ToDecimal(txtTotalAmount.Text);;
                //objInputVoucher.Denominator = "";
                //objInputVoucher.TotalVAT = decTotalVAT_In;
                objInputVoucher.TotalAmount = Convert.ToDecimal(txtTotalAmount.Text);
                objInputVoucher.IsNew = true;
                objInputVoucher.CurrencyUnitID = MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyUnitDefaultID();
                objInputVoucher.CurrencyExchange = MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyExchange(objInputVoucher.CurrencyUnitID);
                objInputVoucher.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objInputVoucher.IsCheckRealInput = objInputType.IsAutoCheckRealInput;
                objInputVoucher.CheckRealInputNote = objInputType.IsAutoCheckRealInput == true ? "Tự động thực nhập" : "";
                //Chi tiết phiếu nhập
                List<PLC.SplitProduct.WSSplitProduct.InputVoucherDetail> objInputVoucherDetailList = new List<PLC.SplitProduct.WSSplitProduct.InputVoucherDetail>();
                DataTable dtbDetail=grdProductDetail.DataSource as DataTable;
                foreach (DataRow item in dtbDetail.Rows)
	            {
		            PLC.SplitProduct.WSSplitProduct.InputVoucherDetail objInputVoucherDetail = new PLC.SplitProduct.WSSplitProduct.InputVoucherDetail();
                    objInputVoucherDetail.InputStoreID = objStore.StoreID;
                    objInputVoucherDetail.CostPrice = Convert.ToDecimal(item["PRICE"]);
                    objInputVoucherDetail.ORIGINALInputPrice = Convert.ToDecimal(item["PRICE"]);
                    objInputVoucherDetail.FirstPrice = Convert.ToDecimal(item["PRICE"]);
                    objInputVoucherDetail.FirstInputTypeID=objInputType.InputTypeID;
                    objInputVoucherDetail.ProductID = item["PRODUCTID"].ToString();
                    objInputVoucherDetail.Quantity = Convert.ToInt32(item["QUANTITY"]);
                    objInputVoucherDetail.InputPrice = Convert.ToDecimal(item["PRICE"]);
                    objInputVoucherDetail.ENDWarrantyDate = DateTime.Now;
                    objInputVoucherDetail.ProductIONDate = DateTime.Now;
                    objInputVoucherDetail.FirstInputDate = DateTime.Now;
                    objInputVoucherDetail.FirstCustomerID = intCustomerID;
                    objInputVoucherDetail.IsCheckRealInput = objInputType.IsAutoCheckRealInput;
                    objInputVoucherDetail.IsNew = true;
                    objInputVoucherDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    objInputVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objInputVoucherDetailList.Add(objInputVoucherDetail);
                }
                objInputVoucher.InputVoucherDetailList = objInputVoucherDetailList.ToArray();
                objSplitProduct.InputVoucher = objInputVoucher;
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi lấy thông tin phiếu xuất - nhập IMEI", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi lấy thông tin phiếu xuất - nhập IMEI \n Bạn vui lòng kiểm tra lại thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        #endregion

        private DataTable CreatedDataProductDetail()
        {
            DataTable dtb = new DataTable("PRODUCTDETAIL");
            dtb.Columns.Add("PRODUCTID", typeof(String));
            dtb.Columns.Add("PRODUCTNAME", typeof(String));
            dtb.Columns.Add("QUANTITY", typeof(Decimal));
            DataColumn colPrice = new DataColumn("PRICE", typeof(Decimal));
            colPrice.DefaultValue = 0;
            dtb.Columns.Add(colPrice);
            DataColumn colAmount = new DataColumn("AMOUNT", typeof(Decimal));
            colAmount.DefaultValue = 0;
            colAmount.Expression = "QUANTITY * PRICE";
            dtb.Columns.Add(colAmount);
            return dtb;
        }



        private bool LoadTextBox()
        {
            try
            {
                strSplitProduct_StoreIDList = Library.AppCore.AppConfig.GetStringConfigValue("PM_SPLITPRODUCT_STOREIDLIST");
                if (strSplitProduct_StoreIDList.Trim() == string.Empty)
                {
                    MessageBox.Show(this, "Chưa khai báo cấu hình danh sách kho được tách linh kiện [PM_SPLITPRODUCT_STOREIDLIST]!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else
                {
                    ComboBox cbo = new ComboBox();
                    Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter=new Library.AppCore.DataSource.FilterObject.StoreFilter();
                    objStoreFilter.CompanyID=SystemConfig.DefaultCompany.CompanyID;
                    objStoreFilter.IsCheckPermission=true;
                    objStoreFilter.OtherCondition = " ISCANINPUT=1 AND ISINPUTSTORE=1 ";
                    objStoreFilter.StorePermissionType=Library.AppCore.Constant.EnumType.StorePermissionType.OUTPUT;
                    Library.AppCore.LoadControls.SetDataSource.SetStore(this, cbo, objStoreFilter);
                    DataTable dtbStore = cbo.DataSource as DataTable;
                    dtbStore.Rows.RemoveAt(0);
                    DataTable dtbDataStore = null;
                    if (dtbStore != null && dtbStore.Rows.Count > 0)
                    {
                        dtbDataStore = Library.AppCore.DataTableClass.Select(dtbStore, string.Format("STOREID IN({0})", strSplitProduct_StoreIDList));
                    }
                    //string[] arrStoreIDList = strSplitProduct_StoreIDList.Split(',');
                    if (dtbDataStore!=null&&dtbDataStore.Rows.Count>0)
                    {
                        cboStore.InitControl(false, dtbDataStore,"STOREID","STORENAME","--Chọn kho--");
                    }
                    else
                    {
                        MessageBox.Show(this, "Bạn không có kho nào được phép tách linh kiện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                int intOutputTypeID_IMEI = Library.AppCore.AppConfig.GetIntConfigValue("PM_SPLITPRODUCT_OUTPUTTYPEID_IMEI");
                if (intOutputTypeID_IMEI <= 0)
                {
                    MessageBox.Show(this, "Chưa khai báo cấu hình hình thức xuất IMEI [PM_SPLITPRODUCT_OUTPUTTYPEID_IMEI]!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else
                {
                    new ERP.MasterData.PLC.MD.PLCOutputType().LoadInfo(ref objOutputType_IMEI, intOutputTypeID_IMEI);
                    if (objOutputType_IMEI != null)
                    {
                        txtOutputTypeIMEI.Text = objOutputType_IMEI.OutputTypeID + " - " + objOutputType_IMEI.OutputTypeName;
                        if (!objOutputType_IMEI.IsActive || objOutputType_IMEI.IsDeleted)
                        {
                            MessageBox.Show(this, "Hình thức xuất IMEI không được kích hoạt hoặc đã xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }
                }
                int intInputTypeID = Library.AppCore.AppConfig.GetIntConfigValue("PM_SPLITPRODUCT_INPUTTYPEID");
                if (intInputTypeID <= 0)
                {
                    MessageBox.Show(this, "Chưa khai báo cấu hình hình thức nhập linh kiện [PM_SPLITPRODUCT_INPUTTYPEID]!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else
                {
                    objInputType = new ERP.MasterData.PLC.MD.PLCInputType().LoadInfo(intInputTypeID);
                    if (objInputType != null)
                    {
                        txtInputTypeID.Text = objInputType.InputTypeID + " - " + objInputType.InputTypeName;
                        if (!objInputType.IsActive || objInputType.IsDeleted)
                        {
                            MessageBox.Show(this, "Hình thức nhập linh kiện không được kích hoạt hoặc đã xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }
                }

                int intInputTypeID_IMEI = Library.AppCore.AppConfig.GetIntConfigValue("PM_SPLITPRODUCT_INPUTTYPEID_IMEI");
                if (intInputTypeID_IMEI <= 0)
                {
                    MessageBox.Show(this, "Chưa khai báo cấu hình hình thức nhập lại IMEI [PM_SPLITPRODUCT_INPUTTYPEID_IMEI]!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else
                {
                    objInputType_IMEI = new ERP.MasterData.PLC.MD.PLCInputType().LoadInfo(intInputTypeID_IMEI);
                    if (objInputType_IMEI != null)
                    {
                        txtInputTypeIMEI.Text = objInputType_IMEI.InputTypeID + " - " + objInputType_IMEI.InputTypeName;
                        if (!objInputType_IMEI.IsActive || objInputType_IMEI.IsDeleted)
                        {
                            MessageBox.Show(this, "Hình thức nhập lại IMEI không được kích hoạt hoặc đã xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }
                }

                strMainGroupID = Library.AppCore.AppConfig.GetStringConfigValue("PM_SPLITPRODUCT_MAINGROUPID");
                if (strMainGroupID.Trim()==string.Empty)
                {
                    MessageBox.Show(this, "Chưa khai báo cấu hình mã ngành hàng linh kiện [PM_SPLITPRODUCT_MAINGROUPID]!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                intCustomerID = Library.AppCore.AppConfig.GetIntConfigValue("PM_SPLITPRODUCT_CUSTOMERID");
                if (intCustomerID<=0)
                {
                    MessageBox.Show(this, "Chưa khai báo cấu hình mã ngành hàng linh kiện [PM_SPLITPRODUCT_CUSTOMERID]!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi thông tin cấu hình ứng dụng");
                SystemErrorWS.Insert("Lỗi thông tin cấu hình ứng dụng", objExec, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Lấy giá xuất
        /// </summary>
        /// <param name="objDProductInStock">Sản phẩm tồn kho.</param>
        private decimal GetOutputPrice(PLC.WSProductInStock.ProductInStock objProductInStock)
        {
            //Cách lấy giá. 1: Giá ban, 2: Giá vốn, 3: Giá bằng 0; 4: Giá mua; 5: Giá mua - Bảo vệ giá
            decimal decOutputPrice = 0;
            if (objOutputType_IMEI == null)
            {
                MessageBox.Show(this, "Hình thức xuất không tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return decOutputPrice;
            }
            switch (objOutputType_IMEI.GetPriceType)
            {
                case 1:
                    decOutputPrice = objProductInStock.Price;
                    break;
                case 2:
                    decOutputPrice = objProductInStock.CostPrice;
                    break;
                case 3:
                    decOutputPrice = 0;
                    break;
                case 4:
                    decOutputPrice = objProductInStock.FirstPrice;
                    break;
            }
            return decOutputPrice;
        }

        /// <summary>
        /// Lấy giá nhập
        /// </summary>
        /// <param name="objDProductInStock">Sản phẩm tồn kho.</param>
        private decimal GetInputPrice(PLC.WSProductInStock.ProductInStock objProductInStock)
        {
            if (objInputType_IMEI == null)
            {
                MessageBox.Show(this, "Hình thức nhập không tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
            decimal decInputPrice = 0;
            //Cách lấy giá nhập; 1: Gia bán;2: Gia vốn; 3: Giá bằng 0
            switch (objInputType_IMEI.GetPriceType)
            {
                case 1:
                    decInputPrice = objProductInStock.Price;
                    break;
                case 2:
                    decInputPrice = objProductInStock.CostPrice;
                    break;
                case 3:
                    decInputPrice = 0;
                    break;
            }
            return decInputPrice;
        }


        /// <summary>
        /// Nhập IMEI
        /// </summary>
        /// <param name="strIMEI">IMEI.</param>
        /// <returns></returns>
        private bool AddIMEI(string strIMEI)
        {
            if (objStore == null)
            {
                txtIMEI.Text = string.Empty;
                MessageBox.Show(this, "Vui lòng chọn kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboStore.Focus();
                return false;
            }
            objProductInStock = new ERP.Inventory.PLC.PLCProductInStock().LoadInfo(strIMEI,cboStore.ColumnID,objOutputType_IMEI.OutputTypeID,"",false);
            if (objProductInStock == null)
            {
                txtIMEI.Text = string.Empty;
                MessageBox.Show(this, "Số IMEI không tồn tại trong kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (objProductInStock.IMEI.Trim() != strIMEI.Trim())
            {
                txtIMEI.Text = string.Empty;
                MessageBox.Show(this, "Số IMEI không tồn tại trong kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (!objProductInStock.IsCheckRealInput)
            {
                txtIMEI.Text = string.Empty;
                MessageBox.Show(this, "Số IMEI này chưa được xác nhận nhập hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (objProductInStock.IsOrder)
            {
                txtIMEI.Text = string.Empty;
                MessageBox.Show(this, "Số IMEI đã được đặt hàng ở đơn hàng " + objProductInStock.OrderID, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            txtProductNameIMEI.Text = objProductInStock.ProductID +" - "+ objProductInStock.ProductName;
            txtSalePrice.Text = GetOutputPrice(objProductInStock).ToString("###,###,##0");
            return true;
        }

        private bool AddProduct(string strBarcode)
        {
            if (txtIMEI.Text.Trim()==string.Empty||txtProductNameIMEI.Text.Trim()==string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập IMEI cần tách trước khi bắn barcode!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtIMEI.Focus();
                return false;
            }
            //Lấy thông tin sản phẩm tồn kho
            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strBarcode);
            if (objProduct == null)
            {
                MessageBox.Show(this, "Mã sản phẩm này không tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
           
            if (objProduct.IsRequestIMEI)
            {
                MessageBox.Show(this, "Bạn phải nhập vào sản phẩm không yêu cầu IMEI!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(objProduct.MainGroupID, Library.AppCore.Constant.EnumType.MainGroupPermissionType.INPUT))
            {
                MessageBox.Show(this, "Bạn không có quyền nhập trên ngành hàng " + objProduct.MainGroupName + "!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(objStore.StoreID, objProduct.MainGroupID,
                 Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.INPUT))
            {
                MessageBox.Show(this, "Kho này không được phép nhập trên ngành hàng " + objProduct.MainGroupName + "!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (objProduct.IsService)
            {
                MessageBox.Show(this, "Bạn không được phép nhập hàng mặt hàng dịch vụ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (grdProductDetail.DataSource == null)
                grdProductDetail.DataSource = CreatedDataProductDetail();
            DataTable dtbProductDetail=grdProductDetail.DataSource as DataTable;
            DataRow[] drExist=dtbProductDetail.Select(string.Format("PRODUCTID='{0}'",objProduct.ProductID));
            if (drExist.Length > 0)
            {
                drExist[0]["QUANTITY"] = Convert.ToInt32(drExist[0]["QUANTITY"]) + 1;
            }
            else
            {
                DataRow drow = dtbProductDetail.NewRow();
                drow["PRODUCTID"] = objProduct.ProductID;
                drow["PRODUCTNAME"] = objProduct.ProductName;
                drow["QUANTITY"] = 1;
                drow["PRICE"] = 0;
                dtbProductDetail.Rows.Add(drow);
            }
            grdProductDetail.RefreshDataSource();
            Total();
            return true;
        }

        private void Total()
        {
            if (grvProductDetail.RowCount <= 0)
            {
                txtTotalAmount.Text = "0";
                txtTotalRow.Text = "0";
                txtInputPriceIMEI.Text = txtSalePrice.Text;
            }
            else
            {
                DataTable dtb = grdProductDetail.DataSource as DataTable;
                dtb.AcceptChanges();
                txtTotalRow.Text = Convert.ToDecimal(dtb.Compute("Sum(QUANTITY)", string.Empty)).ToString("###,###,##0");
                txtTotalAmount.Text = Convert.ToDecimal(dtb.Compute("Sum(AMOUNT)", string.Empty)).ToString("###,###,##0");
                txtInputPriceIMEI.Text = (Convert.ToDecimal(txtSalePrice.Text) - Convert.ToDecimal(txtTotalAmount.Text)).ToString("###,###,##0");
            }
        }

        private void btnDeleteIMEI_Click(object sender, EventArgs e)
        {
            if (objProductInStock != null && grvProductDetail.RowCount > 0)
            {
                if (MessageBox.Show(this, "Bạn xóa IMEI danh sách linh kiện sẽ được làm mới! Bạn chắc chắn muốn xóa IMEI không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    grdProductDetail.DataSource = CreatedDataProductDetail();
                    grdProductDetail.RefreshDataSource();
                    objProductInStock = null;
                    
                }
                else
                    return;
            }
            txtIMEI.Text = string.Empty;
            txtIMEI.ReadOnly = false;
            txtProductNameIMEI.Text = string.Empty;
            txtSalePrice.Text = "0";
        }

        private void txtIMEI_Leave(object sender, EventArgs e)
        {
            string strBarcode = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtIMEI.Text.Trim());
            if (strBarcode == string.Empty || txtIMEI.ReadOnly)
            {
                return;
            }

            if (!AddIMEI(strBarcode))
            {
                txtSalePrice.Text = "0";
                txtProductNameIMEI.Text = string.Empty;
                objProductInStock = null;
            }
            else
                txtIMEI.ReadOnly = true;
            Total();
        }

        private void txtIMEI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtIMEI_Leave(null, null);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ERP.MasterData.DUI.MasterData_Globals.ProductSearchParentForm_Text = this.Text;
            ERP.MasterData.DUI.Search.frmProductSearch frm = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
            frm.IsMultiSelect = false;
            frm.IsServiceType = Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE;
            frm.MainGroupPermissionType = Library.AppCore.Constant.EnumType.MainGroupPermissionType.OUTPUT;
            frm.MainGroupIDList = strMainGroupID;
            frm.ShowDialog();
            if (frm.Product != null)
            {
                txtBarcode.Text = frm.Product.ProductID;
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strBarcode = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtBarcode.Text.Trim());
            if (strBarcode.Length > 0 && e.KeyChar == 13)
            {
                if (txtProductNameIMEI.Text.Trim() == string.Empty)
                {
                    MessageBox.Show(this,"Vui lòng nhập IMEI cần tách trước khi nhập linh kiện!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                    txtIMEI.Focus();
                    return;
                }
                if (AddProduct(strBarcode))
                    txtBarcode.Text = string.Empty;
            }
        }

        private void grvProductDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (grvProductDetail.FocusedRowHandle < 0)
                return;
            (grdProductDetail.DataSource as DataTable).AcceptChanges();
            grdProductDetail.RefreshDataSource();
            if (grvProductDetail.FocusedColumn.FieldName == "QUANTITY")
            {
                Decimal decTotalAmount = Convert.ToDecimal((grdProductDetail.DataSource as DataTable).Compute("Sum(AMOUNT)", string.Empty));
                if (Convert.ToDecimal(txtSalePrice.Text) < decTotalAmount)
                {
                    grvProductDetail.GetDataRow(grvProductDetail.FocusedRowHandle)["QUANTITY"] = 0;
                    (grdProductDetail.DataSource as DataTable).AcceptChanges();
                    MessageBox.Show(this, "Tổng tiền linh kiện không được lớn hơn giá IMEI!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    grvProductDetail.FocusedColumn = grvProductDetail.Columns["QUANTITY"];
                }
            }
            else
                if (grvProductDetail.FocusedColumn.FieldName == "PRICE")
                {
                    object objTotalAmount = (grdProductDetail.DataSource as DataTable).Compute("Sum(AMOUNT)", string.Empty);
                    Decimal decTotalAmount = 0;
                    if (!Convert.IsDBNull(objTotalAmount))
                        decTotalAmount = Convert.ToDecimal(objTotalAmount);
                    if (Convert.ToDecimal(txtSalePrice.Text) < decTotalAmount)
                    {
                        grvProductDetail.GetDataRow(grvProductDetail.FocusedRowHandle)["PRICE"] = 0;
                        (grdProductDetail.DataSource as DataTable).AcceptChanges();
                        MessageBox.Show(this, "Tổng tiền linh kiện không được lớn hơn giá IMEI!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        grvProductDetail.FocusedColumn = grvProductDetail.Columns["PRICE"];
                    }
                }
            Total();
        }

        private void txtItemQuantity_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            if (txt != null)
            {
                try
                {
                    if (txt.Text.Trim() == string.Empty)
                        txt.Text = "0";
                    txt.Text = System.Math.Abs(Convert.ToDecimal(txt.Text)).ToString();
                   
                }
                catch
                {
                }
            }
            grvProductDetail.PostEditor();
        }

        private void txtItemPrice_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            if (txt != null)
            {
                try
                {
                    if (txt.Text.Trim() == string.Empty)
                        txt.Text = "0";
                    txt.Text = System.Math.Abs(Convert.ToDecimal(txt.Text)).ToString();

                }
                catch
                {
                }
            }
            grvProductDetail.PostEditor();
        }

        private void ClearControl()
        {
            txtIMEI.Text = string.Empty;
            txtIMEI.ReadOnly = false;
            txtProductNameIMEI.Text = string.Empty;
            txtSalePrice.Text = "0";
            txtContent.Text = string.Empty;
            txtBarcode.Text = string.Empty;
            grdProductDetail.DataSource = CreatedDataProductDetail();
            objProductInStock = null;
            objSplitProduct = new PLC.SplitProduct.WSSplitProduct.SplitProduct();
            txtIMEI.Focus();
            Total();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;
            btnUpdate.Enabled = false;
            btnSearch.Enabled = false;
            if (UpdateData())
                ClearControl();
            btnUpdate.Enabled = true;
            btnSearch.Enabled = true;

        }

        private void mnuItemDeleteProduct_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvProductDetail.FocusedRowHandle < 0)
                    return;
                if (grvProductDetail.RowCount>0)
                {
                    if (MessageBox.Show(this, "Bạn muốn xóa sản phẩm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        grvProductDetail.DeleteRow(grvProductDetail.FocusedRowHandle);
                        (grdProductDetail.DataSource as DataTable).AcceptChanges();
                        Total();
                    }
                }
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi xóa sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi xóa sản phẩm", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
            }
        }

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cboStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (objProductInStock != null && grvProductDetail.RowCount > 0)
            {
                if (MessageBox.Show(this, "Bạn xóa IMEI danh sách linh kiện sẽ được làm mới! Bạn chắc chắn muốn xóa IMEI không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    grdProductDetail.DataSource = CreatedDataProductDetail();
                    grdProductDetail.RefreshDataSource();
                    objProductInStock = null;
                }
                else
                {
                    cboStore.SetValue(objStore.StoreID);
                    return;
                }
            }
            txtIMEI.Text = string.Empty;
            txtIMEI.ReadOnly = false;
            txtProductNameIMEI.Text = string.Empty;
            txtSalePrice.Text = "0";

            if (cboStore.ColumnID <= 0)
                objStore = null;
            else
            {
                new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objStore, cboStore.ColumnID);
            }
        }

        private void txtIMEI_ReadOnlyChanged(object sender, EventArgs e)
        {
            if (txtIMEI.ReadOnly)
                txtIMEI.BackColor = System.Drawing.SystemColors.Info;
            else
                txtIMEI.BackColor = Color.White;
        }
    }
}
