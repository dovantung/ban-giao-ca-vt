﻿namespace ERP.Inventory.DUI.SplitProduct
{
    partial class frmSplitProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOutputTypeIMEI = new System.Windows.Forms.TextBox();
            this.txtInputTypeIMEI = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.btnDeleteIMEI = new DevExpress.XtraEditors.SimpleButton();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.txtBarcode = new DevExpress.XtraEditors.TextEdit();
            this.txtContent = new DevExpress.XtraEditors.MemoEdit();
            this.txtSalePrice = new System.Windows.Forms.TextBox();
            this.txtProductNameIMEI = new System.Windows.Forms.TextBox();
            this.txtInputTypeID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIMEI = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.grdProductDetail = new DevExpress.XtraGrid.GridControl();
            this.mnuFlex = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemIsAutoSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemDeleteProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.grvProductDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtItemQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtItemPrice = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTotalRow = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtInputPriceIMEI = new System.Windows.Forms.TextBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProductDetail)).BeginInit();
            this.mnuFlex.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvProductDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Kho:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(4, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 38);
            this.label6.TabIndex = 4;
            this.label6.Text = "Hình thức nhập linh kiện:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(420, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Hình thức xuất IMEI:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(420, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Hình thức nhập IMEI:";
            // 
            // txtOutputTypeIMEI
            // 
            this.txtOutputTypeIMEI.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputTypeIMEI.Location = new System.Drawing.Point(549, 12);
            this.txtOutputTypeIMEI.MaxLength = 20;
            this.txtOutputTypeIMEI.Name = "txtOutputTypeIMEI";
            this.txtOutputTypeIMEI.ReadOnly = true;
            this.txtOutputTypeIMEI.Size = new System.Drawing.Size(301, 22);
            this.txtOutputTypeIMEI.TabIndex = 3;
            // 
            // txtInputTypeIMEI
            // 
            this.txtInputTypeIMEI.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputTypeIMEI.Location = new System.Drawing.Point(549, 42);
            this.txtInputTypeIMEI.MaxLength = 20;
            this.txtInputTypeIMEI.Name = "txtInputTypeIMEI";
            this.txtInputTypeIMEI.ReadOnly = true;
            this.txtInputTypeIMEI.Size = new System.Drawing.Size(301, 22);
            this.txtInputTypeIMEI.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboStore);
            this.groupBox1.Controls.Add(this.btnDeleteIMEI);
            this.groupBox1.Controls.Add(this.lblBarcode);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.txtBarcode);
            this.groupBox1.Controls.Add(this.txtContent);
            this.groupBox1.Controls.Add(this.txtSalePrice);
            this.groupBox1.Controls.Add(this.txtProductNameIMEI);
            this.groupBox1.Controls.Add(this.txtInputTypeID);
            this.groupBox1.Controls.Add(this.txtInputTypeIMEI);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtIMEI);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtOutputTypeIMEI);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(4, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(866, 171);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(112, 12);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(301, 24);
            this.cboStore.TabIndex = 40;
            this.cboStore.SelectionChangeCommitted += new System.EventHandler(this.cboStore_SelectionChangeCommitted);
            // 
            // btnDeleteIMEI
            // 
            this.btnDeleteIMEI.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteIMEI.Appearance.Options.UseFont = true;
            this.btnDeleteIMEI.Image = global::ERP.Inventory.DUI.Properties.Resources.del;
            this.btnDeleteIMEI.Location = new System.Drawing.Point(286, 71);
            this.btnDeleteIMEI.LookAndFeel.SkinName = "Blue";
            this.btnDeleteIMEI.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteIMEI.Name = "btnDeleteIMEI";
            this.btnDeleteIMEI.Size = new System.Drawing.Size(24, 22);
            this.btnDeleteIMEI.TabIndex = 10;
            this.btnDeleteIMEI.Click += new System.EventHandler(this.btnDeleteIMEI_Click);
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(6, 148);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(63, 16);
            this.lblBarcode.TabIndex = 17;
            this.lblBarcode.Text = "Barcode:";
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Appearance.Options.UseFont = true;
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.Location = new System.Drawing.Point(286, 145);
            this.btnSearch.LookAndFeel.SkinName = "Blue";
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(24, 22);
            this.btnSearch.TabIndex = 19;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(112, 145);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Properties.Appearance.Options.UseFont = true;
            this.txtBarcode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarcode.Properties.LookAndFeel.SkinName = "Blue";
            this.txtBarcode.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtBarcode.Size = new System.Drawing.Size(174, 22);
            this.txtBarcode.TabIndex = 18;
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(112, 100);
            this.txtContent.Name = "txtContent";
            this.txtContent.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContent.Properties.Appearance.Options.UseFont = true;
            this.txtContent.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.Info;
            this.txtContent.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtContent.Properties.LookAndFeel.SkinName = "Blue";
            this.txtContent.Properties.MaxLength = 2000;
            this.txtContent.Size = new System.Drawing.Size(738, 39);
            this.txtContent.TabIndex = 16;
            // 
            // txtSalePrice
            // 
            this.txtSalePrice.BackColor = System.Drawing.SystemColors.Info;
            this.txtSalePrice.Location = new System.Drawing.Point(711, 71);
            this.txtSalePrice.Name = "txtSalePrice";
            this.txtSalePrice.ReadOnly = true;
            this.txtSalePrice.Size = new System.Drawing.Size(139, 22);
            this.txtSalePrice.TabIndex = 14;
            // 
            // txtProductNameIMEI
            // 
            this.txtProductNameIMEI.BackColor = System.Drawing.SystemColors.Info;
            this.txtProductNameIMEI.Location = new System.Drawing.Point(413, 71);
            this.txtProductNameIMEI.Name = "txtProductNameIMEI";
            this.txtProductNameIMEI.ReadOnly = true;
            this.txtProductNameIMEI.Size = new System.Drawing.Size(254, 22);
            this.txtProductNameIMEI.TabIndex = 12;
            // 
            // txtInputTypeID
            // 
            this.txtInputTypeID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputTypeID.Location = new System.Drawing.Point(112, 42);
            this.txtInputTypeID.MaxLength = 20;
            this.txtInputTypeID.Name = "txtInputTypeID";
            this.txtInputTypeID.ReadOnly = true;
            this.txtInputTypeID.Size = new System.Drawing.Size(301, 22);
            this.txtInputTypeID.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 15;
            this.label8.Text = "Nội dung:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "IMEI:";
            // 
            // txtIMEI
            // 
            this.txtIMEI.Location = new System.Drawing.Point(112, 71);
            this.txtIMEI.MaxLength = 20;
            this.txtIMEI.Name = "txtIMEI";
            this.txtIMEI.Size = new System.Drawing.Size(174, 22);
            this.txtIMEI.TabIndex = 9;
            this.txtIMEI.ReadOnlyChanged += new System.EventHandler(this.txtIMEI_ReadOnlyChanged);
            this.txtIMEI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIMEI_KeyPress);
            this.txtIMEI.Leave += new System.EventHandler(this.txtIMEI_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(673, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 16);
            this.label5.TabIndex = 13;
            this.label5.Text = "Giá:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(314, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "Tên sản phẩm:";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.grdProductDetail);
            this.groupControl1.Location = new System.Drawing.Point(5, 176);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(866, 287);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Danh sách linh kiện";
            // 
            // grdProductDetail
            // 
            this.grdProductDetail.ContextMenuStrip = this.mnuFlex;
            this.grdProductDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdProductDetail.Location = new System.Drawing.Point(2, 24);
            this.grdProductDetail.MainView = this.grvProductDetail;
            this.grdProductDetail.Name = "grdProductDetail";
            this.grdProductDetail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txtItemQuantity,
            this.txtItemPrice});
            this.grdProductDetail.Size = new System.Drawing.Size(862, 261);
            this.grdProductDetail.TabIndex = 0;
            this.grdProductDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvProductDetail});
            // 
            // mnuFlex
            // 
            this.mnuFlex.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemIsAutoSeparator,
            this.toolStripSeparator1,
            this.mnuItemDeleteProduct});
            this.mnuFlex.Name = "mnuFlex";
            this.mnuFlex.Size = new System.Drawing.Size(150, 38);
            // 
            // mnuItemIsAutoSeparator
            // 
            this.mnuItemIsAutoSeparator.Name = "mnuItemIsAutoSeparator";
            this.mnuItemIsAutoSeparator.Size = new System.Drawing.Size(146, 6);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(146, 6);
            // 
            // mnuItemDeleteProduct
            // 
            this.mnuItemDeleteProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.mnuItemDeleteProduct.Name = "mnuItemDeleteProduct";
            this.mnuItemDeleteProduct.Size = new System.Drawing.Size(149, 22);
            this.mnuItemDeleteProduct.Text = "Xóa sản phẩm";
            this.mnuItemDeleteProduct.Click += new System.EventHandler(this.mnuItemDeleteProduct_Click);
            // 
            // grvProductDetail
            // 
            this.grvProductDetail.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvProductDetail.Appearance.FocusedRow.Options.UseFont = true;
            this.grvProductDetail.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvProductDetail.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.grvProductDetail.Appearance.FooterPanel.Options.UseFont = true;
            this.grvProductDetail.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grvProductDetail.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvProductDetail.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvProductDetail.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvProductDetail.Appearance.Row.Options.UseFont = true;
            this.grvProductDetail.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grvProductDetail.ColumnPanelRowHeight = 20;
            this.grvProductDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.grvProductDetail.GridControl = this.grdProductDetail;
            this.grvProductDetail.Name = "grvProductDetail";
            this.grvProductDetail.OptionsView.ColumnAutoWidth = false;
            this.grvProductDetail.OptionsView.ShowGroupPanel = false;
            this.grvProductDetail.OptionsView.ShowIndicator = false;
            this.grvProductDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvProductDetail_CellValueChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Mã linh kiện";
            this.gridColumn1.FieldName = "PRODUCTID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 149;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "Tên linh kiện";
            this.gridColumn2.FieldName = "PRODUCTNAME";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 261;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Số lượng";
            this.gridColumn3.ColumnEdit = this.txtItemQuantity;
            this.gridColumn3.FieldName = "QUANTITY";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 77;
            // 
            // txtItemQuantity
            // 
            this.txtItemQuantity.AutoHeight = false;
            this.txtItemQuantity.Mask.EditMask = "N0";
            this.txtItemQuantity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtItemQuantity.Mask.UseMaskAsDisplayFormat = true;
            this.txtItemQuantity.MaxLength = 10;
            this.txtItemQuantity.Name = "txtItemQuantity";
            this.txtItemQuantity.NullText = "0";
            this.txtItemQuantity.EditValueChanged += new System.EventHandler(this.txtItemQuantity_EditValueChanged);
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Đơn giá";
            this.gridColumn4.ColumnEdit = this.txtItemPrice;
            this.gridColumn4.FieldName = "PRICE";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 147;
            // 
            // txtItemPrice
            // 
            this.txtItemPrice.AutoHeight = false;
            this.txtItemPrice.Mask.EditMask = "N0";
            this.txtItemPrice.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtItemPrice.Mask.UseMaskAsDisplayFormat = true;
            this.txtItemPrice.MaxLength = 17;
            this.txtItemPrice.Name = "txtItemPrice";
            this.txtItemPrice.NullText = "0";
            this.txtItemPrice.EditValueChanged += new System.EventHandler(this.txtItemPrice_EditValueChanged);
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn5.AppearanceCell.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Thành tiền";
            this.gridColumn5.DisplayFormat.FormatString = "N0";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn5.FieldName = "AMOUNT";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 183;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 16);
            this.label9.TabIndex = 20;
            this.label9.Text = "Tổng SL linh kiện:";
            // 
            // txtTotalRow
            // 
            this.txtTotalRow.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalRow.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalRow.Location = new System.Drawing.Point(116, 6);
            this.txtTotalRow.Name = "txtTotalRow";
            this.txtTotalRow.ReadOnly = true;
            this.txtTotalRow.Size = new System.Drawing.Size(107, 22);
            this.txtTotalRow.TabIndex = 27;
            this.txtTotalRow.Text = "0";
            this.txtTotalRow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(229, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(118, 16);
            this.label10.TabIndex = 20;
            this.label10.Text = "Tổng tiền linh kiện:";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalAmount.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalAmount.Location = new System.Drawing.Point(348, 6);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(127, 22);
            this.txtTotalAmount.TabIndex = 27;
            this.txtTotalAmount.Text = "0";
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(481, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 16);
            this.label11.TabIndex = 20;
            this.label11.Text = "Tiền nhập lại IMEI:";
            // 
            // txtInputPriceIMEI
            // 
            this.txtInputPriceIMEI.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputPriceIMEI.ForeColor = System.Drawing.Color.Red;
            this.txtInputPriceIMEI.Location = new System.Drawing.Point(601, 6);
            this.txtInputPriceIMEI.Name = "txtInputPriceIMEI";
            this.txtInputPriceIMEI.ReadOnly = true;
            this.txtInputPriceIMEI.Size = new System.Drawing.Size(127, 22);
            this.txtInputPriceIMEI.TabIndex = 27;
            this.txtInputPriceIMEI.Text = "0";
            this.txtInputPriceIMEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnUpdate);
            this.panelControl1.Controls.Add(this.txtTotalRow);
            this.panelControl1.Controls.Add(this.label9);
            this.panelControl1.Controls.Add(this.txtInputPriceIMEI);
            this.panelControl1.Controls.Add(this.label10);
            this.panelControl1.Controls.Add(this.label11);
            this.panelControl1.Controls.Add(this.txtTotalAmount);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 469);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(876, 34);
            this.panelControl1.TabIndex = 1;
            this.panelControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl1_Paint);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Appearance.Options.UseFont = true;
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.Location = new System.Drawing.Point(776, 5);
            this.btnUpdate.LookAndFeel.SkinName = "Blue";
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(94, 25);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // frmSplitProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 503);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(892, 542);
            this.Name = "frmSplitProduct";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tách linh kiện từ sản phẩm";
            this.Load += new System.EventHandler(this.frmSplitProduct_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProductDetail)).EndInit();
            this.mnuFlex.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvProductDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOutputTypeIMEI;
        private System.Windows.Forms.TextBox txtInputTypeIMEI;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl grdProductDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView grvProductDetail;
        private System.Windows.Forms.TextBox txtSalePrice;
        private System.Windows.Forms.TextBox txtProductNameIMEI;
        private System.Windows.Forms.TextBox txtInputTypeID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIMEI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.MemoEdit txtContent;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.SimpleButton btnDeleteIMEI;
        private System.Windows.Forms.Label lblBarcode;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.TextEdit txtBarcode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTotalRow;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtTotalAmount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtInputPriceIMEI;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnUpdate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtItemQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtItemPrice;
        private System.Windows.Forms.ContextMenuStrip mnuFlex;
        private System.Windows.Forms.ToolStripSeparator mnuItemIsAutoSeparator;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDeleteProduct;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStore;
    }
}