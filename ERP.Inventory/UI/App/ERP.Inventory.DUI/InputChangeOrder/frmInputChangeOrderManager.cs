﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder;
using ERP.Inventory.PLC.InputChangeOrder;
using ERP.MasterData.PLC.MD;
using Library.AppCore.Other;
namespace ERP.Inventory.DUI.InputChangeOrder
{
    /// <summary>
    /// Quản lý yêu cầu nhập đổi/trả hàng
    /// Created by  :   Đặng Minh Hùng
    /// Date        :   01/04/2014
    /// </summary>
    public partial class frmInputChangeOrderManager : Form
    {
        #region Variable
        private ERP.Inventory.PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
        private ERP.Inventory.PLC.Input.PLCInputVoucher objPLCInputVoucher = new PLC.Input.PLCInputVoucher();
        private PLCInputChangeOrder objPLCInputChangeOrder = new PLCInputChangeOrder();
        private string strPermission_Search = "PM_INPUTCHANGEORDER_VIEW";//Quản lý yêu cầu nhập đổi/trả hàng - Xem
        private string strPermission_ExportExcel = "PM_INPUTCHANGEORDER_EXPORTEXCEL";//Quản lý yêu cầu nhập đổi/trả hàng - Xuất Excel
        private int intReportID = 0;
        private int intReportID1 = 0;
        private int intReportID2 = 0;
        private int intReportID3 = 0;
        private int intReportID4 = 0;
        private int intReportProductChange = 0;
        private int intType = 0;
        private MasterData.PLC.MD.PLCSaleOrderType objPLCSaleOrderType = new MasterData.PLC.MD.PLCSaleOrderType();
        private string strOutputVoucherID = string.Empty;
        private string strInputVoucherID = string.Empty;
        #endregion

        #region Constructor
        public frmInputChangeOrderManager()
        {
            InitializeComponent();
        }
        #endregion

        #region Method
        /// <summary>
        /// Load ComboBox
        /// </summary>
        /// 
        private void LoadComboBox()
        {
            try
            {
                cboSearchType.SelectedIndex = 0;
                cboStoreIDList.InitControl(true);
                ERP.MasterData.DUI.Common.CommonFunction.SetToolTip(txtKeyword, "Tìm kiếm theo mã phiếu yêu cầu, tên kho nhập, mã phiếu xuất cũ");

                Report.PLC.PLCReportDataSource objPLCReport = new Report.PLC.PLCReportDataSource();
                DataTable dtbInputChangeOrderType = objPLCReport.GetDataSource("MD_INPUTCHANGEORDERTYPE_GET", new object[] { });
                cboInputChangeOrderType.InitControl(true, dtbInputChangeOrderType, "InputChangeOrderTypeID", "InputChangeOrderTypeName", "-- Chọn loại yêu cầu đổi trả --");
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi nạp combobox", objExce, DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp combobox");
            }
        }
        /// <summary>
        /// Kiểm tra dữ liệu nhập vào
        /// </summary>
        private bool CheckInput()
        {
            if (Globals.DateDiff(dtpToDate.Value, dtpFromDate.Value, Globals.DateDiffType.DATE) < 0)
            {
                MessageBox.Show("Từ ngày không được lớn hơn đến ngày yêu cầu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpToDate.Focus();
                return false;
            }
            return true;
        }
        /// <summary>
        /// Truyền tham số vào hàm tìm kiếm yêu cầu nhập đổi/trả hàng
        /// </summary>
        private void LoadData()
        {
            try
            {
                List<object> lstKeywords = new List<object>();
                lstKeywords.Add("@Keyword");
                lstKeywords.Add(txtKeyword.Text.Trim());
                lstKeywords.Add("@SearchType");
                lstKeywords.Add(cboSearchType.SelectedIndex);
                if (dtpFromDate.Checked && dtpToDate.Checked)
                {
                    lstKeywords.Add("@FromDate");
                    lstKeywords.Add(dtpFromDate.Value);
                    lstKeywords.Add("@ToDate");
                    lstKeywords.Add(dtpToDate.Value);
                }
                lstKeywords.Add("@StoreIDList");
                lstKeywords.Add(cboStoreIDList.StoreIDList);
                lstKeywords.Add("@IsDeleted");
                lstKeywords.Add(chkIsDeleted.Checked);
                lstKeywords.Add("@InputChangeOrderTypeList");
                lstKeywords.Add(cboInputChangeOrderType.ColumnIDList);

                DataTable dtbData = objPLCInputChangeOrder.SearchData(lstKeywords.ToArray());
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                    return;
                }
                flexData.DataSource = dtbData;
                FormatFlex(flexData);
                if (dtbData.Rows.Count > 0)
                    btnExportExcel.Enabled = true;
                else
                    btnExportExcel.Enabled = false;
            }
            catch (System.Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tìm kiếm yêu cầu nhập đổi/trả hàng", objExce.ToString(), "frmInputChangeOrderManager -> LoadData", DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi tìm kiếm yêu cầu nhập đổi/trả hàng");
            }
        }
        private void FormatFlex(C1.Win.C1FlexGrid.C1FlexGrid _flex)
        {
            for (int i = 0; i < _flex.Cols.Count; i++)
            {
                _flex.Cols[i].Visible = false;
                _flex.Cols[i].AllowSorting = false;
                _flex.Cols[i].AllowDragging = false;
                _flex.Cols[i].AllowEditing = false;
            }
            _flex.Rows.Fixed = 2;
            _flex.Cols[0].Visible = true;
            C1FlexGridObject.SetColumnVisible(_flex, true, "InputChangeOrderDate,InputChangeOrderID,InputChangeOrderTypeName,CustomerName,InputChangeStoreName,OldOutputVoucherID,IsReviewed,CURRENTREVIEWFULLNAME,ReviewedDate,IsInputChanged,InputChangeDate,CreateInputFullName,NewOutputVoucherID,NewInputVoucherID,InputChangeOrderContent");
            C1FlexGridObject.SetColumnAllowMerging(_flex, "InputChangeOrderDate,InputChangeOrderID,InputChangeOrderTypeName,CustomerName,InputChangeStoreName,OldOutputVoucherID,IsReviewed,CURRENTREVIEWFULLNAME,ReviewedDate,InputChangeOrderContent", true);
            C1FlexGridObject.SetColumnCaption(_flex, "InputChangeOrderDate,Ngày yêu cầu,InputChangeOrderID,Mã phiếu yêu cầu,InputChangeOrderTypeName,Loại yêu cầu,CustomerName,Tên khách hàng,InputChangeStoreName,Kho nhập,OldOutputVoucherID,Mã phiếu xuất cũ,IsReviewed,Duyệt,CURRENTREVIEWFULLNAME,Nhân viên duyệt,ReviewedDate,Ngày duyệt,IsInputChanged,Đã nhập,InputChangeDate,Ngày nhập,InputChangeOrderContent,Nội dụng yêu cầu");
            _flex[0, "InputChangeOrderDate"] = "Ngày yêu cầu";
            _flex[1, "InputChangeOrderDate"] = "Ngày yêu cầu";
            _flex[0, "InputChangeOrderID"] = "Mã phiếu yêu cầu";
            _flex[1, "InputChangeOrderID"] = "Mã phiếu yêu cầu";
            _flex[0, "InputChangeOrderTypeName"] = "Loại yêu cầu";
            _flex[1, "InputChangeOrderTypeName"] = "Loại yêu cầu";
            _flex[0, "CustomerName"] = "Tên khách hàng";
            _flex[1, "CustomerName"] = "Tên khách hàng";
            _flex[0, "InputChangeStoreName"] = "Kho nhập";
            _flex[1, "InputChangeStoreName"] = "Kho nhập";
            _flex[0, "OldOutputVoucherID"] = "Mã phiếu xuất cũ";
            _flex[1, "OldOutputVoucherID"] = "Mã phiếu xuất cũ";
            _flex[0, "IsReviewed"] = "Duyệt";
            _flex[1, "IsReviewed"] = "Duyệt";
            _flex[0, "CURRENTREVIEWFULLNAME"] = "Nhân viên duyệt";
            _flex[1, "CURRENTREVIEWFULLNAME"] = "Nhân viên duyệt";
            _flex[0, "ReviewedDate"] = "Ngày duyệt";
            _flex[1, "ReviewedDate"] = "Ngày duyệt";
            _flex[0, "IsInputChanged"] = "Thông tin xuất";
            _flex[1, "IsInputChanged"] = "Đã nhập/ xuất";
            _flex[0, "InputChangeDate"] = "Thông tin xuất";
            _flex[1, "InputChangeDate"] = "Ngày nhập/ xuất";
            _flex[0, "CreateInputFullName"] = "Thông tin xuất";
            _flex[1, "CreateInputFullName"] = "NV nhập/ xuất";
            _flex[0, "NewOutputVoucherID"] = "Thông tin xuất";
            _flex[1, "NewOutputVoucherID"] = "Mã phiếu xuất mới";
            _flex[0, "NewInputVoucherID"] = "Thông tin xuất";
            _flex[1, "NewInputVoucherID"] = "Mã phiếu nhập mới";
            _flex[0, "InputChangeOrderContent"] = "Nội dụng yêu cầu";
            _flex[1, "InputChangeOrderContent"] = "Nội dụng yêu cầu";
            C1FlexGridObject.SetColumnWidth(_flex, "InputChangeOrderDate,115,InputChangeOrderID,120,InputChangeOrderTypeName,210,CustomerName,150,InputChangeStoreName,150,OldOutputVoucherID,120,IsReviewed,50,CURRENTREVIEWFULLNAME,180,ReviewedDate,115,IsInputChanged,70,InputChangeDate,115,CreateInputFullName,180,NewOutputVoucherID,120,NewInputVoucherID,120,InputChangeOrderContent,150");
            if (chkIsDeleted.Checked)
            {
                C1FlexGridObject.SetColumnVisible(_flex, true, "DeleteFullName,DeletedDate");
                C1FlexGridObject.SetColumnAllowMerging(_flex, "DeleteFullName,DeletedDate", true);
                C1FlexGridObject.SetColumnCaption(_flex, "DeleteFullName,Nhân viên hủy,DeletedDate,Ngày hủy");
                C1FlexGridObject.SetColumnWidth(_flex, "DeleteFullName,180,DeletedDate,115");
                _flex.Cols["DeletedDate"].Format = "dd/MM/yyyy HH:mm";
                _flex[0, "DeleteFullName"] = "Nhân viên hủy";
                _flex[1, "DeleteFullName"] = "Nhân viên hủy";
                _flex[0, "DeletedDate"] = "Ngày hủy";
                _flex[1, "DeletedDate"] = "Ngày hủy";
            }

            _flex.Cols["IsReviewed"].DataType = typeof(bool);
            _flex.Cols["IsInputChanged"].DataType = typeof(bool);
            _flex.Cols["IsDeleted"].DataType = typeof(bool);
            _flex.Cols["InputChangeOrderDate"].Format = "dd/MM/yyyy HH:mm";
            _flex.Cols["ReviewedDate"].Format = "dd/MM/yyyy HH:mm";
            _flex.Cols["InputChangeDate"].Format = "dd/MM/yyyy HH:mm";

            C1.Win.C1FlexGrid.CellStyle style = _flex.Styles.Add("flexStyle");
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            style.WordWrap = true;
            C1.Win.C1FlexGrid.CellRange range = _flex.GetCellRange(0, 0, 1, _flex.Cols.Count - 1);
            range.Style = style;
            _flex.Cols.Frozen = _flex.Cols["InputChangeStoreName"].Index;
            _flex.Rows[0].Height = 22;
            _flex.Rows[1].Height = 36;
            _flex.Rows[0].AllowMerging = true;
            _flex.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;

            int intSTT = 0;
            for (int i = _flex.Rows.Fixed; i < _flex.Rows.Count; i++)
                _flex[i, 0] = ++intSTT;
        }
        #endregion

        #region Event
        private void frmInputChangeOrderManager_Load(object sender, EventArgs e)
        {
            mnuItemPrintProductChange.Visible = false;
            mnuItemCreateInvoiceReturn.Visible = false;
            mnuItemCreateInvoiceOutput.Visible = false;
            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    intReportID = Convert.ToInt32(hstbParam["ReportID"]);
                    if (intReportID > 0)
                    {
                        ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
                        if (objReport != null)
                            mnuItemPrintOutputVoucher.Text = "In " + objReport.ReportName.ToLower();
                    }
                    intReportID1 = Convert.ToInt32(hstbParam["ReportID1"]);
                    if (intReportID1 > 0)
                    {
                        mnuItemPrintVoucherOV.Enabled = true;
                        ERP.MasterData.PLC.MD.WSReport.Report objReport1 = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID1);
                        if (objReport1 != null)
                            mnuItemPrintVoucherOV.Text = "In " + objReport1.ReportName.ToLower();
                    }
                    intReportID2 = Convert.ToInt32(hstbParam["ReportID2"]);
                    if (intReportID2 > 0)
                    {
                        mnuItemPrintOutputStore.Enabled = true;
                        ERP.MasterData.PLC.MD.WSReport.Report objReport2 = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID2);
                        if (objReport2 != null)
                            mnuItemPrintOutputStore.Text = "In " + objReport2.ReportName.ToLower();
                    }
                    intReportID3 = Convert.ToInt32(hstbParam["ReportID3"]);
                    if (intReportID3 > 0)
                    {
                        mnuItemPrintWarrantyExt.Enabled = true;
                        ERP.MasterData.PLC.MD.WSReport.Report objReport3 = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID3);
                        if (objReport3 != null)
                            mnuItemPrintWarrantyExt.Text = "In " + objReport3.ReportName.ToLower();
                    }
                    intReportID4 = Convert.ToInt32(hstbParam["ReportID4"]);
                    if (intReportID4 > 0)
                    {
                        mnuItemPrintCare.Enabled = true;
                        ERP.MasterData.PLC.MD.WSReport.Report objReport4 = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID4);
                        if (objReport4 != null)
                            mnuItemPrintCare.Text = "In " + objReport4.ReportName.ToLower();
                    }
                    intReportProductChange = Convert.ToInt32(hstbParam["ReportProductChange"]);
                    if (intReportProductChange > 0)
                    {
                        mnuItemPrintProductChange.Visible = true;
                        mnuItemPrintProductChange.Enabled = true;
                        ERP.MasterData.PLC.MD.WSReport.Report objReportProductChange = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportProductChange);
                        if (objReportProductChange != null)
                            mnuItemPrintProductChange.Text = "In " + objReportProductChange.ReportName.ToLower();
                    }
                    if (hstbParam.Contains("TYPE"))
                    {
                        int.TryParse(hstbParam["TYPE"].ToString(), out intType);
                        if (intType == 0)
                        {
                            mnuItemCreateInvoiceReturn.Visible = false;
                            mnuItemCreateInvoiceOutput.Visible = false;
                        }
                        else
                        {
                            mnuItemCreateInvoiceReturn.Visible = true;
                            mnuItemCreateInvoiceOutput.Visible = true;
                        }
                    }
                }
                catch { }
            }
            btnExportExcel.Enabled = false;
            LoadComboBox();

            btnSearch.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Search);
            btnExportExcel.Enabled = (flexData.Rows.Count > flexData.Rows.Fixed && SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel));
        }

        private void flexData_DoubleClick(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            //int intType = 0;
            //Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
            //if (hstbParam.Contains("TYPE"))
            //{
            //    intType = Convert.ToInt32(hstbParam["TYPE"]);
            //}
            //if (intType == 0)
            //{
            //    frmInputChangeOrder frm1 = new frmInputChangeOrder(Convert.ToInt32(flexData[flexData.RowSel, "InputChangeOrderTypeID"]), Convert.ToString(flexData[flexData.RowSel, "InputChangeOrderID"]).Trim(), Convert.ToBoolean(flexData[flexData.RowSel, "IsDeleted"]), true);
            //    frm1.strOutputVoucherID = Convert.ToString(flexData[flexData.RowSel, "OldOutputVoucherID"]).Trim();
            //    frm1.intReportID = intReportID;
            //    frm1.Text = Convert.ToString(flexData[flexData.RowSel, "InputChangeOrderTypeName"]).Trim();
            //    frm1.ShowDialog();
            //    if (frm1.IsHasAction)
            //    {
            //        btnSearch_Click(null, null);
            //    }
            //}
            //else
            //{
            frmInputChangeOrderViettel frm1 = new frmInputChangeOrderViettel(Convert.ToInt32(flexData[flexData.RowSel, "InputChangeOrderTypeID"]), Convert.ToString(flexData[flexData.RowSel, "InputChangeOrderID"]).Trim(), Convert.ToBoolean(flexData[flexData.RowSel, "IsDeleted"]), true);
            frm1.strOutputVoucherID = Convert.ToString(flexData[flexData.RowSel, "OldOutputVoucherID"]).Trim();
            frm1.intReportID = intReportID;
            frm1.Text = Convert.ToString(flexData[flexData.RowSel, "InputChangeOrderTypeName"]).Trim();
            frm1.ShowDialog();
            if (frm1.IsHasAction)
            {
                btnSearch_Click(null, null);
            }
            //}

        }
        private void mnuContext_Opening(object sender, CancelEventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            if (intType != 0)
            {
                bool bolCreateReturnInvoiceVAT = SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHERRETURN_PRINTVAT");
                bool bolCreateChangeInvoiceVAT = SystemConfig.objSessionUser.IsPermission("PM_INPUTCHANGE_PRINTVAT");
                mnuItemCreateInvoiceOutput.Enabled = bolCreateChangeInvoiceVAT;
                mnuItemCreateInvoiceReturn.Enabled = bolCreateReturnInvoiceVAT;

                if (flexData[flexData.RowSel, "NewInputVoucherID"].ToString() != string.Empty ||
                    flexData[flexData.RowSel, "NewOutputVoucherID"].ToString() != string.Empty)
                {
                    DataTable dtbOrderChangeType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_INPUTCHANGEORDERTYPE_SEL",
                        new object[] { "@InputChangeOrderTypeID", flexData[flexData.RowSel, "InputChangeOrderTypeID"] });
                    if (dtbOrderChangeType != null && dtbOrderChangeType.Rows.Count > 0 && bolCreateChangeInvoiceVAT)
                    {
                        int bolReturn = 1;
                        int.TryParse(dtbOrderChangeType.Rows[0]["ISINPUTRETURN"].ToString(), out bolReturn);
                        mnuItemCreateInvoiceOutput.Enabled = !Convert.ToBoolean(bolReturn);
                    }
                    DataTable dtbChangeOrder = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_INPUTCHANGEORDER_ISINVOICE",
                        new object[] { "@INPUTCHANGEORDERID", flexData[flexData.RowSel, "INPUTCHANGEORDERID"] });
                    if (dtbChangeOrder != null && dtbChangeOrder.Rows[0][0].ToString().CompareTo("1") == 0)
                    {
                        mnuItemCreateInvoiceReturn.Enabled = false;
                    }
                    else if (dtbChangeOrder != null && dtbChangeOrder.Rows[0][0].ToString().CompareTo("2") == 0)
                    {
                        mnuItemCreateInvoiceOutput.Enabled = false;
                    }
                    else if (dtbChangeOrder != null && dtbChangeOrder.Rows[0][0].ToString().CompareTo("3") == 0)
                    {
                        mnuItemCreateInvoiceReturn.Enabled = mnuItemCreateInvoiceOutput.Enabled = false;
                    }
                }
                else
                {
                    mnuItemCreateInvoiceOutput.Enabled = mnuItemCreateInvoiceReturn.Enabled = false;
                }
                if (bolCreateReturnInvoiceVAT || bolCreateChangeInvoiceVAT)
                    mnuItemPrintInput.Enabled = false;

                if (!string.IsNullOrEmpty(flexData[flexData.RowSel, "OldOutputVoucherID"].ToString()))
                {
                    DataTable dtbInvoiceId = new ERP.Report.PLC.PLCReportDataSource()
                     .GetDataSource("VAT_INVOICE_SELBYOUTPUTID", new object[] { "@OUTPUTVOUCHERID", flexData[flexData.RowSel, "OldOutputVoucherID"].ToString() });
                    if (dtbInvoiceId == null || dtbInvoiceId.Rows.Count == 0)
                    {
                        mnuItemCreateInvoiceReturn.Enabled = false;
                    }
                }
                //var strOutputVoucher = flexData[flexData.RowSel, "OLDOUTPUTVOUCHERID"].ToString();
                //var objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucher);
                //if (string.IsNullOrEmpty(objOutputVoucher.InvoiceSymbol.Trim()))
                //{
                //    mnuItemCreateInvoiceOutput.Enabled = mnuItemCreateInvoiceReturn.Enabled = false;
                //}

            }
            mnuItemPrintOutputVoucher.Enabled = false;
            mnuItemPrintVoucherOV.Enabled = false;
            mnuContextViewInputChangeOrder_Comment.Enabled = false;
            mnuItemPrintOutputStore.Enabled = false;
            mnuItemPrintProductChange.Enabled = false;
            mnuItemPrintWarrantyExt.Enabled = false;
            mnuItemPrintCare.Enabled = false;
            mnuItemPrintSaleOrder.DropDownItems.Clear();
            mnuItemPrintSaleOrder.Enabled = false;
            mnuItemPrintSaleOrder.Visible = false;
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            String strViewCommentFunctionID = Convert.ToString(flexData[flexData.RowSel, "ViewConversationFunctionID"]).Trim();
            String strAddCommentFunctionID = Convert.ToString(flexData[flexData.RowSel, "AddConversationFunctionID"]).Trim();
            String strDeleteCommentFunctionID = Convert.ToString(flexData[flexData.RowSel, "DelConversationFunctionID"]).Trim();
            String strDelAllConversationFunctionID = Convert.ToString(flexData[flexData.RowSel, "DelAllConversationFunctionID"]).Trim();
            mnuContextViewInputChangeOrder_Comment.Enabled = SystemConfig.objSessionUser.IsPermission(strViewCommentFunctionID) || SystemConfig.objSessionUser.IsPermission(strAddCommentFunctionID) || SystemConfig.objSessionUser.IsPermission(strDeleteCommentFunctionID) || SystemConfig.objSessionUser.IsPermission(strDelAllConversationFunctionID);
            if (Convert.ToBoolean(flexData[flexData.RowSel, "IsInputChanged"]) || Convert.ToBoolean(flexData[flexData.RowSel, "IsOutProduct"]))
            {
                string strNewInputVoucherID = string.Empty;
                string strNewOutputVoucherID = string.Empty;
                string strSaleOrderID = string.Empty;
                if (flexData[flexData.RowSel, "NewInputVoucherID"] != DBNull.Value)
                    strNewInputVoucherID = flexData[flexData.RowSel, "NewInputVoucherID"].ToString().Trim();
                if (Convert.ToBoolean(flexData[flexData.RowSel, "IsInputChanged"]) && flexData[flexData.RowSel, "NewOutputVoucherID"] != DBNull.Value)
                    strNewOutputVoucherID = flexData[flexData.RowSel, "NewOutputVoucherID"].ToString().Trim();
                if (strNewInputVoucherID != string.Empty)
                    mnuItemPrintInput.Enabled = true;
                if (strNewOutputVoucherID != string.Empty && intReportID > 0)
                    mnuItemPrintOutputVoucher.Enabled = true;
                if (strNewOutputVoucherID != string.Empty && intReportID1 > 0)
                    mnuItemPrintVoucherOV.Enabled = true;
                if (strNewOutputVoucherID != string.Empty && intReportID2 > 0)
                    mnuItemPrintOutputStore.Enabled = true;
                if (strNewOutputVoucherID != string.Empty && intReportID3 > 0)
                    mnuItemPrintWarrantyExt.Enabled = true;
                if (strNewOutputVoucherID != string.Empty && intReportID4 > 0)
                    mnuItemPrintCare.Enabled = true;
                if (strNewOutputVoucherID != string.Empty && intReportProductChange > 0)
                    mnuItemPrintProductChange.Enabled = true;
                if (Convert.ToBoolean(flexData[flexData.RowSel, "IsOutProduct"]) && flexData[flexData.RowSel, "SaleOrderID"] != DBNull.Value)
                    strSaleOrderID = flexData[flexData.RowSel, "SaleOrderID"].ToString().Trim();
                if (strSaleOrderID != string.Empty)
                {
                    mnuItemPrintSaleOrder.Enabled = true;
                    mnuItemPrintSaleOrder.Visible = true;
                    mnuItemPrintOutputVoucher.Visible = false;
                    mnuItemPrintVoucherOV.Visible = false;
                    mnuItemPrintOutputStore.Visible = false;
                    mnuItemPrintWarrantyExt.Visible = false;
                    mnuItemPrintCare.Visible = false;

                    int intSaleOrderTypeID = Convert.ToInt32(flexData[flexData.RowSel, "SaleOrderTypeID"]);
                    bool bolIsOutProduct = Convert.ToBoolean(flexData[flexData.RowSel, "IsOutProduct"]);
                    bool bolIsInCome = Convert.ToBoolean(flexData[flexData.RowSel, "IsInCome"]);
                    bool bolIsReview = Convert.ToBoolean(flexData[flexData.RowSel, "IsReviewed"]);
                    ERP.MasterData.PLC.MD.WSSaleOrderType.SaleOrderType objSaleOrderType = objPLCSaleOrderType.LoadInfoFromCache(intSaleOrderTypeID);

                    String strExp = "ISPRINTVAT = 0 ";
                    if (!bolIsReview)
                        strExp += "AND PRINTREPORTTYPE <> 2";
                    if (!bolIsInCome)
                        strExp += "AND PRINTREPORTTYPE <> 5";
                    if (!bolIsOutProduct)
                        strExp += "AND PRINTREPORTTYPE <> 3";
                    DataTable tblReportList = objSaleOrderType.DtbSaleOrderType_RP_Report;
                    DataTable tblReport = Globals.SelectDistinct(tblReportList, "ReportID", strExp);
                    foreach (DataRow objRow in tblReport.Rows)
                    {
                        bool bolIsPermissionView = SystemConfig.objSessionUser.IsPermission(Convert.ToString(objRow["ViewAllFunctionID"]).Trim());
                        if (!bolIsPermissionView)
                        {
                            bolIsPermissionView = SystemConfig.objSessionUser.IsPermission(Convert.ToString(objRow["ViewFunctionID"]).Trim())
                                && SystemConfig.objSessionUser.UserName.Equals(flexData[flexData.RowSel, "CreateInputUser"].ToString().Trim());
                        }
                        int intReportIDSaleOrder = Convert.ToInt32(objRow["ReportID"]);
                        String strReportName = Convert.ToString(objRow["ReportName"]).Trim();

                        ToolStripMenuItem mnuItem = new ToolStripMenuItem(strReportName);
                        mnuItem.Enabled = bolIsPermissionView;
                        mnuItem.Tag = objRow;
                        mnuItem.Click += new EventHandler(mnuItemSaleOrder_Click);
                        mnuItemPrintSaleOrder.DropDownItems.Add(mnuItem);
                    }
                }
                else
                {
                    mnuItemPrintOutputVoucher.Visible = true;
                    mnuItemPrintVoucherOV.Visible = true;
                    mnuItemPrintOutputStore.Visible = true;
                    mnuItemPrintWarrantyExt.Visible = true;
                    mnuItemPrintCare.Visible = true;
                    mnuItemPrintSaleOrder.Visible = false;
                    mnuItemPrintSaleOrder.Enabled = false;
                }
            }
        }
        void mnuItemSaleOrder_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            ToolStripMenuItem mnuItem = sender as ToolStripMenuItem;
            DataRow objReportInfo = mnuItem.Tag as DataRow;
            int intReportID = Convert.ToInt32(objReportInfo["REPORTID"]);
            String strPrinterTypeID = Convert.ToString(objReportInfo["PRINTERTYPEID"]).Trim();
            String strSaleOrderID = Convert.ToString(flexData[flexData.RowSel, "SaleOrderID"]).Trim();
            if (strSaleOrderID != string.Empty)
                SalesAndServices.SaleOrders.DUI.DUISaleOrders_Common.ShowReport(this, strSaleOrderID, intReportID, strPrinterTypeID);
        }
        private void mnuContextViewInputChangeOrder_Click(object sender, EventArgs e)
        {
            flexData_DoubleClick(null, null);
        }
        private void mnuContextViewInputChangeOrder_Comment_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            string strInputChangeOrderID = Convert.ToString(flexData[flexData.RowSel, "InputChangeOrderID"]).Trim();
            String strAddCommentFunctionID = Convert.ToString(flexData[flexData.RowSel, "AddConversationFunctionID"]).Trim();
            String strDeleteCommentFunctionID = Convert.ToString(flexData[flexData.RowSel, "DelConversationFunctionID"]).Trim();
            String strDelAllConversationFunctionID = Convert.ToString(flexData[flexData.RowSel, "DelAllConversationFunctionID"]).Trim();

            frmInputChangeOrder_Comment frmComment1 = new frmInputChangeOrder_Comment(strInputChangeOrderID,
                strAddCommentFunctionID, strDeleteCommentFunctionID, strDelAllConversationFunctionID);
            frmComment1.Text = strInputChangeOrderID + " - Bình luận";
            frmComment1.ShowDialog(this);
        }
        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            C1FlexGridObject.ExportExcel(flexData);
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!btnSearch.Enabled)
                return;
            if (!CheckInput())
                return;
            btnSearch.Enabled = false;
            LoadData();
            btnSearch.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Search);
            btnExportExcel.Enabled = (flexData.Rows.Count > flexData.Rows.Fixed && SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel));
        }

        private void txtKeyword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                btnSearch_Click(null, null);
            }
        }

        private void mnuItemPrintInput_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            string strNewInputVoucherID = string.Empty;
            if (flexData[flexData.RowSel, "NewInputVoucherID"] != DBNull.Value)
                strNewInputVoucherID = flexData[flexData.RowSel, "NewInputVoucherID"].ToString().Trim();
            if (strNewInputVoucherID != string.Empty)
            {
                Reports.Input.frmReportView frm = new Reports.Input.frmReportView();
                frm.InputVoucherID = strNewInputVoucherID;
                frm.bolIsPriceHide = false;
                frm.ShowDialog();
            }
        }

        #endregion

        private void mnuItemPrintOutputVoucher_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            if (intReportID < 1)
                return;
            string strNewOutputVoucherID = string.Empty;
            if (flexData[flexData.RowSel, "NewOutputVoucherID"] != DBNull.Value)
                strNewOutputVoucherID = flexData[flexData.RowSel, "NewOutputVoucherID"].ToString().Trim();
            if (strNewOutputVoucherID != string.Empty)
            {
                ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
                if (objReport == null)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in báo cáo xuất hàng");
                    return;
                }
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
                htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
                htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
                htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
                htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
                htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
                htbParameterValue.Add("OUTPUTVOUCHERID", strNewOutputVoucherID);
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
        }

        private void mnuItemPrintSaleOrder_Click(object sender, EventArgs e)
        {

        }

        private void mnuItemPrintVoucherOV_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            if (intReportID1 < 1)
                return;
            string strNewOutputVoucherID = string.Empty;
            if (flexData[flexData.RowSel, "NewOutputVoucherID"] != DBNull.Value)
                strNewOutputVoucherID = flexData[flexData.RowSel, "NewOutputVoucherID"].ToString().Trim();
            if (strNewOutputVoucherID != string.Empty)
            {
                ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID1);
                if (objReport == null)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in phiếu bán hàng");
                    return;
                }
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
                htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
                htbParameterValue.Add("OutputVoucherID", strNewOutputVoucherID);
                htbParameterValue.Add("TelephoneMainGroupIDList", Library.AppCore.AppConfig.GetConfigValue("TELEPHONEMAINGROUPIDLIST"));
                htbParameterValue.Add("Tablet_LaptopMainGroupIDList", Library.AppCore.AppConfig.GetConfigValue("TABLET_LAPTOPMAINGROUPIDLIST"));
                htbParameterValue.Add("AccessoriesMainGroupIDList", Library.AppCore.AppConfig.GetConfigValue("ACCESSORIESMAINGROUPIDLIST"));
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
        }

        private void mnuItemPrintOutputStore_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            if (intReportID2 < 1)
                return;
            string strNewOutputVoucherID = string.Empty;
            if (flexData[flexData.RowSel, "NewOutputVoucherID"] != DBNull.Value)
                strNewOutputVoucherID = flexData[flexData.RowSel, "NewOutputVoucherID"].ToString().Trim();
            if (strNewOutputVoucherID != string.Empty)
            {
                ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID2);
                if (objReport == null)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in báo cáo xuất kho hàng hóa");
                    return;
                }
                Hashtable htbParameterValue = new Hashtable();
                bool bolIsViewCostPrice = SystemConfig.objSessionUser.IsPermission("OUTPUTREPORT_VIEWCOSTPRICE");
                htbParameterValue.Add("OutputVoucherID", strNewOutputVoucherID);
                htbParameterValue.Add("IsCostPriceHide", !bolIsViewCostPrice);
                htbParameterValue.Add("IsSalePriceHide", false);
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
        }

        private void mnuItemPrintProductChange_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            if (intReportProductChange < 1)
                return;
            string strNewOutputVoucherID = string.Empty;
            if (flexData[flexData.RowSel, "NewOutputVoucherID"] != DBNull.Value)
                strNewOutputVoucherID = flexData[flexData.RowSel, "NewOutputVoucherID"].ToString().Trim();
            if (strNewOutputVoucherID != string.Empty)
            {
                ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportProductChange);
                if (objReport == null)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in báo cáo xuất hàng");
                    return;
                }
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
                htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
                htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
                htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
                htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
                htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
                htbParameterValue.Add("OUTPUTVOUCHERID", strNewOutputVoucherID);
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
        }

        private void mnuItemPrintWarrantyExt_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            if (intReportID3 < 1)
                return;
            string strNewOutputVoucherID = string.Empty;
            if (flexData[flexData.RowSel, "NewOutputVoucherID"] != DBNull.Value)
                strNewOutputVoucherID = flexData[flexData.RowSel, "NewOutputVoucherID"].ToString().Trim();
            if (strNewOutputVoucherID != string.Empty)
            {
                ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID3);
                if (objReport == null)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in báo cáo xuất kho hàng hóa");
                    return;
                }
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("OutputVoucherID", strNewOutputVoucherID);
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
        }

        private void mnuItemPrintCare_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            if (intReportID4 < 1)
                return;
            string strNewOutputVoucherID = string.Empty;
            if (flexData[flexData.RowSel, "NewOutputVoucherID"] != DBNull.Value)
                strNewOutputVoucherID = flexData[flexData.RowSel, "NewOutputVoucherID"].ToString().Trim();
            if (strNewOutputVoucherID != string.Empty)
            {
                ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID4);
                if (objReport == null)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in báo cáo xuất kho hàng hóa");
                    return;
                }
                Hashtable htbParameterValue = new Hashtable();
                htbParameterValue.Add("OutputVoucherID", strNewOutputVoucherID);
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
            }
        }

        private void mnuItemCreateInvoiceReturn_Click(object sender, EventArgs e)
        {
            try
            {
                if (flexData.RowSel < flexData.Rows.Fixed)
                    return;
                strInputVoucherID = flexData[flexData.RowSel, "NEWINPUTVOUCHERID"].ToString();
                strOutputVoucherID = flexData[flexData.RowSel, "OLDOUTPUTVOUCHERID"].ToString();
                int intInputChangeStoreID = Convert.ToInt32(flexData[flexData.RowSel, "INPUTCHANGEORDERSTOREID"]);
                int intInputChangeOrderTypeID = 0;
                ERP.MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType objInputChangeOrderType = null;
                try
                {
                    intInputChangeOrderTypeID = Convert.ToInt32(flexData[flexData.RowSel, "INPUTCHANGEORDERTYPEID"].ToString());
                    new ERP.MasterData.PLC.MD.PLCInputChangeOrderType().LoadInfo(ref objInputChangeOrderType, intInputChangeOrderTypeID);
                }
                catch
                {
                }

                var objInputVoucher = objPLCInputVoucher.LoadInfo(strInputVoucherID);
                ERP.MasterData.PLC.MD.WSInputType.InputType objInputType = new ERP.MasterData.PLC.MD.PLCInputType().LoadInfo(objInputVoucher.InputTypeID);
                if (objInputType == null || objInputType.InvoiceTransTypeID < 1)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Chưa khai báo loại nghiệp vụ cho hình thức nhập mã: " + objInputVoucher.InputTypeID + "!");
                    return;
                }


                var objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                //ERP.SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrder objSaleOrder = new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().LoadInfo(objOutputVoucher.OrderID);
                //if (objSaleOrder != null)
                //{
                //var objSaleOrderType = new ERP.MasterData.PLC.MD.PLCSaleOrderType().LoadInfoFromCache(objSaleOrder.SaleOrderTypeID);
                //object[] objsKeyword = new object[]
                //{
                //"@SaleOrderTypeID", objSaleOrderType.SaleOrderTypeID,
                //"@StoreID", intInputChangeStoreID //objOutputVoucher.OutputStoreID
                //};
                if (objOutputVoucher.IsSInvoice)
                {
                    #region Hóa đơn điện tử trả hàng
                    ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
                    string strVATInvoiceID = string.Empty;
                    string strInvoiceNo = string.Empty;
                    string strTemplateCode = string.Empty;
                    PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVATInvoice = null;
                    var objResultMessage = objPLCPrintVAT.CreateVATSInvoiceInput(strInputVoucherID, objInputType.InvoiceTransTypeID,
                        objOutputVoucher.InvoiceSymbol.Trim() + objOutputVoucher.InvoiceID.Trim(), (DateTime)objOutputVoucher.InvoiceDate, ref objVATInvoice);
                    if (objResultMessage.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, objResultMessage.MessageDetail);
                        SystemErrorWS.Insert("Lỗi khi thêm mơi hóa đơn GTGT!", objResultMessage.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuCreateInvoiceVAT_Click");
                        return;
                    }
                   
                    PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                    objPrintVAT_PrepareData_Viettel.PrintVATSInvoice(this, objVATInvoice, true);
                    if (objVATInvoice != null)
                    {
                        objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, objVATInvoice, false, false);
                    }
                    #endregion
                }
                else
                {
                    #region Hóa đơn trả hàng luồng cũ
                    DataTable dtsFormType = null;//new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().GetFormTypebySaleOrderType(objsKeyword);
                    object[] objsKeyword = null;
                    objsKeyword = new object[]
                        {
                    "@STOREID", intInputChangeStoreID,
                    "@INVOICETRANSACTIONTYPE",objInputType.InvoiceTransTypeID
                        };
                    var objResultMessage = new PrintVAT.PLC.PrintVAT.PLCPrintVAT().SearchData_Invoiceno(ref dtsFormType, objsKeyword);
                    if (dtsFormType != null && dtsFormType.Rows.Count > 0)
                    {
                        int intFormTypeID = 0;
                        string strDenominator = string.Empty;
                        string strInvoiceSymbol1 = string.Empty;
                        int intBranchID = 0;
                        string strINVOICENO = string.Empty;
                        string strFormTypeBranchID = "0";
                        int.TryParse(dtsFormType.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                        int.TryParse(dtsFormType.Rows[0]["BRANCHID"].ToString(), out intBranchID);
                        strDenominator = dtsFormType.Rows[0]["Denominator"].ToString();
                        strInvoiceSymbol1 = dtsFormType.Rows[0]["InvoiceSymbol"].ToString();
                        strFormTypeBranchID = dtsFormType.Rows[0]["FormTypeBranchID"].ToString();
                        strINVOICENO = dtsFormType.Rows[0]["INVOICENO"].ToString();
                        ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                        objCustomer.CustomerID = objOutputVoucher.CustomerID;
                        objCustomer.CustomerName = objOutputVoucher.CustomerName;
                        objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                        objCustomer.CustomerPhone = objOutputVoucher.CustomerPhone;
                        objCustomer.TaxCustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.TaxCustomerAddress) ? objOutputVoucher.CustomerAddress : objOutputVoucher.TaxCustomerAddress;
                        objCustomer.CustomerAddress = objOutputVoucher.CustomerAddress;
                        objCustomer.CustomerTaxID = objOutputVoucher.CustomerTaxID;


                        DataTable dtbDataProduct = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_GETPRODUCT_INPUTVOUCHER",
                        new object[] { "@INPUTVOUCHERID",objInputVoucher.InputVoucherID,
                        "@STOREID",objInputVoucher.InputStoreID});


                        if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                        {
                            string strAddress = string.Empty;
                            string strTax = string.Empty;
                            var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                            if (objBranch.Count() > 0)
                            {
                                strAddress = objBranch[0]["TAXADDRESS"].ToString();
                                strTax = objBranch[0]["TAXCODE"].ToString();
                            }
                            string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                            PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                                dtbDataProduct, objCustomer, intBranchID, intInputChangeStoreID, strAddress, strTax, strDenominator,
                                strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 2, strFormTypeBranchID
                                , objInputVoucher.DiscountReasonID, objInputVoucher.Discount, objInputVoucher.StaffUser, strLstCustomerName);
                            if (objInputChangeOrderType != null)
                                objfrmInvoiceVAT.InputChangeOrderType = objInputChangeOrderType;
                            objfrmInvoiceVAT.intInvoiceTransactionTypeID = objInputType.InvoiceTransTypeID;
                            objfrmInvoiceVAT.ShowDialog();
                        }
                    }
                    else
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thiết lập số hóa đơn!");
                    }

                    #endregion
                }
                btnSearch_Click(null, null);
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi xem hóa đơn bán hàng", objEx.ToString(), this.Name + " -> mnuItemCreateInvoiceReturn_Click", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xem hóa đơn bán hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void mnuItemCreateInvoiceOutput_Click(object sender, EventArgs e)
        {
            try
            {
                if (flexData.RowSel < flexData.Rows.Fixed)
                    return;
                String strOutputVoucherID = flexData[flexData.RowSel, "OLDOUTPUTVOUCHERID"].ToString();
                int intInputChangeStoreID = Convert.ToInt32(flexData[flexData.RowSel, "INPUTCHANGEORDERSTOREID"]);
                int intInputChangeOrderTypeID = 0;
                ERP.MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType objInputChangeOrderType = null;
                try
                {
                    intInputChangeOrderTypeID = Convert.ToInt32(flexData[flexData.RowSel, "INPUTCHANGEORDERTYPEID"].ToString());
                    new ERP.MasterData.PLC.MD.PLCInputChangeOrderType().LoadInfo(ref objInputChangeOrderType, intInputChangeOrderTypeID);
                }
                catch
                {
                }

                var objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                ERP.SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrder objSaleOrder = new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().LoadInfo(objOutputVoucher.OrderID);
                //int intInvoiceNo = 0;
                //int.TryParse(objOutputVoucher.InvoiceID, out intInvoiceNo);

                string strOutputVoucheNewrID = flexData[flexData.RowSel, "NEWOUTPUTVOUCHERID"].ToString();
                var objOutputVoucherNew = objPLCOutputVoucher.LoadInfo(strOutputVoucheNewrID);
                if (objSaleOrder != null)
                {
                    var objSaleOrderType = new ERP.MasterData.PLC.MD.PLCSaleOrderType().LoadInfoFromCache(objSaleOrder.SaleOrderTypeID);
                    object[] objsKeyword = new object[]
                        {
                            "@STOREID", objOutputVoucher.OutputStoreID,
                            "@TYPE", 1,
                            "@SALEORDERTYPEID",objSaleOrderType.SaleOrderTypeID,
                            "@INVOICETRANSACTIONTYPE", objSaleOrderType.InvoiceTransTypeID
                        };
                    DataTable dtsFormType = null;// new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().GetFormTypebySaleOrderType(objsKeyword);
                    var objResultMessage = new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().SearchData_Invoiceno(ref dtsFormType, objsKeyword);
                    if (dtsFormType != null && dtsFormType.Rows.Count > 0)// && intInvoiceNo > 0)
                    {
                        int intFormTypeID = 0;
                        string strDenominator = string.Empty;
                        string strInvoiceSymbol1 = string.Empty;
                        int intBranchID = 0;
                        string strINVOICENO = string.Empty;
                        string strFormTypeBranchID = "0";
                        int.TryParse(dtsFormType.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                        int.TryParse(dtsFormType.Rows[0]["BRANCHID"].ToString(), out intBranchID);
                        strDenominator = dtsFormType.Rows[0]["Denominator"].ToString();
                        strInvoiceSymbol1 = dtsFormType.Rows[0]["InvoiceSymbol"].ToString();
                        strFormTypeBranchID = dtsFormType.Rows[0]["FormTypeBranchID"].ToString();
                        strINVOICENO = dtsFormType.Rows[0]["INVOICENO"].ToString();
                        ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                        objCustomer.CustomerID = objOutputVoucher.CustomerID;
                        objCustomer.CustomerName = objOutputVoucher.CustomerName;
                        objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                        objCustomer.CustomerPhone = objOutputVoucher.CustomerPhone;
                        objCustomer.TaxCustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.TaxCustomerAddress) ? objOutputVoucher.CustomerAddress : objOutputVoucher.TaxCustomerAddress;
                        objCustomer.CustomerAddress = objOutputVoucher.CustomerAddress;
                        objCustomer.CustomerTaxID = objOutputVoucher.CustomerTaxID;

                        strOutputVoucherID = flexData[flexData.RowSel, "NEWOUTPUTVOUCHERID"].ToString();
                        objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                        DataTable dtbDataProduct = null;
                        object[] objsKeyword1 = new object[]
                    {
                        "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                        "@StoreID", objOutputVoucher.OutputStoreID
                    };
                        new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                        if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                        {
                            string strAddress = string.Empty;
                            string strTax = string.Empty;
                            var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                            if (objBranch.Count() > 0)
                            {
                                strAddress = objBranch[0]["TAXADDRESS"].ToString();
                                strTax = objBranch[0]["TAXCODE"].ToString();
                            }
                            string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                            PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                                dtbDataProduct, objCustomer, intBranchID, objOutputVoucher.OutputStoreID, strAddress, strTax, strDenominator,
                                strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 1, strFormTypeBranchID
                                , objOutputVoucher.DiscountReasonID, objOutputVoucher.Discount, objOutputVoucher.StaffUser, strLstCustomerName);
                            //objfrmInvoiceVAT.SaleOrderType = objSaleOrderType;
                            objfrmInvoiceVAT.intInvoiceTransactionTypeID = objInputChangeOrderType.InvoiceTransTypeID;
                            objfrmInvoiceVAT.ShowDialog();
                        }
                    }
                    else
                    {
                        DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
                        var drStore = dtbStore.Select("STOREID=" + objOutputVoucher.OutputStoreID);
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất: " + objOutputVoucher.OutputStoreID + " - " + drStore[0]["STORENAME"].ToString() + "!");
                        //if (intInvoiceNo > 0)
                        //{
                        //    DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
                        //    var drStore = dtbStore.Select("STOREID=" + objOutputVoucher.OutputStoreID);
                        //    if (drStore.Any())
                        //    {
                        //        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất: " + objOutputVoucher.OutputStoreID + " - " + drStore[0]["STORENAME"].ToString() + "!");
                        //    }
                        //    else
                        //    {
                        //        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất!");
                        //    }
                        //}
                        //else
                        //{
                        //    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Hóa đơn bán hàng chưa được tạo không thể tạo hóa đơn xuất đổi trả!");
                        //}
                    }
                }
                else
                {
                    object[] objsKeyword = null;
                    objsKeyword = new object[]
                    {
                    "@STOREID", objOutputVoucherNew.OutputStoreID,
                    "@INVOICETRANSACTIONTYPE",objInputChangeOrderType.InvoiceTransTypeID
                    };
                    DataTable dtsFormType = null;//new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().GetFormTypebySaleOrderType(objsKeyword);
                    var objResultMessage = new PrintVAT.PLC.PrintVAT.PLCPrintVAT().SearchData_Invoiceno(ref dtsFormType, objsKeyword);
                    if (dtsFormType != null && dtsFormType.Rows.Count > 0)// && intInvoiceNo > 0)
                    {
                        int intFormTypeID = 0;
                        string strDenominator = string.Empty;
                        string strInvoiceSymbol1 = string.Empty;
                        int intBranchID = 0;
                        string strINVOICENO = string.Empty;
                        string strFormTypeBranchID = "0";
                        int.TryParse(dtsFormType.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                        int.TryParse(dtsFormType.Rows[0]["BRANCHID"].ToString(), out intBranchID);
                        strDenominator = dtsFormType.Rows[0]["Denominator"].ToString();
                        strInvoiceSymbol1 = dtsFormType.Rows[0]["InvoiceSymbol"].ToString();
                        strFormTypeBranchID = dtsFormType.Rows[0]["FormTypeBranchID"].ToString();
                        strINVOICENO = dtsFormType.Rows[0]["INVOICENO"].ToString();
                        ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                        objCustomer.CustomerID = objOutputVoucher.CustomerID;
                        objCustomer.CustomerName = objOutputVoucher.CustomerName;
                        objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                        objCustomer.CustomerPhone = objOutputVoucher.CustomerPhone;
                        objCustomer.TaxCustomerAddress = objOutputVoucher.CustomerAddress;
                        objCustomer.CustomerAddress = objOutputVoucher.CustomerAddress;
                        objCustomer.CustomerTaxID = objOutputVoucher.CustomerTaxID;

                        strOutputVoucherID = flexData[flexData.RowSel, "NEWOUTPUTVOUCHERID"].ToString();
                        objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                        DataTable dtbDataProduct = null;
                        object[] objsKeyword1 = new object[]
                    {
                        "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                        "@StoreID", objOutputVoucher.OutputStoreID
                    };
                        new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                        if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                        {
                            string strAddress = string.Empty;
                            string strTax = string.Empty;
                            var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                            if (objBranch.Count() > 0)
                            {
                                strAddress = objBranch[0]["TAXADDRESS"].ToString();
                                strTax = objBranch[0]["TAXCODE"].ToString();
                            }
                            string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                            PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                                dtbDataProduct, objCustomer, intBranchID, objOutputVoucher.OutputStoreID, strAddress, strTax, strDenominator,
                                strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 1, strFormTypeBranchID
                                , objOutputVoucher.DiscountReasonID, objOutputVoucher.Discount, objOutputVoucher.StaffUser, strLstCustomerName);
                            //objfrmInvoiceVAT.SaleOrderType = objSaleOrderType;
                            objfrmInvoiceVAT.intInvoiceTransactionTypeID = objInputChangeOrderType.InvoiceTransTypeID;
                            objfrmInvoiceVAT.ShowDialog();
                        }
                    }
                    else
                    {
                        DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
                        var drStore = dtbStore.Select("STOREID=" + objOutputVoucher.OutputStoreID);
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất: " + objOutputVoucher.OutputStoreID + " - " + drStore[0]["STORENAME"].ToString() + "!");

                        //if (intInvoiceNo > 0)
                        //{
                        //    DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
                        //    var drStore = dtbStore.Select("STOREID=" + objOutputVoucher.OutputStoreID);
                        //    if (drStore.Any())
                        //    {
                        //        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất: " + objOutputVoucher.OutputStoreID + " - " + drStore[0]["STORENAME"].ToString() + "!");
                        //    }
                        //    else
                        //    {
                        //        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất!");
                        //    }
                        //}
                        //else
                        //{
                        //    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Hóa đơn bán hàng chưa được tạo không thể tạo hóa đơn xuất đổi trả!");
                        //}
                    }
                }
                btnSearch_Click(null, null);
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi xem hóa đơn bán hàng", objEx.ToString(), this.Name + " -> mnuItemCreateInvoiceReturn_Click", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xem hóa đơn bán hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }
    }
}
