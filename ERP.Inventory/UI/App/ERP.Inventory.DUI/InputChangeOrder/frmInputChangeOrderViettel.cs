﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Library.AppCore;
using Library.AppCore.LoadControls;
using Library.AppCore.CustomControls.C1FlexGrid;
using System.IO;
using ERP.Inventory.PLC;
using ERP.Inventory.PLC.InputChangeOrder;
using System.Globalization;
using ERP.SaleProgram.PLC;
using ERP.SaleProgram.PLC.WSVTT_VERIFIEDPARTNER;
using ERP.SalesAndServices.SaleOrders.DUI;
using ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder;
using ERP.MasterData.PLC.MD.WSCustomer;
using ERP.SalesAndServices.Payment.DUI;
using ERP.Inventory.DUI.FIFO;

namespace ERP.Inventory.DUI.InputChangeOrder
{
    /// <summary>
    /// Created by: Nguyễn Văn Tài
    /// Created date: 07/07/2017
    /// Desc: Yêu cầu nhập đổi/trả hàng
    /// </summary>
    public partial class frmInputChangeOrderViettel : Form
    {

        #region FormType
        public enum FormStateType
        {
            VIEW,
            ADD,
            EDIT
        }
        private FormStateType intFormState = FormStateType.VIEW;
        public FormStateType FormState
        {
            set
            {
                intFormState = value;
            }
            get
            {
                return intFormState;
            }
        }
        #endregion
        public frmInputChangeOrderViettel()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        public frmInputChangeOrderViettel(int intInputChangeOrderTypeID)
        {
            InitializeComponent();
            this.intInputChangeOrderTypeID = intInputChangeOrderTypeID;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        public frmInputChangeOrderViettel(int intInputChangeOrderTypeID, string strInputChangeOrderID, bool bolIsDeleted, bool bolIsAllowEdit)
        {
            InitializeComponent();
            this.intInputChangeOrderTypeID = intInputChangeOrderTypeID;
            this.strInputChangeOrderID = strInputChangeOrderID;
            this.bolIsDeleted = bolIsDeleted;
            this.bolIsAllowEdit = bolIsAllowEdit;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        #region 
        //LE VAN LONG
        private string strvtplusPointTs = string.Empty;
        //
        public string strIMEIInputChange = string.Empty;
        private int intInputChangeOrderTypeID = 0;
        private string strInputChangeOrderID = string.Empty;
        private bool bolIsDeleted = false;
        private MasterData.PLC.MD.PLCSaleOrderType objPLCSaleOrderType = new MasterData.PLC.MD.PLCSaleOrderType();
        private SalesAndServices.SaleOrders.PLC.PLCSaleOrders objPLCSaleOrders = new SalesAndServices.SaleOrders.PLC.PLCSaleOrders();
        private PLC.InputChangeOrder.PLCInputChangeOrder objPLCInputChangeOrder = new PLC.InputChangeOrder.PLCInputChangeOrder();
        private MasterData.PLC.MD.PLCInputChangeOrderType objPLCInputChangeOrderType = new MasterData.PLC.MD.PLCInputChangeOrderType();
        private MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType objInputChangeOrderType = null;
        //Thoong tin ve trang thai sp
        private List<MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType_PS> lstInputChangeOrderType_PS = null;//Danh sách trạng thái sản phẩm theo loại y/c đổi trả
        private Hashtable hstbInstockStatus = null;
        //End
        private DataTable dtbMachineErrorList = null;
        private PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder objInputChangeOrder = null;
        private const string strFMSAplication = "FMSAplication_ProERP_InputChangeOrderAttachment";
        private List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment> lstInputChangeOrder_Attachment = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment>();
        private List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment> lstInputChangeOrder_Attachment_Del = null;
        private StringBuilder stbAttachmentLog = new StringBuilder();
        public string strOutputVoucherID = string.Empty;
        private string strOldOutputVoucherID = string.Empty;
        public event EventHandler evtCloseParentForm = null;
        public int intReportID = 0;
        private PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
        private PLC.Input.PLCInputVoucher objPLCInputVoucher = new PLC.Input.PLCInputVoucher();
        private PLC.Output.WSOutputVoucher.OutputVoucher objOutputVoucherLoad = new PLC.Output.WSOutputVoucher.OutputVoucher();
        private PLCProductInStock objPLCProductInStock = new PLCProductInStock();
        private SalesAndServices.Payment.PLC.PLCVoucher objPLCVoucher = new SalesAndServices.Payment.PLC.PLCVoucher();
        private int intReviewLevelCurrent = -1;
        private int intReviewStatus = -1;
        private int intWorkFlowCurrent = -1;
        private bool bolIsFinalLevel = false;
        private bool bolIsHasAction = false;
        private DataRow rOldEdit = null;
        private int intOldStoreID = -1;
        private DataTable dtbOVDetailReturn = null;
        private DataTable dtbInputTypeAll = null;
        private DataTable dtbOutputTypeAll = null;
        private DataTable tblInputChangeReasonAll = null;
        private bool bolIsLockByClosingDataMonth = false;
        private PLC.ProductChange.PLCProductChange objPLCProductChange = new PLC.ProductChange.PLCProductChange();
        private Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private decimal decVNDCash = 0;
        private decimal decVNDCashDeposit = 0;
        private decimal decVNDCash_In = 0;
        private decimal decVNDCash_Out = 0;
        private decimal decPaymentCardAmount = 0;
        private decimal decPaymentCardAmountDeposit = 0;
        private decimal decPaymentCardAmount_In = 0;
        private decimal decPaymentCardAmount_Out = 0;
        private int intPaymentCardID = 0;
        private int intPaymentCardIDDeposit = 0;
        private int intPaymentCardID_In = 0;
        private int intPaymentCardID_Out = 0;
        private decimal decPaymentCardSpend = 0;
        private decimal decPaymentCardSpendDeposit = 0;
        private decimal decPaymentCardSpend_In = 0;
        private decimal decPaymentCardSpend_Out = 0;
        private string strPaymentCardVoucherID = string.Empty;
        private string strPaymentCardVoucherIDDeposit = string.Empty;
        private string strPaymentCardVoucherID_In = string.Empty;
        private string strPaymentCardVoucherID_Out = string.Empty;
        private string strPaymentCardName_In = string.Empty;
        private string strPaymentCardName = string.Empty;
        private string strPaymentCardNameDeposit = string.Empty;
        private decimal decRefundAmount_In = 0;
        private SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrder objSaleOrder_Out = null;
        private string strMessageCloseForm = string.Empty;
        private int intClosedForm = 0;
        private bool bolIsHasPermission_Create = false;
        private bool bolIsHasPermission_Edit = false;
        private bool bolIsHasPermission_EditAll = false;
        private bool bolIsHasPermission_Delete = false;
        private bool bolIsHasPermission_DeleteAll = false;
        private bool bolIsHasPermission_AddAttachment = false;
        private bool bolIsHasPermission_DelAttachment = false;
        private bool bolIsHasPermission_ViewAttachment = false;
        private bool bolIsHasPermission_AfterEditFunction = false;
        private bool bolIsHasPermission_AfterEditLimitFunction = false;
        private bool bolIsHasPermission_ViewInvoice = false;
        private bool bolIsHasPermission_UpdateCare = false;
        private bool bolIsAllowEdit = false;
        private string strRightUpdateCare = "PM_INPUTCHANGEORDER_UPDCARE";
        private string strMessageForm = "nhập đổi trả hàng";
        private decimal decTotalPaid = 0;
        private decimal decOldVoucherDebt = 0;
        private string strInVoucherID = string.Empty;
        private bool bolIsDebt = false;
        private DataTable dtbDetailAll = null;
        private bool bolIsNotCreateOutVoucherDetail = false;
        private bool bolApplyErrorType = false;
        private List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail> lstAddVoucherDetail = new List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail>();
        private List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail> lstAddVoucherDetail_In = new List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail>();
        private bool bolSpecialCase = false;
        private decimal decReturnAmount = 0;
        //  private decimal decReturnAmountDeposit = 0;
        private List<string> lstIMEi = new List<string>();
        private string strInputChangeOrderTypeIdSub = string.Empty;

        private List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail> lstTemp = new List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail>();
        private ERP.SalesAndServices.Payment.PLC.WSVoucher.Voucher objVoucherDepositSpend = null;
        private ERP.SalesAndServices.Payment.PLC.WSVoucher.Voucher objVoucherDeposit = null;
        private ERP.SalesAndServices.Payment.PLC.WSVoucher.ResultMessage objResultMessageVoucher = null;
        private List<string> lstIMEIUpdateConcern = new List<string>();
        public bool ApplyErrorType
        {
            set { bolApplyErrorType = value; }
        }
        public bool IsHasAction
        {
            get { return bolIsHasAction; }
        }
        private string strVatInvoiceId = string.Empty;
        private const String strPermission_View = "VAT_INVOICE_VIEW";
        private string strInputVoucherId = string.Empty;
        private string strInputVoucherReturnId = string.Empty;
        private string strProductNameContent = string.Empty;
        private PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVatInvoiceInputVoucher = null;
        private PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVatInvoiceOutputVoucher = null;
        private string strInvoiceId = string.Empty;
        private int intVatPagesPrint = 3;

        private bool bolIsCreateVoucherReturnFee = false;
        private bool bolIsPaymentCard = false;
        private bool bolIsExceptionSpend = false;
        private decimal decExceptionSpend = 0;
        private bool bolIsReload = false;

        private bool IsOnlineOrder = false;
        // private bool 
        #endregion

        #region method
        bool bolAlowUpdateAfter = false;
        private bool LoadOutputVoucher(ref string strMessWarning)
        {
            try
            {
                if (strOutputVoucherID != string.Empty)
                {
                    objOutputVoucherLoad = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                    if (objOutputVoucherLoad != null)
                    {
                        //Lấy số tiền đã thu trong chi tiết phiếu thu
                        strInVoucherID = objPLCOutputVoucher.GetTotalPaidByOrderID(ref decTotalPaid, ref decOldVoucherDebt, objOutputVoucherLoad.OrderID, objOutputVoucherLoad.OutputVoucherID);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }

                        //Lấy chi tiết ban đầu của phiếu xuất(bao gồm luôn sp đã trả nếu có)
                        dtbDetailAll = objPLCOutputVoucher.GetAll(objOutputVoucherLoad.OutputVoucherID);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                        txtOutputVoucherID.Text = objOutputVoucherLoad.OutputVoucherID;
                        dtOutputDate.Value = objOutputVoucherLoad.OutputDate.Value;
                        try
                        {
                            bolIsLockByClosingDataMonth = ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.OUTPUTVOUCHER,
                                objOutputVoucherLoad.OutputDate.Value, objOutputVoucherLoad.OutputStoreID);
                        }
                        catch { }
                        if (strInvoiceId != string.Empty)
                        {
                            txtInVoiceID.Text = strInvoiceId;
                        }
                        else
                            txtInVoiceID.Text = objOutputVoucherLoad.InvoiceID;
                        txtInvoiceSymbol.Text = objOutputVoucherLoad.InvoiceSymbol;
                        ctrlStaffUser.UserName = objOutputVoucherLoad.StaffUser;
                        txtDenominator.Text = objOutputVoucherLoad.Denominator;
                        txtStaffUser.Text = ctrlStaffUser.UserName + " - " + ctrlStaffUser.FullName;
                        strVatInvoiceId = objOutputVoucherLoad.InvoiceID;


                        cboStore.SetValue(SystemConfig.intDefaultStoreID);
                        if (cboStore.StoreID < 1)
                            cboStore.SetValue(objOutputVoucherLoad.OutputStoreID);
                        if (cboStore.StoreID < 1 && ((DataTable)(cboStore.DataSource)).Rows.Count > 0)
                        {
                            cboStore.SetIndex(0);
                        }
                        intOldStoreID = cboStore.StoreID;

                        txtCustomerID.Text = objOutputVoucherLoad.CustomerID.ToString();
                        txtCustomerName.Text = objOutputVoucherLoad.CustomerName;
                        txtCustomerAddress.Text = objOutputVoucherLoad.CustomerAddress;
                        txtCustomerPhone.Text = objOutputVoucherLoad.CustomerPhone;
                        txtTaxID.Text = objOutputVoucherLoad.CustomerTaxID;
                        txtContentOV.Text = objOutputVoucherLoad.OutputContent;

                        if (strInputChangeOrderID == string.Empty)
                        {
                            dtbOVDetailReturn = objPLCInputChangeOrder.LoadOutputVoucherDetailForReturn(objOutputVoucherLoad.OutputVoucherID, intInputChangeOrderTypeID);
                            if (!string.IsNullOrEmpty(strInputChangeOrderTypeIdSub) && dtbOVDetailReturn != null && dtbOVDetailReturn.Rows.Count > 0)
                            {
                                List<int> lstInputChangeOrderId = strInputChangeOrderTypeIdSub.Split(',').Select(int.Parse).ToList();
                                if (lstInputChangeOrderId.Any(x => x == objInputChangeOrderType.InputChangeOrderTypeID)) //chọn sub
                                {
                                    //   var a = dtbOVDetailReturn.AsEnumerable().Where(x => ( !string.IsNullOrEmpty(x.Field<object>("IMEI").ToString()) && CheckSubsidy(x.Field<object>("IMEI").ToString())));
                                    var ArrRow = dtbOVDetailReturn.AsEnumerable().Where(x => (x.Field<object>("IMEI") != null && !string.IsNullOrEmpty(x.Field<object>("IMEI").ToString()) && CheckSubsidy(x.Field<object>("IMEI").ToString())));
                                    if (ArrRow == null || ArrRow.Count() < 1)
                                        dtbOVDetailReturn.Rows.Clear();
                                    else
                                        dtbOVDetailReturn = ArrRow.CopyToDataTable();
                                }
                                else
                                {
                                    var ArrRow = dtbOVDetailReturn.AsEnumerable().Where(x => (x.Field<object>("IMEI") == null || string.IsNullOrEmpty(x.Field<object>("IMEI").ToString()) || !CheckSubsidy(x.Field<object>("IMEI").ToString())));
                                    if (ArrRow == null || ArrRow.Count() < 1)
                                        dtbOVDetailReturn.Rows.Clear();
                                    else
                                        dtbOVDetailReturn = ArrRow.CopyToDataTable();
                                }
                            }
                            if (dtbOVDetailReturn.AsEnumerable().Any(x => Convert.ToBoolean(x.Field<object>("ISONLINEORDER")) == true))
                                IsOnlineOrder = true;
                            if (objInputChangeOrderType.ReturnFeeBook == 2)
                            {
                                for (int i = 0; i < dtbOVDetailReturn.Rows.Count; i++)
                                {
                                    DataRow row = dtbOVDetailReturn.Rows[i];
                                    row["TOTALCOST"] = Convert.ToDecimal(row["TOTALCOST"]) + Convert.ToDecimal(row["TOTALFEE"]);
                                    row["InputSalePrice"] = Convert.ToDecimal(row["SalePrice"]);
                                }
                                dtbOVDetailReturn.AcceptChanges();
                            }
                            if (!string.IsNullOrWhiteSpace(strIMEIInputChange))
                            {
                                if (dtbOVDetailReturn != null && dtbOVDetailReturn.Rows.Count > 0)
                                {
                                    DataRow[] rows = dtbOVDetailReturn.AsEnumerable()
                                        .Where(r => !r.IsNull("IMEI") && (r.Field<string>("IMEI")).Trim() == strIMEIInputChange).ToArray();
                                    if (rows == null || rows.Count() == 0)
                                    {
                                        dtbOVDetailReturn.Rows.Clear();
                                    }
                                    else
                                    {
                                        #region Lấy thêm sản phẩm KM áp dụng cho IMEI (trường hợp buộc trả sản phẩm KM)

                                        if (Convert.ToInt32(rows[0]["ISRETURNPROMOTION"]) == 1)
                                        {
                                            DataTable dtbOVDetailReturnTemp = dtbOVDetailReturn.Copy();
                                            dtbOVDetailReturn = rows.CopyToDataTable();
                                            foreach (DataRow rowPromotionProduct in dtbOVDetailReturnTemp.Rows)
                                            {
                                                if (rowPromotionProduct["APPLYSALEORDERDETAILID"].ToString().Trim() == rows[0]["SALEORDERDETAILID"].ToString().Trim())
                                                {
                                                    dtbOVDetailReturn.Rows.Add(rowPromotionProduct.ItemArray);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            dtbOVDetailReturn = rows.CopyToDataTable();
                                        }

                                        #endregion

                                    }
                                }
                            }
                            dtbOVDetailReturn.DefaultView.Sort = "SalePrice DESC";
                            dtbOVDetailReturn = dtbOVDetailReturn.DefaultView.ToTable();

                            if (objInputChangeOrderType.IsNew == 1 || objInputChangeOrderType.IsNew == 0)
                            {
                                dtbOVDetailReturn.DefaultView.RowFilter = "IsNew = " + objInputChangeOrderType.IsNew;
                                dtbOVDetailReturn = dtbOVDetailReturn.DefaultView.ToTable();
                            }

                            string strOutputTypeFilter = string.Empty;
                            List<MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType_OType> LstInputChangeOrderType_OType = new List<MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType_OType>();
                            objPLCInputChangeOrderType.LoadInfo_OType(ref LstInputChangeOrderType_OType, objInputChangeOrderType.InputChangeOrderTypeID);

                            for (int i = 0; i < LstInputChangeOrderType_OType.Count; i++)
                            {
                                if (Convert.ToBoolean(LstInputChangeOrderType_OType[i].IsSelect))
                                    strOutputTypeFilter += LstInputChangeOrderType_OType[i].OutputTypeID.ToString() + ",";
                            }
                            strOutputTypeFilter = strOutputTypeFilter.Trim(',');
                            if (strOutputTypeFilter != string.Empty)
                            {
                                dtbOVDetailReturn.DefaultView.RowFilter = "IsPromotionOutputType = True OR OutputTypeID IN(" + strOutputTypeFilter + ")";
                                dtbOVDetailReturn = dtbOVDetailReturn.DefaultView.ToTable();
                            }

                            if (!dtbOVDetailReturn.Columns.Contains("IsSelect"))
                            {
                                dtbOVDetailReturn.Columns.Add("IsSelect", typeof(bool));
                                dtbOVDetailReturn.Columns["IsSelect"].DefaultValue = false;
                                dtbOVDetailReturn.Columns["IsSelect"].SetOrdinal(0);
                            }
                            int intOrdinal = dtbOVDetailReturn.Columns["Endwarrantydate"].Ordinal + 2;

                            dtbOVDetailReturn.Columns.Add("InputPrice_In", typeof(decimal));
                            dtbOVDetailReturn.Columns.Add("INSTOCKSTATUSID_IN", typeof(decimal));
                            dtbOVDetailReturn.Columns.Add("INSTOCKSTATUSID_OUT", typeof(decimal));
                            dtbOVDetailReturn.Columns.Add("OutputTypeID_Out", typeof(int));
                            dtbOVDetailReturn.Columns.Add("OutputTypeName_Out", typeof(string));
                            dtbOVDetailReturn.Columns.Add("ProductID_Out", typeof(string));
                            dtbOVDetailReturn.Columns.Add("ProductName_Out", typeof(string));
                            dtbOVDetailReturn.Columns.Add("IMEI_Out", typeof(string));
                            dtbOVDetailReturn.Columns.Add("SalePrice_Out", typeof(decimal));
                            // dtbOVDetailReturn.Columns.Add("SalePrice_Out", typeof(decimal));

                            dtbOVDetailReturn.Columns["SalePrice_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["INSTOCKSTATUSID_OUT"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["IMEI_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["ProductName_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["ProductID_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["OutputTypeName_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["InputPrice_In"].SetOrdinal(intOrdinal - 2);
                            dtbOVDetailReturn.Columns["INSTOCKSTATUSID_IN"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns.Add("IsRequestIMEI_Out", typeof(bool));
                            dtbOVDetailReturn.Columns["IsRequestIMEI_Out"].DefaultValue = false;
                            //Nguyen Van Tai bo sung theo yeu cau BA Trung - 30/11/2017
                            dtbOVDetailReturn.Columns["MACHINEERRORNAME"].SetOrdinal(intOrdinal + 1);
                            dtbOVDetailReturn.Columns["MACHINEERRORGROUPNAME"].SetOrdinal(intOrdinal + 1);
                            //End
                            dtbOVDetailReturn.Columns["TOTALFEE"].SetOrdinal(intOrdinal - 3);
                            if (!dtbOVDetailReturn.Columns.Contains("Quantity2"))
                                dtbOVDetailReturn.Columns.Add("Quantity2", typeof(decimal));
                            if (!dtbOVDetailReturn.Columns.Contains("TotalCost2"))
                                dtbOVDetailReturn.Columns.Add("TotalCost2", typeof(decimal));
                            if (!dtbOVDetailReturn.Columns.Contains("TotalFee2"))
                                dtbOVDetailReturn.Columns.Add("TotalFee2", typeof(decimal));

                            for (int i = dtbOVDetailReturn.Rows.Count - 1; i > -1; i--)
                            {
                                bool IsSelect = false;
                                if (dtbOVDetailReturn.Rows.Count < 2) IsSelect = true;
                                DataRow rDetail = dtbOVDetailReturn.Rows[i];
                                rDetail["IsSelect"] = IsSelect;
                                rDetail["IsRequestIMEI_Out"] = false;
                                rDetail["Quantity2"] = rDetail["Quantity"];
                                rDetail["TotalCost2"] = rDetail["TotalCost"];
                                rDetail["TotalFee2"] = rDetail["TotalFee"];
                                int intInstockStatusID_Source = Convert.ToInt32(rDetail["InstockStatusID"]);
                                //Nếu là thay đổi trạng thái sp thì lấy trạng thái đích.
                                if (objInputChangeOrderType.IsChangeStatus)
                                {
                                    if (lstInputChangeOrderType_PS != null && lstInputChangeOrderType_PS.Count > 0)
                                    {
                                        var obj = from o in lstInputChangeOrderType_PS
                                                  where o.ProductStatusIDFrom == intInstockStatusID_Source
                                                  select o;
                                        if (obj.Count() > 0)
                                        {
                                            rDetail["INSTOCKSTATUSID_IN"] = obj.First().ProductStatusIDTo;
                                        }
                                    }
                                    else
                                    {
                                        strMessWarning = "Không có định nghĩa trạng thái sản phẩm áp dụng nhập đổi/ trả hàng";
                                        return false;
                                    }
                                }
                                else // ngược lại lấy trạng thái nguồn làm trạng thái đích
                                    rDetail["INSTOCKSTATUSID_IN"] = intInstockStatusID_Source;

                                int intOutputType_Out = Convert.ToInt32(rDetail["OutputTypeID"]);
                                if (objInputChangeOrderType.IsInputChangeFree)
                                {
                                    rDetail["ReturnInputTypeID"] = objInputChangeOrderType.InputTypeID;
                                    if ((!objInputChangeOrderType.IsInputReturn) && objInputChangeOrderType.OutputTypeID > 0)
                                        intOutputType_Out = objInputChangeOrderType.OutputTypeID;
                                }
                                int intReturnInputTypeID = Convert.ToInt32(rDetail["ReturnInputTypeID"]);
                                ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(intOutputType_Out);
                                if (objOutputType_Out == null)
                                {
                                    flexDetail.Enabled = false;
                                    EnableControl(false);
                                    strMessWarning = "Bạn không có quyền trên hình thức xuất " + (objInputChangeOrderType.IsInputChangeFree == false ? rDetail["OutputTypeName"].ToString() : (objInputChangeOrderType.IsInputReturn == false ? " khi đổi trả không tính phí" : string.Empty)) + ". Vui lòng kiểm tra lại.";
                                    return false;
                                }

                                rDetail["OutputTypeID_Out"] = objOutputType_Out.OutputTypeID.ToString();
                                rDetail["OutputTypeName_Out"] = objOutputType_Out.OutputTypeName;

                                DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + intReturnInputTypeID);
                                if (drReturnInputType.Length < 1)
                                {
                                    flexDetail.Enabled = false;
                                    EnableControl(false);
                                    strMessWarning = "Bạn không có quyền trên hình thức nhập của hình thức xuất [" + ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(rDetail["OutputTypeID"])).OutputTypeName + "]. Vui lòng kiểm tra lại.";
                                    return false;
                                }
                                rDetail["InputPrice_In"] = Convert.ToDecimal(rDetail["InputSalePrice"]);
                                if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 2)
                                {
                                    rDetail["InputPrice_In"] = Convert.ToDecimal(rDetail["InputCostPrice"]);
                                    if (objInputChangeOrderType.IsInputReturn)
                                    {
                                        rDetail["SalePrice"] = rDetail["CostPrice"];
                                        rDetail["TotalCost"] = rDetail["TotalCostPrice"];
                                        rDetail["TotalCost2"] = rDetail["TotalCostPrice"];
                                    }
                                }
                                else if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 3)
                                    rDetail["InputPrice_In"] = 0;
                                //xét HT nhập có lấy VAT
                                if (!Convert.ToBoolean(drReturnInputType[0]["IsVATZero"]))
                                    rDetail["InputPrice_In"] = Convert.ToDecimal(rDetail["InputPrice_In"]) * (Convert.ToDecimal(1) + Convert.ToDecimal(rDetail["VAT"]) * Convert.ToDecimal(rDetail["VATPercent"]) * Convert.ToDecimal(0.0001));

                                rDetail["InputTypeName"] = drReturnInputType[0]["InputTypeName"];
                            }

                            if (dtbOVDetailReturn.Rows.Count < 1)
                            {
                                flexDetail.Enabled = false;
                                EnableControl(false);
                                if (!string.IsNullOrWhiteSpace(strIMEIInputChange))
                                {
                                    strMessWarning = "IMEI " + strIMEIInputChange + " không hợp lệ để tạo yêu cầu " + strMessageForm + ". Vui lòng kiểm tra lại.";
                                }
                                else
                                {
                                    strMessWarning = "Phiếu xuất " + strOutputVoucherID + " không có sản phẩm(hoặc IMEI) nào để tạo yêu cầu " + strMessageForm + ". Vui lòng kiểm tra lại.";
                                }
                                return false;
                            }
                            flexDetail.DataSource = dtbOVDetailReturn;

                            if (!objInputChangeOrderType.IsInputReturn)
                                FormatFlex();
                            else
                                FormatFlexReturn();
                        }
                        else
                        {
                            dtbOVDetailReturn = objPLCInputChangeOrder.GetDetail(new object[] { "@InputChangeOrderID", strInputChangeOrderID, "@IncludeDeleted", bolIsDeleted });
                            if (dtbOVDetailReturn.Rows.Count < 1)
                            {
                                flexDetail.Enabled = false;
                                EnableControl(false);
                                strMessWarning = "Phiếu xuất " + strOutputVoucherID + " không có sản phẩm(hoặc IMEI) nào để tạo yêu cầu " + strMessageForm + ". Vui lòng kiểm tra lại.";
                                return false;
                            }

                            for (int i = 0; i < dtbOVDetailReturn.Rows.Count; i++)
                            {
                                DataRow rReturn = dtbOVDetailReturn.Rows[i];
                                ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = null;
                                if (!objInputChangeOrderType.IsInputReturn)
                                {
                                    if (string.IsNullOrEmpty(txtSaleOrderID.Text))
                                    {
                                        if (objInputChangeOrderType.IsInputChangeFree)
                                        {
                                            objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(objInputChangeOrderType.OutputTypeID);
                                            if (objOutputType_Out != null)
                                            {
                                                rReturn["OutputTypeName_Out"] = objOutputType_Out.OutputTypeName;
                                                rReturn["OutputTypeID_Out"] = objOutputType_Out.OutputTypeID;
                                            }
                                            else
                                            {
                                                flexDetail.Enabled = false;
                                                EnableControl(false);
                                                strMessWarning = "Bạn không có quyền trên hình thức xuất mã [" + objInputChangeOrderType.OutputTypeID.ToString() + "] khi đổi trả không tính phí. Vui lòng kiểm tra lại.";
                                                return false;
                                            }
                                        }
                                        else
                                            objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(rReturn["OutputTypeID_Out"]));
                                        if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                                        {
                                            ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rReturn["ProductID_Out"].ToString().Trim());
                                            rReturn["SalePrice_Out"] = Convert.ToDecimal(rReturn["SalePrice_Out"]) * (Convert.ToDecimal(1) + Convert.ToDecimal(objProductOut.VAT) * Convert.ToDecimal(objProductOut.VATPercent) * Convert.ToDecimal(0.0001));
                                        }
                                        else
                                        {
                                            if (objOutputType_Out == null)
                                            {
                                                flexDetail.Enabled = false;
                                                EnableControl(false);
                                                strMessWarning = "Bạn không có quyền trên hình thức xuất [" + rReturn["OutputTypeID_Out"].ToString() + " - " + rReturn["OutputTypeName_Out"].ToString() + "]. Vui lòng kiểm tra lại.";
                                                return false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        rReturn["OutputTypeID_Out"] = 0;
                                        rReturn["OutputTypeName_Out"] = string.Empty;
                                        rReturn["SalePrice_Out"] = 0;
                                    }
                                }
                                else
                                {
                                    rReturn["OutputTypeID_Out"] = 0;
                                    rReturn["OutputTypeName_Out"] = string.Empty;
                                    rReturn["SalePrice_Out"] = 0;
                                }

                                if (objInputChangeOrderType.IsInputChangeFree)
                                    rReturn["ReturnInputTypeID"] = objInputChangeOrderType.InputTypeID;

                                DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + Convert.ToInt32(rReturn["ReturnInputTypeID"]));
                                if (drReturnInputType.Length < 1)
                                {
                                    flexDetail.Enabled = false;
                                    EnableControl(false);
                                    strMessWarning = "Bạn không có quyền trên hình thức nhập của hình thức xuất [" + rReturn["OutputTypeID_Out"].ToString() + " - " + rReturn["OutputTypeName_Out"].ToString() + "]. Vui lòng kiểm tra lại.";
                                    return false;
                                }
                                //xét HT nhập có lấy VAT
                                if (!Convert.ToBoolean(drReturnInputType[0]["IsVATZero"]))
                                {
                                    if (Convert.ToDecimal(rReturn["Quantity"]) > 1)
                                        rReturn["InputPrice_In"] = Convert.ToDecimal(rReturn["InputPrice_In"]) * (Convert.ToDecimal(1) + Convert.ToDecimal(rReturn["VAT"]) * Convert.ToDecimal(rReturn["VATPercent"]) * Convert.ToDecimal(0.0001));
                                    else
                                        rReturn["InputPrice_In"] = Math.Round(Convert.ToDecimal(rReturn["InputPrice_In"]) * (Convert.ToDecimal(1) + Convert.ToDecimal(rReturn["VAT"]) * Convert.ToDecimal(rReturn["VATPercent"]) * Convert.ToDecimal(0.0001)));
                                }
                            }
                            flexDetail.DataSource = dtbOVDetailReturn;
                            flexDetail.AllowEditing = false;
                            FormatFlexDetail();
                            decimal decSumInputPrice = 0;
                            decimal decSumSalePrice = 0;
                            decimal decTotalFee = 0;
                            decimal decSumInputPriceTemp = 0;
                            DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select(string.Empty);
                            for (int i = 0; i < drDetail.Length; i++)
                            {
                                decTotalFee += Convert.ToDecimal(drDetail[i]["TotalFee"]);
                                decSumInputPriceTemp += Convert.ToDecimal(drDetail[i]["InputPrice_In"]) * Convert.ToDecimal(drDetail[i]["Quantity"]);
                            }
                            if (!objInputChangeOrder.IsInputChanged)
                            {
                                if (objInputChangeOrderType.ReturnFeeBook == 1)
                                {
                                    decSumInputPrice = GetCompute_In(string.Empty);
                                }
                                else
                                {
                                    decSumInputPrice = decSumInputPriceTemp - decTotalFee;
                                    if (objInputChangeOrderType.IsInputReturn && txtReturnAmount.Visible)
                                    {
                                        if (strInVoucherID != string.Empty)
                                        {
                                            if (decTotalPaid == 0)
                                            {
                                                decSumInputPrice = 0;
                                                bolIsDebt = true;
                                                bolIsNotCreateOutVoucherDetail = true;
                                            }
                                            else
                                            {
                                                if (Math.Round(decSumInputPriceTemp) > Math.Round(decTotalPaid))
                                                {
                                                    decSumInputPrice = decTotalPaid;
                                                    bolIsDebt = true;
                                                    DataTable dtbCheckDetailReturn1 = objPLCInputChangeOrder.GetListReturn(objOutputVoucherLoad.OutputVoucherID, objInputChangeOrder.InputChangeOrderID);
                                                    decimal decSumTotalAmount = Math.Abs(Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbCheckDetailReturn1.Compute("Sum(TotalAmount)", "Quantity < 0"), 0)));
                                                    decSumInputPrice = decSumInputPrice - decSumTotalAmount - decTotalFee;
                                                    if (decSumInputPrice <= 0)
                                                    {
                                                        //  bolIsDebt = true;
                                                        bolIsNotCreateOutVoucherDetail = true;
                                                        decSumInputPrice = 0;
                                                    }
                                                }
                                                else
                                                {

                                                    DataTable dtbCheckDetailReturn1 = objPLCInputChangeOrder.GetListReturn(objOutputVoucherLoad.OutputVoucherID, objInputChangeOrder.InputChangeOrderID);
                                                    decimal decSumTotalAmount = Math.Abs(Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbCheckDetailReturn1.Compute("Sum(TotalAmount)", "Quantity < 0"), 0)));
                                                    decimal decAmount = decTotalPaid - decSumTotalAmount;
                                                    if (Math.Round(decAmount) < Math.Round(decSumInputPrice))
                                                        decSumInputPrice = decAmount - decTotalFee;
                                                    if (decSumInputPrice <= 0)
                                                    {
                                                        bolIsDebt = true;
                                                        bolIsNotCreateOutVoucherDetail = true;
                                                        decSumInputPrice = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                decSumSalePrice = GetCompute_Out(string.Empty);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objInputChangeOrder.OutVoucherID))
                                {
                                    ERP.SalesAndServices.Payment.PLC.WSVoucher.Voucher objVoucher = objPLCVoucher.LoadInfo(objInputChangeOrder.OutVoucherID);
                                    if (objVoucher != null)
                                        decSumInputPrice = objVoucher.TotalLiquidate - objVoucher.Debt;
                                }
                                if (!string.IsNullOrEmpty(objInputChangeOrder.InVoucherID))
                                {
                                    ERP.SalesAndServices.Payment.PLC.WSVoucher.Voucher objVoucher = objPLCVoucher.LoadInfo(objInputChangeOrder.InVoucherID);
                                    if (objVoucher != null)
                                        decSumSalePrice = objVoucher.TotalLiquidate - objVoucher.Debt;
                                }
                            }



                            if (objInputChangeOrderType.ReturnFeeBook != 2 || (objInputChangeOrderType.ReturnFeeBook == 2 && !objInputChangeOrder.IsInputChanged))
                            {

                                if (Math.Round(decSumInputPrice) > Math.Round(decSumSalePrice))
                                {
                                    lblTitlePayment.Text = "Phải chi";
                                    decReturnAmount = Math.Round(decSumInputPrice - decSumSalePrice);
                                    txtReturnAmount.Text = decReturnAmount.ToString("#,##0");
                                }
                                else
                                {
                                    if (Math.Round(decSumInputPrice) == Math.Round(decSumSalePrice))
                                        lblTitlePayment.Text = "Phải chi";
                                    else
                                        lblTitlePayment.Text = "Phải thu";
                                    decReturnAmount = Math.Round(decSumSalePrice - decSumInputPrice);
                                    txtReturnAmount.Text = decReturnAmount.ToString("#,##0");
                                }
                                // }
                            }

                            else
                            {
                                //if (!string.IsNullOrEmpty(objInputChangeOrder.InVoucherID))
                                //{
                                //    lblTitlePayment.Text = "Phải thu";
                                //    txtReturnAmount.Text = (decSumInputPrice - decSumSalePrice + decTotalFee).ToString("#,##0");
                                //}
                                //else if (!string.IsNullOrEmpty(objInputChangeOrder.OutVoucherID))
                                //{
                                //    if (decTotalFee - (decSumInputPrice - decSumSalePrice) > 0)
                                //    {
                                //        lblTitlePayment.Text = "Phải thu";
                                //        txtReturnAmount.Text = (decTotalFee - (decSumInputPrice - decSumSalePrice)).ToString("#,##0");
                                //    }
                                //    else if (decTotalFee - (decSumInputPrice - decSumSalePrice) < 0)
                                //    {
                                //        lblTitlePayment.Text = "Phải chi";
                                //        txtReturnAmount.Text = ((decTotalFee - (decSumInputPrice - decSumSalePrice)) * -1).ToString("#,##0");
                                //    }
                                //    else
                                //    {
                                //        lblTitlePayment.Text = "Phải thu";
                                //        txtReturnAmount.Text = (0).ToString("#,##0");
                                //    }
                                //}
                                //else
                                //{
                                //    lblTitlePayment.Text = "Phải thu";
                                //    txtReturnAmount.Text = (decTotalFee).ToString("#,##0");
                                //}


                                if (objInputChangeOrder.IsDebtOrder && decTotalPaid > decTotalFee)
                                {
                                    //  if (decTotalPaid > decTotalFee)
                                    //  {
                                    lblTitlePayment.Text = "Phải chi";
                                    decReturnAmount = Math.Round(decTotalPaid - decTotalFee);
                                    txtReturnAmount.Text = decReturnAmount.ToString("#,##0");
                                    //  }
                                    //else if (decTotalPaid < decTotalFee)
                                    //{
                                    //    lblTitlePayment.Text = "Phải thu";
                                    //    txtReturnAmount.Text = (decTotalFee - decTotalPaid).ToString("#,##0");
                                    //}
                                    //else
                                    //{
                                    //    lblTitlePayment.Text = "Phải thu";
                                    //    txtReturnAmount.Text = (0).ToString("#,##0");
                                    //}
                                }
                                else
                                {
                                    if (objInputChangeOrder.UnevenAmount + decTotalFee > 0)
                                    {
                                        lblTitlePayment.Text = "Phải thu";

                                        txtReturnAmount.Text = (objInputChangeOrder.UnevenAmount + decTotalFee).ToString("#,##0");
                                    }
                                    else if (objInputChangeOrder.UnevenAmount + decTotalFee < 0)
                                    {
                                        lblTitlePayment.Text = "Phải chi";
                                        txtReturnAmount.Text = ((objInputChangeOrder.UnevenAmount + decTotalFee) * -1).ToString("#,##0");
                                    }
                                    else
                                    {
                                        lblTitlePayment.Text = "Phải thu";
                                        txtReturnAmount.Text = (0).ToString("#,##0");
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy thông tin phiếu xuất");
                SystemErrorWS.Insert("Lỗi lấy thông tin phiếu xuất", objExec, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void LoadInputType()
        {
            try
            {
                if (strInputChangeOrderID == string.Empty)
                {
                    bool bolIsSelected = DataTableClass.CheckIsExist(dtbOVDetailReturn, "IsSelect = 1");
                    String strFilter = "";
                    if (bolIsSelected)
                        strFilter = "IsSelect = 1";
                    DataTable tblInputTypeFilter = DataTableClass.SelectDistinct(dtbOVDetailReturn, "ReturnInputTypeID", strFilter);
                    DataTable tblInputType = dtbInputTypeAll.Clone();
                    for (int i = 0; i < tblInputTypeFilter.Rows.Count; i++)
                    {
                        DataRow[] rInputType = dtbInputTypeAll.Select("InputTypeID = " + Convert.ToInt32(tblInputTypeFilter.Rows[i]["ReturnInputTypeID"]));
                        DataRow r = tblInputType.NewRow();
                        r.ItemArray = rInputType[0].ItemArray;
                        tblInputType.Rows.Add(r);
                    }
                    cboInputTypeID.Title = "--Chọn hình thức nhập--";
                    cboInputTypeID.InitControl(false, tblInputType.Copy());
                    if (tblInputType.Rows.Count == 1)
                        cboInputTypeID.SetIndex(0);
                }
                else
                {
                    int intReturnInputTypeID = Convert.ToInt32(dtbOVDetailReturn.Rows[0]["ReturnInputTypeID"]);
                    DataTable tblInputType = dtbInputTypeAll.Clone();
                    DataRow[] drInputType = dtbInputTypeAll.Select("InputTypeID = " + intReturnInputTypeID);
                    DataRow rInputType = tblInputType.NewRow();
                    rInputType.ItemArray = drInputType[0].ItemArray;
                    tblInputType.Rows.Add(rInputType);
                    cboInputTypeID.Title = "--Chọn hình thức nhập--";
                    cboInputTypeID.InitControl(false, tblInputType);
                    if (tblInputType.Rows.Count == 1)
                        cboInputTypeID.SetIndex(0);
                }
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy hình thức nhập");
                SystemErrorWS.Insert("Lỗi lấy hình thức nhập", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        private bool LoadCombobox()
        {
            try
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilter.IsCheckPermission = true;
                objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.OUTPUT;
                cboStore.InitControl(false, objStoreFilter);
                cboInputChangeOrderStoreID.InitControl(false, objStoreFilter);
                dtbInputTypeAll = Library.AppCore.DataSource.GetDataSource.GetInputType();
                dtbOutputTypeAll = Library.AppCore.DataSource.GetDataSource.GetOutputType();

                tblInputChangeReasonAll = Library.AppCore.DataSource.GetDataSource.GetInputChangeReason().Copy();
                DataTable tblInputChangeReason_Filter = tblInputChangeReasonAll.Clone();
                DataTable tblInputChangeReason_InputChangeOrderType = null;
                objPLCInputChangeOrderType.GetData_Reason(ref tblInputChangeReason_InputChangeOrderType, intInputChangeOrderTypeID);
                if (tblInputChangeReason_InputChangeOrderType != null && tblInputChangeReason_InputChangeOrderType.Rows.Count > 0)
                {
                    tblInputChangeReason_InputChangeOrderType.DefaultView.RowFilter = "ISSELECT = True";
                    tblInputChangeReason_InputChangeOrderType = tblInputChangeReason_InputChangeOrderType.DefaultView.ToTable();
                    if (tblInputChangeReason_InputChangeOrderType.Rows.Count > 0)
                    {
                        string strInputChangeReasonIDList = string.Empty;
                        for (int i = 0; i < tblInputChangeReason_InputChangeOrderType.Rows.Count; i++)
                        {
                            DataRow rReason = tblInputChangeReason_InputChangeOrderType.Rows[i];
                            strInputChangeReasonIDList += rReason["INPUTCHANGEREASONID"].ToString() + ",";
                        }
                        strInputChangeReasonIDList = strInputChangeReasonIDList.Trim(',');
                        tblInputChangeReasonAll.DefaultView.RowFilter = "INPUTCHANGEREASONID IN (" + strInputChangeReasonIDList + ")";
                        tblInputChangeReason_Filter = tblInputChangeReasonAll.DefaultView.ToTable();
                    }
                }
                cboReasonOV.InitControl(false, tblInputChangeReason_Filter.Copy(), "INPUTCHANGEREASONID", "INPUTCHANGEREASONNAME", "--Chọn lý do đổi trả--");
                cboReason.InitControl(false, tblInputChangeReason_Filter.Copy(), "INPUTCHANGEREASONID", "INPUTCHANGEREASONNAME", "--Chọn lý do đổi trả--");
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nạp combobox");
                SystemErrorWS.Insert("Lỗi nạp combobox", objExec, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        public float GetCurrencyExchange(int CurrencyUnitID)
        {
            DataTable dtbCurrencyUnit = Library.AppCore.DataSource.GetDataSource.GetCurrencyUnit();
            DataRow[] rows = dtbCurrencyUnit.Select("CURRENCYUNITID = " + CurrencyUnitID);
            if (rows.Length < 1)
            {
                return 0;
            }
            return Convert.ToSingle(rows[0]["CURRENCYEXCHANGE"]);
        }

        private bool CheckAdd()
        {
            if (cboStore.StoreID < 1)
            {
                cboStore.Focus();
                MessageBox.Show(this, "Bạn chưa chọn kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            //20/06/2019 Pham Hai Dang bo sung dieu kien neu la doi trả PVI được phép đổi trả khác chi nhánh
            DataTable dtbCheckInfoSaleOrderType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_OUTPUTVOUCHER_CHKSOTYPE", new object[] { "@ORDERID", objOutputVoucherLoad.OrderID });
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->frmInputChangeOrder_Load");
            }
            bool bolCheckPVI = false;
            if (dtbCheckInfoSaleOrderType != null && dtbCheckInfoSaleOrderType.Rows.Count > 0)
            {
                bolCheckPVI = Convert.ToInt32(dtbCheckInfoSaleOrderType.Rows[0]["INVOICESTOREID"]) > 0;
            }
            if (!bolCheckPVI)
            {
                // 06/03/2018 LE VAN DONG BỔ SUNG ĐIỀU KIỆN
                // Là hình thức đổi trả: thì yêu câu cùng chi nhánh
                // Là hình thức trả hàng: thì yêu cầu cùng kho
                if (objInputChangeOrderType.IsInputReturn) // la loai nhập tra
                {
                    if (objOutputVoucherLoad.OutputStoreID != cboStore.StoreID)
                    {
                        MessageBox.Show(this, "Không được phép nhập trả hàng khác kho xuất!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                else
                {
                    ERP.MasterData.PLC.MD.WSStore.Store objOutputStore = null;
                    new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objOutputStore, objOutputVoucherLoad.OutputStoreID);

                    ERP.MasterData.PLC.MD.WSStore.Store objDefaultStore = null;
                    new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objDefaultStore, cboStore.StoreID);

                    if (objOutputStore.BranchID != objDefaultStore.BranchID)
                    {
                        MessageBox.Show(this, "Không được phép nhập đổi trả hàng khác chi nhánh!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                //

            }

            if (cboInputTypeID.InputTypeID < 1)
            {
                MessageBox.Show(this, "Bạn chỉ được phép chọn các sản phẩm cùng hình thức nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtContentIOT.Text))
            {
                MessageBox.Show(this, "Vui lòng nhập nội dung yêu cầu nhập đổi trả", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtContentIOT.Focus();
                return false;
            }

            dtbOVDetailReturn.AcceptChanges();

            DataRow[] rIsSelect = dtbOVDetailReturn.Select("IsSelect = 1");
            if (rIsSelect.Length < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất 1 sản phẩm cần " + strMessageForm, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (objInputChangeOrderType.IsGetPromotion && rIsSelect.Length > 1)
            {
                MessageBox.Show(this, "Vui lòng chỉ chọn 1 sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            DataRow[] rSelectNew = dtbOVDetailReturn.Select("IsSelect = 1 AND IsNew = True");
            DataRow[] rSelectOld = dtbOVDetailReturn.Select("IsSelect = 1 AND IsNew = False");
            if (rSelectNew.Length > 0 && rSelectOld.Length > 0)
            {
                MessageBox.Show(this, "Không thể " + strMessageForm + " cùng lúc sản phẩm mới và cũ. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            ERP.Inventory.PLC.Input.PLCInputVoucherReturn objPLCInputVoucherReturn = new ERP.Inventory.PLC.Input.PLCInputVoucherReturn();
            DataTable dtbIMEIWarranty = null;
            if ((!objInputChangeOrderType.IsInputReturn) && (/*!chkCreateSaleOrder.Checked ||*/ !objInputChangeOrderType.IsGetPromotion))
            {
                //nếu là loại nhập đổi trả hàng
                for (int i = 0; i < rIsSelect.Length; i++)
                {
                    if (rIsSelect[i]["ProductID_Out"] == DBNull.Value || rIsSelect[i]["ProductID_Out"].ToString().Trim() == string.Empty)
                    {
                        MessageBox.Show(this, "Vui lòng nhập sản phẩm xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    if (Convert.ToBoolean(rIsSelect[i]["IsRequestIMEI_Out"]))
                    {
                        if (rIsSelect[i]["IMEI_Out"] == DBNull.Value || rIsSelect[i]["IMEI_Out"].ToString().Trim() == string.Empty)
                        {
                            MessageBox.Show(this, "Vui lòng nhập IMEI xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }

                        dtbIMEIWarranty = objPLCInputVoucherReturn.LoadOutputVoucherWarrantyExtend(Convert.ToString(objOutputVoucherLoad.OutputVoucherID).Trim(), rIsSelect[i]["IMEI"].ToString().Trim());
                        if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                        if (dtbIMEIWarranty.Rows.Count > 0)
                        {
                            if (rIsSelect[i]["ProductID"].ToString().Trim() != rIsSelect[i]["ProductID_Out"].ToString().Trim() || Math.Round(Convert.ToDecimal(rIsSelect[i]["InputPrice_In"])) != Math.Round(Convert.ToDecimal(rIsSelect[i]["SalePrice_Out"])))
                            {
                                if (Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim() != string.Empty && Convert.ToString(dtbIMEIWarranty.Rows[0]["IMEI"]).Trim() == Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim())
                                {
                                    if (MessageBox.Show(this, "Phiếu xuất " + Convert.ToString(dtbIMEIWarranty.Rows[0]["OutputVoucherExtID"]).Trim() + " chưa nhập trả Care+\r\nNếu đồng ý IMEI sẽ không xuất được Care+ cho lần sau\r\nBạn có chắc muốn đồng ý không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                        return false;
                                }
                            }
                            else
                            {
                                if (Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim() != string.Empty && Convert.ToString(dtbIMEIWarranty.Rows[0]["IMEI"]).Trim() == Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim())
                                {
                                    if (objInputChangeOrderType.IsContinueInstallInfo)
                                    {
                                        if (MessageBox.Show(this, "IMEI " + rIsSelect[i]["IMEI"].ToString().Trim() + " sẽ chuyển tiếp Care+ cho IMEI " + rIsSelect[i]["IMEI_Out"].ToString().Trim() + "\r\nBạn có chắc muốn đồng ý không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                            return false;
                                    }
                                    else
                                    {
                                        if (MessageBox.Show(this, "Phiếu xuất " + Convert.ToString(dtbIMEIWarranty.Rows[0]["OutputVoucherExtID"]).Trim() + " chưa nhập trả Care+\r\nNếu đồng ý IMEI sẽ không xuất được Care+ cho lần sau\r\nBạn có chắc muốn đồng ý không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                            return false;
                                    }
                                }
                            }
                        }
                    }
                    if (rIsSelect[i]["InStockStatusID_Out"] == DBNull.Value || Convert.ToInt32(rIsSelect[i]["InstockStatusID_Out"]) < 0)
                    {
                        MessageBox.Show(this, "Sản phẩm hiện tại không xác định được trạng thái! Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
            }
            else
            {
                DataRow[] drIMEISelect = dtbOVDetailReturn.Select("IsSelect = 1 AND IMEI IS NOT NULL");
                if (drIMEISelect.Length > 0)
                {
                    for (int i = 0; i < drIMEISelect.Length; i++)
                    {
                        string strIMEI = Convert.ToString(drIMEISelect[i]["IMEI"]).Trim();
                        if (strIMEI != string.Empty)
                        {
                            dtbIMEIWarranty = objPLCInputVoucherReturn.LoadOutputVoucherWarrantyExtend(Convert.ToString(objOutputVoucherLoad.OutputVoucherID).Trim(), strIMEI);
                            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                            {
                                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                                return false;
                            }
                            if (dtbIMEIWarranty.Rows.Count > 0)
                            {
                                if (Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim() != string.Empty && Convert.ToString(dtbIMEIWarranty.Rows[0]["IMEI"]).Trim() == Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim())
                                {
                                    if (MessageBox.Show(this, "Phiếu xuất " + Convert.ToString(dtbIMEIWarranty.Rows[0]["OutputVoucherExtID"]).Trim() + " chưa nhập trả Care+\r\nNếu đồng ý IMEI sẽ không xuất được Care+ cho lần sau\r\nBạn có chắc muốn đồng ý không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                        return false;
                                }
                            }
                        }
                    }
                }
                drIMEISelect = dtbOVDetailReturn.Select("IsSelect = 1 AND (InStockStatusID_In is null Or InStockStatusID_In < 0)");
                if (drIMEISelect != null && drIMEISelect.Length > 0)
                {
                    if (MessageBox.Show(this, "Danh sách sản phẩm nhập chưa xác định được trạng thái sản phẩm nhập! Vui lòng kiểm tra lại khai báo!", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                        return false;
                }

            }

            decimal decInvoice = 0;
            try
            {
                decInvoice = Convert.ToDecimal(txtInVoiceID.Text);
            }
            catch { }
            if (decInvoice != 0)
            {
                if (MessageBox.Show(this, "Phiếu xuất trước đó đã In hóa đơn\r\nBạn có chắc đã nắm thông tin?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    return false;
            }
            DataRow[] rowCheckErrorGroup = dtbOVDetailReturn.Select("IsSelect = 1 AND INSTOCKSTATUSID_IN IS NOT NULL AND (INSTOCKSTATUSID_IN =5 OR INSTOCKSTATUSID_IN =3) AND MACHINEERRORNAME IS NULL");
            if (rowCheckErrorGroup.Any())
            {
                MessageBox.Show(this, "Vui lòng chọn lỗi, nhóm lỗi " + strMessageForm, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        public string Parse(decimal number)
        {
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            return number.ToString("N0", nfi);
        }

        private bool CheckCreateConcern()
        {
            if (cboStore.StoreID < 1)
            {
                MessageBox.Show(this, "Bạn không có quyền trên mã kho nhập " + objInputChangeOrder.InputChangeOrderStoreID, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            // decimal decTotalReturnAmount = Parse(txtReturnAmount.Text);
            strPaymentCardName = string.Empty;
            dtbOVDetailReturn.AcceptChanges();
            decimal decSumInputPrice = GetCompute_In(string.Empty);
            decimal decSumSalePrice = GetCompute_Out(string.Empty);

            decimal decTotalFee = 0;
            DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select(string.Empty);
            string strOutputVoucherDetailIDList = string.Empty;
            for (int i = 0; i < drDetail.Length; i++)
            {
                decTotalFee += Convert.ToDecimal(drDetail[i]["TotalFee"]);
                strOutputVoucherDetailIDList += drDetail[i]["OldOutputVoucherDetailID"].ToString().Trim() + ",";
            }

            #region Nếu là hình thức nhập trả do nhân viên xuất sai thì bắt buộc phải trả những khuyến mãi kèm theo

            string strMessagePromotionNeedReturn = string.Empty;
            string strProductList = string.Empty;
            string strCouponCodeList = string.Empty;
            List<string> lstProductList = new List<string>();
            List<string> lstCouponCodeList = new List<string>();
            DataTable dtPromotionNeedReturn = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_INPUTCHANGE_GETPROMOTION", new object[] { "@InputChangeOrderTypeID", objInputChangeOrderType.InputChangeOrderTypeID,
                                                                                                                                                       "@OutputVoucherDetailIDList", strOutputVoucherDetailIDList});
            if (dtPromotionNeedReturn != null && dtPromotionNeedReturn.Rows.Count > 0)
            {
                for (int i = 0; i < drDetail.Length; i++)
                {
                    DataRow rDetail = drDetail[i];

                    DataRow[] lst = dtPromotionNeedReturn.Select(string.Format(@"OUTPUTVOUCHERDETAILID = '{0}'", rDetail["OldOutputVoucherDetailID"].ToString().Trim()));
                    if (lst != null && lst.Length > 0)
                    {
                        foreach (DataRow rowPromotion in lst)
                        {
                            if (!drDetail.Any(x => x["OldOutputVoucherDetailID"].ToString().Trim() == rowPromotion["PROMOTIONOUTPUTVOUCHERDETAILID"].ToString().Trim()))
                            {
                                if (rowPromotion["COUPONCODE"] != DBNull.Value)
                                {
                                    if (!lstCouponCodeList.Contains(rowPromotion["COUPONCODE"].ToString().Trim()))
                                        lstCouponCodeList.Add(rowPromotion["COUPONCODE"].ToString().Trim());
                                }
                                else
                                {
                                    if (!lstProductList.Contains(rowPromotion["PRODUCTID"].ToString().Trim()))
                                        lstProductList.Add(rowPromotion["PRODUCTID"].ToString().Trim());
                                }
                            }
                        }
                    }

                    if (lstProductList.Count > 0 || lstCouponCodeList.Count > 0)
                    {
                        foreach (string strProductID in lstProductList)
                        {
                            strProductList += "\n- " + strProductID;
                        }

                        foreach (string strCouponCode in lstCouponCodeList)
                        {
                            strCouponCodeList += "\n- " + strCouponCode;
                        }

                        strMessagePromotionNeedReturn += string.Format(@"Để nhập trả sản phẩm {0} bạn cần phải trả những khuyến mãi kèm theo:", rDetail["PRODUCTID"].ToString().Trim());
                        if (!string.IsNullOrEmpty(strProductList))
                        {
                            strMessagePromotionNeedReturn += "\nSản phẩm:" + strProductList;
                        }

                        if (!string.IsNullOrEmpty(strCouponCodeList))
                        {
                            strMessagePromotionNeedReturn += "\nCoupon:" + strCouponCodeList;
                        }

                        MessageBox.Show(strMessagePromotionNeedReturn, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
            }

            #endregion

            if (!string.IsNullOrEmpty(txtSaleOrderID.Text))
            {
                bool bolIsRetry = false;
                bool bolIsOKPayment = false;
                if ((decVNDCash_Out + decPaymentCardAmount_Out) != Math.Round(decSumInputPrice))
                {
                    POPUP:
                    if (bolIsRetry)
                    {
                        decVNDCash_Out = 0;
                        decSumInputPrice += Math.Round(decTotalFee);
                    }
                    decimal decVNDCashTMP = decVNDCash_Out;
                    if (decVNDCashTMP == 0)
                    {
                        if (objInputChangeOrderType.ReturnFeeBook == 2 && !objInputChangeOrderType.IsInputReturn)
                        {
                            decVNDCashTMP = Math.Round(decSumInputPrice) - Math.Round(decTotalFee);
                            decSumInputPrice = decVNDCashTMP;
                        }
                        else
                            decVNDCashTMP = Math.Round(decSumInputPrice);
                    }
                    if (objInputChangeOrderType.ReturnFeeBook == 2 && !objInputChangeOrderType.IsInputReturn)
                        decSumInputPrice = decVNDCashTMP;
                    string PaymentCardName = string.Empty;
                    string storename = string.Empty, createuser = string.Empty, contactid = string.Empty;
                    GetInforVNPAY(ref storename, ref contactid, ref createuser);
                    ShowPaymentForm1(true, ref bolIsOKPayment, "Chi tiền nhập trả", decVNDCashTMP, decPaymentCardAmount, intPaymentCardID, decPaymentCardSpend, strPaymentCardVoucherID, Math.Round(decSumInputPrice),
                                        ref decVNDCash_Out, ref decPaymentCardAmount_Out, ref intPaymentCardID, ref PaymentCardName, ref decPaymentCardSpend, ref strPaymentCardVoucherID, ref lstAddVoucherDetail,
                                        storename,
                                        createuser,
                                        contactid);
                    if (bolIsOKPayment)
                    {
                        if (lstAddVoucherDetail != null && lstAddVoucherDetail.Count > 0)
                        {
                            decVNDCash_Out = lstAddVoucherDetail.Sum(x => x.VNDCash);
                            decPaymentCardAmount_Out = lstAddVoucherDetail.Sum(x => x.PaymentCardAmount);
                        }
                        if ((decVNDCash_Out + decPaymentCardAmount_Out) == Math.Round(decSumInputPrice))
                            bolIsOKPayment = true;
                        else
                        {
                            if (Math.Round(decVNDCash_Out + decPaymentCardAmount_Out) == 0)
                            {
                                if (MessageBox.Show(this, "Phiếu chi chưa chi hết. Bạn có chắc muốn tạo phiếu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                MessageBox.Show(this, "Phải chi đúng số tiền chi nhập trả", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                bolIsRetry = true;
                                goto POPUP;
                            }
                        }
                    }
                }
                else
                    bolIsOKPayment = true;

                bool bolIsOKPayment_In = false;
                if (bolIsOKPayment)
                {
                    if ((decVNDCash_In + decPaymentCardAmount_In) != Math.Round(decSumSalePrice))
                    {
                        string storename = string.Empty, createuser = string.Empty, contactid = string.Empty;
                        GetInforVNPAY(ref storename, ref contactid, ref createuser);
                        ShowPaymentForm1(false, ref bolIsOKPayment_In, "Thu tiền thanh toán đơn hàng", decVNDCash_In, decPaymentCardAmount_In, intPaymentCardID_In, decPaymentCardSpend_In, strPaymentCardVoucherID_In, Math.Round(decSumSalePrice),
                                            ref decVNDCash_In, ref decPaymentCardAmount_In, ref intPaymentCardID_In, ref strPaymentCardName_In, ref decPaymentCardSpend_In, ref strPaymentCardVoucherID_In, ref lstAddVoucherDetail_In,
                                            storename,
                                            createuser,
                                            contactid);
                        if (bolIsOKPayment_In)
                        {
                            if (lstAddVoucherDetail_In != null && lstAddVoucherDetail_In.Count > 0)
                            {
                                decVNDCash_In = lstAddVoucherDetail_In.Sum(x => x.VNDCash);
                                decPaymentCardAmount_In = lstAddVoucherDetail_In.Sum(x => x.PaymentCardAmount);
                            }
                            if ((decVNDCash_In + decPaymentCardAmount_In) == Math.Round(decSumSalePrice))
                                bolIsOKPayment_In = true;
                        }
                    }
                    else
                        bolIsOKPayment_In = true;
                }

                if ((!bolIsOKPayment) || (!bolIsOKPayment_In))
                    return false;
                decVNDCash = decVNDCash_Out;
                decPaymentCardAmount = decPaymentCardAmount_Out;
            }
            else
            {
                if (((decVNDCash + decPaymentCardAmount) != Math.Abs(Math.Round(decSumSalePrice - decSumInputPrice)) || (Math.Abs(Math.Round(decSumSalePrice - decSumInputPrice)) <= 0 && decTotalFee > 0 && objInputChangeOrderType.ReturnFeeBook == 2 && !objInputChangeOrderType.IsInputReturn)) && decReturnAmount > 0)
                {
                    bool bolIsOKPayment = false;
                    string strTextForm = string.Empty;
                    bool bolIsSpend = true;
                    if (objInputChangeOrderType.IsInputReturn)
                    {
                        //if (bolSpecialCase)
                        //    strTextForm = "Thu tiền phí nhập trả";
                        //else
                        strTextForm = "Chi tiền nhập trả";
                    }
                    //if (bolSpecialCase)
                    //    bolIsSpend = false;
                    else if (Math.Round(decSumSalePrice) > Math.Round(decSumInputPrice))
                    {
                        bolIsSpend = false;
                    }
                    else if (Math.Round(decSumSalePrice) <= Math.Round(decSumInputPrice) && Math.Round(decSumInputPrice) - Math.Round(decSumSalePrice) < decTotalFee && decTotalFee > 0 && objInputChangeOrderType.ReturnFeeBook == 2 && !objInputChangeOrderType.IsInputReturn)
                    {
                        bolIsSpend = false;
                    }
                    string storename = string.Empty, createuser = string.Empty, contactid = string.Empty;
                    GetInforVNPAY(ref storename, ref contactid, ref createuser);
                    ShowPaymentForm1(bolIsSpend, ref bolIsOKPayment, strTextForm, decVNDCash, decPaymentCardAmount, intPaymentCardID, decPaymentCardSpend, strPaymentCardVoucherID, decReturnAmount,
                                        ref decVNDCash, ref decPaymentCardAmount, ref intPaymentCardID, ref strPaymentCardName, ref decPaymentCardSpend, ref strPaymentCardVoucherID, ref lstAddVoucherDetail,
                                        storename,
                                        createuser,
                                        contactid);
                    if (bolIsOKPayment)
                    {
                        if (lstAddVoucherDetail != null && lstAddVoucherDetail.Count > 0)
                        {
                            decVNDCash = lstAddVoucherDetail.Sum(x => x.VNDCash);
                            decPaymentCardAmount = lstAddVoucherDetail.Sum(x => x.PaymentCardAmount);
                        }
                        if (objInputChangeOrderType.ReturnFeeBook == 2 && !objInputChangeOrderType.IsInputReturn /*&& Math.Round(decSumSalePrice) >= Math.Round(decSumInputPrice)*/)
                        {
                            if ((decVNDCash + decPaymentCardAmount) == Math.Abs(Math.Round((decSumSalePrice - decSumInputPrice + decTotalFee), MidpointRounding.AwayFromZero)) || bolIsSpend)
                            {
                                if (Math.Round(decSumSalePrice) == Math.Round(decSumInputPrice))
                                    bolIsCreateVoucherReturnFee = true;
                                return true;
                            }
                        }
                        else if ((decVNDCash + decPaymentCardAmount) == Math.Abs(Math.Round((decSumSalePrice - decSumInputPrice), MidpointRounding.AwayFromZero)) || bolIsSpend)
                        {
                            return true;
                        }
                        MessageBox.Show(this, "Phải thanh toán đúng số tiền thu/chi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    return false;
                }
            }
            return true;
        }

        private void GetInforVNPAY(ref string storename, ref string contact, ref string createuser)
        {
            storename = cboStore.StoreName.Split('-')[1].Trim();
            if (storename.Length > 5)
            {
                storename = storename.Substring(0, 5);
            }
            //createuser = objInputChangeOrder.objSaleOrder.StaffUser;
            createuser = SystemConfig.objSessionUser.UserName;
            //createuser = txtStaffUser.Text.Trim().Split('-')[0].Trim();
            contact = txtCustomerPhone.Text.Trim();
        }

        private void ClearDetail()
        {
            for (int i = 0; i < dtbOVDetailReturn.Rows.Count; i++)
            {
                DataRow r = dtbOVDetailReturn.Rows[i];
                r["ProductID_Out"] = string.Empty;
                r["ProductName_Out"] = string.Empty;
                r["IMEI_Out"] = string.Empty;
                r["SalePrice_Out"] = DBNull.Value;
                r["IsRequestIMEI_Out"] = false;
            }
        }

        private void FormatFlex()
        {
            flexDetail.Rows.Fixed = 1;
            //CellStyle style1 = flexDetail.Styles.Add("ComboStatus");
            //style1.DataType = typeof(Int64);
            //style1.DataMap = hstbInstockStatus;
            for (int i = 0; i < flexDetail.Cols.Count; i++)
            {
                flexDetail.Cols[i].Visible = false;
                flexDetail.Cols[i].AllowSorting = false;
                flexDetail.Cols[i].AllowDragging = false;
                flexDetail.Cols[i].AllowEditing = false;
            }
            flexDetail.Cols["IsSelect"].Visible = true;
            flexDetail.Cols["ProductID"].Visible = true;
            flexDetail.Cols["ProductName"].Visible = true;

            flexDetail.Cols["INSTOCKSTATUSID_OUT"].Visible = true;
            flexDetail.Cols["IMEI"].Visible = true;
            flexDetail.Cols["EndWarrantyDate"].Visible = true;
            flexDetail.Cols["INSTOCKSTATUSID_IN"].Visible = true;
            flexDetail.Cols["TotalFee"].Visible = true;
            //Nguyen Van Tai bo sung theo yeu cau BA Trung -30/11/2017
            flexDetail.Cols["MACHINEERRORGROUPNAME"].Visible = true;
            flexDetail.Cols["MACHINEERRORNAME"].Visible = true;
            //End
            flexDetail.Cols["InputPrice_In"].Visible = true;
            flexDetail.Cols["Quantity"].Visible = true;
            flexDetail.Cols["OutputTypeName_Out"].Visible = true;
            flexDetail.Cols["ProductID_Out"].Visible = true;
            flexDetail.Cols["ProductName_Out"].Visible = true;
            flexDetail.Cols["IMEI_Out"].Visible = true;
            flexDetail.Cols["SalePrice_Out"].Visible = true;
            flexDetail.Cols["IsSelect"].Caption = "Chọn";
            flexDetail.Cols["ProductID"].Caption = "Mã sản phẩm nhập";
            flexDetail.Cols["ProductName"].Caption = "Tên sản phẩm nhập";
            flexDetail.Cols["INSTOCKSTATUSID_IN"].Caption = "TTSP nhập";
            flexDetail.Cols["INSTOCKSTATUSID_OUT"].Caption = "TTSP xuất đổi";
            flexDetail.Cols["IMEI"].Caption = "IMEI nhập";
            flexDetail.Cols["InputPrice_In"].Caption = "Giá nhập";
            flexDetail.Cols["OutputTypeName_Out"].Caption = "Hình thức xuất";
            flexDetail.Cols["ProductID_Out"].Caption = "Mã sản phẩm xuất";
            flexDetail.Cols["ProductName_Out"].Caption = "Tên sản phẩm xuất";
            flexDetail.Cols["IMEI_Out"].Caption = "IMEI xuất";
            flexDetail.Cols["EndWarrantyDate"].Caption = "Ngày bảo hành";
            //Nguyen Van Tai bo sung theo yeu cau BA Trung -30/11/2017
            flexDetail.Cols["MACHINEERRORGROUPNAME"].Caption = "Nhóm lỗi";
            flexDetail.Cols["MACHINEERRORNAME"].Caption = "Lỗi";
            flexDetail.Cols["MACHINEERRORNAME"].ComboList = "...";

            flexDetail.Cols["MACHINEERRORNAME"].AllowEditing = true;
            //End
            flexDetail.Cols["SalePrice_Out"].Caption = "Giá bán";
            flexDetail.Cols["Quantity"].Caption = "Số lượng";
            flexDetail.Cols["TotalFee"].Caption = "Phí trả";

            flexDetail.Cols["IsSelect"].Width = 40;
            flexDetail.Cols["ProductID"].Width = 120;
            flexDetail.Cols["ProductName"].Width = 140;
            flexDetail.Cols["INSTOCKSTATUSID_IN"].Width = 120;
            flexDetail.Cols["INSTOCKSTATUSID_IN"].DataMap = hstbInstockStatus;
            flexDetail.Cols["INSTOCKSTATUSID_OUT"].Width = 120;
            flexDetail.Cols["INSTOCKSTATUSID_OUT"].DataMap = hstbInstockStatus;

            flexDetail.Cols["IMEI"].Width = 120;
            flexDetail.Cols["EndWarrantyDate"].Width = 80;
            //Nguyen Van Tai bo sung theo yeu cau BA Trung -30/11/2017
            flexDetail.Cols["MACHINEERRORGROUPNAME"].Width = 240;
            flexDetail.Cols["MACHINEERRORNAME"].Width = 240;
            //End
            flexDetail.Cols["InputPrice_In"].Width = 70;
            flexDetail.Cols["OutputTypeName_Out"].Width = 100;
            flexDetail.Cols["ProductID_Out"].Width = 120;
            flexDetail.Cols["ProductName_Out"].Width = 140;

            flexDetail.Cols["IMEI_Out"].Width = 120;
            flexDetail.Cols["SalePrice_Out"].Width = 70;
            flexDetail.Cols["Quantity"].Width = 50;
            flexDetail.Cols["TotalFee"].Width = 90;

            flexDetail.Cols["InputPrice_In"].Format = "#,##0";
            flexDetail.Cols["SalePrice_Out"].Format = "#,##0";
            flexDetail.Cols["Quantity"].Format = "#,##0";
            flexDetail.Cols["TotalFee"].Format = "#,##0";
            flexDetail.Cols["EndWarrantyDate"].Format = "dd/MM/yyyy";
            flexDetail.Cols["IsSelect"].DataType = typeof(bool);
            flexDetail.Cols["IsSelect"].AllowEditing = true;
            flexDetail.Cols["ProductID_Out"].AllowEditing = true;
            flexDetail.Cols["IMEI_Out"].AllowEditing = true;
            flexDetail.Cols["Quantity"].AllowEditing = true;


            flexDetail.Cols["ProductID_Out"].ComboList = "...";

            C1.Win.C1FlexGrid.CellStyle style = flexDetail.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexDetail.GetCellRange(0, 0, 0, flexDetail.Cols.Count - 1);
            range.Style = style;
            flexDetail.Rows[0].Height = 40;
            flexDetail.Cols.Fixed = 1;

            this.flexDetail.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexDetail_BeforeEdit);
        }

        private void FormatFlexDetail()
        {
            flexDetail.Rows.Fixed = 1;
            flexDetail.AllowEditing = !objInputChangeOrder.IsReviewed;
            for (int i = 0; i < flexDetail.Cols.Count; i++)
            {
                flexDetail.Cols[i].Visible = false;
                flexDetail.Cols[i].AllowSorting = false;
                flexDetail.Cols[i].AllowDragging = false;
                flexDetail.Cols[i].AllowEditing = false;
            }
            if (!objInputChangeOrderType.IsInputReturn)
            {
                CustomizeC1FlexGrid.SetColumnVisible(flexDetail, true, true, "ProductID,ProductName,IMEI,InputPrice_In,EndWarrantyDate," +
                                            "INSTOCKSTATUSID_IN,MACHINEERRORGROUPNAME,MACHINEERRORNAME,OutputTypeName_Out,ProductID_Out,ProductName_Out,INSTOCKSTATUSID_OUT,IMEI_Out,SalePrice_Out,Quantity,TotalFee");
                CustomizeC1FlexGrid.SetColumnCaption(flexDetail, "ProductID,Mã sản phẩm nhập,ProductName,Tên sản phẩm nhập,IMEI,IMEI nhập,InputPrice_In,Giá nhập,EndWarrantyDate,Ngày bảo hành," +
                                            "INSTOCKSTATUSID_IN,TTSP nhập,MACHINEERRORGROUPNAME,Nhóm lỗi,MACHINEERRORNAME,Lỗi,OutputTypeName_Out,Hình thức xuất,ProductID_Out,Mã sản phẩm xuất,ProductName_Out,Tên sản phẩm xuất,INSTOCKSTATUSID_OUT,TTSP xuất,IMEI_Out,IMEI xuất,SalePrice_Out,Giá bán,Quantity,Số lượng,TotalFee,Phí trả");
                CustomizeC1FlexGrid.SetColumnWidth(flexDetail, "ProductID,120,ProductName,140,IMEI,120,InputPrice_In,70,EndWarrantyDate,80," +
                                            "INSTOCKSTATUSID_IN,120,MACHINEERRORGROUPNAME,240,MACHINEERRORNAME,240,OutputTypeName_Out,100,ProductID_Out,120,ProductName_Out,140,INSTOCKSTATUSID_OUT,120,IMEI_Out,120,SalePrice_Out,70,Quantity,50,TotalFee,90");

            }
            else
            {
                CustomizeC1FlexGrid.SetColumnVisible(flexDetail, true, true, "ProductID,ProductName,IMEI,SalePrice,EndWarrantyDate,INSTOCKSTATUSID_IN,MACHINEERRORGROUPNAME,MACHINEERRORNAME,TotalFee,InputPrice_In,Quantity");
                CustomizeC1FlexGrid.SetColumnCaption(flexDetail, "ProductID,Mã sản phẩm,ProductName,Tên sản phẩm,IMEI,IMEI,SalePrice,Đơn giá,EndWarrantyDate,Ngày bảo hành,INSTOCKSTATUSID_IN,Trạng thái SP,MACHINEERRORGROUPNAME,Nhóm lỗi,MACHINEERRORNAME,Lỗi,TotalFee,Phí trả,InputPrice_In,Giá nhập,Quantity,Số lượng nhập trả");
                CustomizeC1FlexGrid.SetColumnWidth(flexDetail, "ProductID,120,ProductName,250,INSTOCKSTATUSID_IN,120,IMEI,180,SalePrice,90,TotalFee,90,EndWarrantyDate,110,MACHINEERRORGROUPNAME,240,MACHINEERRORNAME,240,InputPrice_In,90,Quantity,90");

            }
            flexDetail.Cols["SalePrice"].Format = "#,##0";
            flexDetail.Cols["TotalFee"].Format = "#,##0";
            flexDetail.Cols["InputPrice_In"].Format = "#,##0";
            flexDetail.Cols["SalePrice_Out"].Format = "#,##0";
            flexDetail.Cols["Quantity"].Format = "#,##0";
            flexDetail.Cols["EndWarrantyDate"].Format = "dd/MM/yyyy";
            flexDetail.Cols["INSTOCKSTATUSID_IN"].DataMap = hstbInstockStatus;
            flexDetail.Cols["INSTOCKSTATUSID_OUT"].DataMap = hstbInstockStatus;
            if (!objInputChangeOrder.IsReviewed)
            {
                flexDetail.Cols["MACHINEERRORNAME"].ComboList = "...";
                //flexDetail.Cols["MACHINEERRORGROUPNAME"].AllowEditing = !objInputChangeOrder.IsReviewed;
                flexDetail.Cols["MACHINEERRORNAME"].AllowEditing = !objInputChangeOrder.IsReviewed;
            }
            C1.Win.C1FlexGrid.CellStyle style = flexDetail.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexDetail.GetCellRange(0, 0, 0, flexDetail.Cols.Count - 1);
            range.Style = style;
            flexDetail.Rows[0].Height = 40;
            flexDetail.Cols.Fixed = 1;
        }

        private void FormatFlexReturn()
        {
            for (int i = 0; i < flexDetail.Cols.Count; i++)
            {
                flexDetail.Cols[i].Visible = false;
                flexDetail.Cols[i].AllowSorting = false;
                flexDetail.Cols[i].AllowDragging = false;
                flexDetail.Cols[i].AllowEditing = false;
            }
            flexDetail.Cols["IsSelect"].DataType = typeof(bool);
            CustomizeC1FlexGrid.SetColumnVisible(flexDetail, true, true, "IsSelect,OutputTypeName,ProductID,ProductName,IMEI,Endwarrantydate,INSTOCKSTATUSID_IN,MACHINEERRORGROUPNAME,MACHINEERRORNAME,SalePrice,VAT,Quantity,TotalFee,TotalCost");
            CustomizeC1FlexGrid.SetColumnCaption(flexDetail, "IsSelect", "Chọn",
                                                                           "OutputTypeName", "Hình thức xuất",
                                                                           "ProductID", "Mã sản phẩm",
                                                                           "ProductName", "Tên sản phẩm",
                                                                           "Endwarrantydate", "Ngày hết BH",
                                                                           "INSTOCKSTATUSID_IN", "Trạng thái SP nhập",
                                                                           "MACHINEERRORGROUPNAME", "Nhóm lỗi",
                                                                           "MACHINEERRORNAME", "Lỗi",
                                                                           "SalePrice", "Đơn giá",
                                                                           "TotalFee", "Phí trả",
                                                                           "Quantity", "Số lượng",
                                                                           "TotalCost", "Thành tiền");
            CustomizeC1FlexGrid.SetColumnAllowEditing(flexDetail, true, true, "IsSelect,Quantity,MACHINEERRORGROUPNAME,MACHINEERRORNAME");
            CustomizeC1FlexGrid.SetColumnFormat(flexDetail, "Endwarrantydate", "dd/MM/yyyy",
                                                                         "SalePrice", "###,##0",
                                                                         "TotalFee", "###,##0",
                                                                         "Quantity", "###,##0.##",
                                                                         "TotalCost", "###,##0");
            CustomizeC1FlexGrid.SetColumnWidth(flexDetail, "IsSelect", 40,
                                                                        "OutputTypeName", 150,
                                                                        "ProductID", 120,
                                                                        "ProductName", 140,
                                                                        "INSTOCKSTATUSID_IN", 120,
                                                                        "IMEI", 150,
                                                                        "Endwarrantydate", 80,
                                                                        "SalePrice", 80,
                                                                        "VAT", 40,
                                                                        "Quantity", 40,
                                                                        "MACHINEERRORNAME", 240);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnDigit(flexDetail, 6, 2, false, "Quantity");
            flexDetail.Cols["INSTOCKSTATUSID_IN"].DataMap = hstbInstockStatus;
            //Nguyen Van Tai bo sung theo yeu cau BA Trung - 30/11/2017

            flexDetail.Cols["MACHINEERRORNAME"].AllowEditing = true;
            flexDetail.Cols["MACHINEERRORNAME"].ComboList = "...";
            //End
        }

        private void EnableControl(bool bolEnable)
        {
            cboStore.Enabled = bolEnable;
            lnkCustomer.Enabled = bolEnable;
            btnAdd.Enabled = bolEnable;
            //    chkCreateSaleOrder.Enabled = bolEnable;
            chkCreateSaleOrder.Enabled = false;
        }
        private bool InitFlexGridAttachment()
        {
            try
            {
                if (strInputChangeOrderID != string.Empty)
                {
                    lstInputChangeOrder_Attachment = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment>();
                    DataTable tblAttachment = objPLCInputChangeOrder.GetAttachment(new object[] { "@InputChangeOrderID", strInputChangeOrderID });
                    if (tblAttachment != null && tblAttachment.Rows.Count > 0)
                    {
                        int intSTT = 0;
                        for (int i = 0; i < tblAttachment.Rows.Count; i++)
                        {
                            DataRow rAttachment = tblAttachment.Rows[i];
                            PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objAttachment = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment();
                            objAttachment.AttachmentID = Convert.ToString(rAttachment["AttachmentID"]).Trim();
                            objAttachment.InputChangeOrderID = strInputChangeOrderID;
                            objAttachment.AttachmentName = Convert.ToString(rAttachment["AttachmentName"]);
                            objAttachment.AttachmentPath = Convert.ToString(rAttachment["AttachmentPath"]);
                            objAttachment.FileID = Convert.ToString(rAttachment["FileID"]);
                            objAttachment.Description = Convert.ToString(rAttachment["Description"]);
                            objAttachment.CreatedDate = Convert.ToDateTime(rAttachment["CreatedDate"]);
                            objAttachment.CreatedUser = Convert.ToString(rAttachment["CreatedUser"]);
                            objAttachment.CreatedFullName = Convert.ToString(rAttachment["CreatedFullName"]);
                            objAttachment.STT = ++intSTT;
                            lstInputChangeOrder_Attachment.Add(objAttachment);
                        }
                    }
                }
                else
                    grvAttachment.Columns[grvAttachment.Columns.Count - 1].Visible = false;
                if (bolIsAllowEdit)
                    mnuAttachment.Enabled = true;
                else
                    mnuAttachment.Enabled = false;
                grdAttachment.DataSource = lstInputChangeOrder_Attachment;
                grdAttachment.RefreshDataSource();
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo tập tin đính kèm", objExce, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private bool InitFlexGridWorkFlow()
        {
            try
            {
                if (strInputChangeOrderID != string.Empty)
                {
                    DataTable tblWorkFlow = objPLCInputChangeOrder.GetWorkFlow(new object[] { "@InputChangeOrderID", strInputChangeOrderID });
                    flexWorkFlow.DataSource = tblWorkFlow;
                    FormatFlexGridWorkFlow(flexWorkFlow);

                    DataRow[] drWflow = tblWorkFlow.Select("IsProcessed = 0");
                    if (drWflow.Length > 0)
                        intWorkFlowCurrent = Convert.ToInt32(drWflow[0]["InputChangeOrderStepID"]);

                    //if (objInputChangeOrder.IsDeleted || objInputChangeOrder.IsReviewed)
                    //    flexWorkFlow.AllowEditing = false;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo flexCost", objExce, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private void EnableControlWhenSave(bool bolEnable)
        {
            btnAdd.Enabled = bolEnable;
            //   chkCreateSaleOrder.Enabled = bolEnable;
            chkCreateSaleOrder.Enabled = false;
            btnCreateConcern.Enabled = bolEnable;
            btnUpdate.Enabled = bolEnable;
            btnReviewList.Enabled = bolEnable;
            btnDelete.Enabled = bolEnable;
            btnSupportCare.Enabled = bolEnable;
        }

        private bool InitFlexGridReviewLevel()
        {
            try
            {
                if (strInputChangeOrderID != string.Empty)
                {
                    DataTable tblReviewTable = objPLCInputChangeOrder.GetReviewList(new object[]{
                                "@InputChangeOrderTypeID", objInputChangeOrderType.InputChangeOrderTypeID,
                                "@InputChangeOrderID", strInputChangeOrderID});
                    flexReviewLevel.DataSource = tblReviewTable;
                }
                else
                {
                    DataTable tblReviewTable = objPLCInputChangeOrder.GetReviewList(new object[]{
                                "@InputChangeOrderTypeID", objInputChangeOrderType.InputChangeOrderTypeID});
                    flexReviewLevel.DataSource = tblReviewTable;
                }
                FormatFlexGridReviewLevel(flexReviewLevel);
                bool bolIsError = false;
                if (strInputChangeOrderID != string.Empty)
                    SetReviewLevelCurrent(ref bolIsError);
                if (bolIsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi khởi tạo lưới mức duyệt");
                    return false;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo flexReviewLevel", objExce, DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khởi tạo lưới mức duyệt");
                return false;
            }
            return true;
        }
        private void FormatFlexGridWorkFlow(C1.Win.C1FlexGrid.C1FlexGrid _flex)
        {
            for (int i = 0; i < _flex.Cols.Count; i++)
            {
                _flex.Cols[i].Visible = false;
                _flex.Cols[i].AllowSorting = false;
                _flex.Cols[i].AllowDragging = false;
                _flex.Cols[i].AllowEditing = false;
            }
            C1FlexGridObject.SetColumnVisible(_flex, true, "InputChangeOrderStepID,InputChangeOrderStepName,IsProcessed,ProcessFullName,ProcessedTime,Note");
            C1FlexGridObject.SetColumnCaption(_flex, "InputChangeOrderStepID,Mã bước xử lý,InputChangeOrderStepName,Tên bước xử lý,IsProcessed,Đã xử lý,ProcessFullName,Người xử lý,ProcessedTime,Thời gian xử lý,Note,Ghi chú");
            C1FlexGridObject.SetColumnWidth(_flex, "InputChangeOrderStepID,80,InputChangeOrderStepName,180,IsProcessed,70,ProcessFullName,250,ProcessedTime,80,Note,500");

            _flex.Cols["IsProcessed"].DataType = typeof(bool);
            _flex.Cols["ProcessedTime"].Format = "dd/MM/yyyy HH:mm";

            C1FlexGridObject.SetColumnAllowEditing(_flex, true, "IsProcessed", "Note");

            C1.Win.C1FlexGrid.CellStyle style = _flex.Styles.Add("flexStyle");
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            style.WordWrap = true;
            C1.Win.C1FlexGrid.CellRange range = _flex.GetCellRange(0, 0, 0, _flex.Cols.Count - 1);
            range.Style = style;
            _flex.Rows[0].Height = 54;
            _flex.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
        }
        private void FormatFlexGridReviewLevel(C1.Win.C1FlexGrid.C1FlexGrid _flex)
        {
            for (int i = 0; i < _flex.Cols.Count; i++)
            {
                _flex.Cols[i].Visible = false;
                _flex.Cols[i].AllowSorting = false;
                _flex.Cols[i].AllowDragging = false;
                _flex.Cols[i].AllowEditing = false;
            }
            C1FlexGridObject.SetColumnVisible(_flex, true, "ReviewLevelName,UserFullName,ReviewStatusName,ReviewedDate,Note");
            C1FlexGridObject.SetColumnCaption(_flex, "ReviewLevelName,Mức duyệt,UserFullName,Người duyệt,ReviewStatusName,Trạng thái,ReviewedDate,Ngày duyệt,Note, Ghi chú");
            C1FlexGridObject.SetColumnWidth(_flex, "ReviewLevelName,200,UserFullName,200,ReviewStatusName,120," +
                "ReviewedDate,120,Note,150");
            _flex.Cols["ReviewedDate"].Format = "dd/MM/yyyy HH:mm";

            C1.Win.C1FlexGrid.CellStyle style = _flex.Styles.Add("flexStyle");
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = _flex.GetCellRange(0, 0, 0, _flex.Cols.Count - 1);
            range.Style = style;
            _flex.Rows[0].Height = 30;
            _flex.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
        }
        string strPermissionReview = string.Empty;
        private void SetReviewLevelCurrent(ref bool bolIsError)
        {
            bolIsError = false;
            if (strInputChangeOrderID == string.Empty)
                return;
            if (flexReviewLevel.Rows.Count <= flexReviewLevel.Rows.Fixed)
                return;
            try
            {
                DataRow[] rowsReviewStatus = ((DataTable)flexReviewLevel.DataSource).Select("REVIEWSTATUS=" + (int)PLCInputChangeOrder.ReviewStatus.CANCEL);
                if (rowsReviewStatus.Length > 0)
                {

                    string status = "\"Từ chối\"";
                    lblReviewStatus.Text = "Yêu cầu đã được duyệt " + status + " bởi " + rowsReviewStatus[0]["UserFullName"].ToString() + " vào lúc " + ((DateTime)rowsReviewStatus[0]["ReviewedDate"]).ToString("dd/MM/yyyy HH:mm");
                    lblReviewStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;

                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;

                    cboReason.Enabled = false;

                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                    return;

                }
                for (int i = flexReviewLevel.Rows.Fixed; i < flexReviewLevel.Rows.Count; i++)
                {
                    int intCurrentStatus = Convert.ToInt32(flexReviewLevel[i, "ReviewStatus"]);
                    if (((DataTable)flexReviewLevel.DataSource).Select("ReviewLevelID = " + Convert.ToInt32(flexReviewLevel[i, "ReviewLevelID"]) + " AND ReviewStatus > " + intCurrentStatus).Length > 0)
                    {
                        for (int iNext = i + 1; iNext < flexReviewLevel.Rows.Count; iNext++)
                        {
                            if (Convert.ToInt32(flexReviewLevel[iNext, "ReviewLevelID"]) == Convert.ToInt32(flexReviewLevel[i, "ReviewLevelID"]) && Convert.ToInt32(flexReviewLevel[iNext, "ReviewStatus"]) == (int)PLCInputChangeOrder.ReviewStatus.ACCEPT)
                                if (iNext + 1 < flexReviewLevel.Rows.Count && Convert.ToInt32(flexReviewLevel[iNext + 1, "ReviewLevelID"]) != Convert.ToInt32(flexReviewLevel[i, "ReviewLevelID"]))
                                {
                                    i = iNext + 1;
                                    intCurrentStatus = Convert.ToInt32(flexReviewLevel[i, "ReviewStatus"]);
                                    break;
                                }
                        }
                    }
                    if (i == flexReviewLevel.Rows.Count - 1 && (intCurrentStatus == (int)PLCInputChangeOrder.ReviewStatus.CANCEL
                        || intCurrentStatus == (int)PLCInputChangeOrder.ReviewStatus.ACCEPT
                        ))
                    {
                        string status = "\"Đồng ý\"";
                        if (intCurrentStatus == (int)PLCInputChangeOrder.ReviewStatus.CANCEL)
                            status = "\"Từ chối\"";
                        lblReviewStatus.Text = "Yêu cầu đã được duyệt " + status + " bởi " + flexReviewLevel[i, "UserFullName"].ToString() + " vào lúc " + ((DateTime)flexReviewLevel[i, "ReviewedDate"]).ToString("dd/MM/yyyy HH:mm");
                        lblReviewStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
                    }
                    if (intCurrentStatus != (int)PLCInputChangeOrder.ReviewStatus.ACCEPT)
                    {
                        strPermissionReview = flexReviewLevel[i, "ReviewFunctionID"].ToString();
                        C1FlexGridObject.SetRowBackColor(flexReviewLevel, i, SystemColors.Info);
                        //if (SystemConfig.objSessionUser.IsPermission(strPermissionReview)
                        //    || objInputChangeOrderType.IsOnlyCertifyFinger
                        //    )
                        //{
                        btnReviewList.Visible = true;
                        intReviewLevelCurrent = Convert.ToInt32(flexReviewLevel[i, "ReviewLevelID"]);
                        bolIsFinalLevel = (i == flexReviewLevel.Rows.Count - 1);
                        for (int j = intCurrentStatus + 2; j < 4; j++)
                        {
                            DevExpress.XtraBars.BarButtonItem barItem = new DevExpress.XtraBars.BarButtonItem();
                            string strCaption = string.Empty;
                            switch ((PLCInputChangeOrder.ReviewStatus)j)
                            {
                                //case PLCInputChangeOrder.ReviewStatus.INPROCESS: strCaption = "Đang xử lý";
                                //    barItem.ImageIndex = 0;
                                //    break;
                                case PLCInputChangeOrder.ReviewStatus.CANCEL:
                                    strCaption = "Từ chối";
                                    barItem.ImageIndex = 1;
                                    break;
                                case PLCInputChangeOrder.ReviewStatus.ACCEPT:
                                    strCaption = "Đồng ý";
                                    barItem.ImageIndex = 2;
                                    break;
                            }
                            barItem.Caption = strCaption;
                            barItem.Name = "btnReviewLevel" + j;
                            barItem.Tag = j;
                            barItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
                            pMnu.AddItem(barItem);
                        }
                        //}
                        break;
                    }
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo flexReviewLevel", objExce, DUIInventory_Globals.ModuleName);
            }
        }

        private void ActionCloseForm()
        {
            flexDetail.Enabled = false;
            tmrCloseForm.Enabled = true;
            tmrCloseForm.Start();
        }

        private void InitControl()
        {
            lblReviewStatus.Text = string.Empty;
            btnReviewList.Visible = false;
            btnDelete.Visible = false;
            btnUpdate.Visible = false;
            btnSupportCare.Visible = false;
            chkCreateSaleOrder.Visible = false;
        }

        void barItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraBars.BarButtonItem barItem = e.Item as DevExpress.XtraBars.BarButtonItem;

            if (!Globals.ValidatingSystemDate(this))
                return;

            DataTable tblRv = (DataTable)flexReviewLevel.DataSource;
            if (tblRv == null)
                tblRv = (DataTable)flexReviewLevel.DataSource;
            DataRow[] drReviewLevelCurrent = tblRv.Select("ReviewLevelID = " + intReviewLevelCurrent);
            DataTable tblWF = (DataTable)flexWorkFlow.DataSource;
            if (tblWF == null)
                tblWF = (DataTable)flexWorkFlow.DataSource;
            if (tblWF != null && tblWF.Rows.Count > 0 && Convert.ToInt32(drReviewLevelCurrent[0]["PrecedingOrderStepID"]) > 0)
            {
                DataRow[] drWorkFlow = tblWF.Select("InputChangeOrderStepID = " + Convert.ToInt32(drReviewLevelCurrent[0]["PrecedingOrderStepID"]));
                if (drWorkFlow.Length > 0 && (!Convert.ToBoolean(drWorkFlow[0]["IsProcessed2"])))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng xử lý trước bước xử lý [" + Convert.ToString(drWorkFlow[0]["InputChangeOrderStepID"]) + " - " + Convert.ToString(drWorkFlow[0]["InputChangeOrderStepName"]) + "]");
                    tabControl.SelectedTab = tabPageWorkflow;
                    return;
                }
            }
            ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_ReviewList objInputChangeOrder_ReviewList = new ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_ReviewList();
            objInputChangeOrder_ReviewList.IsReviewByFinger = false;
            string strReviewString = "\"" + barItem.Caption.ToLower() + "\"";
            bool IsAllowReview = false;
            string ReviewList_UserName = SystemConfig.objSessionUser.UserName;
            if (SystemConfig.objSessionUser.IsPermission(strPermissionReview)
                && !objInputChangeOrderType.IsOnlyCertifyFinger)
            {
                if (MessageBox.Show("Bạn có muốn " + strReviewString.ToLower() + " yêu cầu " + strMessageForm + " này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                IsAllowReview = true;
            }
            else
            {

                List<string> lstContainsUser = new List<string>();
                lstContainsUser.Add(strPermissionReview);
                Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận " + strReviewString + " yêu cầu đổi trả", "Xác nhận", null, lstContainsUser, !objInputChangeOrderType.IsOnlyCertifyFinger, true);
                //Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận duyệt yêu cầu đổi trả", "Xác nhận", null, lstContainsUser, false, true);
                frmCertifyLogin1.ShowDialog(this);
                if (!frmCertifyLogin1.IsCorrect)
                    return;
                ReviewList_UserName = frmCertifyLogin1.UserName;
                IsAllowReview = true;
                objInputChangeOrder_ReviewList.IsReviewByFinger = frmCertifyLogin1.IsCertifyByFinger;
            }
            if (IsAllowReview)
            {
                if (objOutputVoucherLoad != null)
                {
                    if (objInputChangeOrderType.AllowReturnDaysFrom == 0 && objInputChangeOrderType.AllowReturnDaysTo == 0)
                    {
                        if (DateTime.Now.Date != objOutputVoucherLoad.OutputDate.Value.Date)
                        {
                            MessageBox.Show(this, "Loại yêu cầu đổi trả " + objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép đổi trả trong ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }

                    //double Numdays = (objOutputVoucherLoad.OutputDate.Value.Date - DateTime.Now.Date).TotalDays + 0.5;
                    //if (Numdays < objInputChangeOrderType.AllowReturnDaysFrom || Numdays > objInputChangeOrderType.AllowReturnDaysTo)
                    //{
                    //    MessageBox.Show(this, "Loại yêu cầu đổi trả " + objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép đổi trả từ " + objInputChangeOrderType.AllowReturnDaysFrom + " đến " + objInputChangeOrderType.AllowReturnDaysTo, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //    return;
                    //}
                }
                objInputChangeOrder_ReviewList.InputChangeOrderID = strInputChangeOrderID;
                objInputChangeOrder_ReviewList.ReviewLevelID = intReviewLevelCurrent;
                objInputChangeOrder_ReviewList.RowSCN = objInputChangeOrder.RowSCN;
                objInputChangeOrder_ReviewList.UserName = ReviewList_UserName;
                objInputChangeOrder_ReviewList.ReviewStatus = Convert.ToInt32(barItem.Tag);
                if (objInputChangeOrder_ReviewList.ReviewStatus == 3)
                    objInputChangeOrder_ReviewList.AutoUpdateOrderStepID = Convert.ToInt32(drReviewLevelCurrent[0]["AutoUpdateOrderStepID"]);

                objPLCInputChangeOrder.UpdateReviewList(objInputChangeOrder_ReviewList, flexReviewLevel.Rows.Count - flexReviewLevel.Rows.Fixed);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                }
                else
                {
                    MessageBox.Show("Duyệt " + strReviewString + " yêu cầu " + strMessageForm + " thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.bolIsHasAction = true;
                this.Close();
            }
        }

        #endregion

        #region event
        int intNumDayAfterEdit = 0;
        private void frmInputChangeOrder_Load(object sender, EventArgs e)
        {
            InitControl(); // Khoi tao control

            // Lay danh sach loai nhap doi tra
            objPLCInputChangeOrderType.LoadInfo(ref objInputChangeOrderType, intInputChangeOrderTypeID);
            strInputChangeOrderTypeIdSub = Library.AppCore.AppConfig.GetConfigValue("MD_INPUTCHANGEORDERTYPE_SUB").ToString();

            //Nạp danh sách trạng thái sản phẩm khai báo theo loại y/c
            objPLCInputChangeOrderType.LoadProductStatusTemp(ref lstInputChangeOrderType_PS, intInputChangeOrderTypeID);
            DataTable dtbInstockStatus = null;
            objPLCInputChangeOrderType.GetProductStatusTemp(ref dtbInstockStatus);
            if (dtbInstockStatus != null)
            {
                hstbInstockStatus = new Hashtable();
                dtbInstockStatus.Columns[0].ColumnName = "INSTOCKSTATUSID_IN";
                foreach (DataRow item in dtbInstockStatus.Rows)
                {
                    hstbInstockStatus.Add(item["INSTOCKSTATUSID_IN"], item["PRODUCTSTATUSNAME"]);
                }
            }

            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                EnableControl(false);
                strMessageCloseForm = Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail;
                ActionCloseForm();
                return;
            }
            if (Convert.ToString(objInputChangeOrderType.AddFunctionID).Trim() == string.Empty)
            {
                EnableControl(false);
                strMessageCloseForm = "Quyền thêm yêu cầu " + strMessageForm + " chưa được khai báo! Vui lòng kiểm tra lại";
                ActionCloseForm();
                return;
            }

            // Get Quyen khoi tao
            bolIsHasPermission_Create = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.AddFunctionID);
            bolIsHasPermission_Edit = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.EditFunctionID);
            bolIsHasPermission_EditAll = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.EditAllFunctionID);
            bolIsHasPermission_Delete = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.DeleteFunctionID);
            bolIsHasPermission_DeleteAll = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.DeleteAllFunctionID);
            bolIsHasPermission_AddAttachment = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.AddAttachmentFunctionID);
            bolIsHasPermission_DelAttachment = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.DeleteAttachmentFunctionID);
            bolIsHasPermission_ViewAttachment = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.ViewAttachmentFunctionID);
            bolIsHasPermission_AfterEditFunction = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.AfterEditFunctionID);
            bolIsHasPermission_AfterEditLimitFunction = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.AfterEditLimitFunctionID);
            bolIsHasPermission_ViewInvoice = SystemConfig.objSessionUser.IsPermission(strPermission_View);
            intNumDayAfterEdit = objInputChangeOrderType.NumDayAfterEdit;

            if (objInputChangeOrderType.IsInputReturn)
                strMessageForm = "nhập trả hàng";
            if ((!bolIsHasPermission_Create) && strInputChangeOrderID == string.Empty)
            {
                EnableControl(false);
                strMessageCloseForm = "Bạn không có quyền thêm yêu cầu " + strMessageForm + "! Vui lòng kiểm tra lại";
                ActionCloseForm();
                return;
            }
            if (!LoadCombobox())
            {
                ActionCloseForm();
                return;
            }
            dtbMachineErrorList = objPLCInputChangeOrder.GetMachineError(strInputChangeOrderID);
            if (dtbMachineErrorList != null)
                dtbMachineErrorList.TableName = "TableMachineErrorList";
            if (strInputChangeOrderID != string.Empty) // Load Thong Tin Man HInh
            {
                objInputChangeOrder = objPLCInputChangeOrder.LoadInfo(strInputChangeOrderID);
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    EnableControl(false);
                    strMessageCloseForm = Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail;
                    ActionCloseForm();
                    return;
                }
                txtSaleOrderID.Text = objInputChangeOrder.SaleOrderID;
            }
            if (!LoadOutputVoucher(ref strMessageCloseForm))
            {
                ActionCloseForm();
                return;
            }
            LoadInputType();

            if (strInputChangeOrderID == string.Empty)
            {
                FormState = FormStateType.ADD;
                btnAdd.Enabled = bolIsHasPermission_Create;
                //    chkCreateSaleOrder.Enabled = bolIsHasPermission_Create;
                chkCreateSaleOrder.Enabled = false;
                if (objInputChangeOrderType.IsInputReturn)
                {
                    chkCreateSaleOrder.Visible = false;
                }
                // else 
                //     chkCreateSaleOrder.Visible = true;
                bolIsAllowEdit = btnAdd.Enabled;
                btnDelete.Enabled = false;
                cboStore.Enabled = true;
                if (!objInputChangeOrderType.IsInputReturn)
                    lblInstruction.Visible = true;

                tabControl.TabPages.Remove(tabPageInputChangeOrder);

                tabControl.TabPages.Remove(tabPageReviewList);

                tabControl.TabPages.Remove(tabPageWorkflow);

                lblTitlePayment.Visible = false;
                txtReturnAmount.Visible = false;
                btnCreateConcern.Visible = false;
                chkApplyErrorType.Checked = bolApplyErrorType;
            }
            else
            {
                FormState = FormStateType.EDIT;
                btnAdd.Visible = false;
                chkCreateSaleOrder.Visible = false;
                label1.Visible = false;

                label3.Visible = false;
                label4.Visible = false;
                txtContentIOT.Visible = false;
                cboReasonOV.Visible = false;
                txtReasonNoteOV.Visible = false;
                btnUpdate.Visible = true;
                bolIsHasPermission_UpdateCare = SystemConfig.objSessionUser.IsPermission(strRightUpdateCare);
                if (objInputChangeOrder.IsDeleted)
                {
                    FormState = FormStateType.VIEW;
                    bolIsAllowEdit = false;
                    lblContent.Text = "Nội dung hủy:";
                    lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " đã bị hủy";
                    lblReviewStatus.ForeColor = Color.Red;
                    btnUpdate.Visible = false;
                }
                else
                {
                    btnDelete.Visible = true;
                    if (objInputChangeOrder.IsReviewed)
                    {
                        lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " đã được duyệt";
                        EnableControl(false);
                        btnDelete.Visible = false;
                        btnDelete.Enabled = false;
                        btnCreateConcern.Enabled = bolIsHasPermission_EditAll || (bolIsHasPermission_Edit && objInputChangeOrder.CreatedUser.Equals(SystemConfig.objSessionUser.UserName));
                    }
                    else
                    {
                        // flexDetail.Cols["MACHINEERRORNAME"].ComboList = "...";
                        // flexDetail.Cols["MACHINEERRORNAME"].AllowEditing = true;
                        lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " chưa duyệt";
                        btnDelete.Enabled = bolIsHasPermission_DeleteAll || (bolIsHasPermission_Delete && objInputChangeOrder.CreatedUser.Equals(SystemConfig.objSessionUser.UserName));
                    }
                    btnSupportCare.Visible = objInputChangeOrderType.IsInputReturn && bolIsHasPermission_UpdateCare && (((DataTable)flexDetail.DataSource).Select("IMEI IS NULL OR TRIM(IMEI)=''").Length > 0);
                }

                txtInputChangeOrderID.Text = objInputChangeOrder.InputChangeOrderID;
                dtpInputChangeOrderDate.Value = (DateTime)objInputChangeOrder.InputChangeOrderDate;

                cboInputChangeOrderStoreID.SetValue(objInputChangeOrder.InputChangeOrderStoreID);

                txtInVoucherID.Text = objInputChangeOrder.InVoucherID;
                txtNewOutputVoucherID.Text = objInputChangeOrder.NewOutputVoucherID;
                txtOutVoucherID.Text = objInputChangeOrder.OutVoucherID;
                txtNewInputVoucherID.Text = objInputChangeOrder.NewInputVoucherID;
                txtSaleOrderID.Text = objInputChangeOrder.SaleOrderID;
                chkApplyErrorType.Checked = objInputChangeOrder.ApplyErrorType;

                DataTable dtbChangeOrder = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_CHANGEORDER_OUT_SELINVOICE", new object[] { "@InputChangeOrderId", objInputChangeOrder.InputChangeOrderID });
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->frmInputChangeOrder_Load");
                }
                txtInvoiceIdOut.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["INVOICEIDOUTPUT"].ToString() : string.Empty;
                txtInvoiceSymbolOutput.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["INVOICESYMBOLOUTPUT"].ToString() : string.Empty;
                txtDenominatorOutput.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["DENOMINATOROUTPUT"].ToString() : string.Empty;

                dtbChangeOrder = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_CHANGEORDER_SELINVOICE", new object[] { "@InputChangeOrderId", objInputChangeOrder.InputChangeOrderID });
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->frmInputChangeOrder_Load");
                }
                txtInvoiceIdReturn.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["INVOICEIDINPUT"].ToString() : string.Empty;
                txtInvoiceSymbolReturn.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["INVOICESYMBOLINPUT"].ToString() : string.Empty;
                txtDenominatorReturn.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["DENOMINATORINPUT"].ToString() : string.Empty;
                if (objInputChangeOrder.InputChangeReasonID > 0)
                    cboReason.SetValue(objInputChangeOrder.InputChangeReasonID);
                else cboReason.ResetDefaultValue();
                txtReasonNote.Text = objInputChangeOrder.InputChangeReasonNote.Trim().Replace("\n", "\r\n");
                if ((!objInputChangeOrderType.IsInputReturn) && !string.IsNullOrEmpty(txtSaleOrderID.Text))
                {
                    if (flexDetail.Cols.Contains("ProductID_Out"))
                    {
                        flexDetail.Cols["OutputTypeName_Out"].Visible = false;
                        flexDetail.Cols["ProductID_Out"].Visible = false;
                        flexDetail.Cols["ProductName_Out"].Visible = false;
                        flexDetail.Cols["IMEI_Out"].Visible = false;
                        flexDetail.Cols["SalePrice_Out"].Visible = false;

                        CustomizeC1FlexGrid.SetColumnWidth(flexDetail, "ProductID,190,ProductName,210,IMEI,190,InputPrice_In,140,EndWarrantyDate,150,MACHINEERRORGROUPID,120,MACHINEERRORID,120,Quantity,80");
                        if (objSaleOrder_Out == null)
                            objSaleOrder_Out = objPLCSaleOrders.LoadInfo(objInputChangeOrder.SaleOrderID);
                        if (objSaleOrder_Out != null && !string.IsNullOrEmpty(objSaleOrder_Out.SaleOrderID))
                        {
                            txtCustomerID.Text = objSaleOrder_Out.CustomerID.ToString();
                            txtCustomerName.Text = objSaleOrder_Out.CustomerName;
                            txtCustomerAddress.Text = objSaleOrder_Out.CustomerAddress;
                            txtCustomerPhone.Text = objSaleOrder_Out.CustomerPhone;
                            txtTaxID.Text = objSaleOrder_Out.CustomerTaxID;
                        }
                    }
                }
                ucCreateUser.UserName = objInputChangeOrder.CreatedUser;
                txtContent.Text = objInputChangeOrder.InputChangeOrderContent.Trim().Replace("\n", "\r\n");
                if (objInputChangeOrder.IsDeleted)
                    txtContent.Text = objInputChangeOrder.ContentDeleted;
                chkIsReviewed.Checked = objInputChangeOrder.IsReviewed;
                if (objInputChangeOrder.ReviewedDate != null)
                    dtpReviewedDate.DateTime = (DateTime)objInputChangeOrder.ReviewedDate;
                else
                    dtpReviewedDate.EditValue = null;
                chkIsInputchange.Checked = objInputChangeOrder.IsInputChanged;
                if (objInputChangeOrder.InputChangeDate != null)
                    dtpInputchangeDate.DateTime = (DateTime)objInputChangeOrder.InputChangeDate;
                else
                    dtpInputchangeDate.EditValue = null;

                cboStore.SetValue(objInputChangeOrder.InputChangeOrderStoreID);
                DataTable dbStore = ((DataTable)(cboStore.DataSource));

                if (dbStore == null || dbStore.Select("STOREID = " + objInputChangeOrder.InputChangeOrderStoreID.ToString()).Length == 0) // && ((DataTable)(cboStore.DataSource)).Rows.Count > 0)
                {

                    strMessageCloseForm = "Bạn không có quyền trên kho!";
                    ActionCloseForm();
                    return;
                }

                if ((!objInputChangeOrderType.IsInputReturn) && !string.IsNullOrEmpty(txtSaleOrderID.Text))
                {
                    if (objSaleOrder_Out == null)
                        objSaleOrder_Out = objPLCSaleOrders.LoadInfo(objInputChangeOrder.SaleOrderID);
                }

                if (chkIsInputchange.Checked) // Khi Chack Xu ly đã duoc check 
                {
                    btnCreateConcern.Visible = false;
                    btnSupportCare.Visible = false;
                    bolIsAllowEdit = false;
                    lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " đã được xử lý";
                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;
                    cboReason.Enabled = false;
                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                }
                else
                {
                    btnUpdate.Enabled = bolIsHasPermission_EditAll || (bolIsHasPermission_Edit && objInputChangeOrder.CreatedUser.Equals(SystemConfig.objSessionUser.UserName));
                    if (btnUpdate.Enabled)
                    {
                        txtContent.ReadOnly = false;
                        txtContent.BackColor = SystemColors.Window;
                        cboReason.Enabled = true;
                        txtReasonNote.BackColor = SystemColors.Window;
                        txtReasonNote.ReadOnly = false;
                    }
                }

                if (objInputChangeOrder.IsDeleted) // Neu Đã bi xoa 
                {
                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;
                    cboReason.Enabled = false;
                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                }

                txtInputTypeName.Text = cboInputTypeID.Text;
                cboInputTypeID.Visible = false;
                lblInputType.Location = new Point(591, 232);
                txtInputTypeName.Visible = true;
                txtStoreName.Text = cboStore.Text;
                cboStore.Visible = false;
                txtStoreName.Visible = true;
                txtInputChangeOrderStoreName.Text = cboInputChangeOrderStoreID.Text;
                cboInputChangeOrderStoreID.Visible = false;
                txtInputChangeOrderStoreName.Visible = true;
                txtCreateUser.Text = ucCreateUser.UserName + " - " + ucCreateUser.FullName;
                ucCreateUser.Visible = false;
                txtCreateUser.Visible = true;

                if (objInputChangeOrderType.IsAutoReview)
                {

                    tabControl.TabPages.Remove(tabPageReviewList);
                    tabControl.TabPages.Remove(tabPageWorkflow);
                }
                else
                {
                    if (!InitFlexGridWorkFlow())
                    {
                        //MessageBoxObject.ShowWarningMessage(this, "Lỗi khởi tạo lưới bước xử lý");
                        strMessageCloseForm = "Lỗi khởi tạo lưới bước xử lý";
                        ActionCloseForm();
                        return;
                    }

                    if (!InitFlexGridReviewLevel())
                    {
                        ActionCloseForm();
                        return;
                    }

                    DataRow[] drReviewStatus = ((DataTable)flexReviewLevel.DataSource).Select("ReviewStatus = 2");
                    if (drReviewStatus.Length > 0)
                    {
                        intReviewStatus = 2;
                    }
                }
            }
            if (!InitFlexGridAttachment())
            {
                strMessageCloseForm = "Lỗi khởi tạo lưới tập tin đính kèm";
                ActionCloseForm();
                return;
            }
            if (!bolIsAllowEdit)
            {
                EnableControl(bolIsAllowEdit || (bolIsHasPermission_Create && strInputChangeOrderID == string.Empty));
                btnDelete.Enabled = false;
                btnDownload.Enabled = false;
                btnReviewList.Enabled = false;
                btnUpdate.Enabled = false;
                btnAdd.Enabled = false;
                chkCreateSaleOrder.Enabled = false;
                if (bolIsHasPermission_Create && strInputChangeOrderID == string.Empty)
                {
                    btnAdd.Enabled = true;
                    //  if (!objInputChangeOrderType.IsInputReturn)
                    //  chkCreateSaleOrder.Enabled = true;
                }
            }
            else
            {
                mnuAddAttachment.Enabled = btnUpdate.Enabled;
                mnuDelAttachment.Enabled = btnUpdate.Enabled;
                if (btnUpdate.Visible && !btnUpdate.Enabled)
                {
                    mnuAddAttachment.Enabled = bolIsHasPermission_AddAttachment;
                    mnuDelAttachment.Enabled = bolIsHasPermission_DelAttachment;
                    btnUpdate.Enabled = (bolIsHasPermission_AddAttachment || bolIsHasPermission_DelAttachment);
                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;

                    cboReason.Enabled = false;

                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                }
                if (intReviewStatus == 2)
                {
                    EnableControl(false);
                    btnCreateConcern.Enabled = false;
                    btnDownload.Enabled = false;
                    btnReviewList.Enabled = false;
                    btnAdd.Enabled = false;
                    chkCreateSaleOrder.Enabled = false;
                    btnUpdate.Enabled = false;
                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;
                    cboReason.Enabled = false;
                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                }
                if (strInputChangeOrderID != string.Empty && objInputChangeOrder.IsReviewed)
                {
                    if (!chkIsInputchange.Checked)
                    {
                        if (!objInputChangeOrder.IsDeleted)
                        {
                            btnDelete.Visible = SystemConfig.objSessionUser.IsPermission("PM_INPUTCHANGEORDER_DELETE_AFTERREVIEWED");
                            btnDelete.Enabled = btnDelete.Visible;
                        }
                        btnSupportCare.Visible = objInputChangeOrderType.IsInputReturn && bolIsHasPermission_UpdateCare && (((DataTable)flexDetail.DataSource).Select("IMEI IS NULL OR TRIM(IMEI)=''").Length > 0);
                    }
                    else
                    {
                        if (objInputChangeOrder.IsInputReturn && !objInputChangeOrder.IsDeleted)
                        {
                            btnDelete.Visible = objPLCInputChangeOrder.CheckDelete(objInputChangeOrder.NewInputVoucherID, objInputChangeOrder.OutVoucherID);
                            btnDelete.Enabled = btnDelete.Visible;
                        }
                    }
                }
            }
            if (bolIsLockByClosingDataMonth)
            {
                EnableControl(false);
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tháng " + dtOutputDate.Value.Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa");
                strMessageCloseForm = "Tháng " + dtOutputDate.Value.Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa";
                return;
            }
            CheckAfterEditFunction();
            tabControl.SelectedIndex = 0;
            if (evtCloseParentForm != null)
                evtCloseParentForm(this, null);
            lblInvoiceNo.Enabled = bolIsHasPermission_ViewInvoice;
            if (objInputChangeOrderType.IsGetPromotion && string.IsNullOrEmpty(strInputChangeOrderID))
            {
                chkCreateSaleOrder.Visible = true;
                chkCreateSaleOrder.Checked = true;
                chkCreateSaleOrder.Enabled = false;
                // chkCreateSaleOrder_CheckedChanged(null, null);
            }
        }

        bool IsUpdateContent = false;
        public void CheckAfterEditFunction()
        {
            DateTime sysDate = Library.AppCore.Globals.GetServerDateTime();
            // Khi checked da duoc check
            if (chkIsInputchange.Checked && dtpInputchangeDate.EditValue != null)
            {
                DateTime InputchangeDate = (DateTime)dtpInputchangeDate.EditValue;
                // Phai có ngay duoc phep chinh sua hoac co quyen chinh sua
                if (intNumDayAfterEdit > 0 || bolIsHasPermission_AfterEditFunction)
                {
                    //if (bolIsHasPermission_AfterEditLimitFunction && objInputChangeOrder.CreatedUser.Equals(SystemConfig.objSessionUser.UserName)
                    //    && ((InputchangeDate.AddDays(intNumDayAfterEdit - 1).Date - sysDate.Date).Days >= 0))
                    //    IsUpdateContent = true;
                    if (bolIsHasPermission_AfterEditFunction)
                        IsUpdateContent = true;
                    if (bolIsHasPermission_AfterEditLimitFunction && ((InputchangeDate.AddDays(intNumDayAfterEdit - 1).Date - sysDate.Date).Days >= 0))
                        IsUpdateContent = true;
                }
                if (IsUpdateContent)
                {
                    btnUpdate.Enabled = IsUpdateContent;
                    txtContent.BackColor = SystemColors.Window;
                    txtContent.ReadOnly = false;
                    cboReason.Enabled = true;
                    txtReasonNote.BackColor = SystemColors.Window;
                    txtReasonNote.ReadOnly = false;
                }
            }
        }

        private void cboStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (intOldStoreID > 0 && intOldStoreID != cboStore.StoreID)
            {
                if (MessageBox.Show("Khi thay đổi kho nhập, " + (objInputChangeOrderType.IsInputReturn ? "bạn phải chọn lại sản phẩm nhập trả!" : "dữ liệu chi tiết xuất vào sẽ bị xóa!") + "\n Bạn có chắc muốn thay đổi kho nhập không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ClearDetail();
                }
                else
                {
                    cboStore.SetValue(intOldStoreID);
                }
            }
            if (cboStore.StoreID > 0)
            {
                intOldStoreID = cboStore.StoreID;
            }
        }

        private bool UploadAttachment()
        {
            if (lstInputChangeOrder_Attachment == null || lstInputChangeOrder_Attachment.Count < 1)
                return true;
            //Tạo trạng thái chờ trong khi đính kèm tập tin
            try
            {
                if (lstInputChangeOrder_Attachment != null && lstInputChangeOrder_Attachment.Count > 0)
                {
                    var CheckIsNew = from o in lstInputChangeOrder_Attachment
                                     where o.IsAddNew == true
                                     select o;
                    if (CheckIsNew.Count() <= 0)
                    {
                        return true;
                    }
                    string strFilePath = string.Empty;
                    string strFileID = string.Empty;
                    foreach (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objInputChangeOrder_Attachment in CheckIsNew)
                    {
                        string strResult = ERP.FMS.DUI.DUIFileManager.UploadFile(this, strFMSAplication, objInputChangeOrder_Attachment.AttachmentLocalPath, ref strFileID, ref strFilePath);
                        objInputChangeOrder_Attachment.AttachmentPath = strFilePath;
                        objInputChangeOrder_Attachment.FileID = strFileID;
                        if (!string.IsNullOrEmpty(strResult))
                        {
                            MessageBoxObject.ShowWarningMessage(this, strResult);
                            return false;
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool DownloadFile(string strFileName, string strFileID)
        {
            try
            {
                SaveFileDialog dlgSaveFile = new SaveFileDialog();
                dlgSaveFile.Title = "Lưu file tải về";
                dlgSaveFile.FileName = strFileName;
                dlgSaveFile.Filter = "All Files (*.*)|*.*";
                string strExtension = Path.GetExtension(strFileName);
                switch (strExtension)
                {
                    case ".xls":
                    case ".xlsx":
                        dlgSaveFile.Filter = "Excel Worksheets(*.xls, *.xlsx)|*.xls; *.xlsx";
                        break;
                    case ".doc":
                    case ".docx":
                        dlgSaveFile.Filter = "Word Documents(*.doc, *.docx)|*.doc; *.docx";
                        break;
                    case ".pdf":
                        dlgSaveFile.Filter = "PDF Files(*.pdf)|*.pdf";
                        break;
                    case ".zip":
                    case ".rar":
                        dlgSaveFile.Filter = "Compressed Files(*.zip, *.rar)|*.zip;*.rar";
                        break;
                    case ".jpg":
                    case ".jpe":
                    case ".jpeg":
                    case ".gif":
                    case ".png":
                    case ".bmp":
                    case ".tif":
                    case ".tiff":
                        dlgSaveFile.Filter = "Image Files(*.jpg, *.jpe, *.jpeg, *.gif, *.png, *.bmp, *.tif, *.tiff)|*.jpg; *.jpe; *.jpeg; *.gif; *.png; *.bmp; *.tif; *.tiff";
                        break;
                    default:
                        dlgSaveFile.Filter = "All Files (*)|*";
                        break;
                }
                if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                {
                    if (dlgSaveFile.FileName != string.Empty)
                    {
                        Library.AppCore.FTP.FileAttachment objFileAttachment = new Library.AppCore.FTP.FileAttachment();
                        ERP.FMS.DUI.DUIFileManager.DownloadFile(this, strFMSAplication, strFileID, string.Empty, dlgSaveFile.FileName);
                        if (stbAttachmentLog.Length > 0)
                            stbAttachmentLog.Append("\r\n");
                        stbAttachmentLog.Append("Tải về tập tin: " + strFileName);
                    }
                }
                return true;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tải file đính kèm", objExce.ToString(), " frmInputChangeOrder-> DownloadFile", DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi tải file đính kèm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objInputChangeOrder_Attachment = (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
            if (objInputChangeOrder_Attachment.IsAddNew == true)
            {
                MessageBox.Show(this, "File chưa cập nhật trên server!");
                return;
            }
            if (!bolIsHasPermission_ViewAttachment)
            {
                MessageBox.Show(this, "Bạn không có quyền xem file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DownloadFile(objInputChangeOrder_Attachment.AttachmentName, objInputChangeOrder_Attachment.FileID);
        }

        private bool AddInputchangeOrder()
        {
            if (bolIsLockByClosingDataMonth)
            {
                return false;
            }
            try
            {
                EnableControlWhenSave(false);
                if (!CheckAdd())
                {
                    EnableControlWhenSave(true);
                    return false;
                }

                btnAdd.Enabled = false;

                int intStoreID = cboStore.StoreID;
                string strUserName = Library.AppCore.SystemConfig.objSessionUser.UserName;
                int intCreateStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                DateTime dtmInputChangeOrderDate = Library.AppCore.Globals.GetServerDateTime();
                if (objOutputVoucherLoad != null)
                {
                    if (objInputChangeOrderType.AllowReturnDaysFrom == 0 && objInputChangeOrderType.AllowReturnDaysTo == 0)
                    {
                        if (DateTime.Now.Date != objOutputVoucherLoad.OutputDate.Value.Date)
                        {
                            MessageBox.Show(this, "Loại yêu cầu đổi trả " + objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép đổi trả trong ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }

                    //double Numdays = (objOutputVoucherLoad.OutputDate.Value.Date - DateTime.Now.Date).TotalDays + 0.5;
                    //if (Numdays < objInputChangeOrderType.AllowReturnDaysFrom || Numdays > objInputChangeOrderType.AllowReturnDaysTo)
                    //{
                    //    MessageBox.Show(this, "Loại yêu cầu đổi trả " + objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép đổi trả từ " + objInputChangeOrderType.AllowReturnDaysFrom + " đến " + objInputChangeOrderType.AllowReturnDaysTo, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //    return false;
                    //}
                }

                if (/*!chkCreateSaleOrder.Checked ||*/ !objInputChangeOrderType.IsGetPromotion)
                {
                    if (MessageBox.Show("Bạn có chắc muốn " + ((strInputChangeOrderID != string.Empty) ? "chỉnh sửa" : "tạo") + " yêu cầu " + strMessageForm + " này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        EnableControlWhenSave(true);
                        return false;
                    }
                }

                if (lstInputChangeOrder_Attachment_Del != null)
                {
                    lstInputChangeOrder_Attachment.InsertRange(lstInputChangeOrder_Attachment.Count, lstInputChangeOrder_Attachment_Del);
                }
                if (!UploadAttachment())
                {
                    if (MessageBox.Show(this, "Upload tập tin thất bại\r\nBạn có muốn tiếp tục lưu thông tin không?", "Thông báo",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    {
                        return false;
                    }
                }

                ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder objInputChangeOrder = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder();
                objInputChangeOrder.InputChangeOrderTypeID = objInputChangeOrderType.InputChangeOrderTypeID;
                objInputChangeOrder.OldOutputVoucherID = strOutputVoucherID;
                objInputChangeOrder.InputChangeOrderStoreID = intStoreID;
                objInputChangeOrder.InputChangeOrderDate = dtmInputChangeOrderDate;
                objInputChangeOrder.CreatedStoreID = intCreateStoreID;
                objInputChangeOrder.InputChangeOrderContent = txtContentIOT.Text;
                objInputChangeOrder.IsInputReturn = objInputChangeOrderType.IsInputReturn;
                objInputChangeOrder.InputChangeReasonID = cboReasonOV.ColumnID;
                objInputChangeOrder.InputChangeReasonNote = txtReasonNoteOV.Text.Trim();
                objInputChangeOrder.ApplyErrorType = chkApplyErrorType.Checked;
                objInputChangeOrder.BrokerUser = objOutputVoucherLoad.BrokerUser;
                //objInputChangeOrder.objSaleOrder
                DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select("IsSelect = 1");
                List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail> objInputChangeOrderDetailList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail>();
                lstIMEi = new List<string>();

                #region Nếu là hình thức nhập trả do nhân viên xuất sai thì bắt buộc phải trả những khuyến mãi kèm theo

                string strOutputVoucherDetailIDList = string.Empty;
                string strMessagePromotionNeedReturn = string.Empty;
                string strProductList = string.Empty;
                string strCouponCodeList = string.Empty;
                List<string> lstProductList = new List<string>();
                List<string> lstCouponCodeList = new List<string>();
                for (int i = 0; i < drDetail.Length; i++)
                {
                    strOutputVoucherDetailIDList += drDetail[i]["OutputVoucherDetailID"].ToString().Trim() + ",";
                }
                DataTable dtPromotionNeedReturn = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_INPUTCHANGE_GETPROMOTION", new object[] { "@InputChangeOrderTypeID", objInputChangeOrderType.InputChangeOrderTypeID,
                                                                                                                                                       "@OutputVoucherDetailIDList", strOutputVoucherDetailIDList});
                if (dtPromotionNeedReturn != null && dtPromotionNeedReturn.Rows.Count > 0)
                {
                    for (int i = 0; i < drDetail.Length; i++)
                    {
                        DataRow rDetail = drDetail[i];

                        DataRow[] lst = dtPromotionNeedReturn.Select(string.Format(@"OUTPUTVOUCHERDETAILID = '{0}'", rDetail["OutputVoucherDetailID"].ToString().Trim()));
                        if (lst != null && lst.Length > 0)
                        {
                            foreach (DataRow rowPromotion in lst)
                            {
                                if (!drDetail.Any(x => x["OUTPUTVOUCHERDETAILID"].ToString().Trim() == rowPromotion["PROMOTIONOUTPUTVOUCHERDETAILID"].ToString().Trim()))
                                {
                                    if (rowPromotion["COUPONCODE"] != DBNull.Value)
                                    {
                                        if (!lstCouponCodeList.Contains(rowPromotion["COUPONCODE"].ToString().Trim()))
                                            lstCouponCodeList.Add(rowPromotion["COUPONCODE"].ToString().Trim());
                                    }
                                    else
                                    {
                                        if (!lstProductList.Contains(rowPromotion["PRODUCTID"].ToString().Trim()))
                                            lstProductList.Add(rowPromotion["PRODUCTID"].ToString().Trim());
                                    }
                                }
                            }
                        }

                        if (lstProductList.Count > 0 || lstCouponCodeList.Count > 0)
                        {
                            foreach (string strProductID in lstProductList)
                            {
                                strProductList += "\n- " + strProductID;
                            }

                            foreach (string strCouponCode in lstCouponCodeList)
                            {
                                strCouponCodeList += "\n- " + strCouponCode;
                            }

                            strMessagePromotionNeedReturn += string.Format(@"Để nhập trả sản phẩm {0} bạn cần phải trả những khuyến mãi kèm theo:", rDetail["PRODUCTID"].ToString().Trim());
                            if (!string.IsNullOrEmpty(strProductList))
                            {
                                strMessagePromotionNeedReturn += "\nSản phẩm:" + strProductList;
                            }

                            if (!string.IsNullOrEmpty(strCouponCodeList))
                            {
                                strMessagePromotionNeedReturn += "\nCoupon:" + strCouponCodeList;
                            }

                            MessageBox.Show(strMessagePromotionNeedReturn, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            btnAdd.Enabled = true;
                            return false;
                        }
                    }
                }

                #endregion

                if (!objInputChangeOrderType.IsGetPromotion)
                {
                    for (int i = 0; i < drDetail.Length; i++)
                    {
                        DataRow rDetail = drDetail[i];
                        PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail();
                        objInputChangeOrderDetail.ProductID_In = rDetail["ProductID"].ToString().Trim();
                        objInputChangeOrderDetail.IMEI_In = rDetail["IMEI"].ToString().Trim();
                        lstIMEi.Add(rDetail["IMEI"].ToString().Trim());
                        objInputChangeOrderDetail.ProductID_Out = rDetail["ProductID_Out"].ToString().Trim();
                        objInputChangeOrderDetail.IMEI_Out = rDetail["IMEI_Out"].ToString().Trim();
                        objInputChangeOrderDetail.OldOutputVoucherDetailID = rDetail["OutputVoucherDetailID"].ToString().Trim();
                        objInputChangeOrderDetail.InputChangeOrderDate = dtmInputChangeOrderDate;
                        objInputChangeOrderDetail.InputChangeOrderStoreID = intStoreID;
                        objInputChangeOrderDetail.CreatedStoreID = intCreateStoreID;
                        objInputChangeOrderDetail.Quantity = Convert.ToDecimal(rDetail["Quantity"]);
                        objInputChangeOrderDetail.ReturnFee = Convert.ToDecimal(rDetail["TotalFee"]);
                        objInputChangeOrderDetail.ReturnInputTypeID = cboInputTypeID.InputTypeID;
                        objInputChangeOrderDetail.IsNew_In = Convert.ToBoolean(rDetail["IsNew"]);
                        //NLT gán trạng thái sp.
                        objInputChangeOrderDetail.InStockStatusID_In = Convert.ToInt32(rDetail["InStockStatusID_In"]);
                        int intStatus_Out = -1;
                        if (Int32.TryParse(Convert.ToString(rDetail["InStockStatusID_Out"]), out intStatus_Out))
                            objInputChangeOrderDetail.InStockStatusID_Out = intStatus_Out;

                        objInputChangeOrderDetail.VAT_In = Convert.ToInt32(rDetail["VAT"]);
                        objInputChangeOrderDetail.VATPercent_In = Convert.ToInt32(rDetail["VATPercent"]);
                        objInputChangeOrderDetail.IsHasWarranty_In = Convert.ToBoolean(rDetail["IsHasWarranty"]);
                        //Nguyen Van Tai bo sung theo yeu cau cua BA Trung - 30/11/2017
                        int intMachineErrorGroupId = 0;
                        int intMachineErrorId = 0;
                        int.TryParse(rDetail["MACHINEERRORGROUPID"].ToString(), out intMachineErrorGroupId);
                        int.TryParse(rDetail["MACHINEERRORID"].ToString(), out intMachineErrorId);
                        //    objInputChangeOrderDetail.MachineErrorGroupId = intMachineErrorGroupId;
                        objInputChangeOrderDetail.MachineErrorId = intMachineErrorId;

                        objInputChangeOrderDetail.MachineErrorContent = rDetail["MACHINEERRORNAME"].ToString();

                        //End
                        if (rDetail["EndWarrantyDate"] != DBNull.Value)
                            objInputChangeOrderDetail.EndWarrantyDate = Convert.ToDateTime(rDetail["EndWarrantyDate"]);
                        if (!objInputChangeOrderType.IsInputReturn)
                            objInputChangeOrderDetail.OutputTypeID_Out = Convert.ToInt32(rDetail["OutputTypeID_Out"]);

                        DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + cboInputTypeID.InputTypeID);
                        objInputChangeOrderDetail.InputPrice = Convert.ToDecimal(rDetail["InputSalePrice"]);
                        if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 2)
                            objInputChangeOrderDetail.InputPrice = Convert.ToDecimal(rDetail["InputCostPrice"]);
                        else if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 3)
                            objInputChangeOrderDetail.InputPrice = 0;

                        objInputChangeOrderDetail.OutputTypeID = Convert.ToInt32(rDetail["OutputTypeID_Out"]);
                        if (rDetail["SalePrice_Out"] == DBNull.Value)
                            rDetail["SalePrice_Out"] = 0;
                        objInputChangeOrderDetail.SalePrice = Convert.ToDecimal(rDetail["SalePrice_Out"]);

                        if (objInputChangeOrderDetail.ProductID_Out == string.Empty)
                        {
                            if (objInputChangeOrderType.IsInputReturn)
                            {
                                objInputChangeOrderDetail.OutputTypeID = 0;
                                objInputChangeOrderDetail.SalePrice = 0;
                            }
                        }
                        else
                        {
                            if (/*!chkCreateSaleOrder.Checked ||*/ !objInputChangeOrderType.IsGetPromotion)
                            {
                                ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(objInputChangeOrderDetail.OutputTypeID);
                                if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                                {
                                    ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(objInputChangeOrderDetail.ProductID_Out);
                                    objInputChangeOrderDetail.SalePrice = Convert.ToDecimal(rDetail["SalePrice_Out"]) / (Convert.ToDecimal(1) + Convert.ToDecimal(objProductOut.VAT) * Convert.ToDecimal(objProductOut.VATPercent) * Convert.ToDecimal(0.0001));
                                }
                            }
                        }
                        //if (objInputChangeOrderType.IsInputChangeFree)
                        //    objInputChangeOrderDetail.SalePrice = objInputChangeOrderDetail.InputPrice;
                        if (lstIMEIUpdateConcern.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(objInputChangeOrderDetail.IMEI_Out) && lstIMEIUpdateConcern.Any(x => x == objInputChangeOrderDetail.IMEI_Out.Trim()))
                            {
                                objInputChangeOrderDetail.IsUpdateConcern = true;
                            }
                        }
                        objInputChangeOrderDetailList.Add(objInputChangeOrderDetail);
                    }
                    objInputChangeOrder.InputChangeOrderDetailList = objInputChangeOrderDetailList.ToArray();
                }
                else
                {
                    for (int i = 0; i < drDetail.Length; i++)
                    {
                        DataRow rDetail = drDetail[i];
                        lstIMEi.Add(rDetail["IMEI"].ToString().Trim());
                    }
                }


                if (!objInputChangeOrderType.IsAutoReview)
                {
                    objInputChangeOrder.InputChangeOrder_ReviewTable = (DataTable)flexReviewLevel.DataSource;
                }
                else
                {
                    objInputChangeOrder.IsReviewed = true;
                    objInputChangeOrder.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();
                }

                objInputChangeOrder.InputChangeOrder_AttachmentList = lstInputChangeOrder_Attachment.ToArray();

                objInputChangeOrder.InputChangeOrder_WorkFlowTable = objPLCInputChangeOrder.GetWorkFlow(objInputChangeOrderType.InputChangeOrderTypeID);
                objInputChangeOrder.InputChangeOrder_WorkFlowTable.DefaultView.RowFilter = "IsDeleted = 0 AND IsActive = 1";
                objInputChangeOrder.InputChangeOrder_WorkFlowTable.DefaultView.Sort = "ProcessIndex ASC";
                objInputChangeOrder.InputChangeOrder_WorkFlowTable = objInputChangeOrder.InputChangeOrder_WorkFlowTable.DefaultView.ToTable();

                decimal decTotalLiquidate = 0;
                if ((/*chkCreateSaleOrder.Checked ||*/ objInputChangeOrderType.IsGetPromotion) && (!objInputChangeOrderType.IsInputReturn))
                {
                    frmSelectInfo frmCreateSaleOrder = new frmSelectInfo(DUISaleOrders_Common.SelectInfoType.SALEORDERTYPE);
                    frmCreateSaleOrder.IsInputChangeOrder = true;
                    frmCreateSaleOrder.InputChangeOrderDetailList = objInputChangeOrderDetailList;
                    MasterData.PLC.MD.WSCustomer.Customer objCustomerRefer = new MasterData.PLC.MD.WSCustomer.Customer();
                    try
                    {
                        objCustomerRefer.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                    }
                    catch { }
                    List<InputChangeOrderParams> lstInputChangeOrderParams = new List<InputChangeOrderParams>();
                    //if (objInputChangeOrderDetailList != null && objInputChangeOrderDetailList.Count > 0)
                    //{
                    for (int i = 0; i < drDetail.Length; i++)
                    {
                        DataRow rDetail = drDetail[i];
                        //  InputChangeOrderDetail objInputChangeOrderDetail = objInputChangeOrderDetailList[i];
                        ERP.MasterData.PLC.MD.WSProduct.Product objProductIn = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rDetail["ProductID"].ToString().Trim());
                        InputChangeOrderParams InputChangeOrderParams = new InputChangeOrderParams();
                        InputChangeOrderParams.ProductId = rDetail["ProductID"].ToString().Trim();
                        DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + cboInputTypeID.InputTypeID);
                        InputChangeOrderParams.Price = Convert.ToDecimal(rDetail["InputSalePrice"]);
                        if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 2)
                            InputChangeOrderParams.Price = Convert.ToDecimal(rDetail["InputCostPrice"]);
                        else if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 3)
                            InputChangeOrderParams.Price = 0;
                        InputChangeOrderParams.Price = Math.Round(InputChangeOrderParams.Price * (Convert.ToDecimal(Convert.ToInt32(rDetail["VATPercent"]) * Convert.ToInt32(rDetail["VAT"])) / Convert.ToDecimal(10000.0) + 1));
                        InputChangeOrderParams.ModelId = objProductIn.ModelID;
                        lstInputChangeOrderParams.Add(InputChangeOrderParams);
                    }

                    objCustomerRefer.CustomerName = txtCustomerName.Text;
                    objCustomerRefer.CustomerAddress = txtCustomerAddress.Text;
                    objCustomerRefer.CustomerPhone = txtCustomerPhone.Text;
                    objCustomerRefer.CustomerTaxID = txtTaxID.Text;
                    frmCreateSaleOrder.CustomerRefer = objCustomerRefer;
                    frmCreateSaleOrder.StaffUser = objOutputVoucherLoad.StaffUser;
                    frmCreateSaleOrder.ListInputChangeOrderParams = lstInputChangeOrderParams;
                    frmCreateSaleOrder.InputChangeOrderTypeName = objInputChangeOrderType.InputChangeOrderTypeName;
                    frmCreateSaleOrder.ProductChangeConditionType = objInputChangeOrderType.ProductChangeConditionType;
                    frmCreateSaleOrder.IsOnlineOrder = IsOnlineOrder;
                    frmCreateSaleOrder.SaleOrderSourceId = objOutputVoucherLoad.OrderID;
                    //  frmCreateSaleOrder.InputChangeOrderDetailList
                    ERP.SaleProgram.PLC.PLCVTT_VERIFIEDPARTNER objPLCVTTVERIFIEDPARTNER = new ERP.SaleProgram.PLC.PLCVTT_VERIFIEDPARTNER();
                    string strPartnerCode = string.Empty;
                    if (this.lstIMEi.Count > 1)
                    {
                        for (int i = 0; i < this.lstIMEi.Count; i++)
                        {
                            DataTable dtbResult = null;
                            var objResultMessage = objPLCVTTVERIFIEDPARTNER.CheckPartnerCode(ref dtbResult, this.lstIMEi[i]);
                            if (objResultMessage.IsError)
                            {
                                MessageBoxObject.ShowWarningMessage(this, objResultMessage.MessageDetail);
                                return false;
                            }
                            if (dtbResult != null && dtbResult.Rows.Count > 0)
                            {
                                if (!Convert.IsDBNull(dtbResult.Rows[0]["PARTNERCODE"]))
                                {
                                    strPartnerCode = dtbResult.Rows[0]["PARTNERCODE"].ToString();
                                }
                            }

                            if (!string.IsNullOrEmpty(strPartnerCode))
                            {
                                //   chkCreateSaleOrder.Enabled = true;
                                chkCreateSaleOrder.Enabled = false;
                                btnAdd.Enabled = true;
                                MessageBox.Show(this, "Yêu cầu đổi trả sản phẩm Subsidy có tồn tại các sản phẩm không Subsidy.Vui lòng kiểm tra lại ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return false;
                            }
                        }
                    }
                    frmCreateSaleOrder.InputIMEI = lstIMEi;
                    frmCreateSaleOrder.ShowDialog();
                    if (frmCreateSaleOrder.SaleOrder != null && (!string.IsNullOrEmpty(frmCreateSaleOrder.SaleOrder.SaleOrderID)) && frmCreateSaleOrder.SaleOrder.SaleOrderTypeID > 0)
                    {
                        decTotalLiquidate = frmCreateSaleOrder.SaleOrder.TotalLiquidate;
                        objInputChangeOrder.SaleOrderID = frmCreateSaleOrder.SaleOrder.SaleOrderID;

                        //var objSaleOrderDetail = frmCreateSaleOrder.SaleOrder.SaleOrderDetailList.Where(x => x.IsPromotionProduct == false).SingleOrDefault();
                        var objSaleOrderDetail = frmCreateSaleOrder.SaleOrder.SaleOrderDetailList.Where(x => x.IsPromotionProduct == false).FirstOrDefault();
                        if (objSaleOrderDetail != null)
                        {
                            for (int i = 0; i < drDetail.Length; i++)
                            {
                                DataRow rDetail = drDetail[i];
                                PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail();
                                objInputChangeOrderDetail.ProductID_In = rDetail["ProductID"].ToString().Trim();
                                objInputChangeOrderDetail.IMEI_In = rDetail["IMEI"].ToString().Trim();
                                lstIMEi.Add(rDetail["IMEI"].ToString().Trim());
                                objInputChangeOrderDetail.ProductID_Out = objSaleOrderDetail.ProductID.Trim();
                                objInputChangeOrderDetail.IMEI_Out = objSaleOrderDetail.IMEI.Trim();
                                objInputChangeOrderDetail.OldOutputVoucherDetailID = rDetail["OutputVoucherDetailID"].ToString().Trim();
                                objInputChangeOrderDetail.InputChangeOrderDate = dtmInputChangeOrderDate;
                                objInputChangeOrderDetail.InputChangeOrderStoreID = intStoreID;
                                objInputChangeOrderDetail.CreatedStoreID = intCreateStoreID;
                                objInputChangeOrderDetail.Quantity = Convert.ToDecimal(rDetail["Quantity"]);
                                objInputChangeOrderDetail.ReturnFee = Convert.ToDecimal(rDetail["TotalFee"]);
                                objInputChangeOrderDetail.ReturnInputTypeID = cboInputTypeID.InputTypeID;
                                objInputChangeOrderDetail.IsNew_In = objSaleOrderDetail.IsNew;
                                //NLT gán trạng thái sp.
                                objInputChangeOrderDetail.InStockStatusID_In = Convert.ToInt32(rDetail["InStockStatusID_In"]);
                                //    int intStatus_Out = -1;
                                //if (Int32.TryParse(Convert.ToString(rDetail["InStockStatusID_Out"]), out intStatus_Out))
                                objInputChangeOrderDetail.InStockStatusID_Out = 1;

                                objInputChangeOrderDetail.VAT_In = Convert.ToInt32(rDetail["VAT"]);
                                objInputChangeOrderDetail.VATPercent_In = Convert.ToInt32(rDetail["VATPercent"]);
                                objInputChangeOrderDetail.IsHasWarranty_In = Convert.ToBoolean(rDetail["IsHasWarranty"]);
                                //Nguyen Van Tai bo sung theo yeu cau cua BA Trung - 30/11/2017
                                int intMachineErrorGroupId = 0;
                                int intMachineErrorId = 0;
                                int.TryParse(rDetail["MACHINEERRORGROUPID"].ToString(), out intMachineErrorGroupId);
                                int.TryParse(rDetail["MACHINEERRORID"].ToString(), out intMachineErrorId);
                                //    objInputChangeOrderDetail.MachineErrorGroupId = intMachineErrorGroupId;
                                objInputChangeOrderDetail.MachineErrorId = intMachineErrorId;

                                objInputChangeOrderDetail.MachineErrorContent = rDetail["MACHINEERRORNAME"].ToString();

                                //End
                                if (rDetail["EndWarrantyDate"] != DBNull.Value)
                                    objInputChangeOrderDetail.EndWarrantyDate = Convert.ToDateTime(rDetail["EndWarrantyDate"]);
                                if (!objInputChangeOrderType.IsInputReturn)
                                    objInputChangeOrderDetail.OutputTypeID_Out = Convert.ToInt32(objSaleOrderDetail.OutputTypeID);

                                DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + cboInputTypeID.InputTypeID);
                                objInputChangeOrderDetail.InputPrice = Convert.ToDecimal(rDetail["InputSalePrice"]);
                                if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 2)
                                    objInputChangeOrderDetail.InputPrice = Convert.ToDecimal(rDetail["InputCostPrice"]);
                                else if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 3)
                                    objInputChangeOrderDetail.InputPrice = 0;

                                objInputChangeOrderDetail.OutputTypeID = Convert.ToInt32(rDetail["OutputTypeID_Out"]);
                                //if (rDetail["SalePrice_Out"] == DBNull.Value)
                                //    rDetail["SalePrice_Out"] = 0;
                                objInputChangeOrderDetail.SalePrice = objSaleOrderDetail.SalePrice;

                                //if (objInputChangeOrderDetail.ProductID_Out == string.Empty)
                                //{
                                //    if (objInputChangeOrderType.IsInputReturn)
                                //    {
                                //        objInputChangeOrderDetail.OutputTypeID = 0;
                                //        objInputChangeOrderDetail.SalePrice = 0;
                                //    }
                                //}
                                //else
                                //{
                                //    if (/*!chkCreateSaleOrder.Checked ||*/ !objInputChangeOrderType.IsGetPromotion)
                                //    {
                                //        ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(objInputChangeOrderDetail.OutputTypeID);
                                //        if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                                //        {
                                //            ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(objInputChangeOrderDetail.ProductID_Out);
                                //            objInputChangeOrderDetail.SalePrice = Convert.ToDecimal(rDetail["SalePrice_Out"]) / (Convert.ToDecimal(1) + Convert.ToDecimal(objProductOut.VAT) * Convert.ToDecimal(objProductOut.VATPercent) * Convert.ToDecimal(0.0001));
                                //        }
                                //    }
                                //}
                                //if (objInputChangeOrderType.IsInputChangeFree)
                                //    objInputChangeOrderDetail.SalePrice = objInputChangeOrderDetail.InputPrice;
                                objInputChangeOrderDetailList.Add(objInputChangeOrderDetail);
                            }
                            objInputChangeOrder.InputChangeOrderDetailList = objInputChangeOrderDetailList.ToArray();
                        }
                    }
                    else
                    {
                        EnableControlWhenSave(true);
                        return false;
                    }
                }

                decimal decSumInputPrice = GetCompute_In("ISSELECT=1");
                decimal decSumSalePrice = GetCompute_Out("ISSELECT=1");
                objInputChangeOrder.UnevenAmount = (decTotalLiquidate + decSumSalePrice) - decSumInputPrice;

                if (!objPLCInputChangeOrder.Insert(objInputChangeOrder, dtbMachineErrorList))
                {
                    btnAdd.Enabled = true;
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                }

                MessageBox.Show(this, "Tạo yêu cầu " + strMessageForm + " thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsHasAction = true;
                this.Close();
            }
            catch (Exception objEx)
            {
                btnAdd.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi tạo yêu cầu " + strMessageForm, objEx.Message + System.Environment.NewLine + objEx.StackTrace);
            }
            return true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!AddInputchangeOrder())
                return;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (bolIsLockByClosingDataMonth || strInputChangeOrderID == string.Empty)
            {
                return;
            }
            try
            {
                btnUpdate.Enabled = false;
                if (lstInputChangeOrder_Attachment_Del != null)
                {
                    lstInputChangeOrder_Attachment.InsertRange(lstInputChangeOrder_Attachment.Count, lstInputChangeOrder_Attachment_Del);
                }
                if (!UploadAttachment())
                {
                    if (MessageBox.Show(this, "Upload tập tin thất bại\r\nBạn có muốn tiếp tục lưu thông tin không?", "Thông báo",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }
                }
                ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder objInputChangeOrderUpdate = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder();
                objInputChangeOrderUpdate.InputChangeOrderID = strInputChangeOrderID;
                objInputChangeOrderUpdate.InputChangeOrderDate = objInputChangeOrder.InputChangeOrderDate;
                objInputChangeOrderUpdate.InputChangeOrderContent = txtContent.Text;
                objInputChangeOrderUpdate.InputChangeReasonID = cboReason.ColumnID;
                objInputChangeOrderUpdate.InputChangeReasonNote = txtReasonNote.Text.Trim();
                objInputChangeOrderUpdate.InputChangeOrder_AttachmentList = lstInputChangeOrder_Attachment.ToArray();
                //An luu loi luc cap nhat
                DataTable drDetail = (DataTable)flexDetail.DataSource;
                List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail> objInputChangeOrderDetailList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail>();
                for (int i = 0; i < drDetail.Rows.Count; i++)
                {
                    DataRow rDetail = drDetail.Rows[i];
                    PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail();

                    objInputChangeOrderDetail.InputChangeOrderDetailID = rDetail["INPUTCHANGEORDERDETAILID"].ToString();
                    int intMachineErrorGroupId = 0;
                    int intMachineErrorId = 0;
                    int.TryParse(rDetail["MACHINEERRORGROUPID"].ToString(), out intMachineErrorGroupId);
                    int.TryParse(rDetail["MACHINEERRORID"].ToString(), out intMachineErrorId);
                    //   objInputChangeOrderDetail.MachineErrorGroupId = intMachineErrorGroupId;
                    //  objInputChangeOrderDetail.MachineErrorId = intMachineErrorId;
                    objInputChangeOrderDetail.OldOutputVoucherDetailID = rDetail["OLDOUTPUTVOUCHERDETAILID"].ToString();
                    objInputChangeOrderDetail.MachineErrorContent = rDetail["MACHINEERRORNAME"].ToString();
                    objInputChangeOrderDetailList.Add(objInputChangeOrderDetail);
                }

                objInputChangeOrderUpdate.InputChangeOrderDetailList = objInputChangeOrderDetailList.ToArray();
                //objInputChangeOrderUpdate.ApplyErrorType = chkApplyErrorType.Checked;
                if (!objPLCInputChangeOrder.Update(objInputChangeOrderUpdate, dtbMachineErrorList))
                {
                    btnUpdate.Enabled = true;
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                }



                MessageBox.Show(this, "Cập nhật yêu cầu " + strMessageForm + " thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsHasAction = true;
                this.Close();
            }
            catch (Exception objEx)
            {
                btnUpdate.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi cập nhật yêu cầu " + strMessageForm, objEx.Message + System.Environment.NewLine + objEx.StackTrace);
            }
        }

        private void btnCreateConcern_Click(object sender, EventArgs e)
        {
            if (bolIsLockByClosingDataMonth)
            {
                return;
            }
            try
            {
                #region Chuẩn bị dữ liệu
                if (!CheckCreateConcern())
                {
                    return;
                }


                btnCreateConcern.Enabled = false;

                int intStoreID = cboStore.StoreID;
                string strUserName = Library.AppCore.SystemConfig.objSessionUser.UserName;
                int intCreateStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                DateTime dtmInputChangeOrderDate = Library.AppCore.Globals.GetServerDateTime();

                int intCurrencyUnitID = objOutputVoucherLoad.CurrencyUnitID;
                decimal decCurrencyExchange = objOutputVoucherLoad.CurrencyExchange;
                int intCustomerID = 0;
                try
                {
                    if (Convert.ToString(txtCustomerID.Text).Trim() != string.Empty)
                    {
                        intCustomerID = Convert.ToInt32(txtCustomerID.Text);
                    }
                }
                catch
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy mã khách hàng! Vui lòng kiểm tra lại");
                    btnCreateConcern.Enabled = true;
                    return;
                }
                string strCustomerName = txtCustomerName.Text.Trim();
                string strCustomerAddress = txtCustomerAddress.Text.Trim();
                string strCustomerPhone = txtCustomerPhone.Text.Trim();
                string strCustomerIDCard = txtCustomerIDCard.Text.Trim();
                string strCustomerTaxID = txtTaxID.Text.Trim();
                int intPayableTypeID = objOutputVoucherLoad.PayableTypeID;
                string strTaxID = txtTaxID.Text.Trim();
                string strStaffUser = ctrlStaffUser.UserName;

                decimal decTotalAmountBF_In = 0;
                decimal decTotalAmountBF_Out = 0;
                decimal decTotalVAT_In = 0;
                decimal decTotalVAT_Out = 0;

                ERP.MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit objCurrencyUnit = new MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit();
                ERP.MasterData.PLC.MD.PLCCurrencyUnit objPLCCurrencyUnit = new MasterData.PLC.MD.PLCCurrencyUnit();
                objPLCCurrencyUnit.LoadInfo(ref objCurrencyUnit, intCurrencyUnitID);
                if (objCurrencyUnit != null)
                {
                    decCurrencyExchange = objCurrencyUnit.CurrencyExchange;
                }
                else
                {
                    DataTable tblCurrencyUnit = null;
                    objPLCCurrencyUnit.SearchData(ref tblCurrencyUnit, new object[] { });
                    if (tblCurrencyUnit != null && tblCurrencyUnit.Rows.Count > 0)
                    {
                        DataRow[] rCurrencyUnit = tblCurrencyUnit.Select("CurrencyUnitName = 'VND'");
                        if (rCurrencyUnit.Length > 0)
                        {
                            intCurrencyUnitID = Convert.ToInt32(rCurrencyUnit[0]["CurrencyUnitID"]);
                            decCurrencyExchange = Convert.ToDecimal(rCurrencyUnit[0]["CurrencyExchange"]);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(objInputChangeOrder.SaleOrderID))
                {
                    if (objSaleOrder_Out == null)
                        objSaleOrder_Out = objPLCSaleOrders.LoadInfo(objInputChangeOrder.SaleOrderID);
                }

                // string stringPackageCode = string.Empty;

                //
                if (objInputChangeOrderType.IsInputReturn || objInputChangeOrderType.IsGetPromotion)
                {
                    for (int i = 0; i < dtbOVDetailReturn.Rows.Count; i++)
                    {
                        DataRow rDetail = dtbOVDetailReturn.Rows[i];
                        string strIMEI_In = string.Empty;
                        if (!Convert.IsDBNull(rDetail["IMEI"]))
                            strIMEI_In = Convert.ToString(rDetail["IMEI"]).Trim();
                        if (string.IsNullOrEmpty(strIMEI_In))
                            continue;
                        DataTable tdbCheck = objPLCSaleOrders.CheckDepositSubsidy(new object[] { "@IMEI", strIMEI_In });
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                            return;
                        }
                        if (tdbCheck != null && tdbCheck.Rows.Count > 0)
                        {
                            DataRow drRow = tdbCheck.Rows[0];
                            if (Convert.ToBoolean(drRow["ISVALID"]) == true)
                            {
                                decimal decReturnAmountDeposit = Convert.ToDecimal(drRow["SUMDISCOUNT"]);
                                int CORRESPONDINGVOUCHERTYPEID = Convert.ToInt32(drRow["CORRESPONDINGVOUCHERTYPEID"]);
                                string CustomerIdCard = drRow["CUSTOMERIDCARD"].ToString().Trim();
                                ERP.SalesAndServices.Payment.DUI.Payment.frmVoucher frmVoucher1 = new ERP.SalesAndServices.Payment.DUI.Payment.frmVoucher();
                                Customer objCustomer_New = new Customer();
                                objCustomer_New.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                                objCustomer_New.CustomerName = txtCustomerName.Text.Trim();
                                objCustomer_New.CustomerPhone = txtCustomerPhone.Text.Trim();
                                objCustomer_New.CustomerAddress = txtCustomerAddress.Text.Trim();
                                objCustomer_New.CustomerIDCard = CustomerIdCard;
                                objCustomer_New.Gender = false;
                                objCustomer_New.CustomerTaxID = txtTaxID.Text.Trim();
                                frmVoucher1.CustomerInfo = objCustomer_New;
                                frmVoucher1.IsSpend = true;
                                frmVoucher1.VoucherTypeID = CORRESPONDINGVOUCHERTYPEID > 0 ? CORRESPONDINGVOUCHERTYPEID : -1;

                                frmVoucher1.TotalMoney = decReturnAmountDeposit;
                                frmVoucher1.IsGetDesopit = true;
                                frmVoucher1.ShowDialog(this);
                                if (frmVoucher1.IsSuccess)
                                {
                                    objVoucherDepositSpend = frmVoucher1.Voucher_Deposit;
                                    objVoucherDepositSpend.VoucherConcern = Convert.ToString(drRow["SALEORDERID"]).Trim();
                                    objVoucherDepositSpend.OrderID = Convert.ToString(drRow["SALEORDERID"]).Trim();
                                    break;
                                }
                                else
                                    return;
                            }

                        }
                    }


                }
                if (objSaleOrder_Out != null)
                {
                    if (!string.IsNullOrEmpty(objSaleOrder_Out.SaleOrderID))
                    {
                        foreach (var item in objSaleOrder_Out.SaleOrderDetailList)
                        {
                            if (!item.IsPromotionProduct && !string.IsNullOrEmpty(item.IMEI))
                            {
                                DataTable tdbCheck = objPLCSaleOrders.CheckDepositSubsidy(new object[] { "@SaleOrderId", objSaleOrder_Out.SaleOrderID, "@IMEI", item.IMEI.Trim() });
                                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                                {
                                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                                    return;
                                }
                                if (tdbCheck != null && tdbCheck.Rows.Count > 0)
                                {
                                    DataRow drRow = tdbCheck.Rows[0];
                                    if (Convert.ToBoolean(drRow["ISVALID"]) == true)
                                    {
                                        decimal decReturnAmountDeposit = Convert.ToDecimal(drRow["SUMDISCOUNT"]);
                                        int INVOUCHERDEPOSIT = Convert.ToInt32(drRow["INVOUCHERDEPOSIT"]);
                                        string Content = Convert.ToString(drRow["DEPOSITCONTENT"]).Trim();
                                        ERP.SalesAndServices.Payment.DUI.Payment.frmVoucher frmVoucher1 = new ERP.SalesAndServices.Payment.DUI.Payment.frmVoucher();
                                        Customer objCustomer_New = new Customer();
                                        objCustomer_New.CustomerID = objSaleOrder_Out.CustomerID;
                                        objCustomer_New.CustomerName = objSaleOrder_Out.CustomerName;
                                        objCustomer_New.CustomerPhone = objSaleOrder_Out.CustomerPhone;
                                        objCustomer_New.CustomerAddress = objSaleOrder_Out.CustomerAddress;
                                        objCustomer_New.CustomerIDCard = objSaleOrder_Out.CustomerIDCard; ;
                                        objCustomer_New.Gender = objSaleOrder_Out.Gender; ;
                                        objCustomer_New.CustomerTaxID = objSaleOrder_Out.CustomerTaxID;
                                        frmVoucher1.CustomerInfo = objCustomer_New;
                                        frmVoucher1.IsSpend = false;
                                        frmVoucher1.VoucherTypeID = INVOUCHERDEPOSIT;
                                        frmVoucher1.VoucherContent = Content;
                                        frmVoucher1.TotalMoney = decReturnAmountDeposit;
                                        frmVoucher1.IsGetDesopit = true;
                                        frmVoucher1.ShowDialog(this);
                                        if (frmVoucher1.IsSuccess)
                                        {
                                            objVoucherDeposit = frmVoucher1.Voucher_Deposit;
                                            break;
                                        }
                                        else
                                            return;
                                    }
                                }
                            }

                        }

                    }
                }
                if (MessageBox.Show("Bạn có chắc muốn xử lý yêu cầu " + strMessageForm + " này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    EnableControlWhenSave(true);

                    decVNDCash = 0;
                    decPaymentCardAmount = 0;
                    intPaymentCardID = 0;
                    decPaymentCardSpend = 0;
                    strPaymentCardVoucherID = string.Empty;

                    decVNDCash_In = 0;
                    decPaymentCardAmount_In = 0;
                    intPaymentCardID_In = 0;
                    strPaymentCardName_In = string.Empty;
                    decPaymentCardSpend_In = 0;
                    strPaymentCardVoucherID_In = string.Empty;

                    decVNDCash_Out = 0;
                    decPaymentCardAmount_Out = 0;
                    intPaymentCardID_Out = 0;
                    decPaymentCardSpend_Out = 0;
                    strPaymentCardVoucherID_Out = string.Empty;

                    return;
                }
                PLC.Output.WSOutputVoucher.OutputVoucher objOldOutputVoucher = new PLC.Output.PLCOutputVoucher().LoadInfo(txtOutputVoucherID.Text.Trim());
                ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputVoucher objInputVoucher = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucher();
                ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucher objOutputVoucher = new PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucher();
                if (objInputChangeOrderType.AllowReturnDaysFrom == 0 && objInputChangeOrderType.AllowReturnDaysTo == 0)
                {
                    if (DateTime.Now.Date != objOldOutputVoucher.OutputDate.Value.Date)
                    {
                        MessageBox.Show(this, "Loại yêu cầu đổi trả " + objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép đổi trả trong ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                //double Numdays = (objOldOutputVoucher.OutputDate.Value.Date - DateTime.Now.Date).TotalDays + 0.5;
                //if (Numdays < objInputChangeOrderType.AllowReturnDaysFrom || Numdays > objInputChangeOrderType.AllowReturnDaysTo)
                //{
                //    MessageBox.Show(this, "Loại yêu cầu đổi trả " + objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép đổi trả từ " + objInputChangeOrderType.AllowReturnDaysFrom + " đến " + objInputChangeOrderType.AllowReturnDaysTo, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return;
                //}

                objInputChangeOrder.OldOutputVoucherID = strOutputVoucherID;
                objInputChangeOrder.InputChangeOrderStoreID = intStoreID;
                objInputChangeOrder.InputChangeDate = dtmInputChangeOrderDate;
                objInputChangeOrder.CreatedStoreID = intCreateStoreID;

                objInputVoucher.IsCheckRealInput = true;
                objInputVoucher.CheckRealInputUser = strUserName;
                objInputVoucher.CheckRealInputTime = dtmInputChangeOrderDate;
                objInputVoucher.InputTypeID = cboInputTypeID.InputTypeID;
                objInputVoucher.InputDate = dtmInputChangeOrderDate;
                objInputVoucher.PayableDate = dtmInputChangeOrderDate;
                //objInputVoucher.InVoiceID = txtInVoiceID.Text;
                //objInputVoucher.InVoiceSymbol = txtInvoiceSymbol.Text.Trim();
                objInputVoucher.InVoiceDate = dtmInputChangeOrderDate;
                objInputVoucher.CustomerID = intCustomerID;
                objInputVoucher.CustomerName = strCustomerName;
                objInputVoucher.CustomerAddress = strCustomerAddress;
                objInputVoucher.CustomerPhone = strCustomerPhone;
                objInputVoucher.CustomerIDCard = strCustomerIDCard;
                objInputVoucher.CreatedStoreID = intCreateStoreID;
                objInputVoucher.InputStoreID = intStoreID;
                objInputVoucher.PayableTypeID = intPayableTypeID;
                objInputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objInputVoucher.CurrencyExchange = decCurrencyExchange;
                objInputVoucher.Content = objInputChangeOrder.InputChangeOrderContent;

                objOutputVoucher.CreatedStoreID = intCreateStoreID;
                objOutputVoucher.OutputStoreID = intStoreID;
                objOutputVoucher.InvoiceDate = dtmInputChangeOrderDate;
                objOutputVoucher.CustomerID = intCustomerID;
                objOutputVoucher.CustomerName = strCustomerName;
                objOutputVoucher.CustomerAddress = strCustomerAddress;
                objOutputVoucher.TaxCustomerAddress = objOldOutputVoucher.TaxCustomerAddress;
                objOutputVoucher.CustomerPhone = strCustomerPhone;
                objOutputVoucher.CustomerTaxID = strCustomerTaxID;
                objOutputVoucher.OutputContent = string.Empty;
                objOutputVoucher.OutputDate = dtmInputChangeOrderDate;
                objOutputVoucher.PayableTypeID = intPayableTypeID;
                objOutputVoucher.PayableDate = dtmInputChangeOrderDate;
                objOutputVoucher.StaffUser = strStaffUser;
                objOutputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objOutputVoucher.CurrencyExchange = decCurrencyExchange;

                decimal decTotalReturnFee = 0;
                decimal decSumInputPriceMod2 = 0;
                objInputChangeOrder.InputVoucherReturnBO = null;
                DataRow[] drReturnInputype = dtbInputTypeAll.Select("InputTypeID = " + cboInputTypeID.InputTypeID);
                if (drReturnInputype.Length > 0)
                {
                    //lưu đồng bộ PM_InputVoucherReturn
                    objInputChangeOrder.IsInputReturn = objInputChangeOrderType.IsInputReturn;
                    objInputChangeOrder.InputVoucherReturnBO = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherReturn();
                    objInputChangeOrder.InputVoucherReturnBO.OutputVoucherID = strOutputVoucherID;
                    objInputChangeOrder.InputVoucherReturnBO.ReturnStoreID = intStoreID;
                    objInputChangeOrder.InputVoucherReturnBO.ReturnDate = DateTime.Now;
                    objInputChangeOrder.InputVoucherReturnBO.IsReturnWithFee = !objInputChangeOrderType.IsInputChangeFree;
                    objInputChangeOrder.InputVoucherReturnBO.TotalVATLost = 0;
                    objInputChangeOrder.InputVoucherReturnBO.TotalReturnFee = 0;
                    objInputChangeOrder.InputVoucherReturnBO.InVoiceID = txtInVoiceID.Text.Trim();
                    objInputChangeOrder.InputVoucherReturnBO.InVoiceSymbol = txtInvoiceSymbol.Text.Trim();
                    objInputChangeOrder.InputVoucherReturnBO.Denominator = objOutputVoucherLoad.Denominator;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerID = intCustomerID;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerName = strCustomerName;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerAddress = strCustomerAddress;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerPhone = strCustomerPhone;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerTaxID = strCustomerTaxID;
                    objInputChangeOrder.InputVoucherReturnBO.InputTypeID = cboInputTypeID.InputTypeID;
                    objInputChangeOrder.InputVoucherReturnBO.PayableTypeID = intPayableTypeID;
                    objInputChangeOrder.InputVoucherReturnBO.CurrencyUnitID = intCurrencyUnitID;
                    objInputChangeOrder.InputVoucherReturnBO.CurrencyExchange = 1;
                    objInputChangeOrder.InputVoucherReturnBO.CreatedStoreID = intCreateStoreID;
                    objInputChangeOrder.InputVoucherReturnBO.CreatedUser = strUserName;

                    //objInputChangeOrder.InputVoucherReturnBO.OutVoucherID = txtVoucherID.Text.Trim();
                    objInputChangeOrder.InputVoucherReturnBO.ReturnReason = objInputChangeOrder.InputChangeOrderContent;
                    objInputChangeOrder.InputVoucherReturnBO.Discount = 0;
                    objInputChangeOrder.InputVoucherReturnBO.DiscountReasonID = 0;
                    objInputChangeOrder.InputVoucherReturnBO.ReturnNote = string.Empty;
                }


                List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetail> objInputVoucherDetailList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetail>();
                List<PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucherDetail> objOutputVoucherDetailList = new List<PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucherDetail>();
                List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail> objInputChangeDetailList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail>();

                dtbOVDetailReturn.AcceptChanges();
                for (int i = 0; i < dtbOVDetailReturn.Rows.Count; i++)
                {
                    DataRow rDetail = dtbOVDetailReturn.Rows[i];
                    string strOldOutputVoucherDetailID = Convert.ToString(rDetail["OldOutputVoucherDetailID"]).Trim();
                    string strProductID_In = string.Empty;
                    string strProductID_Out = string.Empty;
                    string strIMEI_Out = string.Empty;
                    string strIMEI_In = string.Empty;
                    decimal decQuantity = 0;
                    bool bolIsNew_In = Convert.ToBoolean(rDetail["IsNew_In"]);
                    int intVAT_In = 0;
                    int intVATPercent_In = 0;
                    int intVAT_Out = 0;
                    int intVATPercent_Out = 0;
                    decimal decInputPrice = 0;
                    decimal decSalePrice = 0;
                    //decimal decCostPrice = 0;

                    strProductID_In = Convert.ToString(rDetail["ProductID"]).Trim();
                    strProductID_Out = Convert.ToString(rDetail["ProductID_Out"]).Trim();
                    if (!Convert.IsDBNull(rDetail["IMEI"])) strIMEI_In = Convert.ToString(rDetail["IMEI"]).Trim();
                    if (!Convert.IsDBNull(rDetail["IMEI_Out"])) strIMEI_Out = Convert.ToString(rDetail["IMEI_Out"]).Trim();
                    decQuantity = Convert.ToDecimal(rDetail["Quantity"]);
                    decInputPrice = Convert.ToDecimal(rDetail["InputPrice_In2"]);
                    decSalePrice = Convert.ToDecimal(rDetail["SalePrice_Out2"]);
                    //decCostPrice = Convert.ToDecimal(rDetail["CostPrice"]);
                    intVAT_In = Convert.ToInt32(rDetail["VAT"]);
                    intVATPercent_In = Convert.ToInt32(rDetail["VATPercent"]);
                    bool bolIsHasWarranty = Convert.ToBoolean(rDetail["IsHasWarranty"]);
                    DateTime dtmEndWarrantyDate = Convert.ToDateTime(rDetail["EndWarrantyDate"]);
                    //int intFirstInputTypeID = Convert.ToInt32(rDetail["FirstInputTypeID"]);

                    DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + cboInputTypeID.InputTypeID);
                    if (drReturnInputType.Length > 0 && Convert.ToBoolean(drReturnInputType[0]["IsVATZero"])) intVAT_In = 0;

                    ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(rDetail["OutputTypeID_Out"]));
                    if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                    {
                        ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID_Out);
                        intVAT_Out = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID_Out).VAT;
                    }
                    if (strProductID_Out != string.Empty)
                        intVATPercent_Out = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID_Out).VATPercent;

                    decTotalAmountBF_In += decInputPrice * decQuantity;
                    decTotalVAT_In += decInputPrice * decQuantity * (Convert.ToDecimal(intVAT_In) * Convert.ToDecimal(intVATPercent_In) * Convert.ToDecimal(0.0001));

                    //if (objInputChangeOrderType.IsInputChangeFree)
                    //{
                    //    intVAT_Out = intVAT_In;
                    //    intVATPercent_Out = intVATPercent_In;
                    //}

                    decTotalAmountBF_Out += decSalePrice * decQuantity;
                    decTotalVAT_Out += decSalePrice * decQuantity * (Convert.ToDecimal(intVAT_Out) * Convert.ToDecimal(intVATPercent_Out) * Convert.ToDecimal(0.0001));

                    #region InputChangeDetail
                    PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail();
                    objInputChangeOrderDetail.InputChangeOrderDetailID = rDetail["InputChangeOrderDetailID"].ToString().Trim();
                    objInputChangeOrderDetail.InputChangeOrderID = objInputChangeOrder.InputChangeOrderID;
                    objInputChangeOrderDetail.OldOutputVoucherDetailID = strOldOutputVoucherDetailID;
                    objInputChangeOrderDetail.InputChangeOrderDate = dtmInputChangeOrderDate;
                    objInputChangeOrderDetail.ProductID_Out = strProductID_Out;
                    objInputChangeOrderDetail.ProductID_In = strProductID_In;
                    objInputChangeOrderDetail.IMEI_Out = strIMEI_Out;
                    objInputChangeOrderDetail.IMEI_In = strIMEI_In;
                    objInputChangeOrderDetail.Quantity = decQuantity;
                    objInputChangeOrderDetail.OutputTypeID = Convert.ToInt32(rDetail["OutputTypeID_Out"]);
                    objInputChangeOrderDetail.CreatedStoreID = intCreateStoreID;
                    objInputChangeOrderDetail.InputPrice = decInputPrice;
                    //NLT gán trạng thái sp.
                    objInputChangeOrderDetail.InStockStatusID_In = Convert.ToInt32(rDetail["InStockStatusID_In"]);
                    int intStatus_Out = -1;
                    if (Int32.TryParse(Convert.ToString(rDetail["InStockStatusID_Out"]), out intStatus_Out))
                        objInputChangeOrderDetail.InStockStatusID_Out = intStatus_Out;

                    decTotalReturnFee += Convert.ToDecimal(rDetail["TotalFee"]);
                    decSumInputPriceMod2 += Convert.ToDecimal(rDetail["InputPrice_In"]) * (Convert.ToDecimal(rDetail["Quantity"]));

                    objInputChangeDetailList.Add(objInputChangeOrderDetail);
                    #endregion

                    #region InputVoucherDetail
                    if (objInputVoucherDetailList.Count > 0 && objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].ProductID == strProductID_In)
                    {
                        if (strIMEI_In != string.Empty)
                        {
                            PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI();
                            objInputVoucherDetailIMEI.IMEI = strIMEI_In;
                            objInputVoucherDetailIMEI.CreatedStoreID = intCreateStoreID;
                            objInputVoucherDetailIMEI.InputDate = dtmInputChangeOrderDate;
                            objInputVoucherDetailIMEI.InputStoreID = intStoreID;
                            objInputVoucherDetailIMEI.IsHasWarranty = bolIsHasWarranty;
                            //Luu them loi vao ghi chu chi tiet phieu nhap
                            objInputVoucherDetailIMEI.Note = dtbOVDetailReturn.Rows[i]["MACHINEERRORNAME"].ToString();
                            List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI>();
                            if (objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList.Length > 0)
                            {
                                objInputVoucherDetailIMEIList.AddRange(objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList);
                            }
                            objInputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                            objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList = objInputVoucherDetailIMEIList.ToArray();
                            objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].Quantity = Convert.ToDecimal(objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList.Length);
                        }
                    }
                    else
                    {
                        PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetail objInputVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetail();
                        objInputVoucherDetail.ProductID = strProductID_In;
                        objInputVoucherDetail.Quantity = decQuantity;
                        objInputVoucherDetail.VAT = intVAT_In;
                        objInputVoucherDetail.VATPercent = intVATPercent_In;
                        objInputVoucherDetail.InputDate = dtmInputChangeOrderDate;
                        objInputVoucherDetail.CreatedStoreID = intCreateStoreID;
                        //Luu them loi vao ghi chu chi tiet phieu nhap
                        objInputVoucherDetail.Note = dtbOVDetailReturn.Rows[i]["MACHINEERRORNAME"].ToString();
                        //objInputVoucherDetail.FirstInputTypeID = intFirstInputTypeID;
                        objInputVoucherDetail.InputStoreID = intStoreID;
                        objInputVoucherDetail.IsNew = bolIsNew_In;
                        if (objInputChangeOrderType.IsChangeToOldProduct)
                            objInputVoucherDetail.IsNew = false;
                        if (objInputChangeOrderType.IsChangeToShowProduct)
                            objInputVoucherDetail.IsShowProduct = true;
                        objInputVoucherDetail.InputPrice = decInputPrice;
                        objInputVoucherDetail.ReturnFee = Convert.ToDecimal(rDetail["TotalFee"]);
                        objInputVoucherDetail.ENDWarrantyDate = dtmEndWarrantyDate;
                        if (strIMEI_In != string.Empty)
                        {
                            PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI();
                            objInputVoucherDetailIMEI.IMEI = strIMEI_In;
                            objInputVoucherDetailIMEI.CreatedStoreID = intCreateStoreID;
                            objInputVoucherDetailIMEI.InputDate = dtmInputChangeOrderDate;
                            objInputVoucherDetailIMEI.InputStoreID = intStoreID;
                            objInputVoucherDetailIMEI.IsHasWarranty = bolIsHasWarranty;
                            //Luu them loi vao ghi chu chi tiet phieu nhap
                            objInputVoucherDetailIMEI.Note = dtbOVDetailReturn.Rows[i]["MACHINEERRORNAME"].ToString();
                            List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI>();
                            objInputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                            objInputVoucherDetail.InputVoucherDetailIMEIList = objInputVoucherDetailIMEIList.ToArray();
                            objInputVoucherDetail.Quantity = Convert.ToDecimal(objInputVoucherDetail.InputVoucherDetailIMEIList.Length);
                        }
                        objInputVoucherDetailList.Add(objInputVoucherDetail);
                    }
                    #endregion

                    #region OutputVoucherDetail
                    PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucherDetail objOutputVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucherDetail();
                    objOutputVoucherDetail.ProductID = strProductID_Out;
                    objOutputVoucherDetail.IMEI = strIMEI_Out;
                    //objOutputVoucherDetail.IsNew = objInputChangeOrderType.IsNew;
                    objOutputVoucherDetail.Quantity = decQuantity;
                    objOutputVoucherDetail.VAT = intVAT_Out;
                    objOutputVoucherDetail.VATPercent = intVATPercent_Out;
                    objOutputVoucherDetail.SalePrice = decSalePrice;
                    objOutputVoucherDetail.CreatedStoreID = intCreateStoreID;
                    objOutputVoucherDetail.OutputStoreID = intStoreID;
                    objOutputVoucherDetail.OutputTypeID = Convert.ToInt32(rDetail["OutputTypeID_Out"]);
                    objOutputVoucherDetail.OutputDate = dtmInputChangeOrderDate;
                    objOutputVoucherDetail.InputChangeDate = dtmInputChangeOrderDate;

                    objOutputVoucherDetailList.Add(objOutputVoucherDetail);
                    #endregion
                }

                objInputVoucher.TotalAmountBFT = decTotalAmountBF_In;
                objInputVoucher.TotalVAT = decTotalVAT_In;
                objInputVoucher.TotalAmount = decTotalAmountBF_In + decTotalVAT_In;
                objOutputVoucher.TotalAmountBFT = decTotalAmountBF_Out;
                objOutputVoucher.TotalVAT = decTotalVAT_Out;
                objOutputVoucher.TotalAmount = decTotalAmountBF_Out + decTotalVAT_Out;

                objInputVoucher.IsNew = objInputVoucherDetailList[0].IsNew;
                objInputChangeOrder.InputChangeOrderDetailList = objInputChangeDetailList.ToArray();
                objInputVoucher.InputVoucherDetailList = objInputVoucherDetailList.ToArray();
                objOutputVoucher.OutputVoucherDetailList = objOutputVoucherDetailList.ToArray();
                string strProduct = string.Empty;
                try
                {
                    strProduct = Library.AppCore.AppConfig.GetStringConfigValue("SM_SALEORDERDETAIL_COUPONPRODUCTID");
                }
                catch
                {
                    strProduct = string.Empty;
                }
                var detailtmp = objOutputVoucherDetailList.ToList().FindAll(x => x.ProductID.Trim() == strProduct.Trim());
                if (detailtmp != null && detailtmp.Count() > 0)
                {
                    objOutputVoucher.OutputVoucherDetailCouponList = detailtmp.ToArray();
                }
                if (objInputChangeOrder.InputVoucherReturnBO != null)
                {
                    objInputChangeOrder.InputVoucherReturnBO.TotalAmountBFT = decTotalAmountBF_In;
                    objInputChangeOrder.InputVoucherReturnBO.TotalVAT = decTotalVAT_In;
                    objInputChangeOrder.InputVoucherReturnBO.TotalAmount = decTotalAmountBF_In + decTotalVAT_In;
                    objInputChangeOrder.InputVoucherReturnBO.IsNew = objInputVoucher.IsNew;
                    objInputChangeOrder.InputVoucherReturnBO.TotalReturnFee = decTotalReturnFee;
                    if (decTotalReturnFee != 0)
                        objInputChangeOrder.InputVoucherReturnBO.IsReturnWithFee = true;
                    else
                        objInputChangeOrder.InputVoucherReturnBO.IsReturnWithFee = false;
                }

                List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail> lst = new List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail>();
                decimal debt = 0;
                decimal decSumInputPrice = 0;
                decimal decTotalMountSave = 0;
                int intTypeDebt = 0;
                decimal decTotalLechType2 = 0;
                if (objInputChangeOrderType.ReturnFeeBook != 2)
                    decSumInputPrice = GetCompute_In(string.Empty);
                else if (objInputChangeOrderType.ReturnFeeBook == 2 /*&& string.IsNullOrEmpty(txtSaleOrderID.Text)*/)
                {
                    decSumInputPrice = decSumInputPriceMod2 - decTotalReturnFee;
                    if (objInputChangeOrderType.IsInputReturn && txtReturnAmount.Visible)
                    {
                        if (strInVoucherID != string.Empty)
                        {
                            if (decTotalPaid == 0)
                            {
                                decSumInputPrice = 0;
                                bolIsDebt = true;
                                bolIsNotCreateOutVoucherDetail = true;
                            }
                            else
                            {
                                if (Math.Round(decSumInputPriceMod2) > Math.Round(decTotalPaid))
                                {
                                    decSumInputPrice = decTotalPaid;
                                    bolIsDebt = true;
                                    DataTable dtbCheckDetailReturn1 = objPLCInputChangeOrder.GetListReturn(objOutputVoucherLoad.OutputVoucherID, objInputChangeOrder.InputChangeOrderID);
                                    decimal decSumTotalAmount = Math.Abs(Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbCheckDetailReturn1.Compute("Sum(TotalAmount)", "Quantity < 0"), 0)));
                                    decSumInputPrice = decSumInputPrice - decSumTotalAmount - decTotalReturnFee;
                                    if (decSumInputPrice <= 0)
                                    {
                                        bolIsNotCreateOutVoucherDetail = true;
                                        decSumInputPrice = 0;
                                    }
                                }
                                else
                                {

                                    DataTable dtbCheckDetailReturn1 = objPLCInputChangeOrder.GetListReturn(objOutputVoucherLoad.OutputVoucherID, objInputChangeOrder.InputChangeOrderID);
                                    decimal decSumTotalAmount = Math.Abs(Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbCheckDetailReturn1.Compute("Sum(TotalAmount)", "Quantity < 0"), 0)));
                                    decimal decAmount = decTotalPaid - decSumTotalAmount;
                                    if (Math.Round(decAmount) < Math.Round(decSumInputPrice))
                                        decSumInputPrice = decAmount - decTotalReturnFee;
                                    if (decSumInputPrice <= 0)
                                    {
                                        bolIsDebt = true;
                                        bolIsNotCreateOutVoucherDetail = true;
                                        decSumInputPrice = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                decimal decSumSalePrice = 0;
                decimal decFeePaymentCard = 0;

                DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select(string.Empty);
                for (int i = 0; i < drDetail.Length; i++)
                {
                    if (drDetail[i]["SalePrice_Out"] != DBNull.Value)
                        decSumSalePrice += Convert.ToDecimal(drDetail[i]["SalePrice_Out"]) * Convert.ToDecimal(drDetail[i]["Quantity"]);
                }


                //decimal dectotalChi = lblTitlePayment.Text == "Phải chi" ? Convert.ToDecimal(txtReturnAmount.Text) + decTotalReturnFee : 0;
                //decimal dectotalThu = lblTitlePayment.Text == "Phải thu" ? Convert.ToDecimal(txtReturnAmount.Text) : 0;
                //decimal decNotFeeChi = lblTitlePayment.Text == "Phải chi" ? dectotalChi - decTotalReturnFee : 0;
                //decimal decNotFeeThu = 0;
                //if (lblTitlePayment.Text == "Phải thu")
                //{
                //    if (!bolIsExceptionSpend)
                //        decNotFeeThu = dectotalThu - decTotalReturnFee;
                //    else
                //        decNotFeeThu = dectotalThu;
                //}
                if (lstAddVoucherDetail.Count > 0)
                {
                    foreach (var item in lstAddVoucherDetail)
                    {
                        PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();

                        objVoucherDetail.VNDCash = item.VNDCash;
                        objVoucherDetail.PaymentCardAmount = item.PaymentCardAmount;
                        objVoucherDetail.PaymentCardID = item.PaymentCardID;
                        objVoucherDetail.PaymentCardName = item.PaymentCardName;
                        objVoucherDetail.PaymentCardSpend = item.PaymentCardSpend;
                        objVoucherDetail.PaymentCardVoucherID = item.PaymentCardVoucherID;
                        objVoucherDetail.RefundAmount = 0;
                        objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                        objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objVoucherDetail.VoucherStoreID = intStoreID;
                        objVoucherDetail.CreatedStoreID = intCreateStoreID;
                        if (objInputChangeOrderType.ReturnFeeBook != 2)
                            debt += (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                        lst.Add(objVoucherDetail);

                    }
                    if (objInputChangeOrderType.ReturnFeeBook == 2)
                    {
                        decimal decSumPaymentCardAmount = Math.Round(lst.Sum(x => x.PaymentCardAmount));
                        decimal decSumVNDCash = Math.Round(lst.Sum(x => x.VNDCash));
                        if (!objInputChangeOrderType.IsInputReturn)
                        {
                            if (string.IsNullOrEmpty(txtSaleOrderID.Text))
                            {
                                if (Math.Round(decSumInputPrice) < Math.Round(decSumSalePrice) && ((Math.Round(decSumSalePrice) - Math.Round(decSumInputPrice)) >= decTotalReturnFee))
                                {
                                    decimal decTotalChenhlech = Math.Round(decReturnAmount - decTotalReturnFee);
                                    intTypeDebt = 1;
                                    decTotalMountSave = decTotalChenhlech;
                                    if (decSumPaymentCardAmount > 0 && decSumVNDCash > 0)
                                    {
                                        if (decSumPaymentCardAmount < decTotalChenhlech)
                                        {
                                            decimal decSumVNDCashTemp = decTotalChenhlech - Math.Round(decSumPaymentCardAmount);
                                            if (lst.Count == 1)
                                                lst[0].VNDCash = decSumVNDCashTemp;
                                            else
                                            {

                                                decimal decTemp = 0;
                                                bool Isbreak = false;
                                                foreach (var item in lst.ToList())
                                                {
                                                    if (Isbreak)
                                                    {
                                                        item.VNDCash = 0;
                                                    }
                                                    else
                                                    {
                                                        if (item != lst.Last())
                                                        {
                                                            if (item.VNDCash > decSumVNDCashTemp)
                                                            {
                                                                item.VNDCash = decSumVNDCashTemp;
                                                                Isbreak = true;
                                                            }
                                                            else
                                                                decTemp += item.VNDCash;
                                                        }
                                                        else
                                                        {
                                                            item.VNDCash = decSumVNDCashTemp - decTemp;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if (decSumPaymentCardAmount > decTotalChenhlech)
                                        {
                                            decimal decTemp = 0;
                                            decFeePaymentCard = decSumPaymentCardAmount - decTotalChenhlech;

                                            foreach (var item in lst.ToList())
                                            {
                                                if (item != lst.Last())
                                                {
                                                    decTemp += item.PaymentCardAmount;
                                                }
                                                else
                                                {
                                                    item.PaymentCardAmount = decTotalChenhlech - decTemp;
                                                }
                                                item.VNDCash = 0;
                                            }
                                            // objVoucherDetail.PaymentCardAmount = decTotalChenhlech;
                                            bolIsPaymentCard = true;
                                        }
                                        else
                                        {
                                            //    objVoucherDetail.PaymentCardAmount = decTotalChenhlech;
                                            decFeePaymentCard = decTotalReturnFee;
                                            foreach (var item in lst.ToList())
                                            {
                                                item.VNDCash = 0;
                                            }
                                            bolIsPaymentCard = true;
                                        }
                                    }
                                    else if (decSumPaymentCardAmount > 0 && decSumVNDCash == 0)
                                    {
                                        //  objVoucherDetail.PaymentCardAmount = decTotalChenhlech;
                                        decFeePaymentCard = decTotalReturnFee;
                                        bolIsPaymentCard = true;
                                    }

                                }
                                else if (Math.Round(decSumInputPrice) < Math.Round(decSumSalePrice) && ((Math.Round(decSumSalePrice) - Math.Round(decSumInputPrice))) < decTotalReturnFee)
                                {
                                    bolIsExceptionSpend = true;
                                    decimal decTotalThu = Math.Round(decReturnAmount);
                                    decimal dectotalLech = Math.Round(decTotalReturnFee - decTotalThu);
                                    decTotalMountSave = dectotalLech;
                                    intTypeDebt = 2;
                                    decTotalLechType2 = dectotalLech;
                                    if (decPaymentCardAmount > 0)
                                    {
                                        decFeePaymentCard = decPaymentCardAmount;
                                        //if (lstTempVNDCash != null && lstTempVNDCash.Count > 0)
                                        //    lstTempVNDCash.Clear();
                                        //if (lstTempPaymentAmount != null && lstTempPaymentAmount.Count > 0)
                                        //    lstTempPaymentAmount.Clear();
                                        //foreach (var item in lst)
                                        //{
                                        //    lstTempVNDCash.Add(item.VNDCash);
                                        //    lstTempPaymentAmount.Add(item.PaymentCardAmount);
                                        //}
                                        lstTemp = lst.ToList();
                                        lst.Clear();
                                        //    debt = 0;
                                        PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                                        objVoucherDetail.PaymentCardAmount = 0;
                                        objVoucherDetail.VNDCash = dectotalLech;
                                        objVoucherDetail.PaymentCardID = 0;
                                        objVoucherDetail.PaymentCardName = string.Empty;
                                        objVoucherDetail.PaymentCardSpend = 0;
                                        objVoucherDetail.PaymentCardVoucherID = string.Empty;
                                        objVoucherDetail.RefundAmount = 0;
                                        objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                                        objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                                        objVoucherDetail.VoucherStoreID = intStoreID;
                                        objVoucherDetail.CreatedStoreID = intCreateStoreID;
                                        //   debt += decTotalLechType2;
                                        lst.Add(objVoucherDetail);
                                        bolIsPaymentCard = true;
                                    }
                                    else
                                    {
                                        lstTemp = lst.ToList();
                                        lst.Clear();
                                        //  debt = 0;
                                        PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                                        objVoucherDetail.PaymentCardAmount = 0;
                                        objVoucherDetail.VNDCash = dectotalLech;
                                        objVoucherDetail.PaymentCardID = 0;
                                        objVoucherDetail.PaymentCardName = string.Empty;
                                        objVoucherDetail.PaymentCardSpend = 0;
                                        objVoucherDetail.PaymentCardVoucherID = string.Empty;
                                        objVoucherDetail.RefundAmount = 0;
                                        objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                                        objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                                        objVoucherDetail.VoucherStoreID = intStoreID;
                                        objVoucherDetail.CreatedStoreID = intCreateStoreID;
                                        //    debt += decTotalLechType2;
                                        lst.Add(objVoucherDetail);
                                        bolIsPaymentCard = true;
                                    }
                                }
                                else if (Math.Round(decSumInputPrice) > Math.Round(decSumSalePrice))
                                {
                                    intTypeDebt = 3;
                                    decimal dectotalLech = Math.Round(decSumInputPrice) - Math.Round(decSumSalePrice);

                                    PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                                    objVoucherDetail.VNDCash = /*dectotalLech - decSumPaymentCardAmount - decSumVNDCash +*/ decTotalReturnFee;
                                    objVoucherDetail.PaymentCardAmount = 0;
                                    objVoucherDetail.PaymentCardID = 0;
                                    objVoucherDetail.PaymentCardName = string.Empty;
                                    objVoucherDetail.PaymentCardSpend = 0;
                                    objVoucherDetail.PaymentCardVoucherID = string.Empty;
                                    objVoucherDetail.RefundAmount = 0;
                                    objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                                    objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                                    objVoucherDetail.VoucherStoreID = intStoreID;
                                    objVoucherDetail.CreatedStoreID = intCreateStoreID;
                                    //   debt += (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                                    lst.Add(objVoucherDetail);
                                    // objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                                    //  objVoucherDetail.VNDCash = dectotalLech - decPaymentCardAmount + decTotalReturnFee;
                                    decTotalMountSave = decTotalReturnFee + dectotalLech;
                                }
                                else
                                {

                                }
                            }
                            else
                            {
                                //if (bolSpecialCase)
                                //{

                                //    decimal declech = Math.Round(decTotalReturnFee - Convert.ToDecimal(txtReturnAmount.Text));
                                //    decTotalMountSave = declech;
                                //    if (decSumPaymentCardAmount > 0 && decSumVNDCash > 0)
                                //    {
                                //        if (decSumPaymentCardAmount > declech)
                                //        {
                                //            decimal decTemp = 0;
                                //            foreach (var item in lst.ToList())
                                //            {
                                //                item.VNDCash = 0;
                                //                if (item != lst.Last())
                                //                {
                                //                    decTemp += item.PaymentCardAmount;
                                //                }
                                //                else
                                //                    item.PaymentCardAmount = declech - decTemp;

                                //            }
                                //            decFeePaymentCard = decPaymentCardAmount - declech;
                                //            bolIsPaymentCard = true;
                                //        }
                                //        else if (decSumPaymentCardAmount < declech)
                                //        {
                                //            decimal decTemp = 0;
                                //            foreach (var item in lst.ToList())
                                //            {
                                //                if (item != lst.Last())
                                //                {
                                //                    decTemp += item.VNDCash;
                                //                }
                                //                else
                                //                    item.VNDCash = declech - decSumPaymentCardAmount;
                                //            }
                                //        }
                                //        else
                                //        {
                                //            foreach (var item in lst.ToList())
                                //            {
                                //                item.VNDCash = 0;
                                //            }
                                //        }
                                //    }

                                //    decTotalLechType2 = declech;
                                //    intTypeDebt = 2;
                                //}
                                //   else
                                //{
                                intTypeDebt = 4;
                                PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                                objVoucherDetail.VNDCash = /*dectotalLech - decSumPaymentCardAmount - decSumVNDCash +*/ decTotalReturnFee;
                                objVoucherDetail.PaymentCardAmount = 0;
                                objVoucherDetail.PaymentCardID = 0;
                                objVoucherDetail.PaymentCardName = string.Empty;
                                objVoucherDetail.PaymentCardSpend = 0;
                                objVoucherDetail.PaymentCardVoucherID = string.Empty;
                                objVoucherDetail.RefundAmount = 0;
                                objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                                objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                                objVoucherDetail.VoucherStoreID = intStoreID;
                                objVoucherDetail.CreatedStoreID = intCreateStoreID;
                                //  debt += (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                                lst.Add(objVoucherDetail);
                                //   objVoucherDetail.VNDCash = decVNDCash + decTotalReturnFee;
                                //   objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                                //   decTotalMountSave = decSumVNDCash + decSumPaymentCardAmount + decTotalReturnFee;
                                decTotalMountSave = decReturnAmount + decTotalReturnFee;
                                //  }
                            }

                        }
                        else
                        {
                            intTypeDebt = 4;
                            PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                            objVoucherDetail.VNDCash = /*dectotalLech - decSumPaymentCardAmount - decSumVNDCash +*/ decTotalReturnFee;
                            objVoucherDetail.PaymentCardAmount = 0;
                            objVoucherDetail.PaymentCardID = 0;
                            objVoucherDetail.PaymentCardName = string.Empty;
                            objVoucherDetail.PaymentCardSpend = 0;
                            objVoucherDetail.PaymentCardVoucherID = string.Empty;
                            objVoucherDetail.RefundAmount = 0;
                            objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDetail.VoucherStoreID = intStoreID;
                            objVoucherDetail.CreatedStoreID = intCreateStoreID;
                            //    debt += (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                            lst.Add(objVoucherDetail);
                            // decTotalMountSave = decSumVNDCash + decSumPaymentCardAmount + decTotalReturnFee;
                            decTotalMountSave = decReturnAmount + decTotalReturnFee;
                        }

                        if (intTypeDebt == 2)
                            debt = decTotalLechType2;
                        else
                        {
                            foreach (var item in lst)
                            {
                                debt += (item.VNDCash + item.ForeignCashExchange + item.PaymentCardAmount - item.RefundAmount);
                            }
                            if (intTypeDebt == 4 || intTypeDebt == 3)
                                debt = debt - decTotalReturnFee;
                        }
                    }
                }
                else
                {

                    PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                    #region RemCode
                    //if (Convert.ToDecimal(txtReturnAmount.Text) == 0 && objInputChangeOrderType.ReturnFeeBook == 2)
                    //{
                    //    objVoucherDetail.VNDCash = decTotalReturnFee;
                    //}
                    //else
                    //{
                    //    if (decVNDCash > 0 && (objInputChangeOrderType.ReturnFeeBook == 2 && objInputChangeOrderType.IsInputReturn || (objInputChangeOrderType.ReturnFeeBook == 2 && !objInputChangeOrderType.IsInputReturn && (Math.Round(decSumInputPrice) > Math.Round(decSumSalePrice) || !string.IsNullOrEmpty(txtSaleOrderID.Text)))))
                    //        if (!bolIsExceptionSpend)
                    //            objVoucherDetail.VNDCash = decVNDCash + decTotalReturnFee;
                    //        else
                    //            objVoucherDetail.VNDCash = decTotalReturnFee + decExceptionSpend;

                    //    else
                    //        objVoucherDetail.VNDCash = decVNDCash;
                    //}
                    //objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                    //if (objInputChangeOrderType.ReturnFeeBook == 2 && decPaymentCardAmount > 0)
                    //{

                    //    if (Math.Round(decSumInputPrice) < Math.Round(decSumSalePrice))
                    //    {

                    //        if (objVoucherDetail.VNDCash == 0) //chỉ cà thẻ
                    //        {
                    //            objVoucherDetail.PaymentCardAmount = objVoucherDetail.PaymentCardAmount - decTotalReturnFee;
                    //        }
                    //        else if (objVoucherDetail.VNDCash > 0)
                    //        {
                    //            if (Math.Round(objVoucherDetail.PaymentCardAmount) > Math.Round(decNotFeeThu))
                    //            {
                    //                decFeePaymentCard = Math.Round(objVoucherDetail.PaymentCardAmount) - Math.Round(decNotFeeChi);
                    //                objVoucherDetail.PaymentCardAmount = decNotFeeChi;
                    //                objVoucherDetail.VNDCash = 0;
                    //            }
                    //            else if (Math.Round(objVoucherDetail.PaymentCardAmount) < Math.Round(decNotFeeThu))
                    //            {
                    //                objVoucherDetail.VNDCash = decTotalReturnFee - objVoucherDetail.PaymentCardAmount;
                    //            }
                    //            else if (Math.Round(objVoucherDetail.PaymentCardAmount) < decTotalReturnFee)
                    //            {
                    //                objVoucherDetail.VNDCash = decNotFeeThu - Math.Round(objVoucherDetail.PaymentCardAmount);

                    //            }
                    //        }
                    //    }
                    //    else if (Math.Round(decSumInputPrice) > Math.Round(decSumSalePrice))
                    //    {
                    //        if (!bolIsExceptionSpend)
                    //        {
                    //            if (objVoucherDetail.VNDCash > 0)
                    //            {
                    //                if (Math.Round(objVoucherDetail.PaymentCardAmount) > Math.Round(decNotFeeChi))
                    //                {
                    //                    decFeePaymentCard = Math.Round(objVoucherDetail.PaymentCardAmount) - Math.Round(decNotFeeChi);

                    //                }

                    //                else if (Math.Round(objVoucherDetail.PaymentCardAmount) < decTotalReturnFee)
                    //                {
                    //                    objVoucherDetail.VNDCash = dectotalChi - Math.Round(objVoucherDetail.PaymentCardAmount);
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            decFeePaymentCard = objVoucherDetail.PaymentCardAmount;

                    //            objVoucherDetail.VNDCash = decTotalReturnFee - Convert.ToDecimal(txtReturnAmount.Text);
                    //            objVoucherDetail.PaymentCardAmount = 0;
                    //        }
                    //    }
                    //    else if (Math.Round(decSumInputPrice) == Math.Round(decSumSalePrice))
                    //    {
                    //        if (!objInputChangeOrderType.IsInputReturn)
                    //        {
                    //            if (dectotalThu > 0)
                    //            {
                    //                objVoucherDetail.PaymentCardAmount = dectotalThu - decVNDCash;
                    //                decFeePaymentCard = objVoucherDetail.PaymentCardAmount;
                    //            }
                    //        }
                    //    }
                    //    else if (objVoucherDetail.VNDCash != dectotalChi && dectotalChi > 0)
                    //    {
                    //        objVoucherDetail.VNDCash = dectotalChi - objVoucherDetail.PaymentCardAmount;
                    //        decFeePaymentCard = objVoucherDetail.PaymentCardAmount;
                    //    }
                    //    else { }
                    //    bolIsPaymentCard = true;
                    //}

                    //objVoucherDetail.PaymentCardID = intPaymentCardID;
                    //objVoucherDetail.PaymentCardName = strPaymentCardName;
                    //objVoucherDetail.PaymentCardSpend = decPaymentCardSpend;
                    //objVoucherDetail.PaymentCardVoucherID = strPaymentCardVoucherID;
                    //objVoucherDetail.RefundAmount = 0;
                    //objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                    //objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                    //objVoucherDetail.VoucherStoreID = intStoreID;
                    //objVoucherDetail.CreatedStoreID = intCreateStoreID;
                    //if (objInputChangeOrderType.ReturnFeeBook != 2)
                    //    debt += (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                    //else
                    //    debt += (decVNDCash + objVoucherDetail.ForeignCashExchange + decPaymentCardAmount - objVoucherDetail.RefundAmount);
                    //lst.Add(objVoucherDetail);
                    #endregion

                    if (objInputChangeOrderType.ReturnFeeBook == 2)
                    {
                        objVoucherDetail.VNDCash = decVNDCash;
                        objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                        if (!objInputChangeOrderType.IsInputReturn)
                        {
                            if (string.IsNullOrEmpty(txtSaleOrderID.Text))
                            {
                                if (Math.Round(decSumInputPrice) < Math.Round(decSumSalePrice) && ((Math.Round(decSumSalePrice) - Math.Round(decSumInputPrice)) >= decTotalReturnFee))
                                {
                                    decimal decTotalChenhlech = Math.Round(decReturnAmount - decTotalReturnFee);
                                    intTypeDebt = 1;
                                    decTotalMountSave = decTotalChenhlech;
                                    if (decPaymentCardAmount > 0 && decVNDCash > 0)
                                    {
                                        if (decPaymentCardAmount < decTotalChenhlech)
                                        {
                                            objVoucherDetail.VNDCash = decTotalChenhlech - Math.Round(decPaymentCardAmount);
                                        }
                                        else if (decPaymentCardAmount > decTotalChenhlech)
                                        {
                                            decFeePaymentCard = decPaymentCardAmount - decTotalChenhlech;
                                            objVoucherDetail.PaymentCardAmount = decTotalChenhlech;
                                            objVoucherDetail.VNDCash = 0;
                                            bolIsPaymentCard = true;
                                        }
                                        else
                                        {
                                            objVoucherDetail.PaymentCardAmount = decTotalChenhlech;
                                            objVoucherDetail.VNDCash = 0;
                                            decFeePaymentCard = decTotalReturnFee;
                                            bolIsPaymentCard = true;
                                        }
                                    }
                                    else if (decPaymentCardAmount > 0 && decVNDCash == 0)
                                    {
                                        objVoucherDetail.PaymentCardAmount = decTotalChenhlech;
                                        objVoucherDetail.VNDCash = 0;
                                        decFeePaymentCard = decTotalReturnFee;
                                        bolIsPaymentCard = true;
                                    }

                                }
                                else if (Math.Round(decSumInputPrice) < Math.Round(decSumSalePrice) && ((Math.Round(decSumSalePrice) - Math.Round(decSumInputPrice))) < decTotalReturnFee)
                                {
                                    bolIsExceptionSpend = true;
                                    decimal decTotalThu = Math.Round(decReturnAmount);
                                    decimal dectotalLech = Math.Round(decTotalReturnFee - decTotalThu);
                                    decTotalMountSave = dectotalLech;
                                    intTypeDebt = 2;
                                    decTotalLechType2 = dectotalLech;
                                    //if (decPaymentCardAmount == dectotalLech)
                                    //{
                                    //    objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                                    //    objVoucherDetail.VNDCash = 0;
                                    //}
                                    //else if (decPaymentCardAmount > dectotalLech)
                                    //{
                                    //    objVoucherDetail.PaymentCardAmount = dectotalLech;
                                    //    objVoucherDetail.VNDCash = 0;
                                    //    decFeePaymentCard = Math.Round(decPaymentCardAmount - dectotalLech);
                                    //    bolIsPaymentCard = true;
                                    //}
                                    //else
                                    //{
                                    //    objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                                    //    objVoucherDetail.VNDCash = Math.Round(dectotalLech - decPaymentCardAmount);
                                    //}
                                    if (decPaymentCardAmount > 0)
                                    {
                                        decFeePaymentCard = decPaymentCardAmount;
                                        objVoucherDetail.PaymentCardAmount = 0;
                                        objVoucherDetail.VNDCash = dectotalLech;
                                        bolIsPaymentCard = true;
                                    }
                                    else
                                    {
                                        objVoucherDetail.PaymentCardAmount = 0;
                                        objVoucherDetail.VNDCash = dectotalLech;
                                    }
                                }
                                else if (Math.Round(decSumInputPrice) > Math.Round(decSumSalePrice))
                                {
                                    intTypeDebt = 3;
                                    decimal dectotalLech = Math.Round(decSumInputPrice) - Math.Round(decSumSalePrice);
                                    objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                                    objVoucherDetail.VNDCash = decVNDCash + decTotalReturnFee;
                                    decTotalMountSave = decTotalReturnFee + dectotalLech;
                                }
                                else
                                {
                                    bolIsExceptionSpend = true;
                                    objVoucherDetail.PaymentCardAmount = 0;
                                    objVoucherDetail.VNDCash = decTotalReturnFee;
                                    decTotalMountSave = decTotalReturnFee;
                                    intTypeDebt = 2;
                                    decTotalLechType2 = decTotalReturnFee;
                                }
                            }
                            else
                            {
                                intTypeDebt = 4;
                                objVoucherDetail.VNDCash = decVNDCash + decTotalReturnFee;
                                objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                                decTotalMountSave = Math.Round(decSumInputPrice + decTotalReturnFee);
                            }

                        }
                        else
                        {

                            //if (bolSpecialCase)
                            //{
                            //    decimal declech = Math.Round(decTotalReturnFee - Convert.ToDecimal(txtReturnAmount.Text));
                            //    decTotalMountSave = declech;
                            //    if (decPaymentCardAmount > 0 && decVNDCash > 0)
                            //    {
                            //        if (decPaymentCardAmount > declech)
                            //        {
                            //            objVoucherDetail.VNDCash = 0;
                            //            objVoucherDetail.PaymentCardAmount = declech;
                            //            decFeePaymentCard = decPaymentCardAmount - declech;
                            //            bolIsPaymentCard = true;
                            //        }
                            //        else if (decPaymentCardAmount < declech)
                            //        {
                            //            objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                            //            objVoucherDetail.VNDCash = declech - decPaymentCardAmount;
                            //        }
                            //        else
                            //        {
                            //            objVoucherDetail.VNDCash = 0;
                            //            objVoucherDetail.PaymentCardAmount = declech;
                            //        }
                            //    }
                            //    else if (decVNDCash > 0 && decPaymentCardAmount == 0)
                            //    {
                            //        objVoucherDetail.VNDCash = declech;
                            //        objVoucherDetail.PaymentCardAmount = 0;
                            //    }
                            //    else if (decVNDCash == 0 && decPaymentCardAmount > 0)
                            //    {
                            //        objVoucherDetail.VNDCash = 0;
                            //        objVoucherDetail.PaymentCardAmount = declech;
                            //        decFeePaymentCard = Convert.ToDecimal(txtReturnAmount.Text) - declech;
                            //        bolIsPaymentCard = true;
                            //    }
                            //    decTotalLechType2 = declech;
                            //    intTypeDebt = 2;
                            //}
                            // else
                            //   {
                            intTypeDebt = 4;

                            objVoucherDetail.VNDCash = decVNDCash + decTotalReturnFee;
                            objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                            decTotalMountSave = decReturnAmount + decTotalReturnFee;

                            //  }
                        }

                    }
                    else
                    {
                        objVoucherDetail.VNDCash = decVNDCash;
                        objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                    }
                    if (intTypeDebt != 2)
                    {
                        objVoucherDetail.PaymentCardID = intPaymentCardID;
                        objVoucherDetail.PaymentCardName = strPaymentCardName;
                        objVoucherDetail.PaymentCardSpend = decPaymentCardSpend;
                        objVoucherDetail.PaymentCardVoucherID = strPaymentCardVoucherID;
                    }
                    else
                    {
                        objVoucherDetail.PaymentCardID = 0;
                        objVoucherDetail.PaymentCardName = string.Empty;
                        objVoucherDetail.PaymentCardSpend = 0;
                        objVoucherDetail.PaymentCardVoucherID = string.Empty;
                    }
                    objVoucherDetail.RefundAmount = 0;
                    objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                    objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objVoucherDetail.VoucherStoreID = intStoreID;
                    objVoucherDetail.CreatedStoreID = intCreateStoreID;
                    if (intTypeDebt != 2)
                        debt += (decVNDCash + objVoucherDetail.ForeignCashExchange + decPaymentCardAmount - objVoucherDetail.RefundAmount);
                    else
                        debt += decTotalLechType2;
                    lst.Add(objVoucherDetail);
                }

                PLC.InputChangeOrder.WSInputChangeOrder.Voucher objVoucher = new PLC.InputChangeOrder.WSInputChangeOrder.Voucher();
                objVoucher.VoucherStoreID = intStoreID;
                objVoucher.CashierUser = SystemConfig.objSessionUser.UserName;
                objVoucher.InvoiceID = txtInVoiceID.Text.Trim();
                objVoucher.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                objVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                objVoucher.CustomerName = txtCustomerName.Text.Trim();
                objVoucher.CustomerAddress = txtCustomerAddress.Text.Trim();
                objVoucher.CustomerPhone = txtCustomerPhone.Text.Trim();
                objVoucher.CustomerTaxID = txtTaxID.Text.Trim();
                objVoucher.ProvinceID = 0;
                objVoucher.DistrictID = 0;
                objVoucher.Gender = false;
                objVoucher.AgeRangeID = 0;
                objVoucher.VoucherDate = DateTime.Now;
                objVoucher.InvoiceDate = DateTime.Now;
                objVoucher.OrderID = "";
                objVoucher.VoucherConcern = "";
                objVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objVoucher.Content = string.Empty;
                objVoucher.CurrencyUnitID = intCurrencyUnitID;
                objVoucher.CurrencyExchange = decCurrencyExchange;
                objVoucher.VAT = 0;
                objVoucher.Debt = 0;
                //  if ()

                if (objInputChangeOrderType.ReturnFeeBook == 2 && intTypeDebt != 2)

                    debt += decTotalReturnFee;

                if (objInputChangeOrderType.ReturnFeeBook != 2)
                {
                    if (!bolIsDebt)
                    {
                        if ((!objInputChangeOrderType.IsInputReturn) && (!string.IsNullOrEmpty(txtSaleOrderID.Text)))
                        {
                            objVoucher.TotalMoney = objInputVoucher.TotalAmount;
                            objVoucher.TotalLiquidate = objInputVoucher.TotalAmount;
                        }
                        else
                        {
                            if (!bolSpecialCase)
                            {
                                objVoucher.TotalMoney = decReturnAmount;
                                objVoucher.TotalLiquidate = decReturnAmount;
                            }
                            else
                            {
                                decimal decSumInputPriceSpecialCase = 0;
                                DataRow[] drTemp = ((DataTable)flexDetail.DataSource).Select(string.Empty);
                                for (int i = 0; i < drTemp.Length; i++)
                                {
                                    decSumInputPriceSpecialCase += Convert.ToDecimal(drTemp[i]["InputPrice_In"]) * Convert.ToDecimal(drTemp[i]["Quantity"]);
                                }
                                objVoucher.TotalMoney = decSumInputPriceSpecialCase;
                                objVoucher.TotalLiquidate = decSumInputPriceSpecialCase;
                            }
                        }
                        objVoucher.Debt = objVoucher.TotalLiquidate - debt;
                    }
                    else
                    {
                        if ((!objInputChangeOrderType.IsInputReturn) && (!string.IsNullOrEmpty(txtSaleOrderID.Text)))
                        {
                            objVoucher.TotalMoney = objInputVoucher.TotalAmount;
                            objVoucher.TotalLiquidate = objInputVoucher.TotalAmount;
                        }
                        else
                        {
                            if (!bolSpecialCase)
                            {

                                objVoucher.TotalMoney = decReturnAmount;
                                objVoucher.TotalLiquidate = decReturnAmount;
                            }
                            else
                            {
                                decimal decSumInputPriceSpecialCase = 0;
                                DataRow[] drTemp = ((DataTable)flexDetail.DataSource).Select(string.Empty);
                                for (int i = 0; i < drTemp.Length; i++)
                                {
                                    decSumInputPriceSpecialCase += Convert.ToDecimal(drTemp[i]["InputPrice_In"]) * Convert.ToDecimal(drTemp[i]["Quantity"]);
                                }
                                objVoucher.TotalMoney = decSumInputPriceSpecialCase;
                                objVoucher.TotalLiquidate = decSumInputPriceSpecialCase;
                            }
                        }
                        if (bolIsNotCreateOutVoucherDetail)
                            objVoucher.Debt = objInputVoucher.TotalAmount;
                        else
                        {
                            //nếu trả hết ngay lần 1 thì xét lại Còn nợ của phiếu chi
                            //if (dtbDetailAll.Select("SalePrice > 0").Length == dtbOVDetailReturn.Select("SalePrice > 0").Length && !string.IsNullOrEmpty(objOutputVoucherLoad.OrderID))
                            //    objVoucher.Debt = objInputVoucher.TotalAmount - decTotalPaid;
                            //else
                            //{
                            //    objVoucher.Debt = objVoucher.TotalLiquidate - (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                            //}
                            objVoucher.Debt = objVoucher.TotalLiquidate - debt;
                        }
                        objInputChangeOrder.IsNotCreateOutVoucherDetail = bolIsNotCreateOutVoucherDetail;
                    }
                }
                else
                {
                    if (!bolIsDebt)
                    {
                        if ((!objInputChangeOrderType.IsInputReturn) && (!string.IsNullOrEmpty(txtSaleOrderID.Text)))
                        {
                            objVoucher.TotalMoney = objInputVoucher.TotalAmount;
                            objVoucher.TotalLiquidate = objInputVoucher.TotalAmount;
                        }
                        else
                        {
                            if (!bolSpecialCase)
                            {
                                objVoucher.TotalMoney = decTotalMountSave;
                                objVoucher.TotalLiquidate = decTotalMountSave;
                            }
                            else
                            {
                                decimal decSumInputPriceSpecialCase = 0;
                                DataRow[] drTemp = ((DataTable)flexDetail.DataSource).Select(string.Empty);
                                for (int i = 0; i < drTemp.Length; i++)
                                {
                                    decSumInputPriceSpecialCase += Convert.ToDecimal(drTemp[i]["InputPrice_In"]) * Convert.ToDecimal(drTemp[i]["Quantity"]);
                                }
                                objVoucher.TotalMoney = decSumInputPriceSpecialCase;
                                objVoucher.TotalLiquidate = decSumInputPriceSpecialCase;
                            }
                        }
                        objVoucher.Debt = objVoucher.TotalLiquidate - debt;
                    }
                    else
                    {
                        if ((!objInputChangeOrderType.IsInputReturn) && (!string.IsNullOrEmpty(txtSaleOrderID.Text)))
                        {
                            objVoucher.TotalMoney = objInputVoucher.TotalAmount;
                            objVoucher.TotalLiquidate = objInputVoucher.TotalAmount;
                        }
                        else
                        {
                            if (!bolSpecialCase)
                            {
                                objVoucher.TotalMoney = decTotalMountSave;
                                objVoucher.TotalLiquidate = decTotalMountSave;
                            }
                            else
                            {
                                decimal decSumInputPriceSpecialCase = 0;
                                DataRow[] drTemp = ((DataTable)flexDetail.DataSource).Select(string.Empty);
                                for (int i = 0; i < drTemp.Length; i++)
                                {
                                    decSumInputPriceSpecialCase += Convert.ToDecimal(drTemp[i]["InputPrice_In"]) * Convert.ToDecimal(drTemp[i]["Quantity"]);
                                }
                                objVoucher.TotalMoney = decSumInputPriceSpecialCase;
                                objVoucher.TotalLiquidate = decSumInputPriceSpecialCase;
                            }
                        }
                        if (bolIsNotCreateOutVoucherDetail)
                            objVoucher.Debt = objInputVoucher.TotalAmount;
                        else
                        {
                            //nếu trả hết ngay lần 1 thì xét lại Còn nợ của phiếu chi
                            //if (dtbDetailAll.Select("SalePrice > 0").Length == dtbOVDetailReturn.Select("SalePrice > 0").Length && !string.IsNullOrEmpty(objOutputVoucherLoad.OrderID))
                            //    objVoucher.Debt = objInputVoucher.TotalAmount - decTotalPaid;
                            //else
                            //{
                            //    objVoucher.Debt = objVoucher.TotalLiquidate - (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                            //}
                            objVoucher.Debt = objVoucher.TotalLiquidate - debt;
                        }
                        objInputChangeOrder.IsNotCreateOutVoucherDetail = bolIsNotCreateOutVoucherDetail;
                    }
                }
                if (objVoucher.Debt < 0)
                    objVoucher.Debt = 0;
                objInputChangeOrder.IsDebt = bolIsDebt;
                objVoucher.IsInVoucherOfSaleOrder = false;
                objVoucher.CreatedStoreID = intCreateStoreID;
                objVoucher.OrderID = "";
                objVoucher.IsSupplementary = false;
                objVoucher.ShippingCost = 0;
                objVoucher.VoucherDetailList = bolSpecialCase ? null : lst.ToArray();
                if (Math.Round(decSumInputPrice) != Math.Round(decSumSalePrice))
                {
                    if (Math.Round(decSumInputPrice) > Math.Round(decSumSalePrice))
                    {
                        objVoucher.IsSpend = true;
                        // if (bolSpecialCase && objInputChangeOrderType.ReturnFeeBook == 1)
                        //     objVoucher.IsSpend = false;
                        if (objVoucher.IsSpend)
                        {
                            objVoucher.VoucherTypeID = objInputChangeOrderType.OutVoucherTypeID;
                            if (objVoucher.VoucherTypeID < 1)
                            {
                                DataRow[] drOutVoucherType = dtbInputTypeAll.Select("InputTypeID = " + objInputVoucher.InputTypeID);
                                if (drOutVoucherType.Length > 0)
                                    objVoucher.VoucherTypeID = Convert.ToInt32(drOutVoucherType[0]["VoucherTypeID"]);
                            }
                            if (objVoucher.VoucherTypeID < 1)
                            {
                                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức chi tiền từ hình thức [" + cboInputTypeID.Text + "]. Vui lòng kiểm tra lại");
                                btnCreateConcern.Enabled = true;
                                return;
                            }
                        }
                        else
                        {
                            objVoucher.VoucherTypeID = objInputChangeOrderType.InVoucherTypeID;
                            if (objVoucher.VoucherTypeID < 1)
                                objVoucher.VoucherTypeID = objPLCOutputVoucher.CheckVoucherByOrderID(objOutputVoucherLoad.OrderID, objOutputVoucherLoad.OutputVoucherID);
                            if (objVoucher.VoucherTypeID < 1)
                            {
                                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức thu tiền từ phiếu xuất cũ " + objOutputVoucherLoad.OutputVoucherID + ". Vui lòng kiểm tra lại");
                                btnCreateConcern.Enabled = true;
                                return;
                            }
                        }
                    }
                    else
                    {
                        objVoucher.IsSpend = false;
                        if (bolIsExceptionSpend)
                            objVoucher.IsSpend = true;
                        if (objVoucher.IsSpend)
                        {
                            objVoucher.VoucherTypeID = objInputChangeOrderType.OutVoucherTypeID;
                            if (objVoucher.VoucherTypeID < 1)
                            {
                                DataRow[] drOutVoucherType = dtbInputTypeAll.Select("InputTypeID = " + objInputVoucher.InputTypeID);
                                if (drOutVoucherType.Length > 0)
                                    objVoucher.VoucherTypeID = Convert.ToInt32(drOutVoucherType[0]["VoucherTypeID"]);
                            }
                            if (objVoucher.VoucherTypeID < 1)
                            {
                                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức chi tiền từ hình thức [" + cboInputTypeID.Text + "]. Vui lòng kiểm tra lại");
                                btnCreateConcern.Enabled = true;
                                return;
                            }
                        }
                        else
                        {
                            objVoucher.VoucherTypeID = objInputChangeOrderType.InVoucherTypeID;
                            if (objVoucher.VoucherTypeID < 1)
                                objVoucher.VoucherTypeID = objPLCOutputVoucher.CheckVoucherByOrderID(objOutputVoucherLoad.OrderID, objOutputVoucherLoad.OutputVoucherID);
                            if (objVoucher.VoucherTypeID < 1)
                            {
                                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức thu tiền từ phiếu xuất cũ " + objOutputVoucherLoad.OutputVoucherID + ". Vui lòng kiểm tra lại");
                                btnCreateConcern.Enabled = true;
                                return;
                            }
                        }
                    }
                }
                else
                {
                    if (objInputChangeOrderType.ReturnFeeBook != 2)
                    {
                        if (objInputChangeOrderType.IsInputReturn && objInputChangeOrder.IsDebt)
                        {
                            objVoucher.IsSpend = true;
                            objVoucher.VoucherTypeID = objInputChangeOrderType.OutVoucherTypeID;
                            if (objVoucher.VoucherTypeID < 1)
                            {
                                DataRow[] drOutVoucherType = dtbInputTypeAll.Select("InputTypeID = " + objInputVoucher.InputTypeID);
                                if (drOutVoucherType.Length > 0)
                                    objVoucher.VoucherTypeID = Convert.ToInt32(drOutVoucherType[0]["VoucherTypeID"]);
                            }
                            if (objVoucher.VoucherTypeID < 1)
                            {
                                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức chi tiền từ hình thức [" + cboInputTypeID.Text + "]. Vui lòng kiểm tra lại");
                                btnCreateConcern.Enabled = true;
                                return;
                            }
                        }
                    }
                    else
                    {
                        objVoucher.IsSpend = true;
                        objVoucher.VoucherTypeID = objInputChangeOrderType.OutVoucherTypeID;
                        if (objVoucher.VoucherTypeID < 1)
                        {
                            DataRow[] drOutVoucherType = dtbInputTypeAll.Select("InputTypeID = " + objInputVoucher.InputTypeID);
                            if (drOutVoucherType.Length > 0)
                                objVoucher.VoucherTypeID = Convert.ToInt32(drOutVoucherType[0]["VoucherTypeID"]);
                        }
                        if (objVoucher.VoucherTypeID < 1)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức chi tiền từ hình thức [" + cboInputTypeID.Text + "]. Vui lòng kiểm tra lại");
                            btnCreateConcern.Enabled = true;
                            return;
                        }
                    }
                }

                PLC.InputChangeOrder.WSInputChangeOrder.Voucher objVoucherForReturnFee = null;
                if (objInputChangeOrderType.ReturnFeeBook == 2)
                {
                    objVoucherForReturnFee = new PLC.InputChangeOrder.WSInputChangeOrder.Voucher();
                    objVoucherForReturnFee.VoucherStoreID = intStoreID;
                    objVoucherForReturnFee.CashierUser = SystemConfig.objSessionUser.UserName;
                    objVoucherForReturnFee.InvoiceID = txtInVoiceID.Text.Trim();
                    objVoucherForReturnFee.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                    objVoucherForReturnFee.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                    objVoucherForReturnFee.CustomerName = txtCustomerName.Text.Trim();
                    objVoucherForReturnFee.CustomerAddress = txtCustomerAddress.Text.Trim();
                    objVoucherForReturnFee.CustomerPhone = txtCustomerPhone.Text.Trim();
                    objVoucherForReturnFee.CustomerTaxID = txtTaxID.Text.Trim();
                    objVoucherForReturnFee.ProvinceID = 0;
                    objVoucherForReturnFee.DistrictID = 0;
                    objVoucherForReturnFee.Gender = false;
                    objVoucherForReturnFee.AgeRangeID = 0;
                    objVoucherForReturnFee.VoucherDate = DateTime.Now;
                    objVoucherForReturnFee.InvoiceDate = DateTime.Now;
                    objVoucherForReturnFee.OrderID = "";
                    objVoucherForReturnFee.VoucherConcern = strInputChangeOrderID;
                    objVoucherForReturnFee.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objVoucherForReturnFee.Content = string.Empty;
                    objVoucherForReturnFee.CurrencyUnitID = intCurrencyUnitID;
                    objVoucherForReturnFee.CurrencyExchange = decCurrencyExchange;
                    objVoucherForReturnFee.VAT = 0;
                    objVoucherForReturnFee.Debt = bolSpecialCase ? decTotalReturnFee : 0;
                    objVoucherForReturnFee.TotalMoney = decTotalReturnFee;
                    objVoucherForReturnFee.TotalLiquidate = decTotalReturnFee;
                    objVoucherForReturnFee.IsInVoucherOfSaleOrder = false;
                    objVoucherForReturnFee.CreatedStoreID = intCreateStoreID;
                    objVoucherForReturnFee.OrderID = "";
                    objVoucherForReturnFee.IsSupplementary = false;
                    objVoucherForReturnFee.ShippingCost = 0;
                    //  objVoucher.VoucherDetailList = lst.ToArray(); bổ sung sau
                    objVoucherForReturnFee.IsSpend = false;
                    objVoucherForReturnFee.VoucherTypeID = objInputChangeOrderType.FeeVoucherType;
                    if (objVoucherForReturnFee.VoucherTypeID < 1)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức thu tiền từ phiếu xuất cũ " + objOutputVoucherLoad.OutputVoucherID + ". Vui lòng kiểm tra lại");
                        btnCreateConcern.Enabled = true;
                        return;
                    }
                    List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail> lstVoucherDetailForReturnFee = new List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail>();

                    if (bolSpecialCase)
                        lstVoucherDetailForReturnFee = null;
                    else
                    {
                        if (intTypeDebt != 2 || (intTypeDebt == 2 && lstAddVoucherDetail.Count < 1))
                        {
                            PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetailForReturnFee = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                            if (bolIsPaymentCard && decFeePaymentCard > 0)
                            {
                                objVoucherDetailForReturnFee.VNDCash = decTotalReturnFee - decFeePaymentCard;
                                objVoucherDetailForReturnFee.PaymentCardAmount = decFeePaymentCard;
                                objVoucherDetailForReturnFee.PaymentCardID = intPaymentCardID;
                                objVoucherDetailForReturnFee.PaymentCardName = strPaymentCardName;
                                objVoucherDetailForReturnFee.PaymentCardSpend = decPaymentCardSpend;
                                objVoucherDetailForReturnFee.PaymentCardVoucherID = strPaymentCardVoucherID;
                            }
                            else
                            {
                                objVoucherDetailForReturnFee.VNDCash = decTotalReturnFee;
                                objVoucherDetailForReturnFee.PaymentCardAmount = 0;
                                objVoucherDetailForReturnFee.PaymentCardID = 0;
                                objVoucherDetailForReturnFee.PaymentCardName = string.Empty;
                                objVoucherDetailForReturnFee.PaymentCardSpend = 0;
                                objVoucherDetailForReturnFee.PaymentCardVoucherID = string.Empty;
                            }
                            objVoucherDetailForReturnFee.RefundAmount = 0;
                            objVoucherDetailForReturnFee.CashierUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDetailForReturnFee.CreatedUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDetailForReturnFee.VoucherStoreID = intStoreID;
                            objVoucherDetailForReturnFee.CreatedStoreID = intCreateStoreID;
                            //debt += (objVoucherDetailForReturnFee.VNDCash + objVoucherDetailForReturnFee.ForeignCashExchange + objVoucherDetailForReturnFee.PaymentCardAmount - objVoucherDetailForReturnFee.RefundAmount);
                            lstVoucherDetailForReturnFee.Add(objVoucherDetailForReturnFee);


                        }
                        else
                        {
                            if (lstTemp.Count > 0)
                            {
                                decimal decSumFeeVNDCash = lstTemp.Sum(x => x.VNDCash);
                                decimal decSumFeePaymentAmount = lstTemp.Sum(x => x.PaymentCardAmount);
                                decimal decFeeNeed = Math.Round(decTotalReturnFee - decSumFeeVNDCash - decSumFeePaymentAmount);
                                PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetailForReturnFee = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                                objVoucherDetailForReturnFee.VNDCash = decFeeNeed;
                                objVoucherDetailForReturnFee.PaymentCardAmount = 0;
                                objVoucherDetailForReturnFee.PaymentCardID = 0;
                                objVoucherDetailForReturnFee.PaymentCardName = string.Empty;
                                objVoucherDetailForReturnFee.PaymentCardSpend = 0;
                                objVoucherDetailForReturnFee.PaymentCardVoucherID = string.Empty;

                                objVoucherDetailForReturnFee.RefundAmount = 0;
                                objVoucherDetailForReturnFee.CashierUser = SystemConfig.objSessionUser.UserName;
                                objVoucherDetailForReturnFee.CreatedUser = SystemConfig.objSessionUser.UserName;
                                objVoucherDetailForReturnFee.VoucherStoreID = intStoreID;
                                objVoucherDetailForReturnFee.CreatedStoreID = intCreateStoreID;
                                //debt += (objVoucherDetailForReturnFee.VNDCash + objVoucherDetailForReturnFee.ForeignCashExchange + objVoucherDetailForReturnFee.PaymentCardAmount - objVoucherDetailForReturnFee.RefundAmount);
                                lstTemp.Add(objVoucherDetailForReturnFee);
                                lstVoucherDetailForReturnFee.AddRange(lstTemp);
                            }
                        }
                        objVoucherForReturnFee.VoucherDetailList = lstVoucherDetailForReturnFee.ToArray();
                    }
                }
                if (decTotalReturnFee == 0) //|| bolIsNotCreateOutVoucherDetail)
                {
                    objVoucherForReturnFee = null;
                }

                PLC.InputChangeOrder.WSInputChangeOrder.Voucher objVoucherForSaleOrder = null;
                List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail> lstForSaleOrder = new List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail>();
                #region Phiếu thu tiền đơn hàng
                if ((!objInputChangeOrderType.IsInputReturn) && (!string.IsNullOrEmpty(txtSaleOrderID.Text)))
                {
                    //objVoucher.VoucherDetailList[0].VNDCash = decVNDCash_Out;
                    //objVoucher.VoucherDetailList[0].PaymentCardAmount = decPaymentCardAmount_Out;
                    //objVoucher.VoucherDetailList[0].PaymentCardID = intPaymentCardID_Out;
                    //objVoucher.VoucherDetailList[0].PaymentCardSpend = decPaymentCardSpend_Out;
                    //objVoucher.VoucherDetailList[0].PaymentCardVoucherID = strPaymentCardVoucherID_Out;

                    //objVoucher.Debt = objVoucher.TotalLiquidate - (objVoucher.VoucherDetailList[0].VNDCash + objVoucher.VoucherDetailList[0].ForeignCashExchange + objVoucher.VoucherDetailList[0].PaymentCardAmount - objVoucher.VoucherDetailList[0].RefundAmount);
                    debt = 0;
                    if (lstAddVoucherDetail_In.Count > 0)
                    {
                        foreach (var item in lstAddVoucherDetail_In)
                        {
                            PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDTForSaleOrder = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                            objVoucherDTForSaleOrder.VNDCash = item.VNDCash;
                            objVoucherDTForSaleOrder.PaymentCardAmount = item.PaymentCardAmount;
                            objVoucherDTForSaleOrder.PaymentCardID = item.PaymentCardID;
                            objVoucherDTForSaleOrder.PaymentCardSpend = item.PaymentCardSpend;
                            objVoucherDTForSaleOrder.PaymentCardVoucherID = item.PaymentCardVoucherID;
                            if (item.RefundAmount > 0)
                                objVoucherDTForSaleOrder.RefundAmount = item.RefundAmount;

                            objVoucherDTForSaleOrder.RefundAmount = 0;
                            objVoucherDTForSaleOrder.CashierUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDTForSaleOrder.CreatedUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDTForSaleOrder.VoucherStoreID = intStoreID;
                            objVoucherDTForSaleOrder.CreatedStoreID = intCreateStoreID;
                            debt += (objVoucherDTForSaleOrder.VNDCash + objVoucherDTForSaleOrder.ForeignCashExchange + objVoucherDTForSaleOrder.PaymentCardAmount - objVoucherDTForSaleOrder.RefundAmount);
                            lstForSaleOrder.Add(objVoucherDTForSaleOrder);
                        }
                    }
                    else
                    {
                        PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDTForSaleOrder = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                        objVoucherDTForSaleOrder.VNDCash = decVNDCash_In;
                        objVoucherDTForSaleOrder.PaymentCardAmount = decPaymentCardAmount_In;
                        objVoucherDTForSaleOrder.PaymentCardID = intPaymentCardID_In;
                        objVoucherDTForSaleOrder.PaymentCardSpend = decPaymentCardSpend_In;
                        objVoucherDTForSaleOrder.PaymentCardVoucherID = strPaymentCardVoucherID_In;
                        if (decRefundAmount_In > 0)
                            objVoucherDTForSaleOrder.RefundAmount = decRefundAmount_In;

                        objVoucherDTForSaleOrder.RefundAmount = 0;
                        objVoucherDTForSaleOrder.CashierUser = SystemConfig.objSessionUser.UserName;
                        objVoucherDTForSaleOrder.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objVoucherDTForSaleOrder.VoucherStoreID = intStoreID;
                        objVoucherDTForSaleOrder.CreatedStoreID = intCreateStoreID;
                        debt += (objVoucherDTForSaleOrder.VNDCash + objVoucherDTForSaleOrder.ForeignCashExchange + objVoucherDTForSaleOrder.PaymentCardAmount - objVoucherDTForSaleOrder.RefundAmount);
                        lstForSaleOrder.Add(objVoucherDTForSaleOrder);
                    }
                    objVoucherForSaleOrder = new PLC.InputChangeOrder.WSInputChangeOrder.Voucher();
                    objVoucherForSaleOrder.VoucherStoreID = intStoreID;
                    objVoucherForSaleOrder.CashierUser = SystemConfig.objSessionUser.UserName;
                    objVoucherForSaleOrder.IsInVoucherOfSaleOrder = true;
                    objVoucherForSaleOrder.InvoiceID = txtInVoiceID.Text.Trim();
                    objVoucherForSaleOrder.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                    objVoucherForSaleOrder.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                    objVoucherForSaleOrder.CustomerName = txtCustomerName.Text.Trim();
                    objVoucherForSaleOrder.CustomerAddress = txtCustomerAddress.Text.Trim();
                    objVoucherForSaleOrder.CustomerPhone = txtCustomerPhone.Text.Trim();
                    objVoucherForSaleOrder.CustomerTaxID = txtTaxID.Text.Trim();
                    objVoucherForSaleOrder.ProvinceID = 0;
                    objVoucherForSaleOrder.DistrictID = 0;
                    objVoucherForSaleOrder.Gender = false;
                    objVoucherForSaleOrder.AgeRangeID = 0;
                    objVoucherForSaleOrder.VoucherDate = DateTime.Now;
                    objVoucherForSaleOrder.InvoiceDate = DateTime.Now;
                    objVoucherForSaleOrder.OrderID = "";
                    objVoucherForSaleOrder.VoucherConcern = "";
                    objVoucherForSaleOrder.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objVoucherForSaleOrder.Content = string.Empty;
                    objVoucherForSaleOrder.CurrencyUnitID = intCurrencyUnitID;
                    objVoucherForSaleOrder.CurrencyExchange = decCurrencyExchange;
                    objVoucherForSaleOrder.TotalMoney = objSaleOrder_Out.TotalLiquidate;
                    objVoucherForSaleOrder.TotalLiquidate = objSaleOrder_Out.TotalLiquidate;
                    objVoucherForSaleOrder.TotalPaid = objSaleOrder_Out.TotalLiquidate;
                    objVoucherForSaleOrder.VoucherTypeID = objSaleOrder_Out.VoucherTypeID;

                    objVoucherForSaleOrder.Debt = 0;
                    if (!bolIsDebt)
                    {
                        objVoucherForSaleOrder.Debt = objVoucher.TotalLiquidate - debt;
                    }
                    else
                    {
                        if (bolIsNotCreateOutVoucherDetail)
                            objVoucherForSaleOrder.Debt = objInputVoucher.TotalAmount;
                        else
                        {
                            objVoucherForSaleOrder.Debt = objVoucher.TotalLiquidate - debt;
                        }
                        objInputChangeOrder.IsNotCreateOutVoucherDetail = bolIsNotCreateOutVoucherDetail;
                    }
                    if (objVoucher.Debt < 0)
                        objVoucherForSaleOrder.Debt = 0;
                    objVoucherForSaleOrder.Debt = 0;
                    objVoucherForSaleOrder.VAT = 0;
                    objVoucherForSaleOrder.CreatedStoreID = intCreateStoreID;
                    objVoucherForSaleOrder.OrderID = "";
                    objVoucherForSaleOrder.IsSupplementary = false;
                    objVoucherForSaleOrder.ShippingCost = 0;
                    objVoucherForSaleOrder.VoucherDetailList = lstForSaleOrder.ToArray();//new[] { objVoucherDTForSaleOrder };
                    objVoucherForSaleOrder.VoucherConcern = txtSaleOrderID.Text.Trim();
                    if (intPaymentCardID_In > 0)
                        objVoucherForSaleOrder.PaymentCardInfo = "Thanh toán bằng " + strPaymentCardName_In + ": " + decPaymentCardAmount_In.ToString("#,##0") + " - Số HĐ cà thẻ: " + strPaymentCardVoucherID_In;

                }
                #endregion

                #endregion

                bool bolResult = false;
                string stringPackageCode = string.Empty;
                if (objSaleOrder_Out != null)
                {
                    stringPackageCode = objPLCSaleOrders.CheckSaleOrderIsSubsidy(objSaleOrder_Out.SaleOrderID);
                }

                if (!objInputChangeOrderType.IsInputReturn)
                {
                    #region NDC -- subsidy
                    bool isSubsidy = false;
                    PLCVTT_VERIFIEDPARTNER vttPLC = new PLCVTT_VERIFIEDPARTNER();
                    //-- NDC: có phải là đổi hàng subsidy
                    VTT_VERIFIEDPARTNER objVERIFIEDPARTNER = null;
                    string oldIMEI = "";
                    string newIMEI = "";
                    PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail subsidyDetail = null;
                    if (objSaleOrder_Out != null)
                    {
                        // trường hợp có saleorder out:
                        subsidyDetail = objInputChangeOrder.InputChangeOrderDetailList[0];
                        oldIMEI = subsidyDetail.IMEI_In;
                        var obj = objSaleOrder_Out.SaleOrderDetailList.Where(x => x.IsPromotionProduct == false && !string.IsNullOrEmpty(x.IMEI)).FirstOrDefault();
                        if (obj != null)
                            newIMEI = obj.IMEI;
                        objVERIFIEDPARTNER = vttPLC.GetByIMEI(oldIMEI);
                    }
                    else
                    {
                        foreach (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail detail in objInputChangeOrder.InputChangeOrderDetailList)
                        {
                            oldIMEI = detail.IMEI_In;
                            if (!string.IsNullOrEmpty(oldIMEI))
                            {
                                // check nếu có chương trình subsidy cho IMEI này
                                objVERIFIEDPARTNER = vttPLC.GetByIMEI(oldIMEI);
                                if (objVERIFIEDPARTNER != null)
                                {
                                    subsidyDetail = detail;
                                    newIMEI = detail.IMEI_Out;
                                    break;
                                }
                            }
                        }
                    }
                    //  bool flag = false;
                    //là đổi trà sub, ko check đổi HTX-lấy KM hoặc check HTX-lấy KM và đơn hàng mới là subsidy
                    if (objVERIFIEDPARTNER != null && objSaleOrder_Out == null)
                    {

                        // có chương trình subsidy:
                        if (string.IsNullOrEmpty(newIMEI))
                        {
                            MessageBox.Show("[Subsidy] không có IMEI sản phẩm đổi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        objVERIFIEDPARTNER.IMEI = newIMEI;
                        string oldPartnerCode = objVERIFIEDPARTNER.PAKAgeCode.Trim();
                        //     decimal oldPrice = -1;
                        //   decimal newPrice = -2;
                        string oldProductID = subsidyDetail.ProductID_In;
                        string newProductID = "";
                        //   var r = dtbOVDetailReturn.Select("IMEI = " + oldIMEI)[0];

                        newProductID = subsidyDetail.ProductID_Out;
                        //      decimal.TryParse(r["SALEPRICE_OUT"].ToString(), out newPrice);

                        //  decimal.TryParse(r["SALEPRICE"].ToString(), out oldPrice);
                        // oldPrice = Math.Round(oldPrice, 0);
                        // newPrice = Math.Round(newPrice, 0);

                        //if (oldPrice != newPrice)
                        //{
                        //    MessageBox.Show("[Subsidy] Chênh lệch giá giữa 2 sản phẩm.Vui lòng kiểm tra lại!");
                        //    return;
                        //}
                        int checkResult = vttPLC.CheckForExchangeSubsidy(oldProductID, newProductID, oldPartnerCode, subsidyDetail.IMEI_Out);
                        switch (checkResult)
                        {
                            case 1:
                                isSubsidy = true;
                                break;
                            case 2:
                                MessageBox.Show("[Subsidy] Imei xuất và Imei nhập không cùng Model.Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            case 3:
                                MessageBox.Show("[Subsidy] Mã KM đối tác của Imei xuất không giống Imei nhập.Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            default:
                                MessageBox.Show("[Subsidy] Có lỗi trong quá trình kiểm tra Imei mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;

                        }
                        //  flag = true;
                    }


                    // kiểm tra IMEI mới có thỏa đk subsidy ko

                    //-- NDC ----------------------------
                    #endregion
                    #region Tạo
                    if (string.IsNullOrEmpty(objInputChangeOrder.SaleOrderID))
                    {
                        if ((!objVoucher.IsSpend) && objVoucher.VoucherDetailList.Length > 0 && objVoucher.VoucherDetailList[0].PaymentCardID > 0)
                        {
                            string strPaymentCardInfo = "Thanh toán bằng " + objVoucher.VoucherDetailList[0].PaymentCardName + ": " + objVoucher.VoucherDetailList[0].PaymentCardAmount.ToString("#,##0") + " - Số HĐ cà thẻ: " + objVoucher.VoucherDetailList[0].PaymentCardVoucherID;
                            objOutputVoucher.OutputContent = strPaymentCardInfo;
                        }
                        else
                        {
                            if (Math.Round(decReturnAmount) == 0 && txtContentOV.Text.Trim() != string.Empty)
                                objOutputVoucher.OutputContent = txtContentOV.Text.Trim();
                            else
                            {
                                if (txtContentOV.Text.ToLower().Replace(" ", string.Empty).IndexOf("sốhđcàthẻ") > 0)
                                    objOutputVoucher.OutputContent = txtContentOV.Text.Trim();
                            }
                        }

                        if ((objVoucher.TotalLiquidate == 0 && objInputChangeOrderType.ReturnFeeBook == 2) || bolIsCreateVoucherReturnFee)
                            objVoucher = null;
                        string[] arrVATInvoiceList = null;
                        bolResult = objPLCInputChangeOrder.CreateInputVoucherAndOutputVoucherAndInOutVoucher(objInputChangeOrder, objInputVoucher, objOutputVoucher, objVoucher, objVoucherForReturnFee, ref arrVATInvoiceList);

                        if (!bolResult)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            btnCreateConcern.Enabled = true;
                            return;
                        }




                        #region Đăng bổ sung 07/03/2018
                        if (arrVATInvoiceList != null)
                        {
                            ////le van long/Tích điểm đổi trả/03/06/2020
                            string strInvoiceID = "";
                            string strInvoiceIDNew = "";
                            int index = 0;
                            foreach (string strVATInvoiceID in arrVATInvoiceList)
                            {
                                if (index == 0)
                                {
                                    strInvoiceIDNew = strVATInvoiceID;
                                }
                                else
                                {
                                    strInvoiceID = strVATInvoiceID;
                                }
                                index = index + 1;
                            }

                            if (!string.IsNullOrEmpty(strInvoiceID))
                            {
                                ERP.SalesAndServices.SaleOrders.DUI.VtPlusUrl.getAllUrlApi();
                                int tongdiem = vtPlus.Getpoint(txtCustomerPhone.Text.Trim(), "Lấy tổng điểm : chức năng đổi trả");
                                string CheckVTplus = vtPlus.CallApi_checkExitVtAccount(txtCustomerPhone.Text.Trim(), "Kiểm tra khách hàng tồn tại trong hệ thống - Chức năng: nhập đổi trả");
                                string messageVtplus = VtplusPointAdd(strInputChangeOrderID, strInvoiceID, strInvoiceIDNew, CheckVTplus, tongdiem);
                                //messageVtplus = messageVtplus + VtplusReturnPoint(strInputChangeOrderID, CheckVTplus);
                                messageVtplus = messageVtplus + VtplusRollbackAdjustAccountPoint(strInputChangeOrderID, strInvoiceID, CheckVTplus);
                                if (!string.IsNullOrEmpty(messageVtplus))
                                {
                                    MessageBox.Show(messageVtplus
                                        , "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                }
                            }

                            foreach (string strVATInvoiceID in arrVATInvoiceList)
                            {
                                if (!string.IsNullOrWhiteSpace(strVATInvoiceID))
                                {
                                    ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
                                    if (!string.IsNullOrEmpty(strvtplusPointTs))
                                    {
                                        if (strVATInvoiceID == strInvoiceID)
                                        {
                                            objPLCPrintVAT.CreateVATSInvoiceByVATInvoiceID(strVATInvoiceID, strvtplusPointTs);
                                        }
                                        else
                                        {
                                            objPLCPrintVAT.CreateVATSInvoiceByVATInvoiceID(strVATInvoiceID);
                                        }
                                    }
                                    else
                                    {
                                        objPLCPrintVAT.CreateVATSInvoiceByVATInvoiceID(strVATInvoiceID);
                                    }
                                }
                            }

                            List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice> lstVAT_Invoice = new List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice>();
                            foreach (string strVATInvoiceID in arrVATInvoiceList)
                            {
                                PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVAT_InvoiceMain = null;
                                new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().LoadInfo(ref objVAT_InvoiceMain, strVATInvoiceID);
                                lstVAT_Invoice.Add(objVAT_InvoiceMain);
                            }
                            if (lstVAT_Invoice != null && lstVAT_Invoice.Count() > 0)
                            {
                                ERP.PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                                if (lstVAT_Invoice.Count() == 1)
                                {
                                    int VatPagesPrint = objInputChangeOrderType.VatPagesPrint == 0 ? 3 : objInputChangeOrderType.VatPagesPrint;
                                    objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, lstVAT_Invoice[0], false, false, "", VatPagesPrint);
                                }
                                else
                                {
                                    int VatPagesPrint = objInputChangeOrderType.VatPagesPrint == 0 ? 3 : objInputChangeOrderType.VatPagesPrint;
                                    string strVatPagesPrint = string.Concat(Enumerable.Repeat(VatPagesPrint.ToString() + ",", lstVAT_Invoice.Count));
                                    strVatPagesPrint = strVatPagesPrint.Substring(0, strVatPagesPrint.Length - 1);
                                    objPrintVAT_PrepareData_Viettel.PrintVATInvoiceLIST(this, lstVAT_Invoice, false, false, "", strVatPagesPrint);
                                }
                            }
                        }
                        #endregion
                        #region Đăng rào code 07/03/2018
                        //ERP.PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                        //objPrintVAT_PrepareData_Viettel.CreateVATInvoiceInputChangeOrder(this, 2, objInputChangeOrder.InputChangeOrderID, intInputChangeOrderTypeID);
                        #endregion
                        #region Đổi hàng bán (bỏ code)
                        //DataTable dtbData = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_CHANGEORDER_AUTO_SEL", new object[] { "@INPUTCHANGEORDERID", objInputChangeOrder.InputChangeOrderID });
                        //if (dtbData != null)
                        //{
                        //    int intIsAutoCreateInvoice = 0;
                        //    int.TryParse(dtbData.Rows[0][0].ToString(), out intIsAutoCreateInvoice);
                        //    int intInvoiceNo = 0;
                        //    int.TryParse(objOutputVoucherLoad.InvoiceID, out intInvoiceNo);
                        //    DataTable dtbInvoiceId = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_INVOICE_SELBYOUTPUTID", new object[] { "@OUTPUTVOUCHERID", dtbData.Rows[0][3].ToString() });
                        //    if (intIsAutoCreateInvoice == 1 && intInvoiceNo > 0 && dtbInvoiceId != null && dtbInvoiceId.Rows.Count > 0)
                        //    {
                        //        strInputVoucherId = dtbData.Rows[0][1].ToString();
                        //        strOutputVoucherID = dtbData.Rows[0][2].ToString();
                        //        strOldOutputVoucherID = dtbData.Rows[0][3].ToString();
                        //        strInputVoucherReturnId = dtbData.Rows[0][4].ToString();
                        //        int.TryParse(dtbData.Rows[0]["VATPAGESPRINT"].ToString(), out intVatPagesPrint);
                        //        CreateInputVoucher();
                        //        CreateOutputVoucher();
                        //        List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice> lstVAT_Invoice = new List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice>();
                        //        if (objVatInvoiceInputVoucher != null)
                        //            lstVAT_Invoice.Add(objVatInvoiceInputVoucher);
                        //        if (objVatInvoiceOutputVoucher != null)
                        //            lstVAT_Invoice.Add(objVatInvoiceOutputVoucher);
                        //        if (lstVAT_Invoice.Count > 0)
                        //        {
                        //            PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                        //            if (lstVAT_Invoice.Count == 1)
                        //            {
                        //                objPrintVAT_PrepareData_Viettel.PrintVATInvoice(lstVAT_Invoice[0], false, false, "", intVatPagesPrint);
                        //            }
                        //            else
                        //            {
                        //                string strVatPagesPrint = string.Concat(Enumerable.Repeat(intVatPagesPrint.ToString() + ",", lstVAT_Invoice.Count));
                        //                strVatPagesPrint = strVatPagesPrint.Substring(0, strVatPagesPrint.Length - 1);
                        //                objPrintVAT_PrepareData_Viettel.PrintVATInvoiceLIST(lstVAT_Invoice, false, false, "", strVatPagesPrint);
                        //            }
                        //        }
                        //    }
                        //}
                        #endregion


                    }
                    else
                    {

                        if ((objVoucher.TotalLiquidate == 0 && objInputChangeOrderType.ReturnFeeBook == 2) || bolIsCreateVoucherReturnFee)
                            objVoucher = null;
                        string[] arrVATInvoiceList = null;
                        bolResult = objPLCInputChangeOrder.CreateInputVoucherAndOutVoucherViettel(objInputChangeOrder, objInputVoucher, objVoucher, objVoucherForReturnFee, objVoucherForSaleOrder, ref strInputVoucherReturnId, ref arrVATInvoiceList);
                        if (!bolResult)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            btnCreateConcern.Enabled = true;
                            return;
                        }


                        #region Đăng bổ sung 07/03/2018
                        if (arrVATInvoiceList != null)
                        {
                            ////le van long/Tích điểm đổi trả/03/06/2020
                            string strInvoiceID = "";
                            string strInvoiceIDNew = "";
                            int index = 0;
                            foreach (string strVATInvoiceID in arrVATInvoiceList)
                            {
                                if (index==0)
                                {
                                    strInvoiceIDNew = strVATInvoiceID;
                                }
                                else
                                {
                                    strInvoiceID = strVATInvoiceID;
                                }
                                index=index + 1;
                            }

                            if (!string.IsNullOrEmpty(strInvoiceID))
                            {
                                ERP.SalesAndServices.SaleOrders.DUI.VtPlusUrl.getAllUrlApi();
                                int tongdiem = vtPlus.Getpoint(txtCustomerPhone.Text.Trim(), "Lấy tổng điểm : chức năng đổi trả");
                                string CheckVTplus = vtPlus.CallApi_checkExitVtAccount(txtCustomerPhone.Text.Trim(), "Kiểm tra khách hàng tồn tại trong hệ thống - Chức năng: nhập đổi trả");
                                string messageVtplus = VtplusPointAdd(strInputChangeOrderID, strInvoiceID, strInvoiceIDNew, CheckVTplus, tongdiem);
                                //messageVtplus = messageVtplus + VtplusReturnPoint(strInputChangeOrderID, CheckVTplus);
                                messageVtplus = messageVtplus + VtplusRollbackAdjustAccountPoint(strInputChangeOrderID, strInvoiceID, CheckVTplus);
                                if (!string.IsNullOrEmpty(messageVtplus))
                                {
                                    MessageBox.Show(messageVtplus
                                        , "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                }
                            }

                            foreach (string strVATInvoiceID in arrVATInvoiceList)
                            {
                                if (!string.IsNullOrWhiteSpace(strVATInvoiceID))
                                {
                                    ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
                                    if (!string.IsNullOrEmpty(strvtplusPointTs))
                                    {
                                        if (strVATInvoiceID == strInvoiceID)
                                        {
                                            objPLCPrintVAT.CreateVATSInvoiceByVATInvoiceID(strVATInvoiceID, strvtplusPointTs);
                                        }
                                        else
                                        {
                                            objPLCPrintVAT.CreateVATSInvoiceByVATInvoiceID(strVATInvoiceID);
                                        }
                                    }
                                    else
                                    {
                                        objPLCPrintVAT.CreateVATSInvoiceByVATInvoiceID(strVATInvoiceID);
                                    }
                                }
                            }

                            List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice> lstVAT_Invoice = new List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice>();
                            foreach (string strVATInvoiceID in arrVATInvoiceList)
                            {
                                PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVAT_InvoiceMain = null;
                                new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().LoadInfo(ref objVAT_InvoiceMain, strVATInvoiceID);
                                lstVAT_Invoice.Add(objVAT_InvoiceMain);
                            }
                            if (lstVAT_Invoice != null && lstVAT_Invoice.Count() > 0)
                            {
                                ERP.PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                                if (lstVAT_Invoice.Count() == 1)
                                {
                                    int VatPagesPrint = objInputChangeOrderType.VatPagesPrint == 0 ? 3 : objInputChangeOrderType.VatPagesPrint;
                                    objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, lstVAT_Invoice[0], false, false, "", VatPagesPrint);
                                }
                                else
                                {
                                    int VatPagesPrint = objInputChangeOrderType.VatPagesPrint == 0 ? 3 : objInputChangeOrderType.VatPagesPrint;
                                    string strVatPagesPrint = string.Concat(Enumerable.Repeat(VatPagesPrint.ToString() + ",", lstVAT_Invoice.Count));
                                    strVatPagesPrint = strVatPagesPrint.Substring(0, strVatPagesPrint.Length - 1);
                                    objPrintVAT_PrepareData_Viettel.PrintVATInvoiceLIST(this, lstVAT_Invoice, false, false, "", strVatPagesPrint);
                                }
                            }
                        }
                        #endregion
                        #region Đăng rào code 07/03/2018
                        //ERP.PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                        //objPrintVAT_PrepareData_Viettel.CreateVATInvoiceInputChangeOrder(this, 1, objInputChangeOrder.InputChangeOrderID, intInputChangeOrderTypeID);
                        #endregion
                        #region Nhập đổi(bỏ code)
                        //DataTable dtbData = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_CHANGEORDER_AUTO_SEL", new object[] { "@INPUTCHANGEORDERID", objInputChangeOrder.InputChangeOrderID });
                        //DataTable dtbDataDetail = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_INPUTRETURN_SEL", new object[] { "@INPUTVOUCHERRETURNID", strInputVoucherReturnId });
                        //if (dtbData != null)
                        //{
                        //    int intIsAutoCreateInvoice = 0;
                        //    int.TryParse(dtbData.Rows[0][0].ToString(), out intIsAutoCreateInvoice);
                        //    int intInvoiceNo = 0;
                        //    int.TryParse(objOutputVoucherLoad.InvoiceID, out intInvoiceNo);
                        //    DataTable dtbInvoiceId = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_INVOICE_SELBYOUTPUTID", new object[] { "@OUTPUTVOUCHERID", dtbData.Rows[0][3].ToString() });
                        //    if (intIsAutoCreateInvoice == 1 && intInvoiceNo > 0 && dtbInvoiceId != null && dtbInvoiceId.Rows.Count > 0)
                        //    {
                        //        //strInputVoucherId = dtbDataDetail.Rows[0][1].ToString();
                        //        //strOldOutputVoucherID = dtbDataDetail.Rows[0][0].ToString();
                        //        //CreateInputVoucher();
                        //        //CreateOutputVoucher();

                        //        strInputVoucherId = dtbData.Rows[0][1].ToString();
                        //        strOutputVoucherID = dtbData.Rows[0][2].ToString();
                        //        strOldOutputVoucherID = dtbData.Rows[0][3].ToString();
                        //        strInputVoucherReturnId = dtbData.Rows[0][4].ToString();
                        //        int.TryParse(dtbData.Rows[0]["VATPAGESPRINT"].ToString(), out intVatPagesPrint);
                        //        CreateInputVoucher();
                        //        CreateOutputVoucher();
                        //        List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice> lstVAT_Invoice = new List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice>();
                        //        if (objVatInvoiceInputVoucher != null)
                        //            lstVAT_Invoice.Add(objVatInvoiceInputVoucher);
                        //        if (objVatInvoiceOutputVoucher != null)
                        //            lstVAT_Invoice.Add(objVatInvoiceOutputVoucher);
                        //        if (lstVAT_Invoice.Count > 0)
                        //        {
                        //            PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                        //            if (lstVAT_Invoice.Count == 1)
                        //            {
                        //                objPrintVAT_PrepareData_Viettel.PrintVATInvoice(lstVAT_Invoice[0], false, false, "", intVatPagesPrint);
                        //            }
                        //            else
                        //            {
                        //                string strVatPagesPrint = string.Concat(Enumerable.Repeat(intVatPagesPrint.ToString() + ",", lstVAT_Invoice.Count));
                        //                strVatPagesPrint = strVatPagesPrint.Substring(0, strVatPagesPrint.Length - 1);
                        //                objPrintVAT_PrepareData_Viettel.PrintVATInvoiceLIST(lstVAT_Invoice, false, false, "", strVatPagesPrint);
                        //            }
                        //        }
                        //    }
                        //}
                        #endregion
                    }
                    #endregion
                    #region NDC -- subsidy
                    //-- NDC: nếu là đổi hàng subsidy và đã tạo thành công đơn hàng:
                    if (bolResult && (isSubsidy || !string.IsNullOrEmpty(stringPackageCode)))
                    {
                        // cập nhật IMEI mới và trạng thái chờ-cập-nhật IMEI sang BCCS (mặc định status này = 8)
                        if (!string.IsNullOrEmpty(stringPackageCode))
                        {
                            objVERIFIEDPARTNER.PAKAgeCode = stringPackageCode;
                            objVERIFIEDPARTNER.IMEI = newIMEI;
                        }
                        vttPLC.UpdateNewEMEI(objVERIFIEDPARTNER, objVERIFIEDPARTNER.VERIFIEDPARTNERID, oldIMEI, objSaleOrder_Out == null ? string.Empty : objSaleOrder_Out.SaleOrderID, objInputChangeOrderType.IsGetPromotion);
                    }
                    if (bolResult && objVERIFIEDPARTNER != null && string.IsNullOrEmpty(stringPackageCode) && !isSubsidy)
                    {
                        objVERIFIEDPARTNER.Status = 10;
                        vttPLC.Update(objVERIFIEDPARTNER);
                    }
                    //-- NDC ----------------------------
                    #endregion
                }
                //  ---------
                else
                {

                    if ((objVoucher.TotalLiquidate == 0 && objInputChangeOrderType.ReturnFeeBook == 2) || bolIsCreateVoucherReturnFee)
                        objVoucher = null;
                    else
                    {
                        if ((objVoucher.TotalLiquidate == 0 && objInputChangeOrderType.ReturnFeeBook == 1) || bolIsCreateVoucherReturnFee)
                        {
                            objVoucher.TotalMoney = objVoucher.Debt;
                            objVoucher.TotalLiquidate = objVoucher.Debt;
                        }
                    }
                    string[] arrVATInvoiceList = null;
                    bolResult = objPLCInputChangeOrder.CreateInputVoucherAndOutVoucherViettel(objInputChangeOrder, objInputVoucher, objVoucher, objVoucherForReturnFee, null, ref strInputVoucherReturnId, ref arrVATInvoiceList);
                    if (!bolResult)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        btnCreateConcern.Enabled = true;
                        return;
                    }

                    PLCVTT_VERIFIEDPARTNER vttPLC = new PLCVTT_VERIFIEDPARTNER();
                    //-- NDC: có phải là đổi hàng subsidy
                    VTT_VERIFIEDPARTNER objVERIFIEDPARTNER = null;
                    string oldIMEI = "";
                    foreach (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail detail in objInputChangeOrder.InputChangeOrderDetailList)
                    {
                        oldIMEI = detail.IMEI_In;
                        if (!string.IsNullOrEmpty(oldIMEI))
                        {
                            // check nếu có chương trình subsidy cho IMEI này
                            objVERIFIEDPARTNER = vttPLC.GetByIMEI(oldIMEI);
                            if (objVERIFIEDPARTNER != null)
                            {
                                break;
                            }
                        }

                    }
                    if (objVERIFIEDPARTNER != null)
                    {
                        objVERIFIEDPARTNER.Status = 10;
                        new ERP.SaleProgram.PLC.PLCVTT_VERIFIEDPARTNER().Update(objVERIFIEDPARTNER);
                    }


                    //if (objVERIFIEDPARTNER != null)
                    //{

                    //    // có chương trình subsidy:
                    //    if (string.IsNullOrEmpty(newIMEI))
                    //    {
                    //        MessageBox.Show("[Subsidy] không có IMEI sản phẩm đổi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //        return;
                    //    }
                    //    objVERIFIEDPARTNER.IMEI = newIMEI;
                    //    string oldPartnerCode = objVERIFIEDPARTNER.PAKAgeCode.Trim();
                    //    decimal oldPrice = -1;
                    //    decimal newPrice = -2;
                    //    string oldProductID = subsidyDetail.ProductID_In;
                    //    string newProductID = "";
                    //    var r = dtbOVDetailReturn.Select("IMEI = " + oldIMEI)[0];
                    //    if (objSaleOrder_Out != null)
                    //    {
                    //        newProductID = objSaleOrder_Out.SaleOrderDetailList[0].ProductID;
                    //        newPrice = objSaleOrder_Out.TotalLiquidate;
                    //    }
                    //    else
                    //    {
                    //        newProductID = subsidyDetail.ProductID_Out;
                    //        decimal.TryParse(r["SALEPRICE_OUT"].ToString(), out newPrice);
                    //    }
                    //    decimal.TryParse(r["SALEPRICE"].ToString(), out oldPrice);
                    //    oldPrice = Math.Round(oldPrice, 0);
                    //    newPrice = Math.Round(newPrice, 0);

                    //    //if (oldPrice != newPrice)
                    //    //{
                    //    //    MessageBox.Show("[Subsidy] Chênh lệch giá giữa 2 sản phẩm.Vui lòng kiểm tra lại!");
                    //    //    return;
                    //    //}
                    //    int checkResult = vttPLC.CheckForExchangeSubsidy(oldProductID, newProductID, oldPartnerCode);
                    //    switch (checkResult)
                    //    {
                    //        case 1:
                    //            isSubsidy = true;
                    //            break;
                    //        case 2:
                    //            MessageBox.Show("[Subsidy] Imei xuất và Imei nhập không cùng Model.Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //            return;
                    //        case 3:
                    //            MessageBox.Show("[Subsidy] Mã KM đối tác của Imei xuất không giống Imei nhập.Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //            return;
                    //        default:
                    //            MessageBox.Show("[Subsidy] Có lỗi trong quá trình kiểm tra Imei mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //            return;

                    //    }
                    //}

                    #region Đăng bổ sung 07/03/2018
                    if (arrVATInvoiceList != null && arrVATInvoiceList.Count() > 0)
                    {
                        foreach (string strVATInvoiceID in arrVATInvoiceList)
                        {
                            if (!string.IsNullOrWhiteSpace(strVATInvoiceID))
                            {
                                //LE VAN LONG//31-03-2020 hủy điểm nhập trả hàng
                                ERP.SalesAndServices.SaleOrders.DUI.VtPlusUrl.getAllUrlApi();
                                int tongdiem = vtPlus.Getpoint(txtCustomerPhone.Text.Trim(), "Lấy tổng điểm : chức năng nhập trả");
                                string CheckVTplus = vtPlus.CallApi_checkExitVtAccount(txtCustomerPhone.Text.Trim(), "Kiểm tra khách hàng tồn tại trong hệ thống - Chức năng: nhập trả");
                                string messageVtplus = VtplusReturnPoint(strInputChangeOrderID, CheckVTplus, tongdiem, strVATInvoiceID);
                                messageVtplus = messageVtplus + VtplusCancelPoint(strInputChangeOrderID, CheckVTplus, tongdiem, strVATInvoiceID);
                                if (!string.IsNullOrEmpty(messageVtplus))
                                {
                                    MessageBox.Show(messageVtplus
                                        , "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                }

                                ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
                                objPLCPrintVAT.CreateVATSInvoiceByVATInvoiceID(strVATInvoiceID, "");
                            }
                        }

                        List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice> lstVAT_Invoice = new List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice>();
                        foreach (string strVATInvoiceID in arrVATInvoiceList)
                        {
                            PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVAT_InvoiceMain = null;
                            new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().LoadInfo(ref objVAT_InvoiceMain, strVATInvoiceID);
                            lstVAT_Invoice.Add(objVAT_InvoiceMain);
                        }
                        if (lstVAT_Invoice != null && lstVAT_Invoice.Count() > 0)
                        {
                            ERP.PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                            if (lstVAT_Invoice.Count() == 1)
                            {
                                int VatPagesPrint = objInputChangeOrderType.VatPagesPrint == 0 ? 3 : objInputChangeOrderType.VatPagesPrint;
                                objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, lstVAT_Invoice[0], false, false, "", VatPagesPrint);
                            }
                            else
                            {
                                int VatPagesPrint = objInputChangeOrderType.VatPagesPrint == 0 ? 3 : objInputChangeOrderType.VatPagesPrint;
                                string strVatPagesPrint = string.Concat(Enumerable.Repeat(VatPagesPrint.ToString() + ",", lstVAT_Invoice.Count));
                                strVatPagesPrint = strVatPagesPrint.Substring(0, strVatPagesPrint.Length - 1);
                                objPrintVAT_PrepareData_Viettel.PrintVATInvoiceLIST(this, lstVAT_Invoice, false, false, "", strVatPagesPrint);
                            }
                        }
                    }
                    #endregion
                    #region Đăng rào code 07/03/2018
                    //ERP.PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                    //objPrintVAT_PrepareData_Viettel.CreateVATInvoiceInputChangeOrder(this, 3, objInputChangeOrder.InputChangeOrderID, intInputChangeOrderTypeID);
                    #endregion
                    #region Trả hàng(bỏ code)
                    //DataTable dtbData = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_CHANGEORDER_AUTO_SEL", new object[] { "@INPUTCHANGEORDERID", objInputChangeOrder.InputChangeOrderID });
                    //DataTable dtbDataDetail = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_INPUTRETURN_SEL", new object[] { "@INPUTVOUCHERRETURNID", strInputVoucherReturnId });
                    //if (bolResult)
                    //{
                    //    if (dtbData != null)
                    //    {
                    //        int intIsAutoCreateInvoice = 0;
                    //        int.TryParse(dtbData.Rows[0][0].ToString(), out intIsAutoCreateInvoice);
                    //        int intInvoiceNo = 0;
                    //        int.TryParse(objOutputVoucherLoad.InvoiceID, out intInvoiceNo);
                    //        DataTable dtbInvoiceId = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_INVOICE_SELBYOUTPUTID", new object[] { "@OUTPUTVOUCHERID", dtbData.Rows[0][3].ToString()});
                    //        if (intIsAutoCreateInvoice == 1 && intInvoiceNo > 0 && dtbInvoiceId != null && dtbInvoiceId.Rows.Count > 0)
                    //        {
                    //            strInputVoucherId = dtbDataDetail.Rows[0][1].ToString();
                    //            strOutputVoucherID = dtbDataDetail.Rows[0][0].ToString();
                    //            strOldOutputVoucherID = dtbDataDetail.Rows[0][0].ToString();
                    //            int.TryParse(dtbData.Rows[0]["VATPAGESPRINT"].ToString(), out intVatPagesPrint);
                    //            CreateInputVoucher();
                    //            List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice> lstVAT_Invoice = new List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice>();
                    //            if (objVatInvoiceInputVoucher != null)
                    //                lstVAT_Invoice.Add(objVatInvoiceInputVoucher);
                    //            if (objVatInvoiceOutputVoucher != null)
                    //                lstVAT_Invoice.Add(objVatInvoiceOutputVoucher);
                    //            if (lstVAT_Invoice.Count > 0)
                    //            {
                    //                PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                    //                if (lstVAT_Invoice.Count == 1)
                    //                {
                    //                    objPrintVAT_PrepareData_Viettel.PrintVATInvoice(lstVAT_Invoice[0], false, false, "", intVatPagesPrint);
                    //                }
                    //                else
                    //                {
                    //                    string strVatPagesPrint = string.Concat(Enumerable.Repeat(intVatPagesPrint.ToString() + ",", lstVAT_Invoice.Count));
                    //                    strVatPagesPrint = strVatPagesPrint.Substring(0, strVatPagesPrint.Length - 1);
                    //                    objPrintVAT_PrepareData_Viettel.PrintVATInvoiceLIST(lstVAT_Invoice, false, false, "", strVatPagesPrint);
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion        
                }

                if (!bolResult)
                {
                    if (string.IsNullOrEmpty(Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message) &&
                        string.IsNullOrEmpty(Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail))
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi tạo phiếu nhập/trả", "Không thể tạo phiếu nhập đổi/trả hàng!");
                    else
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnCreateConcern.Enabled = true;
                    return;
                }

                if (bolResult)
                {
                    if (objVoucherDepositSpend != null)
                    {
                        // objVoucherDepositSpend.VoucherConcern = objOutputVoucherLoad.OrderID;
                        // objVoucherDepositSpend.OrderID = objOutputVoucherLoad.OrderID;
                        objVoucherDepositSpend.VoucherKind = 3;
                        objVoucherDepositSpend.InputChangeOrderId = objInputChangeOrder.InputChangeOrderID;
                        objResultMessageVoucher = objPLCVoucher.CreateVoucher(objVoucherDepositSpend);
                        if (objResultMessageVoucher.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessageVoucher.Message, objResultMessageVoucher.MessageDetail);
                            return;
                        }
                        DUIPayment_Common.PrintVoucherForDeposit(this, objVoucherDepositSpend.VoucherID, objVoucherDepositSpend.CurrencyExchange, objVoucherDepositSpend.IsSpend);
                        MessageBox.Show("Thêm phiếu chi trả tiền ký quỹ khuyến mãi thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    if (objVoucherDeposit != null)
                    {
                        objVoucherDeposit.VoucherConcern = objSaleOrder_Out.SaleOrderID;
                        objVoucherDeposit.OrderID = objSaleOrder_Out.SaleOrderID;
                        objVoucherDeposit.VoucherKind = 3;
                        objResultMessageVoucher = objPLCVoucher.CreateVoucher(objVoucherDeposit);
                        if (objResultMessageVoucher.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessageVoucher.Message, objResultMessageVoucher.MessageDetail);
                            return;
                        }
                        DUIPayment_Common.PrintVoucherForDeposit(this, objVoucherDeposit.VoucherID, objVoucherDeposit.CurrencyExchange, objVoucherDeposit.IsSpend);
                        MessageBox.Show("Thêm phiếu thu ký quỹ khuyến mãi thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }


                MessageBox.Show(this, "Xử lý yêu cầu " + strMessageForm + " thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                bolIsHasAction = true;


                DataTable dtbChangeOrder = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_CHANGEORDER_OUT_SELINVOICE", new object[] { "@InputChangeOrderId", objInputChangeOrder.InputChangeOrderID });
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->btnCreateConcern_Click");
                }
                txtInvoiceIdOut.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["INVOICEIDOUTPUT"].ToString() : string.Empty;
                txtInvoiceSymbolOutput.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["INVOICESYMBOLOUTPUT"].ToString() : string.Empty;
                txtDenominatorOutput.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["DENOMINATOROUTPUT"].ToString() : string.Empty;

                dtbChangeOrder = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_CHANGEORDER_SELINVOICE", new object[] { "@InputChangeOrderId", objInputChangeOrder.InputChangeOrderID });
                // SystemConfig.objSessionUser.
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->btnCreateConcern_Click");
                }
                txtInvoiceIdReturn.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["INVOICEIDINPUT"].ToString() : string.Empty;
                txtInvoiceSymbolReturn.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["INVOICESYMBOLINPUT"].ToString() : string.Empty;
                txtDenominatorReturn.Text = dtbChangeOrder.Rows.Count > 0 ? dtbChangeOrder.Rows[0]["DENOMINATORINPUT"].ToString() : string.Empty;

                //this.Close();
                PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder InputChangeOrderTMP = objPLCInputChangeOrder.LoadInfo(strInputChangeOrderID);
                chkIsInputchange.Checked = true;
                btnCreateConcern.Enabled = false;
                lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " đã được xử lý";
                txtContent.BackColor = System.Drawing.SystemColors.Info;
                txtContent.ReadOnly = true;
                txtReasonNote.ReadOnly = true;
                txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                cboReason.Enabled = false;
                //txtReason.Visible = true;
                //if (cboReason.ColumnID > 0)
                //    txtReason.Text = cboReason.ColumnName;
                //else
                //    txtReason.Text = string.Empty;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                btnDownload.Enabled = false;

                DevExpress.XtraBars.BarButtonItem ItemReportInput = new DevExpress.XtraBars.BarButtonItem();
                ItemReportInput.Caption = "Phiếu nhập trả";
                ItemReportInput.Name = "ItemReport" + InputChangeOrderTMP.NewInputVoucherID;
                ItemReportInput.Tag = InputChangeOrderTMP.NewInputVoucherID;
                ItemReportInput.Enabled = true;
                ItemReportInput.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ItemReportInput_ItemClick);
                pMnuPrint.AddItem(ItemReportInput);
                txtNewInputVoucherID.Text = InputChangeOrderTMP.NewInputVoucherID;
                dtpInputchangeDate.EditValue = InputChangeOrderTMP.InputChangeDate;
                if (!string.IsNullOrEmpty(txtSaleOrderID.Text))
                {
                    if (objSaleOrder_Out != null && !string.IsNullOrEmpty(objSaleOrder_Out.SaleOrderID))
                    {
                        ERP.MasterData.PLC.MD.WSSaleOrderType.SaleOrderType objSaleOrderType = objPLCSaleOrderType.LoadInfoFromCache(objSaleOrder_Out.SaleOrderTypeID);
                        if (objSaleOrderType != null && objSaleOrderType.DtbSaleOrderType_RP_Report != null && objSaleOrderType.DtbSaleOrderType_RP_Report.Rows.Count > 0)
                        {
                            bool bolIsOutProduct = true;
                            bool bolIsInCome = true;
                            bool bolIsReview = true;
                            String strExp = "ISPRINTVAT = 0 ";
                            if (!bolIsReview)
                                strExp += "AND PRINTREPORTTYPE <> 2";
                            if (!bolIsInCome)
                                strExp += "AND PRINTREPORTTYPE <> 5";
                            if (!bolIsOutProduct)
                                strExp += "AND PRINTREPORTTYPE <> 3";
                            DataTable tblReportList = objSaleOrderType.DtbSaleOrderType_RP_Report;
                            DataTable tblReport = Globals.SelectDistinct(tblReportList, "ReportID", strExp);
                            foreach (DataRow objRow in tblReport.Rows)
                            {
                                ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(Convert.ToInt32(objRow["ReportID"]));
                                DevExpress.XtraBars.BarButtonItem ItemReportSaleOrder = new DevExpress.XtraBars.BarButtonItem();
                                ItemReportSaleOrder.Caption = Convert.ToString(objRow["ReportName"]).Trim();
                                ItemReportSaleOrder.Name = txtSaleOrderID.Text;
                                ItemReportSaleOrder.Tag = objRow;
                                ItemReportSaleOrder.Enabled = true;
                                ItemReportSaleOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ItemReportSaleOrder_ItemClick);
                                pMnuPrint.AddItem(ItemReportSaleOrder);
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(InputChangeOrderTMP.NewOutputVoucherID))
                    {
                        DevExpress.XtraBars.BarButtonItem ItemReportOutput = new DevExpress.XtraBars.BarButtonItem();
                        ItemReportOutput.Caption = "Phiếu xuất đổi";
                        ItemReportOutput.Name = "ItemReport" + InputChangeOrderTMP.NewOutputVoucherID;
                        ItemReportOutput.Tag = InputChangeOrderTMP.NewOutputVoucherID;
                        ItemReportOutput.Enabled = true;
                        ItemReportOutput.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ItemReportOutput_ItemClick);
                        pMnuPrint.AddItem(ItemReportOutput);
                        txtNewOutputVoucherID.Text = InputChangeOrderTMP.NewOutputVoucherID;
                    }
                }
                strInvoiceId = txtInVoiceID.Text.Trim();
                bolIsReload = true;
                bolSpecialCase = false;
                frmInputChangeOrder_Load(null, null);
                btnPrint.Visible = true;
                btnCreateConcern.Visible = false;
                btnSupportCare.Visible = false;
                tabControl.SelectedTab = tabPageInputChangeOrder;
                CheckAfterEditFunction();
                bolIsCreateVoucherReturnFee = false;
            }
            catch (Exception objEx)
            {
                btnCreateConcern.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi xử lý yêu cầu " + strMessageForm, objEx.Message + System.Environment.NewLine + objEx.StackTrace);
            }
        }

        private void ItemReportOutput_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e == null || e.Item == null)
                return;
            DevExpress.XtraBars.BarButtonItem barItem = e.Item as DevExpress.XtraBars.BarButtonItem;
            if (barItem.Tag == null)
                return;
            string strNewOutputVoucherID = barItem.Tag.ToString();
            Hashtable hstbReportParam = new Hashtable();
            bool bolIsViewCostPrice = SystemConfig.objSessionUser.IsPermission("OUTPUTREPORT_VIEWCOSTPRICE");
            hstbReportParam.Add("OutputVoucherID", strNewOutputVoucherID);
            hstbReportParam.Add("IsCostPriceHide", !bolIsViewCostPrice);
            hstbReportParam.Add("IsSalePriceHide", false);
            MasterData.PLC.MD.PLCReport objPLCReport = new MasterData.PLC.MD.PLCReport();
            ERP.MasterData.PLC.MD.WSReport.Report objReport = new MasterData.PLC.MD.WSReport.Report();
            objPLCReport.LoadInfo(ref objReport, intReportID);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, hstbReportParam);
        }

        private void ItemReportInput_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e == null || e.Item == null)
                return;
            DevExpress.XtraBars.BarButtonItem barItem = e.Item as DevExpress.XtraBars.BarButtonItem;
            if (barItem.Tag == null)
                return;
            string strNewInputVoucherID = barItem.Tag.ToString();
            Reports.Input.frmReportView frm = new Reports.Input.frmReportView();
            frm.InputVoucherID = strNewInputVoucherID;
            frm.bolIsPriceHide = false;
            frm.ShowDialog();
        }

        private void ItemReportSaleOrder_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e == null || e.Item == null)
                return;
            DevExpress.XtraBars.BarButtonItem barItem = e.Item as DevExpress.XtraBars.BarButtonItem;
            if (barItem.Tag == null)
                return;
            DataRow objReportInfo = barItem.Tag as DataRow;
            int intReportID = Convert.ToInt32(objReportInfo["REPORTID"]);
            String strPrinterTypeID = Convert.ToString(objReportInfo["PRINTERTYPEID"]).Trim();
            String strSaleOrderID = barItem.Name.ToString();
            SalesAndServices.SaleOrders.DUI.DUISaleOrders_Common.ShowReport(this, strSaleOrderID, intReportID, strPrinterTypeID);
        }

        private void ShowPaymentForm1(bool bolIsSpend, ref bool bolIsOKPayment, string strTextForm, decimal VNDCash, decimal PaymentCardAmount, int PaymentCardID, decimal PaymentCardSpend, string PaymentCardVoucherID, decimal ReturnAmount,
                                        ref decimal refVNDCash, ref decimal refPaymentCardAmount, ref int refPaymentCardID, ref string refPaymentCardName, ref decimal refPaymentCardSpend, ref string refPaymentCardVoucherID, ref List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail> lstVoucherDetail,
                                        string storename = "", string usercreate = "", string contact = "")
        {
            POPUP:
            ERP.SalesAndServices.SaleOrders.DUI.frmFullPaymentForm frmFullPaymentForm1 = new ERP.SalesAndServices.SaleOrders.DUI.frmFullPaymentForm();
            #region them hinh thuc thanh toan vnpay
            frmFullPaymentForm1.TotalLiquidate = ReturnAmount;
            frmFullPaymentForm1.StoreName = storename;
            frmFullPaymentForm1.CreateUser = usercreate;
            frmFullPaymentForm1.CustomerID = contact;
            frmFullPaymentForm1.StoreID = cboStore.StoreID;
            #endregion
            //Mặc định gán số tiền chi/thu 
            frmFullPaymentForm1.VNDCash = ReturnAmount; //VNDCash;
            frmFullPaymentForm1.ForeignCash = 0;
            frmFullPaymentForm1.CurrencyUnitID = ERP.MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyUnitDefaultID();
            frmFullPaymentForm1.ForeignCashExchange = 0;
            frmFullPaymentForm1.PaymentCardAmount = PaymentCardAmount;
            frmFullPaymentForm1.PaymentCardID = PaymentCardID;
            frmFullPaymentForm1.PaymentCardSpend = PaymentCardSpend;
            frmFullPaymentForm1.PaymentCardVoucherID = PaymentCardVoucherID;
            if (lblTitlePayment.Text == "Phải thu" && strTextForm == "Thu tiền thanh toán đơn hàng")
                frmFullPaymentForm1.RefundAmount = decRefundAmount_In;
            else
                frmFullPaymentForm1.RefundAmount = 0;
            frmFullPaymentForm1.IsAllowDebt = bolIsSpend;
            frmFullPaymentForm1.IsApplyGiftVoucher = true;
            frmFullPaymentForm1.TotalAmountOfOrder = ReturnAmount;
            frmFullPaymentForm1.GiftVoucherTable = null;
            frmFullPaymentForm1.GiftVoucherAmount = 0;
            frmFullPaymentForm1.SaleOrderDetailList = new List<SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrderDetail>();
            frmFullPaymentForm1.OutputStoreID = cboStore.StoreID;
            frmFullPaymentForm1.UneventPriceGiftVoucher = 0;
            frmFullPaymentForm1.OutputTypePromotionID = 0;
            frmFullPaymentForm1.OutputTypeSalePromotionID = 0;
            if (strTextForm != string.Empty)
                frmFullPaymentForm1.Text = strTextForm;
            frmFullPaymentForm1.IsAllowMulti = true;
            frmFullPaymentForm1.ShowDialog(this);
            if (frmFullPaymentForm1.IsOK)
            {
                if (lblTitlePayment.Text == "Phải chi" && strTextForm != "Thu tiền thanh toán đơn hàng" && frmFullPaymentForm1.RefundAmount > 0)
                {
                    MessageBox.Show(this, "Vui lòng chi đủ số tiền", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VNDCash = frmFullPaymentForm1.VNDCash;
                    PaymentCardAmount = frmFullPaymentForm1.PaymentCardAmount;
                    PaymentCardID = frmFullPaymentForm1.PaymentCardID;
                    PaymentCardSpend = frmFullPaymentForm1.PaymentCardSpend;
                    PaymentCardVoucherID = frmFullPaymentForm1.PaymentCardVoucherID;
                    goto POPUP;
                }
                lstVoucherDetail = frmFullPaymentForm1.LstAddVoucherDetail;
                refVNDCash = frmFullPaymentForm1.VNDCash;
                refPaymentCardAmount = frmFullPaymentForm1.PaymentCardAmount;
                refPaymentCardID = frmFullPaymentForm1.PaymentCardID;
                refPaymentCardName = frmFullPaymentForm1.PaymentCardName;
                refPaymentCardSpend = frmFullPaymentForm1.PaymentCardSpend;
                refPaymentCardVoucherID = frmFullPaymentForm1.PaymentCardVoucherID;
                bolIsOKPayment = frmFullPaymentForm1.IsOK;
                if (lblTitlePayment.Text == "Phải thu" && strTextForm == "Thu tiền thanh toán đơn hàng")
                    decRefundAmount_In = frmFullPaymentForm1.RefundAmount;
            }
        }

        private void flexDetail_CellButtonClick(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexDetail.Cols["ProductID_Out"].Index)
            {
                if (cboStore.StoreID < 1)
                {
                    MessageBox.Show(this, "Bạn chưa chọn kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboStore.Focus();
                    return;
                }

                ERP.MasterData.DUI.MasterData_Globals.ProductSearchParentForm_Text = this.Text;
                ERP.MasterData.DUI.Search.frmProductSearch frm = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
                frm.IsMultiSelect = false;
                frm.ShowDialog();
                if (frm.Product != null)
                {
                    string strProduct = Convert.ToString(frm.Product.ProductID).Trim();
                    StringBuilder arrAutoGetIMEIList = new StringBuilder();
                    ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(flexDetail[e.Row, "OutputTypeID_Out"]));
                    ERP.MasterData.PLC.MD.WSProduct.Product objProductIn = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim());
                    if (objInputChangeOrderType.ProductChangeConditionType == 2 || objInputChangeOrderType.ProductChangeConditionType == 3)
                    {
                        if (objInputChangeOrderType.ProductChangeConditionType == 2 && Convert.ToString(objProductIn.ProductID).Trim() != strProduct)
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng mã sản phẩm. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        if (objInputChangeOrderType.ProductChangeConditionType == 3)
                        {
                            if (Convert.ToString(objProductIn.ModelID).Trim() != Convert.ToString(frm.Product.ModelID).Trim())
                            {
                                MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            else
                            {
                                if (Convert.ToString(objProductIn.ModelID).Trim() == string.Empty && Convert.ToString(objProductIn.ModelID).Trim() == Convert.ToString(frm.Product.ModelID).Trim())
                                {
                                    MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                        }
                    }

                    int intIsRequestIMEI_In = Convert.ToBoolean(flexDetail[e.Row, "IsRequestIMEI"]) ? 1 : 0;
                    int intIsRequestIMEI_Out = frm.Product.IsRequestIMEI ? 1 : 0;
                    if (intIsRequestIMEI_In != intIsRequestIMEI_Out)
                    {
                        MessageBox.Show(this, "Sản phẩm nhập " + (intIsRequestIMEI_In == 1 ? "có" : "không có") + " yêu cầu IMEI. Vui lòng chọn sản phẩm xuất " + (intIsRequestIMEI_In == 1 ? "có" : "không có") + " yêu cầu IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(strProduct, cboStore.StoreID, Convert.ToInt32(flexDetail[e.Row, "OutputTypeID_Out"]), arrAutoGetIMEIList.ToString(), Convert.ToBoolean(flexDetail[e.Row, "IsNew"]), true);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    if (!objProductInStock.IsInstock)
                    {
                        MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductName + " không tồn kho. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (objProductInStock.IMEI == string.Empty && (objProductInStock.Quantity - Convert.ToDecimal(flexDetail[e.Row, "Quantity"])) < objProductInStock.MinInStock)
                    {
                        MessageBox.Show(this, "Mặt hàng " + objProductInStock.ProductName + " không đủ số lượng tồn kho tối thiểu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    //Lấy giá bán
                    decimal decSalePrice = objProductInStock.Price;
                    switch (objOutputType_Out.GetPriceType)
                    {
                        case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.COSTPRICE:
                            decSalePrice = objProductInStock.CostPrice;
                            break;
                        case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO:
                            decSalePrice = 0;
                            break;
                        case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.FIRSTPRICE:
                            decSalePrice = objProductInStock.FirstPrice;
                            break;
                    }
                    if (decSalePrice == 0 && objOutputType_Out.GetPriceType != (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO)
                    {
                        MessageBox.Show(this, "Mặt hàng " + objProductInStock.ProductName + " chưa được thiết lập giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    string strIMEIInStock = string.Empty;
                    if (!string.IsNullOrEmpty(objProductInStock.IMEI))
                        strIMEIInStock = Convert.ToString(objProductInStock.IMEI).Trim();
                    if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                        decSalePrice = decSalePrice * (Convert.ToDecimal(1) + Convert.ToDecimal(objProductInStock.VAT) * Convert.ToDecimal(objProductInStock.VATPercent) * Convert.ToDecimal(0.0001));

                    if (objInputChangeOrderType.ProductChangeConditionType == 4
                        || objInputChangeOrderType.ProductChangeConditionType == 5
                        || objInputChangeOrderType.ProductChangeConditionType == 6)
                    {
                        if (objInputChangeOrderType.ProductChangeConditionType == 5 && Math.Round(decSalePrice) < Math.Round(Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"])))
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả với sản phẩm xuất có giá bán lớn hơn hoặc bằng giá nhập lại. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else if (objInputChangeOrderType.ProductChangeConditionType == 6 && Math.Round(decSalePrice) >= Math.Round(Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"])))
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả với sản phẩm xuất có giá bán nhỏ hơn giá nhập lại. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else if (objInputChangeOrderType.ProductChangeConditionType == 4 && Math.Round(decSalePrice) != Math.Round(Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"])))
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng giá(giá nhập lại phải bằng giá bán của sản phẩm xuất). Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    if (objInputChangeOrderType.ProductChangeConditionType == 7) //cùng giá cùng model
                    {
                        if (Convert.ToString(objProductIn.ModelID).Trim() != Convert.ToString(frm.Product.ModelID).Trim())
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else
                        {
                            if (Convert.ToString(objProductIn.ModelID).Trim() == string.Empty && Convert.ToString(objProductIn.ModelID).Trim() == Convert.ToString(frm.Product.ModelID).Trim())
                            {
                                MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                        if (Math.Round(decSalePrice) != Math.Round(Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"])))
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng giá(giá nhập lại phải bằng giá bán của sản phẩm xuất). Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    if (strIMEIInStock != string.Empty)
                    {
                        if (dtbOVDetailReturn.Select("TRIM(IMEI_Out) = '" + strIMEIInStock + "'").Length > 1)
                        {
                            MessageBox.Show(this, "Số IMEI " + strIMEIInStock + " đã tồn tại trên lưới dữ liệu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    //if (objInputChangeOrderType.ProductChangeConditionType == 3 || Convert.ToString(objProductIn.ModelID).Trim() == Convert.ToString(frm.Product.ModelID).Trim())
                    //{
                    //    decSalePrice = Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"]);
                    // // var a=  Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"]) / (1 + Convert.ToDecimal(flexDetail[e.Row, "VAT"]) * Convert.ToDecimal(flexDetail[e.Row, "VATPercent"]) / 10000);
                    //}
                    flexDetail[e.Row, "ProductID_Out"] = strProduct;
                    flexDetail[e.Row, "ProductName_Out"] = frm.Product.ProductName;
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIInStock;
                    flexDetail[e.Row, "InStockStatusID_Out"] = objProductInStock.InStockStatusID;
                    flexDetail[e.Row, "SalePrice_Out"] = decSalePrice;
                    if (objOutputType_Out.GetPriceType != (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO && (Convert.ToString(flexDetail[e.Row, "ProductID_Out"]).Trim() == Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim() && objProductInStock.IsPriceOfProduct) || objInputChangeOrderType.ProductChangeConditionType == 3 || Convert.ToString(objProductIn.ModelID).Trim() == Convert.ToString(frm.Product.ModelID).Trim())
                        flexDetail[e.Row, "SalePrice_Out"] = flexDetail[e.Row, "SalePriceVAT"];
                    flexDetail[e.Row, "IsRequestIMEI_Out"] = frm.Product.IsRequestIMEI;

                    CalTotalMoney();
                }
            }

            //Nguyen Van Tai bo sung theo yeu cau cua BA Trung- 30/11/2017
            if (e.Col == flexDetail.Cols["MACHINEERRORNAME"].Index)
            {
                Search.frmMachineErrorSearch frmMachineErrorSearch = new Search.frmMachineErrorSearch();
                frmMachineErrorSearch.Multiselect = true;
                //if (!Convert.IsDBNull(flexDetail.Rows[1]["MACHINEERRORGROUPID"]))
                //{
                //    int intMachineErrorGroupId = 0;
                //    int.TryParse(flexDetail.Rows[1]["MACHINEERRORGROUPID"].ToString(), out intMachineErrorGroupId);

                //    frmMachineErrorSearch.ListMachineErrorGroupId = intMachineErrorGroupId;
                //}
                if (frmMachineErrorSearch.ShowDialog() == DialogResult.OK)
                {
                    string strErrorContent = string.Empty;
                    string strErrorGroupContent = string.Empty;
                    string strErrorGroupId = string.Empty;
                    if (frmMachineErrorSearch.ListErrorGroup != null && frmMachineErrorSearch.ListErrorGroup.Count > 0)
                    {
                        foreach (var item in frmMachineErrorSearch.ListErrorGroup)
                        {
                            strErrorGroupContent += item.MACHINEERRORGROUPNAME + "; ";
                            strErrorGroupId += item.MACHINEERRORGROUPID + ";";
                        }
                        strErrorGroupContent = strErrorGroupContent.Substring(0, strErrorGroupContent.Length - 2);
                        if (frmMachineErrorSearch.lstMachineError != null && frmMachineErrorSearch.lstMachineError.Count > 0)
                        {
                            string strOldOutputVoucherDetailID = string.Empty;
                            if (string.IsNullOrEmpty(strInputChangeOrderID))
                                strOldOutputVoucherDetailID = Convert.ToString(flexDetail[e.Row, "OUTPUTVOUCHERDETAILID"]);
                            else
                                strOldOutputVoucherDetailID = Convert.ToString(flexDetail[e.Row, "OLDOUTPUTVOUCHERDETAILID"]);
                            string strProductIDIn = Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim();
                            //Xóa dữ liệu cũ 
                            if (dtbMachineErrorList.Rows.Count > 0)
                            {
                                DataRow[] drrMachineError = dtbMachineErrorList.Select("OLDOUTPUTVOUCHERDETAILID = '" + strOldOutputVoucherDetailID + "'");
                                if (!Convert.IsDBNull(drrMachineError) && drrMachineError.Count() > 0)
                                {
                                    foreach (var item in drrMachineError)
                                        dtbMachineErrorList.Rows.Remove(item);
                                }

                            }
                            foreach (var er in frmMachineErrorSearch.lstMachineError)
                            {
                                strErrorContent += er.MachineErrorName + "; ";
                                AddToMachineErrorList(string.Empty, strOldOutputVoucherDetailID, strProductIDIn, string.Empty, er.MachineErrorId);
                                //flexDetail[e.Row, "MACHINEERRORGROUPNAME"] = er.MachineErrorGroupName;
                                //flexDetail[e.Row, "MACHINEERRORID"] = er.MachineErrorId;
                                //flexDetail[e.Row, "MACHINEERRORGROUPID"] = er.MachineErrorGroupId;
                            }
                            strErrorContent = strErrorContent.Substring(0, strErrorContent.Length - 2);
                            flexDetail[e.Row, "MACHINEERRORNAME"] = strErrorContent;
                            flexDetail[e.Row, "MACHINEERRORGROUPNAME"] = strErrorGroupContent;
                            flexDetail[e.Row, "MACHINEERRORGROUPID"] = strErrorGroupId;
                        }
                    }

                }
            }

            //End
        }

        private decimal GetCompute_In(String strFilter)
        {
            decimal decResult = 0;
            decimal decSumInputPrice = 0;
            decimal decTotalFee = 0;
            DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select(strFilter);
            for (int i = 0; i < drDetail.Length; i++)
            {
                decTotalFee += Convert.ToDecimal(drDetail[i]["TotalFee"]);
                //if (objInputChangeOrderType.ReturnFeeBook == 2)
                //    decSumInputPrice += Convert.ToDecimal(drDetail[i]["InputPrice_In"]) * Convert.ToDecimal(drDetail[i]["Quantity"]) - Convert.ToDecimal(drDetail[i]["TotalFee"]);
                //else
                decSumInputPrice += Convert.ToDecimal(drDetail[i]["InputPrice_In"]) * Convert.ToDecimal(drDetail[i]["Quantity"]);
            }
            //if (objInputChangeOrderType.ReturnFeeBook == 2)
            //    decSumInputPrice = decSumInputPrice - decTotalFee;
            decResult = decSumInputPrice;
            if (objInputChangeOrderType.IsInputReturn && txtReturnAmount.Visible)
            {
                if (strInVoucherID != string.Empty)
                {
                    if (decTotalPaid == 0)
                    {
                        decResult = 0;
                        bolIsDebt = true;
                        bolIsNotCreateOutVoucherDetail = true;
                    }
                    else
                    {
                        if ((Math.Round(decSumInputPrice + decTotalFee) > Math.Round(decTotalPaid) /*&& objInputChangeOrderType.ReturnFeeBook != 2) || ((Math.Round(decSumInputPrice) == Math.Round(decTotalPaid) && objInputChangeOrderType.ReturnFeeBook == 2))*/))
                        {
                            decResult = decTotalPaid;
                            bolIsDebt = true;
                            DataTable dtbCheckDetailReturn1 = objPLCInputChangeOrder.GetListReturn(objOutputVoucherLoad.OutputVoucherID, objInputChangeOrder.InputChangeOrderID);
                            decimal decSumTotalAmount = Math.Abs(Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbCheckDetailReturn1.Compute("Sum(TotalAmount)", "Quantity < 0"), 0)));
                            decResult = decResult - decSumTotalAmount - decTotalFee;
                            if (decResult <= 0)
                            {
                                bolSpecialCase = true;
                                bolIsNotCreateOutVoucherDetail = true;
                                decResult = 0;
                            }
                        }
                        else
                        {

                            DataTable dtbCheckDetailReturn1 = objPLCInputChangeOrder.GetListReturn(objOutputVoucherLoad.OutputVoucherID, objInputChangeOrder.InputChangeOrderID);
                            decimal decSumTotalAmount = Math.Abs(Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbCheckDetailReturn1.Compute("Sum(TotalAmount)", "Quantity < 0"), 0)));
                            decimal decAmount = decTotalPaid - decSumTotalAmount;
                            if (Math.Round(decAmount) < Math.Round(decResult))
                                decResult = decAmount - decTotalFee;
                            if (decResult <= 0)
                            {
                                bolIsDebt = true;
                                bolSpecialCase = true;
                                bolIsNotCreateOutVoucherDetail = true;
                                decResult = 0;
                            }
                        }
                    }
                }
            }
            return Math.Round(decResult);
        }
        private decimal GetCompute_Out(String strFilter)
        {
            decimal decSumSalePrice = 0;
            DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select(strFilter);
            for (int i = 0; i < drDetail.Length; i++)
            {
                if (drDetail[i]["SalePrice_Out"] != DBNull.Value)
                    decSumSalePrice += Convert.ToDecimal(drDetail[i]["SalePrice_Out"]) * Convert.ToDecimal(drDetail[i]["Quantity"]);
            }
            if (objInputChangeOrder != null && !string.IsNullOrEmpty(objInputChangeOrder.SaleOrderID))
            {
                if (objSaleOrder_Out == null)
                    objSaleOrder_Out = objPLCSaleOrders.LoadInfo(objInputChangeOrder.SaleOrderID);
                if (objSaleOrder_Out != null && !string.IsNullOrEmpty(objSaleOrder_Out.SaleOrderID))
                {
                    decSumSalePrice = decSumSalePrice + objSaleOrder_Out.TotalLiquidate;
                    if (objSaleOrder_Out.TotalLiquidate != 0)
                    {
                        lblTotalLiquidate.Text = "Tổng tiền thanh toán của đơn hàng " + objSaleOrder_Out.SaleOrderID + " là " + objSaleOrder_Out.TotalLiquidate.ToString("#,##0");
                        lblTotalLiquidate.Visible = txtReturnAmount.Visible;
                    }
                }
            }
            return decSumSalePrice;
        }

        private void CalTotalMoney()
        {
            if (!objInputChangeOrderType.IsInputReturn)
            {
                if (txtReturnAmount.Visible)
                {
                    decimal decSumInputPrice = GetCompute_In("IsSelect = 1");
                    decimal decSumSalePrice = GetCompute_Out("IsSelect = 1");
                    if (Math.Round(decSumInputPrice) > Math.Round(decSumSalePrice))
                    {
                        lblTitlePayment.Text = "Phải chi";
                        decReturnAmount = (decSumInputPrice - decSumSalePrice);
                        txtReturnAmount.Text = decReturnAmount.ToString("#,##0");
                    }
                    else
                    {
                        if (Math.Round(decSumInputPrice) == Math.Round(decSumSalePrice))
                            lblTitlePayment.Text = "Phải chi";
                        else
                            lblTitlePayment.Text = "Phải thu";
                        decReturnAmount = (decSumSalePrice - decSumInputPrice);
                        txtReturnAmount.Text = decReturnAmount.ToString("#,##0");
                    }
                }
            }
            else
            {
                if (txtReturnAmount.Visible)
                {
                    lblTitlePayment.Text = "Phải chi";
                    decimal decSumInputPrice = GetCompute_In("IsSelect = 1");
                    decReturnAmount = decSumInputPrice;
                    txtReturnAmount.Text = decReturnAmount.ToString("#,##0");
                }
            }
        }

        private void flexDetail_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexDetail.Rows.Fixed)
            {
                return;
            }
            if (dtbOVDetailReturn == null || dtbOVDetailReturn.Rows.Count <= (e.Row - flexDetail.Rows.Fixed))
                return;
            rOldEdit = dtbOVDetailReturn.NewRow();
            DataRow rData = dtbOVDetailReturn.Rows[e.Row - flexDetail.Rows.Fixed];
            rOldEdit.ItemArray = rData.ItemArray;

            if (flexDetail.Cols.Contains("IsSelect"))
            {
                if (e.Col != flexDetail.Cols["IsSelect"].Index && (!Convert.ToBoolean(flexDetail[e.Row, "IsSelect"])))
                {
                    e.Cancel = true;
                    return;
                }
            }
            if (e.Col == flexDetail.Cols["IMEI_Out"].Index)
            {
                if (flexDetail[e.Row, "ProductName_Out"] != DBNull.Value && Convert.ToString(flexDetail[e.Row, "ProductName_Out"]).Trim() != string.Empty)
                    e.Cancel = true;
            }
            else if (e.Col == flexDetail.Cols["Quantity"].Index)
            {
                if (rData["IMEI"] != DBNull.Value && Convert.ToString(rData["IMEI"]).Trim() != string.Empty)
                {
                    e.Cancel = true;
                }
                //if (objInputChangeOrderType != null && !objInputChangeOrderType.IsInputReturn)
                //{
                //    if (flexDetail[e.Row, "ProductName_Out"] == DBNull.Value || string.IsNullOrEmpty(flexDetail[e.Row, "ProductName_Out"].ToString()))
                //        e.Cancel = true;
                //}
            }
            if (e.Col == flexDetail.Cols["MACHINEERRORNAME"].Index)
            {
                if (rData["INSTOCKSTATUSID_IN"] != DBNull.Value && Convert.ToUInt32(rData["INSTOCKSTATUSID_IN"]) != 5 && Convert.ToUInt32(rData["INSTOCKSTATUSID_IN"]) != 3)
                {
                    e.Cancel = true;
                }
            }
            if (e.Col == flexDetail.Cols["MACHINEERRORGROUPNAME"].Index)
            {
                e.Cancel = true;
            }
        }

        private void flexDetail_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexDetail.Rows.Fixed)
            {
                return;
            }
            dtbOVDetailReturn.AcceptChanges();
            if (e.Col == flexDetail.Cols["IMEI_Out"].Index)
            {
                string strIMEIOldEdit = string.Empty;
                if (rOldEdit["IMEI_Out"] != DBNull.Value)
                    strIMEIOldEdit = rOldEdit["IMEI_Out"].ToString().Trim();
                string strBarcode = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(flexDetail[e.Row, "IMEI_Out"].ToString().Trim());
                if (strBarcode.Length < 1)
                    return;
                if (!Library.AppCore.Other.CheckObject.CheckIMEI(strBarcode))
                {
                    MessageBox.Show(this, "IMEI không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }
                StringBuilder arrAutoGetIMEIList = new StringBuilder();
                ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(flexDetail[e.Row, "OutputTypeID_Out"]));
                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strBarcode);
                if (objProduct != null)
                {
                    MessageBox.Show(this, "IMEI này trùng với mã sản phẩm. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }
                ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, cboStore.StoreID, Convert.ToInt32(flexDetail[e.Row, "OutputTypeID_Out"]), arrAutoGetIMEIList.ToString(), true, true);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }
                if (objProductInStock == null || (!objProductInStock.IsInstock))
                {
                    MessageBox.Show(this, "Số IMEI " + strBarcode + " không tồn kho. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }

                ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                decimal decOut = 0;
                objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, cboStore.StoreID, objProductInStock.ProductID, objProductInStock.IMEI);
                if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI))
                {
                    if (decOut > 0)
                    {
                        MessageBox.Show(this, "IMEI [" + objProductInStock.IMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                }
                else
                {
                    if (decOut > 0)
                    {
                        objProductInStock.Quantity = objProductInStock.Quantity - decOut;
                    }
                    if (objProductInStock.Quantity <= 0)
                    {
                        MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductID + " - " + objProductInStock.ProductName + " không tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }


                if (!Convert.ToBoolean(flexDetail[e.Row, "IsRequestIMEI"]))
                {
                    MessageBox.Show(this, "Sản phẩm nhập không có yêu cầu IMEI. Vui lòng chọn sản phẩm xuất không có yêu cầu IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (dtbOVDetailReturn.Select("TRIM(IMEI_Out) = '" + strBarcode + "'").Length > 1)
                {
                    MessageBox.Show(this, "Số IMEI " + strBarcode + " đã tồn tại trên lưới dữ liệu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }
                if (objInputChangeOrderType.IsNew == 1 || objInputChangeOrderType.IsNew == 0)
                {
                    bool bolIsNew_InputChangeOrderType = (objInputChangeOrderType.IsNew == 1 ? true : false);
                    if ((!objInputChangeOrderType.IsAllowChangeOtherStatus) && bolIsNew_InputChangeOrderType != objProductInStock.IsNew)
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả sản phẩm " + (bolIsNew_InputChangeOrderType ? "mới" : "cũ") + ". Số IMEI " + objProductInStock.IMEI + " đang tồn " + (objProductInStock.IsNew ? "mới" : "cũ") + ". Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                }
                //NLT gắn InstockStatusID
                int intInstockStatusID_Out = Convert.ToInt32(objProductInStock.InStockStatusID);
                flexDetail[e.Row, "INSTOCKSTATUSID_OUT"] = intInstockStatusID_Out;

                //Lấy giá bán
                decimal decSalePrice = objProductInStock.Price;
                switch (objOutputType_Out.GetPriceType)
                {
                    case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.COSTPRICE:
                        decSalePrice = objProductInStock.CostPrice;
                        break;
                    case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO:
                        decSalePrice = 0;
                        break;
                    case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.FIRSTPRICE:
                        decSalePrice = objProductInStock.FirstPrice;
                        break;
                }
                if (decSalePrice == 0 && objOutputType_Out.GetPriceType != (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO)
                {
                    MessageBox.Show(this, "Mặt hàng " + objProductInStock.ProductName + " chưa được thiết lập giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }

                if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                    decSalePrice = decSalePrice * (Convert.ToDecimal(1) + Convert.ToDecimal(objProductInStock.VAT) * Convert.ToDecimal(objProductInStock.VATPercent) * Convert.ToDecimal(0.0001));

                ERP.MasterData.PLC.MD.WSProduct.Product objProductIn = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim());
                ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(objProductInStock.ProductID);
                if (objInputChangeOrderType.ProductChangeConditionType == 2 || objInputChangeOrderType.ProductChangeConditionType == 3)
                {
                    if (objInputChangeOrderType.ProductChangeConditionType == 2 && Convert.ToString(objProductIn.ProductID).Trim() != Convert.ToString(objProductInStock.ProductID).Trim())
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng mã sản phẩm. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                    if (objInputChangeOrderType.ProductChangeConditionType == 3)
                    {
                        //  ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(objProductInStock.ProductID);
                        if (Convert.ToString(objProductIn.ModelID).Trim() != Convert.ToString(objProductOut.ModelID).Trim())
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                            return;
                        }
                        else
                        {
                            if (Convert.ToString(objProductIn.ModelID).Trim() == string.Empty && Convert.ToString(objProductIn.ModelID).Trim() == Convert.ToString(objProductOut.ModelID).Trim())
                            {
                                MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                                return;
                            }
                        }
                    }
                }

                if (objInputChangeOrderType.ProductChangeConditionType == 4
                    || objInputChangeOrderType.ProductChangeConditionType == 5
                    || objInputChangeOrderType.ProductChangeConditionType == 6)
                {
                    if (objInputChangeOrderType.ProductChangeConditionType == 5 && Math.Round(decSalePrice) < Math.Round(Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"])))
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả với sản phẩm xuất có giá bán lớn hơn hoặc bằng giá nhập lại. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                    else if (objInputChangeOrderType.ProductChangeConditionType == 6 && Math.Round(decSalePrice) >= Math.Round(Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"])))
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả với sản phẩm xuất có giá bán nhỏ hơn giá nhập lại. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                    else if (objInputChangeOrderType.ProductChangeConditionType == 4 && Math.Round(decSalePrice) != Math.Round(Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"])))
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng giá(giá nhập lại phải bằng giá bán của sản phẩm xuất). Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                }
                if (objInputChangeOrderType.ProductChangeConditionType == 7) //cùng giá cùng model
                {
                    if (Convert.ToString(objProductIn.ModelID).Trim() != Convert.ToString(objProductOut.ModelID).Trim())
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        if (Convert.ToString(objProductIn.ModelID).Trim() == string.Empty && Convert.ToString(objProductIn.ModelID).Trim() == Convert.ToString(objProductOut.ModelID).Trim())
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    if (Math.Round(decSalePrice) != Math.Round(Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"])))
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng giá(giá nhập lại phải bằng giá bán của sản phẩm xuất). Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                // bool IsUpdateConcern = false;
                if (objProductInStock.StatusFIFOID > 0)
                {
                    if (objProductInStock.StatusFIFOID == 1 || (objProductInStock.StatusFIFOID > 1 && (objProductInStock.StatusFIFO.IndexOf("<3>") < 0)))
                    {
                        //if (MessageBox.Show(this, "Imei xuất sai FIFO bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        //{
                        //    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        //    return;
                        //}
                        DialogResult drResult =
                    ClsFIFO.ShowMessengerFIFOList((objProductInStock.StatusFIFOID > 1 && (objProductInStock.StatusFIFO.IndexOf("<3>") < 0)) ? "IMEI " + objProductInStock.IMEI + " vi phạm fifo! Bạn có muốn tiếp tục?" : objProductInStock.StatusFIFO, objProductInStock.ProductID, objProductInStock.IMEI, cboStore.StoreID, -1);
                        if (drResult != System.Windows.Forms.DialogResult.OK)
                        {
                            if (!objPLCProductInStock.Update_IsOrderingWithIMEI(objProductInStock.IMEI.ToString().Trim(), 0, cboStore.StoreID))
                            {
                                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                                {
                                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                                    return;
                                }
                            }
                            flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                            return;
                        }
                    }
                    else if (objProductInStock.StatusFIFOID == 2)
                    {
                        if (objProductInStock.StatusFIFO.IndexOf("<3>") >= 0)
                            lstIMEIUpdateConcern.Add(strBarcode.Trim());
                        else
                        {
                            flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                            return;
                        }

                    }
                }
                flexDetail[e.Row, "ProductID_Out"] = objProductInStock.ProductID;
                flexDetail[e.Row, "ProductName_Out"] = objProductInStock.ProductName;
                flexDetail[e.Row, "IsRequestIMEI_Out"] = objProductInStock.IsRequestIMEI;
                flexDetail[e.Row, "IMEI_Out"] = strBarcode;
                flexDetail[e.Row, "SalePrice_Out"] = decSalePrice;
                //if (objOutputType_Out.GetPriceType != (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO && Convert.ToString(flexDetail[e.Row, "ProductID_Out"]).Trim() == Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim() && objProductInStock.IsPriceOfProduct)

                if (objOutputType_Out.GetPriceType != (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO && (Convert.ToString(flexDetail[e.Row, "ProductID_Out"]).Trim() == Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim() && objProductInStock.IsPriceOfProduct) || objInputChangeOrderType.ProductChangeConditionType == 3 || Convert.ToString(objProductIn.ModelID).Trim() == Convert.ToString(objProductOut.ModelID).Trim().Trim())
                    flexDetail[e.Row, "SalePrice_Out"] = flexDetail[e.Row, "SalePriceVAT"];
            }
            else if (e.Col == flexDetail.Cols["IsSelect"].Index)
            {
                LoadInputType();
                if (Convert.ToBoolean(flexDetail[e.Row, "IsSelect"]) == true)
                {
                    if (!string.IsNullOrEmpty(strInputChangeOrderTypeIdSub))
                    {
                        List<int> lstInputChangeOrderId = strInputChangeOrderTypeIdSub.Split(',').Select(int.Parse).ToList();
                        string strIMEI = flexDetail[e.Row, "IMEI"].ToString().Trim();
                        if (string.IsNullOrEmpty(strIMEI)) //ko IMEI là SP thường
                        {
                            if (lstInputChangeOrderId.Any(x => x == objInputChangeOrderType.InputChangeOrderTypeID))
                            {
                                MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả của sản phẩm subsidy. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                flexDetail[e.Row, "IsSelect"] = false;
                                return;
                            }
                        }
                        ERP.SaleProgram.PLC.PLCVTT_VERIFIEDPARTNER objPLCVTTVERIFIEDPARTNER = new ERP.SaleProgram.PLC.PLCVTT_VERIFIEDPARTNER();
                        DataTable dtbResult = null;
                        var objResultMessage = objPLCVTTVERIFIEDPARTNER.CheckPartnerCode(ref dtbResult, strIMEI);
                        if (objResultMessage.IsError)
                        {
                            MessageBoxObject.ShowWarningMessage(this, objResultMessage.MessageDetail);
                            flexDetail[e.Row, "IsSelect"] = false;
                            return;
                        }
                        if (dtbResult == null || dtbResult.Rows.Count < 1) //là IMEI sản phẩm thường
                        {
                            if (lstInputChangeOrderId.Any(x => x == objInputChangeOrderType.InputChangeOrderTypeID))
                            {
                                MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả của sản phẩm subsidy. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                flexDetail[e.Row, "IsSelect"] = false;
                                return;
                            }
                        }
                        else //là IMEI subsidy
                        {
                            if (!lstInputChangeOrderId.Any(x => x == objInputChangeOrderType.InputChangeOrderTypeID))
                            {
                                MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả của sản phẩm không phải subsidy. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                flexDetail[e.Row, "IsSelect"] = false;
                                return;
                            }
                        }
                    }
                }
            }
            else if (e.Col == flexDetail.Cols["Quantity"].Index)
            {
                decimal decQuantity = Convert.ToDecimal(rOldEdit["Quantity2"]);
                if (Convert.ToDecimal(flexDetail[e.Row, "Quantity"]) > decQuantity || Convert.ToDecimal(flexDetail[e.Row, "Quantity"]) < 1)
                    flexDetail[e.Row, "Quantity"] = decQuantity;
                flexDetail[e.Row, "TotalCost"] = (Convert.ToDecimal(flexDetail[e.Row, "TotalCost2"]) / Convert.ToDecimal(flexDetail[e.Row, "Quantity2"])) * Convert.ToDecimal(flexDetail[e.Row, "Quantity"]);
                flexDetail[e.Row, "TotalFee"] = (Convert.ToDecimal(flexDetail[e.Row, "TotalFee2"]) / Convert.ToDecimal(flexDetail[e.Row, "Quantity2"])) * Convert.ToDecimal(flexDetail[e.Row, "Quantity"]);
            }
            else if (e.Col == flexDetail.Cols["ProductID_Out"].Index)
            {
                string strProductIDOldEdit = string.Empty;
                if (rOldEdit["ProductID_Out"] != DBNull.Value)
                    strProductIDOldEdit = rOldEdit["ProductID_Out"].ToString().Trim();
                if (Convert.ToString(flexDetail[e.Row, "ProductID_Out"]).Trim() == string.Empty)
                {
                    flexDetail[e.Row, "ProductName_Out"] = string.Empty;
                    flexDetail[e.Row, "IsRequestIMEI_Out"] = false;
                    flexDetail[e.Row, "IMEI_Out"] = string.Empty;
                    flexDetail[e.Row, "SalePrice_Out"] = 0;
                }
                else
                    flexDetail[e.Row, "ProductID_Out"] = strProductIDOldEdit;
            }
            CalTotalMoney();
        }

        private void lnkCustomer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ERP.MasterData.DUI.Search.frmCustomerSearch frmCustomerSearch1 = new MasterData.DUI.Search.frmCustomerSearch();
            frmCustomerSearch1.ShowDialog();
            if (frmCustomerSearch1.Customer != null && frmCustomerSearch1.Customer.CustomerID > 0)
            {
                txtCustomerName.Text = frmCustomerSearch1.Customer.CustomerName;
                txtCustomerAddress.Text = frmCustomerSearch1.Customer.CustomerAddress;
                txtTaxID.Text = frmCustomerSearch1.Customer.CustomerTaxID;
                txtCustomerPhone.Text = frmCustomerSearch1.Customer.CustomerPhone;
                txtCustomerIDCard.Text = frmCustomerSearch1.Customer.CustomerIDCard;
            }
            else
            {
                if (Convert.ToString(txtCustomerID.Text).Trim() != string.Empty)
                {
                    if (MessageBox.Show(this, "Bạn muốn bỏ chọn khách hàng hiện tại không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                }
                txtCustomerName.Text = string.Empty;
                txtCustomerAddress.Text = string.Empty;
                txtTaxID.Text = string.Empty;
                txtCustomerPhone.Text = string.Empty;
                txtCustomerIDCard.Text = string.Empty;
            }
        }

        #endregion
        private void flexWorkFlow_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < flexWorkFlow.Rows.Fixed)
                return;
            if (Convert.ToBoolean(flexWorkFlow[e.Row, "IsProcessed2"]))
                e.Cancel = true;
            if (intWorkFlowCurrent < 1 || Convert.ToInt32(flexWorkFlow[e.Row, "InputChangeOrderStepID"]) != intWorkFlowCurrent)
                e.Cancel = true;
        }

        private void flexWorkFlow_Click(object sender, EventArgs e)
        {
            if (flexWorkFlow.RowSel < flexWorkFlow.Rows.Fixed || Convert.ToBoolean(flexWorkFlow[flexWorkFlow.RowSel, "IsProcessed2"]))
                return;
            if (intWorkFlowCurrent > 0 && Convert.ToInt32(flexWorkFlow[flexWorkFlow.RowSel, "InputChangeOrderStepID"]) != intWorkFlowCurrent)
            {
                if (flexWorkFlow.ColSel == flexWorkFlow.Cols["IsProcessed"].Index && Convert.ToInt32(flexWorkFlow[flexWorkFlow.RowSel, "InputChangeOrderStepID"]) != intWorkFlowCurrent)
                {
                    if ((!chkIsInputchange.Checked) && (!bolIsDeleted))
                    {
                        MessageBox.Show(this, "Vui lòng xử lý qui trình theo thứ tự", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
        }

        private void mnuAttachment_Opening(object sender, CancelEventArgs e)
        {
            mnuDelAttachment.Enabled = false;
            if (chkIsInputchange.Checked || intReviewStatus == 2)
                mnuAddAttachment.Enabled = false;
            try
            {
                if (grvAttachment.FocusedRowHandle < 0) return;
                PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objAttachmentInfo = lstInputChangeOrder_Attachment[grvAttachment.FocusedRowHandle];
                if (objAttachmentInfo.AttachmentName != null && Convert.ToString(objAttachmentInfo.AttachmentName).Trim() != string.Empty)
                    mnuDelAttachment.Enabled = (btnAdd.Enabled || strInputChangeOrderID != string.Empty);
            }
            catch { }
        }

        private void mnuAddAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog objOpenFileDialog = new OpenFileDialog();
                objOpenFileDialog.Multiselect = true;
                objOpenFileDialog.Filter = "Office Files|*.doc;*.docx;*.xls;*.xlsx;*.pdf;*.ppt;*.zip;*.rar;*.jpg;*.jpe;*.jpeg;*.gif;*.png;*.bmp;*.tif;*.tiff;*.txt";
                if (objOpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String[] strLocalFilePaths = objOpenFileDialog.FileNames;
                    foreach (string strLocalFilePath in strLocalFilePaths)
                    {
                        FileInfo objFileInfo = new FileInfo(strLocalFilePath);
                        var ListCheck = from o in lstInputChangeOrder_Attachment
                                        where o.AttachmentName == objFileInfo.Name
                                        select o;
                        if (ListCheck.Count() > 0)
                            continue;
                        decimal FileSize = Math.Round(objFileInfo.Length / Convert.ToDecimal((1024 * 1024)), 2); //dung lượng tính theo MB
                        decimal FileSizeLimit = 2;//MB
                        if (FileSize > FileSizeLimit)
                        {
                            MessageBox.Show(this, "Vui lòng chọn tập tin đính kèm dung lượng không vượt quá 2 MB", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objInputChangeOrder_Attachment = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment();
                        objInputChangeOrder_Attachment.AttachmentName = objFileInfo.Name;
                        objInputChangeOrder_Attachment.AttachmentLocalPath = strLocalFilePath;
                        objInputChangeOrder_Attachment.AttachmentPath = DateTime.Now.ToString("yyyy_MM") + "/{0}/" + Guid.NewGuid() + "_" + objInputChangeOrder_Attachment.AttachmentName;
                        objInputChangeOrder_Attachment.CreatedDate = Globals.GetServerDateTime();
                        objInputChangeOrder_Attachment.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objInputChangeOrder_Attachment.CreatedFullName = SystemConfig.objSessionUser.UserName + " - " + SystemConfig.objSessionUser.FullName;
                        objInputChangeOrder_Attachment.IsAddNew = true;
                        objInputChangeOrder_Attachment.STT = lstInputChangeOrder_Attachment.Count + 1;
                        lstInputChangeOrder_Attachment.Add(objInputChangeOrder_Attachment);
                    }
                    grdAttachment.RefreshDataSource();
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi thêm tập tin", objExce.ToString(), this.Name + " -> mnuAddAttachment_Click", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi thêm tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuDelAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvAttachment.FocusedRowHandle < 0)
                    return;
                PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objInputChangeOrder_Attachment = (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
                if (objInputChangeOrder_Attachment != null && lstInputChangeOrder_Attachment != null)
                {
                    if (objInputChangeOrder_Attachment.AttachmentID != null && objInputChangeOrder_Attachment.AttachmentID.Trim() != string.Empty)
                    {
                        if (btnAdd.Enabled || strInputChangeOrderID != string.Empty)
                        {
                            if (!btnUpdate.Enabled)
                            {
                                MessageBox.Show(this, "Bạn không có quyền xóa file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                    if (MessageBox.Show(this, "Bạn muốn xóa file đính kèm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        lstInputChangeOrder_Attachment.Remove(objInputChangeOrder_Attachment);
                        int intSTT = 0;
                        for (int i = 0; i < lstInputChangeOrder_Attachment.Count; i++)
                            lstInputChangeOrder_Attachment[i].STT = ++intSTT;
                        if (objInputChangeOrder_Attachment.AttachmentID != null && objInputChangeOrder_Attachment.AttachmentID.Trim() != string.Empty)
                        {
                            if (lstInputChangeOrder_Attachment_Del == null)
                                lstInputChangeOrder_Attachment_Del = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment>();
                            objInputChangeOrder_Attachment.IsAddNew = false;
                            objInputChangeOrder_Attachment.IsDeleted = true;
                            objInputChangeOrder_Attachment.DeletedUser = SystemConfig.objSessionUser.UserName;
                            lstInputChangeOrder_Attachment_Del.Add(objInputChangeOrder_Attachment);
                            if (stbAttachmentLog.Length > 0)
                                stbAttachmentLog.Append("\r\n");
                            stbAttachmentLog.Append("Xóa tập tin: " + objInputChangeOrder_Attachment.AttachmentName);
                        }
                        grdAttachment.RefreshDataSource();
                    }
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi xóa tập tin", objExce.ToString(), this.Name + " -> mnuDelAttachment_Click", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xóa tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (strInputChangeOrderID == string.Empty)
                return;
            if (!btnDelete.Enabled || !btnDelete.Visible)
                return;
            if (!Globals.ValidatingSystemDate(this))
                return;
            bool bolEnableDelete = btnDelete.Enabled;
            btnDelete.Enabled = false;
            Library.AppControl.FormCommon.frmInputString frmInputString1 = new Library.AppControl.FormCommon.frmInputString();
            frmInputString1.MaxLengthString = 400;
            frmInputString1.Text = "Nhập ghi chú hủy yêu cầu " + strMessageForm + " " + strInputChangeOrderID;
            frmInputString1.ShowDialog(this);
            if (!frmInputString1.IsAccept)
            {
                btnDelete.Enabled = bolEnableDelete;
                return;
            }
            objPLCInputChangeOrder.Delete(strInputChangeOrderID, frmInputString1.ContentString);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                btnDelete.Enabled = bolEnableDelete;
            }
            else
            {
                MessageBox.Show("Hủy yêu cầu " + strMessageForm + " thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsHasAction = true;
                this.Close();
            }
        }

        private void mnuWorkFlow_Opening(object sender, CancelEventArgs e)
        {
            mnuWorkFlow_IsProcess.Enabled = false;
            mnuWorkFlow_IsProcess.Tag = 0;
            if (!flexWorkFlow.Cols["IsProcessed"].AllowEditing)
                return;
            if (flexWorkFlow.RowSel < flexWorkFlow.Rows.Fixed)
                return;
            if (intWorkFlowCurrent > 0 && Convert.ToInt32(flexWorkFlow[flexWorkFlow.RowSel, "InputChangeOrderStepID"]) == intWorkFlowCurrent)
            {
                if (SystemConfig.objSessionUser.IsPermission(flexWorkFlow[flexWorkFlow.RowSel, "ProcessFunctionID"].ToString()) &&
                    Convert.ToBoolean(flexWorkFlow[flexWorkFlow.RowSel, "IsProcessed"]))
                {
                    if (intReviewStatus != 2)
                    {
                        mnuWorkFlow_IsProcess.Enabled = true;
                        mnuWorkFlow_IsProcess.Tag = intWorkFlowCurrent;
                    }
                }
            }
        }

        private void mnuWorkFlow_IsProcess_Click(object sender, EventArgs e)
        {
            if (strInputChangeOrderID == string.Empty)
                return;
            if (!Convert.ToBoolean(flexWorkFlow[flexWorkFlow.RowSel, "IsProcessed"]))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng tích chọn Đã xử lý");
                return;
            }
            PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_WorkFlow objWorkFlow = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_WorkFlow();
            objWorkFlow.InputChangeOrderID = strInputChangeOrderID;
            objWorkFlow.InputChangeOrderStepID = Convert.ToInt32(mnuWorkFlow_IsProcess.Tag);
            objWorkFlow.InputChangeOrderDate = objInputChangeOrder.InputChangeOrderDate;
            objWorkFlow.ProcessedUser = SystemConfig.objSessionUser.UserName;
            objWorkFlow.ProcessedTime = Globals.GetServerDateTime();
            objWorkFlow.IsProcessed = true;
            objWorkFlow.Note = Convert.ToString(flexWorkFlow[flexWorkFlow.RowSel, "Note"]).Trim();

            objPLCInputChangeOrder.UpdateProcess(objWorkFlow);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
            }
            else
            {
                MessageBox.Show("Cập nhật xử lý bước xử lý yêu cầu " + strMessageForm + " thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void flexWorkFlow_MouseEnterCell(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < flexWorkFlow.Rows.Fixed)
                return;
            toolTip1.SetToolTip(flexWorkFlow, string.Empty);
            if (e.Col == flexWorkFlow.Cols["Note"].Index)
            {
                string strNote = Convert.ToString(flexWorkFlow[e.Row, "Note"]).Trim();
                toolTip1.SetToolTip(flexWorkFlow, strNote);
            }
            //   toolTip4

        }

        private void flexWorkFlow_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexWorkFlow.DataSource != null)
                if (e.Col == flexWorkFlow.Cols["Note"].Index)
                {
                    TextBox textBox = (TextBox)this.flexWorkFlow.Editor;
                    textBox.MaxLength = 600;
                }
        }

        private void tmrCloseForm_Tick(object sender, EventArgs e)
        {
            if (intClosedForm == 0)
            {
                intClosedForm = 1;
                if (strMessageCloseForm != string.Empty)
                    MessageBoxObject.ShowWarningMessage(this, strMessageCloseForm);
                System.Threading.Thread.Sleep(500);
                this.Close();
            }
        }

        private void grdAttachment_EditorKeyPress(object sender, KeyPressEventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            if (grvAttachment.FocusedColumn.FieldName.ToUpper().Equals("DESCRIPTION"))
            {
                PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objAttachment = (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
                if (!objAttachment.IsAddNew)
                {
                    e.Handled = true;
                }
            }
            else if (grvAttachment.FocusedColumn.FieldName.ToUpper().Equals("COLDOWNLOAD"))
                e.Handled = true;
        }

        private void lnkSaleOrderID_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (txtSaleOrderID.Text.Trim() != string.Empty)
                SalesAndServices.SaleOrders.DUI.DUISaleOrders_Common.ViewSaleOrder(this, objInputChangeOrder.SaleOrderID);
        }

        private void lblInVoiceID_Click(object sender, EventArgs e)
        {

        }

        private void chkCreateSaleOrder_CheckedChanged(object sender, EventArgs e)
        {
            if (!objInputChangeOrderType.IsInputReturn)
            {
                if (/*chkCreateSaleOrder.Checked ||*/ objInputChangeOrderType.IsGetPromotion)
                    FormatFlexReturn();
                else
                    FormatFlex();
            }
        }

        private void btnSupportCare_Click(object sender, EventArgs e)
        {
            if (strInputChangeOrderID == string.Empty)
                return;
            if (!btnSupportCare.Visible)
                return;
            btnUpdate.Enabled = false;
            DataRow[] drNotIMEI = ((DataTable)flexDetail.DataSource).Select("IMEI IS NULL OR TRIM(IMEI)=''");
            if (drNotIMEI.Length > 0)
            {
                if (objPLCInputChangeOrder.UpdateCare(drNotIMEI[0]["OldOutputVoucherDetailID"].ToString()) > 0)
                    MessageBox.Show(this, "Hỗ trợ trả Care thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show(this, "Không tìm thấy Gói bảo hành hỗ trợ trả Care", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            btnUpdate.Enabled = true;
        }

        private void btnViewInvoice_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> lstVatInvoiceID = new List<string>();
                List<ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice> lstVatInvoice = new List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice>();
                ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
                DataTable dtbDetail = (DataTable)flexDetail.DataSource;
                foreach (DataRow row in dtbDetail.Rows)
                {
                    DataTable dtbInvoiceId = new ERP.Report.PLC.PLCReportDataSource()
                        .GetDataSource("VAT_INVOICE_SELBYOUTPUTID", new object[] { "@OUTPUTVOUCHERDETAILID", row["OUTPUTVOUCHERDETAILID"].ToString() });
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->btnViewInvoice_Click");
                    }
                    if (dtbInvoiceId != null)
                    {
                        string VatInvoiceId = dtbInvoiceId.Rows[0][0].ToString();
                        if (!lstVatInvoiceID.Contains(VatInvoiceId))
                        {
                            lstVatInvoiceID.Add(VatInvoiceId);
                            ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVATInvoice = new PrintVAT.PLC.WSPrintVAT.VAT_Invoice();
                            objPLCPrintVAT.LoadInfo(ref objVATInvoice, VatInvoiceId);
                            lstVatInvoice.Add(objVATInvoice);
                        }
                    }
                }
                DataTable dtbData = new DataTable();
                DataColumn col = new DataColumn();
                col.ColumnName = "VATINVOICEID";
                col.DefaultValue = strVatInvoiceId;
                dtbData.Columns.Add(col);
                col.ColumnName = "ISDELETE";
                col.DefaultValue = 0;
                dtbData.Columns.Add(col);
                col.ColumnName = "ISCANCEL";
                col.DefaultValue = 0;
                dtbData.Columns.Add(col);

                DataRow item = dtbData.Rows[0];
                ERP.PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT frm = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(1, item, 1);
                frm.ShowDialog();
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi xem hóa đơn bán hàng", objEx.ToString(), this.Name + " -> btnViewInvoice_Click", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xem hóa đơn bán hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //private void lblInVoiceID_Click_1(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        List<string> lstVatInvoiceID = new List<string>();
        //        List<ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice> lstVatInvoice = new List<PrintVAT.PLC.WSPrintVAT.VAT_Invoice>();
        //        ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
        //        DataTable dtbDetail = (DataTable)flexDetail.DataSource;
        //        foreach (DataRow row in dtbDetail.Rows)
        //        {
        //            DataTable dtbInvoiceId = new ERP.Report.PLC.PLCReportDataSource()
        //                .GetDataSource("VAT_INVOICE_SELBYOUTPUTID", new object[] { "@OUTPUTVOUCHERDETAILID", row["OUTPUTVOUCHERDETAILID"].ToString() });
        //            if (dtbInvoiceId != null)
        //            {
        //                string VatInvoiceId = dtbInvoiceId.Rows[0][0].ToString();
        //                if (!lstVatInvoiceID.Contains(VatInvoiceId))
        //                {
        //                    lstVatInvoiceID.Add(VatInvoiceId);
        //                    ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVATInvoice = new PrintVAT.PLC.WSPrintVAT.VAT_Invoice();
        //                    objPLCPrintVAT.LoadInfo(ref objVATInvoice, VatInvoiceId);
        //                    lstVatInvoice.Add(objVATInvoice);
        //                }
        //            }
        //        }
        //        DataTable dtbData = new DataTable();
        //        DataColumn col = new DataColumn();
        //        col.ColumnName = "VATINVOICEID";
        //        col.DefaultValue = strVatInvoiceId;
        //        dtbData.Columns.Add(col);
        //        col.ColumnName = "ISDELETE";
        //        col.DefaultValue = 0;
        //        dtbData.Columns.Add(col);
        //        col.ColumnName = "ISCANCEL";
        //        col.DefaultValue = 0;
        //        dtbData.Columns.Add(col);

        //        DataRow item = dtbData.Rows[0];
        //        ERP.PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT frm = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(1, item, 1);
        //        frm.ShowDialog();
        //    }
        //    catch (Exception objEx)
        //    {
        //        SystemErrorWS.Insert("Lỗi xem hóa đơn bán hàng", objEx.ToString(), this.Name + " -> btnViewInvoice_Click", DUIInventory_Globals.ModuleName);
        //        MessageBox.Show(this, "Lỗi xem hóa đơn bán hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //    }
        //}

        private void lblInvoiceNo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtInVoiceID.Text))
                return;
            try
            {
                ERP.PrintVAT.PLC.WSVAT_Invoice_Request.VAT_Invoice objVAT_Invoice = null;
                objVAT_Invoice = new ERP.PrintVAT.PLC.PrintVAT.PLCVAT_Invoice_Request().LoadInfo_VAT_Invoice(txtInVoiceID.Text.Trim(), txtDenominator.Text.Trim(), txtInvoiceSymbol.Text.Trim());
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                    return;
                }

                if (objVAT_Invoice != null)
                {
                    ERP.PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT frm = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(objVAT_Invoice.VATInvoiceID, objVAT_Invoice.FormTypeID);
                    frm.IsEdit = false;
                    frm.ShowDialog();
                }
                else
                {
                    MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin hóa đơn!");
                }

                /*
                String strOutputVoucherID = objOutputVoucherLoad.OutputVoucherID;
                var objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                ERP.SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrder objSaleOrder = new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().LoadInfo(objOutputVoucher.OrderID);
                //if (objSaleOrder != null)
                {
                    object[] objsKeyword = null;
                    if (objSaleOrder != null)
                    {
                        var objSaleOrderType = new ERP.MasterData.PLC.MD.PLCSaleOrderType().LoadInfoFromCache(objSaleOrder.SaleOrderTypeID);
                        objsKeyword = new object[]
                        {
                            "@SaleOrderTypeID", objSaleOrderType.SaleOrderTypeID,
                            "@StoreID", objOutputVoucher.OutputStoreID
                        };
                    }
                    else
                    {
                        objsKeyword = new object[]
                        {
                            "@NewOutputVoucherId", strOutputVoucherID,
                            "@StoreID", objOutputVoucher.OutputStoreID
                        };
                    }
                    DataTable dtsFormType = new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().GetFormTypebySaleOrderType(objsKeyword);
                    if (dtsFormType != null && dtsFormType.Rows.Count > 0)
                    {
                        int intFormTypeID = 0;
                        string strDenominator = string.Empty;
                        string strInvoiceSymbol1 = string.Empty;
                        int intCurrentNo = 0;
                        int intBranchID = 0;
                        string strINVOICENO = string.Empty;
                        string strFormTypeBranchID = "0";
                        int.TryParse(dtsFormType.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                        int.TryParse(dtsFormType.Rows[0]["BRANCHID"].ToString(), out intBranchID);
                        strDenominator = objOutputVoucherLoad.Denominator;//dtsFormType.Rows[0]["Denominator"].ToString();
                        strInvoiceSymbol1 = objOutputVoucherLoad.InvoiceSymbol;//dtsFormType.Rows[0]["InvoiceSymbol"].ToString();
                        int.TryParse(dtsFormType.Rows[0]["CURRENTNO"].ToString(), out intCurrentNo);
                        strFormTypeBranchID = dtsFormType.Rows[0]["FormTypeBranchID"].ToString();
                        strINVOICENO = objOutputVoucherLoad.InvoiceID;// dtsFormType.Rows[0]["INVOICENO"].ToString();
                        ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                        objCustomer.CustomerID = objOutputVoucher.CustomerID;
                        objCustomer.CustomerName = objOutputVoucher.CustomerName;
                        objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                        objCustomer.CustomerPhone = objOutputVoucher.CustomerPhone;
                        objCustomer.TaxCustomerAddress = objOutputVoucher.CustomerAddress;
                        objCustomer.CustomerAddress = objOutputVoucher.CustomerAddress;
                        objCustomer.CustomerTaxID = objOutputVoucher.CustomerTaxID;
                        DataTable dtbDataProduct = null;
                        object[] objsKeyword1 = new object[]
                        {
                        "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                        "@StoreID", objOutputVoucher.OutputStoreID
                        };
                        new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                        if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                        {
                            string strAddress = string.Empty;
                            string strTax = string.Empty;
                            var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                            if (objBranch.Count() > 0)
                            {
                                strAddress = objBranch[0]["TAXADDRESS"].ToString();
                                strTax = objBranch[0]["TAXCODE"].ToString();
                            }
                            string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                            PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                                dtbDataProduct, objCustomer, intBranchID, objOutputVoucher.OutputStoreID, strAddress, strTax, strDenominator,
                                strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 1, strFormTypeBranchID
                                , objOutputVoucher.DiscountReasonID, objOutputVoucher.Discount, objOutputVoucher.StaffUser, strLstCustomerName);
                            objfrmInvoiceVAT.IsEdit = false;
                            objfrmInvoiceVAT.ShowDialog();
                        }
                    }
                    else
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin hóa đơn xuất!");
                    }
                }
                */
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi xem hóa đơn bán hàng", objEx.ToString(), this.Name + " -> lblInvoiceNo_LinkClicked", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xem hóa đơn bán hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void flexData_MouseEnterCell(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < flexDetail.Rows.Fixed)
                return;
            toolTip2.SetToolTip(flexDetail, string.Empty);
            if (e.Col == flexDetail.Cols["MACHINEERRORNAME"].Index)
            {
                string strNote = Convert.ToString(flexDetail[e.Row, "MACHINEERRORNAME"]).Trim();
                toolTip2.SetToolTip(flexDetail, strNote);
            }

            toolTip4.SetToolTip(flexDetail, string.Empty);
            if (e.Col == flexDetail.Cols["MACHINEERRORGROUPNAME"].Index)
            {
                string strNote = Convert.ToString(flexDetail[e.Row, "MACHINEERRORGROUPNAME"]).Trim();
                toolTip4.SetToolTip(flexDetail, strNote);
            }
        }

        private void AddToMachineErrorList(string strInputChangeOrderId, string strOldOutputVoucherDetailID, string strProductIDIn, string strInputChangeOrderDetailID, int intMachineErrorID)
        {
            try
            {
                DataRow dataRow = dtbMachineErrorList.NewRow();
                dataRow["INPUTCHANGEORDERID"] = strInputChangeOrderId;
                dataRow["OLDOUTPUTVOUCHERDETAILID"] = strOldOutputVoucherDetailID;
                dataRow["PRODUCTID_IN"] = strProductIDIn;
                dataRow["INPUTCHANGEORDERDETAILID"] = strInputChangeOrderDetailID;
                dataRow["MACHINEERRORID"] = intMachineErrorID;
                dtbMachineErrorList.Rows.Add(dataRow);
            }
            catch (Exception ex)
            {

            }
        }
        private bool CheckSubsidy(string strIMEI)
        {
            ERP.SaleProgram.PLC.PLCVTT_VERIFIEDPARTNER objPLCVTTVERIFIEDPARTNER = new ERP.SaleProgram.PLC.PLCVTT_VERIFIEDPARTNER();
            DataTable dtbResult = null;
            var objResultMessage = objPLCVTTVERIFIEDPARTNER.CheckPartnerCode(ref dtbResult, strIMEI);
            if (objResultMessage.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, objResultMessage.MessageDetail);
                return false;
            }
            if (dtbResult == null || dtbResult.Rows.Count < 1) //là IMEI sản phẩm thường
            {
                return false;
            }
            return true;
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        #region variable
        private InputChangeOderVtplus vtPlus = new InputChangeOderVtplus();

        #endregion

        #region Tiêu điểm
        public string VtplusRollbackAdjustAccountPoint(string inputchangeorderid, string strInvoiceID, string CheckVTplus)
        {
            bool statusCheckReturnPoint = true; //trang thái để tiếp tục
            string resultError = null;//thông báo lỗi
            //string CheckVTplus = vtPlus.CallApi_checkExitVtAccount(txtCustomerPhone.Text.Trim(), "Kiểm tra khách hàng tồn tại trong hệ thống - Chức năng: nhập đổi trả");
            if (CheckVTplus == "001")
            {
                statusCheckReturnPoint = false;
                resultError = "Lỗi hệ thống: Kiểm tra thông tin khách hàng trong viettel ++\n";
            }
            else
            {
                if (!string.IsNullOrEmpty(CheckVTplus))//kiểm tra khách hàng
                {
                    #region
                    DataTable dtReturnPoint = vtPlus.dtSearchRoillback_TieuDiem(inputchangeorderid);// lấy thông tin tiêu điểm
                    if (dtReturnPoint == null)//lỗi tìm kiếm thông tin tiêu điểm
                    {
                        statusCheckReturnPoint = false;
                        resultError = "Lỗi hệ thống : tìm yêu cầu tiêu điểm\n";
                    }
                    if (dtReturnPoint.Rows.Count <= 0)//không có tiêu điểm
                    {
                        statusCheckReturnPoint = false;
                        //resultError = "Không tìm thấy yêu cầu tiêu điểm";
                    }
                    if (statusCheckReturnPoint)
                    {
                        string errAPI = null;
                        string errUsing = null;

                        string idExchane = dtReturnPoint.Rows[0]["ID_Exchange"].ToString().Trim();
                        string isdn = dtReturnPoint.Rows[0]["ISDN"].ToString().Trim();
                        int point = Convert.ToInt32(dtReturnPoint.Rows[0]["POINT_ADD"]);
                        var timeEndPoint = DateTime.Now.AddDays(365);
                        string pointExpTime = timeEndPoint.ToString("yyyyMMdd");

                        nhaplai:
                        string transId = Guid.NewGuid().ToString();
                        errAPI = vtPlus.CallApiTieuDiem(isdn, transId, point, pointExpTime);
                        if (!string.IsNullOrEmpty(errAPI))
                        {
                            if (errAPI == "023")
                            {
                                goto nhaplai;
                            }
                            else
                            {
                                statusCheckReturnPoint = false;
                                resultError = resultError + "Api : Tiêu điểm thất bại\n";
                            }
                        }

                        errUsing = vtPlus.InsertTieudiem(inputchangeorderid, idExchane, strInvoiceID, point, statusCheckReturnPoint);
                        if (!string.IsNullOrEmpty(errUsing))
                        {
                            statusCheckReturnPoint = false;
                            resultError = resultError + errUsing + "\n";
                        }


                        if (statusCheckReturnPoint)
                        {
                            return "Tiêu điểm cho đơn hàng mới: " + Convert.ToInt32(dtReturnPoint.Rows[0]["POINT_ADD"]) + " điểm\n";
                        }
                    }
                    #endregion
                }
            }
            if (!string.IsNullOrEmpty(resultError))
            {
                return resultError;
            }
            return null;
        }
        #endregion

        #region Hoàn điểm
        public string VtplusReturnPoint(string inputchangeorderid, string CheckVTplus, int tongdiem,string strVATInvoiceID)
        {
            //lấy thông tin phiếu
            bool statusCheckReturnPoint = true; //trang thái để tiếp tục Hoàn điểm
            string resultError = null;//thông báo lỗi

            //string CheckVTplus = vtPlus.CallApi_checkExitVtAccount(txtCustomerPhone.Text.Trim(), "Kiểm tra khách hàng tồn tại trong hệ thống - Chức năng: nhập đổi trả");
            if (CheckVTplus == "001")
            {
                statusCheckReturnPoint = false;
                resultError = "Lỗi hệ thống: Kiểm tra thông tin khách hàng trong viettel ++\n";
            }
            else
            {
                if (!string.IsNullOrEmpty(CheckVTplus))//kiểm tra khách hàng
                {
                    DataTable dtReturnPoint = vtPlus.dtSearchRoillback(inputchangeorderid, 0);// lấy thông tin tiêu điểm
                    if (dtReturnPoint == null)//lỗi tìm kiếm thông tin tiêu điểm
                    {
                        statusCheckReturnPoint = false;
                        resultError = "Lỗi hệ thống : Tra cứu thông tin tiêu điểm\nfrmInputChangeOrderViettel->VtplusReturnPoint->dtSearchVtplusPoint\n";
                    }
                    if (dtReturnPoint.Rows.Count <= 0)//không có tiêu điểm
                    {
                        statusCheckReturnPoint = false;
                    }
                    if (statusCheckReturnPoint)
                    {
                        int pointAmout = vtPlus.getReturnPoint(inputchangeorderid, Convert.ToInt32(dtReturnPoint.Rows[0]["POINT_ADD"]), Convert.ToString(dtReturnPoint.Rows[0]["ID_Exchange"]));
                        if (pointAmout == -1)
                        {
                            statusCheckReturnPoint = false;
                            resultError = resultError + "Lỗi hệ thống : tra cứu chi tiết đơn hàng trả.\nmã phiếu (" + inputchangeorderid + ")-> InputChangeOderVtplus >> getReturnPoint \n";
                        }
                        if (pointAmout == -2)
                        {
                            statusCheckReturnPoint = false;
                            resultError = resultError + "Lỗi hệ thống : Lỗi hệ thống : tra cứu giá phân bổ  -> InputChangeOderVtplus >> getReturnPoint \n";
                        }
                        if (pointAmout == -3)
                        {
                            statusCheckReturnPoint = false;
                            resultError = resultError + "Không tìm thấy thông tin phân bổ giá cho hoàn điểm \n";
                        }
                        if (statusCheckReturnPoint)
                        {
                            string errUsing = vtPlus.InsertReturnPoint(dtReturnPoint, inputchangeorderid, pointAmout, tongdiem, "Hoàn điểm form đổi trả");
                            if (errUsing != null)
                            {
                                statusCheckReturnPoint = false;
                                resultError = resultError + errUsing + "\n";
                            }
                            else
                            {
                                errUsing = vtPlus.InsertReturnpointDetail(inputchangeorderid, Convert.ToInt32(dtReturnPoint.Rows[0]["POINT_ADD"]), Convert.ToString(dtReturnPoint.Rows[0]["ID_Exchange"]), strVATInvoiceID);
                                if (errUsing != null)
                                {
                                    statusCheckReturnPoint = false;
                                    resultError = resultError + errUsing + "\n";
                                }
                            }
                            if (statusCheckReturnPoint)
                            {
                                return "Hoàn điểm cho đơn hàng trả: " + pointAmout + " điểm\n";
                            }
                        }

                    }
                }
            }

            if (resultError != null)
            {
                return resultError;
            }
            return null;
        }

        #endregion

        #region hủy điểm
        public string VtplusCancelPoint(string inputchangeorderid, string CheckVTplus, int tongdiem,string strVATInvoiceID)
        {
            //báo lỗi
            bool statusCheckCancelPoint = true; //trang thái để tiếp tục hủy điểm
            string resultError = null;
            //string CheckVTplus = vtPlus.CallApi_checkExitVtAccount(txtCustomerPhone.Text.Trim(), "Kiểm tra khách hàng tồn tại trong hệ thống - Chức năng: nhập đổi trả");

            if (CheckVTplus == "001")
            {
                statusCheckCancelPoint = false;
                resultError = "Lỗi hệ thống: Kiểm tra thông tin khách hàng trong viettel ++\n";
            }
            else
            {
                if (!string.IsNullOrEmpty(CheckVTplus))//kiểm tra khách hàng
                {
                    int cancelpoint = vtPlus.getCancellationPoint(inputchangeorderid.Trim()); //lấy điểm hủy

                    if (cancelpoint == -1)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "Lỗi hệ thống : tra cứu chi tiết đơn hàng trả.\nmã phiếu (" + inputchangeorderid + ")-> InputChangeOderVtplus >> RetrievePointInformation \n";
                    }
                    if (cancelpoint == -2)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "Không tìm chi tiết phiếu xuất đơn hàng trả khi thực hiện yêu cầu nhập trả  (" + inputchangeorderid + ").\n";
                    }
                    if (cancelpoint == -3)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "Lỗi hệ thống : tra cứu giá phân bổ.\n";
                    }
                    if (cancelpoint == -4)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "Không tìm thấy thông tin phân bổ giá.\n";
                    }
                    if (cancelpoint == -5)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "Lỗi hệ thống : tra cứu tích điểm đơn hàng trả thuộc mã phiếu (" + inputchangeorderid + ") -> InputChangeOderVtplus >> getCancellationPoint \n";
                    }
                    if (cancelpoint == -6)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "lỗi hệ thống tra cứu tiêu điểm đơn hàng trả thuộc mã phiếu (" + inputchangeorderid + ") -> InputChangeOderVtplus >> getCancellationPoint \n";
                    }
                    if (cancelpoint == 0)
                    {
                        statusCheckCancelPoint = false;//đơn hàng trả không được tích điểm hoặc không có sản phẩm nào được tích điểm
                    }
                    if (statusCheckCancelPoint)
                    {
                        #region kiểm tra đơn hàng trả - lấy tổng điểm
                        DataTable dtCancelPoint = vtPlus.dtSearchVtplusPoint(inputchangeorderid.Trim(), 1); //kiểm tra đơn hàng trả đã được tích điểm hay chưa
                        if (dtCancelPoint == null)
                        {
                            statusCheckCancelPoint = false;
                            resultError = resultError + "Lỗi hệ thống tra cứu tích điểm đơn hàng trả thuộc mã phiếu (" + inputchangeorderid + ") -> InputChangeOderVtplus >> getCancellationPoint \n";
                        }

                        //int tong = vtPlus.Getpoint(dtCancelPoint.Rows[0]["ISDN"].ToString().Trim(), "");
                        #endregion

                        #region Chức năng hủy điểm đổi/nhập trả
                        if (statusCheckCancelPoint)
                        {
                            string returnCancelpointStatus = vtPlus.InsertCancelPoint(dtCancelPoint, inputchangeorderid, cancelpoint, tongdiem, strVATInvoiceID, "Chức năng hủy điểm đổi/nhập trả");

                            #region messager
                            if (!string.IsNullOrEmpty(returnCancelpointStatus))
                            {
                                statusCheckCancelPoint = false;
                                resultError = resultError + returnCancelpointStatus + "\n";
                            }

                            #endregion
                            if (statusCheckCancelPoint)
                            {

                                if (tongdiem < cancelpoint)
                                {
                                    return "Hủy điểm đơn hàng trả, số điểm đã hủy là: " + cancelpoint.ToString("N0") + "\n\nSố điểm thực tế đã hủy :" + tongdiem.ToString("N0") + "\n\n";
                                }
                                else
                                {
                                    return "Hủy điểm đơn hàng trả, số điểm đã hủy là: " + cancelpoint.ToString("N0") + "\n\n";
                                }

                            }
                        }
                        #endregion
                    }
                }
            }
            if (!string.IsNullOrEmpty(resultError))
            {
                return resultError;
            }
            return null;
        }
        #endregion

        #region Tích điểm
        public string VtplusPointAdd(string inputchangeorderid, string strInvoiceID,string strInvoiceIDNew, string CheckVTplus, int tongdiem)
        {
            bool statusCheckCancelPoint = true; //trang thái để tiếp tục
            string resultError = null;//thông báo lỗi
            //string CheckVTplus = vtPlus.CallApi_checkExitVtAccount(txtCustomerPhone.Text.Trim(), "Kiểm tra khách hàng tồn tại trong hệ thống - Chức năng: nhập đổi trả");

            if (CheckVTplus == "001")
            {
                statusCheckCancelPoint = false;
                resultError = "Lỗi hệ thống: Kiểm tra thông tin khách hàng trong viettel ++\n";
            }
            else
            {
                if (!string.IsNullOrEmpty(CheckVTplus))//kiểm tra khách hàng
                {
                    int pluspoint = vtPlus.getPlusPoints(inputchangeorderid, strInvoiceID); //lấy điểm tích
                    if (pluspoint == -1)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "lỗi hệ thống: tra cứu chi tiết đơn hàng xuất mới -> InputChangeOderVtplus >> RetrievePointInformation \n";
                    }
                    if (pluspoint == -2)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "Không tìm chi tiết phiếu xuất mới đơn hàng mới khi thực hiện yêu cầu tích điểm đổi trả \n";
                    }
                    if (pluspoint == -3)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "Lỗi hệ thống : tra cứu giá phân bổ cho đơn hàng xuất mới  -> InputChangeOderVtplus >> RetrievePointInformation \n";
                    }
                    if (pluspoint == -4)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "Không tìm thấy thông tin phân bổ giá cho đơn hàng xuất mới \n";
                    }
                    if (pluspoint == -5)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "lỗi hệ thống: tra cứu thông tin đơn hàng xuất mới -> InputChangeOderVtplus >> getPlusPoints \n";
                    }
                    if (pluspoint == -6)
                    {
                        statusCheckCancelPoint = false;
                        resultError = resultError + "không tìm thấy thông tin đơn hàng xuất mới \n";
                    }

                    if (statusCheckCancelPoint)
                    {
                        int cancelpoint = vtPlus.getCancellationPoint(inputchangeorderid); //lấy điểm hủy
                        if (cancelpoint == -1)
                        {
                            statusCheckCancelPoint = false;
                            resultError = resultError + "Lỗi hệ thống : tra cứu chi tiết đơn hàng trả  -> InputChangeOderVtplus >> RetrievePointInformation \n";
                        }
                        if (cancelpoint == -2)
                        {
                            statusCheckCancelPoint = false;
                            resultError = resultError + "Không tìm chi tiết phiếu xuất đơn hàng trả khi thực hiện yêu cầu hủy điểm đổi trả  \n";
                        }
                        if (cancelpoint == -3)
                        {
                            statusCheckCancelPoint = false;
                            resultError = resultError + "Lỗi hệ thống : tra cứu giá phân bổ cho đơn hàng trả -> InputChangeOderVtplus >> RetrievePointInformation \n";
                        }
                        if (cancelpoint == -4)
                        {
                            statusCheckCancelPoint = false;
                            resultError = resultError + "Không tìm thấy thông tin phân bổ giá cho đơn hàng trả\n";
                        }
                        if (cancelpoint == -5)
                        {
                            statusCheckCancelPoint = false;
                            resultError = resultError + "lỗi hệ thống: tra cứu tích điểm đơn hàng trả -> InputChangeOderVtplus >> getCancellationPoint\n";
                        }
                        if (cancelpoint == -6)
                        {
                            statusCheckCancelPoint = false;
                            resultError = resultError + "lỗi hệ thống: tra cứu tiêu điểm đơn hàng trả -> InputChangeOderVtplus >> getCancellationPoint\n";
                        }

                        if (statusCheckCancelPoint)
                        {
                            DataTable dtSaleoder = vtPlus.dtSaleoder(inputchangeorderid);//Tra cưu đơn hàng xuất mới

                            if (dtSaleoder == null)
                            {
                                statusCheckCancelPoint = false;
                                resultError = resultError + "lỗi hệ thống: tra cứu thông tin đơn hàng xuất mới -> InputChangeOderVtplus >> dtSaleoder \n";
                            }

                            int pointAmount = pluspoint - cancelpoint;//tính điểm để cộng tích điểm

                            if (pointAmount == 0)
                            {
                                statusCheckCancelPoint = false;
                            }
                            if (statusCheckCancelPoint)
                            {
                                string errAPI = null;
                                string isdn = txtCustomerPhone.Text.Trim();
                                var timeEndPoint = DateTime.Now.AddDays(365);
                                string pointExpTime = timeEndPoint.ToString("yyyyMMdd");

                                string transid = "";
                                nhaplai:
                                transid = Guid.NewGuid().ToString();
                                errAPI = vtPlus.AddPoint(isdn, transid, pointAmount, tongdiem, pointExpTime, "Tích điểm cho khách hàng - Chức năng: nhập đổi trả");
                                if (!string.IsNullOrEmpty(errAPI))
                                {
                                    if (errAPI == "023")
                                    {
                                        goto nhaplai;
                                    }
                                    else
                                    {
                                        statusCheckCancelPoint = false;
                                        resultError = resultError + errAPI + "\n";
                                    }
                                }

                                string errUsing = null;


                                if (pluspoint > 0)
                                {
                                    string idExchange = Convert.ToString(dtSaleoder.Rows[0]["saleorderid"]).Trim();
                                    int idStore = SystemConfig.intDefaultStoreID;
                                    int idcustomer = Convert.ToInt32(txtCustomerID.Text.Trim());
                                    int id_OutputType = 1;
                                    DataTable dtOutputType = vtPlus.ListProductAddPoint(inputchangeorderid.Trim());
                                    if (dtOutputDate != null && dtOutputType.Rows.Count > 0)
                                    {
                                        id_OutputType = Convert.ToInt32(dtOutputType.Rows[0]["OUTPUTTYPEID"]);
                                    }
                                    errUsing = vtPlus.InsertAddpointTable(idcustomer, pluspoint, tongdiem, idExchange, DateTime.Now.AddDays(365), idStore, 4, transid.ToString(), isdn, statusCheckCancelPoint, id_OutputType, 1, CheckVTplus, inputchangeorderid, strInvoiceID);
                                    if (errUsing != null)
                                    {
                                        resultError = resultError + errUsing + "\n";
                                        statusCheckCancelPoint = false;
                                    }
                                }

                                if (cancelpoint > 0)
                                {
                                    DataTable dtCancelPoint = vtPlus.dtSearchVtplusPoint(inputchangeorderid.Trim(), 1); //kiểm tra đơn hàng trả đã được tích điểm hay chưa
                                    if (dtCancelPoint == null)
                                    {
                                        statusCheckCancelPoint = false;
                                        resultError = resultError + "Lỗi hệ thống tra cứu tích điểm đơn hàng trả thuộc mã phiếu (" + inputchangeorderid + ") -> InputChangeOderVtplus >> getCancellationPoint \n";
                                    }
                                    if (dtCancelPoint.Rows.Count > 0)
                                    {
                                        int id_OutputType = 0;
                                        string idExchange = Convert.ToString(dtCancelPoint.Rows[0]["ID_Exchange"]).Trim();
                                        int idcustomer = Convert.ToInt32(txtCustomerID.Text.Trim());
                                        int idStore = SystemConfig.intDefaultStoreID;
                                        errUsing = vtPlus.InsertAddpointTable(idcustomer, cancelpoint * -1, tongdiem, idExchange, DateTime.Now.AddDays(365), idStore, 4, transid.ToString(), isdn, statusCheckCancelPoint, id_OutputType, 1, CheckVTplus, inputchangeorderid, strInvoiceIDNew);
                                        if (errUsing != null)
                                        {
                                            resultError = resultError + errUsing + "\n";
                                            statusCheckCancelPoint = false;
                                        }
                                    }
                                }

                                //thông báo

                                if (statusCheckCancelPoint)
                                {
                                    if (pointAmount > 0)
                                    {
                                        strvtplusPointTs = "Tích điểm Viettell++ : " + pointAmount + " điểm";
                                        return "Đơn hàng trả: bị trừ " + cancelpoint.ToString() + " điểm \n\nĐơn hàng mua mới: được cộng " + pluspoint.ToString() + " điểm\n\nKhách hàng được cộng : " + pointAmount.ToString("N0") + " điểm\n\n";
                                    }
                                    if (pointAmount < 0)
                                    {
                                        if (tongdiem < pointAmount * -1)
                                        {
                                            strvtplusPointTs = "Tích điểm Viettell++ : " + tongdiem * -1 + " điểm";
                                            return "Đơn hàng trả: bị trừ " + cancelpoint.ToString() + " điểm \n\nĐơn hàng mua mới: được cộng " + pluspoint.ToString() + " điểm\n\nKhách hàng bị trừ : " + (pointAmount * -1).ToString("N0") + " điểm\nSố điểm trừ thực tế : " + tongdiem + "\n\n";
                                        }
                                        else
                                        {
                                            strvtplusPointTs = "Tích điểm Viettell++ : " + pointAmount + " điểm";
                                            return "Đơn hàng trả: bị trừ " + cancelpoint.ToString() + " điểm \n\nĐơn hàng mua mới: được cộng " + pluspoint.ToString() + " điểm\n\nKhách hàng bị trừ : " + (pointAmount * -1).ToString("N0") + " điểm\n\n";
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
            //thông báo
            if (!statusCheckCancelPoint)
            {
                if (!string.IsNullOrEmpty(resultError))
                {
                    return resultError;
                }
            }
            return null;
        }
        #endregion

        #region Token class
        public class Token
        {
            static public string strISDN;
            static public int strTransid;
            static public string strToken;
        }
        #endregion
    }
}