﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.InputChangeOrder
{
    public partial class frmSelectOV : Form
    {
        public frmSelectOV()
        {
            InitializeComponent();
        }

        public frmSelectOV(DataTable dtbOV)
        {
            InitializeComponent();
            this.dtbOV = dtbOV;
        }

        private PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
        private DataTable dtbOV = null;
        public string strOutputVoucherID = string.Empty;

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            if (row == null)
                return;
            strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            this.Close();
        }

        private void frmSelectOV_Load(object sender, EventArgs e)
        {
            strOutputVoucherID = string.Empty;
            grdData.DataSource = dtbOV;//objPLCOutputVoucher.GetListOV(strOrderID);
        }
    }
}
