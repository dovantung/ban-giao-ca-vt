﻿namespace ERP.Inventory.DUI.InputChangeOrder
{
    partial class frmInputChangeOrderInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnInput = new System.Windows.Forms.Button();
            this.radApplyTypeError = new System.Windows.Forms.RadioButton();
            this.radApplyTypeNoError = new System.Windows.Forms.RadioButton();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.txtOutputVoucherID = new System.Windows.Forms.TextBox();
            this.lnkType = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã phiếu xuất hoặc IMEI:";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.btnInput);
            this.groupControl1.Controls.Add(this.radApplyTypeError);
            this.groupControl1.Controls.Add(this.radApplyTypeNoError);
            this.groupControl1.Controls.Add(this.cboType);
            this.groupControl1.Controls.Add(this.txtOutputVoucherID);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.lnkType);
            this.groupControl1.Location = new System.Drawing.Point(1, -1);
            this.groupControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(850, 177);
            this.groupControl1.TabIndex = 1;
            // 
            // btnInput
            // 
            this.btnInput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInput.Enabled = false;
            this.btnInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInput.Image = global::ERP.Inventory.DUI.Properties.Resources.communication_32;
            this.btnInput.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInput.Location = new System.Drawing.Point(252, 120);
            this.btnInput.Name = "btnInput";
            this.btnInput.Size = new System.Drawing.Size(216, 41);
            this.btnInput.TabIndex = 7;
            this.btnInput.Text = "Nhập đổi/trả hàng";
            this.btnInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInput.Click += new System.EventHandler(this.btnInput_Click);
            // 
            // radApplyTypeError
            // 
            this.radApplyTypeError.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radApplyTypeError.AutoSize = true;
            this.radApplyTypeError.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radApplyTypeError.Location = new System.Drawing.Point(589, 32);
            this.radApplyTypeError.Name = "radApplyTypeError";
            this.radApplyTypeError.Size = new System.Drawing.Size(87, 28);
            this.radApplyTypeError.TabIndex = 3;
            this.radApplyTypeError.Text = "Máy lỗi";
            this.radApplyTypeError.UseVisualStyleBackColor = true;
            // 
            // radApplyTypeNoError
            // 
            this.radApplyTypeNoError.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radApplyTypeNoError.AutoSize = true;
            this.radApplyTypeNoError.Checked = true;
            this.radApplyTypeNoError.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radApplyTypeNoError.Location = new System.Drawing.Point(476, 31);
            this.radApplyTypeNoError.Name = "radApplyTypeNoError";
            this.radApplyTypeNoError.Size = new System.Drawing.Size(108, 28);
            this.radApplyTypeNoError.TabIndex = 2;
            this.radApplyTypeNoError.TabStop = true;
            this.radApplyTypeNoError.Text = "Không lỗi";
            this.radApplyTypeNoError.UseVisualStyleBackColor = true;
            // 
            // cboType
            // 
            this.cboType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.DropDownWidth = 650;
            this.cboType.Enabled = false;
            this.cboType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboType.Location = new System.Drawing.Point(252, 74);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(583, 32);
            this.cboType.TabIndex = 6;
            // 
            // txtOutputVoucherID
            // 
            this.txtOutputVoucherID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtOutputVoucherID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOutputVoucherID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOutputVoucherID.Location = new System.Drawing.Point(252, 31);
            this.txtOutputVoucherID.Name = "txtOutputVoucherID";
            this.txtOutputVoucherID.Size = new System.Drawing.Size(216, 29);
            this.txtOutputVoucherID.TabIndex = 1;
            this.txtOutputVoucherID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOutputVoucherID_KeyPress);
            // 
            // lnkType
            // 
            this.lnkType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lnkType.AutoSize = true;
            this.lnkType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkType.ForeColor = System.Drawing.Color.Blue;
            this.lnkType.LinkArea = new System.Windows.Forms.LinkArea(0, 30);
            this.lnkType.Location = new System.Drawing.Point(27, 78);
            this.lnkType.Name = "lnkType";
            this.lnkType.Size = new System.Drawing.Size(214, 27);
            this.lnkType.TabIndex = 5;
            this.lnkType.TabStop = true;
            this.lnkType.Text = "Lấy yêu cầu được phép:";
            this.lnkType.UseCompatibleTextRendering = true;
            this.lnkType.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkType_LinkClicked);
            // 
            // frmInputChangeOrderInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 177);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmInputChangeOrderInput";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yêu cầu nhập đổi/trả hàng";
            this.Load += new System.EventHandler(this.frmInputChangeOrderInput_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TextBox txtOutputVoucherID;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.Button btnInput;
        private System.Windows.Forms.LinkLabel lnkType;
        private System.Windows.Forms.RadioButton radApplyTypeError;
        private System.Windows.Forms.RadioButton radApplyTypeNoError;
    }
}