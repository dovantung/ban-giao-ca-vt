﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.InputChangeOrder
{
    public partial class frmOTPchange : Form
    {
        public JavaScriptSerializer javascriptslz = new JavaScriptSerializer();
        private ERP.SalesAndServices.SaleOrders.DUI.ApiViettelPlus apiVtplus = new ERP.SalesAndServices.SaleOrders.DUI.ApiViettelPlus();
        private InputChangeOderVtplus vtPlus = new InputChangeOderVtplus();
        public frmOTPchange()
        {
            InitializeComponent();
        }

        private void frmOTPchange_Load(object sender, EventArgs e)
        {
            ERP.SalesAndServices.SaleOrders.DUI.VtPlusUrl.getAllUrlApi();
        }

        public class Token
        {
            static public string strToken;
        }

        private void btnSen_Click(object sender, EventArgs e)
        {
            string str = apiVtplus.getToken(frmInputChangeOrderViettel.Token.strISDN, txtotp.Text.Trim(), frmInputChangeOrderViettel.Token.strTransid.ToString(), "Lấy token cho tiêu điểm đổi trả");
            dynamic objdataTrans = javascriptslz.Deserialize<dynamic>(str);
            if (Convert.ToInt32(objdataTrans["code"]) == 000)
            {
                Token.strToken = objdataTrans["token"];
                this.Close();
            }
            else
            {
                MessageBox.Show(objdataTrans["message"].ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void txtToken_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            frmInputChangeOrderViettel.Token.strTransid = vtPlus.createTransaction(frmInputChangeOrderViettel.Token.strISDN, "Khởi tạo giao dịch - Chức năng: nhập/đổi trả");//tạo giao dịch
            if (frmInputChangeOrderViettel.Token.strTransid == 0)
            {
                MessageBox.Show("Xảy ra lỗi gửi lại otp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
