﻿namespace ERP.Inventory.DUI.InputChangeOrder
{
    partial class frmInputChangeOrderManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInputChangeOrderManager));
            this.grpSearchInfo = new System.Windows.Forms.GroupBox();
            this.cboInputChangeOrderType = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.cboStoreIDList = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label11 = new System.Windows.Forms.Label();
            this.cboSearchType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.txtKeyword = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chkIsDeleted = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.flexData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuContextViewInputChangeOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuContextViewInputChangeOrder_Comment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintInput = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintOutputVoucher = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintOutputStore = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintVoucherOV = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintProductChange = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintWarrantyExt = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintCare = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintSaleOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemCreateInvoiceReturn = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemCreateInvoiceOutput = new System.Windows.Forms.ToolStripMenuItem();
            this.grpSearchInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).BeginInit();
            this.mnuContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpSearchInfo
            // 
            this.grpSearchInfo.Controls.Add(this.cboInputChangeOrderType);
            this.grpSearchInfo.Controls.Add(this.dtpToDate);
            this.grpSearchInfo.Controls.Add(this.dtpFromDate);
            this.grpSearchInfo.Controls.Add(this.cboStoreIDList);
            this.grpSearchInfo.Controls.Add(this.label11);
            this.grpSearchInfo.Controls.Add(this.cboSearchType);
            this.grpSearchInfo.Controls.Add(this.label3);
            this.grpSearchInfo.Controls.Add(this.btnExportExcel);
            this.grpSearchInfo.Controls.Add(this.txtKeyword);
            this.grpSearchInfo.Controls.Add(this.btnSearch);
            this.grpSearchInfo.Controls.Add(this.chkIsDeleted);
            this.grpSearchInfo.Controls.Add(this.label2);
            this.grpSearchInfo.Controls.Add(this.label4);
            this.grpSearchInfo.Controls.Add(this.label1);
            this.grpSearchInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.grpSearchInfo.Location = new System.Drawing.Point(4, 4);
            this.grpSearchInfo.Name = "grpSearchInfo";
            this.grpSearchInfo.Size = new System.Drawing.Size(1006, 80);
            this.grpSearchInfo.TabIndex = 0;
            this.grpSearchInfo.TabStop = false;
            this.grpSearchInfo.Text = "Thông tin tìm kiếm";
            // 
            // cboInputChangeOrderType
            // 
            this.cboInputChangeOrderType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputChangeOrderType.Location = new System.Drawing.Point(474, 18);
            this.cboInputChangeOrderType.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputChangeOrderType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputChangeOrderType.Name = "cboInputChangeOrderType";
            this.cboInputChangeOrderType.Size = new System.Drawing.Size(221, 24);
            this.cboInputChangeOrderType.TabIndex = 2;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(239, 20);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(122, 22);
            this.dtpToDate.TabIndex = 1;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(113, 20);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(122, 22);
            this.dtpFromDate.TabIndex = 0;
            // 
            // cboStoreIDList
            // 
            this.cboStoreIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreIDList.Location = new System.Drawing.Point(769, 18);
            this.cboStoreIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreIDList.Name = "cboStoreIDList";
            this.cboStoreIDList.Size = new System.Drawing.Size(225, 24);
            this.cboStoreIDList.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(701, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 16);
            this.label11.TabIndex = 83;
            this.label11.Text = "Kho nhập:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboSearchType
            // 
            this.cboSearchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchType.DropDownWidth = 250;
            this.cboSearchType.Items.AddRange(new object[] {
            "-- Chọn tìm theo --",
            "Mã phiếu yêu cầu",
            "Mã phiếu xuất cũ",
            "Mã phiếu xuất mới",
            "Mã phiếu nhập mới",
            "SĐT khách hàng",
            "Tên khách hàng",
            "IMEI"});
            this.cboSearchType.Location = new System.Drawing.Point(470, 49);
            this.cboSearchType.Name = "cboSearchType";
            this.cboSearchType.Size = new System.Drawing.Size(225, 24);
            this.cboSearchType.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(376, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 16);
            this.label3.TabIndex = 81;
            this.label3.Text = "Tìm theo:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(901, 48);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(93, 25);
            this.btnExportExcel.TabIndex = 8;
            this.btnExportExcel.Text = "     Xuất Excel";
            this.btnExportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // txtKeyword
            // 
            this.txtKeyword.Location = new System.Drawing.Point(113, 49);
            this.txtKeyword.Name = "txtKeyword";
            this.txtKeyword.Size = new System.Drawing.Size(248, 22);
            this.txtKeyword.TabIndex = 4;
            this.txtKeyword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyword_KeyPress);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(805, 48);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 25);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "   Tìm kiếm";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // chkIsDeleted
            // 
            this.chkIsDeleted.AutoSize = true;
            this.chkIsDeleted.Location = new System.Drawing.Point(705, 53);
            this.chkIsDeleted.Name = "chkIsDeleted";
            this.chkIsDeleted.Size = new System.Drawing.Size(68, 20);
            this.chkIsDeleted.TabIndex = 6;
            this.chkIsDeleted.Text = "Đã hủy";
            this.chkIsDeleted.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 16);
            this.label2.TabIndex = 80;
            this.label2.Text = "Ngày yêu cầu:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(376, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 16);
            this.label4.TabIndex = 79;
            this.label4.Text = "Loại Y/C đổi trả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 16);
            this.label1.TabIndex = 79;
            this.label1.Text = "Chuỗi tìm:";
            // 
            // flexData
            // 
            this.flexData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexData.AllowEditing = false;
            this.flexData.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.FixedOnly;
            this.flexData.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flexData.AutoClipboard = true;
            this.flexData.ColumnInfo = "1,1,0,0,0,95,Columns:0{Width:20;}\t";
            this.flexData.ContextMenuStrip = this.mnuContext;
            this.flexData.Cursor = System.Windows.Forms.Cursors.Default;
            this.flexData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.flexData.Location = new System.Drawing.Point(4, 86);
            this.flexData.Name = "flexData";
            this.flexData.Rows.Count = 1;
            this.flexData.Rows.DefaultSize = 19;
            this.flexData.Size = new System.Drawing.Size(1006, 368);
            this.flexData.StyleInfo = resources.GetString("flexData.StyleInfo");
            this.flexData.TabIndex = 1;
            this.flexData.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            this.flexData.DoubleClick += new System.EventHandler(this.flexData_DoubleClick);
            // 
            // mnuContext
            // 
            this.mnuContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuContextViewInputChangeOrder,
            this.mnuItemCreateInvoiceReturn,
            this.mnuItemCreateInvoiceOutput,
            this.mnuContextViewInputChangeOrder_Comment,
            this.mnuItemPrintInput,
            this.mnuItemPrintOutputVoucher,
            this.mnuItemPrintOutputStore,
            this.mnuItemPrintVoucherOV,
            this.mnuItemPrintProductChange,
            this.mnuItemPrintWarrantyExt,
            this.mnuItemPrintCare,
            this.mnuItemPrintSaleOrder});
            this.mnuContext.Name = "mnuFlex";
            this.mnuContext.Size = new System.Drawing.Size(242, 290);
            this.mnuContext.Opening += new System.ComponentModel.CancelEventHandler(this.mnuContext_Opening);
            // 
            // mnuContextViewInputChangeOrder
            // 
            this.mnuContextViewInputChangeOrder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuContextViewInputChangeOrder.Name = "mnuContextViewInputChangeOrder";
            this.mnuContextViewInputChangeOrder.Size = new System.Drawing.Size(241, 22);
            this.mnuContextViewInputChangeOrder.Text = "Xem thông tin yêu cầu";
            this.mnuContextViewInputChangeOrder.Click += new System.EventHandler(this.mnuContextViewInputChangeOrder_Click);
            // 
            // mnuContextViewInputChangeOrder_Comment
            // 
            this.mnuContextViewInputChangeOrder_Comment.Name = "mnuContextViewInputChangeOrder_Comment";
            this.mnuContextViewInputChangeOrder_Comment.Size = new System.Drawing.Size(241, 22);
            this.mnuContextViewInputChangeOrder_Comment.Text = "Xem bình luận";
            this.mnuContextViewInputChangeOrder_Comment.Click += new System.EventHandler(this.mnuContextViewInputChangeOrder_Comment_Click);
            // 
            // mnuItemPrintInput
            // 
            this.mnuItemPrintInput.Enabled = false;
            this.mnuItemPrintInput.Name = "mnuItemPrintInput";
            this.mnuItemPrintInput.Size = new System.Drawing.Size(241, 22);
            this.mnuItemPrintInput.Text = "In phiếu nhập trả";
            this.mnuItemPrintInput.Click += new System.EventHandler(this.mnuItemPrintInput_Click);
            // 
            // mnuItemPrintOutputVoucher
            // 
            this.mnuItemPrintOutputVoucher.Enabled = false;
            this.mnuItemPrintOutputVoucher.Name = "mnuItemPrintOutputVoucher";
            this.mnuItemPrintOutputVoucher.Size = new System.Drawing.Size(241, 22);
            this.mnuItemPrintOutputVoucher.Text = "In phiếu xuất hàng";
            this.mnuItemPrintOutputVoucher.Click += new System.EventHandler(this.mnuItemPrintOutputVoucher_Click);
            // 
            // mnuItemPrintOutputStore
            // 
            this.mnuItemPrintOutputStore.Enabled = false;
            this.mnuItemPrintOutputStore.Name = "mnuItemPrintOutputStore";
            this.mnuItemPrintOutputStore.Size = new System.Drawing.Size(241, 22);
            this.mnuItemPrintOutputStore.Text = "In phiếu xuất kho hàng hóa";
            this.mnuItemPrintOutputStore.Click += new System.EventHandler(this.mnuItemPrintOutputStore_Click);
            // 
            // mnuItemPrintVoucherOV
            // 
            this.mnuItemPrintVoucherOV.Enabled = false;
            this.mnuItemPrintVoucherOV.Name = "mnuItemPrintVoucherOV";
            this.mnuItemPrintVoucherOV.Size = new System.Drawing.Size(241, 22);
            this.mnuItemPrintVoucherOV.Text = "In phiếu bán hàng";
            this.mnuItemPrintVoucherOV.Click += new System.EventHandler(this.mnuItemPrintVoucherOV_Click);
            // 
            // mnuItemPrintProductChange
            // 
            this.mnuItemPrintProductChange.Enabled = false;
            this.mnuItemPrintProductChange.Name = "mnuItemPrintProductChange";
            this.mnuItemPrintProductChange.Size = new System.Drawing.Size(241, 22);
            this.mnuItemPrintProductChange.Text = "In phiếu xuất đổi imei bảo hành";
            this.mnuItemPrintProductChange.Click += new System.EventHandler(this.mnuItemPrintProductChange_Click);
            // 
            // mnuItemPrintWarrantyExt
            // 
            this.mnuItemPrintWarrantyExt.Enabled = false;
            this.mnuItemPrintWarrantyExt.Name = "mnuItemPrintWarrantyExt";
            this.mnuItemPrintWarrantyExt.Size = new System.Drawing.Size(241, 22);
            this.mnuItemPrintWarrantyExt.Text = "Hợp đồng bảo hành mở rộng";
            this.mnuItemPrintWarrantyExt.Click += new System.EventHandler(this.mnuItemPrintWarrantyExt_Click);
            // 
            // mnuItemPrintCare
            // 
            this.mnuItemPrintCare.Enabled = false;
            this.mnuItemPrintCare.Name = "mnuItemPrintCare";
            this.mnuItemPrintCare.Size = new System.Drawing.Size(241, 22);
            this.mnuItemPrintCare.Text = "Hợp đồng Care+ Diamond";
            this.mnuItemPrintCare.Click += new System.EventHandler(this.mnuItemPrintCare_Click);
            // 
            // mnuItemPrintSaleOrder
            // 
            this.mnuItemPrintSaleOrder.Enabled = false;
            this.mnuItemPrintSaleOrder.Name = "mnuItemPrintSaleOrder";
            this.mnuItemPrintSaleOrder.Size = new System.Drawing.Size(241, 22);
            this.mnuItemPrintSaleOrder.Text = "Xem chứng từ đơn hàng";
            this.mnuItemPrintSaleOrder.Click += new System.EventHandler(this.mnuItemPrintSaleOrder_Click);
            // 
            // mnuItemCreateInvoiceReturn
            // 
            this.mnuItemCreateInvoiceReturn.Enabled = false;
            this.mnuItemCreateInvoiceReturn.Name = "mnuItemCreateInvoiceReturn";
            this.mnuItemCreateInvoiceReturn.Size = new System.Drawing.Size(241, 22);
            this.mnuItemCreateInvoiceReturn.Text = "Tạo hóa đơn trả hàng";
            this.mnuItemCreateInvoiceReturn.Click += new System.EventHandler(this.mnuItemCreateInvoiceReturn_Click);
            // 
            // mnuItemCreateInvoiceOutput
            // 
            this.mnuItemCreateInvoiceOutput.Enabled = false;
            this.mnuItemCreateInvoiceOutput.Name = "mnuItemCreateInvoiceOutput";
            this.mnuItemCreateInvoiceOutput.Size = new System.Drawing.Size(241, 22);
            this.mnuItemCreateInvoiceOutput.Text = "Tạo hóa đơn xuất mới hàng";
            this.mnuItemCreateInvoiceOutput.Click += new System.EventHandler(this.mnuItemCreateInvoiceOutput_Click);
            // 
            // frmInputChangeOrderManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 453);
            this.Controls.Add(this.flexData);
            this.Controls.Add(this.grpSearchInfo);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmInputChangeOrderManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý yêu cầu nhập đổi/trả hàng";
            this.Load += new System.EventHandler(this.frmInputChangeOrderManager_Load);
            this.grpSearchInfo.ResumeLayout(false);
            this.grpSearchInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).EndInit();
            this.mnuContext.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpSearchInfo;
        private System.Windows.Forms.TextBox txtKeyword;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox chkIsDeleted;
        private C1.Win.C1FlexGrid.C1FlexGrid flexData;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboSearchType;
        private System.Windows.Forms.Label label3;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreIDList;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.ContextMenuStrip mnuContext;
        private System.Windows.Forms.ToolStripMenuItem mnuContextViewInputChangeOrder;
        private System.Windows.Forms.ToolStripMenuItem mnuContextViewInputChangeOrder_Comment;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintInput;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintOutputVoucher;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintSaleOrder;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboInputChangeOrderType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintVoucherOV;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintOutputStore;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintProductChange;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintWarrantyExt;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintCare;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCreateInvoiceReturn;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCreateInvoiceOutput;
    }
}