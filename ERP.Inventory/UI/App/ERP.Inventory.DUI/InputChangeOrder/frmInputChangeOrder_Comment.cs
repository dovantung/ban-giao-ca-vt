﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder;
namespace ERP.Inventory.DUI.InputChangeOrder
{
    /// <summary>
    /// Thông tin bình luận yêu cầu nhập đổi/trả hàng
    /// Created by  :   Đặng Minh Hùng
    /// </summary>
    public partial class frmInputChangeOrder_Comment : Form
    {
        #region Variable
        private string strInputChangeOrderID = string.Empty;
        private ERP.Inventory.PLC.InputChangeOrder.PLCInputChangeOrder objPLCInputChangeOrder = new ERP.Inventory.PLC.InputChangeOrder.PLCInputChangeOrder();
        private DataTable dtbComment = null;
        private string strAddCommentFunctionID = string.Empty;
        private string strDeleteCommentFunctionID = string.Empty;
        private string strDelAllConversationFunctionID = string.Empty;
        #endregion
        #region Constructor
        public frmInputChangeOrder_Comment(string strInputChangeOrderID,
            String strAddCommentFunctionID, string strDeleteCommentFunctionID, string strDelAllConversationFunctionID)
        {
            InitializeComponent();
            this.strInputChangeOrderID = strInputChangeOrderID;
            this.strAddCommentFunctionID = strAddCommentFunctionID;
            this.strDeleteCommentFunctionID = strDeleteCommentFunctionID;
            this.strDelAllConversationFunctionID = strDelAllConversationFunctionID;
            mnuCommentDel.Enabled = SystemConfig.objSessionUser.IsPermission(strDeleteCommentFunctionID) || SystemConfig.objSessionUser.IsPermission(strDelAllConversationFunctionID);
        }
        #endregion
        #region Event
        private void frmInputChangeOrder_Comment_Load(object sender, EventArgs e)
        {
            if (!LoadComment())
                btnUpdate.Enabled = false;
            else
                btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(strAddCommentFunctionID);
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (dtbComment == null)
                return;
            if (string.IsNullOrEmpty(txtCommentContent.Text.Trim()))
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn chưa nhập nội dung bình luận");
                txtCommentContent.Focus();
                return;
            }
            bool bolEnableUpdate = btnUpdate.Enabled;
            try
            {
                btnUpdate.Enabled = false;
                DataRow row = dtbComment.NewRow();
                row["CommentDate"] = Globals.GetServerDateTime();
                row["CommentContent"] = txtCommentContent.Text.Trim();
                row["CommentUserFullName"] = SystemConfig.objSessionUser.UserName + " - " + SystemConfig.objSessionUser.FullName;
                dtbComment.Rows.InsertAt(row, 0);
                flexData.AutoSizeRows();
                flexData.Refresh();
                InputChangeOrder_Comment objInputChangeOrder_Comment = new InputChangeOrder_Comment();
                objInputChangeOrder_Comment.CommentTitle = row["CommentUserFullName"].ToString() + " đã gởi bình luận";
                objInputChangeOrder_Comment.CommentContent = row["CommentContent"].ToString();
                objInputChangeOrder_Comment.CommentUser = SystemConfig.objSessionUser.UserName;
                objInputChangeOrder_Comment.IsActive = true;
                objInputChangeOrder_Comment.InputChangeOrderID = strInputChangeOrderID;
                string strCommentID = string.Empty;
                objPLCInputChangeOrder.InsertComment(ref strCommentID, objInputChangeOrder_Comment);
                row["CommentID"] = strCommentID;
                flexData.Select(flexData.Rows.Fixed, flexData.Cols["CommentContent"].Index);
                txtCommentContent.Text = string.Empty;
                txtCommentContent.Focus();
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi thêm thông tin bình luận yêu cầu nhập đổi/trả hàng", objExc, DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi thêm thông tin bình luận yêu cầu nhập đổi/trả hàng");
            }
            finally
            {
                btnUpdate.Enabled = bolEnableUpdate;
            }
        }
        private void flexData_Resize(object sender, EventArgs e)
        {
            flexData.Cols["CommentContent"].Width = flexData.Width - flexData.Cols["CommentDate"].Width - flexData.Cols["CommentUserFullName"].Width - 30;
        }
        private void flexData_MouseEnterCell(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < flexData.Rows.Fixed)
                return;
            toolTip1.SetToolTip(flexData, string.Empty);
            if (e.Col == flexData.Cols["CommentContent"].Index)
            {
                string strNote = Convert.ToString(flexData[e.Row, "CommentContent"]).Trim();
                toolTip1.SetToolTip(flexData, strNote);
            }
        }
        private void mnuComment_Opening(object sender, CancelEventArgs e)
        {
            mnuCommentDel.Enabled = false;
            if (flexData.Rows.Count <= flexData.Rows.Fixed || flexData.RowSel < flexData.Rows.Fixed)
                return;
            if (Convert.ToString(flexData[flexData.RowSel, "CommentUser"]).Trim() == SystemConfig.objSessionUser.UserName)
                mnuCommentDel.Enabled = SystemConfig.objSessionUser.IsPermission(strDeleteCommentFunctionID) || SystemConfig.objSessionUser.IsPermission(strDelAllConversationFunctionID);
            else
                mnuCommentDel.Enabled = SystemConfig.objSessionUser.IsPermission(strDelAllConversationFunctionID);
        }

        #endregion
        #region Function
        private bool LoadComment()
        {
            try
            {
                dtbComment = objPLCInputChangeOrder.GetComment(new object[] { "@InputChangeOrderID", strInputChangeOrderID });
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                    return false;
                }
                flexData.DataSource = dtbComment;
                FormatFlex(flexData);
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo lưới thông tin bình luận", objExce, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private void FormatFlex(C1.Win.C1FlexGrid.C1FlexGrid _flex)
        {
            for (int i = 0; i < _flex.Cols.Count; i++)
            {
                _flex.Cols[i].Visible = false;
                _flex.Cols[i].AllowSorting = false;
                _flex.Cols[i].AllowDragging = false;
                _flex.Cols[i].AllowEditing = false;
            }
            C1FlexGridObject.SetColumnVisible(_flex, true, "CommentDate,CommentContent,CommentUserFullName");
            C1FlexGridObject.SetColumnCaption(_flex, "CommentDate,Ngày bình luận,CommentContent,Nội dung,CommentUserFullName,Người bình luận");
            C1FlexGridObject.SetColumnWidth(_flex, "CommentDate,120,CommentContent,550,CommentUserFullName,200");
            _flex.Cols["CommentDate"].Format = "dd/MM/yyyy HH:mm";
            C1.Win.C1FlexGrid.CellStyle style = _flex.Styles.Add("flexStyle");
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = _flex.GetCellRange(0, 0, 0, _flex.Cols.Count - 1);
            range.Style = style;
            _flex.Rows[0].Height = 30;
            _flex.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            _flex.AutoSizeRows();
        }
        private void mnuCommentDel_Click(object sender, EventArgs e)
        {
            if (flexData.RowSel < flexData.Rows.Fixed)
                return;
            string strCommentID = flexData[flexData.RowSel, "CommentID"].ToString().Trim();
            if (!string.IsNullOrEmpty(strCommentID))
            {
                InputChangeOrder_Comment objInputChangeOrder_Comment = new InputChangeOrder_Comment();
                objInputChangeOrder_Comment.CommentID = strCommentID;
                objInputChangeOrder_Comment.DeletedUser = SystemConfig.objSessionUser.UserName;
                objPLCInputChangeOrder.DeleteComment(objInputChangeOrder_Comment);
            }
            flexData.Rows.Remove(flexData.RowSel);
            flexData.Refresh();
        }
        #endregion
    }
}
