﻿namespace ERP.Inventory.DUI.InputChangeOrder
{
    partial class frmSelectOV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grvData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOutputVoucherID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceSysbol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserOutput = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputContent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // grdData
            // 
            this.grdData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdData.Location = new System.Drawing.Point(0, 0);
            this.grdData.MainView = this.grvData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemCheckEdit1});
            this.grdData.Size = new System.Drawing.Size(924, 318);
            this.grdData.TabIndex = 2;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvData});
            this.grdData.DoubleClick += new System.EventHandler(this.grdData_DoubleClick);
            // 
            // grvData
            // 
            this.grvData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.FocusedRow.Options.UseFont = true;
            this.grvData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grvData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.Preview.Options.UseFont = true;
            this.grvData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.Row.Options.UseFont = true;
            this.grvData.ColumnPanelRowHeight = 30;
            this.grvData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOutputVoucherID,
            this.colOrderID,
            this.colCustomer,
            this.colInvoiceID,
            this.colInvoiceSysbol,
            this.colInvoiceDate,
            this.colOutputDate,
            this.colStore,
            this.colUserOutput,
            this.colStaffUser,
            this.colDiscount,
            this.colTotalVAT,
            this.colTotalAmount,
            this.colOutputContent});
            this.grvData.GridControl = this.grdData;
            this.grvData.Name = "grvData";
            this.grvData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvData.OptionsView.ColumnAutoWidth = false;
            this.grvData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvData.OptionsView.ShowFooter = true;
            this.grvData.OptionsView.ShowGroupPanel = false;
            // 
            // colOutputVoucherID
            // 
            this.colOutputVoucherID.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputVoucherID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputVoucherID.Caption = "Mã phiếu xuất";
            this.colOutputVoucherID.FieldName = "OUTPUTVOUCHERID";
            this.colOutputVoucherID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colOutputVoucherID.Name = "colOutputVoucherID";
            this.colOutputVoucherID.OptionsColumn.AllowEdit = false;
            this.colOutputVoucherID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colOutputVoucherID.OptionsColumn.ReadOnly = true;
            this.colOutputVoucherID.Visible = true;
            this.colOutputVoucherID.VisibleIndex = 0;
            this.colOutputVoucherID.Width = 150;
            // 
            // colOrderID
            // 
            this.colOrderID.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrderID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrderID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOrderID.Caption = "Mã đơn hàng";
            this.colOrderID.FieldName = "ORDERID";
            this.colOrderID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colOrderID.Name = "colOrderID";
            this.colOrderID.OptionsColumn.AllowEdit = false;
            this.colOrderID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colOrderID.OptionsColumn.ReadOnly = true;
            this.colOrderID.Visible = true;
            this.colOrderID.VisibleIndex = 1;
            this.colOrderID.Width = 150;
            // 
            // colCustomer
            // 
            this.colCustomer.AppearanceHeader.Options.UseTextOptions = true;
            this.colCustomer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCustomer.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCustomer.Caption = "Khách hàng";
            this.colCustomer.FieldName = "CUSTOMERNAME";
            this.colCustomer.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCustomer.Name = "colCustomer";
            this.colCustomer.OptionsColumn.AllowEdit = false;
            this.colCustomer.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colCustomer.OptionsColumn.ReadOnly = true;
            this.colCustomer.Visible = true;
            this.colCustomer.VisibleIndex = 2;
            this.colCustomer.Width = 150;
            // 
            // colInvoiceID
            // 
            this.colInvoiceID.AppearanceHeader.Options.UseTextOptions = true;
            this.colInvoiceID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInvoiceID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInvoiceID.Caption = "Số hóa đơn";
            this.colInvoiceID.FieldName = "INVOICEID";
            this.colInvoiceID.Name = "colInvoiceID";
            this.colInvoiceID.OptionsColumn.AllowEdit = false;
            this.colInvoiceID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colInvoiceID.OptionsColumn.ReadOnly = true;
            this.colInvoiceID.Visible = true;
            this.colInvoiceID.VisibleIndex = 3;
            this.colInvoiceID.Width = 80;
            // 
            // colInvoiceSysbol
            // 
            this.colInvoiceSysbol.AppearanceHeader.Options.UseTextOptions = true;
            this.colInvoiceSysbol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInvoiceSysbol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInvoiceSysbol.Caption = "Ký hiệu HĐ";
            this.colInvoiceSysbol.FieldName = "INVOICESYMBOL";
            this.colInvoiceSysbol.Name = "colInvoiceSysbol";
            this.colInvoiceSysbol.OptionsColumn.AllowEdit = false;
            this.colInvoiceSysbol.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colInvoiceSysbol.OptionsColumn.ReadOnly = true;
            this.colInvoiceSysbol.Visible = true;
            this.colInvoiceSysbol.VisibleIndex = 4;
            this.colInvoiceSysbol.Width = 80;
            // 
            // colInvoiceDate
            // 
            this.colInvoiceDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colInvoiceDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInvoiceDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInvoiceDate.Caption = "Ngày HĐ";
            this.colInvoiceDate.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colInvoiceDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colInvoiceDate.FieldName = "INVOICEDATE";
            this.colInvoiceDate.Name = "colInvoiceDate";
            this.colInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colInvoiceDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colInvoiceDate.Visible = true;
            this.colInvoiceDate.VisibleIndex = 5;
            this.colInvoiceDate.Width = 80;
            // 
            // colOutputDate
            // 
            this.colOutputDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutputDate.Caption = "Ngày xuất";
            this.colOutputDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colOutputDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colOutputDate.FieldName = "OUTPUTDATE";
            this.colOutputDate.Name = "colOutputDate";
            this.colOutputDate.OptionsColumn.AllowEdit = false;
            this.colOutputDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colOutputDate.OptionsColumn.ReadOnly = true;
            this.colOutputDate.Visible = true;
            this.colOutputDate.VisibleIndex = 6;
            this.colOutputDate.Width = 80;
            // 
            // colStore
            // 
            this.colStore.AppearanceHeader.Options.UseTextOptions = true;
            this.colStore.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStore.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStore.Caption = "Kho xuất";
            this.colStore.FieldName = "STORENAME";
            this.colStore.Name = "colStore";
            this.colStore.OptionsColumn.AllowEdit = false;
            this.colStore.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colStore.OptionsColumn.ReadOnly = true;
            this.colStore.Visible = true;
            this.colStore.VisibleIndex = 7;
            this.colStore.Width = 130;
            // 
            // colUserOutput
            // 
            this.colUserOutput.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserOutput.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserOutput.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colUserOutput.Caption = "Nhân viên xuất";
            this.colUserOutput.FieldName = "OUTPUTUSERFULLNAME";
            this.colUserOutput.Name = "colUserOutput";
            this.colUserOutput.OptionsColumn.AllowEdit = false;
            this.colUserOutput.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colUserOutput.OptionsColumn.ReadOnly = true;
            this.colUserOutput.Visible = true;
            this.colUserOutput.VisibleIndex = 8;
            this.colUserOutput.Width = 130;
            // 
            // colStaffUser
            // 
            this.colStaffUser.AppearanceHeader.Options.UseTextOptions = true;
            this.colStaffUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStaffUser.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStaffUser.Caption = "Nhân viên bán hàng";
            this.colStaffUser.FieldName = "STAFFUSERFULLNAME";
            this.colStaffUser.Name = "colStaffUser";
            this.colStaffUser.OptionsColumn.AllowEdit = false;
            this.colStaffUser.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colStaffUser.OptionsColumn.ReadOnly = true;
            this.colStaffUser.Visible = true;
            this.colStaffUser.VisibleIndex = 9;
            this.colStaffUser.Width = 130;
            // 
            // colDiscount
            // 
            this.colDiscount.AppearanceHeader.Options.UseTextOptions = true;
            this.colDiscount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiscount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDiscount.Caption = "Giảm giá";
            this.colDiscount.DisplayFormat.FormatString = "#,##0";
            this.colDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDiscount.FieldName = "DISCOUNT";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.OptionsColumn.AllowEdit = false;
            this.colDiscount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colDiscount.OptionsColumn.ReadOnly = true;
            this.colDiscount.Visible = true;
            this.colDiscount.VisibleIndex = 10;
            this.colDiscount.Width = 100;
            // 
            // colTotalVAT
            // 
            this.colTotalVAT.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalVAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalVAT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalVAT.Caption = "Tổng tiền VAT";
            this.colTotalVAT.DisplayFormat.FormatString = "n2";
            this.colTotalVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalVAT.FieldName = "TOTALVAT";
            this.colTotalVAT.Name = "colTotalVAT";
            this.colTotalVAT.OptionsColumn.AllowEdit = false;
            this.colTotalVAT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colTotalVAT.OptionsColumn.ReadOnly = true;
            this.colTotalVAT.Visible = true;
            this.colTotalVAT.VisibleIndex = 11;
            this.colTotalVAT.Width = 120;
            // 
            // colTotalAmount
            // 
            this.colTotalAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalAmount.Caption = "Tổng tiền";
            this.colTotalAmount.DisplayFormat.FormatString = "#,##0";
            this.colTotalAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalAmount.FieldName = "TOTALAMOUNT";
            this.colTotalAmount.Name = "colTotalAmount";
            this.colTotalAmount.OptionsColumn.AllowEdit = false;
            this.colTotalAmount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colTotalAmount.OptionsColumn.ReadOnly = true;
            this.colTotalAmount.Visible = true;
            this.colTotalAmount.VisibleIndex = 12;
            this.colTotalAmount.Width = 120;
            // 
            // colOutputContent
            // 
            this.colOutputContent.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputContent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputContent.Caption = "Nội dung";
            this.colOutputContent.FieldName = "OUTPUTCONTENT";
            this.colOutputContent.Name = "colOutputContent";
            this.colOutputContent.OptionsColumn.AllowEdit = false;
            this.colOutputContent.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colOutputContent.OptionsColumn.ReadOnly = true;
            this.colOutputContent.Visible = true;
            this.colOutputContent.VisibleIndex = 13;
            this.colOutputContent.Width = 400;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.EditFormat.FormatString = "N4";
            this.repositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.Mask.EditMask = "##,###,###,###.##";
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit1.ValueUnchecked = ((short)(0));
            // 
            // frmSelectOV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 317);
            this.Controls.Add(this.grdData);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmSelectOV";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn phiếu xuất";
            this.Load += new System.EventHandler(this.frmSelectOV_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grvData;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputVoucherID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceSysbol;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStore;
        private DevExpress.XtraGrid.Columns.GridColumn colUserOutput;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffUser;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputContent;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
    }
}