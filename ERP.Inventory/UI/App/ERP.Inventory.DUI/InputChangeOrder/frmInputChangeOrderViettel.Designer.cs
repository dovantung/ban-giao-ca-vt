﻿namespace ERP.Inventory.DUI.InputChangeOrder
{
    partial class frmInputChangeOrderViettel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInputChangeOrderViettel));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageOutputVoucher = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblInvoiceNo = new System.Windows.Forms.LinkLabel();
            this.txtInVoiceID = new System.Windows.Forms.TextBox();
            this.txtDenominator = new System.Windows.Forms.TextBox();
            this.txtInvoiceSymbol = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lblInvoiceSymbol = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtReasonNoteOV = new System.Windows.Forms.TextBox();
            this.cboReasonOV = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.lnkCustomer = new System.Windows.Forms.LinkLabel();
            this.cboInputTypeID = new Library.AppControl.ComboBoxControl.ComboBoxInputType();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.txtCustomerPhone = new System.Windows.Forms.TextBox();
            this.txtStaffUser = new System.Windows.Forms.TextBox();
            this.txtTaxID = new System.Windows.Forms.TextBox();
            this.ctrlStaffUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.txtCustomerAddress = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtCustomerIDCard = new System.Windows.Forms.TextBox();
            this.txtContentIOT = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtContentOV = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lblOutputContent = new System.Windows.Forms.Label();
            this.lblStore = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.dtOutputDate = new System.Windows.Forms.DateTimePicker();
            this.lblOutputDate = new System.Windows.Forms.Label();
            this.lblTaxID = new System.Windows.Forms.Label();
            this.txtOutputVoucherID = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblOutputVoucherID = new System.Windows.Forms.Label();
            this.txtInputTypeName = new System.Windows.Forms.TextBox();
            this.txtStoreName = new System.Windows.Forms.TextBox();
            this.lblInputType = new System.Windows.Forms.Label();
            this.chkCreateSaleOrder = new System.Windows.Forms.CheckBox();
            this.lblTotalLiquidate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPageInputChangeOrder = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dtpInputchangeDate = new DevExpress.XtraEditors.DateEdit();
            this.txtDenominatorReturn = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtInvoiceSymbolReturn = new System.Windows.Forms.TextBox();
            this.txtInvoiceIdReturn = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtOutVoucherID = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtNewInputVoucherID = new System.Windows.Forms.TextBox();
            this.chkIsInputchange = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDenominatorOutput = new System.Windows.Forms.TextBox();
            this.txtInvoiceSymbolOutput = new System.Windows.Forms.TextBox();
            this.txtInvoiceIdOut = new System.Windows.Forms.TextBox();
            this.dtpReviewedDate = new DevExpress.XtraEditors.DateEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtInVoucherID = new System.Windows.Forms.TextBox();
            this.chkIsReviewed = new System.Windows.Forms.CheckBox();
            this.txtNewOutputVoucherID = new System.Windows.Forms.TextBox();
            this.chkApplyErrorType = new System.Windows.Forms.CheckBox();
            this.txtReasonNote = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtInputChangeOrderID = new System.Windows.Forms.TextBox();
            this.txtSaleOrderID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCreateUser = new System.Windows.Forms.TextBox();
            this.txtInputChangeOrderStoreName = new System.Windows.Forms.TextBox();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.cboInputChangeOrderStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.dtpInputChangeOrderDate = new System.Windows.Forms.DateTimePicker();
            this.ucCreateUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.label20 = new System.Windows.Forms.Label();
            this.lnkSaleOrderID = new System.Windows.Forms.LinkLabel();
            this.label23 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblContent = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cboReason = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.tabPageReviewList = new System.Windows.Forms.TabPage();
            this.flexReviewLevel = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.tabPageWorkflow = new System.Windows.Forms.TabPage();
            this.flexWorkFlow = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuWorkFlow = new System.Windows.Forms.ContextMenuStrip();
            this.mnuWorkFlow_IsProcess = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPageAttach = new System.Windows.Forms.TabPage();
            this.grdAttachment = new DevExpress.XtraGrid.GridControl();
            this.mnuAttachment = new System.Windows.Forms.ContextMenuStrip();
            this.mnuAddAttachment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelAttachment = new System.Windows.Forms.ToolStripMenuItem();
            this.grvAttachment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDownload = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDownload = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblInstruction = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.flexDetail = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.btnCreateConcern = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList();
            this.btnReviewList = new DevExpress.XtraEditors.DropDownButton();
            this.pMnu = new DevExpress.XtraBars.PopupMenu();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.lblReviewStatus = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip();
            this.txtReturnAmount = new System.Windows.Forms.TextBox();
            this.tmrCloseForm = new System.Windows.Forms.Timer();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblTitlePayment = new System.Windows.Forms.Label();
            this.btnPrint = new DevExpress.XtraEditors.DropDownButton();
            this.pMnuPrint = new DevExpress.XtraBars.PopupMenu();
            this.btnSupportCare = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolTip2 = new System.Windows.Forms.ToolTip();
            this.toolTip3 = new System.Windows.Forms.ToolTip();
            this.toolTip4 = new System.Windows.Forms.ToolTip();
            this.tabControl.SuspendLayout();
            this.tabPageOutputVoucher.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPageInputChangeOrder.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInputchangeDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInputchangeDate.Properties)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpReviewedDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpReviewedDate.Properties)).BeginInit();
            this.tabPageReviewList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexReviewLevel)).BeginInit();
            this.tabPageWorkflow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexWorkFlow)).BeginInit();
            this.mnuWorkFlow.SuspendLayout();
            this.tabPageAttach.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).BeginInit();
            this.mnuAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuPrint)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageOutputVoucher);
            this.tabControl.Controls.Add(this.tabPageInputChangeOrder);
            this.tabControl.Controls.Add(this.tabPageReviewList);
            this.tabControl.Controls.Add(this.tabPageWorkflow);
            this.tabControl.Controls.Add(this.tabPageAttach);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1061, 405);
            this.tabControl.TabIndex = 5;
            // 
            // tabPageOutputVoucher
            // 
            this.tabPageOutputVoucher.BackColor = System.Drawing.SystemColors.Window;
            this.tabPageOutputVoucher.Controls.Add(this.groupBox1);
            this.tabPageOutputVoucher.Location = new System.Drawing.Point(4, 25);
            this.tabPageOutputVoucher.Name = "tabPageOutputVoucher";
            this.tabPageOutputVoucher.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOutputVoucher.Size = new System.Drawing.Size(1053, 376);
            this.tabPageOutputVoucher.TabIndex = 0;
            this.tabPageOutputVoucher.Text = "Thông tin phiếu xuất";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtReasonNoteOV);
            this.groupBox1.Controls.Add(this.cboReasonOV);
            this.groupBox1.Controls.Add(this.lnkCustomer);
            this.groupBox1.Controls.Add(this.cboInputTypeID);
            this.groupBox1.Controls.Add(this.txtCustomerID);
            this.groupBox1.Controls.Add(this.cboStore);
            this.groupBox1.Controls.Add(this.txtCustomerPhone);
            this.groupBox1.Controls.Add(this.txtStaffUser);
            this.groupBox1.Controls.Add(this.txtTaxID);
            this.groupBox1.Controls.Add(this.ctrlStaffUser);
            this.groupBox1.Controls.Add(this.txtCustomerAddress);
            this.groupBox1.Controls.Add(this.txtCustomerName);
            this.groupBox1.Controls.Add(this.txtCustomerIDCard);
            this.groupBox1.Controls.Add(this.txtContentIOT);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtContentOV);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.lblOutputContent);
            this.groupBox1.Controls.Add(this.lblStore);
            this.groupBox1.Controls.Add(this.lblPhone);
            this.groupBox1.Controls.Add(this.dtOutputDate);
            this.groupBox1.Controls.Add(this.lblOutputDate);
            this.groupBox1.Controls.Add(this.lblTaxID);
            this.groupBox1.Controls.Add(this.txtOutputVoucherID);
            this.groupBox1.Controls.Add(this.lblAddress);
            this.groupBox1.Controls.Add(this.lblOutputVoucherID);
            this.groupBox1.Controls.Add(this.txtInputTypeName);
            this.groupBox1.Controls.Add(this.txtStoreName);
            this.groupBox1.Controls.Add(this.lblInputType);
            this.groupBox1.Controls.Add(this.chkCreateSaleOrder);
            this.groupBox1.Controls.Add(this.lblTotalLiquidate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(3, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(978, 374);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblInvoiceNo);
            this.groupBox3.Controls.Add(this.txtInVoiceID);
            this.groupBox3.Controls.Add(this.txtDenominator);
            this.groupBox3.Controls.Add(this.txtInvoiceSymbol);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.lblInvoiceSymbol);
            this.groupBox3.Location = new System.Drawing.Point(606, 121);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(342, 98);
            this.groupBox3.TabIndex = 40;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Hóa đơn bán hàng";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.AutoSize = true;
            this.lblInvoiceNo.Location = new System.Drawing.Point(1, 20);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(80, 16);
            this.lblInvoiceNo.TabIndex = 41;
            this.lblInvoiceNo.TabStop = true;
            this.lblInvoiceNo.Text = "Số hóa đơn:";
            this.lblInvoiceNo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblInvoiceNo_LinkClicked);
            // 
            // txtInVoiceID
            // 
            this.txtInVoiceID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInVoiceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtInVoiceID.ForeColor = System.Drawing.Color.Red;
            this.txtInVoiceID.Location = new System.Drawing.Point(105, 16);
            this.txtInVoiceID.MaxLength = 20;
            this.txtInVoiceID.Name = "txtInVoiceID";
            this.txtInVoiceID.ReadOnly = true;
            this.txtInVoiceID.Size = new System.Drawing.Size(229, 22);
            this.txtInVoiceID.TabIndex = 24;
            // 
            // txtDenominator
            // 
            this.txtDenominator.BackColor = System.Drawing.SystemColors.Info;
            this.txtDenominator.Location = new System.Drawing.Point(105, 71);
            this.txtDenominator.MaxLength = 20;
            this.txtDenominator.Name = "txtDenominator";
            this.txtDenominator.ReadOnly = true;
            this.txtDenominator.Size = new System.Drawing.Size(229, 22);
            this.txtDenominator.TabIndex = 27;
            // 
            // txtInvoiceSymbol
            // 
            this.txtInvoiceSymbol.BackColor = System.Drawing.SystemColors.Info;
            this.txtInvoiceSymbol.Location = new System.Drawing.Point(105, 43);
            this.txtInvoiceSymbol.MaxLength = 20;
            this.txtInvoiceSymbol.Name = "txtInvoiceSymbol";
            this.txtInvoiceSymbol.ReadOnly = true;
            this.txtInvoiceSymbol.Size = new System.Drawing.Size(229, 22);
            this.txtInvoiceSymbol.TabIndex = 28;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 16);
            this.label13.TabIndex = 25;
            this.label13.Text = "Mẫu số:";
            // 
            // lblInvoiceSymbol
            // 
            this.lblInvoiceSymbol.AutoSize = true;
            this.lblInvoiceSymbol.Location = new System.Drawing.Point(1, 46);
            this.lblInvoiceSymbol.Name = "lblInvoiceSymbol";
            this.lblInvoiceSymbol.Size = new System.Drawing.Size(76, 16);
            this.lblInvoiceSymbol.TabIndex = 26;
            this.lblInvoiceSymbol.Text = "Ký hiệu HĐ:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(607, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Nhân viên xuất: ";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(491, 288);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 67);
            this.label4.TabIndex = 35;
            this.label4.Text = "Ghi chú lý do đổi trả:";
            // 
            // txtReasonNoteOV
            // 
            this.txtReasonNoteOV.Location = new System.Drawing.Point(606, 288);
            this.txtReasonNoteOV.MaxLength = 300;
            this.txtReasonNoteOV.Multiline = true;
            this.txtReasonNoteOV.Name = "txtReasonNoteOV";
            this.txtReasonNoteOV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReasonNoteOV.Size = new System.Drawing.Size(334, 67);
            this.txtReasonNoteOV.TabIndex = 37;
            // 
            // cboReasonOV
            // 
            this.cboReasonOV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboReasonOV.Location = new System.Drawing.Point(606, 261);
            this.cboReasonOV.Margin = new System.Windows.Forms.Padding(0);
            this.cboReasonOV.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboReasonOV.Name = "cboReasonOV";
            this.cboReasonOV.Size = new System.Drawing.Size(334, 24);
            this.cboReasonOV.TabIndex = 34;
            // 
            // lnkCustomer
            // 
            this.lnkCustomer.AutoSize = true;
            this.lnkCustomer.Location = new System.Drawing.Point(7, 16);
            this.lnkCustomer.Name = "lnkCustomer";
            this.lnkCustomer.Size = new System.Drawing.Size(81, 16);
            this.lnkCustomer.TabIndex = 0;
            this.lnkCustomer.TabStop = true;
            this.lnkCustomer.Text = "Khách hàng:";
            this.lnkCustomer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkCustomer_LinkClicked);
            // 
            // cboInputTypeID
            // 
            this.cboInputTypeID.Enabled = false;
            this.cboInputTypeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputTypeID.Location = new System.Drawing.Point(606, 230);
            this.cboInputTypeID.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputTypeID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputTypeID.Name = "cboInputTypeID";
            this.cboInputTypeID.Size = new System.Drawing.Size(334, 24);
            this.cboInputTypeID.TabIndex = 28;
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerID.Location = new System.Drawing.Point(135, 14);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.ReadOnly = true;
            this.txtCustomerID.Size = new System.Drawing.Size(449, 22);
            this.txtCustomerID.TabIndex = 1;
            this.txtCustomerID.TabStop = false;
            // 
            // cboStore
            // 
            this.cboStore.Enabled = false;
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(134, 222);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(331, 24);
            this.cboStore.TabIndex = 26;
            this.cboStore.SelectionChangeCommitted += new System.EventHandler(this.cboStore_SelectionChangeCommitted);
            // 
            // txtCustomerPhone
            // 
            this.txtCustomerPhone.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerPhone.Location = new System.Drawing.Point(711, 40);
            this.txtCustomerPhone.MaxLength = 9;
            this.txtCustomerPhone.Name = "txtCustomerPhone";
            this.txtCustomerPhone.ReadOnly = true;
            this.txtCustomerPhone.Size = new System.Drawing.Size(229, 22);
            this.txtCustomerPhone.TabIndex = 7;
            // 
            // txtStaffUser
            // 
            this.txtStaffUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtStaffUser.Location = new System.Drawing.Point(711, 96);
            this.txtStaffUser.MaxLength = 20;
            this.txtStaffUser.Name = "txtStaffUser";
            this.txtStaffUser.ReadOnly = true;
            this.txtStaffUser.Size = new System.Drawing.Size(229, 22);
            this.txtStaffUser.TabIndex = 13;
            // 
            // txtTaxID
            // 
            this.txtTaxID.BackColor = System.Drawing.SystemColors.Info;
            this.txtTaxID.Location = new System.Drawing.Point(711, 68);
            this.txtTaxID.MaxLength = 20;
            this.txtTaxID.Name = "txtTaxID";
            this.txtTaxID.ReadOnly = true;
            this.txtTaxID.Size = new System.Drawing.Size(229, 22);
            this.txtTaxID.TabIndex = 11;
            // 
            // ctrlStaffUser
            // 
            this.ctrlStaffUser.Enabled = false;
            this.ctrlStaffUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrlStaffUser.IsOnlyShowRealStaff = false;
            this.ctrlStaffUser.IsValidate = true;
            this.ctrlStaffUser.Location = new System.Drawing.Point(849, 96);
            this.ctrlStaffUser.Margin = new System.Windows.Forms.Padding(4);
            this.ctrlStaffUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ctrlStaffUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ctrlStaffUser.Name = "ctrlStaffUser";
            this.ctrlStaffUser.Size = new System.Drawing.Size(91, 22);
            this.ctrlStaffUser.TabIndex = 14;
            this.ctrlStaffUser.UserName = "";
            this.ctrlStaffUser.Visible = false;
            this.ctrlStaffUser.WorkingPositionID = 0;
            // 
            // txtCustomerAddress
            // 
            this.txtCustomerAddress.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerAddress.Location = new System.Drawing.Point(135, 68);
            this.txtCustomerAddress.MaxLength = 400;
            this.txtCustomerAddress.Multiline = true;
            this.txtCustomerAddress.Name = "txtCustomerAddress";
            this.txtCustomerAddress.ReadOnly = true;
            this.txtCustomerAddress.Size = new System.Drawing.Size(449, 52);
            this.txtCustomerAddress.TabIndex = 9;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerName.Location = new System.Drawing.Point(135, 40);
            this.txtCustomerName.MaxLength = 200;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.ReadOnly = true;
            this.txtCustomerName.Size = new System.Drawing.Size(449, 22);
            this.txtCustomerName.TabIndex = 5;
            // 
            // txtCustomerIDCard
            // 
            this.txtCustomerIDCard.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerIDCard.Location = new System.Drawing.Point(711, 14);
            this.txtCustomerIDCard.MaxLength = 9;
            this.txtCustomerIDCard.Name = "txtCustomerIDCard";
            this.txtCustomerIDCard.ReadOnly = true;
            this.txtCustomerIDCard.Size = new System.Drawing.Size(229, 22);
            this.txtCustomerIDCard.TabIndex = 3;
            // 
            // txtContentIOT
            // 
            this.txtContentIOT.Location = new System.Drawing.Point(134, 251);
            this.txtContentIOT.MaxLength = 200;
            this.txtContentIOT.Multiline = true;
            this.txtContentIOT.Name = "txtContentIOT";
            this.txtContentIOT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContentIOT.Size = new System.Drawing.Size(331, 94);
            this.txtContentIOT.TabIndex = 31;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(607, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 16);
            this.label19.TabIndex = 2;
            this.label19.Text = "Số CMND:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(7, 251);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 76);
            this.label1.TabIndex = 30;
            this.label1.Text = "Nội dung yêu cầu nhập đổi trả:";
            // 
            // txtContentOV
            // 
            this.txtContentOV.BackColor = System.Drawing.SystemColors.Info;
            this.txtContentOV.Location = new System.Drawing.Point(135, 152);
            this.txtContentOV.MaxLength = 200;
            this.txtContentOV.Multiline = true;
            this.txtContentOV.Name = "txtContentOV";
            this.txtContentOV.ReadOnly = true;
            this.txtContentOV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContentOV.Size = new System.Drawing.Size(449, 64);
            this.txtContentOV.TabIndex = 24;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 43);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 16);
            this.label22.TabIndex = 4;
            this.label22.Text = "Họ tên KH:";
            // 
            // lblOutputContent
            // 
            this.lblOutputContent.AutoSize = true;
            this.lblOutputContent.Location = new System.Drawing.Point(7, 155);
            this.lblOutputContent.Name = "lblOutputContent";
            this.lblOutputContent.Size = new System.Drawing.Size(128, 16);
            this.lblOutputContent.TabIndex = 23;
            this.lblOutputContent.Text = "Nội dung phiếu xuất:";
            // 
            // lblStore
            // 
            this.lblStore.AutoSize = true;
            this.lblStore.Location = new System.Drawing.Point(7, 225);
            this.lblStore.Name = "lblStore";
            this.lblStore.Size = new System.Drawing.Size(67, 16);
            this.lblStore.TabIndex = 25;
            this.lblStore.Text = "Kho nhập:";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(607, 46);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(70, 16);
            this.lblPhone.TabIndex = 6;
            this.lblPhone.Text = "Điện thoại:";
            // 
            // dtOutputDate
            // 
            this.dtOutputDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtOutputDate.Enabled = false;
            this.dtOutputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtOutputDate.Location = new System.Drawing.Point(440, 125);
            this.dtOutputDate.Name = "dtOutputDate";
            this.dtOutputDate.Size = new System.Drawing.Size(144, 22);
            this.dtOutputDate.TabIndex = 18;
            // 
            // lblOutputDate
            // 
            this.lblOutputDate.AutoSize = true;
            this.lblOutputDate.Location = new System.Drawing.Point(370, 127);
            this.lblOutputDate.Name = "lblOutputDate";
            this.lblOutputDate.Size = new System.Drawing.Size(71, 16);
            this.lblOutputDate.TabIndex = 17;
            this.lblOutputDate.Text = "Ngày xuất:";
            // 
            // lblTaxID
            // 
            this.lblTaxID.AutoSize = true;
            this.lblTaxID.Location = new System.Drawing.Point(607, 74);
            this.lblTaxID.Name = "lblTaxID";
            this.lblTaxID.Size = new System.Drawing.Size(76, 16);
            this.lblTaxID.TabIndex = 10;
            this.lblTaxID.Text = "Mã số thuế:";
            // 
            // txtOutputVoucherID
            // 
            this.txtOutputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputVoucherID.Location = new System.Drawing.Point(135, 125);
            this.txtOutputVoucherID.Name = "txtOutputVoucherID";
            this.txtOutputVoucherID.ReadOnly = true;
            this.txtOutputVoucherID.Size = new System.Drawing.Size(229, 22);
            this.txtOutputVoucherID.TabIndex = 16;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(7, 71);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 16);
            this.lblAddress.TabIndex = 8;
            this.lblAddress.Text = "Địa chỉ:";
            // 
            // lblOutputVoucherID
            // 
            this.lblOutputVoucherID.AutoSize = true;
            this.lblOutputVoucherID.Location = new System.Drawing.Point(7, 127);
            this.lblOutputVoucherID.Name = "lblOutputVoucherID";
            this.lblOutputVoucherID.Size = new System.Drawing.Size(93, 16);
            this.lblOutputVoucherID.TabIndex = 15;
            this.lblOutputVoucherID.Text = "Mã phiếu xuất:";
            // 
            // txtInputTypeName
            // 
            this.txtInputTypeName.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputTypeName.Location = new System.Drawing.Point(711, 230);
            this.txtInputTypeName.MaxLength = 20;
            this.txtInputTypeName.Name = "txtInputTypeName";
            this.txtInputTypeName.ReadOnly = true;
            this.txtInputTypeName.Size = new System.Drawing.Size(229, 22);
            this.txtInputTypeName.TabIndex = 29;
            this.txtInputTypeName.Visible = false;
            // 
            // txtStoreName
            // 
            this.txtStoreName.BackColor = System.Drawing.SystemColors.Info;
            this.txtStoreName.Location = new System.Drawing.Point(134, 222);
            this.txtStoreName.MaxLength = 20;
            this.txtStoreName.Name = "txtStoreName";
            this.txtStoreName.ReadOnly = true;
            this.txtStoreName.Size = new System.Drawing.Size(242, 22);
            this.txtStoreName.TabIndex = 8;
            this.txtStoreName.Visible = false;
            // 
            // lblInputType
            // 
            this.lblInputType.AutoSize = true;
            this.lblInputType.Location = new System.Drawing.Point(491, 237);
            this.lblInputType.Name = "lblInputType";
            this.lblInputType.Size = new System.Drawing.Size(106, 16);
            this.lblInputType.TabIndex = 27;
            this.lblInputType.Text = "Loại phiếu nhập:";
            // 
            // chkCreateSaleOrder
            // 
            this.chkCreateSaleOrder.AutoSize = true;
            this.chkCreateSaleOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCreateSaleOrder.ForeColor = System.Drawing.Color.Red;
            this.chkCreateSaleOrder.Location = new System.Drawing.Point(135, 351);
            this.chkCreateSaleOrder.Name = "chkCreateSaleOrder";
            this.chkCreateSaleOrder.Size = new System.Drawing.Size(266, 20);
            this.chkCreateSaleOrder.TabIndex = 38;
            this.chkCreateSaleOrder.Text = "Đổi hình thức xuất - Lấy khuyến mãi";
            this.chkCreateSaleOrder.UseVisualStyleBackColor = true;
            this.chkCreateSaleOrder.CheckedChanged += new System.EventHandler(this.chkCreateSaleOrder_CheckedChanged);
            // 
            // lblTotalLiquidate
            // 
            this.lblTotalLiquidate.AutoSize = true;
            this.lblTotalLiquidate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalLiquidate.ForeColor = System.Drawing.Color.Blue;
            this.lblTotalLiquidate.Location = new System.Drawing.Point(131, 255);
            this.lblTotalLiquidate.Name = "lblTotalLiquidate";
            this.lblTotalLiquidate.Size = new System.Drawing.Size(306, 24);
            this.lblTotalLiquidate.TabIndex = 39;
            this.lblTotalLiquidate.Text = "Tổng tiền thanh toán của đơn hàng";
            this.lblTotalLiquidate.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(491, 265);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 16);
            this.label3.TabIndex = 33;
            this.label3.Text = "Lý do đổi trả:";
            // 
            // tabPageInputChangeOrder
            // 
            this.tabPageInputChangeOrder.Controls.Add(this.groupBox5);
            this.tabPageInputChangeOrder.Controls.Add(this.groupBox4);
            this.tabPageInputChangeOrder.Controls.Add(this.chkApplyErrorType);
            this.tabPageInputChangeOrder.Controls.Add(this.txtReasonNote);
            this.tabPageInputChangeOrder.Controls.Add(this.label18);
            this.tabPageInputChangeOrder.Controls.Add(this.txtInputChangeOrderID);
            this.tabPageInputChangeOrder.Controls.Add(this.txtSaleOrderID);
            this.tabPageInputChangeOrder.Controls.Add(this.label10);
            this.tabPageInputChangeOrder.Controls.Add(this.txtCreateUser);
            this.tabPageInputChangeOrder.Controls.Add(this.txtInputChangeOrderStoreName);
            this.tabPageInputChangeOrder.Controls.Add(this.txtContent);
            this.tabPageInputChangeOrder.Controls.Add(this.cboInputChangeOrderStoreID);
            this.tabPageInputChangeOrder.Controls.Add(this.dtpInputChangeOrderDate);
            this.tabPageInputChangeOrder.Controls.Add(this.ucCreateUser);
            this.tabPageInputChangeOrder.Controls.Add(this.label20);
            this.tabPageInputChangeOrder.Controls.Add(this.lnkSaleOrderID);
            this.tabPageInputChangeOrder.Controls.Add(this.label23);
            this.tabPageInputChangeOrder.Controls.Add(this.label7);
            this.tabPageInputChangeOrder.Controls.Add(this.lblContent);
            this.tabPageInputChangeOrder.Controls.Add(this.label24);
            this.tabPageInputChangeOrder.Controls.Add(this.cboReason);
            this.tabPageInputChangeOrder.Location = new System.Drawing.Point(4, 25);
            this.tabPageInputChangeOrder.Name = "tabPageInputChangeOrder";
            this.tabPageInputChangeOrder.Size = new System.Drawing.Size(1053, 376);
            this.tabPageInputChangeOrder.TabIndex = 2;
            this.tabPageInputChangeOrder.Text = "Thông tin yêu cầu đổi trả";
            this.tabPageInputChangeOrder.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.dtpInputchangeDate);
            this.groupBox5.Controls.Add(this.txtDenominatorReturn);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.txtInvoiceSymbolReturn);
            this.groupBox5.Controls.Add(this.txtInvoiceIdReturn);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.txtOutVoucherID);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.txtNewInputVoucherID);
            this.groupBox5.Controls.Add(this.chkIsInputchange);
            this.groupBox5.Location = new System.Drawing.Point(15, 148);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(799, 73);
            this.groupBox5.TabIndex = 31;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Thông tin hàng nhập trả";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(0, 48);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 16);
            this.label21.TabIndex = 23;
            this.label21.Text = "Số HĐ:";
            // 
            // dtpInputchangeDate
            // 
            this.dtpInputchangeDate.EditValue = null;
            this.dtpInputchangeDate.Enabled = false;
            this.dtpInputchangeDate.Location = new System.Drawing.Point(83, 16);
            this.dtpInputchangeDate.Name = "dtpInputchangeDate";
            this.dtpInputchangeDate.Properties.Appearance.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dtpInputchangeDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.dtpInputchangeDate.Properties.Appearance.Options.UseBackColor = true;
            this.dtpInputchangeDate.Properties.Appearance.Options.UseFont = true;
            this.dtpInputchangeDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpInputchangeDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.dtpInputchangeDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtpInputchangeDate.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpInputchangeDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtpInputchangeDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtpInputchangeDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpInputchangeDate.Size = new System.Drawing.Size(141, 22);
            this.dtpInputchangeDate.TabIndex = 17;
            // 
            // txtDenominatorReturn
            // 
            this.txtDenominatorReturn.BackColor = System.Drawing.SystemColors.Info;
            this.txtDenominatorReturn.Location = new System.Drawing.Point(590, 45);
            this.txtDenominatorReturn.Name = "txtDenominatorReturn";
            this.txtDenominatorReturn.ReadOnly = true;
            this.txtDenominatorReturn.Size = new System.Drawing.Size(200, 22);
            this.txtDenominatorReturn.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(238, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 16);
            this.label12.TabIndex = 18;
            this.label12.Text = "Phiếu chi mới:";
            // 
            // txtInvoiceSymbolReturn
            // 
            this.txtInvoiceSymbolReturn.BackColor = System.Drawing.SystemColors.Info;
            this.txtInvoiceSymbolReturn.Location = new System.Drawing.Point(331, 45);
            this.txtInvoiceSymbolReturn.Name = "txtInvoiceSymbolReturn";
            this.txtInvoiceSymbolReturn.ReadOnly = true;
            this.txtInvoiceSymbolReturn.Size = new System.Drawing.Size(141, 22);
            this.txtInvoiceSymbolReturn.TabIndex = 22;
            // 
            // txtInvoiceIdReturn
            // 
            this.txtInvoiceIdReturn.BackColor = System.Drawing.SystemColors.Info;
            this.txtInvoiceIdReturn.Location = new System.Drawing.Point(84, 45);
            this.txtInvoiceIdReturn.Name = "txtInvoiceIdReturn";
            this.txtInvoiceIdReturn.ReadOnly = true;
            this.txtInvoiceIdReturn.Size = new System.Drawing.Size(141, 22);
            this.txtInvoiceIdReturn.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(487, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 16);
            this.label11.TabIndex = 20;
            this.label11.Text = "Phiếu nhập mới:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(486, 48);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(74, 16);
            this.label25.TabIndex = 18;
            this.label25.Text = "Mẫu số HĐ";
            // 
            // txtOutVoucherID
            // 
            this.txtOutVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutVoucherID.Location = new System.Drawing.Point(331, 16);
            this.txtOutVoucherID.Name = "txtOutVoucherID";
            this.txtOutVoucherID.ReadOnly = true;
            this.txtOutVoucherID.Size = new System.Drawing.Size(141, 22);
            this.txtOutVoucherID.TabIndex = 19;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(238, 48);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 16);
            this.label26.TabIndex = 19;
            this.label26.Text = "Ký hiệu HĐ";
            // 
            // txtNewInputVoucherID
            // 
            this.txtNewInputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtNewInputVoucherID.Location = new System.Drawing.Point(590, 15);
            this.txtNewInputVoucherID.Name = "txtNewInputVoucherID";
            this.txtNewInputVoucherID.ReadOnly = true;
            this.txtNewInputVoucherID.Size = new System.Drawing.Size(200, 22);
            this.txtNewInputVoucherID.TabIndex = 21;
            // 
            // chkIsInputchange
            // 
            this.chkIsInputchange.AutoSize = true;
            this.chkIsInputchange.Enabled = false;
            this.chkIsInputchange.Location = new System.Drawing.Point(4, 18);
            this.chkIsInputchange.Name = "chkIsInputchange";
            this.chkIsInputchange.Size = new System.Drawing.Size(73, 20);
            this.chkIsInputchange.TabIndex = 16;
            this.chkIsInputchange.Text = "Đã xử lý";
            this.chkIsInputchange.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txtDenominatorOutput);
            this.groupBox4.Controls.Add(this.txtInvoiceSymbolOutput);
            this.groupBox4.Controls.Add(this.txtInvoiceIdOut);
            this.groupBox4.Controls.Add(this.dtpReviewedDate);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.txtInVoucherID);
            this.groupBox4.Controls.Add(this.chkIsReviewed);
            this.groupBox4.Controls.Add(this.txtNewOutputVoucherID);
            this.groupBox4.Location = new System.Drawing.Point(15, 66);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(799, 78);
            this.groupBox4.TabIndex = 30;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Thông tin hàng xuất đổi";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(0, 52);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 16);
            this.label14.TabIndex = 17;
            this.label14.Text = "Số HĐ:";
            // 
            // txtDenominatorOutput
            // 
            this.txtDenominatorOutput.BackColor = System.Drawing.SystemColors.Info;
            this.txtDenominatorOutput.Location = new System.Drawing.Point(590, 49);
            this.txtDenominatorOutput.Name = "txtDenominatorOutput";
            this.txtDenominatorOutput.ReadOnly = true;
            this.txtDenominatorOutput.Size = new System.Drawing.Size(200, 22);
            this.txtDenominatorOutput.TabIndex = 16;
            // 
            // txtInvoiceSymbolOutput
            // 
            this.txtInvoiceSymbolOutput.BackColor = System.Drawing.SystemColors.Info;
            this.txtInvoiceSymbolOutput.Location = new System.Drawing.Point(331, 49);
            this.txtInvoiceSymbolOutput.Name = "txtInvoiceSymbolOutput";
            this.txtInvoiceSymbolOutput.ReadOnly = true;
            this.txtInvoiceSymbolOutput.Size = new System.Drawing.Size(141, 22);
            this.txtInvoiceSymbolOutput.TabIndex = 16;
            // 
            // txtInvoiceIdOut
            // 
            this.txtInvoiceIdOut.BackColor = System.Drawing.SystemColors.Info;
            this.txtInvoiceIdOut.Location = new System.Drawing.Point(84, 49);
            this.txtInvoiceIdOut.Name = "txtInvoiceIdOut";
            this.txtInvoiceIdOut.ReadOnly = true;
            this.txtInvoiceIdOut.Size = new System.Drawing.Size(141, 22);
            this.txtInvoiceIdOut.TabIndex = 16;
            // 
            // dtpReviewedDate
            // 
            this.dtpReviewedDate.EditValue = null;
            this.dtpReviewedDate.Enabled = false;
            this.dtpReviewedDate.Location = new System.Drawing.Point(84, 21);
            this.dtpReviewedDate.Name = "dtpReviewedDate";
            this.dtpReviewedDate.Properties.Appearance.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dtpReviewedDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.dtpReviewedDate.Properties.Appearance.Options.UseBackColor = true;
            this.dtpReviewedDate.Properties.Appearance.Options.UseFont = true;
            this.dtpReviewedDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpReviewedDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.dtpReviewedDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtpReviewedDate.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpReviewedDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtpReviewedDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtpReviewedDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpReviewedDate.Size = new System.Drawing.Size(141, 22);
            this.dtpReviewedDate.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(486, 52);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 16);
            this.label16.TabIndex = 12;
            this.label16.Text = "Mẫu số HĐ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(486, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 16);
            this.label9.TabIndex = 14;
            this.label9.Text = "Phiếu xuất mới:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(238, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 16);
            this.label15.TabIndex = 12;
            this.label15.Text = "Ký hiệu HĐ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(238, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 16);
            this.label8.TabIndex = 12;
            this.label8.Text = "Phiếu thu mới:";
            // 
            // txtInVoucherID
            // 
            this.txtInVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInVoucherID.Location = new System.Drawing.Point(331, 19);
            this.txtInVoucherID.Name = "txtInVoucherID";
            this.txtInVoucherID.ReadOnly = true;
            this.txtInVoucherID.Size = new System.Drawing.Size(141, 22);
            this.txtInVoucherID.TabIndex = 13;
            // 
            // chkIsReviewed
            // 
            this.chkIsReviewed.AutoSize = true;
            this.chkIsReviewed.Enabled = false;
            this.chkIsReviewed.Location = new System.Drawing.Point(3, 21);
            this.chkIsReviewed.Name = "chkIsReviewed";
            this.chkIsReviewed.Size = new System.Drawing.Size(80, 20);
            this.chkIsReviewed.TabIndex = 10;
            this.chkIsReviewed.Text = "Đã duyệt";
            this.chkIsReviewed.UseVisualStyleBackColor = true;
            // 
            // txtNewOutputVoucherID
            // 
            this.txtNewOutputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtNewOutputVoucherID.Location = new System.Drawing.Point(590, 19);
            this.txtNewOutputVoucherID.Name = "txtNewOutputVoucherID";
            this.txtNewOutputVoucherID.ReadOnly = true;
            this.txtNewOutputVoucherID.Size = new System.Drawing.Size(200, 22);
            this.txtNewOutputVoucherID.TabIndex = 15;
            // 
            // chkApplyErrorType
            // 
            this.chkApplyErrorType.AutoSize = true;
            this.chkApplyErrorType.Enabled = false;
            this.chkApplyErrorType.Location = new System.Drawing.Point(501, 14);
            this.chkApplyErrorType.Name = "chkApplyErrorType";
            this.chkApplyErrorType.Size = new System.Drawing.Size(105, 20);
            this.chkApplyErrorType.TabIndex = 4;
            this.chkApplyErrorType.Text = "Sản phẩm lỗi";
            this.chkApplyErrorType.UseVisualStyleBackColor = true;
            // 
            // txtReasonNote
            // 
            this.txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
            this.txtReasonNote.Location = new System.Drawing.Point(99, 254);
            this.txtReasonNote.MaxLength = 300;
            this.txtReasonNote.Multiline = true;
            this.txtReasonNote.Name = "txtReasonNote";
            this.txtReasonNote.ReadOnly = true;
            this.txtReasonNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReasonNote.Size = new System.Drawing.Size(706, 70);
            this.txtReasonNote.TabIndex = 27;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(253, 230);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 16);
            this.label18.TabIndex = 24;
            this.label18.Text = "Lý do đổi trả:";
            // 
            // txtInputChangeOrderID
            // 
            this.txtInputChangeOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputChangeOrderID.Location = new System.Drawing.Point(99, 13);
            this.txtInputChangeOrderID.Name = "txtInputChangeOrderID";
            this.txtInputChangeOrderID.ReadOnly = true;
            this.txtInputChangeOrderID.Size = new System.Drawing.Size(141, 22);
            this.txtInputChangeOrderID.TabIndex = 1;
            // 
            // txtSaleOrderID
            // 
            this.txtSaleOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtSaleOrderID.Location = new System.Drawing.Point(99, 227);
            this.txtSaleOrderID.Name = "txtSaleOrderID";
            this.txtSaleOrderID.ReadOnly = true;
            this.txtSaleOrderID.Size = new System.Drawing.Size(141, 22);
            this.txtSaleOrderID.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(501, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 16);
            this.label10.TabIndex = 7;
            this.label10.Text = "NV yêu cầu: ";
            // 
            // txtCreateUser
            // 
            this.txtCreateUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtCreateUser.Location = new System.Drawing.Point(605, 40);
            this.txtCreateUser.Name = "txtCreateUser";
            this.txtCreateUser.ReadOnly = true;
            this.txtCreateUser.Size = new System.Drawing.Size(200, 22);
            this.txtCreateUser.TabIndex = 9;
            this.txtCreateUser.Visible = false;
            // 
            // txtInputChangeOrderStoreName
            // 
            this.txtInputChangeOrderStoreName.BackColor = System.Drawing.SystemColors.Info;
            this.txtInputChangeOrderStoreName.Location = new System.Drawing.Point(99, 40);
            this.txtInputChangeOrderStoreName.Name = "txtInputChangeOrderStoreName";
            this.txtInputChangeOrderStoreName.ReadOnly = true;
            this.txtInputChangeOrderStoreName.Size = new System.Drawing.Size(388, 22);
            this.txtInputChangeOrderStoreName.TabIndex = 6;
            this.txtInputChangeOrderStoreName.Visible = false;
            // 
            // txtContent
            // 
            this.txtContent.BackColor = System.Drawing.SystemColors.Info;
            this.txtContent.Location = new System.Drawing.Point(99, 330);
            this.txtContent.MaxLength = 200;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ReadOnly = true;
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(706, 70);
            this.txtContent.TabIndex = 29;
            // 
            // cboInputChangeOrderStoreID
            // 
            this.cboInputChangeOrderStoreID.Enabled = false;
            this.cboInputChangeOrderStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputChangeOrderStoreID.Location = new System.Drawing.Point(99, 39);
            this.cboInputChangeOrderStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputChangeOrderStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputChangeOrderStoreID.Name = "cboInputChangeOrderStoreID";
            this.cboInputChangeOrderStoreID.Size = new System.Drawing.Size(389, 24);
            this.cboInputChangeOrderStoreID.TabIndex = 6;
            // 
            // dtpInputChangeOrderDate
            // 
            this.dtpInputChangeOrderDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpInputChangeOrderDate.Enabled = false;
            this.dtpInputChangeOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInputChangeOrderDate.Location = new System.Drawing.Point(346, 13);
            this.dtpInputChangeOrderDate.Name = "dtpInputChangeOrderDate";
            this.dtpInputChangeOrderDate.Size = new System.Drawing.Size(141, 22);
            this.dtpInputChangeOrderDate.TabIndex = 3;
            // 
            // ucCreateUser
            // 
            this.ucCreateUser.Enabled = false;
            this.ucCreateUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucCreateUser.IsOnlyShowRealStaff = false;
            this.ucCreateUser.IsValidate = true;
            this.ucCreateUser.Location = new System.Drawing.Point(605, 40);
            this.ucCreateUser.Margin = new System.Windows.Forms.Padding(4);
            this.ucCreateUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucCreateUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucCreateUser.Name = "ucCreateUser";
            this.ucCreateUser.Size = new System.Drawing.Size(200, 22);
            this.ucCreateUser.TabIndex = 8;
            this.ucCreateUser.UserName = "";
            this.ucCreateUser.WorkingPositionID = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 259);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(87, 16);
            this.label20.TabIndex = 26;
            this.label20.Text = "Ghi chú lý do:";
            // 
            // lnkSaleOrderID
            // 
            this.lnkSaleOrderID.AutoSize = true;
            this.lnkSaleOrderID.Location = new System.Drawing.Point(12, 230);
            this.lnkSaleOrderID.Name = "lnkSaleOrderID";
            this.lnkSaleOrderID.Size = new System.Drawing.Size(86, 16);
            this.lnkSaleOrderID.TabIndex = 22;
            this.lnkSaleOrderID.TabStop = true;
            this.lnkSaleOrderID.Text = "Mã đơn hàng";
            this.lnkSaleOrderID.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkSaleOrderID_LinkClicked);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(80, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "Mã yêu cầu:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 16);
            this.label7.TabIndex = 5;
            this.label7.Text = "Kho nhập:";
            // 
            // lblContent
            // 
            this.lblContent.AutoSize = true;
            this.lblContent.Location = new System.Drawing.Point(12, 338);
            this.lblContent.Name = "lblContent";
            this.lblContent.Size = new System.Drawing.Size(65, 16);
            this.lblContent.TabIndex = 28;
            this.lblContent.Text = "Nội dung:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(253, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(94, 16);
            this.label24.TabIndex = 2;
            this.label24.Text = "Ngày yêu cầu:";
            // 
            // cboReason
            // 
            this.cboReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboReason.Location = new System.Drawing.Point(346, 226);
            this.cboReason.Margin = new System.Windows.Forms.Padding(0);
            this.cboReason.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboReason.Name = "cboReason";
            this.cboReason.Size = new System.Drawing.Size(459, 24);
            this.cboReason.TabIndex = 25;
            // 
            // tabPageReviewList
            // 
            this.tabPageReviewList.Controls.Add(this.flexReviewLevel);
            this.tabPageReviewList.Location = new System.Drawing.Point(4, 25);
            this.tabPageReviewList.Name = "tabPageReviewList";
            this.tabPageReviewList.Size = new System.Drawing.Size(1053, 376);
            this.tabPageReviewList.TabIndex = 3;
            this.tabPageReviewList.Text = "Mức duyệt";
            this.tabPageReviewList.UseVisualStyleBackColor = true;
            // 
            // flexReviewLevel
            // 
            this.flexReviewLevel.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexReviewLevel.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexReviewLevel.ColumnInfo = "1,1,0,0,0,105,Columns:";
            this.flexReviewLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexReviewLevel.Location = new System.Drawing.Point(0, 0);
            this.flexReviewLevel.Name = "flexReviewLevel";
            this.flexReviewLevel.Rows.Count = 1;
            this.flexReviewLevel.Rows.DefaultSize = 21;
            this.flexReviewLevel.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Cell;
            this.flexReviewLevel.Size = new System.Drawing.Size(1053, 376);
            this.flexReviewLevel.StyleInfo = resources.GetString("flexReviewLevel.StyleInfo");
            this.flexReviewLevel.TabIndex = 4;
            // 
            // tabPageWorkflow
            // 
            this.tabPageWorkflow.Controls.Add(this.flexWorkFlow);
            this.tabPageWorkflow.Location = new System.Drawing.Point(4, 25);
            this.tabPageWorkflow.Name = "tabPageWorkflow";
            this.tabPageWorkflow.Size = new System.Drawing.Size(1053, 376);
            this.tabPageWorkflow.TabIndex = 4;
            this.tabPageWorkflow.Text = "Quy trình xử lý";
            this.tabPageWorkflow.UseVisualStyleBackColor = true;
            // 
            // flexWorkFlow
            // 
            this.flexWorkFlow.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexWorkFlow.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexWorkFlow.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexWorkFlow.ColumnInfo = "1,1,0,0,0,105,Columns:";
            this.flexWorkFlow.ContextMenuStrip = this.mnuWorkFlow;
            this.flexWorkFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexWorkFlow.Location = new System.Drawing.Point(0, 0);
            this.flexWorkFlow.Name = "flexWorkFlow";
            this.flexWorkFlow.Rows.Count = 1;
            this.flexWorkFlow.Rows.DefaultSize = 21;
            this.flexWorkFlow.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Cell;
            this.flexWorkFlow.Size = new System.Drawing.Size(1053, 376);
            this.flexWorkFlow.StyleInfo = resources.GetString("flexWorkFlow.StyleInfo");
            this.flexWorkFlow.TabIndex = 7;
            this.flexWorkFlow.MouseEnterCell += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexWorkFlow_MouseEnterCell);
            this.flexWorkFlow.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexWorkFlow_BeforeEdit);
            this.flexWorkFlow.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexWorkFlow_SetupEditor);
            this.flexWorkFlow.Click += new System.EventHandler(this.flexWorkFlow_Click);
            // 
            // mnuWorkFlow
            // 
            this.mnuWorkFlow.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuWorkFlow_IsProcess});
            this.mnuWorkFlow.Name = "mnuWorkFlow";
            this.mnuWorkFlow.Size = new System.Drawing.Size(158, 26);
            this.mnuWorkFlow.Opening += new System.ComponentModel.CancelEventHandler(this.mnuWorkFlow_Opening);
            // 
            // mnuWorkFlow_IsProcess
            // 
            this.mnuWorkFlow_IsProcess.Name = "mnuWorkFlow_IsProcess";
            this.mnuWorkFlow_IsProcess.Size = new System.Drawing.Size(157, 22);
            this.mnuWorkFlow_IsProcess.Text = "Xử lý bước xử lý";
            this.mnuWorkFlow_IsProcess.Click += new System.EventHandler(this.mnuWorkFlow_IsProcess_Click);
            // 
            // tabPageAttach
            // 
            this.tabPageAttach.Controls.Add(this.grdAttachment);
            this.tabPageAttach.Location = new System.Drawing.Point(4, 25);
            this.tabPageAttach.Name = "tabPageAttach";
            this.tabPageAttach.Size = new System.Drawing.Size(1053, 376);
            this.tabPageAttach.TabIndex = 5;
            this.tabPageAttach.Text = "Tập tin đính kèm";
            this.tabPageAttach.UseVisualStyleBackColor = true;
            // 
            // grdAttachment
            // 
            this.grdAttachment.ContextMenuStrip = this.mnuAttachment;
            this.grdAttachment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdAttachment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            gridLevelNode1.RelationName = "Level1";
            this.grdAttachment.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grdAttachment.Location = new System.Drawing.Point(0, 0);
            this.grdAttachment.MainView = this.grvAttachment;
            this.grdAttachment.Name = "grdAttachment";
            this.grdAttachment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDownload,
            this.repositoryItemTextEdit1});
            this.grdAttachment.Size = new System.Drawing.Size(1053, 376);
            this.grdAttachment.TabIndex = 4;
            this.grdAttachment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvAttachment});
            this.grdAttachment.EditorKeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grdAttachment_EditorKeyPress);
            // 
            // mnuAttachment
            // 
            this.mnuAttachment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAddAttachment,
            this.mnuDelAttachment});
            this.mnuAttachment.Name = "mnuFlex";
            this.mnuAttachment.Size = new System.Drawing.Size(143, 48);
            this.mnuAttachment.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAttachment_Opening);
            // 
            // mnuAddAttachment
            // 
            this.mnuAddAttachment.Name = "mnuAddAttachment";
            this.mnuAddAttachment.Size = new System.Drawing.Size(142, 22);
            this.mnuAddAttachment.Text = "Thêm tập tin";
            this.mnuAddAttachment.Click += new System.EventHandler(this.mnuAddAttachment_Click);
            // 
            // mnuDelAttachment
            // 
            this.mnuDelAttachment.Name = "mnuDelAttachment";
            this.mnuDelAttachment.Size = new System.Drawing.Size(142, 22);
            this.mnuDelAttachment.Text = "Xóa tập tin";
            this.mnuDelAttachment.Click += new System.EventHandler(this.mnuDelAttachment_Click);
            // 
            // grvAttachment
            // 
            this.grvAttachment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvAttachment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvAttachment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn14,
            this.gridColumn15,
            this.colDownload});
            this.grvAttachment.GridControl = this.grdAttachment;
            this.grvAttachment.Name = "grvAttachment";
            this.grvAttachment.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "STT";
            this.gridColumn1.FieldName = "STT";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.TabStop = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn8.Caption = "Tên tập tin";
            this.gridColumn8.FieldName = "AttachmentName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.TabStop = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            this.gridColumn8.Width = 194;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.Caption = "Mô tả";
            this.gridColumn9.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn9.FieldName = "Description";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            this.gridColumn9.Width = 273;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 400;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn14.Caption = "Thời gian tạo";
            this.gridColumn14.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn14.FieldName = "CreatedDate";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.TabStop = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 3;
            this.gridColumn14.Width = 119;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn15.Caption = "Nhân viên tạo";
            this.gridColumn15.FieldName = "CreatedFullName";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.TabStop = false;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 4;
            this.gridColumn15.Width = 241;
            // 
            // colDownload
            // 
            this.colDownload.AppearanceHeader.Options.UseTextOptions = true;
            this.colDownload.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDownload.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDownload.Caption = "Tải";
            this.colDownload.ColumnEdit = this.btnDownload;
            this.colDownload.FieldName = "colDownload";
            this.colDownload.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colDownload.Name = "colDownload";
            this.colDownload.OptionsColumn.AllowIncrementalSearch = false;
            this.colDownload.OptionsColumn.AllowMove = false;
            this.colDownload.OptionsColumn.AllowShowHide = false;
            this.colDownload.OptionsColumn.AllowSize = false;
            this.colDownload.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDownload.OptionsColumn.FixedWidth = true;
            this.colDownload.OptionsColumn.ReadOnly = true;
            this.colDownload.OptionsColumn.TabStop = false;
            this.colDownload.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colDownload.Visible = true;
            this.colDownload.VisibleIndex = 5;
            this.colDownload.Width = 30;
            // 
            // btnDownload
            // 
            this.btnDownload.AutoHeight = false;
            this.btnDownload.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnDownload.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Tải tập tin", null, null, true)});
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox2.Controls.Add(this.lblInstruction);
            this.groupBox2.Controls.Add(this.flexDetail);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 405);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1061, 218);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chi tiết nhập đổi/trả";
            // 
            // lblInstruction
            // 
            this.lblInstruction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInstruction.AutoSize = true;
            this.lblInstruction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstruction.ForeColor = System.Drawing.Color.Blue;
            this.lblInstruction.Location = new System.Drawing.Point(641, 2);
            this.lblInstruction.Name = "lblInstruction";
            this.lblInstruction.Size = new System.Drawing.Size(416, 16);
            this.lblInstruction.TabIndex = 64;
            this.lblInstruction.Text = "Cho phép nhập trực tiếp IMEI xuất nếu không quan tâm FIFO";
            this.lblInstruction.Visible = false;
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.BackColor = System.Drawing.SystemColors.Control;
            this.btnAdd.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(949, 8);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(106, 25);
            this.btnAdd.TabIndex = 32;
            this.btnAdd.Text = "Tạo yêu cầu";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // flexDetail
            // 
            this.flexDetail.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexDetail.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexDetail.AutoClipboard = true;
            this.flexDetail.ColumnInfo = "1,1,0,0,0,105,Columns:";
            this.flexDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexDetail.Location = new System.Drawing.Point(3, 18);
            this.flexDetail.Name = "flexDetail";
            this.flexDetail.Rows.Count = 1;
            this.flexDetail.Rows.DefaultSize = 21;
            this.flexDetail.Size = new System.Drawing.Size(1055, 197);
            this.flexDetail.StyleInfo = resources.GetString("flexDetail.StyleInfo");
            this.flexDetail.TabIndex = 37;
            this.flexDetail.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexDetail.MouseEnterCell += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexData_MouseEnterCell);
            this.flexDetail.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexDetail_BeforeEdit);
            this.flexDetail.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexDetail_AfterEdit);
            this.flexDetail.CellButtonClick += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexDetail_CellButtonClick);
            // 
            // btnCreateConcern
            // 
            this.btnCreateConcern.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCreateConcern.BackColor = System.Drawing.SystemColors.Control;
            this.btnCreateConcern.Enabled = false;
            this.btnCreateConcern.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnCreateConcern.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreateConcern.Location = new System.Drawing.Point(661, 6);
            this.btnCreateConcern.Name = "btnCreateConcern";
            this.btnCreateConcern.Size = new System.Drawing.Size(91, 25);
            this.btnCreateConcern.TabIndex = 40;
            this.btnCreateConcern.Text = "   Tạo phiếu";
            this.btnCreateConcern.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateConcern.UseVisualStyleBackColor = false;
            this.btnCreateConcern.Click += new System.EventHandler(this.btnCreateConcern_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "edit.png");
            this.imageList1.Images.SetKeyName(1, "delete_cancel.png");
            this.imageList1.Images.SetKeyName(2, "check_review.png");
            this.imageList1.Images.SetKeyName(3, "Folder-Downloads-icon1.png");
            // 
            // btnReviewList
            // 
            this.btnReviewList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReviewList.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReviewList.Appearance.Options.UseFont = true;
            this.btnReviewList.DropDownControl = this.pMnu;
            this.btnReviewList.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnReviewList.Location = new System.Drawing.Point(366, 5);
            this.btnReviewList.Name = "btnReviewList";
            this.btnReviewList.Size = new System.Drawing.Size(85, 25);
            this.btnReviewList.TabIndex = 41;
            this.btnReviewList.Text = "Duyệt";
            // 
            // pMnu
            // 
            this.pMnu.Manager = this.barManager1;
            this.pMnu.Name = "pMnu";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageList1;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1061, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 660);
            this.barDockControlBottom.Size = new System.Drawing.Size(1061, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 660);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1061, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 660);
            // 
            // lblReviewStatus
            // 
            this.lblReviewStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblReviewStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReviewStatus.ForeColor = System.Drawing.Color.Blue;
            this.lblReviewStatus.Location = new System.Drawing.Point(0, 5);
            this.lblReviewStatus.Name = "lblReviewStatus";
            this.lblReviewStatus.Size = new System.Drawing.Size(360, 30);
            this.lblReviewStatus.TabIndex = 42;
            this.lblReviewStatus.Text = "Trạng thái duyệt";
            this.lblReviewStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(873, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 25);
            this.btnDelete.TabIndex = 43;
            this.btnDelete.Text = "      Hủy";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtReturnAmount
            // 
            this.txtReturnAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtReturnAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtReturnAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReturnAmount.Location = new System.Drawing.Point(530, 5);
            this.txtReturnAmount.MaxLength = 20;
            this.txtReturnAmount.Name = "txtReturnAmount";
            this.txtReturnAmount.ReadOnly = true;
            this.txtReturnAmount.Size = new System.Drawing.Size(128, 26);
            this.txtReturnAmount.TabIndex = 44;
            this.txtReturnAmount.Text = "0";
            // 
            // tmrCloseForm
            // 
            this.tmrCloseForm.Tick += new System.EventHandler(this.tmrCloseForm_Tick);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.BackColor = System.Drawing.SystemColors.Control;
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(949, 6);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(106, 25);
            this.btnUpdate.TabIndex = 49;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblTitlePayment
            // 
            this.lblTitlePayment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTitlePayment.AutoSize = true;
            this.lblTitlePayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitlePayment.ForeColor = System.Drawing.Color.Blue;
            this.lblTitlePayment.Location = new System.Drawing.Point(452, 8);
            this.lblTitlePayment.Name = "lblTitlePayment";
            this.lblTitlePayment.Size = new System.Drawing.Size(75, 20);
            this.lblTitlePayment.TabIndex = 54;
            this.lblTitlePayment.Text = "Phải thu";
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Appearance.Options.UseFont = true;
            this.btnPrint.DropDownControl = this.pMnuPrint;
            this.btnPrint.Location = new System.Drawing.Point(661, 6);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(91, 25);
            this.btnPrint.TabIndex = 59;
            this.btnPrint.Text = "In";
            this.btnPrint.Visible = false;
            // 
            // pMnuPrint
            // 
            this.pMnuPrint.Manager = this.barManager1;
            this.pMnuPrint.Name = "pMnuPrint";
            // 
            // btnSupportCare
            // 
            this.btnSupportCare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSupportCare.BackColor = System.Drawing.SystemColors.Control;
            this.btnSupportCare.Image = global::ERP.Inventory.DUI.Properties.Resources.sentuser;
            this.btnSupportCare.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSupportCare.Location = new System.Drawing.Point(755, 6);
            this.btnSupportCare.Name = "btnSupportCare";
            this.btnSupportCare.Size = new System.Drawing.Size(116, 25);
            this.btnSupportCare.TabIndex = 64;
            this.btnSupportCare.Text = "Hỗ trợ trả Care";
            this.btnSupportCare.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSupportCare.UseVisualStyleBackColor = false;
            this.btnSupportCare.Visible = false;
            this.btnSupportCare.Click += new System.EventHandler(this.btnSupportCare_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.lblTitlePayment);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.btnSupportCare);
            this.panel1.Controls.Add(this.lblReviewStatus);
            this.panel1.Controls.Add(this.txtReturnAmount);
            this.panel1.Controls.Add(this.btnReviewList);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnCreateConcern);
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 623);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1061, 37);
            this.panel1.TabIndex = 69;
            // 
            // frmInputChangeOrderViettel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 660);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1033, 698);
            this.Name = "frmInputChangeOrderViettel";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yêu cầu nhập đổi/trả hàng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmInputChangeOrder_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPageOutputVoucher.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPageInputChangeOrder.ResumeLayout(false);
            this.tabPageInputChangeOrder.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInputchangeDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInputchangeDate.Properties)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpReviewedDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpReviewedDate.Properties)).EndInit();
            this.tabPageReviewList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexReviewLevel)).EndInit();
            this.tabPageWorkflow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexWorkFlow)).EndInit();
            this.mnuWorkFlow.ResumeLayout(false);
            this.tabPageAttach.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).EndInit();
            this.mnuAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuPrint)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageOutputVoucher;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtContentOV;
        private System.Windows.Forms.Label lblOutputContent;
        private System.Windows.Forms.Label lblStore;
        private System.Windows.Forms.DateTimePicker dtOutputDate;
        private System.Windows.Forms.Label lblOutputDate;
        private System.Windows.Forms.TextBox txtOutputVoucherID;
        private System.Windows.Forms.Label lblOutputVoucherID;
        private System.Windows.Forms.TextBox txtCustomerPhone;
        private System.Windows.Forms.TextBox txtTaxID;
        private System.Windows.Forms.TextBox txtCustomerAddress;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.TextBox txtCustomerIDCard;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblTaxID;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAdd;
        private C1.Win.C1FlexGrid.C1FlexGrid flexDetail;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ctrlStaffUser;
        private System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.LinkLabel lnkCustomer;
        private System.Windows.Forms.Button btnCreateConcern;
        private System.Windows.Forms.TextBox txtContentIOT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPageInputChangeOrder;
        private System.Windows.Forms.DateTimePicker dtpInputChangeOrderDate;
        private System.Windows.Forms.TextBox txtInputChangeOrderID;
        private System.Windows.Forms.TabPage tabPageReviewList;
        private System.Windows.Forms.TabPage tabPageWorkflow;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtInVoucherID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNewOutputVoucherID;
        private System.Windows.Forms.Label label9;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucCreateUser;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtOutVoucherID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtNewInputVoucherID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label lblContent;
        private System.Windows.Forms.CheckBox chkIsInputchange;
        private System.Windows.Forms.CheckBox chkIsReviewed;
        private C1.Win.C1FlexGrid.C1FlexGrid flexReviewLevel;
        private C1.Win.C1FlexGrid.C1FlexGrid flexWorkFlow;
        private System.Windows.Forms.TabPage tabPageAttach;
        private DevExpress.XtraGrid.GridControl grdAttachment;
        private DevExpress.XtraGrid.Views.Grid.GridView grvAttachment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colDownload;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDownload;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.DropDownButton btnReviewList;
        private System.Windows.Forms.Label lblReviewStatus;
        private System.Windows.Forms.ContextMenuStrip mnuAttachment;
        private System.Windows.Forms.ToolStripMenuItem mnuAddAttachment;
        private System.Windows.Forms.ToolStripMenuItem mnuDelAttachment;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ContextMenuStrip mnuWorkFlow;
        private System.Windows.Forms.ToolStripMenuItem mnuWorkFlow_IsProcess;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblInputType;
        private DevExpress.XtraEditors.DateEdit dtpInputchangeDate;
        private DevExpress.XtraEditors.DateEdit dtpReviewedDate;
        private System.Windows.Forms.TextBox txtReturnAmount;
        private System.Windows.Forms.Timer tmrCloseForm;
        private System.Windows.Forms.TextBox txtStaffUser;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.PopupMenu pMnu;
        private System.Windows.Forms.Button btnUpdate;
        private Library.AppControl.ComboBoxControl.ComboBoxInputType cboInputTypeID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStore;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboInputChangeOrderStoreID;
        private System.Windows.Forms.TextBox txtInputTypeName;
        private System.Windows.Forms.TextBox txtStoreName;
        private System.Windows.Forms.TextBox txtInputChangeOrderStoreName;
        private System.Windows.Forms.TextBox txtCreateUser;
        private System.Windows.Forms.Label lblTitlePayment;
        private DevExpress.XtraEditors.DropDownButton btnPrint;
        private DevExpress.XtraBars.PopupMenu pMnuPrint;
        private System.Windows.Forms.LinkLabel lnkSaleOrderID;
        private System.Windows.Forms.TextBox txtSaleOrderID;
        private System.Windows.Forms.CheckBox chkCreateSaleOrder;
        private System.Windows.Forms.Label lblTotalLiquidate;
        private System.Windows.Forms.Label lblInstruction;
        private System.Windows.Forms.Button btnSupportCare;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtReasonNoteOV;
        private System.Windows.Forms.Label label3;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboReasonOV;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtReasonNote;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label6;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboReason;
        private System.Windows.Forms.CheckBox chkApplyErrorType;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtInVoiceID;
        private System.Windows.Forms.TextBox txtDenominator;
        private System.Windows.Forms.TextBox txtInvoiceSymbol;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblInvoiceSymbol;
        private System.Windows.Forms.LinkLabel lblInvoiceNo;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtDenominatorReturn;
        private System.Windows.Forms.TextBox txtInvoiceSymbolReturn;
        private System.Windows.Forms.TextBox txtInvoiceIdReturn;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtDenominatorOutput;
        private System.Windows.Forms.TextBox txtInvoiceSymbolOutput;
        private System.Windows.Forms.TextBox txtInvoiceIdOut;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.ToolTip toolTip4;
    }
}