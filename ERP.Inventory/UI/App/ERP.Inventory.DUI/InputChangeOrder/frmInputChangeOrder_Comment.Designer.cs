﻿namespace ERP.Inventory.DUI.InputChangeOrder
{
    partial class frmInputChangeOrder_Comment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInputChangeOrder_Comment));
            this.flexData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuComment = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuCommentDel = new System.Windows.Forms.ToolStripMenuItem();
            this.txtCommentContent = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).BeginInit();
            this.mnuComment.SuspendLayout();
            this.SuspendLayout();
            // 
            // flexData
            // 
            this.flexData.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexData.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flexData.ColumnInfo = "1,1,0,0,0,105,Columns:";
            this.flexData.ContextMenuStrip = this.mnuComment;
            this.flexData.Location = new System.Drawing.Point(1, 65);
            this.flexData.Name = "flexData";
            this.flexData.Rows.Count = 1;
            this.flexData.Rows.DefaultSize = 21;
            this.flexData.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Cell;
            this.flexData.Size = new System.Drawing.Size(895, 403);
            this.flexData.StyleInfo = resources.GetString("flexData.StyleInfo");
            this.flexData.TabIndex = 3;
            this.flexData.MouseEnterCell += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexData_MouseEnterCell);
            this.flexData.Resize += new System.EventHandler(this.flexData_Resize);
            // 
            // mnuComment
            // 
            this.mnuComment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCommentDel});
            this.mnuComment.Name = "contextMenuStrip1";
            this.mnuComment.Size = new System.Drawing.Size(153, 48);
            this.mnuComment.Opening += new System.ComponentModel.CancelEventHandler(this.mnuComment_Opening);
            // 
            // mnuCommentDel
            // 
            this.mnuCommentDel.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuCommentDel.Name = "mnuCommentDel";
            this.mnuCommentDel.Size = new System.Drawing.Size(152, 22);
            this.mnuCommentDel.Text = "Xóa bình luận";
            this.mnuCommentDel.Click += new System.EventHandler(this.mnuCommentDel_Click);
            // 
            // txtCommentContent
            // 
            this.txtCommentContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCommentContent.Location = new System.Drawing.Point(70, 1);
            this.txtCommentContent.MaxLength = 2000;
            this.txtCommentContent.Multiline = true;
            this.txtCommentContent.Name = "txtCommentContent";
            this.txtCommentContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCommentContent.Size = new System.Drawing.Size(710, 62);
            this.txtCommentContent.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Info;
            this.label7.Location = new System.Drawing.Point(2, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 62);
            this.label7.TabIndex = 0;
            this.label7.Text = "Bình luận:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.edit;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(783, 1);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(113, 62);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "     Gởi bình luận";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // frmInputChangeOrder_Comment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 469);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.txtCommentContent);
            this.Controls.Add(this.flexData);
            this.Controls.Add(this.label7);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmInputChangeOrder_Comment";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmInputChangeOrder_Comment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).EndInit();
            this.mnuComment.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private C1.Win.C1FlexGrid.C1FlexGrid flexData;
        private System.Windows.Forms.TextBox txtCommentContent;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ContextMenuStrip mnuComment;
        private System.Windows.Forms.ToolStripMenuItem mnuCommentDel;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}