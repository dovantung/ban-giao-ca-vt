﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
using Library.AppCore;

namespace ERP.Inventory.DUI.InputChangeOrder
{
    /// <summary>
    /// Created by  : ???
    /// 
    /// Modified by :   Trương Trung Lợi
    /// Date        :   28/08/2014
    /// Desc        :   Sửa lại cho tiện ích
    /// </summary>
    public partial class frmInputChangeOrderInput : Form
    {
        //Nguyễn Văn Tài - bổ sung - 07/07/2017
        //xử lý lập hóa đơn của ERP hay Viettel
        private bool bolIsViettel = false;
        private string strCompany = string.Empty;
        public bool IsViettel
        {
            get { return bolIsViettel; }
            set { bolIsViettel = value; }
        }
        private bool bolIsPrivilege = false;

        public bool IsPrivilege
        {
            get { return bolIsPrivilege; }
            set { bolIsPrivilege = value; }
        }
        public frmInputChangeOrderInput()
        {
            InitializeComponent();
        }
        public frmInputChangeOrderInput(string strOutputVoucherID, int intApplyErrorType)
        {
            InitializeComponent();
            this.strOutputVoucherID = strOutputVoucherID;
            this.intApplyErrorType = intApplyErrorType;
            txtOutputVoucherID.Text = strOutputVoucherID;
            txtOutputVoucherID.ReadOnly = true;
            if (intApplyErrorType == 0)
            {
                radApplyTypeNoError.Checked = true;
            }
            else
                radApplyTypeError.Checked = true;
            radApplyTypeError.Enabled = false;
            radApplyTypeNoError.Enabled = false;
            lnkType.Enabled = false;
        }

        private string strOutputVoucherID = string.Empty;
        private string strIMEI = string.Empty;
        private bool isIMEIInput = false;
        private int intApplyErrorType = 0;
        private ERP.Inventory.PLC.InputChangeOrder.PLCInputChangeOrder objPLCInputChangeOrder = new PLC.InputChangeOrder.PLCInputChangeOrder();
        private PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
        private int intReportID = 0;
        public event EventHandler evtShowInputChangeOrderForm = null;

        private void frmInputChangeOrderInput_Load(object sender, EventArgs e)
        {
            GetParameterForm();
            this.txtOutputVoucherID.TextChanged += new System.EventHandler(this.txtOutputVoucherID_TextChanged);
            this.radApplyTypeError.CheckedChanged += new System.EventHandler(this.radApplyTypeError_CheckedChanged);
            this.radApplyTypeNoError.CheckedChanged += new System.EventHandler(this.radApplyTypeNoError_CheckedChanged);

            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    intReportID = Convert.ToInt32(hstbParam["ReportID"]);
                }
                catch { }
            }
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            if (!btnInput.Enabled)
                return;
            if (txtOutputVoucherID.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập mã phiếu xuất hoặc IMEI.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtOutputVoucherID.Focus();
                return;
            }
            if (strOutputVoucherID == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng click chọn loại yêu cầu.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lnkType.Focus();
                return;
            }
            if((isIMEIInput && txtOutputVoucherID.Text.Trim() != strIMEI) 
                || (!isIMEIInput && strOutputVoucherID != txtOutputVoucherID.Text.Trim()))
            {
                ResetInputChangeOrderType();
                MessageBox.Show(this, "Mã phiếu xuất hoặc IMEI đã được nhập lại. Vui lòng click chọn lại loại yêu cầu.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cboType.Items.Count < 1)
            {
                MessageBox.Show(this, "Không tìm thấy loại yêu cầu nhập đổi/trả hàng phù hợp.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DataTable dtbInputChangeOrderType = (DataTable)cboType.DataSource;
            DataRow[] rInputChangeOrderType = dtbInputChangeOrderType.Select("InputChangeOrderTypeID = " + Convert.ToInt32(cboType.SelectedValue));
            if (Convert.ToString(rInputChangeOrderType[0]["AddFunctionID"]).Trim() != string.Empty &&
                (!Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(rInputChangeOrderType[0]["AddFunctionID"]).Trim())))
            {
                MessageBox.Show(this, "Bạn không có quyền tạo " + cboType.Text + ". Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            bolIsPrivilege = Convert.ToBoolean(Convert.ToInt32(rInputChangeOrderType[0]["IsPrivilege"]));
            if (evtShowInputChangeOrderForm != null)
            {
                this.Text = Convert.ToInt32(cboType.SelectedValue).ToString() + "$" + strOutputVoucherID + "$" + cboType.Text + "$" + radApplyTypeError.Checked;
                evtShowInputChangeOrderForm(this, null);
                return;
            }
            IsViettel = true;
            //if (!IsViettel)
            //{
            //    frmInputChangeOrder frm1 = new frmInputChangeOrder(Convert.ToInt32(cboType.SelectedValue));
            //    frm1.strIMEIInputChange = strIMEI;
            //    frm1.strOutputVoucherID = strOutputVoucherID;
            //    frm1.intReportID = intReportID;
            //    frm1.Text = cboType.Text;
            //    frm1.ApplyErrorType = radApplyTypeError.Checked;
            //    frm1.evtCloseParentForm += new EventHandler(frm1_evtCloseParentForm);
            //    frm1.ShowDialog();
            //}
            //else if(IsViettel)
            //{
                frmInputChangeOrderViettel frm1 = new frmInputChangeOrderViettel(Convert.ToInt32(cboType.SelectedValue));
                frm1.strIMEIInputChange = strIMEI;
                frm1.strOutputVoucherID = strOutputVoucherID;
                frm1.intReportID = intReportID;
                frm1.Text = cboType.Text;
                frm1.ApplyErrorType = radApplyTypeError.Checked;
                frm1.evtCloseParentForm += new EventHandler(frm1_evtCloseParentForm);
                frm1.ShowDialog();
            //}
        }

        private void frm1_evtCloseParentForm(object sender, EventArgs e)
        {
            //if (sender != null && sender is frmInputChangeOrder)
            //{

            //}
        }

        private void lnkType_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            isIMEIInput = false;
            strOutputVoucherID = string.Empty;
            strIMEI = string.Empty;

            if (txtOutputVoucherID.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập mã phiếu xuất hoặc IMEI.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtOutputVoucherID.Focus();
                return;
            }
            if (txtOutputVoucherID.Text.ToUpper().IndexOf("SO") > 0)
            {
                string strOV = LoadOVByOrder(txtOutputVoucherID.Text.Trim());
                if (strOV == string.Empty)
                    return;
                txtOutputVoucherID.Text = strOV;
            }
            intApplyErrorType = 0;
            if (radApplyTypeError.Checked)
                intApplyErrorType = 1;
            if (!GetInputChangeOrderType(this, txtOutputVoucherID.Text.Trim(), intApplyErrorType))
                return;
        }
        public bool GetInputChangeOrderType(IWin32Window window, string _strOutputVoucherID, int intApplyErrorType)
        {
            DataTable dtbInputChangeOrderType = objPLCInputChangeOrder.LoadInputChangeOrderType(_strOutputVoucherID, intApplyErrorType);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(window, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp);
                return false;
            }
            if (dtbInputChangeOrderType == null || dtbInputChangeOrderType.Rows.Count < 1)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(window, "Không tìm thấy loại yêu cầu nhập đổi/trả hàng phù hợp");
                return false;
            }
            strOutputVoucherID = dtbInputChangeOrderType.Rows[0]["OutputVoucherID"].ToString().Trim();
            if (strOutputVoucherID != txtOutputVoucherID.Text.Trim())
            {
                strIMEI = txtOutputVoucherID.Text.Trim();
                isIMEIInput = true;
            }

            Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbInputChangeOrderType, cboType);
            cboType.Enabled = true;
            btnInput.Enabled = true;
            return true;
        }
        private void ResetInputChangeOrderType()
        {
            cboType.DataSource = null;
            cboType.Enabled = false;
            btnInput.Enabled = false;
        }
        private string LoadOVByOrder(string strOrderID)
        {
            ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders objPLCSaleOrders = new SalesAndServices.SaleOrders.PLC.PLCSaleOrders();
            ERP.SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrder objSaleOrder = objPLCSaleOrders.LoadInfo(txtOutputVoucherID.Text.Trim());
            if (objSaleOrder == null || string.IsNullOrEmpty(objSaleOrder.SaleOrderID))
            {
                MessageBox.Show(this, "Mã đơn hàng không đúng. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtOutputVoucherID.Focus();
                return string.Empty;
            }
            DataTable dtbOV = objPLCOutputVoucher.GetListOV(txtOutputVoucherID.Text.Trim());
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp);
                return string.Empty;
            }
            if (dtbOV.Rows.Count < 1)
            {
                MessageBox.Show(this, "Không có phiếu xuất nào cho mã đơn hàng " + txtOutputVoucherID.Text.Trim() + ". Vui lòng kiểm tra lại mã đơn hàng " + txtOutputVoucherID.Text.Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (dtbOV.Rows.Count == 1)
            {
                return Convert.ToString(dtbOV.Rows[0]["OutputVoucherID"]).Trim();
            }
            frmSelectOV frm1 = new frmSelectOV(dtbOV);
            frm1.ShowDialog();
            return frm1.strOutputVoucherID.Trim();
        }
        private void txtOutputVoucherID_TextChanged(object sender, EventArgs e)
        {
            //if (txtOutputVoucherID.Text.Length > 0)
            //{
            //    txtOutputVoucherID.Text = txtOutputVoucherID.Text.ToUpper();
            //    txtOutputVoucherID.SelectionStart = txtOutputVoucherID.Text.Length;
            //}
            ResetInputChangeOrderType();
        }

        private void radApplyTypeError_CheckedChanged(object sender, EventArgs e)
        {
            ResetInputChangeOrderType();
        }

        private void radApplyTypeNoError_CheckedChanged(object sender, EventArgs e)
        {
            ResetInputChangeOrderType();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtOutputVoucherID.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập mã đơn hàng cho tìm kiếm phiếu xuất.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtOutputVoucherID.Focus();
                return;
            }
            string strOV = LoadOVByOrder(txtOutputVoucherID.Text.Trim());
            if (strOV != string.Empty)
                txtOutputVoucherID.Text = strOV;
        }

        private void txtOutputVoucherID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void GetParameterForm()
        {
            try
            {
                if (this.Tag == null || this.Tag.ToString() == string.Empty)
                {
                    IsViettel = false;
                    return;
                }
                Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                if (hstbParam.ContainsKey("TYPE"))
                    strCompany = (hstbParam["TYPE"] ?? "").ToString().Trim();
                if (string.IsNullOrEmpty(strCompany))
                {
                    IsViettel = false;
                }
                else if(strCompany.CompareTo("1")==0)
                    IsViettel = true;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi lấy tham số cấu hình form " + this.Text, objExce, "frmInputChangeOrderInput ->GetParameterForm");
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi lấy tham số cấu hình form " + this.Text);
                return;
            }
        }
    }
}
