﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Library.AppCore;
using Library.AppCore.LoadControls;
using Library.AppCore.Other;
using Library.AppCore.CustomControls.C1FlexGrid;
using System.IO;
using ERP.Inventory.PLC;
using ERP.Inventory.PLC.InputChangeOrder;

namespace ERP.Inventory.DUI.InputChangeOrder
{
    /// <summary>
    /// Created by: Đặng Minh Hùng
    /// Desc: Yêu cầu nhập đổi/trả hàng
    /// </summary>
    public partial class frmInputChangeOrder : Form
    {

        #region FormType
        public enum FormStateType
        {
            VIEW,
            ADD,
            EDIT
        }
        private FormStateType intFormState = FormStateType.VIEW;
        public FormStateType FormState
        {
            set
            {
                intFormState = value;
            }
            get
            {
                return intFormState;
            }
        }
        #endregion



        public frmInputChangeOrder()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        public frmInputChangeOrder(int intInputChangeOrderTypeID)
        {
            InitializeComponent();
            this.intInputChangeOrderTypeID = intInputChangeOrderTypeID;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        public frmInputChangeOrder(int intInputChangeOrderTypeID, string strInputChangeOrderID, bool bolIsDeleted, bool bolIsAllowEdit)
        {
            InitializeComponent();
            this.intInputChangeOrderTypeID = intInputChangeOrderTypeID;
            this.strInputChangeOrderID = strInputChangeOrderID;
            this.bolIsDeleted = bolIsDeleted;
            this.bolIsAllowEdit = bolIsAllowEdit;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        #region variable
        private int intInputChangeOrderTypeID = 0;
        private string strInputChangeOrderID = string.Empty;
        private bool bolIsDeleted = false;
        private MasterData.PLC.MD.PLCSaleOrderType objPLCSaleOrderType = new MasterData.PLC.MD.PLCSaleOrderType();
        private SalesAndServices.SaleOrders.PLC.PLCSaleOrders objPLCSaleOrders = new SalesAndServices.SaleOrders.PLC.PLCSaleOrders();
        private PLC.InputChangeOrder.PLCInputChangeOrder objPLCInputChangeOrder = new PLC.InputChangeOrder.PLCInputChangeOrder();
        private MasterData.PLC.MD.PLCInputChangeOrderType objPLCInputChangeOrderType = new MasterData.PLC.MD.PLCInputChangeOrderType();
        private MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType objInputChangeOrderType = null;
        private PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder objInputChangeOrder = null;
        private const string strFMSAplication = "FMSAplication_ProERP_InputChangeOrderAttachment";
        private List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment> lstInputChangeOrder_Attachment = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment>();
        private List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment> lstInputChangeOrder_Attachment_Del = null;
        private StringBuilder stbAttachmentLog = new StringBuilder();
        public string strOutputVoucherID = string.Empty;
        public event EventHandler evtCloseParentForm = null;
        public int intReportID = 0;
        private PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
        private PLC.Output.WSOutputVoucher.OutputVoucher objOutputVoucherLoad = new PLC.Output.WSOutputVoucher.OutputVoucher();
        private PLCProductInStock objPLCProductInStock = new PLCProductInStock();
        private SalesAndServices.Payment.PLC.PLCVoucher objPLCVoucher = new SalesAndServices.Payment.PLC.PLCVoucher();
        private int intReviewLevelCurrent = -1;
        private int intReviewStatus = -1;
        private int intWorkFlowCurrent = -1;
        private bool bolIsFinalLevel = false;
        private bool bolIsHasAction = false;
        private DataRow rOldEdit = null;
        private int intOldStoreID = -1;
        private DataTable dtbOVDetailReturn = null;
        private DataTable dtbInputTypeAll = null;
        private DataTable dtbOutputTypeAll = null;
        private DataTable tblInputChangeReasonAll = null;
        private bool bolIsLockByClosingDataMonth = false;
        private PLC.ProductChange.PLCProductChange objPLCProductChange = new PLC.ProductChange.PLCProductChange();
        private Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private decimal decVNDCash = 0;
        private decimal decVNDCash_In = 0;
        private decimal decVNDCash_Out = 0;
        private decimal decPaymentCardAmount = 0;
        private decimal decPaymentCardAmount_In = 0;
        private decimal decPaymentCardAmount_Out = 0;
        private int intPaymentCardID = 0;
        private int intPaymentCardID_In = 0;
        private int intPaymentCardID_Out = 0;
        private decimal decPaymentCardSpend = 0;
        private decimal decPaymentCardSpend_In = 0;
        private decimal decPaymentCardSpend_Out = 0;
        private string strPaymentCardVoucherID = string.Empty;
        private string strPaymentCardVoucherID_In = string.Empty;
        private string strPaymentCardVoucherID_Out = string.Empty;
        private string strPaymentCardName_In = string.Empty;
        private string strPaymentCardName = string.Empty;
        private decimal decRefundAmount_In = 0;
        private SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrder objSaleOrder_Out = null;
        private string strMessageCloseForm = string.Empty;
        private int intClosedForm = 0;
        private bool bolIsHasPermission_Create = false;
        private bool bolIsHasPermission_Edit = false;
        private bool bolIsHasPermission_EditAll = false;
        private bool bolIsHasPermission_Delete = false;
        private bool bolIsHasPermission_DeleteAll = false;
        private bool bolIsHasPermission_AddAttachment = false;
        private bool bolIsHasPermission_DelAttachment = false;
        private bool bolIsHasPermission_ViewAttachment = false;
        private bool bolIsHasPermission_AfterEditFunction = false;
        private bool bolIsHasPermission_AfterEditLimitFunction = false;
        private bool bolIsHasPermission_UpdateCare = false;
        private bool bolIsAllowEdit = false;
        private string strRightUpdateCare = "PM_INPUTCHANGEORDER_UPDCARE";
        private string strMessageForm = "nhập đổi trả hàng";
        private decimal decTotalPaid = 0;
        private decimal decOldVoucherDebt = 0;
        private string strInVoucherID = string.Empty;
        private bool bolIsDebt = false;
        private DataTable dtbDetailAll = null;
        private bool bolIsNotCreateOutVoucherDetail = false;
        private bool bolApplyErrorType = false;
        private List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail> lstAddVoucherDetail = new List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail>();
        private List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail> lstAddVoucherDetail_In = new List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail>();
        public bool ApplyErrorType
        {
            set { bolApplyErrorType = value; }
        }
        public bool IsHasAction
        {
            get { return bolIsHasAction; }
        }
        private bool bolIsPrivilege = false;
        public string strIMEIInputChange = string.Empty;

        public bool IsPrivilege
        {
            get { return bolIsPrivilege; }
            set { bolIsPrivilege = value; }
        }
        #endregion

        #region method
        bool bolAlowUpdateAfter = false;
        private bool LoadOutputVoucher(ref string strMessWarning)
        {
            try
            {
                if (strOutputVoucherID != string.Empty)
                {
                    objOutputVoucherLoad = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                    if (objOutputVoucherLoad != null)
                    {
                        //Lấy số tiền đã thu trong chi tiết phiếu thu
                        strInVoucherID = objPLCOutputVoucher.GetTotalPaidByOrderID(ref decTotalPaid, ref decOldVoucherDebt, objOutputVoucherLoad.OrderID, objOutputVoucherLoad.OutputVoucherID);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }

                        //Lấy chi tiết ban đầu của phiếu xuất(bao gồm luôn sp đã trả nếu có)
                        dtbDetailAll = objPLCOutputVoucher.GetAll(objOutputVoucherLoad.OutputVoucherID);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                        txtOutputVoucherID.Text = objOutputVoucherLoad.OutputVoucherID;
                        dtOutputDate.Value = objOutputVoucherLoad.OutputDate.Value;
                        try
                        {
                            bolIsLockByClosingDataMonth = ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.OUTPUTVOUCHER,
                                objOutputVoucherLoad.OutputDate.Value, objOutputVoucherLoad.OutputStoreID);
                        }
                        catch { }
                        txtInVoiceID.Text = objOutputVoucherLoad.InvoiceID;
                        txtInvoiceSymbol.Text = objOutputVoucherLoad.InvoiceSymbol;
                        ctrlStaffUser.UserName = objOutputVoucherLoad.StaffUser;
                        txtStaffUser.Text = ctrlStaffUser.UserName + " - " + ctrlStaffUser.FullName;

                        cboStore.SetValue(SystemConfig.intDefaultStoreID);
                        if (cboStore.StoreID < 1)
                            cboStore.SetValue(objOutputVoucherLoad.OutputStoreID);
                        if (cboStore.StoreID < 1 && ((DataTable)(cboStore.DataSource)).Rows.Count > 0)
                        {
                            cboStore.SetIndex(0);
                        }
                        intOldStoreID = cboStore.StoreID;

                        txtCustomerID.Text = objOutputVoucherLoad.CustomerID.ToString();
                        txtCustomerName.Text = objOutputVoucherLoad.CustomerName;
                        txtCustomerAddress.Text = objOutputVoucherLoad.CustomerAddress;
                        txtCustomerPhone.Text = objOutputVoucherLoad.CustomerPhone;
                        txtTaxID.Text = objOutputVoucherLoad.CustomerTaxID;
                        txtContentOV.Text = objOutputVoucherLoad.OutputContent;

                        if (strInputChangeOrderID == string.Empty)
                        {
                            dtbOVDetailReturn = objPLCInputChangeOrder.LoadOutputVoucherDetailForReturn(objOutputVoucherLoad.OutputVoucherID, intInputChangeOrderTypeID);
                            if (!string.IsNullOrWhiteSpace(strIMEIInputChange))
                            {
                                if (dtbOVDetailReturn != null && dtbOVDetailReturn.Rows.Count > 0)
                                {
                                    DataRow[] rows = dtbOVDetailReturn.AsEnumerable()
                                        .Where(r => !r.IsNull("IMEI") && (r.Field<string>("IMEI")).Trim() == strIMEIInputChange).ToArray();
                                    if (rows == null || rows.Count() == 0)
                                    {
                                        dtbOVDetailReturn.Rows.Clear();
                                    }
                                    else
                                    {
                                        dtbOVDetailReturn = rows.CopyToDataTable();
                                    }
                                }
                            }
                            dtbOVDetailReturn.DefaultView.Sort = "SalePrice DESC";
                            dtbOVDetailReturn = dtbOVDetailReturn.DefaultView.ToTable();

                            if (objInputChangeOrderType.IsNew == 1 || objInputChangeOrderType.IsNew == 0)
                            {
                                dtbOVDetailReturn.DefaultView.RowFilter = "IsNew = " + objInputChangeOrderType.IsNew;
                                dtbOVDetailReturn = dtbOVDetailReturn.DefaultView.ToTable();
                            }

                            string strOutputTypeFilter = string.Empty;
                            List<MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType_OType> LstInputChangeOrderType_OType = new List<MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType_OType>();
                            objPLCInputChangeOrderType.LoadInfo_OType(ref LstInputChangeOrderType_OType, objInputChangeOrderType.InputChangeOrderTypeID);

                            for (int i = 0; i < LstInputChangeOrderType_OType.Count; i++)
                            {
                                if (Convert.ToBoolean(LstInputChangeOrderType_OType[i].IsSelect))
                                    strOutputTypeFilter += LstInputChangeOrderType_OType[i].OutputTypeID.ToString() + ",";
                            }
                            strOutputTypeFilter = strOutputTypeFilter.Trim(',');
                            if (strOutputTypeFilter != string.Empty)
                            {
                                dtbOVDetailReturn.DefaultView.RowFilter = "IsPromotionOutputType = True OR OutputTypeID IN(" + strOutputTypeFilter + ")";
                                dtbOVDetailReturn = dtbOVDetailReturn.DefaultView.ToTable();
                            }

                            if (!dtbOVDetailReturn.Columns.Contains("IsSelect"))
                            {
                                dtbOVDetailReturn.Columns.Add("IsSelect", typeof(bool));
                                dtbOVDetailReturn.Columns["IsSelect"].DefaultValue = false;
                                dtbOVDetailReturn.Columns["IsSelect"].SetOrdinal(0);
                            }
                            int intOrdinal = dtbOVDetailReturn.Columns["Endwarrantydate"].Ordinal + 1;
                            dtbOVDetailReturn.Columns.Add("InputPrice_In", typeof(decimal));
                            dtbOVDetailReturn.Columns.Add("OutputTypeID_Out", typeof(int));
                            dtbOVDetailReturn.Columns.Add("OutputTypeName_Out", typeof(string));
                            dtbOVDetailReturn.Columns.Add("ProductID_Out", typeof(string));
                            dtbOVDetailReturn.Columns.Add("ProductName_Out", typeof(string));
                            dtbOVDetailReturn.Columns.Add("IMEI_Out", typeof(string));
                            dtbOVDetailReturn.Columns.Add("SalePrice_Out", typeof(decimal));
                            dtbOVDetailReturn.Columns["SalePrice_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["IMEI_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["ProductName_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["ProductID_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["OutputTypeName_Out"].SetOrdinal(intOrdinal);
                            dtbOVDetailReturn.Columns["InputPrice_In"].SetOrdinal(intOrdinal - 1);
                            dtbOVDetailReturn.Columns.Add("IsRequestIMEI_Out", typeof(bool));
                            dtbOVDetailReturn.Columns["IsRequestIMEI_Out"].DefaultValue = false;
                            if (!dtbOVDetailReturn.Columns.Contains("Quantity2"))
                                dtbOVDetailReturn.Columns.Add("Quantity2", typeof(decimal));
                            if (!dtbOVDetailReturn.Columns.Contains("TotalCost2"))
                                dtbOVDetailReturn.Columns.Add("TotalCost2", typeof(decimal));
                            if (!dtbOVDetailReturn.Columns.Contains("TotalFee2"))
                                dtbOVDetailReturn.Columns.Add("TotalFee2", typeof(decimal));

                            for (int i = dtbOVDetailReturn.Rows.Count - 1; i > -1; i--)
                            {
                                bool IsSelect = false;
                                if (dtbOVDetailReturn.Rows.Count < 2) IsSelect = true;
                                DataRow rDetail = dtbOVDetailReturn.Rows[i];
                                rDetail["IsSelect"] = IsSelect;
                                rDetail["IsRequestIMEI_Out"] = false;
                                rDetail["Quantity2"] = rDetail["Quantity"];
                                rDetail["TotalCost2"] = rDetail["TotalCost"];
                                rDetail["TotalFee2"] = rDetail["TotalFee"];
                                int intOutputType_Out = Convert.ToInt32(rDetail["OutputTypeID"]);
                                if (objInputChangeOrderType.IsInputChangeFree)
                                {
                                    rDetail["ReturnInputTypeID"] = objInputChangeOrderType.InputTypeID;
                                    if ((!objInputChangeOrderType.IsInputReturn) && objInputChangeOrderType.OutputTypeID > 0)
                                        intOutputType_Out = objInputChangeOrderType.OutputTypeID;
                                }
                                int intReturnInputTypeID = Convert.ToInt32(rDetail["ReturnInputTypeID"]);
                                ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(intOutputType_Out);
                                if (objOutputType_Out == null)
                                {
                                    flexDetail.Enabled = false;
                                    EnableControl(false);
                                    strMessWarning = "Bạn không có quyền trên hình thức xuất " + (objInputChangeOrderType.IsInputChangeFree == false ? rDetail["OutputTypeName"].ToString() : (objInputChangeOrderType.IsInputReturn == false ? " khi đổi trả không tính phí" : string.Empty)) + ". Vui lòng kiểm tra lại.";
                                    return false;
                                }

                                rDetail["OutputTypeID_Out"] = objOutputType_Out.OutputTypeID.ToString();
                                rDetail["OutputTypeName_Out"] = objOutputType_Out.OutputTypeName;

                                DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + intReturnInputTypeID);
                                if (drReturnInputType.Length < 1)
                                {
                                    flexDetail.Enabled = false;
                                    EnableControl(false);
                                    strMessWarning = "Bạn không có quyền trên hình thức nhập của hình thức xuất [" + ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(rDetail["OutputTypeID"])).OutputTypeName + "]. Vui lòng kiểm tra lại.";
                                    return false;
                                }
                                rDetail["InputPrice_In"] = Convert.ToDecimal(rDetail["InputSalePrice"]);
                                if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 2)
                                {
                                    rDetail["InputPrice_In"] = Convert.ToDecimal(rDetail["InputCostPrice"]);
                                    if (objInputChangeOrderType.IsInputReturn)
                                    {
                                        rDetail["SalePrice"] = rDetail["CostPrice"];
                                        rDetail["TotalCost"] = rDetail["TotalCostPrice"];
                                        rDetail["TotalCost2"] = rDetail["TotalCostPrice"];
                                    }
                                }
                                else if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 3)
                                    rDetail["InputPrice_In"] = 0;
                                //xét HT nhập có lấy VAT
                                if (!Convert.ToBoolean(drReturnInputType[0]["IsVATZero"]))
                                    rDetail["InputPrice_In"] = Convert.ToDecimal(rDetail["InputPrice_In"]) * (Convert.ToDecimal(1) + Convert.ToDecimal(rDetail["VAT"]) * Convert.ToDecimal(rDetail["VATPercent"]) * Convert.ToDecimal(0.0001));

                                rDetail["InputTypeName"] = drReturnInputType[0]["InputTypeName"];
                            }
                            //if (bolIsPrivilege)
                            //{
                            //    if (dtbOVDetailReturn != null && dtbOVDetailReturn.Rows.Count > 0)
                            //    {
                            //        DataTable dtbTemp = dtbOVDetailReturn.Clone();
                            //        foreach (DataRow dr in dtbOVDetailReturn.Rows)
                            //        {
                            //            string strIMEI = dr["IMEI"].ToString().Trim();
                            //            if (!string.IsNullOrWhiteSpace(strIMEI))
                            //            {
                            //                bool bolResult = new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().CheckPrivilegeIMEI(strIMEI);
                            //                if (bolResult)
                            //                {
                            //                    dtbTemp.ImportRow(dr);
                            //                }
                            //            }
                            //        }
                            //        if (dtbTemp != null && dtbTemp.Rows.Count > 0)
                            //            dtbOVDetailReturn = dtbTemp;
                            //    }
                            //}
                            if (dtbOVDetailReturn.Rows.Count < 1)
                            {
                                flexDetail.Enabled = false;
                                EnableControl(false);
                                if (!string.IsNullOrWhiteSpace(strIMEIInputChange))
                                {
                                    strMessWarning = "IMEI " + strIMEIInputChange + " không hợp lệ để tạo yêu cầu " + strMessageForm + ". Vui lòng kiểm tra lại.";
                                }
                                else
                                {
                                    strMessWarning = "Phiếu xuất " + strOutputVoucherID + " không có sản phẩm(hoặc IMEI) nào để tạo yêu cầu " + strMessageForm + ". Vui lòng kiểm tra lại.";
                                }
                                return false;
                            }
                            flexDetail.DataSource = dtbOVDetailReturn;

                            if (!objInputChangeOrderType.IsInputReturn)
                                FormatFlex();
                            else
                                FormatFlexReturn();
                        }
                        else
                        {
                            dtbOVDetailReturn = objPLCInputChangeOrder.GetDetail(new object[] { "@InputChangeOrderID", strInputChangeOrderID, "@IncludeDeleted", bolIsDeleted });
                            if (dtbOVDetailReturn.Rows.Count < 1)
                            {
                                flexDetail.Enabled = false;
                                EnableControl(false);
                                if (!string.IsNullOrWhiteSpace(strIMEIInputChange))
                                {
                                    strMessWarning = "IMEI " + strIMEIInputChange + " không hợp lệ để tạo yêu cầu " + strMessageForm + ". Vui lòng kiểm tra lại.";
                                }
                                else
                                {
                                    strMessWarning = "Phiếu xuất " + strOutputVoucherID + " không có sản phẩm(hoặc IMEI) nào để tạo yêu cầu " + strMessageForm + ". Vui lòng kiểm tra lại.";
                                }
                                return false;
                            }

                            for (int i = 0; i < dtbOVDetailReturn.Rows.Count; i++)
                            {
                                DataRow rReturn = dtbOVDetailReturn.Rows[i];
                                ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = null;
                                if (!objInputChangeOrderType.IsInputReturn)
                                {
                                    if (string.IsNullOrEmpty(txtSaleOrderID.Text))
                                    {
                                        if (objInputChangeOrderType.IsInputChangeFree)
                                        {
                                            objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(objInputChangeOrderType.OutputTypeID);
                                            if (objOutputType_Out != null)
                                            {
                                                rReturn["OutputTypeName_Out"] = objOutputType_Out.OutputTypeName;
                                                rReturn["OutputTypeID_Out"] = objOutputType_Out.OutputTypeID;
                                            }
                                            else
                                            {
                                                flexDetail.Enabled = false;
                                                EnableControl(false);
                                                strMessWarning = "Bạn không có quyền trên hình thức xuất mã [" + objInputChangeOrderType.OutputTypeID.ToString() + "] khi đổi trả không tính phí. Vui lòng kiểm tra lại.";
                                                return false;
                                            }
                                        }
                                        else
                                            objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(rReturn["OutputTypeID_Out"]));
                                        if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                                        {
                                            ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rReturn["ProductID_Out"].ToString().Trim());
                                            rReturn["SalePrice_Out"] = Convert.ToDecimal(rReturn["SalePrice_Out"]) * (Convert.ToDecimal(1) + Convert.ToDecimal(objProductOut.VAT) * Convert.ToDecimal(objProductOut.VATPercent) * Convert.ToDecimal(0.0001));
                                        }
                                        else
                                        {
                                            if (objOutputType_Out == null)
                                            {
                                                flexDetail.Enabled = false;
                                                EnableControl(false);
                                                strMessWarning = "Bạn không có quyền trên hình thức xuất [" + rReturn["OutputTypeID_Out"].ToString() + " - " + rReturn["OutputTypeName_Out"].ToString() + "]. Vui lòng kiểm tra lại.";
                                                return false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        rReturn["OutputTypeID_Out"] = 0;
                                        rReturn["OutputTypeName_Out"] = string.Empty;
                                        rReturn["SalePrice_Out"] = 0;
                                    }
                                }
                                else
                                {
                                    rReturn["OutputTypeID_Out"] = 0;
                                    rReturn["OutputTypeName_Out"] = string.Empty;
                                    rReturn["SalePrice_Out"] = 0;
                                }

                                if (objInputChangeOrderType.IsInputChangeFree)
                                    rReturn["ReturnInputTypeID"] = objInputChangeOrderType.InputTypeID;

                                DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + Convert.ToInt32(rReturn["ReturnInputTypeID"]));
                                if (drReturnInputType.Length < 1)
                                {
                                    flexDetail.Enabled = false;
                                    EnableControl(false);
                                    strMessWarning = "Bạn không có quyền trên hình thức nhập của hình thức xuất [" + rReturn["OutputTypeID_Out"].ToString() + " - " + rReturn["OutputTypeName_Out"].ToString() + "]. Vui lòng kiểm tra lại.";
                                    return false;
                                }
                                //xét HT nhập có lấy VAT
                                if (!Convert.ToBoolean(drReturnInputType[0]["IsVATZero"]))
                                {
                                    if (Convert.ToDecimal(rReturn["Quantity"]) > 1)
                                        rReturn["InputPrice_In"] = Convert.ToDecimal(rReturn["InputPrice_In"]) * (Convert.ToDecimal(1) + Convert.ToDecimal(rReturn["VAT"]) * Convert.ToDecimal(rReturn["VATPercent"]) * Convert.ToDecimal(0.0001));
                                    else
                                        rReturn["InputPrice_In"] = Math.Round(Convert.ToDecimal(rReturn["InputPrice_In"]) * (Convert.ToDecimal(1) + Convert.ToDecimal(rReturn["VAT"]) * Convert.ToDecimal(rReturn["VATPercent"]) * Convert.ToDecimal(0.0001)));
                                }
                            }
                            flexDetail.DataSource = dtbOVDetailReturn;
                            flexDetail.AllowEditing = false;
                            FormatFlexDetail();
                            decimal decSumInputPrice = 0;
                            decimal decSumSalePrice = 0;
                            if (!objInputChangeOrder.IsInputChanged)
                            {
                                decSumInputPrice = GetCompute_In(string.Empty);
                                decSumSalePrice = GetCompute_Out(string.Empty);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objInputChangeOrder.OutVoucherID))
                                {
                                    ERP.SalesAndServices.Payment.PLC.WSVoucher.Voucher objVoucher = objPLCVoucher.LoadInfo(objInputChangeOrder.OutVoucherID);
                                    if (objVoucher != null)
                                        decSumInputPrice = objVoucher.TotalLiquidate - objVoucher.Debt;
                                }
                                if (!string.IsNullOrEmpty(objInputChangeOrder.InVoucherID))
                                {
                                    ERP.SalesAndServices.Payment.PLC.WSVoucher.Voucher objVoucher = objPLCVoucher.LoadInfo(objInputChangeOrder.InVoucherID);
                                    if (objVoucher != null)
                                        decSumSalePrice = objVoucher.TotalLiquidate - objVoucher.Debt;
                                }
                            }
                            if (Math.Round(decSumInputPrice) > Math.Round(decSumSalePrice))
                            {
                                lblTitlePayment.Text = "Phải chi";
                                txtReturnAmount.Text = (decSumInputPrice - decSumSalePrice).ToString("#,##0");
                            }
                            else
                            {
                                if (Math.Round(decSumInputPrice) == Math.Round(decSumSalePrice))
                                    lblTitlePayment.Text = "Phải chi";
                                else
                                    lblTitlePayment.Text = "Phải thu";
                                txtReturnAmount.Text = (decSumSalePrice - decSumInputPrice).ToString("#,##0");
                            }
                        }
                    }
                }
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy thông tin phiếu xuất");
                SystemErrorWS.Insert("Lỗi lấy thông tin phiếu xuất", objExec, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void LoadInputType()
        {
            try
            {
                if (strInputChangeOrderID == string.Empty)
                {
                    bool bolIsSelected = DataTableClass.CheckIsExist(dtbOVDetailReturn, "IsSelect = 1");
                    String strFilter = "";
                    if (bolIsSelected)
                        strFilter = "IsSelect = 1";
                    DataTable tblInputTypeFilter = DataTableClass.SelectDistinct(dtbOVDetailReturn, "ReturnInputTypeID", strFilter);
                    DataTable tblInputType = dtbInputTypeAll.Clone();
                    for (int i = 0; i < tblInputTypeFilter.Rows.Count; i++)
                    {
                        DataRow[] rInputType = dtbInputTypeAll.Select("InputTypeID = " + Convert.ToInt32(tblInputTypeFilter.Rows[i]["ReturnInputTypeID"]));
                        DataRow r = tblInputType.NewRow();
                        r.ItemArray = rInputType[0].ItemArray;
                        tblInputType.Rows.Add(r);
                    }
                    cboInputTypeID.Title = "--Chọn hình thức nhập--";
                    cboInputTypeID.InitControl(false, tblInputType.Copy());
                    if (tblInputType.Rows.Count == 1)
                        cboInputTypeID.SetIndex(0);
                }
                else
                {
                    int intReturnInputTypeID = Convert.ToInt32(dtbOVDetailReturn.Rows[0]["ReturnInputTypeID"]);
                    DataTable tblInputType = dtbInputTypeAll.Clone();
                    DataRow[] drInputType = dtbInputTypeAll.Select("InputTypeID = " + intReturnInputTypeID);
                    DataRow rInputType = tblInputType.NewRow();
                    rInputType.ItemArray = drInputType[0].ItemArray;
                    tblInputType.Rows.Add(rInputType);
                    cboInputTypeID.Title = "--Chọn hình thức nhập--";
                    cboInputTypeID.InitControl(false, tblInputType);
                    if (tblInputType.Rows.Count == 1)
                        cboInputTypeID.SetIndex(0);
                }
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy hình thức nhập");
                SystemErrorWS.Insert("Lỗi lấy hình thức nhập", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        private bool LoadCombobox()
        {
            try
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilter.IsCheckPermission = true;
                objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.OUTPUT;
                cboStore.InitControl(false, objStoreFilter);
                cboInputChangeOrderStoreID.InitControl(false, objStoreFilter);
                dtbInputTypeAll = Library.AppCore.DataSource.GetDataSource.GetInputType();
                dtbOutputTypeAll = Library.AppCore.DataSource.GetDataSource.GetOutputType();

                tblInputChangeReasonAll = Library.AppCore.DataSource.GetDataSource.GetInputChangeReason().Copy();
                DataTable tblInputChangeReason_Filter = tblInputChangeReasonAll.Clone();
                DataTable tblInputChangeReason_InputChangeOrderType = null;
                objPLCInputChangeOrderType.GetData_Reason(ref tblInputChangeReason_InputChangeOrderType, intInputChangeOrderTypeID);
                if (tblInputChangeReason_InputChangeOrderType != null && tblInputChangeReason_InputChangeOrderType.Rows.Count > 0)
                {
                    tblInputChangeReason_InputChangeOrderType.DefaultView.RowFilter = "ISSELECT = True";
                    tblInputChangeReason_InputChangeOrderType = tblInputChangeReason_InputChangeOrderType.DefaultView.ToTable();
                    if (tblInputChangeReason_InputChangeOrderType.Rows.Count > 0)
                    {
                        string strInputChangeReasonIDList = string.Empty;
                        for (int i = 0; i < tblInputChangeReason_InputChangeOrderType.Rows.Count; i++)
                        {
                            DataRow rReason = tblInputChangeReason_InputChangeOrderType.Rows[i];
                            strInputChangeReasonIDList += rReason["INPUTCHANGEREASONID"].ToString() + ",";
                        }
                        strInputChangeReasonIDList = strInputChangeReasonIDList.Trim(',');
                        tblInputChangeReasonAll.DefaultView.RowFilter = "INPUTCHANGEREASONID IN (" + strInputChangeReasonIDList + ")";
                        tblInputChangeReason_Filter = tblInputChangeReasonAll.DefaultView.ToTable();
                    }
                }
                cboReasonOV.InitControl(false, tblInputChangeReason_Filter.Copy(), "INPUTCHANGEREASONID", "INPUTCHANGEREASONNAME", "--Chọn lý do đổi trả--");
                cboReason.InitControl(false, tblInputChangeReason_Filter.Copy(), "INPUTCHANGEREASONID", "INPUTCHANGEREASONNAME", "--Chọn lý do đổi trả--");
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nạp combobox");
                SystemErrorWS.Insert("Lỗi nạp combobox", objExec, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        public float GetCurrencyExchange(int CurrencyUnitID)
        {
            DataTable dtbCurrencyUnit = Library.AppCore.DataSource.GetDataSource.GetCurrencyUnit();
            DataRow[] rows = dtbCurrencyUnit.Select("CURRENCYUNITID = " + CurrencyUnitID);
            if (rows.Length < 1)
            {
                return 0;
            }
            return Convert.ToSingle(rows[0]["CURRENCYEXCHANGE"]);
        }

        private bool CheckAdd()
        {
            if (cboStore.StoreID < 1)
            {
                cboStore.Focus();
                MessageBox.Show(this, "Bạn chưa chọn kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (cboInputTypeID.InputTypeID < 1)
            {
                MessageBox.Show(this, "Bạn chỉ được phép chọn các sản phẩm cùng hình thức nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtContentIOT.Text))
            {
                MessageBox.Show(this, "Vui lòng nhập nội dung yêu cầu nhập đổi trả", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtContentIOT.Focus();
                return false;
            }

            dtbOVDetailReturn.AcceptChanges();

            DataRow[] rIsSelect = dtbOVDetailReturn.Select("IsSelect = 1");
            if (rIsSelect.Length < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất 1 sản phẩm cần " + strMessageForm, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            DataRow[] rSelectNew = dtbOVDetailReturn.Select("IsSelect = 1 AND IsNew = True");
            DataRow[] rSelectOld = dtbOVDetailReturn.Select("IsSelect = 1 AND IsNew = False");
            if (rSelectNew.Length > 0 && rSelectOld.Length > 0)
            {
                MessageBox.Show(this, "Không thể " + strMessageForm + " cùng lúc sản phẩm mới và cũ. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            ERP.Inventory.PLC.Input.PLCInputVoucherReturn objPLCInputVoucherReturn = new ERP.Inventory.PLC.Input.PLCInputVoucherReturn();
            DataTable dtbIMEIWarranty = null;
            if ((!objInputChangeOrderType.IsInputReturn) && !chkCreateSaleOrder.Checked)
            {
                //nếu là loại nhập đổi trả hàng
                for (int i = 0; i < rIsSelect.Length; i++)
                {
                    if (rIsSelect[i]["ProductID_Out"] == DBNull.Value || rIsSelect[i]["ProductID_Out"].ToString().Trim() == string.Empty)
                    {
                        MessageBox.Show(this, "Vui lòng nhập sản phẩm xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    if (Convert.ToBoolean(rIsSelect[i]["IsRequestIMEI_Out"]))
                    {
                        if (rIsSelect[i]["IMEI_Out"] == DBNull.Value || rIsSelect[i]["IMEI_Out"].ToString().Trim() == string.Empty)
                        {
                            MessageBox.Show(this, "Vui lòng nhập IMEI xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }

                        dtbIMEIWarranty = objPLCInputVoucherReturn.LoadOutputVoucherWarrantyExtend(Convert.ToString(objOutputVoucherLoad.OutputVoucherID).Trim(), rIsSelect[i]["IMEI"].ToString().Trim());
                        if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                        if (dtbIMEIWarranty.Rows.Count > 0)
                        {
                            if (rIsSelect[i]["ProductID"].ToString().Trim() != rIsSelect[i]["ProductID_Out"].ToString().Trim() || Math.Round(Convert.ToDecimal(rIsSelect[i]["InputPrice_In"])) != Math.Round(Convert.ToDecimal(rIsSelect[i]["SalePrice_Out"])))
                            {
                                if (Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim() != string.Empty && Convert.ToString(dtbIMEIWarranty.Rows[0]["IMEI"]).Trim() == Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim())
                                {
                                    if (MessageBox.Show(this, "Phiếu xuất " + Convert.ToString(dtbIMEIWarranty.Rows[0]["OutputVoucherExtID"]).Trim() + " chưa nhập trả Care+\r\nNếu đồng ý IMEI sẽ không xuất được Care+ cho lần sau\r\nBạn có chắc muốn đồng ý không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                        return false;
                                }
                            }
                            else
                            {
                                if (Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim() != string.Empty && Convert.ToString(dtbIMEIWarranty.Rows[0]["IMEI"]).Trim() == Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim())
                                {
                                    if (objInputChangeOrderType.IsContinueInstallInfo)
                                    {
                                        if (MessageBox.Show(this, "IMEI " + rIsSelect[i]["IMEI"].ToString().Trim() + " sẽ chuyển tiếp Care+ cho IMEI " + rIsSelect[i]["IMEI_Out"].ToString().Trim() + "\r\nBạn có chắc muốn đồng ý không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                            return false;
                                    }
                                    else
                                    {
                                        if (MessageBox.Show(this, "Phiếu xuất " + Convert.ToString(dtbIMEIWarranty.Rows[0]["OutputVoucherExtID"]).Trim() + " chưa nhập trả Care+\r\nNếu đồng ý IMEI sẽ không xuất được Care+ cho lần sau\r\nBạn có chắc muốn đồng ý không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                            return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                DataRow[] drIMEISelect = dtbOVDetailReturn.Select("IsSelect = 1 AND IMEI IS NOT NULL");
                if (drIMEISelect.Length > 0)
                {
                    for (int i = 0; i < drIMEISelect.Length; i++)
                    {
                        string strIMEI = Convert.ToString(drIMEISelect[i]["IMEI"]).Trim();
                        if (strIMEI != string.Empty)
                        {
                            dtbIMEIWarranty = objPLCInputVoucherReturn.LoadOutputVoucherWarrantyExtend(Convert.ToString(objOutputVoucherLoad.OutputVoucherID).Trim(), strIMEI);
                            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                            {
                                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                                return false;
                            }
                            if (dtbIMEIWarranty.Rows.Count > 0)
                            {
                                if (Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim() != string.Empty && Convert.ToString(dtbIMEIWarranty.Rows[0]["IMEI"]).Trim() == Convert.ToString(dtbIMEIWarranty.Rows[0]["ExtendWarrantyIMEI"]).Trim())
                                {
                                    if (MessageBox.Show(this, "Phiếu xuất " + Convert.ToString(dtbIMEIWarranty.Rows[0]["OutputVoucherExtID"]).Trim() + " chưa nhập trả Care+\r\nNếu đồng ý IMEI sẽ không xuất được Care+ cho lần sau\r\nBạn có chắc muốn đồng ý không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                        return false;
                                }
                            }
                        }
                    }
                }
            }

            decimal decInvoice = 0;
            try
            {
                decInvoice = Convert.ToDecimal(txtInVoiceID.Text);
            }
            catch { }
            if (decInvoice != 0)
            {
                if (MessageBox.Show(this, "Phiếu xuất trước đó đã In hóa đơn\r\nBạn có chắc đã nắm thông tin?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    return false;
            }
            return true;
        }

        private bool CheckCreateConcern()
        {
            if (cboStore.StoreID < 1)
            {
                MessageBox.Show(this, "Bạn không có quyền trên mã kho nhập " + objInputChangeOrder.InputChangeOrderStoreID, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            strPaymentCardName = string.Empty;
            dtbOVDetailReturn.AcceptChanges();
            decimal decSumInputPrice = GetCompute_In(string.Empty);
            decimal decSumSalePrice = GetCompute_Out(string.Empty);
            if (!string.IsNullOrEmpty(txtSaleOrderID.Text))
            {
                bool bolIsOKPayment = false;
                if ((decVNDCash_Out + decPaymentCardAmount_Out) != Math.Round(decSumInputPrice))
                {
                    POPUP:
                    decimal decVNDCashTMP = decVNDCash_Out;
                    if (decVNDCashTMP == 0)
                        decVNDCashTMP = Math.Round(decSumInputPrice);
                    string PaymentCardName = string.Empty;
                    ShowPaymentForm1(true, ref bolIsOKPayment, "Chi tiền nhập trả", decVNDCashTMP, decPaymentCardAmount, intPaymentCardID, decPaymentCardSpend, strPaymentCardVoucherID, Math.Round(decSumInputPrice),
                                        ref decVNDCash_Out, ref decPaymentCardAmount_Out, ref intPaymentCardID, ref PaymentCardName, ref decPaymentCardSpend, ref strPaymentCardVoucherID, ref lstAddVoucherDetail);
                    if (bolIsOKPayment)
                    {
                        if (lstAddVoucherDetail != null && lstAddVoucherDetail.Count > 0)
                        {
                            decVNDCash_Out = lstAddVoucherDetail.Sum(x => x.VNDCash);
                            decPaymentCardAmount_Out = lstAddVoucherDetail.Sum(x => x.PaymentCardAmount);
                        }
                        if ((decVNDCash_Out + decPaymentCardAmount_Out) == Math.Round(decSumInputPrice))
                            bolIsOKPayment = true;
                        else
                        {
                            if (Math.Round(decVNDCash_Out + decPaymentCardAmount_Out) == 0)
                            {
                                if (MessageBox.Show(this, "Phiêu chi chưa chi hết. Bạn có chắc muốn tạo phiếu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                MessageBox.Show(this, "Phải chi đúng số tiền chi nhập trả", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                goto POPUP;
                            }
                        }
                    }
                }
                else
                    bolIsOKPayment = true;

                bool bolIsOKPayment_In = false;
                if (bolIsOKPayment)
                {
                    if ((decVNDCash_In + decPaymentCardAmount_In) != Math.Round(decSumSalePrice))
                    {
                        ShowPaymentForm1(false, ref bolIsOKPayment_In, "Thu tiền thanh toán đơn hàng", decVNDCash_In, decPaymentCardAmount_In, intPaymentCardID_In, decPaymentCardSpend_In, strPaymentCardVoucherID_In, Math.Round(decSumSalePrice),
                                            ref decVNDCash_In, ref decPaymentCardAmount_In, ref intPaymentCardID_In, ref strPaymentCardName_In, ref decPaymentCardSpend_In, ref strPaymentCardVoucherID_In, ref lstAddVoucherDetail_In);
                        if (bolIsOKPayment_In)
                        {
                            if (lstAddVoucherDetail_In != null && lstAddVoucherDetail_In.Count > 0)
                            {
                                decVNDCash_In = lstAddVoucherDetail_In.Sum(x => x.VNDCash);
                                decPaymentCardAmount_In = lstAddVoucherDetail_In.Sum(x => x.PaymentCardAmount);
                            }
                            if ((decVNDCash_In + decPaymentCardAmount_In) == Math.Round(decSumSalePrice))
                                bolIsOKPayment_In = true;
                        }
                    }
                    else
                        bolIsOKPayment_In = true;
                }

                if ((!bolIsOKPayment) || (!bolIsOKPayment_In))
                    return false;
                decVNDCash = decVNDCash_Out;
                decPaymentCardAmount = decPaymentCardAmount_Out;
            }
            else
            {
                if ((decVNDCash + decPaymentCardAmount) != Math.Abs(Math.Round(decSumSalePrice - decSumInputPrice)))
                {
                    bool bolIsOKPayment = false;
                    string strTextForm = string.Empty;
                    bool bolIsSpend = true;
                    if (objInputChangeOrderType.IsInputReturn)
                    {
                        strTextForm = "Chi tiền nhập trả";
                    }
                    if (Math.Round(decSumSalePrice) > Math.Round(decSumInputPrice))
                    {
                        bolIsSpend = false;
                    }
                    ShowPaymentForm1(bolIsSpend, ref bolIsOKPayment, strTextForm, decVNDCash, decPaymentCardAmount, intPaymentCardID, decPaymentCardSpend, strPaymentCardVoucherID, Convert.ToDecimal(txtReturnAmount.Text),
                                        ref decVNDCash, ref decPaymentCardAmount, ref intPaymentCardID, ref strPaymentCardName, ref decPaymentCardSpend, ref strPaymentCardVoucherID, ref lstAddVoucherDetail);
                    if (bolIsOKPayment)
                    {
                        if (lstAddVoucherDetail != null && lstAddVoucherDetail.Count > 0)
                        {
                            decVNDCash = lstAddVoucherDetail.Sum(x => x.VNDCash);
                            decPaymentCardAmount = lstAddVoucherDetail.Sum(x => x.PaymentCardAmount);
                        }
                        if ((decVNDCash + decPaymentCardAmount) == Math.Abs(Math.Round((decSumSalePrice - decSumInputPrice), MidpointRounding.AwayFromZero)) || bolIsSpend)
                        {
                            return true;
                        }
                        MessageBox.Show(this, "Phải thanh toán đúng số tiền thu/chi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    return false;
                }
            }
            return true;
        }

        private void ClearDetail()
        {
            for (int i = 0; i < dtbOVDetailReturn.Rows.Count; i++)
            {
                DataRow r = dtbOVDetailReturn.Rows[i];
                r["ProductID_Out"] = string.Empty;
                r["ProductName_Out"] = string.Empty;
                r["IMEI_Out"] = string.Empty;
                r["SalePrice_Out"] = DBNull.Value;
                r["IsRequestIMEI_Out"] = false;
            }
        }

        private void FormatFlex()
        {
            flexDetail.Rows.Fixed = 1;
            for (int i = 0; i < flexDetail.Cols.Count; i++)
            {
                flexDetail.Cols[i].Visible = false;
                flexDetail.Cols[i].AllowSorting = false;
                flexDetail.Cols[i].AllowDragging = false;
                flexDetail.Cols[i].AllowEditing = false;
            }
            flexDetail.Cols["IsSelect"].Visible = true;
            flexDetail.Cols["ProductID"].Visible = true;
            flexDetail.Cols["ProductName"].Visible = true;
            flexDetail.Cols["IMEI"].Visible = true;
            flexDetail.Cols["EndWarrantyDate"].Visible = true;
            flexDetail.Cols["InputPrice_In"].Visible = true;
            flexDetail.Cols["Quantity"].Visible = true;
            flexDetail.Cols["OutputTypeName_Out"].Visible = true;
            flexDetail.Cols["ProductID_Out"].Visible = true;
            flexDetail.Cols["ProductName_Out"].Visible = true;
            flexDetail.Cols["IMEI_Out"].Visible = true;
            flexDetail.Cols["SalePrice_Out"].Visible = true;
            flexDetail.Cols["IsSelect"].Caption = "Chọn";
            flexDetail.Cols["ProductID"].Caption = "Mã sản phẩm nhập";
            flexDetail.Cols["ProductName"].Caption = "Tên sản phẩm nhập";
            flexDetail.Cols["IMEI"].Caption = "IMEI nhập";
            flexDetail.Cols["InputPrice_In"].Caption = "Giá nhập";
            flexDetail.Cols["OutputTypeName_Out"].Caption = "Hình thức xuất";
            flexDetail.Cols["ProductID_Out"].Caption = "Mã sản phẩm xuất";
            flexDetail.Cols["ProductName_Out"].Caption = "Tên sản phẩm xuất";
            flexDetail.Cols["IMEI_Out"].Caption = "IMEI xuất";
            flexDetail.Cols["EndWarrantyDate"].Caption = "Ngày bảo hành";
            flexDetail.Cols["SalePrice_Out"].Caption = "Giá bán";
            flexDetail.Cols["Quantity"].Caption = "Số lượng";

            flexDetail.Cols["IsSelect"].Width = 40;
            flexDetail.Cols["ProductID"].Width = 120;
            flexDetail.Cols["ProductName"].Width = 140;
            flexDetail.Cols["IMEI"].Width = 120;
            flexDetail.Cols["EndWarrantyDate"].Width = 80;
            flexDetail.Cols["InputPrice_In"].Width = 70;
            flexDetail.Cols["OutputTypeName_Out"].Width = 100;
            flexDetail.Cols["ProductID_Out"].Width = 120;
            flexDetail.Cols["ProductName_Out"].Width = 140;
            flexDetail.Cols["IMEI_Out"].Width = 120;
            flexDetail.Cols["SalePrice_Out"].Width = 70;
            flexDetail.Cols["Quantity"].Width = 50;

            flexDetail.Cols["InputPrice_In"].Format = "#,##0";
            flexDetail.Cols["SalePrice_Out"].Format = "#,##0";
            flexDetail.Cols["Quantity"].Format = "#,##0";
            flexDetail.Cols["EndWarrantyDate"].Format = "dd/MM/yyyy";
            flexDetail.Cols["IsSelect"].DataType = typeof(bool);
            flexDetail.Cols["IsSelect"].AllowEditing = true;
            flexDetail.Cols["ProductID_Out"].AllowEditing = true;
            flexDetail.Cols["IMEI_Out"].AllowEditing = true;
            flexDetail.Cols["Quantity"].AllowEditing = true;

            flexDetail.Cols["ProductID_Out"].ComboList = "...";

            C1.Win.C1FlexGrid.CellStyle style = flexDetail.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexDetail.GetCellRange(0, 0, 0, flexDetail.Cols.Count - 1);
            range.Style = style;
            flexDetail.Rows[0].Height = 40;
            flexDetail.Cols.Fixed = 1;

            this.flexDetail.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexDetail_BeforeEdit);
        }

        private void FormatFlexDetail()
        {
            flexDetail.Rows.Fixed = 1;
            for (int i = 0; i < flexDetail.Cols.Count; i++)
            {
                flexDetail.Cols[i].Visible = false;
                flexDetail.Cols[i].AllowSorting = false;
                flexDetail.Cols[i].AllowDragging = false;
                flexDetail.Cols[i].AllowEditing = false;
            }
            if (!objInputChangeOrderType.IsInputReturn)
            {
                CustomizeC1FlexGrid.SetColumnVisible(flexDetail, true, true, "ProductID,ProductName,IMEI,InputPrice_In,EndWarrantyDate," +
                                            "OutputTypeName_Out,ProductID_Out,ProductName_Out,IMEI_Out,SalePrice_Out,Quantity");
                CustomizeC1FlexGrid.SetColumnCaption(flexDetail, "ProductID,Mã sản phẩm nhập,ProductName,Tên sản phẩm nhập,IMEI,IMEI nhập,InputPrice_In,Giá nhập,EndWarrantyDate,Ngày bảo hành," +
                                            "OutputTypeName_Out,Hình thức xuất,ProductID_Out,Mã sản phẩm xuất,ProductName_Out,Tên sản phẩm xuất,IMEI_Out,IMEI xuất,SalePrice_Out,Giá bán,Quantity,Số lượng");
                CustomizeC1FlexGrid.SetColumnWidth(flexDetail, "ProductID,120,ProductName,140,IMEI,120,InputPrice_In,70,EndWarrantyDate,80," +
                                            "OutputTypeName_Out,100,ProductID_Out,120,ProductName_Out,140,IMEI_Out,120,SalePrice_Out,70,Quantity,50");
            }
            else
            {
                CustomizeC1FlexGrid.SetColumnVisible(flexDetail, true, true, "ProductID,ProductName,IMEI,SalePrice,EndWarrantyDate,TotalFee,InputPrice_In,Quantity");
                CustomizeC1FlexGrid.SetColumnCaption(flexDetail, "ProductID,Mã sản phẩm,ProductName,Tên sản phẩm,IMEI,IMEI,SalePrice,Đơn giá,EndWarrantyDate,Ngày bảo hành,TotalFee,Phí trả,InputPrice_In,Giá nhập,Quantity,Số lượng nhập trả");
                CustomizeC1FlexGrid.SetColumnWidth(flexDetail, "ProductID,120,ProductName,250,IMEI,180,SalePrice,90,TotalFee,90,EndWarrantyDate,110,InputPrice_In,90,Quantity,90");
            }
            flexDetail.Cols["SalePrice"].Format = "#,##0";
            flexDetail.Cols["TotalFee"].Format = "#,##0";
            flexDetail.Cols["InputPrice_In"].Format = "#,##0";
            flexDetail.Cols["SalePrice_Out"].Format = "#,##0";
            flexDetail.Cols["Quantity"].Format = "#,##0";
            flexDetail.Cols["EndWarrantyDate"].Format = "dd/MM/yyyy";

            C1.Win.C1FlexGrid.CellStyle style = flexDetail.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexDetail.GetCellRange(0, 0, 0, flexDetail.Cols.Count - 1);
            range.Style = style;
            flexDetail.Rows[0].Height = 40;
            flexDetail.Cols.Fixed = 1;
        }

        private void FormatFlexReturn()
        {
            for (int i = 0; i < flexDetail.Cols.Count; i++)
            {
                flexDetail.Cols[i].Visible = false;
                flexDetail.Cols[i].AllowSorting = false;
                flexDetail.Cols[i].AllowDragging = false;
                flexDetail.Cols[i].AllowEditing = false;
            }
            flexDetail.Cols["IsSelect"].DataType = typeof(bool);
            CustomizeC1FlexGrid.SetColumnVisible(flexDetail, true, true, "IsSelect,OutputTypeName,ProductID,ProductName,IMEI,Endwarrantydate,SalePrice,VAT,Quantity,TotalFee,TotalCost");
            CustomizeC1FlexGrid.SetColumnCaption(flexDetail, "IsSelect", "Chọn",
                                                                           "OutputTypeName", "Hình thức xuất",
                                                                           "ProductID", "Mã sản phẩm",
                                                                           "ProductName", "Tên sản phẩm",
                                                                           "Endwarrantydate", "Ngày hết BH",
                                                                           "SalePrice", "Đơn giá",
                                                                           "TotalFee", "Phí trả",
                                                                           "Quantity", "Số lượng",
                                                                           "TotalCost", "Thành tiền");
            CustomizeC1FlexGrid.SetColumnAllowEditing(flexDetail, true, true, "IsSelect,Quantity");
            CustomizeC1FlexGrid.SetColumnFormat(flexDetail, "Endwarrantydate", "dd/MM/yyyy",
                                                                         "SalePrice", "###,##0",
                                                                         "TotalFee", "###,##0",
                                                                         "Quantity", "###,##0.##",
                                                                         "TotalCost", "###,##0");
            CustomizeC1FlexGrid.SetColumnWidth(flexDetail, "IsSelect", 40,
                                                                        "OutputTypeName", 150,
                                                                        "ProductID", 120,
                                                                        "ProductName", 140,
                                                                        "IMEI", 150,
                                                                        "Endwarrantydate", 80,
                                                                        "SalePrice", 80,
                                                                        "VAT", 40,
                                                                        "Quantity", 40);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnDigit(flexDetail, 6, 2, false, "Quantity");
        }

        private void EnableControl(bool bolEnable)
        {
            cboStore.Enabled = bolEnable;
            lnkCustomer.Enabled = bolEnable;
            btnAdd.Enabled = bolEnable;
            chkCreateSaleOrder.Enabled = bolEnable;
        }
        private bool InitFlexGridAttachment()
        {
            try
            {
                if (strInputChangeOrderID != string.Empty)
                {
                    lstInputChangeOrder_Attachment = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment>();
                    DataTable tblAttachment = objPLCInputChangeOrder.GetAttachment(new object[] { "@InputChangeOrderID", strInputChangeOrderID });
                    if (tblAttachment != null && tblAttachment.Rows.Count > 0)
                    {
                        int intSTT = 0;
                        for (int i = 0; i < tblAttachment.Rows.Count; i++)
                        {
                            DataRow rAttachment = tblAttachment.Rows[i];
                            PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objAttachment = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment();
                            objAttachment.AttachmentID = Convert.ToString(rAttachment["AttachmentID"]).Trim();
                            objAttachment.InputChangeOrderID = strInputChangeOrderID;
                            objAttachment.AttachmentName = Convert.ToString(rAttachment["AttachmentName"]);
                            objAttachment.AttachmentPath = Convert.ToString(rAttachment["AttachmentPath"]);
                            objAttachment.FileID = Convert.ToString(rAttachment["FileID"]);
                            objAttachment.Description = Convert.ToString(rAttachment["Description"]);
                            objAttachment.CreatedDate = Convert.ToDateTime(rAttachment["CreatedDate"]);
                            objAttachment.CreatedUser = Convert.ToString(rAttachment["CreatedUser"]);
                            objAttachment.CreatedFullName = Convert.ToString(rAttachment["CreatedFullName"]);
                            objAttachment.STT = ++intSTT;
                            lstInputChangeOrder_Attachment.Add(objAttachment);
                        }
                    }
                }
                else
                    grvAttachment.Columns[grvAttachment.Columns.Count - 1].Visible = false;
                if (bolIsAllowEdit)
                    mnuAttachment.Enabled = true;
                else
                    mnuAttachment.Enabled = false;
                grdAttachment.DataSource = lstInputChangeOrder_Attachment;
                grdAttachment.RefreshDataSource();
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo tập tin đính kèm", objExce, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private bool InitFlexGridWorkFlow()
        {
            try
            {
                if (strInputChangeOrderID != string.Empty)
                {
                    DataTable tblWorkFlow = objPLCInputChangeOrder.GetWorkFlow(new object[] { "@InputChangeOrderID", strInputChangeOrderID });
                    flexWorkFlow.DataSource = tblWorkFlow;
                    FormatFlexGridWorkFlow(flexWorkFlow);

                    DataRow[] drWflow = tblWorkFlow.Select("IsProcessed = 0");
                    if (drWflow.Length > 0)
                        intWorkFlowCurrent = Convert.ToInt32(drWflow[0]["InputChangeOrderStepID"]);

                    //if (objInputChangeOrder.IsDeleted || objInputChangeOrder.IsReviewed)
                    //    flexWorkFlow.AllowEditing = false;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo flexCost", objExce, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private void EnableControlWhenSave(bool bolEnable)
        {
            btnAdd.Enabled = bolEnable;
            chkCreateSaleOrder.Enabled = bolEnable;
            btnCreateConcern.Enabled = bolEnable;
            btnUpdate.Enabled = bolEnable;
            btnReviewList.Enabled = bolEnable;
            btnDelete.Enabled = bolEnable;
            btnSupportCare.Enabled = bolEnable;
        }

        private bool InitFlexGridReviewLevel()
        {
            try
            {
                if (strInputChangeOrderID != string.Empty)
                {
                    DataTable tblReviewTable = objPLCInputChangeOrder.GetReviewList(new object[]{
                                "@InputChangeOrderTypeID", objInputChangeOrderType.InputChangeOrderTypeID,
                                "@InputChangeOrderID", strInputChangeOrderID});
                    flexReviewLevel.DataSource = tblReviewTable;
                }
                else
                {
                    DataTable tblReviewTable = objPLCInputChangeOrder.GetReviewList(new object[]{
                                "@InputChangeOrderTypeID", objInputChangeOrderType.InputChangeOrderTypeID});
                    flexReviewLevel.DataSource = tblReviewTable;
                }
                FormatFlexGridReviewLevel(flexReviewLevel);
                bool bolIsError = false;
                if (strInputChangeOrderID != string.Empty)
                    SetReviewLevelCurrent(ref bolIsError);
                if (bolIsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi khởi tạo lưới mức duyệt");
                    return false;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo flexReviewLevel", objExce, DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khởi tạo lưới mức duyệt");
                return false;
            }
            return true;
        }
        private void FormatFlexGridWorkFlow(C1.Win.C1FlexGrid.C1FlexGrid _flex)
        {
            for (int i = 0; i < _flex.Cols.Count; i++)
            {
                _flex.Cols[i].Visible = false;
                _flex.Cols[i].AllowSorting = false;
                _flex.Cols[i].AllowDragging = false;
                _flex.Cols[i].AllowEditing = false;
            }
            C1FlexGridObject.SetColumnVisible(_flex, true, "InputChangeOrderStepID,InputChangeOrderStepName,IsProcessed,ProcessFullName,ProcessedTime,Note");
            C1FlexGridObject.SetColumnCaption(_flex, "InputChangeOrderStepID,Mã bước xử lý,InputChangeOrderStepName,Tên bước xử lý,IsProcessed,Đã xử lý,ProcessFullName,Người xử lý,ProcessedTime,Thời gian xử lý,Note,Ghi chú");
            C1FlexGridObject.SetColumnWidth(_flex, "InputChangeOrderStepID,80,InputChangeOrderStepName,180,IsProcessed,70,ProcessFullName,250,ProcessedTime,80,Note,500");

            _flex.Cols["IsProcessed"].DataType = typeof(bool);
            _flex.Cols["ProcessedTime"].Format = "dd/MM/yyyy HH:mm";

            C1FlexGridObject.SetColumnAllowEditing(_flex, true, "IsProcessed", "Note");

            C1.Win.C1FlexGrid.CellStyle style = _flex.Styles.Add("flexStyle");
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            style.WordWrap = true;
            C1.Win.C1FlexGrid.CellRange range = _flex.GetCellRange(0, 0, 0, _flex.Cols.Count - 1);
            range.Style = style;
            _flex.Rows[0].Height = 54;
            _flex.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
        }
        private void FormatFlexGridReviewLevel(C1.Win.C1FlexGrid.C1FlexGrid _flex)
        {
            for (int i = 0; i < _flex.Cols.Count; i++)
            {
                _flex.Cols[i].Visible = false;
                _flex.Cols[i].AllowSorting = false;
                _flex.Cols[i].AllowDragging = false;
                _flex.Cols[i].AllowEditing = false;
            }
            C1FlexGridObject.SetColumnVisible(_flex, true, "ReviewLevelName,UserFullName,ReviewStatusName,ReviewedDate,Note");
            C1FlexGridObject.SetColumnCaption(_flex, "ReviewLevelName,Mức duyệt,UserFullName,Người duyệt,ReviewStatusName,Trạng thái,ReviewedDate,Ngày duyệt,Note, Ghi chú");
            C1FlexGridObject.SetColumnWidth(_flex, "ReviewLevelName,200,UserFullName,200,ReviewStatusName,120," +
                "ReviewedDate,120,Note,150");
            _flex.Cols["ReviewedDate"].Format = "dd/MM/yyyy HH:mm";

            C1.Win.C1FlexGrid.CellStyle style = _flex.Styles.Add("flexStyle");
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = _flex.GetCellRange(0, 0, 0, _flex.Cols.Count - 1);
            range.Style = style;
            _flex.Rows[0].Height = 30;
            _flex.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
        }
        string strPermissionReview = string.Empty;
        private void SetReviewLevelCurrent(ref bool bolIsError)
        {
            bolIsError = false;
            if (strInputChangeOrderID == string.Empty)
                return;
            if (flexReviewLevel.Rows.Count <= flexReviewLevel.Rows.Fixed)
                return;
            try
            {
                DataRow[] rowsReviewStatus = ((DataTable)flexReviewLevel.DataSource).Select("REVIEWSTATUS=" + (int)PLCInputChangeOrder.ReviewStatus.CANCEL);
                if (rowsReviewStatus.Length > 0)
                {

                    string status = "\"Từ chối\"";
                    lblReviewStatus.Text = "Yêu cầu đã được duyệt " + status + " bởi " + rowsReviewStatus[0]["UserFullName"].ToString() + " vào lúc " + ((DateTime)rowsReviewStatus[0]["ReviewedDate"]).ToString("dd/MM/yyyy HH:mm");
                    lblReviewStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;

                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;

                    cboReason.Enabled = false;

                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                    return;

                }
                for (int i = flexReviewLevel.Rows.Fixed; i < flexReviewLevel.Rows.Count; i++)
                {
                    int intCurrentStatus = Convert.ToInt32(flexReviewLevel[i, "ReviewStatus"]);
                    if (((DataTable)flexReviewLevel.DataSource).Select("ReviewLevelID = " + Convert.ToInt32(flexReviewLevel[i, "ReviewLevelID"]) + " AND ReviewStatus > " + intCurrentStatus).Length > 0)
                    {
                        for (int iNext = i + 1; iNext < flexReviewLevel.Rows.Count; iNext++)
                        {
                            if (Convert.ToInt32(flexReviewLevel[iNext, "ReviewLevelID"]) == Convert.ToInt32(flexReviewLevel[i, "ReviewLevelID"]) && Convert.ToInt32(flexReviewLevel[iNext, "ReviewStatus"]) == (int)PLCInputChangeOrder.ReviewStatus.ACCEPT)
                                if (iNext + 1 < flexReviewLevel.Rows.Count && Convert.ToInt32(flexReviewLevel[iNext + 1, "ReviewLevelID"]) != Convert.ToInt32(flexReviewLevel[i, "ReviewLevelID"]))
                                {
                                    i = iNext + 1;
                                    intCurrentStatus = Convert.ToInt32(flexReviewLevel[i, "ReviewStatus"]);
                                    break;
                                }
                        }
                    }
                    if (i == flexReviewLevel.Rows.Count - 1 && (intCurrentStatus == (int)PLCInputChangeOrder.ReviewStatus.CANCEL
                        || intCurrentStatus == (int)PLCInputChangeOrder.ReviewStatus.ACCEPT
                        ))
                    {
                        string status = "\"Đồng ý\"";
                        if (intCurrentStatus == (int)PLCInputChangeOrder.ReviewStatus.CANCEL)
                            status = "\"Từ chối\"";
                        lblReviewStatus.Text = "Yêu cầu đã được duyệt " + status + " bởi " + flexReviewLevel[i, "UserFullName"].ToString() + " vào lúc " + ((DateTime)flexReviewLevel[i, "ReviewedDate"]).ToString("dd/MM/yyyy HH:mm");
                        lblReviewStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
                    }
                    if (intCurrentStatus != (int)PLCInputChangeOrder.ReviewStatus.ACCEPT)
                    {
                        strPermissionReview = flexReviewLevel[i, "ReviewFunctionID"].ToString();
                        C1FlexGridObject.SetRowBackColor(flexReviewLevel, i, SystemColors.Info);
                        //if (SystemConfig.objSessionUser.IsPermission(strPermissionReview)
                        //    || objInputChangeOrderType.IsOnlyCertifyFinger
                        //    )
                        //{
                        btnReviewList.Visible = true;
                        intReviewLevelCurrent = Convert.ToInt32(flexReviewLevel[i, "ReviewLevelID"]);
                        bolIsFinalLevel = (i == flexReviewLevel.Rows.Count - 1);
                        for (int j = intCurrentStatus + 2; j < 4; j++)
                        {
                            DevExpress.XtraBars.BarButtonItem barItem = new DevExpress.XtraBars.BarButtonItem();
                            string strCaption = string.Empty;
                            switch ((PLCInputChangeOrder.ReviewStatus)j)
                            {
                                //case PLCInputChangeOrder.ReviewStatus.INPROCESS: strCaption = "Đang xử lý";
                                //    barItem.ImageIndex = 0;
                                //    break;
                                case PLCInputChangeOrder.ReviewStatus.CANCEL:
                                    strCaption = "Từ chối";
                                    barItem.ImageIndex = 1;
                                    break;
                                case PLCInputChangeOrder.ReviewStatus.ACCEPT:
                                    strCaption = "Đồng ý";
                                    barItem.ImageIndex = 2;
                                    break;
                            }
                            barItem.Caption = strCaption;
                            barItem.Name = "btnReviewLevel" + j;
                            barItem.Tag = j;
                            barItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barItem_ItemClick);
                            pMnu.AddItem(barItem);
                        }
                        //}
                        break;
                    }
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi khởi tạo flexReviewLevel", objExce, DUIInventory_Globals.ModuleName);
            }
        }

        private void ActionCloseForm()
        {
            flexDetail.Enabled = false;
            tmrCloseForm.Enabled = true;
            tmrCloseForm.Start();
        }

        private void InitControl()
        {
            lblReviewStatus.Text = string.Empty;
            btnReviewList.Visible = false;
            btnDelete.Visible = false;
            btnUpdate.Visible = false;
            btnSupportCare.Visible = false;
            chkCreateSaleOrder.Visible = false;
        }

        void barItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraBars.BarButtonItem barItem = e.Item as DevExpress.XtraBars.BarButtonItem;

            if (!Globals.ValidatingSystemDate(this))
                return;

            DataTable tblRv = (DataTable)flexReviewLevel.DataSource;
            if (tblRv == null)
                tblRv = (DataTable)flexReviewLevel.DataSource;
            DataRow[] drReviewLevelCurrent = tblRv.Select("ReviewLevelID = " + intReviewLevelCurrent);
            DataTable tblWF = (DataTable)flexWorkFlow.DataSource;
            if (tblWF == null)
                tblWF = (DataTable)flexWorkFlow.DataSource;
            if (tblWF != null && tblWF.Rows.Count > 0 && Convert.ToInt32(drReviewLevelCurrent[0]["PrecedingOrderStepID"]) > 0)
            {
                DataRow[] drWorkFlow = tblWF.Select("InputChangeOrderStepID = " + Convert.ToInt32(drReviewLevelCurrent[0]["PrecedingOrderStepID"]));
                if (drWorkFlow.Length > 0 && (!Convert.ToBoolean(drWorkFlow[0]["IsProcessed2"])))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng xử lý trước bước xử lý [" + Convert.ToString(drWorkFlow[0]["InputChangeOrderStepID"]) + " - " + Convert.ToString(drWorkFlow[0]["InputChangeOrderStepName"]) + "]");
                    tabControl.SelectedTab = tabPageWorkflow;
                    return;
                }
            }
            ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_ReviewList objInputChangeOrder_ReviewList = new ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_ReviewList();
            objInputChangeOrder_ReviewList.IsReviewByFinger = false;
            string strReviewString = "\"" + barItem.Caption.ToLower() + "\"";
            bool IsAllowReview = false;
            string ReviewList_UserName = SystemConfig.objSessionUser.UserName;
            if (SystemConfig.objSessionUser.IsPermission(strPermissionReview)
                && !objInputChangeOrderType.IsOnlyCertifyFinger)
            {
                if (MessageBox.Show("Bạn có muốn " + strReviewString.ToLower() + " yêu cầu " + strMessageForm + " này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                IsAllowReview = true;
            }
            else
            {

                List<string> lstContainsUser = new List<string>();
                lstContainsUser.Add(strPermissionReview);
                Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận " + strReviewString + " yêu cầu đổi trả", "Xác nhận", null, lstContainsUser, !objInputChangeOrderType.IsOnlyCertifyFinger, true);
                //Library.Login.frmCertifyLogin frmCertifyLogin1 = new Library.Login.frmCertifyLogin("Xác nhận duyệt yêu cầu đổi trả", "Xác nhận", null, lstContainsUser, false, true);
                frmCertifyLogin1.ShowDialog(this);
                if (!frmCertifyLogin1.IsCorrect)
                    return;
                ReviewList_UserName = frmCertifyLogin1.UserName;
                IsAllowReview = true;
                objInputChangeOrder_ReviewList.IsReviewByFinger = frmCertifyLogin1.IsCertifyByFinger;
            }
            if (IsAllowReview)
            {
                objInputChangeOrder_ReviewList.InputChangeOrderID = strInputChangeOrderID;
                objInputChangeOrder_ReviewList.ReviewLevelID = intReviewLevelCurrent;
                objInputChangeOrder_ReviewList.RowSCN = objInputChangeOrder.RowSCN;
                objInputChangeOrder_ReviewList.UserName = ReviewList_UserName;
                objInputChangeOrder_ReviewList.ReviewStatus = Convert.ToInt32(barItem.Tag);
                if (objInputChangeOrder_ReviewList.ReviewStatus == 3)
                    objInputChangeOrder_ReviewList.AutoUpdateOrderStepID = Convert.ToInt32(drReviewLevelCurrent[0]["AutoUpdateOrderStepID"]);

                objPLCInputChangeOrder.UpdateReviewList(objInputChangeOrder_ReviewList, flexReviewLevel.Rows.Count - flexReviewLevel.Rows.Fixed);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                }
                else
                {
                    MessageBox.Show("Duyệt " + strReviewString + " yêu cầu " + strMessageForm + " thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.bolIsHasAction = true;
                this.Close();
            }
        }

        #endregion

        #region event
        int intNumDayAfterEdit = 0;
        private void frmInputChangeOrder_Load(object sender, EventArgs e)
        {
            InitControl(); // Khoi tao control

            // Lay danh sach loai nhap doi tra
            objPLCInputChangeOrderType.LoadInfo(ref objInputChangeOrderType, intInputChangeOrderTypeID);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                EnableControl(false);
                strMessageCloseForm = Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail;
                ActionCloseForm();
                return;
            }
            if (Convert.ToString(objInputChangeOrderType.AddFunctionID).Trim() == string.Empty)
            {
                EnableControl(false);
                strMessageCloseForm = "Quyền thêm yêu cầu " + strMessageForm + " chưa được khai báo! Vui lòng kiểm tra lại";
                ActionCloseForm();
                return;
            }

            // Get Quyen khoi tao
            bolIsHasPermission_Create = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.AddFunctionID);
            bolIsHasPermission_Edit = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.EditFunctionID);
            bolIsHasPermission_EditAll = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.EditAllFunctionID);
            bolIsHasPermission_Delete = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.DeleteFunctionID);
            bolIsHasPermission_DeleteAll = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.DeleteAllFunctionID);
            bolIsHasPermission_AddAttachment = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.AddAttachmentFunctionID);
            bolIsHasPermission_DelAttachment = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.DeleteAttachmentFunctionID);
            bolIsHasPermission_ViewAttachment = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.ViewAttachmentFunctionID);
            bolIsHasPermission_AfterEditFunction = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.AfterEditFunctionID);
            bolIsHasPermission_AfterEditLimitFunction = SystemConfig.objSessionUser.IsPermission(objInputChangeOrderType.AfterEditLimitFunctionID);
            intNumDayAfterEdit = objInputChangeOrderType.NumDayAfterEdit;

            if (objInputChangeOrderType.IsInputReturn)
                strMessageForm = "nhập trả hàng";
            if ((!bolIsHasPermission_Create) && strInputChangeOrderID == string.Empty)
            {
                EnableControl(false);
                strMessageCloseForm = "Bạn không có quyền thêm yêu cầu " + strMessageForm + "! Vui lòng kiểm tra lại";
                ActionCloseForm();
                return;
            }
            if (!LoadCombobox())
            {
                ActionCloseForm();
                return;
            }
            if (strInputChangeOrderID != string.Empty) // Load Thong Tin Man HInh
            {
                objInputChangeOrder = objPLCInputChangeOrder.LoadInfo(strInputChangeOrderID);
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    EnableControl(false);
                    strMessageCloseForm = Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail;
                    ActionCloseForm();
                    return;
                }
                txtSaleOrderID.Text = objInputChangeOrder.SaleOrderID;
            }
            if (!LoadOutputVoucher(ref strMessageCloseForm))
            {
                ActionCloseForm();
                return;
            }
            LoadInputType();

            if (strInputChangeOrderID == string.Empty)
            {
                FormState = FormStateType.ADD;
                btnAdd.Enabled = bolIsHasPermission_Create;
                chkCreateSaleOrder.Enabled = bolIsHasPermission_Create;
                if (objInputChangeOrderType.IsInputReturn)
                    chkCreateSaleOrder.Visible = false;
                else
                    chkCreateSaleOrder.Visible = true;
                bolIsAllowEdit = btnAdd.Enabled;
                btnDelete.Enabled = false;
                cboStore.Enabled = true;
                if (!objInputChangeOrderType.IsInputReturn)
                    lblInstruction.Visible = true;

                tabControl.TabPages.Remove(tabPageInputChangeOrder);

                tabControl.TabPages.Remove(tabPageReviewList);

                tabControl.TabPages.Remove(tabPageWorkflow);

                lblTitlePayment.Visible = false;
                txtReturnAmount.Visible = false;
                btnCreateConcern.Visible = false;
                chkApplyErrorType.Checked = bolApplyErrorType;
            }
            else
            {
                FormState = FormStateType.EDIT;
                btnAdd.Visible = false;
                chkCreateSaleOrder.Visible = false;
                label1.Visible = false;
                label2.Visible = false;
                label3.Visible = false;
                label4.Visible = false;
                label5.Visible = false;
                txtContentIOT.Visible = false;
                cboReasonOV.Visible = false;
                txtReasonNoteOV.Visible = false;
                btnUpdate.Visible = true;
                bolIsHasPermission_UpdateCare = SystemConfig.objSessionUser.IsPermission(strRightUpdateCare);
                if (objInputChangeOrder.IsDeleted)
                {
                    FormState = FormStateType.VIEW;
                    bolIsAllowEdit = false;
                    lblContent.Text = "Nội dung hủy:";
                    lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " đã bị hủy";
                    lblReviewStatus.ForeColor = Color.Red;
                    btnUpdate.Visible = false;
                }
                else
                {
                    btnDelete.Visible = true;
                    if (objInputChangeOrder.IsReviewed)
                    {
                        lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " đã được duyệt";
                        EnableControl(false);
                        btnDelete.Visible = false;
                        btnDelete.Enabled = false;
                        btnCreateConcern.Enabled = bolIsHasPermission_EditAll || (bolIsHasPermission_Edit && objInputChangeOrder.CreatedUser.Equals(SystemConfig.objSessionUser.UserName));
                    }
                    else
                    {
                        lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " chưa duyệt";
                        btnDelete.Enabled = bolIsHasPermission_DeleteAll || (bolIsHasPermission_Delete && objInputChangeOrder.CreatedUser.Equals(SystemConfig.objSessionUser.UserName));
                    }
                    btnSupportCare.Visible = objInputChangeOrderType.IsInputReturn && bolIsHasPermission_UpdateCare && (((DataTable)flexDetail.DataSource).Select("IMEI IS NULL OR TRIM(IMEI)=''").Length > 0);
                }

                txtInputChangeOrderID.Text = objInputChangeOrder.InputChangeOrderID;
                dtpInputChangeOrderDate.Value = (DateTime)objInputChangeOrder.InputChangeOrderDate;

                cboInputChangeOrderStoreID.SetValue(objInputChangeOrder.InputChangeOrderStoreID);

                txtInVoucherID.Text = objInputChangeOrder.InVoucherID;
                txtNewOutputVoucherID.Text = objInputChangeOrder.NewOutputVoucherID;
                txtOutVoucherID.Text = objInputChangeOrder.OutVoucherID;
                txtNewInputVoucherID.Text = objInputChangeOrder.NewInputVoucherID;
                txtSaleOrderID.Text = objInputChangeOrder.SaleOrderID;
                chkApplyErrorType.Checked = objInputChangeOrder.ApplyErrorType;
                if (objInputChangeOrder.InputChangeReasonID > 0)
                    cboReason.SetValue(objInputChangeOrder.InputChangeReasonID);
                else cboReason.ResetDefaultValue();
                txtReasonNote.Text = objInputChangeOrder.InputChangeReasonNote.Trim().Replace("\n", "\r\n");
                if ((!objInputChangeOrderType.IsInputReturn) && !string.IsNullOrEmpty(txtSaleOrderID.Text))
                {
                    if (flexDetail.Cols.Contains("ProductID_Out"))
                    {
                        flexDetail.Cols["OutputTypeName_Out"].Visible = false;
                        flexDetail.Cols["ProductID_Out"].Visible = false;
                        flexDetail.Cols["ProductName_Out"].Visible = false;
                        flexDetail.Cols["IMEI_Out"].Visible = false;
                        flexDetail.Cols["SalePrice_Out"].Visible = false;
                        CustomizeC1FlexGrid.SetColumnWidth(flexDetail, "ProductID,190,ProductName,210,IMEI,190,InputPrice_In,140,EndWarrantyDate,150,Quantity,80");
                        if (objSaleOrder_Out == null)
                            objSaleOrder_Out = objPLCSaleOrders.LoadInfo(objInputChangeOrder.SaleOrderID);
                        if (objSaleOrder_Out != null && !string.IsNullOrEmpty(objSaleOrder_Out.SaleOrderID))
                        {
                            txtCustomerID.Text = objSaleOrder_Out.CustomerID.ToString();
                            txtCustomerName.Text = objSaleOrder_Out.CustomerName;
                            txtCustomerAddress.Text = objSaleOrder_Out.CustomerAddress;
                            txtCustomerPhone.Text = objSaleOrder_Out.CustomerPhone;
                            txtTaxID.Text = objSaleOrder_Out.CustomerTaxID;
                        }
                    }
                }
                ucCreateUser.UserName = objInputChangeOrder.CreatedUser;
                txtContent.Text = objInputChangeOrder.InputChangeOrderContent.Trim().Replace("\n", "\r\n");
                if (objInputChangeOrder.IsDeleted)
                    txtContent.Text = objInputChangeOrder.ContentDeleted;
                chkIsReviewed.Checked = objInputChangeOrder.IsReviewed;
                if (objInputChangeOrder.ReviewedDate != null)
                    dtpReviewedDate.DateTime = (DateTime)objInputChangeOrder.ReviewedDate;
                else
                    dtpReviewedDate.EditValue = null;
                chkIsInputchange.Checked = objInputChangeOrder.IsInputChanged;
                if (objInputChangeOrder.InputChangeDate != null)
                    dtpInputchangeDate.DateTime = (DateTime)objInputChangeOrder.InputChangeDate;
                else
                    dtpInputchangeDate.EditValue = null;

                cboStore.SetValue(objInputChangeOrder.InputChangeOrderStoreID);
                if (cboStore.StoreID < 1 && ((DataTable)(cboStore.DataSource)).Rows.Count > 0)
                {
                    cboStore.SetIndex(0);
                }

                if ((!objInputChangeOrderType.IsInputReturn) && !string.IsNullOrEmpty(txtSaleOrderID.Text))
                {
                    if (objSaleOrder_Out == null)
                        objSaleOrder_Out = objPLCSaleOrders.LoadInfo(objInputChangeOrder.SaleOrderID);
                }

                if (chkIsInputchange.Checked) // Khi Chack Xu ly đã duoc check 
                {
                    btnCreateConcern.Visible = false;
                    btnSupportCare.Visible = false;
                    bolIsAllowEdit = false;
                    lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " đã được xử lý";
                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;
                    cboReason.Enabled = false;
                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                }
                else
                {
                    btnUpdate.Enabled = bolIsHasPermission_EditAll || (bolIsHasPermission_Edit && objInputChangeOrder.CreatedUser.Equals(SystemConfig.objSessionUser.UserName));
                    if (btnUpdate.Enabled)
                    {
                        txtContent.ReadOnly = false;
                        txtContent.BackColor = SystemColors.Window;
                        cboReason.Enabled = true;
                        txtReasonNote.BackColor = SystemColors.Window;
                        txtReasonNote.ReadOnly = false;
                    }
                }

                if (objInputChangeOrder.IsDeleted) // Neu Đã bi xoa 
                {
                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;
                    cboReason.Enabled = false;
                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                }

                txtInputTypeName.Text = cboInputTypeID.Text;
                cboInputTypeID.Visible = false;
                txtInputTypeName.Visible = true;
                txtStoreName.Text = cboStore.Text;
                cboStore.Visible = false;
                txtStoreName.Visible = true;
                txtInputChangeOrderStoreName.Text = cboInputChangeOrderStoreID.Text;
                cboInputChangeOrderStoreID.Visible = false;
                txtInputChangeOrderStoreName.Visible = true;
                txtCreateUser.Text = ucCreateUser.UserName + " - " + ucCreateUser.FullName;
                ucCreateUser.Visible = false;
                txtCreateUser.Visible = true;

                if (objInputChangeOrderType.IsAutoReview)
                {

                    tabControl.TabPages.Remove(tabPageReviewList);
                    tabControl.TabPages.Remove(tabPageWorkflow);
                }
                else
                {
                    if (!InitFlexGridWorkFlow())
                    {
                        //MessageBoxObject.ShowWarningMessage(this, "Lỗi khởi tạo lưới bước xử lý");
                        strMessageCloseForm = "Lỗi khởi tạo lưới bước xử lý";
                        ActionCloseForm();
                        return;
                    }

                    if (!InitFlexGridReviewLevel())
                    {
                        ActionCloseForm();
                        return;
                    }

                    DataRow[] drReviewStatus = ((DataTable)flexReviewLevel.DataSource).Select("ReviewStatus = 2");
                    if (drReviewStatus.Length > 0)
                    {
                        intReviewStatus = 2;
                    }
                }
            }
            if (!InitFlexGridAttachment())
            {
                strMessageCloseForm = "Lỗi khởi tạo lưới tập tin đính kèm";
                ActionCloseForm();
                return;
            }
            if (!bolIsAllowEdit)
            {
                EnableControl(bolIsAllowEdit || (bolIsHasPermission_Create && strInputChangeOrderID == string.Empty));
                btnDelete.Enabled = false;
                btnDownload.Enabled = false;
                btnReviewList.Enabled = false;
                btnUpdate.Enabled = false;
                btnAdd.Enabled = false;
                chkCreateSaleOrder.Enabled = false;
                if (bolIsHasPermission_Create && strInputChangeOrderID == string.Empty)
                {
                    btnAdd.Enabled = true;
                    if (!objInputChangeOrderType.IsInputReturn)
                        chkCreateSaleOrder.Enabled = true;
                }
            }
            else
            {
                mnuAddAttachment.Enabled = btnUpdate.Enabled;
                mnuDelAttachment.Enabled = btnUpdate.Enabled;
                if (btnUpdate.Visible && !btnUpdate.Enabled)
                {
                    mnuAddAttachment.Enabled = bolIsHasPermission_AddAttachment;
                    mnuDelAttachment.Enabled = bolIsHasPermission_DelAttachment;
                    btnUpdate.Enabled = (bolIsHasPermission_AddAttachment || bolIsHasPermission_DelAttachment);
                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;

                    cboReason.Enabled = false;

                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                }
                if (intReviewStatus == 2)
                {
                    EnableControl(false);
                    btnCreateConcern.Enabled = false;
                    btnDownload.Enabled = false;
                    btnReviewList.Enabled = false;
                    btnAdd.Enabled = false;
                    chkCreateSaleOrder.Enabled = false;
                    btnUpdate.Enabled = false;
                    txtContent.BackColor = System.Drawing.SystemColors.Info;
                    txtContent.ReadOnly = true;
                    cboReason.Enabled = false;
                    txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                    txtReasonNote.ReadOnly = true;
                }
                if (strInputChangeOrderID != string.Empty && objInputChangeOrder.IsReviewed)
                {
                    if (!chkIsInputchange.Checked)
                    {
                        if (!objInputChangeOrder.IsDeleted)
                        {
                            btnDelete.Visible = SystemConfig.objSessionUser.IsPermission("PM_INPUTCHANGEORDER_DELETE_AFTERREVIEWED");
                            btnDelete.Enabled = btnDelete.Visible;
                        }
                        btnSupportCare.Visible = objInputChangeOrderType.IsInputReturn && bolIsHasPermission_UpdateCare && (((DataTable)flexDetail.DataSource).Select("IMEI IS NULL OR TRIM(IMEI)=''").Length > 0);
                    }
                    else
                    {
                        if (objInputChangeOrder.IsInputReturn && !objInputChangeOrder.IsDeleted)
                        {
                            btnDelete.Visible = objPLCInputChangeOrder.CheckDelete(objInputChangeOrder.NewInputVoucherID, objInputChangeOrder.OutVoucherID);
                            btnDelete.Enabled = btnDelete.Visible;
                        }
                    }
                }
            }
            if (bolIsLockByClosingDataMonth)
            {
                EnableControl(false);
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tháng " + dtOutputDate.Value.Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa");
                strMessageCloseForm = "Tháng " + dtOutputDate.Value.Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa";
                return;
            }
            CheckAfterEditFunction();
            tabControl.SelectedIndex = 0;
            if (evtCloseParentForm != null)
                evtCloseParentForm(this, null);
        }

        bool IsUpdateContent = false;
        public void CheckAfterEditFunction()
        {
            DateTime sysDate = Library.AppCore.Globals.GetServerDateTime();
            // Khi checked da duoc check
            if (chkIsInputchange.Checked && dtpInputchangeDate.EditValue != null)
            {
                DateTime InputchangeDate = (DateTime)dtpInputchangeDate.EditValue;
                // Phai có ngay duoc phep chinh sua hoac co quyen chinh sua
                if (intNumDayAfterEdit > 0 || bolIsHasPermission_AfterEditFunction)
                {
                    //if (bolIsHasPermission_AfterEditLimitFunction && objInputChangeOrder.CreatedUser.Equals(SystemConfig.objSessionUser.UserName)
                    //    && ((InputchangeDate.AddDays(intNumDayAfterEdit - 1).Date - sysDate.Date).Days >= 0))
                    //    IsUpdateContent = true;
                    if (bolIsHasPermission_AfterEditFunction)
                        IsUpdateContent = true;
                    if (bolIsHasPermission_AfterEditLimitFunction && ((InputchangeDate.AddDays(intNumDayAfterEdit - 1).Date - sysDate.Date).Days >= 0))
                        IsUpdateContent = true;
                }
                if (IsUpdateContent)
                {
                    btnUpdate.Enabled = IsUpdateContent;
                    txtContent.BackColor = SystemColors.Window;
                    txtContent.ReadOnly = false;
                    cboReason.Enabled = true;
                    txtReasonNote.BackColor = SystemColors.Window;
                    txtReasonNote.ReadOnly = false;
                }
            }
        }

        private void cboStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (intOldStoreID > 0 && intOldStoreID != cboStore.StoreID)
            {
                if (MessageBox.Show("Khi thay đổi kho nhập, " + (objInputChangeOrderType.IsInputReturn ? "bạn phải chọn lại sản phẩm nhập trả!" : "dữ liệu chi tiết xuất vào sẽ bị xóa!") + "\n Bạn có chắc muốn thay đổi kho nhập không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ClearDetail();
                }
                else
                {
                    cboStore.SetValue(intOldStoreID);
                }
            }
            if (cboStore.StoreID > 0)
            {
                intOldStoreID = cboStore.StoreID;
            }
        }

        private bool UploadAttachment()
        {
            if (lstInputChangeOrder_Attachment == null || lstInputChangeOrder_Attachment.Count < 1)
                return true;
            //Tạo trạng thái chờ trong khi đính kèm tập tin
            try
            {
                if (lstInputChangeOrder_Attachment != null && lstInputChangeOrder_Attachment.Count > 0)
                {
                    var CheckIsNew = from o in lstInputChangeOrder_Attachment
                                     where o.IsAddNew == true
                                     select o;
                    if (CheckIsNew.Count() <= 0)
                    {
                        return true;
                    }
                    string strFilePath = string.Empty;
                    string strFileID = string.Empty;
                    foreach (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objInputChangeOrder_Attachment in CheckIsNew)
                    {
                        string strResult = ERP.FMS.DUI.DUIFileManager.UploadFile(this, strFMSAplication, objInputChangeOrder_Attachment.AttachmentLocalPath, ref strFileID, ref strFilePath);
                        objInputChangeOrder_Attachment.AttachmentPath = strFilePath;
                        objInputChangeOrder_Attachment.FileID = strFileID;
                        if (!string.IsNullOrEmpty(strResult))
                        {
                            MessageBoxObject.ShowWarningMessage(this, strResult);
                            return false;
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool DownloadFile(string strFileName, string strFileID)
        {
            try
            {
                SaveFileDialog dlgSaveFile = new SaveFileDialog();
                dlgSaveFile.Title = "Lưu file tải về";
                dlgSaveFile.FileName = strFileName;
                dlgSaveFile.Filter = "All Files (*.*)|*.*";
                if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                {
                    if (dlgSaveFile.FileName != string.Empty)
                    {
                        Library.AppCore.FTP.FileAttachment objFileAttachment = new Library.AppCore.FTP.FileAttachment();
                        ERP.FMS.DUI.DUIFileManager.DownloadFile(this, strFMSAplication, strFileID, string.Empty, dlgSaveFile.FileName);
                        if (stbAttachmentLog.Length > 0)
                            stbAttachmentLog.Append("\r\n");
                        stbAttachmentLog.Append("Tải về tập tin: " + strFileName);
                    }
                }
                return true;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tải file đính kèm", objExce.ToString(), " frmInputChangeOrder-> DownloadFile", DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi tải file đính kèm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objInputChangeOrder_Attachment = (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
            if (objInputChangeOrder_Attachment.IsAddNew == true)
            {
                return;
            }
            if (!bolIsHasPermission_ViewAttachment)
            {
                MessageBox.Show(this, "Bạn không có quyền xem file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DownloadFile(objInputChangeOrder_Attachment.AttachmentName, objInputChangeOrder_Attachment.FileID);
        }

        private bool AddInputchangeOrder()
        {
            if (bolIsLockByClosingDataMonth)
            {
                return false;
            }
            try
            {
                EnableControlWhenSave(false);
                if (!CheckAdd())
                {
                    EnableControlWhenSave(true);
                    return false;
                }

                btnAdd.Enabled = false;

                int intStoreID = cboStore.StoreID;
                string strUserName = Library.AppCore.SystemConfig.objSessionUser.UserName;
                int intCreateStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                DateTime dtmInputChangeOrderDate = Library.AppCore.Globals.GetServerDateTime();

                if (!chkCreateSaleOrder.Checked)
                {
                    if (MessageBox.Show("Bạn có chắc muốn " + ((strInputChangeOrderID != string.Empty) ? "chỉnh sửa" : "tạo") + " yêu cầu " + strMessageForm + " này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        EnableControlWhenSave(true);
                        return false;
                    }
                }

                if (lstInputChangeOrder_Attachment_Del != null)
                {
                    lstInputChangeOrder_Attachment.InsertRange(lstInputChangeOrder_Attachment.Count, lstInputChangeOrder_Attachment_Del);
                }
                if (!UploadAttachment())
                {
                    if (MessageBox.Show(this, "Upload tập tin thất bại\r\nBạn có muốn tiếp tục lưu thông tin không?", "Thông báo",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    {
                        return false;
                    }
                }

                ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder objInputChangeOrder = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder();
                objInputChangeOrder.InputChangeOrderTypeID = objInputChangeOrderType.InputChangeOrderTypeID;
                objInputChangeOrder.OldOutputVoucherID = strOutputVoucherID;
                objInputChangeOrder.InputChangeOrderStoreID = intStoreID;
                objInputChangeOrder.InputChangeOrderDate = dtmInputChangeOrderDate;
                objInputChangeOrder.CreatedStoreID = intCreateStoreID;
                objInputChangeOrder.InputChangeOrderContent = txtContentIOT.Text;
                objInputChangeOrder.IsInputReturn = objInputChangeOrderType.IsInputReturn;
                objInputChangeOrder.InputChangeReasonID = cboReasonOV.ColumnID;
                objInputChangeOrder.InputChangeReasonNote = txtReasonNoteOV.Text.Trim();
                objInputChangeOrder.ApplyErrorType = chkApplyErrorType.Checked;
                DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select("IsSelect = 1");
                List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail> objInputChangeOrderDetailList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail>();
                for (int i = 0; i < drDetail.Length; i++)
                {
                    DataRow rDetail = drDetail[i];
                    PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail();
                    objInputChangeOrderDetail.ProductID_In = rDetail["ProductID"].ToString().Trim();
                    objInputChangeOrderDetail.IMEI_In = rDetail["IMEI"].ToString().Trim();
                    objInputChangeOrderDetail.ProductID_Out = rDetail["ProductID_Out"].ToString().Trim();
                    objInputChangeOrderDetail.IMEI_Out = rDetail["IMEI_Out"].ToString().Trim();
                    objInputChangeOrderDetail.OldOutputVoucherDetailID = rDetail["OutputVoucherDetailID"].ToString().Trim();
                    objInputChangeOrderDetail.InputChangeOrderDate = dtmInputChangeOrderDate;
                    objInputChangeOrderDetail.InputChangeOrderStoreID = intStoreID;
                    objInputChangeOrderDetail.CreatedStoreID = intCreateStoreID;
                    objInputChangeOrderDetail.Quantity = Convert.ToDecimal(rDetail["Quantity"]);
                    objInputChangeOrderDetail.ReturnFee = Convert.ToDecimal(rDetail["TotalFee"]);
                    objInputChangeOrderDetail.ReturnInputTypeID = cboInputTypeID.InputTypeID;
                    objInputChangeOrderDetail.IsNew_In = Convert.ToBoolean(rDetail["IsNew"]);
                    objInputChangeOrderDetail.VAT_In = Convert.ToInt32(rDetail["VAT"]);
                    objInputChangeOrderDetail.VATPercent_In = Convert.ToInt32(rDetail["VATPercent"]);
                    objInputChangeOrderDetail.IsHasWarranty_In = Convert.ToBoolean(rDetail["IsHasWarranty"]);
                    if (rDetail["EndWarrantyDate"] != DBNull.Value)
                        objInputChangeOrderDetail.EndWarrantyDate = Convert.ToDateTime(rDetail["EndWarrantyDate"]);
                    if (!objInputChangeOrderType.IsInputReturn)
                        objInputChangeOrderDetail.OutputTypeID_Out = Convert.ToInt32(rDetail["OutputTypeID_Out"]);

                    DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + cboInputTypeID.InputTypeID);
                    objInputChangeOrderDetail.InputPrice = Convert.ToDecimal(rDetail["InputSalePrice"]);
                    if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 2)
                        objInputChangeOrderDetail.InputPrice = Convert.ToDecimal(rDetail["InputCostPrice"]);
                    else if (Convert.ToInt32(drReturnInputType[0]["GetPriceType"]) == 3)
                        objInputChangeOrderDetail.InputPrice = 0;

                    objInputChangeOrderDetail.OutputTypeID = Convert.ToInt32(rDetail["OutputTypeID_Out"]);
                    if (rDetail["SalePrice_Out"] == DBNull.Value)
                        rDetail["SalePrice_Out"] = 0;
                    objInputChangeOrderDetail.SalePrice = Convert.ToDecimal(rDetail["SalePrice_Out"]);

                    if (objInputChangeOrderDetail.ProductID_Out == string.Empty)
                    {
                        if (objInputChangeOrderType.IsInputReturn)
                        {
                            objInputChangeOrderDetail.OutputTypeID = 0;
                            objInputChangeOrderDetail.SalePrice = 0;
                        }
                    }
                    else
                    {
                        if (!chkCreateSaleOrder.Checked)
                        {
                            ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(objInputChangeOrderDetail.OutputTypeID);
                            if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                            {
                                ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(objInputChangeOrderDetail.ProductID_Out);
                                objInputChangeOrderDetail.SalePrice = Convert.ToDecimal(rDetail["SalePrice_Out"]) / (Convert.ToDecimal(1) + Convert.ToDecimal(objProductOut.VAT) * Convert.ToDecimal(objProductOut.VATPercent) * Convert.ToDecimal(0.0001));
                            }
                        }
                    }
                    //if (objInputChangeOrderType.IsInputChangeFree)
                    //    objInputChangeOrderDetail.SalePrice = objInputChangeOrderDetail.InputPrice;
                    objInputChangeOrderDetailList.Add(objInputChangeOrderDetail);
                }

                objInputChangeOrder.InputChangeOrderDetailList = objInputChangeOrderDetailList.ToArray();

                if (!objInputChangeOrderType.IsAutoReview)
                {
                    objInputChangeOrder.InputChangeOrder_ReviewTable = (DataTable)flexReviewLevel.DataSource;
                }
                else
                {
                    objInputChangeOrder.IsReviewed = true;
                    objInputChangeOrder.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();
                }

                objInputChangeOrder.InputChangeOrder_AttachmentList = lstInputChangeOrder_Attachment.ToArray();

                objInputChangeOrder.InputChangeOrder_WorkFlowTable = objPLCInputChangeOrder.GetWorkFlow(objInputChangeOrderType.InputChangeOrderTypeID);
                objInputChangeOrder.InputChangeOrder_WorkFlowTable.DefaultView.RowFilter = "IsDeleted = 0 AND IsActive = 1";
                objInputChangeOrder.InputChangeOrder_WorkFlowTable.DefaultView.Sort = "ProcessIndex ASC";
                objInputChangeOrder.InputChangeOrder_WorkFlowTable = objInputChangeOrder.InputChangeOrder_WorkFlowTable.DefaultView.ToTable();

                decimal decTotalLiquidate = 0;
                if (chkCreateSaleOrder.Checked && (!objInputChangeOrderType.IsInputReturn))
                {
                    SalesAndServices.SaleOrders.DUI.frmSelectInfo frmCreateSaleOrder = new SalesAndServices.SaleOrders.DUI.frmSelectInfo(SalesAndServices.SaleOrders.DUI.DUISaleOrders_Common.SelectInfoType.SALEORDERTYPE);
                    frmCreateSaleOrder.IsInputChangeOrder = true;
                    MasterData.PLC.MD.WSCustomer.Customer objCustomerRefer = new MasterData.PLC.MD.WSCustomer.Customer();
                    try
                    {
                        objCustomerRefer.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                    }
                    catch { }
                    objCustomerRefer.CustomerName = txtCustomerName.Text;
                    objCustomerRefer.CustomerAddress = txtCustomerAddress.Text;
                    objCustomerRefer.CustomerPhone = txtCustomerPhone.Text;
                    objCustomerRefer.CustomerTaxID = txtTaxID.Text;
                    frmCreateSaleOrder.CustomerRefer = objCustomerRefer;
                    frmCreateSaleOrder.StaffUser = objOutputVoucherLoad.StaffUser;
                    frmCreateSaleOrder.ShowDialog();
                    if (frmCreateSaleOrder.SaleOrder != null && (!string.IsNullOrEmpty(frmCreateSaleOrder.SaleOrder.SaleOrderID)) && frmCreateSaleOrder.SaleOrder.SaleOrderTypeID > 0)
                    {
                        decTotalLiquidate = frmCreateSaleOrder.SaleOrder.TotalLiquidate;
                        objInputChangeOrder.SaleOrderID = frmCreateSaleOrder.SaleOrder.SaleOrderID;
                    }
                    else
                    {
                        EnableControlWhenSave(true);
                        return false;
                    }
                }

                decimal decSumInputPrice = GetCompute_In(string.Empty);
                decimal decSumSalePrice = GetCompute_Out(string.Empty);
                objInputChangeOrder.UnevenAmount = (decTotalLiquidate + decSumSalePrice) - decSumInputPrice;

                DataTable dtbMachineError = null;
                if (!objPLCInputChangeOrder.Insert(objInputChangeOrder, dtbMachineError))
                {
                    btnAdd.Enabled = true;
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                }

                MessageBox.Show(this, "Tạo yêu cầu " + strMessageForm + " thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsHasAction = true;
                this.Close();
            }
            catch (Exception objEx)
            {
                btnAdd.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi tạo yêu cầu " + strMessageForm, objEx.Message + System.Environment.NewLine + objEx.StackTrace);
            }
            return true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!AddInputchangeOrder())
                return;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (bolIsLockByClosingDataMonth || strInputChangeOrderID == string.Empty)
            {
                return;
            }
            try
            {
                btnUpdate.Enabled = false;
                if (lstInputChangeOrder_Attachment_Del != null)
                {
                    lstInputChangeOrder_Attachment.InsertRange(lstInputChangeOrder_Attachment.Count, lstInputChangeOrder_Attachment_Del);
                }
                if (!UploadAttachment())
                {
                    if (MessageBox.Show(this, "Upload tập tin thất bại\r\nBạn có muốn tiếp tục lưu thông tin không?", "Thông báo",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }
                }
                ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder objInputChangeOrderUpdate = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder();
                objInputChangeOrderUpdate.InputChangeOrderID = strInputChangeOrderID;
                objInputChangeOrderUpdate.InputChangeOrderDate = objInputChangeOrder.InputChangeOrderDate;
                objInputChangeOrderUpdate.InputChangeOrderContent = txtContent.Text;
                objInputChangeOrderUpdate.InputChangeReasonID = cboReason.ColumnID;
                objInputChangeOrderUpdate.InputChangeReasonNote = txtReasonNote.Text.Trim();
                objInputChangeOrderUpdate.InputChangeOrder_AttachmentList = lstInputChangeOrder_Attachment.ToArray();
                //objInputChangeOrderUpdate.ApplyErrorType = chkApplyErrorType.Checked;
                if (!objPLCInputChangeOrder.Update(objInputChangeOrderUpdate, null))
                {
                    btnUpdate.Enabled = true;
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                }



                MessageBox.Show(this, "Cập nhật yêu cầu " + strMessageForm + " thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsHasAction = true;
                this.Close();
            }
            catch (Exception objEx)
            {
                btnUpdate.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi cập nhật yêu cầu " + strMessageForm, objEx.Message + System.Environment.NewLine + objEx.StackTrace);
            }
        }

        private void btnCreateConcern_Click(object sender, EventArgs e)
        {
            if (bolIsLockByClosingDataMonth)
            {
                return;
            }
            try
            {
                if (!CheckCreateConcern())
                {
                    return;
                }

                btnCreateConcern.Enabled = false;

                int intStoreID = cboStore.StoreID;
                string strUserName = Library.AppCore.SystemConfig.objSessionUser.UserName;
                int intCreateStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                DateTime dtmInputChangeOrderDate = Library.AppCore.Globals.GetServerDateTime();

                int intCurrencyUnitID = objOutputVoucherLoad.CurrencyUnitID;
                decimal decCurrencyExchange = objOutputVoucherLoad.CurrencyExchange;
                int intCustomerID = 0;
                try
                {
                    if (Convert.ToString(txtCustomerID.Text).Trim() != string.Empty)
                    {
                        intCustomerID = Convert.ToInt32(txtCustomerID.Text);
                    }
                }
                catch
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy mã khách hàng! Vui lòng kiểm tra lại");
                    btnCreateConcern.Enabled = true;
                    return;
                }
                string strCustomerName = txtCustomerName.Text.Trim();
                string strCustomerAddress = txtCustomerAddress.Text.Trim();
                string strCustomerPhone = txtCustomerPhone.Text.Trim();
                string strCustomerIDCard = txtCustomerIDCard.Text.Trim();
                string strCustomerTaxID = txtTaxID.Text.Trim();
                int intPayableTypeID = objOutputVoucherLoad.PayableTypeID;
                string strTaxID = txtTaxID.Text.Trim();
                string strStaffUser = ctrlStaffUser.UserName;

                decimal decTotalAmountBF_In = 0;
                decimal decTotalAmountBF_Out = 0;
                decimal decTotalVAT_In = 0;
                decimal decTotalVAT_Out = 0;

                ERP.MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit objCurrencyUnit = new MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit();
                ERP.MasterData.PLC.MD.PLCCurrencyUnit objPLCCurrencyUnit = new MasterData.PLC.MD.PLCCurrencyUnit();
                objPLCCurrencyUnit.LoadInfo(ref objCurrencyUnit, intCurrencyUnitID);
                if (objCurrencyUnit != null)
                {
                    decCurrencyExchange = objCurrencyUnit.CurrencyExchange;
                }
                else
                {
                    DataTable tblCurrencyUnit = null;
                    objPLCCurrencyUnit.SearchData(ref tblCurrencyUnit, new object[] { });
                    if (tblCurrencyUnit != null && tblCurrencyUnit.Rows.Count > 0)
                    {
                        DataRow[] rCurrencyUnit = tblCurrencyUnit.Select("CurrencyUnitName = 'VND'");
                        if (rCurrencyUnit.Length > 0)
                        {
                            intCurrencyUnitID = Convert.ToInt32(rCurrencyUnit[0]["CurrencyUnitID"]);
                            decCurrencyExchange = Convert.ToDecimal(rCurrencyUnit[0]["CurrencyExchange"]);
                        }
                    }
                }

                if (MessageBox.Show("Bạn có chắc muốn xử lý yêu cầu " + strMessageForm + " này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    EnableControlWhenSave(true);

                    decVNDCash = 0;
                    decPaymentCardAmount = 0;
                    intPaymentCardID = 0;
                    decPaymentCardSpend = 0;
                    strPaymentCardVoucherID = string.Empty;

                    decVNDCash_In = 0;
                    decPaymentCardAmount_In = 0;
                    intPaymentCardID_In = 0;
                    strPaymentCardName_In = string.Empty;
                    decPaymentCardSpend_In = 0;
                    strPaymentCardVoucherID_In = string.Empty;

                    decVNDCash_Out = 0;
                    decPaymentCardAmount_Out = 0;
                    intPaymentCardID_Out = 0;
                    decPaymentCardSpend_Out = 0;
                    strPaymentCardVoucherID_Out = string.Empty;

                    return;
                }

                ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.InputVoucher objInputVoucher = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucher();
                ERP.Inventory.PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucher objOutputVoucher = new PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucher();

                objInputChangeOrder.OldOutputVoucherID = strOutputVoucherID;
                objInputChangeOrder.InputChangeOrderStoreID = intStoreID;
                objInputChangeOrder.InputChangeDate = dtmInputChangeOrderDate;
                objInputChangeOrder.CreatedStoreID = intCreateStoreID;

                objInputVoucher.IsCheckRealInput = true;
                objInputVoucher.CheckRealInputUser = strUserName;
                objInputVoucher.CheckRealInputTime = dtmInputChangeOrderDate;
                objInputVoucher.InputTypeID = cboInputTypeID.InputTypeID;
                objInputVoucher.InputDate = dtmInputChangeOrderDate;
                objInputVoucher.PayableDate = dtmInputChangeOrderDate;
                objInputVoucher.InVoiceID = txtInVoiceID.Text;
                objInputVoucher.InVoiceDate = dtmInputChangeOrderDate;
                objInputVoucher.CustomerID = intCustomerID;
                objInputVoucher.CustomerName = strCustomerName;
                objInputVoucher.CustomerAddress = strCustomerAddress;
                objInputVoucher.CustomerPhone = strCustomerPhone;
                objInputVoucher.CustomerIDCard = strCustomerIDCard;
                objInputVoucher.CreatedStoreID = intCreateStoreID;
                objInputVoucher.InputStoreID = intStoreID;
                objInputVoucher.PayableTypeID = intPayableTypeID;
                objInputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objInputVoucher.CurrencyExchange = decCurrencyExchange;
                objInputVoucher.Content = objInputChangeOrder.InputChangeOrderContent;

                objOutputVoucher.CreatedStoreID = intCreateStoreID;
                objOutputVoucher.OutputStoreID = intStoreID;
                objOutputVoucher.InvoiceDate = dtmInputChangeOrderDate;
                objOutputVoucher.CustomerID = intCustomerID;
                objOutputVoucher.CustomerName = strCustomerName;
                objOutputVoucher.CustomerAddress = strCustomerAddress;
                objOutputVoucher.CustomerPhone = strCustomerPhone;
                objOutputVoucher.CustomerTaxID = strCustomerTaxID;
                objOutputVoucher.OutputContent = string.Empty;
                objOutputVoucher.OutputDate = dtmInputChangeOrderDate;
                objOutputVoucher.PayableTypeID = intPayableTypeID;
                objOutputVoucher.PayableDate = dtmInputChangeOrderDate;
                objOutputVoucher.StaffUser = strStaffUser;
                objOutputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objOutputVoucher.CurrencyExchange = decCurrencyExchange;

                decimal decTotalReturnFee = 0;
                objInputChangeOrder.InputVoucherReturnBO = null;
                DataRow[] drReturnInputype = dtbInputTypeAll.Select("InputTypeID = " + cboInputTypeID.InputTypeID);
                if (drReturnInputype.Length > 0)
                {
                    //lưu đồng bộ PM_InputVoucherReturn
                    objInputChangeOrder.IsInputReturn = objInputChangeOrderType.IsInputReturn;
                    objInputChangeOrder.InputVoucherReturnBO = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherReturn();
                    objInputChangeOrder.InputVoucherReturnBO.OutputVoucherID = strOutputVoucherID;
                    objInputChangeOrder.InputVoucherReturnBO.ReturnStoreID = intStoreID;
                    objInputChangeOrder.InputVoucherReturnBO.ReturnDate = DateTime.Now;
                    objInputChangeOrder.InputVoucherReturnBO.IsReturnWithFee = !objInputChangeOrderType.IsInputChangeFree;
                    objInputChangeOrder.InputVoucherReturnBO.TotalVATLost = 0;
                    objInputChangeOrder.InputVoucherReturnBO.TotalReturnFee = 0;
                    objInputChangeOrder.InputVoucherReturnBO.InVoiceID = txtInVoiceID.Text.Trim();
                    objInputChangeOrder.InputVoucherReturnBO.InVoiceSymbol = txtInvoiceSymbol.Text.Trim();
                    objInputChangeOrder.InputVoucherReturnBO.Denominator = objOutputVoucherLoad.Denominator;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerID = intCustomerID;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerName = strCustomerName;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerAddress = strCustomerAddress;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerPhone = strCustomerPhone;
                    objInputChangeOrder.InputVoucherReturnBO.CustomerTaxID = strCustomerTaxID;
                    objInputChangeOrder.InputVoucherReturnBO.InputTypeID = cboInputTypeID.InputTypeID;
                    objInputChangeOrder.InputVoucherReturnBO.PayableTypeID = intPayableTypeID;
                    objInputChangeOrder.InputVoucherReturnBO.CurrencyUnitID = intCurrencyUnitID;
                    objInputChangeOrder.InputVoucherReturnBO.CurrencyExchange = 1;
                    objInputChangeOrder.InputVoucherReturnBO.CreatedStoreID = intCreateStoreID;
                    objInputChangeOrder.InputVoucherReturnBO.CreatedUser = strUserName;

                    //objInputChangeOrder.InputVoucherReturnBO.OutVoucherID = txtVoucherID.Text.Trim();
                    objInputChangeOrder.InputVoucherReturnBO.ReturnReason = objInputChangeOrder.InputChangeOrderContent;
                    objInputChangeOrder.InputVoucherReturnBO.Discount = 0;
                    objInputChangeOrder.InputVoucherReturnBO.DiscountReasonID = 0;
                    objInputChangeOrder.InputVoucherReturnBO.ReturnNote = string.Empty;
                }

                if (!string.IsNullOrEmpty(objInputChangeOrder.SaleOrderID))
                {
                    if (objSaleOrder_Out == null)
                        objSaleOrder_Out = objPLCSaleOrders.LoadInfo(objInputChangeOrder.SaleOrderID);
                }

                List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetail> objInputVoucherDetailList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetail>();
                List<PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucherDetail> objOutputVoucherDetailList = new List<PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucherDetail>();
                List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail> objInputChangeDetailList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail>();

                dtbOVDetailReturn.AcceptChanges();
                for (int i = 0; i < dtbOVDetailReturn.Rows.Count; i++)
                {
                    DataRow rDetail = dtbOVDetailReturn.Rows[i];
                    string strOldOutputVoucherDetailID = Convert.ToString(rDetail["OldOutputVoucherDetailID"]).Trim();
                    string strProductID_In = string.Empty;
                    string strProductID_Out = string.Empty;
                    string strIMEI_Out = string.Empty;
                    string strIMEI_In = string.Empty;
                    decimal decQuantity = 0;
                    bool bolIsNew_In = Convert.ToBoolean(rDetail["IsNew_In"]);
                    int intVAT_In = 0;
                    int intVATPercent_In = 0;
                    int intVAT_Out = 0;
                    int intVATPercent_Out = 0;
                    decimal decInputPrice = 0;
                    decimal decSalePrice = 0;
                    //decimal decCostPrice = 0;

                    strProductID_In = Convert.ToString(rDetail["ProductID"]).Trim();
                    strProductID_Out = Convert.ToString(rDetail["ProductID_Out"]).Trim();
                    if (!Convert.IsDBNull(rDetail["IMEI"])) strIMEI_In = Convert.ToString(rDetail["IMEI"]).Trim();
                    if (!Convert.IsDBNull(rDetail["IMEI_Out"])) strIMEI_Out = Convert.ToString(rDetail["IMEI_Out"]).Trim();
                    decQuantity = Convert.ToDecimal(rDetail["Quantity"]);
                    decInputPrice = Convert.ToDecimal(rDetail["InputPrice_In2"]);
                    decSalePrice = Convert.ToDecimal(rDetail["SalePrice_Out2"]);
                    //decCostPrice = Convert.ToDecimal(rDetail["CostPrice"]);
                    intVAT_In = Convert.ToInt32(rDetail["VAT"]);
                    intVATPercent_In = Convert.ToInt32(rDetail["VATPercent"]);
                    bool bolIsHasWarranty = Convert.ToBoolean(rDetail["IsHasWarranty"]);
                    DateTime dtmEndWarrantyDate = Convert.ToDateTime(rDetail["EndWarrantyDate"]);
                    //int intFirstInputTypeID = Convert.ToInt32(rDetail["FirstInputTypeID"]);

                    DataRow[] drReturnInputType = dtbInputTypeAll.Select("InputTypeID = " + cboInputTypeID.InputTypeID);
                    if (drReturnInputType.Length > 0 && Convert.ToBoolean(drReturnInputType[0]["IsVATZero"])) intVAT_In = 0;

                    ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(rDetail["OutputTypeID_Out"]));
                    if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                    {
                        ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID_Out);
                        intVAT_Out = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID_Out).VAT;
                    }
                    if (strProductID_Out != string.Empty)
                        intVATPercent_Out = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID_Out).VATPercent;

                    decTotalAmountBF_In += decInputPrice * decQuantity;
                    decTotalVAT_In += decInputPrice * decQuantity * (Convert.ToDecimal(intVAT_In) * Convert.ToDecimal(intVATPercent_In) * Convert.ToDecimal(0.0001));

                    //if (objInputChangeOrderType.IsInputChangeFree)
                    //{
                    //    intVAT_Out = intVAT_In;
                    //    intVATPercent_Out = intVATPercent_In;
                    //}

                    decTotalAmountBF_Out += decSalePrice * decQuantity;
                    decTotalVAT_Out += decSalePrice * decQuantity * (Convert.ToDecimal(intVAT_Out) * Convert.ToDecimal(intVATPercent_Out) * Convert.ToDecimal(0.0001));

                    #region InputChangeDetail
                    PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail objInputChangeOrderDetail = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrderDetail();
                    objInputChangeOrderDetail.InputChangeOrderDetailID = rDetail["InputChangeOrderDetailID"].ToString().Trim();
                    objInputChangeOrderDetail.InputChangeOrderID = objInputChangeOrder.InputChangeOrderID;
                    objInputChangeOrderDetail.OldOutputVoucherDetailID = strOldOutputVoucherDetailID;
                    objInputChangeOrderDetail.InputChangeOrderDate = dtmInputChangeOrderDate;
                    objInputChangeOrderDetail.ProductID_Out = strProductID_Out;
                    objInputChangeOrderDetail.ProductID_In = strProductID_In;
                    objInputChangeOrderDetail.IMEI_Out = strIMEI_Out;
                    objInputChangeOrderDetail.IMEI_In = strIMEI_In;
                    objInputChangeOrderDetail.Quantity = decQuantity;
                    objInputChangeOrderDetail.OutputTypeID = Convert.ToInt32(rDetail["OutputTypeID_Out"]);
                    objInputChangeOrderDetail.CreatedStoreID = intCreateStoreID;
                    objInputChangeOrderDetail.InputPrice = decInputPrice;
                    decTotalReturnFee += Convert.ToDecimal(rDetail["TotalFee"]);

                    objInputChangeDetailList.Add(objInputChangeOrderDetail);
                    #endregion

                    #region InputVoucherDetail
                    if (objInputVoucherDetailList.Count > 0 && objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].ProductID == strProductID_In)
                    {
                        if (strIMEI_In != string.Empty)
                        {
                            PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI();
                            objInputVoucherDetailIMEI.IMEI = strIMEI_In;
                            objInputVoucherDetailIMEI.CreatedStoreID = intCreateStoreID;
                            objInputVoucherDetailIMEI.InputDate = dtmInputChangeOrderDate;
                            objInputVoucherDetailIMEI.InputStoreID = intStoreID;
                            objInputVoucherDetailIMEI.IsHasWarranty = bolIsHasWarranty;

                            List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI>();
                            if (objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList.Length > 0)
                            {
                                objInputVoucherDetailIMEIList.AddRange(objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList);
                            }
                            objInputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                            objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList = objInputVoucherDetailIMEIList.ToArray();
                            objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].Quantity = Convert.ToDecimal(objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList.Length);
                        }
                    }
                    else
                    {
                        PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetail objInputVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetail();
                        objInputVoucherDetail.ProductID = strProductID_In;
                        objInputVoucherDetail.Quantity = decQuantity;
                        objInputVoucherDetail.VAT = intVAT_In;
                        objInputVoucherDetail.VATPercent = intVATPercent_In;
                        objInputVoucherDetail.InputDate = dtmInputChangeOrderDate;
                        objInputVoucherDetail.CreatedStoreID = intCreateStoreID;
                        //objInputVoucherDetail.FirstInputTypeID = intFirstInputTypeID;
                        objInputVoucherDetail.InputStoreID = intStoreID;
                        objInputVoucherDetail.IsNew = bolIsNew_In;
                        if (objInputChangeOrderType.IsChangeToOldProduct)
                            objInputVoucherDetail.IsNew = false;
                        if (objInputChangeOrderType.IsChangeToShowProduct)
                            objInputVoucherDetail.IsShowProduct = true;
                        objInputVoucherDetail.InputPrice = decInputPrice;
                        objInputVoucherDetail.ReturnFee = Convert.ToDecimal(rDetail["TotalFee"]);
                        objInputVoucherDetail.ENDWarrantyDate = dtmEndWarrantyDate;
                        if (strIMEI_In != string.Empty)
                        {
                            PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI();
                            objInputVoucherDetailIMEI.IMEI = strIMEI_In;
                            objInputVoucherDetailIMEI.CreatedStoreID = intCreateStoreID;
                            objInputVoucherDetailIMEI.InputDate = dtmInputChangeOrderDate;
                            objInputVoucherDetailIMEI.InputStoreID = intStoreID;
                            objInputVoucherDetailIMEI.IsHasWarranty = bolIsHasWarranty;

                            List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputVoucherDetailIMEI>();
                            objInputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                            objInputVoucherDetail.InputVoucherDetailIMEIList = objInputVoucherDetailIMEIList.ToArray();
                            objInputVoucherDetail.Quantity = Convert.ToDecimal(objInputVoucherDetail.InputVoucherDetailIMEIList.Length);
                        }
                        objInputVoucherDetailList.Add(objInputVoucherDetail);
                    }
                    #endregion

                    #region OutputVoucherDetail
                    PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucherDetail objOutputVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.OutputVoucherDetail();
                    objOutputVoucherDetail.ProductID = strProductID_Out;
                    objOutputVoucherDetail.IMEI = strIMEI_Out;
                    //objOutputVoucherDetail.IsNew = objInputChangeOrderType.IsNew;
                    objOutputVoucherDetail.Quantity = decQuantity;
                    objOutputVoucherDetail.VAT = intVAT_Out;
                    objOutputVoucherDetail.VATPercent = intVATPercent_Out;
                    objOutputVoucherDetail.SalePrice = decSalePrice;
                    objOutputVoucherDetail.CreatedStoreID = intCreateStoreID;
                    objOutputVoucherDetail.OutputStoreID = intStoreID;
                    objOutputVoucherDetail.OutputTypeID = Convert.ToInt32(rDetail["OutputTypeID_Out"]);
                    objOutputVoucherDetail.OutputDate = dtmInputChangeOrderDate;
                    objOutputVoucherDetail.InputChangeDate = dtmInputChangeOrderDate;

                    objOutputVoucherDetailList.Add(objOutputVoucherDetail);
                    #endregion
                }

                objInputVoucher.TotalAmountBFT = decTotalAmountBF_In;
                objInputVoucher.TotalVAT = decTotalVAT_In;
                objInputVoucher.TotalAmount = decTotalAmountBF_In + decTotalVAT_In;
                objOutputVoucher.TotalAmountBFT = decTotalAmountBF_Out;
                objOutputVoucher.TotalVAT = decTotalVAT_Out;
                objOutputVoucher.TotalAmount = decTotalAmountBF_Out + decTotalVAT_Out;

                objInputVoucher.IsNew = objInputVoucherDetailList[0].IsNew;
                objInputChangeOrder.InputChangeOrderDetailList = objInputChangeDetailList.ToArray();
                objInputVoucher.InputVoucherDetailList = objInputVoucherDetailList.ToArray();
                objOutputVoucher.OutputVoucherDetailList = objOutputVoucherDetailList.ToArray();

                if (objInputChangeOrder.InputVoucherReturnBO != null)
                {
                    objInputChangeOrder.InputVoucherReturnBO.TotalAmountBFT = decTotalAmountBF_In;
                    objInputChangeOrder.InputVoucherReturnBO.TotalVAT = decTotalVAT_In;
                    objInputChangeOrder.InputVoucherReturnBO.TotalAmount = decTotalAmountBF_In + decTotalVAT_In;
                    objInputChangeOrder.InputVoucherReturnBO.IsNew = objInputVoucher.IsNew;
                    objInputChangeOrder.InputVoucherReturnBO.TotalReturnFee = decTotalReturnFee;
                    if (decTotalReturnFee != 0)
                        objInputChangeOrder.InputVoucherReturnBO.IsReturnWithFee = true;
                    else
                        objInputChangeOrder.InputVoucherReturnBO.IsReturnWithFee = false;
                }

                List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail> lst = new List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail>();
                decimal debt = 0;

                if (lstAddVoucherDetail.Count > 0)
                {
                    foreach (var item in lstAddVoucherDetail)
                    {
                        PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                        objVoucherDetail.VNDCash = item.VNDCash;
                        objVoucherDetail.PaymentCardAmount = item.PaymentCardAmount;
                        objVoucherDetail.PaymentCardID = item.PaymentCardID;
                        objVoucherDetail.PaymentCardName = item.PaymentCardName;
                        objVoucherDetail.PaymentCardSpend = item.PaymentCardSpend;
                        objVoucherDetail.PaymentCardVoucherID = item.PaymentCardVoucherID;
                        objVoucherDetail.RefundAmount = 0;
                        objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                        objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objVoucherDetail.VoucherStoreID = intStoreID;
                        objVoucherDetail.CreatedStoreID = intCreateStoreID;
                        debt += (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                        lst.Add(objVoucherDetail);

                    }
                }
                else
                {
                    PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDetail = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                    objVoucherDetail.VNDCash = decVNDCash;
                    objVoucherDetail.PaymentCardAmount = decPaymentCardAmount;
                    objVoucherDetail.PaymentCardID = intPaymentCardID;
                    objVoucherDetail.PaymentCardName = strPaymentCardName;
                    objVoucherDetail.PaymentCardSpend = decPaymentCardSpend;
                    objVoucherDetail.PaymentCardVoucherID = strPaymentCardVoucherID;
                    objVoucherDetail.RefundAmount = 0;
                    objVoucherDetail.CashierUser = SystemConfig.objSessionUser.UserName;
                    objVoucherDetail.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objVoucherDetail.VoucherStoreID = intStoreID;
                    objVoucherDetail.CreatedStoreID = intCreateStoreID;
                    debt += (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                    lst.Add(objVoucherDetail);
                }
                PLC.InputChangeOrder.WSInputChangeOrder.Voucher objVoucher = new PLC.InputChangeOrder.WSInputChangeOrder.Voucher();
                objVoucher.VoucherStoreID = intStoreID;
                objVoucher.CashierUser = SystemConfig.objSessionUser.UserName;
                objVoucher.InvoiceID = txtInVoiceID.Text.Trim();
                objVoucher.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                objVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                objVoucher.CustomerName = txtCustomerName.Text.Trim();
                objVoucher.CustomerAddress = txtCustomerAddress.Text.Trim();
                objVoucher.CustomerPhone = txtCustomerPhone.Text.Trim();
                objVoucher.CustomerTaxID = txtTaxID.Text.Trim();
                objVoucher.ProvinceID = 0;
                objVoucher.DistrictID = 0;
                objVoucher.Gender = false;
                objVoucher.AgeRangeID = 0;
                objVoucher.VoucherDate = DateTime.Now;
                objVoucher.InvoiceDate = DateTime.Now;
                objVoucher.OrderID = "";
                objVoucher.VoucherConcern = "";
                objVoucher.CreatedUser = SystemConfig.objSessionUser.UserName;
                objVoucher.Content = string.Empty;
                objVoucher.CurrencyUnitID = intCurrencyUnitID;
                objVoucher.CurrencyExchange = decCurrencyExchange;
                objVoucher.VAT = 0;
                objVoucher.Debt = 0;
                if (!bolIsDebt)
                {
                    if ((!objInputChangeOrderType.IsInputReturn) && (!string.IsNullOrEmpty(txtSaleOrderID.Text)))
                    {
                        objVoucher.TotalMoney = objInputVoucher.TotalAmount;
                        objVoucher.TotalLiquidate = objInputVoucher.TotalAmount;
                    }
                    else
                    {
                        objVoucher.TotalMoney = Convert.ToDecimal(txtReturnAmount.Text);
                        objVoucher.TotalLiquidate = Convert.ToDecimal(txtReturnAmount.Text);
                    }
                    objVoucher.Debt = objVoucher.TotalLiquidate - debt;
                }
                else
                {
                    if ((!objInputChangeOrderType.IsInputReturn) && (!string.IsNullOrEmpty(txtSaleOrderID.Text)))
                    {
                        objVoucher.TotalMoney = objInputVoucher.TotalAmount;
                        objVoucher.TotalLiquidate = objInputVoucher.TotalAmount;
                    }
                    else
                    {
                        objVoucher.TotalMoney = Convert.ToDecimal(txtReturnAmount.Text);
                        objVoucher.TotalLiquidate = Convert.ToDecimal(txtReturnAmount.Text);
                    }
                    if (bolIsNotCreateOutVoucherDetail)
                        objVoucher.Debt = objInputVoucher.TotalAmount;
                    else
                    {
                        //nếu trả hết ngay lần 1 thì xét lại Còn nợ của phiếu chi
                        //if (dtbDetailAll.Select("SalePrice > 0").Length == dtbOVDetailReturn.Select("SalePrice > 0").Length && !string.IsNullOrEmpty(objOutputVoucherLoad.OrderID))
                        //    objVoucher.Debt = objInputVoucher.TotalAmount - decTotalPaid;
                        //else
                        //{
                        //    objVoucher.Debt = objVoucher.TotalLiquidate - (objVoucherDetail.VNDCash + objVoucherDetail.ForeignCashExchange + objVoucherDetail.PaymentCardAmount - objVoucherDetail.RefundAmount);
                        //}
                        objVoucher.Debt = objVoucher.TotalLiquidate - debt;
                    }
                    objInputChangeOrder.IsNotCreateOutVoucherDetail = bolIsNotCreateOutVoucherDetail;
                }
                if (objVoucher.Debt < 0)
                    objVoucher.Debt = 0;
                objInputChangeOrder.IsDebt = bolIsDebt;
                objVoucher.IsInVoucherOfSaleOrder = false;
                objVoucher.CreatedStoreID = intCreateStoreID;
                objVoucher.OrderID = "";
                objVoucher.IsSupplementary = false;
                objVoucher.ShippingCost = 0;
                objVoucher.VoucherDetailList = lst.ToArray();
                decimal decSumInputPrice = GetCompute_In(string.Empty);
                decimal decSumSalePrice = 0;
                DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select(string.Empty);
                for (int i = 0; i < drDetail.Length; i++)
                {
                    if (drDetail[i]["SalePrice_Out"] != DBNull.Value)
                        decSumSalePrice += Convert.ToDecimal(drDetail[i]["SalePrice_Out"]) * Convert.ToDecimal(drDetail[i]["Quantity"]);
                }
                if (decSumInputPrice != decSumSalePrice)
                {
                    if (decSumInputPrice > decSumSalePrice)
                    {
                        objVoucher.IsSpend = true;
                        objVoucher.VoucherTypeID = objInputChangeOrderType.OutVoucherTypeID;
                        if (objVoucher.VoucherTypeID < 1)
                        {
                            DataRow[] drOutVoucherType = dtbInputTypeAll.Select("InputTypeID = " + objInputVoucher.InputTypeID);
                            if (drOutVoucherType.Length > 0)
                                objVoucher.VoucherTypeID = Convert.ToInt32(drOutVoucherType[0]["VoucherTypeID"]);
                        }
                        if (objVoucher.VoucherTypeID < 1)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức chi tiền từ hình thức [" + cboInputTypeID.Text + "]. Vui lòng kiểm tra lại");
                            btnCreateConcern.Enabled = true;
                            return;
                        }
                    }
                    else
                    {
                        objVoucher.IsSpend = false;
                        objVoucher.VoucherTypeID = objInputChangeOrderType.InVoucherTypeID;
                        if (objVoucher.VoucherTypeID < 1)
                            objVoucher.VoucherTypeID = objPLCOutputVoucher.CheckVoucherByOrderID(objOutputVoucherLoad.OrderID, objOutputVoucherLoad.OutputVoucherID);
                        if (objVoucher.VoucherTypeID < 1)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức thu tiền từ phiếu xuất cũ " + objOutputVoucherLoad.OutputVoucherID + ". Vui lòng kiểm tra lại");
                            btnCreateConcern.Enabled = true;
                            return;
                        }
                    }
                }
                else
                {
                    if (objInputChangeOrderType.IsInputReturn && objInputChangeOrder.IsDebt)
                    {
                        objVoucher.IsSpend = true;
                        objVoucher.VoucherTypeID = objInputChangeOrderType.OutVoucherTypeID;
                        if (objVoucher.VoucherTypeID < 1)
                        {
                            DataRow[] drOutVoucherType = dtbInputTypeAll.Select("InputTypeID = " + objInputVoucher.InputTypeID);
                            if (drOutVoucherType.Length > 0)
                                objVoucher.VoucherTypeID = Convert.ToInt32(drOutVoucherType[0]["VoucherTypeID"]);
                        }
                        if (objVoucher.VoucherTypeID < 1)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Thông báo", "Không lấy được hình thức chi tiền từ hình thức [" + cboInputTypeID.Text + "]. Vui lòng kiểm tra lại");
                            btnCreateConcern.Enabled = true;
                            return;
                        }
                    }
                }

                PLC.InputChangeOrder.WSInputChangeOrder.Voucher objVoucherForSaleOrder = null;
                List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail> lstForSaleOrder = new List<PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail>();
                #region Phiếu thu tiền đơn hàng
                if ((!objInputChangeOrderType.IsInputReturn) && (!string.IsNullOrEmpty(txtSaleOrderID.Text)))
                {
                    //objVoucher.VoucherDetailList[0].VNDCash = decVNDCash_Out;
                    //objVoucher.VoucherDetailList[0].PaymentCardAmount = decPaymentCardAmount_Out;
                    //objVoucher.VoucherDetailList[0].PaymentCardID = intPaymentCardID_Out;
                    //objVoucher.VoucherDetailList[0].PaymentCardSpend = decPaymentCardSpend_Out;
                    //objVoucher.VoucherDetailList[0].PaymentCardVoucherID = strPaymentCardVoucherID_Out;

                    //objVoucher.Debt = objVoucher.TotalLiquidate - (objVoucher.VoucherDetailList[0].VNDCash + objVoucher.VoucherDetailList[0].ForeignCashExchange + objVoucher.VoucherDetailList[0].PaymentCardAmount - objVoucher.VoucherDetailList[0].RefundAmount);
                    debt = 0;
                    if (lstAddVoucherDetail_In.Count > 0)
                    {
                        foreach (var item in lstAddVoucherDetail_In)
                        {
                            PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDTForSaleOrder = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                            objVoucherDTForSaleOrder.VNDCash = item.VNDCash;
                            objVoucherDTForSaleOrder.PaymentCardAmount = item.PaymentCardAmount;
                            objVoucherDTForSaleOrder.PaymentCardID = item.PaymentCardID;
                            objVoucherDTForSaleOrder.PaymentCardSpend = item.PaymentCardSpend;
                            objVoucherDTForSaleOrder.PaymentCardVoucherID = item.PaymentCardVoucherID;
                            if (item.RefundAmount > 0)
                                objVoucherDTForSaleOrder.RefundAmount = item.RefundAmount;

                            objVoucherDTForSaleOrder.RefundAmount = 0;
                            objVoucherDTForSaleOrder.CashierUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDTForSaleOrder.CreatedUser = SystemConfig.objSessionUser.UserName;
                            objVoucherDTForSaleOrder.VoucherStoreID = intStoreID;
                            objVoucherDTForSaleOrder.CreatedStoreID = intCreateStoreID;
                            debt += (objVoucherDTForSaleOrder.VNDCash + objVoucherDTForSaleOrder.ForeignCashExchange + objVoucherDTForSaleOrder.PaymentCardAmount - objVoucherDTForSaleOrder.RefundAmount);
                            lstForSaleOrder.Add(objVoucherDTForSaleOrder);
                        }
                    }
                    else
                    {
                        PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail objVoucherDTForSaleOrder = new PLC.InputChangeOrder.WSInputChangeOrder.VoucherDetail();
                        objVoucherDTForSaleOrder.VNDCash = decVNDCash_In;
                        objVoucherDTForSaleOrder.PaymentCardAmount = decPaymentCardAmount_In;
                        objVoucherDTForSaleOrder.PaymentCardID = intPaymentCardID_In;
                        objVoucherDTForSaleOrder.PaymentCardSpend = decPaymentCardSpend_In;
                        objVoucherDTForSaleOrder.PaymentCardVoucherID = strPaymentCardVoucherID_In;
                        if (decRefundAmount_In > 0)
                            objVoucherDTForSaleOrder.RefundAmount = decRefundAmount_In;

                        objVoucherDTForSaleOrder.RefundAmount = 0;
                        objVoucherDTForSaleOrder.CashierUser = SystemConfig.objSessionUser.UserName;
                        objVoucherDTForSaleOrder.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objVoucherDTForSaleOrder.VoucherStoreID = intStoreID;
                        objVoucherDTForSaleOrder.CreatedStoreID = intCreateStoreID;
                        debt += (objVoucherDTForSaleOrder.VNDCash + objVoucherDTForSaleOrder.ForeignCashExchange + objVoucherDTForSaleOrder.PaymentCardAmount - objVoucherDTForSaleOrder.RefundAmount);
                        lstForSaleOrder.Add(objVoucherDTForSaleOrder);
                    }
                    objVoucherForSaleOrder = new PLC.InputChangeOrder.WSInputChangeOrder.Voucher();
                    objVoucherForSaleOrder.VoucherStoreID = intStoreID;
                    objVoucherForSaleOrder.CashierUser = SystemConfig.objSessionUser.UserName;
                    objVoucherForSaleOrder.IsInVoucherOfSaleOrder = true;
                    objVoucherForSaleOrder.InvoiceID = txtInVoiceID.Text.Trim();
                    objVoucherForSaleOrder.InvoiceSymbol = txtInvoiceSymbol.Text.Trim();
                    objVoucherForSaleOrder.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                    objVoucherForSaleOrder.CustomerName = txtCustomerName.Text.Trim();
                    objVoucherForSaleOrder.CustomerAddress = txtCustomerAddress.Text.Trim();
                    objVoucherForSaleOrder.CustomerPhone = txtCustomerPhone.Text.Trim();
                    objVoucherForSaleOrder.CustomerTaxID = txtTaxID.Text.Trim();
                    objVoucherForSaleOrder.ProvinceID = 0;
                    objVoucherForSaleOrder.DistrictID = 0;
                    objVoucherForSaleOrder.Gender = false;
                    objVoucherForSaleOrder.AgeRangeID = 0;
                    objVoucherForSaleOrder.VoucherDate = DateTime.Now;
                    objVoucherForSaleOrder.InvoiceDate = DateTime.Now;
                    objVoucherForSaleOrder.OrderID = "";
                    objVoucherForSaleOrder.VoucherConcern = "";
                    objVoucherForSaleOrder.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objVoucherForSaleOrder.Content = string.Empty;
                    objVoucherForSaleOrder.CurrencyUnitID = intCurrencyUnitID;
                    objVoucherForSaleOrder.CurrencyExchange = decCurrencyExchange;
                    objVoucherForSaleOrder.TotalMoney = objSaleOrder_Out.TotalLiquidate;
                    objVoucherForSaleOrder.TotalLiquidate = objSaleOrder_Out.TotalLiquidate;
                    objVoucherForSaleOrder.TotalPaid = objSaleOrder_Out.TotalLiquidate;
                    objVoucherForSaleOrder.VoucherTypeID = objSaleOrder_Out.VoucherTypeID;

                    objVoucherForSaleOrder.Debt = 0;
                    if (!bolIsDebt)
                    {
                        objVoucherForSaleOrder.Debt = objVoucher.TotalLiquidate - debt;
                    }
                    else
                    {
                        if (bolIsNotCreateOutVoucherDetail)
                            objVoucherForSaleOrder.Debt = objInputVoucher.TotalAmount;
                        else
                        {
                            objVoucherForSaleOrder.Debt = objVoucher.TotalLiquidate - debt;
                        }
                        objInputChangeOrder.IsNotCreateOutVoucherDetail = bolIsNotCreateOutVoucherDetail;
                    }
                    if (objVoucher.Debt < 0)
                        objVoucherForSaleOrder.Debt = 0;
                    objVoucherForSaleOrder.Debt = 0;
                    objVoucherForSaleOrder.VAT = 0;
                    objVoucherForSaleOrder.CreatedStoreID = intCreateStoreID;
                    objVoucherForSaleOrder.OrderID = "";
                    objVoucherForSaleOrder.IsSupplementary = false;
                    objVoucherForSaleOrder.ShippingCost = 0;
                    objVoucherForSaleOrder.VoucherDetailList = lstForSaleOrder.ToArray();//new[] { objVoucherDTForSaleOrder };
                    objVoucherForSaleOrder.VoucherConcern = txtSaleOrderID.Text.Trim();
                    if (intPaymentCardID_In > 0)
                        objVoucherForSaleOrder.PaymentCardInfo = "Thanh toán bằng " + strPaymentCardName_In + ": " + decPaymentCardAmount_In.ToString("#,##0") + " - Số HĐ cà thẻ: " + strPaymentCardVoucherID_In;

                }
                #endregion

                bool bolResult = false;
                //nếu là loại nhập đổi trả hàng
                if (!objInputChangeOrderType.IsInputReturn)
                {
                    if (string.IsNullOrEmpty(objInputChangeOrder.SaleOrderID))
                    {
                        if ((!objVoucher.IsSpend) && objVoucher.VoucherDetailList.Length > 0 && objVoucher.VoucherDetailList[0].PaymentCardID > 0)
                        {
                            string strPaymentCardInfo = "Thanh toán bằng " + objVoucher.VoucherDetailList[0].PaymentCardName + ": " + objVoucher.VoucherDetailList[0].PaymentCardAmount.ToString("#,##0") + " - Số HĐ cà thẻ: " + objVoucher.VoucherDetailList[0].PaymentCardVoucherID;
                            objOutputVoucher.OutputContent = strPaymentCardInfo;
                        }
                        else
                        {
                            if (Math.Round(Convert.ToDecimal(txtReturnAmount.Text)) == 0 && txtContentOV.Text.Trim() != string.Empty)
                                objOutputVoucher.OutputContent = txtContentOV.Text.Trim();
                            else
                            {
                                if (txtContentOV.Text.ToLower().Replace(" ", string.Empty).IndexOf("sốhđcàthẻ") > 0)
                                    objOutputVoucher.OutputContent = txtContentOV.Text.Trim();
                            }
                        }
                        //Đổi hàng
                        bolResult = objPLCInputChangeOrder.CreateInputVoucherAndOutputVoucherAndInOutVoucher(objInputChangeOrder, objInputVoucher, objOutputVoucher, objVoucher, null);
                    }
                    else//Nhập đổi
                        bolResult = objPLCInputChangeOrder.CreateInputVoucherAndOutVoucher(objInputChangeOrder, objInputVoucher, objVoucher, objVoucherForSaleOrder);
                }
                else// Trả hàng
                    bolResult = objPLCInputChangeOrder.CreateInputVoucherAndOutVoucher(objInputChangeOrder, objInputVoucher, objVoucher, null);

                if (!bolResult)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnCreateConcern.Enabled = true;
                    return;
                }

                MessageBox.Show(this, "Xử lý yêu cầu " + strMessageForm + " thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsHasAction = true;

                //this.Close();
                PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder InputChangeOrderTMP = objPLCInputChangeOrder.LoadInfo(strInputChangeOrderID);
                chkIsInputchange.Checked = true;
                btnCreateConcern.Enabled = false;
                lblReviewStatus.Text = "Yêu cầu " + strMessageForm + " đã được xử lý";
                txtContent.BackColor = System.Drawing.SystemColors.Info;
                txtContent.ReadOnly = true;
                txtReasonNote.ReadOnly = true;
                txtReasonNote.BackColor = System.Drawing.SystemColors.Info;
                cboReason.Enabled = false;
                //txtReason.Visible = true;
                //if (cboReason.ColumnID > 0)
                //    txtReason.Text = cboReason.ColumnName;
                //else
                //    txtReason.Text = string.Empty;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                btnDownload.Enabled = false;

                DevExpress.XtraBars.BarButtonItem ItemReportInput = new DevExpress.XtraBars.BarButtonItem();
                ItemReportInput.Caption = "Phiếu nhập trả";
                ItemReportInput.Name = "ItemReport" + InputChangeOrderTMP.NewInputVoucherID;
                ItemReportInput.Tag = InputChangeOrderTMP.NewInputVoucherID;
                ItemReportInput.Enabled = true;
                ItemReportInput.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ItemReportInput_ItemClick);
                pMnuPrint.AddItem(ItemReportInput);
                txtNewInputVoucherID.Text = InputChangeOrderTMP.NewInputVoucherID;
                dtpInputchangeDate.EditValue = InputChangeOrderTMP.InputChangeDate;
                if (!string.IsNullOrEmpty(txtSaleOrderID.Text))
                {
                    if (objSaleOrder_Out != null && !string.IsNullOrEmpty(objSaleOrder_Out.SaleOrderID))
                    {
                        ERP.MasterData.PLC.MD.WSSaleOrderType.SaleOrderType objSaleOrderType = objPLCSaleOrderType.LoadInfoFromCache(objSaleOrder_Out.SaleOrderTypeID);
                        if (objSaleOrderType != null && objSaleOrderType.DtbSaleOrderType_RP_Report != null && objSaleOrderType.DtbSaleOrderType_RP_Report.Rows.Count > 0)
                        {
                            bool bolIsOutProduct = true;
                            bool bolIsInCome = true;
                            bool bolIsReview = true;
                            String strExp = "ISPRINTVAT = 0 ";
                            if (!bolIsReview)
                                strExp += "AND PRINTREPORTTYPE <> 2";
                            if (!bolIsInCome)
                                strExp += "AND PRINTREPORTTYPE <> 5";
                            if (!bolIsOutProduct)
                                strExp += "AND PRINTREPORTTYPE <> 3";
                            DataTable tblReportList = objSaleOrderType.DtbSaleOrderType_RP_Report;
                            DataTable tblReport = Globals.SelectDistinct(tblReportList, "ReportID", strExp);
                            foreach (DataRow objRow in tblReport.Rows)
                            {
                                ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(Convert.ToInt32(objRow["ReportID"]));
                                DevExpress.XtraBars.BarButtonItem ItemReportSaleOrder = new DevExpress.XtraBars.BarButtonItem();
                                ItemReportSaleOrder.Caption = Convert.ToString(objRow["ReportName"]).Trim();
                                ItemReportSaleOrder.Name = txtSaleOrderID.Text;
                                ItemReportSaleOrder.Tag = objRow;
                                ItemReportSaleOrder.Enabled = true;
                                ItemReportSaleOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ItemReportSaleOrder_ItemClick);
                                pMnuPrint.AddItem(ItemReportSaleOrder);
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(InputChangeOrderTMP.NewOutputVoucherID))
                    {
                        DevExpress.XtraBars.BarButtonItem ItemReportOutput = new DevExpress.XtraBars.BarButtonItem();
                        ItemReportOutput.Caption = "Phiếu xuất đổi";
                        ItemReportOutput.Name = "ItemReport" + InputChangeOrderTMP.NewOutputVoucherID;
                        ItemReportOutput.Tag = InputChangeOrderTMP.NewOutputVoucherID;
                        ItemReportOutput.Enabled = true;
                        ItemReportOutput.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ItemReportOutput_ItemClick);
                        pMnuPrint.AddItem(ItemReportOutput);
                        txtNewOutputVoucherID.Text = InputChangeOrderTMP.NewOutputVoucherID;
                    }
                }
                btnPrint.Visible = true;
                btnCreateConcern.Visible = false;
                btnSupportCare.Visible = false;
                tabControl.SelectedTab = tabPageInputChangeOrder;
                CheckAfterEditFunction();

            }
            catch (Exception objEx)
            {
                btnCreateConcern.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi xử lý yêu cầu " + strMessageForm, objEx.Message + System.Environment.NewLine + objEx.StackTrace);
            }
        }

        private void ItemReportOutput_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e == null || e.Item == null)
                return;
            DevExpress.XtraBars.BarButtonItem barItem = e.Item as DevExpress.XtraBars.BarButtonItem;
            if (barItem.Tag == null)
                return;
            string strNewOutputVoucherID = barItem.Tag.ToString();
            Hashtable hstbReportParam = new Hashtable();
            bool bolIsViewCostPrice = SystemConfig.objSessionUser.IsPermission("OUTPUTREPORT_VIEWCOSTPRICE");
            hstbReportParam.Add("OutputVoucherID", strNewOutputVoucherID);
            hstbReportParam.Add("IsCostPriceHide", !bolIsViewCostPrice);
            hstbReportParam.Add("IsSalePriceHide", false);
            MasterData.PLC.MD.PLCReport objPLCReport = new MasterData.PLC.MD.PLCReport();
            ERP.MasterData.PLC.MD.WSReport.Report objReport = new MasterData.PLC.MD.WSReport.Report();
            objPLCReport.LoadInfo(ref objReport, intReportID);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, hstbReportParam);
        }

        private void ItemReportInput_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e == null || e.Item == null)
                return;
            DevExpress.XtraBars.BarButtonItem barItem = e.Item as DevExpress.XtraBars.BarButtonItem;
            if (barItem.Tag == null)
                return;
            string strNewInputVoucherID = barItem.Tag.ToString();
            Reports.Input.frmReportView frm = new Reports.Input.frmReportView();
            frm.InputVoucherID = strNewInputVoucherID;
            frm.bolIsPriceHide = false;
            frm.ShowDialog();
        }

        private void ItemReportSaleOrder_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e == null || e.Item == null)
                return;
            DevExpress.XtraBars.BarButtonItem barItem = e.Item as DevExpress.XtraBars.BarButtonItem;
            if (barItem.Tag == null)
                return;
            DataRow objReportInfo = barItem.Tag as DataRow;
            int intReportID = Convert.ToInt32(objReportInfo["REPORTID"]);
            String strPrinterTypeID = Convert.ToString(objReportInfo["PRINTERTYPEID"]).Trim();
            String strSaleOrderID = barItem.Name.ToString();
            SalesAndServices.SaleOrders.DUI.DUISaleOrders_Common.ShowReport(this, strSaleOrderID, intReportID, strPrinterTypeID);
        }

        private void ShowPaymentForm1(bool bolIsSpend, ref bool bolIsOKPayment, string strTextForm, decimal VNDCash, decimal PaymentCardAmount, int PaymentCardID, decimal PaymentCardSpend, string PaymentCardVoucherID, decimal ReturnAmount,
                                        ref decimal refVNDCash, ref decimal refPaymentCardAmount, ref int refPaymentCardID, ref string refPaymentCardName, ref decimal refPaymentCardSpend, ref string refPaymentCardVoucherID, ref List<ERP.SalesAndServices.Payment.PLC.WSVoucher.VoucherDetail> lstVoucherDetail)
        {
            POPUP:
            ERP.SalesAndServices.SaleOrders.DUI.frmFullPaymentForm frmFullPaymentForm1 = new ERP.SalesAndServices.SaleOrders.DUI.frmFullPaymentForm();
            frmFullPaymentForm1.TotalLiquidate = ReturnAmount;
            frmFullPaymentForm1.VNDCash = VNDCash;
            frmFullPaymentForm1.ForeignCash = 0;
            frmFullPaymentForm1.CurrencyUnitID = ERP.MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyUnitDefaultID();
            frmFullPaymentForm1.ForeignCashExchange = 0;
            frmFullPaymentForm1.PaymentCardAmount = PaymentCardAmount;
            frmFullPaymentForm1.PaymentCardID = PaymentCardID;
            frmFullPaymentForm1.PaymentCardSpend = PaymentCardSpend;
            frmFullPaymentForm1.PaymentCardVoucherID = PaymentCardVoucherID;
            if (lblTitlePayment.Text == "Phải thu" && strTextForm == "Thu tiền thanh toán đơn hàng")
                frmFullPaymentForm1.RefundAmount = decRefundAmount_In;
            else
                frmFullPaymentForm1.RefundAmount = 0;
            frmFullPaymentForm1.IsAllowDebt = bolIsSpend;
            frmFullPaymentForm1.IsApplyGiftVoucher = true;
            frmFullPaymentForm1.TotalAmountOfOrder = ReturnAmount;
            frmFullPaymentForm1.GiftVoucherTable = null;
            frmFullPaymentForm1.GiftVoucherAmount = 0;
            frmFullPaymentForm1.SaleOrderDetailList = new List<SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrderDetail>();
            frmFullPaymentForm1.OutputStoreID = cboStore.StoreID;
            frmFullPaymentForm1.UneventPriceGiftVoucher = 0;
            frmFullPaymentForm1.OutputTypePromotionID = 0;
            frmFullPaymentForm1.OutputTypeSalePromotionID = 0;
            if (strTextForm != string.Empty)
                frmFullPaymentForm1.Text = strTextForm;
            frmFullPaymentForm1.IsAllowMulti = true;
            frmFullPaymentForm1.ShowDialog(this);
            if (frmFullPaymentForm1.IsOK)
            {
                if (lblTitlePayment.Text == "Phải chi" && strTextForm != "Thu tiền thanh toán đơn hàng" && frmFullPaymentForm1.RefundAmount > 0)
                {
                    MessageBox.Show(this, "Vui lòng chi đủ số tiền", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VNDCash = frmFullPaymentForm1.VNDCash;
                    PaymentCardAmount = frmFullPaymentForm1.PaymentCardAmount;
                    PaymentCardID = frmFullPaymentForm1.PaymentCardID;
                    PaymentCardSpend = frmFullPaymentForm1.PaymentCardSpend;
                    PaymentCardVoucherID = frmFullPaymentForm1.PaymentCardVoucherID;
                    goto POPUP;
                }
                lstVoucherDetail = frmFullPaymentForm1.LstAddVoucherDetail;
                refVNDCash = frmFullPaymentForm1.VNDCash;
                refPaymentCardAmount = frmFullPaymentForm1.PaymentCardAmount;
                refPaymentCardID = frmFullPaymentForm1.PaymentCardID;
                refPaymentCardName = frmFullPaymentForm1.PaymentCardName;
                refPaymentCardSpend = frmFullPaymentForm1.PaymentCardSpend;
                refPaymentCardVoucherID = frmFullPaymentForm1.PaymentCardVoucherID;
                bolIsOKPayment = frmFullPaymentForm1.IsOK;
                if (lblTitlePayment.Text == "Phải thu" && strTextForm == "Thu tiền thanh toán đơn hàng")
                    decRefundAmount_In = frmFullPaymentForm1.RefundAmount;
            }
        }

        private void flexDetail_CellButtonClick(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexDetail.Cols["ProductID_Out"].Index)
            {
                if (cboStore.StoreID < 1)
                {
                    MessageBox.Show(this, "Bạn chưa chọn kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboStore.Focus();
                    return;
                }
                ERP.MasterData.DUI.MasterData_Globals.ProductSearchParentForm_Text = this.Text;
                ERP.MasterData.DUI.Search.frmProductSearch frm = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
                frm.IsMultiSelect = false;
                frm.ShowDialog();
                if (frm.Product != null)
                {
                    string strProduct = Convert.ToString(frm.Product.ProductID).Trim();
                    StringBuilder arrAutoGetIMEIList = new StringBuilder();
                    ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(flexDetail[e.Row, "OutputTypeID_Out"]));
                    ERP.MasterData.PLC.MD.WSProduct.Product objProductIn = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim());
                    if (objInputChangeOrderType.ProductChangeConditionType == 2 || objInputChangeOrderType.ProductChangeConditionType == 3)
                    {
                        if (objInputChangeOrderType.ProductChangeConditionType == 2 && Convert.ToString(objProductIn.ProductID).Trim() != strProduct)
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng mã sản phẩm. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        if (objInputChangeOrderType.ProductChangeConditionType == 3)
                        {
                            if (Convert.ToString(objProductIn.ModelID).Trim() != Convert.ToString(frm.Product.ModelID).Trim())
                            {
                                MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            else
                            {
                                if (Convert.ToString(objProductIn.ModelID).Trim() == string.Empty && Convert.ToString(objProductIn.ModelID).Trim() == Convert.ToString(frm.Product.ModelID).Trim())
                                {
                                    MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                        }
                    }

                    int intIsRequestIMEI_In = Convert.ToBoolean(flexDetail[e.Row, "IsRequestIMEI"]) ? 1 : 0;
                    int intIsRequestIMEI_Out = frm.Product.IsRequestIMEI ? 1 : 0;
                    if (intIsRequestIMEI_In != intIsRequestIMEI_Out)
                    {
                        MessageBox.Show(this, "Sản phẩm nhập " + (intIsRequestIMEI_In == 1 ? "có" : "không có") + " yêu cầu IMEI. Vui lòng chọn sản phẩm xuất " + (intIsRequestIMEI_In == 1 ? "có" : "không có") + " yêu cầu IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(strProduct, cboStore.StoreID, Convert.ToInt32(flexDetail[e.Row, "OutputTypeID_Out"]), arrAutoGetIMEIList.ToString(), Convert.ToBoolean(flexDetail[e.Row, "IsNew"]));
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    if (!objProductInStock.IsInstock)
                    {
                        MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductName + " không tồn kho. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (objProductInStock.IMEI == string.Empty && (objProductInStock.Quantity - Convert.ToDecimal(flexDetail[e.Row, "Quantity"])) < objProductInStock.MinInStock)
                    {
                        MessageBox.Show(this, "Mặt hàng " + objProductInStock.ProductName + " không đủ số lượng tồn kho tối thiểu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    //Lấy giá bán
                    decimal decSalePrice = objProductInStock.Price;
                    switch (objOutputType_Out.GetPriceType)
                    {
                        case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.COSTPRICE:
                            decSalePrice = objProductInStock.CostPrice;
                            break;
                        case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO:
                            decSalePrice = 0;
                            break;
                        case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.FIRSTPRICE:
                            decSalePrice = objProductInStock.FirstPrice;
                            break;
                    }
                    if (decSalePrice == 0 && objOutputType_Out.GetPriceType != (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO)
                    {
                        MessageBox.Show(this, "Mặt hàng " + objProductInStock.ProductName + " chưa được thiết lập giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    string strIMEIInStock = string.Empty;
                    if (!string.IsNullOrEmpty(objProductInStock.IMEI))
                        strIMEIInStock = Convert.ToString(objProductInStock.IMEI).Trim();
                    if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                        decSalePrice = decSalePrice * (Convert.ToDecimal(1) + Convert.ToDecimal(objProductInStock.VAT) * Convert.ToDecimal(objProductInStock.VATPercent) * Convert.ToDecimal(0.0001));

                    if (objInputChangeOrderType.ProductChangeConditionType == 4
                        || objInputChangeOrderType.ProductChangeConditionType == 5
                        || objInputChangeOrderType.ProductChangeConditionType == 6)
                    {
                        if (objInputChangeOrderType.ProductChangeConditionType == 5 && decSalePrice < Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"]))
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả với sản phẩm xuất có giá bán lớn hơn hoặc bằng giá nhập lại. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else if (objInputChangeOrderType.ProductChangeConditionType == 6 && decSalePrice >= Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"]))
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả với sản phẩm xuất có giá bán nhỏ hơn giá nhập lại. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else if (objInputChangeOrderType.ProductChangeConditionType == 4 && decSalePrice != Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"]))
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng giá(giá nhập lại phải bằng giá bán của sản phẩm xuất). Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    if (strIMEIInStock != string.Empty)
                    {
                        if (dtbOVDetailReturn.Select("TRIM(IMEI_Out) = '" + strIMEIInStock + "'").Length > 1)
                        {
                            MessageBox.Show(this, "Số IMEI " + strIMEIInStock + " đã tồn tại trên lưới dữ liệu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    flexDetail[e.Row, "ProductID_Out"] = strProduct;
                    flexDetail[e.Row, "ProductName_Out"] = frm.Product.ProductName;
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIInStock;
                    flexDetail[e.Row, "SalePrice_Out"] = decSalePrice;
                    if (objOutputType_Out.GetPriceType != (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO && Convert.ToString(flexDetail[e.Row, "ProductID_Out"]).Trim() == Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim() && objProductInStock.IsPriceOfProduct)
                        flexDetail[e.Row, "SalePrice_Out"] = flexDetail[e.Row, "SalePriceVAT"];
                    flexDetail[e.Row, "IsRequestIMEI_Out"] = frm.Product.IsRequestIMEI;

                    CalTotalMoney();
                }
            }
        }

        private decimal GetCompute_In(String strFilter)
        {
            decimal decResult = 0;
            decimal decSumInputPrice = 0;
            decimal decTotalFee = 0;
            DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select(strFilter);
            for (int i = 0; i < drDetail.Length; i++)
            {
                decTotalFee += Convert.ToDecimal(drDetail[i]["TotalFee"]);
                decSumInputPrice += Convert.ToDecimal(drDetail[i]["InputPrice_In"]) * Convert.ToDecimal(drDetail[i]["Quantity"]);
            }
            decResult = decSumInputPrice;
            if (objInputChangeOrderType.IsInputReturn && txtReturnAmount.Visible)
            {
                if (strInVoucherID != string.Empty)
                {
                    if (decTotalPaid == 0)
                    {
                        decResult = 0;
                        bolIsDebt = true;
                        bolIsNotCreateOutVoucherDetail = true;
                    }
                    else
                    {
                        if (Math.Round(decSumInputPrice + decTotalFee) > Math.Round(decTotalPaid))
                        {
                            decResult = decTotalPaid;
                            bolIsDebt = true;
                            DataTable dtbCheckDetailReturn1 = objPLCInputChangeOrder.GetListReturn(objOutputVoucherLoad.OutputVoucherID, objInputChangeOrder.InputChangeOrderID);
                            decimal decSumTotalAmount = Math.Abs(Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbCheckDetailReturn1.Compute("Sum(TotalAmount)", "Quantity < 0"), 0)));
                            decResult = decResult - decSumTotalAmount - decTotalFee;
                            if (decResult <= 0)
                            {
                                bolIsNotCreateOutVoucherDetail = true;
                                decResult = 0;
                            }
                        }
                        else
                        {

                            DataTable dtbCheckDetailReturn1 = objPLCInputChangeOrder.GetListReturn(objOutputVoucherLoad.OutputVoucherID, objInputChangeOrder.InputChangeOrderID);
                            decimal decSumTotalAmount = Math.Abs(Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbCheckDetailReturn1.Compute("Sum(TotalAmount)", "Quantity < 0"), 0)));
                            decimal decAmount = decTotalPaid - decSumTotalAmount;
                            if (Math.Round(decAmount) < Math.Round(decResult))
                                decResult = decAmount - decTotalFee;
                            if (decResult <= 0)
                            {
                                bolIsDebt = true;
                                bolIsNotCreateOutVoucherDetail = true;
                                decResult = 0;
                            }
                        }
                    }
                }
            }
            return Math.Round(decResult);
        }
        private decimal GetCompute_Out(String strFilter)
        {
            decimal decSumSalePrice = 0;
            DataRow[] drDetail = ((DataTable)flexDetail.DataSource).Select(strFilter);
            for (int i = 0; i < drDetail.Length; i++)
            {
                if (drDetail[i]["SalePrice_Out"] != DBNull.Value)
                    decSumSalePrice += Convert.ToDecimal(drDetail[i]["SalePrice_Out"]) * Convert.ToDecimal(drDetail[i]["Quantity"]);
            }
            if (objInputChangeOrder != null && !string.IsNullOrEmpty(objInputChangeOrder.SaleOrderID))
            {
                if (objSaleOrder_Out == null)
                    objSaleOrder_Out = objPLCSaleOrders.LoadInfo(objInputChangeOrder.SaleOrderID);
                if (objSaleOrder_Out != null && !string.IsNullOrEmpty(objSaleOrder_Out.SaleOrderID))
                {
                    decSumSalePrice = decSumSalePrice + objSaleOrder_Out.TotalLiquidate;
                    if (objSaleOrder_Out.TotalLiquidate != 0)
                    {
                        lblTotalLiquidate.Text = "Tổng tiền thanh toán của đơn hàng " + objSaleOrder_Out.SaleOrderID + " là " + objSaleOrder_Out.TotalLiquidate.ToString("#,##0");
                        lblTotalLiquidate.Visible = txtReturnAmount.Visible;
                    }
                }
            }
            return decSumSalePrice;
        }

        private void CalTotalMoney()
        {
            if (!objInputChangeOrderType.IsInputReturn)
            {
                if (txtReturnAmount.Visible)
                {
                    decimal decSumInputPrice = GetCompute_In("IsSelect = 1");
                    decimal decSumSalePrice = GetCompute_Out("IsSelect = 1");
                    if (Math.Round(decSumInputPrice) > Math.Round(decSumSalePrice))
                    {
                        lblTitlePayment.Text = "Phải chi";
                        txtReturnAmount.Text = (decSumInputPrice - decSumSalePrice).ToString("#,##0");
                    }
                    else
                    {
                        if (Math.Round(decSumInputPrice) == Math.Round(decSumSalePrice))
                            lblTitlePayment.Text = "Phải chi";
                        else
                            lblTitlePayment.Text = "Phải thu";
                        txtReturnAmount.Text = (decSumSalePrice - decSumInputPrice).ToString("#,##0");
                    }
                }
            }
            else
            {
                if (txtReturnAmount.Visible)
                {
                    lblTitlePayment.Text = "Phải chi";
                    decimal decSumInputPrice = GetCompute_In("IsSelect = 1");
                    txtReturnAmount.Text = decSumInputPrice.ToString("#,##0");
                }
            }
        }

        private void flexDetail_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexDetail.Rows.Fixed)
            {
                return;
            }
            if (dtbOVDetailReturn == null || dtbOVDetailReturn.Rows.Count <= (e.Row - flexDetail.Rows.Fixed))
                return;
            rOldEdit = dtbOVDetailReturn.NewRow();
            DataRow rData = dtbOVDetailReturn.Rows[e.Row - flexDetail.Rows.Fixed];
            rOldEdit.ItemArray = rData.ItemArray;

            if (flexDetail.Cols.Contains("IsSelect"))
            {
                if (e.Col != flexDetail.Cols["IsSelect"].Index && (!Convert.ToBoolean(flexDetail[e.Row, "IsSelect"])))
                {
                    e.Cancel = true;
                    return;
                }
            }
            if (e.Col == flexDetail.Cols["IMEI_Out"].Index)
            {
                if (flexDetail[e.Row, "ProductName_Out"] != DBNull.Value && Convert.ToString(flexDetail[e.Row, "ProductName_Out"]).Trim() != string.Empty)
                    e.Cancel = true;
            }
            else if (e.Col == flexDetail.Cols["Quantity"].Index)
            {
                if (rData["IMEI"] != DBNull.Value && Convert.ToString(rData["IMEI"]).Trim() != string.Empty)
                {
                    e.Cancel = true;
                }
                //if (objInputChangeOrderType != null && !objInputChangeOrderType.IsInputReturn)
                //{
                //    if (flexDetail[e.Row, "ProductName_Out"] == DBNull.Value || string.IsNullOrEmpty(flexDetail[e.Row, "ProductName_Out"].ToString()))
                //        e.Cancel = true;
                //}
            }
        }

        private void flexDetail_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexDetail.Rows.Fixed)
            {
                return;
            }
            dtbOVDetailReturn.AcceptChanges();
            if (e.Col == flexDetail.Cols["IMEI_Out"].Index)
            {
                string strIMEIOldEdit = string.Empty;
                if (rOldEdit["IMEI_Out"] != DBNull.Value)
                    strIMEIOldEdit = rOldEdit["IMEI_Out"].ToString().Trim();
                string strBarcode = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(flexDetail[e.Row, "IMEI_Out"].ToString().Trim());
                if (strBarcode.Length < 1)
                    return;
                if (!Library.AppCore.Other.CheckObject.CheckIMEI(strBarcode))
                {
                    MessageBox.Show(this, "IMEI không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }
                StringBuilder arrAutoGetIMEIList = new StringBuilder();
                ERP.MasterData.PLC.MD.WSOutputType.OutputType objOutputType_Out = ERP.MasterData.PLC.MD.PLCOutputType.LoadInfoFromCache(Convert.ToInt32(flexDetail[e.Row, "OutputTypeID_Out"]));
                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strBarcode);
                if (objProduct != null)
                {
                    MessageBox.Show(this, "IMEI này trùng với mã sản phẩm. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }
                ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, cboStore.StoreID, Convert.ToInt32(flexDetail[e.Row, "OutputTypeID_Out"]), arrAutoGetIMEIList.ToString());
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }
                if (objProductInStock == null || (!objProductInStock.IsInstock))
                {
                    MessageBox.Show(this, "Số IMEI " + strBarcode + " không tồn kho. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }

                ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct();
                decimal decOut = 0;
                objPLCBorrowProduct.LockOutPut_CHKIMEI(ref decOut, cboStore.StoreID, objProductInStock.ProductID, objProductInStock.IMEI);
                if (!string.IsNullOrWhiteSpace(objProductInStock.IMEI))
                {
                    if (decOut > 0)
                    {
                        MessageBox.Show(this, "IMEI [" + objProductInStock.IMEI + "] đang cho mượn, không được thao tác trên IMEI này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                }
                else
                {
                    if (decOut > 0)
                    {
                        objProductInStock.Quantity = objProductInStock.Quantity - decOut;
                    }
                    if (objProductInStock.Quantity <= 0)
                    {
                        MessageBox.Show(this, "Sản phẩm " + objProductInStock.ProductID + " - " + objProductInStock.ProductName + " không tồn kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }


                if (!Convert.ToBoolean(flexDetail[e.Row, "IsRequestIMEI"]))
                {
                    MessageBox.Show(this, "Sản phẩm nhập không có yêu cầu IMEI. Vui lòng chọn sản phẩm xuất không có yêu cầu IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (dtbOVDetailReturn.Select("TRIM(IMEI_Out) = '" + strBarcode + "'").Length > 1)
                {
                    MessageBox.Show(this, "Số IMEI " + strBarcode + " đã tồn tại trên lưới dữ liệu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }
                if (objInputChangeOrderType.IsNew == 1 || objInputChangeOrderType.IsNew == 0)
                {
                    bool bolIsNew_InputChangeOrderType = (objInputChangeOrderType.IsNew == 1 ? true : false);
                    if ((!objInputChangeOrderType.IsAllowChangeOtherStatus) && bolIsNew_InputChangeOrderType != objProductInStock.IsNew)
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả sản phẩm " + (bolIsNew_InputChangeOrderType ? "mới" : "cũ") + ". Số IMEI " + objProductInStock.IMEI + " đang tồn " + (objProductInStock.IsNew ? "mới" : "cũ") + ". Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                }
                //Lấy giá bán
                decimal decSalePrice = objProductInStock.Price;
                switch (objOutputType_Out.GetPriceType)
                {
                    case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.COSTPRICE:
                        decSalePrice = objProductInStock.CostPrice;
                        break;
                    case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO:
                        decSalePrice = 0;
                        break;
                    case (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.FIRSTPRICE:
                        decSalePrice = objProductInStock.FirstPrice;
                        break;
                }
                if (decSalePrice == 0 && objOutputType_Out.GetPriceType != (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO)
                {
                    MessageBox.Show(this, "Mặt hàng " + objProductInStock.ProductName + " chưa được thiết lập giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                    return;
                }

                if (objOutputType_Out != null && !objOutputType_Out.IsVATZero)
                    decSalePrice = decSalePrice * (Convert.ToDecimal(1) + Convert.ToDecimal(objProductInStock.VAT) * Convert.ToDecimal(objProductInStock.VATPercent) * Convert.ToDecimal(0.0001));

                ERP.MasterData.PLC.MD.WSProduct.Product objProductIn = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim());
                if (objInputChangeOrderType.ProductChangeConditionType == 2 || objInputChangeOrderType.ProductChangeConditionType == 3)
                {
                    if (objInputChangeOrderType.ProductChangeConditionType == 2 && Convert.ToString(objProductIn.ProductID).Trim() != Convert.ToString(objProductInStock.ProductID).Trim())
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng mã sản phẩm. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                    if (objInputChangeOrderType.ProductChangeConditionType == 3)
                    {
                        ERP.MasterData.PLC.MD.WSProduct.Product objProductOut = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(objProductInStock.ProductID);
                        if (Convert.ToString(objProductIn.ModelID).Trim() != Convert.ToString(objProductOut.ModelID).Trim())
                        {
                            MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                            return;
                        }
                        else
                        {
                            if (Convert.ToString(objProductIn.ModelID).Trim() == string.Empty && Convert.ToString(objProductIn.ModelID).Trim() == Convert.ToString(objProductOut.ModelID).Trim())
                            {
                                MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng model. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                                return;
                            }
                        }
                    }
                }

                if (objInputChangeOrderType.ProductChangeConditionType == 4
                    || objInputChangeOrderType.ProductChangeConditionType == 5
                    || objInputChangeOrderType.ProductChangeConditionType == 6)
                {
                    if (objInputChangeOrderType.ProductChangeConditionType == 5 && decSalePrice < Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"]))
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả với sản phẩm xuất có giá bán lớn hơn hoặc bằng giá nhập lại. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                    else if (objInputChangeOrderType.ProductChangeConditionType == 6 && decSalePrice >= Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"]))
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả với sản phẩm xuất có giá bán nhỏ hơn giá nhập lại. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                    else if (objInputChangeOrderType.ProductChangeConditionType == 4 && decSalePrice != Convert.ToDecimal(flexDetail[e.Row, "InputPrice_In"]))
                    {
                        MessageBox.Show(this, objInputChangeOrderType.InputChangeOrderTypeName + " chỉ cho phép nhập đổi trả cùng giá(giá nhập lại phải bằng giá bán của sản phẩm xuất). Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI_Out"] = strIMEIOldEdit;
                        return;
                    }
                }
                flexDetail[e.Row, "ProductID_Out"] = objProductInStock.ProductID;
                flexDetail[e.Row, "ProductName_Out"] = objProductInStock.ProductName;
                flexDetail[e.Row, "IsRequestIMEI_Out"] = objProductInStock.IsRequestIMEI;
                flexDetail[e.Row, "IMEI_Out"] = strBarcode;
                flexDetail[e.Row, "SalePrice_Out"] = decSalePrice;
                if (objOutputType_Out.GetPriceType != (int)ERP.MasterData.PLC.MD.PLCOutputType.GetPriceTypeEnum.PRICEZERO && Convert.ToString(flexDetail[e.Row, "ProductID_Out"]).Trim() == Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim() && objProductInStock.IsPriceOfProduct)
                    flexDetail[e.Row, "SalePrice_Out"] = flexDetail[e.Row, "SalePriceVAT"];
            }
            else if (e.Col == flexDetail.Cols["IsSelect"].Index)
            {
                LoadInputType();
            }
            else if (e.Col == flexDetail.Cols["Quantity"].Index)
            {
                decimal decQuantity = Convert.ToDecimal(rOldEdit["Quantity2"]);
                if (Convert.ToDecimal(flexDetail[e.Row, "Quantity"]) > decQuantity || Convert.ToDecimal(flexDetail[e.Row, "Quantity"]) < 1)
                    flexDetail[e.Row, "Quantity"] = decQuantity;
                flexDetail[e.Row, "TotalCost"] = (Convert.ToDecimal(flexDetail[e.Row, "TotalCost2"]) / Convert.ToDecimal(flexDetail[e.Row, "Quantity2"])) * Convert.ToDecimal(flexDetail[e.Row, "Quantity"]);
                flexDetail[e.Row, "TotalFee"] = (Convert.ToDecimal(flexDetail[e.Row, "TotalFee2"]) / Convert.ToDecimal(flexDetail[e.Row, "Quantity2"])) * Convert.ToDecimal(flexDetail[e.Row, "Quantity"]);
            }
            else if (e.Col == flexDetail.Cols["ProductID_Out"].Index)
            {
                string strProductIDOldEdit = string.Empty;
                if (rOldEdit["ProductID_Out"] != DBNull.Value)
                    strProductIDOldEdit = rOldEdit["ProductID_Out"].ToString().Trim();
                if (Convert.ToString(flexDetail[e.Row, "ProductID_Out"]).Trim() == string.Empty)
                {
                    flexDetail[e.Row, "ProductName_Out"] = string.Empty;
                    flexDetail[e.Row, "IsRequestIMEI_Out"] = false;
                    flexDetail[e.Row, "IMEI_Out"] = string.Empty;
                    flexDetail[e.Row, "SalePrice_Out"] = 0;
                }
                else
                    flexDetail[e.Row, "ProductID_Out"] = strProductIDOldEdit;
            }
            CalTotalMoney();
        }

        private void lnkCustomer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ERP.MasterData.DUI.Search.frmCustomerSearch frmCustomerSearch1 = new MasterData.DUI.Search.frmCustomerSearch();
            frmCustomerSearch1.ShowDialog();
            if (frmCustomerSearch1.Customer != null && frmCustomerSearch1.Customer.CustomerID > 0)
            {
                txtCustomerName.Text = frmCustomerSearch1.Customer.CustomerName;
                txtCustomerAddress.Text = frmCustomerSearch1.Customer.CustomerAddress;
                txtTaxID.Text = frmCustomerSearch1.Customer.CustomerTaxID;
                txtCustomerPhone.Text = frmCustomerSearch1.Customer.CustomerPhone;
                txtCustomerIDCard.Text = frmCustomerSearch1.Customer.CustomerIDCard;
            }
            else
            {
                if (Convert.ToString(txtCustomerID.Text).Trim() != string.Empty)
                {
                    if (MessageBox.Show(this, "Bạn muốn bỏ chọn khách hàng hiện tại không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                }
                txtCustomerName.Text = string.Empty;
                txtCustomerAddress.Text = string.Empty;
                txtTaxID.Text = string.Empty;
                txtCustomerPhone.Text = string.Empty;
                txtCustomerIDCard.Text = string.Empty;
            }
        }

        #endregion
        private void flexWorkFlow_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < flexWorkFlow.Rows.Fixed)
                return;
            if (Convert.ToBoolean(flexWorkFlow[e.Row, "IsProcessed2"]))
                e.Cancel = true;
            if (intWorkFlowCurrent < 1 || Convert.ToInt32(flexWorkFlow[e.Row, "InputChangeOrderStepID"]) != intWorkFlowCurrent)
                e.Cancel = true;
        }

        private void flexWorkFlow_Click(object sender, EventArgs e)
        {
            if (flexWorkFlow.RowSel < flexWorkFlow.Rows.Fixed || Convert.ToBoolean(flexWorkFlow[flexWorkFlow.RowSel, "IsProcessed2"]))
                return;
            if (intWorkFlowCurrent > 0 && Convert.ToInt32(flexWorkFlow[flexWorkFlow.RowSel, "InputChangeOrderStepID"]) != intWorkFlowCurrent)
            {
                if (flexWorkFlow.ColSel == flexWorkFlow.Cols["IsProcessed"].Index && Convert.ToInt32(flexWorkFlow[flexWorkFlow.RowSel, "InputChangeOrderStepID"]) != intWorkFlowCurrent)
                {
                    if ((!chkIsInputchange.Checked) && (!bolIsDeleted))
                    {
                        MessageBox.Show(this, "Vui lòng xử lý qui trình theo thứ tự", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
        }

        private void mnuAttachment_Opening(object sender, CancelEventArgs e)
        {
            mnuDelAttachment.Enabled = false;
            if (chkIsInputchange.Checked || intReviewStatus == 2)
                mnuAddAttachment.Enabled = false;
            try
            {
                if (grvAttachment.FocusedRowHandle < 0) return;
                PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objAttachmentInfo = lstInputChangeOrder_Attachment[grvAttachment.FocusedRowHandle];
                if (objAttachmentInfo.AttachmentName != null && Convert.ToString(objAttachmentInfo.AttachmentName).Trim() != string.Empty)
                    mnuDelAttachment.Enabled = (btnAdd.Enabled || strInputChangeOrderID != string.Empty);
            }
            catch { }
        }

        private void mnuAddAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog objOpenFileDialog = new OpenFileDialog();
                objOpenFileDialog.Multiselect = true;
                objOpenFileDialog.Filter = "Office Files|*.doc;*.docx;*.xls;*.xlsx;*.pdf;*.ppt;*.zip;*.rar;*.jpg;*.jpe;*.jpeg;*.gif;*.png;*.bmp;*.tif;*.tiff;*.txt";
                if (objOpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String[] strLocalFilePaths = objOpenFileDialog.FileNames;
                    foreach (string strLocalFilePath in strLocalFilePaths)
                    {
                        FileInfo objFileInfo = new FileInfo(strLocalFilePath);
                        var ListCheck = from o in lstInputChangeOrder_Attachment
                                        where o.AttachmentName == objFileInfo.Name
                                        select o;
                        if (ListCheck.Count() > 0)
                            continue;
                        decimal FileSize = Math.Round(objFileInfo.Length / Convert.ToDecimal((1024 * 1024)), 2); //dung lượng tính theo MB
                        decimal FileSizeLimit = 2;//MB
                        if (FileSize > FileSizeLimit)
                        {
                            MessageBox.Show(this, "Vui lòng chọn tập tin đính kèm dung lượng không vượt quá 2 MB", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objInputChangeOrder_Attachment = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment();
                        objInputChangeOrder_Attachment.AttachmentName = objFileInfo.Name;
                        objInputChangeOrder_Attachment.AttachmentLocalPath = strLocalFilePath;
                        objInputChangeOrder_Attachment.AttachmentPath = DateTime.Now.ToString("yyyy_MM") + "/{0}/" + Guid.NewGuid() + "_" + objInputChangeOrder_Attachment.AttachmentName;
                        objInputChangeOrder_Attachment.CreatedDate = Globals.GetServerDateTime();
                        objInputChangeOrder_Attachment.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objInputChangeOrder_Attachment.CreatedFullName = SystemConfig.objSessionUser.UserName + " - " + SystemConfig.objSessionUser.FullName;
                        objInputChangeOrder_Attachment.IsAddNew = true;
                        objInputChangeOrder_Attachment.STT = lstInputChangeOrder_Attachment.Count + 1;
                        lstInputChangeOrder_Attachment.Add(objInputChangeOrder_Attachment);
                    }
                    grdAttachment.RefreshDataSource();
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi thêm tập tin", objExce.ToString(), this.Name + " -> mnuAddAttachment_Click", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi thêm tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuDelAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvAttachment.FocusedRowHandle < 0)
                    return;
                PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objInputChangeOrder_Attachment = (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
                if (objInputChangeOrder_Attachment != null && lstInputChangeOrder_Attachment != null)
                {
                    if (objInputChangeOrder_Attachment.AttachmentID != null && objInputChangeOrder_Attachment.AttachmentID.Trim() != string.Empty)
                    {
                        if (btnAdd.Enabled || strInputChangeOrderID != string.Empty)
                        {
                            if (!btnUpdate.Enabled)
                            {
                                MessageBox.Show(this, "Bạn không có quyền xóa file này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                    if (MessageBox.Show(this, "Bạn muốn xóa file đính kèm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        lstInputChangeOrder_Attachment.Remove(objInputChangeOrder_Attachment);
                        int intSTT = 0;
                        for (int i = 0; i < lstInputChangeOrder_Attachment.Count; i++)
                            lstInputChangeOrder_Attachment[i].STT = ++intSTT;
                        if (objInputChangeOrder_Attachment.AttachmentID != null && objInputChangeOrder_Attachment.AttachmentID.Trim() != string.Empty)
                        {
                            if (lstInputChangeOrder_Attachment_Del == null)
                                lstInputChangeOrder_Attachment_Del = new List<PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment>();
                            objInputChangeOrder_Attachment.IsAddNew = false;
                            objInputChangeOrder_Attachment.IsDeleted = true;
                            objInputChangeOrder_Attachment.DeletedUser = SystemConfig.objSessionUser.UserName;
                            lstInputChangeOrder_Attachment_Del.Add(objInputChangeOrder_Attachment);
                            if (stbAttachmentLog.Length > 0)
                                stbAttachmentLog.Append("\r\n");
                            stbAttachmentLog.Append("Xóa tập tin: " + objInputChangeOrder_Attachment.AttachmentName);
                        }
                        grdAttachment.RefreshDataSource();
                    }
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi xóa tập tin", objExce.ToString(), this.Name + " -> mnuDelAttachment_Click", DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xóa tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (strInputChangeOrderID == string.Empty)
                return;
            if (!btnDelete.Enabled || !btnDelete.Visible)
                return;
            if (!Globals.ValidatingSystemDate(this))
                return;
            bool bolEnableDelete = btnDelete.Enabled;
            btnDelete.Enabled = false;
            Library.AppControl.FormCommon.frmInputString frmInputString1 = new Library.AppControl.FormCommon.frmInputString();
            frmInputString1.MaxLengthString = 400;
            frmInputString1.Text = "Nhập ghi chú hủy yêu cầu " + strMessageForm + " " + strInputChangeOrderID;
            frmInputString1.ShowDialog(this);
            if (!frmInputString1.IsAccept)
            {
                btnDelete.Enabled = bolEnableDelete;
                return;
            }
            objPLCInputChangeOrder.Delete(strInputChangeOrderID, frmInputString1.ContentString);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                btnDelete.Enabled = bolEnableDelete;
            }
            else
            {
                MessageBox.Show("Hủy yêu cầu " + strMessageForm + " thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsHasAction = true;
                this.Close();
            }
        }

        private void mnuWorkFlow_Opening(object sender, CancelEventArgs e)
        {
            mnuWorkFlow_IsProcess.Enabled = false;
            mnuWorkFlow_IsProcess.Tag = 0;
            if (!flexWorkFlow.Cols["IsProcessed"].AllowEditing)
                return;
            if (flexWorkFlow.RowSel < flexWorkFlow.Rows.Fixed)
                return;
            if (intWorkFlowCurrent > 0 && Convert.ToInt32(flexWorkFlow[flexWorkFlow.RowSel, "InputChangeOrderStepID"]) == intWorkFlowCurrent)
            {
                if (SystemConfig.objSessionUser.IsPermission(flexWorkFlow[flexWorkFlow.RowSel, "ProcessFunctionID"].ToString()) &&
                    Convert.ToBoolean(flexWorkFlow[flexWorkFlow.RowSel, "IsProcessed"]))
                {
                    if (intReviewStatus != 2)
                    {
                        mnuWorkFlow_IsProcess.Enabled = true;
                        mnuWorkFlow_IsProcess.Tag = intWorkFlowCurrent;
                    }
                }
            }
        }

        private void mnuWorkFlow_IsProcess_Click(object sender, EventArgs e)
        {
            if (strInputChangeOrderID == string.Empty)
                return;
            if (!Convert.ToBoolean(flexWorkFlow[flexWorkFlow.RowSel, "IsProcessed"]))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng tích chọn Đã xử lý");
                return;
            }
            PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_WorkFlow objWorkFlow = new PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_WorkFlow();
            objWorkFlow.InputChangeOrderID = strInputChangeOrderID;
            objWorkFlow.InputChangeOrderStepID = Convert.ToInt32(mnuWorkFlow_IsProcess.Tag);
            objWorkFlow.InputChangeOrderDate = objInputChangeOrder.InputChangeOrderDate;
            objWorkFlow.ProcessedUser = SystemConfig.objSessionUser.UserName;
            objWorkFlow.ProcessedTime = Globals.GetServerDateTime();
            objWorkFlow.IsProcessed = true;
            objWorkFlow.Note = Convert.ToString(flexWorkFlow[flexWorkFlow.RowSel, "Note"]).Trim();

            objPLCInputChangeOrder.UpdateProcess(objWorkFlow);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
            }
            else
            {
                MessageBox.Show("Cập nhật xử lý bước xử lý yêu cầu " + strMessageForm + " thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void flexWorkFlow_MouseEnterCell(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < flexWorkFlow.Rows.Fixed)
                return;
            toolTip1.SetToolTip(flexWorkFlow, string.Empty);
            if (e.Col == flexWorkFlow.Cols["Note"].Index)
            {
                string strNote = Convert.ToString(flexWorkFlow[e.Row, "Note"]).Trim();
                toolTip1.SetToolTip(flexWorkFlow, strNote);
            }
        }

        private void flexWorkFlow_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexWorkFlow.DataSource != null)
                if (e.Col == flexWorkFlow.Cols["Note"].Index)
                {
                    TextBox textBox = (TextBox)this.flexWorkFlow.Editor;
                    textBox.MaxLength = 600;
                }
        }

        private void tmrCloseForm_Tick(object sender, EventArgs e)
        {
            if (intClosedForm == 0)
            {
                intClosedForm = 1;
                if (strMessageCloseForm != string.Empty)
                    MessageBoxObject.ShowWarningMessage(this, strMessageCloseForm);
                System.Threading.Thread.Sleep(500);
                this.Close();
            }
        }

        private void grdAttachment_EditorKeyPress(object sender, KeyPressEventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            if (grvAttachment.FocusedColumn.FieldName.ToUpper().Equals("DESCRIPTION"))
            {
                PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment objAttachment = (PLC.InputChangeOrder.WSInputChangeOrder.InputChangeOrder_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
                if (!objAttachment.IsAddNew)
                {
                    e.Handled = true;
                }
            }
            else if (grvAttachment.FocusedColumn.FieldName.ToUpper().Equals("COLDOWNLOAD"))
                e.Handled = true;
        }

        private void lnkSaleOrderID_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (txtSaleOrderID.Text.Trim() != string.Empty)
                SalesAndServices.SaleOrders.DUI.DUISaleOrders_Common.ViewSaleOrder(this, objInputChangeOrder.SaleOrderID);
        }

        private void lblInVoiceID_Click(object sender, EventArgs e)
        {

        }

        private void chkCreateSaleOrder_CheckedChanged(object sender, EventArgs e)
        {
            if (!objInputChangeOrderType.IsInputReturn)
            {
                if (chkCreateSaleOrder.Checked)
                    FormatFlexReturn();
                else
                    FormatFlex();
            }
        }

        private void btnSupportCare_Click(object sender, EventArgs e)
        {
            if (strInputChangeOrderID == string.Empty)
                return;
            if (!btnSupportCare.Visible)
                return;
            btnUpdate.Enabled = false;
            DataRow[] drNotIMEI = ((DataTable)flexDetail.DataSource).Select("IMEI IS NULL OR TRIM(IMEI)=''");
            if (drNotIMEI.Length > 0)
            {
                if (objPLCInputChangeOrder.UpdateCare(drNotIMEI[0]["OldOutputVoucherDetailID"].ToString()) > 0)
                    MessageBox.Show(this, "Hỗ trợ trả Care thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show(this, "Không tìm thấy Gói bảo hành hỗ trợ trả Care", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            btnUpdate.Enabled = true;
        }

    }
}
