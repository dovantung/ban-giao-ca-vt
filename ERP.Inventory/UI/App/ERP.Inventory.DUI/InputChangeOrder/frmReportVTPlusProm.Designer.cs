﻿namespace ERP.Report.DUI.Reports.Promotion
{
    partial class frmReportVTPlusProm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINVOICESYMBOL_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINVOICENO_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOTALMONEY_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSALEORDERID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMERID_VTPLUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMERNAME_VTPLUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMERPHONE_VTPLUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDENOMINATOR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINVOICESYMBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINVOICENO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.showInvoiceVAT = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colINVOICEDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMERID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMERNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMERPHONE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMERTAXID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSUBGROUPNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRODUCTID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRODUCTNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOTALQUANTITY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOTALAMOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOTALMONEY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOTALMONEY_SUB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT_SUB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRATIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOINT_ADD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATGROUP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATINVOICENAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOUTPUTDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOUTPUTVOUCHERID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMERNAME_OUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBRANCH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMISAPRODUCTID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repCheckBoxPrinted = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.cboVAT = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.chkIsExport = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panelTop = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxTypeReport = new System.Windows.Forms.ComboBox();
            this.cboTransferInvoice = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnTransferInvoice = new System.Windows.Forms.Button();
            this.cboDataReport = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label9 = new System.Windows.Forms.Label();
            this.cboOutputType = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cboInvoiceTransTypeID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label8 = new System.Windows.Forms.Label();
            this.cboInvoiceKind = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label2 = new System.Windows.Forms.Label();
            this.chkInvoiceDT = new System.Windows.Forms.CheckBox();
            this.chkGroupVAT = new System.Windows.Forms.CheckBox();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.cboBranch = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.cboSearchBy = new System.Windows.Forms.ComboBox();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboMainGroup = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.label7 = new System.Windows.Forms.Label();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panelBottom = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showInvoiceVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBoxPrinted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsExport)).BeginInit();
            this.panelTop.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(5);
            this.grdData.Location = new System.Drawing.Point(0, 119);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Margin = new System.Windows.Forms.Padding(5);
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repCheckBoxPrinted,
            this.repositoryItemCheckEdit1,
            this.cboVAT,
            this.chkIsExport,
            this.repositoryItemTextEdit1,
            this.showInvoiceVAT});
            this.grdData.Size = new System.Drawing.Size(1260, 377);
            this.grdData.TabIndex = 1;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewData.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.grdViewData.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.GroupFooter.Options.UseFont = true;
            this.grdViewData.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.grdViewData.Appearance.GroupRow.Options.UseFont = true;
            this.grdViewData.Appearance.GroupRow.Options.UseForeColor = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.ColumnPanelRowHeight = 50;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSTT,
            this.colINVOICESYMBOL_2,
            this.colINVOICENO_2,
            this.colTOTALMONEY_2,
            this.colVAT_2,
            this.colSALEORDERID,
            this.colCUSTOMERID_VTPLUS,
            this.colCUSTOMERNAME_VTPLUS,
            this.colCUSTOMERPHONE_VTPLUS,
            this.colDENOMINATOR,
            this.colINVOICESYMBOL,
            this.colINVOICENO,
            this.colINVOICEDATE,
            this.colCUSTOMERID,
            this.colCUSTOMERNAME,
            this.colCUSTOMERPHONE,
            this.gridColumn1,
            this.colCUSTOMERTAXID,
            this.colSUBGROUPNAME,
            this.colPRODUCTID,
            this.colPRODUCTNAME,
            this.colTOTALQUANTITY,
            this.colTOTALAMOUNT,
            this.colTOTALMONEY,
            this.colVAT,
            this.colTOTALMONEY_SUB,
            this.colVAT_SUB,
            this.colRATIO,
            this.colPOINT_ADD,
            this.colVATGROUP,
            this.colVATINVOICENAME,
            this.colOUTPUTDATE,
            this.colOUTPUTVOUCHERID,
            this.colCUSTOMERNAME_OUT,
            this.colBRANCH,
            this.colMISAPRODUCTID});
            this.grdViewData.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.GroupFormat = " [#image]{1} {2}";
            this.grdViewData.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "DENOMINATOR", this.colDENOMINATOR, "Tổng"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VAT", this.colVAT, "{0:#,##0;(#,##0)}", new decimal(new int[] {
                            0,
                            0,
                            0,
                            0})),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALMONEY", this.colTOTALMONEY, "{0:#,##0;(#,##0)}", new decimal(new int[] {
                            0,
                            0,
                            0,
                            0}))});
            this.grdViewData.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsBehavior.AutoExpandAllGroups = true;
            this.grdViewData.OptionsBehavior.SummariesIgnoreNullValues = true;
            this.grdViewData.OptionsMenu.EnableColumnMenu = false;
            this.grdViewData.OptionsMenu.EnableFooterMenu = false;
            this.grdViewData.OptionsMenu.EnableGroupPanelMenu = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.CustomDrawRowFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.grdViewData_CustomDrawFooterCell);
            this.grdViewData.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.grdViewData_CustomDrawFooterCell);
            // 
            // colSTT
            // 
            this.colSTT.AppearanceCell.Options.UseTextOptions = true;
            this.colSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colSTT.AppearanceHeader.Options.UseTextOptions = true;
            this.colSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSTT.Caption = "STT";
            this.colSTT.FieldName = "STT";
            this.colSTT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colSTT.Name = "colSTT";
            this.colSTT.OptionsColumn.AllowEdit = false;
            this.colSTT.OptionsColumn.ReadOnly = true;
            this.colSTT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colSTT.Visible = true;
            this.colSTT.VisibleIndex = 3;
            this.colSTT.Width = 50;
            // 
            // colINVOICESYMBOL_2
            // 
            this.colINVOICESYMBOL_2.Caption = "Ký hiệu hóa đơn";
            this.colINVOICESYMBOL_2.FieldName = "INVOICESYMBOL_2";
            this.colINVOICESYMBOL_2.Name = "colINVOICESYMBOL_2";
            this.colINVOICESYMBOL_2.Visible = true;
            this.colINVOICESYMBOL_2.VisibleIndex = 1;
            this.colINVOICESYMBOL_2.Width = 148;
            // 
            // colINVOICENO_2
            // 
            this.colINVOICENO_2.Caption = "Số hóa đơn";
            this.colINVOICENO_2.FieldName = "INVOICENO_2";
            this.colINVOICENO_2.Name = "colINVOICENO_2";
            this.colINVOICENO_2.Visible = true;
            this.colINVOICENO_2.VisibleIndex = 0;
            this.colINVOICENO_2.Width = 105;
            // 
            // colTOTALMONEY_2
            // 
            this.colTOTALMONEY_2.Caption = "Doanh thu trước thuế";
            this.colTOTALMONEY_2.FieldName = "TOTALMONEY_2";
            this.colTOTALMONEY_2.Name = "colTOTALMONEY_2";
            this.colTOTALMONEY_2.Visible = true;
            this.colTOTALMONEY_2.VisibleIndex = 2;
            this.colTOTALMONEY_2.Width = 202;
            // 
            // colVAT_2
            // 
            this.colVAT_2.Caption = "Tiền thuế";
            this.colVAT_2.FieldName = "VAT_2";
            this.colVAT_2.Name = "colVAT_2";
            this.colVAT_2.Visible = true;
            this.colVAT_2.VisibleIndex = 4;
            this.colVAT_2.Width = 89;
            // 
            // colSALEORDERID
            // 
            this.colSALEORDERID.Caption = "Mã giao dịch Viettel++";
            this.colSALEORDERID.FieldName = "SALEORDERID";
            this.colSALEORDERID.Name = "colSALEORDERID";
            this.colSALEORDERID.OptionsColumn.AllowEdit = false;
            this.colSALEORDERID.Visible = true;
            this.colSALEORDERID.VisibleIndex = 5;
            this.colSALEORDERID.Width = 204;
            // 
            // colCUSTOMERID_VTPLUS
            // 
            this.colCUSTOMERID_VTPLUS.Caption = "Mã KH Viettel++";
            this.colCUSTOMERID_VTPLUS.FieldName = "CUSTOMERID_VTPLUS";
            this.colCUSTOMERID_VTPLUS.Name = "colCUSTOMERID_VTPLUS";
            this.colCUSTOMERID_VTPLUS.OptionsColumn.AllowEdit = false;
            this.colCUSTOMERID_VTPLUS.Visible = true;
            this.colCUSTOMERID_VTPLUS.VisibleIndex = 6;
            this.colCUSTOMERID_VTPLUS.Width = 153;
            // 
            // colCUSTOMERNAME_VTPLUS
            // 
            this.colCUSTOMERNAME_VTPLUS.Caption = "Tên KH Viettel++";
            this.colCUSTOMERNAME_VTPLUS.FieldName = "CUSTOMERNAME_VTPLUS";
            this.colCUSTOMERNAME_VTPLUS.Name = "colCUSTOMERNAME_VTPLUS";
            this.colCUSTOMERNAME_VTPLUS.OptionsColumn.AllowEdit = false;
            this.colCUSTOMERNAME_VTPLUS.Visible = true;
            this.colCUSTOMERNAME_VTPLUS.VisibleIndex = 7;
            this.colCUSTOMERNAME_VTPLUS.Width = 223;
            // 
            // colCUSTOMERPHONE_VTPLUS
            // 
            this.colCUSTOMERPHONE_VTPLUS.AppearanceCell.Options.UseTextOptions = true;
            this.colCUSTOMERPHONE_VTPLUS.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCUSTOMERPHONE_VTPLUS.Caption = "SĐT KH Viettel++";
            this.colCUSTOMERPHONE_VTPLUS.FieldName = "CUSTOMERPHONE_VTPLUS";
            this.colCUSTOMERPHONE_VTPLUS.Name = "colCUSTOMERPHONE_VTPLUS";
            this.colCUSTOMERPHONE_VTPLUS.OptionsColumn.AllowEdit = false;
            this.colCUSTOMERPHONE_VTPLUS.OptionsColumn.ReadOnly = true;
            this.colCUSTOMERPHONE_VTPLUS.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCUSTOMERPHONE_VTPLUS.Visible = true;
            this.colCUSTOMERPHONE_VTPLUS.VisibleIndex = 8;
            this.colCUSTOMERPHONE_VTPLUS.Width = 180;
            // 
            // colDENOMINATOR
            // 
            this.colDENOMINATOR.AppearanceHeader.Options.UseTextOptions = true;
            this.colDENOMINATOR.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDENOMINATOR.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDENOMINATOR.Caption = "Ký hiệu mẫu HĐ";
            this.colDENOMINATOR.FieldName = "DENOMINATOR";
            this.colDENOMINATOR.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colDENOMINATOR.Name = "colDENOMINATOR";
            this.colDENOMINATOR.OptionsColumn.AllowEdit = false;
            this.colDENOMINATOR.OptionsColumn.ReadOnly = true;
            this.colDENOMINATOR.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDENOMINATOR.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "DENOMINATOR", "Tổng cộng")});
            this.colDENOMINATOR.Visible = true;
            this.colDENOMINATOR.VisibleIndex = 9;
            this.colDENOMINATOR.Width = 98;
            // 
            // colINVOICESYMBOL
            // 
            this.colINVOICESYMBOL.AppearanceHeader.Options.UseTextOptions = true;
            this.colINVOICESYMBOL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colINVOICESYMBOL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colINVOICESYMBOL.Caption = "Ký hiệu HĐ";
            this.colINVOICESYMBOL.FieldName = "INVOICESYMBOL";
            this.colINVOICESYMBOL.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colINVOICESYMBOL.Name = "colINVOICESYMBOL";
            this.colINVOICESYMBOL.OptionsColumn.AllowEdit = false;
            this.colINVOICESYMBOL.OptionsColumn.ReadOnly = true;
            this.colINVOICESYMBOL.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINVOICESYMBOL.Visible = true;
            this.colINVOICESYMBOL.VisibleIndex = 10;
            this.colINVOICESYMBOL.Width = 90;
            // 
            // colINVOICENO
            // 
            this.colINVOICENO.AppearanceCell.Options.UseTextOptions = true;
            this.colINVOICENO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colINVOICENO.AppearanceHeader.Options.UseTextOptions = true;
            this.colINVOICENO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colINVOICENO.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colINVOICENO.Caption = "Số hóa đơn";
            this.colINVOICENO.ColumnEdit = this.showInvoiceVAT;
            this.colINVOICENO.FieldName = "INVOICENO";
            this.colINVOICENO.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colINVOICENO.Name = "colINVOICENO";
            this.colINVOICENO.OptionsColumn.AllowEdit = false;
            this.colINVOICENO.OptionsColumn.ReadOnly = true;
            this.colINVOICENO.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINVOICENO.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom)});
            this.colINVOICENO.Visible = true;
            this.colINVOICENO.VisibleIndex = 11;
            this.colINVOICENO.Width = 100;
            // 
            // showInvoiceVAT
            // 
            this.showInvoiceVAT.AutoHeight = false;
            this.showInvoiceVAT.Name = "showInvoiceVAT";
            // 
            // colINVOICEDATE
            // 
            this.colINVOICEDATE.AppearanceHeader.Options.UseTextOptions = true;
            this.colINVOICEDATE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colINVOICEDATE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colINVOICEDATE.Caption = "Ngày hóa đơn";
            this.colINVOICEDATE.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colINVOICEDATE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colINVOICEDATE.FieldName = "INVOICEDATE";
            this.colINVOICEDATE.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colINVOICEDATE.Name = "colINVOICEDATE";
            this.colINVOICEDATE.OptionsColumn.AllowEdit = false;
            this.colINVOICEDATE.OptionsColumn.ReadOnly = true;
            this.colINVOICEDATE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINVOICEDATE.Visible = true;
            this.colINVOICEDATE.VisibleIndex = 12;
            this.colINVOICEDATE.Width = 120;
            // 
            // colCUSTOMERID
            // 
            this.colCUSTOMERID.Caption = "Mã khách hàng";
            this.colCUSTOMERID.FieldName = "CUSTOMERID";
            this.colCUSTOMERID.Name = "colCUSTOMERID";
            this.colCUSTOMERID.OptionsColumn.AllowEdit = false;
            this.colCUSTOMERID.Visible = true;
            this.colCUSTOMERID.VisibleIndex = 13;
            this.colCUSTOMERID.Width = 135;
            // 
            // colCUSTOMERNAME
            // 
            this.colCUSTOMERNAME.Caption = "Tên khách hàng";
            this.colCUSTOMERNAME.FieldName = "CUSTOMERNAME";
            this.colCUSTOMERNAME.Name = "colCUSTOMERNAME";
            this.colCUSTOMERNAME.OptionsColumn.AllowEdit = false;
            this.colCUSTOMERNAME.Visible = true;
            this.colCUSTOMERNAME.VisibleIndex = 14;
            this.colCUSTOMERNAME.Width = 142;
            // 
            // colCUSTOMERPHONE
            // 
            this.colCUSTOMERPHONE.Caption = "Số điện thoại";
            this.colCUSTOMERPHONE.FieldName = "CUSTOMERPHONE";
            this.colCUSTOMERPHONE.Name = "colCUSTOMERPHONE";
            this.colCUSTOMERPHONE.OptionsColumn.AllowEdit = false;
            this.colCUSTOMERPHONE.Visible = true;
            this.colCUSTOMERPHONE.VisibleIndex = 15;
            this.colCUSTOMERPHONE.Width = 121;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Tên đơn vị";
            this.gridColumn1.FieldName = "VATCUSTOMERNAME";
            this.gridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 16;
            this.gridColumn1.Width = 200;
            // 
            // colCUSTOMERTAXID
            // 
            this.colCUSTOMERTAXID.AppearanceHeader.Options.UseTextOptions = true;
            this.colCUSTOMERTAXID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCUSTOMERTAXID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCUSTOMERTAXID.Caption = "Mã số thuế người mua";
            this.colCUSTOMERTAXID.FieldName = "CUSTOMERTAXID";
            this.colCUSTOMERTAXID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCUSTOMERTAXID.Name = "colCUSTOMERTAXID";
            this.colCUSTOMERTAXID.OptionsColumn.AllowEdit = false;
            this.colCUSTOMERTAXID.OptionsColumn.ReadOnly = true;
            this.colCUSTOMERTAXID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCUSTOMERTAXID.Visible = true;
            this.colCUSTOMERTAXID.VisibleIndex = 17;
            this.colCUSTOMERTAXID.Width = 115;
            // 
            // colSUBGROUPNAME
            // 
            this.colSUBGROUPNAME.Caption = "Nhóm hàng được tích điểm";
            this.colSUBGROUPNAME.FieldName = "SUBGROUPNAME";
            this.colSUBGROUPNAME.Name = "colSUBGROUPNAME";
            this.colSUBGROUPNAME.OptionsColumn.AllowEdit = false;
            this.colSUBGROUPNAME.Visible = true;
            this.colSUBGROUPNAME.VisibleIndex = 18;
            this.colSUBGROUPNAME.Width = 240;
            // 
            // colPRODUCTID
            // 
            this.colPRODUCTID.Caption = "Mã sản phẩm";
            this.colPRODUCTID.FieldName = "PRODUCTID";
            this.colPRODUCTID.Name = "colPRODUCTID";
            this.colPRODUCTID.OptionsColumn.AllowEdit = false;
            this.colPRODUCTID.Visible = true;
            this.colPRODUCTID.VisibleIndex = 19;
            this.colPRODUCTID.Width = 124;
            // 
            // colPRODUCTNAME
            // 
            this.colPRODUCTNAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colPRODUCTNAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPRODUCTNAME.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPRODUCTNAME.Caption = "Mặt hàng";
            this.colPRODUCTNAME.FieldName = "PRODUCTNAME";
            this.colPRODUCTNAME.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPRODUCTNAME.Name = "colPRODUCTNAME";
            this.colPRODUCTNAME.OptionsColumn.AllowEdit = false;
            this.colPRODUCTNAME.OptionsColumn.ReadOnly = true;
            this.colPRODUCTNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTNAME.Visible = true;
            this.colPRODUCTNAME.VisibleIndex = 20;
            this.colPRODUCTNAME.Width = 450;
            // 
            // colTOTALQUANTITY
            // 
            this.colTOTALQUANTITY.Caption = "Số lượng";
            this.colTOTALQUANTITY.FieldName = "TOTALQUANTITY";
            this.colTOTALQUANTITY.Name = "colTOTALQUANTITY";
            this.colTOTALQUANTITY.OptionsColumn.AllowEdit = false;
            this.colTOTALQUANTITY.Visible = true;
            this.colTOTALQUANTITY.VisibleIndex = 21;
            this.colTOTALQUANTITY.Width = 93;
            // 
            // colTOTALAMOUNT
            // 
            this.colTOTALAMOUNT.Caption = "Đơn giá";
            this.colTOTALAMOUNT.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.colTOTALAMOUNT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTOTALAMOUNT.FieldName = "TOTALAMOUNT";
            this.colTOTALAMOUNT.Name = "colTOTALAMOUNT";
            this.colTOTALAMOUNT.OptionsColumn.AllowEdit = false;
            this.colTOTALAMOUNT.Visible = true;
            this.colTOTALAMOUNT.VisibleIndex = 22;
            // 
            // colTOTALMONEY
            // 
            this.colTOTALMONEY.AppearanceCell.Options.UseTextOptions = true;
            this.colTOTALMONEY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTOTALMONEY.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOTALMONEY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOTALMONEY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOTALMONEY.Caption = "Doanh thu chưa có thuế GTGT";
            this.colTOTALMONEY.ColumnEdit = this.repositoryItemTextEdit1;
            this.colTOTALMONEY.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.colTOTALMONEY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTOTALMONEY.FieldName = "TOTALMONEY";
            this.colTOTALMONEY.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTOTALMONEY.Name = "colTOTALMONEY";
            this.colTOTALMONEY.OptionsColumn.AllowEdit = false;
            this.colTOTALMONEY.OptionsColumn.ReadOnly = true;
            this.colTOTALMONEY.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTOTALMONEY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALMONEY", "{0:#,##0;(#,##0)}")});
            this.colTOTALMONEY.Visible = true;
            this.colTOTALMONEY.VisibleIndex = 23;
            this.colTOTALMONEY.Width = 152;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTextEdit1.EditFormat.FormatString = "#,##0.####;(#,##0.####)";
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTextEdit1.Mask.EditMask = "#,###,###,###,###,##0.####;";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colVAT
            // 
            this.colVAT.AppearanceCell.Options.UseTextOptions = true;
            this.colVAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colVAT.AppearanceHeader.Options.UseTextOptions = true;
            this.colVAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVAT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVAT.Caption = "Thuế GTGT";
            this.colVAT.ColumnEdit = this.repositoryItemTextEdit1;
            this.colVAT.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.colVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colVAT.FieldName = "VAT";
            this.colVAT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colVAT.Name = "colVAT";
            this.colVAT.OptionsColumn.AllowEdit = false;
            this.colVAT.OptionsColumn.ReadOnly = true;
            this.colVAT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VAT", "{0:#,##0;(#,##0)}")});
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 24;
            this.colVAT.Width = 101;
            // 
            // colTOTALMONEY_SUB
            // 
            this.colTOTALMONEY_SUB.Caption = "Chênh Doanh thu trước thuế";
            this.colTOTALMONEY_SUB.FieldName = "TOTALMONEY_SUB";
            this.colTOTALMONEY_SUB.Name = "colTOTALMONEY_SUB";
            this.colTOTALMONEY_SUB.Visible = true;
            this.colTOTALMONEY_SUB.VisibleIndex = 25;
            this.colTOTALMONEY_SUB.Width = 266;
            // 
            // colVAT_SUB
            // 
            this.colVAT_SUB.Caption = "Chênh tiền thuế";
            this.colVAT_SUB.FieldName = "VAT_SUB";
            this.colVAT_SUB.Name = "colVAT_SUB";
            this.colVAT_SUB.Visible = true;
            this.colVAT_SUB.VisibleIndex = 26;
            this.colVAT_SUB.Width = 159;
            // 
            // colRATIO
            // 
            this.colRATIO.Caption = "Tỉ lệ quy đổi tích điểm theo nhóm hàng";
            this.colRATIO.FieldName = "RATIO";
            this.colRATIO.Name = "colRATIO";
            this.colRATIO.OptionsColumn.AllowEdit = false;
            this.colRATIO.Visible = true;
            this.colRATIO.VisibleIndex = 27;
            this.colRATIO.Width = 351;
            // 
            // colPOINT_ADD
            // 
            this.colPOINT_ADD.Caption = "Tích điểm Viettel++";
            this.colPOINT_ADD.FieldName = "POINT_ADD";
            this.colPOINT_ADD.Name = "colPOINT_ADD";
            this.colPOINT_ADD.OptionsColumn.AllowEdit = false;
            this.colPOINT_ADD.Visible = true;
            this.colPOINT_ADD.VisibleIndex = 28;
            this.colPOINT_ADD.Width = 183;
            // 
            // colVATGROUP
            // 
            this.colVATGROUP.Caption = "VATGROUP";
            this.colVATGROUP.FieldName = "VATINVOICENAME_GROUP";
            this.colVATGROUP.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colVATGROUP.Name = "colVATGROUP";
            this.colVATGROUP.OptionsColumn.ReadOnly = true;
            this.colVATGROUP.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // colVATINVOICENAME
            // 
            this.colVATINVOICENAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colVATINVOICENAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVATINVOICENAME.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVATINVOICENAME.Caption = "Phân loại VAT";
            this.colVATINVOICENAME.FieldName = "VATINVOICENAME";
            this.colVATINVOICENAME.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colVATINVOICENAME.Name = "colVATINVOICENAME";
            this.colVATINVOICENAME.OptionsColumn.AllowEdit = false;
            this.colVATINVOICENAME.OptionsColumn.ReadOnly = true;
            this.colVATINVOICENAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colVATINVOICENAME.Width = 100;
            // 
            // colOUTPUTDATE
            // 
            this.colOUTPUTDATE.AppearanceHeader.Options.UseTextOptions = true;
            this.colOUTPUTDATE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOUTPUTDATE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOUTPUTDATE.Caption = "Ngày chứng từ";
            this.colOUTPUTDATE.FieldName = "OUTPUTDATE";
            this.colOUTPUTDATE.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colOUTPUTDATE.Name = "colOUTPUTDATE";
            this.colOUTPUTDATE.OptionsColumn.AllowEdit = false;
            this.colOUTPUTDATE.OptionsColumn.ReadOnly = true;
            this.colOUTPUTDATE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOUTPUTDATE.Width = 200;
            // 
            // colOUTPUTVOUCHERID
            // 
            this.colOUTPUTVOUCHERID.AppearanceHeader.Options.UseTextOptions = true;
            this.colOUTPUTVOUCHERID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOUTPUTVOUCHERID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOUTPUTVOUCHERID.Caption = "Số chứng từ";
            this.colOUTPUTVOUCHERID.FieldName = "OUTPUTVOUCHERID";
            this.colOUTPUTVOUCHERID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colOUTPUTVOUCHERID.Name = "colOUTPUTVOUCHERID";
            this.colOUTPUTVOUCHERID.OptionsColumn.AllowEdit = false;
            this.colOUTPUTVOUCHERID.OptionsColumn.ReadOnly = true;
            this.colOUTPUTVOUCHERID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOUTPUTVOUCHERID.Width = 250;
            // 
            // colCUSTOMERNAME_OUT
            // 
            this.colCUSTOMERNAME_OUT.AppearanceHeader.Options.UseTextOptions = true;
            this.colCUSTOMERNAME_OUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCUSTOMERNAME_OUT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCUSTOMERNAME_OUT.Caption = "Tên người mua ERP";
            this.colCUSTOMERNAME_OUT.FieldName = "CUSTOMERNAME_OUT";
            this.colCUSTOMERNAME_OUT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCUSTOMERNAME_OUT.Name = "colCUSTOMERNAME_OUT";
            this.colCUSTOMERNAME_OUT.OptionsColumn.AllowEdit = false;
            this.colCUSTOMERNAME_OUT.OptionsColumn.ReadOnly = true;
            this.colCUSTOMERNAME_OUT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCUSTOMERNAME_OUT.Width = 200;
            // 
            // colBRANCH
            // 
            this.colBRANCH.AppearanceHeader.Options.UseTextOptions = true;
            this.colBRANCH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBRANCH.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBRANCH.Caption = "Chi nhánh";
            this.colBRANCH.FieldName = "BRANCH";
            this.colBRANCH.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colBRANCH.Name = "colBRANCH";
            this.colBRANCH.OptionsColumn.AllowEdit = false;
            this.colBRANCH.OptionsColumn.ReadOnly = true;
            this.colBRANCH.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colBRANCH.Visible = true;
            this.colBRANCH.VisibleIndex = 29;
            this.colBRANCH.Width = 150;
            // 
            // colMISAPRODUCTID
            // 
            this.colMISAPRODUCTID.AppearanceHeader.Options.UseTextOptions = true;
            this.colMISAPRODUCTID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMISAPRODUCTID.Caption = "Mã sản phẩm";
            this.colMISAPRODUCTID.FieldName = "MISAPRODUCTID";
            this.colMISAPRODUCTID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colMISAPRODUCTID.Name = "colMISAPRODUCTID";
            this.colMISAPRODUCTID.OptionsColumn.AllowEdit = false;
            this.colMISAPRODUCTID.OptionsColumn.ReadOnly = true;
            this.colMISAPRODUCTID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colMISAPRODUCTID.Width = 120;
            // 
            // repCheckBoxPrinted
            // 
            this.repCheckBoxPrinted.AutoHeight = false;
            this.repCheckBoxPrinted.Name = "repCheckBoxPrinted";
            this.repCheckBoxPrinted.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repCheckBoxPrinted.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repCheckBoxPrinted.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // cboVAT
            // 
            this.cboVAT.AutoHeight = false;
            this.cboVAT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboVAT.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPEID", "Loại thuế"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPENAME", 60, "Mức thuế")});
            this.cboVAT.DisplayMember = "VATTYPENAME";
            this.cboVAT.Name = "cboVAT";
            this.cboVAT.NullText = "--Chọn thuế suất HĐ--";
            this.cboVAT.ReadOnly = true;
            this.cboVAT.ValueMember = "VATTYPEID";
            // 
            // chkIsExport
            // 
            this.chkIsExport.AutoHeight = false;
            this.chkIsExport.Name = "chkIsExport";
            this.chkIsExport.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.chkIsExport.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chkIsExport.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.label13);
            this.panelTop.Controls.Add(this.comboBoxTypeReport);
            this.panelTop.Controls.Add(this.cboTransferInvoice);
            this.panelTop.Controls.Add(this.label11);
            this.panelTop.Controls.Add(this.btnTransferInvoice);
            this.panelTop.Controls.Add(this.cboDataReport);
            this.panelTop.Controls.Add(this.label9);
            this.panelTop.Controls.Add(this.cboOutputType);
            this.panelTop.Controls.Add(this.label6);
            this.panelTop.Controls.Add(this.label12);
            this.panelTop.Controls.Add(this.label10);
            this.panelTop.Controls.Add(this.cboInvoiceTransTypeID);
            this.panelTop.Controls.Add(this.label8);
            this.panelTop.Controls.Add(this.cboInvoiceKind);
            this.panelTop.Controls.Add(this.label2);
            this.panelTop.Controls.Add(this.chkInvoiceDT);
            this.panelTop.Controls.Add(this.chkGroupVAT);
            this.panelTop.Controls.Add(this.cboStore);
            this.panelTop.Controls.Add(this.cboBranch);
            this.panelTop.Controls.Add(this.cboSearchBy);
            this.panelTop.Controls.Add(this.dtpToDate);
            this.panelTop.Controls.Add(this.dtpFromDate);
            this.panelTop.Controls.Add(this.txtKeywords);
            this.panelTop.Controls.Add(this.btnExportExcel);
            this.panelTop.Controls.Add(this.btnSearch);
            this.panelTop.Controls.Add(this.label4);
            this.panelTop.Controls.Add(this.label5);
            this.panelTop.Controls.Add(this.label3);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Margin = new System.Windows.Forms.Padding(5);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1260, 119);
            this.panelTop.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(978, 10);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 20);
            this.label13.TabIndex = 29;
            this.label13.Text = "Loại báo cáo:";
            // 
            // comboBoxTypeReport
            // 
            this.comboBoxTypeReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTypeReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTypeReport.FormattingEnabled = true;
            this.comboBoxTypeReport.Items.AddRange(new object[] {
            "Chi tiết tích điểm Viettel++",
            "Tổng hợp tích điểm Viettel++",
            "Đối soát tích điểm Viettel++"});
            this.comboBoxTypeReport.Location = new System.Drawing.Point(1086, 7);
            this.comboBoxTypeReport.Margin = new System.Windows.Forms.Padding(5);
            this.comboBoxTypeReport.Name = "comboBoxTypeReport";
            this.comboBoxTypeReport.Size = new System.Drawing.Size(160, 28);
            this.comboBoxTypeReport.TabIndex = 28;
            // 
            // cboTransferInvoice
            // 
            this.cboTransferInvoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransferInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransferInvoice.FormattingEnabled = true;
            this.cboTransferInvoice.Items.AddRange(new object[] {
            "Số hóa đơn",
            "Ký hiệu mẫu HĐ",
            "Ký hiệu",
            "Số chứng từ"});
            this.cboTransferInvoice.Location = new System.Drawing.Point(757, 61);
            this.cboTransferInvoice.Margin = new System.Windows.Forms.Padding(5);
            this.cboTransferInvoice.Name = "cboTransferInvoice";
            this.cboTransferInvoice.Size = new System.Drawing.Size(206, 28);
            this.cboTransferInvoice.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(656, 66);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 20);
            this.label11.TabIndex = 17;
            this.label11.Text = "Kết chuyển DL:";
            // 
            // btnTransferInvoice
            // 
            this.btnTransferInvoice.Enabled = false;
            this.btnTransferInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransferInvoice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTransferInvoice.Location = new System.Drawing.Point(863, 89);
            this.btnTransferInvoice.Margin = new System.Windows.Forms.Padding(5);
            this.btnTransferInvoice.Name = "btnTransferInvoice";
            this.btnTransferInvoice.Size = new System.Drawing.Size(100, 24);
            this.btnTransferInvoice.TabIndex = 27;
            this.btnTransferInvoice.Text = "Kết chuyển";
            this.btnTransferInvoice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTransferInvoice.UseVisualStyleBackColor = true;
            this.btnTransferInvoice.Click += new System.EventHandler(this.btnTransferInvoice_Click);
            // 
            // cboDataReport
            // 
            this.cboDataReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDataReport.Location = new System.Drawing.Point(105, 35);
            this.cboDataReport.Margin = new System.Windows.Forms.Padding(0);
            this.cboDataReport.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboDataReport.Name = "cboDataReport";
            this.cboDataReport.Size = new System.Drawing.Size(220, 24);
            this.cboDataReport.TabIndex = 7;
            this.cboDataReport.SelectionChangeCommitted += new System.EventHandler(this.cboDataReport_SelectionChangeCommitted);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 39);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Nhóm dữ liệu:";
            // 
            // cboOutputType
            // 
            this.cboOutputType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboOutputType.Location = new System.Drawing.Point(432, 61);
            this.cboOutputType.Margin = new System.Windows.Forms.Padding(0);
            this.cboOutputType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboOutputType.Name = "cboOutputType";
            this.cboOutputType.Size = new System.Drawing.Size(219, 24);
            this.cboOutputType.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(331, 65);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 20);
            this.label6.TabIndex = 15;
            this.label6.Text = "Hình thức xuất:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(209, 66);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 20);
            this.label12.TabIndex = 8;
            this.label12.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(333, 13);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "Kho xuất:";
            // 
            // cboInvoiceTransTypeID
            // 
            this.cboInvoiceTransTypeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInvoiceTransTypeID.Location = new System.Drawing.Point(432, 35);
            this.cboInvoiceTransTypeID.Margin = new System.Windows.Forms.Padding(0);
            this.cboInvoiceTransTypeID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInvoiceTransTypeID.Name = "cboInvoiceTransTypeID";
            this.cboInvoiceTransTypeID.Size = new System.Drawing.Size(219, 24);
            this.cboInvoiceTransTypeID.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(331, 39);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 20);
            this.label8.TabIndex = 8;
            this.label8.Text = "Loại NV HĐ:";
            // 
            // cboInvoiceKind
            // 
            this.cboInvoiceKind.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInvoiceKind.Location = new System.Drawing.Point(757, 9);
            this.cboInvoiceKind.Margin = new System.Windows.Forms.Padding(0);
            this.cboInvoiceKind.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInvoiceKind.Name = "cboInvoiceKind";
            this.cboInvoiceKind.Size = new System.Drawing.Size(206, 24);
            this.cboInvoiceKind.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(656, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Loại hóa đơn:";
            // 
            // chkInvoiceDT
            // 
            this.chkInvoiceDT.AutoSize = true;
            this.chkInvoiceDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInvoiceDT.Location = new System.Drawing.Point(875, 39);
            this.chkInvoiceDT.Margin = new System.Windows.Forms.Padding(4);
            this.chkInvoiceDT.Name = "chkInvoiceDT";
            this.chkInvoiceDT.Size = new System.Drawing.Size(114, 24);
            this.chkInvoiceDT.TabIndex = 11;
            this.chkInvoiceDT.Text = "Chi tiết HĐ";
            this.chkInvoiceDT.UseVisualStyleBackColor = true;
            this.chkInvoiceDT.Visible = false;
            // 
            // chkGroupVAT
            // 
            this.chkGroupVAT.AutoSize = true;
            this.chkGroupVAT.Checked = true;
            this.chkGroupVAT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGroupVAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGroupVAT.Location = new System.Drawing.Point(659, 39);
            this.chkGroupVAT.Margin = new System.Windows.Forms.Padding(4);
            this.chkGroupVAT.Name = "chkGroupVAT";
            this.chkGroupVAT.Size = new System.Drawing.Size(232, 24);
            this.chkGroupVAT.TabIndex = 10;
            this.chkGroupVAT.Text = "Nhóm theo nhóm thuế suất";
            this.chkGroupVAT.UseVisualStyleBackColor = true;
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(432, 9);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(219, 24);
            this.cboStore.TabIndex = 3;
            this.cboStore.SelectionChangeCommitted += new System.EventHandler(this.cboStore_SelectionChangeCommitted);
            // 
            // cboBranch
            // 
            this.cboBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBranch.Location = new System.Drawing.Point(105, 9);
            this.cboBranch.Margin = new System.Windows.Forms.Padding(0);
            this.cboBranch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(220, 24);
            this.cboBranch.TabIndex = 1;
            this.cboBranch.SelectionChangeCommitted += new System.EventHandler(this.cboBranch_SelectionChangeCommitted);
            // 
            // cboSearchBy
            // 
            this.cboSearchBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSearchBy.FormattingEnabled = true;
            this.cboSearchBy.Items.AddRange(new object[] {
            "Số hóa đơn",
            "Ký hiệu mẫu HĐ",
            "Ký hiệu",
            "Số chứng từ"});
            this.cboSearchBy.Location = new System.Drawing.Point(432, 87);
            this.cboSearchBy.Margin = new System.Windows.Forms.Padding(5);
            this.cboSearchBy.Name = "cboSearchBy";
            this.cboSearchBy.Size = new System.Drawing.Size(220, 28);
            this.cboSearchBy.TabIndex = 22;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(223, 63);
            this.dtpToDate.Margin = new System.Windows.Forms.Padding(5);
            this.dtpToDate.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dtpToDate.MinDate = new System.DateTime(1950, 1, 1, 0, 0, 0, 0);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(102, 26);
            this.dtpToDate.TabIndex = 14;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(105, 63);
            this.dtpFromDate.Margin = new System.Windows.Forms.Padding(5);
            this.dtpFromDate.MinDate = new System.DateTime(1950, 1, 1, 0, 0, 0, 0);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(102, 26);
            this.dtpFromDate.TabIndex = 13;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // txtKeywords
            // 
            this.txtKeywords.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeywords.Location = new System.Drawing.Point(105, 89);
            this.txtKeywords.Margin = new System.Windows.Forms.Padding(5);
            this.txtKeywords.MaxLength = 100;
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(220, 26);
            this.txtKeywords.TabIndex = 20;
            this.txtKeywords.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKeywords_KeyDown);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(757, 89);
            this.btnExportExcel.Margin = new System.Windows.Forms.Padding(5);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(99, 24);
            this.btnExportExcel.TabIndex = 25;
            this.btnExportExcel.Text = "    Xuất Excel";
            this.btnExportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(659, 88);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(91, 24);
            this.btnSearch.TabIndex = 23;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(331, 92);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 20);
            this.label4.TabIndex = 21;
            this.label4.Text = "Tìm theo:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 65);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "Ngày hóa đơn:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 92);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 20);
            this.label3.TabIndex = 19;
            this.label3.Text = "Từ khóa:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chi nhánh:";
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroup.Location = new System.Drawing.Point(499, 282);
            this.cboMainGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(215, 24);
            this.cboMainGroup.TabIndex = 5;
            this.cboMainGroup.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(407, 284);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Ngành hàng:";
            this.label7.Visible = false;
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.grdData);
            this.panelMain.Controls.Add(this.panelTop);
            this.panelMain.Controls.Add(this.label7);
            this.panelMain.Controls.Add(this.cboMainGroup);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Margin = new System.Windows.Forms.Padding(5);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1260, 496);
            this.panelMain.TabIndex = 0;
            // 
            // panelBottom
            // 
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 496);
            this.panelBottom.Margin = new System.Windows.Forms.Padding(5);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1260, 0);
            this.panelBottom.TabIndex = 0;
            // 
            // frmReportVTPlusProm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1260, 496);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.panelBottom);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmReportVTPlusProm";
            this.ShowIcon = false;
            this.Text = "Báo cáo tích điểm Viettel++";
            this.Load += new System.EventHandler(this.frmReportVATInvoice_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showInvoiceVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBoxPrinted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsExport)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsExport;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repCheckBoxPrinted;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboVAT;
        private System.Windows.Forms.Panel panelTop;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboBranch;
        private System.Windows.Forms.ComboBox cboSearchBy;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.TextBox txtKeywords;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.CheckBox chkInvoiceDT;
        private System.Windows.Forms.CheckBox chkGroupVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colVATINVOICENAME;
        private DevExpress.XtraGrid.Columns.GridColumn colDENOMINATOR;
        private DevExpress.XtraGrid.Columns.GridColumn colINVOICESYMBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colINVOICENO;
        private DevExpress.XtraGrid.Columns.GridColumn colINVOICEDATE;
        private DevExpress.XtraGrid.Columns.GridColumn colOUTPUTDATE;
        private DevExpress.XtraGrid.Columns.GridColumn colOUTPUTVOUCHERID;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMERNAME_OUT;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMERTAXID;
        private DevExpress.XtraGrid.Columns.GridColumn colPRODUCTNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colTOTALMONEY;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colBRANCH;
        private DevExpress.XtraGrid.Columns.GridColumn colVATGROUP;
        private DevExpress.XtraGrid.Columns.GridColumn colSTT;
        private DevExpress.XtraGrid.Columns.GridColumn colMISAPRODUCTID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit showInvoiceVAT;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboInvoiceKind;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroup;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboInvoiceTransTypeID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStore;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboDataReport;
        private System.Windows.Forms.Label label9;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboOutputType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnTransferInvoice;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboTransferInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colSALEORDERID;
        internal DevExpress.XtraGrid.Columns.GridColumn colCUSTOMERID_VTPLUS;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMERNAME_VTPLUS;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMERPHONE_VTPLUS;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMERID;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMERNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMERPHONE;
        private DevExpress.XtraGrid.Columns.GridColumn colSUBGROUPNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colPRODUCTID;
        private DevExpress.XtraGrid.Columns.GridColumn colTOTALQUANTITY;
        private DevExpress.XtraGrid.Columns.GridColumn colTOTALAMOUNT;
        private DevExpress.XtraGrid.Columns.GridColumn colRATIO;
        private DevExpress.XtraGrid.Columns.GridColumn colPOINT_ADD;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBoxTypeReport;
        private DevExpress.XtraGrid.Columns.GridColumn colINVOICESYMBOL_2;
        private DevExpress.XtraGrid.Columns.GridColumn colINVOICENO_2;
        private DevExpress.XtraGrid.Columns.GridColumn colTOTALMONEY_2;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT_2;
        private DevExpress.XtraGrid.Columns.GridColumn colTOTALMONEY_SUB;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT_SUB;
    }
}