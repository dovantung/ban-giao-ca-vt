﻿namespace ERP.Inventory.DUI.InputChange
{
    partial class frmInputChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInputChange));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageOutputVoucher = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ctrlStaffUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.txtInfoNumber = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDenominator = new System.Windows.Forms.TextBox();
            this.txtCurrencyExchange = new C1.Win.C1Input.C1NumericEdit();
            this.dtInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.lblInvoiceDate = new System.Windows.Forms.Label();
            this.cboCurrencyUnit = new System.Windows.Forms.ComboBox();
            this.lblCurrencyUnit = new System.Windows.Forms.Label();
            this.lblDenominator = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.lblOutputContent = new System.Windows.Forms.Label();
            this.lblStore = new System.Windows.Forms.Label();
            this.dtOutputDate = new System.Windows.Forms.DateTimePicker();
            this.lblOutputDate = new System.Windows.Forms.Label();
            this.lblInVoiceID = new System.Windows.Forms.Label();
            this.txtOutputVoucherID = new System.Windows.Forms.TextBox();
            this.lblOutputVoucherID = new System.Windows.Forms.Label();
            this.txtInVoiceID = new System.Windows.Forms.TextBox();
            this.lblOrderID = new System.Windows.Forms.Label();
            this.txtOrderID = new System.Windows.Forms.TextBox();
            this.txtInvoiceSymbol = new System.Windows.Forms.TextBox();
            this.cboPayableType = new System.Windows.Forms.ComboBox();
            this.lblPayableType = new System.Windows.Forms.Label();
            this.lblInvoiceSymbol = new System.Windows.Forms.Label();
            this.tabPageCustomer = new System.Windows.Forms.TabPage();
            this.grpCustomer = new System.Windows.Forms.GroupBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.lnkCustomer = new System.Windows.Forms.LinkLabel();
            this.txtCustomerPhone = new System.Windows.Forms.TextBox();
            this.txtTaxID = new System.Windows.Forms.TextBox();
            this.txtCustomerAddress = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtIDCardIssuePlace = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dtIDCardIssueDate = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCustomerIDCard = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblTaxID = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flexInputProduct = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.flexOutputProduct = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.btnInputChange = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.tabControl1.SuspendLayout();
            this.tabPageOutputVoucher.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrencyExchange)).BeginInit();
            this.tabPageCustomer.SuspendLayout();
            this.grpCustomer.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexInputProduct)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexOutputProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageOutputVoucher);
            this.tabControl1.Controls.Add(this.tabPageCustomer);
            this.tabControl1.Location = new System.Drawing.Point(6, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(895, 240);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPageOutputVoucher
            // 
            this.tabPageOutputVoucher.BackColor = System.Drawing.SystemColors.Window;
            this.tabPageOutputVoucher.Controls.Add(this.groupBox1);
            this.tabPageOutputVoucher.Location = new System.Drawing.Point(4, 25);
            this.tabPageOutputVoucher.Name = "tabPageOutputVoucher";
            this.tabPageOutputVoucher.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOutputVoucher.Size = new System.Drawing.Size(887, 211);
            this.tabPageOutputVoucher.TabIndex = 0;
            this.tabPageOutputVoucher.Text = "Thông tin phiếu xuất";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboStore);
            this.groupBox1.Controls.Add(this.ctrlStaffUser);
            this.groupBox1.Controls.Add(this.txtInfoNumber);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtDenominator);
            this.groupBox1.Controls.Add(this.txtCurrencyExchange);
            this.groupBox1.Controls.Add(this.dtInvoiceDate);
            this.groupBox1.Controls.Add(this.lblInvoiceDate);
            this.groupBox1.Controls.Add(this.cboCurrencyUnit);
            this.groupBox1.Controls.Add(this.lblCurrencyUnit);
            this.groupBox1.Controls.Add(this.lblDenominator);
            this.groupBox1.Controls.Add(this.txtContent);
            this.groupBox1.Controls.Add(this.lblOutputContent);
            this.groupBox1.Controls.Add(this.lblStore);
            this.groupBox1.Controls.Add(this.dtOutputDate);
            this.groupBox1.Controls.Add(this.lblOutputDate);
            this.groupBox1.Controls.Add(this.lblInVoiceID);
            this.groupBox1.Controls.Add(this.txtOutputVoucherID);
            this.groupBox1.Controls.Add(this.lblOutputVoucherID);
            this.groupBox1.Controls.Add(this.txtInVoiceID);
            this.groupBox1.Controls.Add(this.lblOrderID);
            this.groupBox1.Controls.Add(this.txtOrderID);
            this.groupBox1.Controls.Add(this.txtInvoiceSymbol);
            this.groupBox1.Controls.Add(this.cboPayableType);
            this.groupBox1.Controls.Add(this.lblPayableType);
            this.groupBox1.Controls.Add(this.lblInvoiceSymbol);
            this.groupBox1.Location = new System.Drawing.Point(5, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(880, 201);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin phiếu";
            // 
            // ctrlStaffUser
            // 
            this.ctrlStaffUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrlStaffUser.IsValidate = true;
            this.ctrlStaffUser.Location = new System.Drawing.Point(649, 78);
            this.ctrlStaffUser.Margin = new System.Windows.Forms.Padding(4);
            this.ctrlStaffUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ctrlStaffUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ctrlStaffUser.Name = "ctrlStaffUser";
            this.ctrlStaffUser.Size = new System.Drawing.Size(226, 22);
            this.ctrlStaffUser.TabIndex = 59;
            this.ctrlStaffUser.UserName = "";
            this.ctrlStaffUser.WorkingPositionID = 0;
            // 
            // txtInfoNumber
            // 
            this.txtInfoNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtInfoNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInfoNumber.Location = new System.Drawing.Point(98, 109);
            this.txtInfoNumber.MaxLength = 40;
            this.txtInfoNumber.Name = "txtInfoNumber";
            this.txtInfoNumber.ReadOnly = true;
            this.txtInfoNumber.Size = new System.Drawing.Size(160, 22);
            this.txtInfoNumber.TabIndex = 47;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(3, 112);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 16);
            this.label28.TabIndex = 58;
            this.label28.Text = "Phiếu TTKH:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(554, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "Nhân viên xuất: ";
            // 
            // txtDenominator
            // 
            this.txtDenominator.BackColor = System.Drawing.SystemColors.Info;
            this.txtDenominator.Location = new System.Drawing.Point(649, 50);
            this.txtDenominator.MaxLength = 20;
            this.txtDenominator.Name = "txtDenominator";
            this.txtDenominator.ReadOnly = true;
            this.txtDenominator.Size = new System.Drawing.Size(227, 22);
            this.txtDenominator.TabIndex = 11;
            // 
            // txtCurrencyExchange
            // 
            this.txtCurrencyExchange.BackColor = System.Drawing.SystemColors.Info;
            this.txtCurrencyExchange.CustomFormat = "###,###,##0";
            this.txtCurrencyExchange.Enabled = false;
            this.txtCurrencyExchange.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCurrencyExchange.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtCurrencyExchange.Location = new System.Drawing.Point(727, 107);
            this.txtCurrencyExchange.Name = "txtCurrencyExchange";
            this.txtCurrencyExchange.Size = new System.Drawing.Size(148, 24);
            this.txtCurrencyExchange.TabIndex = 30;
            this.txtCurrencyExchange.Tag = null;
            this.txtCurrencyExchange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrencyExchange.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCurrencyExchange.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // dtInvoiceDate
            // 
            this.dtInvoiceDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtInvoiceDate.Enabled = false;
            this.dtInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtInvoiceDate.Location = new System.Drawing.Point(649, 24);
            this.dtInvoiceDate.Name = "dtInvoiceDate";
            this.dtInvoiceDate.Size = new System.Drawing.Size(227, 22);
            this.dtInvoiceDate.TabIndex = 5;
            // 
            // lblInvoiceDate
            // 
            this.lblInvoiceDate.AutoSize = true;
            this.lblInvoiceDate.Location = new System.Drawing.Point(554, 25);
            this.lblInvoiceDate.Name = "lblInvoiceDate";
            this.lblInvoiceDate.Size = new System.Drawing.Size(96, 16);
            this.lblInvoiceDate.TabIndex = 4;
            this.lblInvoiceDate.Text = "Ngày hóa đơn:";
            // 
            // cboCurrencyUnit
            // 
            this.cboCurrencyUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyUnit.DropDownWidth = 250;
            this.cboCurrencyUnit.Enabled = false;
            this.cboCurrencyUnit.Location = new System.Drawing.Point(649, 107);
            this.cboCurrencyUnit.Name = "cboCurrencyUnit";
            this.cboCurrencyUnit.Size = new System.Drawing.Size(78, 24);
            this.cboCurrencyUnit.TabIndex = 28;
            this.cboCurrencyUnit.SelectionChangeCommitted += new System.EventHandler(this.cboCurrencyUnit_SelectionChangeCommitted);
            // 
            // lblCurrencyUnit
            // 
            this.lblCurrencyUnit.AutoSize = true;
            this.lblCurrencyUnit.Location = new System.Drawing.Point(554, 112);
            this.lblCurrencyUnit.Name = "lblCurrencyUnit";
            this.lblCurrencyUnit.Size = new System.Drawing.Size(61, 16);
            this.lblCurrencyUnit.TabIndex = 27;
            this.lblCurrencyUnit.Text = "Loại tiền:";
            // 
            // lblDenominator
            // 
            this.lblDenominator.AutoSize = true;
            this.lblDenominator.Location = new System.Drawing.Point(554, 53);
            this.lblDenominator.Name = "lblDenominator";
            this.lblDenominator.Size = new System.Drawing.Size(58, 16);
            this.lblDenominator.TabIndex = 10;
            this.lblDenominator.Text = "Mẫu số: ";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(98, 138);
            this.txtContent.MaxLength = 200;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(449, 53);
            this.txtContent.TabIndex = 26;
            // 
            // lblOutputContent
            // 
            this.lblOutputContent.AutoSize = true;
            this.lblOutputContent.Location = new System.Drawing.Point(3, 145);
            this.lblOutputContent.Name = "lblOutputContent";
            this.lblOutputContent.Size = new System.Drawing.Size(65, 16);
            this.lblOutputContent.TabIndex = 25;
            this.lblOutputContent.Text = "Nội dung:";
            // 
            // lblStore
            // 
            this.lblStore.AutoSize = true;
            this.lblStore.Location = new System.Drawing.Point(263, 82);
            this.lblStore.Name = "lblStore";
            this.lblStore.Size = new System.Drawing.Size(67, 16);
            this.lblStore.TabIndex = 15;
            this.lblStore.Text = "Kho nhập:";
            // 
            // dtOutputDate
            // 
            this.dtOutputDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtOutputDate.Enabled = false;
            this.dtOutputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtOutputDate.Location = new System.Drawing.Point(367, 24);
            this.dtOutputDate.Name = "dtOutputDate";
            this.dtOutputDate.Size = new System.Drawing.Size(180, 22);
            this.dtOutputDate.TabIndex = 3;
            // 
            // lblOutputDate
            // 
            this.lblOutputDate.AutoSize = true;
            this.lblOutputDate.Location = new System.Drawing.Point(263, 27);
            this.lblOutputDate.Name = "lblOutputDate";
            this.lblOutputDate.Size = new System.Drawing.Size(71, 16);
            this.lblOutputDate.TabIndex = 2;
            this.lblOutputDate.Text = "Ngày xuất:";
            // 
            // lblInVoiceID
            // 
            this.lblInVoiceID.AutoSize = true;
            this.lblInVoiceID.Location = new System.Drawing.Point(3, 52);
            this.lblInVoiceID.Name = "lblInVoiceID";
            this.lblInVoiceID.Size = new System.Drawing.Size(84, 16);
            this.lblInVoiceID.TabIndex = 6;
            this.lblInVoiceID.Text = "Hóa đơn số: ";
            // 
            // txtOutputVoucherID
            // 
            this.txtOutputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputVoucherID.Location = new System.Drawing.Point(98, 24);
            this.txtOutputVoucherID.Name = "txtOutputVoucherID";
            this.txtOutputVoucherID.ReadOnly = true;
            this.txtOutputVoucherID.Size = new System.Drawing.Size(160, 22);
            this.txtOutputVoucherID.TabIndex = 1;
            // 
            // lblOutputVoucherID
            // 
            this.lblOutputVoucherID.AutoSize = true;
            this.lblOutputVoucherID.Location = new System.Drawing.Point(3, 26);
            this.lblOutputVoucherID.Name = "lblOutputVoucherID";
            this.lblOutputVoucherID.Size = new System.Drawing.Size(93, 16);
            this.lblOutputVoucherID.TabIndex = 0;
            this.lblOutputVoucherID.Text = "Mã phiếu xuất:";
            // 
            // txtInVoiceID
            // 
            this.txtInVoiceID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInVoiceID.Location = new System.Drawing.Point(98, 51);
            this.txtInVoiceID.MaxLength = 20;
            this.txtInVoiceID.Name = "txtInVoiceID";
            this.txtInVoiceID.ReadOnly = true;
            this.txtInVoiceID.Size = new System.Drawing.Size(160, 22);
            this.txtInVoiceID.TabIndex = 7;
            // 
            // lblOrderID
            // 
            this.lblOrderID.AutoSize = true;
            this.lblOrderID.Location = new System.Drawing.Point(3, 82);
            this.lblOrderID.Name = "lblOrderID";
            this.lblOrderID.Size = new System.Drawing.Size(89, 16);
            this.lblOrderID.TabIndex = 19;
            this.lblOrderID.Text = "Mã đơn hàng:";
            // 
            // txtOrderID
            // 
            this.txtOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOrderID.Location = new System.Drawing.Point(98, 79);
            this.txtOrderID.MaxLength = 20;
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.ReadOnly = true;
            this.txtOrderID.Size = new System.Drawing.Size(160, 22);
            this.txtOrderID.TabIndex = 20;
            // 
            // txtInvoiceSymbol
            // 
            this.txtInvoiceSymbol.BackColor = System.Drawing.SystemColors.Info;
            this.txtInvoiceSymbol.Location = new System.Drawing.Point(367, 51);
            this.txtInvoiceSymbol.MaxLength = 20;
            this.txtInvoiceSymbol.Name = "txtInvoiceSymbol";
            this.txtInvoiceSymbol.ReadOnly = true;
            this.txtInvoiceSymbol.Size = new System.Drawing.Size(180, 22);
            this.txtInvoiceSymbol.TabIndex = 9;
            // 
            // cboPayableType
            // 
            this.cboPayableType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPayableType.DropDownWidth = 250;
            this.cboPayableType.Enabled = false;
            this.cboPayableType.Location = new System.Drawing.Point(367, 108);
            this.cboPayableType.Name = "cboPayableType";
            this.cboPayableType.Size = new System.Drawing.Size(180, 24);
            this.cboPayableType.TabIndex = 22;
            // 
            // lblPayableType
            // 
            this.lblPayableType.AutoSize = true;
            this.lblPayableType.Location = new System.Drawing.Point(263, 112);
            this.lblPayableType.Name = "lblPayableType";
            this.lblPayableType.Size = new System.Drawing.Size(97, 16);
            this.lblPayableType.TabIndex = 21;
            this.lblPayableType.Text = "HT thanh toán: ";
            // 
            // lblInvoiceSymbol
            // 
            this.lblInvoiceSymbol.AutoSize = true;
            this.lblInvoiceSymbol.Location = new System.Drawing.Point(263, 52);
            this.lblInvoiceSymbol.Name = "lblInvoiceSymbol";
            this.lblInvoiceSymbol.Size = new System.Drawing.Size(106, 16);
            this.lblInvoiceSymbol.TabIndex = 8;
            this.lblInvoiceSymbol.Text = "Ký hiệu hóa đơn:";
            // 
            // tabPageCustomer
            // 
            this.tabPageCustomer.BackColor = System.Drawing.SystemColors.Window;
            this.tabPageCustomer.Controls.Add(this.grpCustomer);
            this.tabPageCustomer.Location = new System.Drawing.Point(4, 25);
            this.tabPageCustomer.Name = "tabPageCustomer";
            this.tabPageCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCustomer.Size = new System.Drawing.Size(887, 211);
            this.tabPageCustomer.TabIndex = 1;
            this.tabPageCustomer.Text = "Thông tin khách hàng";
            // 
            // grpCustomer
            // 
            this.grpCustomer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grpCustomer.BackColor = System.Drawing.SystemColors.Window;
            this.grpCustomer.Controls.Add(this.txtCustomerID);
            this.grpCustomer.Controls.Add(this.lnkCustomer);
            this.grpCustomer.Controls.Add(this.txtCustomerPhone);
            this.grpCustomer.Controls.Add(this.txtTaxID);
            this.grpCustomer.Controls.Add(this.txtCustomerAddress);
            this.grpCustomer.Controls.Add(this.txtCustomerName);
            this.grpCustomer.Controls.Add(this.txtIDCardIssuePlace);
            this.grpCustomer.Controls.Add(this.label21);
            this.grpCustomer.Controls.Add(this.dtIDCardIssueDate);
            this.grpCustomer.Controls.Add(this.label16);
            this.grpCustomer.Controls.Add(this.txtCustomerIDCard);
            this.grpCustomer.Controls.Add(this.label19);
            this.grpCustomer.Controls.Add(this.label22);
            this.grpCustomer.Controls.Add(this.lblPhone);
            this.grpCustomer.Controls.Add(this.lblTaxID);
            this.grpCustomer.Controls.Add(this.lblAddress);
            this.grpCustomer.Location = new System.Drawing.Point(5, 6);
            this.grpCustomer.Name = "grpCustomer";
            this.grpCustomer.Size = new System.Drawing.Size(879, 199);
            this.grpCustomer.TabIndex = 1;
            this.grpCustomer.TabStop = false;
            this.grpCustomer.Text = "Thông tin khách hàng";
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerID.Location = new System.Drawing.Point(91, 28);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.ReadOnly = true;
            this.txtCustomerID.Size = new System.Drawing.Size(303, 22);
            this.txtCustomerID.TabIndex = 69;
            this.txtCustomerID.TabStop = false;
            // 
            // lnkCustomer
            // 
            this.lnkCustomer.AutoSize = true;
            this.lnkCustomer.Location = new System.Drawing.Point(4, 30);
            this.lnkCustomer.Name = "lnkCustomer";
            this.lnkCustomer.Size = new System.Drawing.Size(81, 16);
            this.lnkCustomer.TabIndex = 3;
            this.lnkCustomer.TabStop = true;
            this.lnkCustomer.Text = "Khách hàng:";
            this.lnkCustomer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkCustomer_LinkClicked);
            // 
            // txtCustomerPhone
            // 
            this.txtCustomerPhone.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerPhone.Location = new System.Drawing.Point(91, 135);
            this.txtCustomerPhone.MaxLength = 9;
            this.txtCustomerPhone.Name = "txtCustomerPhone";
            this.txtCustomerPhone.ReadOnly = true;
            this.txtCustomerPhone.Size = new System.Drawing.Size(160, 22);
            this.txtCustomerPhone.TabIndex = 65;
            // 
            // txtTaxID
            // 
            this.txtTaxID.BackColor = System.Drawing.SystemColors.Info;
            this.txtTaxID.Location = new System.Drawing.Point(91, 163);
            this.txtTaxID.MaxLength = 20;
            this.txtTaxID.Name = "txtTaxID";
            this.txtTaxID.ReadOnly = true;
            this.txtTaxID.Size = new System.Drawing.Size(160, 22);
            this.txtTaxID.TabIndex = 64;
            // 
            // txtCustomerAddress
            // 
            this.txtCustomerAddress.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerAddress.Location = new System.Drawing.Point(91, 82);
            this.txtCustomerAddress.MaxLength = 400;
            this.txtCustomerAddress.Multiline = true;
            this.txtCustomerAddress.Name = "txtCustomerAddress";
            this.txtCustomerAddress.ReadOnly = true;
            this.txtCustomerAddress.Size = new System.Drawing.Size(303, 47);
            this.txtCustomerAddress.TabIndex = 63;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerName.Location = new System.Drawing.Point(91, 54);
            this.txtCustomerName.MaxLength = 200;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.ReadOnly = true;
            this.txtCustomerName.Size = new System.Drawing.Size(303, 22);
            this.txtCustomerName.TabIndex = 62;
            // 
            // txtIDCardIssuePlace
            // 
            this.txtIDCardIssuePlace.BackColor = System.Drawing.SystemColors.Info;
            this.txtIDCardIssuePlace.Location = new System.Drawing.Point(546, 82);
            this.txtIDCardIssuePlace.MaxLength = 50;
            this.txtIDCardIssuePlace.Name = "txtIDCardIssuePlace";
            this.txtIDCardIssuePlace.ReadOnly = true;
            this.txtIDCardIssuePlace.Size = new System.Drawing.Size(191, 22);
            this.txtIDCardIssuePlace.TabIndex = 6;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(469, 82);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 22);
            this.label21.TabIndex = 60;
            this.label21.Text = "Nơi cấp:";
            // 
            // dtIDCardIssueDate
            // 
            this.dtIDCardIssueDate.CustomFormat = "dd/MM/yyyy";
            this.dtIDCardIssueDate.Enabled = false;
            this.dtIDCardIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtIDCardIssueDate.Location = new System.Drawing.Point(546, 54);
            this.dtIDCardIssueDate.Name = "dtIDCardIssueDate";
            this.dtIDCardIssueDate.Size = new System.Drawing.Size(191, 22);
            this.dtIDCardIssueDate.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(469, 54);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 22);
            this.label16.TabIndex = 58;
            this.label16.Text = "Ngày cấp:";
            // 
            // txtCustomerIDCard
            // 
            this.txtCustomerIDCard.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerIDCard.Location = new System.Drawing.Point(546, 25);
            this.txtCustomerIDCard.MaxLength = 9;
            this.txtCustomerIDCard.Name = "txtCustomerIDCard";
            this.txtCustomerIDCard.ReadOnly = true;
            this.txtCustomerIDCard.Size = new System.Drawing.Size(191, 22);
            this.txtCustomerIDCard.TabIndex = 4;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(469, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 16);
            this.label19.TabIndex = 22;
            this.label19.Text = "Số CMND:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(4, 57);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 16);
            this.label22.TabIndex = 3;
            this.label22.Text = "Họ tên KH:";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(4, 138);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(70, 16);
            this.lblPhone.TabIndex = 7;
            this.lblPhone.Text = "Điện thoại:";
            // 
            // lblTaxID
            // 
            this.lblTaxID.AutoSize = true;
            this.lblTaxID.Location = new System.Drawing.Point(4, 166);
            this.lblTaxID.Name = "lblTaxID";
            this.lblTaxID.Size = new System.Drawing.Size(76, 16);
            this.lblTaxID.TabIndex = 9;
            this.lblTaxID.Text = "Mã số thuế:";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(4, 85);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 16);
            this.lblAddress.TabIndex = 5;
            this.lblAddress.Text = "Địa chỉ:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox2.Controls.Add(this.flexInputProduct);
            this.groupBox2.Location = new System.Drawing.Point(6, 246);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(895, 159);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chi tiết nhập";
            // 
            // flexInputProduct
            // 
            this.flexInputProduct.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexInputProduct.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexInputProduct.AutoClipboard = true;
            this.flexInputProduct.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexInputProduct.Location = new System.Drawing.Point(4, 21);
            this.flexInputProduct.Name = "flexInputProduct";
            this.flexInputProduct.Rows.DefaultSize = 21;
            this.flexInputProduct.Size = new System.Drawing.Size(887, 132);
            this.flexInputProduct.StyleInfo = resources.GetString("flexInputProduct.StyleInfo");
            this.flexInputProduct.TabIndex = 37;
            this.flexInputProduct.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexInputProduct.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexInputProduct_AfterEdit);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox3.Controls.Add(this.flexOutputProduct);
            this.groupBox3.Location = new System.Drawing.Point(6, 406);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(895, 160);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Chi tiết xuất";
            // 
            // flexOutputProduct
            // 
            this.flexOutputProduct.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexOutputProduct.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexOutputProduct.AutoClipboard = true;
            this.flexOutputProduct.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexOutputProduct.Location = new System.Drawing.Point(4, 21);
            this.flexOutputProduct.Name = "flexOutputProduct";
            this.flexOutputProduct.Rows.DefaultSize = 21;
            this.flexOutputProduct.Size = new System.Drawing.Size(887, 134);
            this.flexOutputProduct.StyleInfo = resources.GetString("flexOutputProduct.StyleInfo");
            this.flexOutputProduct.TabIndex = 38;
            this.flexOutputProduct.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexOutputProduct.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexOutputProduct_BeforeEdit);
            this.flexOutputProduct.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexOutputProduct_AfterEdit);
            this.flexOutputProduct.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexOutputProduct_SetupEditor);
            // 
            // btnInputChange
            // 
            this.btnInputChange.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnInputChange.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInputChange.Location = new System.Drawing.Point(672, 572);
            this.btnInputChange.Name = "btnInputChange";
            this.btnInputChange.Size = new System.Drawing.Size(129, 25);
            this.btnInputChange.TabIndex = 32;
            this.btnInputChange.Text = "       Nhập đổi hàng";
            this.btnInputChange.Click += new System.EventHandler(this.btnInputChange_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::ERP.Inventory.DUI.Properties.Resources.close;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(807, 572);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(94, 25);
            this.btnClose.TabIndex = 33;
            this.btnClose.Text = "      Đóng lại";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(557, 167);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(206, 24);
            this.cboStore.TabIndex = 60;
            this.cboStore.SelectionChangeCommitted += new System.EventHandler(this.cboStore_SelectionChangeCommitted);
            // 
            // frmInputChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 602);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnInputChange);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(913, 627);
            this.Name = "frmInputChange";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập đổi hàng";
            this.Load += new System.EventHandler(this.frmInputChange_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPageOutputVoucher.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrencyExchange)).EndInit();
            this.tabPageCustomer.ResumeLayout(false);
            this.grpCustomer.ResumeLayout(false);
            this.grpCustomer.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexInputProduct)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexOutputProduct)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageOutputVoucher;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtInfoNumber;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDenominator;
        private C1.Win.C1Input.C1NumericEdit txtCurrencyExchange;
        private System.Windows.Forms.DateTimePicker dtInvoiceDate;
        private System.Windows.Forms.Label lblInvoiceDate;
        private System.Windows.Forms.ComboBox cboCurrencyUnit;
        private System.Windows.Forms.Label lblCurrencyUnit;
        private System.Windows.Forms.Label lblDenominator;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label lblOutputContent;
        private System.Windows.Forms.Label lblStore;
        private System.Windows.Forms.DateTimePicker dtOutputDate;
        private System.Windows.Forms.Label lblOutputDate;
        private System.Windows.Forms.Label lblInVoiceID;
        private System.Windows.Forms.TextBox txtOutputVoucherID;
        private System.Windows.Forms.Label lblOutputVoucherID;
        private System.Windows.Forms.TextBox txtInVoiceID;
        private System.Windows.Forms.Label lblOrderID;
        private System.Windows.Forms.TextBox txtOrderID;
        private System.Windows.Forms.TextBox txtInvoiceSymbol;
        private System.Windows.Forms.ComboBox cboPayableType;
        private System.Windows.Forms.Label lblPayableType;
        private System.Windows.Forms.Label lblInvoiceSymbol;
        private System.Windows.Forms.TabPage tabPageCustomer;
        private System.Windows.Forms.GroupBox grpCustomer;
        private System.Windows.Forms.TextBox txtCustomerPhone;
        private System.Windows.Forms.TextBox txtTaxID;
        private System.Windows.Forms.TextBox txtCustomerAddress;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.TextBox txtIDCardIssuePlace;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dtIDCardIssueDate;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCustomerIDCard;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblTaxID;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnInputChange;
        private System.Windows.Forms.Button btnClose;
        private C1.Win.C1FlexGrid.C1FlexGrid flexInputProduct;
        private C1.Win.C1FlexGrid.C1FlexGrid flexOutputProduct;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ctrlStaffUser;
        private System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.LinkLabel lnkCustomer;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStore;
    }
}