﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.InputChange
{
    public partial class frmInputChangeInput : Form
    {
        public frmInputChangeInput()
        {
            InitializeComponent();
        }

        private void cmdInputChange_Click(object sender, EventArgs e)
        {
            cmdInputChange.Enabled = false;
            ERP.Inventory.PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
            String strOutputVoucherID = txtOutputVoucherID.Text.Trim();
            DataTable dtbOutputVoucherDetail = objPLCOutputVoucher.GetList(strOutputVoucherID);
            ERP.Inventory.PLC.InputChange.PLCInputChange objPLCInputChange = new PLC.InputChange.PLCInputChange();
            DataTable dtbInputChangeDetail = objPLCInputChange.GetListByOutputVoucher(strOutputVoucherID);
            if (dtbInputChangeDetail.Rows.Count < 1)
            {
                MessageBox.Show(this, "Phiếu xuất " + strOutputVoucherID + " không có sản phẩm(hoặc IMEI) nào để nhập đổi hàng. Vui lòng chọn phiếu xuất khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmdInputChange.Enabled = true;
                return;
            }
            ERP.Inventory.DUI.InputChange.frmInputChange frmInputChange1 = new InputChange.frmInputChange();
            frmInputChange1.strOutputVoucherID = strOutputVoucherID;
            frmInputChange1.ShowDialog();
            cmdInputChange.Enabled = true;
        }

        private void frmInputChangeInput_Load(object sender, EventArgs e)
        {

        }
    }
}
