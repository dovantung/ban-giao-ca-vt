﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC;

namespace ERP.Inventory.DUI.InputChange
{
    /// <summary>
    /// Created by: Đặng Minh Hùng
    /// Desc: Nhập đổi hàng
    /// </summary>
    public partial class frmInputChange : Form
    {
        public frmInputChange()
        {
            InitializeComponent();
        }

        #region variable
        private ERP.Inventory.PLC.InputChange.PLCInputChange objPLCInputChange = new PLC.InputChange.PLCInputChange();
        public string strOutputVoucherID = string.Empty;
        private ERP.Inventory.PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
        private ERP.Inventory.PLC.Output.WSOutputVoucher.OutputVoucher objOutputVoucher = new PLC.Output.WSOutputVoucher.OutputVoucher();
        private int intOldStoreID = -1;
        private DataTable dtbInputProduct = null;
        private DataTable dtbOutputProduct = null;
        private DataTable dtbStoreInStock = null;
        private DataTable dtbFirstPriceInputVoucherDetail = null;
        private int intInputTypeID = 0;
        private int intOutputTypeID = 0;
        private bool bolIsCheckDuplicateIMEIAllProduct = false;
        private bool bolIsCheckDuplicateIMEIAllStore = false;
        private bool bolIsLockByClosingDataMonth = false;
        private ERP.Inventory.PLC.ProductChange.PLCProductChange objPLCProductChange = new PLC.ProductChange.PLCProductChange();
        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        #endregion

        #region method

        private void LoadOutputVoucher()
        {
            try
            {
                if (strOutputVoucherID != string.Empty)
                {
                    objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    if (objOutputVoucher != null)
                    {
                        txtOutputVoucherID.Text = objOutputVoucher.OutputVoucherID;
                        dtOutputDate.Value = objOutputVoucher.OutputDate.Value;
                        try
                        {
                            bolIsLockByClosingDataMonth = ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.OUTPUTVOUCHER,
                                objOutputVoucher.OutputDate.Value, objOutputVoucher.OutputStoreID);
                        }
                        catch { }
                        dtInvoiceDate.Value = objOutputVoucher.InvoiceDate.Value;
                        txtInVoiceID.Text = objOutputVoucher.InvoiceID;
                        txtInvoiceSymbol.Text = objOutputVoucher.InvoiceSymbol;
                        txtDenominator.Text = objOutputVoucher.Denominator;
                        ctrlStaffUser.UserName = objOutputVoucher.StaffUser;
                        cboStore.SetValue(objOutputVoucher.OutputStoreID);
                        if (cboStore.StoreID < 0)
                        {
                            cboStore.SetValue(0);
                        }
                        intOldStoreID = cboStore.StoreID;
                        txtOrderID.Text = objOutputVoucher.OrderID;
                        txtCustomerID.Text = objOutputVoucher.CustomerID.ToString();
                        txtCustomerName.Text = objOutputVoucher.CustomerName;
                        txtCustomerAddress.Text = objOutputVoucher.CustomerAddress;
                        txtCustomerPhone.Text = objOutputVoucher.CustomerPhone;
                        txtTaxID.Text = objOutputVoucher.CustomerTaxID;
                        cboPayableType.SelectedValue = objOutputVoucher.PayableTypeID;
                        if (cboPayableType.SelectedIndex < 0 && cboPayableType.Items.Count > 0)
                        {
                            cboPayableType.SelectedIndex = 0;
                        }
                        txtContent.Text = objOutputVoucher.OutputContent;
                        cboCurrencyUnit.SelectedValue = objOutputVoucher.CurrencyUnitID;
                        if (cboCurrencyUnit.SelectedIndex > 0)
                        {
                            cboCurrencyUnit_SelectionChangeCommitted(null, null);
                        }
                        if (cboCurrencyUnit.SelectedIndex < 0 && cboCurrencyUnit.Items.Count > 0)
                        {
                            cboCurrencyUnit.SelectedIndex = 0;
                        }
                        dtbInputProduct = objPLCInputChange.GetListByOutputVoucher(objOutputVoucher.OutputVoucherID);
                        if (!dtbInputProduct.Columns.Contains("IsSelect"))
                        {
                            dtbInputProduct.Columns.Add("IsSelect", typeof(bool));
                            dtbInputProduct.Columns["IsSelect"].DefaultValue = false;
                            dtbInputProduct.Columns["IsSelect"].SetOrdinal(0);
                        }

                        string strInputVoucherDetailIDList = string.Empty;
                        for (int i = dtbInputProduct.Rows.Count - 1; i > -1; i--)
                        {
                            dtbInputProduct.Rows[i]["IsSelect"] = false;
                            if ((!Convert.IsDBNull(dtbInputProduct.Rows[i]["InputVoucherDetailID"])) && Convert.ToString(dtbInputProduct.Rows[i]["InputVoucherDetailID"]).Trim() != string.Empty)
                            {
                                strInputVoucherDetailIDList = "<" + Convert.ToString(dtbInputProduct.Rows[i]["InputVoucherDetailID"]).Trim() + ">";
                            }
                        }

                        if (dtbInputProduct.Rows.Count < 1)
                        {
                            LockControl();
                        }
                        flexInputProduct.DataSource = dtbInputProduct;
                        FormatFlexInput();
                        dtbOutputProduct = dtbInputProduct.Clone();
                        flexOutputProduct.DataSource = dtbOutputProduct;
                        FormatFlexOutput();
                    }
                }
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy thông tin phiếu xuất");
                SystemErrorWS.Insert("Lỗi lấy thông tin phiếu xuất", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        private void LoadCombobox()
        {
            try
            {
                //Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboStore, true, -1, -1, -1, Library.AppCore.Constant.EnumType.StorePermissionType.INPUT);
                Library.AppCore.DataSource.FilterObject.StoreFilter obj = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                obj.IsCheckPermission = true;
                obj.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.INPUT;
                cboStore.InitControl(false, obj);
                Library.AppCore.LoadControls.SetDataSource.SetPayableType(this, cboPayableType);
                Library.AppCore.LoadControls.SetDataSource.SetCurrencyUnit(this, cboCurrencyUnit);
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nạp combobox");
                SystemErrorWS.Insert("Lỗi nạp combobox", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        public float GetCurrencyExchange(int CurrencyUnitID)
        {
            DataTable dtbCurrencyUnit = Library.AppCore.DataSource.GetDataSource.GetCurrencyUnit();
            DataRow[] rows = dtbCurrencyUnit.Select("CURRENCYUNITID = " + CurrencyUnitID);
            if (rows.Length < 1)
            {
                return 0;
            }
            return Convert.ToSingle(rows[0]["CURRENCYEXCHANGE"]);
        }

        private bool CheckUpdate()
        {
            if (cboStore.StoreID < 1)
            {
                cboStore.Focus();
                MessageBox.Show(this, "Bạn chưa chọn kho nhập đổi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (txtCustomerID.Text.Trim() == "")
            {
                txtCustomerID.Focus();
                MessageBox.Show(this, "Vui lòng chọn khách hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (Convert.ToString(ctrlStaffUser.UserName).Trim() == string.Empty)
            {
                ctrlStaffUser.Focus();
                MessageBox.Show(this, "Bạn chưa nhập nhân viên xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            dtbInputProduct.AcceptChanges();

            DataRow[] rIsSelect = dtbInputProduct.Select("IsSelect = 1");
            if (rIsSelect.Length < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất 1 sản phẩm cần nhập đổi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            DataRow[] rNotIMEIOutput = dtbOutputProduct.Select("IMEI IS NULL OR TRIM(IMEI)=''");
            if (rNotIMEIOutput.Length > 0)
            {
                for (int i = 0; i < rNotIMEIOutput.Length; i++)
                {
                    string strProductID = Convert.ToString(rNotIMEIOutput[i]["ProductID"]).Trim();
                    DataRow[] rIMEIInput = dtbInputProduct.Select("ProductID = '" + strProductID + "' AND IMEI IS NOT NULL AND TRIM(IMEI)<>''");
                    if (rIMEIInput.Length > 0)
                    {
                        MessageBox.Show(this, "Vui lòng nhập IMEI cho sản phẩm xuất đổi " + strProductID, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
            }

            DataRow[] rQuantityNotIMEIOutput = dtbOutputProduct.Select("IsRequestIMEI <> 1 AND (Quantity = 0 OR Quantity IS NULL)");
            if (rQuantityNotIMEIOutput.Length > 0)
            {
                MessageBox.Show(this, "Vui lòng nhập số lượng cho sản phẩm xuất đổi " + Convert.ToString(rQuantityNotIMEIOutput[0]["ProductID"]).Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void ClearOutputProduct()
        {
            Library.AppCore.LoadControls.C1FlexGridObject.CheckAll(flexInputProduct, 1, false, 0);
            dtbOutputProduct = new DataTable();
            dtbOutputProduct = dtbInputProduct.Clone();
            flexOutputProduct.DataSource = dtbOutputProduct;
            FormatFlexOutput();
        }

        private void FormatFlexInput()
        {
            flexInputProduct.Rows.Fixed = 1;
            for (int i = 0; i < flexInputProduct.Cols.Count; i++)
            {
                flexInputProduct.Cols[i].Visible = false;
                flexInputProduct.Cols[i].AllowSorting = false;
                flexInputProduct.Cols[i].AllowDragging = false;
                flexInputProduct.Cols[i].AllowEditing = false;
            }
            flexInputProduct.Cols["IsSelect"].Visible = true;
            flexInputProduct.Cols["ProductID"].Visible = true;
            flexInputProduct.Cols["ProductName"].Visible = true;
            flexInputProduct.Cols["IMEI"].Visible = true;
            flexInputProduct.Cols["EndWarrantyDate"].Visible = true;
            flexInputProduct.Cols["SalePrice"].Visible = true;
            flexInputProduct.Cols["Quantity"].Visible = true;
            flexInputProduct.Cols["TotalAmount"].Visible = true;
            flexInputProduct.Cols["IsSelect"].Caption = "Chọn";
            flexInputProduct.Cols["ProductID"].Caption = "Mã sản phẩm";
            flexInputProduct.Cols["ProductName"].Caption = "Tên sản phẩm";
            flexInputProduct.Cols["IMEI"].Caption = "IMEI";
            flexInputProduct.Cols["EndWarrantyDate"].Caption = "Ngày bảo hành";
            flexInputProduct.Cols["SalePrice"].Caption = "Giá bán";
            flexInputProduct.Cols["Quantity"].Caption = "Số lượng";
            flexInputProduct.Cols["TotalAmount"].Caption = "Tổng tiền";
            flexInputProduct.Cols["IsSelect"].Width = 40;
            flexInputProduct.Cols["ProductID"].Width = 120;
            flexInputProduct.Cols["ProductName"].Width = 200;
            flexInputProduct.Cols["IMEI"].Width = 150;
            flexInputProduct.Cols["EndWarrantyDate"].Width = 80;
            flexInputProduct.Cols["SalePrice"].Width = 80;
            flexInputProduct.Cols["Quantity"].Width = 80;
            flexInputProduct.Cols["TotalAmount"].Width = 100;

            flexInputProduct.Cols["SalePrice"].Format = "#,##0";
            flexInputProduct.Cols["TotalAmount"].Format = "#,##0";
            flexInputProduct.Cols["Quantity"].Format = "#,##0";
            flexInputProduct.Cols["EndWarrantyDate"].Format = "dd/MM/yyyy";
            flexInputProduct.Cols["IsSelect"].DataType = typeof(bool);
            flexInputProduct.Cols["IsSelect"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexInputProduct.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexInputProduct.GetCellRange(0, 0, 0, flexInputProduct.Cols.Count - 1);
            range.Style = style;
            flexInputProduct.Rows[0].Height = 40;
            flexInputProduct.Cols.Fixed = 1;

            for (int i = flexInputProduct.Rows.Fixed; i < flexInputProduct.Rows.Count; i++)
            {
                flexInputProduct.Rows[i].AllowDragging = false;
                C1.Win.C1FlexGrid.CellStyle sty = flexInputProduct.Styles.Add("flexStyleRow" + i.ToString());
                sty.WordWrap = true;
                sty.BackColor = flexInputProduct.BackColor;
                sty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular);
                sty.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
                C1.Win.C1FlexGrid.CellRange rang = flexInputProduct.GetCellRange(i, 0, i, flexInputProduct.Cols.Count - 1);
                rang.Style = sty;
            }
        }

        private void FormatFlexOutput()
        {
            flexOutputProduct.Rows.Fixed = 1;
            for (int i = 0; i < flexOutputProduct.Cols.Count; i++)
            {
                flexOutputProduct.Cols[i].Visible = false;
                flexOutputProduct.Cols[i].AllowSorting = false;
                flexOutputProduct.Cols[i].AllowEditing = false;
            }
            flexOutputProduct.Cols["ProductID"].Visible = true;
            flexOutputProduct.Cols["ProductName"].Visible = true;
            flexOutputProduct.Cols["IMEI"].Visible = true;
            flexOutputProduct.Cols["EndWarrantyDate"].Visible = true;
            flexOutputProduct.Cols["SalePrice"].Visible = true;
            flexOutputProduct.Cols["Quantity"].Visible = true;
            flexOutputProduct.Cols["TotalAmount"].Visible = true;
            flexOutputProduct.Cols["ProductID"].Caption = "Mã sản phẩm";
            flexOutputProduct.Cols["ProductName"].Caption = "Tên sản phẩm";
            flexOutputProduct.Cols["IMEI"].Caption = "IMEI";
            flexOutputProduct.Cols["EndWarrantyDate"].Caption = "Ngày bảo hành";
            flexOutputProduct.Cols["SalePrice"].Caption = "Giá bán";
            flexOutputProduct.Cols["Quantity"].Caption = "Số lượng";
            flexOutputProduct.Cols["TotalAmount"].Caption = "Tổng tiền";
            flexOutputProduct.Cols["ProductID"].Width = 160;
            flexOutputProduct.Cols["ProductName"].Width = 200;
            flexOutputProduct.Cols["IMEI"].Width = 150;
            flexOutputProduct.Cols["EndWarrantyDate"].Width = 80;
            flexOutputProduct.Cols["SalePrice"].Width = 80;
            flexOutputProduct.Cols["Quantity"].Width = 80;
            flexOutputProduct.Cols["TotalAmount"].Width = 100;

            flexOutputProduct.Cols["IMEI"].AllowEditing = true;
            flexOutputProduct.Cols["Quantity"].AllowEditing = true;

            flexOutputProduct.Cols["SalePrice"].Format = "#,##0";
            flexOutputProduct.Cols["TotalAmount"].Format = "#,##0";
            flexOutputProduct.Cols["Quantity"].Format = "#,##0";
            flexOutputProduct.Cols["EndWarrantyDate"].Format = "dd/MM/yyyy";

            C1.Win.C1FlexGrid.CellStyle style = flexOutputProduct.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexOutputProduct.GetCellRange(0, 0, 0, flexOutputProduct.Cols.Count - 1);
            range.Style = style;
            flexOutputProduct.Rows[0].Height = 40;
            flexOutputProduct.Cols.Fixed = 1;

            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnDigit(flexOutputProduct, 6, 0, false, "Quantity");
        }
        private void LockControl()
        {
            cboStore.Enabled = false;
            cboPayableType.Enabled = false;
            cboCurrencyUnit.Enabled = false;
            flexInputProduct.Enabled = false;
            lnkCustomer.Enabled = false;
            flexOutputProduct.Enabled = false;
            btnInputChange.Enabled = false;
        }

        #endregion

        #region event

        private void frmInputChange_Load(object sender, EventArgs e)
        {
            LoadCombobox();
            LoadOutputVoucher();

            dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0, "@OutputVoucherID", strOutputVoucherID, "@IsRequestIMEI", true });
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                LockControl();
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }

            ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfigInputType = null;
            ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfigOutputType = null;
            ERP.MasterData.SYS.PLC.PLCAppConfig objPLCAppConfig = new MasterData.SYS.PLC.PLCAppConfig();
            objPLCAppConfig.LoadInfo(ref objAppConfigInputType, "InputChange_InputTypeID");
            if (objAppConfigInputType != null && objAppConfigInputType.ConfigValue != string.Empty)
            {
                try
                {
                    intInputTypeID = Convert.ToInt32(objAppConfigInputType.ConfigValue);
                }
                catch
                {
                }
            }
            objPLCAppConfig.LoadInfo(ref objAppConfigOutputType, "InputChange_OutputTypeID");
            if (objAppConfigOutputType != null && objAppConfigOutputType.ConfigValue != string.Empty)
            {
                try
                {
                    intOutputTypeID = Convert.ToInt32(objAppConfigOutputType.ConfigValue);
                }
                catch
                {
                }
            }

            if (intInputTypeID < 1 || intOutputTypeID < 1)
            {
                LockControl();
                MessageBox.Show(this, "Chưa khai báo cấu hình ứng dụng hình thức nhập, hình thức xuất của nhập đổi hàng. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (bolIsLockByClosingDataMonth)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tháng " + dtOutputDate.Value.Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa");
                LockControl();
                btnInputChange.Enabled = false;
                return;
            }

            ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfig_IsCheckDuplicateIMEIAllProduct = null;
            ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfig_IsCheckDuplicateIMEIAllStore = null;
            objPLCAppConfig.LoadInfo(ref objAppConfig_IsCheckDuplicateIMEIAllProduct, "ISCHECKDUPLICATEIMEIALLPRODUCT");
            if (objAppConfig_IsCheckDuplicateIMEIAllProduct != null && objAppConfig_IsCheckDuplicateIMEIAllProduct.ConfigValue != string.Empty)
            {
                try
                {
                    bolIsCheckDuplicateIMEIAllProduct = Convert.ToBoolean(Convert.ToInt32(objAppConfig_IsCheckDuplicateIMEIAllProduct.ConfigValue));
                }
                catch
                {
                }
            }
            objPLCAppConfig.LoadInfo(ref objAppConfig_IsCheckDuplicateIMEIAllStore, "ISCHECKDUPLICATEIMEIALLSTORE");
            if (objAppConfig_IsCheckDuplicateIMEIAllStore != null && objAppConfig_IsCheckDuplicateIMEIAllStore.ConfigValue != string.Empty)
            {
                try
                {
                    bolIsCheckDuplicateIMEIAllStore = Convert.ToBoolean(Convert.ToInt32(objAppConfig_IsCheckDuplicateIMEIAllStore.ConfigValue));
                }
                catch
                {
                }
            }

            if (strOutputVoucherID != string.Empty)
            {
                dtbFirstPriceInputVoucherDetail = objPLCInputChange.GetFirstPriceInputVoucherDetail(strOutputVoucherID);
            }
            else
            {
                dtbFirstPriceInputVoucherDetail = objPLCInputChange.GetFirstPriceInputVoucherDetail("-1");
            }
        }

        private void cboStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (intOldStoreID > 0 && intOldStoreID != cboStore.StoreID && dtbOutputProduct.Rows.Count > 0)
            {
                if (MessageBox.Show("Khi thay đổi kho nhập, dữ liệu chi tiết xuất vào sẽ bị xóa!\n Bạn có chắc muốn thay đổi kho nhập không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ClearOutputProduct();
                }
                else
                {
                    cboStore.SetValue(intOldStoreID);
                }
            }
            if (cboStore.StoreID > 0)
            {
                intOldStoreID = cboStore.StoreID;
            }
        }

        private void cboCurrencyUnit_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtCurrencyExchange.Value = ERP.MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyExchange(Convert.ToInt32(cboCurrencyUnit.SelectedValue)).ToString("#,###,##0");
        }

        private void btnInputChange_Click(object sender, EventArgs e)
        {
            if (bolIsLockByClosingDataMonth)
            {
                return;
            }
            try
            {
                if (!CheckUpdate())
                {
                    return;
                }

                if (bolIsCheckDuplicateIMEIAllProduct || bolIsCheckDuplicateIMEIAllStore)
                {
                    DataRow[] rIsSelect = dtbInputProduct.Select("IsSelect = 1");
                    DataRow[] rCheckDuplicateIMEI;
                    if (bolIsCheckDuplicateIMEIAllProduct && bolIsCheckDuplicateIMEIAllStore)
                    {
                        for (int i = 0; i < rIsSelect.Length; i++)
                        {
                            rCheckDuplicateIMEI = dtbStoreInStock.Select("TRIM(IMEI) = '" + Convert.ToString(rIsSelect[i]["IMEI"]).Trim() + "'");
                            if (rCheckDuplicateIMEI.Length > 0)
                            {
                                MessageBox.Show(this, "Hệ thống không cho phép nhập trùng IMEI trên tất cả sản phẩm và tất cả kho. Vui lòng kiểm tra lại IMEI nhập vào " + Convert.ToString(rIsSelect[i]["IMEI"]).Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                    else if (bolIsCheckDuplicateIMEIAllProduct)
                    {
                        for (int i = 0; i < rIsSelect.Length; i++)
                        {
                            rCheckDuplicateIMEI = dtbStoreInStock.Select("StoreID = " + cboStore.StoreID + " AND TRIM(IMEI) = '" + Convert.ToString(rIsSelect[i]["IMEI"]).Trim() + "'");
                            if (rCheckDuplicateIMEI.Length > 0)
                            {
                                MessageBox.Show(this, "Hệ thống không cho phép nhập trùng IMEI trên tất cả sản phẩm. Vui lòng kiểm tra lại IMEI nhập vào " + Convert.ToString(rIsSelect[i]["IMEI"]).Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                    else if (bolIsCheckDuplicateIMEIAllStore)
                    {
                        for (int i = 0; i < rIsSelect.Length; i++)
                        {
                            rCheckDuplicateIMEI = dtbStoreInStock.Select("TRIM(ProductID) = '" + Convert.ToString(rIsSelect[i]["ProductID"]).Trim() + "' AND TRIM(IMEI) = '" + Convert.ToString(rIsSelect[i]["IMEI"]).Trim() + "'");
                            if (rCheckDuplicateIMEI.Length > 0)
                            {
                                MessageBox.Show(this, "Hệ thống không cho phép nhập trùng IMEI trên tất cả kho. Vui lòng kiểm tra lại IMEI nhập vào " + Convert.ToString(rIsSelect[i]["IMEI"]).Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                }
                else
                {
                    DataRow[] rIsCheckSelect = dtbInputProduct.Select("IsSelect = 1");
                    for (int i = 0; i < rIsCheckSelect.Length; i++)
                    {
                        DataRow[] rCheckInputIMEI = dtbStoreInStock.Select("StoreID = " + cboStore.StoreID + " AND TRIM(ProductID) = '" + Convert.ToString(rIsCheckSelect[i]["ProductID"]).Trim() + "' AND TRIM(IMEI) = '" + Convert.ToString(rIsCheckSelect[i]["IMEI"]).Trim() + "'");
                        if (rCheckInputIMEI.Length > 0)
                        {
                            MessageBox.Show(this, "IMEI nhập vào " + Convert.ToString(rIsCheckSelect[i]["IMEI"]).Trim() + " đã tồn tại trong kho. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }

                btnInputChange.Enabled = false;

                int intStoreID = cboStore.StoreID;
                string strUserName = Library.AppCore.SystemConfig.objSessionUser.UserName;
                int intCreateStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                DateTime dtmInputDate = Library.AppCore.Globals.GetServerDateTime();
                string strContent = txtContent.Text.Trim();

                int intCurrencyUnitID = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
                decimal decCurrencyExchange = Convert.ToDecimal(txtCurrencyExchange.Value);
                int intCustomerID = 0;
                try
                {
                    if (Convert.ToString(txtCustomerID.Text).Trim() != string.Empty)
                    {
                        intCustomerID = Convert.ToInt32(txtCustomerID.Text);
                    }
                }
                catch
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy mã khách hàng! Vui lòng kiểm tra lại");
                    btnInputChange.Enabled = true;
                    return;
                }
                string strCustomerName = txtCustomerName.Text.Trim();
                string strCustomerAddress = txtCustomerAddress.Text.Trim();
                string strCustomerPhone = txtCustomerPhone.Text.Trim();
                string strCustomerIDCard = txtCustomerIDCard.Text.Trim();
                string strCustomerTaxID = txtTaxID.Text.Trim();
                int intPayableTypeID = Convert.ToInt32(cboPayableType.SelectedValue);
                string strTaxID = txtTaxID.Text.Trim();
                string strStaffUser = ctrlStaffUser.UserName;

                decimal decTotalAmountBF_In = 0;
                decimal decTotalAmountBF_Out = 0;
                decimal decTotalVAT_In = 0;
                decimal decTotalVAT_Out = 0;

                ERP.MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit objCurrencyUnit = new MasterData.PLC.MD.WSCurrencyUnit.CurrencyUnit();
                ERP.MasterData.PLC.MD.PLCCurrencyUnit objPLCCurrencyUnit = new MasterData.PLC.MD.PLCCurrencyUnit();
                objPLCCurrencyUnit.LoadInfo(ref objCurrencyUnit, intCurrencyUnitID);
                if (objCurrencyUnit != null)
                {
                    decCurrencyExchange = objCurrencyUnit.CurrencyExchange;
                }
                else
                {
                    DataTable tblCurrencyUnit = null;
                    objPLCCurrencyUnit.SearchData(ref tblCurrencyUnit, new object[] { });
                    if (tblCurrencyUnit != null && tblCurrencyUnit.Rows.Count > 0)
                    {
                        DataRow[] rCurrencyUnit = tblCurrencyUnit.Select("CurrencyUnitName = 'VND'");
                        if (rCurrencyUnit.Length > 0)
                        {
                            intCurrencyUnitID = Convert.ToInt32(rCurrencyUnit[0]["CurrencyUnitID"]);
                            decCurrencyExchange = Convert.ToDecimal(rCurrencyUnit[0]["CurrencyExchange"]);
                        }
                    }
                }

                ERP.Inventory.PLC.InputChange.WSInputChange.InputChange objInputChange = new PLC.InputChange.WSInputChange.InputChange();
                ERP.Inventory.PLC.InputChange.WSInputChange.InputVoucher objInputVoucher = new PLC.InputChange.WSInputChange.InputVoucher();
                ERP.Inventory.PLC.InputChange.WSInputChange.OutputVoucher objOutputVoucher = new PLC.InputChange.WSInputChange.OutputVoucher();

                objInputChange.OldOutputVoucherID = strOutputVoucherID;
                objInputChange.InputChangeStoreID = intStoreID;
                objInputChange.InputChangeDate = dtmInputDate;
                objInputChange.CreatedStoreID = intCreateStoreID;

                objInputVoucher.IsCheckRealInput = true;
                objInputVoucher.CheckRealInputUser = strUserName;
                objInputVoucher.CheckRealInputTime = dtmInputDate;
                objInputVoucher.InputTypeID = intInputTypeID;
                objInputVoucher.InputDate = dtmInputDate;
                objInputVoucher.PayableDate = dtmInputDate;
                objInputVoucher.InVoiceDate = dtInvoiceDate.Value;
                objInputVoucher.CustomerID = intCustomerID;
                objInputVoucher.CustomerName = strCustomerName;
                objInputVoucher.CustomerAddress = strCustomerAddress;
                objInputVoucher.CustomerPhone = strCustomerPhone;
                objInputVoucher.CustomerIDCard = strCustomerIDCard;
                objInputVoucher.CreatedStoreID = intCreateStoreID;
                objInputVoucher.InputStoreID = intStoreID;
                objInputVoucher.PayableTypeID = intPayableTypeID;
                objInputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objInputVoucher.CurrencyExchange = decCurrencyExchange;
                objInputChange.InputVoucherBO = objInputVoucher;

                objOutputVoucher.CreatedStoreID = intCreateStoreID;
                objOutputVoucher.OutputStoreID = intStoreID;
                objOutputVoucher.InvoiceDate = dtInvoiceDate.Value;
                objOutputVoucher.CustomerID = intCustomerID;
                objOutputVoucher.CustomerName = strCustomerName;
                objOutputVoucher.CustomerAddress = strCustomerAddress;
                objOutputVoucher.CustomerPhone = strCustomerPhone;
                objOutputVoucher.CustomerTaxID = strCustomerTaxID;
                objOutputVoucher.OutputContent = strContent;
                objOutputVoucher.OutputDate = dtmInputDate;
                objOutputVoucher.PayableTypeID = intPayableTypeID;
                objOutputVoucher.PayableDate = dtmInputDate;
                objOutputVoucher.StaffUser = strStaffUser;
                objOutputVoucher.CurrencyUnitID = intCurrencyUnitID;
                objOutputVoucher.CurrencyExchange = decCurrencyExchange;
                objInputChange.OutputVoucherBO = objOutputVoucher;

                List<PLC.InputChange.WSInputChange.InputVoucherDetail> objInputVoucherDetailList = new List<PLC.InputChange.WSInputChange.InputVoucherDetail>();
                List<PLC.InputChange.WSInputChange.OutputVoucherDetail> objOutputVoucherDetailList = new List<PLC.InputChange.WSInputChange.OutputVoucherDetail>();
                List<PLC.InputChange.WSInputChange.InputChangeDetail> objInputChangeDetailList = new List<PLC.InputChange.WSInputChange.InputChangeDetail>();

                dtbInputProduct.AcceptChanges();

                dtbInputProduct.DefaultView.Sort = "ProductID ASC";
                dtbInputProduct.DefaultView.RowFilter = "IsSelect = 1";
                dtbInputProduct = dtbInputProduct.DefaultView.ToTable();

                for (int i = 0; i < dtbInputProduct.Rows.Count; i++)
                {
                    DataRow rInput = dtbInputProduct.Rows[i];
                    string strOldOutputVoucherDetailID = Convert.ToString(rInput["OutputVoucherDetailID"]).Trim();
                    DataRow[] rOutput = dtbOutputProduct.Select("TRIM(OutputVoucherDetailID) = '" + strOldOutputVoucherDetailID + "'");
                    if (rOutput.Length > 0)
                    {
                        string strProductID = string.Empty;
                        string strIMEI_Out = string.Empty;
                        string strIMEI_In = string.Empty;
                        decimal decQuantity = 0;
                        int intVAT = 0;
                        int intVATPercent = 0;
                        decimal decSalePrice = 0;
                        decimal decCostPrice = 0;

                        if ((!Convert.IsDBNull(rOutput[0]["InVoiceID"])) && Convert.ToString(rOutput[0]["InVoiceID"]).Trim() != string.Empty) objInputVoucher.InVoiceID = Convert.ToString(rOutput[0]["InVoiceID"]).Trim();
                        if (!Convert.IsDBNull(rOutput[0]["InVoiceDate"])) objInputVoucher.InVoiceDate = Convert.ToDateTime(rOutput[0]["InVoiceDate"]);

                        strProductID = Convert.ToString(rOutput[0]["ProductID"]).Trim();
                        if (!Convert.IsDBNull(rInput["IMEI"])) strIMEI_In = Convert.ToString(rInput["IMEI"]).Trim();
                        if (!Convert.IsDBNull(rOutput[0]["IMEI"])) strIMEI_Out = Convert.ToString(rOutput[0]["IMEI"]).Trim();
                        decQuantity = Convert.ToDecimal(rOutput[0]["Quantity"]);
                        decSalePrice = Convert.ToDecimal(rOutput[0]["SalePrice"]);
                        decCostPrice = Convert.ToDecimal(rOutput[0]["CostPrice"]);
                        intVAT = Convert.ToInt32(rOutput[0]["VAT"]);
                        intVATPercent = Convert.ToInt32(rOutput[0]["VATPercent"]);
                        bool bolIsHasWarranty = Convert.ToBoolean(rOutput[0]["IsHasWarranty"]);
                        int intFirstInputTypeID = Convert.ToInt32(rOutput[0]["FirstInputTypeID"]);
                        bool bolIsShowProduct = Convert.ToBoolean(rOutput[0]["IsShowProduct"]);
                        bool bolIsNew = Convert.ToBoolean(rOutput[0]["IsNew"]);

                        decimal decPrice = decSalePrice * decQuantity;
                        decimal decPriceVAT = decSalePrice * decQuantity * (Convert.ToDecimal(intVAT) * Convert.ToDecimal(intVATPercent) * Convert.ToDecimal(0.0001));

                        decTotalAmountBF_In += decPrice;
                        decTotalAmountBF_Out += decPrice;
                        decTotalVAT_In += decPriceVAT;
                        decTotalVAT_Out += decPriceVAT;

                        #region InputChangeDetail
                        PLC.InputChange.WSInputChange.InputChangeDetail objInputChangeDetail = new PLC.InputChange.WSInputChange.InputChangeDetail();
                        objInputChangeDetail.OldOutputVoucherDetailID = strOldOutputVoucherDetailID;
                        objInputChangeDetail.InputChangeStoreID = intStoreID;
                        objInputChangeDetail.InputChangeDate = dtmInputDate;
                        objInputChangeDetail.ProductID_Out = strProductID;
                        objInputChangeDetail.ProductID_In = strProductID;
                        objInputChangeDetail.IMEI_Out = strIMEI_Out;
                        objInputChangeDetail.IMEI_In = strIMEI_In;
                        objInputChangeDetail.Quantity = decQuantity;
                        objInputChangeDetail.OutputTypeID = intOutputTypeID;
                        objInputChangeDetail.InputTypeID = intInputTypeID;
                        objInputChangeDetail.CreatedStoreID = intCreateStoreID;

                        objInputChangeDetailList.Add(objInputChangeDetail);
                        #endregion

                        #region InputVoucherDetail
                        if (objInputVoucherDetailList.Count > 0 && objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].ProductID == strProductID)
                        {
                            if (strIMEI_In != string.Empty)
                            {
                                PLC.InputChange.WSInputChange.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.InputChange.WSInputChange.InputVoucherDetailIMEI();
                                objInputVoucherDetailIMEI.IMEI = strIMEI_In;
                                objInputVoucherDetailIMEI.CreatedStoreID = intCreateStoreID;
                                objInputVoucherDetailIMEI.InputDate = dtmInputDate;
                                objInputVoucherDetailIMEI.InputStoreID = intStoreID;
                                objInputVoucherDetailIMEI.IsHasWarranty = bolIsHasWarranty;

                                List<PLC.InputChange.WSInputChange.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = new List<PLC.InputChange.WSInputChange.InputVoucherDetailIMEI>();
                                if (objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList.Length > 0)
                                {
                                    objInputVoucherDetailIMEIList.AddRange(objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList);
                                }
                                objInputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                                objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList = objInputVoucherDetailIMEIList.ToArray();
                                objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].Quantity = Convert.ToDecimal(objInputVoucherDetailList[objInputVoucherDetailList.Count - 1].InputVoucherDetailIMEIList.Length);
                            }
                        }
                        else
                        {
                            PLC.InputChange.WSInputChange.InputVoucherDetail objInputVoucherDetail = new PLC.InputChange.WSInputChange.InputVoucherDetail();
                            objInputVoucherDetail.ProductID = strProductID;
                            objInputVoucherDetail.Quantity = decQuantity;
                            objInputVoucherDetail.VAT = intVAT;
                            objInputVoucherDetail.VATPercent = intVATPercent;
                            objInputVoucherDetail.InputDate = dtmInputDate;
                            objInputVoucherDetail.CreatedStoreID = intCreateStoreID;
                            objInputVoucherDetail.FirstInputTypeID = intFirstInputTypeID;
                            objInputVoucherDetail.InputStoreID = intStoreID;
                            objInputVoucherDetail.IsNew = bolIsNew;
                            objInputVoucherDetail.CostPrice = decCostPrice;

                            if (!Convert.IsDBNull(rOutput[0]["FirstInputDate"])) objInputVoucherDetail.FirstInputDate = Convert.ToDateTime(rOutput[0]["FirstInputDate"]);
                            if (!Convert.IsDBNull(rOutput[0]["FirstInVoiceDate"])) objInputVoucherDetail.FirstInVoiceDate = Convert.ToDateTime(rOutput[0]["FirstInVoiceDate"]);
                            if (!Convert.IsDBNull(rOutput[0]["FirstCustomerID"])) objInputVoucherDetail.FirstCustomerID = Convert.ToInt32(rOutput[0]["FirstCustomerID"]);
                            if (!Convert.IsDBNull(rOutput[0]["FirstInputVoucherID"])) objInputVoucherDetail.FirstInputVoucherID = Convert.ToString(rOutput[0]["FirstInputVoucherID"]);
                            if (!Convert.IsDBNull(rOutput[0]["FirstInputVoucherDetailID"])) objInputVoucherDetail.FirstInputVoucherDetailID = Convert.ToString(rOutput[0]["FirstInputVoucherDetailID"]);

                            if (objInputVoucherDetail.FirstInputVoucherDetailID != null && Convert.ToString(objInputVoucherDetail.FirstInputVoucherDetailID).Trim() != string.Empty)
                            {
                                DataRow[] drFirstPrice = dtbFirstPriceInputVoucherDetail.Select("TRIM(InputVoucherDetailID)='" + Convert.ToString(objInputVoucherDetail.FirstInputVoucherDetailID).Trim() + "'");
                                if (drFirstPrice.Length > 0)
                                {
                                    objInputVoucherDetail.FirstPrice = Convert.ToDecimal(drFirstPrice[0]["FirstPrice"]);
                                }
                            }

                            int intMonthOfWarranty = ERP.MasterData.PLC.MD.PLCWarrantyMonthByProduct.GetWarrantyMonthByProduct(objInputVoucherDetail.ProductID, objInputVoucherDetail.IsNew);
                            objInputVoucherDetail.ENDWarrantyDate = Library.AppCore.Globals.GetServerDateTime().AddMonths(intMonthOfWarranty);

                            if (strIMEI_In != string.Empty)
                            {
                                PLC.InputChange.WSInputChange.InputVoucherDetailIMEI objInputVoucherDetailIMEI = new PLC.InputChange.WSInputChange.InputVoucherDetailIMEI();
                                objInputVoucherDetailIMEI.IMEI = strIMEI_In;
                                objInputVoucherDetailIMEI.CreatedStoreID = intCreateStoreID;
                                objInputVoucherDetailIMEI.InputDate = dtmInputDate;
                                objInputVoucherDetailIMEI.InputStoreID = intStoreID;
                                objInputVoucherDetailIMEI.IsHasWarranty = bolIsHasWarranty;

                                List<PLC.InputChange.WSInputChange.InputVoucherDetailIMEI> objInputVoucherDetailIMEIList = new List<PLC.InputChange.WSInputChange.InputVoucherDetailIMEI>();
                                objInputVoucherDetailIMEIList.Add(objInputVoucherDetailIMEI);
                                objInputVoucherDetail.InputVoucherDetailIMEIList = objInputVoucherDetailIMEIList.ToArray();
                                objInputVoucherDetail.Quantity = Convert.ToDecimal(objInputVoucherDetail.InputVoucherDetailIMEIList.Length);
                            }
                            objInputVoucherDetailList.Add(objInputVoucherDetail);
                        }
                        #endregion

                        #region OutputVoucherDetail
                        PLC.InputChange.WSInputChange.OutputVoucherDetail objOutputVoucherDetail = new PLC.InputChange.WSInputChange.OutputVoucherDetail();
                        objOutputVoucherDetail.ProductID = strProductID;
                        objOutputVoucherDetail.IMEI = strIMEI_Out;
                        objOutputVoucherDetail.IsNew = bolIsNew;
                        objOutputVoucherDetail.Quantity = decQuantity;
                        objOutputVoucherDetail.VAT = intVAT;
                        objOutputVoucherDetail.VATPercent = intVATPercent;
                        objOutputVoucherDetail.SalePrice = decSalePrice;
                        objOutputVoucherDetail.CostPrice = decCostPrice;
                        objOutputVoucherDetail.CreatedStoreID = intCreateStoreID;
                        objOutputVoucherDetail.OutputStoreID = intStoreID;
                        objOutputVoucherDetail.OutputTypeID = intOutputTypeID;
                        objOutputVoucherDetail.OutputDate = dtmInputDate;
                        objOutputVoucherDetail.InputChangeDate = dtmInputDate;

                        objOutputVoucherDetailList.Add(objOutputVoucherDetail);
                        #endregion

                    }
                }

                objInputChange.InputVoucherBO.TotalAmountBFT = decTotalAmountBF_In;
                objInputChange.InputVoucherBO.TotalVAT = decTotalVAT_In;
                objInputChange.InputVoucherBO.TotalAmount = decTotalAmountBF_In + decTotalVAT_In;
                objInputChange.OutputVoucherBO.TotalAmountBFT = decTotalAmountBF_Out;
                objInputChange.OutputVoucherBO.TotalVAT = decTotalVAT_Out;
                objInputChange.OutputVoucherBO.TotalAmount = decTotalAmountBF_Out + decTotalVAT_Out;

                objInputChange.InputVoucherBO.IsNew = objInputVoucherDetailList[0].IsNew;
                objInputChange.InputChangeDetailList = objInputChangeDetailList.ToArray();
                objInputChange.InputVoucherBO.InputVoucherDetailList = objInputVoucherDetailList.ToArray();
                objInputChange.OutputVoucherBO.OutputVoucherDetailList = objOutputVoucherDetailList.ToArray();

                if (!objPLCInputChange.InsertMasterAndDetail(objInputChange))
                {
                    btnInputChange.Enabled = true;
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                }

                MessageBox.Show(this, "Nhập đổi hàng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception objEx)
            {
                btnInputChange.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nhập đổi hàng", objEx.Message + System.Environment.NewLine + objEx.StackTrace);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void flexInputProduct_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexInputProduct.Rows.Count <= flexInputProduct.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexInputProduct.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexInputProduct.Cols["IsSelect"].Index)
            {
                dtbInputProduct.AcceptChanges();

                string strProductID = Convert.ToString(flexInputProduct[e.Row, "ProductID"]).Trim();
                string strIMEI = Convert.ToString(flexInputProduct[e.Row, "IMEI"]).Trim();

                if (Convert.ToBoolean(flexInputProduct[e.Row, "IsSelect"]))
                {
                    if (strIMEI != string.Empty)
                    {
                        DataRow[] drInStock = dtbStoreInStock.Select("TRIM(ProductID) = '" + strProductID + "' AND TRIM(IMEI) = '" + strIMEI + "'");
                        if (drInStock.Length > 0)
                        {
                            ERP.MasterData.PLC.MD.WSStore.Store objStoreInStock = null;
                            ERP.MasterData.PLC.MD.PLCStore objPLCStore = new MasterData.PLC.MD.PLCStore();
                            objPLCStore.LoadInfo(ref objStoreInStock, Convert.ToInt32(drInStock[0]["StoreID"]));
                            MessageBox.Show(this, "IMEI " + Convert.ToString(flexInputProduct[e.Row, "IMEI"]).Trim() + " này đã tồn tại trên kho " + objStoreInStock.StoreName + ". Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexInputProduct[e.Row, "IsSelect"] = false;
                            return;
                        }
                    }
                    DataRow[] rExits = dtbOutputProduct.Select("TRIM(ProductID) = '" + strProductID + "' AND TRIM(IMEI) = '" + strIMEI + "'");
                    if (rExits.Length < 1)
                    {
                        DataRow rOutputPrd = dtbOutputProduct.NewRow();
                        rOutputPrd.ItemArray = dtbInputProduct.Rows[e.Row - flexInputProduct.Rows.Fixed].ItemArray;
                        rOutputPrd["IMEI"] = string.Empty;
                        dtbOutputProduct.Rows.Add(rOutputPrd);
                    }
                }
                else
                {
                    DataRow[] rIsSelect = dtbInputProduct.Select("IsSelect = 1");
                    if (rIsSelect.Length == 0)
                    {
                        dtbOutputProduct.Rows.Clear();
                    }
                    else
                    {
                        for (int i = dtbOutputProduct.Rows.Count - 1; i > -1; i--)
                        {
                            if (Convert.ToString(dtbOutputProduct.Rows[i]["ProductID"]).Trim() == strProductID)
                            {
                                if (rIsSelect.Length < dtbOutputProduct.Rows.Count)
                                {
                                    dtbOutputProduct.Rows.RemoveAt(i);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void flexOutputProduct_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexOutputProduct.Rows.Count <= flexOutputProduct.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexOutputProduct.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexOutputProduct.Cols["IMEI"].Index)
            {
                string strProductID = Convert.ToString(flexOutputProduct[e.Row, "ProductID"]).Trim();
                string strBarcode = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(Convert.ToString(flexOutputProduct[e.Row, "IMEI"]).Trim());
                if (strBarcode != string.Empty)
                {
                    string strStoreNameIMEIInStock = string.Empty;
                    DataRow[] rNotStoreIMEIOutput = dtbStoreInStock.Select("TRIM(ProductID) = '" + strProductID + "' AND TRIM(IMEI) = '" + strBarcode + "'");
                    if (rNotStoreIMEIOutput.Length > 0)
                    {
                        DataTable tblStoreTMP = (DataTable)cboStore.DataSource;
                        DataRow[] drStoreIMEIInStock = tblStoreTMP.Select("StoreID = " + Convert.ToInt32(rNotStoreIMEIOutput[0]["StoreID"]));
                        if (drStoreIMEIInStock.Length > 0)
                            strStoreNameIMEIInStock = Convert.ToString(drStoreIMEIInStock[0]["StoreName"]).Trim();
                    }

                    if (cboStore.StoreID < 1)
                    {
                        MessageBox.Show(this, (strStoreNameIMEIInStock != string.Empty ? ("IMEI này tồn kho ở " + strStoreNameIMEIInStock + ". ") : string.Empty) + "Vui lòng chọn kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexOutputProduct[e.Row, "IMEI"] = string.Empty;
                        cboStore.Focus();
                        return;
                    }

                    flexOutputProduct[e.Row, "IMEI"] = strBarcode;
                    DataRow[] rOutputIMEI = dtbStoreInStock.Select("StoreID = " + cboStore.StoreID + " AND TRIM(ProductID) = '" + strProductID + "' AND TRIM(IMEI) = '" + strBarcode + "'");
                    if (rOutputIMEI.Length < 1)
                    {
                        MessageBox.Show(this, "IMEI " + strBarcode + " không tồn trên kho " + cboStore.StoreName + ". " + (strStoreNameIMEIInStock != string.Empty ? ("IMEI này tồn kho ở " + strStoreNameIMEIInStock + ". ") : string.Empty) + "Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexOutputProduct[e.Row, "IMEI"] = string.Empty;
                        return;
                    }
                    else
                    {
                        int intIsNew_IMEIOut = Convert.ToInt32(rOutputIMEI[0]["IsNew"]);
                        string strOutputVoucherDetailID = Convert.ToString(flexOutputProduct[e.Row, "OutputVoucherDetailID"]).Trim();
                        for (int i = 0; i < dtbInputProduct.Rows.Count; i++)
                        {
                            if (strOutputVoucherDetailID == Convert.ToString(dtbInputProduct.Rows[i]["OutputVoucherDetailID"]).Trim())
                            {
                                if (Convert.ToInt32(dtbInputProduct.Rows[i]["IsNew"]) != intIsNew_IMEIOut)
                                {
                                    MessageBox.Show(this, "Không thể nhập đổi IMEI " + Convert.ToString(dtbInputProduct.Rows[i]["IMEI"]).Trim() + (intIsNew_IMEIOut == 1 ? " cũ" : " mới") + " với IMEI " + strBarcode + (intIsNew_IMEIOut == 1 ? " mới" : " cũ") + ". Vui lòng nhập lại IMEI khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    flexOutputProduct[e.Row, "IMEI"] = string.Empty;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            else if (e.Col == flexOutputProduct.Cols["Quantity"].Index)
            {
                DataRow[] drQuantityInput = dtbInputProduct.Select("TRIM(OutputVoucherDetailID) = '" + Convert.ToString(flexOutputProduct[e.Row, "OutputVoucherDetailID"]).Trim() + "'");
                decimal decQuantity = Convert.ToDecimal(flexOutputProduct[e.Row, "Quantity"]);
                if (decQuantity > Convert.ToDecimal(drQuantityInput[0]["Quantity"]))
                {
                    flexOutputProduct[e.Row, "Quantity"] = Convert.ToDecimal(drQuantityInput[0]["Quantity"]);
                }
                else
                {
                    if (decQuantity < 0)
                        flexOutputProduct[e.Row, "Quantity"] = Math.Abs(decQuantity);
                }
            }
        }

        private void flexOutputProduct_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexOutputProduct.Rows.Count <= flexOutputProduct.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexOutputProduct.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexOutputProduct.Cols["IMEI"].Index)
            {
                string strProductID = Convert.ToString(flexOutputProduct[e.Row, "ProductID"]).Trim();
                DataRow[] r = dtbInputProduct.Select("ProductID = '" + strProductID + "'");
                if (r.Length > 0)
                {
                    if (Convert.IsDBNull(r[0]["IMEI"]) || Convert.ToString(r[0]["IMEI"]).Trim() == string.Empty)
                    {
                        e.Cancel = true;
                    }
                }
            }
            else if (e.Col == flexOutputProduct.Cols["Quantity"].Index)
            {
                if (Convert.ToBoolean(flexOutputProduct[e.Row, "IsRequestIMEI"]))
                {
                    e.Cancel = true;
                }
            }
        }

        #endregion

        private void lnkCustomer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ERP.MasterData.DUI.Search.frmCustomerSearch frmCustomerSearch1 = new MasterData.DUI.Search.frmCustomerSearch();
            frmCustomerSearch1.ShowDialog();
            if (frmCustomerSearch1.Customer != null && frmCustomerSearch1.Customer.CustomerID > 0)
            {
                txtCustomerName.Text = frmCustomerSearch1.Customer.CustomerName;
                txtCustomerAddress.Text = frmCustomerSearch1.Customer.CustomerAddress;
                txtTaxID.Text = frmCustomerSearch1.Customer.CustomerTaxID;
                txtCustomerPhone.Text = frmCustomerSearch1.Customer.CustomerPhone;
                txtCustomerIDCard.Text = frmCustomerSearch1.Customer.CustomerIDCard;
            }
            else
            {
                if (Convert.ToString(txtCustomerID.Text).Trim() != string.Empty)
                {
                    if (MessageBox.Show(this, "Bạn muốn bỏ chọn khách hàng hiện tại không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                }
                txtCustomerName.Text = string.Empty;
                txtCustomerAddress.Text = string.Empty;
                txtTaxID.Text = string.Empty;
                txtCustomerPhone.Text = string.Empty;
                txtCustomerIDCard.Text = string.Empty;
            }
        }

        private void flexOutputProduct_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexOutputProduct.Rows.Count <= flexOutputProduct.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexOutputProduct.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexOutputProduct.Cols["IMEI"].Index)
            {
                TextBox txt = (TextBox)flexOutputProduct.Editor;
                txt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            }
        }

    }
}
