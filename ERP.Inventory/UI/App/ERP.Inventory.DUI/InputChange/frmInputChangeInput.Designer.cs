﻿namespace ERP.Inventory.DUI.InputChange
{
    partial class frmInputChangeInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInputChangeInput));
            this.label1 = new System.Windows.Forms.Label();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtOutputVoucherID = new System.Windows.Forms.TextBox();
            this.cmdInputChange = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã phiếu xuất:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtOutputVoucherID);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Location = new System.Drawing.Point(6, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(340, 76);
            this.groupControl1.TabIndex = 1;
            // 
            // txtOutputVoucherID
            // 
            this.txtOutputVoucherID.Location = new System.Drawing.Point(105, 39);
            this.txtOutputVoucherID.Name = "txtOutputVoucherID";
            this.txtOutputVoucherID.Size = new System.Drawing.Size(226, 22);
            this.txtOutputVoucherID.TabIndex = 1;
            // 
            // cmdInputChange
            // 
            this.cmdInputChange.Image = ((System.Drawing.Image)(resources.GetObject("cmdInputChange.Image")));
            this.cmdInputChange.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdInputChange.Location = new System.Drawing.Point(221, 86);
            this.cmdInputChange.Name = "cmdInputChange";
            this.cmdInputChange.Size = new System.Drawing.Size(125, 25);
            this.cmdInputChange.TabIndex = 2;
            this.cmdInputChange.Text = "      Nhập đổi hàng";
            this.cmdInputChange.UseVisualStyleBackColor = true;
            this.cmdInputChange.Click += new System.EventHandler(this.cmdInputChange_Click);
            // 
            // frmInputChangeInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 116);
            this.Controls.Add(this.cmdInputChange);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInputChangeInput";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập đổi hàng";
            this.Load += new System.EventHandler(this.frmInputChangeInput_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TextBox txtOutputVoucherID;
        private System.Windows.Forms.Button cmdInputChange;
    }
}