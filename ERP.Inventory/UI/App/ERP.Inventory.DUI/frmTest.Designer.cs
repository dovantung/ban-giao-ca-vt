﻿namespace ERP.Inventory.DUI
{
    partial class frmTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTest));
            this.btnLogin = new DevExpress.XtraEditors.SimpleButton();
            this.txtUser = new DevExpress.XtraEditors.TextEdit();
            this.txtPass = new DevExpress.XtraEditors.TextEdit();
            this.btnInput = new DevExpress.XtraEditors.SimpleButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnStoreChangeOrder = new DevExpress.XtraEditors.SimpleButton();
            this.cboSC = new System.Windows.Forms.ComboBox();
            this.txtOutputVoucher = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnOutput = new System.Windows.Forms.Button();
            this.cboOutput = new System.Windows.Forms.ComboBox();
            this.btnPrChange = new System.Windows.Forms.Button();
            this.cboPrChange = new System.Windows.Forms.ComboBox();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.radNew = new System.Windows.Forms.RadioButton();
            this.radOld = new System.Windows.Forms.RadioButton();
            this.radOutput = new System.Windows.Forms.RadioButton();
            this.radInput = new System.Windows.Forms.RadioButton();
            this.btnBarcode = new System.Windows.Forms.Button();
            this.cboBarcode = new System.Windows.Forms.ComboBox();
            this.btnInputChange = new System.Windows.Forms.Button();
            this.cboInputChange = new System.Windows.Forms.ComboBox();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.cboStoreChangeCommand = new System.Windows.Forms.ComboBox();
            this.ucUserQuickSearch1 = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.cboBeginTermInStock = new System.Windows.Forms.ComboBox();
            this.btnBeginTermInStock = new DevExpress.XtraEditors.SimpleButton();
            this.cachedrptInputVoucherReport1 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            this.txtStoreChangeOrderTypeID = new DevExpress.XtraEditors.TextEdit();
            this.btnInventory = new System.Windows.Forms.Button();
            this.cboInventory = new System.Windows.Forms.ComboBox();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.btnWarranty = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnQuanLyXuatChuyenKho = new System.Windows.Forms.Button();
            this.btnQuanLyYeuCauChuyenKho = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.cboPM = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnInputChangeOrderInput = new System.Windows.Forms.Button();
            this.btnInputChangeOrderManager = new System.Windows.Forms.Button();
            this.butbtnInputChangeOrderInputton5 = new System.Windows.Forms.Button();
            this.cboIMEI = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.btnSplitProduct = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabDetail = new DevExpress.XtraTab.XtraTabControl();
            this.tabPagePayment = new DevExpress.XtraTab.XtraTabPage();
            this.tabPagePaymentHistory = new DevExpress.XtraTab.XtraTabPage();
            this.flexSaleOrderDetail = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.txtTotalAmount = new C1.Win.C1Input.C1NumericEdit();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.c1XLBook1 = new C1.C1Excel.C1XLBook();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.cachedrptPrintProductQRCode_211 = new ERP.Inventory.DUI.Barcode.CachedrptPrintProductQRCode_21();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.txtProductTypeID = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOutputVoucher.Properties)).BeginInit();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreChangeOrderTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabDetail)).BeginInit();
            this.tabDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexSaleOrderDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductTypeID.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(366, 48);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(243, 16);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(117, 20);
            this.txtUser.TabIndex = 1;
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(243, 51);
            this.txtPass.Name = "txtPass";
            this.txtPass.Properties.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(117, 20);
            this.txtPass.TabIndex = 1;
            // 
            // btnInput
            // 
            this.btnInput.Location = new System.Drawing.Point(429, 91);
            this.btnInput.Name = "btnInput";
            this.btnInput.Size = new System.Drawing.Size(75, 23);
            this.btnInput.TabIndex = 0;
            this.btnInput.Text = "Input";
            this.btnInput.Click += new System.EventHandler(this.btnInput_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "frmInputVoucher",
            "frmInputVoucherManager",
            "frmProductIMEIHistory",
            "frmBorrowProduct",
            "frmBorrowProduct_Update"});
            this.comboBox1.Location = new System.Drawing.Point(243, 91);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(180, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // btnStoreChangeOrder
            // 
            this.btnStoreChangeOrder.Location = new System.Drawing.Point(429, 118);
            this.btnStoreChangeOrder.Name = "btnStoreChangeOrder";
            this.btnStoreChangeOrder.Size = new System.Drawing.Size(106, 23);
            this.btnStoreChangeOrder.TabIndex = 0;
            this.btnStoreChangeOrder.Text = "StoreChange";
            this.btnStoreChangeOrder.Click += new System.EventHandler(this.btnStoreChangeOrder_Click);
            // 
            // cboSC
            // 
            this.cboSC.FormattingEnabled = true;
            this.cboSC.Items.AddRange(new object[] {
            "frmStoreChangeOrderManager",
            "frmStoreChangeOrder",
            "frmStoreChange",
            "frmStoreChangeManager",
            "frmStoreChangeManagerFix"});
            this.cboSC.Location = new System.Drawing.Point(243, 118);
            this.cboSC.Name = "cboSC";
            this.cboSC.Size = new System.Drawing.Size(180, 21);
            this.cboSC.TabIndex = 2;
            // 
            // txtOutputVoucher
            // 
            this.txtOutputVoucher.Location = new System.Drawing.Point(550, 218);
            this.txtOutputVoucher.Name = "txtOutputVoucher";
            this.txtOutputVoucher.Size = new System.Drawing.Size(117, 20);
            this.txtOutputVoucher.TabIndex = 3;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(688, 221);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(104, 23);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "Nhập trả hàng";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnOutput
            // 
            this.btnOutput.Location = new System.Drawing.Point(429, 176);
            this.btnOutput.Name = "btnOutput";
            this.btnOutput.Size = new System.Drawing.Size(75, 23);
            this.btnOutput.TabIndex = 5;
            this.btnOutput.Text = "Output";
            this.btnOutput.UseVisualStyleBackColor = true;
            this.btnOutput.Click += new System.EventHandler(this.btnOutput_Click);
            // 
            // cboOutput
            // 
            this.cboOutput.FormattingEnabled = true;
            this.cboOutput.Items.AddRange(new object[] {
            "frmOutputVoucherManager"});
            this.cboOutput.Location = new System.Drawing.Point(243, 176);
            this.cboOutput.Name = "cboOutput";
            this.cboOutput.Size = new System.Drawing.Size(180, 21);
            this.cboOutput.TabIndex = 6;
            // 
            // btnPrChange
            // 
            this.btnPrChange.Location = new System.Drawing.Point(429, 208);
            this.btnPrChange.Name = "btnPrChange";
            this.btnPrChange.Size = new System.Drawing.Size(106, 23);
            this.btnPrChange.TabIndex = 7;
            this.btnPrChange.Text = "ProductChange";
            this.btnPrChange.UseVisualStyleBackColor = true;
            this.btnPrChange.Click += new System.EventHandler(this.btnPrChange_Click);
            // 
            // cboPrChange
            // 
            this.cboPrChange.FormattingEnabled = true;
            this.cboPrChange.Items.AddRange(new object[] {
            "frmProductChangeOrder",
            "frmProductChange",
            "frmProductChangeOrderManagement"});
            this.cboPrChange.Location = new System.Drawing.Point(243, 209);
            this.cboPrChange.Name = "cboPrChange";
            this.cboPrChange.Size = new System.Drawing.Size(180, 21);
            this.cboPrChange.TabIndex = 8;
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.radNew);
            this.GroupBox2.Controls.Add(this.radOld);
            this.GroupBox2.Location = new System.Drawing.Point(674, 377);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(126, 46);
            this.GroupBox2.TabIndex = 42;
            this.GroupBox2.TabStop = false;
            // 
            // radNew
            // 
            this.radNew.AutoSize = true;
            this.radNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNew.ForeColor = System.Drawing.Color.Red;
            this.radNew.Location = new System.Drawing.Point(10, 13);
            this.radNew.Name = "radNew";
            this.radNew.Size = new System.Drawing.Size(51, 20);
            this.radNew.TabIndex = 27;
            this.radNew.TabStop = true;
            this.radNew.Text = "Mới";
            this.radNew.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radNew.UseVisualStyleBackColor = true;
            // 
            // radOld
            // 
            this.radOld.AutoSize = true;
            this.radOld.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radOld.ForeColor = System.Drawing.Color.Red;
            this.radOld.Location = new System.Drawing.Point(67, 13);
            this.radOld.Name = "radOld";
            this.radOld.Size = new System.Drawing.Size(44, 20);
            this.radOld.TabIndex = 28;
            this.radOld.TabStop = true;
            this.radOld.Text = "Cũ";
            this.radOld.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radOld.UseVisualStyleBackColor = true;
            // 
            // radOutput
            // 
            this.radOutput.AutoSize = true;
            this.radOutput.Location = new System.Drawing.Point(562, 390);
            this.radOutput.Name = "radOutput";
            this.radOutput.Size = new System.Drawing.Size(47, 17);
            this.radOutput.TabIndex = 40;
            this.radOutput.TabStop = true;
            this.radOutput.Text = "Xuất";
            this.radOutput.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radOutput.UseVisualStyleBackColor = true;
            // 
            // radInput
            // 
            this.radInput.AutoSize = true;
            this.radInput.Location = new System.Drawing.Point(622, 390);
            this.radInput.Name = "radInput";
            this.radInput.Size = new System.Drawing.Size(51, 17);
            this.radInput.TabIndex = 41;
            this.radInput.TabStop = true;
            this.radInput.Text = "Nhập";
            this.radInput.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radInput.UseVisualStyleBackColor = true;
            // 
            // btnBarcode
            // 
            this.btnBarcode.Location = new System.Drawing.Point(429, 237);
            this.btnBarcode.Name = "btnBarcode";
            this.btnBarcode.Size = new System.Drawing.Size(106, 23);
            this.btnBarcode.TabIndex = 7;
            this.btnBarcode.Text = "Barcode";
            this.btnBarcode.UseVisualStyleBackColor = true;
            this.btnBarcode.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // cboBarcode
            // 
            this.cboBarcode.FormattingEnabled = true;
            this.cboBarcode.Items.AddRange(new object[] {
            "frmPrintProductBarcode"});
            this.cboBarcode.Location = new System.Drawing.Point(243, 238);
            this.cboBarcode.Name = "cboBarcode";
            this.cboBarcode.Size = new System.Drawing.Size(180, 21);
            this.cboBarcode.TabIndex = 8;
            // 
            // btnInputChange
            // 
            this.btnInputChange.Location = new System.Drawing.Point(429, 268);
            this.btnInputChange.Name = "btnInputChange";
            this.btnInputChange.Size = new System.Drawing.Size(106, 23);
            this.btnInputChange.TabIndex = 43;
            this.btnInputChange.Text = "InputChange";
            this.btnInputChange.UseVisualStyleBackColor = true;
            this.btnInputChange.Click += new System.EventHandler(this.btnInputChange_Click);
            // 
            // cboInputChange
            // 
            this.cboInputChange.FormattingEnabled = true;
            this.cboInputChange.Items.AddRange(new object[] {
            "frmInputChange"});
            this.cboInputChange.Location = new System.Drawing.Point(243, 269);
            this.cboInputChange.Name = "cboInputChange";
            this.cboInputChange.Size = new System.Drawing.Size(180, 21);
            this.cboInputChange.TabIndex = 44;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(429, 147);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(130, 23);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "StoreChangeCommand";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // cboStoreChangeCommand
            // 
            this.cboStoreChangeCommand.FormattingEnabled = true;
            this.cboStoreChangeCommand.Items.AddRange(new object[] {
            "frmAutoDistributeInventory",
            "frmAutoSplitProduct",
            "frmAutoStoreChange",
            "frmAutoStoreChangeDetail",
            "frmStoreChangeCommandManager",
            "frmStoreChangeShowProduct",
            "frmShowProductManager",
            "frmAutoStoreChangeDetailIMEI",
            "frmStoreChangeDistribute"});
            this.cboStoreChangeCommand.Location = new System.Drawing.Point(243, 147);
            this.cboStoreChangeCommand.Name = "cboStoreChangeCommand";
            this.cboStoreChangeCommand.Size = new System.Drawing.Size(180, 21);
            this.cboStoreChangeCommand.TabIndex = 2;
            // 
            // ucUserQuickSearch1
            // 
            this.ucUserQuickSearch1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucUserQuickSearch1.IsOnlyShowRealStaff = false;
            this.ucUserQuickSearch1.IsValidate = true;
            this.ucUserQuickSearch1.Location = new System.Drawing.Point(508, 33);
            this.ucUserQuickSearch1.Margin = new System.Windows.Forms.Padding(0);
            this.ucUserQuickSearch1.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucUserQuickSearch1.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucUserQuickSearch1.Name = "ucUserQuickSearch1";
            this.ucUserQuickSearch1.Size = new System.Drawing.Size(284, 22);
            this.ucUserQuickSearch1.TabIndex = 45;
            this.ucUserQuickSearch1.UserName = "";
            this.ucUserQuickSearch1.WorkingPositionID = 0;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(610, 136);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(139, 23);
            this.simpleButton3.TabIndex = 46;
            this.simpleButton3.Text = "QL định giá máy mua lẻ";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // cboBeginTermInStock
            // 
            this.cboBeginTermInStock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBeginTermInStock.FormattingEnabled = true;
            this.cboBeginTermInStock.Items.AddRange(new object[] {
            "frmCumulationTimeOffCal",
            "frmBeginTermInStock",
            "frmBeginTermMoneyCal",
            "frmSearchBeginTermMoney"});
            this.cboBeginTermInStock.Location = new System.Drawing.Point(531, 89);
            this.cboBeginTermInStock.Name = "cboBeginTermInStock";
            this.cboBeginTermInStock.Size = new System.Drawing.Size(180, 21);
            this.cboBeginTermInStock.TabIndex = 48;
            // 
            // btnBeginTermInStock
            // 
            this.btnBeginTermInStock.Location = new System.Drawing.Point(717, 89);
            this.btnBeginTermInStock.Name = "btnBeginTermInStock";
            this.btnBeginTermInStock.Size = new System.Drawing.Size(112, 23);
            this.btnBeginTermInStock.TabIndex = 47;
            this.btnBeginTermInStock.Text = "BeginTermInStock";
            this.btnBeginTermInStock.Click += new System.EventHandler(this.btnBeginTermInStock_Click);
            // 
            // txtStoreChangeOrderTypeID
            // 
            this.txtStoreChangeOrderTypeID.EditValue = "StoreChangeOrderTypeID=1";
            this.txtStoreChangeOrderTypeID.Location = new System.Drawing.Point(59, 147);
            this.txtStoreChangeOrderTypeID.Name = "txtStoreChangeOrderTypeID";
            this.txtStoreChangeOrderTypeID.Size = new System.Drawing.Size(178, 20);
            this.txtStoreChangeOrderTypeID.TabIndex = 3;
            // 
            // btnInventory
            // 
            this.btnInventory.Location = new System.Drawing.Point(429, 297);
            this.btnInventory.Name = "btnInventory";
            this.btnInventory.Size = new System.Drawing.Size(106, 23);
            this.btnInventory.TabIndex = 49;
            this.btnInventory.Text = "Inventory";
            this.btnInventory.UseVisualStyleBackColor = true;
            this.btnInventory.Click += new System.EventHandler(this.btnInventory_Click);
            // 
            // cboInventory
            // 
            this.cboInventory.FormattingEnabled = true;
            this.cboInventory.Items.AddRange(new object[] {
            "frmInventorySuperJoin",
            "frmCreateQuickInventory",
            "AverageMonth.frmAverageMonthList",
            "frmInventoryTerm",
            "frmInventoryInput",
            "frmInventoryManager",
            "frmResolveManager",
            "frmQuickInventorySearch",
            "frmInventoryJoin",
            "frmInventory",
            "frmInventoryDifferenceResult"});
            this.cboInventory.Location = new System.Drawing.Point(243, 297);
            this.cboInventory.Name = "cboInventory";
            this.cboInventory.Size = new System.Drawing.Size(180, 21);
            this.cboInventory.TabIndex = 50;
            this.cboInventory.SelectedIndexChanged += new System.EventHandler(this.cboInventory_SelectedIndexChanged);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(698, 250);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(104, 23);
            this.simpleButton4.TabIndex = 4;
            this.simpleButton4.Text = "Lịch sử imei";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // btnWarranty
            // 
            this.btnWarranty.Location = new System.Drawing.Point(429, 326);
            this.btnWarranty.Name = "btnWarranty";
            this.btnWarranty.Size = new System.Drawing.Size(106, 23);
            this.btnWarranty.TabIndex = 49;
            this.btnWarranty.Text = "Warranty";
            this.btnWarranty.UseVisualStyleBackColor = true;
            this.btnWarranty.Click += new System.EventHandler(this.btnWarranty_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "frmWarrantyInfo.cs",
            "frmWarrantyRegisterManager.cs"});
            this.comboBox2.Location = new System.Drawing.Point(243, 326);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(180, 21);
            this.comboBox2.TabIndex = 50;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btnQuanLyXuatChuyenKho);
            this.groupControl1.Controls.Add(this.btnQuanLyYeuCauChuyenKho);
            this.groupControl1.Location = new System.Drawing.Point(840, 12);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(261, 147);
            this.groupControl1.TabIndex = 51;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            // 
            // btnQuanLyXuatChuyenKho
            // 
            this.btnQuanLyXuatChuyenKho.BackColor = System.Drawing.Color.MistyRose;
            this.btnQuanLyXuatChuyenKho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuanLyXuatChuyenKho.Location = new System.Drawing.Point(6, 55);
            this.btnQuanLyXuatChuyenKho.Name = "btnQuanLyXuatChuyenKho";
            this.btnQuanLyXuatChuyenKho.Size = new System.Drawing.Size(250, 23);
            this.btnQuanLyXuatChuyenKho.TabIndex = 73;
            this.btnQuanLyXuatChuyenKho.Text = "Quản lý xuất chuyển kho";
            this.btnQuanLyXuatChuyenKho.UseVisualStyleBackColor = false;
            this.btnQuanLyXuatChuyenKho.Click += new System.EventHandler(this.btnQuanLyXuatChuyenKho_Click);
            // 
            // btnQuanLyYeuCauChuyenKho
            // 
            this.btnQuanLyYeuCauChuyenKho.BackColor = System.Drawing.Color.MistyRose;
            this.btnQuanLyYeuCauChuyenKho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuanLyYeuCauChuyenKho.Location = new System.Drawing.Point(6, 26);
            this.btnQuanLyYeuCauChuyenKho.Name = "btnQuanLyYeuCauChuyenKho";
            this.btnQuanLyYeuCauChuyenKho.Size = new System.Drawing.Size(250, 23);
            this.btnQuanLyYeuCauChuyenKho.TabIndex = 73;
            this.btnQuanLyYeuCauChuyenKho.Text = "Quản lý yêu cầu chuyển kho";
            this.btnQuanLyYeuCauChuyenKho.UseVisualStyleBackColor = false;
            this.btnQuanLyYeuCauChuyenKho.Click += new System.EventHandler(this.btnQuanLyYeuCauChuyenKho_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 324);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 23);
            this.button1.TabIndex = 52;
            this.button1.Text = "IMEISalesInfo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 353);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(125, 23);
            this.button2.TabIndex = 53;
            this.button2.Text = "IMEISalesInfoManager";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "IMEI=true";
            this.textEdit1.Location = new System.Drawing.Point(59, 240);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(178, 20);
            this.textEdit1.TabIndex = 3;
            // 
            // cboPM
            // 
            this.cboPM.FormattingEnabled = true;
            this.cboPM.Items.AddRange(new object[] {
            "frmLotIMEISalesInfo",
            "frmProductIMEIHistory_Old"});
            this.cboPM.Location = new System.Drawing.Point(243, 353);
            this.cboPM.Name = "cboPM";
            this.cboPM.Size = new System.Drawing.Size(180, 21);
            this.cboPM.TabIndex = 55;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(429, 353);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 23);
            this.button3.TabIndex = 54;
            this.button3.Text = "PM";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(550, 237);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(125, 23);
            this.button4.TabIndex = 56;
            this.button4.Text = "IMEISalesInfoManager";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnInputChangeOrderInput
            // 
            this.btnInputChangeOrderInput.Location = new System.Drawing.Point(12, 381);
            this.btnInputChangeOrderInput.Name = "btnInputChangeOrderInput";
            this.btnInputChangeOrderInput.Size = new System.Drawing.Size(155, 23);
            this.btnInputChangeOrderInput.TabIndex = 58;
            this.btnInputChangeOrderInput.Text = "Yêu cầu nhập đổi/trả hàng";
            this.btnInputChangeOrderInput.UseVisualStyleBackColor = true;
            this.btnInputChangeOrderInput.Click += new System.EventHandler(this.btnInputChangeOrderInput_Click);
            // 
            // btnInputChangeOrderManager
            // 
            this.btnInputChangeOrderManager.Location = new System.Drawing.Point(12, 410);
            this.btnInputChangeOrderManager.Name = "btnInputChangeOrderManager";
            this.btnInputChangeOrderManager.Size = new System.Drawing.Size(200, 23);
            this.btnInputChangeOrderManager.TabIndex = 59;
            this.btnInputChangeOrderManager.Text = "Quản lý yêu cầu nhập đổi/trả hàng";
            this.btnInputChangeOrderManager.UseVisualStyleBackColor = true;
            this.btnInputChangeOrderManager.Click += new System.EventHandler(this.butbtnInputChangeOrderInputton5_Click);
            // 
            // butbtnInputChangeOrderInputton5
            // 
            this.butbtnInputChangeOrderInputton5.Location = new System.Drawing.Point(12, 410);
            this.butbtnInputChangeOrderInputton5.Name = "butbtnInputChangeOrderInputton5";
            this.butbtnInputChangeOrderInputton5.Size = new System.Drawing.Size(200, 23);
            this.butbtnInputChangeOrderInputton5.TabIndex = 59;
            this.butbtnInputChangeOrderInputton5.Text = "Quản lý yêu cầu nhập đổi/trả hàng";
            this.butbtnInputChangeOrderInputton5.UseVisualStyleBackColor = true;
            // 
            // cboIMEI
            // 
            this.cboIMEI.FormattingEnabled = true;
            this.cboIMEI.Items.AddRange(new object[] {
            "frmProductIMEI_OrderIndexEdit",
            "frmProductIMEI_OrderIndexSearch"});
            this.cboIMEI.Location = new System.Drawing.Point(243, 382);
            this.cboIMEI.Name = "cboIMEI";
            this.cboIMEI.Size = new System.Drawing.Size(180, 21);
            this.cboIMEI.TabIndex = 61;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(429, 382);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(106, 23);
            this.button5.TabIndex = 60;
            this.button5.Text = "IMEI";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(243, 409);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(180, 21);
            this.comboBox3.TabIndex = 62;
            // 
            // btnSplitProduct
            // 
            this.btnSplitProduct.Location = new System.Drawing.Point(429, 438);
            this.btnSplitProduct.Name = "btnSplitProduct";
            this.btnSplitProduct.Size = new System.Drawing.Size(106, 23);
            this.btnSplitProduct.TabIndex = 60;
            this.btnSplitProduct.Text = "SplitProduct";
            this.btnSplitProduct.UseVisualStyleBackColor = true;
            this.btnSplitProduct.Click += new System.EventHandler(this.btnSplitProduct_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabDetail);
            this.groupBox1.Controls.Add(this.flexSaleOrderDetail);
            this.groupBox1.Controls.Add(this.txtTotalAmount);
            this.groupBox1.Location = new System.Drawing.Point(822, 176);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(236, 147);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kiểm tra License";
            // 
            // tabDetail
            // 
            this.tabDetail.Location = new System.Drawing.Point(6, 87);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.SelectedTabPage = this.tabPagePayment;
            this.tabDetail.Size = new System.Drawing.Size(218, 38);
            this.tabDetail.TabIndex = 11;
            this.tabDetail.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPagePayment,
            this.tabPagePaymentHistory});
            // 
            // tabPagePayment
            // 
            this.tabPagePayment.Name = "tabPagePayment";
            this.tabPagePayment.Size = new System.Drawing.Size(212, 12);
            this.tabPagePayment.Text = "Thanh toán";
            // 
            // tabPagePaymentHistory
            // 
            this.tabPagePaymentHistory.Name = "tabPagePaymentHistory";
            this.tabPagePaymentHistory.Size = new System.Drawing.Size(212, 12);
            this.tabPagePaymentHistory.Text = "Chi tiết thanh toán";
            // 
            // flexSaleOrderDetail
            // 
            this.flexSaleOrderDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flexSaleOrderDetail.AutoClipboard = true;
            this.flexSaleOrderDetail.ColumnInfo = "1,1,0,0,0,105,Columns:";
            this.flexSaleOrderDetail.Location = new System.Drawing.Point(6, 49);
            this.flexSaleOrderDetail.Name = "flexSaleOrderDetail";
            this.flexSaleOrderDetail.Rows.Count = 1;
            this.flexSaleOrderDetail.Rows.DefaultSize = 21;
            this.flexSaleOrderDetail.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Cell;
            this.flexSaleOrderDetail.Size = new System.Drawing.Size(116, 32);
            this.flexSaleOrderDetail.StyleInfo = resources.GetString("flexSaleOrderDetail.StyleInfo");
            this.flexSaleOrderDetail.TabIndex = 11;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalAmount.CustomFormat = "###,###,###,##0";
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalAmount.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtTotalAmount.Location = new System.Drawing.Point(6, 21);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(134, 22);
            this.txtTotalAmount.TabIndex = 2;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.Tag = null;
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTotalAmount.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(541, 297);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(275, 20);
            this.textBox1.TabIndex = 64;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(598, 324);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 65;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(765, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 66;
            this.label1.Text = "Double Click chuột";
            this.label1.DoubleClick += new System.EventHandler(this.label1_DoubleClick);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(879, 329);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(179, 20);
            this.textBox2.TabIndex = 67;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(550, 438);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(142, 23);
            this.button7.TabIndex = 68;
            this.button7.Text = "IMEI Change";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(419, 16);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 69;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(92, 65);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 69;
            this.button9.Text = "button8";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(611, 173);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(124, 23);
            this.button10.TabIndex = 70;
            this.button10.Text = "Hóa đơn hỗ trợ giá hàng tồn";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(592, 60);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 71;
            this.button11.Text = "button11";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(12, 12);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(121, 23);
            this.button12.TabIndex = 72;
            this.button12.Text = "Xuất đổi / Nhập trả hàng";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(12, 439);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(200, 23);
            this.button14.TabIndex = 74;
            this.button14.Text = "Đông: QL Hỗ trợ giá hàng tồn ";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(12, 463);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(200, 23);
            this.button15.TabIndex = 75;
            this.button15.Text = "Đông: Hỗ trợ giá hàng tồn ";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(21, 108);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 76;
            this.button16.Text = "button16";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(844, 380);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 77;
            this.button17.Text = "Thu hồi SP";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(125, 116);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 78;
            this.button18.Text = "button18";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(59, 294);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(167, 23);
            this.button19.TabIndex = 69;
            this.button19.Text = "frmConsignmentArisingReport";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(12, 176);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(155, 23);
            this.button20.TabIndex = 76;
            this.button20.Text = "QL chốt tồn kiểm kê theo kỳ";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(12, 205);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(155, 23);
            this.button21.TabIndex = 79;
            this.button21.Text = "Kiểm kê hàng đi đường";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(12, 263);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(214, 23);
            this.button22.TabIndex = 79;
            this.button22.Text = "Báo cáo Kiểm kê hàng đi đường";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(541, 353);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(158, 23);
            this.button23.TabIndex = 80;
            this.button23.Text = "Yêu cầu IMEI không xét FIFO";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // txtProductTypeID
            // 
            this.txtProductTypeID.EditValue = "1";
            this.txtProductTypeID.Location = new System.Drawing.Point(33, 294);
            this.txtProductTypeID.Name = "txtProductTypeID";
            this.txtProductTypeID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtProductTypeID.Size = new System.Drawing.Size(25, 20);
            this.txtProductTypeID.TabIndex = 81;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 82;
            this.label2.Text = "ID:";
            // 
            // frmTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1237, 498);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtProductTypeID);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.cboIMEI);
            this.Controls.Add(this.btnSplitProduct);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btnInputChangeOrderManager);
            this.Controls.Add(this.btnInputChangeOrderInput);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.cboPM);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.cboInventory);
            this.Controls.Add(this.btnWarranty);
            this.Controls.Add(this.btnInventory);
            this.Controls.Add(this.cboBeginTermInStock);
            this.Controls.Add(this.btnBeginTermInStock);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.ucUserQuickSearch1);
            this.Controls.Add(this.cboInputChange);
            this.Controls.Add(this.btnInputChange);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.radOutput);
            this.Controls.Add(this.radInput);
            this.Controls.Add(this.cboBarcode);
            this.Controls.Add(this.cboPrChange);
            this.Controls.Add(this.btnBarcode);
            this.Controls.Add(this.btnPrChange);
            this.Controls.Add(this.cboOutput);
            this.Controls.Add(this.btnOutput);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.txtStoreChangeOrderTypeID);
            this.Controls.Add(this.txtOutputVoucher);
            this.Controls.Add(this.cboStoreChangeCommand);
            this.Controls.Add(this.cboSC);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.btnStoreChangeOrder);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.btnInput);
            this.Controls.Add(this.btnLogin);
            this.Name = "frmTest";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOutputVoucher.Properties)).EndInit();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreChangeOrderTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabDetail)).EndInit();
            this.tabDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexSaleOrderDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductTypeID.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLogin;
        private DevExpress.XtraEditors.TextEdit txtUser;
        private DevExpress.XtraEditors.TextEdit txtPass;
        private DevExpress.XtraEditors.SimpleButton btnInput;
        private System.Windows.Forms.ComboBox comboBox1;
        private DevExpress.XtraEditors.SimpleButton btnStoreChangeOrder;
        private System.Windows.Forms.ComboBox cboSC;
        private DevExpress.XtraEditors.TextEdit txtOutputVoucher;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Button btnOutput;
        private System.Windows.Forms.ComboBox cboOutput;
        private System.Windows.Forms.Button btnPrChange;
        private System.Windows.Forms.ComboBox cboPrChange;
        private System.Windows.Forms.GroupBox GroupBox2;
        private System.Windows.Forms.RadioButton radNew;
        private System.Windows.Forms.RadioButton radOld;
        private System.Windows.Forms.RadioButton radOutput;
        private System.Windows.Forms.RadioButton radInput;
        private System.Windows.Forms.Button btnBarcode;
        private System.Windows.Forms.ComboBox cboBarcode;
        private System.Windows.Forms.Button btnInputChange;
        private System.Windows.Forms.ComboBox cboInputChange;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.ComboBox cboStoreChangeCommand;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucUserQuickSearch1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private System.Windows.Forms.ComboBox cboBeginTermInStock;
        private DevExpress.XtraEditors.SimpleButton btnBeginTermInStock;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport1;
        private DevExpress.XtraEditors.TextEdit txtStoreChangeOrderTypeID;
        private System.Windows.Forms.Button btnInventory;
        private System.Windows.Forms.ComboBox cboInventory;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private System.Windows.Forms.Button btnWarranty;
        private System.Windows.Forms.ComboBox comboBox2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.ComboBox cboPM;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnInputChangeOrderInput;
        private System.Windows.Forms.Button btnInputChangeOrderManager;
        private System.Windows.Forms.Button butbtnInputChangeOrderInputton5;
        private System.Windows.Forms.ComboBox cboIMEI;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Button btnSplitProduct;
        private System.Windows.Forms.GroupBox groupBox1;
        private C1.Win.C1Input.C1Label lblC1TotalQuantityOrder;
        private DevExpress.XtraTab.XtraTabControl tabDetail;
        private DevExpress.XtraTab.XtraTabPage tabPagePayment;
        private DevExpress.XtraTab.XtraTabPage tabPagePaymentHistory;
        private C1.Win.C1FlexGrid.C1FlexGrid flexSaleOrderDetail;
        private C1.Win.C1Input.C1NumericEdit txtTotalAmount;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button6;
        private C1.C1Excel.C1XLBook c1XLBook1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button7;
        private Barcode.CachedrptPrintProductQRCode_21 cachedrptPrintProductQRCode_211;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button btnQuanLyYeuCauChuyenKho;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button btnQuanLyXuatChuyenKho;
        private DevExpress.XtraEditors.TextEdit txtProductTypeID;
        private System.Windows.Forms.Label label2;
    }
}

