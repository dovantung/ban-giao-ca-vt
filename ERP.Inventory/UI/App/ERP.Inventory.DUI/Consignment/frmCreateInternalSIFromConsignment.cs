﻿using DevExpress.XtraEditors.Repository;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Consignment
{
    public partial class frmCreateInternalSIFromConsignment : Form
    {
        private DataTable dtStore = null;
        public DataTable dtbDataFromConsign = null;
        private DataTable dtbData = null;
        private DataTable dtbVatType = null;
        public DateTime? dtLastDate = null;
        public int intCreateInvoiceType = 0;
        public int intCompanyID = 0;
        private int intProductTypeID = 0;
        public bool issinvoice = false;

        public int ProductTypeID
        {
            get { return intProductTypeID; }
            set { intProductTypeID = value; }
        }

        public frmCreateInternalSIFromConsignment()
        {
            InitializeComponent();
        }

        private void frmConsignmentArisingReport_Load(object sender, EventArgs e)
        {
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            // Thue suất
            dtbVatType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_INVOICEVAT_VATTYPE");
            if (dtbVatType != null)
            {
                cboVAT.DataSource = dtbVatType.DefaultView.ToTable();
            }

            if (intCreateInvoiceType == 0)
            {
                this.Text = "Tạo hóa đơn xuất bán nội bộ";
            }
            else
            {
                this.Text = "Tạo hóa đơn xuất trả nội bộ";
            }

            FormatGrid();
            LoadData();
        }

        private void FormatGrid()
        {
            string[] lstColumn = new string[] { "CUSTOMERNAME", "STORENAME", "PRODUCTID", "PRODUCTNAME", "QUANTITYUNITNAME", "QUANTITY", "AMOUNTINVOICE", "TOTALAMOUNTINVOICEWITHOUTVAT", "INVOICEVAT", "INVOICEAMOUNT", "TOTALAMOUNTINVOICE" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, lstColumn);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);

            grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            grdViewData.OptionsView.ShowAutoFilterRow = true;
            grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            grdViewData.OptionsView.ShowFooter = true;
            grdViewData.OptionsView.ShowGroupPanel = false;
            grdViewData.OptionsBehavior.Editable = false;
            for (int i = 0; i < grdViewData.Columns.Count; i++)
            {
                //grdViewData.Columns[i].Visible = false;
                grdViewData.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns[i].AppearanceHeader.Options.UseTextOptions = true;
                grdViewData.Columns[i].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewData.Columns[i].AppearanceCell.BackColor = SystemColors.Info;
                grdViewData.Columns[i].AppearanceCell.Options.UseTextOptions = true;
                grdViewData.Columns[i].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewData.Columns[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            }

            RepositoryItemDateEdit repDate = new RepositoryItemDateEdit();
            repDate.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            repDate.Mask.UseMaskAsDisplayFormat = true;

            RepositoryItemSpinEdit repQuantity = new RepositoryItemSpinEdit();
            repQuantity.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            repQuantity.MinValue = 0;
            repQuantity.MaxValue = 9999999;
            repQuantity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            repQuantity.Mask.EditMask = "#,##0.####;";
            repQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            repQuantity.DisplayFormat.FormatString = "#,##0;";
            repQuantity.NullText = "0";
            repQuantity.NullValuePrompt = "0";
            repQuantity.NullValuePromptShowForEmptyValue = true;
            repQuantity.Buttons.Clear();

            RepositoryItemSpinEdit repQuantityCus = new RepositoryItemSpinEdit();
            repQuantityCus.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            repQuantityCus.MinValue = 0;
            repQuantityCus.MaxValue = 9999999;
            repQuantityCus.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            repQuantityCus.DisplayFormat.FormatString = "#,##0;";
            repQuantityCus.Buttons.Clear();

            RepositoryItemSpinEdit repAmount = new RepositoryItemSpinEdit();
            repAmount.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            repAmount.MinValue = 0;
            repAmount.MaxValue = 9999999999;
            repAmount.Mask.EditMask = "#,###,###,##0.####;";
            repAmount.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            repAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            repAmount.DisplayFormat.FormatString = "#,##0;";
            repAmount.NullText = "0";
            repAmount.NullValuePrompt = "0";
            repAmount.NullValuePromptShowForEmptyValue = true;

            repAmount.Buttons.Clear();

            RepositoryItemTextEdit repProductName = new RepositoryItemTextEdit();
            repProductName.MaxLength = 400;

            grdViewData.Columns["CUSTOMERNAME"].Caption = "Tên NCC";
            grdViewData.Columns["CUSTOMERNAME"].Visible = true;
            grdViewData.Columns["CUSTOMERNAME"].VisibleIndex = 0;
            grdViewData.Columns["CUSTOMERNAME"].OptionsColumn.AllowEdit = false;
            grdViewData.Columns["CUSTOMERNAME"].AppearanceCell.BackColor = SystemColors.Info;
            //grdViewData.Columns["CUSTOMERNAME"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grdViewData.Columns["CUSTOMERNAME"].Width = 120;

            grdViewData.Columns["STORENAME"].Caption = "Siêu thị";
            grdViewData.Columns["STORENAME"].Visible = true;
            grdViewData.Columns["STORENAME"].VisibleIndex = 1;
            grdViewData.Columns["STORENAME"].OptionsColumn.AllowEdit = false;
            grdViewData.Columns["STORENAME"].AppearanceCell.BackColor = SystemColors.Info;
            //grdViewData.Columns["STORENAME"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grdViewData.Columns["STORENAME"].Width = 120;

            grdViewData.Columns["PRODUCTID"].Caption = "Mã hàng hóa, dịch vụ";
            grdViewData.Columns["PRODUCTID"].Visible = true;
            grdViewData.Columns["PRODUCTID"].VisibleIndex = 2;
            grdViewData.Columns["PRODUCTID"].OptionsColumn.AllowEdit = false;
            grdViewData.Columns["PRODUCTID"].AppearanceCell.BackColor = SystemColors.Info;
            //grdViewData.Columns["PRODUCTID"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grdViewData.Columns["PRODUCTID"].Width = 120;

            grdViewData.Columns["PRODUCTNAME"].Caption = "Tên hàng hóa, dịch vụ";
            grdViewData.Columns["PRODUCTNAME"].Visible = true;
            grdViewData.Columns["PRODUCTNAME"].VisibleIndex = 3;
            grdViewData.Columns["PRODUCTNAME"].OptionsColumn.AllowEdit = false;
            grdViewData.Columns["PRODUCTNAME"].AppearanceCell.BackColor = SystemColors.Info;
            //grdViewData.Columns["PRODUCTNAME"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grdViewData.Columns["PRODUCTNAME"].ColumnEdit = repProductName;
            grdViewData.Columns["PRODUCTNAME"].Width = 350;

            grdViewData.Columns["QUANTITY"].Caption = "SL";
            grdViewData.Columns["QUANTITY"].Visible = true;
            grdViewData.Columns["QUANTITY"].VisibleIndex = 4;
            grdViewData.Columns["QUANTITY"].OptionsColumn.AllowEdit = false;
            grdViewData.Columns["QUANTITY"].AppearanceCell.BackColor = SystemColors.Info;
            grdViewData.Columns["QUANTITY"].ColumnEdit = repQuantity;
            grdViewData.Columns["QUANTITY"].Width = 60;
            grdViewData.Columns["QUANTITY"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grdViewData.Columns["QUANTITY"].DisplayFormat.FormatString = "#,##0.####;";

            grdViewData.Columns["QUANTITYUNITNAME"].Caption = "ĐVT";
            grdViewData.Columns["QUANTITYUNITNAME"].Visible = true;
            grdViewData.Columns["QUANTITYUNITNAME"].VisibleIndex = 5;
            grdViewData.Columns["QUANTITYUNITNAME"].OptionsColumn.AllowEdit = false;
            grdViewData.Columns["QUANTITYUNITNAME"].AppearanceCell.BackColor = SystemColors.Info;
            grdViewData.Columns["QUANTITYUNITNAME"].Width = 50;

            grdViewData.Columns["AMOUNTINVOICE"].Caption = "Đơn giá chưa VAT";
            grdViewData.Columns["AMOUNTINVOICE"].Visible = true;
            grdViewData.Columns["AMOUNTINVOICE"].VisibleIndex = 6;
            grdViewData.Columns["AMOUNTINVOICE"].OptionsColumn.AllowEdit = true;
            grdViewData.Columns["AMOUNTINVOICE"].ColumnEdit = repAmount;
            grdViewData.Columns["AMOUNTINVOICE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grdViewData.Columns["AMOUNTINVOICE"].DisplayFormat.FormatString = "#,##0;";
            grdViewData.Columns["AMOUNTINVOICE"].Width = 110;

            grdViewData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].Caption = "Thành tiền chưa VAT";
            grdViewData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].Visible = true;
            grdViewData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].VisibleIndex = 7;
            grdViewData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].OptionsColumn.AllowEdit = false;
            grdViewData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].ColumnEdit = repAmount;
            grdViewData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].Width = 120;
            grdViewData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grdViewData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].DisplayFormat.FormatString = "#,##0;";

            grdViewData.Columns["INVOICEVAT"].Caption = "Thuế suất";
            grdViewData.Columns["INVOICEVAT"].Visible = true;
            grdViewData.Columns["INVOICEVAT"].VisibleIndex = 8;
            grdViewData.Columns["INVOICEVAT"].OptionsColumn.AllowEdit = true;
            grdViewData.Columns["INVOICEVAT"].ColumnEdit = cboVAT;
            grdViewData.Columns["INVOICEVAT"].Width = 70;

            grdViewData.Columns["INVOICEAMOUNT"].Caption = "Tiền thuế";
            grdViewData.Columns["INVOICEAMOUNT"].Visible = true;
            grdViewData.Columns["INVOICEAMOUNT"].VisibleIndex = 9;
            grdViewData.Columns["INVOICEAMOUNT"].OptionsColumn.AllowEdit = true;
            grdViewData.Columns["INVOICEAMOUNT"].ColumnEdit = repAmount;
            grdViewData.Columns["INVOICEAMOUNT"].Width = 110;

            grdViewData.Columns["TOTALAMOUNTINVOICE"].Caption = "Thành tiền có VAT";
            grdViewData.Columns["TOTALAMOUNTINVOICE"].Visible = true;
            grdViewData.Columns["TOTALAMOUNTINVOICE"].VisibleIndex = 10;
            grdViewData.Columns["TOTALAMOUNTINVOICE"].OptionsColumn.AllowEdit = true;
            grdViewData.Columns["TOTALAMOUNTINVOICE"].ColumnEdit = repAmount;
            grdViewData.Columns["TOTALAMOUNTINVOICE"].Width = 120;

            //grdViewData.Columns["GROUPSERVICEID"].Caption = "Nhóm hàng hóa dịch vụ";
            //grdViewData.Columns["GROUPSERVICEID"].VisibleIndex = 22;
            //grdViewData.Columns["GROUPSERVICEID"].OptionsColumn.AllowEdit = true;
            //grdViewData.Columns["GROUPSERVICEID"].ColumnEdit = repLockUpServiceGroup;
            //grdViewData.Columns["GROUPSERVICEID"].AppearanceCell.BackColor = SystemColors.Window;
            //grdViewData.Columns["GROUPSERVICEID"].Width = 200;


            grdViewData.Columns["PRODUCTNAME"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PRODUCTNAME", "Tổng cộng: {0:N0}")});

            grdViewData.Columns["QUANTITY"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:#,##0.####}")});

            grdViewData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTINVOICEWITHOUTVAT", "{0:N0}")});

            grdViewData.Columns["INVOICEAMOUNT"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "INVOICEAMOUNT", "{0:N0}")});

            grdViewData.Columns["TOTALAMOUNTINVOICE"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTINVOICE", "{0:N0}")});

            //grdViewData.Columns["GROUPSERVICEID"].Visible = false;
        }

        void CreateDataProductService()
        {
            dtbData = new DataTable();
            dtbData.TableName = "IMPORTVATADD";
            dtbData.Columns.Add("CUSTOMERID", typeof(Int32));
            dtbData.Columns["CUSTOMERID"].DefaultValue = -1;
            dtbData.Columns.Add("CUSTOMERNAME", typeof(string));
            dtbData.Columns.Add("STOREID", typeof(Int32));
            dtbData.Columns.Add("STORENAME", typeof(string));
            dtbData.Columns.Add("TOSTOREID", typeof(Int32));
            dtbData.Columns.Add("OUTINVOICETRANSTYPEID", typeof(Int32));
            dtbData.Columns.Add("ININVOICETRANSTYPEID", typeof(Int32));
            dtbData.Columns.Add("PRODUCTID", typeof(string));
            dtbData.Columns.Add("PRODUCTNAME", typeof(string));
            dtbData.Columns.Add("QUANTITY", typeof(decimal));
            dtbData.Columns["QUANTITY"].DefaultValue = 0;
            dtbData.Columns.Add("ISPROMOTIONPRODUCTMISA", typeof(Int32));
            dtbData.Columns["ISPROMOTIONPRODUCTMISA"].DefaultValue = 0;
            dtbData.Columns.Add("QUANTITYUNITNAME", typeof(string));
            dtbData.Columns.Add("AMOUNTINVOICE", typeof(decimal));
            dtbData.Columns["AMOUNTINVOICE"].DefaultValue = 0;
            dtbData.Columns.Add("TOTALAMOUNTINVOICEWITHOUTVAT", typeof(decimal));
            dtbData.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].DefaultValue = 0;
            dtbData.Columns.Add("INVOICEVAT", typeof(decimal));
            dtbData.Columns["INVOICEVAT"].DefaultValue = 0;
            dtbData.Columns.Add("INVOICEAMOUNT", typeof(decimal));
            dtbData.Columns["INVOICEAMOUNT"].DefaultValue = 0;
            dtbData.Columns.Add("TOTALAMOUNTINVOICE", typeof(decimal));
            dtbData.Columns["TOTALAMOUNTINVOICE"].DefaultValue = 0;
            dtbData.Columns.Add("DESCRIPTION", typeof(string));
            dtbData.Columns.Add("GROUPSERVICEID", typeof(Int32));
            dtbData.Columns["GROUPSERVICEID"].DefaultValue = 0;
            dtbData.Columns.Add("COMPANYID", typeof(Int32));
            dtbData.Columns["COMPANYID"].DefaultValue = 0;
        }

        private void LoadData()
        {
            if (dtbDataFromConsign == null || dtbDataFromConsign.Rows.Count == null)
                return;

            CreateDataProductService();

            foreach (DataRow row in dtbDataFromConsign.Rows)
            {
                DataRow rowNew = dtbData.NewRow();
                rowNew["CUSTOMERID"] = row["CUSTOMERID"];
                rowNew["CUSTOMERNAME"] = row["CUSTOMERNAME"];
                rowNew["COMPANYID"] = intCompanyID;
                if (intCreateInvoiceType == 0)
                {
                    rowNew["STOREID"] = row["STORECENTERID"];
                    rowNew["STORENAME"] = row["STORENAME"];
                    rowNew["TOSTOREID"] = row["STOREID"];
                    rowNew["OUTINVOICETRANSTYPEID"] = row["INTERNALSITRANSTYPEID"];
                    rowNew["ININVOICETRANSTYPEID"] = row["INTERNALPITRANSTYPEID"];
                    rowNew["QUANTITY"] = row["SALEQUANTITY"];
                }
                else if (intCreateInvoiceType == 1)
                {
                    rowNew["STOREID"] = row["STOREID"];
                    rowNew["STORENAME"] = row["STORENAME"];
                    rowNew["TOSTOREID"] = row["STORECENTERID"];
                    rowNew["OUTINVOICETRANSTYPEID"] = row["INTERNALRETURNSITRANSTYPEID"];
                    rowNew["ININVOICETRANSTYPEID"] = row["INTERNALRETURNPITRANSTYPEID"];
                    rowNew["QUANTITY"] = row["RETURNQUANTITY"];
                }

                rowNew["PRODUCTID"] = row["PRODUCTID"];
                rowNew["PRODUCTNAME"] = row["PRODUCTNAME"];

                rowNew["QUANTITYUNITNAME"] = row["QUANTITYUNITNAME"];
                rowNew["QUANTITYUNITNAME"] = row["QUANTITYUNITNAME"];
                rowNew["INVOICEVAT"] = row["VAT"];
                rowNew["AMOUNTINVOICE"] = row["INPUTPRICE"];
                rowNew["INVOICEVAT"] = row["VAT"];

                dtbData.Rows.Add(rowNew);
            }

            grdData.DataSource = dtbData;
            for (int i = 0; i < dtbData.Rows.Count; i++)
            {
                grdViewData.FocusedRowHandle = i;
                Calculate("AMOUNTINVOICE", dtbData.Rows[i]["PRODUCTID"].ToString());
            }

            grdViewData.RefreshData();
        }

        private void grdViewData_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                if (grdViewData.FocusedRowHandle < 0) return;
                DataRowView item = (DataRowView)grdViewData.GetFocusedRow();
                if (e.Column.FieldName == "QUANTITY" ||
                    e.Column.FieldName == "AMOUNTINVOICE" ||
                    e.Column.FieldName == "TOTALAMOUNTINVOICEWITHOUTVAT" ||
                    e.Column.FieldName == "INVOICEVAT" ||
                    e.Column.FieldName == "INVOICEAMOUNT" ||
                    e.Column.FieldName == "TOTALAMOUNTINVOICE")
                {
                    Calculate(e.Column.FieldName, item["PRODUCTID"].ToString());
                    decimal decTOTALAMOUNTINVOICE = Convert.ToDecimal(grdViewData.GetRowCellValue(e.RowHandle, "TOTALAMOUNTINVOICEWITHOUTVAT"));
                    decimal decTOTALAMOUNT = Convert.ToDecimal(grdViewData.GetRowCellValue(e.RowHandle, "TOTALAMOUNTWITHOUTVAT"));
                    grdViewData.SetRowCellValue(e.RowHandle, "UNEVEN", decTOTALAMOUNTINVOICE - decTOTALAMOUNT);
                    grdViewData.RefreshData();
                }

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi tạo dữ liệu", objExce.ToString());
                SystemErrorWS.Insert("Lỗi tạo dữ liệu", objExce.ToString(), this.Name + " -> grdViewDataProductService_CellValueChanged", DUIInventory_Globals.ModuleName);
            }
        }

        private decimal ConvertDecimal(string value)
        {
            decimal ret = -1;
            decimal.TryParse(value, out ret);
            return ret;
        }

        private void Calculate(string strFieldName, string strProductID)
        {
            if (dtbData != null && dtbData.Rows.Count > 0)
            {
                DataRow dr = grdViewData.GetFocusedDataRow();
                if (dr == null)
                    return;

                decimal MaxINVOICEVAT = Convert.ToDecimal(dr["INVOICEVAT"]);
                decimal MaxInvoiceVATCaculate = MaxINVOICEVAT; //VAT để tính toán (Trường hợp không chịu thuế sẽ = 0)
                if (MaxINVOICEVAT == -1)
                    MaxInvoiceVATCaculate = 0;
                switch (strFieldName)
                {
                    case "QUANTITY":
                        {
                            dr["INVOICEVAT"] = MaxINVOICEVAT;
                            dr["TOTALAMOUNTINVOICEWITHOUTVAT"] = ConvertDecimal(dr["AMOUNTINVOICE"].ToString()) * ConvertDecimal(dr["QUANTITY"].ToString());
                            dr["INVOICEAMOUNT"] = ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) * (Convert.ToDecimal(MaxInvoiceVATCaculate) / 100);
                            dr["TOTALAMOUNTINVOICE"] = ConvertDecimal(dr["INVOICEAMOUNT"].ToString()) + ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());
                        }
                        break;
                    case "AMOUNTINVOICE":
                        {
                            dr["INVOICEVAT"] = MaxINVOICEVAT;
                            dr["TOTALAMOUNTINVOICEWITHOUTVAT"] = ConvertDecimal(dr["AMOUNTINVOICE"].ToString()) * ConvertDecimal(dr["QUANTITY"].ToString());
                            dr["INVOICEAMOUNT"] = ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) * (Convert.ToDecimal(MaxInvoiceVATCaculate) / 100);
                            dr["TOTALAMOUNTINVOICE"] = ConvertDecimal(dr["INVOICEAMOUNT"].ToString()) + ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());
                        }
                        break;

                    case "TOTALAMOUNTINVOICEWITHOUTVAT":
                        {
                            decimal decTOTALAMOUNTINVOICEWITHOUTVAT = Math.Round(ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()), 4, MidpointRounding.AwayFromZero);
                            decimal decQuantityRowChange = ConvertDecimal(dr["QUANTITY"].ToString());
                            decimal decMain = 0;
                            if (decQuantityRowChange == 1)
                            {
                                decMain = decTOTALAMOUNTINVOICEWITHOUTVAT;
                            }
                            else
                            {
                                decMain = decTOTALAMOUNTINVOICEWITHOUTVAT / decQuantityRowChange;
                            }

                            dr["TOTALAMOUNTINVOICEWITHOUTVAT"] = decMain * ConvertDecimal(dr["QUANTITY"].ToString());
                            var SumAMOUNTINVOICE = ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());
                            var SumQUANTITY = ConvertDecimal(dr["QUANTITY"].ToString());
                            dr["AMOUNTINVOICE"] = SumAMOUNTINVOICE / SumQUANTITY;
                            dr["INVOICEVAT"] = MaxINVOICEVAT;
                            dr["INVOICEAMOUNT"] = ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) * (Convert.ToDecimal(MaxInvoiceVATCaculate) / 100);
                            dr["TOTALAMOUNTINVOICE"] = ConvertDecimal(dr["INVOICEAMOUNT"].ToString()) + ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());
                        }
                        break;

                    case "INVOICEVAT":
                        {
                            decimal decINVOICEVAT = ConvertDecimal(dr["INVOICEVAT"].ToString());
                            decimal decVAT = decINVOICEVAT; //VAT mang đi tính toán (Trường hợp không chịu thuế sẽ = 0)
                            if (decINVOICEVAT == -1)
                                decVAT = 0;

                            dr["INVOICEVAT"] = decINVOICEVAT;
                            dr["TOTALAMOUNTINVOICEWITHOUTVAT"] = ConvertDecimal(dr["AMOUNTINVOICE"].ToString()) * ConvertDecimal(dr["QUANTITY"].ToString());
                            dr["INVOICEAMOUNT"] = ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) * (decVAT / 100);
                            dr["TOTALAMOUNTINVOICE"] = ConvertDecimal(dr["INVOICEAMOUNT"].ToString()) + ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());
                        }
                        break;

                    case "INVOICEAMOUNT":
                        {
                            decimal decINVOICEAMOUNT = Math.Round(ConvertDecimal(dr["INVOICEAMOUNT"].ToString()), 4, MidpointRounding.AwayFromZero);
                            decimal decQuantityRowChange = Math.Round(ConvertDecimal(dr["QUANTITY"].ToString()), 4, MidpointRounding.AwayFromZero);
                            decimal decMain = 0;
                            if (decQuantityRowChange == 1)
                            {
                                decMain = decINVOICEAMOUNT;
                            }
                            else
                            {
                                decMain = decINVOICEAMOUNT / decQuantityRowChange;
                            }

                            dr["INVOICEAMOUNT"] = decMain * ConvertDecimal(dr["QUANTITY"].ToString());
                            if (MaxInvoiceVATCaculate == 0)
                            {
                                dr["INVOICEAMOUNT"] = 0; // Trường hợp VAT = 0 hoặc không chịu thuế thì tiền thuế luôn = 0
                            }

                            if (MaxInvoiceVATCaculate > 0) // Chỉ tính lại khi VAT >0
                            {
                                var SumAMOUNTINVOICE = ConvertDecimal(dr["AMOUNTINVOICE"].ToString()) * ConvertDecimal(dr["QUANTITY"].ToString());
                                var SumQUANTITY = ConvertDecimal(dr["QUANTITY"].ToString());
                                dr["INVOICEVAT"] = MaxINVOICEVAT;
                                dr["TOTALAMOUNTINVOICE"] = ConvertDecimal(dr["INVOICEAMOUNT"].ToString()) + ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());
                            }
                        }
                        break;

                    case "TOTALAMOUNTINVOICE":
                        {
                            decimal decTOTALAMOUNTINVOICE = Math.Round(ConvertDecimal(dr["TOTALAMOUNTINVOICE"].ToString()), 4, MidpointRounding.AwayFromZero);
                            decimal decQuantityRowChange = Math.Round(ConvertDecimal(dr["QUANTITY"].ToString()), 4, MidpointRounding.AwayFromZero);
                            decimal decMain = 0;
                            if (decQuantityRowChange == 1)
                            {
                                decMain = decTOTALAMOUNTINVOICE;
                            }
                            else
                            {
                                decMain = decTOTALAMOUNTINVOICE / decQuantityRowChange;
                            }

                            dr["TOTALAMOUNTINVOICE"] = decMain * ConvertDecimal(dr["QUANTITY"].ToString());

                            var SumQUANTITY = ConvertDecimal(dr["QUANTITY"].ToString());
                            var SumTOTALAMOUNTINVOICE = ConvertDecimal(dr["TOTALAMOUNTINVOICE"].ToString());

                            var SumAMOUNTINVOICE = (SumTOTALAMOUNTINVOICE / ((decimal)(MaxInvoiceVATCaculate + 100) / 100));
                            dr["AMOUNTINVOICE"] = SumAMOUNTINVOICE / SumQUANTITY;
                            dr["INVOICEVAT"] = MaxINVOICEVAT;
                            dr["TOTALAMOUNTINVOICEWITHOUTVAT"] = Convert.ToDecimal(dr["AMOUNTINVOICE"]) * ConvertDecimal(dr["QUANTITY"].ToString());
                            dr["INVOICEAMOUNT"] = Convert.ToDecimal(dr["TOTALAMOUNTINVOICE"]) - (Convert.ToDecimal(dr["AMOUNTINVOICE"]) * Convert.ToDecimal(dr["QUANTITY"].ToString()));
                        }
                        break;
                }
            }
        }

        private void btnCreatePInvoice_Click(object sender, EventArgs e)
        {
            if (dtbData == null)
                return;

            //for(int i = 0; i < dtbData.Rows.Count; i++)
            //{
            //    if(ConvertDecimal(dtbData.Rows[i]["AMOUNTINVOICE"].ToString()) == 0)
            //    {
            //        MessageBox.Show("Vui lòng nhập giá đầy đủ trước khi cập nhật.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        grdViewData.FocusedRowHandle = i;
            //        grdViewData.FocusedColumn = grdViewData.Columns["AMOUNTINVOICE"];
            //        return;
            //    }
            //}

            ERP.PrintVAT.PLC.WSPrintVAT.ResultMessage resMess = new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().InsertInvoiceInternalSI(intProductTypeID, dtbData, dtLastDate.Value, intCreateInvoiceType, issinvoice);
            if (resMess.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, resMess.Message, resMess.MessageDetail);
                //MessageBox.Show(resMess.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            
            string strMessage = string.Empty;
            if (intCreateInvoiceType == 0)
                strMessage = "Tạo hóa đơn xuất bán nội bộ thành công!";
            else
                strMessage = "Tạo hóa đơn xuất trả nội bộ thành công!";

            MessageBox.Show(strMessage, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

    }
}
