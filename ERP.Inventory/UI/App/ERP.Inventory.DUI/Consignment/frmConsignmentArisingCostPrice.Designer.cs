﻿namespace ERP.Inventory.DUI.Consignment
{
    partial class frmConsignmentArisingCostPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdReport = new DevExpress.XtraGrid.GridControl();
            this.grvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnCal = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.grdResult = new DevExpress.XtraGrid.GridControl();
            this.grvResult = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.grdReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvReport)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvResult)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdReport
            // 
            this.grdReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdReport.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.grdReport.Location = new System.Drawing.Point(4, 4);
            this.grdReport.MainView = this.grvReport;
            this.grdReport.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.grdReport.Name = "grdReport";
            this.grdReport.Size = new System.Drawing.Size(853, 388);
            this.grdReport.TabIndex = 0;
            this.grdReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvReport});
            // 
            // grvReport
            // 
            this.grvReport.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvReport.Appearance.FocusedRow.Options.UseFont = true;
            this.grvReport.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvReport.Appearance.FooterPanel.Options.UseFont = true;
            this.grvReport.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvReport.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.grvReport.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvReport.Appearance.GroupFooter.Options.UseFont = true;
            this.grvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvReport.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.grvReport.Appearance.GroupRow.Options.UseFont = true;
            this.grvReport.Appearance.GroupRow.Options.UseForeColor = true;
            this.grvReport.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvReport.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvReport.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvReport.Appearance.Preview.Options.UseFont = true;
            this.grvReport.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvReport.Appearance.Row.Options.UseFont = true;
            this.grvReport.ColumnPanelRowHeight = 25;
            this.grvReport.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grvReport.GridControl = this.grdReport;
            this.grvReport.GroupFormat = " [#image]{1} {2}";
            this.grvReport.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grvReport.Name = "grvReport";
            this.grvReport.OptionsBehavior.AutoExpandAllGroups = true;
            this.grvReport.OptionsBehavior.SummariesIgnoreNullValues = true;
            this.grvReport.OptionsMenu.EnableColumnMenu = false;
            this.grvReport.OptionsMenu.EnableFooterMenu = false;
            this.grvReport.OptionsMenu.EnableGroupPanelMenu = false;
            this.grvReport.OptionsNavigation.UseTabKey = false;
            this.grvReport.OptionsPrint.AutoWidth = false;
            this.grvReport.OptionsPrint.UsePrintStyles = true;
            this.grvReport.OptionsView.ColumnAutoWidth = false;
            this.grvReport.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvReport.OptionsView.ShowFooter = true;
            this.grvReport.OptionsView.ShowGroupPanel = false;
            // 
            // btnCal
            // 
            this.btnCal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCal.Image = global::ERP.Inventory.DUI.Properties.Resources.calculator;
            this.btnCal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCal.Location = new System.Drawing.Point(647, 8);
            this.btnCal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCal.Name = "btnCal";
            this.btnCal.Size = new System.Drawing.Size(100, 28);
            this.btnCal.TabIndex = 4;
            this.btnCal.Text = "&Tính giá";
            this.btnCal.UseVisualStyleBackColor = true;
            this.btnCal.Click += new System.EventHandler(this.btnCal_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(756, 8);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Hủy bỏ";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(140, 18);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(869, 422);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 6;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.grdReport);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(861, 396);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Giá xuất theo imei";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.grdResult);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Size = new System.Drawing.Size(1136, 452);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Kết quả cập nhật";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // grdResult
            // 
            this.grdResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdResult.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.grdResult.Location = new System.Drawing.Point(4, 4);
            this.grdResult.MainView = this.grvResult;
            this.grdResult.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.grdResult.Name = "grdResult";
            this.grdResult.Size = new System.Drawing.Size(1128, 444);
            this.grdResult.TabIndex = 1;
            this.grdResult.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvResult});
            // 
            // grvResult
            // 
            this.grvResult.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvResult.Appearance.FocusedRow.Options.UseFont = true;
            this.grvResult.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvResult.Appearance.FooterPanel.Options.UseFont = true;
            this.grvResult.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvResult.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.grvResult.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvResult.Appearance.GroupFooter.Options.UseFont = true;
            this.grvResult.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvResult.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.grvResult.Appearance.GroupRow.Options.UseFont = true;
            this.grvResult.Appearance.GroupRow.Options.UseForeColor = true;
            this.grvResult.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvResult.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvResult.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvResult.Appearance.Preview.Options.UseFont = true;
            this.grvResult.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvResult.Appearance.Row.Options.UseFont = true;
            this.grvResult.ColumnPanelRowHeight = 25;
            this.grvResult.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grvResult.GridControl = this.grdResult;
            this.grvResult.GroupFormat = " [#image]{1} {2}";
            this.grvResult.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grvResult.Name = "grvResult";
            this.grvResult.OptionsBehavior.AutoExpandAllGroups = true;
            this.grvResult.OptionsBehavior.SummariesIgnoreNullValues = true;
            this.grvResult.OptionsMenu.EnableColumnMenu = false;
            this.grvResult.OptionsMenu.EnableFooterMenu = false;
            this.grvResult.OptionsMenu.EnableGroupPanelMenu = false;
            this.grvResult.OptionsNavigation.UseTabKey = false;
            this.grvResult.OptionsPrint.AutoWidth = false;
            this.grvResult.OptionsPrint.UsePrintStyles = true;
            this.grvResult.OptionsView.ColumnAutoWidth = false;
            this.grvResult.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvResult.OptionsView.ShowFooter = true;
            this.grvResult.OptionsView.ShowGroupPanel = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnCal);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 422);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(869, 45);
            this.panel1.TabIndex = 7;
            // 
            // frmConsignmentArisingCostPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 467);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmConsignmentArisingCostPrice";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Giá vốn";
            this.Load += new System.EventHandler(this.frmConsignmentArisingCostPrice_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvReport)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvResult)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdReport;
        private DevExpress.XtraGrid.Views.Grid.GridView grvReport;
        private System.Windows.Forms.Button btnCal;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private DevExpress.XtraGrid.GridControl grdResult;
        private DevExpress.XtraGrid.Views.Grid.GridView grvResult;
        private System.Windows.Forms.Panel panel1;
    }
}