﻿using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Consignment
{
    public partial class frmConsignmentArisingCostPrice : Form
    {
        public frmConsignmentArisingCostPrice(bool enableCalCostPrice, DateTime date)
        {
            InitializeComponent();
            this.enableCalCostPrice = enableCalCostPrice;
            this.date = date;
        }

        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new ERP.Report.PLC.PLCReportDataSource();
        private DataTable PVISalePriceTable = null;
        private bool enableCalCostPrice = true;
        private DateTime date;
        private bool enableTabPage2 = false;

        private void frmConsignmentArisingCostPrice_Load(object sender, EventArgs e)
        {
            this.btnCal.Enabled = enableCalCostPrice;
            this.enableTabPage2 = false;
            try
            {
                this.LoadData();

                if (this.PVISalePriceTable != null)
                {
                    this.grdReport.DataSource = PVISalePriceTable;

                    this.grvReport.OptionsView.ShowAutoFilterRow = true;
                    this.grvReport.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;

                    foreach (DevExpress.XtraGrid.Columns.GridColumn _col in this.grvReport.Columns)
                    {
                        _col.OptionsColumn.ReadOnly = true;
                        _col.OptionsColumn.AllowEdit = false;

                        _col.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                        _col.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                    }

                    this.grvReport.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                    this.grvReport.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                    this.grvReport.Columns["IMEI"].Caption = "IMEI";
                    this.grvReport.Columns["SALEPRICE"].Caption = "Giá bán";

                    this.grvReport.Columns["PRODUCTID"].Width = 150;
                    this.grvReport.Columns["PRODUCTNAME"].Width = 300;
                    this.grvReport.Columns["IMEI"].Width = 150;
                    this.grvReport.Columns["SALEPRICE"].Width = 120;
                    this.grvReport.Columns["SALEPRICE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    this.grvReport.Columns["SALEPRICE"].DisplayFormat.FormatString = "N6";
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.ToString());
            }
        }

        private void LoadData()
        {
            object[] objKeywords = new object[]
            {
                "@ProductIDList", string.Empty,
                "@DATE", this.date
            };

            try
            {
                DataTable dtbResource = objPLCReportDataSource.GetDataSource("PM_CONSIGNMENTARISING_PVI_SHR", objKeywords.ToArray());
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);

                    if (this.PVISalePriceTable != null)
                    {
                        this.PVISalePriceTable.Rows.Clear();
                    }
                    return;
                }
                else
                {
                    if (dtbResource != null && dtbResource.Rows.Count > 0)
                    {
                        this.PVISalePriceTable = dtbResource;
                    }
                    else
                    {
                        MessageBox.Show("Không tìm thấy dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void CostPrice()
        {
            object[] objKeywords = new object[]
            {
                "@ProductIDList", string.Empty,
                "@DATE", this.date
            };

            try
            {
                DataTable dtbResource = objPLCReportDataSource.GetDataSource("PM_CONSIGNMENT_PVI_COST_PRICE", objKeywords.ToArray());
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);

                    if (this.grdResult.DataSource != null)
                    {
                        ((DataTable)this.grdResult.DataSource).Rows.Clear();
                    }
                    return;
                }
                else
                {
                    if (dtbResource != null && dtbResource.Rows.Count > 0)
                    {
                        this.grdResult.DataSource = dtbResource;
                        #region Dinh dang luoi
                        this.grvResult.OptionsView.ShowAutoFilterRow = true;
                        this.grvResult.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;

                        foreach (DevExpress.XtraGrid.Columns.GridColumn _col in this.grvResult.Columns)
                        {
                            _col.OptionsColumn.ReadOnly = true;
                            _col.OptionsColumn.AllowEdit = false;

                            _col.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                            _col.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                        }

                        this.grvResult.Columns["OUTPUTVOUCHERID"].Caption = "Mã CT xuất";
                        this.grvResult.Columns["INPUTVOUCHERID"].Caption = "Mã CT nhập";
                        this.grvResult.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                        this.grvResult.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                        this.grvResult.Columns["IMEI"].Caption = "IMEI";
                        this.grvResult.Columns["COSTPRICE"].Caption = "Giá vốn";

                        this.grvResult.Columns["OUTPUTVOUCHERID"].Width = 150;
                        this.grvResult.Columns["INPUTVOUCHERID"].Width = 150;
                        this.grvResult.Columns["PRODUCTID"].Width = 120;
                        this.grvResult.Columns["PRODUCTNAME"].Width = 250;
                        this.grvResult.Columns["IMEI"].Width = 100;
                        this.grvResult.Columns["COSTPRICE"].Width = 120;
                        this.grvResult.Columns["COSTPRICE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                        this.grvResult.Columns["COSTPRICE"].DisplayFormat.FormatString = "N6";

                        this.btnCal.Enabled = false;
                        #endregion
                    }
                    else
                    {
                        MessageBox.Show("Không tìm thấy dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.None;
            this.Close();
        }

        private void btnCal_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Có chắc chắn thực hiện cập nhật giá vốn không?", "Thông báo", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) != DialogResult.Yes)
            {
                return;
            }

            this.enableTabPage2 = false;
            this.CostPrice();

            this.enableTabPage2 = true;
            this.tabControl1.SelectedTab = this.tabPage2;
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPage == this.tabPage2 && !this.enableTabPage2)
                e.Cancel = true;
        }
    }
}
