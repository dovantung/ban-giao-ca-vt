﻿namespace ERP.Inventory.DUI.Consignment
{
    partial class frmConsignmentArisingReportTCDT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsignmentArisingReportTCDT));
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chk_IsSinvoice = new System.Windows.Forms.CheckBox();
            this.chkViewDetail = new System.Windows.Forms.CheckBox();
            this.cboReturnInvoiceType = new System.Windows.Forms.ComboBox();
            this.cboInternalInvoiceType = new System.Windows.Forms.ComboBox();
            this.cboInternalReturnInvoiceType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboPInvoiceType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.cboCustomerIDList = new Library.AppControl.ComboBoxControl.ComboBoxCustomer();
            this.dteSearchDate = new System.Windows.Forms.DateTimePicker();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cboBRANCHID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemCreatePInvoice = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemCreateInternalSISinvoice = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemCreateInternalSI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemCreateInternalReturnSI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemReturnSI = new System.Windows.Forms.ToolStripMenuItem();
            this.grdReport = new DevExpress.XtraGrid.GridControl();
            this.bagrvReport = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.repCheckBox = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.cboVAT = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panel1.SuspendLayout();
            this.mnuAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bagrvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVAT)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 185);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 16);
            this.label11.TabIndex = 11;
            this.label11.Text = "Siêu thị:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "NCC:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chk_IsSinvoice);
            this.panel1.Controls.Add(this.chkViewDetail);
            this.panel1.Controls.Add(this.cboReturnInvoiceType);
            this.panel1.Controls.Add(this.cboInternalInvoiceType);
            this.panel1.Controls.Add(this.cboInternalReturnInvoiceType);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cboPInvoiceType);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cboCompany);
            this.panel1.Controls.Add(this.cboCustomerIDList);
            this.panel1.Controls.Add(this.dteSearchDate);
            this.panel1.Controls.Add(this.cboStore);
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.cboBRANCHID);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(986, 92);
            this.panel1.TabIndex = 0;
            // 
            // chk_IsSinvoice
            // 
            this.chk_IsSinvoice.AutoSize = true;
            this.chk_IsSinvoice.Location = new System.Drawing.Point(673, 36);
            this.chk_IsSinvoice.Name = "chk_IsSinvoice";
            this.chk_IsSinvoice.Size = new System.Drawing.Size(121, 20);
            this.chk_IsSinvoice.TabIndex = 21;
            this.chk_IsSinvoice.Text = "Hóa đơn điện tử";
            this.chk_IsSinvoice.UseVisualStyleBackColor = true;
            // 
            // chkViewDetail
            // 
            this.chkViewDetail.AutoSize = true;
            this.chkViewDetail.Location = new System.Drawing.Point(673, 11);
            this.chkViewDetail.Name = "chkViewDetail";
            this.chkViewDetail.Size = new System.Drawing.Size(94, 20);
            this.chkViewDetail.TabIndex = 4;
            this.chkViewDetail.Text = "Xem chi tiết";
            this.chkViewDetail.UseVisualStyleBackColor = true;
            // 
            // cboReturnInvoiceType
            // 
            this.cboReturnInvoiceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReturnInvoiceType.DropDownWidth = 145;
            this.cboReturnInvoiceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboReturnInvoiceType.Items.AddRange(new object[] {
            "--HĐ xuất trả NCC--",
            "Chưa tạo hóa đơn",
            "Đã tạo hóa đơn"});
            this.cboReturnInvoiceType.Location = new System.Drawing.Point(516, 63);
            this.cboReturnInvoiceType.Name = "cboReturnInvoiceType";
            this.cboReturnInvoiceType.Size = new System.Drawing.Size(145, 24);
            this.cboReturnInvoiceType.TabIndex = 11;
            // 
            // cboInternalInvoiceType
            // 
            this.cboInternalInvoiceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInternalInvoiceType.DropDownWidth = 145;
            this.cboInternalInvoiceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInternalInvoiceType.Items.AddRange(new object[] {
            "--HĐ xuất bán NB--",
            "Chưa tạo hóa đơn",
            "Đã tạo hóa đơn"});
            this.cboInternalInvoiceType.Location = new System.Drawing.Point(224, 63);
            this.cboInternalInvoiceType.Name = "cboInternalInvoiceType";
            this.cboInternalInvoiceType.Size = new System.Drawing.Size(145, 24);
            this.cboInternalInvoiceType.TabIndex = 9;
            // 
            // cboInternalReturnInvoiceType
            // 
            this.cboInternalReturnInvoiceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInternalReturnInvoiceType.DropDownWidth = 145;
            this.cboInternalReturnInvoiceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInternalReturnInvoiceType.Items.AddRange(new object[] {
            "--HĐ xuất trả NB--",
            "Chưa tạo hóa đơn",
            "Đã tạo hóa đơn"});
            this.cboInternalReturnInvoiceType.Location = new System.Drawing.Point(370, 63);
            this.cboInternalReturnInvoiceType.Name = "cboInternalReturnInvoiceType";
            this.cboInternalReturnInvoiceType.Size = new System.Drawing.Size(145, 24);
            this.cboInternalReturnInvoiceType.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Tình trạng:";
            // 
            // cboPInvoiceType
            // 
            this.cboPInvoiceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPInvoiceType.DropDownWidth = 145;
            this.cboPInvoiceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPInvoiceType.Items.AddRange(new object[] {
            "--HĐ mua hàng--",
            "Chưa tạo hóa đơn",
            "Đã tạo hóa đơn"});
            this.cboPInvoiceType.Location = new System.Drawing.Point(78, 63);
            this.cboPInvoiceType.Name = "cboPInvoiceType";
            this.cboPInvoiceType.Size = new System.Drawing.Size(145, 24);
            this.cboPInvoiceType.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Công ty:";
            // 
            // cboCompany
            // 
            this.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(78, 9);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(359, 24);
            this.cboCompany.TabIndex = 1;
            this.cboCompany.SelectedValueChanged += new System.EventHandler(this.cboCompany_SelectedValueChanged);
            // 
            // cboCustomerIDList
            // 
            this.cboCustomerIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomerIDList.IsActivatedType = Library.AppCore.Constant.EnumType.IsActivatedType.ACTIVATED;
            this.cboCustomerIDList.IsRequireActive = false;
            this.cboCustomerIDList.Location = new System.Drawing.Point(78, 36);
            this.cboCustomerIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboCustomerIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboCustomerIDList.Name = "cboCustomerIDList";
            this.cboCustomerIDList.Size = new System.Drawing.Size(583, 24);
            this.cboCustomerIDList.TabIndex = 6;
            // 
            // dteSearchDate
            // 
            this.dteSearchDate.CustomFormat = "dd/MM/yyyy";
            this.dteSearchDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteSearchDate.Location = new System.Drawing.Point(516, 10);
            this.dteSearchDate.Name = "dteSearchDate";
            this.dteSearchDate.ShowUpDown = true;
            this.dteSearchDate.Size = new System.Drawing.Size(145, 22);
            this.dteSearchDate.TabIndex = 3;
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(95, 181);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(240, 24);
            this.cboStore.TabIndex = 19;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Enabled = false;
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(807, 63);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(128, 24);
            this.btnExportExcel.TabIndex = 13;
            this.btnExportExcel.Text = "   Xuất excel";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(672, 63);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(128, 24);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "   Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cboBRANCHID
            // 
            this.cboBRANCHID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBRANCHID.Location = new System.Drawing.Point(419, 177);
            this.cboBRANCHID.Margin = new System.Windows.Forms.Padding(0);
            this.cboBRANCHID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBRANCHID.Name = "cboBRANCHID";
            this.cboBRANCHID.Size = new System.Drawing.Size(358, 24);
            this.cboBRANCHID.TabIndex = 17;
            this.cboBRANCHID.SelectionChangeCommitted += new System.EventHandler(this.cboBRANCHID_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(345, 182);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 16;
            this.label1.Text = "Chi nhánh:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(443, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Thời gian:";
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemCreatePInvoice,
            this.mnuItemCreateInternalSISinvoice,
            this.mnuItemCreateInternalSI,
            this.mnuItemCreateInternalReturnSI,
            this.mnuItemReturnSI});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(216, 114);
            this.mnuAction.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // mnuItemCreatePInvoice
            // 
            this.mnuItemCreatePInvoice.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemCreatePInvoice.Image")));
            this.mnuItemCreatePInvoice.Name = "mnuItemCreatePInvoice";
            this.mnuItemCreatePInvoice.Size = new System.Drawing.Size(215, 22);
            this.mnuItemCreatePInvoice.Text = "Tạo hóa đơn mua hàng";
            this.mnuItemCreatePInvoice.Click += new System.EventHandler(this.mnuItemCreatePInvoice_Click);
            // 
            // mnuItemCreateInternalSISinvoice
            // 
            this.mnuItemCreateInternalSISinvoice.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemCreateInternalSISinvoice.Image")));
            this.mnuItemCreateInternalSISinvoice.Name = "mnuItemCreateInternalSISinvoice";
            this.mnuItemCreateInternalSISinvoice.Size = new System.Drawing.Size(215, 22);
            this.mnuItemCreateInternalSISinvoice.Text = "Tạo HDDT Xuất bán nội bộ";
            this.mnuItemCreateInternalSISinvoice.Click += new System.EventHandler(this.mnuItemCreateInternalSI_Click);
            // 
            // mnuItemCreateInternalSI
            // 
            this.mnuItemCreateInternalSI.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemCreateInternalSI.Image")));
            this.mnuItemCreateInternalSI.Name = "mnuItemCreateInternalSI";
            this.mnuItemCreateInternalSI.Size = new System.Drawing.Size(215, 22);
            this.mnuItemCreateInternalSI.Text = "Tạo HĐ xuất bán nội bộ";
            this.mnuItemCreateInternalSI.Click += new System.EventHandler(this.mnuItemCreateInternalSI_Click);
            // 
            // mnuItemCreateInternalReturnSI
            // 
            this.mnuItemCreateInternalReturnSI.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemCreateInternalReturnSI.Image")));
            this.mnuItemCreateInternalReturnSI.Name = "mnuItemCreateInternalReturnSI";
            this.mnuItemCreateInternalReturnSI.Size = new System.Drawing.Size(215, 22);
            this.mnuItemCreateInternalReturnSI.Text = "Tạo HĐ xuất trả nội bộ";
            this.mnuItemCreateInternalReturnSI.Visible = false;
            this.mnuItemCreateInternalReturnSI.Click += new System.EventHandler(this.mnuItemCreateInternalReturnSI_Click);
            // 
            // mnuItemReturnSI
            // 
            this.mnuItemReturnSI.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemReturnSI.Image")));
            this.mnuItemReturnSI.Name = "mnuItemReturnSI";
            this.mnuItemReturnSI.Size = new System.Drawing.Size(215, 22);
            this.mnuItemReturnSI.Text = "Tạo HĐ xuất trả NCC";
            this.mnuItemReturnSI.Visible = false;
            this.mnuItemReturnSI.Click += new System.EventHandler(this.mnuItemReturnSI_Click);
            // 
            // grdReport
            // 
            this.grdReport.ContextMenuStrip = this.mnuAction;
            this.grdReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdReport.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(5);
            this.grdReport.Location = new System.Drawing.Point(0, 92);
            this.grdReport.MainView = this.bagrvReport;
            this.grdReport.Margin = new System.Windows.Forms.Padding(5);
            this.grdReport.Name = "grdReport";
            this.grdReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repCheckBox,
            this.cboVAT});
            this.grdReport.Size = new System.Drawing.Size(986, 371);
            this.grdReport.TabIndex = 2;
            this.grdReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bagrvReport});
            // 
            // bagrvReport
            // 
            this.bagrvReport.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bagrvReport.Appearance.BandPanel.Options.UseFont = true;
            this.bagrvReport.Appearance.BandPanel.Options.UseTextOptions = true;
            this.bagrvReport.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bagrvReport.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bagrvReport.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bagrvReport.Appearance.FocusedRow.Options.UseFont = true;
            this.bagrvReport.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bagrvReport.Appearance.FooterPanel.Options.UseFont = true;
            this.bagrvReport.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.bagrvReport.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bagrvReport.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.bagrvReport.Appearance.GroupFooter.Options.UseFont = true;
            this.bagrvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.bagrvReport.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bagrvReport.Appearance.GroupRow.Options.UseFont = true;
            this.bagrvReport.Appearance.GroupRow.Options.UseForeColor = true;
            this.bagrvReport.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bagrvReport.Appearance.HeaderPanel.Options.UseFont = true;
            this.bagrvReport.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bagrvReport.Appearance.Preview.Options.UseFont = true;
            this.bagrvReport.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bagrvReport.Appearance.Row.Options.UseFont = true;
            this.bagrvReport.ColumnPanelRowHeight = 25;
            this.bagrvReport.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.bagrvReport.GridControl = this.grdReport;
            this.bagrvReport.GroupFormat = " [#image]{1} {2}";
            this.bagrvReport.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.bagrvReport.Name = "bagrvReport";
            this.bagrvReport.OptionsBehavior.AutoExpandAllGroups = true;
            this.bagrvReport.OptionsBehavior.SummariesIgnoreNullValues = true;
            this.bagrvReport.OptionsMenu.EnableColumnMenu = false;
            this.bagrvReport.OptionsMenu.EnableFooterMenu = false;
            this.bagrvReport.OptionsMenu.EnableGroupPanelMenu = false;
            this.bagrvReport.OptionsNavigation.UseTabKey = false;
            this.bagrvReport.OptionsPrint.AutoWidth = false;
            this.bagrvReport.OptionsPrint.PrintHeader = false;
            this.bagrvReport.OptionsPrint.UsePrintStyles = true;
            this.bagrvReport.OptionsView.ColumnAutoWidth = false;
            this.bagrvReport.OptionsView.ShowColumnHeaders = false;
            this.bagrvReport.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.bagrvReport.OptionsView.ShowFooter = true;
            this.bagrvReport.OptionsView.ShowGroupPanel = false;
            this.bagrvReport.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.bagrvReport_CellValueChanging);
            // 
            // repCheckBox
            // 
            this.repCheckBox.AutoHeight = false;
            this.repCheckBox.Name = "repCheckBox";
            this.repCheckBox.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repCheckBox.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repCheckBox.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // cboVAT
            // 
            this.cboVAT.AutoHeight = false;
            this.cboVAT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboVAT.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPEID", "Loại thuế"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPENAME", 60, "Mức thuế")});
            this.cboVAT.DisplayMember = "VATTYPENAME";
            this.cboVAT.Name = "cboVAT";
            this.cboVAT.NullText = "--Chọn thuế suất HĐ--";
            this.cboVAT.ReadOnly = true;
            this.cboVAT.ValueMember = "VATTYPEID";
            // 
            // frmConsignmentArisingReportTCDT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 463);
            this.Controls.Add(this.grdReport);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmConsignmentArisingReportTCDT";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Báo cáo bán hàng thẻ cào, tạo hóa đơn đầu vào";
            this.Load += new System.EventHandler(this.frmConsignmentArisingReport_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.mnuAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bagrvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVAT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSearch;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboBRANCHID;
        private System.Windows.Forms.Label label1;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStore;
        private System.Windows.Forms.DateTimePicker dteSearchDate;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomer cboCustomerIDList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboCompany;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCreatePInvoice;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCreateInternalSI;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCreateInternalReturnSI;
        private System.Windows.Forms.ToolStripMenuItem mnuItemReturnSI;
        private System.Windows.Forms.ComboBox cboReturnInvoiceType;
        private System.Windows.Forms.ComboBox cboInternalInvoiceType;
        private System.Windows.Forms.ComboBox cboInternalReturnInvoiceType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboPInvoiceType;
        private System.Windows.Forms.CheckBox chkViewDetail;
        private DevExpress.XtraGrid.GridControl grdReport;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bagrvReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repCheckBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboVAT;
        private System.Windows.Forms.CheckBox chk_IsSinvoice;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCreateInternalSISinvoice;
    }
}