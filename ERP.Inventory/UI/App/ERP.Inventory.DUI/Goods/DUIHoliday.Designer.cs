﻿using System.Collections.Generic;
namespace ERP.Inventory.DUI.Goods
{
    partial class DUIHoliday
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panelTableDetail = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tblList = new DevExpress.XtraGrid.GridControl();
            this.grdList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.tblPanelAciton = new System.Windows.Forms.TableLayoutPanel();
            this.pnlButton = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.lbType = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.txtNameEvent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nudHeSo = new System.Windows.Forms.NumericUpDown();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbCN = new System.Windows.Forms.CheckBox();
            this.cbT2 = new System.Windows.Forms.CheckBox();
            this.cbT3 = new System.Windows.Forms.CheckBox();
            this.cbT4 = new System.Windows.Forms.CheckBox();
            this.cbT5 = new System.Windows.Forms.CheckBox();
            this.cbT6 = new System.Windows.Forms.CheckBox();
            this.cbT7 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tblShop = new DevExpress.XtraGrid.GridControl();
            this.grdStore = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStoreId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoreName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.spePrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.txtNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.speOldPrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cboGoods = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.label7 = new System.Windows.Forms.Label();
            this.cboGoodsGroup = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.label2 = new System.Windows.Forms.Label();
            this.cboProductType = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAddAllGoods = new System.Windows.Forms.Button();
            this.btnRemoveGoodsAll = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnAddGoods = new System.Windows.Forms.Button();
            this.btnRemoveGoods = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDateEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.pnSearchStatus = new System.Windows.Forms.Panel();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.label11 = new System.Windows.Forms.Label();
            this.wsProductBarcode1 = new ERP.Inventory.PLC.Barcode.WSProductBarcode.WSProductBarcode();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panelTableDetail.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).BeginInit();
            this.pnlSearch.SuspendLayout();
            this.tblPanelAciton.SuspendLayout();
            this.pnlButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHeSo)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speOldPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.VistaTimeProperties)).BeginInit();
            this.pnSearchStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panelTableDetail);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(1303, 561);
            this.splitContainer1.SplitterDistance = 639;
            this.splitContainer1.TabIndex = 11;
            // 
            // panelTableDetail
            // 
            this.panelTableDetail.Controls.Add(this.groupBox2);
            this.panelTableDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTableDetail.Location = new System.Drawing.Point(0, 0);
            this.panelTableDetail.Name = "panelTableDetail";
            this.panelTableDetail.Size = new System.Drawing.Size(639, 561);
            this.panelTableDetail.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tblList);
            this.groupBox2.Controls.Add(this.pnlSearch);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(639, 561);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chi tiết";
            // 
            // tblList
            // 
            this.tblList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblList.Location = new System.Drawing.Point(3, 182);
            this.tblList.MainView = this.grdList;
            this.tblList.Name = "tblList";
            this.tblList.Size = new System.Drawing.Size(633, 376);
            this.tblList.TabIndex = 11;
            this.tblList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdList});
            this.tblList.Click += new System.EventHandler(this.tblList_Click);
            // 
            // grdList
            // 
            this.grdList.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdList.Appearance.FocusedRow.Options.UseFont = true;
            this.grdList.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdList.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdList.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdList.Appearance.Preview.Options.UseFont = true;
            this.grdList.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdList.Appearance.Row.Options.UseFont = true;
            this.grdList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn11,
            this.gridColumn12});
            this.grdList.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdList.GridControl = this.tblList;
            this.grdList.IndicatorWidth = 35;
            this.grdList.Name = "grdList";
            this.grdList.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdList.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdList.OptionsNavigation.UseTabKey = false;
            this.grdList.OptionsView.ColumnAutoWidth = false;
            this.grdList.OptionsView.ShowAutoFilterRow = true;
            this.grdList.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdList.OptionsView.ShowFooter = true;
            this.grdList.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Tên sự kiện";
            this.gridColumn1.FieldName = "NAME";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 162;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Hệ số";
            this.gridColumn2.FieldName = "RATE";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 94;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Từ ngày";
            this.gridColumn6.FieldName = "FROM_DATE";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 122;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.Caption = "Đến ngày";
            this.gridColumn7.FieldName = "TO_DATE";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 109;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Các thứ";
            this.gridColumn3.FieldName = "DAYS";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 85;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "Các siêu thị";
            this.gridColumn4.FieldName = "SHOP_LIST";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            this.gridColumn4.Width = 106;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "Người nhập";
            this.gridColumn5.FieldName = "USER_NAME";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            this.gridColumn5.Width = 110;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "eventId";
            this.gridColumn11.FieldName = "EVENT_LIST_ID";
            this.gridColumn11.Name = "gridColumn11";
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "SHOP_ID_LIST";
            this.gridColumn12.FieldName = "SHOP_ID_LIST";
            this.gridColumn12.Name = "gridColumn12";
            // 
            // pnlSearch
            // 
            this.pnlSearch.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlSearch.Controls.Add(this.tblPanelAciton);
            this.pnlSearch.Controls.Add(this.label5);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Location = new System.Drawing.Point(3, 18);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(633, 164);
            this.pnlSearch.TabIndex = 0;
            // 
            // tblPanelAciton
            // 
            this.tblPanelAciton.ColumnCount = 4;
            this.tblPanelAciton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblPanelAciton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tblPanelAciton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblPanelAciton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tblPanelAciton.Controls.Add(this.pnlButton, 0, 4);
            this.tblPanelAciton.Controls.Add(this.label6, 2, 1);
            this.tblPanelAciton.Controls.Add(this.dtpToDate, 3, 1);
            this.tblPanelAciton.Controls.Add(this.lbType, 0, 0);
            this.tblPanelAciton.Controls.Add(this.label4, 0, 1);
            this.tblPanelAciton.Controls.Add(this.dtpFromDate, 1, 1);
            this.tblPanelAciton.Controls.Add(this.txtNameEvent, 1, 0);
            this.tblPanelAciton.Controls.Add(this.label1, 0, 2);
            this.tblPanelAciton.Controls.Add(this.nudHeSo, 1, 2);
            this.tblPanelAciton.Controls.Add(this.flowLayoutPanel1, 0, 3);
            this.tblPanelAciton.Location = new System.Drawing.Point(0, 0);
            this.tblPanelAciton.Name = "tblPanelAciton";
            this.tblPanelAciton.RowCount = 5;
            this.tblPanelAciton.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblPanelAciton.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblPanelAciton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tblPanelAciton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tblPanelAciton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tblPanelAciton.Size = new System.Drawing.Size(637, 161);
            this.tblPanelAciton.TabIndex = 12;
            // 
            // pnlButton
            // 
            this.tblPanelAciton.SetColumnSpan(this.pnlButton, 4);
            this.pnlButton.Controls.Add(this.btnAdd);
            this.pnlButton.Controls.Add(this.btnModify);
            this.pnlButton.Controls.Add(this.btnDelete);
            this.pnlButton.Controls.Add(this.btnExit);
            this.pnlButton.Controls.Add(this.btnOK);
            this.pnlButton.Controls.Add(this.btnCancel);
            this.pnlButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButton.Location = new System.Drawing.Point(3, 120);
            this.pnlButton.Name = "pnlButton";
            this.pnlButton.Padding = new System.Windows.Forms.Padding(3);
            this.pnlButton.Size = new System.Drawing.Size(631, 38);
            this.pnlButton.TabIndex = 13;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(6, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(85, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(97, 6);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(85, 23);
            this.btnModify.TabIndex = 1;
            this.btnModify.Text = "Sửa";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(188, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 23);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(279, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(85, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Thoát";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(370, 6);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(85, 23);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "Lưu";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Visible = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click_1);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(461, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Hủy";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(321, 31);
            this.label6.Margin = new System.Windows.Forms.Padding(3);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(3);
            this.label6.Size = new System.Drawing.Size(121, 22);
            this.label6.TabIndex = 9;
            this.label6.Text = "Đến ngày";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpToDate.Enabled = false;
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(448, 31);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.ShowCheckBox = true;
            this.dtpToDate.Size = new System.Drawing.Size(186, 22);
            this.dtpToDate.TabIndex = 10;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbType.Location = new System.Drawing.Point(3, 3);
            this.lbType.Margin = new System.Windows.Forms.Padding(3);
            this.lbType.Name = "lbType";
            this.lbType.Padding = new System.Windows.Forms.Padding(3);
            this.lbType.Size = new System.Drawing.Size(121, 22);
            this.lbType.TabIndex = 0;
            this.lbType.Text = "Tên sự kiện";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(3);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(3);
            this.label4.Size = new System.Drawing.Size(121, 22);
            this.label4.TabIndex = 8;
            this.label4.Text = "Từ ngày";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpFromDate.Enabled = false;
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(130, 31);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowCheckBox = true;
            this.dtpFromDate.Size = new System.Drawing.Size(185, 22);
            this.dtpFromDate.TabIndex = 0;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // txtNameEvent
            // 
            this.tblPanelAciton.SetColumnSpan(this.txtNameEvent, 3);
            this.txtNameEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNameEvent.Enabled = false;
            this.txtNameEvent.Location = new System.Drawing.Point(130, 3);
            this.txtNameEvent.Name = "txtNameEvent";
            this.txtNameEvent.Size = new System.Drawing.Size(504, 22);
            this.txtNameEvent.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 59);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(3);
            this.label1.Size = new System.Drawing.Size(121, 23);
            this.label1.TabIndex = 12;
            this.label1.Text = "Hệ số";
            // 
            // nudHeSo
            // 
            this.nudHeSo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudHeSo.Enabled = false;
            this.nudHeSo.Location = new System.Drawing.Point(130, 59);
            this.nudHeSo.Name = "nudHeSo";
            this.nudHeSo.Size = new System.Drawing.Size(185, 22);
            this.nudHeSo.TabIndex = 13;
            // 
            // flowLayoutPanel1
            // 
            this.tblPanelAciton.SetColumnSpan(this.flowLayoutPanel1, 4);
            this.flowLayoutPanel1.Controls.Add(this.cbCN);
            this.flowLayoutPanel1.Controls.Add(this.cbT2);
            this.flowLayoutPanel1.Controls.Add(this.cbT3);
            this.flowLayoutPanel1.Controls.Add(this.cbT4);
            this.flowLayoutPanel1.Controls.Add(this.cbT5);
            this.flowLayoutPanel1.Controls.Add(this.cbT6);
            this.flowLayoutPanel1.Controls.Add(this.cbT7);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 88);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(631, 26);
            this.flowLayoutPanel1.TabIndex = 14;
            // 
            // cbCN
            // 
            this.cbCN.AutoSize = true;
            this.cbCN.Enabled = false;
            this.cbCN.Location = new System.Drawing.Point(3, 3);
            this.cbCN.Name = "cbCN";
            this.cbCN.Size = new System.Drawing.Size(78, 20);
            this.cbCN.TabIndex = 0;
            this.cbCN.Text = "Chủ nhật";
            this.cbCN.UseVisualStyleBackColor = true;
            // 
            // cbT2
            // 
            this.cbT2.AutoSize = true;
            this.cbT2.Enabled = false;
            this.cbT2.Location = new System.Drawing.Point(87, 3);
            this.cbT2.Name = "cbT2";
            this.cbT2.Size = new System.Drawing.Size(60, 20);
            this.cbT2.TabIndex = 1;
            this.cbT2.Text = "Thứ 2";
            this.cbT2.UseVisualStyleBackColor = true;
            // 
            // cbT3
            // 
            this.cbT3.AutoSize = true;
            this.cbT3.Enabled = false;
            this.cbT3.Location = new System.Drawing.Point(153, 3);
            this.cbT3.Name = "cbT3";
            this.cbT3.Size = new System.Drawing.Size(60, 20);
            this.cbT3.TabIndex = 2;
            this.cbT3.Text = "Thứ 3";
            this.cbT3.UseVisualStyleBackColor = true;
            // 
            // cbT4
            // 
            this.cbT4.AutoSize = true;
            this.cbT4.Enabled = false;
            this.cbT4.Location = new System.Drawing.Point(219, 3);
            this.cbT4.Name = "cbT4";
            this.cbT4.Size = new System.Drawing.Size(60, 20);
            this.cbT4.TabIndex = 3;
            this.cbT4.Text = "Thứ 4";
            this.cbT4.UseVisualStyleBackColor = true;
            // 
            // cbT5
            // 
            this.cbT5.AutoSize = true;
            this.cbT5.Enabled = false;
            this.cbT5.Location = new System.Drawing.Point(285, 3);
            this.cbT5.Name = "cbT5";
            this.cbT5.Size = new System.Drawing.Size(60, 20);
            this.cbT5.TabIndex = 4;
            this.cbT5.Text = "Thứ 5";
            this.cbT5.UseVisualStyleBackColor = true;
            // 
            // cbT6
            // 
            this.cbT6.AutoSize = true;
            this.cbT6.Enabled = false;
            this.cbT6.Location = new System.Drawing.Point(351, 3);
            this.cbT6.Name = "cbT6";
            this.cbT6.Size = new System.Drawing.Size(60, 20);
            this.cbT6.TabIndex = 5;
            this.cbT6.Text = "Thứ 6";
            this.cbT6.UseVisualStyleBackColor = true;
            // 
            // cbT7
            // 
            this.cbT7.AutoSize = true;
            this.cbT7.Enabled = false;
            this.cbT7.Location = new System.Drawing.Point(417, 3);
            this.cbT7.Name = "cbT7";
            this.cbT7.Size = new System.Drawing.Size(60, 20);
            this.cbT7.TabIndex = 6;
            this.cbT7.Text = "Thứ 7";
            this.cbT7.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(328, 123);
            this.label5.Margin = new System.Windows.Forms.Padding(3);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(3);
            this.label5.Size = new System.Drawing.Size(71, 22);
            this.label5.TabIndex = 9;
            this.label5.Text = "Đến ngày";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(660, 561);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tblShop, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 163F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(654, 540);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tblShop
            // 
            this.tblShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblShop.Enabled = false;
            this.tblShop.Location = new System.Drawing.Point(3, 130);
            this.tblShop.MainView = this.grdStore;
            this.tblShop.Name = "tblShop";
            this.tblShop.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.spePrice,
            this.txtNote,
            this.repositoryItemDateEdit1,
            this.speOldPrice,
            this.repositoryItemDateEdit2});
            this.tblShop.Size = new System.Drawing.Size(648, 157);
            this.tblShop.TabIndex = 12;
            this.tblShop.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdStore});
            // 
            // grdStore
            // 
            this.grdStore.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdStore.Appearance.FocusedRow.Options.UseFont = true;
            this.grdStore.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdStore.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdStore.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdStore.Appearance.Preview.Options.UseFont = true;
            this.grdStore.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdStore.Appearance.Row.Options.UseFont = true;
            this.grdStore.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStoreId,
            this.colStoreName});
            this.grdStore.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdStore.GridControl = this.tblShop;
            this.grdStore.IndicatorWidth = 35;
            this.grdStore.Name = "grdStore";
            this.grdStore.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdStore.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdStore.OptionsNavigation.UseTabKey = false;
            this.grdStore.OptionsView.ColumnAutoWidth = false;
            this.grdStore.OptionsView.ShowAutoFilterRow = true;
            this.grdStore.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdStore.OptionsView.ShowFooter = true;
            this.grdStore.OptionsView.ShowGroupPanel = false;
            this.grdStore.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.LiveHorzScroll;
            // 
            // colStoreId
            // 
            this.colStoreId.AppearanceHeader.Options.UseTextOptions = true;
            this.colStoreId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStoreId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStoreId.Caption = "Mã Sản Phẩm";
            this.colStoreId.FieldName = "VALUE";
            this.colStoreId.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStoreId.Name = "colStoreId";
            this.colStoreId.OptionsColumn.AllowEdit = false;
            this.colStoreId.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colStoreId.OptionsColumn.ReadOnly = true;
            this.colStoreId.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colStoreId.Visible = true;
            this.colStoreId.VisibleIndex = 0;
            this.colStoreId.Width = 202;
            // 
            // colStoreName
            // 
            this.colStoreName.AppearanceHeader.Options.UseTextOptions = true;
            this.colStoreName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStoreName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStoreName.Caption = "Tên Sản Phẩm";
            this.colStoreName.FieldName = "NAME";
            this.colStoreName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStoreName.Name = "colStoreName";
            this.colStoreName.OptionsColumn.AllowEdit = false;
            this.colStoreName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colStoreName.OptionsColumn.ReadOnly = true;
            this.colStoreName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colStoreName.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colStoreName.Visible = true;
            this.colStoreName.VisibleIndex = 1;
            this.colStoreName.Width = 381;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            this.chkIsSelect.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chkIsSelect.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // spePrice
            // 
            this.spePrice.AutoHeight = false;
            this.spePrice.DisplayFormat.FormatString = "#,##0.##";
            this.spePrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spePrice.EditFormat.FormatString = "#,##0.##";
            this.spePrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spePrice.Mask.EditMask = "\\d{0,12}";
            this.spePrice.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.spePrice.MaxLength = 13;
            this.spePrice.Name = "spePrice";
            this.spePrice.NullText = "0";
            // 
            // txtNote
            // 
            this.txtNote.AutoHeight = false;
            this.txtNote.MaxLength = 200;
            this.txtNote.Name = "txtNote";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // speOldPrice
            // 
            this.speOldPrice.AutoHeight = false;
            this.speOldPrice.DisplayFormat.FormatString = "#,##0.##";
            this.speOldPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.speOldPrice.EditFormat.FormatString = "#,##0.##";
            this.speOldPrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.speOldPrice.Name = "speOldPrice";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            this.repositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.72365F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.27635F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 134F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 199F));
            this.tableLayoutPanel2.Controls.Add(this.cboGoods, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cboGoodsGroup, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.cboProductType, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.64935F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.35065F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(648, 121);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // cboGoods
            // 
            this.cboGoods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboGoods.Enabled = false;
            this.cboGoods.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGoods.Location = new System.Drawing.Point(147, 39);
            this.cboGoods.Margin = new System.Windows.Forms.Padding(0);
            this.cboGoods.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboGoods.Name = "cboGoods";
            this.cboGoods.Padding = new System.Windows.Forms.Padding(3);
            this.cboGoods.Size = new System.Drawing.Size(167, 38);
            this.cboGoods.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Location = new System.Drawing.Point(3, 42);
            this.label7.Margin = new System.Windows.Forms.Padding(3);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(3);
            this.label7.Size = new System.Drawing.Size(141, 32);
            this.label7.TabIndex = 11;
            this.label7.Text = "Mặt hàng";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboGoodsGroup
            // 
            this.cboGoodsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboGoodsGroup.Enabled = false;
            this.cboGoodsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGoodsGroup.Location = new System.Drawing.Point(448, 0);
            this.cboGoodsGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboGoodsGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboGoodsGroup.Name = "cboGoodsGroup";
            this.cboGoodsGroup.Padding = new System.Windows.Forms.Padding(3);
            this.cboGoodsGroup.Size = new System.Drawing.Size(200, 39);
            this.cboGoodsGroup.TabIndex = 10;
            this.cboGoodsGroup.SelectionChangeCommitted += new System.EventHandler(this.cboGoodsGroup_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(317, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 39);
            this.label2.TabIndex = 9;
            this.label2.Text = "Nhóm hàng";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboProductType
            // 
            this.cboProductType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboProductType.Enabled = false;
            this.cboProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductType.Location = new System.Drawing.Point(147, 0);
            this.cboProductType.Margin = new System.Windows.Forms.Padding(0);
            this.cboProductType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboProductType.Name = "cboProductType";
            this.cboProductType.Padding = new System.Windows.Forms.Padding(3);
            this.cboProductType.Size = new System.Drawing.Size(167, 39);
            this.cboProductType.TabIndex = 8;
            this.cboProductType.SelectionChangeCommitted += new System.EventHandler(this.cboProductType_SelectionChangeCommitted);
            this.cboProductType.Load += new System.EventHandler(this.cboProductType_Load);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 39);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngành hàng";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.panel1, 4);
            this.panel1.Controls.Add(this.btnAddAllGoods);
            this.panel1.Controls.Add(this.btnRemoveGoodsAll);
            this.panel1.Controls.Add(this.btnImport);
            this.panel1.Controls.Add(this.btnAddGoods);
            this.panel1.Controls.Add(this.btnRemoveGoods);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 80);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(642, 38);
            this.panel1.TabIndex = 0;
            // 
            // btnAddAllGoods
            // 
            this.btnAddAllGoods.Enabled = false;
            this.btnAddAllGoods.Location = new System.Drawing.Point(174, 7);
            this.btnAddAllGoods.Name = "btnAddAllGoods";
            this.btnAddAllGoods.Size = new System.Drawing.Size(75, 23);
            this.btnAddAllGoods.TabIndex = 4;
            this.btnAddAllGoods.Text = "Tất cả";
            this.btnAddAllGoods.UseVisualStyleBackColor = true;
            this.btnAddAllGoods.Click += new System.EventHandler(this.btnAddAllGoods_Click);
            // 
            // btnRemoveGoodsAll
            // 
            this.btnRemoveGoodsAll.Enabled = false;
            this.btnRemoveGoodsAll.Location = new System.Drawing.Point(389, 7);
            this.btnRemoveGoodsAll.Name = "btnRemoveGoodsAll";
            this.btnRemoveGoodsAll.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveGoodsAll.TabIndex = 3;
            this.btnRemoveGoodsAll.Text = "Xóa tất cả";
            this.btnRemoveGoodsAll.UseVisualStyleBackColor = true;
            this.btnRemoveGoodsAll.Click += new System.EventHandler(this.btnRemoveGoodsAll_Click);
            // 
            // btnImport
            // 
            this.btnImport.Enabled = false;
            this.btnImport.Location = new System.Drawing.Point(495, 6);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(75, 23);
            this.btnImport.TabIndex = 2;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Visible = false;
            // 
            // btnAddGoods
            // 
            this.btnAddGoods.Enabled = false;
            this.btnAddGoods.Location = new System.Drawing.Point(70, 7);
            this.btnAddGoods.Name = "btnAddGoods";
            this.btnAddGoods.Size = new System.Drawing.Size(75, 23);
            this.btnAddGoods.TabIndex = 1;
            this.btnAddGoods.Text = "Thêm";
            this.btnAddGoods.UseVisualStyleBackColor = true;
            this.btnAddGoods.Click += new System.EventHandler(this.btnAddGoods_Click);
            // 
            // btnRemoveGoods
            // 
            this.btnRemoveGoods.Enabled = false;
            this.btnRemoveGoods.Location = new System.Drawing.Point(280, 8);
            this.btnRemoveGoods.Name = "btnRemoveGoods";
            this.btnRemoveGoods.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveGoods.TabIndex = 0;
            this.btnRemoveGoods.Text = "Xóa";
            this.btnRemoveGoods.UseVisualStyleBackColor = true;
            this.btnRemoveGoods.Click += new System.EventHandler(this.btnRemoveGoods_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gridControl1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 293);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(648, 244);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Danh sách siêu thị";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Enabled = false;
            this.gridControl1.Location = new System.Drawing.Point(3, 18);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemDateEdit3,
            this.repositoryItemSpinEdit2,
            this.repositoryItemDateEdit4});
            this.gridControl1.Size = new System.Drawing.Size(642, 223);
            this.gridControl1.TabIndex = 12;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.IndicatorWidth = 35;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView1.OptionsNavigation.UseTabKey = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.LiveHorzScroll;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn8.Caption = "Mã siêu thị";
            this.gridColumn8.FieldName = "SHOP_CODE";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 202;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Tên siêu thị";
            this.gridColumn9.FieldName = "NAME";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 381;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Id siêu thị";
            this.gridColumn10.FieldName = "SHOP_ID";
            this.gridColumn10.Name = "gridColumn10";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemCheckEdit1.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit1.Mask.EditMask = "\\d{0,12}";
            this.repositoryItemSpinEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEdit1.MaxLength = 13;
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            this.repositoryItemSpinEdit1.NullText = "0";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 200;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit3.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit3.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit3.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            this.repositoryItemDateEdit3.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.DisplayFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit2.EditFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // repositoryItemDateEdit4
            // 
            this.repositoryItemDateEdit4.AutoHeight = false;
            this.repositoryItemDateEdit4.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit4.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit4.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit4.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit4.Name = "repositoryItemDateEdit4";
            this.repositoryItemDateEdit4.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // pnSearchStatus
            // 
            this.pnSearchStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnSearchStatus.BackColor = System.Drawing.SystemColors.Control;
            this.pnSearchStatus.Controls.Add(this.marqueeProgressBarControl1);
            this.pnSearchStatus.Controls.Add(this.label11);
            this.pnSearchStatus.Location = new System.Drawing.Point(423, 236);
            this.pnSearchStatus.Name = "pnSearchStatus";
            this.pnSearchStatus.Size = new System.Drawing.Size(456, 88);
            this.pnSearchStatus.TabIndex = 12;
            this.pnSearchStatus.Visible = false;
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(25, 46);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Properties.LookAndFeel.SkinName = "The Asphalt World";
            this.marqueeProgressBarControl1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(406, 25);
            this.marqueeProgressBarControl1.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label11.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label11.Location = new System.Drawing.Point(24, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(416, 23);
            this.label11.TabIndex = 3;
            this.label11.Text = "Đang tìm kiếm. Vui lòng chờ trong giây lát...";
            // 
            // wsProductBarcode1
            // 
            this.wsProductBarcode1.Url = "http://localhost:3124/Inventory/Barcode/WSProductBarcode.asmx";
            this.wsProductBarcode1.UseDefaultCredentials = true;
            // 
            // DUIHoliday
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1303, 561);
            this.Controls.Add(this.pnSearchStatus);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "DUIHoliday";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Các ngày lễ, kỷ niệm, sự kiện";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DUIHoliday_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panelTableDetail.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).EndInit();
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.tblPanelAciton.ResumeLayout(false);
            this.tblPanelAciton.PerformLayout();
            this.pnlButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudHeSo)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speOldPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).EndInit();
            this.pnSearchStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        

        

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panelTableDetail;
        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Label lbType;
        private Library.AppControl.ComboBoxControl.ComboBoxBranch cboGoodsGroupa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tblPanelAciton;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraGrid.GridControl tblList;
        private DevExpress.XtraGrid.Views.Grid.GridView grdList;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private System.Windows.Forms.FlowLayoutPanel pnlButton;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Panel pnSearchStatus;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCancel;
        private PLC.Barcode.WSProductBarcode.WSProductBarcode wsProductBarcode1;
        private System.Windows.Forms.TextBox txtNameEvent;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudHeSo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.CheckBox cbCN;
        private System.Windows.Forms.CheckBox cbT2;
        private System.Windows.Forms.CheckBox cbT3;
        private System.Windows.Forms.CheckBox cbT4;
        private System.Windows.Forms.CheckBox cbT5;
        private System.Windows.Forms.CheckBox cbT6;
        private System.Windows.Forms.CheckBox cbT7;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString cboProductType;
        private System.Windows.Forms.Label label2;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString cboGoodsGroup;
        private System.Windows.Forms.Label label7;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString cboGoods;
        private System.Windows.Forms.Button btnAddAllGoods;
        private System.Windows.Forms.Button btnRemoveGoodsAll;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnAddGoods;
        private System.Windows.Forms.Button btnRemoveGoods;
        private DevExpress.XtraGrid.GridControl tblShop;
        private DevExpress.XtraGrid.Views.Grid.GridView grdStore;
        private DevExpress.XtraGrid.Columns.GridColumn colStoreId;
        private DevExpress.XtraGrid.Columns.GridColumn colStoreName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit spePrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtNote;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit speOldPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
    }
}