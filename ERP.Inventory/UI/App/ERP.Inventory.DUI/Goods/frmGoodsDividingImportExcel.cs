﻿using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class frmGoodsDividingImportExcel : Form
    {
        #region Variable

        private bool bolIsUpdate = false;
        private DataTable dtbSource = new DataTable();
	    private ERP.Inventory.PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = null;

        public bool IsUpdate
        {
            get { return bolIsUpdate; }
        }

        public DataTable SourceTable
        {
            get { return dtbSource; }
            set { dtbSource = value; }
        }

        private DataTable dtbGoodsResource;

        public DataTable DtbGoodsResource
        {
            get { return dtbGoodsResource; }
            set { dtbGoodsResource = value; }
        }

        private string goods_division_source_list_id;

        public string Goods_division_source_list_id
        {
            get { return goods_division_source_list_id; }
            set { goods_division_source_list_id = value; }
        }

	    #endregion


        #region Constructor 

        public frmGoodsDividingImportExcel()
        {
            InitializeComponent();
        }

        #endregion

        #region Event		 

        private void frmGoodsDmtbImportExcel_Load(object sender, EventArgs e)
        {
            string[] fieldNames = null;

            fieldNames = new string[] { "GOODS_CODE", "SHOP_CODE", "VALUE", "ISERROR", "NOTE" };

            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
           
                FormatGrid();
            
            grdData.DataSource = dtbSource;
            ValidateData(ref dtbSource);
            btnUpdate.Enabled = dtbSource.Rows.Count > 0;
        }

        private void findValueByCode(ref string goods_name, ref string goods_id, string goods_code)
        {
            goods_name = "";
            goods_id = "";
            if (dtbGoodsResource == null || dtbGoodsResource.Rows.Count == 0)
            {
                return;
            }
            for (int i = 0; i < dtbGoodsResource.Rows.Count; i++)
            {
                if (dtbGoodsResource.Rows[i]["goods_code"].ToString().Equals(goods_code))
                {
                    goods_name = dtbGoodsResource.Rows[i]["name"].ToString();
                    goods_id = dtbGoodsResource.Rows[i]["goods_id"].ToString();
                    return;
                }
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Chương trình sẽ chỉ lấy những dữ liệu hợp lệ. Bạn có muốn tiếp tục không?", "Thống báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                bolIsUpdate = true;
            }
            Close();
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {

        }

        private void grvIMEI_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            bool bolIsError = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["ISERROR"]));
            if (bolIsError)
                e.Appearance.BackColor = Color.Pink;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region Method

        private bool FormatGrid()
        {
            try
            {
                grvIMEI.Columns["GOODS_CODE"].Caption = "Mã hàng";
                grvIMEI.Columns["SHOP_CODE"].Caption = "Từ kho";
                grvIMEI.Columns["VALUE"].Caption = "Số điều chỉnh";
                grvIMEI.Columns["ISERROR"].Caption = "Lỗi";
                grvIMEI.Columns["NOTE"].Caption = "Ghi lỗi";

                grvIMEI.OptionsView.ColumnAutoWidth = false;
                grvIMEI.Columns["GOODS_CODE"].Width = 150;
                grvIMEI.Columns["SHOP_CODE"].Width = 100;
                grvIMEI.Columns["VALUE"].Width = 180;
                grvIMEI.Columns["ISERROR"].Width = 80;
                grvIMEI.Columns["NOTE"].Width = 500;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsError = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsError.ValueChecked = true;
                chkCheckBoxIsError.ValueUnchecked = false;
                grvIMEI.Columns["ISERROR"].ColumnEdit = chkCheckBoxIsError;

                grvIMEI.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grvIMEI.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grvIMEI.OptionsView.ShowAutoFilterRow = true;
                grvIMEI.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grvIMEI.OptionsView.ShowFooter = true;
                grvIMEI.OptionsView.ShowGroupPanel = false;
                grvIMEI.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grvIMEI.ColumnPanelRowHeight = 40;

                grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "ISERROR";
                grvIMEI.Columns["NOTE"].SummaryItem.DisplayFormat = "{0:n0}";
                grvIMEI.Columns["NOTE"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;

               //grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "NOTE";
                grvIMEI.Columns["ISERROR"].SummaryItem.DisplayFormat = "Tổng số dòng:";
                grvIMEI.Columns["ISERROR"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool ValidateData(ref DataTable dtbSource)
        {
            dtbSource.Rows.RemoveAt(0);
            int i = 0;
            dtbSource.Columns[i++].ColumnName = "GOODS_CODE";
            dtbSource.Columns[i++].ColumnName = "SHOP_CODE";
            dtbSource.Columns[i++].ColumnName = "VALUE";

            DataColumn dtcIsError = new DataColumn("ISERROR", typeof(bool));
            dtcIsError.DefaultValue = false;
            dtbSource.Columns.Add(dtcIsError);
            DataColumn dtcNote = new DataColumn("NOTE", typeof(string));
            dtbSource.Columns.Add(dtcNote);
            DataColumn dtcStockId = new DataColumn("GOODS_ID", typeof(string));
            dtbSource.Columns.Add(dtcStockId);
            dtbSource.Columns["GOODS_ID"].ColumnMapping = MappingType.Hidden;
            DataColumn NAME = new DataColumn("NAME", typeof(string));
            dtbSource.Columns.Add(NAME);
            dtbSource.Columns["NAME"].ColumnMapping = MappingType.Hidden;
            DataColumn ORG_VALUE = new DataColumn("ORG_VALUE", typeof(string));
            dtbSource.Columns.Add(ORG_VALUE);
            dtbSource.Columns["ORG_VALUE"].ColumnMapping = MappingType.Hidden;
            DataColumn OLD_VALUE = new DataColumn("OLD_VALUE", typeof(string));
            dtbSource.Columns.Add(OLD_VALUE);
            dtbSource.Columns["OLD_VALUE"].ColumnMapping = MappingType.Hidden;
            DataColumn MODIFY_DATE = new DataColumn("MODIFY_DATE", typeof(string));
            dtbSource.Columns.Add(MODIFY_DATE);
            dtbSource.Columns["MODIFY_DATE"].ColumnMapping = MappingType.Hidden;
            DataColumn GOODS_DIVISION_SOURCE_LIST_ID = new DataColumn("GOODS_DIVISION_SOURCE_LIST_ID", typeof(string));
            dtbSource.Columns.Add(GOODS_DIVISION_SOURCE_LIST_ID);
            dtbSource.Columns["GOODS_DIVISION_SOURCE_LIST_ID"].ColumnMapping = MappingType.Hidden;
            dtbSource.Columns["GOODS_CODE"].SetOrdinal(dtbSource.Columns["GOODS_CODE"].Ordinal + 1);

            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();
           
            foreach (DataRow row in dtbSource.Rows)
            {
                validateRowImport(row);
            }
            dtbSource.AcceptChanges();
            return true;
        }

        private void validateRowImport(DataRow row)
        {
            string errNote = "";
            String goods_name = "";
            String goods_id = "";
            
            long value;

            if (row["VALUE"].ToString().Equals(null)
                || row["VALUE"].ToString().Equals("")
                || !long.TryParse(row["VALUE"].ToString(), out value))
            {
                errNote += "Không đúng định dạng số " + row["VALUE"].ToString() + " ; ";
            }
            findValueByCode(ref goods_name, ref goods_id, row["GOODS_CODE"].ToString());
            if (goods_id.Equals(null)
                || goods_id.Equals(""))
            {
                errNote += "không tìm thấy hàng có mã (" + row["GOODS_CODE"].ToString() + ") trong danh mục chia hàng ; ";
            }
          

            if (!errNote.Equals(""))
            {
                row["IsError"] = true;
                row["NOTE"] = errNote;
            }
            else
            {
                row["GOODS_DIVISION_SOURCE_LIST_ID"] = goods_division_source_list_id;
                row["NAME"] = goods_name;
                row["GOODS_ID"] = goods_id;
                row["ORG_VALUE"] = row["VALUE"].ToString();
                row["OLD_VALUE"] = row["VALUE"].ToString();
            }
            
               
        }

        #endregion
    }

}
