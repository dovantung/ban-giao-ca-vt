﻿using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class frmGoodsImportExcel : Form
    {
        #region Variable

        private bool bolIsUpdate = false;
        private DataTable dtbSource = new DataTable();
	    private ERP.Inventory.PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = null;

        private ImportMode curImportMode;

        public ImportMode CurImportMode
        {
            get { return curImportMode; }
            set { curImportMode = value; }
        }

        public bool IsUpdate
        {
            get { return bolIsUpdate; }
        }

        public DataTable SourceTable
        {
            get { return dtbSource; }
            set { dtbSource = value; }
        }

        private DataTable dtbShopResource;

        public DataTable DtbShopResource
        {
            get { return dtbShopResource; }
            set { dtbShopResource = value; }
        }

        private DataTable dtbCoupleGoodsResource;

        public DataTable DtbCoupleGoodsResource
        {
            get { return dtbCoupleGoodsResource; }
            set { dtbCoupleGoodsResource = value; }
        }

        private string goods_division_history_id;

        public string Goods_division_history_id
        {
            get { return goods_division_history_id; }
            set { goods_division_history_id = value; }
        }



        private List<object[]> lsImport;

        public List<object[]> LsImport
        {
            get { return lsImport; }
            set { lsImport = value; }
        }

	    #endregion


        #region Constructor 

        public frmGoodsImportExcel()
        {
            InitializeComponent();
        }

        #endregion

        #region Event		 

        private void frmGoodsImportExcel_Load(object sender, EventArgs e)
        {
            string[] fieldNames = null;
            if (curImportMode == ImportMode.IMPORT) {
                fieldNames = new string[] { "COUPLE_GOODS", "SHOP_CODE", "CHECK_DANHMUC", "ISERROR", "NOTE" };
            }
            
           
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
            if (curImportMode == ImportMode.IMPORT)
            {
                FormatGrid();
            }
            else if (curImportMode == ImportMode.ALL_IMPORT)
            {
                FormatGridAllImport();
            }
            
            grdData.DataSource = dtbSource;
            ValidateData(ref dtbSource);
            btnUpdate.Enabled = dtbSource.Rows.Count > 0;
        }

        private bool FormatGridAllImport()
        {
            try
            {
                
                grvIMEI.Columns["GOODS_CODE"].Caption = "MÃ GHÉP";
                grvIMEI.Columns["VALUE"].Caption = "DMTB (%)";
                grvIMEI.Columns["FROM_DATE"].Caption = "TỪ NGÀY";
                grvIMEI.Columns["TO_DATE"].Caption = "ĐẾN NGÀY";
                grvIMEI.Columns["ISERROR"].Caption = "Lỗi";
                grvIMEI.Columns["NOTE"].Caption = "Ghi lỗi";

                grvIMEI.OptionsView.ColumnAutoWidth = false;
                grvIMEI.Columns["GOODS_CODE"].Width = 230;
                grvIMEI.Columns["VALUE"].Width = 100;
                grvIMEI.Columns["FROM_DATE"].Width = 180;
                grvIMEI.Columns["TO_DATE"].Width = 180;
                grvIMEI.Columns["ISERROR"].Width = 80;
                grvIMEI.Columns["NOTE"].Width = 500;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsError = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsError.ValueChecked = true;
                chkCheckBoxIsError.ValueUnchecked = false;
                grvIMEI.Columns["ISERROR"].ColumnEdit = chkCheckBoxIsError;

                grvIMEI.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grvIMEI.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grvIMEI.OptionsView.ShowAutoFilterRow = true;
                grvIMEI.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grvIMEI.OptionsView.ShowFooter = true;
                grvIMEI.OptionsView.ShowGroupPanel = false;
                grvIMEI.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grvIMEI.ColumnPanelRowHeight = 40;

                grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "ISERROR";
                grvIMEI.Columns["NOTE"].SummaryItem.DisplayFormat = "{0:n0}";
                grvIMEI.Columns["NOTE"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;

                //grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "NOTE";
                grvIMEI.Columns["ISERROR"].SummaryItem.DisplayFormat = "Tổng số dòng:";
                grvIMEI.Columns["ISERROR"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private string findShopIdByCode(string shopCode)
        {
            if (dtbShopResource == null || dtbShopResource.Rows.Count == 0)
            {
                return "";
            }
            for (int i = 0; i < dtbShopResource.Rows.Count; i++)
            {
                if (dtbShopResource.Rows[i]["shop_code"].ToString().Equals(shopCode))
                {
                    return dtbShopResource.Rows[i]["stock_id"].ToString();
                }
            }
            return "";
        }

        private string findCoupleGoodsByCode(string goodsCode)
        {
            if (dtbCoupleGoodsResource == null || dtbCoupleGoodsResource.Rows.Count == 0)
            {
                return "";
            }
            for (int i = 0; i < dtbCoupleGoodsResource.Rows.Count; i++)
            {
                if (dtbCoupleGoodsResource.Rows[i]["couple_goods"].ToString().Equals(goodsCode))
                {
                    return dtbCoupleGoodsResource.Rows[i]["couple_goods"].ToString();
                }
            }
            return "";
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Chương trình sẽ chỉ lấy những dữ liệu hợp lệ. Bạn có muốn tiếp tục không?", "Thống báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                bolIsUpdate = true;
            }
            Close();
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {

        }

        private void grvIMEI_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            bool bolIsError = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["ISERROR"]));
            if (bolIsError)
                e.Appearance.BackColor = Color.Pink;
            //else if (category == "N/A")
            //    e.Appearance.BackColor = Color.Pink;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region Method

        private bool FormatGrid()
        {
            try
            {
                grvIMEI.Columns["COUPLE_GOODS"].Caption = "MÃ HÀNG GHÉP";
                grvIMEI.Columns["SHOP_CODE"].Caption = "MÃ SIÊU THỊ";
                grvIMEI.Columns["CHECK_DANHMUC"].Caption = "DANH MỤC";
                grvIMEI.Columns["ISERROR"].Caption = "Lỗi";
                grvIMEI.Columns["NOTE"].Caption = "Ghi lỗi";

                grvIMEI.OptionsView.ColumnAutoWidth = false;
                grvIMEI.Columns["COUPLE_GOODS"].Width = 150;
                grvIMEI.Columns["SHOP_CODE"].Width = 230;
                grvIMEI.Columns["CHECK_DANHMUC"].Width = 100;
                grvIMEI.Columns["ISERROR"].Width = 80;
                grvIMEI.Columns["NOTE"].Width = 500;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsError = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsError.ValueChecked = true;
                chkCheckBoxIsError.ValueUnchecked = false;
                grvIMEI.Columns["ISERROR"].ColumnEdit = chkCheckBoxIsError;

                grvIMEI.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grvIMEI.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grvIMEI.OptionsView.ShowAutoFilterRow = true;
                grvIMEI.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grvIMEI.OptionsView.ShowFooter = true;
                grvIMEI.OptionsView.ShowGroupPanel = false;
                grvIMEI.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grvIMEI.ColumnPanelRowHeight = 40;

                grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "ISERROR";
                grvIMEI.Columns["NOTE"].SummaryItem.DisplayFormat = "{0:n0}";
                grvIMEI.Columns["NOTE"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;

               //grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "NOTE";
                grvIMEI.Columns["ISERROR"].SummaryItem.DisplayFormat = "Tổng số dòng:";
                grvIMEI.Columns["ISERROR"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool ValidateData(ref DataTable dtbSource)
        {
            dtbSource.Rows.RemoveAt(0);
            int i = 0;
            dtbSource.Columns[i++].ColumnName = "COUPLE_GOODS";
            dtbSource.Columns[i++].ColumnName = "SHOP_CODE";
            dtbSource.Columns[i++].ColumnName = "CHECK_DANHMUC";

            DataColumn dtcIsError = new DataColumn("ISERROR", typeof(bool));
            dtcIsError.DefaultValue = false;
            dtbSource.Columns.Add(dtcIsError);
            DataColumn dtcNote = new DataColumn("NOTE", typeof(string));
            dtbSource.Columns.Add(dtcNote);
            DataColumn dtcStockId = new DataColumn("STOCK_ID", typeof(string));
            dtbSource.Columns.Add(dtcStockId);
            dtbSource.Columns["STOCK_ID"].ColumnMapping = MappingType.Hidden;
            dtbSource.Columns["COUPLE_GOODS"].SetOrdinal(dtbSource.Columns["COUPLE_GOODS"].Ordinal + 1); 
            lsImport = new List<object[]>();
            foreach (DataRow row in dtbSource.Rows)
            {
                validateRowImport(row);
            }
            dtbSource.AcceptChanges();
            return true;
        }

        private void validateRowImport(DataRow row)
        {
            string errNote = "";

            string goodsCode = findCoupleGoodsByCode(row["COUPLE_GOODS"].ToString());
            if (goodsCode == null || goodsCode == "")
            {
                errNote += "Không tìm thấy hàng có mã " + row["COUPLE_GOODS"].ToString() + " ; ";
            }

            string stockId = findShopIdByCode(row["SHOP_CODE"].ToString());
            long value;
            if (stockId == null || stockId == "")
            {
                errNote += "Không tìm thấy mã siêu thị " + row["SHOP_CODE"].ToString() + " ; ";
            }
            if (row["CHECK_DANHMUC"].ToString().Equals(null)
                || row["CHECK_DANHMUC"].ToString().Equals("")
                || !long.TryParse(row["CHECK_DANHMUC"].ToString(), out value))
            {
                errNote += "Không đúng định dạng số " + row["CHECK_DANHMUC"].ToString() + " ; ";
            }
            

            if (!errNote.Equals(""))
            {
                row["IsError"] = true;
                row["NOTE"] = errNote;
            }
            else
            {
                row["STOCK_ID"] = stockId;
                string user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                string user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                object[] objKeywordsImport = new object[]{
                            "@couple_goods", row["COUPLE_GOODS"].ToString() == null ? "":row["COUPLE_GOODS"].ToString(),
                            "@check_danhmuc",row["CHECK_DANHMUC"].ToString() == null ? "":row["CHECK_DANHMUC"].ToString(),
                            "@stock_id",stockId,
                            "@goods_division_history_id",goods_division_history_id
                        };
                lsImport.Add(objKeywordsImport);
            }
            
               
        }

        #endregion
    }

}
