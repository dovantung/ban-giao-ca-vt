﻿namespace ERP.Inventory.DUI.Goods
{
    partial class DUISameLevelDividing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnNewRotate = new System.Windows.Forms.Button();
            this.btnCancelRotate = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tblRotateList = new DevExpress.XtraGrid.GridControl();
            this.grdRotateList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreateDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.spePrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.txtNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.speOldPrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.tabDetail = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlShop = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tblShopList = new DevExpress.XtraGrid.GridControl();
            this.grdShopList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemSpinEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDateEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnDownTemplate = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.lovShop = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlGoods = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tblGoodsList = new DevExpress.XtraGrid.GridControl();
            this.grdGoodsList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDateEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlButton = new System.Windows.Forms.FlowLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnAddGoods = new System.Windows.Forms.Button();
            this.btnAllGoods = new System.Windows.Forms.Button();
            this.btnRemoveGoods = new System.Windows.Forms.Button();
            this.btnRemoveAllGoods = new System.Windows.Forms.Button();
            this.btnImportGoods = new System.Windows.Forms.Button();
            this.btnDownTemplateGoods = new System.Windows.Forms.Button();
            this.lovGoods = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.cboGoodsGroup = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboProduct = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.lbType = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cboOverDaysRedundanceShop = new System.Windows.Forms.ComboBox();
            this.txtShopLackDays = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.txtRecyclingName = new System.Windows.Forms.TextBox();
            this.txtShopRedundanceDays = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pnlLackShop = new System.Windows.Forms.GroupBox();
            this.tblLackShop = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlRedundanceShop = new System.Windows.Forms.GroupBox();
            this.tblRedundanceShop = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnCompute = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.pnList = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tblList = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemSpinEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDateEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.cboPage = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnRecycling = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.pnSearchStatus = new System.Windows.Forms.Panel();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.label11 = new System.Windows.Forms.Label();
            this.tmrFormatFlex = new System.Windows.Forms.Timer(this.components);
            this.contextMenuAll = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripAll = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMB = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMN = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblRotateList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdRotateList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speOldPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).BeginInit();
            this.tabDetail.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlShop.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblShopList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdShopList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.VistaTimeProperties)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnlGoods.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblGoodsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdGoodsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.VistaTimeProperties)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.pnlButton.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.pnlLackShop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblLackShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.pnlRedundanceShop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblRedundanceShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.panel8.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel11.SuspendLayout();
            this.pnList.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit8.VistaTimeProperties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.pnSearchStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            this.contextMenuAll.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel4);
            this.splitContainer1.Panel1.Controls.Add(this.tblRotateList);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabDetail);
            this.splitContainer1.Size = new System.Drawing.Size(1304, 685);
            this.splitContainer1.SplitterDistance = 378;
            this.splitContainer1.TabIndex = 0;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.btnNewRotate);
            this.flowLayoutPanel4.Controls.Add(this.btnCancelRotate);
            this.flowLayoutPanel4.Controls.Add(this.btnClose);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(0, 653);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Padding = new System.Windows.Forms.Padding(3);
            this.flowLayoutPanel4.Size = new System.Drawing.Size(378, 32);
            this.flowLayoutPanel4.TabIndex = 16;
            // 
            // btnNewRotate
            // 
            this.btnNewRotate.Location = new System.Drawing.Point(6, 6);
            this.btnNewRotate.Name = "btnNewRotate";
            this.btnNewRotate.Size = new System.Drawing.Size(79, 23);
            this.btnNewRotate.TabIndex = 0;
            this.btnNewRotate.Text = "Thêm mới";
            this.btnNewRotate.UseVisualStyleBackColor = true;
            this.btnNewRotate.Click += new System.EventHandler(this.btnNewRotate_Click);
            // 
            // btnCancelRotate
            // 
            this.btnCancelRotate.Location = new System.Drawing.Point(91, 6);
            this.btnCancelRotate.Name = "btnCancelRotate";
            this.btnCancelRotate.Size = new System.Drawing.Size(79, 23);
            this.btnCancelRotate.TabIndex = 6;
            this.btnCancelRotate.Text = "Hủy";
            this.btnCancelRotate.UseVisualStyleBackColor = true;
            this.btnCancelRotate.Click += new System.EventHandler(this.btnCancelRotate_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(176, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(79, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Thoát";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tblRotateList
            // 
            this.tblRotateList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblRotateList.Location = new System.Drawing.Point(0, 0);
            this.tblRotateList.MainView = this.grdRotateList;
            this.tblRotateList.Name = "tblRotateList";
            this.tblRotateList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.spePrice,
            this.txtNote,
            this.repositoryItemDateEdit1,
            this.speOldPrice,
            this.repositoryItemDateEdit2});
            this.tblRotateList.Size = new System.Drawing.Size(378, 685);
            this.tblRotateList.TabIndex = 11;
            this.tblRotateList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdRotateList});
            this.tblRotateList.Click += new System.EventHandler(this.tblRotateList_Click);
            // 
            // grdRotateList
            // 
            this.grdRotateList.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdRotateList.Appearance.FocusedRow.Options.UseFont = true;
            this.grdRotateList.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdRotateList.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdRotateList.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdRotateList.Appearance.Preview.Options.UseFont = true;
            this.grdRotateList.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdRotateList.Appearance.Row.Options.UseFont = true;
            this.grdRotateList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colCreateDate});
            this.grdRotateList.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdRotateList.GridControl = this.tblRotateList;
            this.grdRotateList.IndicatorWidth = 35;
            this.grdRotateList.Name = "grdRotateList";
            this.grdRotateList.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdRotateList.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdRotateList.OptionsNavigation.UseTabKey = false;
            this.grdRotateList.OptionsView.ColumnAutoWidth = false;
            this.grdRotateList.OptionsView.ShowAutoFilterRow = true;
            this.grdRotateList.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdRotateList.OptionsView.ShowFooter = true;
            this.grdRotateList.OptionsView.ShowGroupPanel = false;
            this.grdRotateList.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.LiveHorzScroll;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colName.Caption = "Tên lần điều chuyển";
            this.colName.FieldName = "NAME";
            this.colName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 193;
            // 
            // colCreateDate
            // 
            this.colCreateDate.AppearanceCell.Options.UseTextOptions = true;
            this.colCreateDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreateDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreateDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreateDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCreateDate.Caption = "Thời gian thực hiện";
            this.colCreateDate.FieldName = "CREATE_DATE";
            this.colCreateDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCreateDate.Name = "colCreateDate";
            this.colCreateDate.OptionsColumn.AllowEdit = false;
            this.colCreateDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCreateDate.OptionsColumn.ReadOnly = true;
            this.colCreateDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCreateDate.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colCreateDate.Visible = true;
            this.colCreateDate.VisibleIndex = 1;
            this.colCreateDate.Width = 150;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            this.chkIsSelect.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chkIsSelect.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // spePrice
            // 
            this.spePrice.AutoHeight = false;
            this.spePrice.DisplayFormat.FormatString = "#,##0.##";
            this.spePrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spePrice.EditFormat.FormatString = "#,##0.##";
            this.spePrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spePrice.Mask.EditMask = "\\d{0,12}";
            this.spePrice.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.spePrice.MaxLength = 13;
            this.spePrice.Name = "spePrice";
            this.spePrice.NullText = "0";
            // 
            // txtNote
            // 
            this.txtNote.AutoHeight = false;
            this.txtNote.MaxLength = 200;
            this.txtNote.Name = "txtNote";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // speOldPrice
            // 
            this.speOldPrice.AutoHeight = false;
            this.speOldPrice.DisplayFormat.FormatString = "#,##0.##";
            this.speOldPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.speOldPrice.EditFormat.FormatString = "#,##0.##";
            this.speOldPrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.speOldPrice.Name = "speOldPrice";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            this.repositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // tabDetail
            // 
            this.tabDetail.Controls.Add(this.tabPage1);
            this.tabDetail.Controls.Add(this.tabPage2);
            this.tabDetail.Controls.Add(this.tabPage3);
            this.tabDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabDetail.Location = new System.Drawing.Point(0, 0);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.SelectedIndex = 0;
            this.tabDetail.Size = new System.Drawing.Size(922, 685);
            this.tabDetail.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(914, 659);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Nguồn chia hàng";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnlShop);
            this.panel2.Controls.Add(this.pnlGoods);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 86);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(906, 568);
            this.panel2.TabIndex = 1;
            // 
            // pnlShop
            // 
            this.pnlShop.Controls.Add(this.panel4);
            this.pnlShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlShop.Location = new System.Drawing.Point(0, 318);
            this.pnlShop.Name = "pnlShop";
            this.pnlShop.Padding = new System.Windows.Forms.Padding(0);
            this.pnlShop.Size = new System.Drawing.Size(906, 250);
            this.pnlShop.TabIndex = 1;
            this.pnlShop.TabStop = false;
            this.pnlShop.Text = "Các siêu thị";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.tblShopList);
            this.panel4.Controls.Add(this.tableLayoutPanel3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 13);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(906, 237);
            this.panel4.TabIndex = 0;
            // 
            // tblShopList
            // 
            this.tblShopList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblShopList.Location = new System.Drawing.Point(0, 66);
            this.tblShopList.MainView = this.grdShopList;
            this.tblShopList.Name = "tblShopList";
            this.tblShopList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemSpinEdit3,
            this.repositoryItemTextEdit2,
            this.repositoryItemDateEdit5,
            this.repositoryItemSpinEdit4,
            this.repositoryItemDateEdit6});
            this.tblShopList.Size = new System.Drawing.Size(904, 169);
            this.tblShopList.TabIndex = 15;
            this.tblShopList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdShopList});
            // 
            // grdShopList
            // 
            this.grdShopList.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdShopList.Appearance.FocusedRow.Options.UseFont = true;
            this.grdShopList.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdShopList.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdShopList.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdShopList.Appearance.Preview.Options.UseFont = true;
            this.grdShopList.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdShopList.Appearance.Row.Options.UseFont = true;
            this.grdShopList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn7});
            this.grdShopList.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdShopList.GridControl = this.tblShopList;
            this.grdShopList.IndicatorWidth = 35;
            this.grdShopList.Name = "grdShopList";
            this.grdShopList.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdShopList.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdShopList.OptionsNavigation.UseTabKey = false;
            this.grdShopList.OptionsView.ColumnAutoWidth = false;
            this.grdShopList.OptionsView.ShowAutoFilterRow = true;
            this.grdShopList.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdShopList.OptionsView.ShowFooter = true;
            this.grdShopList.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Mã siêu thị";
            this.gridColumn1.FieldName = "SHOP_CODE";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 362;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.Caption = "Tên siêu thị";
            this.gridColumn7.FieldName = "SHOP_NAME";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            this.gridColumn7.Width = 525;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemCheckEdit2.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.DisplayFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit3.EditFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit3.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit3.Mask.EditMask = "\\d{0,12}";
            this.repositoryItemSpinEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEdit3.MaxLength = 13;
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            this.repositoryItemSpinEdit3.NullText = "0";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.MaxLength = 200;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemDateEdit5
            // 
            this.repositoryItemDateEdit5.AutoHeight = false;
            this.repositoryItemDateEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit5.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit5.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit5.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit5.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit5.Name = "repositoryItemDateEdit5";
            this.repositoryItemDateEdit5.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemSpinEdit4
            // 
            this.repositoryItemSpinEdit4.AutoHeight = false;
            this.repositoryItemSpinEdit4.DisplayFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit4.EditFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit4.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit4.Name = "repositoryItemSpinEdit4";
            // 
            // repositoryItemDateEdit6
            // 
            this.repositoryItemDateEdit6.AutoHeight = false;
            this.repositoryItemDateEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit6.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit6.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit6.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit6.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit6.Name = "repositoryItemDateEdit6";
            this.repositoryItemDateEdit6.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.lovShop, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(904, 66);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.panel7);
            this.flowLayoutPanel1.Controls.Add(this.btnImport);
            this.flowLayoutPanel1.Controls.Add(this.btnDownTemplate);
            this.flowLayoutPanel1.Controls.Add(this.btnAll);
            this.flowLayoutPanel1.Controls.Add(this.btnAdd);
            this.flowLayoutPanel1.Controls.Add(this.btnRemove);
            this.flowLayoutPanel1.Controls.Add(this.btnRemoveAll);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 31);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(3);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(898, 32);
            this.flowLayoutPanel1.TabIndex = 15;
            // 
            // panel7
            // 
            this.panel7.Location = new System.Drawing.Point(6, 6);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(189, 27);
            this.panel7.TabIndex = 4;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(201, 6);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(79, 23);
            this.btnImport.TabIndex = 5;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnDownTemplate
            // 
            this.btnDownTemplate.Location = new System.Drawing.Point(286, 6);
            this.btnDownTemplate.Name = "btnDownTemplate";
            this.btnDownTemplate.Size = new System.Drawing.Size(75, 23);
            this.btnDownTemplate.TabIndex = 12;
            this.btnDownTemplate.Text = "File mẫu";
            this.btnDownTemplate.UseVisualStyleBackColor = true;
            this.btnDownTemplate.Click += new System.EventHandler(this.btnDownTemplate_Click);
            // 
            // btnAll
            // 
            this.btnAll.ContextMenuStrip = this.contextMenuAll;
            this.btnAll.Location = new System.Drawing.Point(367, 6);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(79, 23);
            this.btnAll.TabIndex = 1;
            this.btnAll.Text = "Tất cả";
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(452, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(79, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(537, 6);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(79, 23);
            this.btnRemove.TabIndex = 2;
            this.btnRemove.Text = "Xóa";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Location = new System.Drawing.Point(622, 6);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(79, 23);
            this.btnRemoveAll.TabIndex = 3;
            this.btnRemoveAll.Text = "Xóa tất cả";
            this.btnRemoveAll.UseVisualStyleBackColor = true;
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // lovShop
            // 
            this.lovShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lovShop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lovShop.Location = new System.Drawing.Point(90, 0);
            this.lovShop.Margin = new System.Windows.Forms.Padding(0);
            this.lovShop.MinimumSize = new System.Drawing.Size(24, 24);
            this.lovShop.Name = "lovShop";
            this.lovShop.Padding = new System.Windows.Forms.Padding(3);
            this.lovShop.Size = new System.Drawing.Size(814, 28);
            this.lovShop.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 3);
            this.label7.Margin = new System.Windows.Forms.Padding(3);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(3);
            this.label7.Size = new System.Drawing.Size(84, 22);
            this.label7.TabIndex = 1;
            this.label7.Text = "Siêu thị";
            // 
            // pnlGoods
            // 
            this.pnlGoods.Controls.Add(this.panel3);
            this.pnlGoods.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlGoods.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlGoods.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pnlGoods.Location = new System.Drawing.Point(0, 0);
            this.pnlGoods.Name = "pnlGoods";
            this.pnlGoods.Padding = new System.Windows.Forms.Padding(0);
            this.pnlGoods.Size = new System.Drawing.Size(906, 318);
            this.pnlGoods.TabIndex = 0;
            this.pnlGoods.TabStop = false;
            this.pnlGoods.Text = "Nguồn hàng";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 13);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(906, 305);
            this.panel3.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.tblGoodsList);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 121);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(904, 182);
            this.panel6.TabIndex = 1;
            // 
            // tblGoodsList
            // 
            this.tblGoodsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblGoodsList.Location = new System.Drawing.Point(0, 0);
            this.tblGoodsList.MainView = this.grdGoodsList;
            this.tblGoodsList.Name = "tblGoodsList";
            this.tblGoodsList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemDateEdit3,
            this.repositoryItemSpinEdit2,
            this.repositoryItemDateEdit4});
            this.tblGoodsList.Size = new System.Drawing.Size(904, 182);
            this.tblGoodsList.TabIndex = 14;
            this.tblGoodsList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdGoodsList});
            // 
            // grdGoodsList
            // 
            this.grdGoodsList.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdGoodsList.Appearance.FocusedRow.Options.UseFont = true;
            this.grdGoodsList.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdGoodsList.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdGoodsList.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdGoodsList.Appearance.Preview.Options.UseFont = true;
            this.grdGoodsList.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdGoodsList.Appearance.Row.Options.UseFont = true;
            this.grdGoodsList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11});
            this.grdGoodsList.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdGoodsList.GridControl = this.tblGoodsList;
            this.grdGoodsList.IndicatorWidth = 35;
            this.grdGoodsList.Name = "grdGoodsList";
            this.grdGoodsList.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdGoodsList.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdGoodsList.OptionsNavigation.UseTabKey = false;
            this.grdGoodsList.OptionsView.ColumnAutoWidth = false;
            this.grdGoodsList.OptionsView.ShowAutoFilterRow = true;
            this.grdGoodsList.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdGoodsList.OptionsView.ShowFooter = true;
            this.grdGoodsList.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn11.Caption = "Mã ghép";
            this.gridColumn11.FieldName = "GOODS_CODE";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 809;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemCheckEdit1.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit1.Mask.EditMask = "\\d{0,12}";
            this.repositoryItemSpinEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEdit1.MaxLength = 13;
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            this.repositoryItemSpinEdit1.NullText = "0";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 200;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit3.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit3.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit3.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            this.repositoryItemDateEdit3.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.DisplayFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit2.EditFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // repositoryItemDateEdit4
            // 
            this.repositoryItemDateEdit4.AutoHeight = false;
            this.repositoryItemDateEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit4.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit4.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit4.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit4.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit4.Name = "repositoryItemDateEdit4";
            this.repositoryItemDateEdit4.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel2.Controls.Add(this.pnlButton, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lovGoods, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.cboGoodsGroup, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.cboProduct, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lbType, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(904, 121);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // pnlButton
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.pnlButton, 2);
            this.pnlButton.Controls.Add(this.panel5);
            this.pnlButton.Controls.Add(this.btnAddGoods);
            this.pnlButton.Controls.Add(this.btnAllGoods);
            this.pnlButton.Controls.Add(this.btnRemoveGoods);
            this.pnlButton.Controls.Add(this.btnRemoveAllGoods);
            this.pnlButton.Controls.Add(this.btnImportGoods);
            this.pnlButton.Controls.Add(this.btnDownTemplateGoods);
            this.pnlButton.Location = new System.Drawing.Point(3, 87);
            this.pnlButton.Name = "pnlButton";
            this.pnlButton.Padding = new System.Windows.Forms.Padding(3);
            this.pnlButton.Size = new System.Drawing.Size(898, 32);
            this.pnlButton.TabIndex = 15;
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(6, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(189, 27);
            this.panel5.TabIndex = 4;
            // 
            // btnAddGoods
            // 
            this.btnAddGoods.Location = new System.Drawing.Point(201, 6);
            this.btnAddGoods.Name = "btnAddGoods";
            this.btnAddGoods.Size = new System.Drawing.Size(79, 23);
            this.btnAddGoods.TabIndex = 0;
            this.btnAddGoods.Text = "Thêm";
            this.btnAddGoods.UseVisualStyleBackColor = true;
            this.btnAddGoods.Click += new System.EventHandler(this.btnAddGoods_Click);
            // 
            // btnAllGoods
            // 
            this.btnAllGoods.Location = new System.Drawing.Point(286, 6);
            this.btnAllGoods.Name = "btnAllGoods";
            this.btnAllGoods.Size = new System.Drawing.Size(79, 23);
            this.btnAllGoods.TabIndex = 1;
            this.btnAllGoods.Text = "Tất cả";
            this.btnAllGoods.UseVisualStyleBackColor = true;
            this.btnAllGoods.Click += new System.EventHandler(this.btnAllGoods_Click);
            // 
            // btnRemoveGoods
            // 
            this.btnRemoveGoods.Location = new System.Drawing.Point(371, 6);
            this.btnRemoveGoods.Name = "btnRemoveGoods";
            this.btnRemoveGoods.Size = new System.Drawing.Size(79, 23);
            this.btnRemoveGoods.TabIndex = 2;
            this.btnRemoveGoods.Text = "Xóa";
            this.btnRemoveGoods.UseVisualStyleBackColor = true;
            this.btnRemoveGoods.Click += new System.EventHandler(this.btnRemoveGoods_Click);
            // 
            // btnRemoveAllGoods
            // 
            this.btnRemoveAllGoods.Location = new System.Drawing.Point(456, 6);
            this.btnRemoveAllGoods.Name = "btnRemoveAllGoods";
            this.btnRemoveAllGoods.Size = new System.Drawing.Size(79, 23);
            this.btnRemoveAllGoods.TabIndex = 3;
            this.btnRemoveAllGoods.Text = "Xóa tất cả";
            this.btnRemoveAllGoods.UseVisualStyleBackColor = true;
            this.btnRemoveAllGoods.Click += new System.EventHandler(this.btnRemoveAllGoods_Click);
            // 
            // btnImportGoods
            // 
            this.btnImportGoods.Location = new System.Drawing.Point(541, 6);
            this.btnImportGoods.Name = "btnImportGoods";
            this.btnImportGoods.Size = new System.Drawing.Size(79, 23);
            this.btnImportGoods.TabIndex = 5;
            this.btnImportGoods.Text = "Import";
            this.btnImportGoods.UseVisualStyleBackColor = true;
            this.btnImportGoods.Click += new System.EventHandler(this.btnImportGoods_Click);
            // 
            // btnDownTemplateGoods
            // 
            this.btnDownTemplateGoods.Location = new System.Drawing.Point(626, 6);
            this.btnDownTemplateGoods.Name = "btnDownTemplateGoods";
            this.btnDownTemplateGoods.Size = new System.Drawing.Size(75, 23);
            this.btnDownTemplateGoods.TabIndex = 12;
            this.btnDownTemplateGoods.Text = "File mẫu";
            this.btnDownTemplateGoods.UseVisualStyleBackColor = true;
            this.btnDownTemplateGoods.Click += new System.EventHandler(this.btnDownTemplateGoods_Click);
            // 
            // lovGoods
            // 
            this.lovGoods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lovGoods.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lovGoods.Location = new System.Drawing.Point(90, 56);
            this.lovGoods.Margin = new System.Windows.Forms.Padding(0);
            this.lovGoods.MinimumSize = new System.Drawing.Size(24, 24);
            this.lovGoods.Name = "lovGoods";
            this.lovGoods.Padding = new System.Windows.Forms.Padding(3);
            this.lovGoods.Size = new System.Drawing.Size(814, 28);
            this.lovGoods.TabIndex = 8;
            // 
            // cboGoodsGroup
            // 
            this.cboGoodsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboGoodsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGoodsGroup.Location = new System.Drawing.Point(90, 28);
            this.cboGoodsGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboGoodsGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboGoodsGroup.Name = "cboGoodsGroup";
            this.cboGoodsGroup.Padding = new System.Windows.Forms.Padding(3);
            this.cboGoodsGroup.Size = new System.Drawing.Size(814, 28);
            this.cboGoodsGroup.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(3);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(3);
            this.label4.Size = new System.Drawing.Size(84, 22);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nhóm hàng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 59);
            this.label3.Margin = new System.Windows.Forms.Padding(3);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(3);
            this.label3.Size = new System.Drawing.Size(84, 22);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mã ghép";
            // 
            // cboProduct
            // 
            this.cboProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProduct.Location = new System.Drawing.Point(90, 0);
            this.cboProduct.Margin = new System.Windows.Forms.Padding(0);
            this.cboProduct.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboProduct.Name = "cboProduct";
            this.cboProduct.Padding = new System.Windows.Forms.Padding(3);
            this.cboProduct.Size = new System.Drawing.Size(814, 28);
            this.cboProduct.TabIndex = 4;
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbType.Location = new System.Drawing.Point(3, 3);
            this.lbType.Margin = new System.Windows.Forms.Padding(3);
            this.lbType.Name = "lbType";
            this.lbType.Padding = new System.Windows.Forms.Padding(3);
            this.lbType.Size = new System.Drawing.Size(84, 22);
            this.lbType.TabIndex = 1;
            this.lbType.Text = "Ngành hàng";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(906, 83);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.Controls.Add(this.cboOverDaysRedundanceShop, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtShopLackDays, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtRecyclingName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtShopRedundanceDays, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(904, 81);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cboOverDaysRedundanceShop
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.cboOverDaysRedundanceShop, 3);
            this.cboOverDaysRedundanceShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboOverDaysRedundanceShop.FormattingEnabled = true;
            this.cboOverDaysRedundanceShop.Items.AddRange(new object[] {
            "20",
            "30",
            "60"});
            this.cboOverDaysRedundanceShop.Location = new System.Drawing.Point(201, 55);
            this.cboOverDaysRedundanceShop.Name = "cboOverDaysRedundanceShop";
            this.cboOverDaysRedundanceShop.Size = new System.Drawing.Size(700, 21);
            this.cboOverDaysRedundanceShop.TabIndex = 9;
            // 
            // txtShopLackDays
            // 
            this.txtShopLackDays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtShopLackDays.Location = new System.Drawing.Point(652, 29);
            this.txtShopLackDays.Name = "txtShopLackDays";
            this.txtShopLackDays.Size = new System.Drawing.Size(249, 20);
            this.txtShopLackDays.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(454, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(3);
            this.label2.Size = new System.Drawing.Size(192, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Số ngày cần hàng của ST thiếu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 29);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(3);
            this.label1.Size = new System.Drawing.Size(192, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Số ngày đảm bảo hàng cho ST thừa";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbName.Location = new System.Drawing.Point(3, 3);
            this.lbName.Margin = new System.Windows.Forms.Padding(3);
            this.lbName.Name = "lbName";
            this.lbName.Padding = new System.Windows.Forms.Padding(3);
            this.lbName.Size = new System.Drawing.Size(192, 20);
            this.lbName.TabIndex = 2;
            this.lbName.Text = "Tên lần điều chuyển";
            // 
            // txtRecyclingName
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtRecyclingName, 3);
            this.txtRecyclingName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRecyclingName.Location = new System.Drawing.Point(201, 3);
            this.txtRecyclingName.Name = "txtRecyclingName";
            this.txtRecyclingName.Size = new System.Drawing.Size(700, 20);
            this.txtRecyclingName.TabIndex = 3;
            // 
            // txtShopRedundanceDays
            // 
            this.txtShopRedundanceDays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtShopRedundanceDays.Location = new System.Drawing.Point(201, 29);
            this.txtShopRedundanceDays.Name = "txtShopRedundanceDays";
            this.txtShopRedundanceDays.Size = new System.Drawing.Size(247, 20);
            this.txtShopRedundanceDays.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 55);
            this.label6.Margin = new System.Windows.Forms.Padding(3);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(3);
            this.label6.Size = new System.Drawing.Size(192, 23);
            this.label6.TabIndex = 8;
            this.label6.Text = "Số ngày tính QH cho ST thừa";
            // 
            // tabPage2
            // 
            this.tabPage2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(914, 659);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Xác định ST thừa, thiếu";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.pnlLackShop);
            this.panel9.Controls.Add(this.pnlRedundanceShop);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 37);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(906, 617);
            this.panel9.TabIndex = 1;
            // 
            // pnlLackShop
            // 
            this.pnlLackShop.Controls.Add(this.tblLackShop);
            this.pnlLackShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLackShop.Location = new System.Drawing.Point(0, 294);
            this.pnlLackShop.Name = "pnlLackShop";
            this.pnlLackShop.Size = new System.Drawing.Size(906, 323);
            this.pnlLackShop.TabIndex = 1;
            this.pnlLackShop.TabStop = false;
            this.pnlLackShop.Text = "Siêu thị thiếu hàng";
            // 
            // tblLackShop
            // 
            this.tblLackShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblLackShop.Location = new System.Drawing.Point(3, 16);
            this.tblLackShop.MainView = this.gridView3;
            this.tblLackShop.Name = "tblLackShop";
            this.tblLackShop.Size = new System.Drawing.Size(900, 304);
            this.tblLackShop.TabIndex = 17;
            this.tblLackShop.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView3.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView3.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView3.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView3.Appearance.Preview.Options.UseFont = true;
            this.gridView3.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView3.Appearance.Row.Options.UseFont = true;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.GridControl = this.tblLackShop;
            this.gridView3.IndicatorWidth = 35;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView3.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView3.OptionsNavigation.UseTabKey = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Mã siêu thị";
            this.gridColumn4.FieldName = "SHOP_CODE";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 122;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Tên siêu thị";
            this.gridColumn5.FieldName = "SHOP_NAME";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 229;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "Mã ghép";
            this.gridColumn20.FieldName = "COUPLE_GOODS";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 2;
            this.gridColumn20.Width = 120;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "Số lượng";
            this.gridColumn21.FieldName = "VALUE";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 3;
            this.gridColumn21.Width = 129;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "Danh mục";
            this.gridColumn22.FieldName = "DMSP_VALUE";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 4;
            this.gridColumn22.Width = 143;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.Caption = "Sản lượng 30 ngày";
            this.gridColumn23.FieldName = "SLG30_VALUE";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 5;
            this.gridColumn23.Width = 150;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.Caption = "Tồn";
            this.gridColumn24.FieldName = "REMAIN_VALUE";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 6;
            this.gridColumn24.Width = 127;
            // 
            // pnlRedundanceShop
            // 
            this.pnlRedundanceShop.Controls.Add(this.tblRedundanceShop);
            this.pnlRedundanceShop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRedundanceShop.Location = new System.Drawing.Point(0, 0);
            this.pnlRedundanceShop.Name = "pnlRedundanceShop";
            this.pnlRedundanceShop.Size = new System.Drawing.Size(906, 294);
            this.pnlRedundanceShop.TabIndex = 0;
            this.pnlRedundanceShop.TabStop = false;
            this.pnlRedundanceShop.Text = "Siêu thị thừa hàng";
            // 
            // tblRedundanceShop
            // 
            this.tblRedundanceShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblRedundanceShop.Location = new System.Drawing.Point(3, 16);
            this.tblRedundanceShop.MainView = this.gridView2;
            this.tblRedundanceShop.Name = "tblRedundanceShop";
            this.tblRedundanceShop.Size = new System.Drawing.Size(900, 275);
            this.tblRedundanceShop.TabIndex = 16;
            this.tblRedundanceShop.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView2.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.GridControl = this.tblRedundanceShop;
            this.gridView2.IndicatorWidth = 35;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView2.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView2.OptionsNavigation.UseTabKey = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Mã siêu thị";
            this.gridColumn2.FieldName = "SHOP_CODE";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 122;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Tên siêu thị";
            this.gridColumn3.FieldName = "SHOP_NAME";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 229;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn15.Caption = "Mã ghép";
            this.gridColumn15.FieldName = "COUPLE_GOODS";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 2;
            this.gridColumn15.Width = 120;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn16.Caption = "Số lượng";
            this.gridColumn16.FieldName = "VALUE";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 3;
            this.gridColumn16.Width = 129;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.Caption = "Danh mục";
            this.gridColumn17.FieldName = "DMSP_VALUE";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 4;
            this.gridColumn17.Width = 143;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn18.Caption = "Sản lượng 30 ngày";
            this.gridColumn18.FieldName = "SLG30_VALUE";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            this.gridColumn18.Width = 150;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn19.Caption = "Tồn";
            this.gridColumn19.FieldName = "REMAIN_VALUE";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 6;
            this.gridColumn19.Width = 127;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnCompute);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(906, 34);
            this.panel8.TabIndex = 0;
            // 
            // btnCompute
            // 
            this.btnCompute.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCompute.Location = new System.Drawing.Point(419, 5);
            this.btnCompute.MaximumSize = new System.Drawing.Size(80, 28);
            this.btnCompute.Name = "btnCompute";
            this.btnCompute.Size = new System.Drawing.Size(80, 25);
            this.btnCompute.TabIndex = 0;
            this.btnCompute.Text = "Xác định ST thừa, thiếu";
            this.btnCompute.UseVisualStyleBackColor = true;
            this.btnCompute.Click += new System.EventHandler(this.btnCompute_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage3.Controls.Add(this.panel11);
            this.tabPage3.Controls.Add(this.flowLayoutPanel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(914, 659);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Điều chuyển hàng";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.pnList);
            this.panel11.Controls.Add(this.flowLayoutPanel3);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(0, 36);
            this.panel11.MinimumSize = new System.Drawing.Size(30, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(912, 621);
            this.panel11.TabIndex = 16;
            // 
            // pnList
            // 
            this.pnList.Controls.Add(this.groupBox2);
            this.pnList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnList.Location = new System.Drawing.Point(0, 29);
            this.pnList.Name = "pnList";
            this.pnList.Size = new System.Drawing.Size(912, 592);
            this.pnList.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tblList);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(912, 592);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kết quả điều chuyển";
            // 
            // tblList
            // 
            this.tblList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblList.Location = new System.Drawing.Point(3, 16);
            this.tblList.MainView = this.gridView4;
            this.tblList.Name = "tblList";
            this.tblList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemSpinEdit5,
            this.repositoryItemTextEdit3,
            this.repositoryItemDateEdit7,
            this.repositoryItemSpinEdit6,
            this.repositoryItemDateEdit8});
            this.tblList.Size = new System.Drawing.Size(906, 573);
            this.tblList.TabIndex = 14;
            this.tblList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView4.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView4.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridView4.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView4.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView4.Appearance.Preview.Options.UseFont = true;
            this.gridView4.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridView4.Appearance.Row.Options.UseFont = true;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.GridControl = this.tblList;
            this.gridView4.IndicatorWidth = 35;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView4.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView4.OptionsNavigation.UseTabKey = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowFooter = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Từ siêu thị";
            this.gridColumn6.FieldName = "FROM_SHOP";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 164;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn8.Caption = "Đến siêu thị";
            this.gridColumn8.FieldName = "TO_SHOP";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            this.gridColumn8.Width = 163;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Mã hàng";
            this.gridColumn9.FieldName = "GOODS_CODE";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            this.gridColumn9.Width = 184;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "Tên hàng";
            this.gridColumn10.FieldName = "GOODS_NAME";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            this.gridColumn10.Width = 234;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn12.Caption = "Số lượng";
            this.gridColumn12.FieldName = "ORG_VALUE";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            this.gridColumn12.Width = 93;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn13.Caption = "Số lượng thật";
            this.gridColumn13.FieldName = "VALUE";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            this.gridColumn13.Width = 121;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn14.Caption = "Khoảng cách";
            this.gridColumn14.FieldName = "DISTANCE";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 6;
            this.gridColumn14.Width = 133;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemCheckEdit3.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // repositoryItemSpinEdit5
            // 
            this.repositoryItemSpinEdit5.AutoHeight = false;
            this.repositoryItemSpinEdit5.DisplayFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit5.EditFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit5.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit5.Mask.EditMask = "\\d{0,12}";
            this.repositoryItemSpinEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEdit5.MaxLength = 13;
            this.repositoryItemSpinEdit5.Name = "repositoryItemSpinEdit5";
            this.repositoryItemSpinEdit5.NullText = "0";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.MaxLength = 200;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemDateEdit7
            // 
            this.repositoryItemDateEdit7.AutoHeight = false;
            this.repositoryItemDateEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit7.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit7.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit7.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit7.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit7.Name = "repositoryItemDateEdit7";
            this.repositoryItemDateEdit7.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemSpinEdit6
            // 
            this.repositoryItemSpinEdit6.AutoHeight = false;
            this.repositoryItemSpinEdit6.DisplayFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit6.EditFormat.FormatString = "#,##0.##";
            this.repositoryItemSpinEdit6.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemSpinEdit6.Name = "repositoryItemSpinEdit6";
            // 
            // repositoryItemDateEdit8
            // 
            this.repositoryItemDateEdit8.AutoHeight = false;
            this.repositoryItemDateEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit8.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit8.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit8.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit8.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit8.Name = "repositoryItemDateEdit8";
            this.repositoryItemDateEdit8.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.cboPage);
            this.flowLayoutPanel3.Controls.Add(this.label5);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(912, 29);
            this.flowLayoutPanel3.TabIndex = 14;
            // 
            // cboPage
            // 
            this.cboPage.FormattingEnabled = true;
            this.cboPage.Items.AddRange(new object[] {
            "50",
            "100",
            "200",
            "500",
            "Tất cả"});
            this.cboPage.Location = new System.Drawing.Point(826, 3);
            this.cboPage.Name = "cboPage";
            this.cboPage.Size = new System.Drawing.Size(83, 21);
            this.cboPage.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(773, 6);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số dòng";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.panel10);
            this.flowLayoutPanel2.Controls.Add(this.btnRecycling);
            this.flowLayoutPanel2.Controls.Add(this.btnExport);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Padding = new System.Windows.Forms.Padding(3);
            this.flowLayoutPanel2.Size = new System.Drawing.Size(912, 36);
            this.flowLayoutPanel2.TabIndex = 15;
            // 
            // panel10
            // 
            this.panel10.Location = new System.Drawing.Point(6, 6);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(231, 27);
            this.panel10.TabIndex = 4;
            // 
            // btnRecycling
            // 
            this.btnRecycling.Location = new System.Drawing.Point(243, 6);
            this.btnRecycling.Name = "btnRecycling";
            this.btnRecycling.Size = new System.Drawing.Size(120, 23);
            this.btnRecycling.TabIndex = 0;
            this.btnRecycling.Text = "Điều chuyển hàng";
            this.btnRecycling.UseVisualStyleBackColor = true;
            this.btnRecycling.Click += new System.EventHandler(this.btnRecycling_Click);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(369, 6);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(120, 23);
            this.btnExport.TabIndex = 2;
            this.btnExport.Text = "Kết xuất";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // pnSearchStatus
            // 
            this.pnSearchStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnSearchStatus.BackColor = System.Drawing.SystemColors.Control;
            this.pnSearchStatus.Controls.Add(this.marqueeProgressBarControl1);
            this.pnSearchStatus.Controls.Add(this.label11);
            this.pnSearchStatus.Location = new System.Drawing.Point(424, 298);
            this.pnSearchStatus.Name = "pnSearchStatus";
            this.pnSearchStatus.Size = new System.Drawing.Size(456, 88);
            this.pnSearchStatus.TabIndex = 14;
            this.pnSearchStatus.Visible = false;
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(25, 46);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Properties.LookAndFeel.SkinName = "The Asphalt World";
            this.marqueeProgressBarControl1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(406, 25);
            this.marqueeProgressBarControl1.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label11.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label11.Location = new System.Drawing.Point(24, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(416, 23);
            this.label11.TabIndex = 3;
            this.label11.Text = "Đang xử lý. Vui lòng chờ trong giây lát...";
            // 
            // tmrFormatFlex
            // 
            this.tmrFormatFlex.Enabled = true;
            this.tmrFormatFlex.Interval = 1000;
            this.tmrFormatFlex.Tick += new System.EventHandler(this.tmrFormatFlex_Tick);
            // 
            // contextMenuAll
            // 
            this.contextMenuAll.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripAll,
            this.testToolStripMB,
            this.toolStripMN});
            this.contextMenuAll.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.contextMenuAll.Name = "contextMenuAll";
            this.contextMenuAll.Size = new System.Drawing.Size(173, 70);
            // 
            // toolStripAll
            // 
            this.toolStripAll.Name = "toolStripAll";
            this.toolStripAll.Size = new System.Drawing.Size(172, 22);
            this.toolStripAll.Text = "Tất cả";
            this.toolStripAll.Click += new System.EventHandler(this.toolStripAll_Click);
            // 
            // testToolStripMB
            // 
            this.testToolStripMB.Name = "testToolStripMB";
            this.testToolStripMB.Size = new System.Drawing.Size(172, 22);
            this.testToolStripMB.Text = "Các siêu thị VTMB";
            this.testToolStripMB.Click += new System.EventHandler(this.testToolStripMB_Click);
            // 
            // toolStripMN
            // 
            this.toolStripMN.Name = "toolStripMN";
            this.toolStripMN.Size = new System.Drawing.Size(172, 22);
            this.toolStripMN.Text = "Các siêu thị VTMN";
            this.toolStripMN.Click += new System.EventHandler(this.toolStripMN_Click);
            // 
            // DUISameLevelDividing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 685);
            this.Controls.Add(this.pnSearchStatus);
            this.Controls.Add(this.splitContainer1);
            this.Name = "DUISameLevelDividing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Điều chuyển ngang hàng có serial";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblRotateList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdRotateList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speOldPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            this.tabDetail.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlShop.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblShopList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdShopList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.pnlGoods.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblGoodsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdGoodsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.pnlButton.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.pnlLackShop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblLackShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.pnlRedundanceShop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblRedundanceShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.panel8.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.pnList.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit8.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit8)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.pnSearchStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            this.contextMenuAll.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraGrid.GridControl tblRotateList;
        private DevExpress.XtraGrid.Views.Grid.GridView grdRotateList;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit spePrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtNote;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit speOldPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private System.Windows.Forms.TabControl tabDetail;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.TextBox txtShopLackDays;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRecyclingName;
        private System.Windows.Forms.TextBox txtShopRedundanceDays;
        private System.Windows.Forms.GroupBox pnlShop;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox pnlGoods;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lbType;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString lovGoods;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString cboGoodsGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString cboProduct;
        private System.Windows.Forms.FlowLayoutPanel pnlButton;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnAddGoods;
        private System.Windows.Forms.Button btnAllGoods;
        private System.Windows.Forms.Button btnRemoveGoods;
        private System.Windows.Forms.Button btnRemoveAllGoods;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnImportGoods;
        private DevExpress.XtraGrid.GridControl tblGoodsList;
        private DevExpress.XtraGrid.Views.Grid.GridView grdGoodsList;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Button btnImport;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString lovShop;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraGrid.GridControl tblShopList;
        private DevExpress.XtraGrid.Views.Grid.GridView grdShopList;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnCompute;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.GroupBox pnlLackShop;
        private System.Windows.Forms.GroupBox pnlRedundanceShop;
        private DevExpress.XtraGrid.GridControl tblRedundanceShop;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnRecycling;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel pnList;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraGrid.GridControl tblList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.ComboBox cboPage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboOverDaysRedundanceShop;
        private DevExpress.XtraGrid.GridControl tblLackShop;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button btnNewRotate;
        private System.Windows.Forms.Button btnCancelRotate;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel pnSearchStatus;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Timer tmrFormatFlex;
        private System.Windows.Forms.Button btnDownTemplateGoods;
        private System.Windows.Forms.Button btnDownTemplate;
        private System.Windows.Forms.ContextMenuStrip contextMenuAll;
        private System.Windows.Forms.ToolStripMenuItem toolStripAll;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMB;
        private System.Windows.Forms.ToolStripMenuItem toolStripMN;
    }
}