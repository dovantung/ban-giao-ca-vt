﻿using GemBox.Spreadsheet;
using Library.AppCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class DUIGoodsPriceSegment : Form
    {
        private DataTable dtbResourceProductType = null;
        private DataTable dtbResourceGoodsPriceSegment = null;

        private PLC.Goods.PLCGoodsPriceSegment plcObject = new PLC.Goods.PLCGoodsPriceSegment();
        private PLC.Goods.WSGoodsDmtb.ResultMessage objResultMessage = null;

        private ActionMode currentAction = ActionMode.NONE;
        public DUIGoodsPriceSegment()
        {
            InitializeComponent();
            SearchDataProductType();       
            enableControl(true);
            search();
            
        }

        private void SearchDataProductType()
        {
            dtbResourceProductType = plcObject.LoadDataProductType();
            //dtbResourceProductType = Library.AppCore.DataSource.GetDataSource.GetProductType();
            tblProductType.DataSource = dtbResourceProductType;
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {

                MessageBox.Show(SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, SystemConfig.objSessionUser.ResultMessageApp.Message,
                      MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void grdProductType_Click(object sender, EventArgs e)
        {
            search();
           
        }

        private void onSearch()
        {
            if (currentAction == ActionMode.NONE)
            {
                currentAction = ActionMode.SEARCH_MODE;
                clearDetailValue();
                enableControl(false);
                search();
            }
            else
            {
                search();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Delete du lieu cho cac hop text box
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        private void clearDetailValue()
        {
            txtName.Text = "";
            txtDescription.Text = "";
            txtFromPrice.Value = 0;
            txtToPrice.Value = 0;

        }

        private void enableControl(bool enable)
        {
            btnAdd.Visible = enable;
            btnModify.Visible = enable;
            btnDelete.Visible = enable;
            btnExit.Visible = enable;

            btnOK.Visible =!enable;
            btnCancel.Visible = !enable;

            tblProductType.Enabled = enable;
            tblList.Enabled = enable;
           
            dtpFromDate.Enabled = !enable;
            dtpToDate.Enabled = !enable;
            txtFromPrice.Enabled = !enable;
            txtToPrice.Enabled = !enable;
            txtName.Enabled = !enable;
            txtDescription.Enabled = !enable;
           
           
           // formData.setFieldEnabled(!enable);
            if (currentAction == ActionMode.ADD_MODE || currentAction == ActionMode.MODIFY_MODE)
            {

                if (currentAction == ActionMode.ADD_MODE)
                {

                    dtpFromDate.Value = DateTime.Now;
                   
                }
                else
                {
                  
                }
            }
           
        }

        public bool search()
        {
            try
            {
                if (grdProductType.GetFocusedDataRow() == null) { 
                    return true;
                }

                string product_id = "";
                DataRow dtRow = grdProductType.GetFocusedDataRow();
                product_id = dtRow["code"].ToString();
                if (currentAction != ActionMode.SEARCH_MODE && currentAction != ActionMode.NONE)
                {

                }
                else
                {
                }

                object[] objKeywords = new object[]{
                    "@product_id", product_id,
                };
                dtbResourceGoodsPriceSegment = plcObject.LoadDataGoodsPriceSegment(objKeywords);
                tblList.DataSource = dtbResourceGoodsPriceSegment;
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }

                
               
                return true;
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                   e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                // TODO: handle exception
                return false;
            }
        }

        private void fillDetailValue()
        {
            try
            {
                if (currentAction == ActionMode.SEARCH_MODE || currentAction == ActionMode.SEARCH_MODE) return;
                if (grdList.GetFocusedDataRow() == null) return;
                DataRow data = grdList.GetFocusedDataRow();
                string from_date = data["from_date"].ToString();
                string to_date = data["to_date"].ToString();

                if (from_date != null && !from_date.Equals(""))
                {
                    dtpFromDate.Checked = true;

                    dtpToDate.Value = DateTime.ParseExact(from_date , "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtpFromDate.Checked = false;
                }
                    dtpFromDate_ValueChanged(null, null);

                    if (to_date != null && !to_date.Equals(""))
                {
                    dtpToDate.Checked = true;
                    dtpToDate.Value = DateTime.ParseExact(to_date , "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtpToDate.Checked = false;
                }

                dtpToDate_ValueChanged(null, null);

                txtName.Text = data["NAME"].ToString();
                long from_price;
                long.TryParse(data["FROM_PRICE"].ToString(), out from_price);
                txtFromPrice.Value = from_price;

                long to_price;
                long.TryParse(data["TO_PRICE"].ToString(), out to_price);
                txtToPrice.Value = to_price;
                txtDescription.Text = data["DESCRIPTION"].ToString();


            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                       ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            currentAction = ActionMode.NONE;
            search();
            fillDetailValue();
            enableControl(true);
            onChangeAction(ACTION.ACTION_NONE);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc chắn muốn thoát chức năng?", "Thông báo"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                == System.Windows.Forms.DialogResult.No)
            {
            }
            else
            {
                this.Close();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            currentAction = ActionMode.ADD_MODE;
            clearDetailValue();
            enableControl(false);
            onChangeAction(ACTION.ACTION_ADD);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            onSearch();
            currentAction = ActionMode.NONE;
            onChangeAction(ACTION.ACTION_NONE);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            currentAction = ActionMode.MODIFY_MODE;
            enableControl(false);
            onChangeAction(ACTION.ACTION_MODIFY);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Bat su kien dau vao
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public void onChangeAction(ACTION iNewAction)
        {
            if (iNewAction == ACTION.ACTION_NONE)
            {
                fillDetailValue();
                enableControl(true);
                tblList.Enabled = true;
              
                dtpFromDate.Enabled = false;
                dtpToDate.Enabled = false;
                txtName.Enabled = false;
                txtDescription.Enabled = false;
                txtFromPrice.Enabled = false;
                txtToPrice.Enabled = false;
            }
            else if (iNewAction == ACTION.ACTION_ADD || iNewAction == ACTION.ACTION_MODIFY)
            {
                
                dtpFromDate.Enabled = true;
                dtpToDate.Enabled = true;
                
                enableControl(false);
                if (iNewAction == ACTION.ACTION_ADD)
                {
                    clearDetailValue();
                    dtpFromDate.Value = DateTime.Now;
                    
                }
                else
                {
                  
                }
            }
            if (iNewAction == ACTION.ACTION_SAVE)
            {
                enableControl(true);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (currentAction == ActionMode.ADD_MODE || currentAction == ActionMode.MODIFY_MODE)
            {
                if (!validInputDetail()) return;
            }
            if (currentAction == ActionMode.ADD_MODE)
            {
                add();
            }
            else if (currentAction == ActionMode.MODIFY_MODE)
            {
                modify();
            }
            currentAction = ActionMode.NONE;
            enableControl(true);
            onChangeAction(ACTION.ACTION_SAVE);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ ADD
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public bool add()
        {
            try
            {
                DataRow dtRow = grdProductType.GetFocusedDataRow();

                string from_date = "";
                string to_date = "";
                string from_price = "";
                string to_price = "";
                string name = "";
                string description = "";
                string product_type = "";
                string user_id = "";
                string user_shop_id = "";
                user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                name = txtName.Text;
                from_price = txtFromPrice.Value.ToString();
                to_price = txtToPrice.Value.ToString();
                description = txtDescription.Text;
                product_type = dtRow["code"].ToString();
             
                if (dtpFromDate.Checked)
                {
                    from_date = dtpFromDate.Value.ToString("dd/MM/yyyy");
                }
                if (dtpToDate.Checked)
                {
                    to_date = dtpToDate.Value.ToString("dd/MM/yyyy");
                }
                object[] objKeywords = new object[]{
                    "@from_date", from_date,
                    "@to_date", to_date,
                    "@name", name,
                    "@from_price", from_price,
                    "@to_price", to_price,
                    "@description",description,
                    "@product_type",product_type,
                    "@user_id",user_id,
                    "@user_shop_id",user_shop_id,

                };
                string id = plcObject.Insert(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
               
                DataRow data = dtbResourceGoodsPriceSegment.NewRow();

                data["price_segment_id"] = id;

                data["name"] = name;
                data["from_date"] = from_date;
                data["TO_DATE"] = to_date;
                data["from_price"] = from_price;
                data["to_price"] = to_price;
                data["description"] = description;
                

                dtbResourceGoodsPriceSegment.Rows.Add(data);
                dtbResourceGoodsPriceSegment.AcceptChanges();
                //if (tblList.getRowCount() > 0)
                //{
                //    int selected = tblList.getRowCount() - 1;
                //    tblList.setRowSelectionInterval(selected, selected);
                //}
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                  e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                return false;
            }
            return true;
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Modify
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public bool modify()
        {
            try
            {
                DataRow dtRow = grdProductType.GetFocusedDataRow();

                string id = grdList.GetFocusedDataRow()["price_segment_id"].ToString();
                string from_date = "";
                string to_date = "";
                string from_price = "";
                string to_price = "";
                string name = "";
                string description = "";
                string product_type = "";
                name = txtName.Text;
                from_price = txtFromPrice.Value.ToString();
                to_price = txtToPrice.Value.ToString();
                description = txtDescription.Text;
                product_type = dtRow["code"].ToString();
             
                if (dtpFromDate.Checked)
                {
                    from_date = dtpFromDate.Value.ToString("dd/MM/yyyy");
                }
                if (dtpToDate.Checked)
                {
                    to_date = dtpToDate.Value.ToString("dd/MM/yyyy");
                }
                object[] objKeywords = new object[]{
                    "@from_date", from_date,
                    "@to_date", to_date,
                    "@name", name,
                    "@from_price", from_price,
                    "@to_price", to_price,
                    "@description",description,
                    "@product_type",product_type,
                    "@id",id,
                };

                plcObject.Update(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                
                DataRow data = grdList.GetFocusedDataRow();

                data["price_segment_id"] = id;

                data["name"] = name;
                data["from_date"] = from_date;
                data["TO_DATE"] = to_date;
                data["from_price"] = from_price;
                data["to_price"] = to_price;
                data["description"] = description;

                dtbResourceGoodsPriceSegment.AcceptChanges();
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                  e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                return false;
            }
            return true;
        }
        private int findRow(ref long id, String ProductType_id, String goods_id)
        {
            int index = -1;
            for (int i = 0; i < dtbResourceGoodsPriceSegment.Rows.Count; i++)
            {

                DataRow row = dtbResourceGoodsPriceSegment.Rows[i];
                string cellGoodsCode = row["goods_code"].ToString();
                string cellProductTypeCode = row["ProductType_code"].ToString();
                if (cellGoodsCode.Equals(goods_id) && cellProductTypeCode.Equals(ProductType_id))
                {
                    id = long.Parse(row["goods_PriceSegment_id"].ToString());
                    return i;
                }
            }
            return index;
        }
        private bool validInputDetail()
        {
            if (grdProductType.GetFocusedDataRow().Equals(null))
            {
                MessageBox.Show("Bạn phải chọn Loại mặt hàng", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

              
                return false;
            }
           
            if (!dtpFromDate.Checked)
            {
                MessageBox.Show("Bạn phải nhập từ ngày", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (DateTime.Now.CompareTo(dtpFromDate.Value) >= 1)
            {
                MessageBox.Show("Ngày ở trường từ ngày phải lớn hơn ngày hệ thống","Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                dtpFromDate.Focus();
                return false;
            }
            if (dtpToDate.Checked)
            {
                if ((DateTime.Now.CompareTo(dtpToDate.Value) >= 1))
                {
                    MessageBox.Show("Ngày ở trường đến ngày phải lớn hơn ngày hệ thống", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dtpToDate.Focus();
                    return false;
                }
                if (dtpFromDate.Value.CompareTo(dtpToDate.Value) >= 1)
                {
                   
                    MessageBox.Show("Ngày ở trường đến ngày phải lớn hơn từ ngày", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dtpToDate.Focus();
                    return false;
                }
            }

            if (txtFromPrice.Value > txtToPrice.Value)
            {
                MessageBox.Show("giá ở trường từ mức giá phải nhỏ hơn đến phân khúc giá", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtFromPrice.Focus();
                return false;
            }
            
            return true;
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (this.dtpFromDate.Checked != true)
            {
                this.dtpFromDate.CustomFormat = " ";
            }
            else
            {
                this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            }
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            if (this.dtpToDate.Checked != true)
            {
                this.dtpToDate.CustomFormat = " ";
            }
            else
            {
                this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            }
        }

        private void tblList_Click(object sender, EventArgs e)
        {
            fillDetailValue();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            onDelete();
        }

        private void onDelete()
        {
            try
            {
                if (grdList.GetFocusedDataRow() == null || grdList.GetFocusedDataRow()["price_segment_id"].ToString() == "")
                {
                    MessageBox.Show("Bạn phải chọn bản ghi để xóa", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (MessageBox.Show(this, "Bạn có muốn xóa không", "Xác nhận"
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                    == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
               
                remove();
                enableControl(true);
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                 e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Remove
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public bool remove() {
		try {
            string price_segment_id = grdList.GetFocusedDataRow()["price_segment_id"].ToString();

            plcObject.Delete(new object[]{
                    "@id", price_segment_id,});
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }

            for (int i = 0; i < dtbResourceGoodsPriceSegment.Rows.Count; i++)
            {

                DataRow row = dtbResourceGoodsPriceSegment.Rows[i];
                string cellId = row["price_segment_id"].ToString();
                if (price_segment_id.Equals(cellId))
                {
                    dtbResourceGoodsPriceSegment.Rows.Remove(row);
                    break;
                }
            }

		} catch (Exception e) {
			Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                 e.Message);
            Console.WriteLine("Exception source: {0}", e.Source);
		}
		return true;
	}

        public enum ActionMode
        {
            NONE, VIEW_MODE, ADD_MODE, MODIFY_MODE, SEARCH_MODE
        }
        public enum ACTION
        {
            ACTION_NONE, ACTION_ADD, ACTION_MODIFY, ACTION_SAVE
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Common.frmExportExcel frmExportExcel = new frmExport()
            {
                StrFileTemplateName = "goods_PriceSegment_report",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\goods_PriceSegment_report.xlsx",
                DtbResourceSource = dtbResourceGoodsPriceSegment
            };
            frmExportExcel.btnExport_Click();
        }

        class frmExport : Common.frmExportExcel
        {

            public override void AddRowDetail(ref ExcelWorksheet sheet, DataRow row, ref int iRow, ref int iSTT)
            {
                InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
                InsertValueCell(ref sheet, "B", iRow, row["ProductType_CODE"].ToString(), false, false);
                InsertValueCell(ref sheet, "C", iRow, row["GOODS_CODE"].ToString(), false, false);
                InsertValueCell(ref sheet, "D", iRow, row["GOODS_NAME"].ToString(), false, false);
                InsertValueCell(ref sheet, "E", iRow, row["PriceSegment"].ToString(), false, false);
                InsertValueCell(ref sheet, "F", iRow, row["FROM_DATE"].ToString(), false, false);
                InsertValueCell(ref sheet, "G", iRow, row["TO_DATE"].ToString(), false, false);
                iSTT++;
                iRow++;
            }
        }
    }
   
   

}
