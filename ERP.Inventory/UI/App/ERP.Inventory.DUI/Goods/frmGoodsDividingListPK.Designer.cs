﻿namespace ERP.Inventory.DUI.Goods
{
    partial class frmGoodsDividingListPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gridControlShop = new DevExpress.XtraGrid.GridControl();
            this.gridViewShop = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colShopCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShopName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStockId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreateDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRowCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repIsDeleted = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repIsFinished = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExitCreate = new System.Windows.Forms.Button();
            this.btnExitSys = new System.Windows.Forms.Button();
            this.btnDellNC = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDeleteAll = new System.Windows.Forms.Button();
            this.btnDeleteShop = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.contextMenuAll = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripAll = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMB = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMN = new System.Windows.Forms.ToolStripMenuItem();
            this.btnImport = new System.Windows.Forms.Button();
            this.cboShop = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.panel6 = new System.Windows.Forms.Panel();
            this.gridControlDevideItem = new DevExpress.XtraGrid.GridControl();
            this.gridViewDevideItem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCoupleGoods = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShopCo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShopnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSLG30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDemoGoods = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDmtb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParentShop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCenterName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.testThanhPT = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnGetTemplate = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFilter = new System.Windows.Forms.Button();
            this.cboPage = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cboGoodsGroup = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.label5 = new System.Windows.Forms.Label();
            this.cboProductType = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTenLT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsFinished)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.contextMenuAll.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDevideItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDevideItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testThanhPT)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel6);
            this.splitContainer1.Panel2.Controls.Add(this.panel5);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(1208, 515);
            this.splitContainer1.SplitterDistance = 420;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(420, 515);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Các lần tính nhu cầu";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.gridControlShop, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.grdData, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(414, 496);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // gridControlShop
            // 
            this.gridControlShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlShop.Location = new System.Drawing.Point(3, 318);
            this.gridControlShop.MainView = this.gridViewShop;
            this.gridControlShop.Name = "gridControlShop";
            this.gridControlShop.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEdit1});
            this.gridControlShop.Size = new System.Drawing.Size(408, 175);
            this.gridControlShop.TabIndex = 15;
            this.gridControlShop.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewShop});
            // 
            // gridViewShop
            // 
            this.gridViewShop.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewShop.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewShop.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridViewShop.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewShop.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewShop.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewShop.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewShop.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewShop.Appearance.Preview.Options.UseFont = true;
            this.gridViewShop.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewShop.Appearance.Row.Options.UseFont = true;
            this.gridViewShop.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colShopCode,
            this.colShopName,
            this.colStockId});
            this.gridViewShop.GridControl = this.gridControlShop;
            this.gridViewShop.Name = "gridViewShop";
            this.gridViewShop.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridViewShop.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridViewShop.OptionsNavigation.UseTabKey = false;
            this.gridViewShop.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewShop.OptionsView.ColumnAutoWidth = false;
            this.gridViewShop.OptionsView.ShowAutoFilterRow = true;
            this.gridViewShop.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewShop.OptionsView.ShowFooter = true;
            this.gridViewShop.OptionsView.ShowGroupPanel = false;
            // 
            // colShopCode
            // 
            this.colShopCode.AppearanceCell.Options.UseTextOptions = true;
            this.colShopCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colShopCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colShopCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShopCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colShopCode.Caption = "Mã siêu thị";
            this.colShopCode.FieldName = "SHOP_CODE";
            this.colShopCode.Name = "colShopCode";
            this.colShopCode.OptionsColumn.AllowEdit = false;
            this.colShopCode.OptionsColumn.AllowMove = false;
            this.colShopCode.OptionsColumn.ReadOnly = true;
            this.colShopCode.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colShopCode.Visible = true;
            this.colShopCode.VisibleIndex = 0;
            this.colShopCode.Width = 116;
            // 
            // colShopName
            // 
            this.colShopName.Caption = "Tên siêu thị";
            this.colShopName.FieldName = "NAME";
            this.colShopName.MinWidth = 10;
            this.colShopName.Name = "colShopName";
            this.colShopName.OptionsColumn.AllowEdit = false;
            this.colShopName.Visible = true;
            this.colShopName.VisibleIndex = 1;
            this.colShopName.Width = 306;
            // 
            // colStockId
            // 
            this.colStockId.Caption = "Id Siêu thị";
            this.colStockId.FieldName = "STOCK_ID";
            this.colStockId.Name = "colStockId";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit1.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit2.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit3.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 499;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(3, 3);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repIsDeleted,
            this.repIsFinished,
            this.repositoryItemCheckEdit8,
            this.repNote});
            this.grdData.Size = new System.Drawing.Size(408, 169);
            this.grdData.TabIndex = 12;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            this.grdData.Click += new System.EventHandler(this.grdData_Click_1);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colCreateDateTime,
            this.colRowCount,
            this.colId});
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.AllowHtmlDrawHeaders = true;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.grdData_CustomColumnDisplayText);
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colName.Caption = "Tên";
            this.colName.FieldName = "NAME";
            this.colName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowMove = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 100;
            // 
            // colCreateDateTime
            // 
            this.colCreateDateTime.Caption = "Thời gian tạo";
            this.colCreateDateTime.FieldName = "CREATE_DATETIME";
            this.colCreateDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCreateDateTime.MinWidth = 10;
            this.colCreateDateTime.Name = "colCreateDateTime";
            this.colCreateDateTime.OptionsColumn.AllowEdit = false;
            this.colCreateDateTime.Visible = true;
            this.colCreateDateTime.VisibleIndex = 1;
            this.colCreateDateTime.Width = 157;
            // 
            // colRowCount
            // 
            this.colRowCount.Caption = "Số lượng";
            this.colRowCount.FieldName = "ROW_COUNT";
            this.colRowCount.Name = "colRowCount";
            this.colRowCount.OptionsColumn.AllowEdit = false;
            this.colRowCount.Visible = true;
            this.colRowCount.VisibleIndex = 2;
            this.colRowCount.Width = 129;
            // 
            // colId
            // 
            this.colId.Caption = "ID";
            this.colId.FieldName = "GOODS_DIVISION_HISTORY_ID";
            this.colId.Name = "colId";
            // 
            // repIsDeleted
            // 
            this.repIsDeleted.AutoHeight = false;
            this.repIsDeleted.Name = "repIsDeleted";
            this.repIsDeleted.ValueChecked = ((short)(1));
            this.repIsDeleted.ValueUnchecked = ((short)(0));
            // 
            // repIsFinished
            // 
            this.repIsFinished.AutoHeight = false;
            this.repIsFinished.Name = "repIsFinished";
            this.repIsFinished.ValueChecked = ((short)(1));
            this.repIsFinished.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit8.ValueUnchecked = ((short)(0));
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 499;
            this.repNote.Name = "repNote";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnExitCreate);
            this.panel1.Controls.Add(this.btnExitSys);
            this.panel1.Controls.Add(this.btnDellNC);
            this.panel1.Controls.Add(this.btnCreate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 178);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(408, 49);
            this.panel1.TabIndex = 13;
            // 
            // btnExitCreate
            // 
            this.btnExitCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnExitCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitCreate.Location = new System.Drawing.Point(139, 8);
            this.btnExitCreate.Name = "btnExitCreate";
            this.btnExitCreate.Size = new System.Drawing.Size(74, 27);
            this.btnExitCreate.TabIndex = 3;
            this.btnExitCreate.Text = "Thoát";
            this.btnExitCreate.UseVisualStyleBackColor = true;
            this.btnExitCreate.Visible = false;
            this.btnExitCreate.Click += new System.EventHandler(this.btnExitCreate_Click);
            // 
            // btnExitSys
            // 
            this.btnExitSys.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnExitSys.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitSys.Location = new System.Drawing.Point(228, 8);
            this.btnExitSys.Name = "btnExitSys";
            this.btnExitSys.Size = new System.Drawing.Size(68, 27);
            this.btnExitSys.TabIndex = 2;
            this.btnExitSys.Text = "Thoát";
            this.btnExitSys.UseVisualStyleBackColor = true;
            this.btnExitSys.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // btnDellNC
            // 
            this.btnDellNC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnDellNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDellNC.Location = new System.Drawing.Point(139, 8);
            this.btnDellNC.Name = "btnDellNC";
            this.btnDellNC.Size = new System.Drawing.Size(74, 27);
            this.btnDellNC.TabIndex = 1;
            this.btnDellNC.Text = "Xóa";
            this.btnDellNC.UseVisualStyleBackColor = true;
            this.btnDellNC.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreate.Location = new System.Drawing.Point(52, 8);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(68, 27);
            this.btnCreate.TabIndex = 0;
            this.btnCreate.Text = "Tạo mới";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.btnDeleteAll);
            this.panel2.Controls.Add(this.btnDeleteShop);
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Controls.Add(this.btnAll);
            this.panel2.Controls.Add(this.btnImport);
            this.panel2.Controls.Add(this.cboShop);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 233);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(408, 79);
            this.panel2.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Siêu thị";
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnDeleteAll.Enabled = false;
            this.btnDeleteAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAll.Location = new System.Drawing.Point(320, 39);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(74, 31);
            this.btnDeleteAll.TabIndex = 4;
            this.btnDeleteAll.Text = "Xóa tất cả";
            this.btnDeleteAll.UseVisualStyleBackColor = true;
            this.btnDeleteAll.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnDeleteShop
            // 
            this.btnDeleteShop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnDeleteShop.Enabled = false;
            this.btnDeleteShop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteShop.Location = new System.Drawing.Point(238, 39);
            this.btnDeleteShop.Name = "btnDeleteShop";
            this.btnDeleteShop.Size = new System.Drawing.Size(68, 31);
            this.btnDeleteShop.TabIndex = 3;
            this.btnDeleteShop.Text = "Xóa";
            this.btnDeleteShop.UseVisualStyleBackColor = true;
            this.btnDeleteShop.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnAdd.Enabled = false;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(158, 39);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(68, 32);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnAll
            // 
            this.btnAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnAll.ContextMenuStrip = this.contextMenuAll;
            this.btnAll.Enabled = false;
            this.btnAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAll.Location = new System.Drawing.Point(81, 39);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(68, 32);
            this.btnAll.TabIndex = 1;
            this.btnAll.Text = "Tất cả";
            this.btnAll.UseVisualStyleBackColor = true;
            // 
            // contextMenuAll
            // 
            this.contextMenuAll.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripAll,
            this.testToolStripMB,
            this.toolStripMN});
            this.contextMenuAll.Name = "contextMenuAll";
            this.contextMenuAll.Size = new System.Drawing.Size(173, 70);
            // 
            // toolStripAll
            // 
            this.toolStripAll.Name = "toolStripAll";
            this.toolStripAll.Size = new System.Drawing.Size(172, 22);
            this.toolStripAll.Text = "Tất cả";
            this.toolStripAll.Click += new System.EventHandler(this.toolStripAll_Click);
            // 
            // testToolStripMB
            // 
            this.testToolStripMB.Name = "testToolStripMB";
            this.testToolStripMB.Size = new System.Drawing.Size(172, 22);
            this.testToolStripMB.Text = "Các siêu thị VTMB";
            this.testToolStripMB.Click += new System.EventHandler(this.testToolStripMB_Click);
            // 
            // toolStripMN
            // 
            this.toolStripMN.Name = "toolStripMN";
            this.toolStripMN.Size = new System.Drawing.Size(172, 22);
            this.toolStripMN.Text = "Các siêu thị VTMN";
            this.toolStripMN.Click += new System.EventHandler(this.toolStripMN_Click);
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnImport.Enabled = false;
            this.btnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Location = new System.Drawing.Point(6, 38);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(68, 32);
            this.btnImport.TabIndex = 0;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // cboShop
            // 
            this.cboShop.Enabled = false;
            this.cboShop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboShop.Location = new System.Drawing.Point(78, 6);
            this.cboShop.Margin = new System.Windows.Forms.Padding(0);
            this.cboShop.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboShop.Name = "cboShop";
            this.cboShop.Size = new System.Drawing.Size(303, 24);
            this.cboShop.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.gridControlDevideItem);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 200);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(784, 272);
            this.panel6.TabIndex = 2;
            // 
            // gridControlDevideItem
            // 
            this.gridControlDevideItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDevideItem.Location = new System.Drawing.Point(0, 0);
            this.gridControlDevideItem.MainView = this.gridViewDevideItem;
            this.gridControlDevideItem.Name = "gridControlDevideItem";
            this.gridControlDevideItem.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemTextEdit2,
            this.repositoryItemCheckEdit7,
            this.testThanhPT});
            this.gridControlDevideItem.Size = new System.Drawing.Size(784, 272);
            this.gridControlDevideItem.TabIndex = 15;
            this.gridControlDevideItem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDevideItem});
            // 
            // gridViewDevideItem
            // 
            this.gridViewDevideItem.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewDevideItem.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewDevideItem.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridViewDevideItem.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewDevideItem.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewDevideItem.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewDevideItem.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewDevideItem.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewDevideItem.Appearance.Preview.Options.UseFont = true;
            this.gridViewDevideItem.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewDevideItem.Appearance.Row.Options.UseFont = true;
            this.gridViewDevideItem.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCoupleGoods,
            this.colShopCo,
            this.colShopnName,
            this.colSLG30,
            this.colDemoGoods,
            this.colRemain,
            this.colDmtb,
            this.colNc,
            this.colParentShop,
            this.colCenterName});
            this.gridViewDevideItem.GridControl = this.gridControlDevideItem;
            this.gridViewDevideItem.Name = "gridViewDevideItem";
            this.gridViewDevideItem.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridViewDevideItem.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridViewDevideItem.OptionsNavigation.UseTabKey = false;
            this.gridViewDevideItem.OptionsSelection.InvertSelection = true;
            this.gridViewDevideItem.OptionsSelection.MultiSelect = true;
            this.gridViewDevideItem.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewDevideItem.OptionsView.ColumnAutoWidth = false;
            this.gridViewDevideItem.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDevideItem.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDevideItem.OptionsView.ShowFooter = true;
            this.gridViewDevideItem.OptionsView.ShowGroupPanel = false;
            // 
            // colCoupleGoods
            // 
            this.colCoupleGoods.AppearanceCell.Options.UseTextOptions = true;
            this.colCoupleGoods.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCoupleGoods.AppearanceHeader.Options.UseTextOptions = true;
            this.colCoupleGoods.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCoupleGoods.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCoupleGoods.Caption = "Mã ghép";
            this.colCoupleGoods.FieldName = "COUPLE_GOODS";
            this.colCoupleGoods.Name = "colCoupleGoods";
            this.colCoupleGoods.OptionsColumn.AllowEdit = false;
            this.colCoupleGoods.OptionsColumn.AllowMove = false;
            this.colCoupleGoods.OptionsColumn.ReadOnly = true;
            this.colCoupleGoods.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCoupleGoods.Visible = true;
            this.colCoupleGoods.VisibleIndex = 0;
            this.colCoupleGoods.Width = 100;
            // 
            // colShopCo
            // 
            this.colShopCo.Caption = "Mã siêu thị";
            this.colShopCo.FieldName = "SHOP_CODE";
            this.colShopCo.MinWidth = 10;
            this.colShopCo.Name = "colShopCo";
            this.colShopCo.OptionsColumn.AllowEdit = false;
            this.colShopCo.Visible = true;
            this.colShopCo.VisibleIndex = 1;
            this.colShopCo.Width = 103;
            // 
            // colShopnName
            // 
            this.colShopnName.Caption = "Siêu thị";
            this.colShopnName.FieldName = "SHOP_NAME";
            this.colShopnName.Name = "colShopnName";
            this.colShopnName.OptionsColumn.AllowEdit = false;
            this.colShopnName.Visible = true;
            this.colShopnName.VisibleIndex = 2;
            this.colShopnName.Width = 178;
            // 
            // colSLG30
            // 
            this.colSLG30.Caption = "Sản lượng 30 ngày";
            this.colSLG30.FieldName = "SLG30";
            this.colSLG30.Name = "colSLG30";
            this.colSLG30.OptionsColumn.AllowEdit = false;
            this.colSLG30.Visible = true;
            this.colSLG30.VisibleIndex = 4;
            // 
            // colDemoGoods
            // 
            this.colDemoGoods.Caption = "Sản lượng TB";
            this.colDemoGoods.FieldName = "SLTB";
            this.colDemoGoods.Name = "colDemoGoods";
            this.colDemoGoods.OptionsColumn.AllowEdit = false;
            this.colDemoGoods.Visible = true;
            this.colDemoGoods.VisibleIndex = 3;
            this.colDemoGoods.Width = 117;
            // 
            // colRemain
            // 
            this.colRemain.Caption = "Tồn";
            this.colRemain.FieldName = "REMAIN";
            this.colRemain.Name = "colRemain";
            this.colRemain.OptionsColumn.AllowEdit = false;
            this.colRemain.Visible = true;
            this.colRemain.VisibleIndex = 5;
            // 
            // colDmtb
            // 
            this.colDmtb.Caption = "ĐMTB";
            this.colDmtb.FieldName = "DMTB";
            this.colDmtb.Name = "colDmtb";
            this.colDmtb.OptionsColumn.AllowEdit = false;
            this.colDmtb.Visible = true;
            this.colDmtb.VisibleIndex = 6;
            // 
            // colNc
            // 
            this.colNc.Caption = "Nhu cầu";
            this.colNc.FieldName = "NC";
            this.colNc.Name = "colNc";
            this.colNc.OptionsColumn.AllowEdit = false;
            this.colNc.Visible = true;
            this.colNc.VisibleIndex = 7;
            // 
            // colParentShop
            // 
            this.colParentShop.Caption = "Kho tổng";
            this.colParentShop.FieldName = "PARENT_SHOP";
            this.colParentShop.Name = "colParentShop";
            this.colParentShop.OptionsColumn.AllowEdit = false;
            this.colParentShop.Visible = true;
            this.colParentShop.VisibleIndex = 8;
            // 
            // colCenterName
            // 
            this.colCenterName.Caption = "Khu vực";
            this.colCenterName.FieldName = "CENTER_NAME";
            this.colCenterName.Name = "colCenterName";
            this.colCenterName.OptionsColumn.AllowEdit = false;
            this.colCenterName.Visible = true;
            this.colCenterName.VisibleIndex = 9;
            this.colCenterName.Width = 162;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit4.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit5.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit6.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.MaxLength = 499;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // testThanhPT
            // 
            this.testThanhPT.AutoHeight = false;
            this.testThanhPT.Name = "testThanhPT";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnGetTemplate);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 472);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(784, 43);
            this.panel5.TabIndex = 1;
            // 
            // btnGetTemplate
            // 
            this.btnGetTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnGetTemplate.Location = new System.Drawing.Point(336, 8);
            this.btnGetTemplate.Name = "btnGetTemplate";
            this.btnGetTemplate.Size = new System.Drawing.Size(112, 27);
            this.btnGetTemplate.TabIndex = 1;
            this.btnGetTemplate.Text = "Lấy File import mẫu";
            this.btnGetTemplate.UseVisualStyleBackColor = true;
            this.btnGetTemplate.Click += new System.EventHandler(this.btnGetTemplate_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel8);
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(784, 200);
            this.panel3.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.btnFilter);
            this.panel8.Controls.Add(this.cboPage);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 163);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(784, 37);
            this.panel8.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(628, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Số dòng";
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(3, 5);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(113, 27);
            this.btnFilter.TabIndex = 0;
            this.btnFilter.Text = "Lọc thông tin";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // cboPage
            // 
            this.cboPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPage.FormattingEnabled = true;
            this.cboPage.Location = new System.Drawing.Point(687, 10);
            this.cboPage.Name = "cboPage";
            this.cboPage.Size = new System.Drawing.Size(86, 21);
            this.cboPage.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(784, 159);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin chia hàng";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.02314F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.97686F));
            this.tableLayoutPanel2.Controls.Add(this.cboGoodsGroup, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.cboProductType, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtTenLT, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(778, 102);
            this.tableLayoutPanel2.TabIndex = 9;
            // 
            // cboGoodsGroup
            // 
            this.cboGoodsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboGoodsGroup.Enabled = false;
            this.cboGoodsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGoodsGroup.Location = new System.Drawing.Point(148, 55);
            this.cboGoodsGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboGoodsGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboGoodsGroup.Name = "cboGoodsGroup";
            this.cboGoodsGroup.Padding = new System.Windows.Forms.Padding(3);
            this.cboGoodsGroup.Size = new System.Drawing.Size(630, 47);
            this.cboGoodsGroup.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 47);
            this.label5.TabIndex = 9;
            this.label5.Text = "Chọn nhóm hàng";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboProductType
            // 
            this.cboProductType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboProductType.Enabled = false;
            this.cboProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductType.Location = new System.Drawing.Point(148, 26);
            this.cboProductType.Margin = new System.Windows.Forms.Padding(0);
            this.cboProductType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboProductType.Name = "cboProductType";
            this.cboProductType.Padding = new System.Windows.Forms.Padding(3);
            this.cboProductType.Size = new System.Drawing.Size(630, 29);
            this.cboProductType.TabIndex = 8;
            this.cboProductType.SelectionChangeCommitted += new System.EventHandler(this.cbbType_SelectionChangeCommitted);
            this.cboProductType.Load += new System.EventHandler(this.cbbType_Load);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 29);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngành hàng";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTenLT
            // 
            this.txtTenLT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTenLT.Enabled = false;
            this.txtTenLT.Location = new System.Drawing.Point(151, 3);
            this.txtTenLT.Name = "txtTenLT";
            this.txtTenLT.Size = new System.Drawing.Size(624, 20);
            this.txtTenLT.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên lần tính";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnSave);
            this.panel4.Controls.Add(this.button12);
            this.panel4.Controls.Add(this.button11);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 118);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(778, 38);
            this.panel4.TabIndex = 8;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(183, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(124, 27);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Khởi tạo danh sách";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(470, 5);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(107, 27);
            this.button12.TabIndex = 7;
            this.button12.Text = "Kết xuất";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(337, 5);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(106, 27);
            this.button11.TabIndex = 6;
            this.button11.Text = "Import";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // frmGoodsDividingListPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1208, 515);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmGoodsDividingListPK";
            this.Text = "Tính nhu cầu chia hàng phụ kiện";
            this.Load += new System.EventHandler(this.frmGoodsDividingList_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsFinished)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.contextMenuAll.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDevideItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDevideItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testThanhPT)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        private void grdData_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "DIVISION_STATUS")
            {
                string strStatus = (e.Value ?? "").ToString().Trim();
                //|| string.IsNullOrEmpty(strStatus)
                if (strStatus == "0")
                {
                    e.DisplayText = "Chưa chia";
                    return;
                }
                else if (strStatus == "1")
                {
                    e.DisplayText = "Đã chia";
                    return;
                }
            }
        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repIsDeleted;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repIsFinished;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExitSys;
        private System.Windows.Forms.Button btnDellNC;
        private System.Windows.Forms.Button btnCreate;
        private DevExpress.XtraGrid.GridControl gridControlShop;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewShop;
        private DevExpress.XtraGrid.Columns.GridColumn colShopCode;
        private DevExpress.XtraGrid.Columns.GridColumn colShopName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnDeleteAll;
        private System.Windows.Forms.Button btnDeleteShop;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.Button btnImport;

        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboShop;
        private DevExpress.XtraGrid.Columns.GridColumn colRowCount;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colStockId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnExitCreate;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboPage;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnGetTemplate;
        private System.Windows.Forms.ContextMenuStrip contextMenuAll;
        private System.Windows.Forms.ToolStripMenuItem toolStripAll;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMB;
        private System.Windows.Forms.ToolStripMenuItem toolStripMN;
        private DevExpress.XtraGrid.GridControl gridControlDevideItem;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDevideItem;
        private DevExpress.XtraGrid.Columns.GridColumn colCoupleGoods;
        private DevExpress.XtraGrid.Columns.GridColumn colShopCo;
        private DevExpress.XtraGrid.Columns.GridColumn colShopnName;
        private DevExpress.XtraGrid.Columns.GridColumn colDemoGoods;
        private DevExpress.XtraGrid.Columns.GridColumn colSLG30;
        private DevExpress.XtraGrid.Columns.GridColumn colRemain;
        private DevExpress.XtraGrid.Columns.GridColumn colDmtb;
        private DevExpress.XtraGrid.Columns.GridColumn colNc;
        private DevExpress.XtraGrid.Columns.GridColumn colParentShop;
        private DevExpress.XtraGrid.Columns.GridColumn colCenterName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit testThanhPT;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTenLT;
        private System.Windows.Forms.Label label3;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString cboProductType;
        private System.Windows.Forms.Label label5;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString cboGoodsGroup;
    }
}