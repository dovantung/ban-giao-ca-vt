﻿using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class frmSameLevelDividingImportExcel : Form
    {
        public enum IMPORT_TYPE { IMPORT_GOODS,IMPORT_SHOP};
        #region Variable

        private bool bolIsUpdate = false;
        private DataTable dtbSource = new DataTable();
	    private ERP.Inventory.PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = null;
        private IMPORT_TYPE import_type;

        public frmSameLevelDividingImportExcel(bool is_import_shop)
        {
            if (is_import_shop)
            {
                import_type = IMPORT_TYPE.IMPORT_SHOP;
            }
            else
            {
                import_type = IMPORT_TYPE.IMPORT_GOODS;
            }
            InitializeComponent();
        }
       
        public bool IsUpdate
        {
            get { return bolIsUpdate; }
        }

        public DataTable SourceTable
        {
            get { return dtbSource; }
            set { dtbSource = value; }
        }

        private DataTable dtbGoodsResource;

        public DataTable DtbGoodsResource
        {
            get { return dtbGoodsResource; }
            set { dtbGoodsResource = value; }
        }

        private DataTable dtbShopResource;

        public DataTable DtbShopResource
        {
            get { return dtbShopResource; }
            set { dtbShopResource = value; }
        }

	    #endregion


        #region Constructor 

        public frmSameLevelDividingImportExcel()
        {
            InitializeComponent();
        }

        #endregion

        #region Event		 

        private void frmGoodsDmtbImportExcel_Load(object sender, EventArgs e)
        {
            string[] fieldNames = null;

            if (import_type.Equals(IMPORT_TYPE.IMPORT_GOODS))
            {
                fieldNames = new string[] { "GOODS_CODE", "ISERROR", "NOTE" };
            }
            if (import_type.Equals(IMPORT_TYPE.IMPORT_SHOP))
            {
                fieldNames = new string[] { "SHOP_CODE", "SHOP_NAME", "ISERROR", "NOTE" };
            }
            

            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
           
                FormatGrid();
            
            grdData.DataSource = dtbSource;

            if (import_type.Equals(IMPORT_TYPE.IMPORT_GOODS))
            {
                ValidateDataGoods(ref dtbSource);
            }
            if (import_type.Equals(IMPORT_TYPE.IMPORT_SHOP))
            {
                ValidateDataShop(ref dtbSource);
            }
            
            btnUpdate.Enabled = dtbSource.Rows.Count > 0;
        }

        private void findValueByCode(ref string goods_name, ref string goods_id, string goods_code)
        {
            goods_name = "";
            goods_id = "";
            if (dtbGoodsResource == null || dtbGoodsResource.Rows.Count == 0)
            {
                return;
            }
            for (int i = 0; i < dtbGoodsResource.Rows.Count; i++)
            {
                if (dtbGoodsResource.Rows[i]["code"].ToString().Equals(goods_code))
                {
                    goods_name = dtbGoodsResource.Rows[i]["code"].ToString();
                    goods_id = dtbGoodsResource.Rows[i]["code"].ToString();
                    return;
                }
            }
        }

        private void findShopByCode(ref string shop_name, ref string stock_id, string shop_code)
        {
            shop_name = "";
            stock_id = "";
            if (dtbShopResource == null || dtbShopResource.Rows.Count == 0)
            {
                return;
            }
            for (int i = 0; i < dtbShopResource.Rows.Count; i++)
            {
                if (dtbShopResource.Rows[i]["shop_code"].ToString().Equals(shop_code))
                {
                    shop_name = dtbShopResource.Rows[i]["name"].ToString();
                    stock_id = dtbShopResource.Rows[i]["stock_id"].ToString();
                    return;
                }
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Chương trình sẽ chỉ lấy những dữ liệu hợp lệ. Bạn có muốn tiếp tục không?", "Thống báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                bolIsUpdate = true;
            }
            Close();
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {

        }

        private void grvIMEI_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            bool bolIsError = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["ISERROR"]));
            if (bolIsError)
                e.Appearance.BackColor = Color.Pink;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region Method

        private bool FormatGrid()
        {
            try
            {
                if (import_type.Equals(IMPORT_TYPE.IMPORT_GOODS))
                {
                    grvIMEI.Columns["GOODS_CODE"].Caption = "Mã ghép";
                    grvIMEI.Columns["ISERROR"].Caption = "Lỗi";
                    grvIMEI.Columns["NOTE"].Caption = "Ghi lỗi";

                    grvIMEI.OptionsView.ColumnAutoWidth = false;
                    grvIMEI.Columns["GOODS_CODE"].Width = 150;
                    grvIMEI.Columns["ISERROR"].Width = 80;
                    grvIMEI.Columns["NOTE"].Width = 500;
                }

                if (import_type.Equals(IMPORT_TYPE.IMPORT_SHOP))
                {
                    grvIMEI.Columns["SHOP_CODE"].Caption = "Mã siêu thị";
                    grvIMEI.Columns["SHOP_NAME"].Caption = "Số siêu thị";
                    grvIMEI.Columns["ISERROR"].Caption = "Lỗi";
                    grvIMEI.Columns["NOTE"].Caption = "Ghi lỗi";

                    grvIMEI.OptionsView.ColumnAutoWidth = false;
                    grvIMEI.Columns["SHOP_CODE"].Width = 150;
                    grvIMEI.Columns["SHOP_NAME"].Width = 250;
                    grvIMEI.Columns["ISERROR"].Width = 80;
                    grvIMEI.Columns["NOTE"].Width = 500;
                }
                

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsError = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsError.ValueChecked = true;
                chkCheckBoxIsError.ValueUnchecked = false;
                grvIMEI.Columns["ISERROR"].ColumnEdit = chkCheckBoxIsError;

                grvIMEI.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grvIMEI.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grvIMEI.OptionsView.ShowAutoFilterRow = true;
                grvIMEI.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grvIMEI.OptionsView.ShowFooter = true;
                grvIMEI.OptionsView.ShowGroupPanel = false;
                grvIMEI.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grvIMEI.ColumnPanelRowHeight = 40;

                grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "ISERROR";
                grvIMEI.Columns["NOTE"].SummaryItem.DisplayFormat = "{0:n0}";
                grvIMEI.Columns["NOTE"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;

               //grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "NOTE";
                grvIMEI.Columns["ISERROR"].SummaryItem.DisplayFormat = "Tổng số dòng:";
                grvIMEI.Columns["ISERROR"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool ValidateDataGoods(ref DataTable dtbSource)
        {
            dtbSource.Rows.RemoveAt(0);
            int i = 0;
            dtbSource.Columns[i++].ColumnName = "GOODS_CODE";

            DataColumn dtcIsError = new DataColumn("ISERROR", typeof(bool));
            dtcIsError.DefaultValue = false;
            dtbSource.Columns.Add(dtcIsError);
            DataColumn dtcNote = new DataColumn("NOTE", typeof(string));
            dtbSource.Columns.Add(dtcNote);
            DataColumn dtcStockId = new DataColumn("VALUE_ID", typeof(string));
            dtbSource.Columns.Add(dtcStockId);
            dtbSource.Columns["VALUE_ID"].ColumnMapping = MappingType.Hidden;
            DataColumn NAME = new DataColumn("NAME", typeof(string));
            dtbSource.Columns.Add(NAME);
            dtbSource.Columns["NAME"].ColumnMapping = MappingType.Hidden;
            dtbSource.Columns["GOODS_CODE"].SetOrdinal(dtbSource.Columns["GOODS_CODE"].Ordinal + 1);

            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();

            foreach (DataRow row in dtbSource.Rows)
            {
                validateRowImport(row);
            }
            dtbSource.AcceptChanges();
            return true;
        }

        private void validateRowImport(DataRow row)
        {
            string errNote = "";
            String goods_name = "";
            String value_id = "";

            findValueByCode(ref goods_name, ref value_id, row["GOODS_CODE"].ToString());
            if (value_id.Equals(null)
                || value_id.Equals(""))
            {
                errNote += "không tìm thấy hàng có mã (" + row["GOODS_CODE"].ToString() + ") ; ";
            }
          

            if (!errNote.Equals(""))
            {
                row["IsError"] = true;
                row["NOTE"] = errNote;
            }
            else
            {
                row["NAME"] = goods_name;
                row["value_id"] = value_id;
            }
            
               
        }

        private bool ValidateDataShop(ref DataTable dtbSource)
        {
            dtbSource.Rows.RemoveAt(0);
            int i = 0;
            dtbSource.Columns[i++].ColumnName = "SHOP_CODE";

            DataColumn dtcIsError = new DataColumn("ISERROR", typeof(bool));
            dtcIsError.DefaultValue = false;
            dtbSource.Columns.Add(dtcIsError);
            DataColumn dtcNote = new DataColumn("NOTE", typeof(string));
            dtbSource.Columns.Add(dtcNote);
            DataColumn dtcStockId = new DataColumn("STOCK_ID", typeof(string));
            dtbSource.Columns.Add(dtcStockId);
            dtbSource.Columns["STOCK_ID"].ColumnMapping = MappingType.Hidden;
            DataColumn NAME = new DataColumn("SHOP_NAME", typeof(string));
            dtbSource.Columns.Add(NAME);
            dtbSource.Columns["SHOP_NAME"].ColumnMapping = MappingType.Hidden;
            dtbSource.Columns["SHOP_CODE"].SetOrdinal(dtbSource.Columns["SHOP_CODE"].Ordinal + 1);

            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();

            foreach (DataRow row in dtbSource.Rows)
            {
                validateRowImportShop(row);
            }
            dtbSource.AcceptChanges();
            return true;
            
        }

        private void validateRowImportShop(DataRow row)
        {
            string errNote = "";
            String shop_name = "";
            String stock_id = "";

            findShopByCode(ref shop_name, ref stock_id, row["SHOP_CODE"].ToString());
            if (stock_id.Equals(null)
                || stock_id.Equals(""))
            {
                errNote += "không tìm thấy mã siêu thị (" + row["SHOP_CODE"].ToString() + ") ; ";
            }


            if (!errNote.Equals(""))
            {
                row["IsError"] = true;
                row["NOTE"] = errNote;
            }
            else
            {
                row["SHOP_NAME"] = shop_name;
                row["STOCK_ID"] = stock_id;
            }


        }
        #endregion

        
    }

}
