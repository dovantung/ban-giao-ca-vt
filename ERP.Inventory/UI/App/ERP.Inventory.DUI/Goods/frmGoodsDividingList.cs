﻿using GemBox.Spreadsheet;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class frmGoodsDividingList : Form
    {
        Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection selection;
        private DataTable dtbResourceType = null;
        private DataTable dtbResourceGroup = null;
        private DataTable dtbResourceST = null;    //Combobox Siêu thị
        private DataTable dtbResourceStore = null; //Các lần tính nhu cầu
        private DataTable dtbResourceShop = null;  //danh sách siêu thị
        private DataTable dtbResourceDevideItem = null; //danh sách devide item
        private string id = "";
        private PLC.Goods.PLCGoodsTsvq plcObject;
        private PLC.Goods.PLCGoodsDevidingList plcGoodsDevidingList;
        private ERP.Inventory.DUI.Goods.DUIGoodsTsvq.SQL_COMMON sql_common;
        private StringBuilder lstShopid = new StringBuilder();

        public frmGoodsDividingList()
        {
            InitializeComponent();
            sql_common = new ERP.Inventory.DUI.Goods.DUIGoodsTsvq.SQL_COMMON();
            plcGoodsDevidingList = new PLC.Goods.PLCGoodsDevidingList();
            plcObject = new PLC.Goods.PLCGoodsTsvq();
        }

        private void cbbType_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            if (cboProductType.ColumnID == null || cboProductType.ColumnID.ToString().Equals(""))
            {
                dtbResourceGroup = null;
            }
            else
            {
                Dictionary<string, string> map = new Dictionary<string, string>() {
                                                {":product_type", cboProductType.ColumnID.ToString()},};
                dtbResourceGroup = SearchCommon(sql_common.SQL_GOOD_GROUP, map);
                
            }
            cboGoodsGroup.InitControl(true, dtbResourceGroup, "goods_group_id", "NAME", "");
        }

        private void cbbType_Load(object sender, EventArgs e)
        {
            dtbResourceType = SearchCommon(sql_common.SQL_PRODUCT_TYPE, null);
            cboProductType.InitControl(false, dtbResourceType, "CODE", "NAME", "");
        }

        private DataTable SearchCommon(StringBuilder sqlSelect, Dictionary<string, string> dictParam)
        {
            StringBuilder sql = new StringBuilder(sqlSelect.ToString());
            if (dictParam != null && dictParam.Count > 0)
            {
                foreach (KeyValuePair<string, string> item in dictParam)
                {
                    sql.Replace(item.Key, "'" + item.Value + "'");
                }
            }
            object[] objKeywords = new object[]
            {
              "@command",sql.ToString()
            };
            return plcObject.SearchData(objKeywords);
        }

        private void frmGoodsDividingList_Load(object sender, EventArgs e)
        {
            object[] objKeywords = new object[]{
                    "@type", 0
                };
            dtbResourceStore = plcGoodsDevidingList.LoadListDevideHis(objKeywords);
            grdData.DataSource = dtbResourceStore;

            dtbResourceST = SearchCommon(new StringBuilder("select SHOP_ID STOCK_ID, SHOP_CODE, SHOP_NAME NAME from shop"), null);

            cboShop.InitControl(false, dtbResourceST, "STOCK_ID", "NAME", "");

            objKeywords = new object[]{
                    "@goods_division_history_id", "-1"
            };
            dtbResourceShop = plcGoodsDevidingList.LoadShopTable(objKeywords);
            gridControlShop.DataSource = dtbResourceShop;

            dtbResourceDevideItem = plcGoodsDevidingList.LoadDevideItemValue(objKeywords);
            gridControlDevideItem.DataSource = dtbResourceDevideItem;

            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.btnImport, "File import:\n    Mã siêu thị");
            DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (string.IsNullOrEmpty(id))
                {
                    MessageBox.Show("Bạn phải chọn bản ghi để xóa", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (MessageBox.Show(this, "Bạn có muốn xóa không?", "Xác nhận"
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                    == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
                
                object[] objKeywords = new object[]{
                    "@id", id
                };
                plcGoodsDevidingList.deleteGoodsList(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                for (int i = 0; i < dtbResourceStore.Rows.Count; i++)
                {
                    DataRow row = dtbResourceStore.Rows[i];
                    string cellId = row["GOODS_DIVISION_HISTORY_ID"].ToString();
                    if (id.Equals(cellId))
                    {
                        dtbResourceStore.Rows.Remove(row);
                        break;
                    }
                }
                if (this.dtbResourceShop != null)
                {
                    this.dtbResourceShop.Clear();
                }
                if (this.dtbResourceDevideItem != null)
                {
                    this.dtbResourceDevideItem.Clear();
                }
                id = "";
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message, ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc chắn muốn thoát chức năng?", "Thông báo", MessageBoxButtons.YesNo, 
                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
            {
            }
            else
            {
                this.Close();
            }
        }

        private void grdData_Click_1(object sender, EventArgs e)
        {
            try {
                this.Cursor = Cursors.WaitCursor;
                if (selection != null)
                    selection.ClearSelection();
                DataRow dtRow = grdViewData.GetFocusedDataRow();
                if (dtRow != null || dtRow["GOODS_DIVISION_HISTORY_ID"].ToString() != "")
                {
                    id = dtRow["GOODS_DIVISION_HISTORY_ID"].ToString();
                    object[] objKeywords = new object[]{
                    "@goods_division_history_id", id
                        };
                    dtbResourceShop = plcGoodsDevidingList.LoadShopTable(objKeywords);
                    gridControlShop.DataSource = dtbResourceShop;

                    dtbResourceDevideItem = plcGoodsDevidingList.LoadDevideItemValue(objKeywords);
                    gridControlDevideItem.DataSource = dtbResourceDevideItem;

                    if (selection == null)
                    {
                        selection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, gridViewDevideItem);
                        selection.CheckMarkColumn.VisibleIndex = 0;
                        selection.CheckMarkColumn.Width = 40;
                        selection.ClearSelection();
                    }
                    if (this.frmDialogDividingFilter != null)
                    {
                        this.frmDialogDividingFilter.refreshValue();
                    }
                    
                }
                this.Cursor = Cursors.Default;
            } catch(Exception ex){
                 Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                   ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try {
                //DataRow dtRow = grdViewData.GetFocusedDataRow();
                //if (dtRow == null || dtRow["GOODS_DIVISION_HISTORY_ID"].ToString() == "")
                //{
                //    MessageBox.Show("Bạn phải chọn bản ghi lần tính nhu cầu", "Thông báo",
                //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    return;
                //}
                //id = dtRow["GOODS_DIVISION_HISTORY_ID"].ToString();
                DataRow dtRowShop = gridViewShop.GetFocusedDataRow();
                if (dtRowShop == null || dtRowShop["STOCK_ID"].ToString() == "")
                {
                    MessageBox.Show("Bạn phải chọn bản ghi siêu thị", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string stockId = dtRowShop["STOCK_ID"].ToString();
                
                dtbResourceShop.Rows.Remove(dtRowShop);

                lstShopid.Replace("," + stockId + ",", "");
                dtbResourceShop.AcceptChanges();
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                  ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        public void deleteShop(String id, String idShop, int type)
        {
            object[] objKeywords = new object[]{
                    "@goods_division_history_id", id,
                    "@param", idShop, 
                     "@type", type,
             };
            plcGoodsDevidingList.deleteShop(objKeywords);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                lstShopid.Clear();
                dtbResourceShop.Rows.Clear();
                dtbResourceShop.AcceptChanges();
                gridControlShop.DataSource = dtbResourceShop;
                this.Refresh();

            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                  ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboShop.ColumnID > 0)
                {
                    if (!lstShopid.ToString().Contains("," + cboShop.ColumnID + ",")) { 
                    
                        DataRow data = dtbResourceShop.NewRow();

                        string shopCode = "";
                        for (int i = 0; i < dtbResourceST.Rows.Count; i++)
                        {
                            if (dtbResourceST.Rows[i]["STOCK_ID"].ToString().Equals(cboShop.ColumnID.ToString()))
                            {
                                shopCode = dtbResourceST.Rows[i]["SHOP_CODE"].ToString();
                                
                                break;
                            }
                        }
                        lstShopid.Append("," + cboShop.ColumnID + ",");
                        data["SHOP_CODE"] = shopCode;
                        data["NAME"] = cboShop.ColumnName;
                        data["STOCK_ID"] = cboShop.ColumnID;

                        dtbResourceShop.Rows.Add(data);
                        dtbResourceShop.AcceptChanges();
                    }
                }
                else
                {
                    MessageBox.Show("Bạn phải chọn bản siêu thị", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message, ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(test));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
           
           
        }
        public void test()
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strFileName = string.Empty;
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\import_nhucau_chiahang.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false)
                return;
            strFileName = System.IO.Path.GetFileNameWithoutExtension(strPathFileName);
            if (saveFile(strFileTemplate, strFileName, strPathFileName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
        }

        private bool saveFile(string strFileTemplate, string strFileName, string strPathFileName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
                workbook.SaveExcel(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }
        SaveFileDialog saveDlg = null;
        private Boolean CheckFileTemplate(String strFileTemplate, ref string strPathFileName)
        {
            try
            {
                saveDlg = new SaveFileDialog();
                
                saveDlg.Filter = "Excel Worksheet file(*.xlsx)|*.xls";
                saveDlg.AddExtension = true;
                saveDlg.RestoreDirectory = true;
                saveDlg.Title = "Chọn vị trí lưu file?";
                saveDlg.FileName = "import_nhucau_chiahang_" + System.DateTime.Now.Date.ToString("dd-MM-yyyy");
                if (saveDlg.ShowDialog() == DialogResult.OK)
                {
                    strPathFileName = System.IO.Path.ChangeExtension(saveDlg.FileName, ".xlsx");
                }
                if (string.IsNullOrEmpty(strPathFileName)) return false;

                if (System.IO.File.Exists(strPathFileName))
                {
                    try
                    {
                        System.IO.File.Delete(strPathFileName);
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("File Excel đang được mở, Bạn phải đóng lại trước khi mở file tiếp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }

                if (System.IO.File.Exists(strFileTemplate) == false)
                {
                    MessageBox.Show("Tập tin mẫu không tồn tại");
                    return false;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }

        private void OpenFileExport(String strPathFileName)
        {
            try
            {
                if (MessageBox.Show("Bạn có muốn mở file vừa lưu?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(strPathFileName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = this.Cursor = Cursors.WaitCursor;
                if (selection != null)
                    selection.ClearSelection();
                if (dtbResourceShop == null || dtbResourceShop.Rows.Count <= 0)
                {
                    MessageBox.Show("Danh sách siêu thị không được rỗng", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrEmpty(txtTenLT.Text))
                {
                    MessageBox.Show("Tên lần tính không được để trống", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrEmpty(cboGoodsGroup.ColumnIDList))
                {
                    MessageBox.Show("Bạn phải chọn nhóm hàng", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //pck_dividing.add_goods_division_history
                string user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                string user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                object[] objKeywords = new object[]{
                    "@type", 0,
                    "@name", txtTenLT.Text.Trim(), 
                    "@user_id", user_id,
                    "@user_shop_id", user_shop_id
                 };
                DataTable tmp = plcGoodsDevidingList.AddGoodsDivisionHistory(objKeywords, lstShopid.ToString(), 
                    cboGoodsGroup.ColumnIDList, "0");
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        this.Cursor = this.Cursor = Cursors.Default;
                        return;
                }
                if (tmp != null && tmp.Rows.Count > 0)
                {
                     DataRow data = dtbResourceStore.NewRow();
                     id = tmp.Rows[0]["GOODS_DIVISION_HISTORY_ID"].ToString();
                     data["GOODS_DIVISION_HISTORY_ID"] = id;
                     data["NAME"] = tmp.Rows[0]["NAME"].ToString();
                     data["CREATE_DATETIME"] = tmp.Rows[0]["CREATE_DATETIME"].ToString();
                     data["DIVISION_STATUS"] = tmp.Rows[0]["DIVISION_STATUS"].ToString();
                     data["ROW_COUNT"] = tmp.Rows[0]["ROW_COUNT"];

                     dtbResourceStore.Rows.InsertAt(data, 0);
                     dtbResourceStore.AcceptChanges();

                     objKeywords = new object[]{
                        "@goods_division_history_id", id
                     };
                     dtbResourceShop = plcGoodsDevidingList.LoadShopTable(objKeywords);
                     gridControlShop.DataSource = dtbResourceShop;

                     dtbResourceDevideItem = plcGoodsDevidingList.LoadDevideItemValue(objKeywords);
                     gridControlDevideItem.DataSource = dtbResourceDevideItem;
                     btnExitCreate_Click(null, null);

                     if (selection == null)
                     {
                         selection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, gridViewDevideItem);
                         selection.CheckMarkColumn.VisibleIndex = 0;
                         selection.CheckMarkColumn.Width = 40;
                         selection.ClearSelection();
                     }
                }
                this.Cursor = this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                  ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            id = "";
            this.txtTenLT.Enabled = true;
            this.btnSave.Enabled = true;
            this.btnImport.Enabled = true;
            this.btnAll.Enabled = true;
            this.btnAdd.Enabled = true;
            this.btnDeleteShop.Enabled = true;
            this.btnDeleteAll.Enabled = true;
            this.cboShop.Enabled = true;
            this.lstShopid.Clear();
            if (this.dtbResourceShop != null)
            {
                this.dtbResourceShop.Clear();
            }
            if (this.dtbResourceDevideItem != null)
            {
                this.dtbResourceDevideItem.Clear();
            }
            //---------------
            this.btnDellNC.Visible = false;
            this.btnExitCreate.Visible = true;
            this.btnExitSys.Visible = false;

            this.grdData.Enabled = false;

            this.cboProductType.Enabled = true;
            this.cboGoodsGroup.Enabled = true;
            if (frmDialogDividingFilter != null)
            {
                this.frmDialogDividingFilter.refreshValue();
            }
            
        }

        private void btnExitCreate_Click(object sender, EventArgs e)
        {
            this.txtTenLT.Enabled = false;
            this.btnSave.Enabled = false;
            this.btnImport.Enabled = false;
            this.btnAll.Enabled = false;
            this.btnAdd.Enabled = false;
            this.btnDeleteShop.Enabled = false;
            this.btnDeleteAll.Enabled = false;
            this.cboShop.Enabled = false;

            this.btnDellNC.Visible = true;
            this.btnExitCreate.Visible = false;
            this.btnExitSys.Visible = true;

            this.grdData.Enabled = true;

            this.cboProductType.Enabled = false;
            this.cboGoodsGroup.Enabled = false;
            this.txtTenLT.Text = "";
            this.cboProductType.ResetDefaultValue();
            this.cboGoodsGroup.ResetDefaultValue();
        }

        private void btnDeleteGoodsDivisionItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (selection == null || selection.SelectedCount <= 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng tích chọn các nhu cầu chia hàng");
                    return;
                }
                if (string.IsNullOrEmpty(id))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn lần tính nhu cầu chia hàng");
                    return;
                }
                StringBuilder goodsDivisionItems = new StringBuilder();
                for (int i = 0; i < selection.SelectedCount; i++)
                {
                    DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                    goodsDivisionItems.Append("|" + objRow["GOODS_ID"] + "," + objRow["STOCK_ID"]);
                }
                plcGoodsDevidingList.DeleteGoodsDivisionItem(goodsDivisionItems.ToString(), id);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                for (int i = 0; i < selection.SelectedCount; i++)
                {
                    dtbResourceDevideItem.Rows.Remove(selection.GetSelectedRow(i) as DataRow);
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                  ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }
        ERP.Inventory.DUI.Goods.frmDialogDividingFilter frmDialogDividingFilter = null;
        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(id))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn lần tính nhu cầu chia hàng");
                return;
            }
            if(frmDialogDividingFilter == null){
                frmDialogDividingFilter = new Goods.frmDialogDividingFilter(this, dtbResourceST, "frmGoodsDividingList");
                frmDialogDividingFilter.StartPosition = FormStartPosition.CenterParent;
            }
            frmDialogDividingFilter.ShowDialog();
        }
        public void filterGoodsDividingList(string goodsGroup, string goodsCode, string doubleCode, string shopCode,
            string center, string zone)
        {
            object[] objKeywords = new object[]{
                    "@goods_division_history_id", id,
                    "@goods_group", goodsGroup.Equals("-1") ? "" : goodsGroup,
                    "@goods_code", goodsCode,
                    "@couple_goods_code", doubleCode,
                    "@shop_code", shopCode,
                    "@center_code", center.Equals("-1") ? "" : center ,
                    "@zone", zone,
                    "@rownum", null,
                        };
            dtbResourceDevideItem = plcGoodsDevidingList.SearchDevideItem(objKeywords);
            gridControlDevideItem.DataSource = dtbResourceDevideItem;
        }

        private void toolStripAll_Click(object sender, EventArgs e)
        {
            dtbResourceShop = dtbResourceST;
            gridControlShop.DataSource = dtbResourceST;
            lstShopid.Clear();
            for (int i = 0; i < dtbResourceShop.Rows.Count; i++)
            {
                lstShopid.Append("," + dtbResourceST.Rows[i]["STOCK_ID"].ToString() + ",");
            }
        }

        private void testToolStripMB_Click(object sender, EventArgs e)
        {
            dtbResourceShop = SearchCommon(new StringBuilder("select SHOP_ID STOCK_ID, SHOP_CODE, SHOP_NAME NAME from shop where PAR_SHOP_CODE = 'VTMB'"), null);
            gridControlShop.DataSource = dtbResourceShop;
            lstShopid.Clear();
            for (int i = 0; i < dtbResourceShop.Rows.Count; i++)
            {
                lstShopid.Append("," + dtbResourceST.Rows[i]["STOCK_ID"].ToString() + ",");
            }
        }

        private void toolStripMN_Click(object sender, EventArgs e)
        {
            dtbResourceShop = SearchCommon(new StringBuilder("select SHOP_ID STOCK_ID, SHOP_CODE, SHOP_NAME NAME from shop where PAR_SHOP_CODE = 'VTMN'"), null);
            gridControlShop.DataSource = dtbResourceShop;
            lstShopid.Clear();
            for (int i = 0; i < dtbResourceShop.Rows.Count; i++)
            {
                lstShopid.Append("," + dtbResourceST.Rows[i]["STOCK_ID"].ToString() + ",");
            }
            
        }

        public Dictionary<String, String> GetDict(DataTable dt)
        {
            Dictionary<String, String> tmp = new Dictionary<string,string>();
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dtbResourceStore.Rows[i];
                    tmp.Add(dt.Rows[i]["SHOP_CODE"].ToString(), dt.Rows[i]["STOCK_ID"].ToString() + "|" + dt.Rows[i]["NAME"].ToString());
                }
            }
            return tmp;
        }
        
        private void btnImport_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(importShop));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();

        }
        public void importShop()
        {
            OpenFileDialog Openfile = new OpenFileDialog();
            Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
            DialogResult result = Openfile.ShowDialog();
            if (result == DialogResult.Cancel) return;
            try
            {
                DataTable dt = new DataTable();
                dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);
                Dictionary<string, string> shopDic = GetDict(dtbResourceST);
                Dictionary<string, string> shopTableDic = null;
                if(dtbResourceShop != null && dtbResourceShop.Rows.Count > 0){
                    shopTableDic = GetDict(dtbResourceShop);
                }
                
                if (dt != null)
                {
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        string shopCode = dt.Rows[i].Field<string>(0);
                        if (!string.IsNullOrEmpty(shopCode) && shopDic.ContainsKey(shopCode.Trim()))
                        {
                            shopCode = shopCode.Trim();
                            if (shopTableDic == null)
                            {
                                shopTableDic = new Dictionary<string, string>();
                            }
                            if (!shopTableDic.ContainsKey(shopCode))
                            {
                                DataRow data = dtbResourceShop.NewRow();

                                data["SHOP_CODE"] = shopCode;
                                String tmp = shopDic[shopCode];
                                String[] arrTmp = tmp.Split('|');

                                data["NAME"] = arrTmp[1];
                                data["STOCK_ID"] = arrTmp[0];

                                shopTableDic.Add(shopCode, tmp);
                                lstShopid.Append("," + arrTmp[0] + ",");
                                dtbResourceShop.Rows.Add(data);
                            }
                        }
                    }
                    dtbResourceShop.AcceptChanges();
                }

            }
            catch (Exception objex)
            {
                MessageBox.Show(objex.Message.ToString(), "Lỗi nạp thông tin từ file Excel!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnImportDevidingItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(id))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn lần tính nhu cầu chia hàng");
                return;
            }
            else
            {
                Thread thdSyncRead = new Thread(new ThreadStart(importDevidingItem));
                thdSyncRead.SetApartmentState(ApartmentState.STA);
                thdSyncRead.Start();
            }
            
        }

        public void importDevidingItem()
        {
            List<object[]> lsImport = new List<object[]>();            
            OpenFileDialog Openfile = new OpenFileDialog();
            Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
            DialogResult result = Openfile.ShowDialog();
            if (result == DialogResult.Cancel) return;
            try
            {
                DataTable dt = new DataTable();
                dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);


                object[] objKeywords = new object[]{
                    "@goods_division_history_id", id
                        };
                DataTable dtShop = new DataTable();
                dtShop = plcGoodsDevidingList.ImportDividingListShop(objKeywords);

                DataTable dtGood = new DataTable();
                dtGood = plcGoodsDevidingList.ImportDividingListGood(objKeywords);

                frmGoodsDivingListImport frm = new frmGoodsDivingListImport("1");
                frm.SourceTable = dt;
                frm.DtbShopResource = dtShop;
                frm.DtbCoupleGoodsResource = dtGood;
                frm.Goods_division_history_id = id;
                frm.ShowDialog();
                if (frm.IsUpdate)
                {
                    object[][] arrayObject = frm.LsImport.ToArray();

                    plcGoodsDevidingList.ImportSerialByExcel(arrayObject);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    MessageBox.Show("Import " + frm.LsImport.Count + " Bản ghi!", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);


                    objKeywords = new object[]{
                        "@goods_division_history_id", id
                     };
                    dtbResourceShop = plcGoodsDevidingList.LoadShopTable(objKeywords);
                    gridControlShop.DataSource = dtbResourceShop;

                    dtbResourceDevideItem = plcGoodsDevidingList.LoadDevideItemValue(objKeywords);
                    gridControlDevideItem.DataSource = dtbResourceDevideItem;
                    btnExitCreate_Click(null, null);

                    if (selection == null)
                    {
                        selection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, gridViewDevideItem);
                        selection.CheckMarkColumn.VisibleIndex = 0;
                        selection.CheckMarkColumn.Width = 40;
                        selection.ClearSelection();
                    }

                }



            }
            catch (Exception objex)
            {
                MessageBox.Show(objex.Message.ToString(), "Lỗi nạp thông tin từ file Excel!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(export));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }

        public void export()
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strFileName = string.Empty;
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\dividing_list_report.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false)
                return;
            strFileName = System.IO.Path.GetFileNameWithoutExtension(strPathFileName);

            if (exportFile(dtbResourceDevideItem, strFileTemplate, strFileName, strPathFileName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
        }

        private bool exportFile(DataTable dtbResourceDevideItem, string strFileTemplate, string strFileName, string strPathFileName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                if (sheet != null)
                {
                    #region Insert Header
                    DateTime dateTime = DateTime.UtcNow.Date;
                    string strReviewedDate = string.Format(@"Ngày: {0}", dateTime.ToString("dd/MM/yyyy"));
                    InsertValueCell(ref sheet, "M", 1, strReviewedDate, false, false);
                    #endregion
                    int iRowStart = 4;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    for (int i = 0; i < dtbResourceDevideItem.Rows.Count; i++)
                        {
                            AddRowDetail(ref sheet, dtbResourceDevideItem.Rows[i], ref iRowCurrent, ref intSTT);
                        }
                }
            }
            try
            {
                workbook.SaveExcel(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void AddRowDetail(ref ExcelWorksheet sheet, DataRow row, ref int iRow, ref int iSTT)
        {
            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["COUPLE_GOODS"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["SHOP_CODE"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["SHOP_NAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, row["DEMO_GOODS"].ToString(), false, false);
            InsertValueCell(ref sheet, "F", iRow, row["SLG30"].ToString(), false, false);
            InsertValueCell(ref sheet, "G", iRow, row["REMAIN"].ToString(), false, false);
            InsertValueCell(ref sheet, "H", iRow, row["HCL"].ToString(), false, false);
            InsertValueCell(ref sheet, "I", iRow, "", false, false);
            InsertValueCell(ref sheet, "J", iRow, row["DMTB"].ToString(), false, false);
            InsertValueCell(ref sheet, "K", iRow, row["TSVQ"].ToString(), false, false);
            InsertValueCell(ref sheet, "L", iRow, row["DM"].ToString(), false, false);
            InsertValueCell(ref sheet, "M", iRow, row["NC"].ToString(), false, false);
            InsertValueCell(ref sheet, "N", iRow, row["PARENT_SHOP"].ToString(), false, false);
            InsertValueCell(ref sheet, "O", iRow, row["CENTER_NAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "P", iRow, row["DMSP"].ToString(), false, false);
            iSTT++;
            iRow++;
        }

        private void InsertValueCell(ref ExcelWorksheet worksheet, String strColumnName, int iRow, Object value, Boolean isAdd = false, Boolean isBold = false)
        {
            try
            {
                ExcelCell cell = worksheet.Cells[strColumnName + iRow];
                SetValueRange(ref cell, value, isAdd, isBold);
            }
            catch { }
        }
        private void SetValueRange(ref ExcelCell cell, Object value, Boolean isAdd, Boolean isBold)
        {
            try
            {
                if (isBold)
                    cell.Style.Font.Weight = ExcelFont.BoldWeight;
            }
            catch { }

            if (cell != null)
            {
                try
                {
                    if (isAdd)
                        cell.Value = cell.Value.ToString() + " " + value;
                    else
                        cell.Value = value;

                    if (value.ToString().StartsWith("="))
                        cell.Formula = value.ToString();
                }
                catch { }
            }
        }
    }
}
